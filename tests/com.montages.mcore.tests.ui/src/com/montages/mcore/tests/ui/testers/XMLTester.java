package com.montages.mcore.tests.ui.testers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class XMLTester {

	public void test(IFile file, DefaultHandler handler) {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser saxParser = null;

		try {
			saxParser = factory.newSAXParser();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
		
		if (file.exists()) {
			try {
				saxParser.parse(file.getContents(), handler);
			} catch (IOException e) {
				e.printStackTrace();
			} catch (CoreException e) {
				e.printStackTrace();
			} catch (SAXException e) {
				// Found it
			}
		}
	}

	public static class TestComponentExtensionHandler extends DefaultHandler {
		private final String modelURI;
		private final String modelLocation;
		
		public TestComponentExtensionHandler(String modelURI, String modelLocation) {
			this.modelURI = modelURI;
			this.modelLocation = modelLocation;
		}

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals("component")) {
				assertNotNull(attributes.getValue("uri"));
				assertNotNull(attributes.getValue("model") );
				assertEquals(modelURI, attributes.getValue("uri"));
				assertEquals(modelLocation, attributes.getValue("model"));
			}
		}
	}
	
	public static class TestEditorExtensionHandler extends DefaultHandler {
		private final String editorURI;
		private final String location;

		public TestEditorExtensionHandler(String uri, String location) {
			this.editorURI = uri;
			this.location = location;
		}

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals("editorConfigModelAssociation")) {
				assertNotNull(attributes.getValue("uri"));
				assertNotNull(attributes.getValue("config") );
				assertEquals(editorURI, attributes.getValue("uri"));
				assertEquals(location, attributes.getValue("config"));
			}
		}
	}
	
	public static class TestEcoreHrefFromMcoreHandler extends DefaultHandler {
		
		private final String expected;

		public TestEcoreHrefFromMcoreHandler(String expected) {
			this.expected = expected;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.startsWith("internalE")) {
				assertNotNull(attributes.getValue("href"));
				String href = attributes.getValue("href");
				String actual = href.split("#")[0];
				assertEquals(expected, actual);
			}
		}
	}
	
	public static class TestEcoreHrefFromEditorHandler extends DefaultHandler {
		
		private final String expected;

		public TestEcoreHrefFromEditorHandler(String expected) {
			this.expected = expected;
		}

		@Override
		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (attributes.getValue("href") != null) {
				String href = attributes.getValue("href");
				String actual = href.split("#")[0];
				assertEquals(expected, actual);
			}
		}
	}

}
