package com.montages.mcore.tests.ui.conditions;

import org.eclipse.swtbot.swt.finder.waits.ICondition;

public interface ListenerCondition extends ICondition {
	void removeListener();
	void addListener(); 
}
