package com.montages.mcore.tests.ui.tests;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.swtbot.swt.finder.junit.SWTBotJunit4ClassRunner;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.montages.mcore.tests.ui.conditions.WaitForBuildCondition;
import com.montages.mcore.tests.ui.mock.ComponentMock;
import com.montages.mcore.tests.ui.testers.ComponentProjectTester;
import com.montages.mcore.tests.ui.testers.URIMappingTester;
import com.montages.mcore.tests.ui.testers.XMLTester;
import com.montages.mcore.tests.ui.testers.XMLTester.TestComponentExtensionHandler;
import com.montages.mcore.tests.ui.testers.XMLTester.TestEcoreHrefFromEditorHandler;
import com.montages.mcore.tests.ui.testers.XMLTester.TestEcoreHrefFromMcoreHandler;
import com.montages.mcore.tests.ui.testers.XMLTester.TestEditorExtensionHandler;

@RunWith(SWTBotJunit4ClassRunner.class)
public class CreateProjectTest extends LongOperationTest {

	private final XMLTester xmlTester = new XMLTester();
	private final ComponentProjectTester projectTester = new ComponentProjectTester();
	private final URIMappingTester mappingTester = new URIMappingTester();

	@Test
	public void testCreateDefaultProject() {
		bot.menu("File").menu("New").menu("MComponent").click();
		execFinish(bot, 10000, new WaitForBuildCondition("mycomponent", "mypackage"));

		ComponentMock expected = new ComponentMock("langlets.org", "My Component");
		projectTester.test(expected, 
				"org.langlets.mycomponent", 
				"mycomponent.mcore", 
				"mypackage.ecore", 
				"mypackage.editorconfig");

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IFile pluginFile = root.getProject(expected.project()).getFile(new Path("model/plugin.xml"));
		xmlTester.test(pluginFile, 
				new TestComponentExtensionHandler(
						"http://www.langlets.org/myComponent", 
						"model/mycomponent.mcore"));
		xmlTester.test(pluginFile, 
				new TestEditorExtensionHandler(
						"http://www.langlets.org/myComponent/MyPackage", 
						"model/mypackage.editorconfig"));

		mappingTester.test(expected, 
				"mcore://www.langlets.org/myComponent/",
				"mcore://www.langlets.org/myComponent/mycomponent.mcore",
				"platform:/resource/org.langlets.mycomponent/model/", 
				"platform:/resource/org.langlets.mycomponent/model/mycomponent.mcore");

		IFile mcoreFile = root.getProject(expected.project()).getFile(new Path("model/mycomponent.mcore"));
		IFile editorFile = root.getProject(expected.project()).getFile(new Path("model/mypackage.editorconfig"));

		xmlTester.test(mcoreFile, new TestEcoreHrefFromMcoreHandler("mypackage.ecore"));
		xmlTester.test(editorFile, 
				new TestEcoreHrefFromEditorHandler("http://www.langlets.org/myComponent/MyPackage"));
	}

	@Test
	public void testCreateProjectWithComponentNameDifferentCases() {
		bot.menu("File").menu("New").menu("MComponent").click();
		bot.textWithLabel("Domain Type:").setText("com");
		bot.textWithLabel("Domain Name:").setText("foofoo");
		bot.textWithLabel("Component Name:").setText("I am a ComPonent");
		execFinish(bot, 10000, new WaitForBuildCondition("iamacomponent", "mypackage"));

		ComponentMock expected = new ComponentMock("foofoo.com", "I am a ComPonent");
		projectTester.test(expected, 
				"com.foofoo.iamacomponent", 
				"iamacomponent.mcore", 
				"mypackage.ecore", 
				"mypackage.editorconfig");

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IFile pluginFile = root.getProject(expected.project()).getFile(new Path("model/plugin.xml"));

		xmlTester.test(pluginFile, 
				new TestComponentExtensionHandler(
						"http://www.foofoo.com/iamaComPonent", 
						"model/iamacomponent.mcore"));
		xmlTester.test(pluginFile, 
				new TestEditorExtensionHandler(
						"http://www.langlets.org/iamaComPonent/MyPackage", 
						"model/mypackage.editorconfig"));
	}

}
