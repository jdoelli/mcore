package com.montages.mcore.tests.ui.mock;

import org.eclipse.emf.common.util.URI;

import com.montages.mcore.MComponent;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.util.URIService;

public class ComponentMock {

	private final MComponent component;
	private final URIService uriService =  new URIService();

	public ComponentMock(String domain, String name) {
		this.component = McoreFactory.eINSTANCE.createMComponent();
		this.component.setDomainName(domain.split("\\.")[0]);
		this.component.setDomainType(domain.split("\\.")[1]);
		this.component.setName(name);
	}

	public String project() {
		return component.getDerivedBundleName();
	}

	public URI uri() {
		return uriService.createComponentLocationURI(component);
	}

	public URI fullURI() {
		return uriService.createComponentLocationURI(component).appendSegment(fileName());
	}

	public URI folderURI() {
		return uriService.createComponentLocationURI(component).appendSegment("");
	}

	public URI locationURI() {
		return uriService.createPlatformResource(component);
	}

	public String location() {
		return "model/" + fileName();
	}

	public String fileName() {
		return component.getName().trim().replaceAll("\\s", "").toLowerCase() + ".mcore";
	}

}
