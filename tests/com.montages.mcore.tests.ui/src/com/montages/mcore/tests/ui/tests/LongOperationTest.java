package com.montages.mcore.tests.ui.tests;

import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;

import com.montages.mcore.tests.ui.conditions.ListenerCondition;

public abstract class LongOperationTest extends BotTest {

	public void execFinish(SWTWorkbenchBot bot, int timeout, ListenerCondition condition) {
		condition.addListener();
		try {
			bot.button("Finish").click();
			bot.waitUntil(condition, timeout);
		} finally {
			condition.removeListener();
		}
	}

}
