package com.montages.mcore.tests.ui.tests;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.jface.bindings.keys.KeyStroke;
import org.eclipse.swt.SWT;
import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.finders.ContextMenuHelper;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotMenu;
import org.junit.Test;

import com.montages.mcore.tests.ui.conditions.WaitForBuildCondition;
import com.montages.mcore.tests.ui.mock.ComponentMock;
import com.montages.mcore.tests.ui.testers.XMLTester;
import com.montages.mcore.tests.ui.testers.XMLTester.TestComponentExtensionHandler;
import com.montages.mcore.tests.ui.testers.XMLTester.TestEcoreHrefFromMcoreHandler;

public class ComponentReferencesTest extends LongOperationTest {

	private final XMLTester xmlTester = new XMLTester();

	@Test
	public void testCreateTwoComponentsWithReferences() {
		// component a
		bot.menu("File").menu("New").menu("MComponent").click();
		bot.textWithLabel("Domain Type:").setText("com");
		bot.textWithLabel("Domain Name:").setText("foo");
		bot.textWithLabel("Component Name:").setText("a");
		execFinish(bot, 10000, new WaitForBuildCondition("a", "mypackage"));

		// component b
		bot.menu("File").menu("New").menu("MComponent").click();
		bot.textWithLabel("Domain Type:").setText("com");
		bot.textWithLabel("Domain Name:").setText("foo");
		bot.textWithLabel("Component Name:").setText("b");
		execFinish(bot, 10000, new WaitForBuildCondition("b", "mypackage"));

		ComponentMock expectedA = new ComponentMock("foo.com", "a");
		ComponentMock expectedB = new ComponentMock("foo.com", "b");

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IFile mcoreA = root.getProject(expectedA.project()).getFile(new Path("model/a.mcore"));
		IFile mcoreB = root.getProject(expectedB.project()).getFile(new Path("model/b.mcore"));
		IFile pluginA = root.getProject(expectedA.project()).getFile(new Path("model/plugin.xml"));
		IFile pluginB = root.getProject(expectedB.project()).getFile(new Path("model/plugin.xml"));

		xmlTester.test(pluginA, new TestComponentExtensionHandler("http://www.foo.com/a", "model/a.mcore"));
		xmlTester.test(pluginB, new TestComponentExtensionHandler("http://www.foo.com/b", "model/b.mcore"));
		xmlTester.test(mcoreA, new TestEcoreHrefFromMcoreHandler("mypackage.ecore"));
		xmlTester.test(mcoreB, new TestEcoreHrefFromMcoreHandler("mypackage.ecore"));

		bot.tree()
		.getTreeItem("b.mcore")
		.select()
		.contextMenu("Load Resource...")
		.click();

		bot.table().select("http://www.foo.com/a");
		bot.button("OK").click();

		bot.tree()
		.getTreeItem("b.mcore")
		.expand()
		.getNode("b")
		.expand()
		.getNode("myPackage")
		.expand()
		.getNode("MyRootObject")
		.expand()
		.contextMenu("New Child")
		.menu("MProperty")
		.click();

		SWTBot propertiesBot = bot.viewByPartName("Properties").bot();
		propertiesBot.tree().getTreeItem("Typing").getNode("Type").click(1);
		propertiesBot.ccomboBox().setSelection(2);
		propertiesBot.ccomboBox().pressShortcut(KeyStroke.getInstance(SWT.CR));

		bot.activeEditor().save();

		executeGenerateEcore("http://www.foo.com/b");

		xmlTester.test(mcoreA, new TestEcoreHrefFromMcoreHandler("mypackage.ecore"));
		xmlTester.test(mcoreB, new TestEcoreHrefFromMcoreHandler("mypackage.ecore"));
	}

	private void executeGenerateEcore(String component) {
		final SWTBot explorerBot = bot.viewByPartName("MComponent Explorer").bot();
		explorerBot.table()
		.getTableItem(component + " [workspace]").select();

		new SWTBotMenu(
				ContextMenuHelper.contextMenu(explorerBot.table(), "Ecore", "Generate")
		).click();

		bot.activeShell().bot().button("OK").click();
	}

}
