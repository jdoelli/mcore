package com.montages.mcore.tests.ui.tests;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.swtbot.eclipse.finder.SWTWorkbenchBot;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import com.montages.mcore.perspective.McorePerspective;

public abstract class BotTest {

	protected static SWTWorkbenchBot bot;

	@BeforeClass
	public static void beforeClass() throws Exception {
		bot = new SWTWorkbenchBot();
		try {
			bot.viewByTitle("Welcome").close();
		} catch (Exception e) {
			// Welcome view not there
		}
		bot.perspectiveById(McorePerspective.ID).activate();
	}

	@Before
	public void before() {
		bot = new SWTWorkbenchBot();
	}
	
	@After
	public void after() throws CoreException {
//		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
//		for (IProject project: root.getProjects()) {
//			project.delete(true, new NullProgressMonitor());
//		}
	}

	@AfterClass
	public static void sleep() {
		bot.sleep(2000);
	}
	
}
