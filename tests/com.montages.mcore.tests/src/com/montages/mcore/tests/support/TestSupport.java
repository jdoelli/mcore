package com.montages.mcore.tests.support;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;

public class TestSupport {

	protected final URI baseURI = URI.createURI("platform:/plugin/com.montages.mcore.tests/tests");

	protected Resource loadResource(String fileName) {
		return loadResource(fileName, Collections.<URI, URI> emptyMap());
	}
	
	protected Resource loadResource(String fileName, Map<URI, URI> uriMap) {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getURIConverter().getURIMap().putAll(uriMap);
		return resourceSet.getResource(baseURI.appendSegment(fileName), true);
	}

	protected MComponent loadComponent(String fileName) {
		Resource resource = loadResource(fileName);
		assertEquals(1, resource.getContents().size());
		assertEquals(McorePackage.Literals.MCOMPONENT, resource.getContents().get(0).eClass());

		return (MComponent) resource.getContents().get(0);
	}

	protected List<EPackage> getResults(MComponent component) throws CoreException {
		List<EPackage> result = new ArrayList<EPackage>();
		Mcore2Ecore t = new Mcore2Ecore();

		t.transform(component.eResource().getResourceSet(), component);

		for (MPackage p: component.getOwnedPackage()) {
			result.add(p.getInternalEPackage());
		}

		return result;
	}

	protected MComponent createComponent(String name, String domain, String type) {
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource r = resourceSet.createResource(URI.createURI("test.mcore"));
		MComponent c = McoreFactory.eINSTANCE.createMComponent();
		c.setName(name);
		c.setDomainName(domain);
		c.setDomainType(type);
		r.getContents().add(c);

		return c;
	}

	protected MPackage createPackage(MComponent c, String name) {
		MPackage p = McoreFactory.eINSTANCE.createMPackage();
		p.setName(name);
		c.getOwnedPackage().add(p);
		return p;
	}

	protected MClassifier createClassifier(MPackage p, String name) {
		MClassifier c = McoreFactory.eINSTANCE.createMClassifier();
		c.setName(name);
		p.getClassifier().add(c);

		return c;
	}

	protected MProperty createProperty(MClassifier c, String name, SimpleType type) {
		MProperty p = McoreFactory.eINSTANCE.createMProperty();
		p.setName(name);
		p.setSimpleType(type);
		c.getProperty().add(p);
		return p;
	}

	protected MProperty createProperty(MClassifier c, String name, MClassifier type) {
		MProperty p = McoreFactory.eINSTANCE.createMProperty();
		p.setName(name);
		p.setType(type);
		c.getProperty().add(p);
		return p;
	}

	protected MPackage getCoreData(ResourceSet resourceSet) {
		Resource resource = resourceSet.getResource(URI.createURI("mcore://www.langlets.org/coreTypes/coretypes.mcore"), true);
		MComponent c = (MComponent) resource.getContents().get(0);
		return c.getOwnedPackage().get(0).getSubPackage().get(0);
	}

	protected MClassifier find(MPackage p, String name) {
		for (MClassifier c: p.getClassifier()) {
			if (c.getName().equals(name)) {
				return c;
			}
		}
		return null;
	}

}
