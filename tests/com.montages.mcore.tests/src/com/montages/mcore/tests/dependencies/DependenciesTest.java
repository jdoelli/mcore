package com.montages.mcore.tests.dependencies;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.tests.support.TestSupport;
import com.montages.mcore.util.Dependencies;

public class DependenciesTest extends TestSupport {

	@Test
	public void testSingleDependOnCoreTypes() {
		Resource res =  loadResource("withcoretype.mcore", Collections.singletonMap(
				URI.createURI("http://www.langlets.org/coreTypes"), 
				URI.createURI("platform:/plugin/org.langlets.coretypes/model/coretypes.mcore")));

		MComponent c = (MComponent) res.getContents().get(0);

		assertNotNull(c);
		assertEquals(1, c.getOwnedPackage().size());

		Dependencies dep = Dependencies.compute(c);
		assertEquals(1, dep.getMap().keySet().size());
		assertEquals(1, dep.sort().size());
	}

}
