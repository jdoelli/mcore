package com.montages.mcore.tests.editorconfig;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Test;
import org.xocl.ecore2editorconfig.DefaultGenerator;
import org.xocl.ecore2editorconfig.Generator;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.tests.support.TestSupport;

public class GenerateEditorConfigTest extends TestSupport {

	@Test
	public void testOrderOfColumns() throws CoreException {
		MComponent comp = createTest();
		ResourceSet resourceSet = comp.eResource().getResourceSet();

		EPackage p = ecore(comp.getOwnedPackage().get(0));

		DefaultGenerator generator = new DefaultGenerator(resourceSet);
		generator.initialize(Collections.singletonList(p));
		EditorConfig config = generator.generate(p);

		assertNotNull(config);
		assertEquals(3, config.getTableConfig().size());

		TableConfig c1 = config.getTableConfig().get(0);
		TableConfig c2 = config.getTableConfig().get(1);
		TableConfig c3 = config.getTableConfig().get(2);

		assertEquals(p.getEClassifier("A"), c1.getIntendedClass());
		assertEquals(p.getEClassifier("B"), c2.getIntendedClass());
		assertEquals(p.getEClassifier("C"), c3.getIntendedClass());

		assertEquals(2, c1.getColumn().size());
		assertEquals(2, c2.getColumn().size());
		assertEquals(2, c3.getColumn().size());

		ColumnConfig c1c1 = c1.getColumn().get(0);
		ColumnConfig c1c2 = c1.getColumn().get(1);

		EClass a = (EClass) p.getEClassifier("A");

		RowFeatureCell cell1 = (RowFeatureCell) c1c1.getCellConfig(a);
		RowFeatureCell cell2 = (RowFeatureCell) c1c2.getCellConfig(a);

		assertNotNull(cell1);
		assertEquals(a.getEStructuralFeature("a1"), cell1.getFeature());

		assertNotNull(cell2);
		assertEquals(a.getEStructuralFeature("a2"), cell2.getFeature());
	}
	
	@Test
	public void testGetAllStructuralFeatures() {
		MComponent comp = createTest();
		EPackage p = ecore(comp.getOwnedPackage().get(0));

		EClass a = (EClass) p.getEClassifier("A");
		EClass b = (EClass) p.getEClassifier("B");
		EClass c = (EClass) p.getEClassifier("C");
		EStructuralFeature f1, f2, f3, f4, f5, f6;

		Generator generator = new DefaultGenerator(comp.eResource().getResourceSet());
		generator.initialize(Collections.singletonList(p));
		
		Collection<EStructuralFeature> features = generator.getStructuralFeatures(a);
		Iterator<EStructuralFeature> it = features.iterator();

		assertEquals(2, features.size());
		f1 = it.next();
		f2 = it.next();
		assertEquals(a.getEStructuralFeature("a1"), f1);
		assertEquals(a.getEStructuralFeature("a2"), f2);

		features = generator.getStructuralFeatures(b);
		it = features.iterator();

		assertEquals(2, features.size());
		f1 = it.next();
		f2 = it.next();
		assertEquals(b.getEStructuralFeature("b1"), f1);
		assertEquals(b.getEStructuralFeature("b2"), f2);

		// Add hierarchy
		// A <- B <- C
		//
		b.getESuperTypes().add(a);
		c.getESuperTypes().add(b);

		generator.initialize(Collections.singletonList(p));

		// A should have is features first followed by the one 
		// from B and then C
		features = generator.getStructuralFeatures(a);
		it = features.iterator();

		assertEquals(6, features.size());
		f1 = it.next();
		f2 = it.next();
		f3 = it.next();
		f4 = it.next();
		f5 = it.next();
		f6 = it.next();
		assertEquals(a.getEStructuralFeature("a1"), f1);
		assertEquals(a.getEStructuralFeature("a2"), f2);
		assertEquals(b.getEStructuralFeature("b1"), f3);
		assertEquals(b.getEStructuralFeature("b2"), f4);
		assertEquals(c.getEStructuralFeature("c1"), f5);
		assertEquals(c.getEStructuralFeature("c2"), f6);

		// B should have A features first followed by is 
		// and then Cs
		features = generator.getStructuralFeatures(b);
		it = features.iterator();

		assertEquals(6, features.size());
		f1 = it.next();
		f2 = it.next();
		f3 = it.next();
		f4 = it.next();
		f5 = it.next();
		f6 = it.next();
		assertEquals(a.getEStructuralFeature("a1"), f1);
		assertEquals(a.getEStructuralFeature("a2"), f2);
		assertEquals(b.getEStructuralFeature("b1"), f3);
		assertEquals(b.getEStructuralFeature("b2"), f4);
		assertEquals(c.getEStructuralFeature("c1"), f5);
		assertEquals(c.getEStructuralFeature("c2"), f6);

		// C should have A and B features first followed by is
		//
		features = generator.getStructuralFeatures(b);
		it = features.iterator();

		assertEquals(6, features.size());
		f1 = it.next();
		f2 = it.next();
		f3 = it.next();
		f4 = it.next();
		f5 = it.next();
		f6 = it.next();
		assertEquals(a.getEStructuralFeature("a1"), f1);
		assertEquals(a.getEStructuralFeature("a2"), f2);
		assertEquals(b.getEStructuralFeature("b1"), f3);
		assertEquals(b.getEStructuralFeature("b2"), f4);
		assertEquals(c.getEStructuralFeature("c1"), f5);
		assertEquals(c.getEStructuralFeature("c2"), f6);
	}
	
	@Test
	public void testGeneratorInitialize() {
		MComponent comp = createTest();
		EPackage p = ecore(comp.getOwnedPackage().get(0));

		final EClass a = (EClass) p.getEClassifier("A");
		final EClass b = (EClass) p.getEClassifier("B");
		final EClass c = (EClass) p.getEClassifier("C");

		Generator generator = new DefaultGenerator(comp.eResource().getResourceSet()) {
			@Override
			public void initialize(List<EPackage> ePackages) {
				super.initialize(ePackages);

				assertEquals(3, mapOfSubTypes.keySet().size());
				assertTrue(mapOfSubTypes.containsKey(a));
				assertTrue(mapOfSubTypes.containsKey(b));
				assertTrue(mapOfSubTypes.containsKey(c));
				assertEquals(0, mapOfSubTypes.get(a).size());
				assertEquals(0, mapOfSubTypes.get(b).size());
				assertEquals(0, mapOfSubTypes.get(c).size());
			}
		};

		generator.initialize(Collections.singletonList(p));
	}

	private MComponent createTest() {
		MComponent comp = createComponent("test", "foo", "org");
		MPackage p = createPackage(comp, "p");

		MClassifier a = createClassifier(p, "A");
		createProperty(a, "a1", SimpleType.STRING);
		createProperty(a, "a2", SimpleType.STRING);

		MClassifier b = createClassifier(p, "B");
		createProperty(b, "b1", SimpleType.STRING);
		createProperty(b, "b2", SimpleType.STRING);
		
		MClassifier c = createClassifier(p, "C");
		createProperty(c, "c1", SimpleType.STRING);
		createProperty(c, "c2", SimpleType.STRING);

		return comp;
	}

	private EPackage ecore(MPackage p) {
		try {
			new Mcore2Ecore().transform(p.eResource().getResourceSet(), p.getContainingComponent());
		} catch (CoreException e) {
			e.printStackTrace();
		}

		assertNotNull(p.getInternalEPackage());
		assertNotNull(p.getInternalEPackage().eResource());

		ResourceSet resourceSet = p.getContainingComponent().eResource().getResourceSet();
		Resource resource = resourceSet.createResource(URI.createURI(p.getInternalEPackage().getNsURI()));
		resource.getContents().add(p.getInternalEPackage());

		return p.getInternalEPackage();
	}
	

}
