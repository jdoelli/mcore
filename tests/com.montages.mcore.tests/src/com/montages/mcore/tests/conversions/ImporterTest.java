package com.montages.mcore.tests.conversions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.Collection;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.junit.Ignore;
import org.junit.Test;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.McorePackage;
import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.tests.support.TestSupport;
import com.montages.mcore.util.ResourceService;

@Ignore
public class ImporterTest extends TestSupport {

//	@Test
//	public void testImportRepoWithCoreTypes() throws IOException {
//		ResourceService service = new ResourceService();
//		ResourceSet resourceSet = service.get();
//		service.addPluginMap(resourceSet);
//
//		Resource resource = resourceSet.getResource(baseURI.appendSegment("repowithcoretype.mcore"), true);
//
//		assertNotNull(resource);
//		assertEquals(1, resource.getContents().size());
//		assertEquals(McorePackage.Literals.MREPOSITORY, resource.getContents().get(0).eClass());
//
//		MRepository repository = (MRepository) resource.getContents().get(0);
//
//		Collection<MComponent> components = new Importer().importFrom(repository, null);
//
//		assertEquals(1, components.size());
//
//		MComponent c = components.iterator().next();
//
//		assertEquals(1, c.getOwnedPackage().size());
//		assertEquals(1, c.getOwnedPackage().get(0).getClassifier().size());
//		assertEquals(1, c.getOwnedPackage().get(0).getClassifier().get(0).getProperty().size());
//
//		MProperty p = c.getOwnedPackage().get(0).getClassifier().get(0).getProperty().get(0);
//		MClassifier type = p.getType();
//
//		assertEquals("String", type.getName());
//		assertEquals(URI.createURI("mcore://www.langlets.org/coreTypes/coretypes.mcore"), type.eResource().getURI());
//		assertEquals(resourceSet.getResource(URI.createURI("mcore://www.langlets.org/coreTypes/coretypes.mcore"), true), type.eResource());
//
////		c.eResource().save(System.out, service.getSaveOptions());
//	}

}
