package com.montages.mcore.tests.conversions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ExtensibleURIConverterImpl;
import org.junit.Ignore;
import org.junit.Test;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.codegen.ui.conversions.Exporter;
import com.montages.mcore.tests.support.TestSupport;

@Ignore
public class ExporterTest extends TestSupport {

//	@Test
//	public void testExportSingle() {
//		MComponent c = createComponent("foo", "bar", "org");
//		Exporter e = new Exporter(new ExtensibleURIConverterImpl());
//		MRepository r = e.export(c);
//
//		assertNotNull(r);
//		assertNotNull(r.eResource());
//
//		assertEquals(1, r.getComponent().size());
//
//		MComponent ec = r.getComponent().get(0);
//		assertNotEquals(c, ec);
//
//		assertEquals("foo", ec.getName());
//		assertEquals("bar", ec.getDomainName());
//		assertEquals("org", ec.getDomainType());
//	}
//
//	@Test
//	public void testExportSingleFromURI() {
//		Exporter e = new Exporter(new ExtensibleURIConverterImpl());
//		MRepository r = e.export(baseURI.appendSegment("mycomponent.mcore"));
//
//		assertNotNull(r);
//		assertNotNull(r.eResource());
//
//		assertEquals(1, r.getComponent().size());
//
//		MComponent ec = r.getComponent().get(0);
//
//		assertEquals("My Component", ec.getName());
//		assertEquals("langlets", ec.getDomainName());
//		assertEquals("org", ec.getDomainType());
//	}
//
//	@Test
//	public void testExportTwo() throws IOException {
//		MComponent c1 = createComponent("foo1", "bar", "org");
//		MComponent c2 = createComponent("foo2", "bar", "org");
//
//		createModelExportTwo(c1, c2);
//
//		Exporter e = new Exporter(new ExtensibleURIConverterImpl());
//		MRepository r = e.export(c2);
//
//		assertNotNull(r);
//		assertNotNull(r.eResource());
//
//		assertEquals(2, r.getComponent().size());
//
//		MComponent first = r.getComponent().get(1);
//
//		assertEquals("foo1", first.getName());
//		assertEquals("bar", first.getDomainName());
//		assertEquals("org", first.getDomainType());
//		assertNotEquals(c1, first);
//		assertEquals(1, first.getOwnedPackage().size());
//		MPackage cp1 = first.getOwnedPackage().get(0);
//		assertEquals("p1", cp1.getName());
//		assertEquals(1, cp1.getClassifier().size());
//		MClassifier cc1 = cp1.getClassifier().get(0);
//		assertEquals("A", cc1.getName());
//
//		MComponent second = r.getComponent().get(0);
//
//		assertEquals("foo2", second.getName());
//		assertEquals("bar", second.getDomainName());
//		assertEquals("org", second.getDomainType());
//		assertNotEquals(c2, second);
//		assertEquals(1, second.getOwnedPackage().size());
//		MPackage cp2 = second.getOwnedPackage().get(0);
//		assertEquals("p2", cp2.getName());
//		assertEquals(1, cp2.getClassifier().size());
//		MClassifier cc2 = cp2.getClassifier().get(0);
//		assertEquals("B", cc2.getName());
//		assertEquals(1, cc2.getProperty().size());
//		MProperty pp2 = cc2.getProperty().get(0);
//		assertEquals("as", pp2.getName());
//		assertEquals(cc1, pp2.getType());
//	}
//	
//	@Test
//	public void testExportSingleWithCoreTypes() throws IOException {
//		MComponent c = createComponent("foo", "bar", "org");
//		createModelExportSingleWithCoreTypes(c);
//
//		Exporter e = new Exporter(new ExtensibleURIConverterImpl());
//		MRepository r = e.export(c);
//
//		assertNotNull(r);
//		assertNotNull(r.eResource());
//
//		assertEquals(1, r.getComponent().size());
//	}
//
//	private void createModelExportTwo(MComponent c1, MComponent c2) {
//		MPackage p1 = McoreFactory.eINSTANCE.createMPackage();
//		p1.setName("p1");
//		MClassifier a = McoreFactory.eINSTANCE.createMClassifier();
//		a.setName("A");
//		p1.getClassifier().add(a);
//		c1.getOwnedPackage().add(p1);
//
//		MPackage p2 = McoreFactory.eINSTANCE.createMPackage();
//		p2.setName("p2");
//		MClassifier b = McoreFactory.eINSTANCE.createMClassifier();
//		b.setName("B");
//		MProperty as = McoreFactory.eINSTANCE.createMProperty();
//		as.setName("as");
//		as.setType(a);
//		b.getProperty().add(as);
//		p2.getClassifier().add(b);
//		c2.getOwnedPackage().add(p2);
//	}
//
//	private void createModelExportSingleWithCoreTypes(MComponent c) {
//		MPackage p = McoreFactory.eINSTANCE.createMPackage();
//		p.setName("p");
//		MClassifier a = McoreFactory.eINSTANCE.createMClassifier();
//		a.setName("A");
//		MProperty string = McoreFactory.eINSTANCE.createMProperty();
//		string.setName("string");
//
//		ResourceSet rs = c.eResource().getResourceSet();
//		rs.getURIConverter().getURIMap().put(
//				URI.createURI("http://www.langlets.org/coreTypes"), 
//				URI.createURI("platform:/plugin/org.langlets.coretypes/model/coretypes.mcore"));
//		Resource types = rs.getResource(URI.createURI("http://www.langlets.org/coreTypes"), true);
//		
//		MComponent core = (MComponent) types.getContents().get(0);
//		MPackage data = core.getOwnedPackage().get(0).getSubPackage().get(0);
//		MClassifier stringType = find(data, "String");
//
//		string.setType(stringType);
//		a.getProperty().add(string);
//		p.getClassifier().add(a);
//		c.getOwnedPackage().add(p);
//	}

}
