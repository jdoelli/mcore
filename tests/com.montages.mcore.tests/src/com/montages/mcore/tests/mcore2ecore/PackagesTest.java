package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EPackage;
import org.junit.Before;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.tests.support.TestSupport;

public class PackagesTest extends TestSupport {

	private MComponent component;
	private List<EPackage> results;

	@Before
	public void tearUp() throws IOException, CoreException {
		component = loadComponent("test-1.mcore");
		results = getResults(component);
	}

	@Test
	public void testNumberOfRootPackages() {
		assertEquals(2, results.size());
		String uri = component.getDerivedURI();
		assertEquals("http://www.component.com/component", uri);

		EPackage one = results.get(0);
		EPackage two = results.get(1);

		assertTrue(one instanceof EPackage);
		assertTrue(two instanceof EPackage);

		EPackage oneP = one.getName().equals("first") ? (EPackage) one : (EPackage) two;
		EPackage twoP = two.getName().equals("second") ? (EPackage) two : (EPackage) one;
		
		assertEquals(1, oneP.getESubpackages().size());
		assertEquals("first", oneP.getName());
		assertEquals("first", oneP.getNsPrefix());
		assertEquals("http://www.component.com/component/First", oneP.getNsURI());
		
		EPackage child = oneP.getESubpackages().get(0);
		assertEquals("child", child.getName());
		assertEquals(0, child.getESubpackages().size());
		assertEquals("first.child", child.getNsPrefix()); // Should be just child
		assertEquals("http://www.component.com/component/First/Child", child.getNsURI());
		
		assertEquals(0, twoP.getESubpackages().size());
		assertEquals("second", twoP.getName());
		assertEquals("http://www.component.com/component", uri);
		assertEquals("second", twoP.getNsPrefix());
		assertEquals("http://www.component.com/component/Second", twoP.getNsURI());
	}

}
