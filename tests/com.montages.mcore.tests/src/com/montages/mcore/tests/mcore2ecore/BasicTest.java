package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.junit.Before;
import org.junit.Test;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.SimpleType;
import com.montages.mcore.tests.support.TestSupport;

public class BasicTest extends TestSupport {

	private MComponent component;
	private List<EPackage> results;

	@Before
	public void tearUp() throws IOException, CoreException {
		component = loadComponent("test-2.mcore");
		results = getResults(component);
	}

	@Test
	public void testPropertiesOfEPackage() {
		assertEquals(1, results.size());
		
		MPackage source = component.getOwnedPackage().get(0);
		EPackage actual = results.get(0);

		assertEquals(source.getEName(), actual.getName());
		try {
			new URL(actual.getNsURI());
		} catch (MalformedURLException e) {
			assertFalse(true);
			e.printStackTrace();
		}

		assertEquals(source.getDerivedNsPrefix(), actual.getNsPrefix());
		assertEquals(source.getDerivedNsURI(), actual.getNsURI());
		
		assertEquals(source.getClassifier().size(), actual.getEClassifiers().size());
		assertNotNull(source.getInternalEPackage());
		assertEquals(source.getInternalEPackage(), actual);
		
		testContentOfEPackage(actual, source);
	}

	public void testContentOfEPackage(EPackage actual, MPackage source) {
		assertEquals(6, actual.getEClassifiers().size());
		EClassifier a = actual.getEClassifier("A");
		EClassifier b = actual.getEClassifier("B");
		EClassifier c = actual.getEClassifier("C");
		EClassifier d = actual.getEClassifier("D");
		EClassifier u = actual.getEClassifier("URI");
		EClassifier k = actual.getEClassifier("MyKind");

		assertNotNull(a);
		assertNotNull(b);
		assertNotNull(c);
		assertNotNull(d);
		assertNotNull(u);
		assertNotNull(k);

		assertTrue(a instanceof EClass);
		assertTrue(b instanceof EClass);
		assertTrue(d instanceof EClass);
		assertTrue(d instanceof EClass);
		assertTrue(u instanceof EDataType);
		assertTrue(k instanceof EEnum);

		testClassA((EClass)a, getClassifier(source, "A"));
		testClassB((EClass)b, getClassifier(source, "B"));
		testClassC((EClass)c, getClassifier(source, "C"));
		testClassD((EClass)d, getClassifier(source, "D"));

		assertTrue(((EClass) c).getESuperTypes().contains(b));
		assertTrue(((EClass) d).getESuperTypes().contains(b));

		assertEquals(2, ((EEnum) k).getELiterals().size());
	}

	public void testClassA(EClass actual, MClassifier source) {
		assertNotNull(source.getInternalEClassifier());
		assertEquals(actual, source.getInternalEClassifier());

		assertEquals("A", actual.getName());
		assertFalse(actual.isAbstract());
		assertEquals(10, actual.getEStructuralFeatures().size());
	}

	public void testClassB(EClass actual, MClassifier source) {
		assertNotNull(source.getInternalEClassifier());
		assertEquals(actual, source.getInternalEClassifier());

		assertEquals("B", actual.getName());
		assertTrue(actual.isAbstract());
		assertEquals(1, actual.getEStructuralFeatures().size());
	}

	public void testClassC(EClass actual, MClassifier source) {
		assertNotNull(source.getInternalEClassifier());
		assertEquals(actual, source.getInternalEClassifier());

		assertEquals("C", actual.getName());
		assertFalse(actual.isAbstract());
		assertEquals(0, actual.getEStructuralFeatures().size());
	}

	public void testClassD(EClass actual, MClassifier source) {
		assertNotNull(source.getInternalEClassifier());
		assertEquals(actual, source.getInternalEClassifier());

		assertEquals("D", actual.getName());
		assertFalse(actual.isAbstract());
		assertEquals(0, actual.getEStructuralFeatures().size());
	}

	@Test
	public void testUnamedAttributeWithSimpleType() throws CoreException {
		MComponent c = createComponent("c", "d", "o");
		MPackage p = McoreFactory.eINSTANCE.createMPackage();
		p.setName("p");
		MClassifier c1 = McoreFactory.eINSTANCE.createMClassifier();
		c1.setName("C1");
		MProperty p1 = McoreFactory.eINSTANCE.createMProperty();
		p1.setSimpleType(SimpleType.STRING);
		c1.getProperty().add(p1);
		p.getClassifier().add(c1);
		c.getOwnedPackage().add(p);

		List<EPackage> result = getResults(c);

		assertEquals(1, result.size());

		EPackage pp = (EPackage) result.get(0);
		EClass cc = (EClass) pp.getEClassifier("C1");

		assertNotNull(cc.getEStructuralFeature("stringProperty"));
	}

	@Test
	public void testUnamedAttributeWithDataType() throws CoreException {
		MComponent c = createComponent("c", "d", "o");

		MPackage p = McoreFactory.eINSTANCE.createMPackage();
		p.setName("p");

		MClassifier c1 = McoreFactory.eINSTANCE.createMClassifier();
		c1.setName("C1");
		MClassifier d1 = McoreFactory.eINSTANCE.createMClassifier();
		d1.setName("D1");

		MProperty p1 = McoreFactory.eINSTANCE.createMProperty();
		p1.setType(d1);

		c1.getProperty().add(p1);
		p.getClassifier().add(c1);
		p.getClassifier().add(d1);
		c.getOwnedPackage().add(p);

		List<EPackage> result = getResults(c);

		assertEquals(1, result.size());
		assertTrue(result.get(0) instanceof EPackage);
		
		EPackage pp = (EPackage) result.get(0);
		assertNotNull(pp.getEClassifier("C1"));
		assertNotNull(pp.getEClassifier("D1"));
		assertTrue(pp.getEClassifier("D1") instanceof EDataType);

		EClass cc = (EClass) pp.getEClassifier("C1");
		
		assertNotNull(cc.getEStructuralFeature("d1"));
	}

	@Test
	public void testUnamedRefereneWithType() throws CoreException {
		MComponent c = createComponent("c", "d", "o");
		
		MPackage p = McoreFactory.eINSTANCE.createMPackage();
		p.setName("p");
		
		MClassifier c1 = McoreFactory.eINSTANCE.createMClassifier();
		c1.setName("C1");
		MClassifier c2 = McoreFactory.eINSTANCE.createMClassifier();
		c2.setName("C2");
		
		MProperty p1 = McoreFactory.eINSTANCE.createMProperty();
		p1.setType(c2);
		
		MProperty p2 = McoreFactory.eINSTANCE.createMProperty();
		p2.setName("id");
		p2.setSimpleType(SimpleType.STRING);
		
		c1.getProperty().add(p1);
		c2.getProperty().add(p2);
		p.getClassifier().add(c1);
		p.getClassifier().add(c2);
		c.getOwnedPackage().add(p);

		List<EPackage> result = getResults(c);
		
		assertEquals(1, result.size());
		assertTrue(result.get(0) instanceof EPackage);
		
		EPackage pp = (EPackage) result.get(0);
		assertNotNull(pp.getEClassifier("C1"));
		assertNotNull(pp.getEClassifier("C2"));		

		EClass cc = (EClass) pp.getEClassifier("C1");
		
		assertNotNull(cc.getEStructuralFeature("c2"));
	}

	private MClassifier getClassifier(MPackage p, String name) {
		for (MClassifier c: p.getClassifier()) {
			if (c.getEName().equals(name))
				return c;
		}
		return null;
	}
}
