package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.junit.Test;

import com.montages.mcore.MComponent;
import com.montages.mcore.tests.support.TestSupport;

public class AttributesTest extends TestSupport {

	private MComponent component;
	private List<EPackage> results;

	@Test
	public void testClassAttributes() throws CoreException {
		component = loadComponent("test-2.mcore");
		results = getResults(component);

		EPackage actual = results.get(0);
		EClassifier a = actual.getEClassifier("A");
		assertNotNull(a);
		assertTrue(a instanceof EClass);
		EClass aC = (EClass) a;
		
		EStructuralFeature id = aC.getEStructuralFeature("id");
		assertNotNull(id);
		assertTrue(id instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEString(), id.getEType());
		assertEquals(1, id.getLowerBound());
		assertEquals(1, id.getUpperBound());
		
		EStructuralFeature _int = aC.getEStructuralFeature("int");
		assertNotNull(_int);
		assertTrue(_int instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEIntegerObject(), _int.getEType());
		assertEquals(0, _int.getLowerBound());
		assertEquals(-1, _int.getUpperBound());
		
		EStructuralFeature _bool = aC.getEStructuralFeature("bool");
		assertNotNull(_bool);
		assertTrue(_bool instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEBooleanObject(), _bool.getEType());
		assertEquals(0, _bool.getLowerBound());
		assertEquals(-1, _bool.getUpperBound());
		
		EStructuralFeature _double = aC.getEStructuralFeature("double");
		assertNotNull(_double);
		assertTrue(_double instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEDoubleObject(), _double.getEType());
		assertEquals(0, _double.getLowerBound());
		assertEquals(-1, _double.getUpperBound());
		
		EStructuralFeature _uri = aC.getEStructuralFeature("uri");
		assertNotNull(_uri);
		assertTrue(_uri instanceof EAttribute);
		assertEquals(actual.getEClassifier("URI"), _uri.getEType());
		assertEquals(0, _uri.getLowerBound());
		assertEquals(-1, _uri.getUpperBound());
		
		EStructuralFeature _kind = aC.getEStructuralFeature("kind");
		assertNotNull(_kind);
		assertTrue(_kind instanceof EAttribute);
		assertEquals(actual.getEClassifier("MyKind"), _kind.getEType());
		assertEquals(0, _kind.getLowerBound());
		assertEquals(-1, _kind.getUpperBound());
		
		EStructuralFeature _a = aC.getEStructuralFeature("a");
		assertNotNull(_a);
		assertTrue(_a instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEString(), _a.getEType());
		assertEquals(0, _a.getLowerBound());
		assertEquals(-1, _a.getUpperBound());
		
		EStructuralFeature _b = aC.getEStructuralFeature("b");
		assertNotNull(_b);
		assertTrue(_b instanceof EAttribute);
		assertEquals(EcorePackage.eINSTANCE.getEBooleanObject(), _b.getEType());
		assertEquals(0, _b.getLowerBound());
		assertEquals(-1, _b.getUpperBound());
	}

	@Test
	public void testSimpleTypes() throws CoreException {
		component = loadComponent("mycomponent.mcore");
		results = getResults(component);

		assertEquals(1, results.size());
		EPackage p = results.get(0);
		assertEquals("http://www.langlets.org/myComponent/MyPackage", p.getNsURI());
		assertEquals("mypackage", p.getName());
		assertEquals("mypackage", p.getNsPrefix());
		
		EClassifier c = p.getEClassifier("MyRootObject");
		assertNotNull(c);
		assertEquals(EcorePackage.Literals.ECLASS, c.eClass());
		
		EClass cl = (EClass) c;
		assertEquals(3, cl.getEAllStructuralFeatures().size());
		
		EStructuralFeature f1 = cl.getEStructuralFeatures().get(0);
		assertEquals("id", f1.getName());
		assertEquals(EcorePackage.Literals.ESTRING, f1.getEType());

		EStructuralFeature f2 = cl.getEStructuralFeatures().get(1);
		assertEquals("stringProperty", f2.getName());
		assertEquals(EcorePackage.Literals.ESTRING, f2.getEType());

		EStructuralFeature f3 = cl.getEStructuralFeatures().get(2);
		assertEquals("fooBar", f3.getName());
		assertEquals(cl, f3.getEType());

		assertEquals(1, f2.getEAnnotations().size());

		EAnnotation a = f2.getEAnnotations().get(0);
		assertEquals("http://www.xocl.org/OCL", a.getSource());
		assertEquals("'HELLO'", a.getDetails().get("derive"));
	}

}
