package com.montages.mcore.tests.mcore2ecore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.junit.Test;

import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.MNamedConstant;
import com.montages.mcore.expressions.MSimpleTypeConstantLet;
import com.montages.mcore.fluent.MFluent;
import com.montages.mcore.fluent.MFluent.MFluentComp;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.tests.support.TestSupport;

public class AnnotationTest extends TestSupport {

	@Test
	public void testResultAnnotationWithUseExplicitOcl() throws CoreException {
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));
		
		MFluent f = new MFluent();
		MFluentComp c = f.createComponent(r, "c1", "test", "org")
				.getOrCreate("p1")
				.getOrCreate("A")
					.addDerivedProperty("bs",
							SimpleType.STRING, 
							false, 
							f.createResultAnnotation("'Hello'"))
					.parent.parent;
		
		List<Resource> outs = new Mcore2Ecore().transform(rs, c.component);
		
		assertEquals(1, outs.size());
		assertEquals(1, outs.get(0).getContents().size());
		assertEquals(EcorePackage.Literals.EPACKAGE, outs.get(0).getContents().get(0).eClass());
		
		EPackage out = (EPackage) outs.get(0).getContents().get(0);
		EClass a = (EClass) out.getEClassifier("A");
		EStructuralFeature feature = a.getEStructuralFeature("bs");

		EAnnotation ann = feature.getEAnnotation("http://www.xocl.org/OCL");
		assertNotNull(ann);
		assertTrue(ann.getDetails().containsKey("derive"));
		assertEquals("'Hello'", ann.getDetails().get("derive"));
	}

	@Test
	public void testResultAnnotationWithoutUseExplicitOcl() throws CoreException {
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));
		
		MFluent f = new MFluent();
		MPropertyAnnotations pp = f.createResultAnnotation("'Hello'");
		pp.getResult().setUseExplicitOcl(false);

		MFluentComp c = f.createComponent(r, "c1", "test", "org")
				.getOrCreate("p1")
				.getOrCreate("A")
					.addDerivedProperty("bs",
							SimpleType.STRING, 
							false, 
							pp)
					.parent.parent;
		
		List<Resource> outs = new Mcore2Ecore().transform(rs, c.component);
		
		assertEquals(1, outs.size());
		assertEquals(1, outs.get(0).getContents().size());
		assertEquals(EcorePackage.Literals.EPACKAGE, outs.get(0).getContents().get(0).eClass());
		
		EPackage out = (EPackage) outs.get(0).getContents().get(0);
		EClass a = (EClass) out.getEClassifier("A");
		EStructuralFeature feature = a.getEStructuralFeature("bs");

		EAnnotation ann = feature.getEAnnotation("http://www.xocl.org/OCL");
		assertNotNull(ann);
		assertTrue(ann.getDetails().containsKey("derive"));
		assertEquals("'Hello'", ann.getDetails().get("derive"));
	}
	
	@Test
	public void testResultAnnotationWithExpression() throws CoreException {
		ResourceSet rs = new ResourceSetImpl();
		Resource r = rs.createResource(URI.createURI("test.mcore"));
		
		MFluent f = new MFluent();
		MPropertyAnnotations pp = f.createResultAnnotation(null);
		pp.getResult().setUseExplicitOcl(false);
		MNamedConstant cc = ExpressionsFactory.eINSTANCE.createMNamedConstant();
		MSimpleTypeConstantLet ll = ExpressionsFactory.eINSTANCE.createMSimpleTypeConstantLet();
		ll.setConstant1("Hello");
		ll.setSimpleType(SimpleType.STRING);
		cc.setExpression(ll);
		pp.getResult().getNamedConstant().add(cc);

		MFluentComp c = f.createComponent(r, "c1", "test", "org")
				.getOrCreate("p1")
				.getOrCreate("A")
					.addDerivedProperty("bs",
							SimpleType.STRING, 
							false, 
							pp)
					.parent.parent;
		
		List<Resource> outs = new Mcore2Ecore().transform(rs, c.component);
		
		assertEquals(1, outs.size());
		assertEquals(1, outs.get(0).getContents().size());
		assertEquals(EcorePackage.Literals.EPACKAGE, outs.get(0).getContents().get(0).eClass());
		
		EPackage out = (EPackage) outs.get(0).getContents().get(0);
		EClass a = (EClass) out.getEClassifier("A");
		EStructuralFeature feature = a.getEStructuralFeature("bs");

		EAnnotation ann = feature.getEAnnotation("http://www.xocl.org/OCL");
		assertNotNull(ann);
		assertTrue(ann.getDetails().containsKey("derive"));
		assertEquals("'Hello'\n", ann.getDetails().get("derive"));
	}
	
}
