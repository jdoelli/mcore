package com.montages.mcore.tests.updates;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class ObjectTupleTest {

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"objecttuple/mypackage.ecore", "objecttuple/My.mypackage");

	@Test
	public void testCreateChildFromTuple() {
		EObject root = fixture.getModel().getContents().get(0);

		assertEquals(4, root.eContents().size());

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(root, root.eClass().getEStructuralFeature("addChild"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(5, root.eContents().size());
	}

}
