package com.montages.mcore.tests.updates;

import static org.eclipse.emf.common.util.URI.createURI;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.junit.Rule;
import org.junit.Test;

public class DynamicUpdatePropagateTest {

	URI typeURI = createURI(
			"http://www.langlets.org/testTriggerUpdateInReferencedObject/TestTriggerUpdateInReferencedObject");

	@Rule
	public DynamicUpdateFixture fixture = new DynamicUpdateFixture( //
			"testtriggerupdateinreferencedobject.ecore", "My.testtriggerupdateinreferencedobject");

	@Test
	public void testUpdateExecuteOnProduction() {
		EObject prod = fixture.getModel().getContents().get(0);
		EObject child = prod.eContents().get(0);

		assertEquals(0, child.eContents().size());

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(prod, prod.eClass().getEStructuralFeature("doTriggerReferencedObject"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(1, child.eContents().size());

		EObject instance = child.eContents().get(0);
		assertEquals("1234", instance.eGet(instance.eClass().getEStructuralFeature("tokenID")));
	}

	@Test
	public void testUpdateExecuteOnChild() {
		EObject child = fixture.getModel().getContents().get(0).eContents().get(0);

		assertEquals(0, child.eContents().size());

		Command command = fixture.getEditingDomain().createCommand(SetCommand.class,
				new CommandParameter(child, child.eClass().getEStructuralFeature("produce"), true));

		assertTrue(command.canExecute());
		command.execute();

		assertEquals(1, child.eContents().size());

		EObject instance = child.eContents().get(0);
		assertEquals("1234", instance.eGet(instance.eClass().getEStructuralFeature("tokenID")));
	}

}
