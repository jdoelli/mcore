package com.montages.mcore.tests.updates;

import static org.eclipse.core.commands.operations.OperationHistoryFactory.getOperationHistory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.transaction.impl.InternalTransaction;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;

import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.McorePackage;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;

public class McoreUpdateTest {

	private McoreDiagramEditingDoamin domain;
	private Resource resource;

	@Before
	public void setUp() throws URISyntaxException, IOException {
		Bundle bundle = Platform.getBundle("com.montages.mcore.tests");
		URL url = bundle.getResource("tests/updates/mcore/mycomponent.mcore");
		URI uri = URI.createURI(FileLocator.resolve(url).toURI().toString());

		domain = new McoreEditingDomainFactory().createEditingDomain(getOperationHistory());
		resource = domain.getResourceSet().getResource(uri, true);
	}

	@Test
	public void testCreateClass() throws InterruptedException {
		InternalTransaction tx = domain.startTransaction(false, Collections.emptyMap());

		MPackage root = (MPackage) resource.getContents().get(0).eContents().get(0);

		assertEquals(1, root.getClassifier().size());

		Command command = SetCommand.create(domain, root, McorePackage.Literals.MPACKAGE__DO_ACTION,
				MPackageAction.CLASS);

		assertTrue(command.canExecute());
		assertTrue(command instanceof CompoundCommand);

		assertEquals(8, ((CompoundCommand) command).getCommandList().size());

		command.execute();

		assertEquals(2, root.getClassifier().size());
		assertEquals("Class 1", root.getClassifier().get(1).getName());

		tx.rollback();
	}

}
