/**
 */
package org.langlets.acore.classifiers.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.classifiers.AFeature;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link org.langlets.acore.classifiers.AFeature} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AFeatureItemProvider extends ATypedItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeatureItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addANamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedNameConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABusinessNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAStoredPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAPersistedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAChangeablePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingClassPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addANamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ANamed_aName_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aName_feature", "_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__ANAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI__80ACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Name Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedNameConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Name Constant feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aUndefinedNameConstant_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aUndefinedNameConstant_feature",
								"_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABusiness Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABusinessNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABusiness Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aBusinessName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aBusinessName_feature",
								"_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AStored feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAStoredPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AStored feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AFeature_aStored_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AFeature_aStored_feature",
								"_UI_AFeature_type"),
						ClassifiersPackage.Literals.AFEATURE__ASTORED, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the APersisted feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAPersistedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the APersisted feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AFeature_aPersisted_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AFeature_aPersisted_feature",
								"_UI_AFeature_type"),
						ClassifiersPackage.Literals.AFEATURE__APERSISTED, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AChangeable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAChangeablePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AChangeable feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AFeature_aChangeable_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AFeature_aChangeable_feature",
						"_UI_AFeature_type"),
				ClassifiersPackage.Literals.AFEATURE__ACHANGEABLE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Feature feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AFeature_aActiveFeature_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AFeature_aActiveFeature_feature",
						"_UI_AFeature_type"),
				ClassifiersPackage.Literals.AFEATURE__AACTIVE_FEATURE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI__80ACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AFeature_aContainingClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AFeature_aContainingClass_feature",
								"_UI_AFeature_type"),
						ClassifiersPackage.Literals.AFEATURE__ACONTAINING_CLASS, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AFeature) object).getTClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AFeature.class)) {
		case ClassifiersPackage.AFEATURE__ANAME:
		case ClassifiersPackage.AFEATURE__AUNDEFINED_NAME_CONSTANT:
		case ClassifiersPackage.AFEATURE__ABUSINESS_NAME:
		case ClassifiersPackage.AFEATURE__ASTORED:
		case ClassifiersPackage.AFEATURE__APERSISTED:
		case ClassifiersPackage.AFEATURE__ACHANGEABLE:
		case ClassifiersPackage.AFEATURE__AACTIVE_FEATURE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ClassifiersItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
