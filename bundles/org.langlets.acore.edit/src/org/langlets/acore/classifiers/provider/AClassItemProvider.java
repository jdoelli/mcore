/**
 */
package org.langlets.acore.classifiers.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link org.langlets.acore.classifiers.AClass} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AClassItemProvider extends AClassifierItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addAAbstractPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASpecializedClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAOperationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAllFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAllOperationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASuperTypesLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAllStoredFeaturePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AAbstract feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAbstractPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAbstract feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aAbstract_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aAbstract_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AABSTRACT, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASpecialized Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASpecializedClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASpecialized Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aSpecializedClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aSpecializedClass_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__ASPECIALIZED_CLASS, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AFeature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFeature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aFeature_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AFEATURE, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AOperation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AOperation feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aOperation_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aOperation_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AOPERATION, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAll Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aAllFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aAllFeature_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AALL_FEATURE, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAll Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Operation feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aAllOperation_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aAllOperation_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AALL_OPERATION, false, false, false, null,
						getString("_UI__80ACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASuper Types Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASuperTypesLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASuper Types Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AClass_aSuperTypesLabel_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AClass_aSuperTypesLabel_feature",
						"_UI_AClass_type"),
				ClassifiersPackage.Literals.ACLASS__ASUPER_TYPES_LABEL, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI__91ACoreHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAll Stored Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllStoredFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Stored Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClass_aAllStoredFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClass_aAllStoredFeature_feature",
								"_UI_AClass_type"),
						ClassifiersPackage.Literals.ACLASS__AALL_STORED_FEATURE, false, false, false, null,
						getString("_UI__91ACoreHelpersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AClass) object).getTClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AClass.class)) {
		case ClassifiersPackage.ACLASS__AABSTRACT:
		case ClassifiersPackage.ACLASS__ASUPER_TYPES_LABEL:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ClassifiersItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
