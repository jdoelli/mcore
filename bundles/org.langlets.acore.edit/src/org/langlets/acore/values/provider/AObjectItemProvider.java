/**
 */
package org.langlets.acore.values.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.langlets.acore.values.AObject;
import org.langlets.acore.values.ValuesPackage;

/**
 * This is the item provider adapter for a {@link org.langlets.acore.values.AObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AObjectItemProvider extends AValueItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAClassPropertyDescriptor(object);
			addASlotPropertyDescriptor(object);
			addAContainingObjectPropertyDescriptor(object);
			addAContainmentReferencePropertyDescriptor(object);
			addAContainingResourcePropertyDescriptor(object);
			addAFeatureWithoutSlotPropertyDescriptor(object);
			addAAllObjectPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AClass feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AClass feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aClass_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__ACLASS, false, false, false, null,
						getString("_UI__80ACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ASlot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASlotPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASlot feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aSlot_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aSlot_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__ASLOT, false, false, false, null,
						getString("_UI__80ACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AContaining Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aContainingObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aContainingObject_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__ACONTAINING_OBJECT, false, false, false, null,
						getString("_UI__80ACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AContainment Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainmentReferencePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContainment Reference feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aContainmentReference_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aContainmentReference_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__ACONTAINMENT_REFERENCE, false, false, false, null,
						getString("_UI__80ACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AContaining Resource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingResourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Resource feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aContainingResource_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aContainingResource_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__ACONTAINING_RESOURCE, false, false, false, null,
						getString("_UI__80ACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AFeature Without Slot feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFeatureWithoutSlotPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFeature Without Slot feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aFeatureWithoutSlot_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aFeatureWithoutSlot_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__AFEATURE_WITHOUT_SLOT, false, false, false, null,
						getString("_UI__91ACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AAll Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AObject_aAllObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AObject_aAllObject_feature",
								"_UI_AObject_type"),
						ValuesPackage.Literals.AOBJECT__AALL_OBJECT, false, false, false, null,
						getString("_UI__91ACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AObject) object).getTClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
