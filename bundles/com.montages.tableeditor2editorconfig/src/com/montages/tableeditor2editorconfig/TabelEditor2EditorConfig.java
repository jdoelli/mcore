package com.montages.tableeditor2editorconfig;

import static org.eclipse.emf.common.util.URI.createPlatformPluginURI;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.WriterLog;
import org.xocl.editorconfig.EditorConfig;

import com.montages.tableeditor2editorconfig.activator.Activator;


public class TabelEditor2EditorConfig {

	private final static String path = "com.montages.tableeditor2editorconfig/transforms/tableeditor2editorconfig/Tableeditor2Editorconfig.qvto";
	private final static URI uri = createPlatformPluginURI(path, false);
	private final TransformationExecutor executor = new TransformationExecutor(uri);

	
	public TabelEditor2EditorConfig() {
		executor.loadTransformation();
	}

	public List<EditorConfig> transform(EObject... contents) throws CoreException {

		final ExecutionContextImpl ctx = new ExecutionContextImpl();
		ctx.setLog(new WriterLog(new OutputStreamWriter(System.out)));

		BasicModelExtent outputs = new BasicModelExtent();
		final ExecutionDiagnostic diag = executor.execute(ctx, new BasicModelExtent(Arrays.asList(contents)), outputs);

		if (Diagnostic.ERROR == diag.getSeverity()) {
			throw new CoreException(BasicDiagnostic.toIStatus(diag));
		}

		ArrayList<EditorConfig> configs = new ArrayList<EditorConfig>();
		for (EObject out: outputs.getContents()) {
			if (out instanceof EditorConfig) {
				configs.add((EditorConfig)out);
			}
		}

		return configs;
	}

	public List<EditorConfig> transform(Resource resource) throws IllegalArgumentException {
		if (resource == null) {
			return Collections.<EditorConfig>emptyList();
		}

		if (!resource.isLoaded()) {
			try {
				resource.load(null);
			} catch (IOException e) {
				Activator.error(e);
				return Collections.<EditorConfig>emptyList();
			}
		}

		if (resource.getContents().size() > 1) {
			throw new IllegalArgumentException("Resource should contain only one root object");
		}

		List<EObject> contents = resource.getContents();
		try {
			return transform(contents.toArray(new EObject[contents.size()]));
		} catch (CoreException e) {
			Activator.error(e);
		}

		return Collections.<EditorConfig>emptyList();
	}
}
