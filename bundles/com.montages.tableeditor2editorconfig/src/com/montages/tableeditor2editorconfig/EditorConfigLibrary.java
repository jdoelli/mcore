package com.montages.tableeditor2editorconfig;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.TableConfig;

import com.montages.mtableeditor.MElementWithFont;


public class EditorConfigLibrary {

	public EditorConfigLibrary() {
		super();
	}

	private static final String FONT_SCHEMA = "font"; //$NON-NLS-1$

	private static final String BOLD = "bold"; //$NON-NLS-1$

	private static final String ITALIC = "italic"; //$NON-NLS-1$

	@Operation(contextual=true)
	public static void transformFont(MElementWithFont elementWithFont, Object config) {
		if (elementWithFont == null) {
			return;
		}
		if (config instanceof TableConfig) {
			((TableConfig)config).setFont(createFontURI(elementWithFont));
		} else if (config instanceof CellConfig) {
			((CellConfig)config).setFont(createFontURI(elementWithFont));
		}
		if (config instanceof ColumnConfig) {
			((ColumnConfig)config).setFont(createFontURI(elementWithFont));
		}
	}

	private static URI createFontURI(MElementWithFont elementWithFont) {
		return createFontURI(getValue(elementWithFont.getItalicFont()), getValue(elementWithFont.getBoldFont()), elementWithFont.getFontSize() == null ? 0 : elementWithFont.getFontSize().getValue() - 3/*mcore enum does not have default value*/);
	}

	private static boolean getValue(Boolean bool) {
		return bool == null ? false : bool.booleanValue();
	}

	private static URI createFontURI(boolean italic, boolean bold, int size) {
		String fontStyle = bold ? BOLD : ""; //$NON-NLS-1$
		if (italic) {
			if (fontStyle.length() > 0) {
				fontStyle += '+';
			}
			fontStyle += ITALIC;
		}
		return URI.createHierarchicalURI(FONT_SCHEMA, "", null, new String[] { size > 0 ? '+'+Integer.toString(size) : (size < 0 ? Integer.toString(size) : ""), fontStyle}, null, null);  //$NON-NLS-1$//$NON-NLS-2$ //$NON-NLS-3$
	}
}
