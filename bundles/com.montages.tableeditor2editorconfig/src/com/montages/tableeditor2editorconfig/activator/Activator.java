package com.montages.tableeditor2editorconfig.activator;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;


public class Activator extends Plugin {

	public static final String PLUGIN_ID = "com.montages.tableeditor2editorconfig";

	private static Activator outInstance;

	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		outInstance = this;
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		outInstance = null;
		super.stop(context);
	}

	public static void error(Throwable t) {
		outInstance.getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, null, t));
	}
}
