/**
 */

package com.montages.mmapper;

import com.montages.acore.classifiers.AFeature;

import com.montages.mrules.MRuleAnnotation;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MMap Entry For Target Feature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature <em>ATarget Feature</em>}</li>
 *   <li>{@link com.montages.mmapper.MMapEntryForTargetFeature#getMExplicitlyUsedForContainmentTargetFeature <em>MExplicitly Used For Containment Target Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mmapper.MmapperPackage#getMMapEntryForTargetFeature()
 * @model annotation="http://www.montages.com/mCore/MCore mName='Map Entry For Target Feature'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aContainingComponentDerive='null\n' aTPackageDerive='null\n' aTClassifierDerive='null\n' aTFeatureDerive='null\n' aTCoreAStringClassDerive='null\n' aClassifierDerive='null\n' aMandatoryDerive='null\n' aSingularDerive='null\n' aTypeLabelDerive='null\n' aUndefinedTypeConstantDerive='null\n' asCodeDerive='null\n' asBasicCodeDerive='null\n' collectorDerive='null\n' containingAnnotationDerive='self\n' containingExpressionDerive='null\n' isComplexExpressionDerive='null\n' isSubExpressionDerive='null\n' aCalculatedOwnTypeDerive='null\n' calculatedOwnMandatoryDerive='null\n' calculatedOwnSingularDerive='null\n' aCalculatedOwnSimpleTypeDerive='null\n' aSelfObjectTypeDerive='eContainer().oclAsType(MMap).aSource\n' aSelfObjectPackageDerive='if aSelfObjectType.oclIsUndefined()\n  then null\n  else aSelfObjectType.aContainingPackage\nendif\n' aTargetObjectTypeDerive='eContainer().oclAsType(MMap).aTarget\n' aTargetSimpleTypeDerive='acore::classifiers::ASimpleType::None\n' aObjectObjectTypeDerive='null\n' aExpectedReturnTypeDerive='null\n' aExpectedReturnSimpleTypeDerive='null\n' isReturnValueMandatoryDerive='null\n' isReturnValueSingularDerive='null\n' baseAsCodeDerive='null\n' chainAsCodeDerive='null\n' aChainCalculatedTypeDerive='null\n' aChainCalculatedSimpleTypeDerive='null\n' chainCalculatedSingularDerive='null\n' processorDefinitionDerive='null\n' typeMismatchDerive='null\n' chainCodeforSubchainsDerive='null\n' isOwnXOCLOpDerive='null\n' aAnnotatedDerive='null\n' aAnnotatingDerive='null\n' aExpectedReturnTypeOfAnnotationDerive='null\n' aExpectedReturnSimpleTypeOfAnnotationDerive='null\n' isReturnValueOfAnnotationMandatoryDerive='null\n' isReturnValueOfAnnotationSingularDerive='null\n' aElement1ChoiceConstruction='\nlet annotatedProp: acore::classifiers::AProperty = \nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)\nin\nif self.aChainEntryType.oclIsUndefined() then \nSequence{} else\nself.aChainEntryType.aAllProperty()->asSequence()\nendif\n\n--'"
 * @generated
 */

public interface MMapEntryForTargetFeature extends MRuleAnnotation {
	/**
	 * Returns the value of the '<em><b>ATarget Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATarget Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATarget Feature</em>' reference.
	 * @see #isSetATargetFeature()
	 * @see #unsetATargetFeature()
	 * @see #setATargetFeature(AFeature)
	 * @see com.montages.mmapper.MmapperPackage#getMMapEntryForTargetFeature_ATargetFeature()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Target Feature'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='let chain: OrderedSet(acore::classifiers::AProperty)  = if aTargetObjectType.oclIsUndefined()\n  then OrderedSet{}\n  else aTargetObjectType.aAllProperty()\nendif in\nchain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() \n else r endif)\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	AFeature getATargetFeature();

	/** 
	 * Sets the value of the '{@link com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature <em>ATarget Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ATarget Feature</em>' reference.
	 * @see #isSetATargetFeature()
	 * @see #unsetATargetFeature()
	 * @see #getATargetFeature()
	 * @generated
	 */

	void setATargetFeature(AFeature value);

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature <em>ATarget Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetATargetFeature()
	 * @see #getATargetFeature()
	 * @see #setATargetFeature(AFeature)
	 * @generated
	 */
	void unsetATargetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature <em>ATarget Feature</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ATarget Feature</em>' reference is set.
	 * @see #unsetATargetFeature()
	 * @see #getATargetFeature()
	 * @see #setATargetFeature(AFeature)
	 * @generated
	 */
	boolean isSetATargetFeature();

	/**
	 * Returns the value of the '<em><b>MExplicitly Used For Containment Target Feature</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mmapper.MMap}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MExplicitly Used For Containment Target Feature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MExplicitly Used For Containment Target Feature</em>' containment reference list.
	 * @see #isSetMExplicitlyUsedForContainmentTargetFeature()
	 * @see #unsetMExplicitlyUsedForContainmentTargetFeature()
	 * @see com.montages.mmapper.MmapperPackage#getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Explicitly Used For Containment Target Feature'"
	 * @generated
	 */
	EList<MMap> getMExplicitlyUsedForContainmentTargetFeature();

	/**
	 * Unsets the value of the '{@link com.montages.mmapper.MMapEntryForTargetFeature#getMExplicitlyUsedForContainmentTargetFeature <em>MExplicitly Used For Containment Target Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMExplicitlyUsedForContainmentTargetFeature()
	 * @see #getMExplicitlyUsedForContainmentTargetFeature()
	 * @generated
	 */
	void unsetMExplicitlyUsedForContainmentTargetFeature();

	/**
	 * Returns whether the value of the '{@link com.montages.mmapper.MMapEntryForTargetFeature#getMExplicitlyUsedForContainmentTargetFeature <em>MExplicitly Used For Containment Target Feature</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MExplicitly Used For Containment Target Feature</em>' containment reference list is set.
	 * @see #unsetMExplicitlyUsedForContainmentTargetFeature()
	 * @see #getMExplicitlyUsedForContainmentTargetFeature()
	 * @generated
	 */
	boolean isSetMExplicitlyUsedForContainmentTargetFeature();

} // MMapEntryForTargetFeature
