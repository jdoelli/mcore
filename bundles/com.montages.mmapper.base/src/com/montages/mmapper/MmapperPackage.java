/**
 */
package com.montages.mmapper;

import com.montages.mrules.MrulesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mmapper.MmapperFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'MMapping\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true' uuidAttributeName='_uuid'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface MmapperPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mmapper.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mmapper";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mMapper/MMapper";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mmapper";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MmapperPackage eINSTANCE = com.montages.mmapper.impl.MmapperPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.mmapper.impl.MMappingImpl <em>MMapping</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mmapper.impl.MMappingImpl
	 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMapping()
	 * @generated
	 */
	int MMAPPING = 0;

	/**
	 * The feature id for the '<em><b>MOwned Map</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAPPING__MOWNED_MAP = 0;

	/**
	 * The feature id for the '<em><b>Mname</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAPPING__MNAME = 1;

	/**
	 * The number of structural features of the '<em>MMapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAPPING_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>MMapping</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAPPING_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mmapper.impl.MMapImpl <em>MMap</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mmapper.impl.MMapImpl
	 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMap()
	 * @generated
	 */
	int MMAP = 1;

	/**
	 * The feature id for the '<em><b>ASource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP__ASOURCE = 0;

	/**
	 * The feature id for the '<em><b>ATarget</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP__ATARGET = 1;

	/**
	 * The feature id for the '<em><b>MMap Entry For Target Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP__MMAP_ENTRY_FOR_TARGET_FEATURE = 2;

	/**
	 * The number of structural features of the '<em>MMap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>MMap</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl <em>MMap Entry For Target Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl
	 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMapEntryForTargetFeature()
	 * @generated
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE = 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__KIND_LABEL = MrulesPackage.MRULE_ANNOTATION__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__RENDERED_KIND_LABEL = MrulesPackage.MRULE_ANNOTATION__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__INDENT_LEVEL = MrulesPackage.MRULE_ANNOTATION__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__DESCRIPTION = MrulesPackage.MRULE_ANNOTATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AS_TEXT = MrulesPackage.MRULE_ANNOTATION__AS_TEXT;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ALABEL = MrulesPackage.MRULE_ANNOTATION__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AKIND_BASE = MrulesPackage.MRULE_ANNOTATION__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ARENDERED_KIND = MrulesPackage.MRULE_ANNOTATION__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACONTAINING_COMPONENT = MrulesPackage.MRULE_ANNOTATION__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_PACKAGE_URI = MrulesPackage.MRULE_ANNOTATION__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_CLASSIFIER_NAME = MrulesPackage.MRULE_ANNOTATION__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_FEATURE_NAME = MrulesPackage.MRULE_ANNOTATION__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_PACKAGE = MrulesPackage.MRULE_ANNOTATION__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_CLASSIFIER = MrulesPackage.MRULE_ANNOTATION__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_FEATURE = MrulesPackage.MRULE_ANNOTATION__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AT_CORE_ASTRING_CLASS = MrulesPackage.MRULE_ANNOTATION__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACLASSIFIER = MrulesPackage.MRULE_ANNOTATION__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AMANDATORY = MrulesPackage.MRULE_ANNOTATION__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASINGULAR = MrulesPackage.MRULE_ANNOTATION__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ATYPE_LABEL = MrulesPackage.MRULE_ANNOTATION__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AUNDEFINED_TYPE_CONSTANT = MrulesPackage.MRULE_ANNOTATION__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AS_CODE = MrulesPackage.MRULE_ANNOTATION__AS_CODE;

	/**
	 * The feature id for the '<em><b>As Basic Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AS_BASIC_CODE = MrulesPackage.MRULE_ANNOTATION__AS_BASIC_CODE;

	/**
	 * The feature id for the '<em><b>Collector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__COLLECTOR = MrulesPackage.MRULE_ANNOTATION__COLLECTOR;

	/**
	 * The feature id for the '<em><b>Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ENTIRE_SCOPE = MrulesPackage.MRULE_ANNOTATION__ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_BASE = MrulesPackage.MRULE_ANNOTATION__SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_SELF = MrulesPackage.MRULE_ANNOTATION__SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_TRG = MrulesPackage.MRULE_ANNOTATION__SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_OBJ = MrulesPackage.MRULE_ANNOTATION__SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_SIMPLE_TYPE_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_LITERAL_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_OBJECT_REFERENCE_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_VARIABLES = MrulesPackage.MRULE_ANNOTATION__SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_ITERATOR = MrulesPackage.MRULE_ANNOTATION__SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_ACCUMULATOR = MrulesPackage.MRULE_ANNOTATION__SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_PARAMETERS = MrulesPackage.MRULE_ANNOTATION__SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Scope Container Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_CONTAINER_BASE = MrulesPackage.MRULE_ANNOTATION__SCOPE_CONTAINER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Number Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_NUMBER_BASE = MrulesPackage.MRULE_ANNOTATION__SCOPE_NUMBER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SCOPE_SOURCE = MrulesPackage.MRULE_ANNOTATION__SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_ENTIRE_SCOPE = MrulesPackage.MRULE_ANNOTATION__LOCAL_ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Local Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_BASE = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Local Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_SELF = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Local Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_TRG = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Local Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_OBJ = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Local Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_SOURCE = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_LITERAL_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_VARIABLES = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Local Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_ITERATOR = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_ACCUMULATOR = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_PARAMETERS = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Local Scope Number Base Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_NUMBER_BASE_DEFINITION = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Local Scope Container</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__LOCAL_SCOPE_CONTAINER = MrulesPackage.MRULE_ANNOTATION__LOCAL_SCOPE_CONTAINER;

	/**
	 * The feature id for the '<em><b>Containing Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CONTAINING_ANNOTATION = MrulesPackage.MRULE_ANNOTATION__CONTAINING_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Containing Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CONTAINING_EXPRESSION = MrulesPackage.MRULE_ANNOTATION__CONTAINING_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Complex Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_COMPLEX_EXPRESSION = MrulesPackage.MRULE_ANNOTATION__IS_COMPLEX_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Sub Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_SUB_EXPRESSION = MrulesPackage.MRULE_ANNOTATION__IS_SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>ACalculated Own Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACALCULATED_OWN_TYPE = MrulesPackage.MRULE_ANNOTATION__ACALCULATED_OWN_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Own Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CALCULATED_OWN_MANDATORY = MrulesPackage.MRULE_ANNOTATION__CALCULATED_OWN_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Own Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CALCULATED_OWN_SINGULAR = MrulesPackage.MRULE_ANNOTATION__CALCULATED_OWN_SINGULAR;

	/**
	 * The feature id for the '<em><b>ACalculated Own Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACALCULATED_OWN_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ACALCULATED_OWN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASELF_OBJECT_TYPE = MrulesPackage.MRULE_ANNOTATION__ASELF_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASELF_OBJECT_PACKAGE = MrulesPackage.MRULE_ANNOTATION__ASELF_OBJECT_PACKAGE;

	/**
	 * The feature id for the '<em><b>ATarget Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_OBJECT_TYPE = MrulesPackage.MRULE_ANNOTATION__ATARGET_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ATarget Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ATARGET_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AObject Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AOBJECT_OBJECT_TYPE = MrulesPackage.MRULE_ANNOTATION__AOBJECT_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AEXPECTED_RETURN_TYPE = MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AEXPECTED_RETURN_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Is Return Value Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_RETURN_VALUE_MANDATORY = MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_MANDATORY;

	/**
	 * The feature id for the '<em><b>Is Return Value Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_RETURN_VALUE_SINGULAR = MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_SINGULAR;

	/**
	 * The feature id for the '<em><b>ASrc Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASRC_OBJECT_TYPE = MrulesPackage.MRULE_ANNOTATION__ASRC_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASrc Object Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ASRC_OBJECT_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ASRC_OBJECT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__BASE_AS_CODE = MrulesPackage.MRULE_ANNOTATION__BASE_AS_CODE;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__BASE = MrulesPackage.MRULE_ANNOTATION__BASE;

	/**
	 * The feature id for the '<em><b>Base Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__BASE_DEFINITION = MrulesPackage.MRULE_ANNOTATION__BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>ABase Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ABASE_VAR = MrulesPackage.MRULE_ANNOTATION__ABASE_VAR;

	/**
	 * The feature id for the '<em><b>ABase Exit Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ABASE_EXIT_TYPE = MrulesPackage.MRULE_ANNOTATION__ABASE_EXIT_TYPE;

	/**
	 * The feature id for the '<em><b>ABase Exit Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ABASE_EXIT_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ABASE_EXIT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base Exit Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__BASE_EXIT_MANDATORY = MrulesPackage.MRULE_ANNOTATION__BASE_EXIT_MANDATORY;

	/**
	 * The feature id for the '<em><b>Base Exit Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__BASE_EXIT_SINGULAR = MrulesPackage.MRULE_ANNOTATION__BASE_EXIT_SINGULAR;

	/**
	 * The feature id for the '<em><b>AChain Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACHAIN_ENTRY_TYPE = MrulesPackage.MRULE_ANNOTATION__ACHAIN_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>Chain As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CHAIN_AS_CODE = MrulesPackage.MRULE_ANNOTATION__CHAIN_AS_CODE;

	/**
	 * The feature id for the '<em><b>AElement1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AELEMENT1 = MrulesPackage.MRULE_ANNOTATION__AELEMENT1;

	/**
	 * The feature id for the '<em><b>Element1 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ELEMENT1_CORRECT = MrulesPackage.MRULE_ANNOTATION__ELEMENT1_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement2 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AELEMENT2_ENTRY_TYPE = MrulesPackage.MRULE_ANNOTATION__AELEMENT2_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AELEMENT2 = MrulesPackage.MRULE_ANNOTATION__AELEMENT2;

	/**
	 * The feature id for the '<em><b>Element2 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ELEMENT2_CORRECT = MrulesPackage.MRULE_ANNOTATION__ELEMENT2_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement3 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AELEMENT3_ENTRY_TYPE = MrulesPackage.MRULE_ANNOTATION__AELEMENT3_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AELEMENT3 = MrulesPackage.MRULE_ANNOTATION__AELEMENT3;

	/**
	 * The feature id for the '<em><b>Element3 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ELEMENT3_CORRECT = MrulesPackage.MRULE_ANNOTATION__ELEMENT3_CORRECT;

	/**
	 * The feature id for the '<em><b>ACast Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACAST_TYPE = MrulesPackage.MRULE_ANNOTATION__ACAST_TYPE;

	/**
	 * The feature id for the '<em><b>ALast Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ALAST_ELEMENT = MrulesPackage.MRULE_ANNOTATION__ALAST_ELEMENT;

	/**
	 * The feature id for the '<em><b>AChain Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACHAIN_CALCULATED_TYPE = MrulesPackage.MRULE_ANNOTATION__ACHAIN_CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>AChain Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ACHAIN_CALCULATED_SIMPLE_TYPE = MrulesPackage.MRULE_ANNOTATION__ACHAIN_CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Chain Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CHAIN_CALCULATED_SINGULAR = MrulesPackage.MRULE_ANNOTATION__CHAIN_CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Processor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__PROCESSOR = MrulesPackage.MRULE_ANNOTATION__PROCESSOR;

	/**
	 * The feature id for the '<em><b>Processor Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__PROCESSOR_DEFINITION = MrulesPackage.MRULE_ANNOTATION__PROCESSOR_DEFINITION;

	/**
	 * The feature id for the '<em><b>Type Mismatch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__TYPE_MISMATCH = MrulesPackage.MRULE_ANNOTATION__TYPE_MISMATCH;

	/**
	 * The feature id for the '<em><b>Call Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CALL_ARGUMENT = MrulesPackage.MRULE_ANNOTATION__CALL_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Sub Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__SUB_EXPRESSION = MrulesPackage.MRULE_ANNOTATION__SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CONTAINED_COLLECTOR = MrulesPackage.MRULE_ANNOTATION__CONTAINED_COLLECTOR;

	/**
	 * The feature id for the '<em><b>Chain Codefor Subchains</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__CHAIN_CODEFOR_SUBCHAINS = MrulesPackage.MRULE_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS;

	/**
	 * The feature id for the '<em><b>Is Own XOCL Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_OWN_XOCL_OP = MrulesPackage.MRULE_ANNOTATION__IS_OWN_XOCL_OP;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AANNOTATED = MrulesPackage.MRULE_ANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AANNOTATING = MrulesPackage.MRULE_ANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>Named Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__NAMED_EXPRESSION = MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Named Tuple</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__NAMED_TUPLE = MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE;

	/**
	 * The feature id for the '<em><b>Named Constant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__NAMED_CONSTANT = MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT;

	/**
	 * The feature id for the '<em><b>AExpected Return Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AEXPECTED_RETURN_TYPE_OF_ANNOTATION = MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type Of Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION = MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY = MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR = MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

	/**
	 * The feature id for the '<em><b>Use Explicit Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__USE_EXPLICIT_OCL = MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL;

	/**
	 * The feature id for the '<em><b>Ocl Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__OCL_CHANGED = MrulesPackage.MRULE_ANNOTATION__OCL_CHANGED;

	/**
	 * The feature id for the '<em><b>Ocl Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__OCL_CODE = MrulesPackage.MRULE_ANNOTATION__OCL_CODE;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__DO_ACTION = MrulesPackage.MRULE_ANNOTATION__DO_ACTION;

	/**
	 * The feature id for the '<em><b>ATarget Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE = MrulesPackage.MRULE_ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>MExplicitly Used For Containment Target Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE = MrulesPackage.MRULE_ANNOTATION_FEATURE_COUNT
			+ 1;

	/**
	 * The number of structural features of the '<em>MMap Entry For Target Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE_FEATURE_COUNT = MrulesPackage.MRULE_ANNOTATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___INDENTATION_SPACES = MrulesPackage.MRULE_ANNOTATION___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___INDENTATION_SPACES__INTEGER = MrulesPackage.MRULE_ANNOTATION___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AINDENT_LEVEL = MrulesPackage.MRULE_ANNOTATION___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AINDENTATION_SPACES = MrulesPackage.MRULE_ANNOTATION___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AINDENTATION_SPACES__INTEGER = MrulesPackage.MRULE_ANNOTATION___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ASTRING_OR_MISSING__STRING = MrulesPackage.MRULE_ANNOTATION___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ASTRING_IS_EMPTY__STRING = MrulesPackage.MRULE_ANNOTATION___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MrulesPackage.MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MrulesPackage.MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___APACKAGE_FROM_URI__STRING = MrulesPackage.MRULE_ANNOTATION___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = MrulesPackage.MRULE_ANNOTATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = MrulesPackage.MRULE_ANNOTATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ACORE_ASTRING_CLASS = MrulesPackage.MRULE_ANNOTATION___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ACORE_AREAL_CLASS = MrulesPackage.MRULE_ANNOTATION___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ACORE_AINTEGER_CLASS = MrulesPackage.MRULE_ANNOTATION___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ACORE_AOBJECT_CLASS = MrulesPackage.MRULE_ANNOTATION___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = MrulesPackage.MRULE_ANNOTATION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Get Short Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___GET_SHORT_CODE = MrulesPackage.MRULE_ANNOTATION___GET_SHORT_CODE;

	/**
	 * The operation id for the '<em>Get Scope</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___GET_SCOPE = MrulesPackage.MRULE_ANNOTATION___GET_SCOPE;

	/**
	 * The operation id for the '<em>Base Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION = MrulesPackage.MRULE_ANNOTATION___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION;

	/**
	 * The operation id for the '<em>As Code For Built In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AS_CODE_FOR_BUILT_IN = MrulesPackage.MRULE_ANNOTATION___AS_CODE_FOR_BUILT_IN;

	/**
	 * The operation id for the '<em>As Code For Constants</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AS_CODE_FOR_CONSTANTS = MrulesPackage.MRULE_ANNOTATION___AS_CODE_FOR_CONSTANTS;

	/**
	 * The operation id for the '<em>Length</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___LENGTH = MrulesPackage.MRULE_ANNOTATION___LENGTH;

	/**
	 * The operation id for the '<em>Unsafe Chain Step As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER = MrulesPackage.MRULE_ANNOTATION___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___UNSAFE_CHAIN_AS_CODE__INTEGER = MrulesPackage.MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER = MrulesPackage.MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Proc As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_AS_CODE = MrulesPackage.MRULE_ANNOTATION___PROC_AS_CODE;

	/**
	 * The operation id for the '<em>Is Custom Code Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_CUSTOM_CODE_PROCESSOR = MrulesPackage.MRULE_ANNOTATION___IS_CUSTOM_CODE_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Processor Set Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_PROCESSOR_SET_OPERATOR = MrulesPackage.MRULE_ANNOTATION___IS_PROCESSOR_SET_OPERATOR;

	/**
	 * The operation id for the '<em>Is Own XOCL Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_OWN_XOCL_OPERATOR = MrulesPackage.MRULE_ANNOTATION___IS_OWN_XOCL_OPERATOR;

	/**
	 * The operation id for the '<em>Processor Returns Singular</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROCESSOR_RETURNS_SINGULAR = MrulesPackage.MRULE_ANNOTATION___PROCESSOR_RETURNS_SINGULAR;

	/**
	 * The operation id for the '<em>Processor Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROCESSOR_IS_SET = MrulesPackage.MRULE_ANNOTATION___PROCESSOR_IS_SET;

	/**
	 * The operation id for the '<em>Create Processor Definition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___CREATE_PROCESSOR_DEFINITION = MrulesPackage.MRULE_ANNOTATION___CREATE_PROCESSOR_DEFINITION;

	/**
	 * The operation id for the '<em>Proc Def Choices For Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_OBJECT = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECT;

	/**
	 * The operation id for the '<em>Proc Def Choices For Objects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_OBJECTS = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECTS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Boolean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_BOOLEAN = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEAN;

	/**
	 * The operation id for the '<em>Proc Def Choices For Booleans</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_BOOLEANS = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEANS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_INTEGER = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGER;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_INTEGERS = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGERS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Real</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_REAL = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REAL;

	/**
	 * The operation id for the '<em>Proc Def Choices For Reals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_REALS = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REALS;

	/**
	 * The operation id for the '<em>Proc Def Choices For String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_STRING = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRING;

	/**
	 * The operation id for the '<em>Proc Def Choices For Strings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_STRINGS = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRINGS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_DATE = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATE;

	/**
	 * The operation id for the '<em>Proc Def Choices For Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___PROC_DEF_CHOICES_FOR_DATES = MrulesPackage.MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATES;

	/**
	 * The operation id for the '<em>Auto Cast With Proc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AUTO_CAST_WITH_PROC = MrulesPackage.MRULE_ANNOTATION___AUTO_CAST_WITH_PROC;

	/**
	 * The operation id for the '<em>Own To Apply Mismatch</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___OWN_TO_APPLY_MISMATCH = MrulesPackage.MRULE_ANNOTATION___OWN_TO_APPLY_MISMATCH;

	/**
	 * The operation id for the '<em>Unique Chain Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___UNIQUE_CHAIN_NUMBER = MrulesPackage.MRULE_ANNOTATION___UNIQUE_CHAIN_NUMBER;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN = MrulesPackage.MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN;

	/**
	 * The operation id for the '<em>Reset To Base</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE = MrulesPackage.MRULE_ANNOTATION___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain As Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN = MrulesPackage.MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN;

	/**
	 * The operation id for the '<em>Is Processor Check Equal Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_PROCESSOR_CHECK_EQUAL_OPERATOR = MrulesPackage.MRULE_ANNOTATION___IS_PROCESSOR_CHECK_EQUAL_OPERATOR;

	/**
	 * The operation id for the '<em>Is Prefix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_PREFIX_PROCESSOR = MrulesPackage.MRULE_ANNOTATION___IS_PREFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Postfix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___IS_POSTFIX_PROCESSOR = MrulesPackage.MRULE_ANNOTATION___IS_POSTFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>As Code For Others</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AS_CODE_FOR_OTHERS = MrulesPackage.MRULE_ANNOTATION___AS_CODE_FOR_OTHERS;

	/**
	 * The operation id for the '<em>Unsafe Element As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___UNSAFE_ELEMENT_AS_CODE__INTEGER = MrulesPackage.MRULE_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Code For Length1</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___CODE_FOR_LENGTH1 = MrulesPackage.MRULE_ANNOTATION___CODE_FOR_LENGTH1;

	/**
	 * The operation id for the '<em>Code For Length2</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___CODE_FOR_LENGTH2 = MrulesPackage.MRULE_ANNOTATION___CODE_FOR_LENGTH2;

	/**
	 * The operation id for the '<em>Code For Length3</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___CODE_FOR_LENGTH3 = MrulesPackage.MRULE_ANNOTATION___CODE_FOR_LENGTH3;

	/**
	 * The operation id for the '<em>As Code For Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___AS_CODE_FOR_VARIABLES = MrulesPackage.MRULE_ANNOTATION___AS_CODE_FOR_VARIABLES;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___DO_ACTION_UPDATE__MRULESANNOTATIONACTION = MrulesPackage.MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION;

	/**
	 * The operation id for the '<em>Variable From Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION = MrulesPackage.MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION;

	/**
	 * The number of operations of the '<em>MMap Entry For Target Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMAP_ENTRY_FOR_TARGET_FEATURE_OPERATION_COUNT = MrulesPackage.MRULE_ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link com.montages.mmapper.MMapping <em>MMapping</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MMapping</em>'.
	 * @see com.montages.mmapper.MMapping
	 * @generated
	 */
	EClass getMMapping();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mmapper.MMapping#getMOwnedMap <em>MOwned Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>MOwned Map</em>'.
	 * @see com.montages.mmapper.MMapping#getMOwnedMap()
	 * @see #getMMapping()
	 * @generated
	 */
	EReference getMMapping_MOwnedMap();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mmapper.MMapping#getMname <em>Mname</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mname</em>'.
	 * @see com.montages.mmapper.MMapping#getMname()
	 * @see #getMMapping()
	 * @generated
	 */
	EAttribute getMMapping_Mname();

	/**
	 * Returns the meta object for class '{@link com.montages.mmapper.MMap <em>MMap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MMap</em>'.
	 * @see com.montages.mmapper.MMap
	 * @generated
	 */
	EClass getMMap();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mmapper.MMap#getASource <em>ASource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ASource</em>'.
	 * @see com.montages.mmapper.MMap#getASource()
	 * @see #getMMap()
	 * @generated
	 */
	EReference getMMap_ASource();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mmapper.MMap#getATarget <em>ATarget</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ATarget</em>'.
	 * @see com.montages.mmapper.MMap#getATarget()
	 * @see #getMMap()
	 * @generated
	 */
	EReference getMMap_ATarget();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mmapper.MMap#getMMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>MMap Entry For Target Feature</em>'.
	 * @see com.montages.mmapper.MMap#getMMapEntryForTargetFeature()
	 * @see #getMMap()
	 * @generated
	 */
	EReference getMMap_MMapEntryForTargetFeature();

	/**
	 * Returns the meta object for class '{@link com.montages.mmapper.MMapEntryForTargetFeature <em>MMap Entry For Target Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MMap Entry For Target Feature</em>'.
	 * @see com.montages.mmapper.MMapEntryForTargetFeature
	 * @generated
	 */
	EClass getMMapEntryForTargetFeature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature <em>ATarget Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>ATarget Feature</em>'.
	 * @see com.montages.mmapper.MMapEntryForTargetFeature#getATargetFeature()
	 * @see #getMMapEntryForTargetFeature()
	 * @generated
	 */
	EReference getMMapEntryForTargetFeature_ATargetFeature();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mmapper.MMapEntryForTargetFeature#getMExplicitlyUsedForContainmentTargetFeature <em>MExplicitly Used For Containment Target Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>MExplicitly Used For Containment Target Feature</em>'.
	 * @see com.montages.mmapper.MMapEntryForTargetFeature#getMExplicitlyUsedForContainmentTargetFeature()
	 * @see #getMMapEntryForTargetFeature()
	 * @generated
	 */
	EReference getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MmapperFactory getMmapperFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mmapper.impl.MMappingImpl <em>MMapping</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mmapper.impl.MMappingImpl
		 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMapping()
		 * @generated
		 */
		EClass MMAPPING = eINSTANCE.getMMapping();

		/**
		 * The meta object literal for the '<em><b>MOwned Map</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAPPING__MOWNED_MAP = eINSTANCE.getMMapping_MOwnedMap();

		/**
		 * The meta object literal for the '<em><b>Mname</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMAPPING__MNAME = eINSTANCE.getMMapping_Mname();

		/**
		 * The meta object literal for the '{@link com.montages.mmapper.impl.MMapImpl <em>MMap</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mmapper.impl.MMapImpl
		 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMap()
		 * @generated
		 */
		EClass MMAP = eINSTANCE.getMMap();

		/**
		 * The meta object literal for the '<em><b>ASource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAP__ASOURCE = eINSTANCE.getMMap_ASource();

		/**
		 * The meta object literal for the '<em><b>ATarget</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAP__ATARGET = eINSTANCE.getMMap_ATarget();

		/**
		 * The meta object literal for the '<em><b>MMap Entry For Target Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAP__MMAP_ENTRY_FOR_TARGET_FEATURE = eINSTANCE.getMMap_MMapEntryForTargetFeature();

		/**
		 * The meta object literal for the '{@link com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl <em>MMap Entry For Target Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mmapper.impl.MMapEntryForTargetFeatureImpl
		 * @see com.montages.mmapper.impl.MmapperPackageImpl#getMMapEntryForTargetFeature()
		 * @generated
		 */
		EClass MMAP_ENTRY_FOR_TARGET_FEATURE = eINSTANCE.getMMapEntryForTargetFeature();

		/**
		 * The meta object literal for the '<em><b>ATarget Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAP_ENTRY_FOR_TARGET_FEATURE__ATARGET_FEATURE = eINSTANCE
				.getMMapEntryForTargetFeature_ATargetFeature();

		/**
		 * The meta object literal for the '<em><b>MExplicitly Used For Containment Target Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMAP_ENTRY_FOR_TARGET_FEATURE__MEXPLICITLY_USED_FOR_CONTAINMENT_TARGET_FEATURE = eINSTANCE
				.getMMapEntryForTargetFeature_MExplicitlyUsedForContainmentTargetFeature();

	}

} //MmapperPackage
