/**
 */
package com.montages.mmapper;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mmapper.MmapperPackage
 * @generated
 */
public interface MmapperFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MmapperFactory eINSTANCE = com.montages.mmapper.impl.MmapperFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MMapping</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MMapping</em>'.
	 * @generated
	 */
	MMapping createMMapping();

	/**
	 * Returns a new object of class '<em>MMap</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MMap</em>'.
	 * @generated
	 */
	MMap createMMap();

	/**
	 * Returns a new object of class '<em>MMap Entry For Target Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MMap Entry For Target Feature</em>'.
	 * @generated
	 */
	MMapEntryForTargetFeature createMMapEntryForTargetFeature();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MmapperPackage getMmapperPackage();

} //MmapperFactory
