/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */


package org.xocl.editorconfig.plugin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.emf.common.EMFPlugin;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.montages.common.resource.ResourceResolverHelper;

/**
 * @author Max Stepanov
 *
 */
public class EditorConfigPlugin extends EMFPlugin {

	/**
	 * The plugin ID
	 */
	public static final String ID = "org.xocl.editorconfig";
	
	/**
	 * The singleton instance of the plugin.
	 */
	public static final EditorConfigPlugin INSTANCE = new EditorConfigPlugin();

	/**
	 * The one instance of this class.
	 */
	private static Implementation plugin;

	/**
	 * Creates the singleton instance.
	 */
	private EditorConfigPlugin() {
		super(new ResourceLocator[] {});
	}

	/*
	 */
	@Override
	public ResourceLocator getPluginResourceLocator() {
		return plugin;
	}

	/**
	 * Returns the singleton instance of the Eclipse plugin.
	 * 
	 * @return the singleton instance.
	 */
	public static Implementation getPlugin() {
		return plugin;
	}

	/**
	 * The actual implementation of the Eclipse <b>Plugin</b>.
	 */
	public static class Implementation extends EclipsePlugin {
		/**
		 * Creates an instance.
		 */
		public Implementation() {
			super();

			// Remember the static instance.
			//
			plugin = this;
		}
	}

	/**
	 * getEditorConfig
	 * @param nsURI
	 * @return
	 */
	public URI getEditorConfig(String nsURI) {
		WorkspaceResolver resolver = new WorkspaceResolver();
		URI editorURI = resolver.getEditorConfigURI(nsURI);
		if (editorURI == null) {
			editorURI = EditorConfigAssociations.getInstance().getEditorConfigForURI(nsURI);
		}
		return editorURI;
	}

	/**
	 * getEditorConfig
	 * @param resource
	 * @return
	 */
	public URI getEditorConfig(Resource resource) {
		EList<EObject> contents = resource.getContents();
		if (!contents.isEmpty()) {
			return getEditorConfig(contents.get(0).eClass());
		}
		return null;
	}

	/**
	 * getEditorConfig
	 * @param resource
	 * @return
	 */
	public URI getEditorConfig(EPackage ePackage) {
		return getEditorConfig(ePackage.getNsURI());
	}

	/**
	 * getEditorConfig
	 * @param nsURI
	 * @return
	 */
	public URI getEditorConfig(EClass eClass) {
		return getEditorConfig(eClass.getEPackage());
	}

	private class WorkspaceResolver {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		URI getEditorConfigURI(String ePackageURI) {
			final List<IFile> files = new ArrayList<IFile>();
			try {
				root.accept(new IResourceProxyVisitor() {
					@Override
					public boolean visit(IResourceProxy proxy) throws CoreException {
						if ("plugin.xml".equals(proxy.requestResource().getName())) {
							files.add((IFile) proxy.requestResource());
						}

						return true;
					}
				}, IResource.FILE);
			} catch (CoreException e) {
				e.printStackTrace();
			}

			Iterator<IFile> it = files.iterator();
			IPath location = null;

			while (location == null && it.hasNext()) {
				IFile file = it.next();
				XMLHandler handler = readXML(file);

				if (handler.getLocation() != null && handler.getUri().equals(ePackageURI)) {
					IPath container = file.getProject().getFullPath();
					location = container.append(handler.getLocation());
				}
			}

			return location != null ? URI.createPlatformResourceURI(location.toString(), true) : null;
		}

		private XMLHandler readXML(IFile file) {
			XMLHandler handler = new XMLHandler();

			ResourceResolverHelper.readXML(file, handler);

			return handler;
		}

		private class XMLHandler extends DefaultHandler {
			private String uri = null;
			private String location = null;

			public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {	
				if (qName.equals("editorConfigModelAssociation") && attributes.getValue("config") != null && attributes.getValue("uri") != null) {
					this.uri = attributes.getValue("uri");
					this.location = attributes.getValue("config");
				}

				if (this.uri != null) {
					throw new SAXException("Found " + this.uri);
				}
			};

			public String getLocation() {
				return location;
			}

			public String getUri() {
				return uri;
			}
		}

	}

}
