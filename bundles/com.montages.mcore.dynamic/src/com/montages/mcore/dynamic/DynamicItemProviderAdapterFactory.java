package com.montages.mcore.dynamic;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ReflectiveItemProvider;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary;
import org.xocl.delegates.XOCLAnnotationAdapter;
import org.xocl.semantics.XUpdate;

/**
 * ProviderAdapterFactory used to reproduce xocl edit behavior in dynamic
 * editors.
 * 
 *
 */
public class DynamicItemProviderAdapterFactory extends ReflectiveItemProviderAdapterFactory {

	public DynamicItemProviderAdapterFactory() {
		reflectiveItemProviderAdapter = new DynamicItemProvider(this);

		supportedTypes.add(IStructuredItemContentProvider.class);
		supportedTypes.add(ITreeItemContentProvider.class);
		supportedTypes.add(IItemPropertySource.class);
		supportedTypes.add(IEditingDomainItemProvider.class);
		supportedTypes.add(IItemLabelProvider.class);
		supportedTypes.add(ITableItemLabelProvider.class);
	}

	public static class DynamicItemProvider extends ReflectiveItemProvider {

		public DynamicItemProvider(AdapterFactory adapterFactory) {
			super(adapterFactory);
		}

		@Override
		public Object getImage(Object object) {
			return super.getImage(object);
		}

		@Override
		public String getText(Object object) {
			String fallback = super.getText(object);

			if (object instanceof EObject) {
				final EClass eClass = ((EObject) object).eClass();
				final EAnnotation annotation = eClass.getEAnnotation(XOCLAnnotationAdapter.MODIFIED);

				if (annotation != null && annotation.getDetails().containsKey("label")) {
					OclLabelEvaluator evaluator = new OclLabelEvaluator();
					return evaluator.evaluate(annotation.getDetails().get("label"), (EObject) object, fallback);
				}
			}

			return fallback;
		}

		@Override
		protected Command createSetCommand(EditingDomain domain, EObject owner, EStructuralFeature feature,
				Object value) {

			Command command = null;

			if (feature.isDerived() && feature.isChangeable()) {

				command = owner.eClass().getEAllOperations().stream() //
						.filter(op -> op.getName().equals(feature.getName().concat("$Update"))) //
						.findFirst() //
						.map(op -> {
							XUpdate update;
							try {
								update = (XUpdate) owner.eInvoke(op, ECollections.singletonEList(value));
							} catch (InvocationTargetException e) {
								e.printStackTrace();
								update = null;
							}

							if (update != null) {
								return (Command) update.getContainingTransition().interpreteTransitionAsCommand();
							} else {
								return super.createSetCommand(domain, owner, feature, value);
							}

						}) //
						.orElse(super.createSetCommand(domain, owner, feature, value));
			}

			if (command == null) {
				command = super.createSetCommand(domain, owner, feature, value);
			}

			return command;
		}

		@Override
		public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
			itemPropertyDescriptors = new ArrayList<IItemPropertyDescriptor>();

			final EObject source = (EObject) object;

			for (final EStructuralFeature eFeature : source.eClass().getEAllStructuralFeatures()) {
				if (!(eFeature instanceof EReference) || !((EReference) eFeature).isContainment()) {
					final String choiceConstraint = findChoiceConstraintExpression(source.eClass(), eFeature);
					final String choiceConstruction = findChoiceConstructExpression(source.eClass(), eFeature);

					if (choiceConstraint != null || choiceConstruction != null) {
						final OclChoiceEvaluator choiceEvaluator = new OclChoiceEvaluator();
						final OclChoiceConstraintEvaluator constraintEvaluator = new OclChoiceConstraintEvaluator();

						itemPropertyDescriptors.add(new ItemPropertyDescriptor(
								((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
								getFeatureText(eFeature),
								getResourceLocator().getString("_UI_Property_description",
										new Object[] { getFeatureText(eFeature), eFeature.getEType().getName() }),
								eFeature, eFeature.isChangeable(), ItemPropertyDescriptor.GENERIC_VALUE_IMAGE) {

							@Override
							public Collection<?> getChoiceOfValues(Object object) {
								List<EObject> result = new ArrayList<EObject>();
								@SuppressWarnings("unchecked")
								Collection<? extends EObject> superResult = (Collection<? extends EObject>) super.getChoiceOfValues(
										object);
								if (superResult != null) {
									result.addAll(superResult);
								}

								if (choiceConstruction != null) {
									try {
										result = choiceEvaluator.evaluate(choiceConstruction, source, eFeature, result);
									} catch (Exception e) {
										e.printStackTrace();
									}
								}

								if (choiceConstraint != null) {
									for (Iterator<EObject> iterator = result.iterator(); iterator.hasNext();) {
										EObject trg = iterator.next();
										if (trg == null) {
											continue;
										}
										if (!constraintEvaluator.evaluate(choiceConstraint, source, trg, eFeature)) {
											iterator.remove();
										}
									}
								}

								return result;
							};
						});
					} else {
						itemPropertyDescriptors.add(new ItemPropertyDescriptor(
								((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
								getFeatureText(eFeature),
								getResourceLocator().getString("_UI_Property_description",
										new Object[] { getFeatureText(eFeature), eFeature.getEType().getName() }),
								eFeature, eFeature.isChangeable(), ItemPropertyDescriptor.GENERIC_VALUE_IMAGE));
					}
				}
			}

			return itemPropertyDescriptors;
		}

		private String findChoiceConstraintExpression(EClass source, EStructuralFeature feature) {
			return XoclEmfUtil.findAnnotationText(feature, source, "choiceConstraint", "ChoiceConstraint",
					XOCLAnnotationAdapter.MODIFIED, XoclEmfUtil.OVERRIDE_OCL_ANNOTATION_SOURCE);
		}

		private String findChoiceConstructExpression(EClass source, EStructuralFeature feature) {
			return XoclEmfUtil.findAnnotationText(feature, source, "choiceConstruction", "ChoiceConstruction",
					XOCLAnnotationAdapter.MODIFIED, XoclEmfUtil.OVERRIDE_OCL_ANNOTATION_SOURCE);
		}

		private OCL createOCL() {
			final OCL ocl = OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory());

			ParsingOptions.setOption(ocl.getEnvironment(), ParsingOptions.implicitRootClass(ocl.getEnvironment()),
					EcorePackage.eINSTANCE.getEObject());
			EvaluationOptions.setOption(ocl.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
			return ocl;
		}

		public class OclLabelEvaluator {

			public String evaluate(String expression, EObject source, String fallback) {
				OCL ocl = createOCL();
				OCL.Helper helper = ocl.createOCLHelper();
				helper.setContext(source.eClass());
				OCLExpression oclExpression = null;

				try {
					oclExpression = helper.createQuery(expression);
				} catch (ParserException e) {
					XoclErrorHandler.handleQueryProblems("com.montages.mcore.ui.editors", expression,
							helper.getProblems(), source.eClass(), "label");
				}

				if (oclExpression != null) {
					OCL.Query query = ocl.createQuery(oclExpression);

					try {
						XoclErrorHandler.enterContext("com.montages.mcore.ui.editors", query, source.eClass(), "label");
						return XoclHelper.format(query.evaluate(source));
					} catch (Throwable e) {
						XoclErrorHandler.handleException(e);
					} finally {
						XoclErrorHandler.leaveContext();
					}
				}

				return fallback;
			}
		}

		private class OclChoiceEvaluator {

			@SuppressWarnings("unchecked")
			public List<EObject> evaluate(String expression, EObject source, EStructuralFeature feature,
					List<EObject> choice) {
				OCL ocl = createOCL();
				List<EObject> result = null;
				OCL.Helper helper = ocl.createOCLHelper();
				helper.setContext(source.eClass());
				Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
				choiceVar.setName("choice");
				choiceVar.setType(ocl.getEnvironment().getOCLStandardLibrary().getSequence());
				ocl.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);

				try {
					OCLExpression oclExpression = helper.createQuery(expression);
					OCL.Query query = ocl.createQuery(oclExpression);
					query.getEvaluationEnvironment().add("choice", choice);
					Object queryResult = query.evaluate(source);
					Collection<EObject> resultCollection;
					try {
						resultCollection = (Collection<EObject>) queryResult;
					} catch (ClassCastException e) {
						resultCollection = new ArrayList<EObject>();
					}
					result = new ArrayList<EObject>(resultCollection);
				} catch (ParserException e) {
					e.printStackTrace();
				}

				return result;
			}
		}

		private class OclChoiceConstraintEvaluator {

			public boolean evaluate(String expression, EObject source, EObject trg, EStructuralFeature feature) {
				Boolean result = true;
				OCL ocl = createOCL();
				OCL.Helper helper = ocl.createOCLHelper();
				helper.setContext(source.eClass());

				Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
				trgVar.setName("trg");
				trgVar.setType(feature.getEType());
				ocl.getEnvironment().addElement("trg", trgVar, true);

				OCLExpression oclExpression = null;
				try {
					oclExpression = helper.createQuery(expression);
				} catch (ParserException e) {
					e.printStackTrace();
				}
				if (oclExpression != null) {
					OCL.Query query = ocl.createQuery(oclExpression);
					query.getEvaluationEnvironment().clear();
					query.getEvaluationEnvironment().add("trg", trg);
					Object queryResult = query.evaluate(source);

					if (queryResult instanceof Boolean) {
						result = ((Boolean) queryResult).booleanValue();
					}
				}

				return result;
			}

		}
	}

}
