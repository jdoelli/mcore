package com.montages.mcore.presentation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.widgets.TreeItem;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;

/**
 * Batch 1.1.6: After opening, MCoreEditor should:
 * <ul>
 * <li>expand all children of the first class inside first package inside first
 * component</li>
 * <li>expand all children of the first resource inside first component</li>
 * <li>select the first component</li>
 * </ul>
 * 
 * @author golubev
 */
public class MCoreEditorInitialExpansion {

	public void apply(TreeViewer treeViewer) {
		TreeItem root = treeViewer.getTree().getItem(0);

		TreeItem firstComponent = findDeepChildForConditions(treeViewer, root, //
				MCOMPONENT);
		TreeItem firstClass = findDeepChildForConditions(treeViewer, root, //
				MCOMPONENT, MPACKAGE, MCLASSIFIER);
		TreeItem firstResource = findDeepChildForConditions(treeViewer, root, //
				MCOMPONENT, RESOURCE_FOLDER, RESOURCE_FOLDER, RESOURCE);

		if (firstClass != null) {
			treeViewer.expandToLevel(newTreePathForDeepItem(firstClass), 1);
		}
		if (firstResource != null) {
			treeViewer.expandToLevel(newTreePathForDeepItem(firstResource), 1);
		}
		if (firstComponent != null) {
			treeViewer.setSelection(
					new TreeSelection(newTreePathForDeepItem(firstComponent)));
		}
	}

	/**
	 * Traverses the deep children of the root up to conditions.length depth.
	 * Returns the first deep element having all conditions satisfied for every
	 * segment of the path from root.
	 */
	private static TreeItem findDeepChildForConditions(TreeViewer viewer,
			TreeItem root, ItemCondition... conditions) {
		List<TreeItem> goodParents = Collections.singletonList(root);
		int depth = 0;
		while (depth < conditions.length && !goodParents.isEmpty()) {
			List<TreeItem> goodChildren = new LinkedList<TreeItem>();
			for (TreeItem parent : goodParents) {
				// we need to expand viewer to ensure all the child items are
				// created
				viewer.expandToLevel(newTreePathForDeepItem(parent), 1);

				goodChildren.addAll(filterChildren(parent, conditions[depth]));
			}
			goodParents = goodChildren;
			depth++;
		}

		return goodParents.isEmpty() ? null : goodParents.get(0);
	}

	private static List<TreeItem> filterChildren(TreeItem root,
			ItemCondition condition) {
		List<TreeItem> result = new LinkedList<TreeItem>();
		for (TreeItem next : root.getItems()) {
			if (condition.check(next)) {
				result.add(next);
			}
		}
		return result;
	}

	private static TreePath newTreePathForDeepItem(TreeItem item) {
		LinkedList<Object> segments = new LinkedList<Object>();
		while (item != null) {
			Object segment = item.getData();
			Assert.isNotNull(segment);
			segments.addFirst(segment);
			item = (item).getParentItem();
		}
		return new TreePath(segments.toArray());
	}

	private static interface ItemCondition {
		public boolean check(TreeItem item);
	}

	private static class DataIsOfType implements ItemCondition {
		private Class<?> myType;

		public DataIsOfType(Class<?> type) {
			myType = type;
		}

		@Override
		public boolean check(TreeItem item) {
			Object data = item.getData();
			return myType.isInstance(data);
		}
	}

	private static final ItemCondition MCOMPONENT = new DataIsOfType(
			MComponent.class);
	private static final ItemCondition MCLASSIFIER = new DataIsOfType(
			MClassifier.class);
	private static final ItemCondition MPACKAGE = new DataIsOfType(
			MPackage.class);
	private static final ItemCondition RESOURCE_FOLDER = new DataIsOfType(
			MResourceFolder.class);
	private static final ItemCondition RESOURCE = new DataIsOfType(
			MResource.class);

}
