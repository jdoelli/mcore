/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.TableConfig;

/**
 * @author Max Stepanov
 *
 */
/* package */ class EditorConfigDescriptor {

	private EditorConfig editorConfig;
	
	private Map<TableConfig, TableConfigDescriptor> tableConfigDescriptorMap = new HashMap<TableConfig, TableConfigDescriptor>();
	
	/**
	 * 
	 */
	public EditorConfigDescriptor(EditorConfig editorConfig) {
		this.editorConfig = editorConfig;
	}
	
	protected TableConfigDescriptor getTableConfig(TableConfig tableConfig) {
		if (tableConfig == null) {
			return null;
		}
		TableConfigDescriptor result = tableConfigDescriptorMap.get(tableConfig);
		if (result == null) {
			tableConfigDescriptorMap.put(tableConfig, result = new TableConfigDescriptor(this, tableConfig));
		}
		return result;
	}

	/**
	 * @return
	 */
	public TableConfigDescriptor getHeaderTableConfig() {
		return getTableConfig(editorConfig.getHeaderTableConfig());
	}

	/**
	 * @param eClass
	 * @return
	 */
	public TableConfigDescriptor getTableConfig(EClass eClass) {
		return getTableConfig(editorConfig.getTableConfig(eClass));
	}

	/**
	 * @return
	 * @see org.eclipse.emf.ecore.EObject#eResource()
	 */
	public Resource getResource() {
		return editorConfig.eResource();
	}

}
