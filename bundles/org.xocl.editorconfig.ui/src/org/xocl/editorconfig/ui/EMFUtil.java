/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */


package org.xocl.editorconfig.ui;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.IWrapperItemProvider;

/**
 * @author Max Stepanov
 *
 */
/* package */ final class EMFUtil {
	
	private static final Class<?> ITreeItemContentProviderClass = ITreeItemContentProvider.class;

	/**
	 * 
	 */
	private EMFUtil() {
	}
	
	public static EStructuralFeature getChildFeature(AdapterFactory adapterFactory, Object object, Object child) {
		//#765.3: allow non-containment features with children=true
		if (child instanceof IWrapperItemProvider) {
			IWrapperItemProvider wrapper = (IWrapperItemProvider)child;
			if (wrapper.getOwner() == object) {
				return wrapper.getFeature();
			}
		}
		
		ITreeItemContentProvider treeItemContentProvider =
			(ITreeItemContentProvider)adapterFactory.adapt(object, ITreeItemContentProviderClass);
		if (treeItemContentProvider != null) {
			try {
				Method getChildFeature = treeItemContentProvider.getClass().getDeclaredMethod("getChildFeature", new Class[] { Object.class, Object.class}); //$NON-NLS-1$
				boolean revertAccessible = false;
				if ((getChildFeature.getModifiers() & Modifier.PUBLIC) == 0) {
					if (!getChildFeature.isAccessible()) {
						getChildFeature.setAccessible(true);
						revertAccessible = true;
					}
				}
				Object result = getChildFeature.invoke(treeItemContentProvider, new Object[] { object, child });
				if (revertAccessible) {
					getChildFeature.setAccessible(false);
				}
				if (result instanceof EStructuralFeature) {
					return (EStructuralFeature) result;
				}
			} catch (SecurityException e) {
			} catch (NoSuchMethodException e) {
			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			} catch (InvocationTargetException e) {
			}
		}
		return null;
	}

}
