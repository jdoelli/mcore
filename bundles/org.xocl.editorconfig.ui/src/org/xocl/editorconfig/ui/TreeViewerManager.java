/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.ui;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.jface.viewers.ViewerColumn;
import org.eclipse.swt.widgets.TreeColumn;
import org.xocl.editorconfig.RowFeatureCell;

/**
 * @author Max Stepanov
 *
 */
public class TreeViewerManager extends AbstractViewerManager {

	private final TreeViewer viewer;

	/**
	 * 
	 */
	public TreeViewerManager(TreeViewer viewer, AdapterFactoryEditingDomain editingDomain) {
		super(editingDomain);
		this.viewer = viewer;
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#getViewer()
	 */
	@Override
	protected ColumnViewer getViewer() {
		return viewer;
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#createViewerColumn(int, java.lang.String, int)
	 */
	@Override
	protected ViewerColumn createViewerColumn(int style, String text, int width) {
		TreeViewerColumn column = new TreeViewerColumn(viewer, style);
		adjustViewerColumn(column, text, width);
		addViewerColumn(column);
		return column;
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#disposeViewerColumn(org.eclipse.jface.viewers.ViewerColumn)
	 */
	@Override
	protected void disposeViewerColumn(ViewerColumn column) {
		TreeColumn treeColumn = ((TreeViewerColumn)column).getColumn();
		treeColumn.dispose();
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#adjustViewerColumn(org.eclipse.jface.viewers.ViewerColumn, String, int)
	 */
	@Override
	protected void adjustViewerColumn(ViewerColumn column, String text, int width) {
		TreeColumn treeColumn = ((TreeViewerColumn)column).getColumn();
		width = Math.max(width, treeColumn.getWidth());
		if (width != treeColumn.getWidth()) {
			treeColumn.setWidth(width);
		}
		if (text != null) {
			treeColumn.setText(text);
		}
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#getViewerColumnWidth(org.eclipse.jface.viewers.ViewerColumn)
	 */
	@Override
	protected int getViewerColumnWidth(ViewerColumn column) {
		TreeColumn treeColumn = ((TreeViewerColumn)column).getColumn();
		return treeColumn.getWidth();
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#setViewerColumnWidth(org.eclipse.jface.viewers.ViewerColumn, int)
	 */
	@Override
	protected void setViewerColumnWidth(ViewerColumn column, int width) {
		TreeColumn treeColumn = ((TreeViewerColumn)column).getColumn();
		treeColumn.setWidth(width);
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#setViewerRowHeight(int)
	 */
	@Override
	protected void setViewerRowHeight(int height) {
		SWTUtil.setItemHeight(viewer.getTree(), height);
	}
	/* (non-Javadoc)
	 * Returns the column index from objec c  for given feature f 
	 */
	public int getColumnFromObjAndFeature(Object c,EStructuralFeature f)
	{
		TableConfigLabelProvider  tableConfigLabelProvider = getLabelProviderForObject(c);
		
		for (int i=0;i< tableConfigLabelProvider.getTableConfig().getColumn().size();i++)
		if (tableConfigLabelProvider.getCell(c, i) instanceof RowFeatureCell)
		{
		    RowFeatureCell rowFeat = (RowFeatureCell) tableConfigLabelProvider.getCell(c, i);
		   if (rowFeat.getFeature() == f)
			   return i+1;
		}

		
		// Add other Cells as well !
		return -1;
		
	}

	/* (non-Javadoc)
	 * @see org.xocl.editorconfig.ui.AbstractViewerManager#initViewer()
	 */
	@Override
	protected void initViewer() {
		TreeColumn[] treeColumns = viewer.getTree().getColumns();
		for (int i = 0; i < treeColumns.length; ++i) {
			addViewerColumn(new TreeViewerColumn(viewer, treeColumns[i]));
		}
		super.initViewer();
	}

}