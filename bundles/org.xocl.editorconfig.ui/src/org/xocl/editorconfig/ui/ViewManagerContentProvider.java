package org.xocl.editorconfig.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.IWrapperItemProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;
import org.xocl.editorconfig.EditorConfig;

public class ViewManagerContentProvider extends AdapterFactoryContentProvider {

	private final AbstractViewerManager viewerManager;

	private ScheduledRefresh myScheduledRefresh;

	/**
	 * @param adapterFactory
	 */
	public ViewManagerContentProvider(AdapterFactory adapterFactory, AbstractViewerManager viewerManager) {
		super(adapterFactory);
		this.viewerManager = viewerManager;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#
	 * getChildren(java.lang.Object)
	 */
	@Override
	public Object[] getChildren(Object object) {
		Object[] children = super.getChildren(object);
		if (children.length == 0) {
			return children;
		}
		// HACK: without this code, editorconfig editing in table editor is impossible
		if (object instanceof EObject && getRootContainer((EObject) object) instanceof EditorConfig) {
			return addTableHeaders(object, children);
		}
		Map<EReference, List<Object>> feature2Childrens = mapChildrenByFeature(object, children);
		TableConfigDescriptor parentTableConfigDescriptor = getParentTableConfigDescriptor(object);
		if (parentTableConfigDescriptor == null || !parentTableConfigDescriptor.hasReferenceToTableConfigMap()) {
			return addTableHeaders(object, children);
		}
		Map<EReference, TableConfigDescriptor> feature2TableConfigDescriptor = mapTableConfigDescriptorsByFeature(parentTableConfigDescriptor, feature2Childrens.keySet());
		List<Object> sortedChilds = new ArrayList<Object>();
		for (EReference nextFeature : sortAndFilterFeatures(parentTableConfigDescriptor, feature2TableConfigDescriptor)) {
			sortedChilds.addAll(feature2Childrens.get(nextFeature));
		}
		return sortedChilds.isEmpty() ? sortedChilds.toArray() : addTableHeaders(object, sortedChilds.toArray());
	}

	private Object getRootContainer(EObject object) {
		return object.eContainer() != null ? getRootContainer(object.eContainer()) : object;
	}

	private List<EReference> sortAndFilterFeatures(final TableConfigDescriptor parentTableConfigDescriptor, Map<EReference, TableConfigDescriptor> feature2TableConfigDescriptor) {
		List<EReference> result = new ArrayList<EReference>();
		for (EReference nextFeature : feature2TableConfigDescriptor.keySet()) {
			TableConfigDescriptor nextConfig = feature2TableConfigDescriptor.get(nextFeature);
			if (nextConfig == null) {
				continue;
			}
			result.add(nextFeature);
		}
		Collections.sort(result, parentTableConfigDescriptor.getEReferenceComparator());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider#
	 * inputChanged(org.eclipse.jface.viewers.Viewer, java.lang.Object,
	 * java.lang.Object)
	 */
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (oldInput != newInput) {
			viewerManager.getTableConfigHeaderMap().clear();
		}
		super.inputChanged(viewer, oldInput, newInput);
	}

	protected Map<EReference, TableConfigDescriptor> mapTableConfigDescriptorsByFeature(TableConfigDescriptor parent, Set<EReference> features) {
		Map<EReference, TableConfigDescriptor> result = new HashMap<EReference, TableConfigDescriptor>();
		for (EReference nextReference : features) {
			result.put(nextReference, parent.getTableConfig(nextReference));
		}
		return result;
	}

	protected TableConfigDescriptor getParentTableConfigDescriptor(Object parent) {
		TableConfigLabelProvider tableConfigLabelProvider = !(parent instanceof Resource || parent instanceof ResourceSet) ? viewerManager.getLabelProviderForObject(parent) : null;
		return tableConfigLabelProvider != null ? tableConfigLabelProvider.getTableConfig() : null;
	}

	protected Map<EReference, List<Object>> mapChildrenByFeature(Object parent, Object[] childrens) {
		Map<EReference, List<Object>> result = new HashMap<EReference, List<Object>>();
		for (int i = 0; i < childrens.length; ++i) {
			Object nextChild = childrens[i];
			EStructuralFeature eFeature = nextChild instanceof EObject ? ((EObject) nextChild).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parent, nextChild);
			if (eFeature == null && nextChild instanceof IWrapperItemProvider) {
				eFeature = ((IWrapperItemProvider)nextChild).getFeature();
			}
			if (false == eFeature instanceof EReference) {
				continue;
			}
			List<Object> eReferenceChildren = result.get((EReference) eFeature);
			if (eReferenceChildren == null) {
				eReferenceChildren = new ArrayList<Object>();
				result.put((EReference) eFeature, eReferenceChildren);
			}
			eReferenceChildren.add(nextChild);
		}
		return result;
	}

	private TableConfigHeader getRepeatingTableConfigHeader(Object object, boolean force) {
		Object parentObject = super.getParent(object);
		if (parentObject != null) {
			TableConfigLabelProvider labelProvider = viewerManager.getLabelProviderForObject(object);
			TableConfigDescriptor parentTableConfig = labelProvider != null ? labelProvider.getTableConfig() : null;
			if (parentTableConfig != null && !parentTableConfig.equals(viewerManager.getTableConfigForObject(object)) && parentTableConfig.isDisplayHeader()) {
				Object[] children = super.getChildren(parentObject);
				Object sibling = null;
				for (int i = 0; i < children.length - 1; ++i) {
					if (children[i] == object) {
						sibling = children[i + 1];
						break;
					}
				}
				if (sibling != null) {
					EStructuralFeature eObjectFeature = object instanceof EObject ? ((EObject) object).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, object);
					EStructuralFeature eSiblingFeature = object instanceof EObject ? ((EObject) sibling).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, sibling);
					if (eSiblingFeature instanceof EReference) {
						if (eSiblingFeature.equals(eObjectFeature)) {
							if (IEditorConfigConstants.CONFIGURATION_ALWAYS_DISPLAY_REPEATING_HEADER && !force) {
								return null;
							}
						} else {
							TableConfigLabelProvider siblingLabelProvider = viewerManager.getLabelProviderForObject(sibling);
							TableConfigDescriptor siblingTableConfig = siblingLabelProvider != null ? siblingLabelProvider.getTableConfig() : null;
							if (parentTableConfig.equals(siblingTableConfig) && !force) {
								return null;
							} else if (siblingTableConfig != null && siblingTableConfig.isDisplayHeader()) {
								if (!siblingLabelProvider.equals(labelProvider)) {
									return null;
								}
							}
						}
					}
					return viewerManager.addHeader(new TableConfigHeader(object, parentTableConfig, viewerManager.getTableConfigHeaderLevelForParent(parentObject) + 1, true, viewerManager
							.getColorScheme()));
				} else {
					// check if tree have next sibling
					while (sibling == null && children.length > 0 && children[children.length - 1] == object) {
						if (viewerManager.hasRepeatingTableConfigHeader(parentObject)) {
							break;
						}
						object = parentObject;
						parentObject = super.getParent(object);
						if (parentObject == null) {
							break;
						}
						children = super.getChildren(parentObject);
						for (int i = 0; i < children.length - 1; ++i) {
							if (children[i] == object) {
								sibling = children[i + 1];
								break;
							}
						}
					}
					if (sibling != null) {
						EStructuralFeature eObjectFeature = object instanceof EObject ? ((EObject) object).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, object);
						EStructuralFeature eSiblingFeature = object instanceof EObject ? ((EObject) sibling).eContainmentFeature() : EMFUtil
								.getChildFeature(getAdapterFactory(), parentObject, sibling);
						if (eSiblingFeature instanceof EReference) {
							if (eSiblingFeature.equals(eObjectFeature)) {
								if (IEditorConfigConstants.CONFIGURATION_ALWAYS_DISPLAY_REPEATING_HEADER && !force) {
									return null;
								}
								return viewerManager.addHeader(new TableConfigHeader(object, parentTableConfig, viewerManager.getTableConfigHeaderLevelForParent(parentObject) + 1, true, viewerManager
										.getColorScheme()));
							}
						}
						TableConfigLabelProvider siblingLabelProvider = viewerManager.getLabelProviderForObject(sibling);
						TableConfigDescriptor siblingTableConfig = siblingLabelProvider != null ? siblingLabelProvider.getTableConfig() : null;
						if (siblingTableConfig == null || !siblingTableConfig.isDisplayHeader()) {
							return viewerManager.addHeader(new TableConfigHeader(object, parentTableConfig, viewerManager.getTableConfigHeaderLevelForParent(parentObject) + 1, true, viewerManager
									.getColorScheme()));
						}
					}
				}
			}
		}
		return null;
	}

	protected Object[] addTableHeaders(Object parentObject, Object[] children) {
		TableConfigHeader defaultHeader;
		TableConfigHeader repHeader;
		TableConfigDescriptor parentTableConfig = null;
		TableConfigLabelProvider tableConfigLabelProvider = !(parentObject instanceof Resource || parentObject instanceof ResourceSet) ? viewerManager.getLabelProviderForObject(parentObject) : null;
		if (tableConfigLabelProvider != null) {
			parentTableConfig = tableConfigLabelProvider.getTableConfig();
		}
		if (parentTableConfig == null || !parentTableConfig.hasReferenceToTableConfigMap()) {
			defaultHeader = viewerManager.getTableConfigHeaderForParent(parentObject, false);
			if (defaultHeader != null) {
				repHeader = getRepeatingTableConfigHeader(parentObject, false);
				Object[] childrenWithHeader = new Object[children.length + 1 + (repHeader != null ? 1 : 0)];
				System.arraycopy(children, 0, childrenWithHeader, 1, children.length);
				childrenWithHeader[0] = defaultHeader;
				if (repHeader != null) {
					childrenWithHeader[childrenWithHeader.length - 1] = viewerManager.addHeader(repHeader);
				}
				children = childrenWithHeader;
			}
			return children;
		}
		defaultHeader = viewerManager.getTableConfigHeaderForParent(parentObject, false);
		List<Object> list = new ArrayList<Object>();
		EStructuralFeature lastChildFeature = null;
		TableConfigDescriptor lastTableConfig = null;
		boolean currentIsDefault = (defaultHeader == null);
		boolean hasHeaders = false;
		for (int i = 0; i < children.length; ++i) {
			Object child = children[i];
			EStructuralFeature eFeature = child instanceof EObject ? ((EObject) child).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, child);
			if (eFeature instanceof EReference && !eFeature.equals(lastChildFeature)) {
				TableConfigDescriptor referenceTableConfig = viewerManager.getTableConfigForReference(parentTableConfig, (EReference) eFeature, child);
				if (referenceTableConfig != null) {
					if (parentTableConfig.equals(referenceTableConfig)) {
						if (!currentIsDefault) {
							if (defaultHeader == null) {
								defaultHeader = viewerManager.getTableConfigHeaderForParent(parentObject, true);
							}
							if (defaultHeader != null) {
								list.add(defaultHeader);
								hasHeaders = true;
							}
							currentIsDefault = true;
						}
					} else {
						if (!referenceTableConfig.equals(lastTableConfig) && referenceTableConfig.isDisplayHeader()) {
							viewerManager.getLabelProviderForTableConfig(referenceTableConfig, null);
							list.add(viewerManager.addHeader(new TableConfigHeader(parentObject, referenceTableConfig, viewerManager.getTableConfigHeaderLevelForParent(parentObject) + 1,
									viewerManager.getColorScheme())));
							currentIsDefault = false;
							hasHeaders = true;
						}
					}
				} else if (!currentIsDefault) {
					if (defaultHeader == null) {
						defaultHeader = viewerManager.getTableConfigHeaderForParent(parentObject, true);
					}
					if (defaultHeader != null) {
						list.add(defaultHeader);
						hasHeaders = true;
					}
					currentIsDefault = true;
				}
				lastChildFeature = eFeature;
				lastTableConfig = referenceTableConfig;
			}
			if (i == 0 && list.isEmpty() && !currentIsDefault) {
				if (defaultHeader == null) {
					defaultHeader = viewerManager.getTableConfigHeaderForParent(parentObject, true);
				}
				if (defaultHeader != null) {
					list.add(defaultHeader);
					hasHeaders = true;
				}
				currentIsDefault = true;
			}
			list.add(child);
		}
		if (hasHeaders || IEditorConfigConstants.CONFIGURATION_ALWAYS_DISPLAY_REPEATING_HEADER) {
			repHeader = getRepeatingTableConfigHeader(parentObject, hasHeaders || !parentTableConfig.equals(lastTableConfig));
			if (repHeader != null) {
				list.add(viewerManager.addHeader(repHeader));
			}
		}
		return list.toArray();
	}

	@Override
	public void notifyChanged(Notification notification) {
		if (viewerManager.isRefreshAllMode() && !isViewerDisposed() && !isViewerBusy()) {
			scheduleRefresh();
		} else {
			super.notifyChanged(notification);
		}
	}

	private boolean isViewerDisposed() {
		return viewer == null || viewer.getControl() == null || viewer.getControl().isDisposed();
	}

	private boolean isViewerBusy() {
		return viewer instanceof ColumnViewer && ((ColumnViewer) viewer).isBusy();
	}

	private synchronized void scheduleRefresh() {
		if (myScheduledRefresh == null) {
			myScheduledRefresh = new ScheduledRefresh();
			Display.getDefault().asyncExec(myScheduledRefresh);
		}
	}

	private synchronized void aboutToPerformScheduledRefresh() {
		myScheduledRefresh = null;
	}

	private class ScheduledRefresh implements Runnable {

		@Override
		public void run() {
			aboutToPerformScheduledRefresh();
			if (viewer != null && viewer.getControl() != null && !viewer.getControl().isDisposed()) {
				viewer.refresh();
			}
		}
	}

}