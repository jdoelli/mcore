/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui;

import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.widgets.Control;
import org.xocl.editorconfig.internal.ui.Zoom;

/**
 * @author Max Stepanov
 *
 */
public interface IEditorConfigViewer {

	public void setEditorConfig(URI editorConfigURI);
	public URI getEditorConfigURI();

	public void reloadEditorConfig();
	
	public Zoom getZoom();
	public void setZoom(Zoom zoom);

	public boolean isRefreshAllMode();
	public void setRefreshAllMode(boolean refreshAll);
	
	public Control getControl();

}
