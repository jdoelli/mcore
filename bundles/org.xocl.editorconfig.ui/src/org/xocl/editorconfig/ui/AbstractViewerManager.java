/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.ui;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Assert;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.provider.ExtendedColorRegistry;
import org.eclipse.gmf.runtime.emf.core.resources.GMFResource;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IContributionItem;
import org.eclipse.jface.action.IContributionManager;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationEvent;
import org.eclipse.jface.viewers.ColumnViewerEditorActivationListener;
import org.eclipse.jface.viewers.ColumnViewerEditorDeactivationEvent;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TreeColumnViewerLabelProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.jface.viewers.ViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.xocl.common.edit.ui.provider.PropertyDescriptor;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.CellEditBehaviorOption;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.internal.ui.Activator;
import org.xocl.editorconfig.internal.ui.Zoom;
import org.xocl.editorconfig.internal.ui.actions.IUpdate;
import org.xocl.editorconfig.internal.ui.actions.RefreshAllAction;
import org.xocl.editorconfig.internal.ui.actions.ReloadEditorConfigAction;
import org.xocl.editorconfig.internal.ui.actions.SelectEditorConfigAction;
import org.xocl.editorconfig.internal.ui.actions.ZoomMenuAction;
import org.xocl.editorconfig.plugin.EditorConfigPlugin;

import com.montages.common.resource.EcoreResourceResolver;

/**
 * @author Max Stepanov
 *
 */
public abstract class AbstractViewerManager implements IEditorConfigViewer {

	private URI editorConfigURI;

	private EditorConfigDescriptor editorConfig;

	private Map<URI, EditorConfigDescriptor> editorConfigs = new HashMap<URI, EditorConfigDescriptor>();

	private AdapterFactoryEditingDomain editingDomain;

	private ResourceSet editorConfigResourceSet;

	private boolean isResourceSetInit = false;

	private List<ViewerColumn> viewerColumns = new ArrayList<ViewerColumn>();

	private ColumnLabelProvider labelProvider;

	private ViewManagerContentProvider contentProvider;

	private ColumnViewerEditorActivationListener columnViewerEditorActivationListener;

	private Listener viewerControlListener;

	private Collection<IAction> actions;

	private IAction zoomAction;

	private IAction refreshAllModeAction;

	private Map<Object, TableConfigLabelProvider> labelProviderMap = new HashMap<Object, TableConfigLabelProvider>();

	private Map<TableConfigDescriptor, TableConfigLabelProvider> tableConfigToLabelProviderMap = new HashMap<TableConfigDescriptor, TableConfigLabelProvider>();

	private Map<Object, TableConfigHeader> tableConfigHeaderMap = new HashMap<Object, TableConfigHeader>();

	private List<TableConfigHeader> tableConfigHeaders = new ArrayList<TableConfigHeader>();

	private CellEditor lastCellEditor;

	private boolean defaultEditMode;

	private Object defaultEditModeValue;

	private Zoom zoom = Zoom.NORMAL;

	private URI defaultTextFont = getFont(false, zoom);

	private URI headerFont = getFont(true, zoom);

	private boolean isRefreshAllMode = true;

	private final TableConfigColorScheme myColorScheme = new TableConfigColorScheme.GrayBlueGreen();

	/**
	 * 
	 */
	public AbstractViewerManager(AdapterFactoryEditingDomain editingDomain) {
		this.editingDomain = editingDomain;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xocl.editorconfig.ui.IEditorConfigViewer#getControl()
	 */
	public Control getControl() {
		return getViewer().getControl();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.xocl.editorconfig.ui.IEditorConfigViewer#setEditorConfig(org.eclipse
	 * .emf.common.util.URI)
	 */
	public final void setEditorConfig(URI editorConfigURI) {
		cleanupViewer();
		try {
			this.editorConfigURI = editorConfigURI;
			this.editorConfig = loadEditorConfig(editorConfigURI);
			initViewer();
			getViewer().refresh(true);
		} catch (Exception e) {
			Activator.log(e);
			cleanupViewer();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xocl.editorconfig.ui.IEditorConfigViewer#getEditorConfigURI()
	 */
	public URI getEditorConfigURI() {
		return editorConfigURI;
	}

	public TableConfigColorScheme getColorScheme() {
		return myColorScheme;
	}

	public ResourceSet getEditorConfigResourceSet() {
		if (editorConfigResourceSet == null) {
			editorConfigResourceSet = new ResourceSetImpl();
		}

		return editorConfigResourceSet;
	}

	public ResourceSet getResourceSet() {
		if (!isResourceSetInit) {
			editingDomain.getResourceSet().getURIConverter().getURIMap().putAll(EcorePlugin.computePlatformURIMap(false));
			editingDomain.getResourceSet().getURIConverter().getURIMap().putAll(EcoreResourceResolver.computeWorkspaceResourceMap());
			isResourceSetInit = true;
		}

		return editingDomain.getResourceSet();
	}

	public void reloadEditorConfig() {
		Resource resource = getEditorConfigResourceSet().getResource(editorConfigURI, true);
		if (resource.isLoaded()) {
			resource.unload();
			try {
				resource.load(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		setEditorConfig(getEditorConfigURI());
	}

	private EditorConfigDescriptor loadEditorConfig(URI editorConfigURI) {
		Resource resource = null;
		resource = getEditorConfigResourceSet().getResource(editorConfigURI, true);

		EditorConfig config = null;
		if (resource != null) {
			config = (EditorConfig) resource.getContents().get(0);
		}

		return new EditorConfigDescriptor(config);
	}

	public final void initForEditor(IEditorPart editorPart) {
		String editorId = editorPart.getEditorSite().getId();
		URI editorConfigURI = Activator.getEditorConfigForEditorID(editorId);
		if (editorConfigURI == null) {
			EList<Resource> resources = getResourceSet().getResources();
			if (!resources.isEmpty()) {
				Resource modelResource = null;
				for (Resource res: resources) {
					if (false == res instanceof GMFResource) {
						modelResource = res;
						break;
					}
				}
				if (modelResource != null) {
					editorConfigURI = EditorConfigPlugin.INSTANCE.getEditorConfig(modelResource);
				}
			}
			if (editorConfigURI == null) {
				for (Resource res : resources) {
					if (res.getURI() != null && "editorconfig".equals(res.getURI().fileExtension())) {
						editorConfigURI = res.getURI();
					}
				}
			}
		}
		if (editorConfigURI != null) {
			setEditorConfig(editorConfigURI);
		}

		refreshToolbarActions(editorPart.getEditorSite().getActionBars());
	}

	public void refreshToolbarActions(IActionBars actionBars) {
		IToolBarManager toolBarManager = actionBars.getToolBarManager();
		getActions();
		if (zoomAction != null) {
			refreshToolbarAction(toolBarManager, zoomAction);
		}
		if (refreshAllModeAction != null) {
			refreshToolbarAction(toolBarManager, refreshAllModeAction);
		}
		toolBarManager.update(false);
	}

	private void refreshToolbarAction(IToolBarManager toolbarManager, IAction action) {
		if (false == toolbarManager instanceof IContributionManager) {
			return;
		}
		IContributionManager contributionManager = (IContributionManager) toolbarManager;
		IContributionItem contributionItem = contributionManager.find(action.getId());
		if (contributionItem != null) {
			contributionManager.remove(contributionItem);
		}
		contributionManager.add(action);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xocl.editorconfig.ui.IEditorConfigViewer#getZoom()
	 */
	public Zoom getZoom() {
		return zoom;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.xocl.editorconfig.ui.IEditorConfigViewer#setZoom(org.xocl.editorconfig
	 * .internal.ui.Zoom)
	 */
	public void setZoom(Zoom zoom) {
		if (this.zoom != zoom) {
			double ratio = zoom.getColumnWidthRatio() / this.zoom.getColumnWidthRatio();
			this.zoom = zoom;
			defaultTextFont = getFont(false, zoom);
			headerFont = getFont(true, zoom);
			ColumnViewer viewer = getViewer();
			Control control = viewer.getControl();
			control.setRedraw(false);
			setViewerRowHeight(5);
			for (ViewerColumn column : getViewerColumns()) {
				int width = (int) Math.round(getViewerColumnWidth(column) * ratio);
				setViewerColumnWidth(column, width);
			}
			viewer.refresh(true);
			control.setRedraw(true);
		}
	}

	public EditorConfigDescriptor getEditorConfig() {
		return editorConfig;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.xocl.editorconfig.ui.IEditorConfigViewer#isRefreshAllMode()
	 */
	public boolean isRefreshAllMode() {
		return isRefreshAllMode;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.xocl.editorconfig.ui.IEditorConfigViewer#setRefreshAllMode(boolean)
	 */
	public void setRefreshAllMode(boolean refreshAll) {
		isRefreshAllMode = refreshAll;
		if (isRefreshAllMode) {
			ColumnViewer viewer = getViewer();
			if (viewer.getControl() != null && !viewer.getControl().isDisposed()) {
				getViewer().refresh();
			}
		}
	}

	public final void setContextMenu(IMenuManager menuManager) {
		menuManager.addMenuListener(new IMenuListener() {

			public void menuAboutToShow(IMenuManager manager) {
				for (Iterator<IAction> i = getActions().iterator(); i.hasNext();) {
					IAction action = i.next();
					if (action instanceof IUpdate) {
						((IUpdate) action).update();
					}
					manager.appendToGroup("ui-actions", action); //$NON-NLS-1$
				}
			}
		});
	}

	protected abstract ColumnViewer getViewer();

	protected abstract ViewerColumn createViewerColumn(int style, String text, int width);

	protected abstract void disposeViewerColumn(ViewerColumn column);

	protected abstract void adjustViewerColumn(ViewerColumn column, String text, int width);

	protected abstract int getViewerColumnWidth(ViewerColumn column);

	protected abstract void setViewerColumnWidth(ViewerColumn column, int width);

	protected abstract void setViewerRowHeight(int height);

	protected void initViewer() {
		Assert.isTrue(viewerColumns.size() > 0, "Viewer should have at least one column"); //$NON-NLS-1$
		TableConfigDescriptor tableConfig = editorConfig.getHeaderTableConfig();
		Iterator<ViewerColumn> columnIterator = viewerColumns.iterator();
		if (tableConfig != null) {
			if (/* tableConfig.isDisplayDefaultColumn() && */columnIterator.hasNext()) {
				columnIterator.next();
			}
		} else if (columnIterator.hasNext()) {
			adjustViewerColumn(columnIterator.next(), "", 0); //$NON-NLS-1$
		}
		while (columnIterator.hasNext()) {
			disposeViewerColumn(columnIterator.next());
			columnIterator.remove();
		}
		if (tableConfig != null) {
			adjustColumns(tableConfig, true);
		}
		if (contentProvider == null) {
			getViewer().setContentProvider(getContentProvider());
		}
		getViewer().getColumnViewerEditor().addEditorActivationListener(getColumnViewerEditorActivationListener());
		getViewer().getControl().addListener(SWT.MeasureItem, getViewerControlListener());
	}

	private void cleanupViewer() {
		if (columnViewerEditorActivationListener != null) {
			getViewer().getColumnViewerEditor().removeEditorActivationListener(columnViewerEditorActivationListener);
			columnViewerEditorActivationListener = null;
		}
		if (viewerControlListener != null) {
			getViewer().getControl().removeListener(SWT.MeasureItem, viewerControlListener);
		}
		if (editorConfig != null) {
			Iterator<ViewerColumn> columnIterator = viewerColumns.iterator();
			if (columnIterator.hasNext()) {
				columnIterator.next();
			}
			while (columnIterator.hasNext()) {
				disposeViewerColumn(columnIterator.next());
				columnIterator.remove();
			}
			editorConfig = null;
			viewerColumns.clear();
		}
		editorConfigs.clear();
		labelProviderMap.clear();
		tableConfigToLabelProviderMap.clear();
		tableConfigHeaderMap.clear();
		tableConfigHeaders.clear();
	}

	protected final void addViewerColumn(ViewerColumn column) {
		viewerColumns.add(column);
		hookViewerColumn(column);
	}

	private ViewerColumn[] getViewerColumns() {
		return viewerColumns.toArray(new ViewerColumn[viewerColumns.size()]);
	}

	private void adjustColumns(TableConfigDescriptor tableConfig, boolean initial) {
		EList<ColumnConfig> columnList = tableConfig.getColumn();
		ViewerColumn[] columns = getViewerColumns();
		int shift = tableConfig.isDisplayDefaultColumn() ? 1 : 1 /* 0 */;
		boolean hasNewColumns = false;
		for (int i = 0; i < columnList.size(); ++i) {
			ColumnConfig columnConfig = columnList.get(i);
			ViewerColumn column;
			int width = getColumnWidth(columnConfig.getWidth(), zoom);
			if (width == 0) {
				width = getColumnWidth(IEditorConfigConstants.DEFAULT_COLUMN_WIDTH, zoom);
			}
			String label = initial ? columnConfig.getLabel() : null;
			if (i + shift >= columns.length) {
				column = createViewerColumn(SWT.NONE, label, width);
				hasNewColumns = true;
			} else {
				column = columns[i + shift];
			}
			adjustViewerColumn(column, label, width);
		}
		if (hasNewColumns) {
			updateHeaders(columns.length);
		}
		if (columns.length > 0) {
			String label = initial ? tableConfig.getLabel() : null;
			adjustViewerColumn(columns[0], label, -1);
		}
	}

	private void updateHeaders(int newColumnCount) {
		List<Object> list = new ArrayList<Object>();
		for (TableConfigHeader i : tableConfigHeaders) {
			if (i != null && i.getColumnCount() < newColumnCount) {
				list.add(i);
			}
		}
		if (!list.isEmpty()) {
			asyncUpdateElements(list.toArray());
		}
	}

	public TableConfigHeader addHeader(TableConfigHeader header) {
		tableConfigHeaders.add(header);
		return header;
	}

	private ColumnLabelProvider getColumnLabelProvider() {
		if (labelProvider == null) {
			ColumnViewerToolTipSupport.enableFor(getViewer());
			labelProvider = new TreeColumnViewerLabelProvider(new ColumnViewerLabelProvider(editingDomain.getAdapterFactory(), getViewer())) {

				/*
				 * (non-Javadoc)
				 * 
				 * @see
				 * org.eclipse.jface.viewers.CellLabelProvider#getToolTipText
				 * (java.lang.Object)
				 */
				@Override
				public String getToolTipText(Object element) {
					if (element instanceof Resource) {
						URI uri = ((Resource) element).getURI();
						if (uri != null) {
							return uri.toString();
						}
					}
					return super.getToolTipText(element);
				}
			};
		}
		return labelProvider;
	}

	public ViewManagerContentProvider getContentProvider() {
		if (contentProvider == null) {
			contentProvider = new ViewManagerContentProvider(editingDomain.getAdapterFactory(), this);
		}
		return contentProvider;
	}
	
	public void setContentProvider(ViewManagerContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}

	private Listener getViewerControlListener() {
		if (viewerControlListener == null) {
			viewerControlListener = new Listener() {

				public void handleEvent(Event event) {
					if (event.type == SWT.MeasureItem) {
						event.height = event.gc.getFontMetrics().getHeight() * 18 / 13;
					}
				}
			};
		}
		return viewerControlListener;
	}

	private AdapterFactory getAdapterFactory() {
		return editingDomain.getAdapterFactory();
	}

	private ColumnViewerEditorActivationListener getColumnViewerEditorActivationListener() {
		if (columnViewerEditorActivationListener == null) {
			columnViewerEditorActivationListener = new ColumnViewerEditorActivationListener() {

				@Override
				public void beforeEditorActivated(ColumnViewerEditorActivationEvent event) {
					if (lastCellEditor instanceof ReferenceCellEditor) {
						final ViewerCell viewerCell = (ViewerCell) event.getSource();
						CellEditBehaviorOption cellEditBehavior = getCellEditBehavior(viewerCell.getElement(), viewerCell.getColumnIndex());
						boolean altClick = event.sourceEvent instanceof MouseEvent && ((MouseEvent) event.sourceEvent).stateMask == SWT.ALT;

						/** bug 242: Need to cancel the event if it was triggered with ALT on an property which cannot be set
						 * See canEdit()
						 */
						Object element = viewerCell.getElement();
						if (altClick && (element instanceof EObject)) {
							IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor((EObject) element, viewerCell.getColumnIndex());
							if (itemPropertyDescriptor != null) {
								if (!itemPropertyDescriptor.canSetProperty(element)) {
									event.cancel = true;
									return;
								}
							}
						}

						if (altClick && CellEditBehaviorOption.SELECTION.equals(cellEditBehavior)) {
							lastCellEditor.create((Composite) getViewer().getControl());
							((ReferenceCellEditor) lastCellEditor).setViewerCell(viewerCell);
						} else if (altClick && CellEditBehaviorOption.PULL_DOWN.equals(cellEditBehavior)) {
							event.cancel = true;
							final Object value = ((ReferenceCellEditor) lastCellEditor).getInitialValue();
							afterEditorDeactivated(null);
							getViewer().getControl().getDisplay().asyncExec(new Runnable() {

								public void run() {
									defaultEditMode = true;
									defaultEditModeValue = value;
									getViewer().editElement(viewerCell.getElement(), viewerCell.getColumnIndex());
								}
							});
						} else if (altClick && CellEditBehaviorOption.READ_ONLY.equals(cellEditBehavior)) {
							event.cancel = true;
							afterEditorDeactivated(null);
						} else {
							CellLocateBehaviorOption cellLocateBehavior = getCellLocateBehavior(viewerCell.getElement(), viewerCell.getColumnIndex());
							event.cancel = true;
							if (CellLocateBehaviorOption.SELECTION.equals(cellLocateBehavior)) {
								try {
									lastCellEditor.activate();
									((ReferenceCellEditor) lastCellEditor).selectAndReveal(true);
								} finally {
									lastCellEditor.deactivate();
								}
							}
							afterEditorDeactivated(null);
						}
					}

					//bug654
					if (!event.cancel) {
						adjustCellEditorFont(event);
					}
				}

				@Override
				public void afterEditorActivated(ColumnViewerEditorActivationEvent event) {
					defaultEditMode = false;
					defaultEditModeValue = null;
				}

				@Override
				public void beforeEditorDeactivated(ColumnViewerEditorDeactivationEvent event) {
					if (lastCellEditor instanceof ReferenceCellEditor) {
						defaultEditMode = ((ReferenceCellEditor) lastCellEditor).isSwitchDefaultEditMode();
					}
				}

				@Override
				public void afterEditorDeactivated(ColumnViewerEditorDeactivationEvent event) {
					if (lastCellEditor != null) {
						lastCellEditor.dispose();
						lastCellEditor = null;
					}
				}

				//bug654
				private void adjustCellEditorFont(ColumnViewerEditorActivationEvent event) {
					if (labelProvider != null && lastCellEditor != null && lastCellEditor.getControl() != null && !lastCellEditor.getControl().isDisposed()) {
						ViewerCell viewerCell = (ViewerCell) event.getSource();
						if (viewerCell.getFont() != null) {
							lastCellEditor.getControl().setFont(viewerCell.getFont());
						}
					}
				}
			};
		}
		return columnViewerEditorActivationListener;
	}

	private Collection<IAction> getActions() {
		if (actions == null) {
			actions = createActions();
		}
		return actions;
	}

	protected List<IAction> createActions() {
		return Arrays.asList(
				new SelectEditorConfigAction(this),//
				new ReloadEditorConfigAction(this),//
				zoomAction = new ZoomMenuAction(this),//
				refreshAllModeAction = new RefreshAllAction(this));
	}

	protected void hookViewerColumn(ViewerColumn column) {
		column.setLabelProvider(getColumnLabelProvider());
		int columnIndex = viewerColumns.indexOf(column);
		Assert.isTrue(columnIndex >= 0);
		column.setEditingSupport(new ColumnEditingSupport(getViewer(), columnIndex));
	}

	public TableConfigLabelProvider getLabelProviderForObject(Object object) {
		Object parentObject = contentProvider.getParent(object);
		if (parentObject == null || parentObject instanceof EObject || parentObject instanceof Resource || parentObject instanceof ResourceSet) {
			EObject eObject = parentObject instanceof EObject ? (EObject) parentObject : null;
			EClass eClass = eObject != null ? eObject.eClass() : null;
			TableConfigLabelProvider labelProvider = labelProviderMap.get(eClass != null ? eClass : parentObject);
			if (labelProvider == null) {
				TableConfigDescriptor tableConfig = null;
				if (eClass == null || object instanceof Resource && (parentObject instanceof Resource || parentObject instanceof ResourceSet)) {
					tableConfig = getEditorConfigForObject(object).getHeaderTableConfig();
				} else {
					tableConfig = getEditorConfigForObject(object).getTableConfig(eClass);
				}
				labelProvider = getLabelProviderForTableConfig(tableConfig, object);
				labelProviderMap.put(eClass != null ? eClass : parentObject, labelProvider);
			}
			if (parentObject != null && !(parentObject instanceof Resource || parentObject instanceof ResourceSet)) {
				TableConfigLabelProvider referenceLabelProvider = getLabelProviderForReference(parentObject, object);
				if (referenceLabelProvider != null) {
					return referenceLabelProvider;
				}
			}
			return labelProvider;
		}
		return null;
	}

	private TableConfigLabelProvider getLabelProviderForReference(Object parentObject, Object object) {
		TableConfigDescriptor parentTableConfig = null;
		TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(parentObject);
		if (tableConfigLabelProvider != null) {
			parentTableConfig = tableConfigLabelProvider.getTableConfig();
		}
		if (parentTableConfig != null && parentTableConfig.hasReferenceToTableConfigMap()) {
			EStructuralFeature eFeature = object instanceof EObject ? ((EObject) object).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, object);
			if (eFeature instanceof EReference) {
				TableConfigDescriptor referenceTableConfig = getTableConfigForReference(parentTableConfig, (EReference) eFeature, object);
				if (referenceTableConfig != null) {
					return getLabelProviderForTableConfig(referenceTableConfig, parentObject);
				}
			}
		}
		return null;
	}

	public TableConfigLabelProvider getLabelProviderForTableConfig(TableConfigDescriptor tableConfig, Object object) {
		TableConfigLabelProvider labelProvider = tableConfigToLabelProviderMap.get(tableConfig);
		if (labelProvider == null) {
			if (tableConfig != null) {
				adjustColumns(tableConfig, false);
				if (object != null) {
					asyncUpdateElement(object);
				}
			}
			labelProvider = new TableConfigLabelProvider(tableConfig, getAdapterFactory());
			tableConfigToLabelProviderMap.put(tableConfig, labelProvider);
		}
		return labelProvider;
	}

	private void asyncUpdateElement(final Object element) {
		getViewer().getControl().getDisplay().asyncExec(new Runnable() {

			public void run() {
				getViewer().update(element, null);
			}
		});
	}

	private void asyncUpdateElements(final Object[] elements) {
		getViewer().getControl().getDisplay().asyncExec(new Runnable() {

			public void run() {
				getViewer().update(elements, null);
			}
		});
	}

	private EditorConfigDescriptor getEditorConfigForResource(Resource resource) {
		if (resource != null) {
			URI uri = resource.getURI();
			if (!uri.isPlatform() && !uri.toString().startsWith("cdo://")) {
				uri = resource.getResourceSet().getURIConverter().normalize(uri);
			}

			EditorConfigDescriptor resourceEditorConfig = editorConfigs.get(uri);
			if (resourceEditorConfig == null && !editorConfigs.containsKey(uri)) {
				Resource first = primaryResource;
				if (first == null) {
					first = getResourceSet().getResources().get(0);
				}
				if (first != resource && !first.getURI().fileExtension().equals(uri.fileExtension())) {
					URI resourceEditorConfigURI = EditorConfigPlugin.INSTANCE.getEditorConfig(resource);
					if (resourceEditorConfigURI != null) {
						resourceEditorConfig = loadEditorConfig(resourceEditorConfigURI);
					}
				}
				editorConfigs.put(uri, resourceEditorConfig);
			}

			if (resourceEditorConfig != null) {
				return resourceEditorConfig;
			}
		}
		return editorConfig;
	}

	private Resource primaryResource = null;

	public void setPrimaryResource(Resource resource) {
		this.primaryResource = resource;
	}

	public Map<Object, TableConfigHeader> getTableConfigHeaderMap() {
		return tableConfigHeaderMap;
	}

	private EditorConfigDescriptor getEditorConfigForObject(Object object) {
		if (object instanceof Resource) {
			return getEditorConfigForResource((Resource) object);
		} else if (object instanceof EObject) {
			return getEditorConfigForResource(((EObject) object).eResource());
		} else if (object != null && !(object instanceof ResourceSet)) {
			return getEditorConfigForObject(contentProvider.getParent(object));
		}
		return getEditorConfigForResource(null);
	}

	public TableConfigDescriptor getTableConfigForObject(Object object) {
		EObject eObject = object instanceof EObject ? (EObject) object : null;
		EClass eClass = eObject != null ? eObject.eClass() : null;
		if (object instanceof Resource) {
			return getEditorConfigForObject((Resource) object).getHeaderTableConfig();
		} else if (eClass != null) {
			return getEditorConfigForObject(eObject.eResource()).getTableConfig(eClass);
		} else if (object instanceof ResourceSet) {
			return getEditorConfigForObject(object).getHeaderTableConfig();
		}
		return getEditorConfigForObject(object).getHeaderTableConfig();
	}

	private TableConfigDescriptor getTableConfigForChild(Object parentObject, Object object) {
		TableConfigDescriptor parentTableConfig = null;
		TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(parentObject);
		if (tableConfigLabelProvider != null) {
			parentTableConfig = tableConfigLabelProvider.getTableConfig();
		}
		if (parentTableConfig != null && parentTableConfig.hasReferenceToTableConfigMap()) {
			EStructuralFeature eFeature = object instanceof EObject ? ((EObject) object).eContainmentFeature() : EMFUtil.getChildFeature(getAdapterFactory(), parentObject, object);
			if (eFeature instanceof EReference) {
				TableConfigDescriptor referenceTableConfig = getTableConfigForReference(parentTableConfig, (EReference) eFeature, object);
				if (referenceTableConfig != null) {
					return referenceTableConfig;
				}
			}
		}
		return null;
	}

	public TableConfigDescriptor getTableConfigForReference(TableConfigDescriptor parentTableConfig, EReference eReference, Object object) {
		TableConfigDescriptor tableConfig = parentTableConfig.getTableConfig(eReference);
		if (tableConfig == TableConfigDescriptor.NULL) {
			EClass eClass = eReference.getEReferenceType();
			if (EcorePackage.eINSTANCE.getEObject() == eClass) {
				EObject eObject = object instanceof EObject ? (EObject) object : null;
				eClass = eObject != null ? eObject.eClass() : eClass;
			}

			EObject rootObject = getResourceSet().getResources().get(0).getContents().get(0);
			EditorConfigDescriptor editorConfigDescriptor = null;
			if (eClass.getEPackage() == rootObject.eClass().getEPackage()) {
				editorConfigDescriptor = this.editorConfig;
			} else {
				URI editorConfigURI = EditorConfigPlugin.INSTANCE.getEditorConfig(eClass);
				if (editorConfigURI != null) {
					editorConfigDescriptor = loadEditorConfig(editorConfigURI);
				}
			}
			tableConfig = null;
			if (editorConfigDescriptor != null) {
				tableConfig = editorConfigDescriptor.getTableConfig(eClass);
				if (tableConfig == null) {
					tableConfig = editorConfigDescriptor.getHeaderTableConfig();
				}
			}
			tableConfig = parentTableConfig.setTableConfig(eReference, tableConfig);
		}
		return tableConfig;
	}

	private CellConfig getColumnCellConfig(EObject eObject, int columnIndex) {
		Object parentObject = contentProvider.getParent(eObject);
		if (parentObject == null || parentObject instanceof EObject || parentObject instanceof Resource) {
			TableConfigDescriptor tableConfig = getTableConfigForChild(parentObject, eObject);
			if (tableConfig == null) {
				tableConfig = getTableConfigForObject(parentObject);
			}
			if (tableConfig != null) {
				int shift = tableConfig.isDisplayDefaultColumn() ? 1 : 1 /* 0 */;
				columnIndex -= shift;
				EList<ColumnConfig> columns = tableConfig.getColumn();
				return columnIndex >= 0 && columnIndex < columns.size() ? columns.get(columnIndex).getCellConfig(eObject.eClass()) : null;
			}
		}
		return null;
	}

	private IItemPropertyDescriptor getItemPropertyDescriptor(EObject eObject, int columnIndex) {
		CellConfig cellConfig = getColumnCellConfig(eObject, columnIndex);
		if (cellConfig instanceof RowFeatureCell) {
			EStructuralFeature eFeature = ((RowFeatureCell) cellConfig).getFeature();
			if (eFeature != null) {
				IItemPropertySource itemPropertySource = (IItemPropertySource) getAdapterFactory().adapt(eObject, IItemPropertySource.class);
				if (itemPropertySource != null) {
					return itemPropertySource.getPropertyDescriptor(eObject, eFeature);
				}
			}
		}
		return null;
	}

	public TableConfigHeader getTableConfigHeaderForParent(Object parentObject, boolean force) {
		TableConfigHeader header = tableConfigHeaderMap.get(parentObject);
		if (header == null && !tableConfigHeaderMap.containsKey(parentObject)) {
			TableConfigDescriptor tableConfig = getTableConfigForObject(parentObject);
			if (tableConfig != null && tableConfig.isDisplayHeader()) {
				TableConfigDescriptor parentTableConfig = null;
				TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(parentObject);
				if (tableConfigLabelProvider != null) {
					parentTableConfig = tableConfigLabelProvider.getTableConfig();
				}
				if (!tableConfig.equals(parentTableConfig) || force) {
					header = addHeader(new TableConfigHeader(parentObject, tableConfig, getTableConfigHeaderLevelForParent(parentObject) + 1, myColorScheme));
					if (force) {
						return header;
					}
				}
			}
			tableConfigHeaderMap.put(parentObject, header);
		}
		return header;
	}

	public int getTableConfigHeaderLevelForParent(Object parentObject) {
		int level = 0;
		while ((parentObject = contentProvider.getParent(parentObject)) != null && !(parentObject instanceof Resource) && !(parentObject instanceof ResourceSet)) {
			++level;
		}
		return level;
	}

	private List<TableConfigHeader> findTableConfigHeadersForParent(Object parentObject) {
		List<TableConfigHeader> list = new ArrayList<TableConfigHeader>();
		for (TableConfigHeader i : tableConfigHeaders) {
			if (parentObject.equals(i.getParent(i))) {
				list.add(i);
			}
		}
		return list;
	}

	public boolean hasRepeatingTableConfigHeader(Object parentObject) {
		for (TableConfigHeader i : findTableConfigHeadersForParent(parentObject)) {
			if (i.isRepeating()) {
				return true;
			}
		}
		return false;
	}

	private boolean isReferenceCellEditor(Object element, IItemPropertyDescriptor itemPropertyDescriptor) {
		Object genericFeature = itemPropertyDescriptor.getFeature(element);
		if (element instanceof EObject && genericFeature instanceof EReference) {
			return true;
		}
		return false;
	}

	private CellEditBehaviorOption getCellEditBehavior(Object object, int columnIndex) {
		if (object instanceof EObject) {
			CellConfig cellConfig = getColumnCellConfig((EObject) object, columnIndex);
			if (cellConfig instanceof RowFeatureCell) {
				return ((RowFeatureCell) cellConfig).getCellEditBehavior();
			}
		}
		return null;
	}

	private CellLocateBehaviorOption getCellLocateBehavior(Object object, int columnIndex) {
		if (object instanceof EObject) {
			CellConfig cellConfig = getColumnCellConfig((EObject) object, columnIndex);
			if (cellConfig instanceof RowFeatureCell) {
				return ((RowFeatureCell) cellConfig).getCellLocateBehavior();
			}
		}
		return null;
	}

	private static URI getFont(boolean isHeaderFont, Zoom zoom) {
		URI base = isHeaderFont ? IEditorConfigConstants.HEADER_FONT : IEditorConfigConstants.DEFAULT_FONT;
		return getFont(base, isHeaderFont, zoom);
	}

	private static URI getFont(URI base, boolean isHeaderFont, Zoom zoom) {
		String[] segments = base.segments();
		String zoomSize = isHeaderFont ? zoom.getHeaderFont() : zoom.getDefaultFont();
		if (zoomSize.length() > 0) {
			int delta;
			if (zoomSize.startsWith("+")) { //$NON-NLS-1$
				delta = Integer.parseInt(zoomSize.substring(1));
			} else {
				delta = Integer.parseInt(zoomSize);
			}
			String size = segments[0];
			if (size.startsWith("+")) { //$NON-NLS-1$
				delta = Integer.parseInt(size.substring(1)) + delta;
				size = delta != 0 ? Integer.toString(delta) : ""; //$NON-NLS-1$
			} else if (size.startsWith("-")) { //$NON-NLS-1$
				delta = Integer.parseInt(size) + delta;
				size = delta != 0 ? ((delta > 0 ? "+" : "") + Integer.toString(delta)) : ""; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
			} else if (size.length() > 0) {
				size = Integer.toString(Integer.parseInt(size) + delta);
			} else {
				size = zoomSize;
			}
			segments[0] = size;
		}
		return URI.createHierarchicalURI(base.scheme(), base.authority(), base.device(), segments, base.query(), base.fragment());
	}

	private static int getColumnWidth(int width, Zoom zoom) {
		return (int) Math.round(width * zoom.getColumnWidthRatio());
	}

	private class ColumnViewerLabelProvider extends AdapterFactoryLabelProvider.FontAndColorProvider {

		/**
		 * @param adapterFactory
		 * @param viewer
		 */
		public ColumnViewerLabelProvider(AdapterFactory adapterFactory, Viewer viewer) {
			super(adapterFactory, viewer);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider#
		 * getColumnText(java.lang.Object, int)
		 */
		@Override
		public String getColumnText(Object object, int columnIndex) {
			TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(object);
			if (tableConfigLabelProvider != null) {
				String text = null;
				try {
					text = tableConfigLabelProvider.getColumnText(object, columnIndex);
				} catch (Exception e) {
					e.printStackTrace();
				}
				if (text != null && text != TableConfigLabelProvider.DISPLAY_DEFAULT_COLUMN) {
					return text;
				} else {
					columnIndex = 0;
				}
			}
			if (columnIndex == 0 && object instanceof Resource) {
				URI uri = ((Resource) object).getURI();
				if (uri != null) {
					return uri.lastSegment();
				}
			}
			return super.getColumnText(object, columnIndex);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider#
		 * getColumnImage(java.lang.Object, int)
		 */
		@Override
		public Image getColumnImage(Object object, int columnIndex) {
			TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(object);
			if (tableConfigLabelProvider != null) {
				Object imageObject = tableConfigLabelProvider.getColumnImage(object, columnIndex);
				if (imageObject != TableConfigLabelProvider.DISPLAY_DEFAULT_COLUMN) {
					return getImageFromObject(imageObject);
				} else {
					return super.getColumnImage(object, 0);
				}
			}
			return super.getColumnImage(object, columnIndex);
		}

		@Override
		public Font getFont(Object object, int columnIndex) {
			if (object instanceof TableConfigHeader) {
				return getFontFromObject(headerFont);
			}
			TableConfigLabelProvider tableConfigLabelProvider = getLabelProviderForObject(object);
			if (tableConfigLabelProvider != null) {
				Object font = tableConfigLabelProvider.getFont(object, columnIndex);
				if (font instanceof URI) {
					font = AbstractViewerManager.getFont((URI) font, false, zoom);
				}
				if (font != null) {
					return getFontFromObject(font);
				}
			}
			return getFontFromObject(defaultTextFont);
		}

		@Override
		public Color getBackground(Object object, int columnIndex) {
			if (object instanceof EObject) {
				EObject row = (EObject) object;
				CellConfig cellConfig = getColumnCellConfig(row, columnIndex);
				if (cellConfig != null) {
					boolean isHighlighted = cellConfig.getColorValue(row);
					if (isHighlighted) {
						return ExtendedColorRegistry.INSTANCE.getColor(defaultForeground, defaultBackground, IEditorConfigConstants.BACKGROUND_COLOR_HIGHLIGHTED);
					}
				}
			}
			return super.getBackground(object, columnIndex);
		}
	}

	private class ColumnEditingSupport extends EditingSupport {

		private int columnIndex;

		private Object lastObject;

		private IItemPropertyDescriptor lastObjectItemPropertyDescriptor;

		/**
		 * @param viewer
		 * @param columnIndex
		 */
		public ColumnEditingSupport(ColumnViewer viewer, int columnIndex) {
			super(viewer);
			this.columnIndex = columnIndex;
		}

		private IItemPropertyDescriptor getItemPropertyDescriptor(Object object) {
			if (object == lastObject) {
				return lastObjectItemPropertyDescriptor;
			}
			lastObject = object;
			if (object instanceof EObject) {
				return lastObjectItemPropertyDescriptor = AbstractViewerManager.this.getItemPropertyDescriptor((EObject) object, columnIndex);
			}
			return lastObjectItemPropertyDescriptor = null;
		}

		private CellEditBehaviorOption getCellEditBehavior(Object object) {
			return AbstractViewerManager.this.getCellEditBehavior(object, columnIndex);
		}

		private CellLocateBehaviorOption getCellLocateBehavior(Object object) {
			return AbstractViewerManager.this.getCellLocateBehavior(object, columnIndex);
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.EditingSupport#canEdit(java.lang.Object)
		 */
		@Override
		protected boolean canEdit(Object element) {
			IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(element);
			if (itemPropertyDescriptor != null) {
				CellEditBehaviorOption cellEditBehavior = getCellEditBehavior(element);
				CellLocateBehaviorOption cellLocateBehavior = getCellLocateBehavior(element);
				if (!CellEditBehaviorOption.READ_ONLY.equals(cellEditBehavior) || CellLocateBehaviorOption.SELECTION.equals(cellLocateBehavior)) {
					/** bug 242: Navigation to derived and etc. properties must be allowed
					 * See getColumnViewerEditorActivationListener()
					 */
					if (!defaultEditMode && isReferenceCellEditor(element, itemPropertyDescriptor)) {
						return true;
					}
				}
				return itemPropertyDescriptor.canSetProperty(element) && (!CellEditBehaviorOption.READ_ONLY.equals(cellEditBehavior) || CellLocateBehaviorOption.SELECTION.equals(cellLocateBehavior));
			}
			return false;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.EditingSupport#getCellEditor(java.lang.
		 * Object)
		 */
		@Override
		protected CellEditor getCellEditor(Object element) {
			IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(element);
			if (itemPropertyDescriptor != null) {
				CellEditBehaviorOption cellEditBehavior = getCellEditBehavior(element);
				CellLocateBehaviorOption cellLocateBehavior = getCellLocateBehavior(element);

				if (!defaultEditMode && isReferenceCellEditor(element, itemPropertyDescriptor)) {
					if (CellEditBehaviorOption.SELECTION.equals(cellEditBehavior) || CellLocateBehaviorOption.SELECTION.equals(cellLocateBehavior)) {
						ReferenceCellEditor cellEditor = new ReferenceCellEditor(getViewer(), itemPropertyDescriptor.getChoiceOfValues(element),
								((EStructuralFeature) itemPropertyDescriptor.getFeature(element)).isMany());
						cellEditor.setValue(getValue(element));
						return lastCellEditor = cellEditor;
					}
				}

				if (!CellEditBehaviorOption.READ_ONLY.equals(cellEditBehavior)) {
					PropertyDescriptor propertyDescriptor = new PropertyDescriptor(element, itemPropertyDescriptor);
					return lastCellEditor = propertyDescriptor.createPropertyEditor((Composite) getViewer().getControl());
				}
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.EditingSupport#getValue(java.lang.Object)
		 */
		@Override
		protected Object getValue(Object element) {
			if (defaultEditMode) {
				return defaultEditModeValue;
			}
			IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(element);
			if (itemPropertyDescriptor != null) {
				Object value = itemPropertyDescriptor.getPropertyValue(element);
				if (value instanceof IItemPropertySource) {
					return ((IItemPropertySource) value).getEditableValue(element);
				}
			}
			return null;
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * org.eclipse.jface.viewers.EditingSupport#setValue(java.lang.Object,
		 * java.lang.Object)
		 */
		@Override
		protected void setValue(Object element, Object value) {
			if (defaultEditMode) {
				defaultEditModeValue = value;
				return;
			}
			IItemPropertyDescriptor itemPropertyDescriptor = getItemPropertyDescriptor(element);
			if (itemPropertyDescriptor != null) {
				Object oldValue = itemPropertyDescriptor.getPropertyValue(element);
				if (oldValue instanceof IItemPropertySource) {
					oldValue = ((IItemPropertySource) oldValue).getEditableValue(element);
				}
				if (oldValue != value && (oldValue == null || !oldValue.equals(value))) {
					itemPropertyDescriptor.setPropertyValue(element, value);
				}
			}
		}
	}

}
