/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.ui;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.runtime.Assert;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ColumnViewer;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.MouseAdapter;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Item;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;

/**
 * @author Max Stepanov
 *
 */
/* package*/ class ReferenceCellEditor extends CellEditor {
	
	private ColumnViewer viewer;
	private Collection<?> choiceOfValues;
	private boolean isMany;
	private Object value;
	private List<Item> selectedItems;
	private List<Object> selectedValues;	
	private Listener filterListener;
	private List<TreePath> expandedTreePaths;
	private ViewerCell viewerCell;
	private boolean switchDefaultEditMode;
	
	/**
	 * 
	 */
	public ReferenceCellEditor(ColumnViewer viewer, Collection<?> choiceOfValues, boolean isMany) {
		this.viewer = viewer;
		this.choiceOfValues = choiceOfValues;
		this.isMany = isMany;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#createControl(org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected Control createControl(Composite parent) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(Messages.ReferenceCellEditor_Label);
		label.setToolTipText(Messages.ReferenceCellEditor_Tooltip);
		label.setBackground(parent.getDisplay().getSystemColor(SWT.COLOR_YELLOW));
		label.addMouseListener(new MouseAdapter() {
			public void mouseDown(MouseEvent e) {
				fireApplyEditorValue();
			}
		});
		return label;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doGetValue()
	 */
	@Override
	protected Object doGetValue() {
		Assert.isNotNull(selectedValues);
		if (isMany) {
			value = selectedValues;
		} else if (selectedValues.isEmpty()) {
			value = null;
		} else {
			value = selectedValues.get(0);
		}
		return value;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetFocus()
	 */
	@Override
	protected void doSetFocus() {
		viewer.getControl().setFocus();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#doSetValue(java.lang.Object)
	 */
	@Override
	protected void doSetValue(Object value) {
		this.value = value;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#dependsOnExternalFocusListener()
	 */
	@Override
	protected boolean dependsOnExternalFocusListener() {
		return false;
	}
	
	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#activate()
	 */
	@Override
	public void activate() {
		super.activate();
		if (filterListener == null) {
			filterListener = new Listener() {
				public void handleEvent(Event event) {
					if (event.type == SWT.MouseDown) {
						if (event.widget == viewer.getControl()) {
							event.type = SWT.None;
							ViewerCell cell = viewer.getCell(new Point(event.x, event.y));
							if (cell != null) {
								doSelect(cell);
							}
						} else if (event.widget != getControl()) {
							focusLost();
						}
					} else if (event.type == SWT.KeyUp) {
						if (event.keyCode == SWT.ALT) {
							KeyEvent keyEvent = new KeyEvent(event);
							keyEvent.character = SWT.CR;
							keyReleaseOccured(keyEvent);
						} else if (event.keyCode == SWT.SHIFT) {
							switchDefaultEditMode = true;
							viewer.editElement(viewerCell.getElement(), viewerCell.getColumnIndex());
						} else if (event.character == SWT.ESC
								|| event.character == SWT.BS) {
							KeyEvent keyEvent = new KeyEvent(event);
							keyEvent.character = SWT.ESC;
							keyReleaseOccured(keyEvent);
							event.type = SWT.None;
						} else if (event.character == SWT.DEL) {
							event.type = SWT.None;
							doClearSelection();
						}
					} else if (event.type == SWT.Expand) {
						// Bug 139 workaround
						if (event.widget == viewer.getControl()) {
							event.type = SWT.None;
						}
					}
				}
			};
			Display display = viewer.getControl().getDisplay();
			display.addFilter(SWT.MouseDown, filterListener);
			display.addFilter(SWT.KeyUp, filterListener);
			display.addFilter(SWT.Expand, filterListener);
		}
		saveViewerState();
		selectAndReveal(true);
		revealChoiceOfValues();
	}

	/* (non-Javadoc)
	 * @see org.eclipse.jface.viewers.CellEditor#deactivate()
	 */
	@Override
	public void deactivate() {
		if (filterListener != null) {
			Display display = viewer.getControl().getDisplay();
			display.removeFilter(SWT.MouseDown, filterListener);
			display.removeFilter(SWT.KeyUp, filterListener);
			display.removeFilter(SWT.Expand, filterListener);
			filterListener = null;
			restoreViewerState();
			
			/* delayed async execution to override xEditor's default action:
			 * setSelectionToViewer(mostRecentCommand.getAffectedObjects())
			 */
			display.asyncExec(new Runnable() {
				public void run() {
					if (!viewer.getControl().isDisposed()) {
						selectAndReveal(true);
					}
				}
			});
		}
		viewerCell = null;
		super.deactivate();
	}
	
	private void doSelect(ViewerCell cell) {
		Control control = viewer.getControl();
		if (control instanceof Tree) {
			Tree tree = (Tree) control;
			Object element = cell.getElement();
			if (selectedValues.contains(element)) {
				selectedValues.remove(element);
				selectedItems.remove(cell.getItem());
			} else {
				if (choiceOfValues.contains(element)) {
					if (!isMany) {
						selectedValues.clear();
						selectedItems.clear();
					}
					selectedValues.add(element);
					selectedItems.add((Item) cell.getItem());
				}				
			}
			tree.setSelection(selectedItems.toArray(new TreeItem[selectedItems.size()]));
		}
	}
	
	private void doClearSelection() {
		Control control = viewer.getControl();
		if (control instanceof Tree) {
			Tree tree = (Tree) control;
			selectedValues.clear();
			selectedItems.clear();
			tree.deselectAll();
		}	
	}
				
	/* package */ void selectAndReveal(boolean reveal) {
		List<Object> list = new ArrayList<Object>();
		if (value instanceof Collection<?>) {
			for (Object o : (Collection<?>)value) {
				list.add(o);
			}
		} else {
			list.add(value);
		}
		if (selectedValues == null) {
			selectedValues = new ArrayList<Object>();
			selectedValues.addAll(list);
		}
		viewer.setSelection(new StructuredSelection(list), reveal);
		if (selectedItems == null) {
			selectedItems = new ArrayList<Item>();
			Control control = viewer.getControl();
			if (control instanceof Tree) {
				Tree tree = (Tree) control;
				for (TreeItem item : tree.getSelection()) {
					selectedItems.add(item);
				}
			}
		}
	}
	
	/* package */ Object getInitialValue() {
		return value;
	}
	
	/* package */ void setViewerCell(ViewerCell viewerCell) {
		this.viewerCell = viewerCell;
	}
	
	/* package */ boolean isSwitchDefaultEditMode() {
		return switchDefaultEditMode;
	}
	
	private void revealChoiceOfValues() {
		for (Object element : choiceOfValues) {
			if (element != null) {
				viewer.reveal(element);
			}
		}
	}
	
	private void saveViewerState() {
		if (viewer instanceof TreeViewer) {
			expandedTreePaths = new ArrayList<TreePath>();
			for (TreePath path : ((TreeViewer) viewer).getExpandedTreePaths()) {
				expandedTreePaths.add(path);
			}
		}
	}
	
	private void restoreViewerState() {
		if (expandedTreePaths != null) {
			((TreeViewer) viewer).setExpandedTreePaths(
					expandedTreePaths.toArray(new TreePath[expandedTreePaths.size()]));
			expandedTreePaths = null;
		}
	}

}
