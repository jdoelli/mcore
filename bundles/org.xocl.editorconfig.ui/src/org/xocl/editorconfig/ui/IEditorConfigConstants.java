/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui;

import org.eclipse.emf.common.util.URI;

/**
 * @author Max Stepanov
 *
 */
public interface IEditorConfigConstants {

	public static final int DEFAULT_COLUMN_WIDTH = 100;	
	public static final URI DEFAULT_FONT = URI.createHierarchicalURI("font", "", null, new String[] { "", "" }, null, null); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	public static final URI HEADER_FONT = URI.createHierarchicalURI("font", "", null, new String[] { "-2", "italic" }, null, null); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
	
	public static final URI BACKGROUND_COLOR_HIGHLIGHTED = URI.createHierarchicalURI("color", "rgb", null, new String[] { "255", "120", "120"}, null, null); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$ //$NON-NLS-5$

	public static final boolean CONFIGURATION_ALWAYS_DISPLAY_REPEATING_HEADER = true;

}
