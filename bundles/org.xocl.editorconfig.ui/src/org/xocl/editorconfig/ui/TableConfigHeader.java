/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.editorconfig.ui;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.ITableItemColorProvider;
import org.eclipse.emf.edit.provider.ITableItemFontProvider;
import org.eclipse.emf.edit.provider.ITableItemLabelProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

/**
 * @author Max Stepanov
 *
 */
/* package */ class TableConfigHeader implements ITreeItemContentProvider, IItemLabelProvider, ITableItemLabelProvider, ITableItemFontProvider, ITableItemColorProvider {

	private static final String EMPTY_STRING = ""; //$NON-NLS-1$

	private Object parent;
	private TableConfigDescriptor tableConfig;
	private int level;
	private int columnCount = 0;
	private boolean displayDefaultColumn = true;
	private int shift;
	private boolean repeating;
	
	private Object font;
	private Object background;

	/**
	 * 
	 * @param parent
	 * @param tableConfig
	 * @param level
	 * @param repeating
	 */
	public TableConfigHeader(Object parent, TableConfigDescriptor tableConfig, int level, boolean repeating, TableConfigColorScheme colorScheme) {
		this.parent = parent;
		this.tableConfig = tableConfig;
		this.level = level;
		this.repeating = repeating;
		if (tableConfig != null) {
			columnCount = tableConfig.getColumn().size();
			displayDefaultColumn = tableConfig.isDisplayDefaultColumn();
		}
		shift = displayDefaultColumn ? 1 : 1 /* 0 */;
		font = URI.createHierarchicalURI("font", "", null, new String[] { "", "italic"}, null, null); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$ //$NON-NLS-4$
		background = colorScheme.getHeaderBackground(level);
	}

	public TableConfigHeader(Object parent, TableConfigDescriptor tableConfig, int level, TableConfigColorScheme colorScheme) {
		this(parent, tableConfig, level, false, colorScheme);
	}

	/* package */ int getColumnCount() {
		return columnCount;
	}
	
	/* package */ int getLevel() {
		return level;
	}

	/* package */ boolean isRepeating() {
		return repeating;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemLabelProvider#getColumnText(java.lang.Object, int)
	 */
	public String getColumnText(Object object, int columnIndex) {
		if (columnIndex == 0 /* && displayDefaultColumn*/) {
			String text = tableConfig.getLabel();
			if (!repeating && level > 1) {
				return "    " + text; //$NON-NLS-1$
			}
			return text;
		}
		columnIndex -= shift;
		if (0 <= columnIndex && columnIndex < columnCount) {
			return tableConfig.getColumn().get(columnIndex).getLabel();
		}
		return EMPTY_STRING;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemLabelProvider#getColumnImage(java.lang.Object, int)
	 */
	public Object getColumnImage(Object object, int columnIndex) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.IItemLabelProvider#getImage(java.lang.Object)
	 */
	public Object getImage(Object object) {
		return getColumnImage(object, 0);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.IItemLabelProvider#getText(java.lang.Object)
	 */
	public String getText(Object object) {
		return getColumnText(object, 0);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemFontProvider#getFont(java.lang.Object, int)
	 */
	public Object getFont(Object object, int columnIndex) {
		return font;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemColorProvider#getBackground(java.lang.Object, int)
	 */
	public Object getBackground(Object object, int columnIndex) {
		return background;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITableItemColorProvider#getForeground(java.lang.Object, int)
	 */
	public Object getForeground(Object object, int columnIndex) {
		return null;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITreeItemContentProvider#getChildren(java.lang.Object)
	 */
	public Collection<?> getChildren(Object object) {
		return Collections.EMPTY_LIST;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.IStructuredItemContentProvider#getElements(java.lang.Object)
	 */
	public Collection<?> getElements(Object object) {
		return Collections.EMPTY_LIST;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITreeItemContentProvider#getParent(java.lang.Object)
	 */
	public Object getParent(Object object) {
		return parent;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.provider.ITreeItemContentProvider#hasChildren(java.lang.Object)
	 */
	public boolean hasChildren(Object object) {
		return false;
	}

}
