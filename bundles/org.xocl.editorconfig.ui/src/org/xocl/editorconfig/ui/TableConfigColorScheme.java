package org.xocl.editorconfig.ui;

import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.graphics.RGB;

public abstract class TableConfigColorScheme {

	public abstract URI getHeaderBackground(int level);

	protected static URI createRGBColor(int red, int green, int blue) {
		return URI.createHierarchicalURI("color", "rgb", null, new String[] { String.valueOf(red), String.valueOf(green), String.valueOf(blue) }, null, null); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static class Grayscale extends TableConfigColorScheme {

		public URI getHeaderBackground(int level) {
			int colorIndex = 240 - 20 * level;
			return createRGBColor(colorIndex, colorIndex, colorIndex);
		}
	}

	public static class Gradient extends TableConfigColorScheme {

		private final RGB myStart;

		private final RGB myStop;

		private final int mySteps;

		public Gradient(RGB start, RGB stop, int steps) {
			myStart = start;
			myStop = stop;
			mySteps = steps;
		}

		@Override
		public URI getHeaderBackground(int level) {
			level = level % mySteps;
			int red = myStart.red + (myStop.red - myStart.red) / mySteps * level;
			int green = myStart.green + (myStop.green - myStart.green) / mySteps * level;
			int blue = myStart.blue + (myStop.blue - myStart.blue) / mySteps * level;
			return createRGBColor(red, green, blue);
		}
	}

	public static class AlternateColors extends TableConfigColorScheme {

		private final TableConfigColorScheme[] myColors;

		public AlternateColors(TableConfigColorScheme... colors) {
			myColors = colors;
		}

		@Override
		public URI getHeaderBackground(int level) {
			TableConfigColorScheme scheme = myColors[level % myColors.length];
			return scheme.getHeaderBackground(level / myColors.length);
		}
	}

	public static class GrayBlueGreen extends AlternateColors {

		public GrayBlueGreen() {
			super(//
					new Gradient(new RGB(0xD8, 0xEE, 0xD8), new RGB(0x32, 0xF5, 0x4F), 10), //
					new Gradient(new RGB(0xBD, 0xC1, 0xDB), new RGB(0x3C, 0x51, 0xF6), 10), //
					new Gradient(new RGB(0xE9, 0xD9, 0xED), new RGB(0x6E, 0x47, 0x7B), 10) //
			);
		}
	}
}
