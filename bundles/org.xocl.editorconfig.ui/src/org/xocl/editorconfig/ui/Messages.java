package org.xocl.editorconfig.ui;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.editorconfig.ui.messages"; //$NON-NLS-1$
	public static String ReferenceCellEditor_Label;
	public static String ReferenceCellEditor_Tooltip;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
