/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EReference;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.TableConfig;
import org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl;

/**
 * @author Max Stepanov
 *
 */
/* package */class TableConfigDescriptor {

	public static final TableConfigDescriptor NULL = new TableConfigDescriptor(null, null);

	private EditorConfigDescriptor editorConfig;

	private TableConfig tableConfig;

	private String label;

	private Map<EReference, TableConfigDescriptor> eReferenceToTableConfigMap = new HashMap<EReference, TableConfigDescriptor>();

	/**
	 * 
	 */
	public TableConfigDescriptor(EditorConfigDescriptor editorConfig, TableConfig tableConfig, String label) {
		this.editorConfig = editorConfig;
		this.tableConfig = tableConfig;
		this.label = label != null && label.length() > 0 ? label : null;
	}

	public TableConfigDescriptor(EditorConfigDescriptor editorConfig, TableConfig tableConfig) {
		this(editorConfig, tableConfig, null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof TableConfigDescriptor) {
			TableConfigDescriptor other = (TableConfigDescriptor) obj;
			return other.tableConfig.equals(tableConfig) && ((other.label != null && other.label.equals(label)) || other.label == label);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		result = prime * result + tableConfig.hashCode();
		return result;
	}

	/**
	 * @return
	 * @see org.xocl.editorconfig.TableConfig#getColumn()
	 */
	public EList<ColumnConfig> getColumn() {
		return tableConfig.getColumn();
	}

	/**
	 * @return
	 */
	public boolean hasReferenceToTableConfigMap() {
		return !tableConfig.getReferenceToTableConfigMap().isEmpty();
	}

	/**
	 * @param eReference
	 * @return
	 */
	public TableConfigDescriptor getTableConfig(EReference eReference) {
		if (eReferenceToTableConfigMap.containsKey(eReference)) {
			return eReferenceToTableConfigMap.get(eReference);
		}
		TableConfigDescriptor result = null;
		EMap<EReference, TableConfig> map = tableConfig.getReferenceToTableConfigMap();
		int index = map.indexOfKey(eReference);
		if (index >= 0) {
			EReferenceToTableConfigMapEntryImpl entry = (EReferenceToTableConfigMapEntryImpl) map.get(index);
			if (entry != null) {
				TableConfig referenceTableConfig = entry.getValue();
				String label = entry.getLabel();
				if (referenceTableConfig != null && label != null && !label.equals(referenceTableConfig.getLabel())) {
					result = new TableConfigDescriptor(editorConfig, referenceTableConfig, label);
				} else {
					result = editorConfig.getTableConfig(referenceTableConfig);
					if (result == null) {
						result = NULL;
					}
				}
			}
		}
		eReferenceToTableConfigMap.put(eReference, result);
		return result;
	}

	public TableConfigDescriptor setTableConfig(EReference eReference, TableConfigDescriptor referenceTableConfig) {
		TableConfigDescriptor result = referenceTableConfig;
		EMap<EReference, TableConfig> map = tableConfig.getReferenceToTableConfigMap();
		int index = map.indexOfKey(eReference);
		if (index >= 0) {
			EReferenceToTableConfigMapEntryImpl entry = (EReferenceToTableConfigMapEntryImpl) map.get(index);
			if (entry != null) {
				String label = entry.getLabel();
				if (referenceTableConfig != null && label != null && !label.equals(referenceTableConfig.getLabel())) {
					result = new TableConfigDescriptor(editorConfig, referenceTableConfig.tableConfig, label);
				}
			}
		}
		eReferenceToTableConfigMap.put(eReference, result);
		return result;
	}

	/**
	 * @return
	 * @see org.xocl.editorconfig.TableConfig#isDisplayDefaultColumn()
	 */
	public boolean isDisplayDefaultColumn() {
		return tableConfig.isDisplayDefaultColumn();
	}

	/**
	 * @return
	 * @see org.xocl.editorconfig.TableConfig#isDisplayHeader()
	 */
	public boolean isDisplayHeader() {
		return tableConfig.isDisplayHeader();
	}

	/**
	 * @return
	 * @see org.xocl.editorconfig.TableConfig#getLabel()
	 */
	public String getLabel() {
		if (label != null) {
			return label;
		}
		return tableConfig.getLabel();
	}

	private int getTableConfigReferenceIndex(EReference eReference) {
		return tableConfig.getReferenceToTableConfigMap().indexOfKey(eReference);
	}

	public Comparator<EReference> getEReferenceComparator() {
		return new Comparator<EReference>() {

			@Override
			public int compare(EReference o1, EReference o2) {
				return getTableConfigReferenceIndex(o1) - getTableConfigReferenceIndex(o2);
			}
		};
	}

}
