/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.eclipse.swt.widgets.Tree;

/**
 * @author Max Stepanov
 *
 */
/* package */final class SWTUtil {

	/**
	 * 
	 */
	private SWTUtil() {
	}
	
	public static void setItemHeight(Tree tree, int itemHeight) {
		try {
			Method setItemHeight = tree.getClass().getDeclaredMethod("setItemHeight", int.class); //$NON-NLS-1$
			boolean prevAccessible = setItemHeight.isAccessible();
			setItemHeight.setAccessible(true);
			setItemHeight.invoke(tree, itemHeight);
			setItemHeight.setAccessible(prevAccessible);
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
	}

}
