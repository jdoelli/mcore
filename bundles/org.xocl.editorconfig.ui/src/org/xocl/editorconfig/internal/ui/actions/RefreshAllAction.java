/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.xocl.editorconfig.internal.ui.Activator;
import org.xocl.editorconfig.ui.IEditorConfigViewer;

/**
 * @author Max Stepanov
 *
 */
public class RefreshAllAction extends Action {
	private static final String REFRESH_ALL_ACTION_ID = "refresh_all_action_id"; //$NON-NLS-1$

	private final IEditorConfigViewer editorConfigViewer;

	public RefreshAllAction(IEditorConfigViewer editorConfigViewer) {
		super(Messages.RefreshAllAction_Text, IAction.AS_CHECK_BOX);
		setId(REFRESH_ALL_ACTION_ID);
		this.editorConfigViewer = editorConfigViewer;
		setImageDescriptor(Activator.getDefault().getImageRegistry().getDescriptor(Activator.REFRESH_ALL_MODE_BUTTON_IMAGE_ID));
		setToolTipText(Messages.RefreshAllAction_Tooltip);
		setChecked(true);
	}

	@Override
	public void run() {
		editorConfigViewer.setRefreshAllMode(isChecked());
	}

}