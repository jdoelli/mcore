/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

/**
 * @author Max Stepanov
 *
 */
public interface IUpdate {

	public void update();
}
