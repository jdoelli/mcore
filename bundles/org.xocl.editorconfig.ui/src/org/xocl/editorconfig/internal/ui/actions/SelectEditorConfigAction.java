/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.internal.ui.actions;

import org.eclipse.emf.common.ui.dialogs.ResourceDialog;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.Window;
import org.xocl.editorconfig.internal.ui.dialogs.SelectEditorConfigDialog;
import org.xocl.editorconfig.ui.IEditorConfigViewer;

/**
 * @author Max Stepanov
 *
 */
public final class SelectEditorConfigAction extends Action {
	
	private final IEditorConfigViewer editorConfigViewer;

	public SelectEditorConfigAction(IEditorConfigViewer editorConfigViewer) {
		super(Messages.SelectEditorConfigAction_Text);
		this.editorConfigViewer = editorConfigViewer;
	}

	@Override
	public void run() {
		ResourceDialog dlg = new SelectEditorConfigDialog(editorConfigViewer.getControl().getShell());
		if (dlg.open() == Window.OK) {
			editorConfigViewer.setEditorConfig(dlg.getURIs().get(0));
		}
	}
}