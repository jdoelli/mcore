package org.xocl.editorconfig.internal.ui.dialogs;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.editorconfig.internal.ui.dialogs.messages"; //$NON-NLS-1$
	public static String SelectEditorConfigDialog_Title;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
