/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Constant Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition#getConstantLet <em>Constant Let</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralConstantBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<const> \'.concat(if namedConstant.oclIsUndefined() then \'\' else namedConstant.aName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='if namedConstant.oclIsUndefined() then \'MISSING EXPRESSION\' else namedConstant.aName endif' calculatedBaseDerive='ExpressionBase::ConstantLiteralValue' aMandatoryDerive='if constantLet.oclIsUndefined() then true\r\nelse constantLet.aMandatory endif' aClassifierDerive='if constantLet.oclIsUndefined() then null\r\nelse constantLet.aClassifier endif' aSingularDerive='if constantLet.oclIsUndefined() then true\r\nelse constantLet.aSingular endif'"
 * @generated
 */

public interface MLiteralConstantBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Named Constant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Constant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Constant</em>' reference.
	 * @see #isSetNamedConstant()
	 * @see #unsetNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralConstantBaseDefinition_NamedConstant()
	 * @model unsettable="true"
	 * @generated
	 */
	MNamedConstant getNamedConstant();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Constant</em>' reference.
	 * @see #isSetNamedConstant()
	 * @see #unsetNamedConstant()
	 * @see #getNamedConstant()
	 * @generated
	 */

	void setNamedConstant(MNamedConstant value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedConstant()
	 * @see #getNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @generated
	 */
	void unsetNamedConstant();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Constant</em>' reference is set.
	 * @see #unsetNamedConstant()
	 * @see #getNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @generated
	 */
	boolean isSetNamedConstant();

	/**
	 * Returns the value of the '<em><b>Constant Let</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Let</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Let</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMLiteralConstantBaseDefinition_ConstantLet()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if namedConstant.expression.oclIsUndefined() then  null\r\nelse if namedConstant.expression.oclIsKindOf(MLiteralLet) \r\n\tthen namedConstant.expression.oclAsType(MLiteralLet)\r\n\telse null endif \r\nendif'"
	 * @generated
	 */
	MLiteralLet getConstantLet();

} // MLiteralConstantBaseDefinition
