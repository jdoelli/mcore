/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MContainer Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMContainerBaseDefinition()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MContainerBaseDefinition'"
 *        annotation="http://www.xocl.org/OCL label='\ncalculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'eContainer()\'\n' calculatedBaseDerive='ExpressionBase::EContainer' aMandatoryDerive='true\n' aSimpleTypeDerive='acore::classifiers::ASimpleType::None' aClassifierDerive='null /* SimpleType::Object \052/'"
 * @generated
 */

public interface MContainerBaseDefinition extends MAbstractBaseDefinition {
} // MContainerBaseDefinition
