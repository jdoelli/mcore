/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.ASimpleType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAChainEntryType <em>AChain Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getChainAsCode <em>Chain As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAElement1 <em>AElement1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getElement1Correct <em>Element1 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAElement2EntryType <em>AElement2 Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAElement2 <em>AElement2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getElement2Correct <em>Element2 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAElement3EntryType <em>AElement3 Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAElement3 <em>AElement3</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getElement3Correct <em>Element3 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getACastType <em>ACast Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getALastElement <em>ALast Element</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAChainCalculatedType <em>AChain Calculated Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getAChainCalculatedSimpleType <em>AChain Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getChainCalculatedSingular <em>Chain Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getProcessor <em>Processor</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractChain#getProcessorDefinition <em>Processor Definition</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractChain extends EObject {
	/**
	 * Returns the value of the '<em><b>AChain Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AChain Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AChain Entry Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AChainEntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Chain Entry Type'"
	 *        annotation="http://www.xocl.org/OCL derive='let c:acore::classifiers::AClassifier=null in c'"
	 * @generated
	 */
	AClassifier getAChainEntryType();

	/**
	 * Returns the value of the '<em><b>Chain As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain As Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain As Code</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_ChainAsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='unsafeChainAsCode(1)'"
	 * @generated
	 */
	String getChainAsCode();

	/**
	 * Returns the value of the '<em><b>AElement1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AElement1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AElement1</em>' reference.
	 * @see #isSetAElement1()
	 * @see #unsetAElement1()
	 * @see #setAElement1(AProperty)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AElement1()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Element 1'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='\r\nlet annotatedProp: acore::classifiers::AProperty = \r\nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)\r\nin\r\nif self.aChainEntryType.oclIsUndefined() then \r\nOrderedSet{} else\r\nself.aChainEntryType.aAllProperty()\r\nendif\r\n\r\n--'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	AProperty getAElement1();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement1 <em>AElement1</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AElement1</em>' reference.
	 * @see #isSetAElement1()
	 * @see #unsetAElement1()
	 * @see #getAElement1()
	 * @generated
	 */

	void setAElement1(AProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement1 <em>AElement1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAElement1()
	 * @see #getAElement1()
	 * @see #setAElement1(AProperty)
	 * @generated
	 */
	void unsetAElement1();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement1 <em>AElement1</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AElement1</em>' reference is set.
	 * @see #unsetAElement1()
	 * @see #getAElement1()
	 * @see #setAElement1(AProperty)
	 * @generated
	 */
	boolean isSetAElement1();

	/**
	 * Returns the value of the '<em><b>Element1 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element1 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element1 Correct</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_Element1Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aElement1.oclIsUndefined() then true else\r\n--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else\r\nif aChainEntryType.oclIsUndefined() then false \r\n  else aChainEntryType.aAllProperty()->includes(aElement1)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement1Correct();

	/**
	 * Returns the value of the '<em><b>AElement2 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AElement2 Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AElement2 Entry Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AElement2EntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Element 2 Entry Type'"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.element1Correct then null\r\nelse \r\nif self.aElement1.oclIsUndefined() then null \r\nelse self.aElement1.aClassifier endif endif'"
	 * @generated
	 */
	AClassifier getAElement2EntryType();

	/**
	 * Returns the value of the '<em><b>AElement2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AElement2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AElement2</em>' reference.
	 * @see #isSetAElement2()
	 * @see #unsetAElement2()
	 * @see #setAElement2(AProperty)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AElement2()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Element 2'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='let annotatedProp: acore::classifiers::AProperty = \r\nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)\r\nin\r\n\r\nif aElement1.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if not(aElement1.aOperation->isEmpty())\r\n    then OrderedSet{} \r\n    else if aElement2EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else aElement2EntryType.aAllProperty() endif\r\n  \t endif\r\n  endif\r\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	AProperty getAElement2();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement2 <em>AElement2</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AElement2</em>' reference.
	 * @see #isSetAElement2()
	 * @see #unsetAElement2()
	 * @see #getAElement2()
	 * @generated
	 */

	void setAElement2(AProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement2 <em>AElement2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAElement2()
	 * @see #getAElement2()
	 * @see #setAElement2(AProperty)
	 * @generated
	 */
	void unsetAElement2();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement2 <em>AElement2</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AElement2</em>' reference is set.
	 * @see #unsetAElement2()
	 * @see #getAElement2()
	 * @see #setAElement2(AProperty)
	 * @generated
	 */
	boolean isSetAElement2();

	/**
	 * Returns the value of the '<em><b>Element2 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element2 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element2 Correct</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_Element2Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aElement2.oclIsUndefined() then true else\r\n--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else\r\nif aElement2EntryType.oclIsUndefined() then false \r\n  else aElement2EntryType.aAllProperty()->includes(self.aElement2)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement2Correct();

	/**
	 * Returns the value of the '<em><b>AElement3 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AElement3 Entry Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AElement3 Entry Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AElement3EntryType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Element 3 Entry Type'"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.element2Correct then null\r\nelse \r\nif self.aElement2.oclIsUndefined() then null\r\nelse self.aElement2.aClassifier endif endif'"
	 * @generated
	 */
	AClassifier getAElement3EntryType();

	/**
	 * Returns the value of the '<em><b>AElement3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AElement3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AElement3</em>' reference.
	 * @see #isSetAElement3()
	 * @see #unsetAElement3()
	 * @see #setAElement3(AProperty)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AElement3()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Element 3'"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='let annotatedProp: acore::classifiers::AProperty = \r\nself.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty )\r\nin\r\n\r\nif aElement2.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if not aElement2.aOperation->isEmpty()\r\n    then OrderedSet{} \r\n    else if aElement3EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else aElement3EntryType.aAllProperty() endif\r\n  \t endif\r\n  endif\r\n'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	AProperty getAElement3();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement3 <em>AElement3</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AElement3</em>' reference.
	 * @see #isSetAElement3()
	 * @see #unsetAElement3()
	 * @see #getAElement3()
	 * @generated
	 */

	void setAElement3(AProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement3 <em>AElement3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAElement3()
	 * @see #getAElement3()
	 * @see #setAElement3(AProperty)
	 * @generated
	 */
	void unsetAElement3();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getAElement3 <em>AElement3</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AElement3</em>' reference is set.
	 * @see #unsetAElement3()
	 * @see #getAElement3()
	 * @see #setAElement3(AProperty)
	 * @generated
	 */
	boolean isSetAElement3();

	/**
	 * Returns the value of the '<em><b>Element3 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Element3 Correct</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Element3 Correct</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_Element3Correct()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aElement3.oclIsUndefined() then true else\r\n--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else\r\nif aElement3EntryType.oclIsUndefined() then false\r\n  else aElement3EntryType.aAllProperty()->includes(self.aElement3)\r\nendif endif \r\n--endif'"
	 * @generated
	 */
	Boolean getElement3Correct();

	/**
	 * Returns the value of the '<em><b>ACast Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ACast Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACast Type</em>' reference.
	 * @see #isSetACastType()
	 * @see #unsetACastType()
	 * @see #setACastType(AClassifier)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_ACastType()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Cast Type'"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.aActiveClass'"
	 * @generated
	 */
	AClassifier getACastType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getACastType <em>ACast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ACast Type</em>' reference.
	 * @see #isSetACastType()
	 * @see #unsetACastType()
	 * @see #getACastType()
	 * @generated
	 */

	void setACastType(AClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getACastType <em>ACast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetACastType()
	 * @see #getACastType()
	 * @see #setACastType(AClassifier)
	 * @generated
	 */
	void unsetACastType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getACastType <em>ACast Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ACast Type</em>' reference is set.
	 * @see #unsetACastType()
	 * @see #getACastType()
	 * @see #setACastType(AClassifier)
	 * @generated
	 */
	boolean isSetACastType();

	/**
	 * Returns the value of the '<em><b>ALast Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALast Element</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALast Element</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_ALastElement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Last Element'"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.aElement3.oclIsUndefined() then self.aElement3 else\r\nif not self.aElement2.oclIsUndefined() then self.aElement2 else\r\nif not self.aElement1.oclIsUndefined() then self.aElement1 else\r\nnull endif endif endif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AProperty getALastElement();

	/**
	 * Returns the value of the '<em><b>AChain Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AChain Calculated Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AChain Calculated Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AChainCalculatedType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Chain Calculated Type'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 * @generated
	 */
	AClassifier getAChainCalculatedType();

	/**
	 * Returns the value of the '<em><b>AChain Calculated Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AChain Calculated Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AChain Calculated Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_AChainCalculatedSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='A Chain Calculated SimpleType'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::ASimpleType = null in nl\n'"
	 * @generated
	 */
	ASimpleType getAChainCalculatedSimpleType();

	/**
	 * Returns the value of the '<em><b>Chain Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Chain Calculated Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Chain Calculated Singular</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_ChainCalculatedSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: Boolean = null in nl\n'"
	 * @generated
	 */
	Boolean getChainCalculatedSingular();

	/**
	 * Returns the value of the '<em><b>Processor</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MProcessor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mrules.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #setProcessor(MProcessor)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_Processor()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='false'"
	 * @generated
	 */
	MProcessor getProcessor();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Processor</em>' attribute.
	 * @see com.montages.mrules.expressions.MProcessor
	 * @see #isSetProcessor()
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @generated
	 */

	void setProcessor(MProcessor value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	void unsetProcessor();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractChain#getProcessor <em>Processor</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Processor</em>' attribute is set.
	 * @see #unsetProcessor()
	 * @see #getProcessor()
	 * @see #setProcessor(MProcessor)
	 * @generated
	 */
	boolean isSetProcessor();

	/**
	 * Returns the value of the '<em><b>Processor Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Processor Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Processor Definition</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractChain_ProcessorDefinition()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = processor = mrules::expressions::MProcessor::None in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen null\n  else createProcessorDefinition()\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='false'"
	 * @generated
	 */
	MProcessorDefinition getProcessorDefinition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if not aElement3.oclIsUndefined() then 3\r\nelse if not aElement2.oclIsUndefined() then 2\r\nelse if not aElement1.oclIsUndefined() then 1\r\nelse 0 endif endif endif'"
	 * @generated
	 */
	Integer length();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stepRequired="true"
	 *        stepAnnotation="http://www.montages.com/mCore/MCore mName='step'"
	 *        annotation="http://www.xocl.org/OCL body='let element: acore::classifiers::AProperty = if step=1 then aElement1\r\n\telse if step=2 then aElement2 \r\n\t\telse if step=3 then aElement3\r\n\t\t\telse null endif endif endif in\r\n\t\r\nif element.oclIsUndefined() then \'ERROR\' else element.aName endif'"
	 * @generated
	 */
	String unsafeElementAsCode(Integer step);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model stepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='if step=1 then\r\n\tif aElement1.oclIsUndefined() then \'MISSING ELEMENT 1\'\r\n\telse unsafeElementAsCode(1) endif\r\nelse if step=2 then\r\n\tif aElement2.oclIsUndefined() then \'MISSING ELEMENT 2\'\r\n\telse unsafeElementAsCode(2) endif\r\nelse if step=3 then\r\n\tif aElement3.oclIsUndefined() then \'MISSING ELEMENT 3\'\r\n\telse unsafeElementAsCode(3) endif\r\nelse \'ERROR\'\r\nendif endif endif'"
	 * @generated
	 */
	String unsafeChainStepAsCode(Integer step);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fromStepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='unsafeChainAsCode(fromStep, length())'"
	 * @generated
	 */
	String unsafeChainAsCode(Integer fromStep);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model fromStepRequired="true" toStepRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='let end: Integer = if (length() > toStep) then toStep else length() endif in\r\n\r\nif fromStep=1 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2)).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then\r\n  \t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2))\r\n\telse if end=1 then unsafeChainStepAsCode(1) else \'\' endif\r\n\tendif endif\r\nelse if fromStep=2 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(2).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then unsafeChainStepAsCode(2) else \'\' endif\r\n\tendif\r\nelse if fromStep=3 then\r\n\tif end=3 then unsafeChainStepAsCode(3) else \'\' endif\r\n\telse \'ERROR\'\r\nendif endif endif\r\n'"
	 * @generated
	 */
	String unsafeChainAsCode(Integer fromStep, Integer toStep);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String asCodeForOthers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length1'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength1();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length2'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength2();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length3'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String codeForLength3();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='procAsCode'"
	 *        annotation="http://www.xocl.org/OCL body='if self.processor= mrules::expressions::MProcessor::IsNull then \'.oclIsUndefined()\' \r\nelse if self.processor = mrules::expressions::MProcessor::AllUpperCase then \'toUpperCase()\'\r\nelse if self.processor = mrules::expressions::MProcessor::IsInvalid then \'.oclIsInvalid()\'\r\nelse if self.processor = mrules::expressions::MProcessor::Container then \'eContainer()\'\r\nelse  self.processor.toString()\r\nendif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	String procAsCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Is CustomCode Processor'"
	 *        annotation="http://www.xocl.org/OCL body='if self.processorIsSet().oclIsUndefined() then null\r\nelse \r\nprocessor = mrules::expressions::MProcessor::Head or\r\nprocessor = mrules::expressions::MProcessor::Tail or\r\nprocessor = mrules::expressions::MProcessor::And or\r\nprocessor = mrules::expressions::MProcessor::Or  \r\nendif\r\n\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isCustomCodeProcessor();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if processor = mrules::expressions::MProcessor::None then false \r\nelse\r\nprocessor=mrules::expressions::MProcessor::AsOrderedSet or\r\nprocessor=mrules::expressions::MProcessor::First or\r\nprocessor=mrules::expressions::MProcessor::IsEmpty or\r\nprocessor=mrules::expressions::MProcessor::Last or\r\nprocessor=mrules::expressions::MProcessor::NotEmpty or\r\nprocessor=mrules::expressions::MProcessor::Size or\r\nprocessor=mrules::expressions::MProcessor::Sum or\r\nprocessor=mrules::expressions::MProcessor::Head or\r\nprocessor=mrules::expressions::MProcessor::Tail or\r\nprocessor=mrules::expressions::MProcessor::And or\r\nprocessor=mrules::expressions::MProcessor::Or\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isProcessorSetOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='IsOwnXOCLOperator'"
	 *        annotation="http://www.xocl.org/OCL body='processor =mrules::expressions::MProcessor::CamelCaseLower or\r\nprocessor =mrules::expressions::MProcessor::CamelCaseToBusiness or\r\nprocessor =mrules::expressions::MProcessor::CamelCaseUpper '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean isOwnXOCLOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.processor = mrules::expressions::MProcessor::None then null\r\nelse if\r\n self.processor = mrules::expressions::MProcessor::AsOrderedSet or\r\n processor = mrules::expressions::MProcessor::Head or\r\n processor= mrules::expressions::MProcessor::Tail\r\n\r\nthen \r\nfalse\r\nelse true\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean processorReturnsSingular();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='self.processor <> mrules::expressions::MProcessor::None'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor' createColumn='true'"
	 * @generated
	 */
	Boolean processorIsSet();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='Tuple{processor=processor}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	MProcessorDefinition createProcessorDefinition();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Object' mName='Processor Definition Choices For Singular Object'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::Container},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForObject();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Objects' mName='Processor Definition Choices For Objects'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size},\nTuple{processor=mrules::expressions::MProcessor::First},\nTuple{processor=mrules::expressions::MProcessor::Last},\nTuple{processor=mrules::expressions::MProcessor::Head},\nTuple{processor=mrules::expressions::MProcessor::Tail},\nTuple{processor=mrules::expressions::MProcessor::Container}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForObjects();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Boolean' mName='Processor Definition Choices For Boolean'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::IsFalse},\nTuple{processor=mrules::expressions::MProcessor::IsTrue},\nTuple{processor=mrules::expressions::MProcessor::Not},\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForBoolean();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Booleans' mName='Processor Definition Choices For Booleans'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::And},\nTuple{processor=mrules::expressions::MProcessor::Or},\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForBooleans();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Integer' mName='Processor Definition Choices For Integer'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::IsZero},\nTuple{processor=mrules::expressions::MProcessor::IsOne},\nTuple{processor=mrules::expressions::MProcessor::PlusOne},\nTuple{processor=mrules::expressions::MProcessor::MinusOne},\nTuple{processor=mrules::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mrules::expressions::MProcessor::Absolute},\nTuple{processor=mrules::expressions::MProcessor::OneDividedBy},\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForInteger();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Integers' mName='Processor Definition Choices For Integers'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Sum},\nTuple{processor=mrules::expressions::MProcessor::PlusOne},\nTuple{processor=mrules::expressions::MProcessor::MinusOne},\nTuple{processor=mrules::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mrules::expressions::MProcessor::Absolute},\nTuple{processor=mrules::expressions::MProcessor::OneDividedBy},\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size},\nTuple{processor=mrules::expressions::MProcessor::First},\nTuple{processor=mrules::expressions::MProcessor::Last},\nTuple{processor=mrules::expressions::MProcessor::Head},\nTuple{processor=mrules::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForIntegers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Real' mName='Processor Definition Choices For Real'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Round},\nTuple{processor=mrules::expressions::MProcessor::Floor},\nTuple{processor=mrules::expressions::MProcessor::IsZero},\nTuple{processor=mrules::expressions::MProcessor::IsOne},\nTuple{processor=mrules::expressions::MProcessor::PlusOne},\nTuple{processor=mrules::expressions::MProcessor::MinusOne},\nTuple{processor=mrules::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mrules::expressions::MProcessor::Absolute},\nTuple{processor=mrules::expressions::MProcessor::OneDividedBy},\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForReal();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Reals' mName='Processor Definition Choices For Reals'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Round},\nTuple{processor=mrules::expressions::MProcessor::Floor},\nTuple{processor=mrules::expressions::MProcessor::Sum},\nTuple{processor=mrules::expressions::MProcessor::PlusOne},\nTuple{processor=mrules::expressions::MProcessor::MinusOne},\nTuple{processor=mrules::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mrules::expressions::MProcessor::Absolute},\nTuple{processor=mrules::expressions::MProcessor::OneDividedBy},\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size},\nTuple{processor=mrules::expressions::MProcessor::First},\nTuple{processor=mrules::expressions::MProcessor::Last},\nTuple{processor=mrules::expressions::MProcessor::Head},\nTuple{processor=mrules::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForReals();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For String' mName='Processor Definition Choices For String'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Trim},\nTuple{processor=mrules::expressions::MProcessor::AllLowerCase},\nTuple{processor=mrules::expressions::MProcessor::AllUpperCase},\nTuple{processor=mrules::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mrules::expressions::MProcessor::ToBoolean},\nTuple{processor=mrules::expressions::MProcessor::ToInteger},\nTuple{processor=mrules::expressions::MProcessor::ToReal},\nTuple{processor=mrules::expressions::MProcessor::ToDate},\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Strings' mName='Processor Definition Choices For Strings'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Trim},\nTuple{processor=mrules::expressions::MProcessor::AllLowerCase},\nTuple{processor=mrules::expressions::MProcessor::AllUpperCase},\nTuple{processor=mrules::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mrules::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mrules::expressions::MProcessor::ToBoolean},\nTuple{processor=mrules::expressions::MProcessor::ToInteger},\nTuple{processor=mrules::expressions::MProcessor::ToReal},\nTuple{processor=mrules::expressions::MProcessor::ToDate},\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size},\nTuple{processor=mrules::expressions::MProcessor::First},\nTuple{processor=mrules::expressions::MProcessor::Last},\nTuple{processor=mrules::expressions::MProcessor::Head},\nTuple{processor=mrules::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForStrings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Date' mName='Processor Definition Choices For Date'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Year},\nTuple{processor=mrules::expressions::MProcessor::Month},\nTuple{processor=mrules::expressions::MProcessor::Day},\nTuple{processor=mrules::expressions::MProcessor::Hour},\nTuple{processor=mrules::expressions::MProcessor::Minute},\nTuple{processor=mrules::expressions::MProcessor::Second},\nTuple{processor=mrules::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mrules::expressions::MProcessor::ToHhMm},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::IsNull},\nTuple{processor=mrules::expressions::MProcessor::NotNull},\nTuple{processor=mrules::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mrules::expressions::MProcessor::IsInvalid}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForDate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mShortName='Proc Def Choices For Dates' mName='Processor Definition Choices For Dates'"
	 *        annotation="http://www.xocl.org/OCL body='OrderedSet{\nTuple{processor=mrules::expressions::MProcessor::Year},\nTuple{processor=mrules::expressions::MProcessor::Month},\nTuple{processor=mrules::expressions::MProcessor::Day},\nTuple{processor=mrules::expressions::MProcessor::Hour},\nTuple{processor=mrules::expressions::MProcessor::Minute},\nTuple{processor=mrules::expressions::MProcessor::Second},\nTuple{processor=mrules::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mrules::expressions::MProcessor::ToHhMm},\nTuple{processor=mrules::expressions::MProcessor::ToString},\nTuple{processor=mrules::expressions::MProcessor::IsEmpty},\nTuple{processor=mrules::expressions::MProcessor::NotEmpty},\nTuple{processor=mrules::expressions::MProcessor::Size},\nTuple{processor=mrules::expressions::MProcessor::First},\nTuple{processor=mrules::expressions::MProcessor::Last},\nTuple{processor=mrules::expressions::MProcessor::Head},\nTuple{processor=mrules::expressions::MProcessor::Tail}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Processor/Definition' createColumn='true'"
	 * @generated
	 */
	EList<MProcessorDefinition> procDefChoicesForDates();

} // MAbstractChain
