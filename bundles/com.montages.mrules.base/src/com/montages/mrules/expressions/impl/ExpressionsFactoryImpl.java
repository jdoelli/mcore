/**
 */
package com.montages.mrules.expressions.impl;

import com.montages.mrules.expressions.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsFactoryImpl extends EFactoryImpl implements ExpressionsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExpressionsFactory init() {
		try {
			ExpressionsFactory theExpressionsFactory = (ExpressionsFactory) EPackage.Registry.INSTANCE
					.getEFactory(ExpressionsPackage.eNS_URI);
			if (theExpressionsFactory != null) {
				return theExpressionsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExpressionsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE:
			return createMAbstractExpressionWithBase();
		case ExpressionsPackage.MCONTAINER_BASE_DEFINITION:
			return createMContainerBaseDefinition();
		case ExpressionsPackage.MBASE_DEFINITION:
			return createMBaseDefinition();
		case ExpressionsPackage.MNUMBER_BASE_DEFINITION:
			return createMNumberBaseDefinition();
		case ExpressionsPackage.MSELF_BASE_DEFINITION:
			return createMSelfBaseDefinition();
		case ExpressionsPackage.MTARGET_BASE_DEFINITION:
			return createMTargetBaseDefinition();
		case ExpressionsPackage.MSOURCE_BASE_DEFINITION:
			return createMSourceBaseDefinition();
		case ExpressionsPackage.MOBJECT_BASE_DEFINITION:
			return createMObjectBaseDefinition();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_BASE_DEFINITION:
			return createMSimpleTypeConstantBaseDefinition();
		case ExpressionsPackage.MLITERAL_CONSTANT_BASE_DEFINITION:
			return createMLiteralConstantBaseDefinition();
		case ExpressionsPackage.MOBJECT_REFERENCE_CONSTANT_BASE_DEFINITION:
			return createMObjectReferenceConstantBaseDefinition();
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION:
			return createMVariableBaseDefinition();
		case ExpressionsPackage.MITERATOR_BASE_DEFINITION:
			return createMIteratorBaseDefinition();
		case ExpressionsPackage.MPARAMETER_BASE_DEFINITION:
			return createMParameterBaseDefinition();
		case ExpressionsPackage.MACCUMULATOR_BASE_DEFINITION:
			return createMAccumulatorBaseDefinition();
		case ExpressionsPackage.MTUPLE:
			return createMTuple();
		case ExpressionsPackage.MTUPLE_ENTRY:
			return createMTupleEntry();
		case ExpressionsPackage.MNEW_OBJECT:
			return createMNewObject();
		case ExpressionsPackage.MNEW_OBJECT_FEATURE_VALUE:
			return createMNewObjectFeatureValue();
		case ExpressionsPackage.MNAMED_EXPRESSION:
			return createMNamedExpression();
		case ExpressionsPackage.MPROCESSOR_DEFINITION:
			return createMProcessorDefinition();
		case ExpressionsPackage.MCHAIN:
			return createMChain();
		case ExpressionsPackage.MCALL_ARGUMENT:
			return createMCallArgument();
		case ExpressionsPackage.MSUB_CHAIN:
			return createMSubChain();
		case ExpressionsPackage.MDATA_VALUE_EXPR:
			return createMDataValueExpr();
		case ExpressionsPackage.MLITERAL_VALUE_EXPR:
			return createMLiteralValueExpr();
		case ExpressionsPackage.MIF:
			return createMIf();
		case ExpressionsPackage.MTHEN:
			return createMThen();
		case ExpressionsPackage.MELSE_IF:
			return createMElseIf();
		case ExpressionsPackage.MELSE:
			return createMElse();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION:
			return createMCollectionExpression();
		case ExpressionsPackage.MITERATOR:
			return createMIterator();
		case ExpressionsPackage.MACCUMULATOR:
			return createMAccumulator();
		case ExpressionsPackage.MNAMED_CONSTANT:
			return createMNamedConstant();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET:
			return createMSimpleTypeConstantLet();
		case ExpressionsPackage.MLITERAL_LET:
			return createMLiteralLet();
		case ExpressionsPackage.MAPPLICATION:
			return createMApplication();
		case ExpressionsPackage.MOPERATOR_DEFINITION:
			return createMOperatorDefinition();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case ExpressionsPackage.EXPRESSION_BASE:
			return createExpressionBaseFromString(eDataType, initialValue);
		case ExpressionsPackage.MNAMED_EXPRESSION_ACTION:
			return createMNamedExpressionActionFromString(eDataType, initialValue);
		case ExpressionsPackage.MPROCESSOR:
			return createMProcessorFromString(eDataType, initialValue);
		case ExpressionsPackage.MCHAIN_ACTION:
			return createMChainActionFromString(eDataType, initialValue);
		case ExpressionsPackage.MSUB_CHAIN_ACTION:
			return createMSubChainActionFromString(eDataType, initialValue);
		case ExpressionsPackage.COLLECTION_OPERATOR:
			return createCollectionOperatorFromString(eDataType, initialValue);
		case ExpressionsPackage.MAPPLICATION_ACTION:
			return createMApplicationActionFromString(eDataType, initialValue);
		case ExpressionsPackage.MOPERATOR:
			return createMOperatorFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case ExpressionsPackage.EXPRESSION_BASE:
			return convertExpressionBaseToString(eDataType, instanceValue);
		case ExpressionsPackage.MNAMED_EXPRESSION_ACTION:
			return convertMNamedExpressionActionToString(eDataType, instanceValue);
		case ExpressionsPackage.MPROCESSOR:
			return convertMProcessorToString(eDataType, instanceValue);
		case ExpressionsPackage.MCHAIN_ACTION:
			return convertMChainActionToString(eDataType, instanceValue);
		case ExpressionsPackage.MSUB_CHAIN_ACTION:
			return convertMSubChainActionToString(eDataType, instanceValue);
		case ExpressionsPackage.COLLECTION_OPERATOR:
			return convertCollectionOperatorToString(eDataType, instanceValue);
		case ExpressionsPackage.MAPPLICATION_ACTION:
			return convertMApplicationActionToString(eDataType, instanceValue);
		case ExpressionsPackage.MOPERATOR:
			return convertMOperatorToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpressionWithBase createMAbstractExpressionWithBase() {
		MAbstractExpressionWithBaseImpl mAbstractExpressionWithBase = new MAbstractExpressionWithBaseImpl();
		return mAbstractExpressionWithBase;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MContainerBaseDefinition createMContainerBaseDefinition() {
		MContainerBaseDefinitionImpl mContainerBaseDefinition = new MContainerBaseDefinitionImpl();
		return mContainerBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MBaseDefinition createMBaseDefinition() {
		MBaseDefinitionImpl mBaseDefinition = new MBaseDefinitionImpl();
		return mBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNumberBaseDefinition createMNumberBaseDefinition() {
		MNumberBaseDefinitionImpl mNumberBaseDefinition = new MNumberBaseDefinitionImpl();
		return mNumberBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSelfBaseDefinition createMSelfBaseDefinition() {
		MSelfBaseDefinitionImpl mSelfBaseDefinition = new MSelfBaseDefinitionImpl();
		return mSelfBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MTargetBaseDefinition createMTargetBaseDefinition() {
		MTargetBaseDefinitionImpl mTargetBaseDefinition = new MTargetBaseDefinitionImpl();
		return mTargetBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSourceBaseDefinition createMSourceBaseDefinition() {
		MSourceBaseDefinitionImpl mSourceBaseDefinition = new MSourceBaseDefinitionImpl();
		return mSourceBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectBaseDefinition createMObjectBaseDefinition() {
		MObjectBaseDefinitionImpl mObjectBaseDefinition = new MObjectBaseDefinitionImpl();
		return mObjectBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSimpleTypeConstantBaseDefinition createMSimpleTypeConstantBaseDefinition() {
		MSimpleTypeConstantBaseDefinitionImpl mSimpleTypeConstantBaseDefinition = new MSimpleTypeConstantBaseDefinitionImpl();
		return mSimpleTypeConstantBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralConstantBaseDefinition createMLiteralConstantBaseDefinition() {
		MLiteralConstantBaseDefinitionImpl mLiteralConstantBaseDefinition = new MLiteralConstantBaseDefinitionImpl();
		return mLiteralConstantBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectReferenceConstantBaseDefinition createMObjectReferenceConstantBaseDefinition() {
		MObjectReferenceConstantBaseDefinitionImpl mObjectReferenceConstantBaseDefinition = new MObjectReferenceConstantBaseDefinitionImpl();
		return mObjectReferenceConstantBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariableBaseDefinition createMVariableBaseDefinition() {
		MVariableBaseDefinitionImpl mVariableBaseDefinition = new MVariableBaseDefinitionImpl();
		return mVariableBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MIteratorBaseDefinition createMIteratorBaseDefinition() {
		MIteratorBaseDefinitionImpl mIteratorBaseDefinition = new MIteratorBaseDefinitionImpl();
		return mIteratorBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MParameterBaseDefinition createMParameterBaseDefinition() {
		MParameterBaseDefinitionImpl mParameterBaseDefinition = new MParameterBaseDefinitionImpl();
		return mParameterBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulatorBaseDefinition createMAccumulatorBaseDefinition() {
		MAccumulatorBaseDefinitionImpl mAccumulatorBaseDefinition = new MAccumulatorBaseDefinitionImpl();
		return mAccumulatorBaseDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MTuple createMTuple() {
		MTupleImpl mTuple = new MTupleImpl();
		return mTuple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MTupleEntry createMTupleEntry() {
		MTupleEntryImpl mTupleEntry = new MTupleEntryImpl();
		return mTupleEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNewObject createMNewObject() {
		MNewObjectImpl mNewObject = new MNewObjectImpl();
		return mNewObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNewObjectFeatureValue createMNewObjectFeatureValue() {
		MNewObjectFeatureValueImpl mNewObjectFeatureValue = new MNewObjectFeatureValueImpl();
		return mNewObjectFeatureValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedExpression createMNamedExpression() {
		MNamedExpressionImpl mNamedExpression = new MNamedExpressionImpl();
		return mNamedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition createMProcessorDefinition() {
		MProcessorDefinitionImpl mProcessorDefinition = new MProcessorDefinitionImpl();
		return mProcessorDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain createMChain() {
		MChainImpl mChain = new MChainImpl();
		return mChain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCallArgument createMCallArgument() {
		MCallArgumentImpl mCallArgument = new MCallArgumentImpl();
		return mCallArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSubChain createMSubChain() {
		MSubChainImpl mSubChain = new MSubChainImpl();
		return mSubChain;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MDataValueExpr createMDataValueExpr() {
		MDataValueExprImpl mDataValueExpr = new MDataValueExprImpl();
		return mDataValueExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralValueExpr createMLiteralValueExpr() {
		MLiteralValueExprImpl mLiteralValueExpr = new MLiteralValueExprImpl();
		return mLiteralValueExpr;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MIf createMIf() {
		MIfImpl mIf = new MIfImpl();
		return mIf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MThen createMThen() {
		MThenImpl mThen = new MThenImpl();
		return mThen;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MElseIf createMElseIf() {
		MElseIfImpl mElseIf = new MElseIfImpl();
		return mElseIf;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MElse createMElse() {
		MElseImpl mElse = new MElseImpl();
		return mElse;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression createMCollectionExpression() {
		MCollectionExpressionImpl mCollectionExpression = new MCollectionExpressionImpl();
		return mCollectionExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MIterator createMIterator() {
		MIteratorImpl mIterator = new MIteratorImpl();
		return mIterator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulator createMAccumulator() {
		MAccumulatorImpl mAccumulator = new MAccumulatorImpl();
		return mAccumulator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedConstant createMNamedConstant() {
		MNamedConstantImpl mNamedConstant = new MNamedConstantImpl();
		return mNamedConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSimpleTypeConstantLet createMSimpleTypeConstantLet() {
		MSimpleTypeConstantLetImpl mSimpleTypeConstantLet = new MSimpleTypeConstantLetImpl();
		return mSimpleTypeConstantLet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralLet createMLiteralLet() {
		MLiteralLetImpl mLiteralLet = new MLiteralLetImpl();
		return mLiteralLet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MApplication createMApplication() {
		MApplicationImpl mApplication = new MApplicationImpl();
		return mApplication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperatorDefinition createMOperatorDefinition() {
		MOperatorDefinitionImpl mOperatorDefinition = new MOperatorDefinitionImpl();
		return mOperatorDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionBase createExpressionBaseFromString(EDataType eDataType, String initialValue) {
		ExpressionBase result = ExpressionBase.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertExpressionBaseToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedExpressionAction createMNamedExpressionActionFromString(EDataType eDataType, String initialValue) {
		MNamedExpressionAction result = MNamedExpressionAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMNamedExpressionActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessor createMProcessorFromString(EDataType eDataType, String initialValue) {
		MProcessor result = MProcessor.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMProcessorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainAction createMChainActionFromString(EDataType eDataType, String initialValue) {
		MChainAction result = MChainAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMChainActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSubChainAction createMSubChainActionFromString(EDataType eDataType, String initialValue) {
		MSubChainAction result = MSubChainAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMSubChainActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionOperator createCollectionOperatorFromString(EDataType eDataType, String initialValue) {
		CollectionOperator result = CollectionOperator.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCollectionOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MApplicationAction createMApplicationActionFromString(EDataType eDataType, String initialValue) {
		MApplicationAction result = MApplicationAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMApplicationActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperator createMOperatorFromString(EDataType eDataType, String initialValue) {
		MOperator result = MOperator.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMOperatorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsPackage getExpressionsPackage() {
		return (ExpressionsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExpressionsPackage getPackage() {
		return ExpressionsPackage.eINSTANCE;
	}

} //ExpressionsFactoryImpl
