/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AEnumeration;

import com.montages.acore.values.ALiteral;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MLiteralLet;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MLiteral Let</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralLetImpl#getAEnumerationType <em>AEnumeration Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralLetImpl#getAConstant1 <em>AConstant1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralLetImpl#getAConstant2 <em>AConstant2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MLiteralLetImpl#getAConstant3 <em>AConstant3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MLiteralLetImpl extends MConstantLetImpl implements MLiteralLet {
	/**
	 * The cached value of the '{@link #getAEnumerationType() <em>AEnumeration Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAEnumerationType()
	 * @generated
	 * @ordered
	 */
	protected AEnumeration aEnumerationType;

	/**
	 * This is true if the AEnumeration Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aEnumerationTypeESet;

	/**
	 * The cached value of the '{@link #getAConstant1() <em>AConstant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant1()
	 * @generated
	 * @ordered
	 */
	protected ALiteral aConstant1;

	/**
	 * This is true if the AConstant1 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aConstant1ESet;

	/**
	 * The cached value of the '{@link #getAConstant2() <em>AConstant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant2()
	 * @generated
	 * @ordered
	 */
	protected ALiteral aConstant2;

	/**
	 * This is true if the AConstant2 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aConstant2ESet;

	/**
	 * The cached value of the '{@link #getAConstant3() <em>AConstant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant3()
	 * @generated
	 * @ordered
	 */
	protected ALiteral aConstant3;

	/**
	 * This is true if the AConstant3 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aConstant3ESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #getAllowedLiterals <em>Get Allowed Literals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllowedLiterals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getAllowedLiteralsBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getAEnumerationType <em>AEnumeration Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAEnumerationType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aEnumerationTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAConstant1 <em>AConstant1</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant1
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aConstant1ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAConstant2 <em>AConstant2</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant2
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aConstant2ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAConstant3 <em>AConstant3</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant3
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aConstant3ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralLetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MLITERAL_LET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AEnumeration getAEnumerationType() {
		if (aEnumerationType != null && aEnumerationType.eIsProxy()) {
			InternalEObject oldAEnumerationType = (InternalEObject) aEnumerationType;
			aEnumerationType = (AEnumeration) eResolveProxy(oldAEnumerationType);
			if (aEnumerationType != oldAEnumerationType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE, oldAEnumerationType, aEnumerationType));
			}
		}
		return aEnumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AEnumeration basicGetAEnumerationType() {
		return aEnumerationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAEnumerationType(AEnumeration newAEnumerationType) {
		AEnumeration oldAEnumerationType = aEnumerationType;
		aEnumerationType = newAEnumerationType;
		boolean oldAEnumerationTypeESet = aEnumerationTypeESet;
		aEnumerationTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE,
					oldAEnumerationType, aEnumerationType, !oldAEnumerationTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAEnumerationType() {
		AEnumeration oldAEnumerationType = aEnumerationType;
		boolean oldAEnumerationTypeESet = aEnumerationTypeESet;
		aEnumerationType = null;
		aEnumerationTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE,
					oldAEnumerationType, null, oldAEnumerationTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAEnumerationType() {
		return aEnumerationTypeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>AEnumeration Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AEnumeration
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.aLiteral->size() > 0
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalAEnumerationTypeChoiceConstraint(AEnumeration trg) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_LET;
		if (aEnumerationTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MLITERAL_LET__AENUMERATION_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aEnumerationTypeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "AEnumerationTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aEnumerationTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass,
					"AEnumerationTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral getAConstant1() {
		if (aConstant1 != null && aConstant1.eIsProxy()) {
			InternalEObject oldAConstant1 = (InternalEObject) aConstant1;
			aConstant1 = (ALiteral) eResolveProxy(oldAConstant1);
			if (aConstant1 != oldAConstant1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_LET__ACONSTANT1, oldAConstant1, aConstant1));
			}
		}
		return aConstant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral basicGetAConstant1() {
		return aConstant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAConstant1(ALiteral newAConstant1) {
		ALiteral oldAConstant1 = aConstant1;
		aConstant1 = newAConstant1;
		boolean oldAConstant1ESet = aConstant1ESet;
		aConstant1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MLITERAL_LET__ACONSTANT1,
					oldAConstant1, aConstant1, !oldAConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant1() {
		ALiteral oldAConstant1 = aConstant1;
		boolean oldAConstant1ESet = aConstant1ESet;
		aConstant1 = null;
		aConstant1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MLITERAL_LET__ACONSTANT1,
					oldAConstant1, null, oldAConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant1() {
		return aConstant1ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AConstant1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<ALiteral>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL getAllowedLiterals()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<ALiteral> evalAConstant1ChoiceConstruction(List<ALiteral> choice) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_LET;
		if (aConstant1ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MLITERAL_LET__ACONSTANT1;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aConstant1ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AConstant1ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant1ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant1ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<ALiteral> result = new ArrayList<ALiteral>((Collection<ALiteral>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral getAConstant2() {
		if (aConstant2 != null && aConstant2.eIsProxy()) {
			InternalEObject oldAConstant2 = (InternalEObject) aConstant2;
			aConstant2 = (ALiteral) eResolveProxy(oldAConstant2);
			if (aConstant2 != oldAConstant2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_LET__ACONSTANT2, oldAConstant2, aConstant2));
			}
		}
		return aConstant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral basicGetAConstant2() {
		return aConstant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAConstant2(ALiteral newAConstant2) {
		ALiteral oldAConstant2 = aConstant2;
		aConstant2 = newAConstant2;
		boolean oldAConstant2ESet = aConstant2ESet;
		aConstant2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MLITERAL_LET__ACONSTANT2,
					oldAConstant2, aConstant2, !oldAConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant2() {
		ALiteral oldAConstant2 = aConstant2;
		boolean oldAConstant2ESet = aConstant2ESet;
		aConstant2 = null;
		aConstant2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MLITERAL_LET__ACONSTANT2,
					oldAConstant2, null, oldAConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant2() {
		return aConstant2ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AConstant2</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<ALiteral>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL getAllowedLiterals()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<ALiteral> evalAConstant2ChoiceConstruction(List<ALiteral> choice) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_LET;
		if (aConstant2ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MLITERAL_LET__ACONSTANT2;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aConstant2ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AConstant2ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant2ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant2ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<ALiteral> result = new ArrayList<ALiteral>((Collection<ALiteral>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral getAConstant3() {
		if (aConstant3 != null && aConstant3.eIsProxy()) {
			InternalEObject oldAConstant3 = (InternalEObject) aConstant3;
			aConstant3 = (ALiteral) eResolveProxy(oldAConstant3);
			if (aConstant3 != oldAConstant3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MLITERAL_LET__ACONSTANT3, oldAConstant3, aConstant3));
			}
		}
		return aConstant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ALiteral basicGetAConstant3() {
		return aConstant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAConstant3(ALiteral newAConstant3) {
		ALiteral oldAConstant3 = aConstant3;
		aConstant3 = newAConstant3;
		boolean oldAConstant3ESet = aConstant3ESet;
		aConstant3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MLITERAL_LET__ACONSTANT3,
					oldAConstant3, aConstant3, !oldAConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant3() {
		ALiteral oldAConstant3 = aConstant3;
		boolean oldAConstant3ESet = aConstant3ESet;
		aConstant3 = null;
		aConstant3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MLITERAL_LET__ACONSTANT3,
					oldAConstant3, null, oldAConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant3() {
		return aConstant3ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AConstant3</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<ALiteral>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL getAllowedLiterals()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<ALiteral> evalAConstant3ChoiceConstruction(List<ALiteral> choice) {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_LET;
		if (aConstant3ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MLITERAL_LET__ACONSTANT3;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aConstant3ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AConstant3ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant3ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant3ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<ALiteral> result = new ArrayList<ALiteral>((Collection<ALiteral>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ALiteral> getAllowedLiterals() {

		/**
		 * @OCL self.aEnumerationType.aLiteral
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EOperation eOperation = ExpressionsPackage.Literals.MLITERAL_LET.getEOperations().get(0);
		if (getAllowedLiteralsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getAllowedLiteralsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MLITERAL_LET, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getAllowedLiteralsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MLITERAL_LET,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<ALiteral>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE:
			if (resolve)
				return getAEnumerationType();
			return basicGetAEnumerationType();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT1:
			if (resolve)
				return getAConstant1();
			return basicGetAConstant1();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT2:
			if (resolve)
				return getAConstant2();
			return basicGetAConstant2();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT3:
			if (resolve)
				return getAConstant3();
			return basicGetAConstant3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE:
			setAEnumerationType((AEnumeration) newValue);
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT1:
			setAConstant1((ALiteral) newValue);
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT2:
			setAConstant2((ALiteral) newValue);
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT3:
			setAConstant3((ALiteral) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE:
			unsetAEnumerationType();
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT1:
			unsetAConstant1();
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT2:
			unsetAConstant2();
			return;
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT3:
			unsetAConstant3();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MLITERAL_LET__AENUMERATION_TYPE:
			return isSetAEnumerationType();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT1:
			return isSetAConstant1();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT2:
			return isSetAConstant2();
		case ExpressionsPackage.MLITERAL_LET__ACONSTANT3:
			return isSetAConstant3();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MLITERAL_LET___GET_ALLOWED_LITERALS:
			return getAllowedLiterals();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL asCode
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MLITERAL_LET;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Literal Constant'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode let a: String = if aConstant1.oclIsUndefined() then '' else aConstant1.aQualifiedName endif in
	let b: String = if aConstant2.oclIsUndefined() then '' else aConstant2.aQualifiedName endif in
	let c: String = if aConstant3.oclIsUndefined() then '' else aConstant3.aQualifiedName endif in
	
	if aSingular
	then a.concat(b).concat(c)
	else asSetString(a, b, c)
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular let a: Integer = if aConstant1.oclIsUndefined() then 0 else 1 endif in
	let b: Integer = if aConstant2.oclIsUndefined() then 0 else 1 endif in
	let c: Integer = if aConstant3.oclIsUndefined() then 0 else 1 endif in
	
	(a+b+c) <= 1
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType aEnumerationType
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MLITERAL_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MLiteralLetImpl
