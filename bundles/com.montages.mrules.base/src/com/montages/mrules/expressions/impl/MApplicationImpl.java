/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.AAbstractFolder;
import com.montages.acore.AFolder;
import com.montages.acore.APackage;
import com.montages.acore.AResource;
import com.montages.acore.AStructuringElement;
import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MApplication;
import com.montages.mrules.expressions.MApplicationAction;
import com.montages.mrules.expressions.MChain;
import com.montages.mrules.expressions.MChainOrApplication;
import com.montages.mrules.expressions.MCollectionExpression;
import com.montages.mrules.expressions.MOperator;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.resource.Resource.Internal;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.eclipse.ocl.util.TypeUtil;

import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.core.util.XoclMutlitypeComparisonUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MApplication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAName <em>AName</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getABusinessName <em>ABusiness Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAContainingFolder <em>AContaining Folder</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAUri <em>AUri</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAAllPackages <em>AAll Packages</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAPackage <em>APackage</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAResource <em>AResource</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getAFolder <em>AFolder</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getOperator <em>Operator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getOperands <em>Operands</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getContainedCollector <em>Contained Collector</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MApplicationImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MApplicationImpl extends MChainOrApplicationImpl implements MApplication, IXoclInitializable {
	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessName() <em>ABusiness Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUri() <em>AUri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri()
	 * @generated
	 * @ordered
	 */
	protected static final String AURI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected static final MOperator OPERATOR_EDEFAULT = MOperator.EQUAL;

	/**
	 * The cached value of the '{@link #getOperator() <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperator()
	 * @generated
	 * @ordered
	 */
	protected MOperator operator = OPERATOR_EDEFAULT;

	/**
	 * This is true if the Operator attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean operatorESet;

	/**
	 * The cached value of the '{@link #getOperands() <em>Operands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperands()
	 * @generated
	 * @ordered
	 */
	protected EList<MChainOrApplication> operands;

	/**
	 * The cached value of the '{@link #getContainedCollector() <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedCollector()
	 * @generated
	 * @ordered
	 */
	protected MCollectionExpression containedCollector;

	/**
	 * This is true if the Contained Collector containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean containedCollectorESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MApplicationAction DO_ACTION_EDEFAULT = MApplicationAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #operatorAsCode <em>Operator As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #operatorAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression operatorAsCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorPrefix <em>Is Operator Prefix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorPrefix
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorPrefixBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorInfix <em>Is Operator Infix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorInfix
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorInfixBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorPostfix <em>Is Operator Postfix</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorPostfix
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorPostfixBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorUnaryFunction <em>Is Operator Unary Function</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorUnaryFunction
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorUnaryFunctionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorSetOperator <em>Is Operator Set Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorSetOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorSetOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultValue <em>Default Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #defaultValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorUnaryFunctionTwoParam <em>Is Operator Unary Function Two Param</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorUnaryFunctionTwoParam
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorUnaryFunctionTwoParamBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOperatorParameterUnary <em>Is Operator Parameter Unary</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOperatorParameterUnary
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOperatorParameterUnaryBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #uniqueApplyNumber <em>Unique Apply Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #uniqueApplyNumber
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression uniqueApplyNumberBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #definesOperatorBusinessLogic <em>Defines Operator Business Logic</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #definesOperatorBusinessLogic
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression definesOperatorBusinessLogicBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLogicOperator <em>Code For Logic Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLogicOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLogicOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getCalculatedSimpleDifferentTypes <em>Get Calculated Simple Different Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleDifferentTypes
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getCalculatedSimpleDifferentTypesBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessName <em>ABusiness Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingFolder <em>AContaining Folder</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUri <em>AUri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllPackages <em>AAll Packages</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllPackages
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllPackagesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAPackage <em>APackage</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAResource <em>AResource</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aResourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFolder <em>AFolder</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MApplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MAPPLICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL '<A Name Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME;

		if (aBusinessNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBusinessNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAbstractFolder getAContainingFolder() {
		AAbstractFolder aContainingFolder = basicGetAContainingFolder();
		return aContainingFolder != null && aContainingFolder.eIsProxy()
				? (AAbstractFolder) eResolveProxy((InternalEObject) aContainingFolder) : aContainingFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAbstractFolder basicGetAContainingFolder() {
		/**
		 * @OCL let chain: ecore::EObject = eContainer() in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::AAbstractFolder)
		then chain.oclAsType(acore::AAbstractFolder)
		else null
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;

		if (aContainingFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAbstractFolder result = (AAbstractFolder) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUri() {
		/**
		 * @OCL let nl: String = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AURI;

		if (aUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<APackage> getAAllPackages() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AALL_PACKAGES;

		if (aAllPackagesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllPackagesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllPackagesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<APackage> result = (EList<APackage>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<APackage> getAPackage() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__APACKAGE;

		if (aPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<APackage> result = (EList<APackage>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AResource> getAResource() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__ARESOURCE;

		if (aResourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aResourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aResourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AResource> result = (EList<AResource>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFolder> getAFolder() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__AFOLDER;

		if (aFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFolder> result = (EList<AFolder>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperator getOperator() {
		return operator;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperator(MOperator newOperator) {
		MOperator oldOperator = operator;
		operator = newOperator == null ? OPERATOR_EDEFAULT : newOperator;
		boolean oldOperatorESet = operatorESet;
		operatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MAPPLICATION__OPERATOR,
					oldOperator, operator, !oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperator() {
		MOperator oldOperator = operator;
		boolean oldOperatorESet = operatorESet;
		operator = OPERATOR_EDEFAULT;
		operatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MAPPLICATION__OPERATOR,
					oldOperator, OPERATOR_EDEFAULT, oldOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperator() {
		return operatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MChainOrApplication> getOperands() {
		if (operands == null) {
			operands = new EObjectContainmentEList.Unsettable.Resolving<MChainOrApplication>(MChainOrApplication.class,
					this, ExpressionsPackage.MAPPLICATION__OPERANDS);
		}
		return operands;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperands() {
		if (operands != null)
			((InternalEList.Unsettable<?>) operands).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperands() {
		return operands != null && ((InternalEList.Unsettable<?>) operands).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getContainedCollector() {
		if (containedCollector != null && containedCollector.eIsProxy()) {
			InternalEObject oldContainedCollector = (InternalEObject) containedCollector;
			containedCollector = (MCollectionExpression) eResolveProxy(oldContainedCollector);
			if (containedCollector != oldContainedCollector) {
				InternalEObject newContainedCollector = (InternalEObject) containedCollector;
				NotificationChain msgs = oldContainedCollector.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, null);
				if (newContainedCollector.eInternalContainer() == null) {
					msgs = newContainedCollector.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, oldContainedCollector,
							containedCollector));
			}
		}
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetContainedCollector() {
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedCollector(MCollectionExpression newContainedCollector,
			NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = newContainedCollector;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, oldContainedCollector, newContainedCollector,
					!oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedCollector(MCollectionExpression newContainedCollector) {
		if (newContainedCollector != containedCollector) {
			NotificationChain msgs = null;
			if (containedCollector != null)
				msgs = ((InternalEObject) containedCollector).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, msgs);
			if (newContainedCollector != null)
				msgs = ((InternalEObject) newContainedCollector).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, msgs);
			msgs = basicSetContainedCollector(newContainedCollector, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, newContainedCollector,
						newContainedCollector, !oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetContainedCollector(NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = null;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, oldContainedCollector, null,
					oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainedCollector() {
		if (containedCollector != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) containedCollector).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, msgs);
			msgs = basicUnsetContainedCollector(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR, null, null, oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainedCollector() {
		return containedCollectorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MApplicationAction getDoAction() {
		/**
		 * @OCL mrules::expressions::MApplicationAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MAPPLICATION__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MApplicationAction result = (MApplicationAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String operatorAsCode() {

		/**
		 * @OCL if self.operator=MOperator::Equal then '=' 
		else if self.operator=MOperator::NotEqual then '<>'
		else if self.operator=MOperator::Greater then '>'
		else if self.operator=MOperator::Smaller then '<'
		else if self.operator=MOperator::GreaterEqual then '>='
		else if self.operator=MOperator::SmallerEqual then '<='
		else if self.operator=MOperator::NotOp then 'not'
		else if self.operator=MOperator::OclAnd then 'and'
		else if self.operator=MOperator::OclOr then 'or'
		else if self.operator=MOperator::XorOp then 'xor'
		else if self.operator=MOperator::ImpliesOp then 'implies'
		else if self.operator=MOperator::Plus then '+'
		else if self.operator=MOperator::Minus then '-'
		else if self .operator= MOperator::Times then '*'
		else if self .operator= MOperator::Divide then '/'
		else if self .operator= MOperator::Div then 'div'
		else if self .operator= MOperator::Mod then 'mod'
		else if self .operator= MOperator::Concat then 'concat'
		else if self .operator= MOperator::OclIsInvalid then 'oclIsInvalid'
		else if self .operator= MOperator::OclIsUndefined then 'oclIsUndefined'
		else if self .operator= MOperator::SetFirst then 'first'
		else if self .operator= MOperator::SetIsEmpty then 'isEmpty'
		else if self .operator= MOperator::SetLast then 'last'
		else if self .operator= MOperator::SetNotEmpty then 'notEmpty'
		else if self .operator= MOperator::SetSize then 'size'
		else if self .operator= MOperator::SetSum then 'sum'
		else if self .operator= MOperator::SetIncluding then 'including'
		
		else if self .operator= MOperator::SetIncludesAll then 'includesAll'
		else if self .operator= MOperator::SetExcludesAll then 'excludesAll'
		else if self .operator= MOperator::Intersection then 'intersection'
		else if (operator = MOperator::DiffInDays) then 'diffInDays'
		else if (operator = MOperator::DiffInHrs)   then 'diffInHours'
		else if  (operator = MOperator::DiffInMinutes) then 'diffInMinutes'
		else if  (operator = MOperator::DiffInMonth) then 'diffInMonths'
		else if   (operator = MOperator::DiffInSeconds) then 'diffInSeconds'
		else if   (operator = MOperator::DiffInYears) then 'diffInYears'
		
		else if self .operator= MOperator::SetExcluding then 'excluding'
		else if self .operator= MOperator::SetUnion then 'union'
		else if self .operator= MOperator::SetIncludes then 'includes'
		else if self .operator= MOperator::SetExcludes then 'excludes'
		else if self .operator= MOperator::IndexOf then 'indexOf'
		else if self .operator= MOperator::At then 'at'
		else if self.operator=MOperator::SetAsOrderedSet then 'asOrderedSet'
		else if self. operator=MOperator::ImpliesOp then 'implies' 
		else if self. operator=MOperator::ToString then 'toString'
		else if self. operator=MOperator::Trim then 'trim'
		else if self.operator= MOperator::SubString then 'substring'
		
		else 'ERROR'  endif endif endif endif endif endif endif endif endif
		endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(0);
		if (operatorAsCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				operatorAsCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(operatorAsCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorPrefix() {

		/**
		 * @OCL if operator.oclIsUndefined() then false 
		else (operator=MOperator::NotOp) endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(1);
		if (isOperatorPrefixBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorPrefixBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorPrefixBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorInfix() {

		/**
		 * @OCL (not isOperatorPrefix()) and (not isOperatorPostfix()) and (not isOperatorSetOperator())
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(2);
		if (isOperatorInfixBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorInfixBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorInfixBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorPostfix() {

		/**
		 * @OCL if operator.oclIsUndefined() then false else
		(operator = MOperator::OclIsInvalid) or (operator = MOperator::OclIsUndefined) 
		or (operator = MOperator::ToString) or (operator = MOperator::Trim) or isOperatorSetOperator() 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(3);
		if (isOperatorPostfixBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorPostfixBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorPostfixBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorUnaryFunction() {

		/**
		 * @OCL if operator.oclIsUndefined() then false else
		(operator = MOperator::Concat) or
		(operator = MOperator::DiffInDays) or
		(operator = MOperator::DiffInHrs) or
		(operator = MOperator::DiffInMinutes) or
		(operator = MOperator::DiffInMonth) or
		(operator = MOperator::DiffInSeconds) or
		(operator = MOperator::DiffInYears)
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(4);
		if (isOperatorUnaryFunctionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorUnaryFunctionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorUnaryFunctionBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorSetOperator() {

		/**
		 * @OCL if operator.oclIsUndefined() then false 
		else
		operator= MOperator::SetFirst or
		operator= MOperator::SetIsEmpty or
		operator= MOperator::SetLast or
		operator= MOperator::SetNotEmpty or
		operator= MOperator::SetSize or
		operator= MOperator::SetSum or
		operator= MOperator::SetIncluding or
		operator= MOperator::SetExcluding or
		operator= MOperator::SetUnion or
		operator= MOperator::SetIncludes or
		operator= MOperator::SetExcludes or
		operator= MOperator::IndexOf or
		operator= MOperator::At or 
		operator=MOperator::SetAsOrderedSet or
		operator=MOperator::SetExcludesAll or
		operator=MOperator::SetIncludesAll or
		operator=MOperator::Intersection 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(5);
		if (isOperatorSetOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorSetOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorSetOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain defaultValue() {

		/**
		 * @OCL Tuple{base=ExpressionBase::SelfObject}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(6);
		if (defaultValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				defaultValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MChain) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorUnaryFunctionTwoParam() {

		/**
		 * @OCL if operator.oclIsUndefined() then false else
		(operator = MOperator::SubString)  endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(7);
		if (isOperatorUnaryFunctionTwoParamBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorUnaryFunctionTwoParamBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorUnaryFunctionTwoParamBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOperatorParameterUnary() {

		/**
		 * @OCL if operator.oclIsUndefined() then false else
		(operator = MOperator::SetExcluding)  or 
		(operator = MOperator::SetIncluding) or  
		(operator = MOperator::At) or
		(operator = MOperator::IndexOf) or
		(operator = MOperator::SubString) or
		(operator = MOperator::SetExcludes) or
		(operator = MOperator::SetIncludes)
		
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(8);
		if (isOperatorParameterUnaryBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOperatorParameterUnaryBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOperatorParameterUnaryBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer uniqueApplyNumber() {

		/**
		 * @OCL let chain : Integer =
		if self.eContainer().oclAsType(MNamedExpression).oclIsUndefined() then 
		self.eContainer()->closure(eContainer())->select(oclIsTypeOf(MNamedExpression)).oclAsType(MNamedExpression)->asOrderedSet()->first().allApplications()->indexOf(self)
		else
		self.eContainer().oclAsType(MNamedExpression).allApplications()->indexOf(self)
		endif
		in if chain.oclIsUndefined() then 0 else chain endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(9);
		if (uniqueApplyNumberBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				uniqueApplyNumberBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(uniqueApplyNumberBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean definesOperatorBusinessLogic() {

		/**
		 * @OCL if operator.oclIsUndefined() then false else
		(operator = MOperator::AndOp) or
		(operator = MOperator::OrOp) or
		(operator = MOperator::BusinessNOT) or
		(operator = MOperator::BusinessImplies)
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(10);
		if (definesOperatorBusinessLogicBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				definesOperatorBusinessLogicBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(definesOperatorBusinessLogicBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLogicOperator() {

		/**
		 * @OCL if operands->first().oclIsUndefined() then ' '
		
		else
		
		let frst: MChainOrApplication =  operands->first() in
		let frstCode: String = (if frst.isComplexExpression then '(' else '' endif).concat(frst.asCode).concat(if frst.isComplexExpression then ')' else '' endif) in
		let businessExpr : String = '' in
		if operator= MOperator::AndOp or operator = MOperator::OrOp
		then
		
		let checkPart : String = if  operator= MOperator::AndOp then 'false' else 'true' endif
		in
		let elsePart : String = if operator = MOperator::AndOp then 'true' else 'false' endif in
		businessExpr.concat('if (').concat(frstCode).concat(')= ').concat(checkPart).concat(' \n then ').concat(checkPart).concat(' \n').concat(operands->excluding(frst)->iterate(x: MChainOrApplication; s:String = '' | s.concat(' else if (').concat(x.asCode).concat(')= ' ).concat(checkPart).concat(' \n').concat(' then ').concat(checkPart).concat(' \n'))).concat('else if (')
		.concat(operands->iterate(x: MChainOrApplication; s:String = '' | s.concat('(').concat(x.asCode).concat(')= null').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined()  then '' else ' or ' endif))).concat(') = true \n then null \n else ').concat(elsePart).concat(' ')
		.concat(operands->iterate(x: MChainOrApplication; s:String = '' | s.concat('endif '))).concat('endif')
		
		else if operator = MOperator::BusinessNOT	
		then
		
		businessExpr.concat('if (').concat(frst.asCode).concat(')= true \n then false \n else if (').concat(frst.asCode).concat(')= false \n then true \n else null endif endif \n ')
		
		else 
		''
		
		endif
		endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(11);
		if (codeForLogicOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				codeForLogicOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLogicOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getCalculatedSimpleDifferentTypes() {

		/**
		 * @OCL 	let x: OrderedSet(acore::classifiers::ASimpleType) = self.operands->collect(aSimpleType)->asOrderedSet() in 
		
		if x->oclIsUndefined() then acore::classifiers::ASimpleType::None 
		else if x->includesAll(OrderedSet{acore::classifiers::ASimpleType::Integer,acore::classifiers::ASimpleType::Real}) then acore::classifiers::ASimpleType::Real
		--else if x->size() > 1 then SimpleType::None
		else
		x->first() endif endif--endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EOperation eOperation = ExpressionsPackage.Literals.MAPPLICATION.getEOperations().get(12);
		if (getCalculatedSimpleDifferentTypesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getCalculatedSimpleDifferentTypesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MAPPLICATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getCalculatedSimpleDifferentTypesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
			return ((InternalEList<?>) getOperands()).basicRemove(otherEnd, msgs);
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			return basicUnsetContainedCollector(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MAPPLICATION__ANAME:
			return getAName();
		case ExpressionsPackage.MAPPLICATION__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case ExpressionsPackage.MAPPLICATION__ABUSINESS_NAME:
			return getABusinessName();
		case ExpressionsPackage.MAPPLICATION__ACONTAINING_FOLDER:
			if (resolve)
				return getAContainingFolder();
			return basicGetAContainingFolder();
		case ExpressionsPackage.MAPPLICATION__AURI:
			return getAUri();
		case ExpressionsPackage.MAPPLICATION__AALL_PACKAGES:
			return getAAllPackages();
		case ExpressionsPackage.MAPPLICATION__APACKAGE:
			return getAPackage();
		case ExpressionsPackage.MAPPLICATION__ARESOURCE:
			return getAResource();
		case ExpressionsPackage.MAPPLICATION__AFOLDER:
			return getAFolder();
		case ExpressionsPackage.MAPPLICATION__OPERATOR:
			return getOperator();
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
			return getOperands();
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			if (resolve)
				return getContainedCollector();
			return basicGetContainedCollector();
		case ExpressionsPackage.MAPPLICATION__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MAPPLICATION__OPERATOR:
			setOperator((MOperator) newValue);
			return;
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
			getOperands().clear();
			getOperands().addAll((Collection<? extends MChainOrApplication>) newValue);
			return;
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			setContainedCollector((MCollectionExpression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MAPPLICATION__OPERATOR:
			unsetOperator();
			return;
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
			unsetOperands();
			return;
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			unsetContainedCollector();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MAPPLICATION__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null : !ANAME_EDEFAULT.equals(getAName());
		case ExpressionsPackage.MAPPLICATION__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null ? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT.equals(getAUndefinedNameConstant());
		case ExpressionsPackage.MAPPLICATION__ABUSINESS_NAME:
			return ABUSINESS_NAME_EDEFAULT == null ? getABusinessName() != null
					: !ABUSINESS_NAME_EDEFAULT.equals(getABusinessName());
		case ExpressionsPackage.MAPPLICATION__ACONTAINING_FOLDER:
			return basicGetAContainingFolder() != null;
		case ExpressionsPackage.MAPPLICATION__AURI:
			return AURI_EDEFAULT == null ? getAUri() != null : !AURI_EDEFAULT.equals(getAUri());
		case ExpressionsPackage.MAPPLICATION__AALL_PACKAGES:
			return !getAAllPackages().isEmpty();
		case ExpressionsPackage.MAPPLICATION__APACKAGE:
			return !getAPackage().isEmpty();
		case ExpressionsPackage.MAPPLICATION__ARESOURCE:
			return !getAResource().isEmpty();
		case ExpressionsPackage.MAPPLICATION__AFOLDER:
			return !getAFolder().isEmpty();
		case ExpressionsPackage.MAPPLICATION__OPERATOR:
			return isSetOperator();
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
			return isSetOperands();
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			return isSetContainedCollector();
		case ExpressionsPackage.MAPPLICATION__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MAPPLICATION__ANAME:
				return AbstractionsPackage.ANAMED__ANAME;
			case ExpressionsPackage.MAPPLICATION__AUNDEFINED_NAME_CONSTANT:
				return AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;
			case ExpressionsPackage.MAPPLICATION__ABUSINESS_NAME:
				return AbstractionsPackage.ANAMED__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AStructuringElement.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MAPPLICATION__ACONTAINING_FOLDER:
				return AcorePackage.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;
			case ExpressionsPackage.MAPPLICATION__AURI:
				return AcorePackage.ASTRUCTURING_ELEMENT__AURI;
			case ExpressionsPackage.MAPPLICATION__AALL_PACKAGES:
				return AcorePackage.ASTRUCTURING_ELEMENT__AALL_PACKAGES;
			default:
				return -1;
			}
		}
		if (baseClass == AAbstractFolder.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MAPPLICATION__APACKAGE:
				return AcorePackage.AABSTRACT_FOLDER__APACKAGE;
			case ExpressionsPackage.MAPPLICATION__ARESOURCE:
				return AcorePackage.AABSTRACT_FOLDER__ARESOURCE;
			case ExpressionsPackage.MAPPLICATION__AFOLDER:
				return AcorePackage.AABSTRACT_FOLDER__AFOLDER;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ANamed.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ANAMED__ANAME:
				return ExpressionsPackage.MAPPLICATION__ANAME;
			case AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
				return ExpressionsPackage.MAPPLICATION__AUNDEFINED_NAME_CONSTANT;
			case AbstractionsPackage.ANAMED__ABUSINESS_NAME:
				return ExpressionsPackage.MAPPLICATION__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AStructuringElement.class) {
			switch (baseFeatureID) {
			case AcorePackage.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER:
				return ExpressionsPackage.MAPPLICATION__ACONTAINING_FOLDER;
			case AcorePackage.ASTRUCTURING_ELEMENT__AURI:
				return ExpressionsPackage.MAPPLICATION__AURI;
			case AcorePackage.ASTRUCTURING_ELEMENT__AALL_PACKAGES:
				return ExpressionsPackage.MAPPLICATION__AALL_PACKAGES;
			default:
				return -1;
			}
		}
		if (baseClass == AAbstractFolder.class) {
			switch (baseFeatureID) {
			case AcorePackage.AABSTRACT_FOLDER__APACKAGE:
				return ExpressionsPackage.MAPPLICATION__APACKAGE;
			case AcorePackage.AABSTRACT_FOLDER__ARESOURCE:
				return ExpressionsPackage.MAPPLICATION__ARESOURCE;
			case AcorePackage.AABSTRACT_FOLDER__AFOLDER:
				return ExpressionsPackage.MAPPLICATION__AFOLDER;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MAPPLICATION___OPERATOR_AS_CODE:
			return operatorAsCode();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_PREFIX:
			return isOperatorPrefix();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_INFIX:
			return isOperatorInfix();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_POSTFIX:
			return isOperatorPostfix();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_UNARY_FUNCTION:
			return isOperatorUnaryFunction();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_SET_OPERATOR:
			return isOperatorSetOperator();
		case ExpressionsPackage.MAPPLICATION___DEFAULT_VALUE:
			return defaultValue();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_UNARY_FUNCTION_TWO_PARAM:
			return isOperatorUnaryFunctionTwoParam();
		case ExpressionsPackage.MAPPLICATION___IS_OPERATOR_PARAMETER_UNARY:
			return isOperatorParameterUnary();
		case ExpressionsPackage.MAPPLICATION___UNIQUE_APPLY_NUMBER:
			return uniqueApplyNumber();
		case ExpressionsPackage.MAPPLICATION___DEFINES_OPERATOR_BUSINESS_LOGIC:
			return definesOperatorBusinessLogic();
		case ExpressionsPackage.MAPPLICATION___CODE_FOR_LOGIC_OPERATOR:
			return codeForLogicOperator();
		case ExpressionsPackage.MAPPLICATION___GET_CALCULATED_SIMPLE_DIFFERENT_TYPES:
			return getCalculatedSimpleDifferentTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (operator: ");
		if (operatorESet)
			result.append(operator);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if operands->forAll(oclIsKindOf(MChain))
	then /* the operand applies only to chains, we show the entire text *\/
	asCode
	else /* we show only the operator *\/
	operatorAsCode()
	endif
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MAPPLICATION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel let kind: String = 'APPLY' in
	kind
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType if
	operator=MOperator::SetFirst  or operator=MOperator::SetLast
	or operator=MOperator::SetIncluding or operator=MOperator::SetExcluding
	or operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet	
	or operator=MOperator::At  or operator=MOperator::Intersection then 
	let x: acore::classifiers::AClassifier = self.operands->first().aClassifier in
	if x.oclIsUndefined() then null else x endif
	else null
	endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType if
	operator=MOperator::Concat or operator=MOperator::ToString or operator=MOperator::Trim or operator=MOperator::SubString
	then acore::classifiers::ASimpleType::String
	else if 
	operator=MOperator::Equal  or operator=MOperator::NotEqual  or operator=MOperator::Greater
	or operator=MOperator::Smaller  or operator=MOperator::GreaterEqual or operator=MOperator::SmallerEqual
	or operator=MOperator::NotOp  or operator=MOperator::OclAnd  or operator=MOperator::OclOr
	or operator=MOperator::XorOp or operator=MOperator::ImpliesOp or operator=MOperator::SetIsEmpty 
	or operator=MOperator::SetNotEmpty  or operator=MOperator::SetIncludes or operator=MOperator::SetExcludes
	or operator=MOperator::ImpliesOp or self.definesOperatorBusinessLogic() or operator=MOperator::SetIncludesAll or operator=MOperator::SetExcludesAll
	
	then acore::classifiers::ASimpleType::Boolean
	else if 
	operator=MOperator::SetSize or operator=MOperator::IndexOf or 
	(operator = MOperator::DiffInDays) or
	(operator = MOperator::DiffInHrs) or
	(operator = MOperator::DiffInMinutes) or
	(operator = MOperator::DiffInMonth) or
	(operator = MOperator::DiffInSeconds) or
	 (operator = MOperator::DiffInYears)
	then acore::classifiers::ASimpleType::Integer
	else if 
	self.operator=MOperator::Plus  or operator=MOperator::Minus or operator= MOperator::Times 
	or operator= MOperator::Divide  or operator= MOperator::Div or operator= MOperator::Mod
	or operator=MOperator::SetFirst  or operator=MOperator::SetLast or operator=MOperator::SetSum
	or operator=MOperator::SetIncluding or operator=MOperator::SetExcluding
	or operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet or operator=MOperator::At  or operator=MOperator::Intersection
	then 
	self.getCalculatedSimpleDifferentTypes()
	--	let x: SimpleType = self.operands->first().calculatedSimpleType in 
	--	if x.oclIsUndefined() then SimpleType::None else x endif
	else if 
	(operator= MOperator::OclIsInvalid) or (operator= MOperator::OclIsUndefined) then acore::classifiers::ASimpleType::Boolean
	else acore::classifiers::ASimpleType::None endif endif endif endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if
	operator=MOperator::SetIncluding or operator=MOperator::SetExcluding
	or operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet	
	or operator=MOperator::Intersection
	then false
	else true
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isComplexExpression isOperatorInfix()
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsComplexExpression() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode let varName : String = 'e'.concat(self.uniqueApplyNumber().toString()) in
	
	'let '.concat(varName).concat(': ').concat( aTypeAsOcl(aSelfObjectPackage, aClassifier, aSimpleType, aSingular) )
	.concat(' = ').concat(
	if operands->size() = 0 then '' else
	let frst: MChainOrApplication = operands->first() in
	let frstCode: String = (if frst.isComplexExpression then '(' else '' endif).concat(frst.asCode).concat(if frst.isComplexExpression then ')' else '' endif) in
	
	
	if self.definesOperatorBusinessLogic() then self.codeForLogicOperator()
	else
	if isOperatorUnaryFunction()
	then
	frstCode.concat(
		operands->excluding(frst)->iterate(
		  x: MChainOrApplication; s: String = '' | 
		  s.concat('.').concat(operatorAsCode()).concat('(').concat(x.asCode).concat(')')
		 ) )
	
	else if isOperatorSetOperator()
	then
	let appCode : String = if self.operands->size() = 1 then frstCode.concat('->').concat(operatorAsCode()).concat('()') else 
	frstCode.concat(
		operands->excluding(frst)->iterate(
	x: MChainOrApplication; s: String = '' | 
	 s.concat('->').concat(operatorAsCode())
	.concat('(').concat(x.asCode).concat(')')
	) ).concat(if self.aSingular then '  ' else ' ->asOrderedSet()  '  endif)
	endif in appCode
	
	else if self.isOperatorUnaryFunctionTwoParam()
	then
	frstCode.concat('.').concat(operatorAsCode()).concat('(').concat(
		operands->excluding(frst)->iterate(
		  x: MChainOrApplication; s: String = '' | 
		  s.concat(x.asCode).concat( if (operands->indexOf(x) = operands->size()) then '' else ', ' endif)
		--  s.concat(x.asCode).concat(',').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined() then '1' else operands->at(operands->indexOf(x)+1).asCode endif)
		 ) ).concat(')')
	else if isOperatorInfix() 
	then
	frstCode.concat(
		operands->excluding(frst)->iterate(
		  x: MChainOrApplication; s: String = '' | 
		  s.concat(' ').concat(operatorAsCode()).concat(' ').concat(
		    (if x.isComplexExpression then '(' else '' endif).concat(x.asCode).concat(if x.isComplexExpression then ')' else '' endif)
		  )
		) 
	)
	else if isOperatorPrefix() 
	then
	operatorAsCode().concat('(').concat(frstCode).concat(
		operands->excluding(frst)->iterate(
		  x: MChainOrApplication; s: String = '' | 
		  s.concat(', ').concat(
		    (if x.isComplexExpression then '(' else '' endif).concat(x.asCode).concat(if x.isComplexExpression then ')' else '' endif)
		  )
		) 
	).concat(')')
	else
	frstCode.concat('.').concat(operatorAsCode()).concat('()')
	endif
	endif
	endif
	endif
	endif
	endif
	endif
	).concat(if self.aSingular then ' in \n if '.concat(varName).concat('.oclIsInvalid() ').concat('then ') else ' in \n    if '.concat(varName).concat('->oclIsInvalid() then ') endif)
	.concat(if self.aSingular then 'null' else 'OrderedSet{}' endif)
	.concat(' else ').concat(varName).concat(' endif')
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL collector containedCollector
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MCollectionExpression basicGetCollector() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MCollectionExpression) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aName
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (ExpressionsPackage.Literals.MAPPLICATION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override

	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MAPPLICATION__CONTAINED_COLLECTOR };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MAPPLICATION,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MApplicationImpl
