/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractChain;
import com.montages.mrules.expressions.MAbstractExpression;
import com.montages.mrules.expressions.MProcessor;
import com.montages.mrules.expressions.MProcessorDefinition;
import com.montages.mrules.expressions.MSubChain;
import com.montages.mrules.expressions.MSubChainAction;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSub Chain</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAChainEntryType <em>AChain Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getChainAsCode <em>Chain As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAElement1 <em>AElement1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getElement1Correct <em>Element1 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAElement2EntryType <em>AElement2 Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAElement2 <em>AElement2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getElement2Correct <em>Element2 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAElement3EntryType <em>AElement3 Entry Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAElement3 <em>AElement3</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getElement3Correct <em>Element3 Correct</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getACastType <em>ACast Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getALastElement <em>ALast Element</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAChainCalculatedType <em>AChain Calculated Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getAChainCalculatedSimpleType <em>AChain Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getChainCalculatedSingular <em>Chain Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getProcessor <em>Processor</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getProcessorDefinition <em>Processor Definition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getPreviousExpression <em>Previous Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getNextExpression <em>Next Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MSubChainImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MSubChainImpl extends MAbstractExpressionImpl implements MSubChain {
	/**
	 * The default value of the '{@link #getChainAsCode() <em>Chain As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAIN_AS_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAElement1() <em>AElement1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement1()
	 * @generated
	 * @ordered
	 */
	protected AProperty aElement1;

	/**
	 * This is true if the AElement1 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aElement1ESet;

	/**
	 * The default value of the '{@link #getElement1Correct() <em>Element1 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT1_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAElement2() <em>AElement2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement2()
	 * @generated
	 * @ordered
	 */
	protected AProperty aElement2;

	/**
	 * This is true if the AElement2 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aElement2ESet;

	/**
	 * The default value of the '{@link #getElement2Correct() <em>Element2 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT2_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAElement3() <em>AElement3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement3()
	 * @generated
	 * @ordered
	 */
	protected AProperty aElement3;

	/**
	 * This is true if the AElement3 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aElement3ESet;

	/**
	 * The default value of the '{@link #getElement3Correct() <em>Element3 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT3_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getACastType() <em>ACast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACastType()
	 * @generated
	 * @ordered
	 */
	protected AClassifier aCastType;

	/**
	 * This is true if the ACast Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aCastTypeESet;

	/**
	 * The default value of the '{@link #getAChainCalculatedSimpleType() <em>AChain Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ACHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getChainCalculatedSingular() <em>Chain Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CHAIN_CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected static final MProcessor PROCESSOR_EDEFAULT = MProcessor.NONE;

	/**
	 * The cached value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected MProcessor processor = PROCESSOR_EDEFAULT;

	/**
	 * This is true if the Processor attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean processorESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MSubChainAction DO_ACTION_EDEFAULT = MSubChainAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #isLastSubchain <em>Is Last Subchain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isLastSubchain
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isLastSubchainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #uniqueSubchainName <em>Unique Subchain Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #uniqueSubchainName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression uniqueSubchainNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #returnSingularPureChain <em>Return Singular Pure Chain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #returnSingularPureChain
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression returnSingularPureChainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForOthers <em>As Code For Others</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForOthers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForOthersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength1 <em>Code For Length1</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength1
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength1BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength2 <em>Code For Length2</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength2
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength2BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength3 <em>Code For Length3</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength3
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength3BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #length <em>Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #length
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression lengthBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeElementAsCode <em>Unsafe Element As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeElementAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeElementAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainStepAsCode <em>Unsafe Chain Step As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainStepAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procAsCode <em>Proc As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procAsCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isCustomCodeProcessor <em>Is Custom Code Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCustomCodeProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isCustomCodeProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isProcessorSetOperator <em>Is Processor Set Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProcessorSetOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isProcessorSetOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOwnXOCLOperator <em>Is Own XOCL Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOwnXOCLOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorReturnsSingular <em>Processor Returns Singular</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorReturnsSingular
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorReturnsSingularBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorIsSet <em>Processor Is Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorIsSet
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorIsSetBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #createProcessorDefinition <em>Create Processor Definition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #createProcessorDefinition
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression createProcessorDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObject <em>Proc Def Choices For Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObjects <em>Proc Def Choices For Objects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObjects
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBoolean <em>Proc Def Choices For Boolean</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBoolean
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleanBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBooleans <em>Proc Def Choices For Booleans</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBooleans
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleansBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForInteger <em>Proc Def Choices For Integer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForInteger
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegerBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForIntegers <em>Proc Def Choices For Integers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForIntegers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReal <em>Proc Def Choices For Real</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReal
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReals <em>Proc Def Choices For Reals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForString <em>Proc Def Choices For String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForStrings <em>Proc Def Choices For Strings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForStrings
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDate <em>Proc Def Choices For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDates <em>Proc Def Choices For Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDates
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDatesBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainAsCode <em>Chain As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAElement1 <em>AElement1</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement1
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aElement1ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement1Correct <em>Element1 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element1CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAElement2EntryType <em>AElement2 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement2EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aElement2EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAElement2 <em>AElement2</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement2
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aElement2ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement2Correct <em>Element2 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element2CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAElement3EntryType <em>AElement3 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement3EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aElement3EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getAElement3 <em>AElement3</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAElement3
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression aElement3ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement3Correct <em>Element3 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element3CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getACastType <em>ACast Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACastType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aCastTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALastElement <em>ALast Element</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALastElement
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLastElementDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression processorDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getPreviousExpression <em>Previous Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreviousExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression previousExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNextExpression <em>Next Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNextExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression nextExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChainEntryType <em>AChain Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainEntryType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aChainEntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSingular <em>Chain Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCalculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChainCalculatedType <em>AChain Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aChainCalculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAChainCalculatedSimpleType <em>AChain Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAChainCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aChainCalculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSubChainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MSUB_CHAIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAChainEntryType() {
		AClassifier aChainEntryType = basicGetAChainEntryType();
		return aChainEntryType != null && aChainEntryType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aChainEntryType) : aChainEntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAChainEntryType() {
		/**
		 * @OCL let c:acore::classifiers::AClassifier=null in c
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_ENTRY_TYPE;

		if (aChainEntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aChainEntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChainEntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChainAsCode() {
		/**
		 * @OCL unsafeChainAsCode(1)
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE;

		if (chainAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				chainAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty getAElement1() {
		if (aElement1 != null && aElement1.eIsProxy()) {
			InternalEObject oldAElement1 = (InternalEObject) aElement1;
			aElement1 = (AProperty) eResolveProxy(oldAElement1);
			if (aElement1 != oldAElement1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MSUB_CHAIN__AELEMENT1,
							oldAElement1, aElement1));
			}
		}
		return aElement1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty basicGetAElement1() {
		return aElement1;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAElement1(AProperty newAElement1) {
		AProperty oldAElement1 = aElement1;
		aElement1 = newAElement1;
		boolean oldAElement1ESet = aElement1ESet;
		aElement1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MSUB_CHAIN__AELEMENT1,
					oldAElement1, aElement1, !oldAElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAElement1() {
		AProperty oldAElement1 = aElement1;
		boolean oldAElement1ESet = aElement1ESet;
		aElement1 = null;
		aElement1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MSUB_CHAIN__AELEMENT1,
					oldAElement1, null, oldAElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAElement1() {
		return aElement1ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AElement1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 
	let annotatedProp: acore::classifiers::AProperty = 
	self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
	in
	if self.aChainEntryType.oclIsUndefined() then 
	OrderedSet{} else
	self.aChainEntryType.aAllProperty()
	endif
	
	--
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AProperty> evalAElement1ChoiceConstruction(List<AProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		if (aElement1ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT1;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aElement1ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AElement1ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aElement1ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AElement1ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<AProperty> result = new ArrayList<AProperty>((Collection<AProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement1Correct() {
		/**
		 * @OCL if aElement1.oclIsUndefined() then true else
		--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else
		if aChainEntryType.oclIsUndefined() then false 
		else aChainEntryType.aAllProperty()->includes(aElement1)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT;

		if (element1CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				element1CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element1CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAElement2EntryType() {
		AClassifier aElement2EntryType = basicGetAElement2EntryType();
		return aElement2EntryType != null && aElement2EntryType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aElement2EntryType) : aElement2EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAElement2EntryType() {
		/**
		 * @OCL if not self.element1Correct then null
		else 
		if self.aElement1.oclIsUndefined() then null 
		else self.aElement1.aClassifier endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT2_ENTRY_TYPE;

		if (aElement2EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aElement2EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aElement2EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty getAElement2() {
		if (aElement2 != null && aElement2.eIsProxy()) {
			InternalEObject oldAElement2 = (InternalEObject) aElement2;
			aElement2 = (AProperty) eResolveProxy(oldAElement2);
			if (aElement2 != oldAElement2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MSUB_CHAIN__AELEMENT2,
							oldAElement2, aElement2));
			}
		}
		return aElement2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty basicGetAElement2() {
		return aElement2;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAElement2(AProperty newAElement2) {
		AProperty oldAElement2 = aElement2;
		aElement2 = newAElement2;
		boolean oldAElement2ESet = aElement2ESet;
		aElement2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MSUB_CHAIN__AELEMENT2,
					oldAElement2, aElement2, !oldAElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAElement2() {
		AProperty oldAElement2 = aElement2;
		boolean oldAElement2ESet = aElement2ESet;
		aElement2 = null;
		aElement2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MSUB_CHAIN__AELEMENT2,
					oldAElement2, null, oldAElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAElement2() {
		return aElement2ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AElement2</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: acore::classifiers::AProperty = 
	self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty)
	in
	
	if aElement1.oclIsUndefined() 
	then OrderedSet{}
	else if not(aElement1.aOperation->isEmpty())
	then OrderedSet{} 
	else if aElement2EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else aElement2EntryType.aAllProperty() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AProperty> evalAElement2ChoiceConstruction(List<AProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		if (aElement2ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT2;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aElement2ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AElement2ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aElement2ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AElement2ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<AProperty> result = new ArrayList<AProperty>((Collection<AProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement2Correct() {
		/**
		 * @OCL if aElement2.oclIsUndefined() then true else
		--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else
		if aElement2EntryType.oclIsUndefined() then false 
		else aElement2EntryType.aAllProperty()->includes(self.aElement2)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT;

		if (element2CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				element2CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element2CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAElement3EntryType() {
		AClassifier aElement3EntryType = basicGetAElement3EntryType();
		return aElement3EntryType != null && aElement3EntryType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aElement3EntryType) : aElement3EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAElement3EntryType() {
		/**
		 * @OCL if not self.element2Correct then null
		else 
		if self.aElement2.oclIsUndefined() then null
		else self.aElement2.aClassifier endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT3_ENTRY_TYPE;

		if (aElement3EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aElement3EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aElement3EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty getAElement3() {
		if (aElement3 != null && aElement3.eIsProxy()) {
			InternalEObject oldAElement3 = (InternalEObject) aElement3;
			aElement3 = (AProperty) eResolveProxy(oldAElement3);
			if (aElement3 != oldAElement3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MSUB_CHAIN__AELEMENT3,
							oldAElement3, aElement3));
			}
		}
		return aElement3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty basicGetAElement3() {
		return aElement3;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAElement3(AProperty newAElement3) {
		AProperty oldAElement3 = aElement3;
		aElement3 = newAElement3;
		boolean oldAElement3ESet = aElement3ESet;
		aElement3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MSUB_CHAIN__AELEMENT3,
					oldAElement3, aElement3, !oldAElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAElement3() {
		AProperty oldAElement3 = aElement3;
		boolean oldAElement3ESet = aElement3ESet;
		aElement3 = null;
		aElement3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MSUB_CHAIN__AELEMENT3,
					oldAElement3, null, oldAElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAElement3() {
		return aElement3ESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>AElement3</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<AProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: acore::classifiers::AProperty = 
	self.oclAsType(mrules::expressions::MAbstractExpression).containingAnnotation.aAnnotated.oclAsType(acore::classifiers::AProperty )
	in
	
	if aElement2.oclIsUndefined() 
	then OrderedSet{}
	else if not aElement2.aOperation->isEmpty()
	then OrderedSet{} 
	else if aElement3EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else aElement3EntryType.aAllProperty() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<AProperty> evalAElement3ChoiceConstruction(List<AProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		if (aElement3ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__AELEMENT3;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				aElement3ChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "AElement3ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(aElement3ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AElement3ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<AProperty> result = new ArrayList<AProperty>((Collection<AProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement3Correct() {
		/**
		 * @OCL if aElement3.oclIsUndefined() then true else
		--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else
		if aElement3EntryType.oclIsUndefined() then false
		else aElement3EntryType.aAllProperty()->includes(self.aElement3)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT;

		if (element3CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				element3CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element3CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getACastType() {
		if (aCastType != null && aCastType.eIsProxy()) {
			InternalEObject oldACastType = (InternalEObject) aCastType;
			aCastType = (AClassifier) eResolveProxy(oldACastType);
			if (aCastType != oldACastType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE,
							oldACastType, aCastType));
			}
		}
		return aCastType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetACastType() {
		return aCastType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setACastType(AClassifier newACastType) {
		AClassifier oldACastType = aCastType;
		aCastType = newACastType;
		boolean oldACastTypeESet = aCastTypeESet;
		aCastTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE,
					oldACastType, aCastType, !oldACastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetACastType() {
		AClassifier oldACastType = aCastType;
		boolean oldACastTypeESet = aCastTypeESet;
		aCastType = null;
		aCastTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE,
					oldACastType, null, oldACastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetACastType() {
		return aCastTypeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>ACast Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.aActiveClass
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalACastTypeChoiceConstraint(AClassifier trg) {
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		if (aCastTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACAST_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aCastTypeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "ACastTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aCastTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "ACastTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty getALastElement() {
		AProperty aLastElement = basicGetALastElement();
		return aLastElement != null && aLastElement.eIsProxy()
				? (AProperty) eResolveProxy((InternalEObject) aLastElement) : aLastElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AProperty basicGetALastElement() {
		/**
		 * @OCL if not self.aElement3.oclIsUndefined() then self.aElement3 else
		if not self.aElement2.oclIsUndefined() then self.aElement2 else
		if not self.aElement1.oclIsUndefined() then self.aElement1 else
		null endif endif endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ALAST_ELEMENT;

		if (aLastElementDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLastElementDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLastElementDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AProperty result = (AProperty) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAChainCalculatedType() {
		AClassifier aChainCalculatedType = basicGetAChainCalculatedType();
		return aChainCalculatedType != null && aChainCalculatedType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aChainCalculatedType) : aChainCalculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAChainCalculatedType() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_TYPE;

		if (aChainCalculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aChainCalculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChainCalculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getAChainCalculatedSimpleType() {
		/**
		 * @OCL let nl: acore::classifiers::ASimpleType = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE;

		if (aChainCalculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aChainCalculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aChainCalculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getChainCalculatedSingular() {
		/**
		 * @OCL let nl: Boolean = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;

		if (chainCalculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCalculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessor getProcessor() {
		return processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessor(MProcessor newProcessor) {
		MProcessor oldProcessor = processor;
		processor = newProcessor == null ? PROCESSOR_EDEFAULT : newProcessor;
		boolean oldProcessorESet = processorESet;
		processorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MSUB_CHAIN__PROCESSOR,
					oldProcessor, processor, !oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProcessor() {
		MProcessor oldProcessor = processor;
		boolean oldProcessorESet = processorESet;
		processor = PROCESSOR_EDEFAULT;
		processorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MSUB_CHAIN__PROCESSOR,
					oldProcessor, PROCESSOR_EDEFAULT, oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProcessor() {
		return processorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition getProcessorDefinition() {
		MProcessorDefinition processorDefinition = basicGetProcessorDefinition();
		return processorDefinition != null && processorDefinition.eIsProxy()
				? (MProcessorDefinition) eResolveProxy((InternalEObject) processorDefinition) : processorDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition basicGetProcessorDefinition() {
		/**
		 * @OCL if (let e0: Boolean = processor = mrules::expressions::MProcessor::None in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then null
		else createProcessorDefinition()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

		if (processorDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				processorDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(processorDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProcessorDefinition result = (MProcessorDefinition) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getPreviousExpression() {
		MAbstractExpression previousExpression = basicGetPreviousExpression();
		return previousExpression != null && previousExpression.eIsProxy()
				? (MAbstractExpression) eResolveProxy((InternalEObject) previousExpression) : previousExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetPreviousExpression() {
		/**
		 * @OCL if containingExpression.oclIsUndefined() then null
		else if not containingExpression.oclIsKindOf(MBaseChain) then null
		else let chain: MBaseChain = containingExpression.oclAsType(MBaseChain) in
		let pos: Integer = chain.subExpression->indexOf(self) in 
		
		if pos=1	then chain.oclAsType(MAbstractExpression)
		else chain.subExpression->at(pos-1).oclAsType(MAbstractExpression) endif
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MSUB_CHAIN__PREVIOUS_EXPRESSION;

		if (previousExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				previousExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(previousExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getNextExpression() {
		MAbstractExpression nextExpression = basicGetNextExpression();
		return nextExpression != null && nextExpression.eIsProxy()
				? (MAbstractExpression) eResolveProxy((InternalEObject) nextExpression) : nextExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetNextExpression() {
		/**
		 * @OCL  let chain: MBaseChain = eContainer().oclAsType(MBaseChain) in
		let pos: Integer = chain.subExpression->indexOf(self) in 
		
		if chain.subExpression->at(pos+1).oclIsUndefined() then null
		else chain.subExpression->at(pos+1).oclAsType(MAbstractExpression)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MSUB_CHAIN__NEXT_EXPRESSION;

		if (nextExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				nextExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(nextExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSubChainAction getDoAction() {
		/**
		 * @OCL mrules::expressions::MSubChainAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MSUB_CHAIN__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MSubChainAction result = (MSubChainAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isLastSubchain() {

		/**
		 * @OCL self.eContainer().oclAsType(MBaseChain).subExpression->last() = self
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(0);
		if (isLastSubchainBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isLastSubchainBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isLastSubchainBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String uniqueSubchainName() {

		/**
		 * @OCL 
		let chainNumber:String = 
		(self.eContainer().oclAsType(MBaseChain).subExpression->size()+1 - self.eContainer().oclAsType(MBaseChain).subExpression->indexOf(self)).toString() in
		let vName: String = 'sub'.concat(if self.eContainer().oclAsType(MBaseChain).uniqueChainNumber().oclIsUndefined() then '' else self.eContainer().oclAsType(MBaseChain).uniqueChainNumber() endif).concat(chainNumber) in 
		vName
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(1);
		if (uniqueSubchainNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				uniqueSubchainNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(uniqueSubchainNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean returnSingularPureChain() {

		/**
		 * @OCL let previousSingular: Boolean =  
		if self.previousExpression.oclAsType(MAbstractChain).processor <> MProcessor::None then self.previousExpression.oclAsType(MAbstractChain).processorReturnsSingular() else self.previousExpression.aSingular endif in
		
		let s1: Boolean = 
		if aElement1.oclIsUndefined() then true else aElement1.aSingular endif in
		let s2: Boolean =
		if aElement2.oclIsUndefined() then true else aElement2.aSingular endif in
		let s3: Boolean = 
		if aElement3.oclIsUndefined() then true else aElement3.aSingular endif in
		
		previousSingular and s1 and s2 and s3 
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(2);
		if (returnSingularPureChainBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				returnSingularPureChainBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(returnSingularPureChainBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForOthers() {

		/**
		 * @OCL 
		if length()=1 then codeForLength1() 
		else if length()=2 then codeForLength2() 
		else if length()=3 then codeForLength3() 
		else self.uniqueSubchainName() endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(3);
		if (asCodeForOthersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				asCodeForOthersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForOthersBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength1() {

		/**
		 * @OCL let chain: MSubChain = self.oclAsType(MSubChain) in
		
		let noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and 
		chain.nextExpression.oclAsType(MAbstractChain).aLastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None) in
		
		
		let b: String =  self.oclAsType(MSubChain).uniqueSubchainName().concat('.') in 
		b.concat(unsafeChainStepAsCode(1)).concat(if self.oclAsType(MSubChain).chainCalculatedSingular then '' else
		if noRejectNeeded then '' else '->reject(oclIsUndefined())'endif.concat('->asOrderedSet()') endif)
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(4);
		if (codeForLength1BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				codeForLength1BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength1BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength2() {

		/**
		 * @OCL 
		let chain: MSubChain = self.oclAsType(MSubChain) in
		
		let noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and 
		chain.nextExpression.oclAsType(MAbstractChain).aLastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)
		
		in
		
		let b: String = self.oclAsType(MSubChain).uniqueSubchainName().concat('.') in 
		
		if  chain.previousExpression.calculatedOwnSingular then
		
		if aElement1.aSingular and aElement2.aSingular then
		let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,2)).concat('\nendif')
		
		else if aElement1.aSingular and (not aElement2.aSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,2)).concat('\nendif')
		
		else  if (not aElement1.aSingular) and aElement2.aSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else  if (not aElement1.aSingular) and (not aElement2.aSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->asOrderedSet()')
		
		
		else null
		
		endif  endif endif endif
		
		else
		
		b.concat(unsafeChainAsCode(1,1)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(2,2)).concat(if noRejectNeeded then '' else '->reject(oclIsUndefined())' endif.concat(
		'->asOrderedSet()'))
		
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(5);
		if (codeForLength2BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				codeForLength2BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength2BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength3() {

		/**
		 * @OCL let subchain: MSubChain = self.oclAsType(MSubChain ) in
		
		let noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (subchain.nextExpression.oclIsUndefined()) and 
		subchain.nextExpression.oclAsType(MAbstractChain).aLastElement.oclIsUndefined() and subchain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)
		in
		let abstract:  MAbstractExpressionWithBase = self.oclAsType(mrules::expressions::MAbstractExpressionWithBase) in
		
		
		let b: String = subchain.uniqueSubchainName().concat('.')  in 
		
		if  subchain.previousExpression.calculatedOwnSingular then
		
		
		if aElement1.aSingular and aElement2.aSingular and aElement3.aSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('\nendif')
		
		else if aElement1.aSingular and aElement2.aSingular and (not aElement3.aSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat(if subchain.nextExpression.oclIsUndefined() then '' else '->asOrderedSet()' endif).concat('\nendif')
		
		else if aElement1.aSingular and (not aElement2.aSingular) and aElement3.aSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat(--'->reject(oclIsUndefined())
		'->asOrderedSet()\nendif')
		
		else if aElement1.aSingular and (not aElement2.aSingular) and (not aElement3.aSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('->asOrderedSet()\nendif')
		
		else if (not aElement1.aSingular) and aElement2.aSingular and aElement3.aSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat(--'->reject(oclIsUndefined()) 
		'->asOrderedSet()')
		
		else if (not aElement1.aSingular) and aElement2.aSingular and (not aElement3.aSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat(if subchain.nextExpression.oclIsUndefined() then '' else '->asOrderedSet()' endif)
		
		else if (not aElement1.aSingular) and (not aElement2.aSingular) and aElement3.aSingular then
		b.concat(unsafeChainAsCode(1,3)).concat(--'->reject(oclIsUndefined())
		'->asOrderedSet()')
		
		else if (not aElement1.aSingular) and (not aElement2.aSingular) and (not aElement3.aSingular) then
		b.concat(unsafeChainAsCode(1,3)).concat(--'->reject(oclIsUndefined())
		'->asOrderedSet()')
		
		else null
		endif endif endif endif endif endif endif endif 
		
		else
		
		b.concat(unsafeChainAsCode(1,1)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(2,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat(if noRejectNeeded then '' else '->reject(oclIsUndefined())' endif.concat(
		'->asOrderedSet()'))
		
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MSUB_CHAIN.getEOperations().get(6);
		if (codeForLength3BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				codeForLength3BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength3BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer length() {

		/**
		 * @OCL if not aElement3.oclIsUndefined() then 3
		else if not aElement2.oclIsUndefined() then 2
		else if not aElement1.oclIsUndefined() then 1
		else 0 endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(0);
		if (lengthBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				lengthBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(lengthBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeElementAsCode(Integer step) {

		/**
		 * @OCL let element: acore::classifiers::AProperty = if step=1 then aElement1
		else if step=2 then aElement2 
		else if step=3 then aElement3
		else null endif endif endif in
		
		if element.oclIsUndefined() then 'ERROR' else element.aName endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(1);
		if (unsafeElementAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				unsafeElementAsCodeecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(unsafeElementAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainStepAsCode(Integer step) {

		/**
		 * @OCL if step=1 then
		if aElement1.oclIsUndefined() then 'MISSING ELEMENT 1'
		else unsafeElementAsCode(1) endif
		else if step=2 then
		if aElement2.oclIsUndefined() then 'MISSING ELEMENT 2'
		else unsafeElementAsCode(2) endif
		else if step=3 then
		if aElement3.oclIsUndefined() then 'MISSING ELEMENT 3'
		else unsafeElementAsCode(3) endif
		else 'ERROR'
		endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(2);
		if (unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep) {

		/**
		 * @OCL unsafeChainAsCode(fromStep, length())
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(3);
		if (unsafeChainAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(unsafeChainAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep, Integer toStep) {

		/**
		 * @OCL let end: Integer = if (length() > toStep) then toStep else length() endif in
		
		if fromStep=1 then
		if end=3 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2)).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2))
		else if end=1 then unsafeChainStepAsCode(1) else '' endif
		endif endif
		else if fromStep=2 then
		if end=3 then
		unsafeChainStepAsCode(2).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then unsafeChainStepAsCode(2) else '' endif
		endif
		else if fromStep=3 then
		if end=3 then unsafeChainStepAsCode(3) else '' endif
		else 'ERROR'
		endif endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(4);
		if (unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			evalEnv.add("toStep", toStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String procAsCode() {

		/**
		 * @OCL if self.processor= mrules::expressions::MProcessor::IsNull then '.oclIsUndefined()' 
		else if self.processor = mrules::expressions::MProcessor::AllUpperCase then 'toUpperCase()'
		else if self.processor = mrules::expressions::MProcessor::IsInvalid then '.oclIsInvalid()'
		else if self.processor = mrules::expressions::MProcessor::Container then 'eContainer()'
		else  self.processor.toString()
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(9);
		if (procAsCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procAsCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procAsCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isCustomCodeProcessor() {

		/**
		 * @OCL if self.processorIsSet().oclIsUndefined() then null
		else 
		processor = mrules::expressions::MProcessor::Head or
		processor = mrules::expressions::MProcessor::Tail or
		processor = mrules::expressions::MProcessor::And or
		processor = mrules::expressions::MProcessor::Or  
		endif
		
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(10);
		if (isCustomCodeProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isCustomCodeProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isCustomCodeProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isProcessorSetOperator() {

		/**
		 * @OCL if processor = mrules::expressions::MProcessor::None then false 
		else
		processor=mrules::expressions::MProcessor::AsOrderedSet or
		processor=mrules::expressions::MProcessor::First or
		processor=mrules::expressions::MProcessor::IsEmpty or
		processor=mrules::expressions::MProcessor::Last or
		processor=mrules::expressions::MProcessor::NotEmpty or
		processor=mrules::expressions::MProcessor::Size or
		processor=mrules::expressions::MProcessor::Sum or
		processor=mrules::expressions::MProcessor::Head or
		processor=mrules::expressions::MProcessor::Tail or
		processor=mrules::expressions::MProcessor::And or
		processor=mrules::expressions::MProcessor::Or
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(11);
		if (isProcessorSetOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isProcessorSetOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isProcessorSetOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOwnXOCLOperator() {

		/**
		 * @OCL processor =mrules::expressions::MProcessor::CamelCaseLower or
		processor =mrules::expressions::MProcessor::CamelCaseToBusiness or
		processor =mrules::expressions::MProcessor::CamelCaseUpper 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(12);
		if (isOwnXOCLOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				isOwnXOCLOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorReturnsSingular() {

		/**
		 * @OCL if self.processor = mrules::expressions::MProcessor::None then null
		else if
		self.processor = mrules::expressions::MProcessor::AsOrderedSet or
		processor = mrules::expressions::MProcessor::Head or
		processor= mrules::expressions::MProcessor::Tail
		
		then 
		false
		else true
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(13);
		if (processorReturnsSingularBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				processorReturnsSingularBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorReturnsSingularBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorIsSet() {

		/**
		 * @OCL self.processor <> mrules::expressions::MProcessor::None
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(14);
		if (processorIsSetBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				processorIsSetBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorIsSetBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition createProcessorDefinition() {

		/**
		 * @OCL Tuple{processor=processor}
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(15);
		if (createProcessorDefinitionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				createProcessorDefinitionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(createProcessorDefinitionBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProcessorDefinition) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObject() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::Container},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(16);
		if (procDefChoicesForObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObjects() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size},
		Tuple{processor=mrules::expressions::MProcessor::First},
		Tuple{processor=mrules::expressions::MProcessor::Last},
		Tuple{processor=mrules::expressions::MProcessor::Head},
		Tuple{processor=mrules::expressions::MProcessor::Tail},
		Tuple{processor=mrules::expressions::MProcessor::Container}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(17);
		if (procDefChoicesForObjectsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForObjectsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBoolean() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::IsFalse},
		Tuple{processor=mrules::expressions::MProcessor::IsTrue},
		Tuple{processor=mrules::expressions::MProcessor::Not},
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(18);
		if (procDefChoicesForBooleanBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForBooleanBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleanBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBooleans() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::And},
		Tuple{processor=mrules::expressions::MProcessor::Or},
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(19);
		if (procDefChoicesForBooleansBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForBooleansBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleansBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForInteger() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::IsZero},
		Tuple{processor=mrules::expressions::MProcessor::IsOne},
		Tuple{processor=mrules::expressions::MProcessor::PlusOne},
		Tuple{processor=mrules::expressions::MProcessor::MinusOne},
		Tuple{processor=mrules::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mrules::expressions::MProcessor::Absolute},
		Tuple{processor=mrules::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(20);
		if (procDefChoicesForIntegerBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForIntegerBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegerBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForIntegers() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Sum},
		Tuple{processor=mrules::expressions::MProcessor::PlusOne},
		Tuple{processor=mrules::expressions::MProcessor::MinusOne},
		Tuple{processor=mrules::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mrules::expressions::MProcessor::Absolute},
		Tuple{processor=mrules::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size},
		Tuple{processor=mrules::expressions::MProcessor::First},
		Tuple{processor=mrules::expressions::MProcessor::Last},
		Tuple{processor=mrules::expressions::MProcessor::Head},
		Tuple{processor=mrules::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(21);
		if (procDefChoicesForIntegersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForIntegersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegersBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReal() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Round},
		Tuple{processor=mrules::expressions::MProcessor::Floor},
		Tuple{processor=mrules::expressions::MProcessor::IsZero},
		Tuple{processor=mrules::expressions::MProcessor::IsOne},
		Tuple{processor=mrules::expressions::MProcessor::PlusOne},
		Tuple{processor=mrules::expressions::MProcessor::MinusOne},
		Tuple{processor=mrules::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mrules::expressions::MProcessor::Absolute},
		Tuple{processor=mrules::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(22);
		if (procDefChoicesForRealBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForRealBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReals() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Round},
		Tuple{processor=mrules::expressions::MProcessor::Floor},
		Tuple{processor=mrules::expressions::MProcessor::Sum},
		Tuple{processor=mrules::expressions::MProcessor::PlusOne},
		Tuple{processor=mrules::expressions::MProcessor::MinusOne},
		Tuple{processor=mrules::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mrules::expressions::MProcessor::Absolute},
		Tuple{processor=mrules::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size},
		Tuple{processor=mrules::expressions::MProcessor::First},
		Tuple{processor=mrules::expressions::MProcessor::Last},
		Tuple{processor=mrules::expressions::MProcessor::Head},
		Tuple{processor=mrules::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(23);
		if (procDefChoicesForRealsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForRealsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForString() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Trim},
		Tuple{processor=mrules::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mrules::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mrules::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mrules::expressions::MProcessor::ToBoolean},
		Tuple{processor=mrules::expressions::MProcessor::ToInteger},
		Tuple{processor=mrules::expressions::MProcessor::ToReal},
		Tuple{processor=mrules::expressions::MProcessor::ToDate},
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(24);
		if (procDefChoicesForStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForStrings() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Trim},
		Tuple{processor=mrules::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mrules::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mrules::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mrules::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mrules::expressions::MProcessor::ToBoolean},
		Tuple{processor=mrules::expressions::MProcessor::ToInteger},
		Tuple{processor=mrules::expressions::MProcessor::ToReal},
		Tuple{processor=mrules::expressions::MProcessor::ToDate},
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size},
		Tuple{processor=mrules::expressions::MProcessor::First},
		Tuple{processor=mrules::expressions::MProcessor::Last},
		Tuple{processor=mrules::expressions::MProcessor::Head},
		Tuple{processor=mrules::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(25);
		if (procDefChoicesForStringsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForStringsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDate() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Year},
		Tuple{processor=mrules::expressions::MProcessor::Month},
		Tuple{processor=mrules::expressions::MProcessor::Day},
		Tuple{processor=mrules::expressions::MProcessor::Hour},
		Tuple{processor=mrules::expressions::MProcessor::Minute},
		Tuple{processor=mrules::expressions::MProcessor::Second},
		Tuple{processor=mrules::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mrules::expressions::MProcessor::ToHhMm},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::IsNull},
		Tuple{processor=mrules::expressions::MProcessor::NotNull},
		Tuple{processor=mrules::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mrules::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(26);
		if (procDefChoicesForDateBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForDateBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDateBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDates() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mrules::expressions::MProcessor::Year},
		Tuple{processor=mrules::expressions::MProcessor::Month},
		Tuple{processor=mrules::expressions::MProcessor::Day},
		Tuple{processor=mrules::expressions::MProcessor::Hour},
		Tuple{processor=mrules::expressions::MProcessor::Minute},
		Tuple{processor=mrules::expressions::MProcessor::Second},
		Tuple{processor=mrules::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mrules::expressions::MProcessor::ToHhMm},
		Tuple{processor=mrules::expressions::MProcessor::ToString},
		Tuple{processor=mrules::expressions::MProcessor::IsEmpty},
		Tuple{processor=mrules::expressions::MProcessor::NotEmpty},
		Tuple{processor=mrules::expressions::MProcessor::Size},
		Tuple{processor=mrules::expressions::MProcessor::First},
		Tuple{processor=mrules::expressions::MProcessor::Last},
		Tuple{processor=mrules::expressions::MProcessor::Head},
		Tuple{processor=mrules::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN.getEOperations().get(27);
		if (procDefChoicesForDatesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				procDefChoicesForDatesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MSUB_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDatesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MSUB_CHAIN,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_ENTRY_TYPE:
			if (resolve)
				return getAChainEntryType();
			return basicGetAChainEntryType();
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_AS_CODE:
			return getChainAsCode();
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT1:
			if (resolve)
				return getAElement1();
			return basicGetAElement1();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT1_CORRECT:
			return getElement1Correct();
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2_ENTRY_TYPE:
			if (resolve)
				return getAElement2EntryType();
			return basicGetAElement2EntryType();
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2:
			if (resolve)
				return getAElement2();
			return basicGetAElement2();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT2_CORRECT:
			return getElement2Correct();
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3_ENTRY_TYPE:
			if (resolve)
				return getAElement3EntryType();
			return basicGetAElement3EntryType();
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3:
			if (resolve)
				return getAElement3();
			return basicGetAElement3();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT3_CORRECT:
			return getElement3Correct();
		case ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE:
			if (resolve)
				return getACastType();
			return basicGetACastType();
		case ExpressionsPackage.MSUB_CHAIN__ALAST_ELEMENT:
			if (resolve)
				return getALastElement();
			return basicGetALastElement();
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_TYPE:
			if (resolve)
				return getAChainCalculatedType();
			return basicGetAChainCalculatedType();
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE:
			return getAChainCalculatedSimpleType();
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SINGULAR:
			return getChainCalculatedSingular();
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
			return getProcessor();
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR_DEFINITION:
			if (resolve)
				return getProcessorDefinition();
			return basicGetProcessorDefinition();
		case ExpressionsPackage.MSUB_CHAIN__PREVIOUS_EXPRESSION:
			if (resolve)
				return getPreviousExpression();
			return basicGetPreviousExpression();
		case ExpressionsPackage.MSUB_CHAIN__NEXT_EXPRESSION:
			if (resolve)
				return getNextExpression();
			return basicGetNextExpression();
		case ExpressionsPackage.MSUB_CHAIN__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT1:
			setAElement1((AProperty) newValue);
			return;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2:
			setAElement2((AProperty) newValue);
			return;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3:
			setAElement3((AProperty) newValue);
			return;
		case ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE:
			setACastType((AClassifier) newValue);
			return;
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
			setProcessor((MProcessor) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT1:
			unsetAElement1();
			return;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2:
			unsetAElement2();
			return;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3:
			unsetAElement3();
			return;
		case ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE:
			unsetACastType();
			return;
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
			unsetProcessor();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_ENTRY_TYPE:
			return basicGetAChainEntryType() != null;
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_AS_CODE:
			return CHAIN_AS_CODE_EDEFAULT == null ? getChainAsCode() != null
					: !CHAIN_AS_CODE_EDEFAULT.equals(getChainAsCode());
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT1:
			return isSetAElement1();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT1_CORRECT:
			return ELEMENT1_CORRECT_EDEFAULT == null ? getElement1Correct() != null
					: !ELEMENT1_CORRECT_EDEFAULT.equals(getElement1Correct());
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2_ENTRY_TYPE:
			return basicGetAElement2EntryType() != null;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT2:
			return isSetAElement2();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT2_CORRECT:
			return ELEMENT2_CORRECT_EDEFAULT == null ? getElement2Correct() != null
					: !ELEMENT2_CORRECT_EDEFAULT.equals(getElement2Correct());
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3_ENTRY_TYPE:
			return basicGetAElement3EntryType() != null;
		case ExpressionsPackage.MSUB_CHAIN__AELEMENT3:
			return isSetAElement3();
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT3_CORRECT:
			return ELEMENT3_CORRECT_EDEFAULT == null ? getElement3Correct() != null
					: !ELEMENT3_CORRECT_EDEFAULT.equals(getElement3Correct());
		case ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE:
			return isSetACastType();
		case ExpressionsPackage.MSUB_CHAIN__ALAST_ELEMENT:
			return basicGetALastElement() != null;
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_TYPE:
			return basicGetAChainCalculatedType() != null;
		case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE:
			return getAChainCalculatedSimpleType() != ACHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SINGULAR:
			return CHAIN_CALCULATED_SINGULAR_EDEFAULT == null ? getChainCalculatedSingular() != null
					: !CHAIN_CALCULATED_SINGULAR_EDEFAULT.equals(getChainCalculatedSingular());
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
			return isSetProcessor();
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR_DEFINITION:
			return basicGetProcessorDefinition() != null;
		case ExpressionsPackage.MSUB_CHAIN__PREVIOUS_EXPRESSION:
			return basicGetPreviousExpression() != null;
		case ExpressionsPackage.MSUB_CHAIN__NEXT_EXPRESSION:
			return basicGetNextExpression() != null;
		case ExpressionsPackage.MSUB_CHAIN__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MSUB_CHAIN__ACHAIN_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_ENTRY_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__CHAIN_AS_CODE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE;
			case ExpressionsPackage.MSUB_CHAIN__AELEMENT1:
				return ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT1;
			case ExpressionsPackage.MSUB_CHAIN__ELEMENT1_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT;
			case ExpressionsPackage.MSUB_CHAIN__AELEMENT2_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT2_ENTRY_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__AELEMENT2:
				return ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT2;
			case ExpressionsPackage.MSUB_CHAIN__ELEMENT2_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT;
			case ExpressionsPackage.MSUB_CHAIN__AELEMENT3_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT3_ENTRY_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__AELEMENT3:
				return ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT3;
			case ExpressionsPackage.MSUB_CHAIN__ELEMENT3_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT;
			case ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ACAST_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__ALAST_ELEMENT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ALAST_ELEMENT;
			case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_CALCULATED_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE;
			case ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SINGULAR:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;
			case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR;
			case ExpressionsPackage.MSUB_CHAIN__PROCESSOR_DEFINITION:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_ENTRY_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__ACHAIN_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE:
				return ExpressionsPackage.MSUB_CHAIN__CHAIN_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT1:
				return ExpressionsPackage.MSUB_CHAIN__AELEMENT1;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT:
				return ExpressionsPackage.MSUB_CHAIN__ELEMENT1_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT2_ENTRY_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__AELEMENT2_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT2:
				return ExpressionsPackage.MSUB_CHAIN__AELEMENT2;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT:
				return ExpressionsPackage.MSUB_CHAIN__ELEMENT2_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT3_ENTRY_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__AELEMENT3_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__AELEMENT3:
				return ExpressionsPackage.MSUB_CHAIN__AELEMENT3;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT:
				return ExpressionsPackage.MSUB_CHAIN__ELEMENT3_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ACAST_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__ACAST_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ALAST_ELEMENT:
				return ExpressionsPackage.MSUB_CHAIN__ALAST_ELEMENT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_CALCULATED_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE:
				return ExpressionsPackage.MSUB_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR:
				return ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR:
				return ExpressionsPackage.MSUB_CHAIN__PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION:
				return ExpressionsPackage.MSUB_CHAIN__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MABSTRACT_CHAIN___LENGTH:
				return ExpressionsPackage.MSUB_CHAIN___LENGTH;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
				return ExpressionsPackage.MSUB_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
				return ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER:
				return ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
				return ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___AS_CODE_FOR_OTHERS:
				return ExpressionsPackage.MSUB_CHAIN___AS_CODE_FOR_OTHERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH1:
				return ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH1;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH2:
				return ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH2;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH3:
				return ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH3;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_AS_CODE:
				return ExpressionsPackage.MSUB_CHAIN___PROC_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_CUSTOM_CODE_PROCESSOR:
				return ExpressionsPackage.MSUB_CHAIN___IS_CUSTOM_CODE_PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_PROCESSOR_SET_OPERATOR:
				return ExpressionsPackage.MSUB_CHAIN___IS_PROCESSOR_SET_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_OWN_XOCL_OPERATOR:
				return ExpressionsPackage.MSUB_CHAIN___IS_OWN_XOCL_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_RETURNS_SINGULAR:
				return ExpressionsPackage.MSUB_CHAIN___PROCESSOR_RETURNS_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_IS_SET:
				return ExpressionsPackage.MSUB_CHAIN___PROCESSOR_IS_SET;
			case ExpressionsPackage.MABSTRACT_CHAIN___CREATE_PROCESSOR_DEFINITION:
				return ExpressionsPackage.MSUB_CHAIN___CREATE_PROCESSOR_DEFINITION;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REAL:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_REAL;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REALS:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_REALS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRING:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_STRING;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATE:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_DATE;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATES:
				return ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_DATES;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MSUB_CHAIN___IS_LAST_SUBCHAIN:
			return isLastSubchain();
		case ExpressionsPackage.MSUB_CHAIN___UNIQUE_SUBCHAIN_NAME:
			return uniqueSubchainName();
		case ExpressionsPackage.MSUB_CHAIN___RETURN_SINGULAR_PURE_CHAIN:
			return returnSingularPureChain();
		case ExpressionsPackage.MSUB_CHAIN___AS_CODE_FOR_OTHERS:
			return asCodeForOthers();
		case ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH1:
			return codeForLength1();
		case ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH2:
			return codeForLength2();
		case ExpressionsPackage.MSUB_CHAIN___CODE_FOR_LENGTH3:
			return codeForLength3();
		case ExpressionsPackage.MSUB_CHAIN___LENGTH:
			return length();
		case ExpressionsPackage.MSUB_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
			return unsafeElementAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
			return unsafeChainStepAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MSUB_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0), (Integer) arguments.get(1));
		case ExpressionsPackage.MSUB_CHAIN___PROC_AS_CODE:
			return procAsCode();
		case ExpressionsPackage.MSUB_CHAIN___IS_CUSTOM_CODE_PROCESSOR:
			return isCustomCodeProcessor();
		case ExpressionsPackage.MSUB_CHAIN___IS_PROCESSOR_SET_OPERATOR:
			return isProcessorSetOperator();
		case ExpressionsPackage.MSUB_CHAIN___IS_OWN_XOCL_OPERATOR:
			return isOwnXOCLOperator();
		case ExpressionsPackage.MSUB_CHAIN___PROCESSOR_RETURNS_SINGULAR:
			return processorReturnsSingular();
		case ExpressionsPackage.MSUB_CHAIN___PROCESSOR_IS_SET:
			return processorIsSet();
		case ExpressionsPackage.MSUB_CHAIN___CREATE_PROCESSOR_DEFINITION:
			return createProcessorDefinition();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT:
			return procDefChoicesForObject();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS:
			return procDefChoicesForObjects();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN:
			return procDefChoicesForBoolean();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS:
			return procDefChoicesForBooleans();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER:
			return procDefChoicesForInteger();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS:
			return procDefChoicesForIntegers();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_REAL:
			return procDefChoicesForReal();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_REALS:
			return procDefChoicesForReals();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_STRING:
			return procDefChoicesForString();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS:
			return procDefChoicesForStrings();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_DATE:
			return procDefChoicesForDate();
		case ExpressionsPackage.MSUB_CHAIN___PROC_DEF_CHOICES_FOR_DATES:
			return procDefChoicesForDates();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (processor: ");
		if (processorESet)
			result.append(processor);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL '   ...   '.concat(
	let c:String=self.chainAsCode in
	if c='' then '' else '.'.concat(c) endif
	)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MSUB_CHAIN;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel '   ...   '.concat(
	let c:String=self.chainAsCode in
	if c='' then '' else '.'.concat(c) endif
	)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType let p:MProcessor = processor in
	if p <> MProcessor::None
	then
	 if      p = MProcessor::ToInteger or
	         p = MProcessor::Size  or
	         p = MProcessor::Round or
	         p = MProcessor::Floor or
	         p = MProcessor::Year or
	         p = MProcessor::Minute or
	         p = MProcessor::Month or
	         p = MProcessor::Second or
	         p = MProcessor::Day or
	         p = MProcessor::Hour
	   then acore::classifiers::ASimpleType::Integer
	 else if p = MProcessor::ToString or  
	         p = MProcessor::ToYyyyMmDd or
	         p = MProcessor::ToHhMm or
	         p = MProcessor::AllLowerCase or
	         p = MProcessor::AllUpperCase or
	         p = MProcessor::CamelCaseLower or
	         p = MProcessor::CamelCaseToBusiness or
	         p = MProcessor::CamelCaseUpper or
	         p = MProcessor::FirstUpperCase or
	         p = MProcessor::Trim
	   then acore::classifiers::ASimpleType::String
	 else if p = MProcessor::ToReal or
	         p = MProcessor::OneDividedBy or
	         p = MProcessor::Sum
	   then acore::classifiers::ASimpleType::Real
	 else if p = MProcessor::ToBoolean or
	         p = MProcessor::IsEmpty or
	         p = MProcessor::NotEmpty or
	         p = MProcessor::Not  or
	         p = MProcessor::IsFalse or
	         p = MProcessor::IsTrue or
	         p = MProcessor::IsZero or
	         p = MProcessor::IsOne or
	         p = MProcessor::NotNull or
	         p = MProcessor::IsNull or
	         p = MProcessor::IsInvalid or
	         p = MProcessor::And or
	         p= MProcessor::Or
	   then acore::classifiers::ASimpleType::Boolean
	  else if p = MProcessor::ToDate 
	   then acore::classifiers::ASimpleType::Date
	  else if  self.aLastElement.oclIsUndefined() 
	   then  self.previousExpression.aCalculatedOwnSimpleType
	 else self.aLastElement.aSimpleType
	  endif endif endif endif endif endif
	else
	if
	   self.aLastElement.oclIsUndefined() then self.previousExpression.aCalculatedOwnSimpleType
	   else self.aLastElement.aSimpleType
	   endif endif
	
	
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory if self.aLastElement=null then self.previousExpression.aMandatory else
	self.aLastElement.aMandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType if self.aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::None then
	if self.aCastType.oclIsUndefined() then
	if self.aLastElement=null then self.previousExpression.aCalculatedOwnType else
	self.aLastElement.aClassifier  endif
	else self.aCastType
	endif
	else null endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if self.processorIsSet() 
	 then self.processorReturnsSingular()
	 else self.chainCalculatedSingular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode -- chainType: the type coming out of the chain
	let chainTypeString:String =
	 if self.aLastElement.oclIsUndefined()  then 
	   aTypeAsOcl( self.aSelfObjectPackage, self.previousExpression.aCalculatedOwnType, self.previousExpression.aCalculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  
	 else
	   aTypeAsOcl( self.aSelfObjectPackage, self.aChainCalculatedType, self.aChainCalculatedSimpleType, self.chainCalculatedSingular )  endif  in
	-- singularVersion of chainType
	let chainTypeStringSingular: String =
	  if self.aLastElement.oclIsUndefined() then 
		  aTypeAsOcl(aSelfObjectPackage, self.previousExpression.aCalculatedOwnType, self.previousExpression.aCalculatedOwnSimpleType, true)
	  else
		   aTypeAsOcl(aSelfObjectPackage, self.aLastElement.aClassifier, self.aLastElement.aSimpleType, true) endif in
	-- castType: the type to which we cast, if we cast or chainType otherwise
	let castTypeString: String = 
	  if aCastType.oclIsUndefined()
	     then chainTypeString
	     else aTypeAsOcl(aSelfObjectPackage, aCastType, acore::classifiers::ASimpleType::None, self.chainCalculatedSingular) endif in
	-- singular version of castType
	let castTypeStringSingular: String = 
	 if aCastType.oclIsUndefined()
	   then chainTypeStringSingular
	   else aTypeAsOcl(aSelfObjectPackage,aCastType, acore::classifiers::ASimpleType::None, true) endif in
	--let name for the input to cast
	let castName: String = 
	 self.uniqueSubchainName().concat('cast') in	 
	--let name for the input to proc
	let procName: String = 
	 self.uniqueSubchainName().concat('proc') in	 
	--code for chained properties/operation:
	let code: String = 
	 self.asCodeForOthers() in
	
	' in \n'
	.concat(if self.processor = MProcessor::None  
	 then '' 
	 else 'let '.concat(procName).concat(':').concat(castTypeString).concat(' = ') endif)
	.concat(if self.aCastType.oclIsUndefined()
	 then  ' if '.concat(self.uniqueSubchainName()).concat( ' = null \n  then null \n else ').concat(code).concat( ' endif \n')
	 else  'let '.concat(castName).concat(':').concat(chainTypeString).concat(' =').concat(' if '.concat(self.uniqueSubchainName()).concat( ' = null \n  then null \n else ')).concat(code).concat( ' endif \n')
	          .concat(' in \n')
	          .concat(if self.chainCalculatedSingular 
	               then 'if '.concat(castName).concat('.oclIsUndefined() then null else ') 
	               else '' endif)
	          .concat(if self.chainCalculatedSingular 
	               then 'if '.concat(castName).concat('.oclIsKindOf(').concat(castTypeStringSingular).concat(')\n then '.concat(castName).concat('.oclAsType(').concat(castTypeStringSingular)
	                        .concat(') else null endif endif '))
	               else  castName.concat('->iterate(i:').concat(chainTypeStringSingular).concat('; r: OrderedSet(').concat(castTypeStringSingular)
	                       .concat(')=OrderedSet{} | if i.oclIsKindOf(').concat(castTypeStringSingular).concat(') then r->including(i.oclAsType(').concat(castTypeStringSingular).concat(')')
	                       .concat(')->asOrderedSet() \n else r endif)') endif) endif)
	.concat(if self.processor = MProcessor::None 
	  then '' 
	  else ' in \n if '.concat(procName)
	          .concat(if self.isProcessorSetOperator() 
	                  then '->' 
	                  else '.' endif)
	          .concat(if self.isCustomCodeProcessor() 
	                   then 'isEmpty()' 
	                   else procAsCode().concat('.oclIsUndefined()') endif)
	           .concat(' then null \n else ').concat(procName)
	           .concat( if self.isProcessorSetOperator() 
	                   then '->' 
	                   else '.' endif)
	           .concat(if self.isCustomCodeProcessor() 
	                  then   let iterateBase: String = 'iterate( x:'.concat(chainTypeStringSingular).concat('; s:') in
	                            if self.processor = MProcessor::And or processor = MProcessor::Or 
	                                   then let checkPart : String = if  processor= MProcessor::And  then 'false' else 'true' endif in 
	                                           let elsePart : String = if processor= MProcessor::And then 'true' else 'false' endif in
	                                           iterateBase.concat(chainTypeStringSingular).concat('= ').concat(elsePart).concat('|  if ( x) = ').concat(checkPart).concat( ' \n then ').concat(checkPart).concat('\n')
	                                           .concat('else if (s)=').concat(checkPart).concat('\n').concat('then ').concat(checkPart).concat('\n')
	                                           .concat(' else if x =null then null \n'
	                                           .concat('else if s =null then null'))
	                                           .concat(' else ').concat(elsePart).concat(' endif endif endif endif)')
	                                   else  let elementToExclude: String = 
	                                               --QUESTION why code, and not procName?? ProcName seems much better... I do the change, change back if problems appear...
	                                               --code
	                                               procName
	                                               .concat(if processor=MProcessor::Head 
	                                               -- CHANGE turned around as head and tail delivered the wrong code.
	                                                    then '->last() ' 
	                                                    else '->first() ' endif) in
	                                             iterateBase.concat(chainTypeString).concat('= ').concat('OrderedSet{} | if x.oclIsUndefined() then s else if x='
	                                            .concat(elementToExclude)
	                                            .concat('then s else s->including(x)->asOrderedSet() endif endif)')) endif
	                   else self.procAsCode() endif)
	           .concat( '\n  endif' ) endif  ) 
		      
	
	/*
	-- chainType: the type coming out of the chain
	let chainTypeString:String =
	 if self.lastElement.oclIsUndefined()  then 
	   typeAsOcl( self.selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  
	 else
	   typeAsOcl( self.selfObjectPackage, self.chainCalculatedType, self.chainCalculatedSimpleType, self.chainCalculatedSingular )  
	 endif  in
	let chainTypeStringSingular: String =
	  if self.lastElement.oclIsUndefined() then 
		  typeAsOcl(selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, true)
	  else
		   typeAsOcl(selfObjectPackage, self.lastElement.calculatedType, self.lastElement.calculatedSimpleType, true) endif in
	-- castType: the type to which we cast.
	let castTypeString: String = 
	  typeAsOcl(selfObjectPackage, castType, SimpleType::None, self.chainCalculatedSingular) in
	let castTypeStringSingular: String = 
	 typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in
	--let name for the input to cast
	let castName: String = 
	 self.uniqueSubchainName().concat('cast') in	 
	--let name for the input to proc
	let procName: String = 
	 self.uniqueSubchainName().concat('proc') in	 
	--code for chained properties/operation:
	let code: String = 
	 self.asCodeForOthers() in
	
	' in \n'
	.concat(if self.processor = MProcessor::None  
	then '' 
	else 'let '.concat(procName.concat(':').concat(if self.castType.oclIsUndefined() then chainTypeString else castTypeString endif).concat(' = ')) endif)
	.concat(if self.castType.oclIsUndefined()then  ' if '.concat(self.uniqueSubchainName()).concat( ' = null \n  then null \n else ')
	.concat(code).concat( ' endif \n')
	else  'let '.concat(castName).concat(':').concat(chainTypeString).concat(' =')
	.concat(' if '.concat(self.uniqueSubchainName()).concat( ' = null \n  then null \n else '))
	.concat(code).concat( ' endif \n')
	.concat(' in \n').concat(if self.chainCalculatedSingular then 'if '.concat(castName).concat('.oclIsUndefined() then null else ') else '' endif)
	.concat(if self.chainCalculatedSingular then 'if '.concat(castName).concat('.oclIsKindOf(').concat(castTypeStringSingular).concat(')\n then '.concat(castName).concat('.oclAsType(').concat(castTypeStringSingular).concat(') else null endif endif '))
	else  
	
	castName.concat('->iterate(i:'.concat(chainTypeStringSingular).concat('; r: OrderedSet(').concat(castTypeStringSingular).concat(')=OrderedSet{} | if i.oclIsKindOf(').concat(castTypeStringSingular).concat(') then r->including(i.oclAsType(').concat(castTypeStringSingular).concat(')').concat(')->asOrderedSet() \n else r endif)'))
	endif) endif)
	
	.concat(if self.processor = MProcessor::None then '' else ' in \n if '.concat(procName.concat(if self.isProcessorSetOperator() then '->' else '.' endif.concat(if self.isCustomCodeProcessor() then 'isEmpty()' else self.procAsCode().concat('.oclIsUndefined()')endif))).concat(' then null \n else ').concat(procName).concat( if self.isProcessorSetOperator() then '->' else '.' endif
	
	.concat(if self.isCustomCodeProcessor() then
		   let checkPart : String = if  processor= MProcessor::And then 'false' else 'true' endif
	in
	let elsePart : String = if processor= MProcessor::And then 'true' else 'false' endif in
	
	let iterateBase: String = 
	'iterate( x:'.concat(chainTypeStringSingular).concat('; s:') in
	
	if self.processor = MProcessor::And or processor = MProcessor::Or then
	iterateBase.concat(chainTypeStringSingular).concat('= ').concat(elsePart)
	.concat('|  if ( x) = ').concat(checkPart).concat( ' \n then ').concat(checkPart).concat('\n').concat('else if (s)=').concat(checkPart).concat('\n').concat('then ').concat(checkPart).concat('\n').concat(' else if x =null then null \n'.concat('else if s =null then null')
	).concat(' else ').concat(elsePart).concat(' endif endif endif endif)')
	
	else
	iterateBase.concat(chainTypeString).concat('= ').concat('OrderedSet{} | if x.oclIsUndefined() then s else if x='.concat(code).concat(if processor=MProcessor::Head then '->first() ' else '->last() ' endif).concat('then s else s->including(x)->asOrderedSet() endif endif)'))
	endif
	else self.procAsCode() endif).concat( 
			 '\n  endif' )) endif  ) 
			 *\/
	
	
	
	
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MSUB_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MSubChainImpl
