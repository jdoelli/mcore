/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MChain;
import com.montages.mrules.expressions.MConstantLet;
import com.montages.mrules.expressions.MNamedConstant;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MNamed Constant</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MNamedConstantImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MNamedConstantImpl extends MAbstractLetImpl implements MNamedConstant {
	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected MConstantLet expression;

	/**
	 * This is true if the Expression containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expressionESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultValue <em>Default Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #defaultValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultValueBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNamedConstantImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MNAMED_CONSTANT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MConstantLet getExpression() {
		if (expression != null && expression.eIsProxy()) {
			InternalEObject oldExpression = (InternalEObject) expression;
			expression = (MConstantLet) eResolveProxy(oldExpression);
			if (expression != oldExpression) {
				InternalEObject newExpression = (InternalEObject) expression;
				NotificationChain msgs = oldExpression.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, null, null);
				if (newExpression.eInternalContainer() == null) {
					msgs = newExpression.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, oldExpression, expression));
			}
		}
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MConstantLet basicGetExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression(MConstantLet newExpression, NotificationChain msgs) {
		MConstantLet oldExpression = expression;
		expression = newExpression;
		boolean oldExpressionESet = expressionESet;
		expressionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, oldExpression, newExpression, !oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(MConstantLet newExpression) {
		if (newExpression != expression) {
			NotificationChain msgs = null;
			if (expression != null)
				msgs = ((InternalEObject) expression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, null, msgs);
			if (newExpression != null)
				msgs = ((InternalEObject) newExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, null, msgs);
			msgs = basicSetExpression(newExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION,
						newExpression, newExpression, !oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpression(NotificationChain msgs) {
		MConstantLet oldExpression = expression;
		expression = null;
		boolean oldExpressionESet = expressionESet;
		expressionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, oldExpression, null, oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpression() {
		if (expression != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expression).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION, null, msgs);
			msgs = basicUnsetExpression(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION,
						null, null, oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpression() {
		return expressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain defaultValue() {

		/**
		 * @OCL Tuple{base=ExpressionBase::SelfObject}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EOperation eOperation = ExpressionsPackage.Literals.MNAMED_CONSTANT.getEOperations().get(0);
		if (defaultValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				defaultValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MNAMED_CONSTANT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MNAMED_CONSTANT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MChain) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION:
			return basicUnsetExpression(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION:
			if (resolve)
				return getExpression();
			return basicGetExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION:
			setExpression((MConstantLet) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION:
			unsetExpression();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_CONSTANT__EXPRESSION:
			return isSetExpression();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MNAMED_CONSTANT___DEFAULT_VALUE:
			return defaultValue();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Where'
	/*
	let c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in
	if c.oclIsUndefined() then 'Let'
	else if c.namedExpression->notEmpty()
		then 'Let'
		else if c.namedConstant->last()=self
		    then 'Definition'
		    else 'Let' endif
		endif
	endif
	*\/
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode if expression.oclIsUndefined() then 'MISSING EXPRESSION' else
	
	let c: String = expression.asCode in
	let t: String = if  expression.aClassifier.oclIsUndefined()
	then expression.aSimpleType.toString()
	else expression.aClassifier.aName endif in
	
	if aName.oclIsUndefined() or aName='' 
	then c
	else 'let '.concat(aName).concat(': ').concat( aTypeAsOcl(aSelfObjectPackage, expression.aClassifier, expression.aSimpleType, expression.aSingular) ).concat(' = ').concat(c).concat(' in')
	endif
	
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory if expression.oclIsUndefined() then false
	else expression.aMandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if expression.oclIsUndefined() then true
	else expression.aSingular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType if expression.oclIsUndefined() then acore::classifiers::ASimpleType::None
	else expression.aSimpleType endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType if expression.oclIsUndefined() then null
	else expression.aClassifier endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_CONSTANT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MNamedConstantImpl
