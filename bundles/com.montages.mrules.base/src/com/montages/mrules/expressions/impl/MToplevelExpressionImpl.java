/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MToplevelExpression;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MToplevel Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public abstract class MToplevelExpressionImpl extends MAbstractExpressionImpl implements MToplevelExpression {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MToplevelExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MTOPLEVEL_EXPRESSION;
	}

} //MToplevelExpressionImpl
