/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MBaseChain;
import com.montages.mrules.expressions.MCallArgument;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MCall Argument</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MCallArgumentImpl#getFirst <em>First</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCallArgumentImpl#getLast <em>Last</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCallArgumentImpl#getCall <em>Call</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MCallArgumentImpl extends MAbstractExpressionWithBaseImpl implements MCallArgument {
	/**
	 * The default value of the '{@link #getFirst() <em>First</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean FIRST_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLast() <em>Last</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLast()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean LAST_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFirst <em>First</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirst
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression firstDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLast <em>Last</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLast
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression lastDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsCode <em>As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCallArgumentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MCALL_ARGUMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getFirst() {
		/**
		 * @OCL if self.call.oclIsUndefined() 
		then false
		else self.call.callArgument->first()=self endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCALL_ARGUMENT;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MCALL_ARGUMENT__FIRST;

		if (firstDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				firstDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCALL_ARGUMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(firstDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCALL_ARGUMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getLast() {
		/**
		 * @OCL if self.call.oclIsUndefined() 
		then false
		else self.call.callArgument->last()=self endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCALL_ARGUMENT;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MCALL_ARGUMENT__LAST;

		if (lastDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				lastDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCALL_ARGUMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(lastDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCALL_ARGUMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MBaseChain getCall() {
		if (eContainerFeatureID() != ExpressionsPackage.MCALL_ARGUMENT__CALL)
			return null;
		return (MBaseChain) eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MBaseChain basicGetCall() {
		if (eContainerFeatureID() != ExpressionsPackage.MCALL_ARGUMENT__CALL)
			return null;
		return (MBaseChain) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCall(MBaseChain newCall, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCall, ExpressionsPackage.MCALL_ARGUMENT__CALL, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCall(MBaseChain newCall) {
		if (newCall != eInternalContainer()
				|| (eContainerFeatureID() != ExpressionsPackage.MCALL_ARGUMENT__CALL && newCall != null)) {
			if (EcoreUtil.isAncestor(this, newCall))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCall != null)
				msgs = ((InternalEObject) newCall).eInverseAdd(this, ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT,
						MBaseChain.class, msgs);
			msgs = basicSetCall(newCall, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MCALL_ARGUMENT__CALL, newCall,
					newCall));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCall((MBaseChain) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			return basicSetCall(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			return eInternalContainer().eInverseRemove(this, ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT,
					MBaseChain.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__FIRST:
			return getFirst();
		case ExpressionsPackage.MCALL_ARGUMENT__LAST:
			return getLast();
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			if (resolve)
				return getCall();
			return basicGetCall();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			setCall((MBaseChain) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			setCall((MBaseChain) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCALL_ARGUMENT__FIRST:
			return FIRST_EDEFAULT == null ? getFirst() != null : !FIRST_EDEFAULT.equals(getFirst());
		case ExpressionsPackage.MCALL_ARGUMENT__LAST:
			return LAST_EDEFAULT == null ? getLast() != null : !LAST_EDEFAULT.equals(getLast());
		case ExpressionsPackage.MCALL_ARGUMENT__CALL:
			return basicGetCall() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asCode let s:String = baseAsCode in
	
	if
	(self.call.aLastElement.aOperation.aParameter->at(self.call.aLastElement.aOperation.aParameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).aParameter))
	.aSingular).oclIsUndefined()
	then s
	else if
	(self.call.aLastElement.aOperation.aParameter->at(self.call.aLastElement.aOperation.aParameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).aParameter))
	.aSingular) 
	then s
	else
	s.concat('->asOrderedSet()')
	endif
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsCode() {
		EClass eClass = (ExpressionsPackage.Literals.MCALL_ARGUMENT);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE;

		if (asCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Argument'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MCALL_ARGUMENT);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MCallArgumentImpl
