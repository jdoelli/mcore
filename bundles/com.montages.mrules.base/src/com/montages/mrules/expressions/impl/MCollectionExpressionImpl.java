/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.CollectionOperator;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractExpression;
import com.montages.mrules.expressions.MAccumulator;
import com.montages.mrules.expressions.MChain;
import com.montages.mrules.expressions.MChainOrApplication;
import com.montages.mrules.expressions.MCollectionExpression;
import com.montages.mrules.expressions.MIterator;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.resource.Resource.Internal;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.eclipse.ocl.util.TypeUtil;

import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.core.util.XoclMutlitypeComparisonUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MCollection Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionExpressionImpl#getCollectionOperator <em>Collection Operator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionExpressionImpl#getCollection <em>Collection</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionExpressionImpl#getIteratorVar <em>Iterator Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionExpressionImpl#getAccumulatorVar <em>Accumulator Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MCollectionExpressionImpl#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MCollectionExpressionImpl extends MAbstractExpressionImpl
		implements MCollectionExpression, IXoclInitializable {
	/**
	 * The default value of the '{@link #getCollectionOperator() <em>Collection Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperator()
	 * @generated
	 * @ordered
	 */
	protected static final CollectionOperator COLLECTION_OPERATOR_EDEFAULT = CollectionOperator.COLLECT;

	/**
	 * The cached value of the '{@link #getCollectionOperator() <em>Collection Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollectionOperator()
	 * @generated
	 * @ordered
	 */
	protected CollectionOperator collectionOperator = COLLECTION_OPERATOR_EDEFAULT;

	/**
	 * This is true if the Collection Operator attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean collectionOperatorESet;

	/**
	 * The cached value of the '{@link #getIteratorVar() <em>Iterator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIteratorVar()
	 * @generated
	 * @ordered
	 */
	protected MIterator iteratorVar;

	/**
	 * This is true if the Iterator Var containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean iteratorVarESet;

	/**
	 * The cached value of the '{@link #getAccumulatorVar() <em>Accumulator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAccumulatorVar()
	 * @generated
	 * @ordered
	 */
	protected MAccumulator accumulatorVar;

	/**
	 * This is true if the Accumulator Var containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean accumulatorVarESet;

	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected MChainOrApplication expression;

	/**
	 * This is true if the Expression containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expressionESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #collectionOperatorAsCode <em>Collection Operator As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #collectionOperatorAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression collectionOperatorAsCodeexpressionsCollectionOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #collectionOperatorAsCode <em>Collection Operator As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #collectionOperatorAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression collectionOperatorAsCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultValue <em>Default Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #defaultValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultValueBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollection <em>Collection</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollection
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression collectionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MCollectionExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CollectionOperator getCollectionOperator() {
		return collectionOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollectionOperator(CollectionOperator newCollectionOperator) {
		CollectionOperator oldCollectionOperator = collectionOperator;
		collectionOperator = newCollectionOperator == null ? COLLECTION_OPERATOR_EDEFAULT : newCollectionOperator;
		boolean oldCollectionOperatorESet = collectionOperatorESet;
		collectionOperatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR, oldCollectionOperator,
					collectionOperator, !oldCollectionOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCollectionOperator() {
		CollectionOperator oldCollectionOperator = collectionOperator;
		boolean oldCollectionOperatorESet = collectionOperatorESet;
		collectionOperator = COLLECTION_OPERATOR_EDEFAULT;
		collectionOperatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR, oldCollectionOperator,
					COLLECTION_OPERATOR_EDEFAULT, oldCollectionOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCollectionOperator() {
		return collectionOperatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getCollection() {
		MAbstractExpression collection = basicGetCollection();
		return collection != null && collection.eIsProxy()
				? (MAbstractExpression) eResolveProxy((InternalEObject) collection) : collection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetCollection() {
		/**
		 * @OCL if eContainer().oclIsKindOf(MAbstractExpression)
		then eContainer().oclAsType(MAbstractExpression)
		else null 
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__COLLECTION;

		if (collectionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				collectionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MIterator getIteratorVar() {
		if (iteratorVar != null && iteratorVar.eIsProxy()) {
			InternalEObject oldIteratorVar = (InternalEObject) iteratorVar;
			iteratorVar = (MIterator) eResolveProxy(oldIteratorVar);
			if (iteratorVar != oldIteratorVar) {
				InternalEObject newIteratorVar = (InternalEObject) iteratorVar;
				NotificationChain msgs = oldIteratorVar.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null, null);
				if (newIteratorVar.eInternalContainer() == null) {
					msgs = newIteratorVar.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, oldIteratorVar, iteratorVar));
			}
		}
		return iteratorVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MIterator basicGetIteratorVar() {
		return iteratorVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetIteratorVar(MIterator newIteratorVar, NotificationChain msgs) {
		MIterator oldIteratorVar = iteratorVar;
		iteratorVar = newIteratorVar;
		boolean oldIteratorVarESet = iteratorVarESet;
		iteratorVarESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, oldIteratorVar, newIteratorVar,
					!oldIteratorVarESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIteratorVar(MIterator newIteratorVar) {
		if (newIteratorVar != iteratorVar) {
			NotificationChain msgs = null;
			if (iteratorVar != null)
				msgs = ((InternalEObject) iteratorVar).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null, msgs);
			if (newIteratorVar != null)
				msgs = ((InternalEObject) newIteratorVar).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null, msgs);
			msgs = basicSetIteratorVar(newIteratorVar, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldIteratorVarESet = iteratorVarESet;
			iteratorVarESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, newIteratorVar, newIteratorVar,
						!oldIteratorVarESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetIteratorVar(NotificationChain msgs) {
		MIterator oldIteratorVar = iteratorVar;
		iteratorVar = null;
		boolean oldIteratorVarESet = iteratorVarESet;
		iteratorVarESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, oldIteratorVar, null, oldIteratorVarESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIteratorVar() {
		if (iteratorVar != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) iteratorVar).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null, msgs);
			msgs = basicUnsetIteratorVar(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldIteratorVarESet = iteratorVarESet;
			iteratorVarESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR, null, null, oldIteratorVarESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIteratorVar() {
		return iteratorVarESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulator getAccumulatorVar() {
		if (accumulatorVar != null && accumulatorVar.eIsProxy()) {
			InternalEObject oldAccumulatorVar = (InternalEObject) accumulatorVar;
			accumulatorVar = (MAccumulator) eResolveProxy(oldAccumulatorVar);
			if (accumulatorVar != oldAccumulatorVar) {
				InternalEObject newAccumulatorVar = (InternalEObject) accumulatorVar;
				NotificationChain msgs = oldAccumulatorVar.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null,
						null);
				if (newAccumulatorVar.eInternalContainer() == null) {
					msgs = newAccumulatorVar.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, oldAccumulatorVar,
							accumulatorVar));
			}
		}
		return accumulatorVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAccumulator basicGetAccumulatorVar() {
		return accumulatorVar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAccumulatorVar(MAccumulator newAccumulatorVar, NotificationChain msgs) {
		MAccumulator oldAccumulatorVar = accumulatorVar;
		accumulatorVar = newAccumulatorVar;
		boolean oldAccumulatorVarESet = accumulatorVarESet;
		accumulatorVarESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, oldAccumulatorVar, newAccumulatorVar,
					!oldAccumulatorVarESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAccumulatorVar(MAccumulator newAccumulatorVar) {
		if (newAccumulatorVar != accumulatorVar) {
			NotificationChain msgs = null;
			if (accumulatorVar != null)
				msgs = ((InternalEObject) accumulatorVar).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null,
						msgs);
			if (newAccumulatorVar != null)
				msgs = ((InternalEObject) newAccumulatorVar).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null,
						msgs);
			msgs = basicSetAccumulatorVar(newAccumulatorVar, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAccumulatorVarESet = accumulatorVarESet;
			accumulatorVarESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, newAccumulatorVar,
						newAccumulatorVar, !oldAccumulatorVarESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAccumulatorVar(NotificationChain msgs) {
		MAccumulator oldAccumulatorVar = accumulatorVar;
		accumulatorVar = null;
		boolean oldAccumulatorVarESet = accumulatorVarESet;
		accumulatorVarESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, oldAccumulatorVar, null,
					oldAccumulatorVarESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAccumulatorVar() {
		if (accumulatorVar != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) accumulatorVar).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null, msgs);
			msgs = basicUnsetAccumulatorVar(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAccumulatorVarESet = accumulatorVarESet;
			accumulatorVarESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR, null, null, oldAccumulatorVarESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAccumulatorVar() {
		return accumulatorVarESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication getExpression() {
		if (expression != null && expression.eIsProxy()) {
			InternalEObject oldExpression = (InternalEObject) expression;
			expression = (MChainOrApplication) eResolveProxy(oldExpression);
			if (expression != oldExpression) {
				InternalEObject newExpression = (InternalEObject) expression;
				NotificationChain msgs = oldExpression.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, null);
				if (newExpression.eInternalContainer() == null) {
					msgs = newExpression.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, oldExpression, expression));
			}
		}
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication basicGetExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression(MChainOrApplication newExpression, NotificationChain msgs) {
		MChainOrApplication oldExpression = expression;
		expression = newExpression;
		boolean oldExpressionESet = expressionESet;
		expressionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, oldExpression, newExpression,
					!oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(MChainOrApplication newExpression) {
		if (newExpression != expression) {
			NotificationChain msgs = null;
			if (expression != null)
				msgs = ((InternalEObject) expression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, msgs);
			if (newExpression != null)
				msgs = ((InternalEObject) newExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, msgs);
			msgs = basicSetExpression(newExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, newExpression, newExpression,
						!oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpression(NotificationChain msgs) {
		MChainOrApplication oldExpression = expression;
		expression = null;
		boolean oldExpressionESet = expressionESet;
		expressionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, oldExpression, null, oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpression() {
		if (expression != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expression).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, msgs);
			msgs = basicUnsetExpression(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION, null, null, oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpression() {
		return expressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String collectionOperatorAsCode(CollectionOperator operator) {

		/**
		 * @OCL if operator= CollectionOperator::Collect then 'collect' else
		if operator= CollectionOperator::Select then 'select' else
		if operator= CollectionOperator::Iterate then 'iterate' else
		if operator= CollectionOperator::Closure then 'closure' else
		'ERROR' 
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION.getEOperations().get(0);
		if (collectionOperatorAsCodeexpressionsCollectionOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				collectionOperatorAsCodeexpressionsCollectionOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(collectionOperatorAsCodeexpressionsCollectionOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("operator", operator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String collectionOperatorAsCode() {

		/**
		 * @OCL collectionOperatorAsCode(collectionOperator)
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION.getEOperations().get(1);
		if (collectionOperatorAsCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				collectionOperatorAsCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(collectionOperatorAsCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain defaultValue() {

		/**
		 * @OCL Tuple{base=ExpressionBase::SelfObject}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION.getEOperations().get(2);
		if (defaultValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				defaultValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MChain) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
			return basicUnsetIteratorVar(msgs);
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
			return basicUnsetAccumulatorVar(msgs);
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			return basicUnsetExpression(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR:
			return getCollectionOperator();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION:
			if (resolve)
				return getCollection();
			return basicGetCollection();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
			if (resolve)
				return getIteratorVar();
			return basicGetIteratorVar();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
			if (resolve)
				return getAccumulatorVar();
			return basicGetAccumulatorVar();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			if (resolve)
				return getExpression();
			return basicGetExpression();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR:
			setCollectionOperator((CollectionOperator) newValue);
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
			setIteratorVar((MIterator) newValue);
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
			setAccumulatorVar((MAccumulator) newValue);
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			setExpression((MChainOrApplication) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR:
			unsetCollectionOperator();
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
			unsetIteratorVar();
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
			unsetAccumulatorVar();
			return;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			unsetExpression();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR:
			return isSetCollectionOperator();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__COLLECTION:
			return basicGetCollection() != null;
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ITERATOR_VAR:
			return isSetIteratorVar();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR:
			return isSetAccumulatorVar();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION__EXPRESSION:
			return isSetExpression();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MCOLLECTION_EXPRESSION___COLLECTION_OPERATOR_AS_CODE__COLLECTIONOPERATOR:
			return collectionOperatorAsCode((CollectionOperator) arguments.get(0));
		case ExpressionsPackage.MCOLLECTION_EXPRESSION___COLLECTION_OPERATOR_AS_CODE:
			return collectionOperatorAsCode();
		case ExpressionsPackage.MCOLLECTION_EXPRESSION___DEFAULT_VALUE:
			return defaultValue();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (collectionOperator: ");
		if (collectionOperatorESet)
			result.append(collectionOperator);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL collectionOperatorAsCode(self.collectionOperator)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel let prefix: String = if collection.oclIsUndefined() then ''
	else if collection.oclIsKindOf(MBaseChain) then ''
	else if collection.oclIsKindOf(MApplication) then ')'
	else if collection.oclIsKindOf(MIf) then 'ENDIF' else ''
	endif endif endif endif in 
	prefix.concat('->').concat(collectionOperatorAsCode()).concat('(')
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isComplexExpression /* if it returns a real collection, then it's a single OCL item, otherwise it's a let with if *\/
	aSingular
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsComplexExpression() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode if collection.oclIsUndefined() then 'MISSING COLLECTION'
	else
	
	let hasIterator: Boolean = not iteratorVar.oclIsUndefined() in
	let hasAccumulator: Boolean=not accumulatorVar.oclIsUndefined() in
	let  ct: String = aTypeAsOcl(aSelfObjectPackage, collection.aCalculatedOwnType, collection.aCalculatedOwnSimpleType , true) in
	
	let exp: String = (if collection.isComplexExpression then '(' else '' endif).concat(collection.asBasicCode).concat(if collection.isComplexExpression then ')' else '' endif).concat(
	'->').concat(collectionOperatorAsCode()).concat('(').concat(
	if hasIterator then iteratorVar.aName.concat(': ').concat(ct)
		else '' endif
	).concat(		
	if hasIterator and hasAccumulator then '; ' else '' endif
	).concat(		
	if hasAccumulator then 
	  accumulatorVar.aName.concat(': ').concat(aTypeAsOcl(aSelfObjectPackage, accumulatorVar.aCalculatedOwnType, accumulatorVar.aCalculatedOwnSimpleType , accumulatorVar.calculatedOwnSingular))
	  .concat(' = ').concat(if self.accumulatorVar.accDefinition.oclIsUndefined() then if self.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::String then '  \' ' '\' ' else if self.aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::Integer or self.aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::Real then '0' else 'null ' endif endif
	  else
	  self.accumulatorVar.accDefinition.asBasicCode
	  endif
	  ) 
	
	else '' endif
	).concat(		
	if hasIterator or hasAccumulator then ' | ' else '' endif
	). concat (
	if expression.oclIsUndefined() then 'true'
	else expression.asCode endif
	).concat(')') in
	
	if calculatedOwnSingular then
	'let c: '.concat(aTypeAsOcl(aSelfObjectPackage,  aClassifier,  aCalculatedOwnSimpleType , true)).concat(' = ').concat(exp).concat(' in if c.oclIsUndefined() then null else c endif')
	else 
	exp.concat('->asOrderedSet()->excluding(null)->asOrderedSet() ')	
	endif
	
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory if collectionOperator = CollectionOperator::Iterate then
	if iteratorVar.oclIsUndefined() then false
	else iteratorVar.aMandatory endif
	else false endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if collectionOperator = CollectionOperator::Iterate then
	if iteratorVar.oclIsUndefined() then false
	else iteratorVar.aSingular endif
	else false endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType let ct: acore::classifiers::ASimpleType = if collection.oclIsUndefined()
	then acore::classifiers::ASimpleType::None
	else collection.aCalculatedOwnSimpleType endif in
	let et: acore::classifiers::ASimpleType = if expression.oclIsUndefined()
	then acore::classifiers::ASimpleType::None
	else expression.aSimpleType endif in
	let at: acore::classifiers::ASimpleType = if accumulatorVar.oclIsUndefined()
	then acore::classifiers::ASimpleType::None
	else accumulatorVar.aSimpleType endif in
		
	if collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure
	then ct 
	else if collectionOperator = CollectionOperator::Collect 
	then  et 
	  else if collectionOperator = CollectionOperator::Iterate
		then  at
		  else acore::classifiers::ASimpleType::None endif
	endif 
	endif 
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aClassifier let ct: acore::classifiers::AClassifier = if collection.oclIsUndefined()
	then null
	else collection.aCalculatedOwnType endif in
	let et: acore::classifiers::AClassifier = if expression.oclIsUndefined()
	then null
	else expression.aClassifier endif in
	let at: acore::classifiers::AClassifier = if accumulatorVar.oclIsUndefined()
	then null
	else accumulatorVar.aClassifier endif in
		
	if collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure
	then ct 
	else if collectionOperator = CollectionOperator::Collect 
	then  et 
	  else if collectionOperator = CollectionOperator::Iterate
		then  at
		  else null endif
	endif 
	endif 
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAClassifier() {
		EClass eClass = (ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override

	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__ITERATOR_VAR,
				ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MCollectionExpressionImpl
