/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MChain;
import com.montages.mrules.expressions.MChainOrApplication;
import com.montages.mrules.expressions.MElseIf;
import com.montages.mrules.expressions.MThen;

import java.lang.reflect.InvocationTargetException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.resource.Resource.Internal;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.eclipse.ocl.util.TypeUtil;

import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.core.util.XoclMutlitypeComparisonUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MElse If</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MElseIfImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MElseIfImpl#getThenPart <em>Then Part</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MElseIfImpl extends MAbstractExpressionImpl implements MElseIf, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected MChainOrApplication condition;

	/**
	 * This is true if the Condition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean conditionESet;

	/**
	 * The cached value of the '{@link #getThenPart() <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenPart()
	 * @generated
	 * @ordered
	 */
	protected MThen thenPart;

	/**
	 * This is true if the Then Part containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean thenPartESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultValue <em>Default Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #defaultValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultValueBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MElseIfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MELSE_IF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication getCondition() {
		if (condition != null && condition.eIsProxy()) {
			InternalEObject oldCondition = (InternalEObject) condition;
			condition = (MChainOrApplication) eResolveProxy(oldCondition);
			if (condition != oldCondition) {
				InternalEObject newCondition = (InternalEObject) condition;
				NotificationChain msgs = oldCondition.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__CONDITION, null, null);
				if (newCondition.eInternalContainer() == null) {
					msgs = newCondition.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__CONDITION, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MELSE_IF__CONDITION,
							oldCondition, condition));
			}
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication basicGetCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(MChainOrApplication newCondition, NotificationChain msgs) {
		MChainOrApplication oldCondition = condition;
		condition = newCondition;
		boolean oldConditionESet = conditionESet;
		conditionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MELSE_IF__CONDITION, oldCondition, newCondition, !oldConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(MChainOrApplication newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject) condition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__CONDITION, null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject) newCondition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__CONDITION, null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldConditionESet = conditionESet;
			conditionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MELSE_IF__CONDITION,
						newCondition, newCondition, !oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetCondition(NotificationChain msgs) {
		MChainOrApplication oldCondition = condition;
		condition = null;
		boolean oldConditionESet = conditionESet;
		conditionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MELSE_IF__CONDITION, oldCondition, null, oldConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCondition() {
		if (condition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) condition).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__CONDITION, null, msgs);
			msgs = basicUnsetCondition(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldConditionESet = conditionESet;
			conditionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MELSE_IF__CONDITION, null,
						null, oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCondition() {
		return conditionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MThen getThenPart() {
		if (thenPart != null && thenPart.eIsProxy()) {
			InternalEObject oldThenPart = (InternalEObject) thenPart;
			thenPart = (MThen) eResolveProxy(oldThenPart);
			if (thenPart != oldThenPart) {
				InternalEObject newThenPart = (InternalEObject) thenPart;
				NotificationChain msgs = oldThenPart.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__THEN_PART, null, null);
				if (newThenPart.eInternalContainer() == null) {
					msgs = newThenPart.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__THEN_PART, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, ExpressionsPackage.MELSE_IF__THEN_PART,
							oldThenPart, thenPart));
			}
		}
		return thenPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MThen basicGetThenPart() {
		return thenPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenPart(MThen newThenPart, NotificationChain msgs) {
		MThen oldThenPart = thenPart;
		thenPart = newThenPart;
		boolean oldThenPartESet = thenPartESet;
		thenPartESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MELSE_IF__THEN_PART, oldThenPart, newThenPart, !oldThenPartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenPart(MThen newThenPart) {
		if (newThenPart != thenPart) {
			NotificationChain msgs = null;
			if (thenPart != null)
				msgs = ((InternalEObject) thenPart).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__THEN_PART, null, msgs);
			if (newThenPart != null)
				msgs = ((InternalEObject) newThenPart).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__THEN_PART, null, msgs);
			msgs = basicSetThenPart(newThenPart, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldThenPartESet = thenPartESet;
			thenPartESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MELSE_IF__THEN_PART,
						newThenPart, newThenPart, !oldThenPartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetThenPart(NotificationChain msgs) {
		MThen oldThenPart = thenPart;
		thenPart = null;
		boolean oldThenPartESet = thenPartESet;
		thenPartESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MELSE_IF__THEN_PART, oldThenPart, null, oldThenPartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetThenPart() {
		if (thenPart != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) thenPart).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ExpressionsPackage.MELSE_IF__THEN_PART, null, msgs);
			msgs = basicUnsetThenPart(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldThenPartESet = thenPartESet;
			thenPartESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, ExpressionsPackage.MELSE_IF__THEN_PART, null,
						null, oldThenPartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetThenPart() {
		return thenPartESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain defaultValue() {

		/**
		 * @OCL Tuple{base=ExpressionBase::SelfObject}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EOperation eOperation = ExpressionsPackage.Literals.MELSE_IF.getEOperations().get(0);
		if (defaultValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				defaultValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MELSE_IF, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MELSE_IF,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MChain) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MELSE_IF__CONDITION:
			return basicUnsetCondition(msgs);
		case ExpressionsPackage.MELSE_IF__THEN_PART:
			return basicUnsetThenPart(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MELSE_IF__CONDITION:
			if (resolve)
				return getCondition();
			return basicGetCondition();
		case ExpressionsPackage.MELSE_IF__THEN_PART:
			if (resolve)
				return getThenPart();
			return basicGetThenPart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MELSE_IF__CONDITION:
			setCondition((MChainOrApplication) newValue);
			return;
		case ExpressionsPackage.MELSE_IF__THEN_PART:
			setThenPart((MThen) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MELSE_IF__CONDITION:
			unsetCondition();
			return;
		case ExpressionsPackage.MELSE_IF__THEN_PART:
			unsetThenPart();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MELSE_IF__CONDITION:
			return isSetCondition();
		case ExpressionsPackage.MELSE_IF__THEN_PART:
			return isSetThenPart();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MELSE_IF___DEFAULT_VALUE:
			return defaultValue();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 'else if '.concat(
	if condition.oclIsUndefined() then 'MISSING CONDITION'
	else condition.getShortCode() endif
	).concat(' ').concat(
	if thenPart.oclIsUndefined() then 'MISSING THEN'
	else thenPart.getShortCode() endif
	)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MELSE_IF;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel let kind: String = 'ELSE IF' in
	kind
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory /* TODO: better typing and verification *\/
	if thenPart.oclIsUndefined() then true
	else thenPart.aMandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType /* TODO: better typing and verification *\/
	if thenPart.oclIsUndefined() then null
	else thenPart.aClassifier endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular /* TODO: better typing and verification *\/
	if thenPart.oclIsUndefined() then true
	else thenPart.aSingular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnSimpleType /* TODO: better typing and verification *\/
	if thenPart.oclIsUndefined() then acore::classifiers::ASimpleType::None
	else thenPart.aSimpleType endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ASimpleType getACalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASimpleType) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode 'else if ('.concat(
	if condition.oclIsUndefined() then 'MISSING IF CONDITION'
	else condition.asCode endif
	).concat(')=true ').concat(
	if thenPart.oclIsUndefined() then 'MISSING THEN'
	else thenPart.asCode endif
	)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MELSE_IF);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override

	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MELSE_IF__CONDITION, ExpressionsPackage.Literals.MELSE_IF__THEN_PART };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, ExpressionsPackage.Literals.MELSE_IF,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MElseIfImpl
