/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;

import com.montages.acore.values.AObject;

import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MObjectReferenceLet;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MObject Reference Let</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MObjectReferenceLetImpl#getAObjectType <em>AObject Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MObjectReferenceLetImpl#getAConstant1 <em>AConstant1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MObjectReferenceLetImpl#getAConstant2 <em>AConstant2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MObjectReferenceLetImpl#getAConstant3 <em>AConstant3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MObjectReferenceLetImpl extends MConstantLetImpl implements MObjectReferenceLet {
	/**
	 * The cached value of the '{@link #getAObjectType() <em>AObject Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObjectType()
	 * @generated
	 * @ordered
	 */
	protected AClassType aObjectType;

	/**
	 * This is true if the AObject Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aObjectTypeESet;

	/**
	 * The cached value of the '{@link #getAConstant1() <em>AConstant1</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant1()
	 * @generated
	 * @ordered
	 */
	protected EList<AObject> aConstant1;

	/**
	 * The cached value of the '{@link #getAConstant2() <em>AConstant2</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant2()
	 * @generated
	 * @ordered
	 */
	protected EList<AObject> aConstant2;

	/**
	 * The cached value of the '{@link #getAConstant3() <em>AConstant3</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant3()
	 * @generated
	 * @ordered
	 */
	protected EList<AObject> aConstant3;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getAConstant1 <em>AConstant1</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant1
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aConstant1ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getAConstant2 <em>AConstant2</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant2
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aConstant2ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getAConstant3 <em>AConstant3</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAConstant3
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression aConstant3ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectReferenceLetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType getAObjectType() {
		if (aObjectType != null && aObjectType.eIsProxy()) {
			InternalEObject oldAObjectType = (InternalEObject) aObjectType;
			aObjectType = (AClassType) eResolveProxy(oldAObjectType);
			if (aObjectType != oldAObjectType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE, oldAObjectType, aObjectType));
			}
		}
		return aObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType basicGetAObjectType() {
		return aObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAObjectType(AClassType newAObjectType) {
		AClassType oldAObjectType = aObjectType;
		aObjectType = newAObjectType;
		boolean oldAObjectTypeESet = aObjectTypeESet;
		aObjectTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE, oldAObjectType, aObjectType,
					!oldAObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAObjectType() {
		AClassType oldAObjectType = aObjectType;
		boolean oldAObjectTypeESet = aObjectTypeESet;
		aObjectType = null;
		aObjectTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE, oldAObjectType, null, oldAObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAObjectType() {
		return aObjectTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAConstant1() {
		if (aConstant1 == null) {
			aConstant1 = new EObjectResolvingEList.Unsettable<AObject>(AObject.class, this,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT1);
		}
		return aConstant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant1() {
		if (aConstant1 != null)
			((InternalEList.Unsettable<?>) aConstant1).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant1() {
		return aConstant1 != null && ((InternalEList.Unsettable<?>) aConstant1).isSet();
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>AConstant1</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if aObjectType.oclIsUndefined() then false
	else trg.aClassifier = aObjectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalAConstant1ChoiceConstraint(AObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (aConstant1ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT1;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aConstant1ChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "AConstant1ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant1ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant1ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAConstant2() {
		if (aConstant2 == null) {
			aConstant2 = new EObjectResolvingEList.Unsettable<AObject>(AObject.class, this,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT2);
		}
		return aConstant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant2() {
		if (aConstant2 != null)
			((InternalEList.Unsettable<?>) aConstant2).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant2() {
		return aConstant2 != null && ((InternalEList.Unsettable<?>) aConstant2).isSet();
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>AConstant2</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if aObjectType.oclIsUndefined() then false
	else trg.aClassifier = aObjectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalAConstant2ChoiceConstraint(AObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (aConstant2ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT2;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aConstant2ChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "AConstant2ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant2ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant2ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAConstant3() {
		if (aConstant3 == null) {
			aConstant3 = new EObjectResolvingEList.Unsettable<AObject>(AObject.class, this,
					ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT3);
		}
		return aConstant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAConstant3() {
		if (aConstant3 != null)
			((InternalEList.Unsettable<?>) aConstant3).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAConstant3() {
		return aConstant3 != null && ((InternalEList.Unsettable<?>) aConstant3).isSet();
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>AConstant3</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type AObject
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if aObjectType.oclIsUndefined() then false
	else trg.aClassifier = aObjectType endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalAConstant3ChoiceConstraint(AObject trg) {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (aConstant3ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT3;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				aConstant3ChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "AConstant3ChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(aConstant3ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "AConstant3ChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE:
			if (resolve)
				return getAObjectType();
			return basicGetAObjectType();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT1:
			return getAConstant1();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT2:
			return getAConstant2();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT3:
			return getAConstant3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE:
			setAObjectType((AClassType) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT1:
			getAConstant1().clear();
			getAConstant1().addAll((Collection<? extends AObject>) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT2:
			getAConstant2().clear();
			getAConstant2().addAll((Collection<? extends AObject>) newValue);
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT3:
			getAConstant3().clear();
			getAConstant3().addAll((Collection<? extends AObject>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE:
			unsetAObjectType();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT1:
			unsetAConstant1();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT2:
			unsetAConstant2();
			return;
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT3:
			unsetAConstant3();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__AOBJECT_TYPE:
			return isSetAObjectType();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT1:
			return isSetAConstant1();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT2:
			return isSetAConstant2();
		case ExpressionsPackage.MOBJECT_REFERENCE_LET__ACONSTANT3:
			return isSetAConstant3();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL asCode
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Object Reference'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode null /*let t: String =  if aObjectType.oclIsUndefined() then 'MISSING TYPE' else
	aObjectType.aName endif in
	let a: String = if aConstant1.oclIsUndefined() then '' else '<obj '.concat(aConstant1.aLabel).concat('>') endif in
	let b: String = if aConstant2.oclIsUndefined() then '' else '<obj '.concat(aConstant2.aLabel).concat('>') endif in
	let c: String = if aConstant3.oclIsUndefined() then '' else '<obj '.concat(aConstant3.aLabel).concat('>')  endif in
	
	if aSingular
	then a.concat(b).concat(c)
	else asSetString(
	'<obj '.concat(aConstant1.aLabel).concat('>'),
	'<obj '.concat(aConstant2.aLabel).concat('>'),
	'<obj '.concat(aConstant3.aLabel).concat('>')
	)
	endif*\/
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory null /*let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in
	let b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in
	let c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in
	
	(a+b+c) > 0*\/
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular null /*
	let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in
	let b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in
	let c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in
	
	(a+b+c) <= 1
	*\/
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aCalculatedOwnType aObjectType
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetACalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MObjectReferenceLetImpl
