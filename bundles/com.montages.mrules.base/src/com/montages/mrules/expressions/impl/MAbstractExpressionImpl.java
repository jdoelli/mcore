/**
 */

package com.montages.mrules.expressions.impl;

import com.montages.acore.AComponent;
import com.montages.acore.APackage;

import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ATyped;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractBaseDefinition;
import com.montages.mrules.expressions.MAbstractExpression;
import com.montages.mrules.expressions.MAccumulatorBaseDefinition;
import com.montages.mrules.expressions.MBaseDefinition;
import com.montages.mrules.expressions.MCollectionExpression;
import com.montages.mrules.expressions.MContainerBaseDefinition;
import com.montages.mrules.expressions.MIteratorBaseDefinition;
import com.montages.mrules.expressions.MLiteralConstantBaseDefinition;
import com.montages.mrules.expressions.MNumberBaseDefinition;
import com.montages.mrules.expressions.MObjectBaseDefinition;
import com.montages.mrules.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mrules.expressions.MParameterBaseDefinition;
import com.montages.mrules.expressions.MSelfBaseDefinition;
import com.montages.mrules.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mrules.expressions.MSourceBaseDefinition;
import com.montages.mrules.expressions.MTargetBaseDefinition;
import com.montages.mrules.expressions.MVariableBaseDefinition;

import com.montages.mrules.impl.MRulesElementImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATPackageUri <em>AT Package Uri</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATClassifierName <em>AT Classifier Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATFeatureName <em>AT Feature Name</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATPackage <em>AT Package</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATClassifier <em>AT Classifier</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATFeature <em>AT Feature</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATCoreAStringClass <em>AT Core AString Class</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAMandatory <em>AMandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASingular <em>ASingular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATypeLabel <em>AType Label</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASimpleType <em>ASimple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAsCode <em>As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAsBasicCode <em>As Basic Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getCollector <em>Collector</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getEntireScope <em>Entire Scope</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeBase <em>Scope Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeSelf <em>Scope Self</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeTrg <em>Scope Trg</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeObj <em>Scope Obj</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeLiteralConstants <em>Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeVariables <em>Scope Variables</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeIterator <em>Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeAccumulator <em>Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeParameters <em>Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeContainerBase <em>Scope Container Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeNumberBase <em>Scope Number Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getScopeSource <em>Scope Source</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalEntireScope <em>Local Entire Scope</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeBase <em>Local Scope Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeSelf <em>Local Scope Self</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeTrg <em>Local Scope Trg</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeObj <em>Local Scope Obj</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeSource <em>Local Scope Source</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeVariables <em>Local Scope Variables</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeIterator <em>Local Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeAccumulator <em>Local Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeParameters <em>Local Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getLocalScopeContainer <em>Local Scope Container</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getContainingAnnotation <em>Containing Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getContainingExpression <em>Containing Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getIsComplexExpression <em>Is Complex Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getIsSubExpression <em>Is Sub Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getACalculatedOwnType <em>ACalculated Own Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnSingular <em>Calculated Own Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASelfObjectType <em>ASelf Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASelfObjectPackage <em>ASelf Object Package</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATargetObjectType <em>ATarget Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getATargetSimpleType <em>ATarget Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAObjectObjectType <em>AObject Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAExpectedReturnType <em>AExpected Return Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getAExpectedReturnSimpleType <em>AExpected Return Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getIsReturnValueSingular <em>Is Return Value Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASrcObjectType <em>ASrc Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.impl.MAbstractExpressionImpl#getASrcObjectSimpleType <em>ASrc Object Simple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractExpressionImpl extends MRulesElementImpl implements MAbstractExpression {
	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getATPackageUri() <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackageUri()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_PACKAGE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATPackageUri() <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackageUri()
	 * @generated
	 * @ordered
	 */
	protected String aTPackageUri = AT_PACKAGE_URI_EDEFAULT;

	/**
	 * This is true if the AT Package Uri attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTPackageUriESet;

	/**
	 * The default value of the '{@link #getATClassifierName() <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifierName()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_CLASSIFIER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATClassifierName() <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifierName()
	 * @generated
	 * @ordered
	 */
	protected String aTClassifierName = AT_CLASSIFIER_NAME_EDEFAULT;

	/**
	 * This is true if the AT Classifier Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTClassifierNameESet;

	/**
	 * The default value of the '{@link #getATFeatureName() <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeatureName()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_FEATURE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATFeatureName() <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeatureName()
	 * @generated
	 * @ordered
	 */
	protected String aTFeatureName = AT_FEATURE_NAME_EDEFAULT;

	/**
	 * This is true if the AT Feature Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTFeatureNameESet;

	/**
	 * The default value of the '{@link #getAMandatory() <em>AMandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AMANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASingular() <em>ASingular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getATypeLabel() <em>AType Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ATYPE_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedTypeConstant() <em>AUndefined Type Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_TYPE_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASimpleType() <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ASIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The cached value of the '{@link #getASimpleType() <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType()
	 * @generated
	 * @ordered
	 */
	protected ASimpleType aSimpleType = ASIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the ASimple Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aSimpleTypeESet;

	/**
	 * The default value of the '{@link #getAsCode() <em>As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAsBasicCode() <em>As Basic Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_BASIC_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsComplexExpression() <em>Is Complex Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_COMPLEX_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsSubExpression() <em>Is Sub Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_SUB_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnMandatory() <em>Calculated Own Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnSingular() <em>Calculated Own Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getACalculatedOwnSimpleType() <em>ACalculated Own Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ACALCULATED_OWN_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getATargetSimpleType() <em>ATarget Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ATARGET_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getAExpectedReturnSimpleType() <em>AExpected Return Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType AEXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getIsReturnValueMandatory() <em>Is Return Value Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReturnValueSingular() <em>Is Return Value Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASrcObjectSimpleType() <em>ASrc Object Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASrcObjectSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ASRC_OBJECT_SIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The parsed OCL expression for the body of the '{@link #getShortCode <em>Get Short Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getShortCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getScope <em>Get Scope</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getScopeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aTypeAsOcl <em>AType As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aTypeAsOcl
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentLevel <em>AIndent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentationSpaces <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentationSpaces <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aStringOrMissing <em>AString Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aStringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aStringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aStringIsEmpty <em>AString Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aStringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aStringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aListOfStringToStringWithSeparator <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aListOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aListOfStringToStringWithSeparator <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aListOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aListOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aPackageFromUri <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aPackageFromUri
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aPackageFromUriecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aClassifierFromUriAndName <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aClassifierFromUriAndName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aFeatureFromUriAndNames <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aFeatureFromUriAndNames
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAStringClass <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAStringClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAStringClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreARealClass <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreARealClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreARealClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAIntegerClass <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAIntegerClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAIntegerClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAObjectClass <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAObjectClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAObjectClassBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase <em>AKind Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATPackage <em>AT Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATClassifier <em>AT Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATFeature <em>AT Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATCoreAStringClass <em>AT Core AString Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATCoreAStringClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTCoreAStringClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATypeLabel <em>AType Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTypeLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedTypeConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsCode <em>As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEntireScope <em>Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression entireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeBase <em>Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSelf <em>Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeTrg <em>Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObj <em>Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeLiteralConstants <em>Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeVariables <em>Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeIterator <em>Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeAccumulator <em>Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeParameters <em>Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeContainerBase <em>Scope Container Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeContainerBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeContainerBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeNumberBase <em>Scope Number Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeNumberBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeNumberBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSource <em>Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalEntireScope <em>Local Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localEntireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeBase <em>Local Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSelf <em>Local Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeTrg <em>Local Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObj <em>Local Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSource <em>Local Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeVariables <em>Local Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeIterator <em>Local Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeAccumulator <em>Local Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeParameters <em>Local Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeNumberBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeNumberBaseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeContainer <em>Local Scope Container</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeContainer
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeContainerDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAnnotation <em>Containing Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingExpression <em>Containing Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsSubExpression <em>Is Sub Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isSubExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnType <em>ACalculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getACalculatedOwnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aCalculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASelfObjectType <em>ASelf Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASelfObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSelfObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASelfObjectPackage <em>ASelf Object Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASelfObjectPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSelfObjectPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATargetObjectType <em>ATarget Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTargetObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATargetSimpleType <em>ATarget Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATargetSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTargetSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAObjectObjectType <em>AObject Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObjectObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aObjectObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnType <em>AExpected Return Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aExpectedReturnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnSimpleType <em>AExpected Return Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aExpectedReturnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueSingular <em>Is Return Value Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASrcObjectType <em>ASrc Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASrcObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSrcObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASrcObjectSimpleType <em>ASrc Object Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASrcObjectSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSrcObjectSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMandatory <em>AMandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASingular <em>ASingular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASimpleType <em>ASimple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aSimpleTypeDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let e1: String = aIndentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent getAContainingComponent() {
		AComponent aContainingComponent = basicGetAContainingComponent();
		return aContainingComponent != null && aContainingComponent.eIsProxy()
				? (AComponent) eResolveProxy((InternalEObject) aContainingComponent) : aContainingComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent basicGetAContainingComponent() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(acore::AComponent)
		then self.eContainer().oclAsType(acore::AComponent)
		else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)
		then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent
		else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AComponent result = (AComponent) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATPackageUri() {
		return aTPackageUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATPackageUri(String newATPackageUri) {
		String oldATPackageUri = aTPackageUri;
		aTPackageUri = newATPackageUri;
		boolean oldATPackageUriESet = aTPackageUriESet;
		aTPackageUriESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI, oldATPackageUri, aTPackageUri,
					!oldATPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATPackageUri() {
		String oldATPackageUri = aTPackageUri;
		boolean oldATPackageUriESet = aTPackageUriESet;
		aTPackageUri = AT_PACKAGE_URI_EDEFAULT;
		aTPackageUriESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI, oldATPackageUri, AT_PACKAGE_URI_EDEFAULT,
					oldATPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATPackageUri() {
		return aTPackageUriESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATClassifierName() {
		return aTClassifierName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATClassifierName(String newATClassifierName) {
		String oldATClassifierName = aTClassifierName;
		aTClassifierName = newATClassifierName;
		boolean oldATClassifierNameESet = aTClassifierNameESet;
		aTClassifierNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME, oldATClassifierName, aTClassifierName,
					!oldATClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATClassifierName() {
		String oldATClassifierName = aTClassifierName;
		boolean oldATClassifierNameESet = aTClassifierNameESet;
		aTClassifierName = AT_CLASSIFIER_NAME_EDEFAULT;
		aTClassifierNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME, oldATClassifierName,
					AT_CLASSIFIER_NAME_EDEFAULT, oldATClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATClassifierName() {
		return aTClassifierNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATFeatureName() {
		return aTFeatureName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATFeatureName(String newATFeatureName) {
		String oldATFeatureName = aTFeatureName;
		aTFeatureName = newATFeatureName;
		boolean oldATFeatureNameESet = aTFeatureNameESet;
		aTFeatureNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME, oldATFeatureName, aTFeatureName,
					!oldATFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATFeatureName() {
		String oldATFeatureName = aTFeatureName;
		boolean oldATFeatureNameESet = aTFeatureNameESet;
		aTFeatureName = AT_FEATURE_NAME_EDEFAULT;
		aTFeatureNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME, oldATFeatureName,
					AT_FEATURE_NAME_EDEFAULT, oldATFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATFeatureName() {
		return aTFeatureNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getATPackage() {
		APackage aTPackage = basicGetATPackage();
		return aTPackage != null && aTPackage.eIsProxy() ? (APackage) eResolveProxy((InternalEObject) aTPackage)
				: aTPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetATPackage() {
		/**
		 * @OCL let p: String = aTPackageUri in
		aPackageFromUri(p)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE;

		if (aTPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getATClassifier() {
		AClassifier aTClassifier = basicGetATClassifier();
		return aTClassifier != null && aTClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aTClassifier) : aTClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetATClassifier() {
		/**
		 * @OCL let p: String = aTPackageUri in
		let c: String = aTClassifierName in
		aClassifierFromUriAndName(p, c)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER;

		if (aTClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getATFeature() {
		AFeature aTFeature = basicGetATFeature();
		return aTFeature != null && aTFeature.eIsProxy() ? (AFeature) eResolveProxy((InternalEObject) aTFeature)
				: aTFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetATFeature() {
		/**
		 * @OCL let p: String = aTPackageUri in
		let c: String = aTClassifierName in
		let f: String = aTFeatureName in
		aFeatureFromUriAndNames(p, c, f)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_FEATURE;

		if (aTFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getATCoreAStringClass() {
		AClassifier aTCoreAStringClass = basicGetATCoreAStringClass();
		return aTCoreAStringClass != null && aTCoreAStringClass.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aTCoreAStringClass) : aTCoreAStringClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetATCoreAStringClass() {
		/**
		 * @OCL aCoreAStringClass()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS;

		if (aTCoreAStringClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTCoreAStringClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTCoreAStringClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAClassifier() {
		AClassifier aClassifier = basicGetAClassifier();
		return aClassifier != null && aClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aClassifier) : aClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAClassifier() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__AMANDATORY;

		if (aMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getASingular() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.ATYPED__ASINGULAR;

		if (aSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATypeLabel() {
		/**
		 * @OCL if (let e0: Boolean = aClassifier = null in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in 
		if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif
		else (let e0: String = if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif.concat('*') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__ATYPE_LABEL;

		if (aTypeLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTypeLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTypeLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedTypeConstant() {
		/**
		 * @OCL '<A Type Is Undefined>'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT;

		if (aUndefinedTypeConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedTypeConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedTypeConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getASimpleType() {
		return aSimpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setASimpleType(ASimpleType newASimpleType) {
		ASimpleType oldASimpleType = aSimpleType;
		aSimpleType = newASimpleType == null ? ASIMPLE_TYPE_EDEFAULT : newASimpleType;
		boolean oldASimpleTypeESet = aSimpleTypeESet;
		aSimpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE,
					oldASimpleType, aSimpleType, !oldASimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetASimpleType() {
		ASimpleType oldASimpleType = aSimpleType;
		boolean oldASimpleTypeESet = aSimpleTypeESet;
		aSimpleType = ASIMPLE_TYPE_EDEFAULT;
		aSimpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE, oldASimpleType, ASIMPLE_TYPE_EDEFAULT,
					oldASimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetASimpleType() {
		return aSimpleTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsCode() {
		/**
		 * @OCL if (let e: Boolean = collector.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then asBasicCode
		else if collector.oclIsUndefined()
		then null
		else collector.asBasicCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE;

		if (asCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				asCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsBasicCode() {
		/**
		 * @OCL eClass().name.concat(' : <?>')
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getCollector() {
		MCollectionExpression collector = basicGetCollector();
		return collector != null && collector.eIsProxy()
				? (MCollectionExpression) eResolveProxy((InternalEObject) collector) : collector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetCollector() {
		/**
		 * @OCL let nl: mrules::expressions::MCollectionExpression = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MCollectionExpression result = (MCollectionExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getEntireScope() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localEntireScope 
		else /* Note this is necessary for some cases like scopeIterator that otherwise won't work *\/ 
		scopeBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition))->union(
		scopeSelf->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeTrg->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeObj->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeSimpleTypeConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeLiteralConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeObjectReferenceConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeVariables->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeParameters->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeIterator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeAccumulator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeNumberBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeContainerBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		scopeSource->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE;

		if (entireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				entireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(entireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getScopeBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeBase 
		else containingExpression.scopeBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE;

		if (scopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getScopeSelf() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSelf
		else containingExpression.scopeSelf
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF;

		if (scopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getScopeTrg() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeTrg
		else containingExpression.scopeTrg
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG;

		if (scopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getScopeObj() {
		/**
		 * @OCL --todo: only for updates...
		if containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ;

		if (scopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getScopeSimpleTypeConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSimpleTypeConstants
		else containingExpression.scopeSimpleTypeConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (scopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeSimpleTypeConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getScopeLiteralConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeLiteralConstants
		else containingExpression.scopeLiteralConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS;

		if (scopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeLiteralConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getScopeObjectReferenceConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeObjectReferenceConstants
		else containingExpression.scopeObjectReferenceConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (scopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeObjectReferenceConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getScopeVariables() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeVariables
		else containingExpression.scopeVariables
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES;

		if (scopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getScopeIterator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeIterator
		else containingExpression.scopeIterator->union(localScopeIterator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR;

		if (scopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getScopeAccumulator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeAccumulator
		else containingExpression.scopeAccumulator->union(localScopeAccumulator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR;

		if (scopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getScopeParameters() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeParameters
		else containingExpression.scopeParameters
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS;

		if (scopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getScopeContainerBase() {
		/**
		 * @OCL localScopeContainer
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE;

		if (scopeContainerBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeContainerBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeContainerBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getScopeNumberBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then self.localScopeNumberBaseDefinition
		else containingExpression.scopeNumberBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE;

		if (scopeNumberBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeNumberBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeNumberBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getScopeSource() {
		/**
		 * @OCL 
		if containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE;

		if (scopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				scopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getLocalEntireScope() {
		/**
		 * @OCL localScopeBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition))->union(
		localScopeSelf->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeObj->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeTrg->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeSimpleTypeConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeLiteralConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeObjectReferenceConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeVariables->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeParameters->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeIterator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeAccumulator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeNumberBaseDefinition->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeContainer->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(
		localScopeSource->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE;

		if (localEntireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localEntireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localEntireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getLocalScopeBase() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mrules::expressions::ExpressionBase::NullValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::FalseValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::TrueValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::EmptyStringValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::EmptyCollection}  
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE;

		if (localScopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getLocalScopeSelf() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF;

		if (localScopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getLocalScopeTrg() {
		/**
		 * @OCL if aTargetObjectType.oclIsUndefined() and self.aTargetSimpleType=acore::classifiers::ASimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG;

		if (localScopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getLocalScopeObj() {
		/**
		 * @OCL if aObjectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ;

		if (localScopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getLocalScopeSource() {
		/**
		 * @OCL if aSrcObjectType.oclIsUndefined() and self.aSrcObjectSimpleType=acore::classifiers::ASimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE;

		if (localScopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getLocalScopeSimpleTypeConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation)
		then let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in*\/
		let a: mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mrules::expressions::MSimpleTypeConstantLet) and (not i.aName.oclIsUndefined()) and (i.aName<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (localScopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeSimpleTypeConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getLocalScopeLiteralConstants() {
		/**
		 * @OCL /* if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation)
		then let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in *\/
		let a: mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mrules::expressions::MLiteralLet) and (not i.aName.oclIsUndefined()) and (i.aName<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/*
		else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS;

		if (localScopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeLiteralConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getLocalScopeObjectReferenceConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation)
		then let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in *\/
		let a: mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		
		a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mrules::expressions::MObjectReferenceLet) and (not i.aName.oclIsUndefined()) and (i.aName<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (localScopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeObjectReferenceConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getLocalScopeVariables() {
		/**
		 * @OCL let a: mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined()
		then  OrderedSet{} 
		else
		a.namedExpression->iterate (i: mrules::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mrules::expressions::MNamedExpression))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if  (not i.aName.oclIsUndefined()) and (i.aName<>'')
		then r->append(Tuple{namedExpression=i}) 
		else r 
		endif
		endif
		) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES;

		if (localScopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getLocalScopeIterator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mrules::expressions::MCollectionExpression)
		then let c: mrules::expressions::MCollectionExpression = eContainer().oclAsType(mrules::expressions::MCollectionExpression) in
		if c.iteratorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{iterator=c.iteratorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR;

		if (localScopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getLocalScopeAccumulator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mrules::expressions::MCollectionExpression)
		then let c: mrules::expressions::MCollectionExpression = eContainer().oclAsType(mrules::expressions::MCollectionExpression) in
		if c.accumulatorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR;

		if (localScopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getLocalScopeParameters() {
		/**
		 * @OCL if self.containingAnnotation=null 
		then OrderedSet{}
		else 
		let e:acore::abstractions::AAnnotatable= self.containingAnnotation.aAnnotated in
		if e=null 
		then OrderedSet{}
		else if not e.oclIsKindOf(acore::classifiers::AOperation)
		then OrderedSet{}
		else e.oclAsType(acore::classifiers::AOperation).aParameter
		 ->iterate (i: acore::classifiers::AParameter; r: OrderedSet(Tuple(aParameter:acore::classifiers::AParameter))=OrderedSet{} 
		                |r->append(Tuple{aParameter=i}) )
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS;

		if (localScopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getLocalScopeNumberBaseDefinition() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mrules::expressions::ExpressionBase::ZeroValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::OneValue},
		Tuple{expressionBase = mrules::expressions::ExpressionBase::MinusOneValue}
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

		if (localScopeNumberBaseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeNumberBaseDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeNumberBaseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getLocalScopeContainer() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER;

		if (localScopeContainerDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				localScopeContainerDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeContainerDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRuleAnnotation getContainingAnnotation() {
		MRuleAnnotation containingAnnotation = basicGetContainingAnnotation();
		return containingAnnotation != null && containingAnnotation.eIsProxy()
				? (MRuleAnnotation) eResolveProxy((InternalEObject) containingAnnotation) : containingAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRuleAnnotation basicGetContainingAnnotation() {
		/**
		 * @OCL -- expressions can now be as well annotations, so the "containing" annotation can be self.
		if self.oclIsKindOf(mrules::MRuleAnnotation) 
		then self.oclAsType(mrules::MRuleAnnotation) 
		else if eContainer().oclIsUndefined() 
		then null 
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation) 
		then eContainer().oclAsType(mrules::MRuleAnnotation) 
		else if eContainer().oclIsTypeOf(mrules::expressions::MNamedExpression) 
		then self.eContainer().oclAsType(mrules::expressions::MNamedExpression).containingAnnotation
		else if eContainer().oclIsKindOf(mrules::expressions::MNewObjectFeatureValue) 
		then eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue)
		.eContainer().oclAsType(mrules::expressions::MNewObject).containingAnnotation
		else if containingExpression.oclIsUndefined() 
		then null 
		else containingExpression.containingAnnotation endif endif endif endif endif	endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION;

		if (containingAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				containingAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MRuleAnnotation result = (MRuleAnnotation) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getContainingExpression() {
		MAbstractExpression containingExpression = basicGetContainingExpression();
		return containingExpression != null && containingExpression.eIsProxy()
				? (MAbstractExpression) eResolveProxy((InternalEObject) containingExpression) : containingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetContainingExpression() {
		/**
		 * @OCL if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression)
		then eContainer().oclAsType(mrules::expressions::MAbstractExpression)
		
		else null endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION;

		if (containingExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				containingExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsComplexExpression() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsSubExpression() {
		/**
		 * @OCL if containingExpression.oclIsUndefined() then false
		else not (containingExpression.oclIsKindOf(mrules::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mrules::expressions::MNamedExpression)) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION;

		if (isSubExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isSubExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isSubExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getACalculatedOwnType() {
		AClassifier aCalculatedOwnType = basicGetACalculatedOwnType();
		return aCalculatedOwnType != null && aCalculatedOwnType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aCalculatedOwnType) : aCalculatedOwnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetACalculatedOwnType() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE;

		if (aCalculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aCalculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnSingular() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getACalculatedOwnSimpleType() {
		/**
		 * @OCL acore::classifiers::ASimpleType::Boolean
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE;

		if (aCalculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aCalculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aCalculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType getASelfObjectType() {
		AClassType aSelfObjectType = basicGetASelfObjectType();
		return aSelfObjectType != null && aSelfObjectType.eIsProxy()
				? (AClassType) eResolveProxy((InternalEObject) aSelfObjectType) : aSelfObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType basicGetASelfObjectType() {
		/**
		 * @OCL let a:mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() or a = self then null
		else a.aSelfObjectType endif
		
		/*if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation) 
		then eContainer().oclAsType(mrules::MRuleAnnotation).selfObjectTypeOfAnnotation
		--else if self.oclsKindOf(mrules::MRuleAnnotation)
		--   then self.oclAsType(mrules::MRuleAnnotation).selfObjectTypeOfAnnotation
		
		--else  if  eContainer().oclIsTypeOf(mrules::expressions::MAbstractExpression) then
		-- eContainer().oclAsType(mrules::expressions::MAbstractExpression).selfObjectType
		else if eContainer().oclIsKindOf(mrules::expressions::MNewObjectFeatureValue)
		then 
		if eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mrules::expressions::MNewObjecct)
		then  eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mrules::expressions::MNewObjecct).selfObjectType
		else 
		null
		endif
		else eContainer().oclAsType(mrules::expressions::MAbstractExpression).selfObjectType
		endif
		--endif
		endif
		endif 
		--endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_TYPE;

		if (aSelfObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSelfObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSelfObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassType result = (AClassType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getASelfObjectPackage() {
		APackage aSelfObjectPackage = basicGetASelfObjectPackage();
		return aSelfObjectPackage != null && aSelfObjectPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aSelfObjectPackage) : aSelfObjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetASelfObjectPackage() {
		/**
		 * @OCL if (let e: Boolean = aSelfObjectType.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if aSelfObjectType.oclIsUndefined()
		then null
		else aSelfObjectType.aContainingPackage
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASELF_OBJECT_PACKAGE;

		if (aSelfObjectPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSelfObjectPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSelfObjectPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType getATargetObjectType() {
		AClassType aTargetObjectType = basicGetATargetObjectType();
		return aTargetObjectType != null && aTargetObjectType.eIsProxy()
				? (AClassType) eResolveProxy((InternalEObject) aTargetObjectType) : aTargetObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType basicGetATargetObjectType() {
		/**
		 * @OCL let a:mrules::MRuleAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() or a = self then null
		else a.aTargetObjectType endif
		/*   
		if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mrules::MRuleAnnotation) 
		then eContainer().oclAsType(mrules::MRuleAnnotation).targetObjectTypeOfAnnotation
		else  if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression) then
		eContainer().oclAsType(mrules::expressions::MAbstractExpression).targetObjectType else null endif
		endif
		endif *\/
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_OBJECT_TYPE;

		if (aTargetObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTargetObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTargetObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassType result = (AClassType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getATargetSimpleType() {
		/**
		 * @OCL if self.containingAnnotation.oclIsUndefined()
		then null
		else self.containingAnnotation.aTargetSimpleType endif
		/*
		if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.targetSimpleTypeOfAnnotation
		endif
		endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE;

		if (aTargetSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTargetSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTargetSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAObjectObjectType() {
		AClassifier aObjectObjectType = basicGetAObjectObjectType();
		return aObjectObjectType != null && aObjectObjectType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aObjectObjectType) : aObjectObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAObjectObjectType() {
		/**
		 * @OCL null
		
		/*if self.oclIsKindOf(mcore::annotations::MUpdateValue)  
		--replaced with self.
		then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation
		else if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)
		then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation
		else if eContainer().oclIsUndefined()  then null 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  
		then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  
		then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   
		else  if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression) 
		then eContainer().oclAsType(mrules::expressions::MAbstractExpression).objectObjectType 
		else null endif endif endif endif endif endif
		*\/
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AOBJECT_OBJECT_TYPE;

		if (aObjectObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aObjectObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aObjectObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAExpectedReturnType() {
		AClassifier aExpectedReturnType = basicGetAExpectedReturnType();
		return aExpectedReturnType != null && aExpectedReturnType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aExpectedReturnType) : aExpectedReturnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAExpectedReturnType() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.aExpectedReturnTypeOfAnnotation
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_TYPE;

		if (aExpectedReturnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aExpectedReturnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getAExpectedReturnSimpleType() {
		/**
		 * @OCL if containingAnnotation.oclIsUndefined() 
		then acore::classifiers::ASimpleType::None
		else containingAnnotation.aExpectedReturnSimpleTypeOfAnnotation endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE;

		if (aExpectedReturnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aExpectedReturnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueMandatory() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY;

		if (isReturnValueMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isReturnValueMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueSingular() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR;

		if (isReturnValueSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isReturnValueSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getASrcObjectType() {
		AClassifier aSrcObjectType = basicGetASrcObjectType();
		return aSrcObjectType != null && aSrcObjectType.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aSrcObjectType) : aSrcObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetASrcObjectType() {
		/**
		 * @OCL null
		
		/*let srcType : MClassifier =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type
		else null 
		endif
		endif
		in
		if srcType.oclIsUndefined() then null else srcType endif*\/
		
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASRC_OBJECT_TYPE;

		if (aSrcObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSrcObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSrcObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getASrcObjectSimpleType() {
		/**
		 * @OCL null
		/*let srcType : SimpleType =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType
		else null
		endif endif
		in
		if srcType.oclIsUndefined() then acore::classifiers::ASimpleType::None else srcType endif *\/
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ASRC_OBJECT_SIMPLE_TYPE;

		if (aSrcObjectSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSrcObjectSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSrcObjectSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortCode() {

		/**
		 * @OCL if asCode.oclIsUndefined() then ''
		else let s: String = asCode in
		if s.size() > 30 then s.substring(1, 27).concat('...')
		else s endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION.getEOperations().get(0);
		if (getShortCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getShortCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getShortCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getScope() {

		/**
		 * @OCL entireScope
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION.getEOperations().get(1);
		if (getScopeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				getScopeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getScopeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MAbstractBaseDefinition>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aTypeAsOcl(APackage aOclPackage, AClassifier classifier, ASimpleType simpleType, Boolean aSingular) {

		/**
		 * @OCL if  aClassifier.oclIsUndefined() and (aSimpleType = acore::classifiers::ASimpleType::None)  
		then 'MISSING TYPE' 
		else let t: String = 
		if  aSimpleType<> acore::classifiers::ASimpleType::None
		then 
		if aSimpleType= acore::classifiers::ASimpleType::Real  then 'Real' 
		else if aSimpleType= acore::classifiers::ASimpleType::Object then 'ecore::EObject'
		else if aSimpleType= acore::classifiers::ASimpleType::Annotation then 'ecore::EAnnotation'
		else if aSimpleType= acore::classifiers::ASimpleType::Attribute then 'ecore::EAttribute'
		else if aSimpleType= acore::classifiers::ASimpleType::Class then 'ecore::EClass'
		else if aSimpleType= acore::classifiers::ASimpleType::Classifier then 'ecore::EClassifier'
		else if aSimpleType= acore::classifiers::ASimpleType::DataType then 'ecore::EDataType'
		else if aSimpleType= acore::classifiers::ASimpleType::Enumeration then 'ecore::Enumeration'
		else if aSimpleType= acore::classifiers::ASimpleType::Feature then 'ecore::EStructuralFeature'
		else if aSimpleType= acore::classifiers::ASimpleType::Literal then 'ecore::EEnumLiteral'
		else if aSimpleType= acore::classifiers::ASimpleType::KeyValue then 'ecore::EStringToStringMapEntry'
		else if aSimpleType= acore::classifiers::ASimpleType::NamedElement then 'ecore::NamedElement'
		else if aSimpleType= acore::classifiers::ASimpleType::Operation then 'ecore::EOperation'
		else if aSimpleType= acore::classifiers::ASimpleType::Package then 'ecore::EPackage'
		else if aSimpleType= acore::classifiers::ASimpleType::Parameter then 'ecore::EParameter'
		else if aSimpleType= acore::classifiers::ASimpleType::Reference then 'ecore::EReference'
		else if aSimpleType=  acore::classifiers::ASimpleType::TypedElement then 'ecore::ETypedElement'
		else if aSimpleType =  acore::classifiers::ASimpleType::Date then 'ecore::EDate' 
		else aSimpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif
		else 
		/* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified 
		let pkg: String = 
		if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat('::')
		  else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then '' else mClassifier.containingPackage.eName.allLowerCase().concat('::') endif 
		endif
		in pkg.concat(mClassifier.eName) 
		NEW Create string with all package qualifiers *\/
		let pkg:String=
		if aClassifier.aContainingPackage   =null then ''
		else aClassifier.aContainingPackage.aName.concat('::' ) endif                            in
		pkg.concat(aClassifier.aName)
		endif
		in 
		
		if  aSingular.oclIsUndefined() then 'MISSING ARITY INFO' else 
		if aSingular=true then t else 'OrderedSet('.concat(t).concat(') ') endif
		endif
		endif
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.ATYPED);
		EOperation eOperation = AbstractionsPackage.Literals.ATYPED.getEOperations().get(0);
		if (aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("aOclPackage", aOclPackage);

			evalEnv.add("classifier", classifier);

			evalEnv.add("simpleType", simpleType);

			evalEnv.add("aSingular", aSingular);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer aIndentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).aIndentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(0);
		if (aIndentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aIndentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = aIndentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		aIndentationSpaces(numberOfSpaces)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(1);
		if (aIndentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aIndentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer = let e1: Integer = size - 1 in 
		if e1.oclIsInvalid() then null else e1 endif in
		if (let e0: Boolean = size < 1 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then ''
		else (let e0: String = aIndentationSpaces(sizeMinusOne).concat(' ') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(2);
		if (aIndentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aStringOrMissing(String p) {

		/**
		 * @OCL if (aStringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(3);
		if (aStringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aStringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aStringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean aStringIsEmpty(String s) {

		/**
		 * @OCL if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(4);
		if (aStringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aStringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aStringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aListOfStringToStringWithSeparator(EList<String> elements, String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(5);
		if (aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aListOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		aListOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(6);
		if (aListOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aListOfStringToStringWithSeparatorecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aListOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage aPackageFromUri(String packageUri) {

		/**
		 * @OCL if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aPackageFromUri(packageUri)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(7);
		if (aPackageFromUriecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aPackageFromUriecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageFromUriecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("packageUri", packageUri);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aClassifierFromUriAndName(String uri, String name) {

		/**
		 * @OCL let p: acore::APackage = aPackageFromUri(uri) in
		if p = null
		then null
		else p.aClassifierFromName(name) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(8);
		if (aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("name", name);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature aFeatureFromUriAndNames(String uri, String className, String featureName) {

		/**
		 * @OCL let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in
		let cAsClass: acore::classifiers::AClassType = let chain: acore::classifiers::AClassifier = c in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClassType)
		then chain.oclAsType(acore::classifiers::AClassType)
		else null
		endif
		endif in
		if cAsClass = null
		then null
		else cAsClass.aFeatureFromName(featureName) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(9);
		if (aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("className", className);

			evalEnv.add("featureName", featureName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAStringClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAStringName: String = 'AString' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(10);
		if (aCoreAStringClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAStringClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAStringClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreARealClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreARealName: String = 'AReal' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(11);
		if (aCoreARealClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreARealClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreARealClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAIntegerClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAIntegerName: String = 'AInteger' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(12);
		if (aCoreAIntegerClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAIntegerClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAIntegerClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAObjectClass() {

		/**
		 * @OCL let aCoreValuesPackageUri: String = 'http://www.langlets.org/ACore/ACore/Values' in
		let aCoreAObjectName: String = 'AObject' in
		aClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(13);
		if (aCoreAObjectClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAObjectClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, body, helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAObjectClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ALABEL:
			return getALabel();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AKIND_BASE:
			return getAKindBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ARENDERED_KIND:
			return getARenderedKind();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACONTAINING_COMPONENT:
			if (resolve)
				return getAContainingComponent();
			return basicGetAContainingComponent();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
			return getATPackageUri();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
			return getATClassifierName();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
			return getATFeatureName();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE:
			if (resolve)
				return getATPackage();
			return basicGetATPackage();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER:
			if (resolve)
				return getATClassifier();
			return basicGetATClassifier();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE:
			if (resolve)
				return getATFeature();
			return basicGetATFeature();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CORE_ASTRING_CLASS:
			if (resolve)
				return getATCoreAStringClass();
			return basicGetATCoreAStringClass();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACLASSIFIER:
			if (resolve)
				return getAClassifier();
			return basicGetAClassifier();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AMANDATORY:
			return getAMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASINGULAR:
			return getASingular();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATYPE_LABEL:
			return getATypeLabel();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AUNDEFINED_TYPE_CONSTANT:
			return getAUndefinedTypeConstant();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
			return getASimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
			return getAsCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
			return getAsBasicCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR:
			if (resolve)
				return getCollector();
			return basicGetCollector();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE:
			return getEntireScope();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE:
			return getScopeBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF:
			return getScopeSelf();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG:
			return getScopeTrg();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ:
			return getScopeObj();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getScopeSimpleTypeConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS:
			return getScopeLiteralConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getScopeObjectReferenceConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES:
			return getScopeVariables();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR:
			return getScopeIterator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR:
			return getScopeAccumulator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS:
			return getScopeParameters();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE:
			return getScopeContainerBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE:
			return getScopeNumberBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE:
			return getScopeSource();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE:
			return getLocalEntireScope();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE:
			return getLocalScopeBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF:
			return getLocalScopeSelf();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG:
			return getLocalScopeTrg();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ:
			return getLocalScopeObj();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE:
			return getLocalScopeSource();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getLocalScopeSimpleTypeConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return getLocalScopeLiteralConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getLocalScopeObjectReferenceConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES:
			return getLocalScopeVariables();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR:
			return getLocalScopeIterator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR:
			return getLocalScopeAccumulator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS:
			return getLocalScopeParameters();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return getLocalScopeNumberBaseDefinition();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER:
			return getLocalScopeContainer();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION:
			if (resolve)
				return getContainingAnnotation();
			return basicGetContainingAnnotation();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION:
			if (resolve)
				return getContainingExpression();
			return basicGetContainingExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
			return getIsComplexExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
			return getIsSubExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE:
			if (resolve)
				return getACalculatedOwnType();
			return basicGetACalculatedOwnType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
			return getCalculatedOwnMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
			return getCalculatedOwnSingular();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE:
			return getACalculatedOwnSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASELF_OBJECT_TYPE:
			if (resolve)
				return getASelfObjectType();
			return basicGetASelfObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASELF_OBJECT_PACKAGE:
			if (resolve)
				return getASelfObjectPackage();
			return basicGetASelfObjectPackage();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATARGET_OBJECT_TYPE:
			if (resolve)
				return getATargetObjectType();
			return basicGetATargetObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE:
			return getATargetSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AOBJECT_OBJECT_TYPE:
			if (resolve)
				return getAObjectObjectType();
			return basicGetAObjectObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_TYPE:
			if (resolve)
				return getAExpectedReturnType();
			return basicGetAExpectedReturnType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE:
			return getAExpectedReturnSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
			return getIsReturnValueMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
			return getIsReturnValueSingular();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASRC_OBJECT_TYPE:
			if (resolve)
				return getASrcObjectType();
			return basicGetASrcObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASRC_OBJECT_SIMPLE_TYPE:
			return getASrcObjectSimpleType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
			setATPackageUri((String) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
			setATClassifierName((String) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
			setATFeatureName((String) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
			setASimpleType((ASimpleType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
			unsetATPackageUri();
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
			unsetATClassifierName();
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
			unsetATFeatureName();
			return;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
			unsetASimpleType();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null : !ALABEL_EDEFAULT.equals(getALabel());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null : !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACONTAINING_COMPONENT:
			return basicGetAContainingComponent() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
			return isSetATPackageUri();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
			return isSetATClassifierName();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
			return isSetATFeatureName();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE:
			return basicGetATPackage() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER:
			return basicGetATClassifier() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE:
			return basicGetATFeature() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CORE_ASTRING_CLASS:
			return basicGetATCoreAStringClass() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACLASSIFIER:
			return basicGetAClassifier() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AMANDATORY:
			return AMANDATORY_EDEFAULT == null ? getAMandatory() != null : !AMANDATORY_EDEFAULT.equals(getAMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASINGULAR:
			return ASINGULAR_EDEFAULT == null ? getASingular() != null : !ASINGULAR_EDEFAULT.equals(getASingular());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATYPE_LABEL:
			return ATYPE_LABEL_EDEFAULT == null ? getATypeLabel() != null
					: !ATYPE_LABEL_EDEFAULT.equals(getATypeLabel());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AUNDEFINED_TYPE_CONSTANT:
			return AUNDEFINED_TYPE_CONSTANT_EDEFAULT == null ? getAUndefinedTypeConstant() != null
					: !AUNDEFINED_TYPE_CONSTANT_EDEFAULT.equals(getAUndefinedTypeConstant());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
			return isSetASimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
			return AS_CODE_EDEFAULT == null ? getAsCode() != null : !AS_CODE_EDEFAULT.equals(getAsCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
			return AS_BASIC_CODE_EDEFAULT == null ? getAsBasicCode() != null
					: !AS_BASIC_CODE_EDEFAULT.equals(getAsBasicCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR:
			return basicGetCollector() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE:
			return !getEntireScope().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE:
			return !getScopeBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF:
			return !getScopeSelf().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG:
			return !getScopeTrg().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ:
			return !getScopeObj().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getScopeSimpleTypeConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS:
			return !getScopeLiteralConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getScopeObjectReferenceConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES:
			return !getScopeVariables().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR:
			return !getScopeIterator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR:
			return !getScopeAccumulator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS:
			return !getScopeParameters().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE:
			return !getScopeContainerBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE:
			return !getScopeNumberBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE:
			return !getScopeSource().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE:
			return !getLocalEntireScope().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE:
			return !getLocalScopeBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF:
			return !getLocalScopeSelf().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG:
			return !getLocalScopeTrg().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ:
			return !getLocalScopeObj().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE:
			return !getLocalScopeSource().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getLocalScopeSimpleTypeConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return !getLocalScopeLiteralConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getLocalScopeObjectReferenceConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES:
			return !getLocalScopeVariables().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR:
			return !getLocalScopeIterator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR:
			return !getLocalScopeAccumulator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS:
			return !getLocalScopeParameters().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return !getLocalScopeNumberBaseDefinition().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER:
			return !getLocalScopeContainer().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION:
			return basicGetContainingAnnotation() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION:
			return basicGetContainingExpression() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
			return IS_COMPLEX_EXPRESSION_EDEFAULT == null ? getIsComplexExpression() != null
					: !IS_COMPLEX_EXPRESSION_EDEFAULT.equals(getIsComplexExpression());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
			return IS_SUB_EXPRESSION_EDEFAULT == null ? getIsSubExpression() != null
					: !IS_SUB_EXPRESSION_EDEFAULT.equals(getIsSubExpression());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACALCULATED_OWN_TYPE:
			return basicGetACalculatedOwnType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
			return CALCULATED_OWN_MANDATORY_EDEFAULT == null ? getCalculatedOwnMandatory() != null
					: !CALCULATED_OWN_MANDATORY_EDEFAULT.equals(getCalculatedOwnMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
			return CALCULATED_OWN_SINGULAR_EDEFAULT == null ? getCalculatedOwnSingular() != null
					: !CALCULATED_OWN_SINGULAR_EDEFAULT.equals(getCalculatedOwnSingular());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ACALCULATED_OWN_SIMPLE_TYPE:
			return getACalculatedOwnSimpleType() != ACALCULATED_OWN_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASELF_OBJECT_TYPE:
			return basicGetASelfObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASELF_OBJECT_PACKAGE:
			return basicGetASelfObjectPackage() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATARGET_OBJECT_TYPE:
			return basicGetATargetObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ATARGET_SIMPLE_TYPE:
			return getATargetSimpleType() != ATARGET_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AOBJECT_OBJECT_TYPE:
			return basicGetAObjectObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_TYPE:
			return basicGetAExpectedReturnType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AEXPECTED_RETURN_SIMPLE_TYPE:
			return getAExpectedReturnSimpleType() != AEXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
			return IS_RETURN_VALUE_MANDATORY_EDEFAULT == null ? getIsReturnValueMandatory() != null
					: !IS_RETURN_VALUE_MANDATORY_EDEFAULT.equals(getIsReturnValueMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
			return IS_RETURN_VALUE_SINGULAR_EDEFAULT == null ? getIsReturnValueSingular() != null
					: !IS_RETURN_VALUE_SINGULAR_EDEFAULT.equals(getIsReturnValueSingular());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASRC_OBJECT_TYPE:
			return basicGetASrcObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ASRC_OBJECT_SIMPLE_TYPE:
			return getASrcObjectSimpleType() != ASRC_OBJECT_SIMPLE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ALABEL:
				return AbstractionsPackage.AELEMENT__ALABEL;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AKIND_BASE:
				return AbstractionsPackage.AELEMENT__AKIND_BASE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ARENDERED_KIND:
				return AbstractionsPackage.AELEMENT__ARENDERED_KIND;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ACONTAINING_COMPONENT:
				return AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI:
				return AbstractionsPackage.AELEMENT__AT_PACKAGE_URI;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME:
				return AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME:
				return AbstractionsPackage.AELEMENT__AT_FEATURE_NAME;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE:
				return AbstractionsPackage.AELEMENT__AT_PACKAGE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER:
				return AbstractionsPackage.AELEMENT__AT_CLASSIFIER;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE:
				return AbstractionsPackage.AELEMENT__AT_FEATURE;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CORE_ASTRING_CLASS:
				return AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ATyped.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ACLASSIFIER:
				return AbstractionsPackage.ATYPED__ACLASSIFIER;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AMANDATORY:
				return AbstractionsPackage.ATYPED__AMANDATORY;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ASINGULAR:
				return AbstractionsPackage.ATYPED__ASINGULAR;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ATYPE_LABEL:
				return AbstractionsPackage.ATYPED__ATYPE_LABEL;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__AUNDEFINED_TYPE_CONSTANT:
				return AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT;
			case ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE:
				return AbstractionsPackage.ATYPED__ASIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AELEMENT__ALABEL:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ALABEL;
			case AbstractionsPackage.AELEMENT__AKIND_BASE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AKIND_BASE;
			case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ARENDERED_KIND;
			case AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ACONTAINING_COMPONENT;
			case AbstractionsPackage.AELEMENT__AT_PACKAGE_URI:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE_URI;
			case AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER_NAME;
			case AbstractionsPackage.AELEMENT__AT_FEATURE_NAME:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE_NAME;
			case AbstractionsPackage.AELEMENT__AT_PACKAGE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_PACKAGE;
			case AbstractionsPackage.AELEMENT__AT_CLASSIFIER:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CLASSIFIER;
			case AbstractionsPackage.AELEMENT__AT_FEATURE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_FEATURE;
			case AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AT_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ATyped.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ATYPED__ACLASSIFIER:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ACLASSIFIER;
			case AbstractionsPackage.ATYPED__AMANDATORY:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AMANDATORY;
			case AbstractionsPackage.ATYPED__ASINGULAR:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ASINGULAR;
			case AbstractionsPackage.ATYPED__ATYPE_LABEL:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ATYPE_LABEL;
			case AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__AUNDEFINED_TYPE_CONSTANT;
			case AbstractionsPackage.ATYPED__ASIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_EXPRESSION__ASIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseOperationID) {
			case AbstractionsPackage.AELEMENT___AINDENT_LEVEL:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENT_LEVEL;
			case AbstractionsPackage.AELEMENT___AINDENTATION_SPACES:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENTATION_SPACES;
			case AbstractionsPackage.AELEMENT___AINDENTATION_SPACES__INTEGER:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENTATION_SPACES__INTEGER;
			case AbstractionsPackage.AELEMENT___ASTRING_OR_MISSING__STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ASTRING_OR_MISSING__STRING;
			case AbstractionsPackage.AELEMENT___ASTRING_IS_EMPTY__STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ASTRING_IS_EMPTY__STRING;
			case AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;
			case AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;
			case AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___APACKAGE_FROM_URI__STRING;
			case AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;
			case AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;
			case AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_ASTRING_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AREAL_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AINTEGER_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AOBJECT_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ATyped.class) {
			switch (baseOperationID) {
			case AbstractionsPackage.ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN:
				return ExpressionsPackage.MABSTRACT_EXPRESSION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SHORT_CODE:
			return getShortCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SCOPE:
			return getScope();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN:
			return aTypeAsOcl((APackage) arguments.get(0), (AClassifier) arguments.get(1),
					(ASimpleType) arguments.get(2), (Boolean) arguments.get(3));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENT_LEVEL:
			return aIndentLevel();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENTATION_SPACES:
			return aIndentationSpaces();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___AINDENTATION_SPACES__INTEGER:
			return aIndentationSpaces((Integer) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ASTRING_OR_MISSING__STRING:
			return aStringOrMissing((String) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ASTRING_IS_EMPTY__STRING:
			return aStringIsEmpty((String) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return aListOfStringToStringWithSeparator((EList<String>) arguments.get(0), (String) arguments.get(1));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return aListOfStringToStringWithSeparator((EList<String>) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___APACKAGE_FROM_URI__STRING:
			return aPackageFromUri((String) arguments.get(0));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
			return aClassifierFromUriAndName((String) arguments.get(0), (String) arguments.get(1));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
			return aFeatureFromUriAndNames((String) arguments.get(0), (String) arguments.get(1),
					(String) arguments.get(2));
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_ASTRING_CLASS:
			return aCoreAStringClass();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AREAL_CLASS:
			return aCoreARealClass();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AINTEGER_CLASS:
			return aCoreAIntegerClass();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___ACORE_AOBJECT_CLASS:
			return aCoreAObjectClass();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (aTPackageUri: ");
		if (aTPackageUriESet)
			result.append(aTPackageUri);
		else
			result.append("<unset>");
		result.append(", aTClassifierName: ");
		if (aTClassifierNameESet)
			result.append(aTClassifierName);
		else
			result.append("<unset>");
		result.append(", aTFeatureName: ");
		if (aTFeatureNameESet)
			result.append(aTFeatureName);
		else
			result.append("<unset>");
		result.append(", aSimpleType: ");
		if (aSimpleTypeESet)
			result.append(aSimpleType);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'OVERRIDE IN SUBCLASS'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = MrulesPackage.Literals.MRULES_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ExpressionsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MAbstractExpressionImpl
