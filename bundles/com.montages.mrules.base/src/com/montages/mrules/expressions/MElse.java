/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MElse</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MElse#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMElse()
 * @model annotation="http://www.xocl.org/OCL label='\'else \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse expression.getShortCode() endif\r\n)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let kind: String = \'ELSE\' in\nkind\n' calculatedOwnMandatoryDerive='if expression.oclIsUndefined() then true\r\nelse expression.aMandatory endif' aCalculatedOwnTypeDerive='if expression.oclIsUndefined() then null\r\nelse expression.aClassifier endif' calculatedOwnSingularDerive='if expression.oclIsUndefined() then true\r\nelse expression.aSingular endif' aCalculatedOwnSimpleTypeDerive='if expression.oclIsUndefined() then acore::classifiers::ASimpleType::None\r\nelse expression.aSimpleType endif' asBasicCodeDerive='\'else \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse if expression.isComplexExpression\r\n\t  then \'(\'.concat(expression.asCode).concat(\')\')\r\n\t  else expression.asCode endif endif\r\n)'"
 * @generated
 */

public interface MElse extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMElse_Expression()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MChainOrApplication getExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MElse#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @generated
	 */

	void setExpression(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MElse#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	void unsetExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MElse#getExpression <em>Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression</em>' containment reference is set.
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	boolean isSetExpression();

} // MElse
