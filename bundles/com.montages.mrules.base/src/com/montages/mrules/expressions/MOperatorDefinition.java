/**
 */

package com.montages.mrules.expressions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MOperator Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MOperatorDefinition#getMOperator <em>MOperator</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMOperatorDefinition()
 * @model
 * @generated
 */

public interface MOperatorDefinition extends EObject {
	/**
	 * Returns the value of the '<em><b>MOperator</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MOperator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MOperator</em>' attribute.
	 * @see com.montages.mrules.expressions.MOperator
	 * @see #isSetMOperator()
	 * @see #unsetMOperator()
	 * @see #setMOperator(MOperator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMOperatorDefinition_MOperator()
	 * @model unsettable="true"
	 * @generated
	 */
	MOperator getMOperator();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MOperatorDefinition#getMOperator <em>MOperator</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>MOperator</em>' attribute.
	 * @see com.montages.mrules.expressions.MOperator
	 * @see #isSetMOperator()
	 * @see #unsetMOperator()
	 * @see #getMOperator()
	 * @generated
	 */

	void setMOperator(MOperator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MOperatorDefinition#getMOperator <em>MOperator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMOperator()
	 * @see #getMOperator()
	 * @see #setMOperator(MOperator)
	 * @generated
	 */
	void unsetMOperator();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MOperatorDefinition#getMOperator <em>MOperator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MOperator</em>' attribute is set.
	 * @see #unsetMOperator()
	 * @see #getMOperator()
	 * @see #setMOperator(MOperator)
	 * @generated
	 */
	boolean isSetMOperator();

} // MOperatorDefinition
