/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.classifiers.ASimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSimple Type Constant Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant1 <em>Constant1</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant2 <em>Constant2</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant3 <em>Constant3</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSimpleTypeConstantLet()
 * @model annotation="http://www.xocl.org/OCL label='asCode'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Value Constant\'\n' asBasicCodeDerive='let a: String = if constant1.oclIsUndefined() then \'\' else if constant1=\'\' then \'\' else constant1 endif endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else if constant2=\'\' then \'\' else constant2 endif endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else if constant3=\'\' then \'\' else constant3 endif endif in\r\n\r\nif aSingular\r\n    then if aSimpleType = acore::classifiers::ASimpleType::String\r\n    \tthen inQuotes(a.concat(b).concat(c))\r\n    \telse a.concat(b).concat(c) endif\r\n    else if aSimpleType = acore::classifiers::ASimpleType::String\r\n    \tthen asSetString(inQuotes(constant1), inQuotes(constant2), inQuotes(constant3))\r\n    \telse asSetString(constant1, constant2, constant3)\r\n    \tendif\r\n    endif' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='let a: Integer = if constant1.oclIsUndefined() then 0\r\n\telse if constant1=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\nlet b: Integer = if constant2.oclIsUndefined() then 0\r\n\telse if constant2=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\nlet c: Integer = if constant3.oclIsUndefined() then 0\r\n\telse if constant3=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\n(a+b+c) <= 1' aCalculatedOwnSimpleTypeDerive='simpleType'"
 * @generated
 */

public interface MSimpleTypeConstantLet extends MConstantLet {
	/**
	 * Returns the value of the '<em><b>Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #setSimpleType(ASimpleType)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSimpleTypeConstantLet_SimpleType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='acore::classifiers::ASimpleType::String' choiceConstruction='OrderedSet{acore::classifiers::ASimpleType::Boolean, acore::classifiers::ASimpleType::Real, acore::classifiers::ASimpleType::Integer, acore::classifiers::ASimpleType::String}'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	ASimpleType getSimpleType();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @generated
	 */

	void setSimpleType(ASimpleType value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(ASimpleType)
	 * @generated
	 */
	void unsetSimpleType();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getSimpleType <em>Simple Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Simple Type</em>' attribute is set.
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(ASimpleType)
	 * @generated
	 */
	boolean isSetSimpleType();

	/**
	 * Returns the value of the '<em><b>Constant1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant1</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant1</em>' attribute.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #setConstant1(String)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSimpleTypeConstantLet_Constant1()
	 * @model unsettable="true"
	 * @generated
	 */
	String getConstant1();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant1 <em>Constant1</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant1</em>' attribute.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @generated
	 */

	void setConstant1(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant1 <em>Constant1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(String)
	 * @generated
	 */
	void unsetConstant1();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant1 <em>Constant1</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant1</em>' attribute is set.
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(String)
	 * @generated
	 */
	boolean isSetConstant1();

	/**
	 * Returns the value of the '<em><b>Constant2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant2</em>' attribute.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #setConstant2(String)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSimpleTypeConstantLet_Constant2()
	 * @model unsettable="true"
	 * @generated
	 */
	String getConstant2();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant2 <em>Constant2</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant2</em>' attribute.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @generated
	 */

	void setConstant2(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant2 <em>Constant2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(String)
	 * @generated
	 */
	void unsetConstant2();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant2 <em>Constant2</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant2</em>' attribute is set.
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(String)
	 * @generated
	 */
	boolean isSetConstant2();

	/**
	 * Returns the value of the '<em><b>Constant3</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant3</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant3</em>' attribute.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #setConstant3(String)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSimpleTypeConstantLet_Constant3()
	 * @model unsettable="true"
	 * @generated
	 */
	String getConstant3();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant3 <em>Constant3</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant3</em>' attribute.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @generated
	 */

	void setConstant3(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant3 <em>Constant3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(String)
	 * @generated
	 */
	void unsetConstant3();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MSimpleTypeConstantLet#getConstant3 <em>Constant3</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant3</em>' attribute is set.
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(String)
	 * @generated
	 */
	boolean isSetConstant3();

} // MSimpleTypeConstantLet
