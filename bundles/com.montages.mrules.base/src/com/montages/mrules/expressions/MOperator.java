/**
 */
package com.montages.mrules.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MOperator</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMOperator()
 * @model
 * @generated
 */
public enum MOperator implements Enumerator {
	/**
	 * The '<em><b>Equal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EQUAL_VALUE
	 * @generated
	 * @ordered
	 */
	EQUAL(0, "Equal", "="),

	/**
	 * The '<em><b>Not Equal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_EQUAL_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_EQUAL(1, "NotEqual", "<>"),

	/**
	 * The '<em><b>Greater</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GREATER_VALUE
	 * @generated
	 * @ordered
	 */
	GREATER(2, "Greater", ">"),

	/**
	 * The '<em><b>Smaller</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SMALLER_VALUE
	 * @generated
	 * @ordered
	 */
	SMALLER(3, "Smaller", "<"),

	/**
	 * The '<em><b>Greater Equal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GREATER_EQUAL_VALUE
	 * @generated
	 * @ordered
	 */
	GREATER_EQUAL(4, "GreaterEqual", ">="),

	/**
	 * The '<em><b>Smaller Equal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SMALLER_EQUAL_VALUE
	 * @generated
	 * @ordered
	 */
	SMALLER_EQUAL(5, "SmallerEqual", "<="),

	/**
	 * The '<em><b>Not Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NOT_OP_VALUE
	 * @generated
	 * @ordered
	 */
	NOT_OP(6, "NotOp", "not"),

	/**
	 * The '<em><b>Ocl And</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OCL_AND_VALUE
	 * @generated
	 * @ordered
	 */
	OCL_AND(7, "OclAnd", "oclAND"),

	/**
	 * The '<em><b>Ocl Or</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OCL_OR_VALUE
	 * @generated
	 * @ordered
	 */
	OCL_OR(8, "OclOr", "oclOR"),

	/**
	 * The '<em><b>Xor Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #XOR_OP_VALUE
	 * @generated
	 * @ordered
	 */
	XOR_OP(9, "XorOp", "xor"),

	/**
	 * The '<em><b>Implies Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #IMPLIES_OP_VALUE
	 * @generated
	 * @ordered
	 */
	IMPLIES_OP(10, "ImpliesOp", "implies"),

	/**
	 * The '<em><b>Plus</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PLUS_VALUE
	 * @generated
	 * @ordered
	 */
	PLUS(11, "Plus", "+"),

	/**
	 * The '<em><b>Minus</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS(12, "Minus", "-"),

	/**
	 * The '<em><b>Times</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TIMES_VALUE
	 * @generated
	 * @ordered
	 */
	TIMES(13, "Times", "*"),

	/**
	 * The '<em><b>Divide</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIVIDE_VALUE
	 * @generated
	 * @ordered
	 */
	DIVIDE(14, "Divide", "/"),

	/**
	 * The '<em><b>Mod</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOD_VALUE
	 * @generated
	 * @ordered
	 */
	MOD(15, "Mod", "mod()"),

	/**
	 * The '<em><b>Div</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIV_VALUE
	 * @generated
	 * @ordered
	 */
	DIV(16, "Div", "div()"),

	/**
	 * The '<em><b>Concat</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONCAT_VALUE
	 * @generated
	 * @ordered
	 */
	CONCAT(17, "Concat", "concat()"),

	/**
	 * The '<em><b>Set Including</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDING_VALUE
	 * @generated
	 * @ordered
	 */
	SET_INCLUDING(18, "SetIncluding", "including()"),

	/**
	 * The '<em><b>Set Excluding</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDING_VALUE
	 * @generated
	 * @ordered
	 */
	SET_EXCLUDING(19, "SetExcluding", "excluding()"),

	/**
	 * The '<em><b>Set Union</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_UNION_VALUE
	 * @generated
	 * @ordered
	 */
	SET_UNION(20, "SetUnion", "union()"),

	/**
	 * The '<em><b>Set Excludes</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDES_VALUE
	 * @generated
	 * @ordered
	 */
	SET_EXCLUDES(21, "SetExcludes", "excludes()"),

	/**
	 * The '<em><b>Set Includes</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDES_VALUE
	 * @generated
	 * @ordered
	 */
	SET_INCLUDES(22, "SetIncludes", "includes()"),

	/**
	 * The '<em><b>Index Of</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INDEX_OF_VALUE
	 * @generated
	 * @ordered
	 */
	INDEX_OF(23, "IndexOf", "indexOf()"),

	/**
	 * The '<em><b>At</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AT_VALUE
	 * @generated
	 * @ordered
	 */
	AT(24, "At", "at()"),

	/**
	 * The '<em><b>Set As Ordered Set</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_AS_ORDERED_SET_VALUE
	 * @generated
	 * @ordered
	 */
	SET_AS_ORDERED_SET(25, "SetAsOrderedSet", "asOrderedSet()"),

	/**
	 * The '<em><b>To String</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TO_STRING_VALUE
	 * @generated
	 * @ordered
	 */
	TO_STRING(26, "ToString", "toString()"),

	/**
	 * The '<em><b>Trim</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRIM_VALUE
	 * @generated
	 * @ordered
	 */
	TRIM(27, "Trim", "trim()"),

	/**
	 * The '<em><b>Sub String</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_STRING_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_STRING(28, "SubString", "substring()"),

	/**
	 * The '<em><b>And Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AND_OP_VALUE
	 * @generated
	 * @ordered
	 */
	AND_OP(29, "AndOp", "and"),

	/**
	 * The '<em><b>Or Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OR_OP_VALUE
	 * @generated
	 * @ordered
	 */
	OR_OP(30, "OrOp", "or"),

	/**
	 * The '<em><b>Business NOT</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BUSINESS_NOT_VALUE
	 * @generated
	 * @ordered
	 */
	BUSINESS_NOT(31, "BusinessNOT", "businessNOT"),

	/**
	 * The '<em><b>Business Implies</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BUSINESS_IMPLIES_VALUE
	 * @generated
	 * @ordered
	 */
	BUSINESS_IMPLIES(32, "BusinessImplies", "businessImplies"),

	/**
	 * The '<em><b>Set Excludes All</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDES_ALL_VALUE
	 * @generated
	 * @ordered
	 */
	SET_EXCLUDES_ALL(33, "SetExcludesAll", "excludesAll()"),

	/**
	 * The '<em><b>Set Includes All</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDES_ALL_VALUE
	 * @generated
	 * @ordered
	 */
	SET_INCLUDES_ALL(34, "SetIncludesAll", "includesAll()"),

	/**
	 * The '<em><b>Intersection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTERSECTION_VALUE
	 * @generated
	 * @ordered
	 */
	INTERSECTION(35, "Intersection", "intersection()"),

	/**
	 * The '<em><b>Diff In Years</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_YEARS_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_YEARS(36, "DiffInYears", "DiffInYears()"),

	/**
	 * The '<em><b>Diff In Days</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_DAYS_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_DAYS(37, "DiffInDays", "DiffInDays()"),

	/**
	 * The '<em><b>Diff In Seconds</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_SECONDS_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_SECONDS(38, "DiffInSeconds", "DiffInSeconds()"),

	/**
	 * The '<em><b>Diff In Minutes</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_MINUTES_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_MINUTES(39, "DiffInMinutes", "DiffInMinutes()"),

	/**
	 * The '<em><b>Diff In Month</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_MONTH_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_MONTH(40, "DiffInMonth", "DiffInMonth()"),

	/**
	 * The '<em><b>Diff In Hrs</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_HRS_VALUE
	 * @generated
	 * @ordered
	 */
	DIFF_IN_HRS(41, "DiffInHrs", "DiffInHours()"),

	/**
	 * The '<em><b>Ocl Is Invalid</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OCL_IS_INVALID_VALUE
	 * @generated
	 * @ordered
	 */
	OCL_IS_INVALID(42, "OclIsInvalid", "oclIsInvalid()"),

	/**
	 * The '<em><b>Set First</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_FIRST_VALUE
	 * @generated
	 * @ordered
	 */
	SET_FIRST(43, "SetFirst", "first()"),

	/**
	 * The '<em><b>Set Is Empty</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_IS_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	SET_IS_EMPTY(44, "SetIsEmpty", "isEmpty()"),

	/**
	 * The '<em><b>Set Last</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_LAST_VALUE
	 * @generated
	 * @ordered
	 */
	SET_LAST(45, "SetLast", "last()"),

	/**
	 * The '<em><b>Set Not Empty</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_NOT_EMPTY_VALUE
	 * @generated
	 * @ordered
	 */
	SET_NOT_EMPTY(46, "SetNotEmpty", "notEmpty()"),

	/**
	 * The '<em><b>Set Size</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_SIZE_VALUE
	 * @generated
	 * @ordered
	 */
	SET_SIZE(47, "SetSize", "size()"),

	/**
	 * The '<em><b>Set Sum</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SET_SUM_VALUE
	 * @generated
	 * @ordered
	 */
	SET_SUM(48, "SetSum", "sum()"),

	/**
	 * The '<em><b>Ocl Is Undefined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OCL_IS_UNDEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	OCL_IS_UNDEFINED(49, "OclIsUndefined", "oclIsUndefined()");

	/**
	 * The '<em><b>Equal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Equal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EQUAL
	 * @model name="Equal" literal="="
	 * @generated
	 * @ordered
	 */
	public static final int EQUAL_VALUE = 0;

	/**
	 * The '<em><b>Not Equal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not Equal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_EQUAL
	 * @model name="NotEqual" literal="<>"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_EQUAL_VALUE = 1;

	/**
	 * The '<em><b>Greater</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Greater</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GREATER
	 * @model name="Greater" literal=">"
	 * @generated
	 * @ordered
	 */
	public static final int GREATER_VALUE = 2;

	/**
	 * The '<em><b>Smaller</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Smaller</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SMALLER
	 * @model name="Smaller" literal="<"
	 * @generated
	 * @ordered
	 */
	public static final int SMALLER_VALUE = 3;

	/**
	 * The '<em><b>Greater Equal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Greater Equal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GREATER_EQUAL
	 * @model name="GreaterEqual" literal=">="
	 * @generated
	 * @ordered
	 */
	public static final int GREATER_EQUAL_VALUE = 4;

	/**
	 * The '<em><b>Smaller Equal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Smaller Equal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SMALLER_EQUAL
	 * @model name="SmallerEqual" literal="<="
	 * @generated
	 * @ordered
	 */
	public static final int SMALLER_EQUAL_VALUE = 5;

	/**
	 * The '<em><b>Not Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Not Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NOT_OP
	 * @model name="NotOp" literal="not"
	 * @generated
	 * @ordered
	 */
	public static final int NOT_OP_VALUE = 6;

	/**
	 * The '<em><b>Ocl And</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ocl And</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OCL_AND
	 * @model name="OclAnd" literal="oclAND"
	 *        annotation="http://www.montages.com/mCore/MCore mName='oclAnd'"
	 * @generated
	 * @ordered
	 */
	public static final int OCL_AND_VALUE = 7;

	/**
	 * The '<em><b>Ocl Or</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ocl Or</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OCL_OR
	 * @model name="OclOr" literal="oclOR"
	 *        annotation="http://www.montages.com/mCore/MCore mName='OclOr'"
	 * @generated
	 * @ordered
	 */
	public static final int OCL_OR_VALUE = 8;

	/**
	 * The '<em><b>Xor Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Xor Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #XOR_OP
	 * @model name="XorOp" literal="xor"
	 * @generated
	 * @ordered
	 */
	public static final int XOR_OP_VALUE = 9;

	/**
	 * The '<em><b>Implies Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Implies Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #IMPLIES_OP
	 * @model name="ImpliesOp" literal="implies"
	 * @generated
	 * @ordered
	 */
	public static final int IMPLIES_OP_VALUE = 10;

	/**
	 * The '<em><b>Plus</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Plus</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PLUS
	 * @model name="Plus" literal="+"
	 * @generated
	 * @ordered
	 */
	public static final int PLUS_VALUE = 11;

	/**
	 * The '<em><b>Minus</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS
	 * @model name="Minus" literal="-"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_VALUE = 12;

	/**
	 * The '<em><b>Times</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Times</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TIMES
	 * @model name="Times" literal="*"
	 * @generated
	 * @ordered
	 */
	public static final int TIMES_VALUE = 13;

	/**
	 * The '<em><b>Divide</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Divide</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIVIDE
	 * @model name="Divide" literal="/"
	 * @generated
	 * @ordered
	 */
	public static final int DIVIDE_VALUE = 14;

	/**
	 * The '<em><b>Mod</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Mod</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOD
	 * @model name="Mod" literal="mod()"
	 * @generated
	 * @ordered
	 */
	public static final int MOD_VALUE = 15;

	/**
	 * The '<em><b>Div</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Div</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIV
	 * @model name="Div" literal="div()"
	 * @generated
	 * @ordered
	 */
	public static final int DIV_VALUE = 16;

	/**
	 * The '<em><b>Concat</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Concat</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONCAT
	 * @model name="Concat" literal="concat()"
	 * @generated
	 * @ordered
	 */
	public static final int CONCAT_VALUE = 17;

	/**
	 * The '<em><b>Set Including</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Including</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDING
	 * @model name="SetIncluding" literal="including()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_INCLUDING_VALUE = 18;

	/**
	 * The '<em><b>Set Excluding</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Excluding</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDING
	 * @model name="SetExcluding" literal="excluding()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_EXCLUDING_VALUE = 19;

	/**
	 * The '<em><b>Set Union</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Union</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_UNION
	 * @model name="SetUnion" literal="union()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_UNION_VALUE = 20;

	/**
	 * The '<em><b>Set Excludes</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Excludes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDES
	 * @model name="SetExcludes" literal="excludes()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_EXCLUDES_VALUE = 21;

	/**
	 * The '<em><b>Set Includes</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Includes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDES
	 * @model name="SetIncludes" literal="includes()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_INCLUDES_VALUE = 22;

	/**
	 * The '<em><b>Index Of</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Index Of</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INDEX_OF
	 * @model name="IndexOf" literal="indexOf()"
	 * @generated
	 * @ordered
	 */
	public static final int INDEX_OF_VALUE = 23;

	/**
	 * The '<em><b>At</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>At</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AT
	 * @model name="At" literal="at()"
	 * @generated
	 * @ordered
	 */
	public static final int AT_VALUE = 24;

	/**
	 * The '<em><b>Set As Ordered Set</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set As Ordered Set</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_AS_ORDERED_SET
	 * @model name="SetAsOrderedSet" literal="asOrderedSet()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_AS_ORDERED_SET_VALUE = 25;

	/**
	 * The '<em><b>To String</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>To String</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TO_STRING
	 * @model name="ToString" literal="toString()"
	 * @generated
	 * @ordered
	 */
	public static final int TO_STRING_VALUE = 26;

	/**
	 * The '<em><b>Trim</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Trim</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRIM
	 * @model name="Trim" literal="trim()"
	 * @generated
	 * @ordered
	 */
	public static final int TRIM_VALUE = 27;

	/**
	 * The '<em><b>Sub String</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sub String</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_STRING
	 * @model name="SubString" literal="substring()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='SubString'"
	 * @generated
	 * @ordered
	 */
	public static final int SUB_STRING_VALUE = 28;

	/**
	 * The '<em><b>And Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>And Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AND_OP
	 * @model name="AndOp" literal="and"
	 *        annotation="http://www.montages.com/mCore/MCore mName='and Op'"
	 * @generated
	 * @ordered
	 */
	public static final int AND_OP_VALUE = 29;

	/**
	 * The '<em><b>Or Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Or Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OR_OP
	 * @model name="OrOp" literal="or"
	 *        annotation="http://www.montages.com/mCore/MCore mName='or Op'"
	 * @generated
	 * @ordered
	 */
	public static final int OR_OP_VALUE = 30;

	/**
	 * The '<em><b>Business NOT</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Business NOT</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BUSINESS_NOT
	 * @model name="BusinessNOT" literal="businessNOT"
	 *        annotation="http://www.montages.com/mCore/MCore mName='BusinessNOT'"
	 * @generated
	 * @ordered
	 */
	public static final int BUSINESS_NOT_VALUE = 31;

	/**
	 * The '<em><b>Business Implies</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Business Implies</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BUSINESS_IMPLIES
	 * @model name="BusinessImplies" literal="businessImplies"
	 *        annotation="http://www.montages.com/mCore/MCore mName='BusinessImplies'"
	 * @generated
	 * @ordered
	 */
	public static final int BUSINESS_IMPLIES_VALUE = 32;

	/**
	 * The '<em><b>Set Excludes All</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Excludes All</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_EXCLUDES_ALL
	 * @model name="SetExcludesAll" literal="excludesAll()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_EXCLUDES_ALL_VALUE = 33;

	/**
	 * The '<em><b>Set Includes All</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Includes All</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_INCLUDES_ALL
	 * @model name="SetIncludesAll" literal="includesAll()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_INCLUDES_ALL_VALUE = 34;

	/**
	 * The '<em><b>Intersection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Intersection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTERSECTION
	 * @model name="Intersection" literal="intersection()"
	 * @generated
	 * @ordered
	 */
	public static final int INTERSECTION_VALUE = 35;

	/**
	 * The '<em><b>Diff In Years</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Years</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_YEARS
	 * @model name="DiffInYears" literal="DiffInYears()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInYears'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_YEARS_VALUE = 36;

	/**
	 * The '<em><b>Diff In Days</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Days</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_DAYS
	 * @model name="DiffInDays" literal="DiffInDays()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInDays'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_DAYS_VALUE = 37;

	/**
	 * The '<em><b>Diff In Seconds</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Seconds</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_SECONDS
	 * @model name="DiffInSeconds" literal="DiffInSeconds()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInSeconds'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_SECONDS_VALUE = 38;

	/**
	 * The '<em><b>Diff In Minutes</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Minutes</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_MINUTES
	 * @model name="DiffInMinutes" literal="DiffInMinutes()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInMinutes'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_MINUTES_VALUE = 39;

	/**
	 * The '<em><b>Diff In Month</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Month</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_MONTH
	 * @model name="DiffInMonth" literal="DiffInMonth()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInMonth'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_MONTH_VALUE = 40;

	/**
	 * The '<em><b>Diff In Hrs</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Diff In Hrs</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DIFF_IN_HRS
	 * @model name="DiffInHrs" literal="DiffInHours()"
	 *        annotation="http://www.montages.com/mCore/MCore mName='DiffInHrs'"
	 * @generated
	 * @ordered
	 */
	public static final int DIFF_IN_HRS_VALUE = 41;

	/**
	 * The '<em><b>Ocl Is Invalid</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ocl Is Invalid</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OCL_IS_INVALID
	 * @model name="OclIsInvalid" literal="oclIsInvalid()"
	 * @generated
	 * @ordered
	 */
	public static final int OCL_IS_INVALID_VALUE = 42;

	/**
	 * The '<em><b>Set First</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set First</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_FIRST
	 * @model name="SetFirst" literal="first()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_FIRST_VALUE = 43;

	/**
	 * The '<em><b>Set Is Empty</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Is Empty</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_IS_EMPTY
	 * @model name="SetIsEmpty" literal="isEmpty()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_IS_EMPTY_VALUE = 44;

	/**
	 * The '<em><b>Set Last</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Last</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_LAST
	 * @model name="SetLast" literal="last()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_LAST_VALUE = 45;

	/**
	 * The '<em><b>Set Not Empty</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Not Empty</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_NOT_EMPTY
	 * @model name="SetNotEmpty" literal="notEmpty()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_NOT_EMPTY_VALUE = 46;

	/**
	 * The '<em><b>Set Size</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Size</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_SIZE
	 * @model name="SetSize" literal="size()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_SIZE_VALUE = 47;

	/**
	 * The '<em><b>Set Sum</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Set Sum</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SET_SUM
	 * @model name="SetSum" literal="sum()"
	 * @generated
	 * @ordered
	 */
	public static final int SET_SUM_VALUE = 48;

	/**
	 * The '<em><b>Ocl Is Undefined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ocl Is Undefined</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OCL_IS_UNDEFINED
	 * @model name="OclIsUndefined" literal="oclIsUndefined()"
	 * @generated
	 * @ordered
	 */
	public static final int OCL_IS_UNDEFINED_VALUE = 49;

	/**
	 * An array of all the '<em><b>MOperator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MOperator[] VALUES_ARRAY = new MOperator[] { EQUAL, NOT_EQUAL, GREATER, SMALLER, GREATER_EQUAL,
			SMALLER_EQUAL, NOT_OP, OCL_AND, OCL_OR, XOR_OP, IMPLIES_OP, PLUS, MINUS, TIMES, DIVIDE, MOD, DIV, CONCAT,
			SET_INCLUDING, SET_EXCLUDING, SET_UNION, SET_EXCLUDES, SET_INCLUDES, INDEX_OF, AT, SET_AS_ORDERED_SET,
			TO_STRING, TRIM, SUB_STRING, AND_OP, OR_OP, BUSINESS_NOT, BUSINESS_IMPLIES, SET_EXCLUDES_ALL,
			SET_INCLUDES_ALL, INTERSECTION, DIFF_IN_YEARS, DIFF_IN_DAYS, DIFF_IN_SECONDS, DIFF_IN_MINUTES,
			DIFF_IN_MONTH, DIFF_IN_HRS, OCL_IS_INVALID, SET_FIRST, SET_IS_EMPTY, SET_LAST, SET_NOT_EMPTY, SET_SIZE,
			SET_SUM, OCL_IS_UNDEFINED, };

	/**
	 * A public read-only list of all the '<em><b>MOperator</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MOperator> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MOperator</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperator get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MOperator result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MOperator</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperator getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MOperator result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MOperator</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MOperator get(int value) {
		switch (value) {
		case EQUAL_VALUE:
			return EQUAL;
		case NOT_EQUAL_VALUE:
			return NOT_EQUAL;
		case GREATER_VALUE:
			return GREATER;
		case SMALLER_VALUE:
			return SMALLER;
		case GREATER_EQUAL_VALUE:
			return GREATER_EQUAL;
		case SMALLER_EQUAL_VALUE:
			return SMALLER_EQUAL;
		case NOT_OP_VALUE:
			return NOT_OP;
		case OCL_AND_VALUE:
			return OCL_AND;
		case OCL_OR_VALUE:
			return OCL_OR;
		case XOR_OP_VALUE:
			return XOR_OP;
		case IMPLIES_OP_VALUE:
			return IMPLIES_OP;
		case PLUS_VALUE:
			return PLUS;
		case MINUS_VALUE:
			return MINUS;
		case TIMES_VALUE:
			return TIMES;
		case DIVIDE_VALUE:
			return DIVIDE;
		case MOD_VALUE:
			return MOD;
		case DIV_VALUE:
			return DIV;
		case CONCAT_VALUE:
			return CONCAT;
		case SET_INCLUDING_VALUE:
			return SET_INCLUDING;
		case SET_EXCLUDING_VALUE:
			return SET_EXCLUDING;
		case SET_UNION_VALUE:
			return SET_UNION;
		case SET_EXCLUDES_VALUE:
			return SET_EXCLUDES;
		case SET_INCLUDES_VALUE:
			return SET_INCLUDES;
		case INDEX_OF_VALUE:
			return INDEX_OF;
		case AT_VALUE:
			return AT;
		case SET_AS_ORDERED_SET_VALUE:
			return SET_AS_ORDERED_SET;
		case TO_STRING_VALUE:
			return TO_STRING;
		case TRIM_VALUE:
			return TRIM;
		case SUB_STRING_VALUE:
			return SUB_STRING;
		case AND_OP_VALUE:
			return AND_OP;
		case OR_OP_VALUE:
			return OR_OP;
		case BUSINESS_NOT_VALUE:
			return BUSINESS_NOT;
		case BUSINESS_IMPLIES_VALUE:
			return BUSINESS_IMPLIES;
		case SET_EXCLUDES_ALL_VALUE:
			return SET_EXCLUDES_ALL;
		case SET_INCLUDES_ALL_VALUE:
			return SET_INCLUDES_ALL;
		case INTERSECTION_VALUE:
			return INTERSECTION;
		case DIFF_IN_YEARS_VALUE:
			return DIFF_IN_YEARS;
		case DIFF_IN_DAYS_VALUE:
			return DIFF_IN_DAYS;
		case DIFF_IN_SECONDS_VALUE:
			return DIFF_IN_SECONDS;
		case DIFF_IN_MINUTES_VALUE:
			return DIFF_IN_MINUTES;
		case DIFF_IN_MONTH_VALUE:
			return DIFF_IN_MONTH;
		case DIFF_IN_HRS_VALUE:
			return DIFF_IN_HRS;
		case OCL_IS_INVALID_VALUE:
			return OCL_IS_INVALID;
		case SET_FIRST_VALUE:
			return SET_FIRST;
		case SET_IS_EMPTY_VALUE:
			return SET_IS_EMPTY;
		case SET_LAST_VALUE:
			return SET_LAST;
		case SET_NOT_EMPTY_VALUE:
			return SET_NOT_EMPTY;
		case SET_SIZE_VALUE:
			return SET_SIZE;
		case SET_SUM_VALUE:
			return SET_SUM;
		case OCL_IS_UNDEFINED_VALUE:
			return OCL_IS_UNDEFINED;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MOperator(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MOperator
