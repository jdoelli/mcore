/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MIterator Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MIteratorBaseDefinition#getIterator <em>Iterator</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMIteratorBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<iterator> \'.concat(if iterator.oclIsUndefined() then \'\' else iterator.aName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='ExpressionBase::Iterator' calculatedAsCodeDerive='if iterator.oclIsUndefined() then \'MISSING ITERATOR\' else iterator.aName endif' aMandatoryDerive='true' aSingularDerive='true' aSimpleTypeDerive='if iterator.oclIsUndefined() then acore::classifiers::ASimpleType::None\r\nelse iterator.aSimpleType\r\nendif' aClassifierDerive='if iterator.oclIsUndefined() then null\r\nelse iterator.aClassifier endif'"
 * @generated
 */

public interface MIteratorBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Iterator</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator</em>' reference.
	 * @see #isSetIterator()
	 * @see #unsetIterator()
	 * @see #setIterator(MIterator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMIteratorBaseDefinition_Iterator()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='iterator'"
	 * @generated
	 */
	MIterator getIterator();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MIteratorBaseDefinition#getIterator <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator</em>' reference.
	 * @see #isSetIterator()
	 * @see #unsetIterator()
	 * @see #getIterator()
	 * @generated
	 */

	void setIterator(MIterator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MIteratorBaseDefinition#getIterator <em>Iterator</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIterator()
	 * @see #getIterator()
	 * @see #setIterator(MIterator)
	 * @generated
	 */
	void unsetIterator();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MIteratorBaseDefinition#getIterator <em>Iterator</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Iterator</em>' reference is set.
	 * @see #unsetIterator()
	 * @see #getIterator()
	 * @see #setIterator(MIterator)
	 * @generated
	 */
	boolean isSetIterator();

} // MIteratorBaseDefinition
