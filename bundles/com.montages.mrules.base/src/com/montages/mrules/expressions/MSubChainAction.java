/**
 */
package com.montages.mrules.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MSub Chain Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMSubChainAction()
 * @model
 * @generated
 */
public enum MSubChainAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	ARGUMENT(1, "Argument", "+ Argument"),

	/**
	 * The '<em><b>Sub Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_CHAIN(2, "SubChain", "+ Sub Chain"),

	/**
	 * The '<em><b>Data Constant As Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_CONSTANT_AS_ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_CONSTANT_AS_ARGUMENT(3, "DataConstantAsArgument", "+ Data Constant as Argument"),

	/**
	 * The '<em><b>Literal Constant As Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_CONSTANT_AS_ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL_CONSTANT_AS_ARGUMENT(4, "LiteralConstantAsArgument", "+ Literal Constant as Argument");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARGUMENT
	 * @model name="Argument" literal="+ Argument"
	 * @generated
	 * @ordered
	 */
	public static final int ARGUMENT_VALUE = 1;

	/**
	 * The '<em><b>Sub Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sub Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_CHAIN
	 * @model name="SubChain" literal="+ Sub Chain"
	 * @generated
	 * @ordered
	 */
	public static final int SUB_CHAIN_VALUE = 2;

	/**
	 * The '<em><b>Data Constant As Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Data Constant As Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATA_CONSTANT_AS_ARGUMENT
	 * @model name="DataConstantAsArgument" literal="+ Data Constant as Argument"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_CONSTANT_AS_ARGUMENT_VALUE = 3;

	/**
	 * The '<em><b>Literal Constant As Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal Constant As Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL_CONSTANT_AS_ARGUMENT
	 * @model name="LiteralConstantAsArgument" literal="+ Literal Constant as Argument"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_CONSTANT_AS_ARGUMENT_VALUE = 4;

	/**
	 * An array of all the '<em><b>MSub Chain Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MSubChainAction[] VALUES_ARRAY = new MSubChainAction[] { DO, ARGUMENT, SUB_CHAIN,
			DATA_CONSTANT_AS_ARGUMENT, LITERAL_CONSTANT_AS_ARGUMENT, };

	/**
	 * A public read-only list of all the '<em><b>MSub Chain Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MSubChainAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MSub Chain Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MSubChainAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MSubChainAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MSub Chain Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MSubChainAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MSubChainAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MSub Chain Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MSubChainAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case ARGUMENT_VALUE:
			return ARGUMENT;
		case SUB_CHAIN_VALUE:
			return SUB_CHAIN;
		case DATA_CONSTANT_AS_ARGUMENT_VALUE:
			return DATA_CONSTANT_AS_ARGUMENT;
		case LITERAL_CONSTANT_AS_ARGUMENT_VALUE:
			return LITERAL_CONSTANT_AS_ARGUMENT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MSubChainAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MSubChainAction
