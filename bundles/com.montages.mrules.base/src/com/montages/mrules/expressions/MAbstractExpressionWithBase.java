/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.abstractions.AVariable;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Expression With Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBaseAsCode <em>Base As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBaseDefinition <em>Base Definition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseVar <em>ABase Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseExitType <em>ABase Exit Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseExitSimpleType <em>ABase Exit Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBaseExitMandatory <em>Base Exit Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBaseExitSingular <em>Base Exit Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase()
 * @model
 * @generated
 */

public interface MAbstractExpressionWithBase extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Base As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base As Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base As Code</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseAsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'\'\n  else if baseDefinition.oclIsTypeOf(mrules::expressions::MNumberBaseDefinition)\n  then  baseDefinition.oclAsType(mrules::expressions::MNumberBaseDefinition).calculateAsCode(self)\n  else baseDefinition.calculatedAsCode\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getBaseAsCode();

	/**
	 * Returns the value of the '<em><b>Base</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.ExpressionBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionBase
	 * @see #isSetBase()
	 * @see #unsetBase()
	 * @see #setBase(ExpressionBase)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_Base()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ExpressionBase getBase();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionBase
	 * @see #isSetBase()
	 * @see #unsetBase()
	 * @see #getBase()
	 * @generated
	 */

	void setBase(ExpressionBase value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBase()
	 * @see #getBase()
	 * @see #setBase(ExpressionBase)
	 * @generated
	 */
	void unsetBase();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Base</em>' attribute is set.
	 * @see #unsetBase()
	 * @see #getBase()
	 * @see #setBase(ExpressionBase)
	 * @generated
	 */
	boolean isSetBase();

	/**
	 * Returns the value of the '<em><b>Base Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Definition</em>' reference.
	 * @see #setBaseDefinition(MAbstractBaseDefinition)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let r: mrules::expressions::MAbstractBaseDefinition =\r\n\r\nif base=mrules::expressions::ExpressionBase::Undefined \r\n  then null\r\nelse if base=mrules::expressions::ExpressionBase::SelfObject\r\n  then scopeSelf->first() \r\n else if base=mrules::expressions::ExpressionBase::EContainer\r\n  then scopeContainerBase->first() \r\nelse if base=mrules::expressions::ExpressionBase::Target\r\n  then scopeTrg->first() \r\nelse if base=mrules::expressions::ExpressionBase::ObjectObject\r\n  then scopeObj->first() \r\n  else if base=mrules::expressions::ExpressionBase::Source\r\n  then scopeSource->first() \r\nelse if base=mrules::expressions::ExpressionBase::ConstantValue \r\n  then scopeSimpleTypeConstants->select(c:mrules::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = aBaseVar)->first()  \r\nelse if base=mrules::expressions::ExpressionBase::ConstantLiteralValue\r\n  then scopeLiteralConstants->select(c:mrules::expressions::MLiteralConstantBaseDefinition | c.namedConstant = aBaseVar)->first() \r\nelse if base=mrules::expressions::ExpressionBase::ConstantObjectReference\r\n  then scopeObjectReferenceConstants->select(c:mrules::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = aBaseVar)->first()\r\nelse if base=mrules::expressions::ExpressionBase::Variable\r\n  then scopeVariables->select(c:mrules::expressions::MVariableBaseDefinition | c.namedExpression = aBaseVar)->first()  \r\nelse if base=mrules::expressions::ExpressionBase::Parameter\r\n  then scopeParameters->select(c:mrules::expressions::MParameterBaseDefinition | c.aParameter = aBaseVar)->first()  \r\nelse if base=mrules::expressions::ExpressionBase::Iterator\r\n  then scopeIterator->select(c:mrules::expressions::MIteratorBaseDefinition | c.iterator = aBaseVar)->first()  \r\nelse if base=mrules::expressions::ExpressionBase::Accumulator\r\n  then scopeAccumulator->select(c:mrules::expressions::MAccumulatorBaseDefinition | c.accumulator = aBaseVar)->first()  \r\n else if base=mrules::expressions::ExpressionBase::OneValue or base=mrules::expressions::ExpressionBase::ZeroValue  or base = mrules::expressions::ExpressionBase::MinusOneValue\r\n then scopeNumberBase->select(c:mrules::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()\r\nelse scopeBase->select(c:mrules::expressions::MBaseDefinition | c.expressionBase=base)->first() \r\nendif endif endif endif endif endif endif endif endif endif endif endif endif endif\r\nin\r\nif r.oclIsUndefined() then null else r endif' choiceConstruction='self.getScope()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MAbstractBaseDefinition getBaseDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getBaseDefinition <em>Base Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Definition</em>' reference.
	 * @see #getBaseDefinition()
	 * @generated
	 */

	void setBaseDefinition(MAbstractBaseDefinition value);

	/**
	 * Returns the value of the '<em><b>ABase Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABase Var</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABase Var</em>' reference.
	 * @see #isSetABaseVar()
	 * @see #unsetABaseVar()
	 * @see #setABaseVar(AVariable)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_ABaseVar()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Base Var'"
	 * @generated
	 */
	AVariable getABaseVar();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseVar <em>ABase Var</em>}' reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ABase Var</em>' reference.
	 * @see #isSetABaseVar()
	 * @see #unsetABaseVar()
	 * @see #getABaseVar()
	 * @generated
	 */

	void setABaseVar(AVariable value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseVar <em>ABase Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetABaseVar()
	 * @see #getABaseVar()
	 * @see #setABaseVar(AVariable)
	 * @generated
	 */
	void unsetABaseVar();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MAbstractExpressionWithBase#getABaseVar <em>ABase Var</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ABase Var</em>' reference is set.
	 * @see #unsetABaseVar()
	 * @see #getABaseVar()
	 * @see #setABaseVar(AVariable)
	 * @generated
	 */
	boolean isSetABaseVar();

	/**
	 * Returns the value of the '<em><b>ABase Exit Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABase Exit Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABase Exit Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_ABaseExitType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Base Exit Type'"
	 *        annotation="http://www.xocl.org/OCL derive='if baseDefinition.oclIsUndefined() \r\nthen null\r\nelse \r\n  /* Trick b/c we need to reference the container here and baseDefinition is generated on the fly \052/\r\n  if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::SelfObject\r\n    then self.aSelfObjectType\r\n\telse if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Target\r\n    \tthen self.aTargetObjectType\r\n\telse if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::ObjectObject\r\n    \tthen self.aObjectObjectType\r\n    \telse if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Source\r\n    \tthen self.aSrcObjectType\r\n   else if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::EContainer\r\n    \tthen null /* if self.aSelfObjectType.allClassesContainedIn()->size() = 1 then self.aSelfObjectType.allClassesContainedIn()->first() else null endif\052/\r\n    \telse baseDefinition.aClassifier\r\n  \tendif endif endif endif\r\n  endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getABaseExitType();

	/**
	 * Returns the value of the '<em><b>ABase Exit Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABase Exit Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABase Exit Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_ABaseExitSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if baseDefinition.oclIsUndefined()  or not (self.aBaseExitType.oclIsUndefined())\r\nthen acore::classifiers::ASimpleType::None\r\nelse if baseDefinition.calculatedBase=mrules::expressions::ExpressionBase::Target\r\nthen self.aTargetSimpleType\r\nelse self.baseDefinition.aSimpleType\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ASimpleType getABaseExitSimpleType();

	/**
	 * Returns the value of the '<em><b>Base Exit Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Mandatory</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitMandatory()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.aMandatory\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getBaseExitMandatory();

	/**
	 * Returns the value of the '<em><b>Base Exit Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Singular</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen true\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.aSingular\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getBaseExitSingular();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	String baseDefinition$Update(MAbstractBaseDefinition trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='baseAsCode\n'"
	 * @generated
	 */
	String asCodeForBuiltIn();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if baseDefinition.oclIsKindOf(mrules::expressions::MSimpleTypeConstantBaseDefinition)\r\nthen let b: mrules::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mrules::expressions::MSimpleTypeConstantBaseDefinition) in\r\nb.namedConstant.aName\r\nelse if baseDefinition.oclIsKindOf(mrules::expressions::MLiteralConstantBaseDefinition)\r\nthen let b: mrules::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mrules::expressions::MLiteralConstantBaseDefinition) in\r\nb.namedConstant.aName\r\nelse null endif endif'"
	 * @generated
	 */
	String asCodeForConstants();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='let v: mrules::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mrules::expressions::MVariableBaseDefinition) in\r\nlet vName: String = v.namedExpression.aName in\r\nbaseAsCode'"
	 * @generated
	 */
	String asCodeForVariables();

} // MAbstractExpressionWithBase
