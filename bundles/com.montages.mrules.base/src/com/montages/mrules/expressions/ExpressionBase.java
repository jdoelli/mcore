/**
 */
package com.montages.mrules.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Expression Base</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage#getExpressionBase()
 * @model
 * @generated
 */
public enum ExpressionBase implements Enumerator {
	/**
	 * The '<em><b>Undefined</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED_VALUE
	 * @generated
	 * @ordered
	 */
	UNDEFINED(0, "Undefined", "Undefined"),

	/**
	 * The '<em><b>Self Object</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELF_OBJECT_VALUE
	 * @generated
	 * @ordered
	 */
	SELF_OBJECT(1, "SelfObject", "SelfObject"),

	/**
	 * The '<em><b>Null Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NULL_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	NULL_VALUE(2, "NullValue", "NullValue"),

	/**
	 * The '<em><b>Zero Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ZERO_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ZERO_VALUE(3, "ZeroValue", "ZeroValue"),

	/**
	 * The '<em><b>One Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ONE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	ONE_VALUE(4, "OneValue", "OneValue"),

	/**
	 * The '<em><b>Minus One Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	MINUS_ONE_VALUE(5, "MinusOneValue", "MinusOneValue"),

	/**
	 * The '<em><b>False Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FALSE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	FALSE_VALUE(6, "FalseValue", "FalseValue"),

	/**
	 * The '<em><b>True Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TRUE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	TRUE_VALUE(7, "TrueValue", "TrueValue"),

	/**
	 * The '<em><b>Empty String Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EMPTY_STRING_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	EMPTY_STRING_VALUE(8, "EmptyStringValue", "EmptyStringValue"),

	/**
	 * The '<em><b>Constant Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTANT_VALUE(9, "ConstantValue", "ConstantValue"),

	/**
	 * The '<em><b>Constant Literal Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_LITERAL_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTANT_LITERAL_VALUE(10, "ConstantLiteralValue", "ConstantLiteralValue"),

	/**
	 * The '<em><b>Constant Object Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_OBJECT_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTANT_OBJECT_REFERENCE(11, "ConstantObjectReference", "ConstantObjectReference"),

	/**
	 * The '<em><b>Target</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #TARGET_VALUE
	 * @generated
	 * @ordered
	 */
	TARGET(12, "Target", "Target"),

	/**
	 * The '<em><b>System</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SYSTEM_VALUE
	 * @generated
	 * @ordered
	 */
	SYSTEM(13, "System", "System"),

	/**
	 * The '<em><b>Variable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #VARIABLE_VALUE
	 * @generated
	 * @ordered
	 */
	VARIABLE(14, "Variable", "Variable"),

	/**
	 * The '<em><b>Iterator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ITERATOR_VALUE
	 * @generated
	 * @ordered
	 */
	ITERATOR(15, "Iterator", "Iterator"),

	/**
	 * The '<em><b>Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	PARAMETER(16, "Parameter", "Parameter"),

	/**
	 * The '<em><b>Empty Collection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #EMPTY_COLLECTION_VALUE
	 * @generated
	 * @ordered
	 */
	EMPTY_COLLECTION(17, "EmptyCollection", "EmptyCollection"),

	/**
	 * The '<em><b>Accumulator</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ACCUMULATOR_VALUE
	 * @generated
	 * @ordered
	 */
	ACCUMULATOR(18, "Accumulator", "Accumulator"),

	/**
	 * The '<em><b>Object Object</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OBJECT_OBJECT_VALUE
	 * @generated
	 * @ordered
	 */
	OBJECT_OBJECT(19, "ObjectObject", "ObjectObject"),

	/**
	 * The '<em><b>EContainer</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ECONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	ECONTAINER(20, "EContainer", "EContainer"),

	/**
	 * The '<em><b>Source</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SOURCE_VALUE
	 * @generated
	 * @ordered
	 */
	SOURCE(21, "Source", "src");

	/**
	 * The '<em><b>Undefined</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Undefined</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNDEFINED
	 * @model name="Undefined"
	 * @generated
	 * @ordered
	 */
	public static final int UNDEFINED_VALUE = 0;

	/**
	 * The '<em><b>Self Object</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Self Object</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELF_OBJECT
	 * @model name="SelfObject"
	 * @generated
	 * @ordered
	 */
	public static final int SELF_OBJECT_VALUE = 1;

	/**
	 * The '<em><b>Null Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Null Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NULL_VALUE
	 * @model name="NullValue"
	 * @generated
	 * @ordered
	 */
	public static final int NULL_VALUE_VALUE = 2;

	/**
	 * The '<em><b>Zero Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Zero Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ZERO_VALUE
	 * @model name="ZeroValue"
	 * @generated
	 * @ordered
	 */
	public static final int ZERO_VALUE_VALUE = 3;

	/**
	 * The '<em><b>One Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>One Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ONE_VALUE
	 * @model name="OneValue"
	 * @generated
	 * @ordered
	 */
	public static final int ONE_VALUE_VALUE = 4;

	/**
	 * The '<em><b>Minus One Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Minus One Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MINUS_ONE_VALUE
	 * @model name="MinusOneValue"
	 * @generated
	 * @ordered
	 */
	public static final int MINUS_ONE_VALUE_VALUE = 5;

	/**
	 * The '<em><b>False Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>False Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FALSE_VALUE
	 * @model name="FalseValue"
	 * @generated
	 * @ordered
	 */
	public static final int FALSE_VALUE_VALUE = 6;

	/**
	 * The '<em><b>True Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>True Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TRUE_VALUE
	 * @model name="TrueValue"
	 * @generated
	 * @ordered
	 */
	public static final int TRUE_VALUE_VALUE = 7;

	/**
	 * The '<em><b>Empty String Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Empty String Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EMPTY_STRING_VALUE
	 * @model name="EmptyStringValue"
	 * @generated
	 * @ordered
	 */
	public static final int EMPTY_STRING_VALUE_VALUE = 8;

	/**
	 * The '<em><b>Constant Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constant Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_VALUE
	 * @model name="ConstantValue"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTANT_VALUE_VALUE = 9;

	/**
	 * The '<em><b>Constant Literal Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constant Literal Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_LITERAL_VALUE
	 * @model name="ConstantLiteralValue"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTANT_LITERAL_VALUE_VALUE = 10;

	/**
	 * The '<em><b>Constant Object Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constant Object Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTANT_OBJECT_REFERENCE
	 * @model name="ConstantObjectReference"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTANT_OBJECT_REFERENCE_VALUE = 11;

	/**
	 * The '<em><b>Target</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Target</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #TARGET
	 * @model name="Target"
	 * @generated
	 * @ordered
	 */
	public static final int TARGET_VALUE = 12;

	/**
	 * The '<em><b>System</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>System</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SYSTEM
	 * @model name="System"
	 * @generated
	 * @ordered
	 */
	public static final int SYSTEM_VALUE = 13;

	/**
	 * The '<em><b>Variable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Variable</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #VARIABLE
	 * @model name="Variable"
	 * @generated
	 * @ordered
	 */
	public static final int VARIABLE_VALUE = 14;

	/**
	 * The '<em><b>Iterator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Iterator</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ITERATOR
	 * @model name="Iterator"
	 * @generated
	 * @ordered
	 */
	public static final int ITERATOR_VALUE = 15;

	/**
	 * The '<em><b>Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PARAMETER
	 * @model name="Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int PARAMETER_VALUE = 16;

	/**
	 * The '<em><b>Empty Collection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Empty Collection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #EMPTY_COLLECTION
	 * @model name="EmptyCollection"
	 * @generated
	 * @ordered
	 */
	public static final int EMPTY_COLLECTION_VALUE = 17;

	/**
	 * The '<em><b>Accumulator</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Accumulator</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ACCUMULATOR
	 * @model name="Accumulator"
	 * @generated
	 * @ordered
	 */
	public static final int ACCUMULATOR_VALUE = 18;

	/**
	 * The '<em><b>Object Object</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Object Object</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OBJECT_OBJECT
	 * @model name="ObjectObject"
	 * @generated
	 * @ordered
	 */
	public static final int OBJECT_OBJECT_VALUE = 19;

	/**
	 * The '<em><b>EContainer</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>EContainer</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ECONTAINER
	 * @model name="EContainer"
	 *        annotation="http://www.montages.com/mCore/MCore mName='EContainer'"
	 * @generated
	 * @ordered
	 */
	public static final int ECONTAINER_VALUE = 20;

	/**
	 * The '<em><b>Source</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Source</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SOURCE
	 * @model name="Source" literal="src"
	 * @generated
	 * @ordered
	 */
	public static final int SOURCE_VALUE = 21;

	/**
	 * An array of all the '<em><b>Expression Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final ExpressionBase[] VALUES_ARRAY = new ExpressionBase[] { UNDEFINED, SELF_OBJECT, NULL_VALUE,
			ZERO_VALUE, ONE_VALUE, MINUS_ONE_VALUE, FALSE_VALUE, TRUE_VALUE, EMPTY_STRING_VALUE, CONSTANT_VALUE,
			CONSTANT_LITERAL_VALUE, CONSTANT_OBJECT_REFERENCE, TARGET, SYSTEM, VARIABLE, ITERATOR, PARAMETER,
			EMPTY_COLLECTION, ACCUMULATOR, OBJECT_OBJECT, ECONTAINER, SOURCE, };

	/**
	 * A public read-only list of all the '<em><b>Expression Base</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<ExpressionBase> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Expression Base</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExpressionBase get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExpressionBase result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Expression Base</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExpressionBase getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ExpressionBase result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Expression Base</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExpressionBase get(int value) {
		switch (value) {
		case UNDEFINED_VALUE:
			return UNDEFINED;
		case SELF_OBJECT_VALUE:
			return SELF_OBJECT;
		case NULL_VALUE_VALUE:
			return NULL_VALUE;
		case ZERO_VALUE_VALUE:
			return ZERO_VALUE;
		case ONE_VALUE_VALUE:
			return ONE_VALUE;
		case MINUS_ONE_VALUE_VALUE:
			return MINUS_ONE_VALUE;
		case FALSE_VALUE_VALUE:
			return FALSE_VALUE;
		case TRUE_VALUE_VALUE:
			return TRUE_VALUE;
		case EMPTY_STRING_VALUE_VALUE:
			return EMPTY_STRING_VALUE;
		case CONSTANT_VALUE_VALUE:
			return CONSTANT_VALUE;
		case CONSTANT_LITERAL_VALUE_VALUE:
			return CONSTANT_LITERAL_VALUE;
		case CONSTANT_OBJECT_REFERENCE_VALUE:
			return CONSTANT_OBJECT_REFERENCE;
		case TARGET_VALUE:
			return TARGET;
		case SYSTEM_VALUE:
			return SYSTEM;
		case VARIABLE_VALUE:
			return VARIABLE;
		case ITERATOR_VALUE:
			return ITERATOR;
		case PARAMETER_VALUE:
			return PARAMETER;
		case EMPTY_COLLECTION_VALUE:
			return EMPTY_COLLECTION;
		case ACCUMULATOR_VALUE:
			return ACCUMULATOR;
		case OBJECT_OBJECT_VALUE:
			return OBJECT_OBJECT;
		case ECONTAINER_VALUE:
			return ECONTAINER;
		case SOURCE_VALUE:
			return SOURCE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private ExpressionBase(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //ExpressionBase
