/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.AAbstractFolder;

import com.montages.acore.classifiers.ASimpleType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MApplication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MApplication#getOperator <em>Operator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MApplication#getOperands <em>Operands</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MApplication#getContainedCollector <em>Contained Collector</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MApplication#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMApplication()
 * @model annotation="http://www.xocl.org/OCL label='if operands->forAll(oclIsKindOf(MChain))\r\nthen /* the operand applies only to chains, we show the entire text \052/\r\n\tasCode\r\nelse /* we show only the operator \052/\r\n\toperatorAsCode()\r\nendif'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let kind: String = \'APPLY\' in\nkind\n' aCalculatedOwnTypeDerive='if\r\n\toperator=MOperator::SetFirst  or operator=MOperator::SetLast\r\n\tor operator=MOperator::SetIncluding or operator=MOperator::SetExcluding\r\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet\t\r\n    or operator=MOperator::At  or operator=MOperator::Intersection then \r\n\tlet x: acore::classifiers::AClassifier = self.operands->first().aClassifier in\r\n\tif x.oclIsUndefined() then null else x endif\r\nelse null\r\nendif' aCalculatedOwnSimpleTypeDerive='if\r\n   operator=MOperator::Concat or operator=MOperator::ToString or operator=MOperator::Trim or operator=MOperator::SubString\r\nthen acore::classifiers::ASimpleType::String\r\nelse if \r\n\toperator=MOperator::Equal  or operator=MOperator::NotEqual  or operator=MOperator::Greater\r\n\tor operator=MOperator::Smaller  or operator=MOperator::GreaterEqual or operator=MOperator::SmallerEqual\r\n\tor operator=MOperator::NotOp  or operator=MOperator::OclAnd  or operator=MOperator::OclOr\r\n\tor operator=MOperator::XorOp or operator=MOperator::ImpliesOp or operator=MOperator::SetIsEmpty \r\n\tor operator=MOperator::SetNotEmpty  or operator=MOperator::SetIncludes or operator=MOperator::SetExcludes\r\n\tor operator=MOperator::ImpliesOp or self.definesOperatorBusinessLogic() or operator=MOperator::SetIncludesAll or operator=MOperator::SetExcludesAll\r\n\t\r\nthen acore::classifiers::ASimpleType::Boolean\r\nelse if \r\n\toperator=MOperator::SetSize or operator=MOperator::IndexOf or \r\n\t(operator = MOperator::DiffInDays) or\r\n (operator = MOperator::DiffInHrs) or\r\n  (operator = MOperator::DiffInMinutes) or\r\n   (operator = MOperator::DiffInMonth) or\r\n    (operator = MOperator::DiffInSeconds) or\r\n     (operator = MOperator::DiffInYears)\r\nthen acore::classifiers::ASimpleType::Integer\r\nelse if \r\n\tself.operator=MOperator::Plus  or operator=MOperator::Minus or operator= MOperator::Times \r\n\tor operator= MOperator::Divide  or operator= MOperator::Div or operator= MOperator::Mod\r\n\tor operator=MOperator::SetFirst  or operator=MOperator::SetLast or operator=MOperator::SetSum\r\n\tor operator=MOperator::SetIncluding or operator=MOperator::SetExcluding\r\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet or operator=MOperator::At  or operator=MOperator::Intersection\r\nthen \r\n   self.getCalculatedSimpleDifferentTypes()\r\n--\tlet x: SimpleType = self.operands->first().calculatedSimpleType in \r\n--\tif x.oclIsUndefined() then SimpleType::None else x endif\r\nelse if \r\n\t(operator= MOperator::OclIsInvalid) or (operator= MOperator::OclIsUndefined) then acore::classifiers::ASimpleType::Boolean\r\nelse acore::classifiers::ASimpleType::None endif endif endif endif endif' calculatedOwnSingularDerive='if\n\toperator=MOperator::SetIncluding or operator=MOperator::SetExcluding\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet\t\n\tor operator=MOperator::Intersection\nthen false\nelse true\nendif' isComplexExpressionDerive='isOperatorInfix()' asBasicCodeDerive='let varName : String = \'e\'.concat(self.uniqueApplyNumber().toString()) in\r\n\r\n\'let \'.concat(varName).concat(\': \').concat( aTypeAsOcl(aSelfObjectPackage, aClassifier, aSimpleType, aSingular) )\r\n.concat(\' = \').concat(\r\nif operands->size() = 0 then \'\' else\r\nlet frst: MChainOrApplication = operands->first() in\r\nlet frstCode: String = (if frst.isComplexExpression then \'(\' else \'\' endif).concat(frst.asCode).concat(if frst.isComplexExpression then \')\' else \'\' endif) in\r\n\r\n\r\nif self.definesOperatorBusinessLogic() then self.codeForLogicOperator()\r\nelse\r\nif isOperatorUnaryFunction()\r\nthen\r\n\tfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\'.\').concat(operatorAsCode()).concat(\'(\').concat(x.asCode).concat(\')\')\r\n\t\t ) )\r\n\t\r\nelse if isOperatorSetOperator()\r\nthen\r\nlet appCode : String = if self.operands->size() = 1 then frstCode.concat(\'->\').concat(operatorAsCode()).concat(\'()\') else \r\nfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\nx: MChainOrApplication; s: String = \'\' | \r\n\t s.concat(\'->\').concat(operatorAsCode())\r\n\t.concat(\'(\').concat(x.asCode).concat(\')\')\r\n\t) ).concat(if self.aSingular then \'  \' else \' ->asOrderedSet()  \'  endif)\r\n\tendif in appCode\r\n\t\r\nelse if self.isOperatorUnaryFunctionTwoParam()\r\nthen\r\n\tfrstCode.concat(\'.\').concat(operatorAsCode()).concat(\'(\').concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(x.asCode).concat( if (operands->indexOf(x) = operands->size()) then \'\' else \', \' endif)\r\n\t\t--  s.concat(x.asCode).concat(\',\').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined() then \'1\' else operands->at(operands->indexOf(x)+1).asCode endif)\r\n\t\t ) ).concat(\')\')\r\nelse if isOperatorInfix() \r\nthen\r\n\tfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\' \').concat(operatorAsCode()).concat(\' \').concat(\r\n\t\t    (if x.isComplexExpression then \'(\' else \'\' endif).concat(x.asCode).concat(if x.isComplexExpression then \')\' else \'\' endif)\r\n\t\t  )\r\n\t\t) \r\n\t)\r\nelse if isOperatorPrefix() \r\nthen\r\n\toperatorAsCode().concat(\'(\').concat(frstCode).concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\', \').concat(\r\n\t\t    (if x.isComplexExpression then \'(\' else \'\' endif).concat(x.asCode).concat(if x.isComplexExpression then \')\' else \'\' endif)\r\n\t\t  )\r\n\t\t) \r\n\t).concat(\')\')\r\nelse\r\n\tfrstCode.concat(\'.\').concat(operatorAsCode()).concat(\'()\')\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\n).concat(if self.aSingular then \' in \\n if \'.concat(varName).concat(\'.oclIsInvalid() \').concat(\'then \') else \' in \\n    if \'.concat(varName).concat(\'->oclIsInvalid() then \') endif)\r\n.concat(if self.aSingular then \'null\' else \'OrderedSet{}\' endif)\r\n.concat(\' else \').concat(varName).concat(\' endif\')' collectorDerive='containedCollector'"
 * @generated
 */

public interface MApplication extends MChainOrApplication, AAbstractFolder {
	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see com.montages.mrules.expressions.MOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #setOperator(MOperator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMApplication_Operator()
	 * @model unsettable="true"
	 * @generated
	 */
	MOperator getOperator();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MApplication#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see com.montages.mrules.expressions.MOperator
	 * @see #isSetOperator()
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @generated
	 */

	void setOperator(MOperator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MApplication#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperator()
	 * @see #getOperator()
	 * @see #setOperator(MOperator)
	 * @generated
	 */
	void unsetOperator();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MApplication#getOperator <em>Operator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operator</em>' attribute is set.
	 * @see #unsetOperator()
	 * @see #getOperator()
	 * @see #setOperator(MOperator)
	 * @generated
	 */
	boolean isSetOperator();

	/**
	 * Returns the value of the '<em><b>Operands</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MChainOrApplication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operands</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operands</em>' containment reference list.
	 * @see #isSetOperands()
	 * @see #unsetOperands()
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMApplication_Operands()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MChainOrApplication> getOperands();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MApplication#getOperands <em>Operands</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperands()
	 * @see #getOperands()
	 * @generated
	 */
	void unsetOperands();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MApplication#getOperands <em>Operands</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operands</em>' containment reference list is set.
	 * @see #unsetOperands()
	 * @see #getOperands()
	 * @generated
	 */
	boolean isSetOperands();

	/**
	 * Returns the value of the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contained Collector</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMApplication_ContainedCollector()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='null'"
	 * @generated
	 */
	MCollectionExpression getContainedCollector();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MApplication#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contained Collector</em>' containment reference.
	 * @see #isSetContainedCollector()
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @generated
	 */

	void setContainedCollector(MCollectionExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MApplication#getContainedCollector <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	void unsetContainedCollector();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MApplication#getContainedCollector <em>Contained Collector</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contained Collector</em>' containment reference is set.
	 * @see #unsetContainedCollector()
	 * @see #getContainedCollector()
	 * @see #setContainedCollector(MCollectionExpression)
	 * @generated
	 */
	boolean isSetContainedCollector();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.MApplicationAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mrules.expressions.MApplicationAction
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMApplication_DoAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mrules::expressions::MApplicationAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MApplicationAction getDoAction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.operator=MOperator::Equal then \'=\' \r\nelse if self.operator=MOperator::NotEqual then \'<>\'\r\nelse if self.operator=MOperator::Greater then \'>\'\r\nelse if self.operator=MOperator::Smaller then \'<\'\r\nelse if self.operator=MOperator::GreaterEqual then \'>=\'\r\nelse if self.operator=MOperator::SmallerEqual then \'<=\'\r\nelse if self.operator=MOperator::NotOp then \'not\'\r\nelse if self.operator=MOperator::OclAnd then \'and\'\r\nelse if self.operator=MOperator::OclOr then \'or\'\r\nelse if self.operator=MOperator::XorOp then \'xor\'\r\nelse if self.operator=MOperator::ImpliesOp then \'implies\'\r\nelse if self.operator=MOperator::Plus then \'+\'\r\nelse if self.operator=MOperator::Minus then \'-\'\r\nelse if self .operator= MOperator::Times then \'*\'\r\nelse if self .operator= MOperator::Divide then \'/\'\r\nelse if self .operator= MOperator::Div then \'div\'\r\nelse if self .operator= MOperator::Mod then \'mod\'\r\nelse if self .operator= MOperator::Concat then \'concat\'\r\nelse if self .operator= MOperator::OclIsInvalid then \'oclIsInvalid\'\r\nelse if self .operator= MOperator::OclIsUndefined then \'oclIsUndefined\'\r\nelse if self .operator= MOperator::SetFirst then \'first\'\r\nelse if self .operator= MOperator::SetIsEmpty then \'isEmpty\'\r\nelse if self .operator= MOperator::SetLast then \'last\'\r\nelse if self .operator= MOperator::SetNotEmpty then \'notEmpty\'\r\nelse if self .operator= MOperator::SetSize then \'size\'\r\nelse if self .operator= MOperator::SetSum then \'sum\'\r\nelse if self .operator= MOperator::SetIncluding then \'including\'\r\n\r\nelse if self .operator= MOperator::SetIncludesAll then \'includesAll\'\r\nelse if self .operator= MOperator::SetExcludesAll then \'excludesAll\'\r\nelse if self .operator= MOperator::Intersection then \'intersection\'\r\nelse if (operator = MOperator::DiffInDays) then \'diffInDays\'\r\n else if (operator = MOperator::DiffInHrs)   then \'diffInHours\'\r\n else if  (operator = MOperator::DiffInMinutes) then \'diffInMinutes\'\r\n else if  (operator = MOperator::DiffInMonth) then \'diffInMonths\'\r\n else if   (operator = MOperator::DiffInSeconds) then \'diffInSeconds\'\r\nelse if   (operator = MOperator::DiffInYears) then \'diffInYears\'\r\n\r\nelse if self .operator= MOperator::SetExcluding then \'excluding\'\r\nelse if self .operator= MOperator::SetUnion then \'union\'\r\nelse if self .operator= MOperator::SetIncludes then \'includes\'\r\nelse if self .operator= MOperator::SetExcludes then \'excludes\'\r\nelse if self .operator= MOperator::IndexOf then \'indexOf\'\r\nelse if self .operator= MOperator::At then \'at\'\r\nelse if self.operator=MOperator::SetAsOrderedSet then \'asOrderedSet\'\r\nelse if self. operator=MOperator::ImpliesOp then \'implies\' \r\nelse if self. operator=MOperator::ToString then \'toString\'\r\nelse if self. operator=MOperator::Trim then \'trim\'\r\nelse if self.operator= MOperator::SubString then \'substring\'\r\n\r\nelse \'ERROR\'  endif endif endif endif endif endif endif endif endif\r\nendif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif'"
	 * @generated
	 */
	String operatorAsCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false \r\nelse (operator=MOperator::NotOp) endif'"
	 * @generated
	 */
	Boolean isOperatorPrefix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='(not isOperatorPrefix()) and (not isOperatorPostfix()) and (not isOperatorSetOperator())\r\n'"
	 * @generated
	 */
	Boolean isOperatorInfix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false else\r\n(operator = MOperator::OclIsInvalid) or (operator = MOperator::OclIsUndefined) \r\nor (operator = MOperator::ToString) or (operator = MOperator::Trim) or isOperatorSetOperator() \r\nendif'"
	 * @generated
	 */
	Boolean isOperatorPostfix();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false else\r\n (operator = MOperator::Concat) or\r\n (operator = MOperator::DiffInDays) or\r\n (operator = MOperator::DiffInHrs) or\r\n  (operator = MOperator::DiffInMinutes) or\r\n   (operator = MOperator::DiffInMonth) or\r\n    (operator = MOperator::DiffInSeconds) or\r\n     (operator = MOperator::DiffInYears)\r\n   endif'"
	 * @generated
	 */
	Boolean isOperatorUnaryFunction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false \r\nelse\r\noperator= MOperator::SetFirst or\r\noperator= MOperator::SetIsEmpty or\r\noperator= MOperator::SetLast or\r\noperator= MOperator::SetNotEmpty or\r\noperator= MOperator::SetSize or\r\noperator= MOperator::SetSum or\r\noperator= MOperator::SetIncluding or\r\noperator= MOperator::SetExcluding or\r\noperator= MOperator::SetUnion or\r\noperator= MOperator::SetIncludes or\r\noperator= MOperator::SetExcludes or\r\noperator= MOperator::IndexOf or\r\noperator= MOperator::At or \r\noperator=MOperator::SetAsOrderedSet or\r\noperator=MOperator::SetExcludesAll or\r\noperator=MOperator::SetIncludesAll or\r\noperator=MOperator::Intersection \r\nendif'"
	 * @generated
	 */
	Boolean isOperatorSetOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false else\r\n (operator = MOperator::SubString)  endif'"
	 * @generated
	 */
	Boolean isOperatorUnaryFunctionTwoParam();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false else\r\n (operator = MOperator::SetExcluding)  or \r\n  (operator = MOperator::SetIncluding) or  \r\n  (operator = MOperator::At) or\r\n  (operator = MOperator::IndexOf) or\r\n  (operator = MOperator::SubString) or\r\n  (operator = MOperator::SetExcludes) or\r\n  (operator = MOperator::SetIncludes)\r\n  \r\n  endif'"
	 * @generated
	 */
	Boolean isOperatorParameterUnary();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='UniqueApplyNumber'"
	 *        annotation="http://www.xocl.org/OCL body='let chain : Integer =\r\nif self.eContainer().oclAsType(MNamedExpression).oclIsUndefined() then \r\nself.eContainer()->closure(eContainer())->select(oclIsTypeOf(MNamedExpression)).oclAsType(MNamedExpression)->asOrderedSet()->first().allApplications()->indexOf(self)\r\nelse\r\nself.eContainer().oclAsType(MNamedExpression).allApplications()->indexOf(self)\r\nendif\r\nin if chain.oclIsUndefined() then 0 else chain endif'"
	 * @generated
	 */
	Integer uniqueApplyNumber();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Defines Operator BusinessLogic'"
	 *        annotation="http://www.xocl.org/OCL body='if operator.oclIsUndefined() then false else\r\n (operator = MOperator::AndOp) or\r\n  (operator = MOperator::OrOp) or\r\n    (operator = MOperator::BusinessNOT) or\r\n    (operator = MOperator::BusinessImplies)\r\n  endif'"
	 * @generated
	 */
	Boolean definesOperatorBusinessLogic();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='codeForLogicOperator'"
	 *        annotation="http://www.xocl.org/OCL body='if operands->first().oclIsUndefined() then \' \'\r\n\r\nelse\r\n\r\nlet frst: MChainOrApplication =  operands->first() in\r\nlet frstCode: String = (if frst.isComplexExpression then \'(\' else \'\' endif).concat(frst.asCode).concat(if frst.isComplexExpression then \')\' else \'\' endif) in\r\nlet businessExpr : String = \'\' in\r\nif operator= MOperator::AndOp or operator = MOperator::OrOp\r\nthen\r\n\r\nlet checkPart : String = if  operator= MOperator::AndOp then \'false\' else \'true\' endif\r\nin\r\nlet elsePart : String = if operator = MOperator::AndOp then \'true\' else \'false\' endif in\r\nbusinessExpr.concat(\'if (\').concat(frstCode).concat(\')= \').concat(checkPart).concat(\' \\n then \').concat(checkPart).concat(\' \\n\').concat(operands->excluding(frst)->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\' else if (\').concat(x.asCode).concat(\')= \' ).concat(checkPart).concat(\' \\n\').concat(\' then \').concat(checkPart).concat(\' \\n\'))).concat(\'else if (\')\r\n.concat(operands->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\'(\').concat(x.asCode).concat(\')= null\').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined()  then \'\' else \' or \' endif))).concat(\') = true \\n then null \\n else \').concat(elsePart).concat(\' \')\r\n.concat(operands->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\'endif \'))).concat(\'endif\')\r\n\r\nelse if operator = MOperator::BusinessNOT\t\r\nthen\r\n\r\nbusinessExpr.concat(\'if (\').concat(frst.asCode).concat(\')= true \\n then false \\n else if (\').concat(frst.asCode).concat(\')= false \\n then true \\n else null endif endif \\n \')\r\n\t\t \r\nelse \r\n\'\'\r\n\r\nendif\r\nendif\r\nendif'"
	 * @generated
	 */
	String codeForLogicOperator();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='getCalculatedSimpleDifferentTypes'"
	 *        annotation="http://www.xocl.org/OCL body='\tlet x: OrderedSet(acore::classifiers::ASimpleType) = self.operands->collect(aSimpleType)->asOrderedSet() in \r\n\t\r\n\tif x->oclIsUndefined() then acore::classifiers::ASimpleType::None \r\n\telse if x->includesAll(OrderedSet{acore::classifiers::ASimpleType::Integer,acore::classifiers::ASimpleType::Real}) then acore::classifiers::ASimpleType::Real\r\n\t--else if x->size() > 1 then SimpleType::None\r\n\telse\r\n\tx->first() endif endif--endif'"
	 * @generated
	 */
	ASimpleType getCalculatedSimpleDifferentTypes();

} // MApplication
