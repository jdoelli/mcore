/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNumber Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MNumberBaseDefinition#getExpressionBase <em>Expression Base</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNumberBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<builtin> \'.concat(calculatedAsCode)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='expressionBase\n' calculatedAsCodeDerive=' if expressionBase=ExpressionBase::ZeroValue then \'0\'\r\nelse if expressionBase=ExpressionBase::OneValue then \'1\'\r\nelse if expressionBase=ExpressionBase::MinusOneValue then \'-1\'\r\nelse \'\' endif endif endif' aSimpleTypeDerive='if expressionBase=ExpressionBase::NullValue or expressionBase=ExpressionBase::EmptyCollection then acore::classifiers::ASimpleType::None\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then acore::classifiers::ASimpleType::String\r\nelse if expressionBase=ExpressionBase::ZeroValue \r\n  or expressionBase=ExpressionBase::OneValue \r\n  or expressionBase=ExpressionBase::MinusOneValue then acore::classifiers::ASimpleType::Integer\r\nelse acore::classifiers::ASimpleType::Boolean endif endif endif' aSingularDerive='true\n'"
 * @generated
 */

public interface MNumberBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Expression Base</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.ExpressionBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Base</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionBase
	 * @see #isSetExpressionBase()
	 * @see #unsetExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNumberBaseDefinition_ExpressionBase()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	ExpressionBase getExpressionBase();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MNumberBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Base</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionBase
	 * @see #isSetExpressionBase()
	 * @see #unsetExpressionBase()
	 * @see #getExpressionBase()
	 * @generated
	 */

	void setExpressionBase(ExpressionBase value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MNumberBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpressionBase()
	 * @see #getExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @generated
	 */
	void unsetExpressionBase();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MNumberBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression Base</em>' attribute is set.
	 * @see #unsetExpressionBase()
	 * @see #getExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @generated
	 */
	boolean isSetExpressionBase();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model callerAnnotation="http://www.montages.com/mCore/MCore mName='caller'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='calculateAsCode'"
	 *        annotation="http://www.xocl.org/OCL body='\r\nself.calculatedAsCode.concat(if caller.aExpectedReturnSimpleType = acore::classifiers::ASimpleType::Real then \'.0\' else \'\'endif)\r\n\r\n\r\n\r\n'"
	 * @generated
	 */
	String calculateAsCode(MAbstractExpression caller);

} // MNumberBaseDefinition
