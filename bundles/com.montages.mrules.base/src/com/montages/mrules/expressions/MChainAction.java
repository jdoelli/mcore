/**
 */
package com.montages.mrules.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MChain Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMChainAction()
 * @model
 * @generated
 */
public enum MChainAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Into Data Constant</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_DATA_CONSTANT_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_DATA_CONSTANT(1, "IntoDataConstant", "-> Data Constant"),

	/**
	 * The '<em><b>Into Literal Constant</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_LITERAL_CONSTANT_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_LITERAL_CONSTANT(2, "IntoLiteralConstant", "-> Literal Constant"),

	/**
	 * The '<em><b>Into Apply</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_APPLY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_APPLY(3, "IntoApply", "-> Apply"),

	/**
	 * The '<em><b>Into Cond Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_COND_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_COND_OF_IF_THEN_ELSE(4, "IntoCondOfIfThenElse", "-> Cond of If-Then-Else\n\n   Constraint"),

	/**
	 * The '<em><b>Into Then Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_THEN_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_THEN_OF_IF_THEN_ELSE(5, "IntoThenOfIfThenElse", "-> Then of If-Then-Else\n\n   Constraint"),

	/**
	 * The '<em><b>Into Else Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_ELSE_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_ELSE_OF_IF_THEN_ELSE(6, "IntoElseOfIfThenElse", "-> Else of If-Then-Else\n\n   Constraint"),

	/**
	 * The '<em><b>Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	ARGUMENT(7, "Argument", "+ Argument"),

	/**
	 * The '<em><b>Data Constant As Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_CONSTANT_AS_ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_CONSTANT_AS_ARGUMENT(8, "DataConstantAsArgument", "+ Data Constant as Argument"),

	/**
	 * The '<em><b>Literal Constant As Argument</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_CONSTANT_AS_ARGUMENT_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL_CONSTANT_AS_ARGUMENT(9, "LiteralConstantAsArgument", "+ Literal Constant as Argument"),

	/**
	 * The '<em><b>Sub Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_CHAIN(10, "SubChain", "+ Sub Chain"),

	/**
	 * The '<em><b>Collection Op</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COLLECTION_OP_VALUE
	 * @generated
	 * @ordered
	 */
	COLLECTION_OP(11, "CollectionOp", "+ Collection Op");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Into Data Constant</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Data Constant</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_DATA_CONSTANT
	 * @model name="IntoDataConstant" literal="-> Data Constant"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_DATA_CONSTANT_VALUE = 1;

	/**
	 * The '<em><b>Into Literal Constant</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Literal Constant</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_LITERAL_CONSTANT
	 * @model name="IntoLiteralConstant" literal="-> Literal Constant"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_LITERAL_CONSTANT_VALUE = 2;

	/**
	 * The '<em><b>Into Apply</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Apply</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_APPLY
	 * @model name="IntoApply" literal="-> Apply"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_APPLY_VALUE = 3;

	/**
	 * The '<em><b>Into Cond Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Cond Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_COND_OF_IF_THEN_ELSE
	 * @model name="IntoCondOfIfThenElse" literal="-> Cond of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_COND_OF_IF_THEN_ELSE_VALUE = 4;

	/**
	 * The '<em><b>Into Then Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Then Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_THEN_OF_IF_THEN_ELSE
	 * @model name="IntoThenOfIfThenElse" literal="-> Then of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_THEN_OF_IF_THEN_ELSE_VALUE = 5;

	/**
	 * The '<em><b>Into Else Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Else Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_ELSE_OF_IF_THEN_ELSE
	 * @model name="IntoElseOfIfThenElse" literal="-> Else of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_ELSE_OF_IF_THEN_ELSE_VALUE = 6;

	/**
	 * The '<em><b>Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ARGUMENT
	 * @model name="Argument" literal="+ Argument"
	 * @generated
	 * @ordered
	 */
	public static final int ARGUMENT_VALUE = 7;

	/**
	 * The '<em><b>Data Constant As Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Data Constant As Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATA_CONSTANT_AS_ARGUMENT
	 * @model name="DataConstantAsArgument" literal="+ Data Constant as Argument"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_CONSTANT_AS_ARGUMENT_VALUE = 8;

	/**
	 * The '<em><b>Literal Constant As Argument</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal Constant As Argument</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL_CONSTANT_AS_ARGUMENT
	 * @model name="LiteralConstantAsArgument" literal="+ Literal Constant as Argument"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_CONSTANT_AS_ARGUMENT_VALUE = 9;

	/**
	 * The '<em><b>Sub Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sub Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_CHAIN
	 * @model name="SubChain" literal="+ Sub Chain"
	 * @generated
	 * @ordered
	 */
	public static final int SUB_CHAIN_VALUE = 10;

	/**
	 * The '<em><b>Collection Op</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Collection Op</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COLLECTION_OP
	 * @model name="CollectionOp" literal="+ Collection Op"
	 * @generated
	 * @ordered
	 */
	public static final int COLLECTION_OP_VALUE = 11;

	/**
	 * An array of all the '<em><b>MChain Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MChainAction[] VALUES_ARRAY = new MChainAction[] { DO, INTO_DATA_CONSTANT,
			INTO_LITERAL_CONSTANT, INTO_APPLY, INTO_COND_OF_IF_THEN_ELSE, INTO_THEN_OF_IF_THEN_ELSE,
			INTO_ELSE_OF_IF_THEN_ELSE, ARGUMENT, DATA_CONSTANT_AS_ARGUMENT, LITERAL_CONSTANT_AS_ARGUMENT, SUB_CHAIN,
			COLLECTION_OP, };

	/**
	 * A public read-only list of all the '<em><b>MChain Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MChainAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MChain Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MChainAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MChainAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MChain Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MChainAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MChainAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MChain Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MChainAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case INTO_DATA_CONSTANT_VALUE:
			return INTO_DATA_CONSTANT;
		case INTO_LITERAL_CONSTANT_VALUE:
			return INTO_LITERAL_CONSTANT;
		case INTO_APPLY_VALUE:
			return INTO_APPLY;
		case INTO_COND_OF_IF_THEN_ELSE_VALUE:
			return INTO_COND_OF_IF_THEN_ELSE;
		case INTO_THEN_OF_IF_THEN_ELSE_VALUE:
			return INTO_THEN_OF_IF_THEN_ELSE;
		case INTO_ELSE_OF_IF_THEN_ELSE_VALUE:
			return INTO_ELSE_OF_IF_THEN_ELSE;
		case ARGUMENT_VALUE:
			return ARGUMENT;
		case DATA_CONSTANT_AS_ARGUMENT_VALUE:
			return DATA_CONSTANT_AS_ARGUMENT;
		case LITERAL_CONSTANT_AS_ARGUMENT_VALUE:
			return LITERAL_CONSTANT_AS_ARGUMENT;
		case SUB_CHAIN_VALUE:
			return SUB_CHAIN;
		case COLLECTION_OP_VALUE:
			return COLLECTION_OP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MChainAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MChainAction
