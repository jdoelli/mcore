/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMObjectBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'obj\'\n' calculatedBaseDerive='ExpressionBase::ObjectObject' aMandatoryDerive='true' aClassifierDerive='if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).aObjectObjectType\r\nelse null\r\nendif\r\n' aSingularDerive='true\n'"
 * @generated
 */

public interface MObjectBaseDefinition extends MAbstractBaseDefinition {
} // MObjectBaseDefinition
