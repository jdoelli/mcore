/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MConstant Let</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMConstantLet()
 * @model abstract="true"
 * @generated
 */

public interface MConstantLet extends MAbstractExpression {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='let s1: String = if p1.oclIsUndefined() then \'\' else if p1=\'\' then \'\' else p1 endif endif in\r\nlet s2: String = if p2.oclIsUndefined() then \'\' else if p2=\'\' then \'\' else p2 endif endif in\r\nlet s3: String = if p3.oclIsUndefined() then \'\' else if p3=\'\' then \'\' else p3 endif endif in\r\n\r\n\'OrderedSet {\'.concat(\r\n\tif s1=\'\'\r\n\tthen \r\n\t\tif s2=\'\'\r\n\t\tthen \r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse s3\r\n\t\t\tendif\r\n\t\telse s2.concat (\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\t\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif)\r\n\t\tendif\r\n\telse s1.concat (\r\n\t\tif s2=\'\'\r\n\t\tthen\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif\r\n\t\telse \', \'.concat(s2).concat (\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif)\r\n\t\tendif)\r\n\tendif\r\n).concat(\'}\')'"
	 * @generated
	 */
	String asSetString(String p1, String p2, String p3);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if p1.oclIsUndefined() then null\r\nelse \'\\\'\'.concat(p1).concat(\'\\\'\') endif'"
	 * @generated
	 */
	String inQuotes(String p1);

} // MConstantLet
