/**
 */
package com.montages.mrules.expressions;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage
 * @generated
 */
public interface ExpressionsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExpressionsFactory eINSTANCE = com.montages.mrules.expressions.impl.ExpressionsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MAbstract Expression With Base</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAbstract Expression With Base</em>'.
	 * @generated
	 */
	MAbstractExpressionWithBase createMAbstractExpressionWithBase();

	/**
	 * Returns a new object of class '<em>MContainer Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MContainer Base Definition</em>'.
	 * @generated
	 */
	MContainerBaseDefinition createMContainerBaseDefinition();

	/**
	 * Returns a new object of class '<em>MBase Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MBase Definition</em>'.
	 * @generated
	 */
	MBaseDefinition createMBaseDefinition();

	/**
	 * Returns a new object of class '<em>MNumber Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNumber Base Definition</em>'.
	 * @generated
	 */
	MNumberBaseDefinition createMNumberBaseDefinition();

	/**
	 * Returns a new object of class '<em>MSelf Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSelf Base Definition</em>'.
	 * @generated
	 */
	MSelfBaseDefinition createMSelfBaseDefinition();

	/**
	 * Returns a new object of class '<em>MTarget Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTarget Base Definition</em>'.
	 * @generated
	 */
	MTargetBaseDefinition createMTargetBaseDefinition();

	/**
	 * Returns a new object of class '<em>MSource Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSource Base Definition</em>'.
	 * @generated
	 */
	MSourceBaseDefinition createMSourceBaseDefinition();

	/**
	 * Returns a new object of class '<em>MObject Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MObject Base Definition</em>'.
	 * @generated
	 */
	MObjectBaseDefinition createMObjectBaseDefinition();

	/**
	 * Returns a new object of class '<em>MSimple Type Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSimple Type Constant Base Definition</em>'.
	 * @generated
	 */
	MSimpleTypeConstantBaseDefinition createMSimpleTypeConstantBaseDefinition();

	/**
	 * Returns a new object of class '<em>MLiteral Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLiteral Constant Base Definition</em>'.
	 * @generated
	 */
	MLiteralConstantBaseDefinition createMLiteralConstantBaseDefinition();

	/**
	 * Returns a new object of class '<em>MObject Reference Constant Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MObject Reference Constant Base Definition</em>'.
	 * @generated
	 */
	MObjectReferenceConstantBaseDefinition createMObjectReferenceConstantBaseDefinition();

	/**
	 * Returns a new object of class '<em>MVariable Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MVariable Base Definition</em>'.
	 * @generated
	 */
	MVariableBaseDefinition createMVariableBaseDefinition();

	/**
	 * Returns a new object of class '<em>MIterator Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MIterator Base Definition</em>'.
	 * @generated
	 */
	MIteratorBaseDefinition createMIteratorBaseDefinition();

	/**
	 * Returns a new object of class '<em>MParameter Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MParameter Base Definition</em>'.
	 * @generated
	 */
	MParameterBaseDefinition createMParameterBaseDefinition();

	/**
	 * Returns a new object of class '<em>MAccumulator Base Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAccumulator Base Definition</em>'.
	 * @generated
	 */
	MAccumulatorBaseDefinition createMAccumulatorBaseDefinition();

	/**
	 * Returns a new object of class '<em>MTuple</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTuple</em>'.
	 * @generated
	 */
	MTuple createMTuple();

	/**
	 * Returns a new object of class '<em>MTuple Entry</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MTuple Entry</em>'.
	 * @generated
	 */
	MTupleEntry createMTupleEntry();

	/**
	 * Returns a new object of class '<em>MNew Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNew Object</em>'.
	 * @generated
	 */
	MNewObject createMNewObject();

	/**
	 * Returns a new object of class '<em>MNew Object Feature Value</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNew Object Feature Value</em>'.
	 * @generated
	 */
	MNewObjectFeatureValue createMNewObjectFeatureValue();

	/**
	 * Returns a new object of class '<em>MNamed Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNamed Expression</em>'.
	 * @generated
	 */
	MNamedExpression createMNamedExpression();

	/**
	 * Returns a new object of class '<em>MProcessor Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MProcessor Definition</em>'.
	 * @generated
	 */
	MProcessorDefinition createMProcessorDefinition();

	/**
	 * Returns a new object of class '<em>MChain</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MChain</em>'.
	 * @generated
	 */
	MChain createMChain();

	/**
	 * Returns a new object of class '<em>MCall Argument</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MCall Argument</em>'.
	 * @generated
	 */
	MCallArgument createMCallArgument();

	/**
	 * Returns a new object of class '<em>MSub Chain</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSub Chain</em>'.
	 * @generated
	 */
	MSubChain createMSubChain();

	/**
	 * Returns a new object of class '<em>MData Value Expr</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MData Value Expr</em>'.
	 * @generated
	 */
	MDataValueExpr createMDataValueExpr();

	/**
	 * Returns a new object of class '<em>MLiteral Value Expr</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLiteral Value Expr</em>'.
	 * @generated
	 */
	MLiteralValueExpr createMLiteralValueExpr();

	/**
	 * Returns a new object of class '<em>MIf</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MIf</em>'.
	 * @generated
	 */
	MIf createMIf();

	/**
	 * Returns a new object of class '<em>MThen</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MThen</em>'.
	 * @generated
	 */
	MThen createMThen();

	/**
	 * Returns a new object of class '<em>MElse If</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MElse If</em>'.
	 * @generated
	 */
	MElseIf createMElseIf();

	/**
	 * Returns a new object of class '<em>MElse</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MElse</em>'.
	 * @generated
	 */
	MElse createMElse();

	/**
	 * Returns a new object of class '<em>MCollection Expression</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MCollection Expression</em>'.
	 * @generated
	 */
	MCollectionExpression createMCollectionExpression();

	/**
	 * Returns a new object of class '<em>MIterator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MIterator</em>'.
	 * @generated
	 */
	MIterator createMIterator();

	/**
	 * Returns a new object of class '<em>MAccumulator</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MAccumulator</em>'.
	 * @generated
	 */
	MAccumulator createMAccumulator();

	/**
	 * Returns a new object of class '<em>MNamed Constant</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MNamed Constant</em>'.
	 * @generated
	 */
	MNamedConstant createMNamedConstant();

	/**
	 * Returns a new object of class '<em>MSimple Type Constant Let</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MSimple Type Constant Let</em>'.
	 * @generated
	 */
	MSimpleTypeConstantLet createMSimpleTypeConstantLet();

	/**
	 * Returns a new object of class '<em>MLiteral Let</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MLiteral Let</em>'.
	 * @generated
	 */
	MLiteralLet createMLiteralLet();

	/**
	 * Returns a new object of class '<em>MApplication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MApplication</em>'.
	 * @generated
	 */
	MApplication createMApplication();

	/**
	 * Returns a new object of class '<em>MOperator Definition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>MOperator Definition</em>'.
	 * @generated
	 */
	MOperatorDefinition createMOperatorDefinition();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ExpressionsPackage getExpressionsPackage();

} //ExpressionsFactory
