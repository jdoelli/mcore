/**
 */
package com.montages.mrules.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MNamed Expression Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMNamedExpressionAction()
 * @model
 * @generated
 */
public enum MNamedExpressionAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>As Base In Main Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_BASE_IN_MAIN_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	AS_BASE_IN_MAIN_CHAIN(1, "AsBaseInMainChain", "As Base in Main Chain"),

	/**
	 * The '<em><b>As Argument In Main Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_ARGUMENT_IN_MAIN_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	AS_ARGUMENT_IN_MAIN_CHAIN(2, "AsArgumentInMainChain", "As Argument in Main Chain"),

	/**
	 * The '<em><b>As Base In Previous Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_BASE_IN_PREVIOUS_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	AS_BASE_IN_PREVIOUS_CHAIN(3, "AsBaseInPreviousChain", "As Base in Previous Chain"),

	/**
	 * The '<em><b>As Argument In Previous Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_ARGUMENT_IN_PREVIOUS_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	AS_ARGUMENT_IN_PREVIOUS_CHAIN(4, "AsArgumentInPreviousChain", "As Argument in Previous Chain"),

	/**
	 * The '<em><b>As Operand In Previous Apply</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_OPERAND_IN_PREVIOUS_APPLY_VALUE
	 * @generated
	 * @ordered
	 */
	AS_OPERAND_IN_PREVIOUS_APPLY(5, "AsOperandInPreviousApply", "As Operand in Previous Apply"),

	/**
	 * The '<em><b>As Cond In Previous If</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_COND_IN_PREVIOUS_IF_VALUE
	 * @generated
	 * @ordered
	 */
	AS_COND_IN_PREVIOUS_IF(6, "AsCondInPreviousIf", "As Cond in Previous If"),

	/**
	 * The '<em><b>As Then In Previous If</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_THEN_IN_PREVIOUS_IF_VALUE
	 * @generated
	 * @ordered
	 */
	AS_THEN_IN_PREVIOUS_IF(7, "AsThenInPreviousIf", "As Then in Previous If"),

	/**
	 * The '<em><b>As Else In Previous If</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #AS_ELSE_IN_PREVIOUS_IF_VALUE
	 * @generated
	 * @ordered
	 */
	AS_ELSE_IN_PREVIOUS_IF(8, "AsElseInPreviousIf", "As Else in Previous If");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>As Base In Main Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Base In Main Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_BASE_IN_MAIN_CHAIN
	 * @model name="AsBaseInMainChain" literal="As Base in Main Chain"
	 * @generated
	 * @ordered
	 */
	public static final int AS_BASE_IN_MAIN_CHAIN_VALUE = 1;

	/**
	 * The '<em><b>As Argument In Main Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Argument In Main Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_ARGUMENT_IN_MAIN_CHAIN
	 * @model name="AsArgumentInMainChain" literal="As Argument in Main Chain"
	 * @generated
	 * @ordered
	 */
	public static final int AS_ARGUMENT_IN_MAIN_CHAIN_VALUE = 2;

	/**
	 * The '<em><b>As Base In Previous Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Base In Previous Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_BASE_IN_PREVIOUS_CHAIN
	 * @model name="AsBaseInPreviousChain" literal="As Base in Previous Chain"
	 * @generated
	 * @ordered
	 */
	public static final int AS_BASE_IN_PREVIOUS_CHAIN_VALUE = 3;

	/**
	 * The '<em><b>As Argument In Previous Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Argument In Previous Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_ARGUMENT_IN_PREVIOUS_CHAIN
	 * @model name="AsArgumentInPreviousChain" literal="As Argument in Previous Chain"
	 * @generated
	 * @ordered
	 */
	public static final int AS_ARGUMENT_IN_PREVIOUS_CHAIN_VALUE = 4;

	/**
	 * The '<em><b>As Operand In Previous Apply</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Operand In Previous Apply</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_OPERAND_IN_PREVIOUS_APPLY
	 * @model name="AsOperandInPreviousApply" literal="As Operand in Previous Apply"
	 * @generated
	 * @ordered
	 */
	public static final int AS_OPERAND_IN_PREVIOUS_APPLY_VALUE = 5;

	/**
	 * The '<em><b>As Cond In Previous If</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Cond In Previous If</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_COND_IN_PREVIOUS_IF
	 * @model name="AsCondInPreviousIf" literal="As Cond in Previous If"
	 * @generated
	 * @ordered
	 */
	public static final int AS_COND_IN_PREVIOUS_IF_VALUE = 6;

	/**
	 * The '<em><b>As Then In Previous If</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Then In Previous If</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_THEN_IN_PREVIOUS_IF
	 * @model name="AsThenInPreviousIf" literal="As Then in Previous If"
	 * @generated
	 * @ordered
	 */
	public static final int AS_THEN_IN_PREVIOUS_IF_VALUE = 7;

	/**
	 * The '<em><b>As Else In Previous If</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>As Else In Previous If</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #AS_ELSE_IN_PREVIOUS_IF
	 * @model name="AsElseInPreviousIf" literal="As Else in Previous If"
	 * @generated
	 * @ordered
	 */
	public static final int AS_ELSE_IN_PREVIOUS_IF_VALUE = 8;

	/**
	 * An array of all the '<em><b>MNamed Expression Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MNamedExpressionAction[] VALUES_ARRAY = new MNamedExpressionAction[] { DO,
			AS_BASE_IN_MAIN_CHAIN, AS_ARGUMENT_IN_MAIN_CHAIN, AS_BASE_IN_PREVIOUS_CHAIN, AS_ARGUMENT_IN_PREVIOUS_CHAIN,
			AS_OPERAND_IN_PREVIOUS_APPLY, AS_COND_IN_PREVIOUS_IF, AS_THEN_IN_PREVIOUS_IF, AS_ELSE_IN_PREVIOUS_IF, };

	/**
	 * A public read-only list of all the '<em><b>MNamed Expression Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MNamedExpressionAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MNamed Expression Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MNamedExpressionAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MNamedExpressionAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MNamed Expression Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MNamedExpressionAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MNamedExpressionAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MNamed Expression Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MNamedExpressionAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case AS_BASE_IN_MAIN_CHAIN_VALUE:
			return AS_BASE_IN_MAIN_CHAIN;
		case AS_ARGUMENT_IN_MAIN_CHAIN_VALUE:
			return AS_ARGUMENT_IN_MAIN_CHAIN;
		case AS_BASE_IN_PREVIOUS_CHAIN_VALUE:
			return AS_BASE_IN_PREVIOUS_CHAIN;
		case AS_ARGUMENT_IN_PREVIOUS_CHAIN_VALUE:
			return AS_ARGUMENT_IN_PREVIOUS_CHAIN;
		case AS_OPERAND_IN_PREVIOUS_APPLY_VALUE:
			return AS_OPERAND_IN_PREVIOUS_APPLY;
		case AS_COND_IN_PREVIOUS_IF_VALUE:
			return AS_COND_IN_PREVIOUS_IF;
		case AS_THEN_IN_PREVIOUS_IF_VALUE:
			return AS_THEN_IN_PREVIOUS_IF;
		case AS_ELSE_IN_PREVIOUS_IF_VALUE:
			return AS_ELSE_IN_PREVIOUS_IF;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MNamedExpressionAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MNamedExpressionAction
