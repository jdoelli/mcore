/**
 */

package com.montages.mrules.expressions;

import com.montages.acore.APackage;

import com.montages.acore.abstractions.ATyped;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MRulesElement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getAsCode <em>As Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getAsBasicCode <em>As Basic Code</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getCollector <em>Collector</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getEntireScope <em>Entire Scope</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeBase <em>Scope Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeSelf <em>Scope Self</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeTrg <em>Scope Trg</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeObj <em>Scope Obj</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeLiteralConstants <em>Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeVariables <em>Scope Variables</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeIterator <em>Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeAccumulator <em>Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeParameters <em>Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeContainerBase <em>Scope Container Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeNumberBase <em>Scope Number Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getScopeSource <em>Scope Source</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalEntireScope <em>Local Entire Scope</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeBase <em>Local Scope Base</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeSelf <em>Local Scope Self</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeTrg <em>Local Scope Trg</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeObj <em>Local Scope Obj</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeSource <em>Local Scope Source</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeVariables <em>Local Scope Variables</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeIterator <em>Local Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeAccumulator <em>Local Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeParameters <em>Local Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getLocalScopeContainer <em>Local Scope Container</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getContainingAnnotation <em>Containing Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getContainingExpression <em>Containing Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getIsComplexExpression <em>Is Complex Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getIsSubExpression <em>Is Sub Expression</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getACalculatedOwnType <em>ACalculated Own Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getCalculatedOwnSingular <em>Calculated Own Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getACalculatedOwnSimpleType <em>ACalculated Own Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getASelfObjectType <em>ASelf Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getASelfObjectPackage <em>ASelf Object Package</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getATargetObjectType <em>ATarget Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getATargetSimpleType <em>ATarget Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getAObjectObjectType <em>AObject Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getAExpectedReturnType <em>AExpected Return Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getAExpectedReturnSimpleType <em>AExpected Return Simple Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getIsReturnValueSingular <em>Is Return Value Singular</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getASrcObjectType <em>ASrc Object Type</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MAbstractExpression#getASrcObjectSimpleType <em>ASrc Object Simple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'OVERRIDE IN SUBCLASS\'\n' aClassifierDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen aCalculatedOwnType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.aClassifier\nendif\nendif\n' aMandatoryDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnMandatory\n  else if collector.oclIsUndefined()\n  then null\n  else collector.aMandatory\nendif\nendif\n' aSingularDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnSingular\n  else if collector.oclIsUndefined()\n  then null\n  else collector.aSingular\nendif\nendif\n' aSimpleTypeDerive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen aCalculatedOwnSimpleType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.aSimpleType\nendif\nendif\n'"
 * @generated
 */

public interface MAbstractExpression extends MRulesElement, ATyped {
	/**
	 * Returns the value of the '<em><b>As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The OCL for this expression. After all transformations have been applied (e.g. conversion of arity, casting, etc.)
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>As Code</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_AsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen asBasicCode\n  else if collector.oclIsUndefined()\n  then null\n  else collector.asBasicCode\nendif\nendif\n'"
	 * @generated
	 */
	String getAsCode();

	/**
	 * Returns the value of the '<em><b>As Basic Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The most basic code for the expression, sometimes this code needs to be augmented (e.g. to convert expression arity). Sub-classes should overwrite this method and not asCode...usually.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>As Basic Code</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_AsBasicCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='eClass().name.concat(\' : <?>\')'"
	 * @generated
	 */
	String getAsBasicCode();

	/**
	 * Returns the value of the '<em><b>Collector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collector</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collector</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_Collector()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: mrules::expressions::MCollectionExpression = null in nl\n'"
	 * @generated
	 */
	MCollectionExpression getCollector();

	/**
	 * Returns the value of the '<em><b>Entire Scope</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MAbstractBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Entire Scope</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Entire Scope</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_EntireScope()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localEntireScope \r\nelse /* Note this is necessary for some cases like scopeIterator that otherwise won\'t work \052/ \r\nscopeBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition))->union(\r\nscopeSelf->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeTrg->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObj->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSimpleTypeConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeLiteralConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObjectReferenceConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeVariables->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeParameters->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeIterator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeAccumulator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeNumberBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeContainerBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSource->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getEntireScope();

	/**
	 * Returns the value of the '<em><b>Scope Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Base</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeBase \r\nelse containingExpression.scopeBase\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MBaseDefinition> getScopeBase();

	/**
	 * Returns the value of the '<em><b>Scope Self</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSelfBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Self</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Self</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSelf()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeSelf\r\nelse containingExpression.scopeSelf\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSelfBaseDefinition> getScopeSelf();

	/**
	 * Returns the value of the '<em><b>Scope Trg</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MTargetBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Trg</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Trg</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeTrg()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeTrg\r\nelse containingExpression.scopeTrg\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTargetBaseDefinition> getScopeTrg();

	/**
	 * Returns the value of the '<em><b>Scope Obj</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MObjectBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Obj</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Obj</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeObj()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='--todo: only for updates...\r\nif containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectBaseDefinition> getScopeObj();

	/**
	 * Returns the value of the '<em><b>Scope Simple Type Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSimpleTypeConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Simple Type Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Simple Type Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSimpleTypeConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeSimpleTypeConstants\r\nelse containingExpression.scopeSimpleTypeConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSimpleTypeConstantBaseDefinition> getScopeSimpleTypeConstants();

	/**
	 * Returns the value of the '<em><b>Scope Literal Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Literal Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Literal Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeLiteralConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeLiteralConstants\r\nelse containingExpression.scopeLiteralConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MLiteralConstantBaseDefinition> getScopeLiteralConstants();

	/**
	 * Returns the value of the '<em><b>Scope Object Reference Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MObjectReferenceConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Object Reference Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Object Reference Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeObjectReferenceConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeObjectReferenceConstants\r\nelse containingExpression.scopeObjectReferenceConstants\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectReferenceConstantBaseDefinition> getScopeObjectReferenceConstants();

	/**
	 * Returns the value of the '<em><b>Scope Variables</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MVariableBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Variables</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeVariables()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeVariables\r\nelse containingExpression.scopeVariables\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MVariableBaseDefinition> getScopeVariables();

	/**
	 * Returns the value of the '<em><b>Scope Iterator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MIteratorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Iterator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Iterator</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeIterator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr \052/\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeIterator\r\nelse containingExpression.scopeIterator->union(localScopeIterator)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MIteratorBaseDefinition> getScopeIterator();

	/**
	 * Returns the value of the '<em><b>Scope Accumulator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MAccumulatorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Accumulator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Accumulator</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeAccumulator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr \052/\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeAccumulator\r\nelse containingExpression.scopeAccumulator->union(localScopeAccumulator)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAccumulatorBaseDefinition> getScopeAccumulator();

	/**
	 * Returns the value of the '<em><b>Scope Parameters</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MParameterBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Parameters</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeParameters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen localScopeParameters\r\nelse containingExpression.scopeParameters\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MParameterBaseDefinition> getScopeParameters();

	/**
	 * Returns the value of the '<em><b>Scope Container Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MContainerBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Container Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Container Base</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeContainerBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeContainerBase'"
	 *        annotation="http://www.xocl.org/OCL derive='localScopeContainer'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MContainerBaseDefinition> getScopeContainerBase();

	/**
	 * Returns the value of the '<em><b>Scope Number Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MNumberBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Number Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Number Base</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeNumberBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeNumberBase'"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined()\r\nthen self.localScopeNumberBaseDefinition\r\nelse containingExpression.scopeNumberBase\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MNumberBaseDefinition> getScopeNumberBase();

	/**
	 * Returns the value of the '<em><b>Scope Source</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSourceBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scope Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scope Source</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ScopeSource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='scopeSource'"
	 *        annotation="http://www.xocl.org/OCL derive='\r\nif containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Base Scope' createColumn='false'"
	 * @generated
	 */
	EList<MSourceBaseDefinition> getScopeSource();

	/**
	 * Returns the value of the '<em><b>Local Entire Scope</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MAbstractBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Entire Scope</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Entire Scope</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalEntireScope()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='localScopeBase->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition))->union(\r\nlocalScopeSelf->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObj->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeTrg->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSimpleTypeConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeLiteralConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObjectReferenceConstants->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeVariables->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeParameters->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeIterator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeAccumulator->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeNumberBaseDefinition->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeContainer->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSource->collect(oclAsType(mrules::expressions::MAbstractBaseDefinition)))\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getLocalEntireScope();

	/**
	 * Returns the value of the '<em><b>Local Scope Base</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Base</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Base</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::NullValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::FalseValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::TrueValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::EmptyStringValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::EmptyCollection}  \r\n}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MBaseDefinition> getLocalScopeBase();

	/**
	 * Returns the value of the '<em><b>Local Scope Self</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSelfBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Self</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Self</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSelf()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{Tuple{debug=\'\'}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSelfBaseDefinition> getLocalScopeSelf();

	/**
	 * Returns the value of the '<em><b>Local Scope Trg</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MTargetBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Trg</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Trg</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeTrg()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aTargetObjectType.oclIsUndefined() and self.aTargetSimpleType=acore::classifiers::ASimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTargetBaseDefinition> getLocalScopeTrg();

	/**
	 * Returns the value of the '<em><b>Local Scope Obj</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MObjectBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Obj</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Obj</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeObj()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aObjectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectBaseDefinition> getLocalScopeObj();

	/**
	 * Returns the value of the '<em><b>Local Scope Source</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSourceBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Source</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aSrcObjectType.oclIsUndefined() and self.aSrcObjectSimpleType=acore::classifiers::ASimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSourceBaseDefinition> getLocalScopeSource();

	/**
	 * Returns the value of the '<em><b>Local Scope Simple Type Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MSimpleTypeConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Simple Type Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Simple Type Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeSimpleTypeConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation)\r\nthen let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in\052/\r\nlet a: mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mrules::expressions::MSimpleTypeConstantLet) and (not i.aName.oclIsUndefined()) and (i.aName<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MSimpleTypeConstantBaseDefinition> getLocalScopeSimpleTypeConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Literal Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MLiteralConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Literal Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Literal Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeLiteralConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/* if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation)\r\nthen let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in \052/\r\nlet a: mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mrules::expressions::MLiteralLet) and (not i.aName.oclIsUndefined()) and (i.aName<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\n  endif\r\n/*\r\nelse OrderedSet{} endif\r\nendif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MLiteralConstantBaseDefinition> getLocalScopeLiteralConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Object Reference Constants</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MObjectReferenceConstantBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Object Reference Constants</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Object Reference Constants</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeObjectReferenceConstants()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation)\r\nthen let a: mrules::MRuleAnnotation = eContainer().oclAsType(mrules::MRuleAnnotation) in \052/\r\nlet a: mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n\r\n  a.namedConstant->iterate (i: mrules::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mrules::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mrules::expressions::MObjectReferenceLet) and (not i.aName.oclIsUndefined()) and (i.aName<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MObjectReferenceConstantBaseDefinition> getLocalScopeObjectReferenceConstants();

	/**
	 * Returns the value of the '<em><b>Local Scope Variables</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MVariableBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Variables</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Variables</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeVariables()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let a: mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined()\r\n  then  OrderedSet{} \r\n  else\r\n  a.namedExpression->iterate (i: mrules::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mrules::expressions::MNamedExpression))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if  (not i.aName.oclIsUndefined()) and (i.aName<>\'\')\r\n    \t\tthen r->append(Tuple{namedExpression=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  ) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MVariableBaseDefinition> getLocalScopeVariables();

	/**
	 * Returns the value of the '<em><b>Local Scope Iterator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MIteratorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Iterator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Iterator</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeIterator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(mrules::expressions::MCollectionExpression)\r\nthen let c: mrules::expressions::MCollectionExpression = eContainer().oclAsType(mrules::expressions::MCollectionExpression) in\r\n  if c.iteratorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{iterator=c.iteratorVar}} endif\r\nelse OrderedSet{} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MIteratorBaseDefinition> getLocalScopeIterator();

	/**
	 * Returns the value of the '<em><b>Local Scope Accumulator</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MAccumulatorBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Accumulator</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Accumulator</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeAccumulator()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(mrules::expressions::MCollectionExpression)\r\nthen let c: mrules::expressions::MCollectionExpression = eContainer().oclAsType(mrules::expressions::MCollectionExpression) in\r\n  if c.accumulatorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif\r\nelse OrderedSet{} endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MAccumulatorBaseDefinition> getLocalScopeAccumulator();

	/**
	 * Returns the value of the '<em><b>Local Scope Parameters</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MParameterBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Parameters</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeParameters()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingAnnotation=null \r\n  then OrderedSet{}\r\n  else \r\n  let e:acore::abstractions::AAnnotatable= self.containingAnnotation.aAnnotated in\r\n    if e=null \r\n      then OrderedSet{}\r\n      else if not e.oclIsKindOf(acore::classifiers::AOperation)\r\n        then OrderedSet{}\r\n        else e.oclAsType(acore::classifiers::AOperation).aParameter\r\n             ->iterate (i: acore::classifiers::AParameter; r: OrderedSet(Tuple(aParameter:acore::classifiers::AParameter))=OrderedSet{} \r\n                            |r->append(Tuple{aParameter=i}) )\r\n    \tendif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MParameterBaseDefinition> getLocalScopeParameters();

	/**
	 * Returns the value of the '<em><b>Local Scope Number Base Definition</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MNumberBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Number Base Definition</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Number Base Definition</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeNumberBaseDefinition()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Local Scope NumberBase Definition'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::ZeroValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::OneValue},\r\n  Tuple{expressionBase = mrules::expressions::ExpressionBase::MinusOneValue}\r\n}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MNumberBaseDefinition> getLocalScopeNumberBaseDefinition();

	/**
	 * Returns the value of the '<em><b>Local Scope Container</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MContainerBaseDefinition}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Scope Container</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Scope Container</em>' reference list.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_LocalScopeContainer()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{Tuple{debug=\'\'}}'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Base Scope/Local Base Scope'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MContainerBaseDefinition> getLocalScopeContainer();

	/**
	 * Returns the value of the '<em><b>Containing Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The annotation that contains, directly or indirectly, this expression.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Annotation</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ContainingAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='-- expressions can now be as well annotations, so the \"containing\" annotation can be self.\r\nif self.oclIsKindOf(mrules::MRuleAnnotation) \r\n\tthen self.oclAsType(mrules::MRuleAnnotation) \r\nelse if eContainer().oclIsUndefined() \r\n\tthen null \r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation) \r\n\tthen eContainer().oclAsType(mrules::MRuleAnnotation) \r\nelse if eContainer().oclIsTypeOf(mrules::expressions::MNamedExpression) \r\n   then self.eContainer().oclAsType(mrules::expressions::MNamedExpression).containingAnnotation\r\nelse if eContainer().oclIsKindOf(mrules::expressions::MNewObjectFeatureValue) \r\n   then eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue)\r\n   \t\t\t.eContainer().oclAsType(mrules::expressions::MNewObject).containingAnnotation\r\nelse if containingExpression.oclIsUndefined() \r\n\tthen null \r\n\telse containingExpression.containingAnnotation endif endif endif endif endif\tendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	MRuleAnnotation getContainingAnnotation();

	/**
	 * Returns the value of the '<em><b>Containing Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The expression, if any, that contains this one.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Containing Expression</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ContainingExpression()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression)\r\nthen eContainer().oclAsType(mrules::expressions::MAbstractExpression)\r\n\r\n else null endif endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	MAbstractExpression getContainingExpression();

	/**
	 * Returns the value of the '<em><b>Is Complex Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * It is true when the resulting OCL is complex enought that it needs to be encloed in parenthesis when it's a sub-expression.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Is Complex Expression</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_IsComplexExpression()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	Boolean getIsComplexExpression();

	/**
	 * Returns the value of the '<em><b>Is Sub Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Sub Expression</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Sub Expression</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_IsSubExpression()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined() then false\r\nelse not (containingExpression.oclIsKindOf(mrules::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mrules::expressions::MNamedExpression)) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	Boolean getIsSubExpression();

	/**
	 * Returns the value of the '<em><b>ACalculated Own Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ACalculated Own Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACalculated Own Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ACalculatedOwnType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Calculated Own Type'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	AClassifier getACalculatedOwnType();

	/**
	 * Returns the value of the '<em><b>Calculated Own Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Mandatory</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnMandatory()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getCalculatedOwnMandatory();

	/**
	 * Returns the value of the '<em><b>Calculated Own Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Own Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Own Singular</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_CalculatedOwnSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getCalculatedOwnSingular();

	/**
	 * Returns the value of the '<em><b>ACalculated Own Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ACalculated Own Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ACalculated Own Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ACalculatedOwnSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='acore::classifiers::ASimpleType::Boolean'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	ASimpleType getACalculatedOwnSimpleType();

	/**
	 * Returns the value of the '<em><b>ASelf Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASelf Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASelf Object Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ASelfObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Self Object Type'"
	 *        annotation="http://www.xocl.org/OCL derive='let a:mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() or a = self then null\r\n  else a.aSelfObjectType endif\r\n  \r\n/*if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation) \r\n  then eContainer().oclAsType(mrules::MRuleAnnotation).selfObjectTypeOfAnnotation\r\n--else if self.oclsKindOf(mrules::MRuleAnnotation)\r\n--   then self.oclAsType(mrules::MRuleAnnotation).selfObjectTypeOfAnnotation\r\n\r\n    --else  if  eContainer().oclIsTypeOf(mrules::expressions::MAbstractExpression) then\r\n  -- eContainer().oclAsType(mrules::expressions::MAbstractExpression).selfObjectType\r\n   else if eContainer().oclIsKindOf(mrules::expressions::MNewObjectFeatureValue)\r\n      then \r\n       if eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mrules::expressions::MNewObjecct)\r\n       then  eContainer().oclAsType(mrules::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mrules::expressions::MNewObjecct).selfObjectType\r\n       else \r\n       null\r\n     endif\r\n       else eContainer().oclAsType(mrules::expressions::MAbstractExpression).selfObjectType\r\n       endif\r\n  --endif\r\nendif\r\nendif \r\n--endif\r\n\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	AClassType getASelfObjectType();

	/**
	 * Returns the value of the '<em><b>ASelf Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The package of "self" object. This is needed because, when writing types in OCL expressions, types not in same package than "self" need to be explicitely named.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>ASelf Object Package</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ASelfObjectPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Self Object Package'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = aSelfObjectType.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if aSelfObjectType.oclIsUndefined()\n  then null\n  else aSelfObjectType.aContainingPackage\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	APackage getASelfObjectPackage();

	/**
	 * Returns the value of the '<em><b>ATarget Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATarget Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATarget Object Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ATargetObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Target Object Type'"
	 *        annotation="http://www.xocl.org/OCL derive='let a:mrules::MRuleAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() or a = self then null\r\n  else a.aTargetObjectType endif\r\n/*   \r\nif eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mrules::MRuleAnnotation) \r\n  then eContainer().oclAsType(mrules::MRuleAnnotation).targetObjectTypeOfAnnotation\r\n  else  if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression) then\r\n   eContainer().oclAsType(mrules::expressions::MAbstractExpression).targetObjectType else null endif\r\nendif\r\nendif \052/\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	AClassType getATargetObjectType();

	/**
	 * Returns the value of the '<em><b>ATarget Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATarget Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATarget Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ATargetSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingAnnotation.oclIsUndefined()\n   then null\n   else self.containingAnnotation.aTargetSimpleType endif\n/*\nif (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.targetSimpleTypeOfAnnotation\nendif\nendif\n\052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	ASimpleType getATargetSimpleType();

	/**
	 * Returns the value of the '<em><b>AObject Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AObject Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AObject Object Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_AObjectObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Object Object Type'"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n\r\n/*if self.oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n  --replaced with self.\r\n     then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation\r\nelse if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)\r\n    then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation\r\nelse if eContainer().oclIsUndefined()  then null \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation \r\n  else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   \r\nelse  if eContainer().oclIsKindOf(mrules::expressions::MAbstractExpression) \r\n     then eContainer().oclAsType(mrules::expressions::MAbstractExpression).objectObjectType \r\n     else null endif endif endif endif endif endif\r\n\052/\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	AClassifier getAObjectObjectType();

	/**
	 * Returns the value of the '<em><b>AExpected Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * The object type that the expression should return, based on where the containing annotation is attached.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>AExpected Return Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_AExpectedReturnType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Expected ReturnType'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.aExpectedReturnTypeOfAnnotation\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	AClassifier getAExpectedReturnType();

	/**
	 * Returns the value of the '<em><b>AExpected Return Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AExpected Return Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AExpected Return Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_AExpectedReturnSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingAnnotation.oclIsUndefined() \r\n     then acore::classifiers::ASimpleType::None\r\n     else containingAnnotation.aExpectedReturnSimpleTypeOfAnnotation endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	ASimpleType getAExpectedReturnSimpleType();

	/**
	 * Returns the value of the '<em><b>Is Return Value Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Mandatory</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_IsReturnValueMandatory()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationMandatory\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueMandatory();

	/**
	 * Returns the value of the '<em><b>Is Return Value Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Singular</em>' attribute.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_IsReturnValueSingular()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationSingular\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Information' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueSingular();

	/**
	 * Returns the value of the '<em><b>ASrc Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASrc Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASrc Object Type</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ASrcObjectType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Src Object Type'"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n  \r\n/*let srcType : MClassifier =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type\r\nelse null \r\nendif\r\nendif\r\nin\r\nif srcType.oclIsUndefined() then null else srcType endif\052/\r\n\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getASrcObjectType();

	/**
	 * Returns the value of the '<em><b>ASrc Object Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASrc Object Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASrc Object Simple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMAbstractExpression_ASrcObjectSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='A Src Object SimpleType'"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n/*let srcType : SimpleType =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType\r\nelse null\r\nendif endif\r\nin\r\nif srcType.oclIsUndefined() then acore::classifiers::ASimpleType::None else srcType endif \052/\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ASimpleType getASrcObjectSimpleType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if asCode.oclIsUndefined() then \'\'\r\nelse let s: String = asCode in\r\n\tif s.size() > 30 then s.substring(1, 27).concat(\'...\')\r\n\telse s endif\r\nendif'"
	 * @generated
	 */
	String getShortCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='entireScope'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Base Scope' createColumn='true'"
	 * @generated
	 */
	EList<MAbstractBaseDefinition> getScope();

} // MAbstractExpression
