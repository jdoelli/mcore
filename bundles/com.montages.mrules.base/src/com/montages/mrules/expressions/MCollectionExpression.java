/**
 */

package com.montages.mrules.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MCollection Expression</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.expressions.MCollectionExpression#getCollectionOperator <em>Collection Operator</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MCollectionExpression#getCollection <em>Collection</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MCollectionExpression#getIteratorVar <em>Iterator Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MCollectionExpression#getAccumulatorVar <em>Accumulator Var</em>}</li>
 *   <li>{@link com.montages.mrules.expressions.MCollectionExpression#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression()
 * @model annotation="http://www.xocl.org/OCL label='collectionOperatorAsCode(self.collectionOperator)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let prefix: String = if collection.oclIsUndefined() then \'\'\r\nelse if collection.oclIsKindOf(MBaseChain) then \'\'\r\nelse if collection.oclIsKindOf(MApplication) then \')\'\r\nelse if collection.oclIsKindOf(MIf) then \'ENDIF\' else \'\'\r\nendif endif endif endif in \r\nprefix.concat(\'->\').concat(collectionOperatorAsCode()).concat(\'(\')' isComplexExpressionDerive='/* if it returns a real collection, then it\'s a single OCL item, otherwise it\'s a let with if \052/\r\naSingular' asBasicCodeDerive='if collection.oclIsUndefined() then \'MISSING COLLECTION\'\r\nelse\r\n\r\nlet hasIterator: Boolean = not iteratorVar.oclIsUndefined() in\r\nlet hasAccumulator: Boolean=not accumulatorVar.oclIsUndefined() in\r\nlet  ct: String = aTypeAsOcl(aSelfObjectPackage, collection.aCalculatedOwnType, collection.aCalculatedOwnSimpleType , true) in\r\n\t\r\nlet exp: String = (if collection.isComplexExpression then \'(\' else \'\' endif).concat(collection.asBasicCode).concat(if collection.isComplexExpression then \')\' else \'\' endif).concat(\r\n\'->\').concat(collectionOperatorAsCode()).concat(\'(\').concat(\r\n\tif hasIterator then iteratorVar.aName.concat(\': \').concat(ct)\r\n\t\telse \'\' endif\r\n).concat(\t\t\r\n\tif hasIterator and hasAccumulator then \'; \' else \'\' endif\r\n).concat(\t\t\r\n\tif hasAccumulator then \r\n\t  accumulatorVar.aName.concat(\': \').concat(aTypeAsOcl(aSelfObjectPackage, accumulatorVar.aCalculatedOwnType, accumulatorVar.aCalculatedOwnSimpleType , accumulatorVar.calculatedOwnSingular))\r\n\t  .concat(\' = \').concat(if self.accumulatorVar.accDefinition.oclIsUndefined() then if self.aCalculatedOwnSimpleType= acore::classifiers::ASimpleType::String then \'  \\\' \' \'\\\' \' else if self.aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::Integer or self.aCalculatedOwnSimpleType = acore::classifiers::ASimpleType::Real then \'0\' else \'null \' endif endif\r\n\t  else\r\n\t  self.accumulatorVar.accDefinition.asBasicCode\r\n\t  endif\r\n\t  ) \r\n\t\r\n\telse \'\' endif\r\n).concat(\t\t\r\n\tif hasIterator or hasAccumulator then \' | \' else \'\' endif\r\n). concat (\r\n\tif expression.oclIsUndefined() then \'true\'\r\n\telse expression.asCode endif\r\n).concat(\')\') in\r\n\r\nif calculatedOwnSingular then\r\n  \'let c: \'.concat(aTypeAsOcl(aSelfObjectPackage,  aClassifier,  aCalculatedOwnSimpleType , true)).concat(\' = \').concat(exp).concat(\' in if c.oclIsUndefined() then null else c endif\')\r\nelse \r\n  exp.concat(\'->asOrderedSet()->excluding(null)->asOrderedSet() \')\t\r\nendif\r\n\r\nendif\r\n' calculatedOwnMandatoryDerive='if collectionOperator = CollectionOperator::Iterate then\r\n\tif iteratorVar.oclIsUndefined() then false\r\n\telse iteratorVar.aMandatory endif\r\nelse false endif' calculatedOwnSingularDerive='if collectionOperator = CollectionOperator::Iterate then\r\n\tif iteratorVar.oclIsUndefined() then false\r\n\telse iteratorVar.aSingular endif\r\nelse false endif' aCalculatedOwnSimpleTypeDerive='let ct: acore::classifiers::ASimpleType = if collection.oclIsUndefined()\r\n\tthen acore::classifiers::ASimpleType::None\r\n\telse collection.aCalculatedOwnSimpleType endif in\r\nlet et: acore::classifiers::ASimpleType = if expression.oclIsUndefined()\r\n\tthen acore::classifiers::ASimpleType::None\r\n\telse expression.aSimpleType endif in\r\nlet at: acore::classifiers::ASimpleType = if accumulatorVar.oclIsUndefined()\r\n\tthen acore::classifiers::ASimpleType::None\r\n\telse accumulatorVar.aSimpleType endif in\r\n\t\t\r\nif collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure\r\n  then ct \r\n  else if collectionOperator = CollectionOperator::Collect \r\n    then  et \r\n      else if collectionOperator = CollectionOperator::Iterate\r\n    \tthen  at\r\n    \t  else acore::classifiers::ASimpleType::None endif\r\n    endif \r\n  endif \r\n' aClassifierDerive='let ct: acore::classifiers::AClassifier = if collection.oclIsUndefined()\r\n\tthen null\r\n\telse collection.aCalculatedOwnType endif in\r\nlet et: acore::classifiers::AClassifier = if expression.oclIsUndefined()\r\n\tthen null\r\n\telse expression.aClassifier endif in\r\nlet at: acore::classifiers::AClassifier = if accumulatorVar.oclIsUndefined()\r\n\tthen null\r\n\telse accumulatorVar.aClassifier endif in\r\n\t\t\r\nif collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure\r\n  then ct \r\n  else if collectionOperator = CollectionOperator::Collect \r\n    then  et \r\n      else if collectionOperator = CollectionOperator::Iterate\r\n    \tthen  at\r\n    \t  else null endif\r\n    endif \r\n  endif '"
 * @generated
 */

public interface MCollectionExpression extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Collection Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.expressions.CollectionOperator}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection Operator</em>' attribute.
	 * @see com.montages.mrules.expressions.CollectionOperator
	 * @see #isSetCollectionOperator()
	 * @see #unsetCollectionOperator()
	 * @see #setCollectionOperator(CollectionOperator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression_CollectionOperator()
	 * @model unsettable="true"
	 * @generated
	 */
	CollectionOperator getCollectionOperator();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getCollectionOperator <em>Collection Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection Operator</em>' attribute.
	 * @see com.montages.mrules.expressions.CollectionOperator
	 * @see #isSetCollectionOperator()
	 * @see #unsetCollectionOperator()
	 * @see #getCollectionOperator()
	 * @generated
	 */

	void setCollectionOperator(CollectionOperator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getCollectionOperator <em>Collection Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCollectionOperator()
	 * @see #getCollectionOperator()
	 * @see #setCollectionOperator(CollectionOperator)
	 * @generated
	 */
	void unsetCollectionOperator();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getCollectionOperator <em>Collection Operator</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Collection Operator</em>' attribute is set.
	 * @see #unsetCollectionOperator()
	 * @see #getCollectionOperator()
	 * @see #setCollectionOperator(CollectionOperator)
	 * @generated
	 */
	boolean isSetCollectionOperator();

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' reference.
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression_Collection()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsKindOf(MAbstractExpression)\r\n  then eContainer().oclAsType(MAbstractExpression)\r\n  else null \r\nendif'"
	 * @generated
	 */
	MAbstractExpression getCollection();

	/**
	 * Returns the value of the '<em><b>Iterator Var</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iterator Var</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iterator Var</em>' containment reference.
	 * @see #isSetIteratorVar()
	 * @see #unsetIteratorVar()
	 * @see #setIteratorVar(MIterator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression_IteratorVar()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='Tuple{name=\'it\'}'"
	 * @generated
	 */
	MIterator getIteratorVar();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getIteratorVar <em>Iterator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iterator Var</em>' containment reference.
	 * @see #isSetIteratorVar()
	 * @see #unsetIteratorVar()
	 * @see #getIteratorVar()
	 * @generated
	 */

	void setIteratorVar(MIterator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getIteratorVar <em>Iterator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetIteratorVar()
	 * @see #getIteratorVar()
	 * @see #setIteratorVar(MIterator)
	 * @generated
	 */
	void unsetIteratorVar();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getIteratorVar <em>Iterator Var</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Iterator Var</em>' containment reference is set.
	 * @see #unsetIteratorVar()
	 * @see #getIteratorVar()
	 * @see #setIteratorVar(MIterator)
	 * @generated
	 */
	boolean isSetIteratorVar();

	/**
	 * Returns the value of the '<em><b>Accumulator Var</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Accumulator Var</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Accumulator Var</em>' containment reference.
	 * @see #isSetAccumulatorVar()
	 * @see #unsetAccumulatorVar()
	 * @see #setAccumulatorVar(MAccumulator)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression_AccumulatorVar()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='accumulatorVar'"
	 * @generated
	 */
	MAccumulator getAccumulatorVar();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getAccumulatorVar <em>Accumulator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Accumulator Var</em>' containment reference.
	 * @see #isSetAccumulatorVar()
	 * @see #unsetAccumulatorVar()
	 * @see #getAccumulatorVar()
	 * @generated
	 */

	void setAccumulatorVar(MAccumulator value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getAccumulatorVar <em>Accumulator Var</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAccumulatorVar()
	 * @see #getAccumulatorVar()
	 * @see #setAccumulatorVar(MAccumulator)
	 * @generated
	 */
	void unsetAccumulatorVar();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getAccumulatorVar <em>Accumulator Var</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Accumulator Var</em>' containment reference is set.
	 * @see #unsetAccumulatorVar()
	 * @see #getAccumulatorVar()
	 * @see #setAccumulatorVar(MAccumulator)
	 * @generated
	 */
	boolean isSetAccumulatorVar();

	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @see com.montages.mrules.expressions.ExpressionsPackage#getMCollectionExpression_Expression()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='defaultValue()'"
	 * @generated
	 */
	MChainOrApplication getExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @generated
	 */

	void setExpression(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	void unsetExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.expressions.MCollectionExpression#getExpression <em>Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression</em>' containment reference is set.
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	boolean isSetExpression();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model operatorRequired="true"
	 *        operatorAnnotation="http://www.montages.com/mCore/MCore mName='operator'"
	 *        annotation="http://www.xocl.org/OCL body='if operator= CollectionOperator::Collect then \'collect\' else\r\nif operator= CollectionOperator::Select then \'select\' else\r\nif operator= CollectionOperator::Iterate then \'iterate\' else\r\nif operator= CollectionOperator::Closure then \'closure\' else\r\n\'ERROR\' \r\nendif endif endif endif'"
	 * @generated
	 */
	String collectionOperatorAsCode(CollectionOperator operator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='collectionOperatorAsCode(collectionOperator)'"
	 * @generated
	 */
	String collectionOperatorAsCode();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

} // MCollectionExpression
