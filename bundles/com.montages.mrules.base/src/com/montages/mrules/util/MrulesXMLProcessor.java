/**
 */
package com.montages.mrules.util;

import com.montages.mrules.MrulesPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MrulesXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MrulesXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		MrulesPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the MrulesResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new MrulesResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new MrulesResourceFactoryImpl());
		}
		return registrations;
	}

} //MrulesXMLProcessor
