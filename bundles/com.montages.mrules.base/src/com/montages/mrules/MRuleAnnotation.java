/**
 */

package com.montages.mrules;

import com.montages.acore.abstractions.AAnnotation;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;
import com.montages.mrules.expressions.MAbstractNamedTuple;
import com.montages.mrules.expressions.MBaseChain;
import com.montages.mrules.expressions.MNamedConstant;
import com.montages.mrules.expressions.MNamedExpression;
import com.montages.mrules.expressions.MVariableBaseDefinition;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MRule Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getNamedExpression <em>Named Expression</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getNamedTuple <em>Named Tuple</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getNamedConstant <em>Named Constant</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getAExpectedReturnTypeOfAnnotation <em>AExpected Return Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getAExpectedReturnSimpleTypeOfAnnotation <em>AExpected Return Simple Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getUseExplicitOcl <em>Use Explicit Ocl</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getOclChanged <em>Ocl Changed</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getOclCode <em>Ocl Code</em>}</li>
 *   <li>{@link com.montages.mrules.MRuleAnnotation#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation()
 * @model abstract="true"
 * @generated
 */

public interface MRuleAnnotation extends MBaseChain, AAnnotation {
	/**
	 * Returns the value of the '<em><b>Named Expression</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MNamedExpression}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Expression</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Expression</em>' containment reference list.
	 * @see #isSetNamedExpression()
	 * @see #unsetNamedExpression()
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_NamedExpression()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MNamedExpression> getNamedExpression();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedExpression <em>Named Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedExpression()
	 * @see #getNamedExpression()
	 * @generated
	 */
	void unsetNamedExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedExpression <em>Named Expression</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Expression</em>' containment reference list is set.
	 * @see #unsetNamedExpression()
	 * @see #getNamedExpression()
	 * @generated
	 */
	boolean isSetNamedExpression();

	/**
	 * Returns the value of the '<em><b>Named Tuple</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MAbstractNamedTuple}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Tuple</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Tuple</em>' containment reference list.
	 * @see #isSetNamedTuple()
	 * @see #unsetNamedTuple()
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_NamedTuple()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MAbstractNamedTuple> getNamedTuple();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedTuple <em>Named Tuple</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedTuple()
	 * @see #getNamedTuple()
	 * @generated
	 */
	void unsetNamedTuple();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedTuple <em>Named Tuple</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Tuple</em>' containment reference list is set.
	 * @see #unsetNamedTuple()
	 * @see #getNamedTuple()
	 * @generated
	 */
	boolean isSetNamedTuple();

	/**
	 * Returns the value of the '<em><b>Named Constant</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mrules.expressions.MNamedConstant}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Constant</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Constant</em>' containment reference list.
	 * @see #isSetNamedConstant()
	 * @see #unsetNamedConstant()
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_NamedConstant()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MNamedConstant> getNamedConstant();

	/**
	 * Unsets the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedConstant <em>Named Constant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedConstant()
	 * @see #getNamedConstant()
	 * @generated
	 */
	void unsetNamedConstant();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.MRuleAnnotation#getNamedConstant <em>Named Constant</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Constant</em>' containment reference list is set.
	 * @see #unsetNamedConstant()
	 * @see #getNamedConstant()
	 * @generated
	 */
	boolean isSetNamedConstant();

	/**
	 * Returns the value of the '<em><b>AExpected Return Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AExpected Return Type Of Annotation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AExpected Return Type Of Annotation</em>' reference.
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_AExpectedReturnTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Expected Return Type Of Annotation'"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n/*let apa:annotations::MAbstractPropertyAnnotations = eContainer().oclAsType(MAbstractPropertyAnnotations)\r\nin if apa.annotatedProperty.oclIsUndefined()\r\n  then null \r\n  else apa.annotatedProperty.calculatedType\r\nendif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	AClassifier getAExpectedReturnTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>AExpected Return Simple Type Of Annotation</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AExpected Return Simple Type Of Annotation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AExpected Return Simple Type Of Annotation</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='acore::classifiers::ASimpleType::None\r\n /*\r\nif self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.simpleType\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.simpleType\r\n      else acore::classifiers::ASimpleType::None \r\nendif endif endif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	ASimpleType getAExpectedReturnSimpleTypeOfAnnotation();

	/**
	 * Returns the value of the '<em><b>Is Return Value Of Annotation Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Of Annotation Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Of Annotation Mandatory</em>' attribute.
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_IsReturnValueOfAnnotationMandatory()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n/*if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.mandatory\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.mandatory\r\n      else true\r\nendif endif endif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueOfAnnotationMandatory();

	/**
	 * Returns the value of the '<em><b>Is Return Value Of Annotation Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Return Value Of Annotation Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Return Value Of Annotation Singular</em>' attribute.
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_IsReturnValueOfAnnotationSingular()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\r\n/* if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else\r\n\r\n\r\n\r\nif eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) \r\n  then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.singular\r\n  else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) \r\n      then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.singular\r\n      else true\r\nendif endif endif \052/'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	Boolean getIsReturnValueOfAnnotationSingular();

	/**
	 * Returns the value of the '<em><b>Use Explicit Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Explicit Ocl</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Explicit Ocl</em>' attribute.
	 * @see #isSetUseExplicitOcl()
	 * @see #unsetUseExplicitOcl()
	 * @see #setUseExplicitOcl(Boolean)
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_UseExplicitOcl()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Explicit OCL' createColumn='false'"
	 * @generated
	 */
	Boolean getUseExplicitOcl();

	/** 
	 * Sets the value of the '{@link com.montages.mrules.MRuleAnnotation#getUseExplicitOcl <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Explicit Ocl</em>' attribute.
	 * @see #isSetUseExplicitOcl()
	 * @see #unsetUseExplicitOcl()
	 * @see #getUseExplicitOcl()
	 * @generated
	 */

	void setUseExplicitOcl(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mrules.MRuleAnnotation#getUseExplicitOcl <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUseExplicitOcl()
	 * @see #getUseExplicitOcl()
	 * @see #setUseExplicitOcl(Boolean)
	 * @generated
	 */
	void unsetUseExplicitOcl();

	/**
	 * Returns whether the value of the '{@link com.montages.mrules.MRuleAnnotation#getUseExplicitOcl <em>Use Explicit Ocl</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Use Explicit Ocl</em>' attribute is set.
	 * @see #unsetUseExplicitOcl()
	 * @see #getUseExplicitOcl()
	 * @see #setUseExplicitOcl(Boolean)
	 * @generated
	 */
	boolean isSetUseExplicitOcl();

	/**
	 * Returns the value of the '<em><b>Ocl Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This is a "fake" property: when changed it activate setter code that will populate value with OCL created by any contained MChain.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Ocl Changed</em>' attribute.
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_OclChanged()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Explicit OCL' createColumn='false'"
	 * @generated
	 */
	Boolean getOclChanged();

	/**
	 * Returns the value of the '<em><b>Ocl Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ocl Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ocl Code</em>' attribute.
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_OclCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let attentionImplementedinJava: String = \'TODO Replace Java Code\' in\nattentionImplementedinJava\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Explicit OCL' createColumn='false'"
	 * @generated
	 */
	String getOclCode();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mrules.MRulesAnnotationAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mrules.MRulesAnnotationAction
	 * @see com.montages.mrules.MrulesPackage#getMRuleAnnotation_DoAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mrules::MRulesAnnotationAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MRulesAnnotationAction getDoAction();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	String doActionUpdate(MRulesAnnotationAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model namedExpressionAnnotation="http://www.montages.com/mCore/MCore mName='NamedExpression'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='VariableFromExpression'"
	 *        annotation="http://www.xocl.org/OCL body='if self.localScopeVariables->isEmpty() then null\r\nelse\r\nself.localScopeVariables->select(x:mrules::expressions::MVariableBaseDefinition| if x.namedExpression<> null  then x.namedExpression = namedExpression else false endif )->first() endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Helper' createColumn='true'"
	 * @generated
	 */
	MVariableBaseDefinition variableFromExpression(MNamedExpression namedExpression);

} // MRuleAnnotation
