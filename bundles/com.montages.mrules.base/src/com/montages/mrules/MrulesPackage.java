/**
 */
package com.montages.mrules;

import com.montages.mrules.expressions.ExpressionsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mrules.MrulesFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'MTestRule\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true' uuidAttributeName='_uuid'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface MrulesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mrules.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mrules";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mRules/MRules";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mrules";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MrulesPackage eINSTANCE = com.montages.mrules.impl.MrulesPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.mrules.impl.MRulesElementImpl <em>MRules Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mrules.impl.MRulesElementImpl
	 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesElement()
	 * @generated
	 */
	int MRULES_ELEMENT = 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT__KIND_LABEL = 0;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT__RENDERED_KIND_LABEL = 1;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT__INDENT_LEVEL = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT__AS_TEXT = 4;

	/**
	 * The number of structural features of the '<em>MRules Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT___INDENTATION_SPACES = 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT___INDENTATION_SPACES__INTEGER = 1;

	/**
	 * The number of operations of the '<em>MRules Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_ELEMENT_OPERATION_COUNT = 2;

	/**
	 * The meta object id for the '{@link com.montages.mrules.impl.MRuleAnnotationImpl <em>MRule Annotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mrules.impl.MRuleAnnotationImpl
	 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRuleAnnotation()
	 * @generated
	 */
	int MRULE_ANNOTATION = 1;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__KIND_LABEL = ExpressionsPackage.MBASE_CHAIN__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__RENDERED_KIND_LABEL = ExpressionsPackage.MBASE_CHAIN__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__INDENT_LEVEL = ExpressionsPackage.MBASE_CHAIN__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__DESCRIPTION = ExpressionsPackage.MBASE_CHAIN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AS_TEXT = ExpressionsPackage.MBASE_CHAIN__AS_TEXT;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ALABEL = ExpressionsPackage.MBASE_CHAIN__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AKIND_BASE = ExpressionsPackage.MBASE_CHAIN__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ARENDERED_KIND = ExpressionsPackage.MBASE_CHAIN__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACONTAINING_COMPONENT = ExpressionsPackage.MBASE_CHAIN__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_PACKAGE_URI = ExpressionsPackage.MBASE_CHAIN__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_CLASSIFIER_NAME = ExpressionsPackage.MBASE_CHAIN__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_FEATURE_NAME = ExpressionsPackage.MBASE_CHAIN__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_PACKAGE = ExpressionsPackage.MBASE_CHAIN__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_CLASSIFIER = ExpressionsPackage.MBASE_CHAIN__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_FEATURE = ExpressionsPackage.MBASE_CHAIN__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AT_CORE_ASTRING_CLASS = ExpressionsPackage.MBASE_CHAIN__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACLASSIFIER = ExpressionsPackage.MBASE_CHAIN__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AMANDATORY = ExpressionsPackage.MBASE_CHAIN__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASINGULAR = ExpressionsPackage.MBASE_CHAIN__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ATYPE_LABEL = ExpressionsPackage.MBASE_CHAIN__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AUNDEFINED_TYPE_CONSTANT = ExpressionsPackage.MBASE_CHAIN__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AS_CODE = ExpressionsPackage.MBASE_CHAIN__AS_CODE;

	/**
	 * The feature id for the '<em><b>As Basic Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AS_BASIC_CODE = ExpressionsPackage.MBASE_CHAIN__AS_BASIC_CODE;

	/**
	 * The feature id for the '<em><b>Collector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__COLLECTOR = ExpressionsPackage.MBASE_CHAIN__COLLECTOR;

	/**
	 * The feature id for the '<em><b>Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ENTIRE_SCOPE = ExpressionsPackage.MBASE_CHAIN__ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_BASE = ExpressionsPackage.MBASE_CHAIN__SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_SELF = ExpressionsPackage.MBASE_CHAIN__SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_TRG = ExpressionsPackage.MBASE_CHAIN__SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_OBJ = ExpressionsPackage.MBASE_CHAIN__SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_LITERAL_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_VARIABLES = ExpressionsPackage.MBASE_CHAIN__SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_ITERATOR = ExpressionsPackage.MBASE_CHAIN__SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_ACCUMULATOR = ExpressionsPackage.MBASE_CHAIN__SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_PARAMETERS = ExpressionsPackage.MBASE_CHAIN__SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Scope Container Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_CONTAINER_BASE = ExpressionsPackage.MBASE_CHAIN__SCOPE_CONTAINER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Number Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_NUMBER_BASE = ExpressionsPackage.MBASE_CHAIN__SCOPE_NUMBER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SCOPE_SOURCE = ExpressionsPackage.MBASE_CHAIN__SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_ENTIRE_SCOPE = ExpressionsPackage.MBASE_CHAIN__LOCAL_ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Local Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_BASE = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Local Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_SELF = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Local Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_TRG = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Local Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_OBJ = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Local Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_SOURCE = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_VARIABLES = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Local Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_ITERATOR = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_PARAMETERS = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Local Scope Number Base Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Local Scope Container</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__LOCAL_SCOPE_CONTAINER = ExpressionsPackage.MBASE_CHAIN__LOCAL_SCOPE_CONTAINER;

	/**
	 * The feature id for the '<em><b>Containing Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CONTAINING_ANNOTATION = ExpressionsPackage.MBASE_CHAIN__CONTAINING_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Containing Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CONTAINING_EXPRESSION = ExpressionsPackage.MBASE_CHAIN__CONTAINING_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Complex Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_COMPLEX_EXPRESSION = ExpressionsPackage.MBASE_CHAIN__IS_COMPLEX_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Sub Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_SUB_EXPRESSION = ExpressionsPackage.MBASE_CHAIN__IS_SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>ACalculated Own Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACALCULATED_OWN_TYPE = ExpressionsPackage.MBASE_CHAIN__ACALCULATED_OWN_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Own Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CALCULATED_OWN_MANDATORY = ExpressionsPackage.MBASE_CHAIN__CALCULATED_OWN_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Own Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CALCULATED_OWN_SINGULAR = ExpressionsPackage.MBASE_CHAIN__CALCULATED_OWN_SINGULAR;

	/**
	 * The feature id for the '<em><b>ACalculated Own Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACALCULATED_OWN_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ACALCULATED_OWN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASELF_OBJECT_TYPE = ExpressionsPackage.MBASE_CHAIN__ASELF_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASELF_OBJECT_PACKAGE = ExpressionsPackage.MBASE_CHAIN__ASELF_OBJECT_PACKAGE;

	/**
	 * The feature id for the '<em><b>ATarget Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ATARGET_OBJECT_TYPE = ExpressionsPackage.MBASE_CHAIN__ATARGET_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ATarget Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ATARGET_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ATARGET_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AObject Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AOBJECT_OBJECT_TYPE = ExpressionsPackage.MBASE_CHAIN__AOBJECT_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE = ExpressionsPackage.MBASE_CHAIN__AEXPECTED_RETURN_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__AEXPECTED_RETURN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Is Return Value Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_RETURN_VALUE_MANDATORY = ExpressionsPackage.MBASE_CHAIN__IS_RETURN_VALUE_MANDATORY;

	/**
	 * The feature id for the '<em><b>Is Return Value Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_RETURN_VALUE_SINGULAR = ExpressionsPackage.MBASE_CHAIN__IS_RETURN_VALUE_SINGULAR;

	/**
	 * The feature id for the '<em><b>ASrc Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASRC_OBJECT_TYPE = ExpressionsPackage.MBASE_CHAIN__ASRC_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASrc Object Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ASRC_OBJECT_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ASRC_OBJECT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__BASE_AS_CODE = ExpressionsPackage.MBASE_CHAIN__BASE_AS_CODE;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__BASE = ExpressionsPackage.MBASE_CHAIN__BASE;

	/**
	 * The feature id for the '<em><b>Base Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__BASE_DEFINITION = ExpressionsPackage.MBASE_CHAIN__BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>ABase Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ABASE_VAR = ExpressionsPackage.MBASE_CHAIN__ABASE_VAR;

	/**
	 * The feature id for the '<em><b>ABase Exit Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ABASE_EXIT_TYPE = ExpressionsPackage.MBASE_CHAIN__ABASE_EXIT_TYPE;

	/**
	 * The feature id for the '<em><b>ABase Exit Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ABASE_EXIT_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ABASE_EXIT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base Exit Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__BASE_EXIT_MANDATORY = ExpressionsPackage.MBASE_CHAIN__BASE_EXIT_MANDATORY;

	/**
	 * The feature id for the '<em><b>Base Exit Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__BASE_EXIT_SINGULAR = ExpressionsPackage.MBASE_CHAIN__BASE_EXIT_SINGULAR;

	/**
	 * The feature id for the '<em><b>AChain Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACHAIN_ENTRY_TYPE = ExpressionsPackage.MBASE_CHAIN__ACHAIN_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>Chain As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CHAIN_AS_CODE = ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE;

	/**
	 * The feature id for the '<em><b>AElement1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AELEMENT1 = ExpressionsPackage.MBASE_CHAIN__AELEMENT1;

	/**
	 * The feature id for the '<em><b>Element1 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ELEMENT1_CORRECT = ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement2 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AELEMENT2_ENTRY_TYPE = ExpressionsPackage.MBASE_CHAIN__AELEMENT2_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AELEMENT2 = ExpressionsPackage.MBASE_CHAIN__AELEMENT2;

	/**
	 * The feature id for the '<em><b>Element2 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ELEMENT2_CORRECT = ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement3 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AELEMENT3_ENTRY_TYPE = ExpressionsPackage.MBASE_CHAIN__AELEMENT3_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AELEMENT3 = ExpressionsPackage.MBASE_CHAIN__AELEMENT3;

	/**
	 * The feature id for the '<em><b>Element3 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ELEMENT3_CORRECT = ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT;

	/**
	 * The feature id for the '<em><b>ACast Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACAST_TYPE = ExpressionsPackage.MBASE_CHAIN__ACAST_TYPE;

	/**
	 * The feature id for the '<em><b>ALast Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ALAST_ELEMENT = ExpressionsPackage.MBASE_CHAIN__ALAST_ELEMENT;

	/**
	 * The feature id for the '<em><b>AChain Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACHAIN_CALCULATED_TYPE = ExpressionsPackage.MBASE_CHAIN__ACHAIN_CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>AChain Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__ACHAIN_CALCULATED_SIMPLE_TYPE = ExpressionsPackage.MBASE_CHAIN__ACHAIN_CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Chain Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CHAIN_CALCULATED_SINGULAR = ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Processor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__PROCESSOR = ExpressionsPackage.MBASE_CHAIN__PROCESSOR;

	/**
	 * The feature id for the '<em><b>Processor Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__PROCESSOR_DEFINITION = ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION;

	/**
	 * The feature id for the '<em><b>Type Mismatch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__TYPE_MISMATCH = ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH;

	/**
	 * The feature id for the '<em><b>Call Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CALL_ARGUMENT = ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Sub Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__SUB_EXPRESSION = ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CONTAINED_COLLECTOR = ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR;

	/**
	 * The feature id for the '<em><b>Chain Codefor Subchains</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS = ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS;

	/**
	 * The feature id for the '<em><b>Is Own XOCL Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_OWN_XOCL_OP = ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AANNOTATED = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AANNOTATING = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Named Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__NAMED_EXPRESSION = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Named Tuple</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__NAMED_TUPLE = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Named Constant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__NAMED_CONSTANT = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AExpected Return Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type Of Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Use Explicit Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__USE_EXPLICIT_OCL = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Ocl Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__OCL_CHANGED = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Ocl Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__OCL_CODE = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION__DO_ACTION = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 12;

	/**
	 * The number of structural features of the '<em>MRule Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION_FEATURE_COUNT = ExpressionsPackage.MBASE_CHAIN_FEATURE_COUNT + 13;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___INDENTATION_SPACES = ExpressionsPackage.MBASE_CHAIN___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___INDENTATION_SPACES__INTEGER = ExpressionsPackage.MBASE_CHAIN___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AINDENT_LEVEL = ExpressionsPackage.MBASE_CHAIN___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AINDENTATION_SPACES = ExpressionsPackage.MBASE_CHAIN___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AINDENTATION_SPACES__INTEGER = ExpressionsPackage.MBASE_CHAIN___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ASTRING_OR_MISSING__STRING = ExpressionsPackage.MBASE_CHAIN___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ASTRING_IS_EMPTY__STRING = ExpressionsPackage.MBASE_CHAIN___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ExpressionsPackage.MBASE_CHAIN___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ExpressionsPackage.MBASE_CHAIN___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___APACKAGE_FROM_URI__STRING = ExpressionsPackage.MBASE_CHAIN___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ExpressionsPackage.MBASE_CHAIN___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ExpressionsPackage.MBASE_CHAIN___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ACORE_ASTRING_CLASS = ExpressionsPackage.MBASE_CHAIN___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ACORE_AREAL_CLASS = ExpressionsPackage.MBASE_CHAIN___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ACORE_AINTEGER_CLASS = ExpressionsPackage.MBASE_CHAIN___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ACORE_AOBJECT_CLASS = ExpressionsPackage.MBASE_CHAIN___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = ExpressionsPackage.MBASE_CHAIN___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Get Short Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___GET_SHORT_CODE = ExpressionsPackage.MBASE_CHAIN___GET_SHORT_CODE;

	/**
	 * The operation id for the '<em>Get Scope</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___GET_SCOPE = ExpressionsPackage.MBASE_CHAIN___GET_SCOPE;

	/**
	 * The operation id for the '<em>Base Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION = ExpressionsPackage.MBASE_CHAIN___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION;

	/**
	 * The operation id for the '<em>As Code For Built In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AS_CODE_FOR_BUILT_IN = ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_BUILT_IN;

	/**
	 * The operation id for the '<em>As Code For Constants</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AS_CODE_FOR_CONSTANTS = ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_CONSTANTS;

	/**
	 * The operation id for the '<em>Length</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___LENGTH = ExpressionsPackage.MBASE_CHAIN___LENGTH;

	/**
	 * The operation id for the '<em>Unsafe Chain Step As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER = ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER = ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER = ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Proc As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_AS_CODE = ExpressionsPackage.MBASE_CHAIN___PROC_AS_CODE;

	/**
	 * The operation id for the '<em>Is Custom Code Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_CUSTOM_CODE_PROCESSOR = ExpressionsPackage.MBASE_CHAIN___IS_CUSTOM_CODE_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Processor Set Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_PROCESSOR_SET_OPERATOR = ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_SET_OPERATOR;

	/**
	 * The operation id for the '<em>Is Own XOCL Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_OWN_XOCL_OPERATOR = ExpressionsPackage.MBASE_CHAIN___IS_OWN_XOCL_OPERATOR;

	/**
	 * The operation id for the '<em>Processor Returns Singular</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROCESSOR_RETURNS_SINGULAR = ExpressionsPackage.MBASE_CHAIN___PROCESSOR_RETURNS_SINGULAR;

	/**
	 * The operation id for the '<em>Processor Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROCESSOR_IS_SET = ExpressionsPackage.MBASE_CHAIN___PROCESSOR_IS_SET;

	/**
	 * The operation id for the '<em>Create Processor Definition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___CREATE_PROCESSOR_DEFINITION = ExpressionsPackage.MBASE_CHAIN___CREATE_PROCESSOR_DEFINITION;

	/**
	 * The operation id for the '<em>Proc Def Choices For Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECT = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT;

	/**
	 * The operation id for the '<em>Proc Def Choices For Objects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECTS = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Boolean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEAN = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN;

	/**
	 * The operation id for the '<em>Proc Def Choices For Booleans</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEANS = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGER = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGERS = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Real</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REAL = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REAL;

	/**
	 * The operation id for the '<em>Proc Def Choices For Reals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REALS = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REALS;

	/**
	 * The operation id for the '<em>Proc Def Choices For String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRING = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRING;

	/**
	 * The operation id for the '<em>Proc Def Choices For Strings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRINGS = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATE = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATE;

	/**
	 * The operation id for the '<em>Proc Def Choices For Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATES = ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATES;

	/**
	 * The operation id for the '<em>Auto Cast With Proc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AUTO_CAST_WITH_PROC = ExpressionsPackage.MBASE_CHAIN___AUTO_CAST_WITH_PROC;

	/**
	 * The operation id for the '<em>Own To Apply Mismatch</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___OWN_TO_APPLY_MISMATCH = ExpressionsPackage.MBASE_CHAIN___OWN_TO_APPLY_MISMATCH;

	/**
	 * The operation id for the '<em>Unique Chain Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___UNIQUE_CHAIN_NUMBER = ExpressionsPackage.MBASE_CHAIN___UNIQUE_CHAIN_NUMBER;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN = ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN;

	/**
	 * The operation id for the '<em>Reset To Base</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE = ExpressionsPackage.MBASE_CHAIN___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain As Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN = ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN;

	/**
	 * The operation id for the '<em>Is Processor Check Equal Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_PROCESSOR_CHECK_EQUAL_OPERATOR = ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_CHECK_EQUAL_OPERATOR;

	/**
	 * The operation id for the '<em>Is Prefix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_PREFIX_PROCESSOR = ExpressionsPackage.MBASE_CHAIN___IS_PREFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Postfix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___IS_POSTFIX_PROCESSOR = ExpressionsPackage.MBASE_CHAIN___IS_POSTFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>As Code For Others</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AS_CODE_FOR_OTHERS = ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_OTHERS;

	/**
	 * The operation id for the '<em>Unsafe Element As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER = ExpressionsPackage.MBASE_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Code For Length1</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___CODE_FOR_LENGTH1 = ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH1;

	/**
	 * The operation id for the '<em>Code For Length2</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___CODE_FOR_LENGTH2 = ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH2;

	/**
	 * The operation id for the '<em>Code For Length3</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___CODE_FOR_LENGTH3 = ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH3;

	/**
	 * The operation id for the '<em>As Code For Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___AS_CODE_FOR_VARIABLES = ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_VARIABLES;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION = ExpressionsPackage.MBASE_CHAIN_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Variable From Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION = ExpressionsPackage.MBASE_CHAIN_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>MRule Annotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULE_ANNOTATION_OPERATION_COUNT = ExpressionsPackage.MBASE_CHAIN_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mrules.impl.MTestRuleImpl <em>MTest Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mrules.impl.MTestRuleImpl
	 * @see com.montages.mrules.impl.MrulesPackageImpl#getMTestRule()
	 * @generated
	 */
	int MTEST_RULE = 0;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__KIND_LABEL = MRULE_ANNOTATION__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__RENDERED_KIND_LABEL = MRULE_ANNOTATION__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__INDENT_LEVEL = MRULE_ANNOTATION__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__DESCRIPTION = MRULE_ANNOTATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AS_TEXT = MRULE_ANNOTATION__AS_TEXT;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ALABEL = MRULE_ANNOTATION__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AKIND_BASE = MRULE_ANNOTATION__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ARENDERED_KIND = MRULE_ANNOTATION__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACONTAINING_COMPONENT = MRULE_ANNOTATION__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_PACKAGE_URI = MRULE_ANNOTATION__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_CLASSIFIER_NAME = MRULE_ANNOTATION__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_FEATURE_NAME = MRULE_ANNOTATION__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_PACKAGE = MRULE_ANNOTATION__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_CLASSIFIER = MRULE_ANNOTATION__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_FEATURE = MRULE_ANNOTATION__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AT_CORE_ASTRING_CLASS = MRULE_ANNOTATION__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACLASSIFIER = MRULE_ANNOTATION__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AMANDATORY = MRULE_ANNOTATION__AMANDATORY;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASINGULAR = MRULE_ANNOTATION__ASINGULAR;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ATYPE_LABEL = MRULE_ANNOTATION__ATYPE_LABEL;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AUNDEFINED_TYPE_CONSTANT = MRULE_ANNOTATION__AUNDEFINED_TYPE_CONSTANT;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASIMPLE_TYPE = MRULE_ANNOTATION__ASIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AS_CODE = MRULE_ANNOTATION__AS_CODE;

	/**
	 * The feature id for the '<em><b>As Basic Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AS_BASIC_CODE = MRULE_ANNOTATION__AS_BASIC_CODE;

	/**
	 * The feature id for the '<em><b>Collector</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__COLLECTOR = MRULE_ANNOTATION__COLLECTOR;

	/**
	 * The feature id for the '<em><b>Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ENTIRE_SCOPE = MRULE_ANNOTATION__ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_BASE = MRULE_ANNOTATION__SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_SELF = MRULE_ANNOTATION__SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_TRG = MRULE_ANNOTATION__SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_OBJ = MRULE_ANNOTATION__SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_SIMPLE_TYPE_CONSTANTS = MRULE_ANNOTATION__SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_LITERAL_CONSTANTS = MRULE_ANNOTATION__SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_OBJECT_REFERENCE_CONSTANTS = MRULE_ANNOTATION__SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_VARIABLES = MRULE_ANNOTATION__SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_ITERATOR = MRULE_ANNOTATION__SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_ACCUMULATOR = MRULE_ANNOTATION__SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_PARAMETERS = MRULE_ANNOTATION__SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Scope Container Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_CONTAINER_BASE = MRULE_ANNOTATION__SCOPE_CONTAINER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Number Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_NUMBER_BASE = MRULE_ANNOTATION__SCOPE_NUMBER_BASE;

	/**
	 * The feature id for the '<em><b>Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SCOPE_SOURCE = MRULE_ANNOTATION__SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Entire Scope</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_ENTIRE_SCOPE = MRULE_ANNOTATION__LOCAL_ENTIRE_SCOPE;

	/**
	 * The feature id for the '<em><b>Local Scope Base</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_BASE = MRULE_ANNOTATION__LOCAL_SCOPE_BASE;

	/**
	 * The feature id for the '<em><b>Local Scope Self</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_SELF = MRULE_ANNOTATION__LOCAL_SCOPE_SELF;

	/**
	 * The feature id for the '<em><b>Local Scope Trg</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_TRG = MRULE_ANNOTATION__LOCAL_SCOPE_TRG;

	/**
	 * The feature id for the '<em><b>Local Scope Obj</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_OBJ = MRULE_ANNOTATION__LOCAL_SCOPE_OBJ;

	/**
	 * The feature id for the '<em><b>Local Scope Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_SOURCE = MRULE_ANNOTATION__LOCAL_SCOPE_SOURCE;

	/**
	 * The feature id for the '<em><b>Local Scope Simple Type Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS = MRULE_ANNOTATION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Literal Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_LITERAL_CONSTANTS = MRULE_ANNOTATION__LOCAL_SCOPE_LITERAL_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Object Reference Constants</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS = MRULE_ANNOTATION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

	/**
	 * The feature id for the '<em><b>Local Scope Variables</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_VARIABLES = MRULE_ANNOTATION__LOCAL_SCOPE_VARIABLES;

	/**
	 * The feature id for the '<em><b>Local Scope Iterator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_ITERATOR = MRULE_ANNOTATION__LOCAL_SCOPE_ITERATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Accumulator</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_ACCUMULATOR = MRULE_ANNOTATION__LOCAL_SCOPE_ACCUMULATOR;

	/**
	 * The feature id for the '<em><b>Local Scope Parameters</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_PARAMETERS = MRULE_ANNOTATION__LOCAL_SCOPE_PARAMETERS;

	/**
	 * The feature id for the '<em><b>Local Scope Number Base Definition</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_NUMBER_BASE_DEFINITION = MRULE_ANNOTATION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>Local Scope Container</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__LOCAL_SCOPE_CONTAINER = MRULE_ANNOTATION__LOCAL_SCOPE_CONTAINER;

	/**
	 * The feature id for the '<em><b>Containing Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CONTAINING_ANNOTATION = MRULE_ANNOTATION__CONTAINING_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Containing Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CONTAINING_EXPRESSION = MRULE_ANNOTATION__CONTAINING_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Complex Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_COMPLEX_EXPRESSION = MRULE_ANNOTATION__IS_COMPLEX_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Is Sub Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_SUB_EXPRESSION = MRULE_ANNOTATION__IS_SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>ACalculated Own Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACALCULATED_OWN_TYPE = MRULE_ANNOTATION__ACALCULATED_OWN_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Own Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CALCULATED_OWN_MANDATORY = MRULE_ANNOTATION__CALCULATED_OWN_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Own Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CALCULATED_OWN_SINGULAR = MRULE_ANNOTATION__CALCULATED_OWN_SINGULAR;

	/**
	 * The feature id for the '<em><b>ACalculated Own Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACALCULATED_OWN_SIMPLE_TYPE = MRULE_ANNOTATION__ACALCULATED_OWN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASELF_OBJECT_TYPE = MRULE_ANNOTATION__ASELF_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASelf Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASELF_OBJECT_PACKAGE = MRULE_ANNOTATION__ASELF_OBJECT_PACKAGE;

	/**
	 * The feature id for the '<em><b>ATarget Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ATARGET_OBJECT_TYPE = MRULE_ANNOTATION__ATARGET_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ATarget Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ATARGET_SIMPLE_TYPE = MRULE_ANNOTATION__ATARGET_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>AObject Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AOBJECT_OBJECT_TYPE = MRULE_ANNOTATION__AOBJECT_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AEXPECTED_RETURN_TYPE = MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AEXPECTED_RETURN_SIMPLE_TYPE = MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Is Return Value Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_RETURN_VALUE_MANDATORY = MRULE_ANNOTATION__IS_RETURN_VALUE_MANDATORY;

	/**
	 * The feature id for the '<em><b>Is Return Value Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_RETURN_VALUE_SINGULAR = MRULE_ANNOTATION__IS_RETURN_VALUE_SINGULAR;

	/**
	 * The feature id for the '<em><b>ASrc Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASRC_OBJECT_TYPE = MRULE_ANNOTATION__ASRC_OBJECT_TYPE;

	/**
	 * The feature id for the '<em><b>ASrc Object Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ASRC_OBJECT_SIMPLE_TYPE = MRULE_ANNOTATION__ASRC_OBJECT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__BASE_AS_CODE = MRULE_ANNOTATION__BASE_AS_CODE;

	/**
	 * The feature id for the '<em><b>Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__BASE = MRULE_ANNOTATION__BASE;

	/**
	 * The feature id for the '<em><b>Base Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__BASE_DEFINITION = MRULE_ANNOTATION__BASE_DEFINITION;

	/**
	 * The feature id for the '<em><b>ABase Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ABASE_VAR = MRULE_ANNOTATION__ABASE_VAR;

	/**
	 * The feature id for the '<em><b>ABase Exit Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ABASE_EXIT_TYPE = MRULE_ANNOTATION__ABASE_EXIT_TYPE;

	/**
	 * The feature id for the '<em><b>ABase Exit Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ABASE_EXIT_SIMPLE_TYPE = MRULE_ANNOTATION__ABASE_EXIT_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Base Exit Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__BASE_EXIT_MANDATORY = MRULE_ANNOTATION__BASE_EXIT_MANDATORY;

	/**
	 * The feature id for the '<em><b>Base Exit Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__BASE_EXIT_SINGULAR = MRULE_ANNOTATION__BASE_EXIT_SINGULAR;

	/**
	 * The feature id for the '<em><b>AChain Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACHAIN_ENTRY_TYPE = MRULE_ANNOTATION__ACHAIN_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>Chain As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CHAIN_AS_CODE = MRULE_ANNOTATION__CHAIN_AS_CODE;

	/**
	 * The feature id for the '<em><b>AElement1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AELEMENT1 = MRULE_ANNOTATION__AELEMENT1;

	/**
	 * The feature id for the '<em><b>Element1 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ELEMENT1_CORRECT = MRULE_ANNOTATION__ELEMENT1_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement2 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AELEMENT2_ENTRY_TYPE = MRULE_ANNOTATION__AELEMENT2_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AELEMENT2 = MRULE_ANNOTATION__AELEMENT2;

	/**
	 * The feature id for the '<em><b>Element2 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ELEMENT2_CORRECT = MRULE_ANNOTATION__ELEMENT2_CORRECT;

	/**
	 * The feature id for the '<em><b>AElement3 Entry Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AELEMENT3_ENTRY_TYPE = MRULE_ANNOTATION__AELEMENT3_ENTRY_TYPE;

	/**
	 * The feature id for the '<em><b>AElement3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AELEMENT3 = MRULE_ANNOTATION__AELEMENT3;

	/**
	 * The feature id for the '<em><b>Element3 Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ELEMENT3_CORRECT = MRULE_ANNOTATION__ELEMENT3_CORRECT;

	/**
	 * The feature id for the '<em><b>ACast Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACAST_TYPE = MRULE_ANNOTATION__ACAST_TYPE;

	/**
	 * The feature id for the '<em><b>ALast Element</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ALAST_ELEMENT = MRULE_ANNOTATION__ALAST_ELEMENT;

	/**
	 * The feature id for the '<em><b>AChain Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACHAIN_CALCULATED_TYPE = MRULE_ANNOTATION__ACHAIN_CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>AChain Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__ACHAIN_CALCULATED_SIMPLE_TYPE = MRULE_ANNOTATION__ACHAIN_CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Chain Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CHAIN_CALCULATED_SINGULAR = MRULE_ANNOTATION__CHAIN_CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Processor</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__PROCESSOR = MRULE_ANNOTATION__PROCESSOR;

	/**
	 * The feature id for the '<em><b>Processor Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__PROCESSOR_DEFINITION = MRULE_ANNOTATION__PROCESSOR_DEFINITION;

	/**
	 * The feature id for the '<em><b>Type Mismatch</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__TYPE_MISMATCH = MRULE_ANNOTATION__TYPE_MISMATCH;

	/**
	 * The feature id for the '<em><b>Call Argument</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CALL_ARGUMENT = MRULE_ANNOTATION__CALL_ARGUMENT;

	/**
	 * The feature id for the '<em><b>Sub Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__SUB_EXPRESSION = MRULE_ANNOTATION__SUB_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Contained Collector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CONTAINED_COLLECTOR = MRULE_ANNOTATION__CONTAINED_COLLECTOR;

	/**
	 * The feature id for the '<em><b>Chain Codefor Subchains</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__CHAIN_CODEFOR_SUBCHAINS = MRULE_ANNOTATION__CHAIN_CODEFOR_SUBCHAINS;

	/**
	 * The feature id for the '<em><b>Is Own XOCL Op</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_OWN_XOCL_OP = MRULE_ANNOTATION__IS_OWN_XOCL_OP;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AANNOTATED = MRULE_ANNOTATION__AANNOTATED;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AANNOTATING = MRULE_ANNOTATION__AANNOTATING;

	/**
	 * The feature id for the '<em><b>Named Expression</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__NAMED_EXPRESSION = MRULE_ANNOTATION__NAMED_EXPRESSION;

	/**
	 * The feature id for the '<em><b>Named Tuple</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__NAMED_TUPLE = MRULE_ANNOTATION__NAMED_TUPLE;

	/**
	 * The feature id for the '<em><b>Named Constant</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__NAMED_CONSTANT = MRULE_ANNOTATION__NAMED_CONSTANT;

	/**
	 * The feature id for the '<em><b>AExpected Return Type Of Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AEXPECTED_RETURN_TYPE_OF_ANNOTATION = MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION;

	/**
	 * The feature id for the '<em><b>AExpected Return Simple Type Of Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION = MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY = MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

	/**
	 * The feature id for the '<em><b>Is Return Value Of Annotation Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR = MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

	/**
	 * The feature id for the '<em><b>Use Explicit Ocl</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__USE_EXPLICIT_OCL = MRULE_ANNOTATION__USE_EXPLICIT_OCL;

	/**
	 * The feature id for the '<em><b>Ocl Changed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__OCL_CHANGED = MRULE_ANNOTATION__OCL_CHANGED;

	/**
	 * The feature id for the '<em><b>Ocl Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__OCL_CODE = MRULE_ANNOTATION__OCL_CODE;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE__DO_ACTION = MRULE_ANNOTATION__DO_ACTION;

	/**
	 * The number of structural features of the '<em>MTest Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE_FEATURE_COUNT = MRULE_ANNOTATION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___INDENTATION_SPACES = MRULE_ANNOTATION___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___INDENTATION_SPACES__INTEGER = MRULE_ANNOTATION___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AINDENT_LEVEL = MRULE_ANNOTATION___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AINDENTATION_SPACES = MRULE_ANNOTATION___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AINDENTATION_SPACES__INTEGER = MRULE_ANNOTATION___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ASTRING_OR_MISSING__STRING = MRULE_ANNOTATION___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ASTRING_IS_EMPTY__STRING = MRULE_ANNOTATION___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MRULE_ANNOTATION___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___APACKAGE_FROM_URI__STRING = MRULE_ANNOTATION___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = MRULE_ANNOTATION___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = MRULE_ANNOTATION___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ACORE_ASTRING_CLASS = MRULE_ANNOTATION___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ACORE_AREAL_CLASS = MRULE_ANNOTATION___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ACORE_AINTEGER_CLASS = MRULE_ANNOTATION___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ACORE_AOBJECT_CLASS = MRULE_ANNOTATION___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = MRULE_ANNOTATION___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Get Short Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___GET_SHORT_CODE = MRULE_ANNOTATION___GET_SHORT_CODE;

	/**
	 * The operation id for the '<em>Get Scope</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___GET_SCOPE = MRULE_ANNOTATION___GET_SCOPE;

	/**
	 * The operation id for the '<em>Base Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION = MRULE_ANNOTATION___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION;

	/**
	 * The operation id for the '<em>As Code For Built In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AS_CODE_FOR_BUILT_IN = MRULE_ANNOTATION___AS_CODE_FOR_BUILT_IN;

	/**
	 * The operation id for the '<em>As Code For Constants</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AS_CODE_FOR_CONSTANTS = MRULE_ANNOTATION___AS_CODE_FOR_CONSTANTS;

	/**
	 * The operation id for the '<em>Length</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___LENGTH = MRULE_ANNOTATION___LENGTH;

	/**
	 * The operation id for the '<em>Unsafe Chain Step As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER = MRULE_ANNOTATION___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___UNSAFE_CHAIN_AS_CODE__INTEGER = MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Unsafe Chain As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER = MRULE_ANNOTATION___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;

	/**
	 * The operation id for the '<em>Proc As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_AS_CODE = MRULE_ANNOTATION___PROC_AS_CODE;

	/**
	 * The operation id for the '<em>Is Custom Code Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_CUSTOM_CODE_PROCESSOR = MRULE_ANNOTATION___IS_CUSTOM_CODE_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Processor Set Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_PROCESSOR_SET_OPERATOR = MRULE_ANNOTATION___IS_PROCESSOR_SET_OPERATOR;

	/**
	 * The operation id for the '<em>Is Own XOCL Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_OWN_XOCL_OPERATOR = MRULE_ANNOTATION___IS_OWN_XOCL_OPERATOR;

	/**
	 * The operation id for the '<em>Processor Returns Singular</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROCESSOR_RETURNS_SINGULAR = MRULE_ANNOTATION___PROCESSOR_RETURNS_SINGULAR;

	/**
	 * The operation id for the '<em>Processor Is Set</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROCESSOR_IS_SET = MRULE_ANNOTATION___PROCESSOR_IS_SET;

	/**
	 * The operation id for the '<em>Create Processor Definition</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___CREATE_PROCESSOR_DEFINITION = MRULE_ANNOTATION___CREATE_PROCESSOR_DEFINITION;

	/**
	 * The operation id for the '<em>Proc Def Choices For Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_OBJECT = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECT;

	/**
	 * The operation id for the '<em>Proc Def Choices For Objects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_OBJECTS = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_OBJECTS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Boolean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_BOOLEAN = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEAN;

	/**
	 * The operation id for the '<em>Proc Def Choices For Booleans</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_BOOLEANS = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_BOOLEANS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integer</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_INTEGER = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGER;

	/**
	 * The operation id for the '<em>Proc Def Choices For Integers</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_INTEGERS = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_INTEGERS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Real</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_REAL = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REAL;

	/**
	 * The operation id for the '<em>Proc Def Choices For Reals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_REALS = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_REALS;

	/**
	 * The operation id for the '<em>Proc Def Choices For String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_STRING = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRING;

	/**
	 * The operation id for the '<em>Proc Def Choices For Strings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_STRINGS = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_STRINGS;

	/**
	 * The operation id for the '<em>Proc Def Choices For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_DATE = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATE;

	/**
	 * The operation id for the '<em>Proc Def Choices For Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___PROC_DEF_CHOICES_FOR_DATES = MRULE_ANNOTATION___PROC_DEF_CHOICES_FOR_DATES;

	/**
	 * The operation id for the '<em>Auto Cast With Proc</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AUTO_CAST_WITH_PROC = MRULE_ANNOTATION___AUTO_CAST_WITH_PROC;

	/**
	 * The operation id for the '<em>Own To Apply Mismatch</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___OWN_TO_APPLY_MISMATCH = MRULE_ANNOTATION___OWN_TO_APPLY_MISMATCH;

	/**
	 * The operation id for the '<em>Unique Chain Number</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___UNIQUE_CHAIN_NUMBER = MRULE_ANNOTATION___UNIQUE_CHAIN_NUMBER;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN = MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN;

	/**
	 * The operation id for the '<em>Reset To Base</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE = MRULE_ANNOTATION___RESET_TO_BASE__EXPRESSIONBASE_AVARIABLE;

	/**
	 * The operation id for the '<em>Reuse From Other No More Used Chain As Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN = MRULE_ANNOTATION___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN;

	/**
	 * The operation id for the '<em>Is Processor Check Equal Operator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_PROCESSOR_CHECK_EQUAL_OPERATOR = MRULE_ANNOTATION___IS_PROCESSOR_CHECK_EQUAL_OPERATOR;

	/**
	 * The operation id for the '<em>Is Prefix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_PREFIX_PROCESSOR = MRULE_ANNOTATION___IS_PREFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>Is Postfix Processor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___IS_POSTFIX_PROCESSOR = MRULE_ANNOTATION___IS_POSTFIX_PROCESSOR;

	/**
	 * The operation id for the '<em>As Code For Others</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AS_CODE_FOR_OTHERS = MRULE_ANNOTATION___AS_CODE_FOR_OTHERS;

	/**
	 * The operation id for the '<em>Unsafe Element As Code</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___UNSAFE_ELEMENT_AS_CODE__INTEGER = MRULE_ANNOTATION___UNSAFE_ELEMENT_AS_CODE__INTEGER;

	/**
	 * The operation id for the '<em>Code For Length1</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___CODE_FOR_LENGTH1 = MRULE_ANNOTATION___CODE_FOR_LENGTH1;

	/**
	 * The operation id for the '<em>Code For Length2</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___CODE_FOR_LENGTH2 = MRULE_ANNOTATION___CODE_FOR_LENGTH2;

	/**
	 * The operation id for the '<em>Code For Length3</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___CODE_FOR_LENGTH3 = MRULE_ANNOTATION___CODE_FOR_LENGTH3;

	/**
	 * The operation id for the '<em>As Code For Variables</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___AS_CODE_FOR_VARIABLES = MRULE_ANNOTATION___AS_CODE_FOR_VARIABLES;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___DO_ACTION_UPDATE__MRULESANNOTATIONACTION = MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION;

	/**
	 * The operation id for the '<em>Variable From Expression</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION = MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION;

	/**
	 * The number of operations of the '<em>MTest Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTEST_RULE_OPERATION_COUNT = MRULE_ANNOTATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mrules.impl.MRulesNamedImpl <em>MRules Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mrules.impl.MRulesNamedImpl
	 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesNamed()
	 * @generated
	 */
	int MRULES_NAMED = 3;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__KIND_LABEL = MRULES_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__RENDERED_KIND_LABEL = MRULES_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__INDENT_LEVEL = MRULES_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__DESCRIPTION = MRULES_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AS_TEXT = MRULES_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ALABEL = MRULES_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AKIND_BASE = MRULES_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ARENDERED_KIND = MRULES_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ACONTAINING_COMPONENT = MRULES_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_PACKAGE_URI = MRULES_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_CLASSIFIER_NAME = MRULES_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_FEATURE_NAME = MRULES_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_PACKAGE = MRULES_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_CLASSIFIER = MRULES_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_FEATURE = MRULES_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AT_CORE_ASTRING_CLASS = MRULES_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ANAME = MRULES_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__AUNDEFINED_NAME_CONSTANT = MRULES_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ABUSINESS_NAME = MRULES_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__SPECIAL_ENAME = MRULES_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__NAME = MRULES_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__SHORT_NAME = MRULES_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__ENAME = MRULES_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__FULL_LABEL = MRULES_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__LOCAL_STRUCTURAL_NAME = MRULES_ELEMENT_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__CALCULATED_NAME = MRULES_ELEMENT_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__CALCULATED_SHORT_NAME = MRULES_ELEMENT_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__CORRECT_NAME = MRULES_ELEMENT_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED__CORRECT_SHORT_NAME = MRULES_ELEMENT_FEATURE_COUNT + 23;

	/**
	 * The number of structural features of the '<em>MRules Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED_FEATURE_COUNT = MRULES_ELEMENT_FEATURE_COUNT + 24;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___INDENTATION_SPACES = MRULES_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___INDENTATION_SPACES__INTEGER = MRULES_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___AINDENT_LEVEL = MRULES_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___AINDENTATION_SPACES = MRULES_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___AINDENTATION_SPACES__INTEGER = MRULES_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ASTRING_OR_MISSING__STRING = MRULES_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ASTRING_IS_EMPTY__STRING = MRULES_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MRULES_ELEMENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MRULES_ELEMENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___APACKAGE_FROM_URI__STRING = MRULES_ELEMENT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = MRULES_ELEMENT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = MRULES_ELEMENT_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ACORE_ASTRING_CLASS = MRULES_ELEMENT_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ACORE_AREAL_CLASS = MRULES_ELEMENT_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ACORE_AINTEGER_CLASS = MRULES_ELEMENT_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___ACORE_AOBJECT_CLASS = MRULES_ELEMENT_OPERATION_COUNT + 13;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___SAME_NAME__MRULESNAMED = MRULES_ELEMENT_OPERATION_COUNT + 14;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___SAME_SHORT_NAME__MRULESNAMED = MRULES_ELEMENT_OPERATION_COUNT + 15;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___SAME_STRING__STRING_STRING = MRULES_ELEMENT_OPERATION_COUNT + 16;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED___STRING_EMPTY__STRING = MRULES_ELEMENT_OPERATION_COUNT + 17;

	/**
	 * The number of operations of the '<em>MRules Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRULES_NAMED_OPERATION_COUNT = MRULES_ELEMENT_OPERATION_COUNT + 18;

	/**
	 * The meta object id for the '{@link com.montages.mrules.MRulesAnnotationAction <em>MRules Annotation Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mrules.MRulesAnnotationAction
	 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesAnnotationAction()
	 * @generated
	 */
	int MRULES_ANNOTATION_ACTION = 4;

	/**
	 * Returns the meta object for class '{@link com.montages.mrules.MTestRule <em>MTest Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MTest Rule</em>'.
	 * @see com.montages.mrules.MTestRule
	 * @generated
	 */
	EClass getMTestRule();

	/**
	 * Returns the meta object for class '{@link com.montages.mrules.MRuleAnnotation <em>MRule Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MRule Annotation</em>'.
	 * @see com.montages.mrules.MRuleAnnotation
	 * @generated
	 */
	EClass getMRuleAnnotation();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mrules.MRuleAnnotation#getNamedExpression <em>Named Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Expression</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getNamedExpression()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EReference getMRuleAnnotation_NamedExpression();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mrules.MRuleAnnotation#getNamedTuple <em>Named Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Tuple</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getNamedTuple()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EReference getMRuleAnnotation_NamedTuple();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mrules.MRuleAnnotation#getNamedConstant <em>Named Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Constant</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getNamedConstant()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EReference getMRuleAnnotation_NamedConstant();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mrules.MRuleAnnotation#getAExpectedReturnTypeOfAnnotation <em>AExpected Return Type Of Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AExpected Return Type Of Annotation</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getAExpectedReturnTypeOfAnnotation()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EReference getMRuleAnnotation_AExpectedReturnTypeOfAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getAExpectedReturnSimpleTypeOfAnnotation <em>AExpected Return Simple Type Of Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AExpected Return Simple Type Of Annotation</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getAExpectedReturnSimpleTypeOfAnnotation()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Return Value Of Annotation Mandatory</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationMandatory()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_IsReturnValueOfAnnotationMandatory();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Return Value Of Annotation Singular</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getIsReturnValueOfAnnotationSingular()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_IsReturnValueOfAnnotationSingular();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getUseExplicitOcl <em>Use Explicit Ocl</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Explicit Ocl</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getUseExplicitOcl()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_UseExplicitOcl();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getOclChanged <em>Ocl Changed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ocl Changed</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getOclChanged()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_OclChanged();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getOclCode <em>Ocl Code</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ocl Code</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getOclCode()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_OclCode();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRuleAnnotation#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mrules.MRuleAnnotation#getDoAction()
	 * @see #getMRuleAnnotation()
	 * @generated
	 */
	EAttribute getMRuleAnnotation_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRuleAnnotation#doActionUpdate(com.montages.mrules.MRulesAnnotationAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mrules.MRuleAnnotation#doActionUpdate(com.montages.mrules.MRulesAnnotationAction)
	 * @generated
	 */
	EOperation getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRuleAnnotation#variableFromExpression(com.montages.mrules.expressions.MNamedExpression) <em>Variable From Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Variable From Expression</em>' operation.
	 * @see com.montages.mrules.MRuleAnnotation#variableFromExpression(com.montages.mrules.expressions.MNamedExpression)
	 * @generated
	 */
	EOperation getMRuleAnnotation__VariableFromExpression__MNamedExpression();

	/**
	 * Returns the meta object for class '{@link com.montages.mrules.MRulesElement <em>MRules Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MRules Element</em>'.
	 * @see com.montages.mrules.MRulesElement
	 * @generated
	 */
	EClass getMRulesElement();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesElement#getKindLabel <em>Kind Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind Label</em>'.
	 * @see com.montages.mrules.MRulesElement#getKindLabel()
	 * @see #getMRulesElement()
	 * @generated
	 */
	EAttribute getMRulesElement_KindLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesElement#getRenderedKindLabel <em>Rendered Kind Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rendered Kind Label</em>'.
	 * @see com.montages.mrules.MRulesElement#getRenderedKindLabel()
	 * @see #getMRulesElement()
	 * @generated
	 */
	EAttribute getMRulesElement_RenderedKindLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesElement#getIndentLevel <em>Indent Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Indent Level</em>'.
	 * @see com.montages.mrules.MRulesElement#getIndentLevel()
	 * @see #getMRulesElement()
	 * @generated
	 */
	EAttribute getMRulesElement_IndentLevel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.montages.mrules.MRulesElement#getDescription()
	 * @see #getMRulesElement()
	 * @generated
	 */
	EAttribute getMRulesElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesElement#getAsText <em>As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Text</em>'.
	 * @see com.montages.mrules.MRulesElement#getAsText()
	 * @see #getMRulesElement()
	 * @generated
	 */
	EAttribute getMRulesElement_AsText();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesElement#indentationSpaces() <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see com.montages.mrules.MRulesElement#indentationSpaces()
	 * @generated
	 */
	EOperation getMRulesElement__IndentationSpaces();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesElement#indentationSpaces(java.lang.Integer) <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see com.montages.mrules.MRulesElement#indentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getMRulesElement__IndentationSpaces__Integer();

	/**
	 * Returns the meta object for class '{@link com.montages.mrules.MRulesNamed <em>MRules Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MRules Named</em>'.
	 * @see com.montages.mrules.MRulesNamed
	 * @generated
	 */
	EClass getMRulesNamed();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getSpecialEName <em>Special EName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special EName</em>'.
	 * @see com.montages.mrules.MRulesNamed#getSpecialEName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_SpecialEName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getShortName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getEName <em>EName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EName</em>'.
	 * @see com.montages.mrules.MRulesNamed#getEName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_EName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getFullLabel <em>Full Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Full Label</em>'.
	 * @see com.montages.mrules.MRulesNamed#getFullLabel()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_FullLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getLocalStructuralName <em>Local Structural Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Structural Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getLocalStructuralName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_LocalStructuralName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getCalculatedName <em>Calculated Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getCalculatedName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_CalculatedName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getCalculatedShortName <em>Calculated Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Short Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getCalculatedShortName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_CalculatedShortName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getCorrectName <em>Correct Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getCorrectName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_CorrectName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mrules.MRulesNamed#getCorrectShortName <em>Correct Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct Short Name</em>'.
	 * @see com.montages.mrules.MRulesNamed#getCorrectShortName()
	 * @see #getMRulesNamed()
	 * @generated
	 */
	EAttribute getMRulesNamed_CorrectShortName();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesNamed#sameName(com.montages.mrules.MRulesNamed) <em>Same Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same Name</em>' operation.
	 * @see com.montages.mrules.MRulesNamed#sameName(com.montages.mrules.MRulesNamed)
	 * @generated
	 */
	EOperation getMRulesNamed__SameName__MRulesNamed();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesNamed#sameShortName(com.montages.mrules.MRulesNamed) <em>Same Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same Short Name</em>' operation.
	 * @see com.montages.mrules.MRulesNamed#sameShortName(com.montages.mrules.MRulesNamed)
	 * @generated
	 */
	EOperation getMRulesNamed__SameShortName__MRulesNamed();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesNamed#sameString(java.lang.String, java.lang.String) <em>Same String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same String</em>' operation.
	 * @see com.montages.mrules.MRulesNamed#sameString(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getMRulesNamed__SameString__String_String();

	/**
	 * Returns the meta object for the '{@link com.montages.mrules.MRulesNamed#stringEmpty(java.lang.String) <em>String Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Empty</em>' operation.
	 * @see com.montages.mrules.MRulesNamed#stringEmpty(java.lang.String)
	 * @generated
	 */
	EOperation getMRulesNamed__StringEmpty__String();

	/**
	 * Returns the meta object for enum '{@link com.montages.mrules.MRulesAnnotationAction <em>MRules Annotation Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MRules Annotation Action</em>'.
	 * @see com.montages.mrules.MRulesAnnotationAction
	 * @generated
	 */
	EEnum getMRulesAnnotationAction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MrulesFactory getMrulesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mrules.impl.MTestRuleImpl <em>MTest Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mrules.impl.MTestRuleImpl
		 * @see com.montages.mrules.impl.MrulesPackageImpl#getMTestRule()
		 * @generated
		 */
		EClass MTEST_RULE = eINSTANCE.getMTestRule();

		/**
		 * The meta object literal for the '{@link com.montages.mrules.impl.MRuleAnnotationImpl <em>MRule Annotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mrules.impl.MRuleAnnotationImpl
		 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRuleAnnotation()
		 * @generated
		 */
		EClass MRULE_ANNOTATION = eINSTANCE.getMRuleAnnotation();

		/**
		 * The meta object literal for the '<em><b>Named Expression</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRULE_ANNOTATION__NAMED_EXPRESSION = eINSTANCE.getMRuleAnnotation_NamedExpression();

		/**
		 * The meta object literal for the '<em><b>Named Tuple</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRULE_ANNOTATION__NAMED_TUPLE = eINSTANCE.getMRuleAnnotation_NamedTuple();

		/**
		 * The meta object literal for the '<em><b>Named Constant</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRULE_ANNOTATION__NAMED_CONSTANT = eINSTANCE.getMRuleAnnotation_NamedConstant();

		/**
		 * The meta object literal for the '<em><b>AExpected Return Type Of Annotation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION = eINSTANCE
				.getMRuleAnnotation_AExpectedReturnTypeOfAnnotation();

		/**
		 * The meta object literal for the '<em><b>AExpected Return Simple Type Of Annotation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION = eINSTANCE
				.getMRuleAnnotation_AExpectedReturnSimpleTypeOfAnnotation();

		/**
		 * The meta object literal for the '<em><b>Is Return Value Of Annotation Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY = eINSTANCE
				.getMRuleAnnotation_IsReturnValueOfAnnotationMandatory();

		/**
		 * The meta object literal for the '<em><b>Is Return Value Of Annotation Singular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR = eINSTANCE
				.getMRuleAnnotation_IsReturnValueOfAnnotationSingular();

		/**
		 * The meta object literal for the '<em><b>Use Explicit Ocl</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__USE_EXPLICIT_OCL = eINSTANCE.getMRuleAnnotation_UseExplicitOcl();

		/**
		 * The meta object literal for the '<em><b>Ocl Changed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__OCL_CHANGED = eINSTANCE.getMRuleAnnotation_OclChanged();

		/**
		 * The meta object literal for the '<em><b>Ocl Code</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__OCL_CODE = eINSTANCE.getMRuleAnnotation_OclCode();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULE_ANNOTATION__DO_ACTION = eINSTANCE.getMRuleAnnotation_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION = eINSTANCE
				.getMRuleAnnotation__DoActionUpdate__MRulesAnnotationAction();

		/**
		 * The meta object literal for the '<em><b>Variable From Expression</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION = eINSTANCE
				.getMRuleAnnotation__VariableFromExpression__MNamedExpression();

		/**
		 * The meta object literal for the '{@link com.montages.mrules.impl.MRulesElementImpl <em>MRules Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mrules.impl.MRulesElementImpl
		 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesElement()
		 * @generated
		 */
		EClass MRULES_ELEMENT = eINSTANCE.getMRulesElement();

		/**
		 * The meta object literal for the '<em><b>Kind Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_ELEMENT__KIND_LABEL = eINSTANCE.getMRulesElement_KindLabel();

		/**
		 * The meta object literal for the '<em><b>Rendered Kind Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_ELEMENT__RENDERED_KIND_LABEL = eINSTANCE.getMRulesElement_RenderedKindLabel();

		/**
		 * The meta object literal for the '<em><b>Indent Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_ELEMENT__INDENT_LEVEL = eINSTANCE.getMRulesElement_IndentLevel();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_ELEMENT__DESCRIPTION = eINSTANCE.getMRulesElement_Description();

		/**
		 * The meta object literal for the '<em><b>As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_ELEMENT__AS_TEXT = eINSTANCE.getMRulesElement_AsText();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_ELEMENT___INDENTATION_SPACES = eINSTANCE.getMRulesElement__IndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_ELEMENT___INDENTATION_SPACES__INTEGER = eINSTANCE
				.getMRulesElement__IndentationSpaces__Integer();

		/**
		 * The meta object literal for the '{@link com.montages.mrules.impl.MRulesNamedImpl <em>MRules Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mrules.impl.MRulesNamedImpl
		 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesNamed()
		 * @generated
		 */
		EClass MRULES_NAMED = eINSTANCE.getMRulesNamed();

		/**
		 * The meta object literal for the '<em><b>Special EName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__SPECIAL_ENAME = eINSTANCE.getMRulesNamed_SpecialEName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__NAME = eINSTANCE.getMRulesNamed_Name();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__SHORT_NAME = eINSTANCE.getMRulesNamed_ShortName();

		/**
		 * The meta object literal for the '<em><b>EName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__ENAME = eINSTANCE.getMRulesNamed_EName();

		/**
		 * The meta object literal for the '<em><b>Full Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__FULL_LABEL = eINSTANCE.getMRulesNamed_FullLabel();

		/**
		 * The meta object literal for the '<em><b>Local Structural Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__LOCAL_STRUCTURAL_NAME = eINSTANCE.getMRulesNamed_LocalStructuralName();

		/**
		 * The meta object literal for the '<em><b>Calculated Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__CALCULATED_NAME = eINSTANCE.getMRulesNamed_CalculatedName();

		/**
		 * The meta object literal for the '<em><b>Calculated Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__CALCULATED_SHORT_NAME = eINSTANCE.getMRulesNamed_CalculatedShortName();

		/**
		 * The meta object literal for the '<em><b>Correct Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__CORRECT_NAME = eINSTANCE.getMRulesNamed_CorrectName();

		/**
		 * The meta object literal for the '<em><b>Correct Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRULES_NAMED__CORRECT_SHORT_NAME = eINSTANCE.getMRulesNamed_CorrectShortName();

		/**
		 * The meta object literal for the '<em><b>Same Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_NAMED___SAME_NAME__MRULESNAMED = eINSTANCE.getMRulesNamed__SameName__MRulesNamed();

		/**
		 * The meta object literal for the '<em><b>Same Short Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_NAMED___SAME_SHORT_NAME__MRULESNAMED = eINSTANCE.getMRulesNamed__SameShortName__MRulesNamed();

		/**
		 * The meta object literal for the '<em><b>Same String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_NAMED___SAME_STRING__STRING_STRING = eINSTANCE.getMRulesNamed__SameString__String_String();

		/**
		 * The meta object literal for the '<em><b>String Empty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRULES_NAMED___STRING_EMPTY__STRING = eINSTANCE.getMRulesNamed__StringEmpty__String();

		/**
		 * The meta object literal for the '{@link com.montages.mrules.MRulesAnnotationAction <em>MRules Annotation Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mrules.MRulesAnnotationAction
		 * @see com.montages.mrules.impl.MrulesPackageImpl#getMRulesAnnotationAction()
		 * @generated
		 */
		EEnum MRULES_ANNOTATION_ACTION = eINSTANCE.getMRulesAnnotationAction();

	}

} //MrulesPackage
