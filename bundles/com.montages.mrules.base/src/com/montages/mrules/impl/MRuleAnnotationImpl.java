/**
 */

package com.montages.mrules.impl;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MRulesAnnotationAction;
import com.montages.mrules.MrulesPackage;
import com.montages.mrules.expressions.MAbstractNamedTuple;
import com.montages.mrules.expressions.MNamedConstant;
import com.montages.mrules.expressions.MNamedExpression;
import com.montages.mrules.expressions.MVariableBaseDefinition;

import com.montages.mrules.expressions.impl.MBaseChainImpl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MRule Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getAAnnotated <em>AAnnotated</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getAAnnotating <em>AAnnotating</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getNamedExpression <em>Named Expression</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getNamedTuple <em>Named Tuple</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getNamedConstant <em>Named Constant</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getAExpectedReturnTypeOfAnnotation <em>AExpected Return Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getAExpectedReturnSimpleTypeOfAnnotation <em>AExpected Return Simple Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getUseExplicitOcl <em>Use Explicit Ocl</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getOclChanged <em>Ocl Changed</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getOclCode <em>Ocl Code</em>}</li>
 *   <li>{@link com.montages.mrules.impl.MRuleAnnotationImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MRuleAnnotationImpl extends MBaseChainImpl implements MRuleAnnotation {
	/**
	 * The cached value of the '{@link #getNamedExpression() <em>Named Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedExpression()
	 * @generated
	 * @ordered
	 */
	protected EList<MNamedExpression> namedExpression;

	/**
	 * The cached value of the '{@link #getNamedTuple() <em>Named Tuple</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedTuple()
	 * @generated
	 * @ordered
	 */
	protected EList<MAbstractNamedTuple> namedTuple;

	/**
	 * The cached value of the '{@link #getNamedConstant() <em>Named Constant</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedConstant()
	 * @generated
	 * @ordered
	 */
	protected EList<MNamedConstant> namedConstant;

	/**
	 * The default value of the '{@link #getAExpectedReturnSimpleTypeOfAnnotation() <em>AExpected Return Simple Type Of Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleTypeOfAnnotation()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT = ASimpleType.NONE;

	/**
	 * The default value of the '{@link #getIsReturnValueOfAnnotationMandatory() <em>Is Return Value Of Annotation Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReturnValueOfAnnotationSingular() <em>Is Return Value Of Annotation Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getUseExplicitOcl() <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseExplicitOcl()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_EXPLICIT_OCL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUseExplicitOcl() <em>Use Explicit Ocl</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseExplicitOcl()
	 * @generated
	 * @ordered
	 */
	protected Boolean useExplicitOcl = USE_EXPLICIT_OCL_EDEFAULT;

	/**
	 * This is true if the Use Explicit Ocl attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean useExplicitOclESet;

	/**
	 * The default value of the '{@link #getOclChanged() <em>Ocl Changed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclChanged()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OCL_CHANGED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOclCode() <em>Ocl Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclCode()
	 * @generated
	 * @ordered
	 */
	protected static final String OCL_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MRulesAnnotationAction DO_ACTION_EDEFAULT = MRulesAnnotationAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemrulesMRulesAnnotationActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #variableFromExpression <em>Variable From Expression</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #variableFromExpression
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression variableFromExpressionexpressionsMNamedExpressionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotated <em>AAnnotated</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotated
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotatedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotating <em>AAnnotating</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotating
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotatingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnTypeOfAnnotation <em>AExpected Return Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aExpectedReturnTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAExpectedReturnSimpleTypeOfAnnotation <em>AExpected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aExpectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationMandatory <em>Is Return Value Of Annotation Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueOfAnnotationSingular <em>Is Return Value Of Annotation Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueOfAnnotationSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueOfAnnotationSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOclChanged <em>Ocl Changed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclChanged
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression oclChangedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOclCode <em>Ocl Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOclCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression oclCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MRuleAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MrulesPackage.Literals.MRULE_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotatable getAAnnotated() {
		AAnnotatable aAnnotated = basicGetAAnnotated();
		return aAnnotated != null && aAnnotated.eIsProxy() ? (AAnnotatable) eResolveProxy((InternalEObject) aAnnotated)
				: aAnnotated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotatable basicGetAAnnotated() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATED;

		if (aAnnotatedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotatedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAnnotatable result = (AAnnotatable) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotation getAAnnotating() {
		AAnnotation aAnnotating = basicGetAAnnotating();
		return aAnnotating != null && aAnnotating.eIsProxy()
				? (AAnnotation) eResolveProxy((InternalEObject) aAnnotating) : aAnnotating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotation basicGetAAnnotating() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATING;

		if (aAnnotatingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotatingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAnnotation result = (AAnnotation) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNamedExpression> getNamedExpression() {
		if (namedExpression == null) {
			namedExpression = new EObjectContainmentEList.Unsettable.Resolving<MNamedExpression>(MNamedExpression.class,
					this, MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION);
		}
		return namedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedExpression() {
		if (namedExpression != null)
			((InternalEList.Unsettable<?>) namedExpression).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedExpression() {
		return namedExpression != null && ((InternalEList.Unsettable<?>) namedExpression).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractNamedTuple> getNamedTuple() {
		if (namedTuple == null) {
			namedTuple = new EObjectContainmentEList.Unsettable.Resolving<MAbstractNamedTuple>(
					MAbstractNamedTuple.class, this, MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE);
		}
		return namedTuple;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedTuple() {
		if (namedTuple != null)
			((InternalEList.Unsettable<?>) namedTuple).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedTuple() {
		return namedTuple != null && ((InternalEList.Unsettable<?>) namedTuple).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNamedConstant> getNamedConstant() {
		if (namedConstant == null) {
			namedConstant = new EObjectContainmentEList.Unsettable.Resolving<MNamedConstant>(MNamedConstant.class, this,
					MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT);
		}
		return namedConstant;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedConstant() {
		if (namedConstant != null)
			((InternalEList.Unsettable<?>) namedConstant).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedConstant() {
		return namedConstant != null && ((InternalEList.Unsettable<?>) namedConstant).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAExpectedReturnTypeOfAnnotation() {
		AClassifier aExpectedReturnTypeOfAnnotation = basicGetAExpectedReturnTypeOfAnnotation();
		return aExpectedReturnTypeOfAnnotation != null && aExpectedReturnTypeOfAnnotation.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aExpectedReturnTypeOfAnnotation)
				: aExpectedReturnTypeOfAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAExpectedReturnTypeOfAnnotation() {
		/**
		 * @OCL null
		/*let apa:annotations::MAbstractPropertyAnnotations = eContainer().oclAsType(MAbstractPropertyAnnotations)
		in if apa.annotatedProperty.oclIsUndefined()
		then null 
		else apa.annotatedProperty.calculatedType
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION;

		if (aExpectedReturnTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aExpectedReturnTypeOfAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getAExpectedReturnSimpleTypeOfAnnotation() {
		/**
		 * @OCL acore::classifiers::ASimpleType::None
		/*
		if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.simpleType
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.simpleType
		else acore::classifiers::ASimpleType::None 
		endif endif endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (aExpectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aExpectedReturnSimpleTypeOfAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aExpectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ASimpleType result = (ASimpleType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueOfAnnotationMandatory() {
		/**
		 * @OCL null
		/*if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.mandatory
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.mandatory
		else true
		endif endif endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY;

		if (isReturnValueOfAnnotationMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isReturnValueOfAnnotationMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueOfAnnotationMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueOfAnnotationSingular() {
		/**
		 * @OCL null
		/* if self.eContainer().oclAsType(annotations::MAbstractPropertyAnnotations).annotatedProperty.oclIsUndefined()  then null else
		
		
		
		if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().oclAsType(annotations::MPropertyAnnotations).annotatedProperty.singular
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().oclAsType(annotations::MOperationAnnotations).annotatedProperty.singular
		else true
		endif endif endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR;

		if (isReturnValueOfAnnotationSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				isReturnValueOfAnnotationSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueOfAnnotationSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUseExplicitOcl() {
		return useExplicitOcl;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseExplicitOcl(Boolean newUseExplicitOcl) {
		Boolean oldUseExplicitOcl = useExplicitOcl;
		useExplicitOcl = newUseExplicitOcl;
		boolean oldUseExplicitOclESet = useExplicitOclESet;
		useExplicitOclESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL,
					oldUseExplicitOcl, useExplicitOcl, !oldUseExplicitOclESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUseExplicitOcl() {
		Boolean oldUseExplicitOcl = useExplicitOcl;
		boolean oldUseExplicitOclESet = useExplicitOclESet;
		useExplicitOcl = USE_EXPLICIT_OCL_EDEFAULT;
		useExplicitOclESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL,
					oldUseExplicitOcl, USE_EXPLICIT_OCL_EDEFAULT, oldUseExplicitOclESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUseExplicitOcl() {
		return useExplicitOclESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getOclChanged() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__OCL_CHANGED;

		if (oclChangedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				oclChangedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(oclChangedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOclCode() {
		/**
		 * @OCL let attentionImplementedinJava: String = 'TODO Replace Java Code' in
		attentionImplementedinJava
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__OCL_CODE;

		if (oclCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				oclCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(oclCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRulesAnnotationAction getDoAction() {
		/**
		 * @OCL mrules::MRulesAnnotationAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MrulesPackage.Literals.MRULE_ANNOTATION;
		EStructuralFeature eFeature = MrulesPackage.Literals.MRULE_ANNOTATION__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, derive, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MRulesAnnotationAction result = (MRulesAnnotationAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String doActionUpdate(MRulesAnnotationAction trg) {

		/**
		 * @OCL null
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULE_ANNOTATION);
		EOperation eOperation = MrulesPackage.Literals.MRULE_ANNOTATION.getEOperations().get(0);
		if (doActionUpdatemrulesMRulesAnnotationActionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				doActionUpdatemrulesMRulesAnnotationActionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, body, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(doActionUpdatemrulesMRulesAnnotationActionBodyOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("trg", trg);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariableBaseDefinition variableFromExpression(MNamedExpression namedExpression) {

		/**
		 * @OCL if self.localScopeVariables->isEmpty() then null
		else
		self.localScopeVariables->select(x:mrules::expressions::MVariableBaseDefinition| if x.namedExpression<> null  then x.namedExpression = namedExpression else false endif )->first() endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (MrulesPackage.Literals.MRULE_ANNOTATION);
		EOperation eOperation = MrulesPackage.Literals.MRULE_ANNOTATION.getEOperations().get(1);
		if (variableFromExpressionexpressionsMNamedExpressionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				variableFromExpressionexpressionsMNamedExpressionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MrulesPackage.PLUGIN_ID, body, helper.getProblems(),
						MrulesPackage.Literals.MRULE_ANNOTATION, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(variableFromExpressionexpressionsMNamedExpressionBodyOCL);
		try {
			XoclErrorHandler.enterContext(MrulesPackage.PLUGIN_ID, query, MrulesPackage.Literals.MRULE_ANNOTATION,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("namedExpression", namedExpression);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MVariableBaseDefinition) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
			return ((InternalEList<?>) getNamedExpression()).basicRemove(otherEnd, msgs);
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
			return ((InternalEList<?>) getNamedTuple()).basicRemove(otherEnd, msgs);
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			return ((InternalEList<?>) getNamedConstant()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MrulesPackage.MRULE_ANNOTATION__AANNOTATED:
			if (resolve)
				return getAAnnotated();
			return basicGetAAnnotated();
		case MrulesPackage.MRULE_ANNOTATION__AANNOTATING:
			if (resolve)
				return getAAnnotating();
			return basicGetAAnnotating();
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
			return getNamedExpression();
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
			return getNamedTuple();
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			return getNamedConstant();
		case MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION:
			if (resolve)
				return getAExpectedReturnTypeOfAnnotation();
			return basicGetAExpectedReturnTypeOfAnnotation();
		case MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
			return getAExpectedReturnSimpleTypeOfAnnotation();
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
			return getIsReturnValueOfAnnotationMandatory();
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
			return getIsReturnValueOfAnnotationSingular();
		case MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL:
			return getUseExplicitOcl();
		case MrulesPackage.MRULE_ANNOTATION__OCL_CHANGED:
			return getOclChanged();
		case MrulesPackage.MRULE_ANNOTATION__OCL_CODE:
			return getOclCode();
		case MrulesPackage.MRULE_ANNOTATION__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
			getNamedExpression().clear();
			getNamedExpression().addAll((Collection<? extends MNamedExpression>) newValue);
			return;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
			getNamedTuple().clear();
			getNamedTuple().addAll((Collection<? extends MAbstractNamedTuple>) newValue);
			return;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			getNamedConstant().clear();
			getNamedConstant().addAll((Collection<? extends MNamedConstant>) newValue);
			return;
		case MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL:
			setUseExplicitOcl((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
			unsetNamedExpression();
			return;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
			unsetNamedTuple();
			return;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			unsetNamedConstant();
			return;
		case MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL:
			unsetUseExplicitOcl();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MrulesPackage.MRULE_ANNOTATION__AANNOTATED:
			return basicGetAAnnotated() != null;
		case MrulesPackage.MRULE_ANNOTATION__AANNOTATING:
			return basicGetAAnnotating() != null;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
			return isSetNamedExpression();
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
			return isSetNamedTuple();
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			return isSetNamedConstant();
		case MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION:
			return basicGetAExpectedReturnTypeOfAnnotation() != null;
		case MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
			return getAExpectedReturnSimpleTypeOfAnnotation() != AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT;
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
			return IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT == null
					? getIsReturnValueOfAnnotationMandatory() != null
					: !IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY_EDEFAULT.equals(getIsReturnValueOfAnnotationMandatory());
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
			return IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT == null
					? getIsReturnValueOfAnnotationSingular() != null
					: !IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR_EDEFAULT.equals(getIsReturnValueOfAnnotationSingular());
		case MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL:
			return isSetUseExplicitOcl();
		case MrulesPackage.MRULE_ANNOTATION__OCL_CHANGED:
			return OCL_CHANGED_EDEFAULT == null ? getOclChanged() != null
					: !OCL_CHANGED_EDEFAULT.equals(getOclChanged());
		case MrulesPackage.MRULE_ANNOTATION__OCL_CODE:
			return OCL_CODE_EDEFAULT == null ? getOclCode() != null : !OCL_CODE_EDEFAULT.equals(getOclCode());
		case MrulesPackage.MRULE_ANNOTATION__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotation.class) {
			switch (derivedFeatureID) {
			case MrulesPackage.MRULE_ANNOTATION__AANNOTATED:
				return AbstractionsPackage.AANNOTATION__AANNOTATED;
			case MrulesPackage.MRULE_ANNOTATION__AANNOTATING:
				return AbstractionsPackage.AANNOTATION__AANNOTATING;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AAnnotation.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AANNOTATION__AANNOTATED:
				return MrulesPackage.MRULE_ANNOTATION__AANNOTATED;
			case AbstractionsPackage.AANNOTATION__AANNOTATING:
				return MrulesPackage.MRULE_ANNOTATION__AANNOTATING;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case MrulesPackage.MRULE_ANNOTATION___DO_ACTION_UPDATE__MRULESANNOTATIONACTION:
			return doActionUpdate((MRulesAnnotationAction) arguments.get(0));
		case MrulesPackage.MRULE_ANNOTATION___VARIABLE_FROM_EXPRESSION__MNAMEDEXPRESSION:
			return variableFromExpression((MNamedExpression) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (useExplicitOcl: ");
		if (useExplicitOclESet)
			result.append(useExplicitOcl);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS14
	 * @generated
	 */

	@Override

	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] baseInititalizedFeatured = super.getInitializedStructuralFeatures();
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				MrulesPackage.Literals.MRULE_ANNOTATION__USE_EXPLICIT_OCL };
		EStructuralFeature[] allInitializedFeatures = new EStructuralFeature[baseInititalizedFeatured.length
				+ initializedFeatures.length];
		System.arraycopy(baseInititalizedFeatured, 0, allInitializedFeatures, 0, baseInititalizedFeatured.length);
		System.arraycopy(initializedFeatures, 0, allInitializedFeatures, baseInititalizedFeatured.length,
				initializedFeatures.length);
		return allInitializedFeatures;
	}

} //MRuleAnnotationImpl
