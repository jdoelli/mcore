/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.core;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.Status;
import org.osgi.framework.BundleContext;
import org.xocl.core.internal.CommonPackageResourceProvider;
import org.xocl.core.packages.IPackageResourceProvider;

/**
 * @author Max Stepanov
 *
 */
public class XOCLCorePlugin extends Plugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.xocl.core"; //$NON-NLS-1$

	// The shared instance
	private static XOCLCorePlugin plugin;
	
	private IPackageResourceProvider defaultPackageResourceProvider;

	/**
	 * 
	 */
	public XOCLCorePlugin() {
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		plugin = this;
		super.start(context);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static XOCLCorePlugin getDefault() {
		return plugin;
	}
	
	public IPackageResourceProvider getPackageResourceProvider() {
		if (defaultPackageResourceProvider == null) {
			defaultPackageResourceProvider = new CommonPackageResourceProvider();
		}
		return defaultPackageResourceProvider;
	}

	public static void log(Throwable e) {
		log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, e.getLocalizedMessage(), e));
	}

	public static void log(String msg) {
		log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK, msg, null)); 
	}

	public static void log(IStatus status) {
		getDefault().getLog().log(status);
	}

}
