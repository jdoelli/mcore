package org.xocl.core.edit.provider;

import java.util.Date;

import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;

public interface DataTimeCellEditorProvider {

	CellEditor getDateTimeCellEditor(Composite composite, Date date);
}
