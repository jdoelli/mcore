/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.core.expr;

import java.util.ArrayList;
import java.util.ListIterator;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ecore.CollectionType;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.types.OCLStandardLibrary;
import org.eclipse.osgi.util.NLS;
import org.xocl.core.XOCLPlugin;
import org.xocl.core.util.XoclLibrary;

/**
 * @author Max Stepanov
 *
 */
public final class XOCLElementContext {

	private static final String OCL_ANNOTATION_SOURCE = ".*?/OCL.*"; //$NON-NLS-1$
	private static final String NAMED_OCL_ANNOTATION_SOURCE = ".*?/NAMED_OCL"; //$NON-NLS-1$
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = ".*?/OVERRIDE_OCL"; //$NON-NLS-1$

	private static OCLStandardLibrary<EClassifier> oclStandardLibrary;
	
	private EClassifier eClassifier;
	private Variable[] variables;
	private EClassifier[] resultTypes;
	
	private XOCLElementContext(EClassifier eModelElement, Variable[] variables, EClassifier[] resultTypes) {
		this.eClassifier = eModelElement;
		this.variables = variables;
		this.resultTypes = resultTypes;
	}

	/**
	 * @return the eClassifier
	 */
	public EClassifier getContextClassifier() {
		return eClassifier;
	}

	/**
	 * @return the variables
	 */
	public Variable[] getVariables() {
		return variables;
	}

	/**
	 * @return the resultType
	 */
	public EClassifier[] getResultTypes() {
		return resultTypes;
	}
	
	public static XOCLElementContext getElementContext(EAnnotation eAnnotation, String detailKey) throws DiagnosticException {
		if (detailKey == null) {
			return null;
		}

		EModelElement eModelElement = eAnnotation.getEModelElement();
		EModelElement exactModelElemet = eModelElement;
		String annotationSource = eAnnotation.getSource();
		Variable[] variables = null;
		EClassifier[] resultTypes = new EClassifier[]{};

		if (checkXOCLAnnotationSource(eAnnotation)) {
			if ("derive".equals(detailKey) || "choiceConstraint".equals(detailKey) || "choiceConstruction".equals(detailKey) //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
					|| "initValue".equals(detailKey) || "initOrder".equals(detailKey) || "color".equals(detailKey)) { //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
				if (eModelElement instanceof EStructuralFeature) {
					if ("choiceConstraint".equals(detailKey) || "initValue".equals(detailKey)) { //$NON-NLS-1$ //$NON-NLS-2$
						Variable v = EcoreFactory.eINSTANCE.createVariable();
						v.setName("trg"); //$NON-NLS-1$
						v.setType(((EStructuralFeature)eModelElement).getEType());
						variables = new Variable[] { v };
					} else if ("choiceConstruction".equals(detailKey)) { //$NON-NLS-1$
						Variable v = EcoreFactory.eINSTANCE.createVariable();
						v.setName("choice"); //$NON-NLS-1$
						v.setType(getOCLStandardLibrary().getSequence());
						variables = new Variable[] { v };							
					}
					if ("derive".equals(detailKey) || "choiceConstruction".equals(detailKey) //$NON-NLS-1$ //$NON-NLS-2$
							|| "initValue".equals(detailKey)) { //$NON-NLS-1$
						EClassifier eType = ((EStructuralFeature)eModelElement).getEType();
						ArrayList<EClassifier> types = new ArrayList<EClassifier>();
						types.add(eType);
						if (((EStructuralFeature)eModelElement).isMany() || ("choiceConstruction".equals(detailKey))) { //$NON-NLS-1$
							CollectionType collectionType = OCLExpressionUtils.getTypedElementCollectionType((EStructuralFeature) eModelElement);
							collectionType.setElementType(eType);
							types.add(collectionType);
							if (!(((EStructuralFeature)eModelElement).isRequired())) {
								collectionType = OCLExpressionUtils.getTypedElementCollectionType((EStructuralFeature) eModelElement);
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								collectionType.setElementType(oclVoidType);
								types.add(collectionType);								
							}								
						}
						if (!(((EStructuralFeature)eModelElement).isRequired())) {
							// Only for references, because returning null for
							// attributes isn't semantically clear
							EStructuralFeature f = (EStructuralFeature)eModelElement;
							EList<EAttribute> eAllAttributes = f.getEContainingClass().getEAllAttributes();
							if (!(eAllAttributes.contains(f))) {
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								types.add(oclVoidType);
							}
						}
						resultTypes = types.toArray(resultTypes);
					} else if ("choiceConstraint".equals(detailKey) || "color".equals(detailKey)) { //$NON-NLS-1$ //$NON-NLS-2$
						resultTypes = new EClassifier[] { getOCLStandardLibrary().getBoolean() };
					} else if ("initOrder".equals(detailKey)) { //$NON-NLS-1$
						resultTypes = getInitOrderResultType();
					}
					eModelElement = ((EStructuralFeature)eModelElement).getEContainingClass();						
				}
			} else if ("body".equals(detailKey)) { //$NON-NLS-1$
				if (eModelElement instanceof EOperation) {
					EList<EParameter> parameters = ((EOperation)eModelElement).getEParameters();
					if (parameters != null && !parameters.isEmpty()) {
						variables = new Variable[parameters.size()];
						for (int i = 0; i < variables.length; ++i) {
							EParameter param = parameters.get(i);
							Variable v = EcoreFactory.eINSTANCE.createVariable();
							v.setName(param.getName());
							v.setType(param.getEType());								
							variables[i] = v;
						}
					}
					EClassifier eType = ((EOperation)eModelElement).getEType();
					ArrayList<EClassifier> types = new ArrayList<EClassifier>();
					types.add(eType);
					if (((EOperation)eModelElement).isMany()) {
						CollectionType collectionType = OCLExpressionUtils.getTypedElementCollectionType((EOperation) eModelElement);
						collectionType.setElementType(eType);
						types.add(collectionType);
						if (!(((EOperation)eModelElement).isRequired())) {
							collectionType = OCLExpressionUtils.getTypedElementCollectionType((EOperation) eModelElement);
							EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
							collectionType.setElementType(oclVoidType);
							types.add(collectionType);
						}
					}
					if (!(((EOperation)eModelElement).isRequired())) {
						// Only for references, because returning null for
						// attributes isn't semantically clear
						EOperation op = (EOperation)eModelElement;
						EClassifier t = op.getEType();
						if (t instanceof EClass) {
							EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
							types.add(oclVoidType);							
						}
					}
					resultTypes = types.toArray(resultTypes);
					eModelElement = ((EOperation)eModelElement).getEContainingClass();
				}
			} else if ("label".equals(detailKey)) { //$NON-NLS-1$
				CollectionType collectionType = EcoreFactory.eINSTANCE.createCollectionType();
				collectionType.setElementType(getOCLStandardLibrary().getString());
				resultTypes = new EClassifier[] { getOCLStandardLibrary().getString(), collectionType };
			} else if ("rootConstraint".equals(detailKey)){ //$NON-NLS-1$
				if (eModelElement instanceof EPackage) {
					Variable v = EcoreFactory.eINSTANCE.createVariable();
					v.setName("trg"); //$NON-NLS-1$
					v.setType(EcorePackage.Literals.ECLASS);
					variables = new Variable[] {v};
					resultTypes = new EClassifier[] { getOCLStandardLibrary().getBoolean() };
					eModelElement = EcorePackage.Literals.EPACKAGE;
				}
			} else if (detailKey.endsWith("Update")) { //$NON-NLS-1$
				if (eModelElement instanceof EStructuralFeature) {
					Variable v = EcoreFactory.eINSTANCE.createVariable();
					v.setName("trg"); //$NON-NLS-1$
					v.setType(((EStructuralFeature)eModelElement).getEType());
					variables = new Variable[] { v };

					String updatedFeatureName = detailKey.substring(0, detailKey.length() - "Update".length()); //$NON-NLS-1$
					EObject eContainer = eModelElement.eContainer();
					if (eContainer instanceof EClass) {
						EClass eClass = (EClass) eContainer;
						EStructuralFeature updatedFeature = eClass.getEStructuralFeature(updatedFeatureName);
						if (updatedFeature == null) {
							throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
									NLS.bind("Feature {0} does not exist in class {1}", updatedFeatureName, eClass.getName()),
									null));
						}
						resultTypes = new EClassifier[] { updatedFeature.getEType() };
					} else {
						throw new RuntimeException(NLS.bind("Feature {0} is not contained in a class!", updatedFeatureName));
					}
				}
			} else {
				throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
					NLS.bind(Messages.XOCLElementContext_UnknownAnnotationError, detailKey, EcoreUtil.getURI(eModelElement).fragment()),
					null));
			}
		} else if (annotationSource != null &&  annotationSource.matches(OVERRIDE_OCL_ANNOTATION_SOURCE)) {
			if (detailKey.endsWith("Derive") || //$NON-NLS-1$ 
					detailKey.endsWith("ChoiceConstraint") || //$NON-NLS-1$
					detailKey.endsWith("ChoiceConstruction") || //$NON-NLS-1$
					detailKey.endsWith("InitValue") || //$NON-NLS-1$
					detailKey.endsWith("InitOrder")) { //$NON-NLS-1$

				if (eModelElement instanceof EClass) {
					if (detailKey.endsWith("ChoiceConstraint")) { //$NON-NLS-1$
						String featureName = detailKey.substring(0, detailKey.length() - 16); 
						Variable v = EcoreFactory.eINSTANCE.createVariable();
						v.setName("trg"); //$NON-NLS-1$
						v.setType(((EClass)eModelElement).getEStructuralFeature(featureName).getEType());
						variables = new Variable[] { v };
						resultTypes = new EClassifier[] { getOCLStandardLibrary().getBoolean() };
					} else if (detailKey.endsWith("ChoiceConstruction")) { //$NON-NLS-1$
						String featureName = detailKey.substring(0, detailKey.length() - 18); 
						Variable v = EcoreFactory.eINSTANCE.createVariable();
						v.setName("choice"); //$NON-NLS-1$
						v.setType(getOCLStandardLibrary().getSequence());
						variables = new Variable[] { v };
						CollectionType collectionType = EcoreFactory.eINSTANCE.createSequenceType();
						collectionType.setElementType(((EClass)eModelElement).getEStructuralFeature(featureName).getEType());
						resultTypes = new EClassifier[] { collectionType };
					} else if (detailKey.endsWith("Derive")) { //$NON-NLS-1$
						String featureName = detailKey.substring(0, detailKey.length() - 6);
						EClass eClass = (EClass) eModelElement;
						EStructuralFeature eFeature = eClass.getEStructuralFeature(featureName);
						if (eFeature == null) {
							throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
									NLS.bind("Feature {0} does not exist in class {1}", featureName, eClass.getName()),
									null));							
						}
						EClassifier eType = eFeature.getEType();
						ArrayList<EClassifier> types = new ArrayList<EClassifier>();
						types.add(eType);
						if (eFeature.isMany()) {
							CollectionType collectionType = OCLExpressionUtils.getTypedElementCollectionType(eFeature);
							collectionType.setElementType(eType);
							types.add(collectionType);
							if (!(eFeature.isRequired())) {
								collectionType = OCLExpressionUtils.getTypedElementCollectionType(eFeature);
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								collectionType.setElementType(oclVoidType);
								types.add(collectionType);
							}
						}						
						if (!(eFeature.isRequired())) {
							// Only for references, because returning null for
							// attributes isn't semantically clear
							EList<EAttribute> eAllAttributes = eFeature.getEContainingClass().getEAllAttributes();
							if (!(eAllAttributes.contains(eFeature))) {
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								types.add(oclVoidType);
							}
						}
						resultTypes = types.toArray(resultTypes);
					} else if (detailKey.endsWith("InitValue")) { //$NON-NLS-1$
						String featureName = detailKey.substring(0, detailKey.length() - "InitValue".length());  //$NON-NLS-1$
						Variable v = EcoreFactory.eINSTANCE.createVariable();
						v.setName("trg"); //$NON-NLS-1$
						EStructuralFeature eFeature = ((EClass)eModelElement).getEStructuralFeature(featureName);
						v.setType(eFeature.getEType());
						variables = new Variable[] { v };
						EClassifier eType = eFeature.getEType();
						ArrayList<EClassifier> types = new ArrayList<EClassifier>();
						types.add(eType);
						if (eFeature.isMany()) {
							CollectionType collectionType = OCLExpressionUtils.getTypedElementCollectionType(eFeature);
							collectionType.setElementType(eType);
							types.add(collectionType);
							if (!(eFeature.isRequired())) {
								collectionType = OCLExpressionUtils.getTypedElementCollectionType(eFeature);
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								collectionType.setElementType(oclVoidType);
								types.add(collectionType);
							}
						}
						if (!(eFeature.isRequired())) {
							// Only for references, because returning null for
							// attributes isn't semantically clear
							EList<EAttribute> eAllAttributes = eFeature.getEContainingClass().getEAllAttributes();
							if (!(eAllAttributes.contains(eFeature))) {
								EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
								types.add(oclVoidType);
							}
						}
						resultTypes = types.toArray(resultTypes);
					} else if (detailKey.endsWith("InitOrder")) { //$NON-NLS-1$
						resultTypes = getInitOrderResultType();
					}
				}
			} else if (detailKey.endsWith("Body")) { //$NON-NLS-1$
				if (eModelElement instanceof EClass) {
					String operationName = detailKey.substring(0, detailKey.length() - 4);
					for (ListIterator<EOperation> j = ((EClass)eModelElement).getEAllOperations().listIterator(); j.hasNext(); ) {
						EOperation op = j.next();
						if (operationName.equals(op.getName())) {
							EList<EParameter> parameters = op.getEParameters();
							if (parameters != null && !parameters.isEmpty()) {
								variables = new Variable[parameters.size()];
								for (int i = 0; i < variables.length; ++i) {
									EParameter param = parameters.get(i);
									Variable v = EcoreFactory.eINSTANCE.createVariable();
									v.setName(param.getName());
									v.setType(param.getEType());								
									variables[i] = v;
								}
							}
							EClassifier eType = op.getEType();							
							ArrayList<EClassifier> types = new ArrayList<EClassifier>();
							types.add(eType);
							if (op.isMany()) {
								CollectionType collectionType = OCLExpressionUtils.getTypedElementCollectionType(op);
								collectionType.setElementType(eType);
								types.add(collectionType);
								if (!(op.isRequired())) {
									collectionType = OCLExpressionUtils.getTypedElementCollectionType(op);
									EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
									collectionType.setElementType(oclVoidType);
									types.add(collectionType);
								}
							}
							if (!(op.isRequired())) {
								// Only for references, because returning null for
								// attributes isn't semantically clear
								EClassifier t = op.getEType();
								if (t instanceof EClass) {
									EClassifier oclVoidType = getOCLStandardLibrary().getOclVoid();
									types.add(oclVoidType);							
								}
							}
							resultTypes = types.toArray(resultTypes);
							break;
						}
					}
				}
			} else if (detailKey.endsWith("Update")) { //$NON-NLS-1$
				if (eModelElement instanceof EClass) {
					String featureNamesString = detailKey.substring(0, detailKey.length() - "Update".length());
					String[] featureNameArray = featureNamesString.split("-");
					if ((featureNameArray == null) || (featureNameArray.length != 2)) {
						throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
								NLS.bind("Error in override update annotation {0}", detailKey),
								null));
					}

					String triggeringFeatureName = featureNameArray[0];
					String updatedFeatureName = featureNameArray[1];
					
					EClass eClass = (EClass) eModelElement;
					EStructuralFeature triggeringFeature = eClass.getEStructuralFeature(triggeringFeatureName);
					if (triggeringFeature == null) {
						throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
								NLS.bind("Feature {0} does not exist in class {1}", triggeringFeatureName, eClass.getName()),
								null));
					}
					EStructuralFeature updatedFeature = eClass.getEStructuralFeature(updatedFeatureName);
					if (updatedFeature == null) {
						throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
								NLS.bind("Feature {0} does not exist in class {1}", updatedFeatureName, eClass.getName()),
								null));
					}

					Variable v = EcoreFactory.eINSTANCE.createVariable();
					v.setName("trg"); //$NON-NLS-1$
					v.setType(((EStructuralFeature) triggeringFeature).getEType());
					variables = new Variable[] { v };

					resultTypes = new EClassifier[] { updatedFeature.getEType() };
				}
			} else {
				throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
						NLS.bind(Messages.XOCLElementContext_UnknownAnnotationError, detailKey, EcoreUtil.getURI(eModelElement).fragment()),
						null));
			}
		} else if (annotationSource != null && annotationSource.matches(NAMED_OCL_ANNOTATION_SOURCE)) {
			if (eModelElement instanceof EClass) {
				resultTypes = new EClassifier[] { getOCLStandardLibrary().getBoolean() };				
			}
		} else {
			return null;
		}
		if ((resultTypes == null) || (resultTypes.length == 0) || ((resultTypes.length == 1) && (resultTypes[0] == null))) {
			throw new DiagnosticException(new BasicDiagnostic(Diagnostic.ERROR, XOCLPlugin.PLUGIN_ID, 0,
					NLS.bind(Messages.XOCLElementContext_TypeNotSetError, detailKey, EcoreUtil.getURI(exactModelElemet).fragment()),
					null));
		}
		
		if (!(eModelElement instanceof EClassifier)) {
			eModelElement = eModelElement.eClass();
		}
		
		return new XOCLElementContext((EClassifier) eModelElement, variables, resultTypes);
	}

	private static EClassifier[] getInitOrderResultType() {
		return new EClassifier[] {
			getOCLStandardLibrary().getInteger(),
			getOCLStandardLibrary().getReal(),
			getOCLStandardLibrary().getString(),
			OCLExpressionUtils.createSequenceType(getOCLStandardLibrary().getInteger()),
			OCLExpressionUtils.createSequenceType(getOCLStandardLibrary().getReal()),
			OCLExpressionUtils.createSequenceType(getOCLStandardLibrary().getString()),
		};
	}
	
	private static OCLStandardLibrary<EClassifier> getOCLStandardLibrary() {
		if (oclStandardLibrary == null) {
			oclStandardLibrary = new XoclLibrary.XoclEnvironmentFactory().createEnvironment().getOCLStandardLibrary();
		}
		return oclStandardLibrary;
	}
	
	public static boolean checkXOCLAnnotationSource(EAnnotation annotation){
		return annotation.getSource() != null && annotation.getSource().matches(OCL_ANNOTATION_SOURCE);
	}
}
