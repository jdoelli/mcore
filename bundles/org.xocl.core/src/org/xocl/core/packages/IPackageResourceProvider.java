/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.core.packages;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * @author Max Stepanov
 *
 */
public interface IPackageResourceProvider {

	URI findResourceURI(String nsURI, EObject eObject);
	
}
