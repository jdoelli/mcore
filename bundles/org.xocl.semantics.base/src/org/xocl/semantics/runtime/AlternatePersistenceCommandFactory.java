package org.xocl.semantics.runtime;

import java.util.HashMap;
import java.util.Iterator;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.IdentityCommand;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;

public class AlternatePersistenceCommandFactory {

	public Command createCommand(EditingDomain domain, Object newValue,
			String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot,
			String alternativePersistenceReference) {

		try {

			Command referenceCommand = null;
			Command command = null;
			ResourceSet resourceSet = domain.getResourceSet();

			Resource sourcePackage;
			EPackage sourceEPackage;

			// LOADING TARGET MODULES
			Resource persistencePackage = resourceSet.getResource(
					URI.createURI(alternativePersistencePackage), true);
			EPackage targetEPackage = (EPackage) persistencePackage
					.getContents().get(0);
			EFactory targetEcoreFac = targetEPackage.getEFactoryInstance();
			Resource resourceTargetTransformation = domain.getResourceSet()
					.createResource(URI.createPlatformResourceURI(
							persistenceLocation, true));
			// Resolve Resources
			EcoreUtil.resolveAll(domain.getResourceSet());

			if (newValue instanceof EObject) {
				EObject value = (EObject) newValue;
				sourcePackage = resourceSet
						.getResource(value.eClass().eResource().getURI(), true);

				EObject targetRootObj = null;
				if (sourcePackage.equals(persistencePackage)) {

					targetRootObj = EcoreUtil.copy(value);
					// EObject test = EcoreUtil.copy(value);
					// Persist into same package
					// TODO: Not just deep copy, referencing the containment
					// objects of source package instead of target package
					command = new AddCommand(domain,
							resourceTargetTransformation.getContents(),
							targetRootObj);
					// TODO:
					/*
					 * Create a new Command instead of "AddCommand" This command
					 * will generate the resource if needed This command will
					 * REMOVE the resource upon UNDO This command will SAVE the
					 * resource after execution
					 * 
					 * //resourceTargetTransformation.getContents().add(
					 * EcoreUtil.copy(value));
					 * resourceTargetTransformation.save(Collections.EMPTY_MAP);
					 * resourceSet.getResources().remove(
					 * resourceTargetTransformation);
					 */
				} else {

					System.out.println("heeere");
					sourceEPackage = (EPackage) sourcePackage.getContents()
							.get(0);
					@SuppressWarnings("unused")
					EFactory sourceEFactory = sourceEPackage
							.getEFactoryInstance();

					@SuppressWarnings("unused")
					EClassifier rootSourceClass = value.eClass();

					/*
					 * //TODO: Find closest match, not FIRST match Example:
					 * SubClass1 and Class1 => Firstmatch is SubClass1 , which
					 * may be wrong!!
					 */
					EClassifier targetSourceClass = searchForEqualNamedClassifierInPackage(
							alternativePersistenceRoot, targetEPackage);
					targetRootObj = targetEcoreFac
							.create((EClass) targetSourceClass);
					EObject sourceRootObj = value;

					// 1.REBUILD CONTAINMENT STRUCTURE
					// SET STORAGE FEATURES (ATTRIBUTES)
					// MAP srcContainment <--> trgContainment
					HashMap<EObject, EObject> srcToTargetObjMap = mapSourceObjectToTargetObject(
							sourceRootObj, targetRootObj);

					for (EAttribute eAttr : sourceRootObj.eClass()
							.getEAllAttributes()) {
						// obj.eGet(eAttr);
						if (sourceRootObj.eIsSet(eAttr))
							targetRootObj.eSet(
									searchForEqualNamedPropertyInClassifier(
											eAttr,
											(EClass) targetRootObj.eClass(),
											false),
									sourceRootObj.eGet(eAttr));
					}

					// 2. REBUILD Reference Structure with containment objs
					// generated
					// Lookup srcObjects and replace with trgObjects

					mapSourceObjectReferencesToTargetObjectReferences(
							srcToTargetObjMap, sourceRootObj, targetRootObj);

					// IF srcType exactly equals targetType => Keep the
					// reference from targetType into the srcType model

					command = new AddCommand(domain,
							resourceTargetTransformation.getContents(),
							targetRootObj);

					// myObj.eSet(myClass.getEStructuralFeature("aText0"), "it
					// works");

					// MAP FROM rootSourceClass to alternativePersistenceRoot
					// MAP FROM sourceEPackage to targetEPackage

					/*
					 * System.out.println( "map from " +
					 * value.eClass().eResource().getURI() + " to :" +
					 * targetEPackage.getNsURI());
					 * System.out.println("resource from current value is :" +
					 * value.eResource().getURI()); System.out.println(
					 * "resource from target is " + persistencePackage);
					 * System.out.println(value.eClass().eResource().getURI());
					 */

					// targetEPackage.getEClassifiers().
					/*
					 * // Do the mapping EObject myObj =
					 * ecoreFac.create((EClass)
					 * ePackage.getEClassifiers().get(0)); EClass myClass =
					 * (EClass) ePackage.getEClassifiers().get(0);
					 * 
					 * myObj.eSet(myClass.getEStructuralFeature("aText0"),
					 * "it works"); // Add it to the new Resource
					 * resourceTargetTransformation.getContents().add(myObj);
					 */

				}

				if (alternativePersistenceReference != null) {
					EStructuralFeature refFeat = value.eClass()
							.getEStructuralFeature(
									alternativePersistenceReference);
					if (refFeat != null && refFeat instanceof EReference) {
						if (refFeat.isMany())
							referenceCommand = new AddCommand(domain, value,
									refFeat, targetRootObj);
						else
							referenceCommand = new SetCommand(domain, value,
									refFeat, targetRootObj);

					}
				}

			}

			// resourceTargetTransformation.getContents().add(EcoreUtil.copy((EObject)
			// newValue));
			// now save the content.

			resourceSet.getResources().remove(persistencePackage);

			// TODO: We have to save after executing the command, otherwise we
			// have an empty resource
			// resourceTargetTransformation.save(saveOptions);
			// resourceSet.getResources().remove(resourceTargetTransformation);

			// System.out.println(s.getResource(URI.createURI("platform:/plugin/org.langlets.acomponent/model/acomponent.mcore"),true));

			// Ensure that all proxies are resolved so that references into the
			// controlled object will be saved to reference the new resource.
			//
			EcoreUtil.resolveAll(domain.getResourceSet());

			if (referenceCommand != null)
				return command.chain(referenceCommand);

			return command;

		} catch (Exception e) {
			return new IdentityCommand("Transformation failed");
		}
	}

	@SuppressWarnings("unchecked")
	private void mapSourceObjectReferencesToTargetObjectReferences(
			HashMap<EObject, EObject> srcToTargetObjMap, EObject sourceRootObj,
			EObject targetRootObj) {

		EClass sourceRootClass = sourceRootObj.eClass();
		EClass targetRootClass = targetRootObj.eClass();
		Object sourceObj;
		EStructuralFeature targetFeat;

		Object objContainments;
		EList<EObject> objRefList;

		for (EReference eFeat : sourceRootClass.getEAllReferences()) {
			if (eFeat.isContainment() | eFeat.isDerived())
				continue;
			sourceObj = sourceRootObj.eGet(eFeat);

			if (sourceObj == null)
				continue;

			targetFeat = searchForEqualNamedPropertyInClassifier(eFeat,
					targetRootClass, false);

			if (targetFeat == null)
				continue;

			if (sourceObj instanceof EObject) // UNARY
			{
				EObject obj = (EObject) sourceObj;

				EObject trgObj = srcToTargetObjMap.get(obj);

				if (trgObj == null)
					continue;
				// Create root OBJ
				/*
				 * SET the attributes Set the MAP from source to trg Call itself
				 * on all containments
				 */
				if (!obj.eClass().getEAllContainments().isEmpty()) {
					// Recursively build structure
					for (EReference eRef : obj.eClass().getEAllContainments()) {
						objContainments = obj.eGet(eRef);

						if (objContainments instanceof EList<?>) {

							objRefList = (EList<EObject>) objContainments;
							for (EObject newSrcObj : objRefList)
								this.mapSourceObjectReferencesToTargetObjectReferences(
										srcToTargetObjMap, newSrcObj,
										srcToTargetObjMap.get(newSrcObj));
						} else
							this.mapSourceObjectReferencesToTargetObjectReferences(
									srcToTargetObjMap,
									(EObject) objContainments,
									srcToTargetObjMap.get(objContainments));
					}
				}

				targetRootObj.eSet(targetFeat, trgObj);

			}

			// TODO: CHECK FOR AVOID BACKREFERENCES
			if (sourceObj instanceof EList<?>) // NARY
			{
				@SuppressWarnings("unchecked")
				EList<EObject> objList = (EList<EObject>) sourceObj;
				EList<EObject> trgObjList = new BasicEList<EObject>();
				EObject trgObj = null;

				for (EObject obj : objList) {
					trgObj = srcToTargetObjMap.get(obj);
					// add obj -> trgObj MAP
					trgObjList.add(trgObj);

					if (!obj.eClass().getEAllContainments().isEmpty()) {
						// Recursively build structure
						for (EReference eRef : obj.eClass()
								.getEAllContainments()) {
							objContainments = obj.eGet(eRef);
							if (objContainments instanceof EList<?>) {
								objRefList = (EList<EObject>) objContainments;
								for (EObject newSourceObject : objRefList)
									this.mapSourceObjectReferencesToTargetObjectReferences(
											srcToTargetObjMap, newSourceObject,
											srcToTargetObjMap
													.get(newSourceObject));
							} else
								this.mapSourceObjectReferencesToTargetObjectReferences(
										srcToTargetObjMap,
										(EObject) objContainments,
										srcToTargetObjMap.get(objContainments));
						}

					}

					targetRootObj.eSet(targetFeat, trgObjList);

				}

			}
		}

	}

	private EClassifier searchForEqualNamedClassifierInPackage(String srcClass,
			EPackage trgPackage) {

		// TODO: Find closest match, not FIRST match
		Iterator<EClassifier> itr = trgPackage.getEClassifiers().iterator();

		while (itr.hasNext()) {
			EClassifier eClassifier = itr.next();
			if (eClassifier.getName().toLowerCase()
					.endsWith(srcClass.toLowerCase()))
				return eClassifier;
		}

		return null;

	}

	private EStructuralFeature searchForEqualNamedPropertyInClassifier(
			EStructuralFeature srcProperty, EClass trgClassifier,
			boolean isContainment) {

		// TODO: Find closest match, not FIRST match

		Iterator<EStructuralFeature> itr = trgClassifier
				.getEStructuralFeatures().iterator();

		while (itr.hasNext()) {
			EStructuralFeature eFeat = itr.next();
			if ((trgClassifier.getEAllContainments().contains(eFeat)
					&& !isContainment)
					|| (!trgClassifier.getEAllContainments().contains(eFeat)
							&& isContainment))
				continue;
			if (eFeat.getName().toLowerCase()
					.endsWith(srcProperty.getName().toLowerCase()))
				return eFeat;
		}

		return null;

	}

	private HashMap<EObject, EObject> mapSourceObjectToTargetObject(
			EObject sourceRootObj, EObject targetRootObj) {
		EClass sourceRootClass = sourceRootObj.eClass();
		EClass targetRootClass = targetRootObj.eClass();
		HashMap<EObject, EObject> srcToTarget = new HashMap<EObject, EObject>();
		Object sourceObj;
		EStructuralFeature targetFeat;
		EFactory trgFactory = targetRootClass.getEPackage()
				.getEFactoryInstance();

		srcToTarget.put(sourceRootObj, targetRootObj);

		for (EStructuralFeature eFeat : sourceRootClass.getEAllContainments()) {
			sourceObj = sourceRootObj.eGet(eFeat);

			if (sourceObj == null)
				return srcToTarget;

			targetFeat = searchForEqualNamedPropertyInClassifier(eFeat,
					targetRootClass, true);

			if (sourceObj instanceof EObject) // UNARY
			{
				System.out.println("TODO UNARY sourceObjs");
				// Create root OBJ
				/*
				 * SET the attributes Set the MAP from source to trg Call itself
				 * on all containments
				 */

			}

			if (sourceObj instanceof EList<?>) // NARY
			{
				@SuppressWarnings("unchecked")
				EList<EObject> objList = (EList<EObject>) sourceObj;
				EList<EObject> trgObjList = new BasicEList<EObject>();
				EObject trgObj = null;
				EClassifier targetClass;

				for (EObject obj : objList) {
					targetClass = targetFeat.getEType();
					trgObj = trgFactory.create((EClass) targetClass);

					// add obj -> trgObj MAP
					trgObjList.add(trgObj);
					srcToTarget.put(obj, trgObj);

					// DO attribute mappings
					for (EAttribute eAttr : obj.eClass().getEAllAttributes()) {
						// obj.eGet(eAttr);
						trgObj.eSet(
								searchForEqualNamedPropertyInClassifier(eAttr,
										(EClass) targetClass, false),
								obj.eGet(eAttr));
					}

					if (!obj.eClass().getEAllContainments().isEmpty()) {
						// Recursively build structure
						HashMap<EObject, EObject> recursiveMap = this
								.mapSourceObjectToTargetObject(obj, trgObj);
						srcToTarget.putAll(recursiveMap);
					}

				}

				targetRootObj.eSet(targetFeat, trgObjList);

				/*
				 * Create multiple obj SET the attributes Set the MAP from
				 * source to trg Call itself on all containments
				 */
			}

		}

		return srcToTarget;

	}

}
