package org.xocl.semantics.runtime;

import static org.eclipse.emf.edit.command.SetCommand.UNSET_VALUE;
import static org.xocl.semantics.runtime.ValueHelpers.getValue;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.commands.DeleteCommandWithOptimizedCrossReferencer;

public class SetUpdateCommandFactory {

	private AlternatePersistenceCommandFactory persistFactory = new AlternatePersistenceCommandFactory();

	public Command createCommand(EditingDomain domain, XUpdatedObject owner,
			XUpdatedFeature feature) {

		if (feature.getInconsistent() != null && feature.getInconsistent()) {
			return null;
		}

		if (feature.getDeleteValues() != null) {
			return createDelete(domain, owner, feature);
		}

		if (feature.getFeature() instanceof EReference) {
			return createSetReferenceCommand(domain, owner, feature);
		} else {
			return createSetAttributeCommand(domain, owner, feature);
		}
	}

	private Command createDelete(EditingDomain domain, EObject owner,
			XUpdatedFeature feature) {

		// Changed: returns a delete command, to be added to the
		// compoundCommand
		if (feature instanceof EReference
				&& ((EReference) feature).isContainment()) {
			return new DeleteCommandWithOptimizedCrossReferencer(domain,
					feature.getDeleteValues().getEObjectValue());
		}

		System.out
				.println("this line unsets everything no matter if it matches");
		System.out
				.println("right now way too complex to check for every object");
		System.out.println("XTransitionImpl");

		return new SetCommand(domain, owner, feature.getFeature(), UNSET_VALUE);
	}

	private Command createSetAttributeCommand(EditingDomain domain,
			XUpdatedObject owner, XUpdatedFeature feature) {

		Object newValue = getValue(feature.getAsFirstInsertValues());

		return newValue == null ? null
				: new SetCommand(domain, //
						owner.getObject(), //
						feature.getFeature(), //
						newValue);
	}

	private Command createSetReferenceCommand(EditingDomain domain,
			XUpdatedObject owner, XUpdatedFeature feature) {

		Object newValue = getValue(feature.getAsFirstInsertValues());
		if (newValue instanceof XUpdatedObject) {
			newValue = ((XUpdatedObject) newValue).getObject();
		}

		if (newValue != null) {
			/*
			 * System.out.println("save " + newValue + "into " +
			 * nextStateObject.getPersistenceLocation());
			 * System.out.println("transform" + newValue + "into package" +
			 * nextStateObject .getAlternativePersistencePackage());
			 * System.out.println("transform" + newValue + "into root" +
			 * nextStateObject.getAlternativePersistenceRoot());
			 * System.out.println("update the reference of " + newValue +
			 * "into reference" + nextStateObject
			 * .getAlternativePersistenceReference());
			 */

			return new SetCommand(domain, owner.getObject(), feature
					.getFeature(), newValue).chain(persistFactory.createCommand(
							domain, newValue, owner.getPersistenceLocation(),
							owner.getAlternativePersistencePackage(),
							owner.getAlternativePersistenceRoot(),
							owner.getAlternativePersistenceReference()));
		}

		// OldCode: updatedEObject.eSet(updatedEFeature, newValue);
		// Changed: returns a set command, to be added to the
		// compoundCommand

		return null;
	}

}
