package org.xocl.semantics.runtime;

import static org.eclipse.emf.ecore.EcorePackage.Literals.EBOOLEAN;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EBOOLEAN_OBJECT;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EDATE;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EDOUBLE;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EDOUBLE_OBJECT;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EENUMERATOR;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EINT;
import static org.eclipse.emf.ecore.EcorePackage.Literals.EINTEGER_OBJECT;
import static org.eclipse.emf.ecore.EcorePackage.Literals.ESTRING;
import static org.xocl.semantics.SemanticsFactory.eINSTANCE;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XValue;

class ValueHelpers {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	static XValue createValue(EObject obj, EStructuralFeature feature) {
		XValue value = SemanticsFactory.eINSTANCE.createXValue();

		List values = null;
		if (feature instanceof EReference) {
			values = value.getUpdatedObjectValue();
		} else {
			if (feature.getEType() == ESTRING) {
				values = value.getStringValue();
			} else if (feature.getEType() == EBOOLEAN
					|| feature.getEType() == EBOOLEAN_OBJECT) {
				values = value.getBooleanValue();
			} else if (feature.getEType() == EINT
					|| feature.getEType() == EINTEGER_OBJECT) {
				values = value.getIntegerValue();
			} else if (feature.getEType() == EDOUBLE
					|| feature.getEType() == EDOUBLE_OBJECT) {
				values = value.getDoubleValue();
			} else if (feature.getEType() == EDATE) {
				values = value.getDateValue();
			} else if (feature.getEType() == EENUMERATOR) {
				values = value.getLiteralValue();
			}
		}

		if (values != null) {
			Stream<?> s = feature.isMany()
					? ((Collection<?>) obj.eGet(feature)).stream()
					: Stream.of(obj.eGet(feature));

			s.map(v -> {
				if (v instanceof EObject) {
					XUpdatedObject wrap = eINSTANCE.createXUpdatedObject();
					wrap.setObject((EObject) v);
					return wrap;
				}
				return v;
			})//
					.filter(Objects::nonNull) //
					.forEach(values::add);
		}
		return value;
	}

	static XValue getValue(XUpdatedFeature feature) {
		if (feature.getAsLastInsertValues() != null) {
			return feature.getAsLastInsertValues();
		}
		if (feature.getTowardsEndInsertValues() != null) {
			return feature.getTowardsEndInsertValues();
		}
		if (feature.getInTheMiddleInsertValues() != null) {
			return feature.getInTheMiddleInsertValues();
		}
		if (feature.getTowardsBeginningInsertValues() != null) {
			return feature.getTowardsBeginningInsertValues();
		}
		if (feature.getAsFirstInsertValues() != null) {
			return feature.getAsFirstInsertValues();
		}
		return null;
	}

	static int getIndex(XUpdatedFeature feature) {
		if (feature.getTowardsEndInsertPosition() != null) {
			return feature.getTowardsEndInsertPosition();
		}
		if (feature.getInTheMiddleInsertPosition() != null) {
			return feature.getInTheMiddleInsertPosition();
		}
		if (feature.getTowardsBeginningInsertPosition() != null) {
			return feature.getTowardsBeginningInsertPosition();
		}
		if (feature.getAsFirstInsertValues() != null) {
			return 0;
		}
		return CommandParameter.NO_INDEX;
	}

	static <T> T getValue(XValue value) {
		List<T> values = getValues(value);

		return values.isEmpty() ? null : values.get(0);
	}

	@SuppressWarnings("unchecked")
	static <T> List<T> getValues(XValue value) {
		if (value == null) {
			return Collections.emptyList();
		}

		// string
		if (!value.getStringValue().isEmpty()) {
			return (List<T>) value.getStringValue();
		}
		// date
		else if (!value.getDateValue().isEmpty()) {
			return (List<T>) value.getDateValue();
		}
		// boolean
		else if (!value.getBooleanValue().isEmpty()) {
			return (List<T>) value.getBooleanValue();
		}
		// double
		else if (!value.getDoubleValue().isEmpty()) {
			return (List<T>) value.getDoubleValue();
		}
		// integer
		else if (!value.getIntegerValue().isEmpty()) {
			return (List<T>) value.getIntegerValue();
		}
		// enum literal
		else if (!value.getLiteralValue().isEmpty()) {
			return (List<T>) value.getLiteralValue().stream() //
					.map(EEnumLiteral::getInstance) //
					.collect(Collectors.toList());
		}
		// object
		else if (!value.getEObjectValue().isEmpty()) {
			return (List<T>) value.getEObjectValue();
		}
		// update
		else if (!value.getUpdatedObjectValue().isEmpty()) {
			return (List<T>) value.getUpdatedObjectValue();
		}
		// default
		else {
			return Collections.emptyList();
		}
	}
}
