package org.xocl.semantics.runtime;

import static org.eclipse.emf.edit.command.CommandParameter.NO_INDEX;
import static org.xocl.semantics.runtime.ValueHelpers.getValues;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.AddCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.commands.DeleteCommandWithOptimizedCrossReferencer;

public class AddUpdateCommandFactory {

	public Command createCommand(EditingDomain domain, XUpdatedObject owner,
			XUpdatedFeature feature) {

		CompoundCommand result = new CompoundCommand();

		if (feature.getInconsistent() != null && feature.getInconsistent()) {
			return null;
		}

		createAddReferenceCommand(domain, owner, feature)
				.forEach(result::append);

		if (feature.getDeleteValues() != null) {
			Command cmd = removeCommand(domain, owner, feature);
			if (cmd != null) {
				result.append(cmd);
			}
		}

		return result.isEmpty() ? null : result;
	}

	private List<Command> createAddReferenceCommand(EditingDomain domain,
			XUpdatedObject owner, XUpdatedFeature feature) {

		List<Command> commands = new ArrayList<>();
		if (feature.getAsLastInsertValues() != null) {
			commands.add(new AddCommand(domain, owner.getObject(),
					feature.getFeature(),
					getValues(feature.getAsLastInsertValues())));
		}
		if (feature.getTowardsEndInsertValues() != null) {
			commands.add(new AddCommand(domain, owner.getObject(),
					feature.getFeature(),
					getValues(feature.getTowardsEndInsertValues()),
					getIndex(feature.getTowardsEndInsertPosition(), NO_INDEX)));
		}
		if (feature.getInTheMiddleInsertValues() != null) {
			commands.add(new AddCommand(domain, owner.getObject(),
					feature.getFeature(),
					getValues(feature.getInTheMiddleInsertValues()), getIndex(
							feature.getInTheMiddleInsertPosition(), NO_INDEX)));
		}
		if (feature.getTowardsBeginningInsertValues() != null) {
			commands.add(new AddCommand(domain, owner.getObject(),
					feature.getFeature(),
					getValues(feature.getTowardsBeginningInsertValues()),
					getIndex(feature.getTowardsBeginningInsertPosition(), 0)));
		}
		if (feature.getAsFirstInsertValues() != null) {
			commands.add(new AddCommand(domain, owner.getObject(),
					feature.getFeature(),
					getValues(feature.getAsFirstInsertValues()), 0));
		}

		return commands;
	}

	private int getIndex(Integer index, int defaultValue) {
		return index == null ? defaultValue : index;
	}

	private Command removeCommand(EditingDomain domain, XUpdatedObject owner,
			XUpdatedFeature feature) {

		EStructuralFeature eFeature = feature.getFeature();
		List<?> values = getValues(feature.getDeleteValues());
		
		if (values == null || values.isEmpty()) {
			return null;
		}

		if (eFeature instanceof EReference
				&& ((EReference) eFeature).isContainment()) {

			return new DeleteCommandWithOptimizedCrossReferencer(domain,
					values);
		}

		return new RemoveCommand(domain, owner.getObject(), eFeature, values);
	}

}
