/**
 */
package org.xocl.semantics.impl;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.DUmmy;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateContainer;
import org.xocl.semantics.XAddSpecification;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XAdditionalTriggeringSpecification;
import org.xocl.semantics.XObjectUpdateType;
import org.xocl.semantics.XOriginalTriggeringSpecification;
import org.xocl.semantics.XSemantics;
import org.xocl.semantics.XSemanticsElement;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XTriggeredUpdate;
import org.xocl.semantics.XTriggeringSpecification;
import org.xocl.semantics.XUpdateMode;
import org.xocl.semantics.XUpdatedFeature;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XUpdatedOperation;
import org.xocl.semantics.XUpdatedTypedElement;
import org.xocl.semantics.XValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SemanticsPackageImpl extends EPackageImpl implements SemanticsPackage {
	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID, choiceConstraint, helper.getProblems(),
						SemanticsPackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xSemanticsEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdateContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xTransitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xAddSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xAbstractTriggeringUpdateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xTriggeringSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xAdditionalTriggeringSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xTriggeredUpdateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xOriginalTriggeringSpecificationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdatedObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdatedFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdatedTypedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xUpdatedOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dUmmyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum xAddUpdateModeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum xObjectUpdateTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass xSemanticsElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum xUpdateModeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.xocl.semantics.SemanticsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private SemanticsPackageImpl() {
		super(eNS_URI, SemanticsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link SemanticsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static SemanticsPackage init() {
		if (isInited)
			return (SemanticsPackage) EPackage.Registry.INSTANCE.getEPackage(SemanticsPackage.eNS_URI);

		// Obtain or create and register package
		SemanticsPackageImpl theSemanticsPackage = (SemanticsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof SemanticsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new SemanticsPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theSemanticsPackage.createPackageContents();

		// Initialize created meta-data
		theSemanticsPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theSemanticsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(SemanticsPackage.eNS_URI, theSemanticsPackage);
		return theSemanticsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXSemantics() {
		return xSemanticsEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXSemantics_Transition() {
		return (EReference) xSemanticsEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdateContainer() {
		return xUpdateContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdateContainer_ContainingTransition() {
		return (EReference) xUpdateContainerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject() {
		return xUpdateContainerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String() {
		return xUpdateContainerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__AddAndMergeUpdate__XUpdate() {
		return xUpdateContainerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject() {
		return xUpdateContainerEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String() {
		return xUpdateContainerEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject() {
		return xUpdateContainerEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__CreateNextStateNewObject__EClass() {
		return xUpdateContainerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__NextStateObjectDefinitionFromObject__EObject() {
		return xUpdateContainerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdateContainer__DeleteObjectInNextState__EObject() {
		return xUpdateContainerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXTransition() {
		return xTransitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXTransition_ExecuteAutomatically() {
		return (EAttribute) xTransitionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXTransition_ExecuteNow() {
		return (EAttribute) xTransitionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXTransition_ExecutionTime() {
		return (EAttribute) xTransitionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTransition_TriggeringUpdate() {
		return (EReference) xTransitionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTransition_UpdatedObject() {
		return (EReference) xTransitionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTransition_DanglingRemovedObject() {
		return (EReference) xTransitionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTransition_AllUpdates() {
		return (EReference) xTransitionEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTransition_FocusObjects() {
		return (EReference) xTransitionEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXTransition__ExecuteNow$Update__Boolean() {
		return xTransitionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdate() {
		return xUpdateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_UpdatedObject() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_Feature() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_Value() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdate_Mode() {
		return (EAttribute) xUpdateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_ContributesTo() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdate_Object() {
		return (EAttribute) xUpdateEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_Operation() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdate_AddSpecification() {
		return (EReference) xUpdateEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXAddSpecification() {
		return xAddSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAddSpecification_BeforeObject() {
		return (EReference) xAddSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAddSpecification_AfterObject() {
		return (EReference) xAddSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXAddSpecification_AddKind() {
		return (EAttribute) xAddSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXAbstractTriggeringUpdate() {
		return xAbstractTriggeringUpdateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAbstractTriggeringUpdate_TriggeredOwnedUpdate() {
		return (EReference) xAbstractTriggeringUpdateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAbstractTriggeringUpdate_AdditionalTriggeringOfThis() {
		return (EReference) xAbstractTriggeringUpdateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXAbstractTriggeringUpdate_Processsed() {
		return (EAttribute) xAbstractTriggeringUpdateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate() {
		return (EReference) xAbstractTriggeringUpdateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAbstractTriggeringUpdate_AllOwnedUpdates() {
		return (EReference) xAbstractTriggeringUpdateEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXTriggeringSpecification() {
		return xTriggeringSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation() {
		return (EReference) xTriggeringSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXTriggeringSpecification_NameofTriggeringUpdateAnnotation() {
		return (EAttribute) xTriggeringSpecificationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTriggeringSpecification_TriggeringUpdate() {
		return (EReference) xTriggeringSpecificationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXAdditionalTriggeringSpecification() {
		return xAdditionalTriggeringSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate() {
		return (EReference) xAdditionalTriggeringSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXTriggeredUpdate() {
		return xTriggeredUpdateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTriggeredUpdate_OriginalTriggeringOfThis() {
		return (EReference) xTriggeredUpdateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTriggeredUpdate_OriginallyTriggeringUpdate() {
		return (EReference) xTriggeredUpdateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXTriggeredUpdate_AdditionalTriggeringByThis() {
		return (EReference) xTriggeredUpdateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXOriginalTriggeringSpecification() {
		return xOriginalTriggeringSpecificationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXOriginalTriggeringSpecification_ContainingTriggeredUpdate() {
		return (EReference) xOriginalTriggeringSpecificationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdatedObject() {
		return xUpdatedObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedObject_Object() {
		return (EReference) xUpdatedObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedObject_UpdatedFeature() {
		return (EReference) xUpdatedObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_ObjectIsFocusedAfterExecution() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedObject_XUpdatedTypedElement() {
		return (EReference) xUpdatedObjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedObject_UpdatedOperation() {
		return (EReference) xUpdatedObjectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_PersistenceLocation() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_AlternativePersistencePackage() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_AlternativePersistenceRoot() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_AlternativePersistenceReference() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedObject_ObjectUpdateType() {
		return (EAttribute) xUpdatedObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedObject_ClassForCreation() {
		return (EReference) xUpdatedObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdatedObject__NextStateFeatureDefinitionFromFeature__EStructuralFeature() {
		return xUpdatedObjectEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation() {
		return xUpdatedObjectEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdatedFeature() {
		return xUpdatedFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedFeature_Feature() {
		return (EReference) xUpdatedFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXValue() {
		return xValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXValue_UpdatedObjectValue() {
		return (EReference) xValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_BooleanValue() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_IntegerValue() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_DoubleValue() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_StringValue() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_DateValue() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXValue_LiteralValue() {
		return (EReference) xValueEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_IsCorrect() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXValue_CreateClone() {
		return (EReference) xValueEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXValue_EObjectValue() {
		return (EReference) xValueEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXValue_ParameterArguments() {
		return (EAttribute) xValueEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXValue__AddOtherValues__XValue() {
		return xValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXValue__DisjointFrom__XValue() {
		return xValueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdatedTypedElement() {
		return xUpdatedTypedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_ContributingUpdate() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_ExpectedToBeAddedValue() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_ExpectedToBeRemovedValue() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_ExpectedResultingValue() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_ExpectationsMet() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_OldValue() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_NewValue() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_Inconsistent() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_Executed() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_TowardsEndInsertPosition() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_TowardsEndInsertValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_InTheMiddleInsertPosition() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_InTheMiddleInsertValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedTypedElement_TowardsBeginningInsertPosition() {
		return (EAttribute) xUpdatedTypedElementEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_TowardsBeginningInsertValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_AsFirstInsertValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_AsLastInsertValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedTypedElement_DeleteValues() {
		return (EReference) xUpdatedTypedElementEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXUpdatedOperation() {
		return xUpdatedOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getXUpdatedOperation_Operation() {
		return (EReference) xUpdatedOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXUpdatedOperation_Parameter() {
		return (EAttribute) xUpdatedOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDUmmy() {
		return dUmmyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDUmmy_Text1() {
		return (EAttribute) dUmmyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getXAddUpdateMode() {
		return xAddUpdateModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getXObjectUpdateType() {
		return xObjectUpdateTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getXSemanticsElement() {
		return xSemanticsElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getXSemanticsElement_KindLabel() {
		return (EAttribute) xSemanticsElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXSemanticsElement__RenderedKindLabel() {
		return xSemanticsElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXSemanticsElement__IndentLevel() {
		return xSemanticsElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXSemanticsElement__IndentationSpaces() {
		return xSemanticsElementEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXSemanticsElement__IndentationSpaces__Integer() {
		return xSemanticsElementEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getXSemanticsElement__StringOrMissing__String() {
		return xSemanticsElementEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getXUpdateMode() {
		return xUpdateModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsFactory getSemanticsFactory() {
		return (SemanticsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		xSemanticsElementEClass = createEClass(XSEMANTICS_ELEMENT);
		createEAttribute(xSemanticsElementEClass, XSEMANTICS_ELEMENT__KIND_LABEL);
		createEOperation(xSemanticsElementEClass, XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL);
		createEOperation(xSemanticsElementEClass, XSEMANTICS_ELEMENT___INDENT_LEVEL);
		createEOperation(xSemanticsElementEClass, XSEMANTICS_ELEMENT___INDENTATION_SPACES);
		createEOperation(xSemanticsElementEClass, XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER);
		createEOperation(xSemanticsElementEClass, XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING);

		xSemanticsEClass = createEClass(XSEMANTICS);
		createEReference(xSemanticsEClass, XSEMANTICS__TRANSITION);

		xUpdateContainerEClass = createEClass(XUPDATE_CONTAINER);
		createEReference(xUpdateContainerEClass, XUPDATE_CONTAINER__CONTAINING_TRANSITION);
		createEOperation(xUpdateContainerEClass, XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS);
		createEOperation(xUpdateContainerEClass, XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT);
		createEOperation(xUpdateContainerEClass, XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT);
		createEOperation(xUpdateContainerEClass,
				XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT);
		createEOperation(xUpdateContainerEClass,
				XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING);
		createEOperation(xUpdateContainerEClass, XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE);
		createEOperation(xUpdateContainerEClass,
				XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT);
		createEOperation(xUpdateContainerEClass,
				XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING);
		createEOperation(xUpdateContainerEClass,
				XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT);

		xTransitionEClass = createEClass(XTRANSITION);
		createEAttribute(xTransitionEClass, XTRANSITION__EXECUTE_AUTOMATICALLY);
		createEAttribute(xTransitionEClass, XTRANSITION__EXECUTE_NOW);
		createEAttribute(xTransitionEClass, XTRANSITION__EXECUTION_TIME);
		createEReference(xTransitionEClass, XTRANSITION__TRIGGERING_UPDATE);
		createEReference(xTransitionEClass, XTRANSITION__UPDATED_OBJECT);
		createEReference(xTransitionEClass, XTRANSITION__DANGLING_REMOVED_OBJECT);
		createEReference(xTransitionEClass, XTRANSITION__ALL_UPDATES);
		createEReference(xTransitionEClass, XTRANSITION__FOCUS_OBJECTS);
		createEOperation(xTransitionEClass, XTRANSITION___EXECUTE_NOW$_UPDATE__BOOLEAN);

		xUpdateEClass = createEClass(XUPDATE);
		createEReference(xUpdateEClass, XUPDATE__UPDATED_OBJECT);
		createEReference(xUpdateEClass, XUPDATE__FEATURE);
		createEReference(xUpdateEClass, XUPDATE__OPERATION);
		createEReference(xUpdateEClass, XUPDATE__VALUE);
		createEAttribute(xUpdateEClass, XUPDATE__MODE);
		createEReference(xUpdateEClass, XUPDATE__ADD_SPECIFICATION);
		createEReference(xUpdateEClass, XUPDATE__CONTRIBUTES_TO);
		createEAttribute(xUpdateEClass, XUPDATE__OBJECT);

		xAddSpecificationEClass = createEClass(XADD_SPECIFICATION);
		createEAttribute(xAddSpecificationEClass, XADD_SPECIFICATION__ADD_KIND);
		createEReference(xAddSpecificationEClass, XADD_SPECIFICATION__BEFORE_OBJECT);
		createEReference(xAddSpecificationEClass, XADD_SPECIFICATION__AFTER_OBJECT);

		xAbstractTriggeringUpdateEClass = createEClass(XABSTRACT_TRIGGERING_UPDATE);
		createEReference(xAbstractTriggeringUpdateEClass, XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE);
		createEReference(xAbstractTriggeringUpdateEClass, XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS);
		createEAttribute(xAbstractTriggeringUpdateEClass, XABSTRACT_TRIGGERING_UPDATE__PROCESSSED);
		createEReference(xAbstractTriggeringUpdateEClass, XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE);
		createEReference(xAbstractTriggeringUpdateEClass, XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES);

		xTriggeringSpecificationEClass = createEClass(XTRIGGERING_SPECIFICATION);
		createEReference(xTriggeringSpecificationEClass,
				XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION);
		createEAttribute(xTriggeringSpecificationEClass,
				XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION);
		createEReference(xTriggeringSpecificationEClass, XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE);

		xAdditionalTriggeringSpecificationEClass = createEClass(XADDITIONAL_TRIGGERING_SPECIFICATION);
		createEReference(xAdditionalTriggeringSpecificationEClass,
				XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE);

		xTriggeredUpdateEClass = createEClass(XTRIGGERED_UPDATE);
		createEReference(xTriggeredUpdateEClass, XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS);
		createEReference(xTriggeredUpdateEClass, XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE);
		createEReference(xTriggeredUpdateEClass, XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS);

		xOriginalTriggeringSpecificationEClass = createEClass(XORIGINAL_TRIGGERING_SPECIFICATION);
		createEReference(xOriginalTriggeringSpecificationEClass,
				XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE);

		xUpdatedObjectEClass = createEClass(XUPDATED_OBJECT);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__OBJECT_UPDATE_TYPE);
		createEReference(xUpdatedObjectEClass, XUPDATED_OBJECT__OBJECT);
		createEReference(xUpdatedObjectEClass, XUPDATED_OBJECT__CLASS_FOR_CREATION);
		createEReference(xUpdatedObjectEClass, XUPDATED_OBJECT__UPDATED_FEATURE);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION);
		createEReference(xUpdatedObjectEClass, XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT);
		createEReference(xUpdatedObjectEClass, XUPDATED_OBJECT__UPDATED_OPERATION);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__PERSISTENCE_LOCATION);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT);
		createEAttribute(xUpdatedObjectEClass, XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE);
		createEOperation(xUpdatedObjectEClass,
				XUPDATED_OBJECT___NEXT_STATE_FEATURE_DEFINITION_FROM_FEATURE__ESTRUCTURALFEATURE);
		createEOperation(xUpdatedObjectEClass,
				XUPDATED_OBJECT___NEXT_STATE_OPERATION_DEFINITION_FROM_OPERATION__EOPERATION);

		xUpdatedFeatureEClass = createEClass(XUPDATED_FEATURE);
		createEReference(xUpdatedFeatureEClass, XUPDATED_FEATURE__FEATURE);

		xValueEClass = createEClass(XVALUE);
		createEReference(xValueEClass, XVALUE__UPDATED_OBJECT_VALUE);
		createEAttribute(xValueEClass, XVALUE__BOOLEAN_VALUE);
		createEAttribute(xValueEClass, XVALUE__INTEGER_VALUE);
		createEAttribute(xValueEClass, XVALUE__DOUBLE_VALUE);
		createEAttribute(xValueEClass, XVALUE__STRING_VALUE);
		createEAttribute(xValueEClass, XVALUE__DATE_VALUE);
		createEReference(xValueEClass, XVALUE__LITERAL_VALUE);
		createEAttribute(xValueEClass, XVALUE__IS_CORRECT);
		createEReference(xValueEClass, XVALUE__CREATE_CLONE);
		createEReference(xValueEClass, XVALUE__EOBJECT_VALUE);
		createEAttribute(xValueEClass, XVALUE__PARAMETER_ARGUMENTS);
		createEOperation(xValueEClass, XVALUE___ADD_OTHER_VALUES__XVALUE);
		createEOperation(xValueEClass, XVALUE___DISJOINT_FROM__XVALUE);

		xUpdatedTypedElementEClass = createEClass(XUPDATED_TYPED_ELEMENT);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__OLD_VALUE);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__NEW_VALUE);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__INCONSISTENT);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__EXECUTED);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES);
		createEAttribute(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES);
		createEReference(xUpdatedTypedElementEClass, XUPDATED_TYPED_ELEMENT__DELETE_VALUES);

		xUpdatedOperationEClass = createEClass(XUPDATED_OPERATION);
		createEReference(xUpdatedOperationEClass, XUPDATED_OPERATION__OPERATION);
		createEAttribute(xUpdatedOperationEClass, XUPDATED_OPERATION__PARAMETER);

		dUmmyEClass = createEClass(DUMMY);
		createEAttribute(dUmmyEClass, DUMMY__TEXT1);

		// Create enums
		xAddUpdateModeEEnum = createEEnum(XADD_UPDATE_MODE);
		xObjectUpdateTypeEEnum = createEEnum(XOBJECT_UPDATE_TYPE);
		xUpdateModeEEnum = createEEnum(XUPDATE_MODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		xSemanticsEClass.getESuperTypes().add(this.getXSemanticsElement());
		xUpdateContainerEClass.getESuperTypes().add(this.getXSemanticsElement());
		xTransitionEClass.getESuperTypes().add(this.getXUpdateContainer());
		xUpdateEClass.getESuperTypes().add(this.getXUpdateContainer());
		xAbstractTriggeringUpdateEClass.getESuperTypes().add(this.getXUpdate());
		xTriggeringSpecificationEClass.getESuperTypes().add(this.getXSemanticsElement());
		xAdditionalTriggeringSpecificationEClass.getESuperTypes().add(this.getXTriggeringSpecification());
		xTriggeredUpdateEClass.getESuperTypes().add(this.getXAbstractTriggeringUpdate());
		xOriginalTriggeringSpecificationEClass.getESuperTypes().add(this.getXTriggeringSpecification());
		xUpdatedObjectEClass.getESuperTypes().add(this.getXSemanticsElement());
		xUpdatedFeatureEClass.getESuperTypes().add(this.getXSemanticsElement());
		xUpdatedFeatureEClass.getESuperTypes().add(this.getXUpdatedTypedElement());
		xValueEClass.getESuperTypes().add(this.getXSemanticsElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(xSemanticsElementEClass, XSemanticsElement.class, "XSemanticsElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXSemanticsElement_KindLabel(), ecorePackage.getEString(), "kindLabel", null, 0, 1,
				XSemanticsElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEOperation(getXSemanticsElement__RenderedKindLabel(), ecorePackage.getEString(), "renderedKindLabel", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getXSemanticsElement__IndentLevel(), ecorePackage.getEIntegerObject(), "indentLevel", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getXSemanticsElement__IndentationSpaces(), ecorePackage.getEString(), "indentationSpaces", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getXSemanticsElement__IndentationSpaces__Integer(), ecorePackage.getEString(),
				"indentationSpaces", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXSemanticsElement__StringOrMissing__String(), ecorePackage.getEString(),
				"stringOrMissing", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xSemanticsEClass, XSemantics.class, "XSemantics", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXSemantics_Transition(), this.getXTransition(), null, "transition", null, 0, -1,
				XSemantics.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xUpdateContainerEClass, XUpdateContainer.class, "XUpdateContainer", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXUpdateContainer_ContainingTransition(), this.getXTransition(), null, "containingTransition",
				null, 0, 1, XUpdateContainer.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getXUpdateContainer__CreateNextStateNewObject__EClass(), ecorePackage.getEObject(),
				"createNextStateNewObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEClass(), "typeOfNewObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXUpdateContainer__NextStateObjectDefinitionFromObject__EObject(),
				this.getXUpdatedObject(), "nextStateObjectDefinitionFromObject", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "currentStateObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXUpdateContainer__DeleteObjectInNextState__EObject(), ecorePackage.getEBooleanObject(),
				"deleteObjectInNextState", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "objectToBeDeleted", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject(),
				this.getXUpdate(), "addReferenceUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "updatedObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "updatedReference", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdateMode(), "updateMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXAddUpdateMode(), "addMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "targetObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "afterObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "beforeObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String(),
				this.getXUpdate(), "addReferenceUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "updatedObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "updatedReference", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdateMode(), "updateMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXAddUpdateMode(), "addMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "targetObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "afterObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "beforeObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "persistenceLocation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistencePackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistenceRoot", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistenceReference", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXUpdateContainer__AddAndMergeUpdate__XUpdate(), this.getXUpdate(), "addAndMergeUpdate",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdate(), "updateToBeMerged", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject(),
				this.getXUpdate(), "addAttributeUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "updatedObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEAttribute(), "updatedAttribute", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdateMode(), "updateMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXAddUpdateMode(), "addMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "afterObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "beforeObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String(),
				this.getXUpdate(), "addAttributeUpdate", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "updatedObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEAttribute(), "updatedAttribute", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdateMode(), "updateMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXAddUpdateMode(), "addMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "value", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "afterObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "beforeObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "persistenceLocation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistencePackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistenceRoot", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "alternativePersistenceReference", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject(),
				this.getXUpdate(), "invokeOperationCall", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "updatedObject", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEOperation(), "operation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEJavaObject(), "parameters", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "afterObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXUpdatedObject(), "beforeObjectConstraint", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xTransitionEClass, XTransition.class, "XTransition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXTransition_ExecuteAutomatically(), ecorePackage.getEBooleanObject(), "executeAutomatically",
				null, 0, 1, XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXTransition_ExecuteNow(), ecorePackage.getEBooleanObject(), "executeNow", null, 0, 1,
				XTransition.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getXTransition_ExecutionTime(), ecorePackage.getEDate(), "executionTime", null, 0, 1,
				XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getXTransition_TriggeringUpdate(), this.getXTriggeredUpdate(), null, "triggeringUpdate", null, 0,
				-1, XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTransition_UpdatedObject(), this.getXUpdatedObject(), null, "updatedObject", null, 0, -1,
				XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTransition_DanglingRemovedObject(), ecorePackage.getEObject(), null, "danglingRemovedObject",
				null, 0, -1, XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTransition_AllUpdates(), this.getXUpdate(), null, "allUpdates", null, 0, -1,
				XTransition.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getXTransition_FocusObjects(), this.getXUpdatedObject(), null, "focusObjects", null, 0, -1,
				XTransition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getXTransition__ExecuteNow$Update__Boolean(), this.getXUpdate(), "executeNow$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xUpdateEClass, XUpdate.class, "XUpdate", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXUpdate_UpdatedObject(), this.getXUpdatedObject(), null, "updatedObject", null, 1, 1,
				XUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdate_Feature(), ecorePackage.getEStructuralFeature(), null, "feature", null, 1, 1,
				XUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdate_Operation(), ecorePackage.getEOperation(), null, "operation", null, 0, 1,
				XUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdate_Value(), this.getXValue(), null, "value", null, 1, 1, XUpdate.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getXUpdate_Mode(), this.getXUpdateMode(), "mode", null, 0, 1, XUpdate.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdate_AddSpecification(), this.getXAddSpecification(), null, "addSpecification", null, 0, 1,
				XUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdate_ContributesTo(), this.getXUpdatedTypedElement(),
				this.getXUpdatedTypedElement_ContributingUpdate(), "contributesTo", null, 0, 1, XUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdate_Object(), ecorePackage.getEJavaObject(), "object", null, 0, 1, XUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xAddSpecificationEClass, XAddSpecification.class, "XAddSpecification", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXAddSpecification_AddKind(), this.getXAddUpdateMode(), "addKind", null, 0, 1,
				XAddSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getXAddSpecification_BeforeObject(), this.getXUpdatedObject(), null, "beforeObject", null, 0, 1,
				XAddSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXAddSpecification_AfterObject(), this.getXUpdatedObject(), null, "afterObject", null, 0, 1,
				XAddSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xAbstractTriggeringUpdateEClass, XAbstractTriggeringUpdate.class, "XAbstractTriggeringUpdate",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXAbstractTriggeringUpdate_TriggeredOwnedUpdate(), this.getXTriggeredUpdate(),
				this.getXTriggeredUpdate_OriginallyTriggeringUpdate(), "triggeredOwnedUpdate", null, 0, -1,
				XAbstractTriggeringUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXAbstractTriggeringUpdate_AdditionalTriggeringOfThis(),
				this.getXAdditionalTriggeringSpecification(), null, "additionalTriggeringOfThis", null, 0, -1,
				XAbstractTriggeringUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXAbstractTriggeringUpdate_Processsed(), ecorePackage.getEBooleanObject(), "processsed", null,
				0, 1, XAbstractTriggeringUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate(), this.getXTriggeredUpdate(), null,
				"additionallyTriggeringUpdate", null, 0, -1, XAbstractTriggeringUpdate.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getXAbstractTriggeringUpdate_AllOwnedUpdates(), this.getXUpdate(), null, "allOwnedUpdates", null,
				0, -1, XAbstractTriggeringUpdate.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(xTriggeringSpecificationEClass, XTriggeringSpecification.class, "XTriggeringSpecification",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation(),
				ecorePackage.getEStructuralFeature(), null, "featureOfTriggeringUpdateAnnotation", null, 0, 1,
				XTriggeringSpecification.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getXTriggeringSpecification_NameofTriggeringUpdateAnnotation(), ecorePackage.getEString(),
				"nameofTriggeringUpdateAnnotation", null, 0, 1, XTriggeringSpecification.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTriggeringSpecification_TriggeringUpdate(), this.getXAbstractTriggeringUpdate(), null,
				"triggeringUpdate", null, 0, 1, XTriggeringSpecification.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(xAdditionalTriggeringSpecificationEClass, XAdditionalTriggeringSpecification.class,
				"XAdditionalTriggeringSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate(), this.getXTriggeredUpdate(),
				this.getXTriggeredUpdate_AdditionalTriggeringByThis(), "additionalTriggeringUpdate", null, 0, 1,
				XAdditionalTriggeringSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xTriggeredUpdateEClass, XTriggeredUpdate.class, "XTriggeredUpdate", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXTriggeredUpdate_OriginalTriggeringOfThis(), this.getXOriginalTriggeringSpecification(),
				this.getXOriginalTriggeringSpecification_ContainingTriggeredUpdate(), "originalTriggeringOfThis", null,
				0, 1, XTriggeredUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTriggeredUpdate_OriginallyTriggeringUpdate(), this.getXAbstractTriggeringUpdate(),
				this.getXAbstractTriggeringUpdate_TriggeredOwnedUpdate(), "originallyTriggeringUpdate", null, 0, 1,
				XTriggeredUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXTriggeredUpdate_AdditionalTriggeringByThis(), this.getXAdditionalTriggeringSpecification(),
				this.getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate(), "additionalTriggeringByThis",
				null, 0, -1, XTriggeredUpdate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xOriginalTriggeringSpecificationEClass, XOriginalTriggeringSpecification.class,
				"XOriginalTriggeringSpecification", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXOriginalTriggeringSpecification_ContainingTriggeredUpdate(), this.getXTriggeredUpdate(),
				this.getXTriggeredUpdate_OriginalTriggeringOfThis(), "containingTriggeredUpdate", null, 1, 1,
				XOriginalTriggeringSpecification.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xUpdatedObjectEClass, XUpdatedObject.class, "XUpdatedObject", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getXUpdatedObject_ObjectUpdateType(), this.getXObjectUpdateType(), "objectUpdateType", null, 0,
				1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedObject_Object(), ecorePackage.getEObject(), null, "object", null, 1, 1,
				XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedObject_ClassForCreation(), ecorePackage.getEClass(), null, "classForCreation", null,
				0, 1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedObject_UpdatedFeature(), this.getXUpdatedFeature(), null, "updatedFeature", null, 0,
				-1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedObject_ObjectIsFocusedAfterExecution(), ecorePackage.getEBooleanObject(),
				"objectIsFocusedAfterExecution", null, 0, 1, XUpdatedObject.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedObject_XUpdatedTypedElement(), this.getXUpdatedTypedElement(), null,
				"xUpdatedTypedElement", null, 0, -1, XUpdatedObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedObject_UpdatedOperation(), this.getXUpdatedOperation(), null, "updatedOperation",
				null, 0, -1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedObject_PersistenceLocation(), ecorePackage.getEString(), "persistenceLocation", null,
				0, 1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedObject_AlternativePersistencePackage(), ecorePackage.getEString(),
				"alternativePersistencePackage", null, 0, 1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedObject_AlternativePersistenceRoot(), ecorePackage.getEString(),
				"alternativePersistenceRoot", null, 0, 1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedObject_AlternativePersistenceReference(), ecorePackage.getEString(),
				"alternativePersistenceReference", null, 0, 1, XUpdatedObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getXUpdatedObject__NextStateFeatureDefinitionFromFeature__EStructuralFeature(),
				this.getXUpdatedFeature(), "nextStateFeatureDefinitionFromFeature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEStructuralFeature(), "updatedFeature", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation(),
				this.getXUpdatedOperation(), "nextStateOperationDefinitionFromOperation", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEOperation(), "updatedOperation", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xUpdatedFeatureEClass, XUpdatedFeature.class, "XUpdatedFeature", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXUpdatedFeature_Feature(), ecorePackage.getEStructuralFeature(), null, "feature", null, 1, 1,
				XUpdatedFeature.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xValueEClass, XValue.class, "XValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXValue_UpdatedObjectValue(), this.getXUpdatedObject(), null, "updatedObjectValue", null, 0,
				-1, XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_BooleanValue(), ecorePackage.getEBooleanObject(), "booleanValue", null, 0, -1,
				XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_IntegerValue(), ecorePackage.getEIntegerObject(), "integerValue", null, 0, -1,
				XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_DoubleValue(), ecorePackage.getEDoubleObject(), "doubleValue", null, 0, -1,
				XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_StringValue(), ecorePackage.getEString(), "stringValue", null, 0, -1, XValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_DateValue(), ecorePackage.getEDate(), "dateValue", null, 0, -1, XValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXValue_LiteralValue(), ecorePackage.getEEnumLiteral(), null, "literalValue", null, 0, -1,
				XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_IsCorrect(), ecorePackage.getEBooleanObject(), "isCorrect", null, 0, 1, XValue.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXValue_CreateClone(), this.getXValue(), null, "createClone", null, 0, 1, XValue.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getXValue_EObjectValue(), ecorePackage.getEObject(), null, "eObjectValue", null, 0, -1,
				XValue.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getXValue_ParameterArguments(), ecorePackage.getEJavaObject(), "parameterArguments", null, 0, -1,
				XValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(getXValue__AddOtherValues__XValue(), ecorePackage.getEBooleanObject(), "addOtherValues", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXValue(), "otherValues", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getXValue__DisjointFrom__XValue(), ecorePackage.getEBooleanObject(), "disjointFrom", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getXValue(), "otherXValue", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(xUpdatedTypedElementEClass, XUpdatedTypedElement.class, "XUpdatedTypedElement", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXUpdatedTypedElement_ContributingUpdate(), this.getXUpdate(), this.getXUpdate_ContributesTo(),
				"contributingUpdate", null, 0, -1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_ExpectedToBeAddedValue(), this.getXValue(), null,
				"expectedToBeAddedValue", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_ExpectedToBeRemovedValue(), this.getXValue(), null,
				"expectedToBeRemovedValue", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_ExpectedResultingValue(), this.getXValue(), null,
				"expectedResultingValue", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_ExpectationsMet(), ecorePackage.getEBooleanObject(), "expectationsMet",
				null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_OldValue(), this.getXValue(), null, "oldValue", null, 1, 1,
				XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_NewValue(), this.getXValue(), null, "newValue", null, 1, 1,
				XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_Inconsistent(), ecorePackage.getEBooleanObject(), "inconsistent", null,
				0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_Executed(), ecorePackage.getEBooleanObject(), "executed", null, 0, 1,
				XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_TowardsEndInsertPosition(), ecorePackage.getEIntegerObject(),
				"towardsEndInsertPosition", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_TowardsEndInsertValues(), this.getXValue(), null,
				"towardsEndInsertValues", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_InTheMiddleInsertPosition(), ecorePackage.getEIntegerObject(),
				"inTheMiddleInsertPosition", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_InTheMiddleInsertValues(), this.getXValue(), null,
				"inTheMiddleInsertValues", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedTypedElement_TowardsBeginningInsertPosition(), ecorePackage.getEIntegerObject(),
				"towardsBeginningInsertPosition", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_TowardsBeginningInsertValues(), this.getXValue(), null,
				"towardsBeginningInsertValues", null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_AsFirstInsertValues(), this.getXValue(), null, "asFirstInsertValues",
				null, 0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_AsLastInsertValues(), this.getXValue(), null, "asLastInsertValues", null,
				0, 1, XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getXUpdatedTypedElement_DeleteValues(), this.getXValue(), null, "deleteValues", null, 0, 1,
				XUpdatedTypedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(xUpdatedOperationEClass, XUpdatedOperation.class, "XUpdatedOperation", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getXUpdatedOperation_Operation(), ecorePackage.getEOperation(), null, "operation", null, 0, 1,
				XUpdatedOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getXUpdatedOperation_Parameter(), ecorePackage.getEJavaObject(), "parameter", null, 0, -1,
				XUpdatedOperation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(dUmmyEClass, DUmmy.class, "DUmmy", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDUmmy_Text1(), ecorePackage.getEString(), "text1", null, 0, 1, DUmmy.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(xAddUpdateModeEEnum, XAddUpdateMode.class, "XAddUpdateMode");
		addEEnumLiteral(xAddUpdateModeEEnum, XAddUpdateMode.FIRST);
		addEEnumLiteral(xAddUpdateModeEEnum, XAddUpdateMode.TOWARDS_BEGINNING);
		addEEnumLiteral(xAddUpdateModeEEnum, XAddUpdateMode.IN_THE_MIDDLE);
		addEEnumLiteral(xAddUpdateModeEEnum, XAddUpdateMode.TOWARDS_END);
		addEEnumLiteral(xAddUpdateModeEEnum, XAddUpdateMode.LAST);

		initEEnum(xObjectUpdateTypeEEnum, XObjectUpdateType.class, "XObjectUpdateType");
		addEEnumLiteral(xObjectUpdateTypeEEnum, XObjectUpdateType.KEEP);
		addEEnumLiteral(xObjectUpdateTypeEEnum, XObjectUpdateType.CREATE);
		addEEnumLiteral(xObjectUpdateTypeEEnum, XObjectUpdateType.DELETE);

		initEEnum(xUpdateModeEEnum, XUpdateMode.class, "XUpdateMode");
		addEEnumLiteral(xUpdateModeEEnum, XUpdateMode.REDEFINE);
		addEEnumLiteral(xUpdateModeEEnum, XUpdateMode.ADD);
		addEEnumLiteral(xUpdateModeEEnum, XUpdateMode.REMOVE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint", "trg.name = \'XSemantics\'" });
		addAnnotation(getXSemanticsElement__RenderedKindLabel(), source, new String[] { "body",
				"let e: String = indentationSpaces().concat(kindLabel.toUpper()) in \n    if e.oclIsInvalid() then null else e endif\n" });
		addAnnotation(getXSemanticsElement__IndentLevel(), source, new String[] { "body",
				"if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(XSemanticsElement)\n  then eContainer().oclAsType(XSemanticsElement).indentLevel() + 1\n  else 0 endif endif\n" });
		addAnnotation(getXSemanticsElement__IndentationSpaces(), source, new String[] { "body",
				"let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nindentationSpaces(numberOfSpaces)\n" });
		addAnnotation(getXSemanticsElement__IndentationSpaces__Integer(), source, new String[] { "body",
				"let sizeMinusOne: Integer = let e: Integer = size - 1 in \n    if e.oclIsInvalid() then null else e endif in\nif (let e: Boolean = size < 1 in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'\'\n  else (let e: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n    if e.oclIsInvalid() then null else e endif)\nendif\n" });
		addAnnotation(getXSemanticsElement__StringOrMissing__String(), source, new String[] { "body",
				"if p=null then \'MISSING\' else if p.trim()=\'\' then \'MISSING\' else p endif endif" });
		addAnnotation(getXSemanticsElement_KindLabel(), source, new String[] { "derive", "\'TODO\'\n" });
		addAnnotation(getXUpdateContainer__CreateNextStateNewObject__EClass(), source, new String[] { "body", "null" });
		addAnnotation(getXUpdateContainer__NextStateObjectDefinitionFromObject__EObject(), source,
				new String[] { "body", "null" });
		addAnnotation(getXUpdateContainer__DeleteObjectInNextState__EObject(), source, new String[] { "body", "null" });
		addAnnotation(
				getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String(),
				source, new String[] { "body", "null" });
		addAnnotation(getXUpdateContainer__AddAndMergeUpdate__XUpdate(), source, new String[] { "body", "null" });
		addAnnotation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject(),
				source, new String[] { "body", "null\n" });
		addAnnotation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getXUpdateContainer_ContainingTransition(), source, new String[] { "derive",
				"if self.oclIsKindOf(XTransition)\n   then self.oclAsType(XTransition) \n   else self.eContainer().oclAsType(XUpdateContainer).containingTransition\nendif" });
		addAnnotation(getXTransition__ExecuteNow$Update__Boolean(), source, new String[] { "body", "null" });
		addAnnotation(getXTransition_ExecuteNow(), source, new String[] { "derive", "null\n" });
		addAnnotation(getXTransition_AllUpdates(), source, new String[] { "derive",
				"self.triggeringUpdate.oclAsType(XUpdate)->union(self.triggeringUpdate.allOwnedUpdates)" });
		addAnnotation(getXUpdate_Feature(), source, new String[] { "choiceConstruction",
				"if updatedObject.oclIsUndefined() then OrderedSet{} else \r\n\r\nif updatedObject.object.oclIsUndefined() then\r\n   if updatedObject.classForCreation.oclIsUndefined() then OrderedSet{} else\r\n          updatedObject.classForCreation.eAllStructuralFeatures endif\r\n     else updatedObject.object.eClass().eAllStructuralFeatures endif\r\n  endif" });
		addAnnotation(getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate(), source, new String[] { "derive",
				"additionalTriggeringOfThis->asOrderedSet()->collect(it: semantics::XAdditionalTriggeringSpecification | it.additionalTriggeringUpdate)->asOrderedSet()->excluding(null)->asOrderedSet() \n" });
		addAnnotation(getXAbstractTriggeringUpdate_AllOwnedUpdates(), source, new String[] { "derive",
				"triggeredOwnedUpdate.oclAsType(XUpdate)->union(triggeredOwnedUpdate.allOwnedUpdates)" });
		addAnnotation(getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation(), source, new String[] {
				"derive",
				"if triggeringUpdate.oclIsUndefined()\n  then null\n  else triggeringUpdate.feature\nendif\n" });
		addAnnotation(getXTriggeringSpecification_TriggeringUpdate(), source,
				new String[] { "derive", "let nl: semantics::XAbstractTriggeringUpdate = null in nl\n" });
		addAnnotation(getXUpdatedObject__NextStateFeatureDefinitionFromFeature__EStructuralFeature(), source,
				new String[] { "body", "null" });
		addAnnotation(getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getXUpdatedObject_ObjectIsFocusedAfterExecution(), source, new String[] { "derive",
				"if (eContainer().oclIsTypeOf(XTransition)) then\r\neContainer().oclAsType(XTransition)\r\n.focusObjects->notEmpty() and eContainer().oclAsType(XTransition)\r\n.focusObjects->notEmpty()->includes(self) else false endif" });
		addAnnotation(getXUpdatedObject_XUpdatedTypedElement(), source, new String[] { "derive",
				"let e1: OrderedSet(semantics::XUpdatedTypedElement)  = let chain: OrderedSet(semantics::XUpdatedOperation)  = updatedOperation->asOrderedSet() in\nchain->iterate(i:semantics::XUpdatedOperation; r: OrderedSet(semantics::XUpdatedTypedElement)=OrderedSet{} | if i.oclIsKindOf(semantics::XUpdatedTypedElement) then r->including(i.oclAsType(semantics::XUpdatedTypedElement))->asOrderedSet() \n else r endif)->union(updatedFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getXValue__AddOtherValues__XValue(), source, new String[] { "body", "null\n" });
		addAnnotation(getXValue__DisjointFrom__XValue(), source, new String[] { "body",
				"/* overwritten in Java */\nstringValue->intersection(otherXValue.stringValue)->isEmpty() and\nbooleanValue->intersection(otherXValue.booleanValue)->isEmpty() and\nintegerValue->intersection(otherXValue.integerValue)->isEmpty() and\ndoubleValue->intersection(otherXValue.doubleValue)->isEmpty() and\ndateValue->intersection(otherXValue.dateValue)->isEmpty() and\nself.updatedObjectValue->intersection(otherXValue.updatedObjectValue)->isEmpty()\n" });
		addAnnotation(getXValue_CreateClone(), source, new String[] { "derive",
				"Tuple{updatedObjectValue=self.updatedObjectValue, booleanValue=self.booleanValue, integerValue= self.integerValue, doubleValue = self.doubleValue, stringValue=self.stringValue, dateValue=self.dateValue, literalValue = self.literalValue}" });
		addAnnotation(getXValue_EObjectValue(), source,
				new String[] { "derive", "updatedObjectValue.object->reject(oclIsUndefined())->asOrderedSet()\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true", "uuidAttributeName", "_uuid" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation((getXSemanticsElement__StringOrMissing__String()).getEParameters().get(0), source,
				new String[] { "mName", "p" });
		addAnnotation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject(),
				source, new String[] { "mName", "AddAttributeUpdate" });
		addAnnotation(
				getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String(),
				source, new String[] { "mName", "AddAttributeUpdate" });
		addAnnotation(
				(getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject())
						.getEParameters().get(1),
				source, new String[] { "mName", "operation" });
		addAnnotation(
				(getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject())
						.getEParameters().get(2),
				source, new String[] { "mName", "parameters" });
		addAnnotation(getXTransition_FocusObjects(), source, new String[] { "mName", "Focus Objects " });
		addAnnotation(getXUpdate_Operation(), source, new String[] { "mName", "operation" });
		addAnnotation(getXUpdate_Object(), source, new String[] { "mName", "object" });
		addAnnotation(getXTriggeringSpecification_NameofTriggeringUpdateAnnotation(), source,
				new String[] { "mName", "Name of Triggering Update Annotation" });
		addAnnotation(getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation(), source,
				new String[] { "mName", "nextStateOperationDefinitionFromOperation" });
		addAnnotation(
				(getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation()).getEParameters().get(0),
				source, new String[] { "mName", "updated Operation" });
		addAnnotation(getXUpdatedObject_XUpdatedTypedElement(), source,
				new String[] { "mName", "XUpdatedTypedElement" });
		addAnnotation(getXUpdatedObject_UpdatedOperation(), source, new String[] { "mName", "updatedOperation" });
		addAnnotation((getXValue__DisjointFrom__XValue()).getEParameters().get(0), source,
				new String[] { "mName", "Other XValue" });
		addAnnotation(getXUpdatedOperation_Operation(), source, new String[] { "mName", "operation" });
		addAnnotation(getXUpdatedOperation_Parameter(), source, new String[] { "mName", "parameter" });
		addAnnotation(dUmmyEClass, source, new String[] { "mName", "DUmmy" });
		addAnnotation(getDUmmy_Text1(), source, new String[] { "mName", "text1" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(xSemanticsEClass, source, new String[] { "kindLabelDerive", "\'Semantics\'\n" });
		addAnnotation(xTransitionEClass, source, new String[] { "kindLabelDerive", "\'->\'\n" });
		addAnnotation(xUpdateEClass, source, new String[] { "kindLabelDerive", "\':=\'\n" });
		addAnnotation(xAdditionalTriggeringSpecificationEClass, source, new String[] { "triggeringUpdateDerive",
				"additionalTriggeringUpdate\n", "kindLabelDerive", "\'Additional Triggering\'\n" });
		addAnnotation(xOriginalTriggeringSpecificationEClass, source,
				new String[] { "triggeringUpdateDerive",
						"if containingTriggeredUpdate.oclIsUndefined()\n  then null\n  else containingTriggeredUpdate.originallyTriggeringUpdate\nendif\n",
						"kindLabelDerive", "\'Original Triggering\'\n" });
		addAnnotation(xUpdatedObjectEClass, source, new String[] { "kindLabelDerive", "\'Object\'\n" });
		addAnnotation(xUpdatedFeatureEClass, source, new String[] { "kindLabelDerive", "\'Feature\'\n" });
		addAnnotation(xValueEClass, source, new String[] { "kindLabelDerive",
				"if self.eContainmentFeature().name=\'expectedToBeAddedValue\'\r\n  then \'Expected Added\'\r\nelse if self.eContainmentFeature().name=\'expectedToBeRemovedValue\'\r\n  then \'Expected Removed\'\r\nelse if self.eContainmentFeature().name=\'expectedResultingValue\'\r\n  then \'Expected Resulting\'\r\nelse if self.eContainmentFeature().name=\'oldValue\'\r\n  then \'Old Value\'\r\nelse if self.eContainmentFeature().name=\'newValue\'\r\n  then \'New Value\'\r\nelse if self.eContainmentFeature().name=\'value\'\r\n  then \'Value\'\r\nelse \'TODO\'.concat(self.eContainmentFeature().name) endif endif endif endif endif endif" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getXUpdateContainer_ContainingTransition(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTransition_ExecuteAutomatically(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTransition_ExecuteNow(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTransition_ExecutionTime(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTransition_AllUpdates(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXUpdate_Feature(), source, new String[] { "propertySortChoices", "false" });
		addAnnotation(getXAbstractTriggeringUpdate_Processsed(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXAbstractTriggeringUpdate_AllOwnedUpdates(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTriggeredUpdate_OriginallyTriggeringUpdate(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXTriggeredUpdate_AdditionalTriggeringByThis(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXValue_IsCorrect(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXValue_CreateClone(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXUpdatedTypedElement_ExpectationsMet(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getXUpdatedTypedElement_Executed(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getXUpdateContainer_ContainingTransition(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXTransition_ExecuteAutomatically(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXTransition_ExecuteNow(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXTransition_ExecutionTime(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXTransition_AllUpdates(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXAbstractTriggeringUpdate_Processsed(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getXAbstractTriggeringUpdate_AllOwnedUpdates(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXTriggeredUpdate_OriginallyTriggeringUpdate(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getXTriggeredUpdate_AdditionalTriggeringByThis(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getXUpdatedObject_PersistenceLocation(), source,
				new String[] { "propertyCategory", "Persistence", "createColumn", "false" });
		addAnnotation(getXUpdatedObject_AlternativePersistencePackage(), source,
				new String[] { "propertyCategory", "Persistence", "createColumn", "false" });
		addAnnotation(getXUpdatedObject_AlternativePersistenceRoot(), source,
				new String[] { "propertyCategory", "Persistence", "createColumn", "false" });
		addAnnotation(getXUpdatedObject_AlternativePersistenceReference(), source,
				new String[] { "propertyCategory", "Persistence", "createColumn", "false" });
		addAnnotation(getXValue_IsCorrect(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXValue_CreateClone(), source, new String[] { "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_ExpectedToBeAddedValue(), source,
				new String[] { "propertyCategory", "Expectation Testing", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_ExpectedToBeRemovedValue(), source,
				new String[] { "propertyCategory", "Expectation Testing", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_ExpectedResultingValue(), source,
				new String[] { "propertyCategory", "Expectation Testing", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_ExpectationsMet(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Expectation Testing" });
		addAnnotation(getXUpdatedTypedElement_OldValue(), source,
				new String[] { "propertyCategory", "Transition Documentation", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_NewValue(), source,
				new String[] { "propertyCategory", "Transition Documentation", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_Inconsistent(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_Executed(), source,
				new String[] { "createColumn", "false", "propertyCategory", "Transition Execution" });
		addAnnotation(getXUpdatedTypedElement_TowardsEndInsertPosition(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_TowardsEndInsertValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_InTheMiddleInsertPosition(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_InTheMiddleInsertValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_TowardsBeginningInsertPosition(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_TowardsBeginningInsertValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "false" });
		addAnnotation(getXUpdatedTypedElement_AsFirstInsertValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_AsLastInsertValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "true" });
		addAnnotation(getXUpdatedTypedElement_DeleteValues(), source,
				new String[] { "propertyCategory", "Transition Execution", "createColumn", "true" });
	}

} //SemanticsPackageImpl
