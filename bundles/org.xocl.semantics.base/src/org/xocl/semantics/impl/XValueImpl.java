/**
 */
package org.xocl.semantics.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XValue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getUpdatedObjectValue <em>Updated Object Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getBooleanValue <em>Boolean Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getIntegerValue <em>Integer Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getDoubleValue <em>Double Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getStringValue <em>String Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getDateValue <em>Date Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getLiteralValue <em>Literal Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getIsCorrect <em>Is Correct</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getCreateClone <em>Create Clone</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getEObjectValue <em>EObject Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XValueImpl#getParameterArguments <em>Parameter Arguments</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XValueImpl extends XSemanticsElementImpl implements XValue {
	/**
	 * The cached value of the '{@link #getUpdatedObjectValue() <em>Updated Object Value</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatedObjectValue()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdatedObject> updatedObjectValue;

	/**
	 * The cached value of the '{@link #getBooleanValue() <em>Boolean Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooleanValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Boolean> booleanValue;

	/**
	 * The cached value of the '{@link #getIntegerValue() <em>Integer Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntegerValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Integer> integerValue;

	/**
	 * The cached value of the '{@link #getDoubleValue() <em>Double Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoubleValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Double> doubleValue;

	/**
	 * The cached value of the '{@link #getStringValue() <em>String Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringValue()
	 * @generated
	 * @ordered
	 */
	protected EList<String> stringValue;

	/**
	 * The cached value of the '{@link #getDateValue() <em>Date Value</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateValue()
	 * @generated
	 * @ordered
	 */
	protected EList<Date> dateValue;

	/**
	 * The cached value of the '{@link #getLiteralValue() <em>Literal Value</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiteralValue()
	 * @generated
	 * @ordered
	 */
	protected EList<EEnumLiteral> literalValue;

	/**
	 * The default value of the '{@link #getIsCorrect() <em>Is Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsCorrect()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getIsCorrect() <em>Is Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsCorrect()
	 * @generated
	 * @ordered
	 */
	protected Boolean isCorrect = IS_CORRECT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameterArguments() <em>Parameter Arguments</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameterArguments()
	 * @generated
	 * @ordered
	 */
	protected EList<Object> parameterArguments;

	/**
	 * The parsed OCL expression for the body of the '{@link #addOtherValues <em>Add Other Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addOtherValues
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addOtherValuessemanticsXValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #disjointFrom <em>Disjoint From</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #disjointFrom
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression disjointFromsemanticsXValueBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCreateClone <em>Create Clone</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCreateClone
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression createCloneDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEObjectValue <em>EObject Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEObjectValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eObjectValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedObject> getUpdatedObjectValue() {
		if (updatedObjectValue == null) {
			updatedObjectValue = new EObjectResolvingEList<XUpdatedObject>(XUpdatedObject.class, this,
					SemanticsPackage.XVALUE__UPDATED_OBJECT_VALUE);
		}
		return updatedObjectValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Boolean> getBooleanValue() {
		if (booleanValue == null) {
			booleanValue = new EDataTypeUniqueEList<Boolean>(Boolean.class, this,
					SemanticsPackage.XVALUE__BOOLEAN_VALUE);
		}
		return booleanValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Integer> getIntegerValue() {
		if (integerValue == null) {
			integerValue = new EDataTypeUniqueEList<Integer>(Integer.class, this,
					SemanticsPackage.XVALUE__INTEGER_VALUE);
		}
		return integerValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Double> getDoubleValue() {
		if (doubleValue == null) {
			doubleValue = new EDataTypeUniqueEList<Double>(Double.class, this, SemanticsPackage.XVALUE__DOUBLE_VALUE);
		}
		return doubleValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getStringValue() {
		if (stringValue == null) {
			stringValue = new EDataTypeUniqueEList<String>(String.class, this, SemanticsPackage.XVALUE__STRING_VALUE);
		}
		return stringValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Date> getDateValue() {
		if (dateValue == null) {
			dateValue = new EDataTypeUniqueEList<Date>(Date.class, this, SemanticsPackage.XVALUE__DATE_VALUE);
		}
		return dateValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EEnumLiteral> getLiteralValue() {
		if (literalValue == null) {
			literalValue = new EObjectResolvingEList.Unsettable<EEnumLiteral>(EEnumLiteral.class, this,
					SemanticsPackage.XVALUE__LITERAL_VALUE);
		}
		return literalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLiteralValue() {
		if (literalValue != null)
			((InternalEList.Unsettable<?>) literalValue).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLiteralValue() {
		return literalValue != null && ((InternalEList.Unsettable<?>) literalValue).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsCorrect() {
		return isCorrect;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsCorrect(Boolean newIsCorrect) {
		Boolean oldIsCorrect = isCorrect;
		isCorrect = newIsCorrect;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XVALUE__IS_CORRECT, oldIsCorrect,
					isCorrect));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getCreateClone() {
		XValue createClone = basicGetCreateClone();
		return createClone != null && createClone.eIsProxy() ? (XValue) eResolveProxy((InternalEObject) createClone)
				: createClone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetCreateClone() {
		/**
		 * @OCL Tuple{updatedObjectValue=self.updatedObjectValue, booleanValue=self.booleanValue, integerValue= self.integerValue, doubleValue = self.doubleValue, stringValue=self.stringValue, dateValue=self.dateValue, literalValue = self.literalValue}
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XVALUE;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XVALUE__CREATE_CLONE;

		if (createCloneDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				createCloneDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(createCloneDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XVALUE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			XValue result = (XValue) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getEObjectValue() {
		/**
		 * @OCL updatedObjectValue.object->reject(oclIsUndefined())->asOrderedSet()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XVALUE;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XVALUE__EOBJECT_VALUE;

		if (eObjectValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eObjectValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eObjectValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XVALUE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<EObject> result = (EList<EObject>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Object> getParameterArguments() {
		if (parameterArguments == null) {
			parameterArguments = new EDataTypeUniqueEList.Unsettable<Object>(Object.class, this,
					SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS);
		}
		return parameterArguments;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParameterArguments() {
		if (parameterArguments != null)
			((InternalEList.Unsettable<?>) parameterArguments).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParameterArguments() {
		return parameterArguments != null && ((InternalEList.Unsettable<?>) parameterArguments).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean addOtherValues(XValue otherValues) {
		if (otherValues != null) {
			getStringValue().addAll(otherValues.getStringValue());
			getBooleanValue().addAll(otherValues.getBooleanValue());
			getIntegerValue().addAll(otherValues.getIntegerValue());
			getDoubleValue().addAll(otherValues.getDoubleValue());
			getDateValue().addAll(otherValues.getDateValue());
			getUpdatedObjectValue().addAll(otherValues.getUpdatedObjectValue());
			getLiteralValue().addAll(otherValues.getLiteralValue());

		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean disjointFrom(XValue otherXValue) {
		if (otherXValue == null) {
			return true;
		} else {
			for (String it1 : getStringValue()) {
				for (String it2 : otherXValue.getStringValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			for (Boolean it1 : getBooleanValue()) {
				for (Boolean it2 : otherXValue.getBooleanValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			for (Integer it1 : getIntegerValue()) {
				for (Integer it2 : otherXValue.getIntegerValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			for (Double it1 : getDoubleValue()) {
				for (Double it2 : otherXValue.getDoubleValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			for (Date it1 : getDateValue()) {
				for (Date it2 : otherXValue.getDateValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			for (XUpdatedObject it1 : getUpdatedObjectValue()) {
				for (XUpdatedObject it2 : otherXValue.getUpdatedObjectValue()) {
					if (it1 != null && it2 != null && it1.equals(it2)) {
						return false;
					}
				}
			}
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XVALUE__UPDATED_OBJECT_VALUE:
			return getUpdatedObjectValue();
		case SemanticsPackage.XVALUE__BOOLEAN_VALUE:
			return getBooleanValue();
		case SemanticsPackage.XVALUE__INTEGER_VALUE:
			return getIntegerValue();
		case SemanticsPackage.XVALUE__DOUBLE_VALUE:
			return getDoubleValue();
		case SemanticsPackage.XVALUE__STRING_VALUE:
			return getStringValue();
		case SemanticsPackage.XVALUE__DATE_VALUE:
			return getDateValue();
		case SemanticsPackage.XVALUE__LITERAL_VALUE:
			return getLiteralValue();
		case SemanticsPackage.XVALUE__IS_CORRECT:
			return getIsCorrect();
		case SemanticsPackage.XVALUE__CREATE_CLONE:
			if (resolve)
				return getCreateClone();
			return basicGetCreateClone();
		case SemanticsPackage.XVALUE__EOBJECT_VALUE:
			return getEObjectValue();
		case SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS:
			return getParameterArguments();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XVALUE__UPDATED_OBJECT_VALUE:
			getUpdatedObjectValue().clear();
			getUpdatedObjectValue().addAll((Collection<? extends XUpdatedObject>) newValue);
			return;
		case SemanticsPackage.XVALUE__BOOLEAN_VALUE:
			getBooleanValue().clear();
			getBooleanValue().addAll((Collection<? extends Boolean>) newValue);
			return;
		case SemanticsPackage.XVALUE__INTEGER_VALUE:
			getIntegerValue().clear();
			getIntegerValue().addAll((Collection<? extends Integer>) newValue);
			return;
		case SemanticsPackage.XVALUE__DOUBLE_VALUE:
			getDoubleValue().clear();
			getDoubleValue().addAll((Collection<? extends Double>) newValue);
			return;
		case SemanticsPackage.XVALUE__STRING_VALUE:
			getStringValue().clear();
			getStringValue().addAll((Collection<? extends String>) newValue);
			return;
		case SemanticsPackage.XVALUE__DATE_VALUE:
			getDateValue().clear();
			getDateValue().addAll((Collection<? extends Date>) newValue);
			return;
		case SemanticsPackage.XVALUE__LITERAL_VALUE:
			getLiteralValue().clear();
			getLiteralValue().addAll((Collection<? extends EEnumLiteral>) newValue);
			return;
		case SemanticsPackage.XVALUE__IS_CORRECT:
			setIsCorrect((Boolean) newValue);
			return;
		case SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS:
			getParameterArguments().clear();
			getParameterArguments().addAll((Collection<? extends Object>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XVALUE__UPDATED_OBJECT_VALUE:
			getUpdatedObjectValue().clear();
			return;
		case SemanticsPackage.XVALUE__BOOLEAN_VALUE:
			getBooleanValue().clear();
			return;
		case SemanticsPackage.XVALUE__INTEGER_VALUE:
			getIntegerValue().clear();
			return;
		case SemanticsPackage.XVALUE__DOUBLE_VALUE:
			getDoubleValue().clear();
			return;
		case SemanticsPackage.XVALUE__STRING_VALUE:
			getStringValue().clear();
			return;
		case SemanticsPackage.XVALUE__DATE_VALUE:
			getDateValue().clear();
			return;
		case SemanticsPackage.XVALUE__LITERAL_VALUE:
			unsetLiteralValue();
			return;
		case SemanticsPackage.XVALUE__IS_CORRECT:
			setIsCorrect(IS_CORRECT_EDEFAULT);
			return;
		case SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS:
			unsetParameterArguments();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XVALUE__UPDATED_OBJECT_VALUE:
			return updatedObjectValue != null && !updatedObjectValue.isEmpty();
		case SemanticsPackage.XVALUE__BOOLEAN_VALUE:
			return booleanValue != null && !booleanValue.isEmpty();
		case SemanticsPackage.XVALUE__INTEGER_VALUE:
			return integerValue != null && !integerValue.isEmpty();
		case SemanticsPackage.XVALUE__DOUBLE_VALUE:
			return doubleValue != null && !doubleValue.isEmpty();
		case SemanticsPackage.XVALUE__STRING_VALUE:
			return stringValue != null && !stringValue.isEmpty();
		case SemanticsPackage.XVALUE__DATE_VALUE:
			return dateValue != null && !dateValue.isEmpty();
		case SemanticsPackage.XVALUE__LITERAL_VALUE:
			return isSetLiteralValue();
		case SemanticsPackage.XVALUE__IS_CORRECT:
			return IS_CORRECT_EDEFAULT == null ? isCorrect != null : !IS_CORRECT_EDEFAULT.equals(isCorrect);
		case SemanticsPackage.XVALUE__CREATE_CLONE:
			return basicGetCreateClone() != null;
		case SemanticsPackage.XVALUE__EOBJECT_VALUE:
			return !getEObjectValue().isEmpty();
		case SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS:
			return isSetParameterArguments();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case SemanticsPackage.XVALUE___ADD_OTHER_VALUES__XVALUE:
			return addOtherValues((XValue) arguments.get(0));
		case SemanticsPackage.XVALUE___DISJOINT_FROM__XVALUE:
			return disjointFrom((XValue) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (booleanValue: ");
		result.append(booleanValue);
		result.append(", integerValue: ");
		result.append(integerValue);
		result.append(", doubleValue: ");
		result.append(doubleValue);
		result.append(", stringValue: ");
		result.append(stringValue);
		result.append(", dateValue: ");
		result.append(dateValue);
		result.append(", isCorrect: ");
		result.append(isCorrect);
		result.append(", parameterArguments: ");
		result.append(parameterArguments);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel if self.eContainmentFeature().name='expectedToBeAddedValue'
	then 'Expected Added'
	else if self.eContainmentFeature().name='expectedToBeRemovedValue'
	then 'Expected Removed'
	else if self.eContainmentFeature().name='expectedResultingValue'
	then 'Expected Resulting'
	else if self.eContainmentFeature().name='oldValue'
	then 'Old Value'
	else if self.eContainmentFeature().name='newValue'
	then 'New Value'
	else if self.eContainmentFeature().name='value'
	then 'Value'
	else 'TODO'.concat(self.eContainmentFeature().name) endif endif endif endif endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XVALUE);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} //XValueImpl
