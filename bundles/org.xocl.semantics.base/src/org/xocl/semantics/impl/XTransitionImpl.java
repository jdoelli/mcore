/**
 */
package org.xocl.semantics.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XTriggeredUpdate;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.commands.CompoundCommandFocus;
import org.xocl.semantics.runtime.UpdateRuntime;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>XTransition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getExecuteAutomatically <em>Execute Automatically</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getExecuteNow <em>Execute Now</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getExecutionTime <em>Execution Time</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getTriggeringUpdate <em>Triggering Update</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getUpdatedObject <em>Updated Object</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getDanglingRemovedObject <em>Dangling Removed Object</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getAllUpdates <em>All Updates</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XTransitionImpl#getFocusObjects <em>Focus Objects</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XTransitionImpl extends XUpdateContainerImpl implements XTransition {
	/**
	 * The default value of the '{@link #getExecuteAutomatically() <em>Execute
	 * Automatically</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getExecuteAutomatically()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean EXECUTE_AUTOMATICALLY_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecuteAutomatically() <em>Execute
	 * Automatically</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getExecuteAutomatically()
	 * @generated
	 * @ordered
	 */
	protected Boolean executeAutomatically = EXECUTE_AUTOMATICALLY_EDEFAULT;

	/**
	 * This is true if the Execute Automatically attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean executeAutomaticallyESet;

	/**
	 * The default value of the '{@link #getExecuteNow() <em>Execute Now</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExecuteNow()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean EXECUTE_NOW_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected static final Date EXECUTION_TIME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecutionTime() <em>Execution Time</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getExecutionTime()
	 * @generated
	 * @ordered
	 */
	protected Date executionTime = EXECUTION_TIME_EDEFAULT;

	/**
	 * This is true if the Execution Time attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean executionTimeESet;

	/**
	 * The cached value of the '{@link #getTriggeringUpdate() <em>Triggering Update</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getTriggeringUpdate()
	 * @generated
	 * @ordered
	 */
	protected EList<XTriggeredUpdate> triggeringUpdate;

	/**
	 * The cached value of the '{@link #getUpdatedObject() <em>Updated Object</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getUpdatedObject()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdatedObject> updatedObject;

	/**
	 * The cached value of the '{@link #getDanglingRemovedObject() <em>Dangling Removed Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDanglingRemovedObject()
	 * @generated
	 * @ordered
	 */
	protected EList<EObject> danglingRemovedObject;

	/**
	 * The cached value of the '{@link #getFocusObjects() <em>Focus
	 * Objects</em>}' reference list. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getFocusObjects()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdatedObject> focusObjects;

	/**
	 * The parsed OCL expression for the body of the '{@link #executeNow$Update <em>Execute Now$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #executeNow$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression executeNow$UpdateecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExecuteNow <em>Execute Now</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExecuteNow
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression executeNowDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllUpdates <em>All Updates</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getAllUpdates
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allUpdatesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected XTransitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XTRANSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getExecuteAutomatically() {
		return executeAutomatically;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecuteAutomatically(Boolean newExecuteAutomatically) {
		Boolean oldExecuteAutomatically = executeAutomatically;
		executeAutomatically = newExecuteAutomatically;
		boolean oldExecuteAutomaticallyESet = executeAutomaticallyESet;
		executeAutomaticallyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY,
					oldExecuteAutomatically, executeAutomatically, !oldExecuteAutomaticallyESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecuteAutomatically() {
		Boolean oldExecuteAutomatically = executeAutomatically;
		boolean oldExecuteAutomaticallyESet = executeAutomaticallyESet;
		executeAutomatically = EXECUTE_AUTOMATICALLY_EDEFAULT;
		executeAutomaticallyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY,
					oldExecuteAutomatically, EXECUTE_AUTOMATICALLY_EDEFAULT, oldExecuteAutomaticallyESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecuteAutomatically() {
		return executeAutomaticallyESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getExecuteNow() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XTRANSITION;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XTRANSITION__EXECUTE_NOW;

		if (executeNowDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				executeNowDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XTRANSITION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(executeNowDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XTRANSITION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	@Override
	public Command interpreteTransitionAsCommand() {

		System.out.println("running it");

		UpdateRuntime runtime = new UpdateRuntime();

		// TODO: Validate that only one existing object as
		// AbstractNextStateObject in MTransition
		// TODO: validate that updates are not contradicting

		// First create the XFeatureUpdates in the XObjectUpdate, based on what
		// is found in the triggering hierarchy.
		runtime.initUpdates(getAllUpdates());

		// do all updates (as well for to be deleted objects):

		CompoundCommand cmd = runtime.asUpdateCommand(getUpdatedObject());

		// Pessimistic StrictCompoundCommand
		if (getFocusObjects().isEmpty()) {
			return cmd;
		}

		List<EObject> focusedObjects = new BasicEList<EObject>();
		for (XUpdatedObject updatedObj : getFocusObjects()) {
			if (updatedObj.getObject() != null) {
				focusedObjects.add(updatedObj.getObject());
			}
		}

		return new CompoundCommandFocus(cmd.getCommandList(), focusedObjects);

		/*
		 * 
		 * CompoundCommand focus = new CompoundCommand(
		 * CompoundCommand.MERGE_COMMAND_ALL, XTransition.FOCUSCOMMAND);
		 * 
		 * for (Command com : s.getCommandList()) focus.append(com);
		 * 
		 * focus.append(new ChangeDoUndoFocusCommand(focusObjects,
		 * getUpdatedObject())); return focus;
		 */
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setExecuteNow(Boolean newExecuteNow) {

		UpdateRuntime runtime = new UpdateRuntime();

		// First create the XFeatureUpdates in the XObjectUpdate, based on what
		// is found in the triggering hierarchy.
		runtime.initUpdates(getAllUpdates());

		// create all new objects:

		// do all updates (as well for to be deleted objects):

		CompoundCommand cmd = runtime.asUpdateCommand(getUpdatedObject());

		// Changed: executes the commandCompound

		if (!cmd.isEmpty() && cmd.canExecute()) {
			cmd.execute();
		}

		// Delete all to be deleted objects
		// TODO: translate this in deleteions of their containing feature
		// for (MNextStateObjectDefinition abstractNextStateObject :
		// getNextStateDefinition()) {
		// if (abstractNextStateObject.eClass() == UpdatesPackage.eINSTANCE
		// .getMNextStateDeletedObjectDefinition()) {
		// MNextStateDeletedObjectDefinition nextStateDeletedObject =
		// (MNextStateDeletedObjectDefinition) abstractNextStateObject;
		//
		// }
		// }

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Date getExecutionTime() {
		return executionTime;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionTime(Date newExecutionTime) {
		Date oldExecutionTime = executionTime;
		executionTime = newExecutionTime;
		boolean oldExecutionTimeESet = executionTimeESet;
		executionTimeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XTRANSITION__EXECUTION_TIME,
					oldExecutionTime, executionTime, !oldExecutionTimeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecutionTime() {
		Date oldExecutionTime = executionTime;
		boolean oldExecutionTimeESet = executionTimeESet;
		executionTime = EXECUTION_TIME_EDEFAULT;
		executionTimeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XTRANSITION__EXECUTION_TIME,
					oldExecutionTime, EXECUTION_TIME_EDEFAULT, oldExecutionTimeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecutionTime() {
		return executionTimeESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XTriggeredUpdate> getTriggeringUpdate() {
		if (triggeringUpdate == null) {
			triggeringUpdate = new EObjectContainmentEList.Unsettable.Resolving<XTriggeredUpdate>(
					XTriggeredUpdate.class, this, SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE);
		}
		return triggeringUpdate;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTriggeringUpdate() {
		if (triggeringUpdate != null)
			((InternalEList.Unsettable<?>) triggeringUpdate).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTriggeringUpdate() {
		return triggeringUpdate != null && ((InternalEList.Unsettable<?>) triggeringUpdate).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedObject> getUpdatedObject() {
		if (updatedObject == null) {
			updatedObject = new EObjectContainmentEList.Unsettable.Resolving<XUpdatedObject>(XUpdatedObject.class, this,
					SemanticsPackage.XTRANSITION__UPDATED_OBJECT);
		}
		return updatedObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUpdatedObject() {
		if (updatedObject != null)
			((InternalEList.Unsettable<?>) updatedObject).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUpdatedObject() {
		return updatedObject != null && ((InternalEList.Unsettable<?>) updatedObject).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EObject> getDanglingRemovedObject() {
		if (danglingRemovedObject == null) {
			danglingRemovedObject = new EObjectContainmentEList.Unsettable.Resolving<EObject>(EObject.class, this,
					SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT);
		}
		return danglingRemovedObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDanglingRemovedObject() {
		if (danglingRemovedObject != null)
			((InternalEList.Unsettable<?>) danglingRemovedObject).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDanglingRemovedObject() {
		return danglingRemovedObject != null && ((InternalEList.Unsettable<?>) danglingRemovedObject).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdate> getAllUpdates() {
		/**
		 * @OCL self.triggeringUpdate.oclAsType(XUpdate)->union(self.triggeringUpdate.allOwnedUpdates)
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XTRANSITION;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XTRANSITION__ALL_UPDATES;

		if (allUpdatesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				allUpdatesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XTRANSITION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allUpdatesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, SemanticsPackage.Literals.XTRANSITION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<XUpdate> result = (EList<XUpdate>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdatedObject> getFocusObjects() {
		if (focusObjects == null) {
			focusObjects = new EObjectResolvingEList.Unsettable<XUpdatedObject>(XUpdatedObject.class, this,
					SemanticsPackage.XTRANSITION__FOCUS_OBJECTS);
		}
		return focusObjects;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFocusObjects() {
		if (focusObjects != null)
			((InternalEList.Unsettable<?>) focusObjects).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFocusObjects() {
		return focusObjects != null && ((InternalEList.Unsettable<?>) focusObjects).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate executeNow$Update(Boolean trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE.createXTransition();
		java.lang.Boolean triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				SemanticsPackage.eINSTANCE.getXTransition_ExecuteNow(), org.xocl.semantics.XUpdateMode.REDEFINE, null,
				triggerValue, null, null);

		return null;

	}

	protected XUpdate addAbstractUpdateToTriggeringHierarchy() {
		XTriggeredUpdate newAbstractUpdate = SemanticsFactory.eINSTANCE.createXTriggeredUpdate();
		getTriggeringUpdate().add(newAbstractUpdate);
		return newAbstractUpdate;
	};

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
			return ((InternalEList<?>) getTriggeringUpdate()).basicRemove(otherEnd, msgs);
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
			return ((InternalEList<?>) getUpdatedObject()).basicRemove(otherEnd, msgs);
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			return ((InternalEList<?>) getDanglingRemovedObject()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY:
			return getExecuteAutomatically();
		case SemanticsPackage.XTRANSITION__EXECUTE_NOW:
			return getExecuteNow();
		case SemanticsPackage.XTRANSITION__EXECUTION_TIME:
			return getExecutionTime();
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
			return getTriggeringUpdate();
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
			return getUpdatedObject();
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			return getDanglingRemovedObject();
		case SemanticsPackage.XTRANSITION__ALL_UPDATES:
			return getAllUpdates();
		case SemanticsPackage.XTRANSITION__FOCUS_OBJECTS:
			return getFocusObjects();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY:
			setExecuteAutomatically((Boolean) newValue);
			return;
		case SemanticsPackage.XTRANSITION__EXECUTE_NOW:
			setExecuteNow((Boolean) newValue);
			return;
		case SemanticsPackage.XTRANSITION__EXECUTION_TIME:
			setExecutionTime((Date) newValue);
			return;
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
			getTriggeringUpdate().clear();
			getTriggeringUpdate().addAll((Collection<? extends XTriggeredUpdate>) newValue);
			return;
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
			getUpdatedObject().clear();
			getUpdatedObject().addAll((Collection<? extends XUpdatedObject>) newValue);
			return;
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			getDanglingRemovedObject().clear();
			getDanglingRemovedObject().addAll((Collection<? extends EObject>) newValue);
			return;
		case SemanticsPackage.XTRANSITION__FOCUS_OBJECTS:
			getFocusObjects().clear();
			getFocusObjects().addAll((Collection<? extends XUpdatedObject>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY:
			unsetExecuteAutomatically();
			return;
		case SemanticsPackage.XTRANSITION__EXECUTE_NOW:
			setExecuteNow(EXECUTE_NOW_EDEFAULT);
			return;
		case SemanticsPackage.XTRANSITION__EXECUTION_TIME:
			unsetExecutionTime();
			return;
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
			unsetTriggeringUpdate();
			return;
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
			unsetUpdatedObject();
			return;
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			unsetDanglingRemovedObject();
			return;
		case SemanticsPackage.XTRANSITION__FOCUS_OBJECTS:
			unsetFocusObjects();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XTRANSITION__EXECUTE_AUTOMATICALLY:
			return isSetExecuteAutomatically();
		case SemanticsPackage.XTRANSITION__EXECUTE_NOW:
			return EXECUTE_NOW_EDEFAULT == null ? getExecuteNow() != null
					: !EXECUTE_NOW_EDEFAULT.equals(getExecuteNow());
		case SemanticsPackage.XTRANSITION__EXECUTION_TIME:
			return isSetExecutionTime();
		case SemanticsPackage.XTRANSITION__TRIGGERING_UPDATE:
			return isSetTriggeringUpdate();
		case SemanticsPackage.XTRANSITION__UPDATED_OBJECT:
			return isSetUpdatedObject();
		case SemanticsPackage.XTRANSITION__DANGLING_REMOVED_OBJECT:
			return isSetDanglingRemovedObject();
		case SemanticsPackage.XTRANSITION__ALL_UPDATES:
			return !getAllUpdates().isEmpty();
		case SemanticsPackage.XTRANSITION__FOCUS_OBJECTS:
			return isSetFocusObjects();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case SemanticsPackage.XTRANSITION___EXECUTE_NOW$_UPDATE__BOOLEAN:
			return executeNow$Update((Boolean) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (executeAutomatically: ");
		if (executeAutomaticallyESet)
			result.append(executeAutomatically);
		else
			result.append("<unset>");
		result.append(", executionTime: ");
		if (executionTimeESet)
			result.append(executionTime);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel '->'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XTRANSITION);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} // XTransitionImpl
