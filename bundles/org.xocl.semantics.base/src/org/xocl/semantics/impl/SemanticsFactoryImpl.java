/**
 */
package org.xocl.semantics.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.xocl.semantics.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class SemanticsFactoryImpl extends EFactoryImpl implements SemanticsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static SemanticsFactory init() {
		try {
			SemanticsFactory theSemanticsFactory = (SemanticsFactory) EPackage.Registry.INSTANCE
					.getEFactory(SemanticsPackage.eNS_URI);
			if (theSemanticsFactory != null) {
				return theSemanticsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new SemanticsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case SemanticsPackage.XSEMANTICS:
			return createXSemantics();
		case SemanticsPackage.XTRANSITION:
			return createXTransition();
		case SemanticsPackage.XADD_SPECIFICATION:
			return createXAddSpecification();
		case SemanticsPackage.XADDITIONAL_TRIGGERING_SPECIFICATION:
			return createXAdditionalTriggeringSpecification();
		case SemanticsPackage.XTRIGGERED_UPDATE:
			return createXTriggeredUpdate();
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION:
			return createXOriginalTriggeringSpecification();
		case SemanticsPackage.XUPDATED_OBJECT:
			return createXUpdatedObject();
		case SemanticsPackage.XUPDATED_FEATURE:
			return createXUpdatedFeature();
		case SemanticsPackage.XVALUE:
			return createXValue();
		case SemanticsPackage.XUPDATED_OPERATION:
			return createXUpdatedOperation();
		case SemanticsPackage.DUMMY:
			return createDUmmy();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case SemanticsPackage.XADD_UPDATE_MODE:
			return createXAddUpdateModeFromString(eDataType, initialValue);
		case SemanticsPackage.XOBJECT_UPDATE_TYPE:
			return createXObjectUpdateTypeFromString(eDataType, initialValue);
		case SemanticsPackage.XUPDATE_MODE:
			return createXUpdateModeFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case SemanticsPackage.XADD_UPDATE_MODE:
			return convertXAddUpdateModeToString(eDataType, instanceValue);
		case SemanticsPackage.XOBJECT_UPDATE_TYPE:
			return convertXObjectUpdateTypeToString(eDataType, instanceValue);
		case SemanticsPackage.XUPDATE_MODE:
			return convertXUpdateModeToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XSemantics createXSemantics() {
		XSemanticsImpl xSemantics = new XSemanticsImpl();
		return xSemantics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTransition createXTransition() {
		XTransitionImpl xTransition = new XTransitionImpl();
		return xTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAddSpecification createXAddSpecification() {
		XAddSpecificationImpl xAddSpecification = new XAddSpecificationImpl();
		return xAddSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAdditionalTriggeringSpecification createXAdditionalTriggeringSpecification() {
		XAdditionalTriggeringSpecificationImpl xAdditionalTriggeringSpecification = new XAdditionalTriggeringSpecificationImpl();
		return xAdditionalTriggeringSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeredUpdate createXTriggeredUpdate() {
		XTriggeredUpdateImpl xTriggeredUpdate = new XTriggeredUpdateImpl();
		return xTriggeredUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XOriginalTriggeringSpecification createXOriginalTriggeringSpecification() {
		XOriginalTriggeringSpecificationImpl xOriginalTriggeringSpecification = new XOriginalTriggeringSpecificationImpl();
		return xOriginalTriggeringSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject createXUpdatedObject() {
		XUpdatedObjectImpl xUpdatedObject = new XUpdatedObjectImpl();
		return xUpdatedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedFeature createXUpdatedFeature() {
		XUpdatedFeatureImpl xUpdatedFeature = new XUpdatedFeatureImpl();
		return xUpdatedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue createXValue() {
		XValueImpl xValue = new XValueImpl();
		return xValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedOperation createXUpdatedOperation() {
		XUpdatedOperationImpl xUpdatedOperation = new XUpdatedOperationImpl();
		return xUpdatedOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DUmmy createDUmmy() {
		DUmmyImpl dUmmy = new DUmmyImpl();
		return dUmmy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAddUpdateMode createXAddUpdateModeFromString(EDataType eDataType, String initialValue) {
		XAddUpdateMode result = XAddUpdateMode.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertXAddUpdateModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XObjectUpdateType createXObjectUpdateTypeFromString(EDataType eDataType, String initialValue) {
		XObjectUpdateType result = XObjectUpdateType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertXObjectUpdateTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdateMode createXUpdateModeFromString(EDataType eDataType, String initialValue) {
		XUpdateMode result = XUpdateMode.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertXUpdateModeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsPackage getSemanticsPackage() {
		return (SemanticsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static SemanticsPackage getPackage() {
		return SemanticsPackage.eINSTANCE;
	}

} //SemanticsFactoryImpl
