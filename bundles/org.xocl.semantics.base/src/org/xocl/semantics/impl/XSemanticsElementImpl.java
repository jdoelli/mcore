/**
 */
package org.xocl.semantics.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XSemanticsElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XSemantics Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XSemanticsElementImpl#getKindLabel <em>Kind Label</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XSemanticsElementImpl extends MinimalEObjectImpl.Container implements XSemanticsElement {
	/**
	 * The default value of the '{@link #getKindLabel() <em>Kind Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String KIND_LABEL_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #renderedKindLabel <em>Rendered Kind Label</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #renderedKindLabel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression renderedKindLabelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentLevel <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringOrMissing <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XSemanticsElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XSEMANTICS_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getKindLabel() {
		/**
		 * @OCL 'TODO'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XSEMANTICS_ELEMENT;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String renderedKindLabel() {

		/**
		 * @OCL let e: String = indentationSpaces().concat(kindLabel.toUpper()) in 
		if e.oclIsInvalid() then null else e endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (SemanticsPackage.Literals.XSEMANTICS_ELEMENT);
		EOperation eOperation = SemanticsPackage.Literals.XSEMANTICS_ELEMENT.getEOperations().get(0);
		if (renderedKindLabelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				renderedKindLabelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, body, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(renderedKindLabelBodyOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer indentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(XSemanticsElement)
		then eContainer().oclAsType(XSemanticsElement).indentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (SemanticsPackage.Literals.XSEMANTICS_ELEMENT);
		EOperation eOperation = SemanticsPackage.Literals.XSEMANTICS_ELEMENT.getEOperations().get(1);
		if (indentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, body, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		indentationSpaces(numberOfSpaces)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (SemanticsPackage.Literals.XSEMANTICS_ELEMENT);
		EOperation eOperation = SemanticsPackage.Literals.XSEMANTICS_ELEMENT.getEOperations().get(2);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, body, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer = let e: Integer = size - 1 in 
		if e.oclIsInvalid() then null else e endif in
		if (let e: Boolean = size < 1 in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then ''
		else (let e: String = indentationSpaces(sizeMinusOne).concat(' ') in 
		if e.oclIsInvalid() then null else e endif)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (SemanticsPackage.Literals.XSEMANTICS_ELEMENT);
		EOperation eOperation = SemanticsPackage.Literals.XSEMANTICS_ELEMENT.getEOperations().get(3);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, body, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String stringOrMissing(String p) {

		/**
		 * @OCL if p=null then 'MISSING' else if p.trim()='' then 'MISSING' else p endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (SemanticsPackage.Literals.XSEMANTICS_ELEMENT);
		EOperation eOperation = SemanticsPackage.Literals.XSEMANTICS_ELEMENT.getEOperations().get(4);
		if (stringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, body, helper.getProblems(),
						SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XSEMANTICS_ELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XSEMANTICS_ELEMENT__KIND_LABEL:
			return getKindLabel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XSEMANTICS_ELEMENT__KIND_LABEL:
			return KIND_LABEL_EDEFAULT == null ? getKindLabel() != null : !KIND_LABEL_EDEFAULT.equals(getKindLabel());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case SemanticsPackage.XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL:
			return renderedKindLabel();
		case SemanticsPackage.XSEMANTICS_ELEMENT___INDENT_LEVEL:
			return indentLevel();
		case SemanticsPackage.XSEMANTICS_ELEMENT___INDENTATION_SPACES:
			return indentationSpaces();
		case SemanticsPackage.XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER:
			return indentationSpaces((Integer) arguments.get(0));
		case SemanticsPackage.XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING:
			return stringOrMissing((String) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //XSemanticsElementImpl
