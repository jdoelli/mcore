/**
 */
package org.xocl.semantics.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XTriggeredUpdate;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateContainer;
import org.xocl.semantics.XAddSpecification;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XObjectUpdateType;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdateMode;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XAbstract Update Container</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XUpdateContainerImpl#getContainingTransition <em>Containing Transition</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XUpdateContainerImpl extends XSemanticsElementImpl implements XUpdateContainer {
	/**
	 * The parsed OCL expression for the body of the '{@link #createNextStateNewObject <em>Create Next State New Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #createNextStateNewObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression createNextStateNewObjectecoreEClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #nextStateObjectDefinitionFromObject <em>Next State Object Definition From Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #nextStateObjectDefinitionFromObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression nextStateObjectDefinitionFromObjectecoreEObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #deleteObjectInNextState <em>Delete Object In Next State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #deleteObjectInNextState
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression deleteObjectInNextStateecoreEObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addReferenceUpdate <em>Add Reference Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addReferenceUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addReferenceUpdateecoreEObjectecoreEReferencesemanticsXUpdateModesemanticsXAddUpdateModeecoreEJavaObjectsemanticsXUpdatedObjectsemanticsXUpdatedObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addReferenceUpdate <em>Add Reference Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addReferenceUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addReferenceUpdateecoreEObjectecoreEReferencesemanticsXUpdateModesemanticsXAddUpdateModeecoreEJavaObjectsemanticsXUpdatedObjectsemanticsXUpdatedObjectecoreEStringecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addAndMergeUpdate <em>Add And Merge Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addAndMergeUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addAndMergeUpdatesemanticsXUpdateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addAttributeUpdate <em>Add Attribute Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addAttributeUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addAttributeUpdateecoreEObjectecoreEAttributesemanticsXUpdateModesemanticsXAddUpdateModeecoreEJavaObjectsemanticsXUpdatedObjectsemanticsXUpdatedObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #addAttributeUpdate <em>Add Attribute Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #addAttributeUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression addAttributeUpdateecoreEObjectecoreEAttributesemanticsXUpdateModesemanticsXAddUpdateModeecoreEJavaObjectsemanticsXUpdatedObjectsemanticsXUpdatedObjectecoreEStringecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeOperationCall <em>Invoke Operation Call</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #invokeOperationCall
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeOperationCallecoreEObjectecoreEOperationecoreEJavaObjectsemanticsXUpdatedObjectsemanticsXUpdatedObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingTransition <em>Containing Transition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingTransition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingTransitionDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XUpdateContainerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XUPDATE_CONTAINER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTransition getContainingTransition() {
		XTransition containingTransition = basicGetContainingTransition();
		return containingTransition != null && containingTransition.eIsProxy()
				? (XTransition) eResolveProxy((InternalEObject) containingTransition) : containingTransition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTransition basicGetContainingTransition() {
		/**
		 * @OCL if self.oclIsKindOf(XTransition)
		then self.oclAsType(XTransition) 
		else self.eContainer().oclAsType(XUpdateContainer).containingTransition
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = SemanticsPackage.Literals.XUPDATE_CONTAINER;
		EStructuralFeature eFeature = SemanticsPackage.Literals.XUPDATE_CONTAINER__CONTAINING_TRANSITION;

		if (containingTransitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				containingTransitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(),
						SemanticsPackage.Literals.XUPDATE_CONTAINER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingTransitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query,
					SemanticsPackage.Literals.XUPDATE_CONTAINER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			XTransition result = (XTransition) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 * 
	 */
	public EObject createNextStateNewObject(EClass typeOfNewObject) {

		XUpdate update = addAbstractUpdateToTriggeringHierarchy();

		EObject newObject = typeOfNewObject.getEPackage().getEFactoryInstance().create(typeOfNewObject);
		// remind it as created object
		update.setObject(newObject);

		if (newObject instanceof IXoclInitializable)
			((IXoclInitializable) newObject).ensureClassInitialized(false);

		return newObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdatedObject nextStateObjectDefinitionFromObject(EObject currentStateObject) {

		for (XUpdatedObject updatedObject : getContainingTransition().getUpdatedObject()) {
			if (updatedObject.getObject() == currentStateObject) {
				return updatedObject;
			}
		}
		XUpdatedObject newUpdatedObject = SemanticsFactory.eINSTANCE.createXUpdatedObject();
		getContainingTransition().getUpdatedObject().add(newUpdatedObject);
		newUpdatedObject.setObject(currentStateObject);
		newUpdatedObject.setObjectUpdateType(XObjectUpdateType.KEEP);
		return newUpdatedObject;

	}

	/**
	 * @return
	 * creates either a XTriggeringUpdate or a XTriggeredUpdate and adds it in the triggering hieararchy
	 */
	protected abstract XUpdate addAbstractUpdateToTriggeringHierarchy();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate addReferenceUpdate(EObject updatedObject, EReference updatedReference, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object targetObject, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint) {

		if (updatedObject == null || updatedReference == null)
			return null;

		XUpdate newAbstractUpdate;
		if (targetObject == null)
			newAbstractUpdate = addUpdate(this.nextStateObjectDefinitionFromObject(updatedObject), updatedReference,
					XUpdateMode.REMOVE, addMode);

		else
			newAbstractUpdate = addUpdate(this.nextStateObjectDefinitionFromObject(updatedObject), updatedReference,
					updateMode, addMode);

		ArrayList<EObject> listOfEObj = new ArrayList<EObject>();

		if (targetObject instanceof List) {
			List<?> genericListOfObj = (List<?>) targetObject;

			for (Object genericObj : genericListOfObj)
				if (genericObj instanceof EObject)
					listOfEObj.add((EObject) genericObj);
		} else if (targetObject instanceof EObject)
			listOfEObj.add((EObject) targetObject);

		for (EObject trg : listOfEObj) {
			newAbstractUpdate.getValue().getUpdatedObjectValue().add(this.nextStateObjectDefinitionFromObject(trg));

		}

		return newAbstractUpdate;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate addReferenceUpdate(EObject updatedObject, EReference updatedReference, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object targetObject, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint, String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot, String alternativePersistenceReference) {

		XUpdate newAbstractUpdate = this.addReferenceUpdate(updatedObject, updatedReference, updateMode, addMode,
				targetObject, afterObjectConstraint, beforeObjectConstraint);

		if (newAbstractUpdate.getUpdatedObject() != null) {
			if (persistenceLocation != null)
				newAbstractUpdate.getUpdatedObject().setPersistenceLocation(persistenceLocation);
			if (alternativePersistencePackage != null)
				newAbstractUpdate.getUpdatedObject().setAlternativePersistencePackage(alternativePersistencePackage);
			if (alternativePersistenceReference != null)
				newAbstractUpdate.getUpdatedObject()
						.setAlternativePersistenceReference(alternativePersistenceReference);
			if (alternativePersistenceRoot != null)
				newAbstractUpdate.getUpdatedObject().setAlternativePersistenceRoot(alternativePersistenceRoot);
		}

		return newAbstractUpdate;

	}

	/**
	 * Core of all add methods.
	 * @param updatedObject
	 * @param updatedFeature
	 * @param updateMode
	 * @param addMode
	 * @return
	 */
	private XUpdate addUpdate(XUpdatedObject updatedObject, ETypedElement updatedFeature, XUpdateMode updateMode,
			XAddUpdateMode addMode) {

		XUpdate newUpdate = addAbstractUpdateToTriggeringHierarchy();
		newUpdate.setUpdatedObject(updatedObject);

		if (updatedFeature instanceof EStructuralFeature)
			newUpdate.setFeature((EStructuralFeature) updatedFeature);
		else {
			newUpdate.setOperation((EOperation) updatedFeature);
		}
		newUpdate.setMode(updateMode);
		if (addMode != null) {
			XAddSpecification addSpec = SemanticsFactory.eINSTANCE.createXAddSpecification();
			addSpec.setAddKind(addMode);
			newUpdate.setAddSpecification(addSpec);
		}
		XValue targetAsXValue = SemanticsFactory.eINSTANCE.createXValue();
		//targetValueAsXValue.getStringValue().add(targetValue);
		newUpdate.setValue(targetAsXValue);
		return newUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 * 
	 * This needs to do the following:
	 * - create a XUpdatedObject with XObjectUpdateType Delete
	 * - find the container of this object, and add an XUpdate with mode REMOVE to it.
	 * - THEORETICALLY calculate all resulting updates, from deleting this object, and add them to the transition, being triggered by the XUpdate that removed the object from the containment hiearchy
	 * - THEORETICALLY find out inconsistencies, if other updates are adding it to containment hierarchy again. 
	 */
	public Boolean deleteObjectInNextState(EObject objectToBeDeleted) {

		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate addAndMergeUpdate(XUpdate updateToBeMerged) {
		//this update container gets added an update resulting from another derived/changeable feature triggering updates.
		//IMPORTANT: this other update is part of another transition, thus its referenced updated objects need to be merged.
		//it takes the update to be merged, moves it into this update container, and moves/merges all referenced updated objects
		//then this method is called recursively on this added update, with its triggered updates as argument.

		//This takes another update, which is included in another transition.
		//1. move this update into triggered updates of this update container
		//addAbstractUpdateToTriggeringHierarchy(xUpdate);
		if (this instanceof XTransition) {
			XTransition thisTransition = (XTransition) this;
			thisTransition.getTriggeringUpdate().add((XTriggeredUpdate) updateToBeMerged);
		} else if (this instanceof XAbstractTriggeringUpdate) {
			XAbstractTriggeringUpdate thisTriggeringUpdate = (XAbstractTriggeringUpdate) this;
			thisTriggeringUpdate.getTriggeredOwnedUpdate().add((XTriggeredUpdate) updateToBeMerged);
		}
		//2. merge all XUpdatedObject of updateToBeMerged into this transition
		XTransition thisTransition = this.getContainingTransition();
		// for keep, merge
		// for delete, merge, and put keep if one of them is keep
		// for new, add them
		//1. merge object being updated.
		XUpdatedObject objectBeingUpdatedToBeMerged = updateToBeMerged.getUpdatedObject();
		XUpdatedObject correspondingObjectBeingUpdatedOfThisTransition = null;
		for (XUpdatedObject objectOfThisTransition : thisTransition.getUpdatedObject()) {
			if (objectBeingUpdatedToBeMerged.getObject() == objectOfThisTransition.getObject()) {
				correspondingObjectBeingUpdatedOfThisTransition = objectOfThisTransition;
				break;
			}
		}
		if (correspondingObjectBeingUpdatedOfThisTransition == null) {
			thisTransition.getUpdatedObject().add(objectBeingUpdatedToBeMerged);
			correspondingObjectBeingUpdatedOfThisTransition = objectBeingUpdatedToBeMerged;
		}
		//take over DELETE status
		if (objectBeingUpdatedToBeMerged.getObjectUpdateType() == XObjectUpdateType.DELETE) {
			//DELETE, ?
			//set this object to be deleted
			correspondingObjectBeingUpdatedOfThisTransition.setObjectUpdateType(XObjectUpdateType.DELETE);
		}
		if (correspondingObjectBeingUpdatedOfThisTransition != objectBeingUpdatedToBeMerged) {
			//replace objectBeingUpdatedToBeMerged with correspondingObjectBeingUpdatedOfThisTransition
			updateToBeMerged.setUpdatedObject(correspondingObjectBeingUpdatedOfThisTransition);

		}
		//2. merge all objects being the new values of the update
		for (XUpdatedObject objectToBeMerged : updateToBeMerged.getValue().getUpdatedObjectValue()) {
			XUpdatedObject correspondingObjectOfThisTransition = null;
			for (XUpdatedObject objectOfThisTransition : thisTransition.getUpdatedObject()) {
				if (objectToBeMerged.getObject() == objectOfThisTransition.getObject()) {
					correspondingObjectOfThisTransition = objectOfThisTransition;
					break;
				}
			}
			if (correspondingObjectOfThisTransition == null) {
				thisTransition.getUpdatedObject().add(objectToBeMerged);
				correspondingObjectOfThisTransition = objectToBeMerged;
			}
			//take over DELETE status
			if (objectToBeMerged.getObjectUpdateType() == XObjectUpdateType.DELETE) {
				//DELETE, ?
				//set this object to be deleted
				correspondingObjectOfThisTransition.setObjectUpdateType(XObjectUpdateType.DELETE);
			}
			if (correspondingObjectOfThisTransition != objectToBeMerged) {
				//replace objectToBeMerged with correspondingObjectOfThisTransition
				EList<XUpdatedObject> l = updateToBeMerged.getValue().getUpdatedObjectValue();
				int pos = l.indexOf(objectToBeMerged);
				l.remove(objectToBeMerged);
				l.add(pos, correspondingObjectOfThisTransition);
			}

		}
		//3. recursively call this method, for all updates triggered by updateToBeMerged
		for (XUpdate updateTriggeredByUpdateToBeMerged : ((XAbstractTriggeringUpdate) updateToBeMerged)
				.getTriggeredOwnedUpdate()) {
			updateToBeMerged.addAndMergeUpdate(updateTriggeredByUpdateToBeMerged);
		}
		return updateToBeMerged;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@SuppressWarnings("unchecked")
	public XUpdate addAttributeUpdate(EObject updatedObject, EAttribute updatedAttribute, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object value, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint) {
		//Value is null is WRONG; UNSET Attribute, rather than ignore
		if (updatedObject == null || updatedAttribute == null)
			return null;

		XUpdate newAbstractUpdate;
		if (value == null)
			newAbstractUpdate = addUpdate(this.nextStateObjectDefinitionFromObject(updatedObject), updatedAttribute,
					XUpdateMode.REMOVE, addMode);
		else
			newAbstractUpdate = addUpdate(this.nextStateObjectDefinitionFromObject(updatedObject), updatedAttribute,
					updateMode, addMode);

		ArrayList<Object> listOfObj = new ArrayList<Object>();

		if (value != null)
			if (value instanceof List) {
				List<?> genericListOfObj = (List<?>) value;

				for (Object genericObj : genericListOfObj)
					if (genericObj instanceof Object)
						listOfObj.add((Object) genericObj);
			}

			else if (value instanceof Object)
				listOfObj.add((Object) value);

		if (!listOfObj.isEmpty()) {
			Object firstElement = listOfObj.get(0);
			ArrayList<? extends Object> allValues = listOfObj;
			if (firstElement instanceof String)
				newAbstractUpdate.getValue().getStringValue().addAll((Collection<? extends String>) allValues);

			else if (firstElement instanceof Boolean)
				newAbstractUpdate.getValue().getBooleanValue().addAll(((Collection<? extends Boolean>) allValues));
			else if (firstElement instanceof EEnumLiteral)
				newAbstractUpdate.getValue().getLiteralValue().addAll((Collection<? extends EEnumLiteral>) allValues);
			else if (firstElement instanceof Integer)
				newAbstractUpdate.getValue().getIntegerValue().addAll((Collection<? extends Integer>) allValues);
			else if (firstElement instanceof Date)
				newAbstractUpdate.getValue().getDateValue().addAll((Collection<? extends Date>) allValues);
			else if (firstElement instanceof Double)
				newAbstractUpdate.getValue().getDoubleValue().addAll((Collection<? extends Double>) allValues);

		}

		return newAbstractUpdate;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate addAttributeUpdate(EObject updatedObject, EAttribute updatedAttribute, XUpdateMode updateMode,
			XAddUpdateMode addMode, Object value, XUpdatedObject afterObjectConstraint,
			XUpdatedObject beforeObjectConstraint, String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot, String alternativePersistenceReference) {

		XUpdate newAbstractUpdate = this.addAttributeUpdate(updatedObject, updatedAttribute, updateMode, addMode, value,
				afterObjectConstraint, beforeObjectConstraint);

		if (newAbstractUpdate.getUpdatedObject() != null) {
			if (persistenceLocation != null)
				newAbstractUpdate.getUpdatedObject().setPersistenceLocation(persistenceLocation);
			if (alternativePersistencePackage != null)
				newAbstractUpdate.getUpdatedObject().setAlternativePersistencePackage(alternativePersistencePackage);
			if (alternativePersistenceReference != null)
				newAbstractUpdate.getUpdatedObject()
						.setAlternativePersistenceReference(alternativePersistenceReference);
			if (alternativePersistenceRoot != null)
				newAbstractUpdate.getUpdatedObject().setAlternativePersistenceRoot(alternativePersistenceRoot);
		}

		return newAbstractUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate invokeOperationCall(EObject updatedObject, EOperation operation, Object parameters,
			XUpdatedObject afterObjectConstraint, XUpdatedObject beforeObjectConstraint) {

		XUpdate newAbstractUpdate = addUpdate(this.nextStateObjectDefinitionFromObject(updatedObject), operation, null,
				null);

		ArrayList<Object> listOfObj = new ArrayList<Object>();

		if (parameters instanceof List) {
			List<?> genericListOfObj = (List<?>) parameters;

			for (Object genericObj : genericListOfObj)
				if (genericObj instanceof Object)
					listOfObj.add((Object) genericObj);
		}

		else if (parameters instanceof Object) {
			listOfObj.add((Object) parameters);
		}

		for (Object trg : listOfObj) {
			newAbstractUpdate.getValue().getParameterArguments().add(trg);
		}

		return newAbstractUpdate;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE_CONTAINER__CONTAINING_TRANSITION:
			if (resolve)
				return getContainingTransition();
			return basicGetContainingTransition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE_CONTAINER__CONTAINING_TRANSITION:
			return basicGetContainingTransition() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case SemanticsPackage.XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS:
			return createNextStateNewObject((EClass) arguments.get(0));
		case SemanticsPackage.XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT:
			return nextStateObjectDefinitionFromObject((EObject) arguments.get(0));
		case SemanticsPackage.XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT:
			return deleteObjectInNextState((EObject) arguments.get(0));
		case SemanticsPackage.XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT:
			return addReferenceUpdate((EObject) arguments.get(0), (EReference) arguments.get(1),
					(XUpdateMode) arguments.get(2), (XAddUpdateMode) arguments.get(3), arguments.get(4),
					(XUpdatedObject) arguments.get(5), (XUpdatedObject) arguments.get(6));
		case SemanticsPackage.XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING:
			return addReferenceUpdate((EObject) arguments.get(0), (EReference) arguments.get(1),
					(XUpdateMode) arguments.get(2), (XAddUpdateMode) arguments.get(3), arguments.get(4),
					(XUpdatedObject) arguments.get(5), (XUpdatedObject) arguments.get(6), (String) arguments.get(7),
					(String) arguments.get(8), (String) arguments.get(9), (String) arguments.get(10));
		case SemanticsPackage.XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE:
			return addAndMergeUpdate((XUpdate) arguments.get(0));
		case SemanticsPackage.XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT:
			return addAttributeUpdate((EObject) arguments.get(0), (EAttribute) arguments.get(1),
					(XUpdateMode) arguments.get(2), (XAddUpdateMode) arguments.get(3), arguments.get(4),
					(XUpdatedObject) arguments.get(5), (XUpdatedObject) arguments.get(6));
		case SemanticsPackage.XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING:
			return addAttributeUpdate((EObject) arguments.get(0), (EAttribute) arguments.get(1),
					(XUpdateMode) arguments.get(2), (XAddUpdateMode) arguments.get(3), arguments.get(4),
					(XUpdatedObject) arguments.get(5), (XUpdatedObject) arguments.get(6), (String) arguments.get(7),
					(String) arguments.get(8), (String) arguments.get(9), (String) arguments.get(10));
		case SemanticsPackage.XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT:
			return invokeOperationCall((EObject) arguments.get(0), (EOperation) arguments.get(1), arguments.get(2),
					(XUpdatedObject) arguments.get(3), (XUpdatedObject) arguments.get(4));
		}
		return super.eInvoke(operationID, arguments);
	}

} //XUpdateContainerImpl
