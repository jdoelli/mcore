/**
 */
package org.xocl.semantics.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAbstractTriggeringUpdate;
import org.xocl.semantics.XOriginalTriggeringSpecification;
import org.xocl.semantics.XTriggeredUpdate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XOriginal Triggering Specification</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XOriginalTriggeringSpecificationImpl#getContainingTriggeredUpdate <em>Containing Triggered Update</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class XOriginalTriggeringSpecificationImpl extends XTriggeringSpecificationImpl
		implements XOriginalTriggeringSpecification {

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTriggeringUpdate <em>Triggering Update</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTriggeringUpdate
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression triggeringUpdateDeriveOCL;
	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";
	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XOriginalTriggeringSpecificationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XORIGINAL_TRIGGERING_SPECIFICATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeredUpdate getContainingTriggeredUpdate() {
		if (eContainerFeatureID() != SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE)
			return null;
		return (XTriggeredUpdate) eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XTriggeredUpdate basicGetContainingTriggeredUpdate() {
		if (eContainerFeatureID() != SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE)
			return null;
		return (XTriggeredUpdate) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingTriggeredUpdate(XTriggeredUpdate newContainingTriggeredUpdate,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContainingTriggeredUpdate,
				SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingTriggeredUpdate(XTriggeredUpdate newContainingTriggeredUpdate) {
		if (newContainingTriggeredUpdate != eInternalContainer()
				|| (eContainerFeatureID() != SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE
						&& newContainingTriggeredUpdate != null)) {
			if (EcoreUtil.isAncestor(this, newContainingTriggeredUpdate))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingTriggeredUpdate != null)
				msgs = ((InternalEObject) newContainingTriggeredUpdate).eInverseAdd(this,
						SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, XTriggeredUpdate.class, msgs);
			msgs = basicSetContainingTriggeredUpdate(newContainingTriggeredUpdate, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE,
					newContainingTriggeredUpdate, newContainingTriggeredUpdate));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingTriggeredUpdate((XTriggeredUpdate) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			return basicSetContainingTriggeredUpdate(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			return eInternalContainer().eInverseRemove(this,
					SemanticsPackage.XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS, XTriggeredUpdate.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			if (resolve)
				return getContainingTriggeredUpdate();
			return basicGetContainingTriggeredUpdate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			setContainingTriggeredUpdate((XTriggeredUpdate) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			setContainingTriggeredUpdate((XTriggeredUpdate) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE:
			return basicGetContainingTriggeredUpdate() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL triggeringUpdate if containingTriggeredUpdate.oclIsUndefined()
	then null
	else containingTriggeredUpdate.originallyTriggeringUpdate
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public XAbstractTriggeringUpdate basicGetTriggeringUpdate() {
		EClass eClass = (SemanticsPackage.Literals.XORIGINAL_TRIGGERING_SPECIFICATION);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE;

		if (triggeringUpdateDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				triggeringUpdateDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(triggeringUpdateDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (XAbstractTriggeringUpdate) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Original Triggering'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XORIGINAL_TRIGGERING_SPECIFICATION);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} //XOriginalTriggeringSpecificationImpl
