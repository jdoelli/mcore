
/**
 */
package org.xocl.semantics.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdatedTypedElement;
import org.xocl.semantics.XValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XUpdated Typed Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getContributingUpdate <em>Contributing Update</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getExpectedResultingValue <em>Expected Resulting Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getExpectationsMet <em>Expectations Met</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getOldValue <em>Old Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getNewValue <em>New Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getInconsistent <em>Inconsistent</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getExecuted <em>Executed</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getTowardsEndInsertValues <em>Towards End Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getAsFirstInsertValues <em>As First Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getAsLastInsertValues <em>As Last Insert Values</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl#getDeleteValues <em>Delete Values</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XUpdatedTypedElementImpl extends MinimalEObjectImpl.Container implements XUpdatedTypedElement {
	/**
	 * The cached value of the '{@link #getContributingUpdate() <em>Contributing Update</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributingUpdate()
	 * @generated
	 * @ordered
	 */
	protected EList<XUpdate> contributingUpdate;

	/**
	 * The cached value of the '{@link #getExpectedToBeAddedValue() <em>Expected To Be Added Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedToBeAddedValue()
	 * @generated
	 * @ordered
	 */
	protected XValue expectedToBeAddedValue;

	/**
	 * This is true if the Expected To Be Added Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expectedToBeAddedValueESet;

	/**
	 * The cached value of the '{@link #getExpectedToBeRemovedValue() <em>Expected To Be Removed Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedToBeRemovedValue()
	 * @generated
	 * @ordered
	 */
	protected XValue expectedToBeRemovedValue;

	/**
	 * This is true if the Expected To Be Removed Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expectedToBeRemovedValueESet;

	/**
	 * The cached value of the '{@link #getExpectedResultingValue() <em>Expected Resulting Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedResultingValue()
	 * @generated
	 * @ordered
	 */
	protected XValue expectedResultingValue;

	/**
	 * This is true if the Expected Resulting Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expectedResultingValueESet;

	/**
	 * The default value of the '{@link #getExpectationsMet() <em>Expectations Met</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectationsMet()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean EXPECTATIONS_MET_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExpectationsMet() <em>Expectations Met</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectationsMet()
	 * @generated
	 * @ordered
	 */
	protected Boolean expectationsMet = EXPECTATIONS_MET_EDEFAULT;

	/**
	 * This is true if the Expectations Met attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expectationsMetESet;

	/**
	 * The cached value of the '{@link #getOldValue() <em>Old Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOldValue()
	 * @generated
	 * @ordered
	 */
	protected XValue oldValue;

	/**
	 * This is true if the Old Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean oldValueESet;

	/**
	 * The cached value of the '{@link #getNewValue() <em>New Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNewValue()
	 * @generated
	 * @ordered
	 */
	protected XValue newValue;

	/**
	 * This is true if the New Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean newValueESet;

	/**
	 * The default value of the '{@link #getInconsistent() <em>Inconsistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInconsistent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean INCONSISTENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInconsistent() <em>Inconsistent</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInconsistent()
	 * @generated
	 * @ordered
	 */
	protected Boolean inconsistent = INCONSISTENT_EDEFAULT;

	/**
	 * This is true if the Inconsistent attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean inconsistentESet;

	/**
	 * The default value of the '{@link #getExecuted() <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecuted()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean EXECUTED_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExecuted() <em>Executed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExecuted()
	 * @generated
	 * @ordered
	 */
	protected Boolean executed = EXECUTED_EDEFAULT;

	/**
	 * This is true if the Executed attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean executedESet;

	/**
	 * The default value of the '{@link #getTowardsEndInsertPosition() <em>Towards End Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsEndInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Integer TOWARDS_END_INSERT_POSITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTowardsEndInsertPosition() <em>Towards End Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsEndInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected Integer towardsEndInsertPosition = TOWARDS_END_INSERT_POSITION_EDEFAULT;

	/**
	 * This is true if the Towards End Insert Position attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean towardsEndInsertPositionESet;

	/**
	 * The cached value of the '{@link #getTowardsEndInsertValues() <em>Towards End Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsEndInsertValues()
	 * @generated
	 * @ordered
	 */
	protected XValue towardsEndInsertValues;

	/**
	 * This is true if the Towards End Insert Values containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean towardsEndInsertValuesESet;

	/**
	 * The default value of the '{@link #getInTheMiddleInsertPosition() <em>In The Middle Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInTheMiddleInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Integer IN_THE_MIDDLE_INSERT_POSITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInTheMiddleInsertPosition() <em>In The Middle Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInTheMiddleInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected Integer inTheMiddleInsertPosition = IN_THE_MIDDLE_INSERT_POSITION_EDEFAULT;

	/**
	 * This is true if the In The Middle Insert Position attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean inTheMiddleInsertPositionESet;

	/**
	 * The cached value of the '{@link #getInTheMiddleInsertValues() <em>In The Middle Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInTheMiddleInsertValues()
	 * @generated
	 * @ordered
	 */
	protected XValue inTheMiddleInsertValues;

	/**
	 * This is true if the In The Middle Insert Values containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean inTheMiddleInsertValuesESet;

	/**
	 * The default value of the '{@link #getTowardsBeginningInsertPosition() <em>Towards Beginning Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsBeginningInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected static final Integer TOWARDS_BEGINNING_INSERT_POSITION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTowardsBeginningInsertPosition() <em>Towards Beginning Insert Position</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsBeginningInsertPosition()
	 * @generated
	 * @ordered
	 */
	protected Integer towardsBeginningInsertPosition = TOWARDS_BEGINNING_INSERT_POSITION_EDEFAULT;

	/**
	 * This is true if the Towards Beginning Insert Position attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean towardsBeginningInsertPositionESet;

	/**
	 * The cached value of the '{@link #getTowardsBeginningInsertValues() <em>Towards Beginning Insert Values</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTowardsBeginningInsertValues()
	 * @generated
	 * @ordered
	 */
	protected XValue towardsBeginningInsertValues;

	/**
	 * This is true if the Towards Beginning Insert Values reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean towardsBeginningInsertValuesESet;

	/**
	 * The cached value of the '{@link #getAsFirstInsertValues() <em>As First Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsFirstInsertValues()
	 * @generated
	 * @ordered
	 */
	protected XValue asFirstInsertValues;

	/**
	 * This is true if the As First Insert Values containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean asFirstInsertValuesESet;

	/**
	 * The cached value of the '{@link #getAsLastInsertValues() <em>As Last Insert Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsLastInsertValues()
	 * @generated
	 * @ordered
	 */
	protected XValue asLastInsertValues;

	/**
	 * This is true if the As Last Insert Values containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean asLastInsertValuesESet;

	/**
	 * The cached value of the '{@link #getDeleteValues() <em>Delete Values</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDeleteValues()
	 * @generated
	 * @ordered
	 */
	protected XValue deleteValues;

	/**
	 * This is true if the Delete Values containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean deleteValuesESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XUpdatedTypedElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XUPDATED_TYPED_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<XUpdate> getContributingUpdate() {
		if (contributingUpdate == null) {
			contributingUpdate = new EObjectWithInverseResolvingEList.Unsettable<XUpdate>(XUpdate.class, this,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE,
					SemanticsPackage.XUPDATE__CONTRIBUTES_TO);
		}
		return contributingUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContributingUpdate() {
		if (contributingUpdate != null)
			((InternalEList.Unsettable<?>) contributingUpdate).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContributingUpdate() {
		return contributingUpdate != null && ((InternalEList.Unsettable<?>) contributingUpdate).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getExpectedToBeAddedValue() {
		if (expectedToBeAddedValue != null && expectedToBeAddedValue.eIsProxy()) {
			InternalEObject oldExpectedToBeAddedValue = (InternalEObject) expectedToBeAddedValue;
			expectedToBeAddedValue = (XValue) eResolveProxy(oldExpectedToBeAddedValue);
			if (expectedToBeAddedValue != oldExpectedToBeAddedValue) {
				InternalEObject newExpectedToBeAddedValue = (InternalEObject) expectedToBeAddedValue;
				NotificationChain msgs = oldExpectedToBeAddedValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE,
						null, null);
				if (newExpectedToBeAddedValue.eInternalContainer() == null) {
					msgs = newExpectedToBeAddedValue.eInverseAdd(this, EOPPOSITE_FEATURE_BASE
							- SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE,
							oldExpectedToBeAddedValue, expectedToBeAddedValue));
			}
		}
		return expectedToBeAddedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetExpectedToBeAddedValue() {
		return expectedToBeAddedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpectedToBeAddedValue(XValue newExpectedToBeAddedValue, NotificationChain msgs) {
		XValue oldExpectedToBeAddedValue = expectedToBeAddedValue;
		expectedToBeAddedValue = newExpectedToBeAddedValue;
		boolean oldExpectedToBeAddedValueESet = expectedToBeAddedValueESet;
		expectedToBeAddedValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, oldExpectedToBeAddedValue,
					newExpectedToBeAddedValue, !oldExpectedToBeAddedValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectedToBeAddedValue(XValue newExpectedToBeAddedValue) {
		if (newExpectedToBeAddedValue != expectedToBeAddedValue) {
			NotificationChain msgs = null;
			if (expectedToBeAddedValue != null)
				msgs = ((InternalEObject) expectedToBeAddedValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE,
						null, msgs);
			if (newExpectedToBeAddedValue != null)
				msgs = ((InternalEObject) newExpectedToBeAddedValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE,
						null, msgs);
			msgs = basicSetExpectedToBeAddedValue(newExpectedToBeAddedValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedToBeAddedValueESet = expectedToBeAddedValueESet;
			expectedToBeAddedValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, newExpectedToBeAddedValue,
						newExpectedToBeAddedValue, !oldExpectedToBeAddedValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpectedToBeAddedValue(NotificationChain msgs) {
		XValue oldExpectedToBeAddedValue = expectedToBeAddedValue;
		expectedToBeAddedValue = null;
		boolean oldExpectedToBeAddedValueESet = expectedToBeAddedValueESet;
		expectedToBeAddedValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, oldExpectedToBeAddedValue,
					null, oldExpectedToBeAddedValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpectedToBeAddedValue() {
		if (expectedToBeAddedValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expectedToBeAddedValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, null,
					msgs);
			msgs = basicUnsetExpectedToBeAddedValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedToBeAddedValueESet = expectedToBeAddedValueESet;
			expectedToBeAddedValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE, null, null,
						oldExpectedToBeAddedValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpectedToBeAddedValue() {
		return expectedToBeAddedValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getExpectedToBeRemovedValue() {
		if (expectedToBeRemovedValue != null && expectedToBeRemovedValue.eIsProxy()) {
			InternalEObject oldExpectedToBeRemovedValue = (InternalEObject) expectedToBeRemovedValue;
			expectedToBeRemovedValue = (XValue) eResolveProxy(oldExpectedToBeRemovedValue);
			if (expectedToBeRemovedValue != oldExpectedToBeRemovedValue) {
				InternalEObject newExpectedToBeRemovedValue = (InternalEObject) expectedToBeRemovedValue;
				NotificationChain msgs = oldExpectedToBeRemovedValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
						null, null);
				if (newExpectedToBeRemovedValue.eInternalContainer() == null) {
					msgs = newExpectedToBeRemovedValue.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
							oldExpectedToBeRemovedValue, expectedToBeRemovedValue));
			}
		}
		return expectedToBeRemovedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetExpectedToBeRemovedValue() {
		return expectedToBeRemovedValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpectedToBeRemovedValue(XValue newExpectedToBeRemovedValue,
			NotificationChain msgs) {
		XValue oldExpectedToBeRemovedValue = expectedToBeRemovedValue;
		expectedToBeRemovedValue = newExpectedToBeRemovedValue;
		boolean oldExpectedToBeRemovedValueESet = expectedToBeRemovedValueESet;
		expectedToBeRemovedValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE, oldExpectedToBeRemovedValue,
					newExpectedToBeRemovedValue, !oldExpectedToBeRemovedValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectedToBeRemovedValue(XValue newExpectedToBeRemovedValue) {
		if (newExpectedToBeRemovedValue != expectedToBeRemovedValue) {
			NotificationChain msgs = null;
			if (expectedToBeRemovedValue != null)
				msgs = ((InternalEObject) expectedToBeRemovedValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
						null, msgs);
			if (newExpectedToBeRemovedValue != null)
				msgs = ((InternalEObject) newExpectedToBeRemovedValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
						null, msgs);
			msgs = basicSetExpectedToBeRemovedValue(newExpectedToBeRemovedValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedToBeRemovedValueESet = expectedToBeRemovedValueESet;
			expectedToBeRemovedValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
						newExpectedToBeRemovedValue, newExpectedToBeRemovedValue, !oldExpectedToBeRemovedValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpectedToBeRemovedValue(NotificationChain msgs) {
		XValue oldExpectedToBeRemovedValue = expectedToBeRemovedValue;
		expectedToBeRemovedValue = null;
		boolean oldExpectedToBeRemovedValueESet = expectedToBeRemovedValueESet;
		expectedToBeRemovedValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE, oldExpectedToBeRemovedValue,
					null, oldExpectedToBeRemovedValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpectedToBeRemovedValue() {
		if (expectedToBeRemovedValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expectedToBeRemovedValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE,
					null, msgs);
			msgs = basicUnsetExpectedToBeRemovedValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedToBeRemovedValueESet = expectedToBeRemovedValueESet;
			expectedToBeRemovedValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE, null, null,
						oldExpectedToBeRemovedValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpectedToBeRemovedValue() {
		return expectedToBeRemovedValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getExpectedResultingValue() {
		if (expectedResultingValue != null && expectedResultingValue.eIsProxy()) {
			InternalEObject oldExpectedResultingValue = (InternalEObject) expectedResultingValue;
			expectedResultingValue = (XValue) eResolveProxy(oldExpectedResultingValue);
			if (expectedResultingValue != oldExpectedResultingValue) {
				InternalEObject newExpectedResultingValue = (InternalEObject) expectedResultingValue;
				NotificationChain msgs = oldExpectedResultingValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
						null, null);
				if (newExpectedResultingValue.eInternalContainer() == null) {
					msgs = newExpectedResultingValue.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
							oldExpectedResultingValue, expectedResultingValue));
			}
		}
		return expectedResultingValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetExpectedResultingValue() {
		return expectedResultingValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpectedResultingValue(XValue newExpectedResultingValue, NotificationChain msgs) {
		XValue oldExpectedResultingValue = expectedResultingValue;
		expectedResultingValue = newExpectedResultingValue;
		boolean oldExpectedResultingValueESet = expectedResultingValueESet;
		expectedResultingValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE, oldExpectedResultingValue,
					newExpectedResultingValue, !oldExpectedResultingValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectedResultingValue(XValue newExpectedResultingValue) {
		if (newExpectedResultingValue != expectedResultingValue) {
			NotificationChain msgs = null;
			if (expectedResultingValue != null)
				msgs = ((InternalEObject) expectedResultingValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
						null, msgs);
			if (newExpectedResultingValue != null)
				msgs = ((InternalEObject) newExpectedResultingValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE,
						null, msgs);
			msgs = basicSetExpectedResultingValue(newExpectedResultingValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedResultingValueESet = expectedResultingValueESet;
			expectedResultingValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE, newExpectedResultingValue,
						newExpectedResultingValue, !oldExpectedResultingValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpectedResultingValue(NotificationChain msgs) {
		XValue oldExpectedResultingValue = expectedResultingValue;
		expectedResultingValue = null;
		boolean oldExpectedResultingValueESet = expectedResultingValueESet;
		expectedResultingValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE, oldExpectedResultingValue, null,
					oldExpectedResultingValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpectedResultingValue() {
		if (expectedResultingValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expectedResultingValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE, null,
					msgs);
			msgs = basicUnsetExpectedResultingValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpectedResultingValueESet = expectedResultingValueESet;
			expectedResultingValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE, null, null,
						oldExpectedResultingValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpectedResultingValue() {
		return expectedResultingValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getExpectationsMet() {
		return expectationsMet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpectationsMet(Boolean newExpectationsMet) {
		Boolean oldExpectationsMet = expectationsMet;
		expectationsMet = newExpectationsMet;
		boolean oldExpectationsMetESet = expectationsMetESet;
		expectationsMetESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET, oldExpectationsMet, expectationsMet,
					!oldExpectationsMetESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpectationsMet() {
		Boolean oldExpectationsMet = expectationsMet;
		boolean oldExpectationsMetESet = expectationsMetESet;
		expectationsMet = EXPECTATIONS_MET_EDEFAULT;
		expectationsMetESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET, oldExpectationsMet,
					EXPECTATIONS_MET_EDEFAULT, oldExpectationsMetESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpectationsMet() {
		return expectationsMetESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getOldValue() {
		if (oldValue != null && oldValue.eIsProxy()) {
			InternalEObject oldOldValue = (InternalEObject) oldValue;
			oldValue = (XValue) eResolveProxy(oldOldValue);
			if (oldValue != oldOldValue) {
				InternalEObject newOldValue = (InternalEObject) oldValue;
				NotificationChain msgs = oldOldValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, null);
				if (newOldValue.eInternalContainer() == null) {
					msgs = newOldValue.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, oldOldValue, oldValue));
			}
		}
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetOldValue() {
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOldValue(XValue newOldValue, NotificationChain msgs) {
		XValue oldOldValue = oldValue;
		oldValue = newOldValue;
		boolean oldOldValueESet = oldValueESet;
		oldValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, oldOldValue, newOldValue, !oldOldValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOldValue(XValue newOldValue) {
		if (newOldValue != oldValue) {
			NotificationChain msgs = null;
			if (oldValue != null)
				msgs = ((InternalEObject) oldValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, msgs);
			if (newOldValue != null)
				msgs = ((InternalEObject) newOldValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, msgs);
			msgs = basicSetOldValue(newOldValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldOldValueESet = oldValueESet;
			oldValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, newOldValue, newOldValue,
						!oldOldValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetOldValue(NotificationChain msgs) {
		XValue oldOldValue = oldValue;
		oldValue = null;
		boolean oldOldValueESet = oldValueESet;
		oldValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, oldOldValue, null, oldOldValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOldValue() {
		if (oldValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) oldValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, msgs);
			msgs = basicUnsetOldValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldOldValueESet = oldValueESet;
			oldValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE, null, null, oldOldValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOldValue() {
		return oldValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getNewValue() {
		if (newValue != null && newValue.eIsProxy()) {
			InternalEObject oldNewValue = (InternalEObject) newValue;
			newValue = (XValue) eResolveProxy(oldNewValue);
			if (newValue != oldNewValue) {
				InternalEObject newNewValue = (InternalEObject) newValue;
				NotificationChain msgs = oldNewValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, null);
				if (newNewValue.eInternalContainer() == null) {
					msgs = newNewValue.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, oldNewValue, newValue));
			}
		}
		return newValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetNewValue() {
		return newValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetNewValue(XValue newNewValue, NotificationChain msgs) {
		XValue oldNewValue = newValue;
		newValue = newNewValue;
		boolean oldNewValueESet = newValueESet;
		newValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, oldNewValue, newNewValue, !oldNewValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNewValue(XValue newNewValue) {
		if (newNewValue != newValue) {
			NotificationChain msgs = null;
			if (newValue != null)
				msgs = ((InternalEObject) newValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, msgs);
			if (newNewValue != null)
				msgs = ((InternalEObject) newNewValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, msgs);
			msgs = basicSetNewValue(newNewValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldNewValueESet = newValueESet;
			newValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, newNewValue, newNewValue,
						!oldNewValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetNewValue(NotificationChain msgs) {
		XValue oldNewValue = newValue;
		newValue = null;
		boolean oldNewValueESet = newValueESet;
		newValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, oldNewValue, null, oldNewValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNewValue() {
		if (newValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) newValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, msgs);
			msgs = basicUnsetNewValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldNewValueESet = newValueESet;
			newValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE, null, null, oldNewValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNewValue() {
		return newValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getInconsistent() {
		return inconsistent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInconsistent(Boolean newInconsistent) {
		Boolean oldInconsistent = inconsistent;
		inconsistent = newInconsistent;
		boolean oldInconsistentESet = inconsistentESet;
		inconsistentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT,
					oldInconsistent, inconsistent, !oldInconsistentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInconsistent() {
		Boolean oldInconsistent = inconsistent;
		boolean oldInconsistentESet = inconsistentESet;
		inconsistent = INCONSISTENT_EDEFAULT;
		inconsistentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT, oldInconsistent, INCONSISTENT_EDEFAULT,
					oldInconsistentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInconsistent() {
		return inconsistentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getExecuted() {
		return executed;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecuted(Boolean newExecuted) {
		Boolean oldExecuted = executed;
		executed = newExecuted;
		boolean oldExecutedESet = executedESet;
		executedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED,
					oldExecuted, executed, !oldExecutedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExecuted() {
		Boolean oldExecuted = executed;
		boolean oldExecutedESet = executedESet;
		executed = EXECUTED_EDEFAULT;
		executedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED,
					oldExecuted, EXECUTED_EDEFAULT, oldExecutedESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExecuted() {
		return executedESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getTowardsEndInsertPosition() {
		return towardsEndInsertPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTowardsEndInsertPosition(Integer newTowardsEndInsertPosition) {
		Integer oldTowardsEndInsertPosition = towardsEndInsertPosition;
		towardsEndInsertPosition = newTowardsEndInsertPosition;
		boolean oldTowardsEndInsertPositionESet = towardsEndInsertPositionESet;
		towardsEndInsertPositionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION, oldTowardsEndInsertPosition,
					towardsEndInsertPosition, !oldTowardsEndInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTowardsEndInsertPosition() {
		Integer oldTowardsEndInsertPosition = towardsEndInsertPosition;
		boolean oldTowardsEndInsertPositionESet = towardsEndInsertPositionESet;
		towardsEndInsertPosition = TOWARDS_END_INSERT_POSITION_EDEFAULT;
		towardsEndInsertPositionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION, oldTowardsEndInsertPosition,
					TOWARDS_END_INSERT_POSITION_EDEFAULT, oldTowardsEndInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTowardsEndInsertPosition() {
		return towardsEndInsertPositionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getTowardsEndInsertValues() {
		if (towardsEndInsertValues != null && towardsEndInsertValues.eIsProxy()) {
			InternalEObject oldTowardsEndInsertValues = (InternalEObject) towardsEndInsertValues;
			towardsEndInsertValues = (XValue) eResolveProxy(oldTowardsEndInsertValues);
			if (towardsEndInsertValues != oldTowardsEndInsertValues) {
				InternalEObject newTowardsEndInsertValues = (InternalEObject) towardsEndInsertValues;
				NotificationChain msgs = oldTowardsEndInsertValues.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
						null, null);
				if (newTowardsEndInsertValues.eInternalContainer() == null) {
					msgs = newTowardsEndInsertValues.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
							oldTowardsEndInsertValues, towardsEndInsertValues));
			}
		}
		return towardsEndInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetTowardsEndInsertValues() {
		return towardsEndInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTowardsEndInsertValues(XValue newTowardsEndInsertValues, NotificationChain msgs) {
		XValue oldTowardsEndInsertValues = towardsEndInsertValues;
		towardsEndInsertValues = newTowardsEndInsertValues;
		boolean oldTowardsEndInsertValuesESet = towardsEndInsertValuesESet;
		towardsEndInsertValuesESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES, oldTowardsEndInsertValues,
					newTowardsEndInsertValues, !oldTowardsEndInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTowardsEndInsertValues(XValue newTowardsEndInsertValues) {
		if (newTowardsEndInsertValues != towardsEndInsertValues) {
			NotificationChain msgs = null;
			if (towardsEndInsertValues != null)
				msgs = ((InternalEObject) towardsEndInsertValues).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
						null, msgs);
			if (newTowardsEndInsertValues != null)
				msgs = ((InternalEObject) newTowardsEndInsertValues).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES,
						null, msgs);
			msgs = basicSetTowardsEndInsertValues(newTowardsEndInsertValues, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldTowardsEndInsertValuesESet = towardsEndInsertValuesESet;
			towardsEndInsertValuesESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES, newTowardsEndInsertValues,
						newTowardsEndInsertValues, !oldTowardsEndInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetTowardsEndInsertValues(NotificationChain msgs) {
		XValue oldTowardsEndInsertValues = towardsEndInsertValues;
		towardsEndInsertValues = null;
		boolean oldTowardsEndInsertValuesESet = towardsEndInsertValuesESet;
		towardsEndInsertValuesESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES, oldTowardsEndInsertValues, null,
					oldTowardsEndInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTowardsEndInsertValues() {
		if (towardsEndInsertValues != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) towardsEndInsertValues).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES, null,
					msgs);
			msgs = basicUnsetTowardsEndInsertValues(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldTowardsEndInsertValuesESet = towardsEndInsertValuesESet;
			towardsEndInsertValuesESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES, null, null,
						oldTowardsEndInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTowardsEndInsertValues() {
		return towardsEndInsertValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getInTheMiddleInsertPosition() {
		return inTheMiddleInsertPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInTheMiddleInsertPosition(Integer newInTheMiddleInsertPosition) {
		Integer oldInTheMiddleInsertPosition = inTheMiddleInsertPosition;
		inTheMiddleInsertPosition = newInTheMiddleInsertPosition;
		boolean oldInTheMiddleInsertPositionESet = inTheMiddleInsertPositionESet;
		inTheMiddleInsertPositionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION,
					oldInTheMiddleInsertPosition, inTheMiddleInsertPosition, !oldInTheMiddleInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInTheMiddleInsertPosition() {
		Integer oldInTheMiddleInsertPosition = inTheMiddleInsertPosition;
		boolean oldInTheMiddleInsertPositionESet = inTheMiddleInsertPositionESet;
		inTheMiddleInsertPosition = IN_THE_MIDDLE_INSERT_POSITION_EDEFAULT;
		inTheMiddleInsertPositionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION,
					oldInTheMiddleInsertPosition, IN_THE_MIDDLE_INSERT_POSITION_EDEFAULT,
					oldInTheMiddleInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInTheMiddleInsertPosition() {
		return inTheMiddleInsertPositionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getInTheMiddleInsertValues() {
		if (inTheMiddleInsertValues != null && inTheMiddleInsertValues.eIsProxy()) {
			InternalEObject oldInTheMiddleInsertValues = (InternalEObject) inTheMiddleInsertValues;
			inTheMiddleInsertValues = (XValue) eResolveProxy(oldInTheMiddleInsertValues);
			if (inTheMiddleInsertValues != oldInTheMiddleInsertValues) {
				InternalEObject newInTheMiddleInsertValues = (InternalEObject) inTheMiddleInsertValues;
				NotificationChain msgs = oldInTheMiddleInsertValues.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
						null, null);
				if (newInTheMiddleInsertValues.eInternalContainer() == null) {
					msgs = newInTheMiddleInsertValues.eInverseAdd(this, EOPPOSITE_FEATURE_BASE
							- SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
							oldInTheMiddleInsertValues, inTheMiddleInsertValues));
			}
		}
		return inTheMiddleInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetInTheMiddleInsertValues() {
		return inTheMiddleInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInTheMiddleInsertValues(XValue newInTheMiddleInsertValues,
			NotificationChain msgs) {
		XValue oldInTheMiddleInsertValues = inTheMiddleInsertValues;
		inTheMiddleInsertValues = newInTheMiddleInsertValues;
		boolean oldInTheMiddleInsertValuesESet = inTheMiddleInsertValuesESet;
		inTheMiddleInsertValuesESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES, oldInTheMiddleInsertValues,
					newInTheMiddleInsertValues, !oldInTheMiddleInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInTheMiddleInsertValues(XValue newInTheMiddleInsertValues) {
		if (newInTheMiddleInsertValues != inTheMiddleInsertValues) {
			NotificationChain msgs = null;
			if (inTheMiddleInsertValues != null)
				msgs = ((InternalEObject) inTheMiddleInsertValues).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
						null, msgs);
			if (newInTheMiddleInsertValues != null)
				msgs = ((InternalEObject) newInTheMiddleInsertValues).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
						null, msgs);
			msgs = basicSetInTheMiddleInsertValues(newInTheMiddleInsertValues, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInTheMiddleInsertValuesESet = inTheMiddleInsertValuesESet;
			inTheMiddleInsertValuesESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES,
						newInTheMiddleInsertValues, newInTheMiddleInsertValues, !oldInTheMiddleInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInTheMiddleInsertValues(NotificationChain msgs) {
		XValue oldInTheMiddleInsertValues = inTheMiddleInsertValues;
		inTheMiddleInsertValues = null;
		boolean oldInTheMiddleInsertValuesESet = inTheMiddleInsertValuesESet;
		inTheMiddleInsertValuesESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES, oldInTheMiddleInsertValues,
					null, oldInTheMiddleInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInTheMiddleInsertValues() {
		if (inTheMiddleInsertValues != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) inTheMiddleInsertValues).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES, null,
					msgs);
			msgs = basicUnsetInTheMiddleInsertValues(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInTheMiddleInsertValuesESet = inTheMiddleInsertValuesESet;
			inTheMiddleInsertValuesESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES, null, null,
						oldInTheMiddleInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInTheMiddleInsertValues() {
		return inTheMiddleInsertValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getTowardsBeginningInsertPosition() {
		return towardsBeginningInsertPosition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTowardsBeginningInsertPosition(Integer newTowardsBeginningInsertPosition) {
		Integer oldTowardsBeginningInsertPosition = towardsBeginningInsertPosition;
		towardsBeginningInsertPosition = newTowardsBeginningInsertPosition;
		boolean oldTowardsBeginningInsertPositionESet = towardsBeginningInsertPositionESet;
		towardsBeginningInsertPositionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION,
					oldTowardsBeginningInsertPosition, towardsBeginningInsertPosition,
					!oldTowardsBeginningInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTowardsBeginningInsertPosition() {
		Integer oldTowardsBeginningInsertPosition = towardsBeginningInsertPosition;
		boolean oldTowardsBeginningInsertPositionESet = towardsBeginningInsertPositionESet;
		towardsBeginningInsertPosition = TOWARDS_BEGINNING_INSERT_POSITION_EDEFAULT;
		towardsBeginningInsertPositionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION,
					oldTowardsBeginningInsertPosition, TOWARDS_BEGINNING_INSERT_POSITION_EDEFAULT,
					oldTowardsBeginningInsertPositionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTowardsBeginningInsertPosition() {
		return towardsBeginningInsertPositionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getTowardsBeginningInsertValues() {
		if (towardsBeginningInsertValues != null && towardsBeginningInsertValues.eIsProxy()) {
			InternalEObject oldTowardsBeginningInsertValues = (InternalEObject) towardsBeginningInsertValues;
			towardsBeginningInsertValues = (XValue) eResolveProxy(oldTowardsBeginningInsertValues);
			if (towardsBeginningInsertValues != oldTowardsBeginningInsertValues) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES,
							oldTowardsBeginningInsertValues, towardsBeginningInsertValues));
			}
		}
		return towardsBeginningInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetTowardsBeginningInsertValues() {
		return towardsBeginningInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTowardsBeginningInsertValues(XValue newTowardsBeginningInsertValues) {
		XValue oldTowardsBeginningInsertValues = towardsBeginningInsertValues;
		towardsBeginningInsertValues = newTowardsBeginningInsertValues;
		boolean oldTowardsBeginningInsertValuesESet = towardsBeginningInsertValuesESet;
		towardsBeginningInsertValuesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES,
					oldTowardsBeginningInsertValues, towardsBeginningInsertValues,
					!oldTowardsBeginningInsertValuesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTowardsBeginningInsertValues() {
		XValue oldTowardsBeginningInsertValues = towardsBeginningInsertValues;
		boolean oldTowardsBeginningInsertValuesESet = towardsBeginningInsertValuesESet;
		towardsBeginningInsertValues = null;
		towardsBeginningInsertValuesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES,
					oldTowardsBeginningInsertValues, null, oldTowardsBeginningInsertValuesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTowardsBeginningInsertValues() {
		return towardsBeginningInsertValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getAsFirstInsertValues() {
		if (asFirstInsertValues != null && asFirstInsertValues.eIsProxy()) {
			InternalEObject oldAsFirstInsertValues = (InternalEObject) asFirstInsertValues;
			asFirstInsertValues = (XValue) eResolveProxy(oldAsFirstInsertValues);
			if (asFirstInsertValues != oldAsFirstInsertValues) {
				InternalEObject newAsFirstInsertValues = (InternalEObject) asFirstInsertValues;
				NotificationChain msgs = oldAsFirstInsertValues.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, null,
						null);
				if (newAsFirstInsertValues.eInternalContainer() == null) {
					msgs = newAsFirstInsertValues.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, oldAsFirstInsertValues,
							asFirstInsertValues));
			}
		}
		return asFirstInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetAsFirstInsertValues() {
		return asFirstInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsFirstInsertValues(XValue newAsFirstInsertValues, NotificationChain msgs) {
		XValue oldAsFirstInsertValues = asFirstInsertValues;
		asFirstInsertValues = newAsFirstInsertValues;
		boolean oldAsFirstInsertValuesESet = asFirstInsertValuesESet;
		asFirstInsertValuesESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, oldAsFirstInsertValues,
					newAsFirstInsertValues, !oldAsFirstInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsFirstInsertValues(XValue newAsFirstInsertValues) {
		if (newAsFirstInsertValues != asFirstInsertValues) {
			NotificationChain msgs = null;
			if (asFirstInsertValues != null)
				msgs = ((InternalEObject) asFirstInsertValues).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, null,
						msgs);
			if (newAsFirstInsertValues != null)
				msgs = ((InternalEObject) newAsFirstInsertValues).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, null,
						msgs);
			msgs = basicSetAsFirstInsertValues(newAsFirstInsertValues, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAsFirstInsertValuesESet = asFirstInsertValuesESet;
			asFirstInsertValuesESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, newAsFirstInsertValues,
						newAsFirstInsertValues, !oldAsFirstInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAsFirstInsertValues(NotificationChain msgs) {
		XValue oldAsFirstInsertValues = asFirstInsertValues;
		asFirstInsertValues = null;
		boolean oldAsFirstInsertValuesESet = asFirstInsertValuesESet;
		asFirstInsertValuesESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, oldAsFirstInsertValues, null,
					oldAsFirstInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAsFirstInsertValues() {
		if (asFirstInsertValues != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) asFirstInsertValues).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, null,
					msgs);
			msgs = basicUnsetAsFirstInsertValues(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAsFirstInsertValuesESet = asFirstInsertValuesESet;
			asFirstInsertValuesESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES, null, null,
						oldAsFirstInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAsFirstInsertValues() {
		return asFirstInsertValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getAsLastInsertValues() {
		if (asLastInsertValues != null && asLastInsertValues.eIsProxy()) {
			InternalEObject oldAsLastInsertValues = (InternalEObject) asLastInsertValues;
			asLastInsertValues = (XValue) eResolveProxy(oldAsLastInsertValues);
			if (asLastInsertValues != oldAsLastInsertValues) {
				InternalEObject newAsLastInsertValues = (InternalEObject) asLastInsertValues;
				NotificationChain msgs = oldAsLastInsertValues.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, null,
						null);
				if (newAsLastInsertValues.eInternalContainer() == null) {
					msgs = newAsLastInsertValues.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, oldAsLastInsertValues,
							asLastInsertValues));
			}
		}
		return asLastInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetAsLastInsertValues() {
		return asLastInsertValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAsLastInsertValues(XValue newAsLastInsertValues, NotificationChain msgs) {
		XValue oldAsLastInsertValues = asLastInsertValues;
		asLastInsertValues = newAsLastInsertValues;
		boolean oldAsLastInsertValuesESet = asLastInsertValuesESet;
		asLastInsertValuesESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, oldAsLastInsertValues,
					newAsLastInsertValues, !oldAsLastInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAsLastInsertValues(XValue newAsLastInsertValues) {
		if (newAsLastInsertValues != asLastInsertValues) {
			NotificationChain msgs = null;
			if (asLastInsertValues != null)
				msgs = ((InternalEObject) asLastInsertValues).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, null,
						msgs);
			if (newAsLastInsertValues != null)
				msgs = ((InternalEObject) newAsLastInsertValues).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, null,
						msgs);
			msgs = basicSetAsLastInsertValues(newAsLastInsertValues, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAsLastInsertValuesESet = asLastInsertValuesESet;
			asLastInsertValuesESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, newAsLastInsertValues,
						newAsLastInsertValues, !oldAsLastInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAsLastInsertValues(NotificationChain msgs) {
		XValue oldAsLastInsertValues = asLastInsertValues;
		asLastInsertValues = null;
		boolean oldAsLastInsertValuesESet = asLastInsertValuesESet;
		asLastInsertValuesESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, oldAsLastInsertValues, null,
					oldAsLastInsertValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAsLastInsertValues() {
		if (asLastInsertValues != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) asLastInsertValues).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, null,
					msgs);
			msgs = basicUnsetAsLastInsertValues(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAsLastInsertValuesESet = asLastInsertValuesESet;
			asLastInsertValuesESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES, null, null,
						oldAsLastInsertValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAsLastInsertValues() {
		return asLastInsertValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getDeleteValues() {
		if (deleteValues != null && deleteValues.eIsProxy()) {
			InternalEObject oldDeleteValues = (InternalEObject) deleteValues;
			deleteValues = (XValue) eResolveProxy(oldDeleteValues);
			if (deleteValues != oldDeleteValues) {
				InternalEObject newDeleteValues = (InternalEObject) deleteValues;
				NotificationChain msgs = oldDeleteValues.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null, null);
				if (newDeleteValues.eInternalContainer() == null) {
					msgs = newDeleteValues.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, oldDeleteValues, deleteValues));
			}
		}
		return deleteValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetDeleteValues() {
		return deleteValues;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetDeleteValues(XValue newDeleteValues, NotificationChain msgs) {
		XValue oldDeleteValues = deleteValues;
		deleteValues = newDeleteValues;
		boolean oldDeleteValuesESet = deleteValuesESet;
		deleteValuesESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, oldDeleteValues, newDeleteValues,
					!oldDeleteValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeleteValues(XValue newDeleteValues) {
		if (newDeleteValues != deleteValues) {
			NotificationChain msgs = null;
			if (deleteValues != null)
				msgs = ((InternalEObject) deleteValues).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null, msgs);
			if (newDeleteValues != null)
				msgs = ((InternalEObject) newDeleteValues).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null, msgs);
			msgs = basicSetDeleteValues(newDeleteValues, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldDeleteValuesESet = deleteValuesESet;
			deleteValuesESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, newDeleteValues, newDeleteValues,
						!oldDeleteValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetDeleteValues(NotificationChain msgs) {
		XValue oldDeleteValues = deleteValues;
		deleteValues = null;
		boolean oldDeleteValuesESet = deleteValuesESet;
		deleteValuesESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, oldDeleteValues, null, oldDeleteValuesESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDeleteValues() {
		if (deleteValues != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) deleteValues).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null, msgs);
			msgs = basicUnsetDeleteValues(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldDeleteValuesESet = deleteValuesESet;
			deleteValuesESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES, null, null, oldDeleteValuesESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDeleteValues() {
		return deleteValuesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getContributingUpdate()).basicAdd(otherEnd,
					msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			return ((InternalEList<?>) getContributingUpdate()).basicRemove(otherEnd, msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE:
			return basicUnsetExpectedToBeAddedValue(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE:
			return basicUnsetExpectedToBeRemovedValue(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE:
			return basicUnsetExpectedResultingValue(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE:
			return basicUnsetOldValue(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE:
			return basicUnsetNewValue(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES:
			return basicUnsetTowardsEndInsertValues(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES:
			return basicUnsetInTheMiddleInsertValues(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES:
			return basicUnsetAsFirstInsertValues(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES:
			return basicUnsetAsLastInsertValues(msgs);
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES:
			return basicUnsetDeleteValues(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			return getContributingUpdate();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE:
			if (resolve)
				return getExpectedToBeAddedValue();
			return basicGetExpectedToBeAddedValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE:
			if (resolve)
				return getExpectedToBeRemovedValue();
			return basicGetExpectedToBeRemovedValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE:
			if (resolve)
				return getExpectedResultingValue();
			return basicGetExpectedResultingValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET:
			return getExpectationsMet();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE:
			if (resolve)
				return getOldValue();
			return basicGetOldValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE:
			if (resolve)
				return getNewValue();
			return basicGetNewValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT:
			return getInconsistent();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED:
			return getExecuted();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION:
			return getTowardsEndInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES:
			if (resolve)
				return getTowardsEndInsertValues();
			return basicGetTowardsEndInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION:
			return getInTheMiddleInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES:
			if (resolve)
				return getInTheMiddleInsertValues();
			return basicGetInTheMiddleInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION:
			return getTowardsBeginningInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES:
			if (resolve)
				return getTowardsBeginningInsertValues();
			return basicGetTowardsBeginningInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES:
			if (resolve)
				return getAsFirstInsertValues();
			return basicGetAsFirstInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES:
			if (resolve)
				return getAsLastInsertValues();
			return basicGetAsLastInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES:
			if (resolve)
				return getDeleteValues();
			return basicGetDeleteValues();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			getContributingUpdate().clear();
			getContributingUpdate().addAll((Collection<? extends XUpdate>) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE:
			setExpectedToBeAddedValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE:
			setExpectedToBeRemovedValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE:
			setExpectedResultingValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET:
			setExpectationsMet((Boolean) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE:
			setOldValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE:
			setNewValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT:
			setInconsistent((Boolean) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED:
			setExecuted((Boolean) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION:
			setTowardsEndInsertPosition((Integer) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES:
			setTowardsEndInsertValues((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION:
			setInTheMiddleInsertPosition((Integer) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES:
			setInTheMiddleInsertValues((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION:
			setTowardsBeginningInsertPosition((Integer) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES:
			setTowardsBeginningInsertValues((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES:
			setAsFirstInsertValues((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES:
			setAsLastInsertValues((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES:
			setDeleteValues((XValue) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			unsetContributingUpdate();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE:
			unsetExpectedToBeAddedValue();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE:
			unsetExpectedToBeRemovedValue();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE:
			unsetExpectedResultingValue();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET:
			unsetExpectationsMet();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE:
			unsetOldValue();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE:
			unsetNewValue();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT:
			unsetInconsistent();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED:
			unsetExecuted();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION:
			unsetTowardsEndInsertPosition();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES:
			unsetTowardsEndInsertValues();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION:
			unsetInTheMiddleInsertPosition();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES:
			unsetInTheMiddleInsertValues();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION:
			unsetTowardsBeginningInsertPosition();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES:
			unsetTowardsBeginningInsertValues();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES:
			unsetAsFirstInsertValues();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES:
			unsetAsLastInsertValues();
			return;
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES:
			unsetDeleteValues();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE:
			return isSetContributingUpdate();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE:
			return isSetExpectedToBeAddedValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE:
			return isSetExpectedToBeRemovedValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE:
			return isSetExpectedResultingValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET:
			return isSetExpectationsMet();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__OLD_VALUE:
			return isSetOldValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__NEW_VALUE:
			return isSetNewValue();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__INCONSISTENT:
			return isSetInconsistent();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__EXECUTED:
			return isSetExecuted();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION:
			return isSetTowardsEndInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES:
			return isSetTowardsEndInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION:
			return isSetInTheMiddleInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES:
			return isSetInTheMiddleInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION:
			return isSetTowardsBeginningInsertPosition();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES:
			return isSetTowardsBeginningInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES:
			return isSetAsFirstInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES:
			return isSetAsLastInsertValues();
		case SemanticsPackage.XUPDATED_TYPED_ELEMENT__DELETE_VALUES:
			return isSetDeleteValues();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (expectationsMet: ");
		if (expectationsMetESet)
			result.append(expectationsMet);
		else
			result.append("<unset>");
		result.append(", inconsistent: ");
		if (inconsistentESet)
			result.append(inconsistent);
		else
			result.append("<unset>");
		result.append(", executed: ");
		if (executedESet)
			result.append(executed);
		else
			result.append("<unset>");
		result.append(", towardsEndInsertPosition: ");
		if (towardsEndInsertPositionESet)
			result.append(towardsEndInsertPosition);
		else
			result.append("<unset>");
		result.append(", inTheMiddleInsertPosition: ");
		if (inTheMiddleInsertPositionESet)
			result.append(inTheMiddleInsertPosition);
		else
			result.append("<unset>");
		result.append(", towardsBeginningInsertPosition: ");
		if (towardsBeginningInsertPositionESet)
			result.append(towardsBeginningInsertPosition);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //XUpdatedTypedElementImpl
