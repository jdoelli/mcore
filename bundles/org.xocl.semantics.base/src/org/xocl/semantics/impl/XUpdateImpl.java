/**
 */
package org.xocl.semantics.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XAddSpecification;
import org.xocl.semantics.XUpdateMode;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XUpdatedTypedElement;
import org.xocl.semantics.XValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>XAbstract Update</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getUpdatedObject <em>Updated Object</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getValue <em>Value</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getMode <em>Mode</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getAddSpecification <em>Add Specification</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getContributesTo <em>Contributes To</em>}</li>
 *   <li>{@link org.xocl.semantics.impl.XUpdateImpl#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class XUpdateImpl extends XUpdateContainerImpl implements XUpdate {
	/**
	 * The cached value of the '{@link #getUpdatedObject() <em>Updated Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdatedObject()
	 * @generated
	 * @ordered
	 */
	protected XUpdatedObject updatedObject;

	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature feature;

	/**
	 * The cached value of the '{@link #getOperation() <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation operation;

	/**
	 * This is true if the Operation reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean operationESet;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected XValue value;

	/**
	 * The default value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected static final XUpdateMode MODE_EDEFAULT = XUpdateMode.REDEFINE;

	/**
	 * The cached value of the '{@link #getMode() <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMode()
	 * @generated
	 * @ordered
	 */
	protected XUpdateMode mode = MODE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getAddSpecification() <em>Add Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddSpecification()
	 * @generated
	 * @ordered
	 */
	protected XAddSpecification addSpecification;

	/**
	 * This is true if the Add Specification containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean addSpecificationESet;

	/**
	 * The cached value of the '{@link #getContributesTo() <em>Contributes To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContributesTo()
	 * @generated
	 * @ordered
	 */
	protected XUpdatedTypedElement contributesTo;

	/**
	 * This is true if the Contributes To reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean contributesToESet;

	/**
	 * The default value of the '{@link #getObject() <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected static final Object OBJECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getObject() <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObject()
	 * @generated
	 * @ordered
	 */
	protected Object object = OBJECT_EDEFAULT;

	/**
	 * This is true if the Object attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean objectESet;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getFeature <em>Feature</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression featureChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected XUpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SemanticsPackage.Literals.XUPDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject getUpdatedObject() {
		if (updatedObject != null && updatedObject.eIsProxy()) {
			InternalEObject oldUpdatedObject = (InternalEObject) updatedObject;
			updatedObject = (XUpdatedObject) eResolveProxy(oldUpdatedObject);
			if (updatedObject != oldUpdatedObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATE__UPDATED_OBJECT,
							oldUpdatedObject, updatedObject));
			}
		}
		return updatedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedObject basicGetUpdatedObject() {
		return updatedObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpdatedObject(XUpdatedObject newUpdatedObject) {
		XUpdatedObject oldUpdatedObject = updatedObject;
		updatedObject = newUpdatedObject;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__UPDATED_OBJECT,
					oldUpdatedObject, updatedObject));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject) feature;
			feature = (EStructuralFeature) eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATE__FEATURE,
							oldFeature, feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeature(EStructuralFeature newFeature) {
		EStructuralFeature oldFeature = feature;
		feature = newFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__FEATURE, oldFeature,
					feature));
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<EStructuralFeature>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if updatedObject.oclIsUndefined() then OrderedSet{} else 
	
	if updatedObject.object.oclIsUndefined() then
	if updatedObject.classForCreation.oclIsUndefined() then OrderedSet{} else
	      updatedObject.classForCreation.eAllStructuralFeatures endif
	 else updatedObject.object.eClass().eAllStructuralFeatures endif
	endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<EStructuralFeature> evalFeatureChoiceConstruction(List<EStructuralFeature> choice) {
		EClass eClass = SemanticsPackage.Literals.XUPDATE;
		if (featureChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar, true);
			EStructuralFeature eStructuralFeature = SemanticsPackage.Literals.XUPDATE__FEATURE;

			String choiceConstruction = XoclEmfUtil.findChoiceConstructionAnnotationText(eStructuralFeature, eClass());

			try {
				featureChoiceConstructionOCL = helper.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass, "FeatureChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(featureChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, "FeatureChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<EStructuralFeature> result = new ArrayList<EStructuralFeature>(
					(Collection<EStructuralFeature>) query.evaluate(this));
			result.remove(null);
			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue getValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject) value;
			value = (XValue) eResolveProxy(oldValue);
			if (value != oldValue) {
				InternalEObject newValue = (InternalEObject) value;
				NotificationChain msgs = oldValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__VALUE, null, null);
				if (newValue.eInternalContainer() == null) {
					msgs = newValue.eInverseAdd(this, EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__VALUE, null,
							msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATE__VALUE, oldValue,
							value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValue basicGetValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetValue(XValue newValue, NotificationChain msgs) {
		XValue oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATE__VALUE, oldValue, newValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(XValue newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject) value).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__VALUE, null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject) newValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__VALUE, null, msgs);
			msgs = basicSetValue(newValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__VALUE, newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdateMode getMode() {
		return mode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMode(XUpdateMode newMode) {
		XUpdateMode oldMode = mode;
		mode = newMode == null ? MODE_EDEFAULT : newMode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__MODE, oldMode, mode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedTypedElement getContributesTo() {
		if (contributesTo != null && contributesTo.eIsProxy()) {
			InternalEObject oldContributesTo = (InternalEObject) contributesTo;
			contributesTo = (XUpdatedTypedElement) eResolveProxy(oldContributesTo);
			if (contributesTo != oldContributesTo) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATE__CONTRIBUTES_TO,
							oldContributesTo, contributesTo));
			}
		}
		return contributesTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdatedTypedElement basicGetContributesTo() {
		return contributesTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContributesTo(XUpdatedTypedElement newContributesTo, NotificationChain msgs) {
		XUpdatedTypedElement oldContributesTo = contributesTo;
		contributesTo = newContributesTo;
		boolean oldContributesToESet = contributesToESet;
		contributesToESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATE__CONTRIBUTES_TO, oldContributesTo, newContributesTo,
					!oldContributesToESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContributesTo(XUpdatedTypedElement newContributesTo) {
		if (newContributesTo != contributesTo) {
			NotificationChain msgs = null;
			if (contributesTo != null)
				msgs = ((InternalEObject) contributesTo).eInverseRemove(this,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE, XUpdatedTypedElement.class, msgs);
			if (newContributesTo != null)
				msgs = ((InternalEObject) newContributesTo).eInverseAdd(this,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE, XUpdatedTypedElement.class, msgs);
			msgs = basicSetContributesTo(newContributesTo, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContributesToESet = contributesToESet;
			contributesToESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__CONTRIBUTES_TO,
						newContributesTo, newContributesTo, !oldContributesToESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetContributesTo(NotificationChain msgs) {
		XUpdatedTypedElement oldContributesTo = contributesTo;
		contributesTo = null;
		boolean oldContributesToESet = contributesToESet;
		contributesToESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATE__CONTRIBUTES_TO, oldContributesTo, null, oldContributesToESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContributesTo() {
		if (contributesTo != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) contributesTo).eInverseRemove(this,
					SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE, XUpdatedTypedElement.class, msgs);
			msgs = basicUnsetContributesTo(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContributesToESet = contributesToESet;
			contributesToESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XUPDATE__CONTRIBUTES_TO, null,
						null, oldContributesToESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContributesTo() {
		return contributesToESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getObject() {
		return object;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(Object newObject) {
		Object oldObject = object;
		object = newObject;
		boolean oldObjectESet = objectESet;
		objectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__OBJECT, oldObject, object,
					!oldObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetObject() {
		Object oldObject = object;
		boolean oldObjectESet = objectESet;
		object = OBJECT_EDEFAULT;
		objectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XUPDATE__OBJECT, oldObject,
					OBJECT_EDEFAULT, oldObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetObject() {
		return objectESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getOperation() {
		if (operation != null && operation.eIsProxy()) {
			InternalEObject oldOperation = (InternalEObject) operation;
			operation = (EOperation) eResolveProxy(oldOperation);
			if (operation != oldOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, SemanticsPackage.XUPDATE__OPERATION,
							oldOperation, operation));
			}
		}
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetOperation() {
		return operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperation(EOperation newOperation) {
		EOperation oldOperation = operation;
		operation = newOperation;
		boolean oldOperationESet = operationESet;
		operationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__OPERATION, oldOperation,
					operation, !oldOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperation() {
		EOperation oldOperation = operation;
		boolean oldOperationESet = operationESet;
		operation = null;
		operationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XUPDATE__OPERATION, oldOperation,
					null, oldOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperation() {
		return operationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAddSpecification getAddSpecification() {
		if (addSpecification != null && addSpecification.eIsProxy()) {
			InternalEObject oldAddSpecification = (InternalEObject) addSpecification;
			addSpecification = (XAddSpecification) eResolveProxy(oldAddSpecification);
			if (addSpecification != oldAddSpecification) {
				InternalEObject newAddSpecification = (InternalEObject) addSpecification;
				NotificationChain msgs = oldAddSpecification.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__ADD_SPECIFICATION, null, null);
				if (newAddSpecification.eInternalContainer() == null) {
					msgs = newAddSpecification.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__ADD_SPECIFICATION, null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							SemanticsPackage.XUPDATE__ADD_SPECIFICATION, oldAddSpecification, addSpecification));
			}
		}
		return addSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XAddSpecification basicGetAddSpecification() {
		return addSpecification;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAddSpecification(XAddSpecification newAddSpecification, NotificationChain msgs) {
		XAddSpecification oldAddSpecification = addSpecification;
		addSpecification = newAddSpecification;
		boolean oldAddSpecificationESet = addSpecificationESet;
		addSpecificationESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					SemanticsPackage.XUPDATE__ADD_SPECIFICATION, oldAddSpecification, newAddSpecification,
					!oldAddSpecificationESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAddSpecification(XAddSpecification newAddSpecification) {
		if (newAddSpecification != addSpecification) {
			NotificationChain msgs = null;
			if (addSpecification != null)
				msgs = ((InternalEObject) addSpecification).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__ADD_SPECIFICATION, null, msgs);
			if (newAddSpecification != null)
				msgs = ((InternalEObject) newAddSpecification).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__ADD_SPECIFICATION, null, msgs);
			msgs = basicSetAddSpecification(newAddSpecification, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAddSpecificationESet = addSpecificationESet;
			addSpecificationESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, SemanticsPackage.XUPDATE__ADD_SPECIFICATION,
						newAddSpecification, newAddSpecification, !oldAddSpecificationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAddSpecification(NotificationChain msgs) {
		XAddSpecification oldAddSpecification = addSpecification;
		addSpecification = null;
		boolean oldAddSpecificationESet = addSpecificationESet;
		addSpecificationESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					SemanticsPackage.XUPDATE__ADD_SPECIFICATION, oldAddSpecification, null, oldAddSpecificationESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAddSpecification() {
		if (addSpecification != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) addSpecification).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - SemanticsPackage.XUPDATE__ADD_SPECIFICATION, null, msgs);
			msgs = basicUnsetAddSpecification(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAddSpecificationESet = addSpecificationESet;
			addSpecificationESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, SemanticsPackage.XUPDATE__ADD_SPECIFICATION,
						null, null, oldAddSpecificationESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAddSpecification() {
		return addSpecificationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			if (contributesTo != null)
				msgs = ((InternalEObject) contributesTo).eInverseRemove(this,
						SemanticsPackage.XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE, XUpdatedTypedElement.class, msgs);
			return basicSetContributesTo((XUpdatedTypedElement) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__VALUE:
			return basicSetValue(null, msgs);
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			return basicUnsetAddSpecification(msgs);
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			return basicUnsetContributesTo(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__UPDATED_OBJECT:
			if (resolve)
				return getUpdatedObject();
			return basicGetUpdatedObject();
		case SemanticsPackage.XUPDATE__FEATURE:
			if (resolve)
				return getFeature();
			return basicGetFeature();
		case SemanticsPackage.XUPDATE__OPERATION:
			if (resolve)
				return getOperation();
			return basicGetOperation();
		case SemanticsPackage.XUPDATE__VALUE:
			if (resolve)
				return getValue();
			return basicGetValue();
		case SemanticsPackage.XUPDATE__MODE:
			return getMode();
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			if (resolve)
				return getAddSpecification();
			return basicGetAddSpecification();
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			if (resolve)
				return getContributesTo();
			return basicGetContributesTo();
		case SemanticsPackage.XUPDATE__OBJECT:
			return getObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__UPDATED_OBJECT:
			setUpdatedObject((XUpdatedObject) newValue);
			return;
		case SemanticsPackage.XUPDATE__FEATURE:
			setFeature((EStructuralFeature) newValue);
			return;
		case SemanticsPackage.XUPDATE__OPERATION:
			setOperation((EOperation) newValue);
			return;
		case SemanticsPackage.XUPDATE__VALUE:
			setValue((XValue) newValue);
			return;
		case SemanticsPackage.XUPDATE__MODE:
			setMode((XUpdateMode) newValue);
			return;
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			setAddSpecification((XAddSpecification) newValue);
			return;
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			setContributesTo((XUpdatedTypedElement) newValue);
			return;
		case SemanticsPackage.XUPDATE__OBJECT:
			setObject(newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__UPDATED_OBJECT:
			setUpdatedObject((XUpdatedObject) null);
			return;
		case SemanticsPackage.XUPDATE__FEATURE:
			setFeature((EStructuralFeature) null);
			return;
		case SemanticsPackage.XUPDATE__OPERATION:
			unsetOperation();
			return;
		case SemanticsPackage.XUPDATE__VALUE:
			setValue((XValue) null);
			return;
		case SemanticsPackage.XUPDATE__MODE:
			setMode(MODE_EDEFAULT);
			return;
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			unsetAddSpecification();
			return;
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			unsetContributesTo();
			return;
		case SemanticsPackage.XUPDATE__OBJECT:
			unsetObject();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case SemanticsPackage.XUPDATE__UPDATED_OBJECT:
			return updatedObject != null;
		case SemanticsPackage.XUPDATE__FEATURE:
			return feature != null;
		case SemanticsPackage.XUPDATE__OPERATION:
			return isSetOperation();
		case SemanticsPackage.XUPDATE__VALUE:
			return value != null;
		case SemanticsPackage.XUPDATE__MODE:
			return mode != MODE_EDEFAULT;
		case SemanticsPackage.XUPDATE__ADD_SPECIFICATION:
			return isSetAddSpecification();
		case SemanticsPackage.XUPDATE__CONTRIBUTES_TO:
			return isSetContributesTo();
		case SemanticsPackage.XUPDATE__OBJECT:
			return isSetObject();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mode: ");
		result.append(mode);
		result.append(", object: ");
		if (objectESet)
			result.append(object);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel ':='
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (SemanticsPackage.Literals.XUPDATE);
		EStructuralFeature eOverrideFeature = SemanticsPackage.Literals.XSEMANTICS_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(SemanticsPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(SemanticsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} //XUpdateImpl
