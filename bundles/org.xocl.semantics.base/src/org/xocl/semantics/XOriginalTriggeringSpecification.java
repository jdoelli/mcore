/**
 */
package org.xocl.semantics;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XOriginal Triggering Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate <em>Containing Triggered Update</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXOriginalTriggeringSpecification()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL triggeringUpdateDerive='if containingTriggeredUpdate.oclIsUndefined()\n  then null\n  else containingTriggeredUpdate.originallyTriggeringUpdate\nendif\n' kindLabelDerive='\'Original Triggering\'\n'"
 * @generated
 */

public interface XOriginalTriggeringSpecification extends XTriggeringSpecification {
	/**
	 * Returns the value of the '<em><b>Containing Triggered Update</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Triggered Update</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Triggered Update</em>' container reference.
	 * @see #setContainingTriggeredUpdate(XTriggeredUpdate)
	 * @see org.xocl.semantics.SemanticsPackage#getXOriginalTriggeringSpecification_ContainingTriggeredUpdate()
	 * @see org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis
	 * @model opposite="originalTriggeringOfThis" unsettable="true" required="true" transient="false"
	 * @generated
	 */
	XTriggeredUpdate getContainingTriggeredUpdate();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate <em>Containing Triggered Update</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Containing Triggered Update</em>' container reference.
	 * @see #getContainingTriggeredUpdate()
	 * @generated
	 */
	void setContainingTriggeredUpdate(XTriggeredUpdate value);

} // XOriginalTriggeringSpecification
