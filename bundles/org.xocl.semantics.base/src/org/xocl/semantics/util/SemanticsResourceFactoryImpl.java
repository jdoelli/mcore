/**
 */
package org.xocl.semantics.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLMapImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource Factory</b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.util.SemanticsResourceImpl
 * @generated
 */
public class SemanticsResourceFactoryImpl extends ResourceFactoryImpl {
	/**
	 * Creates an instance of the resource factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsResourceFactoryImpl() {
		super();
	}

	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Resource createResource(URI uri) {
		SemanticsResourceImpl result = new SemanticsResourceImpl(uri) {

			@Override

			protected boolean useUUIDs() {
				return true;
			}
		};
		XMLResource.XMLMap xmlMap = new XMLMapImpl();
		xmlMap.setIDAttributeName("_uuid");
		result.getDefaultSaveOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		result.getDefaultLoadOptions().put(XMLResource.OPTION_XML_MAP, xmlMap);
		return result;
	}

} //SemanticsResourceFactoryImpl
