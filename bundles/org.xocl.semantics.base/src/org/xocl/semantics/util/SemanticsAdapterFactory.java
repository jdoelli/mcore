/**
 */
package org.xocl.semantics.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import org.xocl.semantics.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.SemanticsPackage
 * @generated
 */
public class SemanticsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static SemanticsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = SemanticsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SemanticsSwitch<Adapter> modelSwitch = new SemanticsSwitch<Adapter>() {
		@Override
		public Adapter caseXSemanticsElement(XSemanticsElement object) {
			return createXSemanticsElementAdapter();
		}

		@Override
		public Adapter caseXSemantics(XSemantics object) {
			return createXSemanticsAdapter();
		}

		@Override
		public Adapter caseXUpdateContainer(XUpdateContainer object) {
			return createXUpdateContainerAdapter();
		}

		@Override
		public Adapter caseXTransition(XTransition object) {
			return createXTransitionAdapter();
		}

		@Override
		public Adapter caseXUpdate(XUpdate object) {
			return createXUpdateAdapter();
		}

		@Override
		public Adapter caseXAddSpecification(XAddSpecification object) {
			return createXAddSpecificationAdapter();
		}

		@Override
		public Adapter caseXAbstractTriggeringUpdate(XAbstractTriggeringUpdate object) {
			return createXAbstractTriggeringUpdateAdapter();
		}

		@Override
		public Adapter caseXTriggeringSpecification(XTriggeringSpecification object) {
			return createXTriggeringSpecificationAdapter();
		}

		@Override
		public Adapter caseXAdditionalTriggeringSpecification(XAdditionalTriggeringSpecification object) {
			return createXAdditionalTriggeringSpecificationAdapter();
		}

		@Override
		public Adapter caseXTriggeredUpdate(XTriggeredUpdate object) {
			return createXTriggeredUpdateAdapter();
		}

		@Override
		public Adapter caseXOriginalTriggeringSpecification(XOriginalTriggeringSpecification object) {
			return createXOriginalTriggeringSpecificationAdapter();
		}

		@Override
		public Adapter caseXUpdatedObject(XUpdatedObject object) {
			return createXUpdatedObjectAdapter();
		}

		@Override
		public Adapter caseXUpdatedFeature(XUpdatedFeature object) {
			return createXUpdatedFeatureAdapter();
		}

		@Override
		public Adapter caseXValue(XValue object) {
			return createXValueAdapter();
		}

		@Override
		public Adapter caseXUpdatedTypedElement(XUpdatedTypedElement object) {
			return createXUpdatedTypedElementAdapter();
		}

		@Override
		public Adapter caseXUpdatedOperation(XUpdatedOperation object) {
			return createXUpdatedOperationAdapter();
		}

		@Override
		public Adapter caseDUmmy(DUmmy object) {
			return createDUmmyAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XSemantics <em>XSemantics</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XSemantics
	 * @generated
	 */
	public Adapter createXSemanticsAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdateContainer <em>XUpdate Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdateContainer
	 * @generated
	 */
	public Adapter createXUpdateContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XTransition <em>XTransition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XTransition
	 * @generated
	 */
	public Adapter createXTransitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdate <em>XUpdate</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdate
	 * @generated
	 */
	public Adapter createXUpdateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XAddSpecification <em>XAdd Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XAddSpecification
	 * @generated
	 */
	public Adapter createXAddSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XAbstractTriggeringUpdate <em>XAbstract Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate
	 * @generated
	 */
	public Adapter createXAbstractTriggeringUpdateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XTriggeringSpecification <em>XTriggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XTriggeringSpecification
	 * @generated
	 */
	public Adapter createXTriggeringSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XAdditionalTriggeringSpecification <em>XAdditional Triggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XAdditionalTriggeringSpecification
	 * @generated
	 */
	public Adapter createXAdditionalTriggeringSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XTriggeredUpdate <em>XTriggered Update</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XTriggeredUpdate
	 * @generated
	 */
	public Adapter createXTriggeredUpdateAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XOriginalTriggeringSpecification <em>XOriginal Triggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XOriginalTriggeringSpecification
	 * @generated
	 */
	public Adapter createXOriginalTriggeringSpecificationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdatedObject <em>XUpdated Object</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdatedObject
	 * @generated
	 */
	public Adapter createXUpdatedObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdatedFeature <em>XUpdated Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdatedFeature
	 * @generated
	 */
	public Adapter createXUpdatedFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XValue <em>XValue</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XValue
	 * @generated
	 */
	public Adapter createXValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdatedTypedElement <em>XUpdated Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdatedTypedElement
	 * @generated
	 */
	public Adapter createXUpdatedTypedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XUpdatedOperation <em>XUpdated Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XUpdatedOperation
	 * @generated
	 */
	public Adapter createXUpdatedOperationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.DUmmy <em>DUmmy</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.DUmmy
	 * @generated
	 */
	public Adapter createDUmmyAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.semantics.XSemanticsElement <em>XSemantics Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.semantics.XSemanticsElement
	 * @generated
	 */
	public Adapter createXSemanticsElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //SemanticsAdapterFactory
