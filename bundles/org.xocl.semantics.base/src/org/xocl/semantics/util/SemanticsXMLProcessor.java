/**
 */
package org.xocl.semantics.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;
import org.xocl.semantics.SemanticsPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class SemanticsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SemanticsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		SemanticsPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the SemanticsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new SemanticsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new SemanticsResourceFactoryImpl());
		}
		return registrations;
	}

} //SemanticsXMLProcessor
