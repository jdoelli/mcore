/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAbstract Update</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdate#getUpdatedObject <em>Updated Object</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getValue <em>Value</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getMode <em>Mode</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getAddSpecification <em>Add Specification</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdate#getObject <em>Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdate()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\':=\'\n'"
 * @generated
 */

public interface XUpdate extends XUpdateContainer {
	/**
	 * Returns the value of the '<em><b>Updated Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Updated Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Updated Object</em>' reference.
	 * @see #setUpdatedObject(XUpdatedObject)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_UpdatedObject()
	 * @model required="true"
	 * @generated
	 */
	XUpdatedObject getUpdatedObject();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getUpdatedObject <em>Updated Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Updated Object</em>' reference.
	 * @see #getUpdatedObject()
	 * @generated
	 */
	void setUpdatedObject(XUpdatedObject value);

	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(EStructuralFeature)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_Feature()
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='if updatedObject.oclIsUndefined() then OrderedSet{} else \r\n\r\nif updatedObject.object.oclIsUndefined() then\r\n   if updatedObject.classForCreation.oclIsUndefined() then OrderedSet{} else\r\n          updatedObject.classForCreation.eAllStructuralFeatures endif\r\n     else updatedObject.object.eClass().eAllStructuralFeatures endif\r\n  endif'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 * @generated
	 */
	EStructuralFeature getFeature();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(XValue)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_Value()
	 * @model containment="true" resolveProxies="true" required="true"
	 * @generated
	 */
	XValue getValue();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(XValue value);

	/**
	 * Returns the value of the '<em><b>Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.xocl.semantics.XUpdateMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mode</em>' attribute.
	 * @see org.xocl.semantics.XUpdateMode
	 * @see #setMode(XUpdateMode)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_Mode()
	 * @model
	 * @generated
	 */
	XUpdateMode getMode();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getMode <em>Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mode</em>' attribute.
	 * @see org.xocl.semantics.XUpdateMode
	 * @see #getMode()
	 * @generated
	 */
	void setMode(XUpdateMode value);

	/**
	 * Returns the value of the '<em><b>Contributes To</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate <em>Contributing Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Contributes To</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Contributes To</em>' reference.
	 * @see #isSetContributesTo()
	 * @see #unsetContributesTo()
	 * @see #setContributesTo(XUpdatedTypedElement)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_ContributesTo()
	 * @see org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate
	 * @model opposite="contributingUpdate" unsettable="true"
	 * @generated
	 */
	XUpdatedTypedElement getContributesTo();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Contributes To</em>' reference.
	 * @see #isSetContributesTo()
	 * @see #unsetContributesTo()
	 * @see #getContributesTo()
	 * @generated
	 */
	void setContributesTo(XUpdatedTypedElement value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetContributesTo()
	 * @see #getContributesTo()
	 * @see #setContributesTo(XUpdatedTypedElement)
	 * @generated
	 */
	void unsetContributesTo();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Contributes To</em>' reference is set.
	 * @see #unsetContributesTo()
	 * @see #getContributesTo()
	 * @see #setContributesTo(XUpdatedTypedElement)
	 * @generated
	 */
	boolean isSetContributesTo();

	/**
	 * Returns the value of the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' attribute.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #setObject(Object)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_Object()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='object'"
	 * @generated
	 */
	Object getObject();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' attribute.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see #getObject()
	 * @generated
	 */
	void setObject(Object value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdate#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObject()
	 * @see #getObject()
	 * @see #setObject(Object)
	 * @generated
	 */
	void unsetObject();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdate#getObject <em>Object</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object</em>' attribute is set.
	 * @see #unsetObject()
	 * @see #getObject()
	 * @see #setObject(Object)
	 * @generated
	 */
	boolean isSetObject();

	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #isSetOperation()
	 * @see #unsetOperation()
	 * @see #setOperation(EOperation)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_Operation()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='operation'"
	 * @generated
	 */
	EOperation getOperation();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #isSetOperation()
	 * @see #unsetOperation()
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(EOperation value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdate#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperation()
	 * @see #getOperation()
	 * @see #setOperation(EOperation)
	 * @generated
	 */
	void unsetOperation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdate#getOperation <em>Operation</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operation</em>' reference is set.
	 * @see #unsetOperation()
	 * @see #getOperation()
	 * @see #setOperation(EOperation)
	 * @generated
	 */
	boolean isSetOperation();

	/**
	 * Returns the value of the '<em><b>Add Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Add Specification</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Add Specification</em>' containment reference.
	 * @see #isSetAddSpecification()
	 * @see #unsetAddSpecification()
	 * @see #setAddSpecification(XAddSpecification)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdate_AddSpecification()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	XAddSpecification getAddSpecification();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdate#getAddSpecification <em>Add Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Add Specification</em>' containment reference.
	 * @see #isSetAddSpecification()
	 * @see #unsetAddSpecification()
	 * @see #getAddSpecification()
	 * @generated
	 */
	void setAddSpecification(XAddSpecification value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdate#getAddSpecification <em>Add Specification</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAddSpecification()
	 * @see #getAddSpecification()
	 * @see #setAddSpecification(XAddSpecification)
	 * @generated
	 */
	void unsetAddSpecification();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdate#getAddSpecification <em>Add Specification</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Add Specification</em>' containment reference is set.
	 * @see #unsetAddSpecification()
	 * @see #getAddSpecification()
	 * @see #setAddSpecification(XAddSpecification)
	 * @generated
	 */
	boolean isSetAddSpecification();

} // XUpdate
