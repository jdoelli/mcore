/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XUpdated Object</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getObjectUpdateType <em>Object Update Type</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getObject <em>Object</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getClassForCreation <em>Class For Creation</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getUpdatedFeature <em>Updated Feature</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getObjectIsFocusedAfterExecution <em>Object Is Focused After Execution</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getXUpdatedTypedElement <em>XUpdated Typed Element</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getUpdatedOperation <em>Updated Operation</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getPersistenceLocation <em>Persistence Location</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Object\'\n'"
 * @generated
 */

public interface XUpdatedObject extends XSemanticsElement {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' reference.
	 * @see #setObject(EObject)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_Object()
	 * @model required="true"
	 * @generated
	 */
	EObject getObject();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getObject <em>Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' reference.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(EObject value);

	/**
	 * Returns the value of the '<em><b>Updated Feature</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Updated Feature</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Updated Feature</em>' containment reference list.
	 * @see #isSetUpdatedFeature()
	 * @see #unsetUpdatedFeature()
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_UpdatedFeature()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<XUpdatedFeature> getUpdatedFeature();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getUpdatedFeature <em>Updated Feature</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUpdatedFeature()
	 * @see #getUpdatedFeature()
	 * @generated
	 */
	void unsetUpdatedFeature();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getUpdatedFeature <em>Updated Feature</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Updated Feature</em>' containment reference list is set.
	 * @see #unsetUpdatedFeature()
	 * @see #getUpdatedFeature()
	 * @generated
	 */
	boolean isSetUpdatedFeature();

	/**
	 * Returns the value of the '<em><b>Object Is Focused After Execution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Is Focused After Execution</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Is Focused After Execution</em>' attribute.
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_ObjectIsFocusedAfterExecution()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (eContainer().oclIsTypeOf(XTransition)) then\r\neContainer().oclAsType(XTransition)\r\n.focusObjects->notEmpty() and eContainer().oclAsType(XTransition)\r\n.focusObjects->notEmpty()->includes(self) else false endif'"
	 * @generated
	 */
	Boolean getObjectIsFocusedAfterExecution();

	/**
	 * Returns the value of the '<em><b>XUpdated Typed Element</b></em>' reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedTypedElement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>XUpdated Typed Element</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>XUpdated Typed Element</em>' reference list.
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_XUpdatedTypedElement()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='XUpdatedTypedElement'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(semantics::XUpdatedTypedElement)  = let chain: OrderedSet(semantics::XUpdatedOperation)  = updatedOperation->asOrderedSet() in\nchain->iterate(i:semantics::XUpdatedOperation; r: OrderedSet(semantics::XUpdatedTypedElement)=OrderedSet{} | if i.oclIsKindOf(semantics::XUpdatedTypedElement) then r->including(i.oclAsType(semantics::XUpdatedTypedElement))->asOrderedSet() \n else r endif)->union(updatedFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 * @generated
	 */
	EList<XUpdatedTypedElement> getXUpdatedTypedElement();

	/**
	 * Returns the value of the '<em><b>Updated Operation</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.semantics.XUpdatedOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Updated Operation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Updated Operation</em>' containment reference list.
	 * @see #isSetUpdatedOperation()
	 * @see #unsetUpdatedOperation()
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_UpdatedOperation()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updatedOperation'"
	 * @generated
	 */
	EList<XUpdatedOperation> getUpdatedOperation();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getUpdatedOperation <em>Updated Operation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUpdatedOperation()
	 * @see #getUpdatedOperation()
	 * @generated
	 */
	void unsetUpdatedOperation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getUpdatedOperation <em>Updated Operation</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Updated Operation</em>' containment reference list is set.
	 * @see #unsetUpdatedOperation()
	 * @see #getUpdatedOperation()
	 * @generated
	 */
	boolean isSetUpdatedOperation();

	/**
	 * Returns the value of the '<em><b>Persistence Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Persistence Location</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Persistence Location</em>' attribute.
	 * @see #isSetPersistenceLocation()
	 * @see #unsetPersistenceLocation()
	 * @see #setPersistenceLocation(String)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_PersistenceLocation()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='false'"
	 * @generated
	 */
	String getPersistenceLocation();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getPersistenceLocation <em>Persistence Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Persistence Location</em>' attribute.
	 * @see #isSetPersistenceLocation()
	 * @see #unsetPersistenceLocation()
	 * @see #getPersistenceLocation()
	 * @generated
	 */

	void setPersistenceLocation(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getPersistenceLocation <em>Persistence Location</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPersistenceLocation()
	 * @see #getPersistenceLocation()
	 * @see #setPersistenceLocation(String)
	 * @generated
	 */
	void unsetPersistenceLocation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getPersistenceLocation <em>Persistence Location</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Persistence Location</em>' attribute is set.
	 * @see #unsetPersistenceLocation()
	 * @see #getPersistenceLocation()
	 * @see #setPersistenceLocation(String)
	 * @generated
	 */
	boolean isSetPersistenceLocation();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Package</em>' attribute.
	 * @see #isSetAlternativePersistencePackage()
	 * @see #unsetAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(String)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_AlternativePersistencePackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='false'"
	 * @generated
	 */
	String getAlternativePersistencePackage();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Package</em>' attribute.
	 * @see #isSetAlternativePersistencePackage()
	 * @see #unsetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @generated
	 */

	void setAlternativePersistencePackage(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(String)
	 * @generated
	 */
	void unsetAlternativePersistencePackage();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Package</em>' attribute is set.
	 * @see #unsetAlternativePersistencePackage()
	 * @see #getAlternativePersistencePackage()
	 * @see #setAlternativePersistencePackage(String)
	 * @generated
	 */
	boolean isSetAlternativePersistencePackage();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Root</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Root</em>' attribute.
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(String)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_AlternativePersistenceRoot()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='false'"
	 * @generated
	 */
	String getAlternativePersistenceRoot();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Root</em>' attribute.
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @generated
	 */

	void setAlternativePersistenceRoot(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(String)
	 * @generated
	 */
	void unsetAlternativePersistenceRoot();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Root</em>' attribute is set.
	 * @see #unsetAlternativePersistenceRoot()
	 * @see #getAlternativePersistenceRoot()
	 * @see #setAlternativePersistenceRoot(String)
	 * @generated
	 */
	boolean isSetAlternativePersistenceRoot();

	/**
	 * Returns the value of the '<em><b>Alternative Persistence Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Alternative Persistence Reference</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Alternative Persistence Reference</em>' attribute.
	 * @see #isSetAlternativePersistenceReference()
	 * @see #unsetAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(String)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_AlternativePersistenceReference()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Persistence' createColumn='false'"
	 * @generated
	 */
	String getAlternativePersistenceReference();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Alternative Persistence Reference</em>' attribute.
	 * @see #isSetAlternativePersistenceReference()
	 * @see #unsetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @generated
	 */

	void setAlternativePersistenceReference(String value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(String)
	 * @generated
	 */
	void unsetAlternativePersistenceReference();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Alternative Persistence Reference</em>' attribute is set.
	 * @see #unsetAlternativePersistenceReference()
	 * @see #getAlternativePersistenceReference()
	 * @see #setAlternativePersistenceReference(String)
	 * @generated
	 */
	boolean isSetAlternativePersistenceReference();

	/**
	 * Returns the value of the '<em><b>Object Update Type</b></em>' attribute.
	 * The literals are from the enumeration {@link org.xocl.semantics.XObjectUpdateType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Update Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Update Type</em>' attribute.
	 * @see org.xocl.semantics.XObjectUpdateType
	 * @see #isSetObjectUpdateType()
	 * @see #unsetObjectUpdateType()
	 * @see #setObjectUpdateType(XObjectUpdateType)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_ObjectUpdateType()
	 * @model unsettable="true"
	 * @generated
	 */
	XObjectUpdateType getObjectUpdateType();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getObjectUpdateType <em>Object Update Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Update Type</em>' attribute.
	 * @see org.xocl.semantics.XObjectUpdateType
	 * @see #isSetObjectUpdateType()
	 * @see #unsetObjectUpdateType()
	 * @see #getObjectUpdateType()
	 * @generated
	 */
	void setObjectUpdateType(XObjectUpdateType value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getObjectUpdateType <em>Object Update Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObjectUpdateType()
	 * @see #getObjectUpdateType()
	 * @see #setObjectUpdateType(XObjectUpdateType)
	 * @generated
	 */
	void unsetObjectUpdateType();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getObjectUpdateType <em>Object Update Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object Update Type</em>' attribute is set.
	 * @see #unsetObjectUpdateType()
	 * @see #getObjectUpdateType()
	 * @see #setObjectUpdateType(XObjectUpdateType)
	 * @generated
	 */
	boolean isSetObjectUpdateType();

	/**
	 * Returns the value of the '<em><b>Class For Creation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class For Creation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class For Creation</em>' reference.
	 * @see #isSetClassForCreation()
	 * @see #unsetClassForCreation()
	 * @see #setClassForCreation(EClass)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedObject_ClassForCreation()
	 * @model unsettable="true"
	 * @generated
	 */
	EClass getClassForCreation();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedObject#getClassForCreation <em>Class For Creation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class For Creation</em>' reference.
	 * @see #isSetClassForCreation()
	 * @see #unsetClassForCreation()
	 * @see #getClassForCreation()
	 * @generated
	 */
	void setClassForCreation(EClass value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedObject#getClassForCreation <em>Class For Creation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetClassForCreation()
	 * @see #getClassForCreation()
	 * @see #setClassForCreation(EClass)
	 * @generated
	 */
	void unsetClassForCreation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedObject#getClassForCreation <em>Class For Creation</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Class For Creation</em>' reference is set.
	 * @see #unsetClassForCreation()
	 * @see #getClassForCreation()
	 * @see #setClassForCreation(EClass)
	 * @generated
	 */
	boolean isSetClassForCreation();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdatedFeature nextStateFeatureDefinitionFromFeature(EStructuralFeature updatedFeature);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model updatedOperationAnnotation="http://www.montages.com/mCore/MCore mName='updated Operation'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='nextStateOperationDefinitionFromOperation'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	XUpdatedOperation nextStateOperationDefinitionFromOperation(EOperation updatedOperation);

} // XUpdatedObject
