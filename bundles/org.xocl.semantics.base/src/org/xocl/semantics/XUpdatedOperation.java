
/**
 */
package org.xocl.semantics;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XUpdated Operation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XUpdatedOperation#getOperation <em>Operation</em>}</li>
 *   <li>{@link org.xocl.semantics.XUpdatedOperation#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedOperation()
 * @model
 * @generated
 */

public interface XUpdatedOperation extends EObject {
	/**
	 * Returns the value of the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operation</em>' reference.
	 * @see #isSetOperation()
	 * @see #unsetOperation()
	 * @see #setOperation(EOperation)
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedOperation_Operation()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='operation'"
	 * @generated
	 */
	EOperation getOperation();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XUpdatedOperation#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operation</em>' reference.
	 * @see #isSetOperation()
	 * @see #unsetOperation()
	 * @see #getOperation()
	 * @generated
	 */
	void setOperation(EOperation value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedOperation#getOperation <em>Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOperation()
	 * @see #getOperation()
	 * @see #setOperation(EOperation)
	 * @generated
	 */
	void unsetOperation();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedOperation#getOperation <em>Operation</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Operation</em>' reference is set.
	 * @see #unsetOperation()
	 * @see #getOperation()
	 * @see #setOperation(EOperation)
	 * @generated
	 */
	boolean isSetOperation();

	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Object}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' attribute list.
	 * @see #isSetParameter()
	 * @see #unsetParameter()
	 * @see org.xocl.semantics.SemanticsPackage#getXUpdatedOperation_Parameter()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='parameter'"
	 * @generated
	 */
	EList<Object> getParameter();

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XUpdatedOperation#getParameter <em>Parameter</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameter()
	 * @see #getParameter()
	 * @generated
	 */
	void unsetParameter();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XUpdatedOperation#getParameter <em>Parameter</em>}' attribute list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameter</em>' attribute list is set.
	 * @see #unsetParameter()
	 * @see #getParameter()
	 * @generated
	 */
	boolean isSetParameter();

} // XUpdatedOperation
