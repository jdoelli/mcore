package org.xocl.semantics.commands;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.change.ChangeDescription;
import org.eclipse.emf.edit.command.ChangeCommand;
import org.xocl.semantics.XUpdatedObject;
import org.xocl.semantics.XUpdatedOperation;

public class ChangeCommandWithOperationCall extends ChangeCommand {

	XUpdatedOperation myOp = null;
	EObject objectInvokingOperation = null;
	Object objectToBeFocused = null;

	public ChangeCommandWithOperationCall(Notifier notifier,
			XUpdatedObject updatedObject, XUpdatedOperation invokeOperation) {
		super(notifier);

		myOp = invokeOperation;
		objectInvokingOperation = updatedObject.getObject();

	}

	public Collection<?> getAffectedObjects() {

		if (!(objectToBeFocused instanceof EObject)
				&& !(objectToBeFocused instanceof List))
			return Collections.singleton(objectInvokingOperation);

		EObject objectToBeFocusedAsEObject = null;
		EObject objectContainer = null;

		if (objectToBeFocused instanceof EObject) {
			objectToBeFocusedAsEObject = (EObject) objectToBeFocused;
			objectContainer = objectToBeFocusedAsEObject.eContainer();
		}

		if (objectToBeFocused instanceof List) {
			List<?> objectToBeFocusedAsList = (List<?>) objectToBeFocused;
			if (objectToBeFocusedAsList.isEmpty())
				return Collections.singleton(objectInvokingOperation);
			if (!(objectToBeFocusedAsList.get(0) instanceof EObject))
				return Collections.singleton(objectInvokingOperation);

			objectToBeFocusedAsEObject = (EObject) ((((List<?>) objectToBeFocused))
					.get(0));
			objectContainer = objectToBeFocusedAsEObject.eContainer();
		}

		while (objectContainer != null) {
			if (objectContainer instanceof ChangeDescription)
				return Collections.singleton(objectInvokingOperation);
			objectContainer = objectContainer.eContainer();
		}

		return objectToBeFocused instanceof List
				? (List<Object>) objectToBeFocused
				: Collections.singleton(objectToBeFocused);

	};

	@Override
	protected void doExecute() {

		BasicEList<Object> l = null;
		if (myOp.getParameter() instanceof List) {

			EList<?> para = myOp.getParameter();
			l = new BasicEList<Object>(para);

		} else {

			l = new BasicEList<Object>();
			l.add(myOp.getParameter().get(0));

		}

		try {

			objectToBeFocused = objectInvokingOperation
					.eInvoke(myOp.getOperation(), l);

		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
