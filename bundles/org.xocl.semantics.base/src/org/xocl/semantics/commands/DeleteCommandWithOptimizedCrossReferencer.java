package org.xocl.semantics.commands;

import java.util.Collection;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.xocl.core.util.OptimizedUsageCrossReferencer;

/**
 * @see bug #666
 */
public class DeleteCommandWithOptimizedCrossReferencer extends DeleteCommand {

	private Map<EObject, Collection<Setting>> myUsages;
	
	public DeleteCommandWithOptimizedCrossReferencer(EditingDomain domain, Collection<?> collection) {
		super(domain, collection);
	}

	@Override
	protected Map<EObject, Collection<Setting>> findReferences(Collection<EObject> eObjects) {
		myUsages = new OptimizedUsageCrossReferencer(domain.getResourceSet()).findAllUsage(eObjects);
		return myUsages;
	}
	
	@Override
	public void execute() {
	    super.execute();
		removeProperties();
	}
	
	private void removeProperties() {
		myUsages.entrySet().forEach(entry -> {
			Optional.ofNullable(entry.getKey())
				.filter(eObject -> Objects.isNull(eObject.eResource()))
				.ifPresent(usagesEntry -> {
					entry.getValue().stream()
					.filter(setting -> setting.getEStructuralFeature().isChangeable())
					.map(Setting::getEObject)
//					.filter(MPropertyInstance.class::isInstance)
					.forEach(eObject -> appendAndExecute(RemoveCommand.create(domain, null, null, eObject)));
				});
		});
	}
}