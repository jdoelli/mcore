/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.SemanticsPackage
 * @generated
 */
public interface SemanticsFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SemanticsFactory eINSTANCE = org.xocl.semantics.impl.SemanticsFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>XSemantics</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XSemantics</em>'.
	 * @generated
	 */
	XSemantics createXSemantics();

	/**
	 * Returns a new object of class '<em>XTransition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XTransition</em>'.
	 * @generated
	 */
	XTransition createXTransition();

	/**
	 * Returns a new object of class '<em>XAdd Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XAdd Specification</em>'.
	 * @generated
	 */
	XAddSpecification createXAddSpecification();

	/**
	 * Returns a new object of class '<em>XAdditional Triggering Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XAdditional Triggering Specification</em>'.
	 * @generated
	 */
	XAdditionalTriggeringSpecification createXAdditionalTriggeringSpecification();

	/**
	 * Returns a new object of class '<em>XTriggered Update</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XTriggered Update</em>'.
	 * @generated
	 */
	XTriggeredUpdate createXTriggeredUpdate();

	/**
	 * Returns a new object of class '<em>XOriginal Triggering Specification</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XOriginal Triggering Specification</em>'.
	 * @generated
	 */
	XOriginalTriggeringSpecification createXOriginalTriggeringSpecification();

	/**
	 * Returns a new object of class '<em>XUpdated Object</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XUpdated Object</em>'.
	 * @generated
	 */
	XUpdatedObject createXUpdatedObject();

	/**
	 * Returns a new object of class '<em>XUpdated Feature</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XUpdated Feature</em>'.
	 * @generated
	 */
	XUpdatedFeature createXUpdatedFeature();

	/**
	 * Returns a new object of class '<em>XValue</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XValue</em>'.
	 * @generated
	 */
	XValue createXValue();

	/**
	 * Returns a new object of class '<em>XUpdated Operation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>XUpdated Operation</em>'.
	 * @generated
	 */
	XUpdatedOperation createXUpdatedOperation();

	/**
	 * Returns a new object of class '<em>DUmmy</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>DUmmy</em>'.
	 * @generated
	 */
	DUmmy createDUmmy();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	SemanticsPackage getSemanticsPackage();

} //SemanticsFactory
