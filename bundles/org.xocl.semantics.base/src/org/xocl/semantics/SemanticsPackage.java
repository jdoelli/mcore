/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xocl.semantics.SemanticsFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'XSemantics\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true' uuidAttributeName='_uuid'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.xocl'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface SemanticsPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.xocl.semantics.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "semantics";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.xocl.org/semantics/Semantics";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "semantics";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SemanticsPackage eINSTANCE = org.xocl.semantics.impl.SemanticsPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XSemanticsElementImpl <em>XSemantics Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XSemanticsElementImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXSemanticsElement()
	 * @generated
	 */
	int XSEMANTICS_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT__KIND_LABEL = 0;

	/**
	 * The number of structural features of the '<em>XSemantics Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL = 0;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT___INDENT_LEVEL = 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT___INDENTATION_SPACES = 2;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER = 3;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING = 4;

	/**
	 * The number of operations of the '<em>XSemantics Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_ELEMENT_OPERATION_COUNT = 5;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XSemanticsImpl <em>XSemantics</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XSemanticsImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXSemantics()
	 * @generated
	 */
	int XSEMANTICS = 1;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Transition</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS__TRANSITION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>XSemantics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The number of operations of the '<em>XSemantics</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XSEMANTICS_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdateContainerImpl <em>XUpdate Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdateContainerImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdateContainer()
	 * @generated
	 */
	int XUPDATE_CONTAINER = 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER__CONTAINING_TRANSITION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>XUpdate Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Create Next State New Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = XSEMANTICS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Next State Object Definition From Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Delete Object In Next State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Add And Merge Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE = XSEMANTICS_ELEMENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Invoke Operation Call</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 8;

	/**
	 * The number of operations of the '<em>XUpdate Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_CONTAINER_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XTransitionImpl <em>XTransition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XTransitionImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTransition()
	 * @generated
	 */
	int XTRANSITION = 3;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__KIND_LABEL = XUPDATE_CONTAINER__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__CONTAINING_TRANSITION = XUPDATE_CONTAINER__CONTAINING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Execute Automatically</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__EXECUTE_AUTOMATICALLY = XUPDATE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Execute Now</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__EXECUTE_NOW = XUPDATE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__EXECUTION_TIME = XUPDATE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Triggering Update</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__TRIGGERING_UPDATE = XUPDATE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Updated Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__UPDATED_OBJECT = XUPDATE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Dangling Removed Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__DANGLING_REMOVED_OBJECT = XUPDATE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>All Updates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__ALL_UPDATES = XUPDATE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Focus Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION__FOCUS_OBJECTS = XUPDATE_CONTAINER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>XTransition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION_FEATURE_COUNT = XUPDATE_CONTAINER_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___RENDERED_KIND_LABEL = XUPDATE_CONTAINER___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___INDENT_LEVEL = XUPDATE_CONTAINER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___INDENTATION_SPACES = XUPDATE_CONTAINER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___INDENTATION_SPACES__INTEGER = XUPDATE_CONTAINER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___STRING_OR_MISSING__STRING = XUPDATE_CONTAINER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Create Next State New Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS;

	/**
	 * The operation id for the '<em>Next State Object Definition From Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT;

	/**
	 * The operation id for the '<em>Delete Object In Next State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add And Merge Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___ADD_AND_MERGE_UPDATE__XUPDATE = XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Invoke Operation Call</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Execute Now$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION___EXECUTE_NOW$_UPDATE__BOOLEAN = XUPDATE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>XTransition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRANSITION_OPERATION_COUNT = XUPDATE_CONTAINER_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdateImpl <em>XUpdate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdateImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdate()
	 * @generated
	 */
	int XUPDATE = 4;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__KIND_LABEL = XUPDATE_CONTAINER__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__CONTAINING_TRANSITION = XUPDATE_CONTAINER__CONTAINING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Updated Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__UPDATED_OBJECT = XUPDATE_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__FEATURE = XUPDATE_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__OPERATION = XUPDATE_CONTAINER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__VALUE = XUPDATE_CONTAINER_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__MODE = XUPDATE_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Add Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__ADD_SPECIFICATION = XUPDATE_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Contributes To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__CONTRIBUTES_TO = XUPDATE_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE__OBJECT = XUPDATE_CONTAINER_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>XUpdate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_FEATURE_COUNT = XUPDATE_CONTAINER_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___RENDERED_KIND_LABEL = XUPDATE_CONTAINER___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___INDENT_LEVEL = XUPDATE_CONTAINER___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___INDENTATION_SPACES = XUPDATE_CONTAINER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___INDENTATION_SPACES__INTEGER = XUPDATE_CONTAINER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___STRING_OR_MISSING__STRING = XUPDATE_CONTAINER___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Create Next State New Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS;

	/**
	 * The operation id for the '<em>Next State Object Definition From Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT;

	/**
	 * The operation id for the '<em>Delete Object In Next State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add And Merge Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___ADD_AND_MERGE_UPDATE__XUPDATE = XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Invoke Operation Call</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The number of operations of the '<em>XUpdate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATE_OPERATION_COUNT = XUPDATE_CONTAINER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XAddSpecificationImpl <em>XAdd Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XAddSpecificationImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAddSpecification()
	 * @generated
	 */
	int XADD_SPECIFICATION = 5;

	/**
	 * The feature id for the '<em><b>Add Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADD_SPECIFICATION__ADD_KIND = 0;

	/**
	 * The feature id for the '<em><b>Before Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADD_SPECIFICATION__BEFORE_OBJECT = 1;

	/**
	 * The feature id for the '<em><b>After Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADD_SPECIFICATION__AFTER_OBJECT = 2;

	/**
	 * The number of structural features of the '<em>XAdd Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADD_SPECIFICATION_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>XAdd Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADD_SPECIFICATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl <em>XAbstract Triggering Update</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAbstractTriggeringUpdate()
	 * @generated
	 */
	int XABSTRACT_TRIGGERING_UPDATE = 6;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__KIND_LABEL = XUPDATE__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__CONTAINING_TRANSITION = XUPDATE__CONTAINING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Updated Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__UPDATED_OBJECT = XUPDATE__UPDATED_OBJECT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__FEATURE = XUPDATE__FEATURE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__OPERATION = XUPDATE__OPERATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__VALUE = XUPDATE__VALUE;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__MODE = XUPDATE__MODE;

	/**
	 * The feature id for the '<em><b>Add Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__ADD_SPECIFICATION = XUPDATE__ADD_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Contributes To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__CONTRIBUTES_TO = XUPDATE__CONTRIBUTES_TO;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__OBJECT = XUPDATE__OBJECT;

	/**
	 * The feature id for the '<em><b>Triggered Owned Update</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE = XUPDATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Additional Triggering Of This</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS = XUPDATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Processsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__PROCESSSED = XUPDATE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Additionally Triggering Update</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE = XUPDATE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>All Owned Updates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES = XUPDATE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>XAbstract Triggering Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE_FEATURE_COUNT = XUPDATE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___RENDERED_KIND_LABEL = XUPDATE___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___INDENT_LEVEL = XUPDATE___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___INDENTATION_SPACES = XUPDATE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___INDENTATION_SPACES__INTEGER = XUPDATE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___STRING_OR_MISSING__STRING = XUPDATE___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Create Next State New Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = XUPDATE___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS;

	/**
	 * The operation id for the '<em>Next State Object Definition From Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = XUPDATE___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT;

	/**
	 * The operation id for the '<em>Delete Object In Next State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = XUPDATE___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add And Merge Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___ADD_AND_MERGE_UPDATE__XUPDATE = XUPDATE___ADD_AND_MERGE_UPDATE__XUPDATE;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XUPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Invoke Operation Call</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XUPDATE___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The number of operations of the '<em>XAbstract Triggering Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XABSTRACT_TRIGGERING_UPDATE_OPERATION_COUNT = XUPDATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XTriggeringSpecificationImpl <em>XTriggering Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XTriggeringSpecificationImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTriggeringSpecification()
	 * @generated
	 */
	int XTRIGGERING_SPECIFICATION = 7;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Feature Of Triggering Update Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Nameof Triggering Update Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Triggering Update</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>XTriggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The number of operations of the '<em>XTriggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERING_SPECIFICATION_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XAdditionalTriggeringSpecificationImpl <em>XAdditional Triggering Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XAdditionalTriggeringSpecificationImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAdditionalTriggeringSpecification()
	 * @generated
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION = 8;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION__KIND_LABEL = XTRIGGERING_SPECIFICATION__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Feature Of Triggering Update Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION = XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Nameof Triggering Update Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION = XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Triggering Update</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION__TRIGGERING_UPDATE = XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE;

	/**
	 * The feature id for the '<em><b>Additional Triggering Update</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE = XTRIGGERING_SPECIFICATION_FEATURE_COUNT
			+ 0;

	/**
	 * The number of structural features of the '<em>XAdditional Triggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION_FEATURE_COUNT = XTRIGGERING_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION___RENDERED_KIND_LABEL = XTRIGGERING_SPECIFICATION___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION___INDENT_LEVEL = XTRIGGERING_SPECIFICATION___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION___INDENTATION_SPACES = XTRIGGERING_SPECIFICATION___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION___INDENTATION_SPACES__INTEGER = XTRIGGERING_SPECIFICATION___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION___STRING_OR_MISSING__STRING = XTRIGGERING_SPECIFICATION___STRING_OR_MISSING__STRING;

	/**
	 * The number of operations of the '<em>XAdditional Triggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XADDITIONAL_TRIGGERING_SPECIFICATION_OPERATION_COUNT = XTRIGGERING_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XTriggeredUpdateImpl <em>XTriggered Update</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XTriggeredUpdateImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTriggeredUpdate()
	 * @generated
	 */
	int XTRIGGERED_UPDATE = 9;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__KIND_LABEL = XABSTRACT_TRIGGERING_UPDATE__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Containing Transition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__CONTAINING_TRANSITION = XABSTRACT_TRIGGERING_UPDATE__CONTAINING_TRANSITION;

	/**
	 * The feature id for the '<em><b>Updated Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__UPDATED_OBJECT = XABSTRACT_TRIGGERING_UPDATE__UPDATED_OBJECT;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__FEATURE = XABSTRACT_TRIGGERING_UPDATE__FEATURE;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__OPERATION = XABSTRACT_TRIGGERING_UPDATE__OPERATION;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__VALUE = XABSTRACT_TRIGGERING_UPDATE__VALUE;

	/**
	 * The feature id for the '<em><b>Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__MODE = XABSTRACT_TRIGGERING_UPDATE__MODE;

	/**
	 * The feature id for the '<em><b>Add Specification</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ADD_SPECIFICATION = XABSTRACT_TRIGGERING_UPDATE__ADD_SPECIFICATION;

	/**
	 * The feature id for the '<em><b>Contributes To</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__CONTRIBUTES_TO = XABSTRACT_TRIGGERING_UPDATE__CONTRIBUTES_TO;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__OBJECT = XABSTRACT_TRIGGERING_UPDATE__OBJECT;

	/**
	 * The feature id for the '<em><b>Triggered Owned Update</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__TRIGGERED_OWNED_UPDATE = XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE;

	/**
	 * The feature id for the '<em><b>Additional Triggering Of This</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS = XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS;

	/**
	 * The feature id for the '<em><b>Processsed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__PROCESSSED = XABSTRACT_TRIGGERING_UPDATE__PROCESSSED;

	/**
	 * The feature id for the '<em><b>Additionally Triggering Update</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE = XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE;

	/**
	 * The feature id for the '<em><b>All Owned Updates</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ALL_OWNED_UPDATES = XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES;

	/**
	 * The feature id for the '<em><b>Original Triggering Of This</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS = XABSTRACT_TRIGGERING_UPDATE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Originally Triggering Update</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE = XABSTRACT_TRIGGERING_UPDATE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Additional Triggering By This</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS = XABSTRACT_TRIGGERING_UPDATE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>XTriggered Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE_FEATURE_COUNT = XABSTRACT_TRIGGERING_UPDATE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___RENDERED_KIND_LABEL = XABSTRACT_TRIGGERING_UPDATE___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___INDENT_LEVEL = XABSTRACT_TRIGGERING_UPDATE___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___INDENTATION_SPACES = XABSTRACT_TRIGGERING_UPDATE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___INDENTATION_SPACES__INTEGER = XABSTRACT_TRIGGERING_UPDATE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___STRING_OR_MISSING__STRING = XABSTRACT_TRIGGERING_UPDATE___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Create Next State New Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = XABSTRACT_TRIGGERING_UPDATE___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS;

	/**
	 * The operation id for the '<em>Next State Object Definition From Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = XABSTRACT_TRIGGERING_UPDATE___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT;

	/**
	 * The operation id for the '<em>Delete Object In Next State</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = XABSTRACT_TRIGGERING_UPDATE___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XABSTRACT_TRIGGERING_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Reference Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XABSTRACT_TRIGGERING_UPDATE___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add And Merge Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___ADD_AND_MERGE_UPDATE__XUPDATE = XABSTRACT_TRIGGERING_UPDATE___ADD_AND_MERGE_UPDATE__XUPDATE;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XABSTRACT_TRIGGERING_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The operation id for the '<em>Add Attribute Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = XABSTRACT_TRIGGERING_UPDATE___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Invoke Operation Call</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = XABSTRACT_TRIGGERING_UPDATE___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT;

	/**
	 * The number of operations of the '<em>XTriggered Update</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XTRIGGERED_UPDATE_OPERATION_COUNT = XABSTRACT_TRIGGERING_UPDATE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XOriginalTriggeringSpecificationImpl <em>XOriginal Triggering Specification</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XOriginalTriggeringSpecificationImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXOriginalTriggeringSpecification()
	 * @generated
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION = 10;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION__KIND_LABEL = XTRIGGERING_SPECIFICATION__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Feature Of Triggering Update Annotation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION = XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Nameof Triggering Update Annotation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION = XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Triggering Update</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION__TRIGGERING_UPDATE = XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE;

	/**
	 * The feature id for the '<em><b>Containing Triggered Update</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE = XTRIGGERING_SPECIFICATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>XOriginal Triggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION_FEATURE_COUNT = XTRIGGERING_SPECIFICATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION___RENDERED_KIND_LABEL = XTRIGGERING_SPECIFICATION___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION___INDENT_LEVEL = XTRIGGERING_SPECIFICATION___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION___INDENTATION_SPACES = XTRIGGERING_SPECIFICATION___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION___INDENTATION_SPACES__INTEGER = XTRIGGERING_SPECIFICATION___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION___STRING_OR_MISSING__STRING = XTRIGGERING_SPECIFICATION___STRING_OR_MISSING__STRING;

	/**
	 * The number of operations of the '<em>XOriginal Triggering Specification</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XORIGINAL_TRIGGERING_SPECIFICATION_OPERATION_COUNT = XTRIGGERING_SPECIFICATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdatedObjectImpl <em>XUpdated Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdatedObjectImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedObject()
	 * @generated
	 */
	int XUPDATED_OBJECT = 11;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Object Update Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__OBJECT_UPDATE_TYPE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__OBJECT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Class For Creation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__CLASS_FOR_CREATION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Updated Feature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__UPDATED_FEATURE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Object Is Focused After Execution</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>XUpdated Typed Element</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Updated Operation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__UPDATED_OPERATION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Persistence Location</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__PERSISTENCE_LOCATION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Alternative Persistence Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Alternative Persistence Root</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Alternative Persistence Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>XUpdated Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Next State Feature Definition From Feature</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___NEXT_STATE_FEATURE_DEFINITION_FROM_FEATURE__ESTRUCTURALFEATURE = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Next State Operation Definition From Operation</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT___NEXT_STATE_OPERATION_DEFINITION_FROM_OPERATION__EOPERATION = XSEMANTICS_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>XUpdated Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OBJECT_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdatedFeatureImpl <em>XUpdated Feature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdatedFeatureImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedFeature()
	 * @generated
	 */
	int XUPDATED_FEATURE = 12;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Contributing Update</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__CONTRIBUTING_UPDATE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Expected To Be Added Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__EXPECTED_TO_BE_ADDED_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Expected To Be Removed Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__EXPECTED_TO_BE_REMOVED_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Expected Resulting Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__EXPECTED_RESULTING_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Expectations Met</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__EXPECTATIONS_MET = XSEMANTICS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Old Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__OLD_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>New Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__NEW_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Inconsistent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__INCONSISTENT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__EXECUTED = XSEMANTICS_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Towards End Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__TOWARDS_END_INSERT_POSITION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Towards End Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__TOWARDS_END_INSERT_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>In The Middle Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__IN_THE_MIDDLE_INSERT_POSITION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>In The Middle Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__IN_THE_MIDDLE_INSERT_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Towards Beginning Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__TOWARDS_BEGINNING_INSERT_POSITION = XSEMANTICS_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Towards Beginning Insert Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__TOWARDS_BEGINNING_INSERT_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>As First Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__AS_FIRST_INSERT_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>As Last Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__AS_LAST_INSERT_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Delete Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__DELETE_VALUES = XSEMANTICS_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE__FEATURE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The number of structural features of the '<em>XUpdated Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 19;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The number of operations of the '<em>XUpdated Feature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_FEATURE_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XValueImpl <em>XValue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XValueImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXValue()
	 * @generated
	 */
	int XVALUE = 13;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__KIND_LABEL = XSEMANTICS_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Updated Object Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__UPDATED_OBJECT_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Boolean Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__BOOLEAN_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Integer Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__INTEGER_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Double Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__DOUBLE_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>String Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__STRING_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Date Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__DATE_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__LITERAL_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__IS_CORRECT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Create Clone</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__CREATE_CLONE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>EObject Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__EOBJECT_VALUE = XSEMANTICS_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Parameter Arguments</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE__PARAMETER_ARGUMENTS = XSEMANTICS_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>XValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE_FEATURE_COUNT = XSEMANTICS_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>Rendered Kind Label</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___RENDERED_KIND_LABEL = XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___INDENT_LEVEL = XSEMANTICS_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___INDENTATION_SPACES = XSEMANTICS_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___INDENTATION_SPACES__INTEGER = XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___STRING_OR_MISSING__STRING = XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>Add Other Values</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___ADD_OTHER_VALUES__XVALUE = XSEMANTICS_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Disjoint From</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE___DISJOINT_FROM__XVALUE = XSEMANTICS_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>XValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XVALUE_OPERATION_COUNT = XSEMANTICS_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl <em>XUpdated Typed Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdatedTypedElementImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedTypedElement()
	 * @generated
	 */
	int XUPDATED_TYPED_ELEMENT = 14;

	/**
	 * The feature id for the '<em><b>Contributing Update</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE = 0;

	/**
	 * The feature id for the '<em><b>Expected To Be Added Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE = 1;

	/**
	 * The feature id for the '<em><b>Expected To Be Removed Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE = 2;

	/**
	 * The feature id for the '<em><b>Expected Resulting Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE = 3;

	/**
	 * The feature id for the '<em><b>Expectations Met</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET = 4;

	/**
	 * The feature id for the '<em><b>Old Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__OLD_VALUE = 5;

	/**
	 * The feature id for the '<em><b>New Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__NEW_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Inconsistent</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__INCONSISTENT = 7;

	/**
	 * The feature id for the '<em><b>Executed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__EXECUTED = 8;

	/**
	 * The feature id for the '<em><b>Towards End Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION = 9;

	/**
	 * The feature id for the '<em><b>Towards End Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES = 10;

	/**
	 * The feature id for the '<em><b>In The Middle Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION = 11;

	/**
	 * The feature id for the '<em><b>In The Middle Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES = 12;

	/**
	 * The feature id for the '<em><b>Towards Beginning Insert Position</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION = 13;

	/**
	 * The feature id for the '<em><b>Towards Beginning Insert Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES = 14;

	/**
	 * The feature id for the '<em><b>As First Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES = 15;

	/**
	 * The feature id for the '<em><b>As Last Insert Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES = 16;

	/**
	 * The feature id for the '<em><b>Delete Values</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT__DELETE_VALUES = 17;

	/**
	 * The number of structural features of the '<em>XUpdated Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT_FEATURE_COUNT = 18;

	/**
	 * The number of operations of the '<em>XUpdated Typed Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_TYPED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.XUpdatedOperationImpl <em>XUpdated Operation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.XUpdatedOperationImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedOperation()
	 * @generated
	 */
	int XUPDATED_OPERATION = 15;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OPERATION__OPERATION = 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OPERATION__PARAMETER = 1;

	/**
	 * The number of structural features of the '<em>XUpdated Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OPERATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>XUpdated Operation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int XUPDATED_OPERATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.impl.DUmmyImpl <em>DUmmy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.impl.DUmmyImpl
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getDUmmy()
	 * @generated
	 */
	int DUMMY = 16;

	/**
	 * The feature id for the '<em><b>Text1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY__TEXT1 = 0;

	/**
	 * The number of structural features of the '<em>DUmmy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>DUmmy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.XAddUpdateMode <em>XAdd Update Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.XAddUpdateMode
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAddUpdateMode()
	 * @generated
	 */
	int XADD_UPDATE_MODE = 17;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.XObjectUpdateType <em>XObject Update Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.XObjectUpdateType
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXObjectUpdateType()
	 * @generated
	 */
	int XOBJECT_UPDATE_TYPE = 18;

	/**
	 * The meta object id for the '{@link org.xocl.semantics.XUpdateMode <em>XUpdate Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.xocl.semantics.XUpdateMode
	 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdateMode()
	 * @generated
	 */
	int XUPDATE_MODE = 19;

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XSemantics <em>XSemantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XSemantics</em>'.
	 * @see org.xocl.semantics.XSemantics
	 * @generated
	 */
	EClass getXSemantics();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XSemantics#getTransition <em>Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Transition</em>'.
	 * @see org.xocl.semantics.XSemantics#getTransition()
	 * @see #getXSemantics()
	 * @generated
	 */
	EReference getXSemantics_Transition();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdateContainer <em>XUpdate Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdate Container</em>'.
	 * @see org.xocl.semantics.XUpdateContainer
	 * @generated
	 */
	EClass getXUpdateContainer();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdateContainer#getContainingTransition <em>Containing Transition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Transition</em>'.
	 * @see org.xocl.semantics.XUpdateContainer#getContainingTransition()
	 * @see #getXUpdateContainer()
	 * @generated
	 */
	EReference getXUpdateContainer_ContainingTransition();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#addReferenceUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject) <em>Add Reference Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Reference Update</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#addReferenceUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject)
	 * @generated
	 */
	EOperation getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#addReferenceUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Add Reference Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Reference Update</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#addReferenceUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EReference, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#addAndMergeUpdate(org.xocl.semantics.XUpdate) <em>Add And Merge Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add And Merge Update</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#addAndMergeUpdate(org.xocl.semantics.XUpdate)
	 * @generated
	 */
	EOperation getXUpdateContainer__AddAndMergeUpdate__XUpdate();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#addAttributeUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EAttribute, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject) <em>Add Attribute Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Attribute Update</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#addAttributeUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EAttribute, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject)
	 * @generated
	 */
	EOperation getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#addAttributeUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EAttribute, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject, java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Add Attribute Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Attribute Update</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#addAttributeUpdate(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EAttribute, org.xocl.semantics.XUpdateMode, org.xocl.semantics.XAddUpdateMode, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#invokeOperationCall(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EOperation, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject) <em>Invoke Operation Call</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Operation Call</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#invokeOperationCall(org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.EOperation, java.lang.Object, org.xocl.semantics.XUpdatedObject, org.xocl.semantics.XUpdatedObject)
	 * @generated
	 */
	EOperation getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#createNextStateNewObject(org.eclipse.emf.ecore.EClass) <em>Create Next State New Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Next State New Object</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#createNextStateNewObject(org.eclipse.emf.ecore.EClass)
	 * @generated
	 */
	EOperation getXUpdateContainer__CreateNextStateNewObject__EClass();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#nextStateObjectDefinitionFromObject(org.eclipse.emf.ecore.EObject) <em>Next State Object Definition From Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Next State Object Definition From Object</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#nextStateObjectDefinitionFromObject(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getXUpdateContainer__NextStateObjectDefinitionFromObject__EObject();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdateContainer#deleteObjectInNextState(org.eclipse.emf.ecore.EObject) <em>Delete Object In Next State</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Delete Object In Next State</em>' operation.
	 * @see org.xocl.semantics.XUpdateContainer#deleteObjectInNextState(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	EOperation getXUpdateContainer__DeleteObjectInNextState__EObject();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XTransition <em>XTransition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XTransition</em>'.
	 * @see org.xocl.semantics.XTransition
	 * @generated
	 */
	EClass getXTransition();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XTransition#getExecuteAutomatically <em>Execute Automatically</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execute Automatically</em>'.
	 * @see org.xocl.semantics.XTransition#getExecuteAutomatically()
	 * @see #getXTransition()
	 * @generated
	 */
	EAttribute getXTransition_ExecuteAutomatically();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XTransition#getExecuteNow <em>Execute Now</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execute Now</em>'.
	 * @see org.xocl.semantics.XTransition#getExecuteNow()
	 * @see #getXTransition()
	 * @generated
	 */
	EAttribute getXTransition_ExecuteNow();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XTransition#getExecutionTime <em>Execution Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Execution Time</em>'.
	 * @see org.xocl.semantics.XTransition#getExecutionTime()
	 * @see #getXTransition()
	 * @generated
	 */
	EAttribute getXTransition_ExecutionTime();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XTransition#getTriggeringUpdate <em>Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Triggering Update</em>'.
	 * @see org.xocl.semantics.XTransition#getTriggeringUpdate()
	 * @see #getXTransition()
	 * @generated
	 */
	EReference getXTransition_TriggeringUpdate();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XTransition#getUpdatedObject <em>Updated Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Updated Object</em>'.
	 * @see org.xocl.semantics.XTransition#getUpdatedObject()
	 * @see #getXTransition()
	 * @generated
	 */
	EReference getXTransition_UpdatedObject();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XTransition#getDanglingRemovedObject <em>Dangling Removed Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dangling Removed Object</em>'.
	 * @see org.xocl.semantics.XTransition#getDanglingRemovedObject()
	 * @see #getXTransition()
	 * @generated
	 */
	EReference getXTransition_DanglingRemovedObject();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XTransition#getAllUpdates <em>All Updates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Updates</em>'.
	 * @see org.xocl.semantics.XTransition#getAllUpdates()
	 * @see #getXTransition()
	 * @generated
	 */
	EReference getXTransition_AllUpdates();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XTransition#getFocusObjects <em>Focus Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Focus Objects</em>'.
	 * @see org.xocl.semantics.XTransition#getFocusObjects()
	 * @see #getXTransition()
	 * @generated
	 */
	EReference getXTransition_FocusObjects();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XTransition#executeNow$Update(java.lang.Boolean) <em>Execute Now$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Execute Now$ Update</em>' operation.
	 * @see org.xocl.semantics.XTransition#executeNow$Update(java.lang.Boolean)
	 * @generated
	 */
	EOperation getXTransition__ExecuteNow$Update__Boolean();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdate <em>XUpdate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdate</em>'.
	 * @see org.xocl.semantics.XUpdate
	 * @generated
	 */
	EClass getXUpdate();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdate#getUpdatedObject <em>Updated Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Updated Object</em>'.
	 * @see org.xocl.semantics.XUpdate#getUpdatedObject()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_UpdatedObject();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdate#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.xocl.semantics.XUpdate#getFeature()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_Feature();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdate#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Value</em>'.
	 * @see org.xocl.semantics.XUpdate#getValue()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_Value();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdate#getMode <em>Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mode</em>'.
	 * @see org.xocl.semantics.XUpdate#getMode()
	 * @see #getXUpdate()
	 * @generated
	 */
	EAttribute getXUpdate_Mode();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdate#getContributesTo <em>Contributes To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Contributes To</em>'.
	 * @see org.xocl.semantics.XUpdate#getContributesTo()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_ContributesTo();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdate#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see org.xocl.semantics.XUpdate#getObject()
	 * @see #getXUpdate()
	 * @generated
	 */
	EAttribute getXUpdate_Object();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdate#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see org.xocl.semantics.XUpdate#getOperation()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_Operation();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdate#getAddSpecification <em>Add Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Add Specification</em>'.
	 * @see org.xocl.semantics.XUpdate#getAddSpecification()
	 * @see #getXUpdate()
	 * @generated
	 */
	EReference getXUpdate_AddSpecification();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XAddSpecification <em>XAdd Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XAdd Specification</em>'.
	 * @see org.xocl.semantics.XAddSpecification
	 * @generated
	 */
	EClass getXAddSpecification();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XAddSpecification#getBeforeObject <em>Before Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Before Object</em>'.
	 * @see org.xocl.semantics.XAddSpecification#getBeforeObject()
	 * @see #getXAddSpecification()
	 * @generated
	 */
	EReference getXAddSpecification_BeforeObject();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XAddSpecification#getAfterObject <em>After Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>After Object</em>'.
	 * @see org.xocl.semantics.XAddSpecification#getAfterObject()
	 * @see #getXAddSpecification()
	 * @generated
	 */
	EReference getXAddSpecification_AfterObject();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XAddSpecification#getAddKind <em>Add Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Add Kind</em>'.
	 * @see org.xocl.semantics.XAddSpecification#getAddKind()
	 * @see #getXAddSpecification()
	 * @generated
	 */
	EAttribute getXAddSpecification_AddKind();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XAbstractTriggeringUpdate <em>XAbstract Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XAbstract Triggering Update</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate
	 * @generated
	 */
	EClass getXAbstractTriggeringUpdate();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate <em>Triggered Owned Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Triggered Owned Update</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getTriggeredOwnedUpdate()
	 * @see #getXAbstractTriggeringUpdate()
	 * @generated
	 */
	EReference getXAbstractTriggeringUpdate_TriggeredOwnedUpdate();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionalTriggeringOfThis <em>Additional Triggering Of This</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Additional Triggering Of This</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionalTriggeringOfThis()
	 * @see #getXAbstractTriggeringUpdate()
	 * @generated
	 */
	EReference getXAbstractTriggeringUpdate_AdditionalTriggeringOfThis();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed <em>Processsed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Processsed</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getProcesssed()
	 * @see #getXAbstractTriggeringUpdate()
	 * @generated
	 */
	EAttribute getXAbstractTriggeringUpdate_Processsed();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionallyTriggeringUpdate <em>Additionally Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Additionally Triggering Update</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getAdditionallyTriggeringUpdate()
	 * @see #getXAbstractTriggeringUpdate()
	 * @generated
	 */
	EReference getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XAbstractTriggeringUpdate#getAllOwnedUpdates <em>All Owned Updates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Owned Updates</em>'.
	 * @see org.xocl.semantics.XAbstractTriggeringUpdate#getAllOwnedUpdates()
	 * @see #getXAbstractTriggeringUpdate()
	 * @generated
	 */
	EReference getXAbstractTriggeringUpdate_AllOwnedUpdates();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XTriggeringSpecification <em>XTriggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XTriggering Specification</em>'.
	 * @see org.xocl.semantics.XTriggeringSpecification
	 * @generated
	 */
	EClass getXTriggeringSpecification();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XTriggeringSpecification#getFeatureOfTriggeringUpdateAnnotation <em>Feature Of Triggering Update Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature Of Triggering Update Annotation</em>'.
	 * @see org.xocl.semantics.XTriggeringSpecification#getFeatureOfTriggeringUpdateAnnotation()
	 * @see #getXTriggeringSpecification()
	 * @generated
	 */
	EReference getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation <em>Nameof Triggering Update Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nameof Triggering Update Annotation</em>'.
	 * @see org.xocl.semantics.XTriggeringSpecification#getNameofTriggeringUpdateAnnotation()
	 * @see #getXTriggeringSpecification()
	 * @generated
	 */
	EAttribute getXTriggeringSpecification_NameofTriggeringUpdateAnnotation();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XTriggeringSpecification#getTriggeringUpdate <em>Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Triggering Update</em>'.
	 * @see org.xocl.semantics.XTriggeringSpecification#getTriggeringUpdate()
	 * @see #getXTriggeringSpecification()
	 * @generated
	 */
	EReference getXTriggeringSpecification_TriggeringUpdate();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XAdditionalTriggeringSpecification <em>XAdditional Triggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XAdditional Triggering Specification</em>'.
	 * @see org.xocl.semantics.XAdditionalTriggeringSpecification
	 * @generated
	 */
	EClass getXAdditionalTriggeringSpecification();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate <em>Additional Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Additional Triggering Update</em>'.
	 * @see org.xocl.semantics.XAdditionalTriggeringSpecification#getAdditionalTriggeringUpdate()
	 * @see #getXAdditionalTriggeringSpecification()
	 * @generated
	 */
	EReference getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XTriggeredUpdate <em>XTriggered Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XTriggered Update</em>'.
	 * @see org.xocl.semantics.XTriggeredUpdate
	 * @generated
	 */
	EClass getXTriggeredUpdate();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis <em>Original Triggering Of This</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Original Triggering Of This</em>'.
	 * @see org.xocl.semantics.XTriggeredUpdate#getOriginalTriggeringOfThis()
	 * @see #getXTriggeredUpdate()
	 * @generated
	 */
	EReference getXTriggeredUpdate_OriginalTriggeringOfThis();

	/**
	 * Returns the meta object for the container reference '{@link org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate <em>Originally Triggering Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Originally Triggering Update</em>'.
	 * @see org.xocl.semantics.XTriggeredUpdate#getOriginallyTriggeringUpdate()
	 * @see #getXTriggeredUpdate()
	 * @generated
	 */
	EReference getXTriggeredUpdate_OriginallyTriggeringUpdate();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis <em>Additional Triggering By This</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Additional Triggering By This</em>'.
	 * @see org.xocl.semantics.XTriggeredUpdate#getAdditionalTriggeringByThis()
	 * @see #getXTriggeredUpdate()
	 * @generated
	 */
	EReference getXTriggeredUpdate_AdditionalTriggeringByThis();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XOriginalTriggeringSpecification <em>XOriginal Triggering Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XOriginal Triggering Specification</em>'.
	 * @see org.xocl.semantics.XOriginalTriggeringSpecification
	 * @generated
	 */
	EClass getXOriginalTriggeringSpecification();

	/**
	 * Returns the meta object for the container reference '{@link org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate <em>Containing Triggered Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Triggered Update</em>'.
	 * @see org.xocl.semantics.XOriginalTriggeringSpecification#getContainingTriggeredUpdate()
	 * @see #getXOriginalTriggeringSpecification()
	 * @generated
	 */
	EReference getXOriginalTriggeringSpecification_ContainingTriggeredUpdate();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdatedObject <em>XUpdated Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdated Object</em>'.
	 * @see org.xocl.semantics.XUpdatedObject
	 * @generated
	 */
	EClass getXUpdatedObject();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdatedObject#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getObject()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EReference getXUpdatedObject_Object();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XUpdatedObject#getUpdatedFeature <em>Updated Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Updated Feature</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getUpdatedFeature()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EReference getXUpdatedObject_UpdatedFeature();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getObjectIsFocusedAfterExecution <em>Object Is Focused After Execution</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Is Focused After Execution</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getObjectIsFocusedAfterExecution()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_ObjectIsFocusedAfterExecution();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XUpdatedObject#getXUpdatedTypedElement <em>XUpdated Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>XUpdated Typed Element</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getXUpdatedTypedElement()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EReference getXUpdatedObject_XUpdatedTypedElement();

	/**
	 * Returns the meta object for the containment reference list '{@link org.xocl.semantics.XUpdatedObject#getUpdatedOperation <em>Updated Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Updated Operation</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getUpdatedOperation()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EReference getXUpdatedObject_UpdatedOperation();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getPersistenceLocation <em>Persistence Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Persistence Location</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getPersistenceLocation()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_PersistenceLocation();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage <em>Alternative Persistence Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alternative Persistence Package</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getAlternativePersistencePackage()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_AlternativePersistencePackage();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot <em>Alternative Persistence Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alternative Persistence Root</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getAlternativePersistenceRoot()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_AlternativePersistenceRoot();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference <em>Alternative Persistence Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Alternative Persistence Reference</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getAlternativePersistenceReference()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_AlternativePersistenceReference();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedObject#getObjectUpdateType <em>Object Update Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object Update Type</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getObjectUpdateType()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EAttribute getXUpdatedObject_ObjectUpdateType();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdatedObject#getClassForCreation <em>Class For Creation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class For Creation</em>'.
	 * @see org.xocl.semantics.XUpdatedObject#getClassForCreation()
	 * @see #getXUpdatedObject()
	 * @generated
	 */
	EReference getXUpdatedObject_ClassForCreation();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdatedObject#nextStateFeatureDefinitionFromFeature(org.eclipse.emf.ecore.EStructuralFeature) <em>Next State Feature Definition From Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Next State Feature Definition From Feature</em>' operation.
	 * @see org.xocl.semantics.XUpdatedObject#nextStateFeatureDefinitionFromFeature(org.eclipse.emf.ecore.EStructuralFeature)
	 * @generated
	 */
	EOperation getXUpdatedObject__NextStateFeatureDefinitionFromFeature__EStructuralFeature();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XUpdatedObject#nextStateOperationDefinitionFromOperation(org.eclipse.emf.ecore.EOperation) <em>Next State Operation Definition From Operation</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Next State Operation Definition From Operation</em>' operation.
	 * @see org.xocl.semantics.XUpdatedObject#nextStateOperationDefinitionFromOperation(org.eclipse.emf.ecore.EOperation)
	 * @generated
	 */
	EOperation getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdatedFeature <em>XUpdated Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdated Feature</em>'.
	 * @see org.xocl.semantics.XUpdatedFeature
	 * @generated
	 */
	EClass getXUpdatedFeature();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdatedFeature#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see org.xocl.semantics.XUpdatedFeature#getFeature()
	 * @see #getXUpdatedFeature()
	 * @generated
	 */
	EReference getXUpdatedFeature_Feature();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XValue <em>XValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XValue</em>'.
	 * @see org.xocl.semantics.XValue
	 * @generated
	 */
	EClass getXValue();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XValue#getUpdatedObjectValue <em>Updated Object Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Updated Object Value</em>'.
	 * @see org.xocl.semantics.XValue#getUpdatedObjectValue()
	 * @see #getXValue()
	 * @generated
	 */
	EReference getXValue_UpdatedObjectValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getBooleanValue <em>Boolean Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Boolean Value</em>'.
	 * @see org.xocl.semantics.XValue#getBooleanValue()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_BooleanValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getIntegerValue <em>Integer Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Integer Value</em>'.
	 * @see org.xocl.semantics.XValue#getIntegerValue()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_IntegerValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getDoubleValue <em>Double Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Double Value</em>'.
	 * @see org.xocl.semantics.XValue#getDoubleValue()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_DoubleValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getStringValue <em>String Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>String Value</em>'.
	 * @see org.xocl.semantics.XValue#getStringValue()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_StringValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getDateValue <em>Date Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Date Value</em>'.
	 * @see org.xocl.semantics.XValue#getDateValue()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_DateValue();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XValue#getLiteralValue <em>Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Literal Value</em>'.
	 * @see org.xocl.semantics.XValue#getLiteralValue()
	 * @see #getXValue()
	 * @generated
	 */
	EReference getXValue_LiteralValue();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XValue#getIsCorrect <em>Is Correct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Correct</em>'.
	 * @see org.xocl.semantics.XValue#getIsCorrect()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_IsCorrect();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XValue#getCreateClone <em>Create Clone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Create Clone</em>'.
	 * @see org.xocl.semantics.XValue#getCreateClone()
	 * @see #getXValue()
	 * @generated
	 */
	EReference getXValue_CreateClone();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XValue#getEObjectValue <em>EObject Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>EObject Value</em>'.
	 * @see org.xocl.semantics.XValue#getEObjectValue()
	 * @see #getXValue()
	 * @generated
	 */
	EReference getXValue_EObjectValue();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XValue#getParameterArguments <em>Parameter Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Parameter Arguments</em>'.
	 * @see org.xocl.semantics.XValue#getParameterArguments()
	 * @see #getXValue()
	 * @generated
	 */
	EAttribute getXValue_ParameterArguments();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XValue#addOtherValues(org.xocl.semantics.XValue) <em>Add Other Values</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Other Values</em>' operation.
	 * @see org.xocl.semantics.XValue#addOtherValues(org.xocl.semantics.XValue)
	 * @generated
	 */
	EOperation getXValue__AddOtherValues__XValue();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XValue#disjointFrom(org.xocl.semantics.XValue) <em>Disjoint From</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Disjoint From</em>' operation.
	 * @see org.xocl.semantics.XValue#disjointFrom(org.xocl.semantics.XValue)
	 * @generated
	 */
	EOperation getXValue__DisjointFrom__XValue();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdatedTypedElement <em>XUpdated Typed Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdated Typed Element</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement
	 * @generated
	 */
	EClass getXUpdatedTypedElement();

	/**
	 * Returns the meta object for the reference list '{@link org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate <em>Contributing Update</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Contributing Update</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getContributingUpdate()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_ContributingUpdate();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue <em>Expected To Be Added Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expected To Be Added Value</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeAddedValue()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_ExpectedToBeAddedValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue <em>Expected To Be Removed Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expected To Be Removed Value</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getExpectedToBeRemovedValue()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_ExpectedToBeRemovedValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue <em>Expected Resulting Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Expected Resulting Value</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getExpectedResultingValue()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_ExpectedResultingValue();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet <em>Expectations Met</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expectations Met</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getExpectationsMet()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_ExpectationsMet();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getOldValue <em>Old Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Old Value</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getOldValue()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_OldValue();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getNewValue <em>New Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>New Value</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getNewValue()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_NewValue();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getInconsistent <em>Inconsistent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Inconsistent</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getInconsistent()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_Inconsistent();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getExecuted <em>Executed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Executed</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getExecuted()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_Executed();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition <em>Towards End Insert Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Towards End Insert Position</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertPosition()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_TowardsEndInsertPosition();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues <em>Towards End Insert Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Towards End Insert Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getTowardsEndInsertValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_TowardsEndInsertValues();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition <em>In The Middle Insert Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>In The Middle Insert Position</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertPosition()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_InTheMiddleInsertPosition();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues <em>In The Middle Insert Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>In The Middle Insert Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getInTheMiddleInsertValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_InTheMiddleInsertValues();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition <em>Towards Beginning Insert Position</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Towards Beginning Insert Position</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertPosition()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EAttribute getXUpdatedTypedElement_TowardsBeginningInsertPosition();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues <em>Towards Beginning Insert Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Towards Beginning Insert Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getTowardsBeginningInsertValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_TowardsBeginningInsertValues();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues <em>As First Insert Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>As First Insert Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getAsFirstInsertValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_AsFirstInsertValues();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues <em>As Last Insert Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>As Last Insert Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getAsLastInsertValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_AsLastInsertValues();

	/**
	 * Returns the meta object for the containment reference '{@link org.xocl.semantics.XUpdatedTypedElement#getDeleteValues <em>Delete Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Delete Values</em>'.
	 * @see org.xocl.semantics.XUpdatedTypedElement#getDeleteValues()
	 * @see #getXUpdatedTypedElement()
	 * @generated
	 */
	EReference getXUpdatedTypedElement_DeleteValues();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XUpdatedOperation <em>XUpdated Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XUpdated Operation</em>'.
	 * @see org.xocl.semantics.XUpdatedOperation
	 * @generated
	 */
	EClass getXUpdatedOperation();

	/**
	 * Returns the meta object for the reference '{@link org.xocl.semantics.XUpdatedOperation#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see org.xocl.semantics.XUpdatedOperation#getOperation()
	 * @see #getXUpdatedOperation()
	 * @generated
	 */
	EReference getXUpdatedOperation_Operation();

	/**
	 * Returns the meta object for the attribute list '{@link org.xocl.semantics.XUpdatedOperation#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Parameter</em>'.
	 * @see org.xocl.semantics.XUpdatedOperation#getParameter()
	 * @see #getXUpdatedOperation()
	 * @generated
	 */
	EAttribute getXUpdatedOperation_Parameter();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.DUmmy <em>DUmmy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DUmmy</em>'.
	 * @see org.xocl.semantics.DUmmy
	 * @generated
	 */
	EClass getDUmmy();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.DUmmy#getText1 <em>Text1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Text1</em>'.
	 * @see org.xocl.semantics.DUmmy#getText1()
	 * @see #getDUmmy()
	 * @generated
	 */
	EAttribute getDUmmy_Text1();

	/**
	 * Returns the meta object for enum '{@link org.xocl.semantics.XAddUpdateMode <em>XAdd Update Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>XAdd Update Mode</em>'.
	 * @see org.xocl.semantics.XAddUpdateMode
	 * @generated
	 */
	EEnum getXAddUpdateMode();

	/**
	 * Returns the meta object for enum '{@link org.xocl.semantics.XObjectUpdateType <em>XObject Update Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>XObject Update Type</em>'.
	 * @see org.xocl.semantics.XObjectUpdateType
	 * @generated
	 */
	EEnum getXObjectUpdateType();

	/**
	 * Returns the meta object for class '{@link org.xocl.semantics.XSemanticsElement <em>XSemantics Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>XSemantics Element</em>'.
	 * @see org.xocl.semantics.XSemanticsElement
	 * @generated
	 */
	EClass getXSemanticsElement();

	/**
	 * Returns the meta object for the attribute '{@link org.xocl.semantics.XSemanticsElement#getKindLabel <em>Kind Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind Label</em>'.
	 * @see org.xocl.semantics.XSemanticsElement#getKindLabel()
	 * @see #getXSemanticsElement()
	 * @generated
	 */
	EAttribute getXSemanticsElement_KindLabel();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XSemanticsElement#renderedKindLabel() <em>Rendered Kind Label</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Rendered Kind Label</em>' operation.
	 * @see org.xocl.semantics.XSemanticsElement#renderedKindLabel()
	 * @generated
	 */
	EOperation getXSemanticsElement__RenderedKindLabel();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XSemanticsElement#indentLevel() <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indent Level</em>' operation.
	 * @see org.xocl.semantics.XSemanticsElement#indentLevel()
	 * @generated
	 */
	EOperation getXSemanticsElement__IndentLevel();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XSemanticsElement#indentationSpaces() <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.xocl.semantics.XSemanticsElement#indentationSpaces()
	 * @generated
	 */
	EOperation getXSemanticsElement__IndentationSpaces();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XSemanticsElement#indentationSpaces(java.lang.Integer) <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.xocl.semantics.XSemanticsElement#indentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getXSemanticsElement__IndentationSpaces__Integer();

	/**
	 * Returns the meta object for the '{@link org.xocl.semantics.XSemanticsElement#stringOrMissing(java.lang.String) <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Or Missing</em>' operation.
	 * @see org.xocl.semantics.XSemanticsElement#stringOrMissing(java.lang.String)
	 * @generated
	 */
	EOperation getXSemanticsElement__StringOrMissing__String();

	/**
	 * Returns the meta object for enum '{@link org.xocl.semantics.XUpdateMode <em>XUpdate Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>XUpdate Mode</em>'.
	 * @see org.xocl.semantics.XUpdateMode
	 * @generated
	 */
	EEnum getXUpdateMode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SemanticsFactory getSemanticsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XSemanticsImpl <em>XSemantics</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XSemanticsImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXSemantics()
		 * @generated
		 */
		EClass XSEMANTICS = eINSTANCE.getXSemantics();

		/**
		 * The meta object literal for the '<em><b>Transition</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XSEMANTICS__TRANSITION = eINSTANCE.getXSemantics_Transition();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdateContainerImpl <em>XUpdate Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdateContainerImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdateContainer()
		 * @generated
		 */
		EClass XUPDATE_CONTAINER = eINSTANCE.getXUpdateContainer();

		/**
		 * The meta object literal for the '<em><b>Containing Transition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE_CONTAINER__CONTAINING_TRANSITION = eINSTANCE.getXUpdateContainer_ContainingTransition();

		/**
		 * The meta object literal for the '<em><b>Add Reference Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = eINSTANCE
				.getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Add Reference Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___ADD_REFERENCE_UPDATE__EOBJECT_EREFERENCE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = eINSTANCE
				.getXUpdateContainer__AddReferenceUpdate__EObject_EReference_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add And Merge Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___ADD_AND_MERGE_UPDATE__XUPDATE = eINSTANCE
				.getXUpdateContainer__AddAndMergeUpdate__XUpdate();

		/**
		 * The meta object literal for the '<em><b>Add Attribute Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = eINSTANCE
				.getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Add Attribute Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___ADD_ATTRIBUTE_UPDATE__EOBJECT_EATTRIBUTE_XUPDATEMODE_XADDUPDATEMODE_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT_STRING_STRING_STRING_STRING = eINSTANCE
				.getXUpdateContainer__AddAttributeUpdate__EObject_EAttribute_XUpdateMode_XAddUpdateMode_Object_XUpdatedObject_XUpdatedObject_String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Invoke Operation Call</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___INVOKE_OPERATION_CALL__EOBJECT_EOPERATION_OBJECT_XUPDATEDOBJECT_XUPDATEDOBJECT = eINSTANCE
				.getXUpdateContainer__InvokeOperationCall__EObject_EOperation_Object_XUpdatedObject_XUpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Create Next State New Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___CREATE_NEXT_STATE_NEW_OBJECT__ECLASS = eINSTANCE
				.getXUpdateContainer__CreateNextStateNewObject__EClass();

		/**
		 * The meta object literal for the '<em><b>Next State Object Definition From Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___NEXT_STATE_OBJECT_DEFINITION_FROM_OBJECT__EOBJECT = eINSTANCE
				.getXUpdateContainer__NextStateObjectDefinitionFromObject__EObject();

		/**
		 * The meta object literal for the '<em><b>Delete Object In Next State</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATE_CONTAINER___DELETE_OBJECT_IN_NEXT_STATE__EOBJECT = eINSTANCE
				.getXUpdateContainer__DeleteObjectInNextState__EObject();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XTransitionImpl <em>XTransition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XTransitionImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTransition()
		 * @generated
		 */
		EClass XTRANSITION = eINSTANCE.getXTransition();

		/**
		 * The meta object literal for the '<em><b>Execute Automatically</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XTRANSITION__EXECUTE_AUTOMATICALLY = eINSTANCE.getXTransition_ExecuteAutomatically();

		/**
		 * The meta object literal for the '<em><b>Execute Now</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XTRANSITION__EXECUTE_NOW = eINSTANCE.getXTransition_ExecuteNow();

		/**
		 * The meta object literal for the '<em><b>Execution Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XTRANSITION__EXECUTION_TIME = eINSTANCE.getXTransition_ExecutionTime();

		/**
		 * The meta object literal for the '<em><b>Triggering Update</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRANSITION__TRIGGERING_UPDATE = eINSTANCE.getXTransition_TriggeringUpdate();

		/**
		 * The meta object literal for the '<em><b>Updated Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRANSITION__UPDATED_OBJECT = eINSTANCE.getXTransition_UpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Dangling Removed Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRANSITION__DANGLING_REMOVED_OBJECT = eINSTANCE.getXTransition_DanglingRemovedObject();

		/**
		 * The meta object literal for the '<em><b>All Updates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRANSITION__ALL_UPDATES = eINSTANCE.getXTransition_AllUpdates();

		/**
		 * The meta object literal for the '<em><b>Focus Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRANSITION__FOCUS_OBJECTS = eINSTANCE.getXTransition_FocusObjects();

		/**
		 * The meta object literal for the '<em><b>Execute Now$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XTRANSITION___EXECUTE_NOW$_UPDATE__BOOLEAN = eINSTANCE.getXTransition__ExecuteNow$Update__Boolean();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdateImpl <em>XUpdate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdateImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdate()
		 * @generated
		 */
		EClass XUPDATE = eINSTANCE.getXUpdate();

		/**
		 * The meta object literal for the '<em><b>Updated Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__UPDATED_OBJECT = eINSTANCE.getXUpdate_UpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__FEATURE = eINSTANCE.getXUpdate_Feature();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__VALUE = eINSTANCE.getXUpdate_Value();

		/**
		 * The meta object literal for the '<em><b>Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATE__MODE = eINSTANCE.getXUpdate_Mode();

		/**
		 * The meta object literal for the '<em><b>Contributes To</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__CONTRIBUTES_TO = eINSTANCE.getXUpdate_ContributesTo();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATE__OBJECT = eINSTANCE.getXUpdate_Object();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__OPERATION = eINSTANCE.getXUpdate_Operation();

		/**
		 * The meta object literal for the '<em><b>Add Specification</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATE__ADD_SPECIFICATION = eINSTANCE.getXUpdate_AddSpecification();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XAddSpecificationImpl <em>XAdd Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XAddSpecificationImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAddSpecification()
		 * @generated
		 */
		EClass XADD_SPECIFICATION = eINSTANCE.getXAddSpecification();

		/**
		 * The meta object literal for the '<em><b>Before Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XADD_SPECIFICATION__BEFORE_OBJECT = eINSTANCE.getXAddSpecification_BeforeObject();

		/**
		 * The meta object literal for the '<em><b>After Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XADD_SPECIFICATION__AFTER_OBJECT = eINSTANCE.getXAddSpecification_AfterObject();

		/**
		 * The meta object literal for the '<em><b>Add Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XADD_SPECIFICATION__ADD_KIND = eINSTANCE.getXAddSpecification_AddKind();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl <em>XAbstract Triggering Update</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XAbstractTriggeringUpdateImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAbstractTriggeringUpdate()
		 * @generated
		 */
		EClass XABSTRACT_TRIGGERING_UPDATE = eINSTANCE.getXAbstractTriggeringUpdate();

		/**
		 * The meta object literal for the '<em><b>Triggered Owned Update</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XABSTRACT_TRIGGERING_UPDATE__TRIGGERED_OWNED_UPDATE = eINSTANCE
				.getXAbstractTriggeringUpdate_TriggeredOwnedUpdate();

		/**
		 * The meta object literal for the '<em><b>Additional Triggering Of This</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XABSTRACT_TRIGGERING_UPDATE__ADDITIONAL_TRIGGERING_OF_THIS = eINSTANCE
				.getXAbstractTriggeringUpdate_AdditionalTriggeringOfThis();

		/**
		 * The meta object literal for the '<em><b>Processsed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XABSTRACT_TRIGGERING_UPDATE__PROCESSSED = eINSTANCE.getXAbstractTriggeringUpdate_Processsed();

		/**
		 * The meta object literal for the '<em><b>Additionally Triggering Update</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XABSTRACT_TRIGGERING_UPDATE__ADDITIONALLY_TRIGGERING_UPDATE = eINSTANCE
				.getXAbstractTriggeringUpdate_AdditionallyTriggeringUpdate();

		/**
		 * The meta object literal for the '<em><b>All Owned Updates</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XABSTRACT_TRIGGERING_UPDATE__ALL_OWNED_UPDATES = eINSTANCE
				.getXAbstractTriggeringUpdate_AllOwnedUpdates();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XTriggeringSpecificationImpl <em>XTriggering Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XTriggeringSpecificationImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTriggeringSpecification()
		 * @generated
		 */
		EClass XTRIGGERING_SPECIFICATION = eINSTANCE.getXTriggeringSpecification();

		/**
		 * The meta object literal for the '<em><b>Feature Of Triggering Update Annotation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRIGGERING_SPECIFICATION__FEATURE_OF_TRIGGERING_UPDATE_ANNOTATION = eINSTANCE
				.getXTriggeringSpecification_FeatureOfTriggeringUpdateAnnotation();

		/**
		 * The meta object literal for the '<em><b>Nameof Triggering Update Annotation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XTRIGGERING_SPECIFICATION__NAMEOF_TRIGGERING_UPDATE_ANNOTATION = eINSTANCE
				.getXTriggeringSpecification_NameofTriggeringUpdateAnnotation();

		/**
		 * The meta object literal for the '<em><b>Triggering Update</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRIGGERING_SPECIFICATION__TRIGGERING_UPDATE = eINSTANCE
				.getXTriggeringSpecification_TriggeringUpdate();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XAdditionalTriggeringSpecificationImpl <em>XAdditional Triggering Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XAdditionalTriggeringSpecificationImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAdditionalTriggeringSpecification()
		 * @generated
		 */
		EClass XADDITIONAL_TRIGGERING_SPECIFICATION = eINSTANCE.getXAdditionalTriggeringSpecification();

		/**
		 * The meta object literal for the '<em><b>Additional Triggering Update</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XADDITIONAL_TRIGGERING_SPECIFICATION__ADDITIONAL_TRIGGERING_UPDATE = eINSTANCE
				.getXAdditionalTriggeringSpecification_AdditionalTriggeringUpdate();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XTriggeredUpdateImpl <em>XTriggered Update</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XTriggeredUpdateImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXTriggeredUpdate()
		 * @generated
		 */
		EClass XTRIGGERED_UPDATE = eINSTANCE.getXTriggeredUpdate();

		/**
		 * The meta object literal for the '<em><b>Original Triggering Of This</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRIGGERED_UPDATE__ORIGINAL_TRIGGERING_OF_THIS = eINSTANCE
				.getXTriggeredUpdate_OriginalTriggeringOfThis();

		/**
		 * The meta object literal for the '<em><b>Originally Triggering Update</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRIGGERED_UPDATE__ORIGINALLY_TRIGGERING_UPDATE = eINSTANCE
				.getXTriggeredUpdate_OriginallyTriggeringUpdate();

		/**
		 * The meta object literal for the '<em><b>Additional Triggering By This</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XTRIGGERED_UPDATE__ADDITIONAL_TRIGGERING_BY_THIS = eINSTANCE
				.getXTriggeredUpdate_AdditionalTriggeringByThis();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XOriginalTriggeringSpecificationImpl <em>XOriginal Triggering Specification</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XOriginalTriggeringSpecificationImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXOriginalTriggeringSpecification()
		 * @generated
		 */
		EClass XORIGINAL_TRIGGERING_SPECIFICATION = eINSTANCE.getXOriginalTriggeringSpecification();

		/**
		 * The meta object literal for the '<em><b>Containing Triggered Update</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XORIGINAL_TRIGGERING_SPECIFICATION__CONTAINING_TRIGGERED_UPDATE = eINSTANCE
				.getXOriginalTriggeringSpecification_ContainingTriggeredUpdate();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdatedObjectImpl <em>XUpdated Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdatedObjectImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedObject()
		 * @generated
		 */
		EClass XUPDATED_OBJECT = eINSTANCE.getXUpdatedObject();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OBJECT__OBJECT = eINSTANCE.getXUpdatedObject_Object();

		/**
		 * The meta object literal for the '<em><b>Updated Feature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OBJECT__UPDATED_FEATURE = eINSTANCE.getXUpdatedObject_UpdatedFeature();

		/**
		 * The meta object literal for the '<em><b>Object Is Focused After Execution</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__OBJECT_IS_FOCUSED_AFTER_EXECUTION = eINSTANCE
				.getXUpdatedObject_ObjectIsFocusedAfterExecution();

		/**
		 * The meta object literal for the '<em><b>XUpdated Typed Element</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OBJECT__XUPDATED_TYPED_ELEMENT = eINSTANCE.getXUpdatedObject_XUpdatedTypedElement();

		/**
		 * The meta object literal for the '<em><b>Updated Operation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OBJECT__UPDATED_OPERATION = eINSTANCE.getXUpdatedObject_UpdatedOperation();

		/**
		 * The meta object literal for the '<em><b>Persistence Location</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__PERSISTENCE_LOCATION = eINSTANCE.getXUpdatedObject_PersistenceLocation();

		/**
		 * The meta object literal for the '<em><b>Alternative Persistence Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_PACKAGE = eINSTANCE
				.getXUpdatedObject_AlternativePersistencePackage();

		/**
		 * The meta object literal for the '<em><b>Alternative Persistence Root</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_ROOT = eINSTANCE
				.getXUpdatedObject_AlternativePersistenceRoot();

		/**
		 * The meta object literal for the '<em><b>Alternative Persistence Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__ALTERNATIVE_PERSISTENCE_REFERENCE = eINSTANCE
				.getXUpdatedObject_AlternativePersistenceReference();

		/**
		 * The meta object literal for the '<em><b>Object Update Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OBJECT__OBJECT_UPDATE_TYPE = eINSTANCE.getXUpdatedObject_ObjectUpdateType();

		/**
		 * The meta object literal for the '<em><b>Class For Creation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OBJECT__CLASS_FOR_CREATION = eINSTANCE.getXUpdatedObject_ClassForCreation();

		/**
		 * The meta object literal for the '<em><b>Next State Feature Definition From Feature</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATED_OBJECT___NEXT_STATE_FEATURE_DEFINITION_FROM_FEATURE__ESTRUCTURALFEATURE = eINSTANCE
				.getXUpdatedObject__NextStateFeatureDefinitionFromFeature__EStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Next State Operation Definition From Operation</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XUPDATED_OBJECT___NEXT_STATE_OPERATION_DEFINITION_FROM_OPERATION__EOPERATION = eINSTANCE
				.getXUpdatedObject__NextStateOperationDefinitionFromOperation__EOperation();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdatedFeatureImpl <em>XUpdated Feature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdatedFeatureImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedFeature()
		 * @generated
		 */
		EClass XUPDATED_FEATURE = eINSTANCE.getXUpdatedFeature();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_FEATURE__FEATURE = eINSTANCE.getXUpdatedFeature_Feature();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XValueImpl <em>XValue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XValueImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXValue()
		 * @generated
		 */
		EClass XVALUE = eINSTANCE.getXValue();

		/**
		 * The meta object literal for the '<em><b>Updated Object Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XVALUE__UPDATED_OBJECT_VALUE = eINSTANCE.getXValue_UpdatedObjectValue();

		/**
		 * The meta object literal for the '<em><b>Boolean Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__BOOLEAN_VALUE = eINSTANCE.getXValue_BooleanValue();

		/**
		 * The meta object literal for the '<em><b>Integer Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__INTEGER_VALUE = eINSTANCE.getXValue_IntegerValue();

		/**
		 * The meta object literal for the '<em><b>Double Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__DOUBLE_VALUE = eINSTANCE.getXValue_DoubleValue();

		/**
		 * The meta object literal for the '<em><b>String Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__STRING_VALUE = eINSTANCE.getXValue_StringValue();

		/**
		 * The meta object literal for the '<em><b>Date Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__DATE_VALUE = eINSTANCE.getXValue_DateValue();

		/**
		 * The meta object literal for the '<em><b>Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XVALUE__LITERAL_VALUE = eINSTANCE.getXValue_LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Is Correct</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__IS_CORRECT = eINSTANCE.getXValue_IsCorrect();

		/**
		 * The meta object literal for the '<em><b>Create Clone</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XVALUE__CREATE_CLONE = eINSTANCE.getXValue_CreateClone();

		/**
		 * The meta object literal for the '<em><b>EObject Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XVALUE__EOBJECT_VALUE = eINSTANCE.getXValue_EObjectValue();

		/**
		 * The meta object literal for the '<em><b>Parameter Arguments</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XVALUE__PARAMETER_ARGUMENTS = eINSTANCE.getXValue_ParameterArguments();

		/**
		 * The meta object literal for the '<em><b>Add Other Values</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XVALUE___ADD_OTHER_VALUES__XVALUE = eINSTANCE.getXValue__AddOtherValues__XValue();

		/**
		 * The meta object literal for the '<em><b>Disjoint From</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XVALUE___DISJOINT_FROM__XVALUE = eINSTANCE.getXValue__DisjointFrom__XValue();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdatedTypedElementImpl <em>XUpdated Typed Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdatedTypedElementImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedTypedElement()
		 * @generated
		 */
		EClass XUPDATED_TYPED_ELEMENT = eINSTANCE.getXUpdatedTypedElement();

		/**
		 * The meta object literal for the '<em><b>Contributing Update</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__CONTRIBUTING_UPDATE = eINSTANCE.getXUpdatedTypedElement_ContributingUpdate();

		/**
		 * The meta object literal for the '<em><b>Expected To Be Added Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_ADDED_VALUE = eINSTANCE
				.getXUpdatedTypedElement_ExpectedToBeAddedValue();

		/**
		 * The meta object literal for the '<em><b>Expected To Be Removed Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__EXPECTED_TO_BE_REMOVED_VALUE = eINSTANCE
				.getXUpdatedTypedElement_ExpectedToBeRemovedValue();

		/**
		 * The meta object literal for the '<em><b>Expected Resulting Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__EXPECTED_RESULTING_VALUE = eINSTANCE
				.getXUpdatedTypedElement_ExpectedResultingValue();

		/**
		 * The meta object literal for the '<em><b>Expectations Met</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__EXPECTATIONS_MET = eINSTANCE.getXUpdatedTypedElement_ExpectationsMet();

		/**
		 * The meta object literal for the '<em><b>Old Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__OLD_VALUE = eINSTANCE.getXUpdatedTypedElement_OldValue();

		/**
		 * The meta object literal for the '<em><b>New Value</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__NEW_VALUE = eINSTANCE.getXUpdatedTypedElement_NewValue();

		/**
		 * The meta object literal for the '<em><b>Inconsistent</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__INCONSISTENT = eINSTANCE.getXUpdatedTypedElement_Inconsistent();

		/**
		 * The meta object literal for the '<em><b>Executed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__EXECUTED = eINSTANCE.getXUpdatedTypedElement_Executed();

		/**
		 * The meta object literal for the '<em><b>Towards End Insert Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_POSITION = eINSTANCE
				.getXUpdatedTypedElement_TowardsEndInsertPosition();

		/**
		 * The meta object literal for the '<em><b>Towards End Insert Values</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__TOWARDS_END_INSERT_VALUES = eINSTANCE
				.getXUpdatedTypedElement_TowardsEndInsertValues();

		/**
		 * The meta object literal for the '<em><b>In The Middle Insert Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_POSITION = eINSTANCE
				.getXUpdatedTypedElement_InTheMiddleInsertPosition();

		/**
		 * The meta object literal for the '<em><b>In The Middle Insert Values</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__IN_THE_MIDDLE_INSERT_VALUES = eINSTANCE
				.getXUpdatedTypedElement_InTheMiddleInsertValues();

		/**
		 * The meta object literal for the '<em><b>Towards Beginning Insert Position</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_POSITION = eINSTANCE
				.getXUpdatedTypedElement_TowardsBeginningInsertPosition();

		/**
		 * The meta object literal for the '<em><b>Towards Beginning Insert Values</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__TOWARDS_BEGINNING_INSERT_VALUES = eINSTANCE
				.getXUpdatedTypedElement_TowardsBeginningInsertValues();

		/**
		 * The meta object literal for the '<em><b>As First Insert Values</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__AS_FIRST_INSERT_VALUES = eINSTANCE
				.getXUpdatedTypedElement_AsFirstInsertValues();

		/**
		 * The meta object literal for the '<em><b>As Last Insert Values</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__AS_LAST_INSERT_VALUES = eINSTANCE
				.getXUpdatedTypedElement_AsLastInsertValues();

		/**
		 * The meta object literal for the '<em><b>Delete Values</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_TYPED_ELEMENT__DELETE_VALUES = eINSTANCE.getXUpdatedTypedElement_DeleteValues();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XUpdatedOperationImpl <em>XUpdated Operation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XUpdatedOperationImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdatedOperation()
		 * @generated
		 */
		EClass XUPDATED_OPERATION = eINSTANCE.getXUpdatedOperation();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference XUPDATED_OPERATION__OPERATION = eINSTANCE.getXUpdatedOperation_Operation();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XUPDATED_OPERATION__PARAMETER = eINSTANCE.getXUpdatedOperation_Parameter();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.DUmmyImpl <em>DUmmy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.DUmmyImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getDUmmy()
		 * @generated
		 */
		EClass DUMMY = eINSTANCE.getDUmmy();

		/**
		 * The meta object literal for the '<em><b>Text1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DUMMY__TEXT1 = eINSTANCE.getDUmmy_Text1();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.XAddUpdateMode <em>XAdd Update Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.XAddUpdateMode
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXAddUpdateMode()
		 * @generated
		 */
		EEnum XADD_UPDATE_MODE = eINSTANCE.getXAddUpdateMode();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.XObjectUpdateType <em>XObject Update Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.XObjectUpdateType
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXObjectUpdateType()
		 * @generated
		 */
		EEnum XOBJECT_UPDATE_TYPE = eINSTANCE.getXObjectUpdateType();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.impl.XSemanticsElementImpl <em>XSemantics Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.impl.XSemanticsElementImpl
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXSemanticsElement()
		 * @generated
		 */
		EClass XSEMANTICS_ELEMENT = eINSTANCE.getXSemanticsElement();

		/**
		 * The meta object literal for the '<em><b>Kind Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute XSEMANTICS_ELEMENT__KIND_LABEL = eINSTANCE.getXSemanticsElement_KindLabel();

		/**
		 * The meta object literal for the '<em><b>Rendered Kind Label</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XSEMANTICS_ELEMENT___RENDERED_KIND_LABEL = eINSTANCE.getXSemanticsElement__RenderedKindLabel();

		/**
		 * The meta object literal for the '<em><b>Indent Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XSEMANTICS_ELEMENT___INDENT_LEVEL = eINSTANCE.getXSemanticsElement__IndentLevel();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XSEMANTICS_ELEMENT___INDENTATION_SPACES = eINSTANCE.getXSemanticsElement__IndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XSEMANTICS_ELEMENT___INDENTATION_SPACES__INTEGER = eINSTANCE
				.getXSemanticsElement__IndentationSpaces__Integer();

		/**
		 * The meta object literal for the '<em><b>String Or Missing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation XSEMANTICS_ELEMENT___STRING_OR_MISSING__STRING = eINSTANCE
				.getXSemanticsElement__StringOrMissing__String();

		/**
		 * The meta object literal for the '{@link org.xocl.semantics.XUpdateMode <em>XUpdate Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.xocl.semantics.XUpdateMode
		 * @see org.xocl.semantics.impl.SemanticsPackageImpl#getXUpdateMode()
		 * @generated
		 */
		EEnum XUPDATE_MODE = eINSTANCE.getXUpdateMode();

	}

} //SemanticsPackage
