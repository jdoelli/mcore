/**
 */
package org.xocl.semantics;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>XAdd Specification</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.semantics.XAddSpecification#getAddKind <em>Add Kind</em>}</li>
 *   <li>{@link org.xocl.semantics.XAddSpecification#getBeforeObject <em>Before Object</em>}</li>
 *   <li>{@link org.xocl.semantics.XAddSpecification#getAfterObject <em>After Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.semantics.SemanticsPackage#getXAddSpecification()
 * @model
 * @generated
 */

public interface XAddSpecification extends EObject {
	/**
	 * Returns the value of the '<em><b>Before Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Before Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Before Object</em>' reference.
	 * @see #isSetBeforeObject()
	 * @see #unsetBeforeObject()
	 * @see #setBeforeObject(XUpdatedObject)
	 * @see org.xocl.semantics.SemanticsPackage#getXAddSpecification_BeforeObject()
	 * @model unsettable="true"
	 * @generated
	 */
	XUpdatedObject getBeforeObject();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XAddSpecification#getBeforeObject <em>Before Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Before Object</em>' reference.
	 * @see #isSetBeforeObject()
	 * @see #unsetBeforeObject()
	 * @see #getBeforeObject()
	 * @generated
	 */
	void setBeforeObject(XUpdatedObject value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAddSpecification#getBeforeObject <em>Before Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBeforeObject()
	 * @see #getBeforeObject()
	 * @see #setBeforeObject(XUpdatedObject)
	 * @generated
	 */
	void unsetBeforeObject();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAddSpecification#getBeforeObject <em>Before Object</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Before Object</em>' reference is set.
	 * @see #unsetBeforeObject()
	 * @see #getBeforeObject()
	 * @see #setBeforeObject(XUpdatedObject)
	 * @generated
	 */
	boolean isSetBeforeObject();

	/**
	 * Returns the value of the '<em><b>After Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>After Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>After Object</em>' reference.
	 * @see #isSetAfterObject()
	 * @see #unsetAfterObject()
	 * @see #setAfterObject(XUpdatedObject)
	 * @see org.xocl.semantics.SemanticsPackage#getXAddSpecification_AfterObject()
	 * @model unsettable="true"
	 * @generated
	 */
	XUpdatedObject getAfterObject();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XAddSpecification#getAfterObject <em>After Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>After Object</em>' reference.
	 * @see #isSetAfterObject()
	 * @see #unsetAfterObject()
	 * @see #getAfterObject()
	 * @generated
	 */
	void setAfterObject(XUpdatedObject value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAddSpecification#getAfterObject <em>After Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAfterObject()
	 * @see #getAfterObject()
	 * @see #setAfterObject(XUpdatedObject)
	 * @generated
	 */
	void unsetAfterObject();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAddSpecification#getAfterObject <em>After Object</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>After Object</em>' reference is set.
	 * @see #unsetAfterObject()
	 * @see #getAfterObject()
	 * @see #setAfterObject(XUpdatedObject)
	 * @generated
	 */
	boolean isSetAfterObject();

	/**
	 * Returns the value of the '<em><b>Add Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link org.xocl.semantics.XAddUpdateMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Add Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Add Kind</em>' attribute.
	 * @see org.xocl.semantics.XAddUpdateMode
	 * @see #isSetAddKind()
	 * @see #unsetAddKind()
	 * @see #setAddKind(XAddUpdateMode)
	 * @see org.xocl.semantics.SemanticsPackage#getXAddSpecification_AddKind()
	 * @model unsettable="true"
	 * @generated
	 */
	XAddUpdateMode getAddKind();

	/** 
	 * Sets the value of the '{@link org.xocl.semantics.XAddSpecification#getAddKind <em>Add Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Add Kind</em>' attribute.
	 * @see org.xocl.semantics.XAddUpdateMode
	 * @see #isSetAddKind()
	 * @see #unsetAddKind()
	 * @see #getAddKind()
	 * @generated
	 */
	void setAddKind(XAddUpdateMode value);

	/**
	 * Unsets the value of the '{@link org.xocl.semantics.XAddSpecification#getAddKind <em>Add Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAddKind()
	 * @see #getAddKind()
	 * @see #setAddKind(XAddUpdateMode)
	 * @generated
	 */
	void unsetAddKind();

	/**
	 * Returns whether the value of the '{@link org.xocl.semantics.XAddSpecification#getAddKind <em>Add Kind</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Add Kind</em>' attribute is set.
	 * @see #unsetAddKind()
	 * @see #getAddKind()
	 * @see #setAddKind(XAddUpdateMode)
	 * @generated
	 */
	boolean isSetAddKind();

} // XAddSpecification
