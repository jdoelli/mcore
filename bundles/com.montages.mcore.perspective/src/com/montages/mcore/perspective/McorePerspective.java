package com.montages.mcore.perspective;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import com.montages.mcore.ui.views.EditorConfigExplorer;
import com.montages.mcore.ui.views.MComponentExplorer;

public class McorePerspective implements IPerspectiveFactory {

	public static final String ID = "com.montages.mcore.perspective";

	@Override
	public void createInitialLayout(IPageLayout layout) {
		IFolderLayout leftFolder = layout.createFolder("lde.left", IPageLayout.LEFT, 0.30f, layout.getEditorArea());
		leftFolder.addView(MComponentExplorer.ID);
		leftFolder.addView(EditorConfigExplorer.ID);
		leftFolder.addView(IPageLayout.ID_OUTLINE);

		IFolderLayout bottomFolder = layout.createFolder("lde.bottom", IPageLayout.BOTTOM, 0.70f, layout.getEditorArea());
		bottomFolder.addView(IPageLayout.ID_PROP_SHEET);
		bottomFolder.addView(IPageLayout.ID_PROJECT_EXPLORER);
	}

}
