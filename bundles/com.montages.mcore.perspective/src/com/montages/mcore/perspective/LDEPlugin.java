package com.montages.mcore.perspective;

import org.eclipse.core.runtime.Status;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;

import com.montages.mcore.perspective.menus.McoreControlContribution;

/**
 * The activator class controls the plug-in life cycle
 */
public class LDEPlugin extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "com.montages.mcore.perspective"; //$NON-NLS-1$

	// The shared instance
	private static LDEPlugin plugin;

	private McoreControlContribution contribution;
	
	/**
	 * The constructor
	 */
	public LDEPlugin() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static LDEPlugin getDefault() {
		return plugin;
	}
	
	public void logError(String msg, Exception ex) {
		Status status = new Status(Status.ERROR, PLUGIN_ID, Status.OK, msg, ex);
		getLog().log(status);
	}
	
	public void setMcoreControlContribution(McoreControlContribution contribution) {
		this.contribution = contribution;
	}
	
	public McoreControlContribution getMcoreControlContribution() {
		return contribution;
	}

}
