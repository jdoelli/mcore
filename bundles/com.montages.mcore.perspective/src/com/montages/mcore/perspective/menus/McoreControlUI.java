package com.montages.mcore.perspective.menus;

import java.util.Collection;
import java.util.Map;

import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IPartListener2;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchPartReference;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import com.montages.common.CommonPlugin;

public class McoreControlUI {

	private final Combo myCombo;
	private final ComboViewer myViewer;

	private MCoreControlUpdater myUpdater;
	private MCoreControlPartListener myPartListener;

	public McoreControlUI(Composite container) {
		myCombo = new Combo(container, SWT.BORDER | SWT.READ_ONLY
				| SWT.DROP_DOWN);

		myViewer = new ComboViewer(myCombo);
		myViewer.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof URI) {
					return getDisplayedLabel((URI)element);
				}
				return super.getText(element);
			}
		});
		myViewer.setContentProvider(new ArrayContentProvider());
		myViewer.setInput(getItems().toArray());

		if (myCombo.getItemCount() > 0) {
			myCombo.setText(myCombo.getItem(0));
		}

		myUpdater = new MCoreControlUpdater();
		ResourcesPlugin.getWorkspace().addResourceChangeListener(myUpdater);

		IWorkbenchPage page = getActiveWorkbenchPage();
		if (page != null) {
			myPartListener = new MCoreControlPartListener(page);
			myPartListener.updateFromActiveEditor();
		}
	}
	
	private static String getDisplayedLabel(URI uri) {
		URI withoutFile = uri.trimSegments(1);
		return withoutFile.toString().replace("mcore:", "http:");
	}

	private IWorkbenchPage getActiveWorkbenchPage() {
		IWorkbench workbench = PlatformUI.getWorkbench();
		if (workbench == null) {
			return null;
		}
		IWorkbenchWindow window = workbench.getActiveWorkbenchWindow();
		if (window == null) {
			return null;
		}
		return window.getActivePage();
	}

	/* package */Combo getCombo() {
		return myCombo;
	}

	/* package */void dispose() {
		if (myUpdater != null) {
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(
					myUpdater);
			myUpdater = null;
		}
		if (myPartListener != null) {
			myPartListener.dispose();
			myPartListener = null;
		}
	}

	protected Collection<URI> getItems() {
		return CommonPlugin.computeWorkspaceModels(null);
	}

	public URI getSelected() {
		ISelection selection = myViewer.getSelection();
		if (selection instanceof StructuredSelection) {
			Object first = ((StructuredSelection) selection).getFirstElement();
			if (first instanceof URI) {
				return (URI) first;
			}
		}
		return null;
	}
	
	private void setSelectedInternalURI(URI uri) {
		setSelectedURI(uri);
	}
	
	private void setSelectedURI(URI uri) {
		if (uri == null) {
			return;
		}
		
		String labelToFind = getDisplayedLabel(uri);
		
		if (myCombo.getSelectionIndex() >= 0) {
			String actual = myCombo.getItem(myCombo.getSelectionIndex());
			if (labelToFind.equals(actual)) {
				return;
			}
		}
		Map<URI, URI> uriMap = CommonPlugin.computeURIMap();
		for (Map.Entry<URI, URI> next : uriMap.entrySet()) {
			URI nextValue = next.getValue();
			if (uri.equals(nextValue)) {
				labelToFind  = getDisplayedLabel(next.getKey());
				break;
			}
		}
		
		int idx = myCombo.indexOf(labelToFind);
		if (idx >= 0) {
			myCombo.select(idx);
		}
	}
	
	private class MCoreControlPartListener implements IPartListener2 {

		private IWorkbenchPage myPage;

		public MCoreControlPartListener(IWorkbenchPage page) {
			myPage = page;
			myPage.addPartListener(this);
		}
		
		public void updateFromActiveEditor() {
			IWorkbenchPage page = getActiveWorkbenchPage();
			if (page == null) {
				return;
			}
			IEditorPart editor = page.getActiveEditor();
			if (editor == null) {
				return;
			}
			if (!isMCoreEditorId(editor.getSite().getId())) {
				return;
			}
			updateFromMcoreEditor(editor);
		}
		
		private boolean isMCoreEditorId(String id) {
			return "com.montages.mcore.presentation.McoreEditorID".equals(id);
		}
		
		private void updateFromPartReference(IWorkbenchPartReference partRef) {
			String id = partRef.getId();
			if (!isMCoreEditorId(id)) {
				return;
			}
			IWorkbenchPart part = partRef.getPart(false);
			if (part instanceof IEditorPart) {
				updateFromMcoreEditor((IEditorPart) part);
			}
		}
		
		private void updateFromMcoreEditor(IEditorPart editor) {
			IEditorInput input = editor.getEditorInput();
			updateFromEditorInput(input);
		}
		
		private void updateFromEditorInput(IEditorInput input) {
			if (input instanceof URIEditorInput) {
				URIEditorInput inputImpl = (URIEditorInput)input;
				URI uri = inputImpl.getURI();
				setSelectedURI(uri);
			}
		}

		@Override
		public void partActivated(IWorkbenchPartReference partRef) {
			updateFromPartReference(partRef);
		}

		@Override
		public void partOpened(IWorkbenchPartReference partRef) {
			updateFromPartReference(partRef);
		}

		@Override
		public void partBroughtToTop(IWorkbenchPartReference partRef) {
			//
		}

		@Override
		public void partClosed(IWorkbenchPartReference partRef) {
			//
		}

		@Override
		public void partDeactivated(IWorkbenchPartReference partRef) {
			//
		}

		@Override
		public void partHidden(IWorkbenchPartReference partRef) {
			//
		}

		@Override
		public void partVisible(IWorkbenchPartReference partRef) {
			//
		}

		@Override
		public void partInputChanged(IWorkbenchPartReference partRef) {
			//
		}
		
		public void dispose() {
			if (myPage != null) {
				myPage.removePartListener(this);
			}
			myPage = null;
		}

	}

	private class MCoreControlUpdater implements IResourceChangeListener,
			IResourceDeltaVisitor {

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			IResourceDelta delta = event.getDelta();
			if (delta == null) {
				return;
			}

			try {
				delta.accept(this);
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		@Override
		public boolean visit(IResourceDelta delta) throws CoreException {
			if (delta.getKind() == IResourceDelta.ADDED
					|| delta.getKind() == IResourceDelta.REMOVED
					|| delta.getKind() == IResourceDelta.CHANGED) {

				Display display = PlatformUI.getWorkbench().getDisplay();
				if (!display.isDisposed()) {
					display.asyncExec(new Runnable() {
						@Override
						public void run() {
							if (!myCombo.isDisposed()) {
								URI selected = getSelected();
								myViewer.setInput(getItems().toArray());
								if (myCombo.getItemCount() > 0) {
									myCombo.setText(myCombo.getItem(0));
									setSelectedInternalURI(selected);
								}
							}
						}
					});
				}
			}
			return false;
		}

	}

}
