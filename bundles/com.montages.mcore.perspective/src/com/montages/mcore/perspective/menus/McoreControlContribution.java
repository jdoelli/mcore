package com.montages.mcore.perspective.menus;

import org.eclipse.emf.common.util.URI;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.menus.WorkbenchWindowControlContribution;

import com.montages.mcore.perspective.LDEPlugin;

public class McoreControlContribution extends
		WorkbenchWindowControlContribution {

	private McoreControlUI myUI;

	@Override
	protected Control createControl(final Composite parent) {

		LDEPlugin.getDefault().setMcoreControlContribution(this);

		Composite container = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout(1, false);
		layout.marginTop = -1;
		layout.marginHeight = 0;
		layout.marginWidth = 0;
		container.setLayout(layout);

		GridData data = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		data.widthHint = 280;

		myUI = new McoreControlUI(container);
		myUI.getCombo().setLayoutData(data);
		return myUI.getCombo();
	}

	public URI getSelected() {
		return myUI == null ? null : myUI.getSelected();

	}

	@Override
	public void dispose() {
		myUI.dispose();
		super.dispose();
	}

}
