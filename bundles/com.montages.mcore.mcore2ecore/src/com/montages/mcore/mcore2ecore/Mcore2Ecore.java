package com.montages.mcore.mcore2ecore;

import static org.eclipse.emf.common.util.URI.createPlatformPluginURI;

import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xml.type.XMLTypePackage;
import org.eclipse.m2m.qvt.oml.BasicModelExtent;
import org.eclipse.m2m.qvt.oml.ExecutionContextImpl;
import org.eclipse.m2m.qvt.oml.ExecutionDiagnostic;
import org.eclipse.m2m.qvt.oml.TransformationExecutor;
import org.eclipse.m2m.qvt.oml.util.WriterLog;

import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.util.Dependencies;

/**
 * Mcore2Ecore executes the qvto transformation that generates ecore model
 * elements from a mcore component.
 * 
 * During the transformation, each ecore model elements is linked to the input
 * mcore elements via their internalE* properties.
 * 
 *
 */
public class Mcore2Ecore {

	private final static String path = "com.montages.mcore.mcore2ecore/transforms/mcore2ecore/Mcore2Ecore.qvto";
	private final static URI uri = createPlatformPluginURI(path, false);
	private static Future<TransformationExecutor> executor = null;

	public synchronized static void prepareExecutor() {
		if (executor == null) {
			ExecutorService pool = Executors.newFixedThreadPool(10);
			executor = pool.submit(new Callable<TransformationExecutor>() {
				@Override
				public TransformationExecutor call() throws Exception {
					TransformationExecutor executor = new TransformationExecutor(
							uri);
					executor.loadTransformation();
					return executor;
				}
			});
		}
	}

	public synchronized static TransformationExecutor getExecutor() {
		if (executor == null) {
			prepareExecutor();
		}

		TransformationExecutor exec = null;
		try {
			exec = executor.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		} catch (ExecutionException e) {
			e.printStackTrace();
			return null;
		}

		return exec;
	}

	/**
	 * Returns the input MComponent to which the internalE* properties have been
	 * set to the external ecore model elements generated by the transformation.
	 * 
	 * @param component
	 * @return the input component linked to the generated ecore models
	 * @throws CoreException
	 */
	public List<Resource> transform(ResourceSet resourceSet,
			MComponent component) throws CoreException {
		TransformationExecutor exec = getExecutor();

		if (exec == null || component == null) {
			return Collections.<Resource> emptyList();
		}

		List<MComponent> dependencies = Dependencies.compute(component).sort();
		dependencies.remove(component);

		final ExecutionContextImpl ctx = new ExecutionContextImpl();
		ctx.setLog(new WriterLog(new OutputStreamWriter(System.out)));

		final BasicModelExtent outputs = new BasicModelExtent();
		final ExecutionDiagnostic diag = exec.execute(ctx,
				new BasicModelExtent(Arrays.asList(component)),
				new BasicModelExtent(dependencies),
				new BasicModelExtent(Arrays.asList(EcorePackage.eINSTANCE)),
				new BasicModelExtent(Arrays.asList(XMLTypePackage.eINSTANCE)),
				outputs);

		if (Diagnostic.ERROR == diag.getSeverity()) {
			throw new CoreException(BasicDiagnostic.toIStatus(diag));
		}

		List<Resource> result = new ArrayList<Resource>();

		URI location = component.eResource().getURI().trimSegments(1);

		for (MPackage mPackage : component.getOwnedPackage()) {
			Resource resource = createEcoreResource(resourceSet, location,
					mPackage);

			if (resource != null) {
				result.add(resource);
			}
		}

		return result;
	}

	private Resource createEcoreResource(ResourceSet resourceSet, URI location,
			MPackage mPackage) {
		final EPackage ePackage = mPackage.getInternalEPackage();
		if (ePackage == null) {
			return null;
		}

		final URI bundleURI = location.appendSegment(getFileName(mPackage))
				.appendFileExtension("ecore");
		final URI locationURI = resourceSet.getURIConverter()
				.normalize(bundleURI);

		Resource resource = null;
		try {
			resource = resourceSet.getResource(locationURI, true);
		} catch (Exception e) {
			resource = resourceSet.getResource(locationURI, false);
		}
		resource.setURI(locationURI);

		if (resource != null && ePackage != null) {
			resource.getContents().clear();
			resource.getContents().add(ePackage);
		}

		return resource;
	}

	private String getFileName(MPackage mPackage) {
		String fileName = null;

		if (mPackage.getSpecialFileName() != null
				&& !mPackage.getSpecialFileName().trim().isEmpty()) {
			fileName = mPackage.getSpecialFileName();
		} else {
			fileName = mPackage.getEName().toLowerCase();
		}

		return fileName;
	}

}
