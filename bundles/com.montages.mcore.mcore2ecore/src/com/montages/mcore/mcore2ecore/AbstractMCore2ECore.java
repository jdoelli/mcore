package com.montages.mcore.mcore2ecore;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MRepository;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.objects.MObject;

public abstract class AbstractMCore2ECore {

	private final Map<MObject, EObject> processed = new HashMap<MObject, EObject>();
	private final MObject2EObject objectTrans = new MObject2EObject(processed);

	public abstract List<EObject> transform(MRepository repository, boolean isInternal) throws CoreException;

	protected void transformInternalObjects(MRepository repository, List<EPackage> targets) {
		processed.clear();

		for (MComponent component: repository.getComponent())
			for (MPackage p: component.getOwnedPackage())
				processPackage(p, targets);
	}
	
	protected void processPackage(MPackage mPackage, List<EPackage> targets) {
		for (MClassifier classifier: mPackage.getClassifier())
			processInternalObjects(classifier, targets);
		
		for (MPackage subPackage: mPackage.getSubPackage())
			processPackage(subPackage, targets);
	}

	private void processInternalObjects(MClassifier classifier, List<EPackage> targets) {
		for (MGeneralAnnotation ann: classifier.getGeneralAnnotation()) {
			EList<MObject> contents = ann.getGeneralContent();
		
			if (!contents.isEmpty()) {
				EClass eClass = findEClass(classifier, targets);
				
				if (eClass != null) {
					EAnnotation eAnnotation = eClass.getEAnnotation(ann.getSource());
					if (eAnnotation != null) {
						for (MObject mo: contents) {
							EObject eo = objectTrans.transform(mo, new XMLResourceImpl());							
							if (eo != null) eAnnotation.getContents().add(eo);
						}
					}
				}
			}
		}
	}

	private EClass findEClass(MClassifier classifier, List<EPackage> targets) {
		for (EPackage ePackage: targets) {
			EClass eClass = (EClass) ePackage.getEClassifier(classifier.getEName());
			if (eClass != null) return eClass;
		}
		return null;
	}

}
