import Base;
import Operations;
import Annotations;
import UpdateAnnotations;

modeltype MCORE uses 'http://www.montages.com/mCore/MCore';
modeltype ECORE uses 'http://www.eclipse.org/emf/2002/Ecore';


library Classifiers;


--
--	Classifiers
--


--
-- Class
--


mapping inout MClassifier::MClassifier2EClass() : EClass
inherits
	MCORE::MModelElement::MModelElemen2EModelElement, 
	MCORE::MNamed::MNamed2ENamed
when
{
	self.kind = ClassifierKind::ClassType and not self.hasSimpleModelingType
}
{
	_abstract := self.abstractClass;
	interface := false;

	eAnnotations += self.annotations.label->map Label2EAnnotation();
	if self.annotations.constraint->notEmpty() or self.allConstraintAnnotations->notEmpty() then {
		//eAnnotations += self.annotations.constraint->map Invariant2EAnnotation();
	eAnnotations +=self.allConstraintAnnotations->map Invariant2EAnnotation();
		eAnnotations += self->map Invariant2EcoreEAnnotation(); 
	} endif;
	
	eAnnotations += self.map Overriding2EAnnotation();

	eStructuralFeatures += self.ownedProperty->map Property2EStructuralFeature();
	eSuperTypes := self.superType.late resolve(EClass);

	self.ownedProperty->select(e | e.getPropertyAnnotations().update->notEmpty())->forEach(p) {
		_result.eStructuralFeatures += p.map PropertyWithUpdate2EStructuralFeature();
		_result.eOperations += p.map PropertyUpdate2EOperationResult();
		_result.eOperations += p.map Property2EOperationChoiceConstraint();
		_result.eOperations += p.map Property2EOperationChoiceConstruct();
		
		p.getPropertyAnnotations().update->forEach(u) {
			_result.eOperations += p.map Property2EOperationObject(u);
			_result.eOperations += p.map Property2EOperationValue(u);
			
		if(u.location != null)
	       _result.eOperations += p.map Property2EOperationValue(u);
		};
	};
	
	self.ownedProperty->select(e | ( (e.changeable and e.propertyBehavior = PropertyBehavior::Derived) or e.isDerivedVolatile() ))->forEach(p)
	{
	_result.eOperations += p.map PropertyDerivedChangeable2EOperationXUpdate();
	};

	eOperations += self.ownedProperty.operationSignature->map OperationSignature2EOperation();
	eOperations += self.getOverridingOperationAnnotations()->map OverrideAnnotation2EOperation();
	
	if (self.eName.find('To') > 0 and self.eName.find('Map') > 0) then
		instanceClassName := 'java.util.Map$Entry'
	endif;
	
	self.internalEClassifier := result;
}


--
-- DataType
--


mapping inout MClassifier::MClassifier2EDataType() : EDataType
inherits 
	MCORE::MModelElement::MModelElemen2EModelElement,
	MCORE::MNamed::MNamed2ENamed
when
{
	self.kind = ClassifierKind::DataType and 
	self.containingPackage.derivedNsURI <> 'http://www.langlets.org/coreTypes/Core/Data/Xml'
} 
{
  	serializable := true;
  	instanceClassName := self.getInstanceClassName();

	self.internalEClassifier := result;
}


--
-- Enumeration
--


mapping inout MClassifier::MClassifier2EEnum() : EEnum
inherits
	MCORE::MModelElement::MModelElemen2EModelElement,
	MCORE::MNamed::MNamed2ENamed
when
{
	self.kind = ClassifierKind::Enumeration
} 
{	
	serializable := true;
   	eLiterals := self._literal->map MLiteral2EEnumliteral();

	self.internalEClassifier := result;	
}


--
-- Literal
--


mapping inout MLiteral::MLiteral2EEnumliteral() : EEnumLiteral 
inherits
	MCORE::MModelElement::MModelElemen2EModelElement,
	MCORE::MNamed::MNamed2ENamed 
{
	value := self.containingEnumeration._literal->indexOf(self) - 1;
	_literal :=
		if isValid(self.constantLabel) then
			self.constantLabel
		else self.calculatedShortName endif;

	self.internalEEnumLiteral := result;
}
