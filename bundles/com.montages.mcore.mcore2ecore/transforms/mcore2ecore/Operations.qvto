import Base;
import Annotations;


modeltype MCORE uses 'http://www.montages.com/mCore/MCore';
modeltype ECORE uses 'http://www.eclipse.org/emf/2002/Ecore';


library Operations;


--
-- PropertyGroup
--


mapping MProperty::propertyGroupAnnotation(pGroup: MPropertiesGroup): EStringToStringMapEntry {
	key := 'propertyCategory';
	value := pGroup.hiearchicalShortName();
}


query MPropertiesGroup::hiearchicalShortName(): String =
	if not self.oclAsType(EObject).eContainer().oclIsKindOf(MPropertiesGroup) then 
		self.calculatedShortName 
	else
		self.oclAsType(EObject).eContainer().oclAsType(MPropertiesGroup).hiearchicalShortName().concat("/").concat(self.calculatedShortName)
	endif;


mapping MProperty::createColumnAnnotation(pGroup: MPropertiesGroup): EStringToStringMapEntry 
when
{
	self.getPropertyAnnotations()->isEmpty() or 
	self.getPropertyAnnotations()->select(e | not e.layout.oclIsUndefined())->isEmpty() 
}
{
	key := 'createColumn';
	value := 'true';
}


--	
--	Operations
--	


mapping inout MOperationSignature::OperationSignature2EOperation(): EOperation 
{
	init {
		result := self.toEOperation();
		result.eAnnotations += self.getOperationAnnotations()->map MOperationAnnotation2EAnnotation();
		self.internalEOperation := result;
	}



	if self.operation.container().oclIsTypeOf(MPropertiesGroup) then {
		var propertyGroup := self.operation.container().oclAsType(MPropertiesGroup);
		if not eAnnotations->exists(source = EDITORCONFIG) then
			eAnnotations += object EAnnotation {source := EDITORCONFIG}
		endif;
		
		var annotation := eAnnotations->selectOne(source = EDITORCONFIG);
		if not annotation.details->exists(key = 'propertyCategory') then
			annotation.details += self.operation->map propertyGroupAnnotation(propertyGroup)
		endif;
		if not annotation.details->exists(key = 'createColumn') then
			annotation.details += self.operation->map createColumnAnnotation(propertyGroup)
		endif;
	} endif;

	self.setBodyAnnotationIfNotSetted(result);
}


helper MCORE::MOperationSignature::setBodyAnnotationIfNotSetted(inout eOperation : EOperation)  {
		if eOperation.eAnnotations->isEmpty() then {
			eOperation.eAnnotations := object EAnnotation {
				source := XOCL;
				};
		} endif;

		if not eOperation.eAnnotations->exists(source = XOCL) then {
			eOperation.eAnnotations += object EAnnotation {
				source := XOCL;
				};
		} endif;

		var xoclAnnotation := eOperation.eAnnotations->selectOne(source = XOCL);
		var body := object EStringToStringMapEntry {
			key := 'body';
			value := 'null';
		};
		if xoclAnnotation.details->isEmpty() then 
		{
			xoclAnnotation.details := body;
		}
		else
		{
			if not xoclAnnotation.details->exists(key = 'body') then {
				xoclAnnotation.details += body;
			} endif;
		} endif;
}


helper MOperationSignature::toEOperation(): EOperation {
	var _result := object EOperation {
		name := self.operation.eName;
		_ordered := true;
		lowerBound := if self.calculatedMandatory then 1 else 0 endif; 
		upperBound := if self.calculatedSingular then 1 else -1 endif;
		unique := if not self.calculatedSingular then upperBound <> 1 else true endif;	
	};

	var component := self.operation.containingClassifier.containingPackage.containingComponent;
	self.operation.setType(_result, component);
	self.operation.setNameAnnotations(_result);

	_result.eParameters := self.parameter->collect(e | MParameter2EParameter(e));

	return _result;
}

helper MParameter::toEParameter(): EParameter = object EParameter {
	name := self.name;
};

abstract mapping MVariable::MVariable2EParameter() : EParameter
inherits 
	MNamed::MNamed2ENamed, 
	MTyped::MTyped2ETyped 
{}

helper MExplicitlyTypedAndNamed::createNameAnnotations(hasShortName: Boolean, isNotSameName: Boolean): EAnnotation = 
return object EAnnotation {
	source := MCORE_URI;
	
	if hasShortName then {
		details += object EStringToStringMapEntry {
			key := 'mShortName';
			value := self.calculatedShortName;
		};
	} endif;

	if isNotSameName then {
		details += object EStringToStringMapEntry {
			key := 'mName';
			value := self.name;
		};
	} endif;

};

helper MExplicitlyTypedAndNamed::setNameAnnotations(inout element: ENamedElement) {
	var isNotSameName := self.eName.camelCaseToBusiness() <> self.name;
	var hasShortName := not self.shortName.oclIsUndefined(); 
	var hasValidName := isValid(self.name);
		
	if hasShortName or (isNotSameName and hasValidName) then {
		element.eAnnotations += self.createNameAnnotations(hasShortName, isNotSameName);
	} endif;	
}

helper MExplicitlyTyped::setType(inout element: ETypedElement, component: MComponent) {
	if self.hasSimpleDataType then {
		element.eType := self.getEDataType()
	} else if self.hasSimpleModelingType then {
		element.eType := self.simpleType.getEModelElement();
	} else { 
		var type:= if self.calculatedType.oclIsUndefined() then self.type else self.calculatedType endif;
		// insure setting of upper bound in case of void type.
		if (type.oclIsUndefined()) then {
			element.upperBound := 1
		} else {
			element.eType := type.late resolveone(EClassifier)
		} endif;
	} endif endif;	
}

helper MParameter2EParameter(inout parameter: MParameter) : EParameter {
 	var _result := object EParameter { 
 		name := parameter.eName;
 		lowerBound := if not parameter.calculatedMandatory then 0 else 1 endif; 
		upperBound := if parameter.multiplicityAsString.endsWith("*]") then -1 else 1 endif;
		unique := if not parameter.calculatedSingular then upperBound <> 1 else true endif;

		-- default is false, but false causes editor generation 
		-- problems and no mcore to map to
		_ordered := true;  	
	};

	var component := parameter.containingSignature.containingClassifier.containingPackage.containingComponent;
	parameter.setType(_result, component);
	parameter.setNameAnnotations(_result);
	parameter.internalEParameter := _result;

	return _result;
}


--	
--	Override Operation
--	


mapping MCORE::annotations::MOperationAnnotations::OverrideAnnotation2EOperation(): EOperation
{
	init {
		result := self.operationSignature.toEOperation();
		result.eAnnotations += object EAnnotation {
			source := XOCL;
			details := object EStringToStringMapEntry {
				key := 'body';
				value := self._result.value; 
			}
		};
	}
}
