package com.montages.transactions;


public enum McoreTransactionEventType {

	SAVE,
	RESOURCE_RELOADED,
	STOP_RS_LISTENING,
	START_RS_LISTENING,
	INPUT_CHANGED
}
