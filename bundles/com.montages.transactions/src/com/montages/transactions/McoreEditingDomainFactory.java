package com.montages.transactions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.operations.IOperationHistory;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryFactory;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.DeleteCommand;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.provider.resource.ResourceItemProviderAdapterFactory;
import org.eclipse.emf.transaction.TransactionalCommandStack;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.TransactionalEditingDomain.Registry;
import org.eclipse.emf.workspace.IWorkspaceCommandStack;
import org.eclipse.gmf.runtime.diagram.core.DiagramEditingDomainFactory;
import org.eclipse.gmf.runtime.emf.commands.core.command.EditingDomainUndoContext;
import org.xocl.semantics.commands.DeleteCommandWithOptimizedCrossReferencer;

import com.montages.common.CommonPlugin;

public class McoreEditingDomainFactory extends DiagramEditingDomainFactory {

	private static McoreEditingDomainFactory instance = new McoreEditingDomainFactory();

	private static Map<TransactionalEditingDomain,List<Object>> ourTransactionMap = new HashMap<TransactionalEditingDomain, List<Object>>();

	public static McoreEditingDomainFactory getInstance() {
		return instance;
	}

	@Override
	public TransactionalEditingDomain createEditingDomain() {
		throw new UnsupportedOperationException();
	}

	public McoreDiagramEditingDoamin createEditingDomain(String id, Object owner) {
		McoreDiagramEditingDoamin editingDomain = (McoreDiagramEditingDoamin)getDomainRegistry().getEditingDomain(id);
		if (editingDomain == null) {
			editingDomain = createEditingDomain(OperationHistoryFactory.getOperationHistory());
			editingDomain.setID(id);
			getDomainRegistry().add(id, editingDomain);
		}

		List<Object> referenseList = getDomainReferenceList(editingDomain);

		if (!referenseList.contains(owner)) {
			referenseList.add(owner);
		}
		return editingDomain;
	}

	public boolean exists(String id) {
		McoreDiagramEditingDoamin editingDomain = (McoreDiagramEditingDoamin)getDomainRegistry().getEditingDomain(id);
		if (editingDomain == null) {
			return false;
		}
		return !getDomainReferenceList(editingDomain).isEmpty();
	}

	public McoreDiagramEditingDoamin createEditingDomain(IOperationHistory history) {
		IWorkspaceCommandStack stack = new MCoreCommandStack(history);
		McoreDiagramEditingDoamin result = new McoreDiagramEditingDoamin(createComposedAdapterFactory(), stack, history);
		mapResourceSet(result);
		result.getResourceSet().getURIConverter().getURIMap().putAll(CommonPlugin.computeURIMap());
		configure(result);
		return result;
	}

	public McoreDiagramEditingDoamin createEditingDomain(ResourceSet rset) {
		throw new UnsupportedOperationException();
	}

	public McoreDiagramEditingDoamin createEditingDomain(ResourceSet rset, IOperationHistory history) {
		throw new UnsupportedOperationException();
	}

	public void dispose(McoreDiagramEditingDoamin editingDomain, Object obj) {
		List<Object> referenseList = getDomainReferenceList(editingDomain);
		referenseList.remove(obj);
		if (referenseList.isEmpty()) {
			if (editingDomain != null) {
				getDomainRegistry().remove(editingDomain.getID());
				editingDomain.getAdapterFactory().dispose();
				for (Resource resource: editingDomain.getResourceSet().getResources()) {
					if (resource.isLoaded()) {
						resource.unload();
					}
				}
				((MCoreCommandStack)editingDomain.getCommandStack()).dispose();
				editingDomain.dispose();
			}
		}
	}

	protected List<Object> getDomainReferenceList(McoreDiagramEditingDoamin editingDomain) {
		List<Object> referenseList = ourTransactionMap.get(editingDomain);
		if (referenseList == null) {
			referenseList = new ArrayList<Object>();
			ourTransactionMap.put(editingDomain, referenseList);
		}
		return referenseList;
	}

	private Registry getDomainRegistry() {
		return TransactionalEditingDomain.Registry.INSTANCE;
	}

	public class McoreDiagramEditingDoamin extends DiagramEditingDomain implements IMcoreTransactionEventPublisher {

		private IOperationHistory myHistory;

		private IUndoContext myUndoContext;

		private List<IMcoreTransactionEventListener> myListeners = new ArrayList<IMcoreTransactionEventListener>();

		public McoreDiagramEditingDoamin(AdapterFactory adapterFactory, TransactionalCommandStack stack, IOperationHistory history) {
			super(adapterFactory, stack);
			myHistory = history;
		}

		public McoreDiagramEditingDoamin(AdapterFactory adapterFactory, TransactionalCommandStack stack, ResourceSet resourceSet, IOperationHistory history) {
			super(adapterFactory, stack, resourceSet);
			myHistory = history;
		}

		public IOperationHistory getHistory() {
			return myHistory;
		}

		public void addListener(IMcoreTransactionEventListener listener) {
			if (listener != null) {
				myListeners.add(listener);
			}
		}

		public void removeListener(IMcoreTransactionEventListener listener) {
			if (listener != null) {
				myListeners.remove(listener);
			}
		}

		@Override
		public void publish(McoreTransactionEventType event, Object owner) {
			for (IMcoreTransactionEventListener listener: myListeners) {
				if (owner == listener) {
					continue;
				}
				listener.handleMcoreEvent(new McoreTransactionEvent(this, event, owner));
			}
		}

		@Override
		public ComposedAdapterFactory getAdapterFactory() {
			return (ComposedAdapterFactory)super.getAdapterFactory();
		}

		public IUndoContext getUndoContext() {
			if (myUndoContext == null) {
				myUndoContext = createUndoContext();
			}
			return myUndoContext;
		}

		public void doSave() {
			((MCoreCommandStack)getCommandStack()).saveIsDone();
		}

		protected IUndoContext createUndoContext() {
			return new EditingDomainUndoContext(this, "Custom undo context:" + this); //$NON-NLS-1$;
		}

		@Override
		public Command createCommand(Class<? extends Command> commandClass, CommandParameter commandParameter) {
			if (commandClass == DeleteCommand.class) {
				return new DeleteCommandWithOptimizedCrossReferencer(this,
						commandParameter.getCollection());
			}

			return super.createCommand(commandClass, commandParameter);
		}

		public boolean isModified() {
			MCoreCommandStack commandStack = (MCoreCommandStack)getCommandStack();
			if (commandStack != null) {
				return commandStack.isSaveNeeded();
			}
			return false;
		}
	}

	private static final ComposedAdapterFactory adapterFactory = new ComposedAdapterFactory(ComposedAdapterFactory.Descriptor.Registry.INSTANCE);

	static {
		//
		List<String> adaptersURIS = new ArrayList<String>();
		adaptersURIS.add("http://www.montages.com/mCore/MCore");
		adaptersURIS.add("http://www.montages.com/mCore/MCore/Annotations");
		adaptersURIS.add("http://www.montages.com/mCore/MCore/Expressions");
		adaptersURIS.add("http://www.montages.com/mCore/MCore/Objects");
		adaptersURIS.add("http://www.montages.com/mCore/MCore/Tables");
		adaptersURIS.add("http://www.montages.com/mCore/MCore/Updates");
		adaptersURIS.add("http://www.langlets.org/aCore/ACore");
		adaptersURIS.add("http://www.langlets.org/aCore/ACore/Abstractions");
		adaptersURIS.add("http://www.langlets.org/aCore/ACore/Classifiers");
		adaptersURIS.add("http://www.langlets.org/aCore/ACore/Values");
		adaptersURIS.add("http://www.langlets.org/aCore/ACore/Updates");
		adaptersURIS.add("http://www.xocl.org/semantics/Semantics");

		final IExtension[] extensions = Platform.getExtensionRegistry()
				.getExtensionPoint("org.eclipse.emf.edit.itemProviderAdapterFactories").getExtensions();
		for (IExtension extension : extensions) {
			for (IConfigurationElement configElement : extension.getConfigurationElements()) {
				String uri = configElement.getAttribute("uri");
				if (adaptersURIS.contains(uri)) {
					try {
						AdapterFactory factory = (AdapterFactory) configElement.createExecutableExtension("class");
						adapterFactory.addAdapterFactory(factory);
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
			}
		}

		adapterFactory.addAdapterFactory(new ResourceItemProviderAdapterFactory());
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
	}
	
	public ComposedAdapterFactory createComposedAdapterFactory() {

		return adapterFactory;
	}
}
