/**
 * <copyright>
 * </copyright>
 *
 * $Id: RowFeatureCellItemProvider.java,v 1.10 2011/07/20 17:05:00 mtg.kutter Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.impl.RowFeatureCellImpl;

/**
 * This is the item provider adapter for a {@link org.xocl.editorconfig.RowFeatureCell} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class RowFeatureCellItemProvider extends CellConfigItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RowFeatureCellItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addFeaturePropertyDescriptor(object);
			addCellEditBehaviorPropertyDescriptor(object);
			addCellLocateBehaviorPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Feature feature.
		 * The list of possible choices is constructed by OCL eContainer().oclAsType(EClassToCellConfigMapEntry).key.eAllStructuralFeatures
		 * The list of possible choices is constraint by OCL if trg.oclIsTypeOf(ecore::EReference) then
		not trg.oclAsType(ecore::EReference).containment
		else 
		true
		endif 
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_RowFeatureCell_feature_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_RowFeatureCell_feature_feature",
						"_UI_RowFeatureCell_type"),
				EditorConfigPackage.Literals.ROW_FEATURE_CELL__FEATURE, true,
				false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<EStructuralFeature> result = new ArrayList<EStructuralFeature>();
				Collection<? extends EStructuralFeature> superResult = (Collection<? extends EStructuralFeature>) super
						.getChoiceOfValues(object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((RowFeatureCellImpl) object)
						.evalFeatureChoiceConstruction(result);
				for (Iterator<EStructuralFeature> iterator = result.iterator(); iterator
						.hasNext();) {
					EStructuralFeature trg = iterator.next();
					if (trg == null
							|| !((RowFeatureCellImpl) object)
									.evalFeatureChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Cell Edit Behavior feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCellEditBehaviorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cell Edit Behavior feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_RowFeatureCell_cellEditBehavior_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_RowFeatureCell_cellEditBehavior_feature",
								"_UI_RowFeatureCell_type"),
						EditorConfigPackage.Literals.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Cell Locate Behavior feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCellLocateBehaviorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cell Locate Behavior feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_RowFeatureCell_cellLocateBehavior_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_RowFeatureCell_cellLocateBehavior_feature",
								"_UI_RowFeatureCell_type"),
						EditorConfigPackage.Literals.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns RowFeatureCell.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/RowFeatureCell"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((RowFeatureCell) object).getColor();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<"
				+ containingFeatureName + ">" : "<" + containingFeatureName
				+ ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(RowFeatureCell.class)) {
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
