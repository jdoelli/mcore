/**
 * <copyright>
 * </copyright>
 *
 * $Id: TableConfigItemProvider.java,v 1.14 2013/04/15 15:24:25 mtg.hillairet Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;
import org.xocl.editorconfig.impl.TableConfigImpl;

/**
 * This is the item provider adapter for a {@link org.xocl.editorconfig.TableConfig} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class TableConfigItemProvider extends ItemProviderAdapter implements
		IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
			addDisplayDefaultColumnPropertyDescriptor(object);
			addDisplayHeaderPropertyDescriptor(object);
			addFontPropertyDescriptor(object);
			addIntendedPackagePropertyDescriptor(object);
			addIntendedClassPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(), getResourceLocator(),
						getString("_UI_TableConfig_name_feature"), getString(
								"_UI_PropertyDescriptor_description",
								"_UI_TableConfig_name_feature",
								"_UI_TableConfig_type"),
						EditorConfigPackage.Literals.TABLE_CONFIG__NAME, true,
						false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(), getResourceLocator(),
						getString("_UI_TableConfig_label_feature"), getString(
								"_UI_PropertyDescriptor_description",
								"_UI_TableConfig_label_feature",
								"_UI_TableConfig_type"),
						EditorConfigPackage.Literals.TABLE_CONFIG__LABEL, true,
						false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Display Default Column feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayDefaultColumnPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Display Default Column feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_TableConfig_displayDefaultColumn_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_TableConfig_displayDefaultColumn_feature",
								"_UI_TableConfig_type"),
						EditorConfigPackage.Literals.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
						true, false, false,
						ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Display Header feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayHeaderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Display Header feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_TableConfig_displayHeader_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_TableConfig_displayHeader_feature",
						"_UI_TableConfig_type"),
				EditorConfigPackage.Literals.TABLE_CONFIG__DISPLAY_HEADER,
				true, false, false, ItemPropertyDescriptor.BOOLEAN_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Font feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Font feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(), getResourceLocator(),
						getString("_UI_TableConfig_font_feature"), getString(
								"_UI_PropertyDescriptor_description",
								"_UI_TableConfig_font_feature",
								"_UI_TableConfig_type"),
						EditorConfigPackage.Literals.TABLE_CONFIG__FONT, true,
						false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Intended Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntendedPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Package feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_TableConfig_intendedPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_TableConfig_intendedPackage_feature",
						"_UI_TableConfig_type"),
				EditorConfigPackage.Literals.TABLE_CONFIG__INTENDED_PACKAGE,
				true, false, true, null, null, null) {
			
			// Fix for removing duplicates packages
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				Collection<Object> result = new ArrayList<Object>();
				Map<String, EPackage> values = new HashMap<String, EPackage>();
				Collection<?> choices = super.getChoiceOfValues(object);
				
				for (Object choice: choices) {
					if (choice instanceof EPackage) {
						EPackage current = (EPackage) choice;
						if (values.containsKey(current.getNsURI())) {
							if (current.eResource().getURI().isPlatformResource()) {
								values.put(current.getNsURI(), current);
							}
						} else {
							values.put(current.getNsURI(), current);
						}
					} else {
						result.add(choice);
					}
				}

				result.addAll(values.values());

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Intended Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntendedClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Class feature.
		 * The list of possible choices is constraint by OCL let p:ecore::EPackage = self.intendedPackage
		in 
		if p.oclIsUndefined() then true
		else if p.eClassifiers->includes(trg) then true
		else p.eSubpackages.eClassifiers->includes(trg)
		endif endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_TableConfig_intendedClass_feature"), getString(
						"_UI_PropertyDescriptor_description",
						"_UI_TableConfig_intendedClass_feature",
						"_UI_TableConfig_type"),
				EditorConfigPackage.Literals.TABLE_CONFIG__INTENDED_CLASS,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<EClass> result = new ArrayList<EClass>();
				Collection<? extends EClass> superResult = (Collection<? extends EClass>) super
						.getChoiceOfValues(object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
						result);
				Resource resource = ((EObject) object).eResource();
				if (resource != null) {
					ResourceSet resourceSet = resource.getResourceSet();
					if (resourceSet != null) {
						Collection<EObject> visited = new HashSet<EObject>(
								eObjects);
						Registry packageRegistry = resourceSet
								.getPackageRegistry();
						for (Iterator<String> i = packageRegistry.keySet()
								.iterator(); i.hasNext();) {
							collectReachableObjectsOfType(
									visited,
									eObjects,
									packageRegistry.getEPackage(i.next()),
									EditorConfigPackage.Literals.TABLE_CONFIG__INTENDED_CLASS
											.getEType());
						}
						result = (List<EClass>) (List<?>) eObjects;
					}
				}
				for (Iterator<EClass> iterator = result.iterator(); iterator
						.hasNext();) {
					EClass trg = iterator.next();
					if (trg == null
							|| !((TableConfigImpl) object)
									.evalIntendedClassChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(EditorConfigPackage.Literals.TABLE_CONFIG__COLUMN);
			childrenFeatures
					.add(EditorConfigPackage.Literals.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns TableConfig.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/TableConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((TableConfigImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(TableConfig.class)) {
		case EditorConfigPackage.TABLE_CONFIG__NAME:
		case EditorConfigPackage.TABLE_CONFIG__LABEL:
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER:
		case EditorConfigPackage.TABLE_CONFIG__FONT:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), false, true));
			return;
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				EditorConfigPackage.Literals.TABLE_CONFIG__COLUMN,
				EditorConfigFactory.eINSTANCE.createColumnConfig()));

		newChildDescriptors
				.add(createChildParameter(
						EditorConfigPackage.Literals.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP,
						EditorConfigFactory.eINSTANCE
								.create(EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY)));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
