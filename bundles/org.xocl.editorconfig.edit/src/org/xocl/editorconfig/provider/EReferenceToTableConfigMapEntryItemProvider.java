/**
 * <copyright>
 * </copyright>
 *
 * $Id: EReferenceToTableConfigMapEntryItemProvider.java,v 1.9 2011/07/20 17:05:00 mtg.kutter Exp $
 */
package org.xocl.editorconfig.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;
import org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl;

/**
 * This is the item provider adapter for a {@link java.util.Map.Entry} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class EReferenceToTableConfigMapEntryItemProvider extends
		ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReferenceToTableConfigMapEntryItemProvider(
			AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addKeyPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Key feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Key feature.
		 * The list of possible choices is constraint by OCL let 
		tc:editorconfig::TableConfig = self.eContainer().oclAsType(editorconfig::TableConfig) in
		let ic:ecore::EClass = tc.intendedClass in
		let intended:Boolean = 
		if not ic.oclIsUndefined()
		then
		ic.eAllReferences->includes(trg)
		else true endif in 
		let unique:Boolean =
		self.key=trg or not (tc.referenceToTableConfigMap.key->includes(trg))
		in
		trg.containment and intended and unique
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EReferenceToTableConfigMapEntry_key_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_EReferenceToTableConfigMapEntry_key_feature",
								"_UI_EReferenceToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY,
						true, false, true, null, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<EReference> result = new ArrayList<EReference>();
						Collection<? extends EReference> superResult = (Collection<? extends EReference>) super
								.getChoiceOfValues(object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						for (Iterator<EReference> iterator = result.iterator(); iterator
								.hasNext();) {
							EReference trg = iterator.next();
							if (trg == null
									|| !((EReferenceToTableConfigMapEntryImpl) object)
											.evalTypedKeyChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EReferenceToTableConfigMapEntry_value_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_EReferenceToTableConfigMapEntry_value_feature",
								"_UI_EReferenceToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE,
						true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(
						((ComposeableAdapterFactory) adapterFactory)
								.getRootAdapterFactory(),
						getResourceLocator(),
						getString("_UI_EReferenceToTableConfigMapEntry_label_feature"),
						getString(
								"_UI_PropertyDescriptor_description",
								"_UI_EReferenceToTableConfigMapEntry_label_feature",
								"_UI_EReferenceToTableConfigMapEntry_type"),
						EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL,
						true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns EReferenceToTableConfigMapEntry.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage(
				"full/obj16/EReferenceToTableConfigMapEntry"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public String getText(Object object) {
		@SuppressWarnings("unchecked")
		Map.Entry<EReference, TableConfig> eReferenceToTableConfigMapEntry = (Map.Entry<EReference, TableConfig>) object;
		EReference eReference = eReferenceToTableConfigMapEntry.getKey();
		TableConfig tableConfig = eReferenceToTableConfigMapEntry.getValue();
		return "" + (eReference != null ? eReference.getName() : eReference)
				+ " -> "
				+ (tableConfig != null ? tableConfig.getLabel() : tableConfig);
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(Map.Entry.class)) {
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL:
			fireNotifyChanged(new ViewerNotification(notification, notification
					.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return EditorConfigEditPlugin.INSTANCE;
	}

}
