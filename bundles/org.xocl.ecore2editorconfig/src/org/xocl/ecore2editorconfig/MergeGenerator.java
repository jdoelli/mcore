package org.xocl.ecore2editorconfig;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.TableConfig;

public class MergeGenerator extends Generator {

	public MergeGenerator(ResourceSet resourceSet) {
		super(resourceSet);
	}

	public EditorConfig generate(EPackage ePackage) {		
		URI editorConfigURI = getEditorConfigURI(ePackage);
		Resource resource = null;
		try {
			resource = resourceSet.getResource(editorConfigURI, true);
		} catch (Exception e) {
			// does not exist
		}

		if (resource != null && !resource.getContents().isEmpty()) {
			return doMerge((EditorConfig) resource.getContents().get(0), ePackage);
		} else {
			return new DefaultGenerator(resourceSet).generate(ePackage);
		}
	}

	private EditorConfig doMerge(EditorConfig editorConfig, EPackage ePackage) {
		for (EClassifier classifier: ePackage.getEClassifiers()) {
			if (classifier instanceof EClass) {
				EClass eClass = (EClass) classifier;
				TableConfig table = findTable(editorConfig, eClass);
				if (table == null) {
					editorConfig.getTableConfig().add(table = createTable(eClass));
				}
			}
		}

		for (EPackage subPackage: ePackage.getESubpackages()) {
			doMerge(editorConfig, subPackage);
		}

		return editorConfig;
	}

}