package org.xocl.ecore2editorconfig;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.xocl.core.util.XoclHelper;
import org.xocl.editorconfig.CellEditBehaviorOption;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;
import org.xocl.editorconfig.plugin.EditorConfigPlugin;

public abstract class Generator {

	protected final static String EDITORCONFIG = "http://www.xocl.org/EDITORCONFIG";

	protected final ResourceSet resourceSet;

	protected static final EditorConfigFactory factory = EditorConfigFactory.eINSTANCE;

	protected Map<EClass, Set<EClass>> mapOfSubTypes = new HashMap<EClass, Set<EClass>>();

	protected Map<EClass, Collection<EStructuralFeature>> mapOfFeatures = new HashMap<EClass, Collection<EStructuralFeature>>();

	public Generator(ResourceSet resourceSet) {
		this.resourceSet = resourceSet;
	}

	public void initialize(List<EPackage> ePackages) {
		mapOfSubTypes.clear();
		mapOfFeatures.clear();

		List<EClass> allClasses = new ArrayList<EClass>();
		for (EPackage upperPackage: ePackages) {
			allEClasses(upperPackage, allClasses);
		}

		mapOfSubTypes.putAll(computeSubTypes(allClasses));
	}

	public URI getEditorConfigURI(EPackage ePackage) {
		URI ePackageURI = URI.createURI(ePackage.getNsURI());
		URI normalizedURI = resourceSet.getURIConverter().normalize(ePackageURI);

		return normalizedURI.trimFileExtension().appendFileExtension("editorconfig");
	}

	public abstract EditorConfig generate(EPackage ePackage);

	public Map<EClass, Set<EClass>> computeSubTypes(List<EClass> allClasses) {
		Map<EClass, Set<EClass>> mapOfSubTypes = new HashMap<EClass, Set<EClass>>();

		for (EClass eClass: allClasses) {
			Set<EClass> subTypes = subTypesOf(eClass, allClasses);
			if (!mapOfSubTypes.containsKey(eClass)) {
				mapOfSubTypes.put(eClass, subTypes);
			} else {
				mapOfSubTypes.get(eClass).addAll(subTypes);
			}
		}

		return mapOfSubTypes;
	}

	protected Set<EClass> subTypesOf(EClass eClass, List<EClass> allClasses) {
		Set<EClass> subTypes = new HashSet<EClass>();
		for (EClass candidate: allClasses) {
			if (candidate.getEAllSuperTypes().contains(eClass)) {
				subTypes.add(candidate);
			}
		}

		return subTypes;
	}

	protected List<EClass> allEClasses(EPackage ePackage, List<EClass> allClasses) {
		if (allClasses == null) {
			allClasses = new ArrayList<EClass>();
		}

		List<EPackage> ePackages = allEPackages(ePackage, null);

		for (EPackage current: ePackages) {
			for (EClassifier classifier: current.getEClassifiers()) {
				if (classifier instanceof EClass) {
					allClasses.add((EClass) classifier);
				}
			}
		}

		return allClasses;
	}

	protected List<EPackage> allEPackages(EPackage ePackage, List<EPackage> allPackages) {
		if (allPackages == null) {
			allPackages = new ArrayList<EPackage>();
		}

		allPackages.add(ePackage);
		for (EPackage subPackage: ePackage.getESubpackages()) {
			allEPackages(subPackage, allPackages);
		}

		return allPackages;
	}

	public Collection<EStructuralFeature> getStructuralFeatures(EClass eClass) {
		if (mapOfFeatures.containsKey(eClass)) {
			return mapOfFeatures.get(eClass);
		}

		List<EStructuralFeature> features = new ArrayList<EStructuralFeature>();
		for (EClass superType: eClass.getEAllSuperTypes()) {
			addAllFeature(features, superType.getEAllStructuralFeatures());
		}

		addAllFeature(features, eClass.getEStructuralFeatures());

		if (mapOfSubTypes.containsKey(eClass)) {
			for (EClass subClass: mapOfSubTypes.get(eClass)) {
				addAllFeature(features, subClass.getEAllStructuralFeatures());
			}
		}

		mapOfFeatures.put(eClass, features);

		return features;
	}

	public void addAllFeature(Collection<EStructuralFeature> features, Collection<EStructuralFeature> shouldAdd) {
		for (EStructuralFeature add: shouldAdd) {
			if (!features.contains(add)) {
				features.add(add);
			}
		}
	}

	protected TableConfig createTable(EClass eClass) {
		TableConfig tableConfig = factory.createTableConfig();
		tableConfig.setName(eClass.getName());
		tableConfig.setLabel(XoclHelper.camelCaseToBusiness(eClass.getName()));
		tableConfig.setIntendedPackage(eClass.getEPackage());
		tableConfig.setIntendedClass(eClass);

		Collection<EStructuralFeature> allFeatures = getStructuralFeatures(eClass);

		for (EStructuralFeature feature: allFeatures) {
			if (feature instanceof EAttribute && shouldCreateColumn(feature)) {
				tableConfig.getColumn().add(createColumn(feature));
			}
		}

		for (EStructuralFeature feature: allFeatures) {
			if (feature instanceof EReference && !((EReference) feature).isContainment()) {
				if (shouldCreateColumn(feature)) {
					EReference reference = (EReference) feature;
					if (reference.getEOpposite() == null ? true : !reference.getEOpposite().isContainment()) {
						tableConfig.getColumn().add(createColumn(reference));
					}
				}
			}
		}

		return tableConfig;
	}

	protected boolean shouldCreateColumn(EStructuralFeature feature) {
		boolean shoulCreate = true;
		EAnnotation ann = feature.getEAnnotation(EDITORCONFIG);
		if (ann != null) {
			shoulCreate = Boolean.parseBoolean(ann.getDetails().get("createColumn"));
		}
		return shoulCreate;
	}

	protected ColumnConfig createColumn(EStructuralFeature feature) {
		ColumnConfig columnConfig = factory.createColumnConfig();
		columnConfig.setName(feature.getName());
		columnConfig.setLabel(XoclHelper.camelCaseToBusiness(feature.getName()));

		RowFeatureCell cellConfig = factory.createRowFeatureCell();
		cellConfig.setFeature(feature);
		cellConfig.setCellEditBehavior(CellEditBehaviorOption.SELECTION);
		cellConfig.setCellLocateBehavior(CellLocateBehaviorOption.SELECTION);

		EAnnotation annotation = feature.getEAnnotation("http://www.xocl.org/OCL");
		if (annotation != null && annotation.getDetails().containsKey("color")) {
			cellConfig.setColor(annotation.getDetails().get("color"));
		}
		columnConfig.getCellConfigMap().put(feature.getEContainingClass(), cellConfig);

		return columnConfig;
	}

	protected TableConfig findTableConfig(ResourceSet rs, EClass eClass, Map<EClass, TableConfig> tableMap) {
		TableConfig table = tableMap.get(eClass);

		if (table == null) {
			if (eClass.eResource() != null) {
				URI uri = eClass.eResource().getURI();
				EditorConfig config = loadOrCreateEditorConfig(rs, uri);
				table = findTable(config, eClass);
				if (table != null) {
					tableMap.put(eClass, table);
				}
			}
		}

		return table; 
	}

	protected TableConfig findTable(EditorConfig c, EClass eClass) {
		if (c == null) return null;

		Iterator<TableConfig> it = c.getTableConfig().iterator();
		TableConfig found = null;
		while(found == null && it.hasNext()) {
			TableConfig current = it.next();
			boolean isSame = true;
			EPackage intendedPackage = current.getIntendedPackage();
			EClass intendedClass = current.getIntendedClass();

			if (intendedPackage != null) {
				isSame = intendedPackage.getName().equals(eClass.getEPackage().getName()); 
			}
			if (intendedClass != null) {
				isSame = isSame && (intendedClass.getName().equals(eClass.getName()) || 
						intendedClass.isSuperTypeOf(eClass));
			}
			if (isSame) {
				found = current;
			}
		}

		return found;
	}

	protected EditorConfig loadOrCreateEditorConfig(ResourceSet rs, URI uri) {
		URI editorURI = EditorConfigPlugin.INSTANCE.getEditorConfig(uri.toString());

		if (editorURI == null) {
			if (!uri.isPlatform()) {
				uri = rs.getURIConverter().normalize(uri);
			}
			editorURI = uri.trimFileExtension().appendFileExtension("editorconfig");
		}

		Resource targetEditResource = null;
		try {
			targetEditResource = rs.getResource(editorURI, true);
		} catch (Exception e) {
			return null;
		}

		if (targetEditResource != null && !targetEditResource.getContents().isEmpty()) {
			return (EditorConfig) targetEditResource.getContents().get(0);
		}

		return null;
	}

}