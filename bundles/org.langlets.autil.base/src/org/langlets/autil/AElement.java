/**
 */
package org.langlets.autil;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AElement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.autil.AElement#getALabel <em>ALabel</em>}</li>
 *   <li>{@link org.langlets.autil.AElement#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link org.langlets.autil.AElement#getARenderedKind <em>ARendered Kind</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.autil.AutilPackage#getAElement()
 * @model abstract="true"
 * @generated
 */

public interface AElement extends EObject {
	/**
	 * Returns the value of the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALabel</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getAElement_ALabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aRenderedKind\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Label And Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getALabel();

	/**
	 * Returns the value of the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AKind Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AKind Base</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getAElement_AKindBase()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eClass().name\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Label And Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAKindBase();

	/**
	 * Returns the value of the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ARendered Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ARendered Kind</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getAElement_ARenderedKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let renderedKind: String = let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif in\nrenderedKind\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Label And Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getARenderedKind();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).indentLevel() + 1\n  else 0 endif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Util/String Utilities' createColumn='true'"
	 * @generated
	 */
	Integer indentLevel();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nlet result: String = indentationSpaces(numberOfSpaces) in\nresult\n'"
	 * @generated
	 */
	String indentationSpaces();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let sizeMinusOne: Integer =  size - 1 in\nlet result: String = if (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif in\nresult\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Util/String Utilities' createColumn='true'"
	 * @generated
	 */
	String indentationSpaces(Integer size);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model pAnnotation="http://www.montages.com/mCore/MCore mName='p'"
	 *        annotation="http://www.xocl.org/OCL body='let result: String = if (stringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif in\nresult\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Util/String Utilities' createColumn='true'"
	 * @generated
	 */
	String stringOrMissing(String p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let result: Boolean = if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif in\nresult\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Util/String Utilities' createColumn='true'"
	 * @generated
	 */
	Boolean stringIsEmpty(String s);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n'"
	 * @generated
	 */
	String listOfStringToStringWithSeparator(EList<String> elements, String separator);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model elementsMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let defaultSeparator: String = \', \' in\nlistOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Util/String Utilities' createColumn='true'"
	 * @generated
	 */
	String listOfStringToStringWithSeparator(EList<String> elements);

} // AElement
