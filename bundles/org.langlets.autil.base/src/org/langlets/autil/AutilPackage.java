/**
 */
package org.langlets.autil;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.langlets.autil.AutilFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='org.langlets'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface AutilPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "org.langlets.autil.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "autil";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.langlets.org/aUtil/AUtil";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "autil";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AutilPackage eINSTANCE = org.langlets.autil.impl.AutilPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.langlets.autil.impl.AElementImpl <em>AElement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.autil.impl.AElementImpl
	 * @see org.langlets.autil.impl.AutilPackageImpl#getAElement()
	 * @generated
	 */
	int AELEMENT = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ALABEL = 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AKIND_BASE = 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ARENDERED_KIND = 2;

	/**
	 * The number of structural features of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_FEATURE_COUNT = 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENT_LEVEL = 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENTATION_SPACES = 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___INDENTATION_SPACES__INTEGER = 2;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___STRING_OR_MISSING__STRING = 3;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___STRING_IS_EMPTY__STRING = 4;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = 5;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = 6;

	/**
	 * The number of operations of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_OPERATION_COUNT = 7;

	/**
	 * The meta object id for the '{@link org.langlets.autil.impl.ANamedImpl <em>ANamed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.autil.impl.ANamedImpl
	 * @see org.langlets.autil.impl.AutilPackageImpl#getANamed()
	 * @generated
	 */
	int ANAMED = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ALABEL = AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AKIND_BASE = AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ARENDERED_KIND = AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ANAME = AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AUNDEFINED_NAME_CONSTANT = AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name From Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ABUSINESS_NAME_FROM_NAME = AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_FEATURE_COUNT = AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENT_LEVEL = AELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENTATION_SPACES = AELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___INDENTATION_SPACES__INTEGER = AELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___STRING_OR_MISSING__STRING = AELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___STRING_IS_EMPTY__STRING = AELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_OPERATION_COUNT = AELEMENT_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link org.langlets.autil.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ANamed</em>'.
	 * @see org.langlets.autil.ANamed
	 * @generated
	 */
	EClass getANamed();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.ANamed#getAName <em>AName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AName</em>'.
	 * @see org.langlets.autil.ANamed#getAName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AName();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.ANamed#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Name Constant</em>'.
	 * @see org.langlets.autil.ANamed#getAUndefinedNameConstant()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AUndefinedNameConstant();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.ANamed#getABusinessNameFromName <em>ABusiness Name From Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ABusiness Name From Name</em>'.
	 * @see org.langlets.autil.ANamed#getABusinessNameFromName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_ABusinessNameFromName();

	/**
	 * Returns the meta object for class '{@link org.langlets.autil.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AElement</em>'.
	 * @see org.langlets.autil.AElement
	 * @generated
	 */
	EClass getAElement();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.AElement#getALabel <em>ALabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ALabel</em>'.
	 * @see org.langlets.autil.AElement#getALabel()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ALabel();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.AElement#getAKindBase <em>AKind Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AKind Base</em>'.
	 * @see org.langlets.autil.AElement#getAKindBase()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_AKindBase();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.autil.AElement#getARenderedKind <em>ARendered Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ARendered Kind</em>'.
	 * @see org.langlets.autil.AElement#getARenderedKind()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ARenderedKind();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#indentLevel() <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indent Level</em>' operation.
	 * @see org.langlets.autil.AElement#indentLevel()
	 * @generated
	 */
	EOperation getAElement__IndentLevel();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#indentationSpaces() <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.langlets.autil.AElement#indentationSpaces()
	 * @generated
	 */
	EOperation getAElement__IndentationSpaces();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#indentationSpaces(java.lang.Integer) <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see org.langlets.autil.AElement#indentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getAElement__IndentationSpaces__Integer();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#stringOrMissing(java.lang.String) <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Or Missing</em>' operation.
	 * @see org.langlets.autil.AElement#stringOrMissing(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__StringOrMissing__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#stringIsEmpty(java.lang.String) <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Is Empty</em>' operation.
	 * @see org.langlets.autil.AElement#stringIsEmpty(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__StringIsEmpty__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String) <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Of String To String With Separator</em>' operation.
	 * @see org.langlets.autil.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__ListOfStringToStringWithSeparator__EList_String();

	/**
	 * Returns the meta object for the '{@link org.langlets.autil.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList) <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Of String To String With Separator</em>' operation.
	 * @see org.langlets.autil.AElement#listOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAElement__ListOfStringToStringWithSeparator__EList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AutilFactory getAutilFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.langlets.autil.impl.ANamedImpl <em>ANamed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.autil.impl.ANamedImpl
		 * @see org.langlets.autil.impl.AutilPackageImpl#getANamed()
		 * @generated
		 */
		EClass ANAMED = eINSTANCE.getANamed();

		/**
		 * The meta object literal for the '<em><b>AName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ANAME = eINSTANCE.getANamed_AName();

		/**
		 * The meta object literal for the '<em><b>AUndefined Name Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__AUNDEFINED_NAME_CONSTANT = eINSTANCE.getANamed_AUndefinedNameConstant();

		/**
		 * The meta object literal for the '<em><b>ABusiness Name From Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ABUSINESS_NAME_FROM_NAME = eINSTANCE.getANamed_ABusinessNameFromName();

		/**
		 * The meta object literal for the '{@link org.langlets.autil.impl.AElementImpl <em>AElement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.autil.impl.AElementImpl
		 * @see org.langlets.autil.impl.AutilPackageImpl#getAElement()
		 * @generated
		 */
		EClass AELEMENT = eINSTANCE.getAElement();

		/**
		 * The meta object literal for the '<em><b>ALabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ALABEL = eINSTANCE.getAElement_ALabel();

		/**
		 * The meta object literal for the '<em><b>AKind Base</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AKIND_BASE = eINSTANCE.getAElement_AKindBase();

		/**
		 * The meta object literal for the '<em><b>ARendered Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ARENDERED_KIND = eINSTANCE.getAElement_ARenderedKind();

		/**
		 * The meta object literal for the '<em><b>Indent Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENT_LEVEL = eINSTANCE.getAElement__IndentLevel();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENTATION_SPACES = eINSTANCE.getAElement__IndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___INDENTATION_SPACES__INTEGER = eINSTANCE.getAElement__IndentationSpaces__Integer();

		/**
		 * The meta object literal for the '<em><b>String Or Missing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___STRING_OR_MISSING__STRING = eINSTANCE.getAElement__StringOrMissing__String();

		/**
		 * The meta object literal for the '<em><b>String Is Empty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___STRING_IS_EMPTY__STRING = eINSTANCE.getAElement__StringIsEmpty__String();

		/**
		 * The meta object literal for the '<em><b>List Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = eINSTANCE
				.getAElement__ListOfStringToStringWithSeparator__EList_String();

		/**
		 * The meta object literal for the '<em><b>List Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = eINSTANCE
				.getAElement__ListOfStringToStringWithSeparator__EList();

	}

} //AutilPackage
