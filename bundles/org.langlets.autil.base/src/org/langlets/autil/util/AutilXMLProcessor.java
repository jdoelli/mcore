/**
 */
package org.langlets.autil.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import org.langlets.autil.AutilPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AutilXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutilXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		AutilPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the AutilResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new AutilResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new AutilResourceFactoryImpl());
		}
		return registrations;
	}

} //AutilXMLProcessor
