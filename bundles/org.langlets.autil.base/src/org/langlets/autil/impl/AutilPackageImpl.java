/**
 */
package org.langlets.autil.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.autil.AElement;
import org.langlets.autil.ANamed;
import org.langlets.autil.AutilFactory;
import org.langlets.autil.AutilPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AutilPackageImpl extends EPackageImpl implements AutilPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.langlets.autil.AutilPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AutilPackageImpl() {
		super(eNS_URI, AutilFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AutilPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AutilPackage init() {
		if (isInited)
			return (AutilPackage) EPackage.Registry.INSTANCE.getEPackage(AutilPackage.eNS_URI);

		// Obtain or create and register package
		AutilPackageImpl theAutilPackage = (AutilPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AutilPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AutilPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theAutilPackage.createPackageContents();

		// Initialize created meta-data
		theAutilPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAutilPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AutilPackage.eNS_URI, theAutilPackage);
		return theAutilPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getANamed() {
		return aNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AUndefinedNameConstant() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_ABusinessNameFromName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAElement() {
		return aElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ALabel() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_AKindBase() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ARenderedKind() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentLevel() {
		return aElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentationSpaces() {
		return aElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentationSpaces__Integer() {
		return aElementEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__StringOrMissing__String() {
		return aElementEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__StringIsEmpty__String() {
		return aElementEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ListOfStringToStringWithSeparator__EList_String() {
		return aElementEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ListOfStringToStringWithSeparator__EList() {
		return aElementEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AutilFactory getAutilFactory() {
		return (AutilFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aNamedEClass = createEClass(ANAMED);
		createEAttribute(aNamedEClass, ANAMED__ANAME);
		createEAttribute(aNamedEClass, ANAMED__AUNDEFINED_NAME_CONSTANT);
		createEAttribute(aNamedEClass, ANAMED__ABUSINESS_NAME_FROM_NAME);

		aElementEClass = createEClass(AELEMENT);
		createEAttribute(aElementEClass, AELEMENT__ALABEL);
		createEAttribute(aElementEClass, AELEMENT__AKIND_BASE);
		createEAttribute(aElementEClass, AELEMENT__ARENDERED_KIND);
		createEOperation(aElementEClass, AELEMENT___INDENT_LEVEL);
		createEOperation(aElementEClass, AELEMENT___INDENTATION_SPACES);
		createEOperation(aElementEClass, AELEMENT___INDENTATION_SPACES__INTEGER);
		createEOperation(aElementEClass, AELEMENT___STRING_OR_MISSING__STRING);
		createEOperation(aElementEClass, AELEMENT___STRING_IS_EMPTY__STRING);
		createEOperation(aElementEClass, AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING);
		createEOperation(aElementEClass, AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aNamedEClass.getESuperTypes().add(this.getAElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(aNamedEClass, ANamed.class, "ANamed", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getANamed_AName(), ecorePackage.getEString(), "aName", null, 0, 1, ANamed.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_AUndefinedNameConstant(), ecorePackage.getEString(), "aUndefinedNameConstant", null, 0,
				1, ANamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_ABusinessNameFromName(), ecorePackage.getEString(), "aBusinessNameFromName", null, 0,
				1, ANamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aElementEClass, AElement.class, "AElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAElement_ALabel(), ecorePackage.getEString(), "aLabel", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_AKindBase(), ecorePackage.getEString(), "aKindBase", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ARenderedKind(), ecorePackage.getEString(), "aRenderedKind", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEOperation(getAElement__IndentLevel(), ecorePackage.getEIntegerObject(), "indentLevel", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getAElement__IndentationSpaces(), ecorePackage.getEString(), "indentationSpaces", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getAElement__IndentationSpaces__Integer(), ecorePackage.getEString(),
				"indentationSpaces", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__StringOrMissing__String(), ecorePackage.getEString(), "stringOrMissing", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__StringIsEmpty__String(), ecorePackage.getEBooleanObject(), "stringIsEmpty", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__ListOfStringToStringWithSeparator__EList_String(), ecorePackage.getEString(),
				"listOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "separator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__ListOfStringToStringWithSeparator__EList(), ecorePackage.getEString(),
				"listOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source, new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getANamed_AName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Naming" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Naming" });
		addAnnotation(getANamed_ABusinessNameFromName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Naming" });
		addAnnotation(getAElement__IndentLevel(), source,
				new String[] { "propertyCategory", "80 A Util/String Utilities", "createColumn", "true" });
		addAnnotation(getAElement__IndentationSpaces(), source, new String[] {});
		addAnnotation(getAElement__IndentationSpaces__Integer(), source,
				new String[] { "propertyCategory", "80 A Util/String Utilities", "createColumn", "true" });
		addAnnotation(getAElement__StringOrMissing__String(), source,
				new String[] { "propertyCategory", "80 A Util/String Utilities", "createColumn", "true" });
		addAnnotation(getAElement__StringIsEmpty__String(), source,
				new String[] { "propertyCategory", "80 A Util/String Utilities", "createColumn", "true" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList_String(), source, new String[] {});
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList(), source,
				new String[] { "propertyCategory", "80 A Util/String Utilities", "createColumn", "true" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Label And Kind" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Label And Kind" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Util/Label And Kind" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelDerive", "aName\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelCreateColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getANamed_AName(), source, new String[] { "derive", "aUndefinedNameConstant\n" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source, new String[] { "derive",
				"let undefinedNameConstant: String = \'<A Name Is Undefined> \' in\nundefinedNameConstant\n" });
		addAnnotation(getANamed_ABusinessNameFromName(), source, new String[] { "derive",
				"let chain : String = aName in\nif chain.oclIsUndefined()\n  then null\n  else chain .camelCaseToBusiness()\n  endif\n" });
		addAnnotation(getAElement__IndentLevel(), source, new String[] { "body",
				"if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).indentLevel() + 1\n  else 0 endif endif\n" });
		addAnnotation(getAElement__IndentationSpaces(), source, new String[] { "body",
				"let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nlet result: String = indentationSpaces(numberOfSpaces) in\nresult\n" });
		addAnnotation(getAElement__IndentationSpaces__Integer(), source, new String[] { "body",
				"let sizeMinusOne: Integer =  size - 1 in\nlet result: String = if (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif in\nresult\n" });
		addAnnotation(getAElement__StringOrMissing__String(), source, new String[] { "body",
				"let result: String = if (stringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif in\nresult\n" });
		addAnnotation(getAElement__StringIsEmpty__String(), source, new String[] { "body",
				"let result: Boolean = if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif in\nresult\n" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList_String(), source, new String[] { "body",
				"let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList(), source, new String[] { "body",
				"let defaultSeparator: String = \', \' in\nlistOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n" });
		addAnnotation(getAElement_ALabel(), source, new String[] { "derive", "aRenderedKind\n" });
		addAnnotation(getAElement_AKindBase(), source, new String[] { "derive", "self.eClass().name\n" });
		addAnnotation(getAElement_ARenderedKind(), source, new String[] { "derive",
				"let renderedKind: String = let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif in\nrenderedKind\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getANamed_AName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_ABusinessNameFromName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation((getAElement__StringOrMissing__String()).getEParameters().get(0), source,
				new String[] { "mName", "p" });
	}

} //AutilPackageImpl
