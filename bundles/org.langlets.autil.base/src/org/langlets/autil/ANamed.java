/**
 */
package org.langlets.autil;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ANamed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.autil.ANamed#getAName <em>AName</em>}</li>
 *   <li>{@link org.langlets.autil.ANamed#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link org.langlets.autil.ANamed#getABusinessNameFromName <em>ABusiness Name From Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.autil.AutilPackage#getANamed()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aLabelDerive='aName\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aLabelCreateColumn='false'"
 * @generated
 */

public interface ANamed extends AElement {
	/**
	 * Returns the value of the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AName</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AName</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getANamed_AName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aUndefinedNameConstant\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAName();

	/**
	 * Returns the value of the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUndefined Name Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUndefined Name Constant</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getANamed_AUndefinedNameConstant()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let undefinedNameConstant: String = \'<A Name Is Undefined> \' in\nundefinedNameConstant\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUndefinedNameConstant();

	/**
	 * Returns the value of the '<em><b>ABusiness Name From Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABusiness Name From Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABusiness Name From Name</em>' attribute.
	 * @see org.langlets.autil.AutilPackage#getANamed_ABusinessNameFromName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let chain : String = aName in\nif chain.oclIsUndefined()\n  then null\n  else chain .camelCaseToBusiness()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Util/Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getABusinessNameFromName();

} // ANamed
