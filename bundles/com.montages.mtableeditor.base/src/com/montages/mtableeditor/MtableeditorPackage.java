/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.McorePackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.langlets.autil.AutilPackage;

/**
 * <!-- begin-user-doc --> The <b>Package</b> for the model. It contains
 * accessors for the meta objects to represent
 * <ul>
 * <li>each class,</li>
 * <li>each feature of each class,</li>
 * <li>each operation of each class,</li>
 * <li>each enum,</li>
 * <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorFactory
 * @model kind="package"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'MEditorConfig\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface MtableeditorPackage extends EPackage {

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mtableeditor.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mtableeditor";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mTableEditor/MTableEditor";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mtableeditor";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	MtableeditorPackage eINSTANCE = com.montages.mtableeditor.impl.MtableeditorPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MTableEditorElementImpl <em>MTable Editor Element</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MTableEditorElementImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableEditorElement()
	 * @generated
	 */
	int MTABLE_EDITOR_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT__ALABEL = AutilPackage.AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT__AKIND_BASE = AutilPackage.AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT__ARENDERED_KIND = AutilPackage.AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT__KIND = AutilPackage.AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>MTable Editor Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT_FEATURE_COUNT = AutilPackage.AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___INDENT_LEVEL = AutilPackage.AELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES = AutilPackage.AELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER = AutilPackage.AELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING = AutilPackage.AELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING = AutilPackage.AELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MTable Editor Element</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_EDITOR_ELEMENT_OPERATION_COUNT = AutilPackage.AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MEditorConfigImpl <em>MEditor Config</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MEditorConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditorConfig()
	 * @generated
	 */
	int MEDITOR_CONFIG = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__ALABEL = McorePackage.MEDITOR_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__AKIND_BASE = McorePackage.MEDITOR_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__ARENDERED_KIND = McorePackage.MEDITOR_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__KIND = McorePackage.MEDITOR_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Header Table Config</b></em>' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__HEADER_TABLE_CONFIG = McorePackage.MEDITOR_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>MTable Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__MTABLE_CONFIG = McorePackage.MEDITOR_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>MColumn Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__MCOLUMN_CONFIG = McorePackage.MEDITOR_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Class To Table Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG = McorePackage.MEDITOR_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Super Config</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__SUPER_CONFIG = McorePackage.MEDITOR_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG__DO_ACTION = McorePackage.MEDITOR_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>MEditor Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG_FEATURE_COUNT = McorePackage.MEDITOR_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___INDENT_LEVEL = McorePackage.MEDITOR_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___INDENTATION_SPACES = McorePackage.MEDITOR_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___INDENTATION_SPACES__INTEGER = McorePackage.MEDITOR_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___STRING_OR_MISSING__STRING = McorePackage.MEDITOR_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___STRING_IS_EMPTY__STRING = McorePackage.MEDITOR_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = McorePackage.MEDITOR_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = McorePackage.MEDITOR_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___DO_ACTION$_UPDATE__MEDITORCONFIGACTION = McorePackage.MEDITOR_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Table Config From Class</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___TABLE_CONFIG_FROM_CLASS__MCLASSIFIER = McorePackage.MEDITOR_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___DO_ACTION_UPDATE__MEDITORCONFIGACTION = McorePackage.MEDITOR_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Reset Editor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG___RESET_EDITOR = McorePackage.MEDITOR_OPERATION_COUNT + 10;

	/**
	 * The number of operations of the '<em>MEditor Config</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDITOR_CONFIG_OPERATION_COUNT = McorePackage.MEDITOR_OPERATION_COUNT + 11;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MElementWithFontImpl <em>MElement With Font</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MElementWithFontImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMElementWithFont()
	 * @generated
	 */
	int MELEMENT_WITH_FONT = 11;

	/**
	 * The meta object id for the '
	 * {@link com.montages.mtableeditor.impl.MTableConfigImpl
	 * <em>MTable Config</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see com.montages.mtableeditor.impl.MTableConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableConfig()
	 * @generated
	 */
	int MTABLE_CONFIG = 2;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__ALABEL = MTABLE_EDITOR_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__AKIND_BASE = MTABLE_EDITOR_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__ARENDERED_KIND = MTABLE_EDITOR_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__KIND = MTABLE_EDITOR_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__NAME = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Column Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__COLUMN_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Reference To Table Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Intended Package</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__INTENDED_PACKAGE = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Intended Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__INTENDED_CLASS = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>EC Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__EC_NAME = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>EC Label</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__EC_LABEL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>EC Column Config</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__EC_COLUMN_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Containing Editor Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Complement Action Table</b></em>' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__LABEL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Display Default Column</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Display Header</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__DISPLAY_HEADER = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__DO_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Intended Action</b></em>' attribute list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__INTENDED_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Complement Action</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__COMPLEMENT_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Split Children Action</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__SPLIT_CHILDREN_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Merge Children Action</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG__MERGE_CHILDREN_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The number of structural features of the '<em>MTable Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG_FEATURE_COUNT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 18;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___INDENT_LEVEL = MTABLE_EDITOR_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___INDENTATION_SPACES = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___INDENTATION_SPACES__INTEGER = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___STRING_OR_MISSING__STRING = MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___STRING_IS_EMPTY__STRING = MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___DO_ACTION$_UPDATE__MTABLECONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Table Config From Reference</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___TABLE_CONFIG_FROM_REFERENCE__MPROPERTY = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get All Properties</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___GET_ALL_PROPERTIES = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___DO_ACTION_UPDATE__MTABLECONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MEDITORCONFIG_MTABLECONFIGACTION_ELIST_BOOLEAN = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MTABLECONFIG_MTABLECONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Invoke Set Do Action</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>MTable Config</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MTABLE_CONFIG_OPERATION_COUNT = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MColumnConfigImpl <em>MColumn Config</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MColumnConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMColumnConfig()
	 * @generated
	 */
	int MCOLUMN_CONFIG = 3;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__ALABEL = MTABLE_EDITOR_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__AKIND_BASE = MTABLE_EDITOR_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__ARENDERED_KIND = MTABLE_EDITOR_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__KIND = MTABLE_EDITOR_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__BOLD_FONT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__ITALIC_FONT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__FONT_SIZE = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__NAME = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Class To Cell Config</b></em>' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>EC Name</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__EC_NAME = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>EC Label</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__EC_LABEL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>EC Width</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__EC_WIDTH = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__LABEL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Width</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__WIDTH = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Minimum Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__MINIMUM_WIDTH = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Maximum Width</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__MAXIMUM_WIDTH = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Containing Table Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG__DO_ACTION = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 14;

	/**
	 * The number of structural features of the '<em>MColumn Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG_FEATURE_COUNT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 15;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___INDENT_LEVEL = MTABLE_EDITOR_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___INDENTATION_SPACES = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___INDENTATION_SPACES__INTEGER = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___STRING_OR_MISSING__STRING = MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___STRING_IS_EMPTY__STRING = MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___DO_ACTION$_UPDATE__MCOLUMNCONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Cell Config From Class</em>' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___CELL_CONFIG_FROM_CLASS__MCLASSIFIER = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG___DO_ACTION_UPDATE__MCOLUMNCONFIGACTION = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>MColumn Config</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCOLUMN_CONFIG_OPERATION_COUNT = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MClassToCellConfigImpl <em>MClass To Cell Config</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MClassToCellConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMClassToCellConfig()
	 * @generated
	 */
	int MCLASS_TO_CELL_CONFIG = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__ALABEL = MTABLE_EDITOR_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__AKIND_BASE = MTABLE_EDITOR_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__ARENDERED_KIND = MTABLE_EDITOR_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__KIND = MTABLE_EDITOR_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__CLASS = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cell Config</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__CELL_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Containing Column Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG__CELL_KIND = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MClass To Cell Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG_FEATURE_COUNT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___INDENT_LEVEL = MTABLE_EDITOR_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___INDENTATION_SPACES = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___INDENTATION_SPACES__INTEGER = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___STRING_OR_MISSING__STRING = MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___STRING_IS_EMPTY__STRING = MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MClass To Cell Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_CELL_CONFIG_OPERATION_COUNT = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '
	 * {@link com.montages.mtableeditor.impl.MCellConfigImpl
	 * <em>MCell Config</em>}' class. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see com.montages.mtableeditor.impl.MCellConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellConfig()
	 * @generated
	 */
	int MCELL_CONFIG = 5;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__ALABEL = MTABLE_EDITOR_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__AKIND_BASE = MTABLE_EDITOR_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__ARENDERED_KIND = MTABLE_EDITOR_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__KIND = MTABLE_EDITOR_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__CLASS = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cell Config</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__CELL_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Containing Column Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__CONTAINING_COLUMN_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__CELL_KIND = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__BOLD_FONT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__ITALIC_FONT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__FONT_SIZE = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Explicit Highlight OCL</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Containing Class To Cell Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>MCell Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG_FEATURE_COUNT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___INDENT_LEVEL = MTABLE_EDITOR_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___INDENTATION_SPACES = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___INDENTATION_SPACES__INTEGER = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___STRING_OR_MISSING__STRING = MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___STRING_IS_EMPTY__STRING = MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MCell Config</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCELL_CONFIG_OPERATION_COUNT = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl <em>MRow Feature Cell</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MRowFeatureCellImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMRowFeatureCell()
	 * @generated
	 */
	int MROW_FEATURE_CELL = 6;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__ALABEL = MCELL_CONFIG__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__AKIND_BASE = MCELL_CONFIG__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__ARENDERED_KIND = MCELL_CONFIG__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__KIND = MCELL_CONFIG__KIND;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CLASS = MCELL_CONFIG__CLASS;

	/**
	 * The feature id for the '<em><b>Cell Config</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CELL_CONFIG = MCELL_CONFIG__CELL_CONFIG;

	/**
	 * The feature id for the '<em><b>Containing Column Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CONTAINING_COLUMN_CONFIG = MCELL_CONFIG__CONTAINING_COLUMN_CONFIG;

	/**
	 * The feature id for the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CELL_KIND = MCELL_CONFIG__CELL_KIND;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__BOLD_FONT = MCELL_CONFIG__BOLD_FONT;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__ITALIC_FONT = MCELL_CONFIG__ITALIC_FONT;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__FONT_SIZE = MCELL_CONFIG__FONT_SIZE;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__DERIVED_FONT_OPTIONS_ENCODING = MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING;

	/**
	 * The feature id for the '<em><b>Explicit Highlight OCL</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__EXPLICIT_HIGHLIGHT_OCL = MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL;

	/**
	 * The feature id for the '<em><b>Containing Class To Cell Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CONTAINING_CLASS_TO_CELL_CONFIG = MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG;

	/**
	 * The feature id for the '<em><b>Feature</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__FEATURE = MCELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cell Edit Behavior</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR = MCELL_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Cell Locate Behavior</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR = MCELL_CONFIG_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Maximum Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__MAXIMUM_SIZE = MCELL_CONFIG_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Average Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__AVERAGE_SIZE = MCELL_CONFIG_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Minimum Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__MINIMUM_SIZE = MCELL_CONFIG_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Size Of Property Value</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE = MCELL_CONFIG_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>MRow Feature Cell</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL_FEATURE_COUNT = MCELL_CONFIG_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___INDENT_LEVEL = MCELL_CONFIG___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___INDENTATION_SPACES = MCELL_CONFIG___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___INDENTATION_SPACES__INTEGER = MCELL_CONFIG___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___STRING_OR_MISSING__STRING = MCELL_CONFIG___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___STRING_IS_EMPTY__STRING = MCELL_CONFIG___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MRow Feature Cell</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MROW_FEATURE_CELL_OPERATION_COUNT = MCELL_CONFIG_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MOclCellImpl <em>MOcl Cell</em>}' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MOclCellImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMOclCell()
	 * @generated
	 */
	int MOCL_CELL = 7;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__ALABEL = MCELL_CONFIG__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__AKIND_BASE = MCELL_CONFIG__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__ARENDERED_KIND = MCELL_CONFIG__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__KIND = MCELL_CONFIG__KIND;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__CLASS = MCELL_CONFIG__CLASS;

	/**
	 * The feature id for the '<em><b>Cell Config</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__CELL_CONFIG = MCELL_CONFIG__CELL_CONFIG;

	/**
	 * The feature id for the '<em><b>Containing Column Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__CONTAINING_COLUMN_CONFIG = MCELL_CONFIG__CONTAINING_COLUMN_CONFIG;

	/**
	 * The feature id for the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__CELL_KIND = MCELL_CONFIG__CELL_KIND;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__BOLD_FONT = MCELL_CONFIG__BOLD_FONT;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__ITALIC_FONT = MCELL_CONFIG__ITALIC_FONT;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__FONT_SIZE = MCELL_CONFIG__FONT_SIZE;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__DERIVED_FONT_OPTIONS_ENCODING = MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING;

	/**
	 * The feature id for the '<em><b>Explicit Highlight OCL</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__EXPLICIT_HIGHLIGHT_OCL = MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL;

	/**
	 * The feature id for the '<em><b>Containing Class To Cell Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__CONTAINING_CLASS_TO_CELL_CONFIG = MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG;

	/**
	 * The feature id for the '<em><b>String Rendering</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL__STRING_RENDERING = MCELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>MOcl Cell</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL_FEATURE_COUNT = MCELL_CONFIG_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___INDENT_LEVEL = MCELL_CONFIG___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___INDENTATION_SPACES = MCELL_CONFIG___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___INDENTATION_SPACES__INTEGER = MCELL_CONFIG___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___STRING_OR_MISSING__STRING = MCELL_CONFIG___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___STRING_IS_EMPTY__STRING = MCELL_CONFIG___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MOcl Cell</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MOCL_CELL_OPERATION_COUNT = MCELL_CONFIG_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MEditProviderCellImpl <em>MEdit Provider Cell</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MEditProviderCellImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditProviderCell()
	 * @generated
	 */
	int MEDIT_PROVIDER_CELL = 8;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__ALABEL = MCELL_CONFIG__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__AKIND_BASE = MCELL_CONFIG__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__ARENDERED_KIND = MCELL_CONFIG__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__KIND = MCELL_CONFIG__KIND;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__CLASS = MCELL_CONFIG__CLASS;

	/**
	 * The feature id for the '<em><b>Cell Config</b></em>' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__CELL_CONFIG = MCELL_CONFIG__CELL_CONFIG;

	/**
	 * The feature id for the '<em><b>Containing Column Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__CONTAINING_COLUMN_CONFIG = MCELL_CONFIG__CONTAINING_COLUMN_CONFIG;

	/**
	 * The feature id for the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__CELL_KIND = MCELL_CONFIG__CELL_KIND;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__BOLD_FONT = MCELL_CONFIG__BOLD_FONT;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__ITALIC_FONT = MCELL_CONFIG__ITALIC_FONT;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__FONT_SIZE = MCELL_CONFIG__FONT_SIZE;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__DERIVED_FONT_OPTIONS_ENCODING = MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING;

	/**
	 * The feature id for the '<em><b>Explicit Highlight OCL</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__EXPLICIT_HIGHLIGHT_OCL = MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL;

	/**
	 * The feature id for the '<em><b>Containing Class To Cell Config</b></em>' container reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL__CONTAINING_CLASS_TO_CELL_CONFIG = MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG;

	/**
	 * The number of structural features of the '<em>MEdit Provider Cell</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL_FEATURE_COUNT = MCELL_CONFIG_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___INDENT_LEVEL = MCELL_CONFIG___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___INDENTATION_SPACES = MCELL_CONFIG___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___INDENTATION_SPACES__INTEGER = MCELL_CONFIG___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___STRING_OR_MISSING__STRING = MCELL_CONFIG___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___STRING_IS_EMPTY__STRING = MCELL_CONFIG___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MCELL_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MEdit Provider Cell</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDIT_PROVIDER_CELL_OPERATION_COUNT = MCELL_CONFIG_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MReferenceToTableConfigImpl <em>MReference To Table Config</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MReferenceToTableConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMReferenceToTableConfig()
	 * @generated
	 */
	int MREFERENCE_TO_TABLE_CONFIG = 9;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__ALABEL = MTABLE_EDITOR_ELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__AKIND_BASE = MTABLE_EDITOR_ELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__ARENDERED_KIND = MTABLE_EDITOR_ELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__KIND = MTABLE_EDITOR_ELEMENT__KIND;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__REFERENCE = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Table Config</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG__LABEL = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>MReference To Table Config</em>' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG_FEATURE_COUNT = MTABLE_EDITOR_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___INDENT_LEVEL = MTABLE_EDITOR_ELEMENT___INDENT_LEVEL;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___INDENTATION_SPACES = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___INDENTATION_SPACES__INTEGER = MTABLE_EDITOR_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___STRING_OR_MISSING__STRING = MTABLE_EDITOR_ELEMENT___STRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___STRING_IS_EMPTY__STRING = MTABLE_EDITOR_ELEMENT___STRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '
	 * <em>List Of String To String With Separator</em>' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MTABLE_EDITOR_ELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The number of operations of the '<em>MReference To Table Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREFERENCE_TO_TABLE_CONFIG_OPERATION_COUNT = MTABLE_EDITOR_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.impl.MClassToTableConfigImpl <em>MClass To Table Config</em>}' class.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.impl.MClassToTableConfigImpl
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMClassToTableConfig()
	 * @generated
	 */
	int MCLASS_TO_TABLE_CONFIG = 10;

	/**
	 * The feature id for the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_TABLE_CONFIG__CLASS = 0;

	/**
	 * The feature id for the '<em><b>Table Config</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG = 1;

	/**
	 * The feature id for the '<em><b>Intended Package</b></em>' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE = 2;

	/**
	 * The number of structural features of the '<em>MClass To Table Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_TABLE_CONFIG_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>MClass To Table Config</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASS_TO_TABLE_CONFIG_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Bold Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT__BOLD_FONT = 0;

	/**
	 * The feature id for the '<em><b>Italic Font</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT__ITALIC_FONT = 1;

	/**
	 * The feature id for the '<em><b>Font Size</b></em>' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT__FONT_SIZE = 2;

	/**
	 * The feature id for the '<em><b>Derived Font Options Encoding</b></em>' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING = 3;

	/**
	 * The number of structural features of the '<em>MElement With Font</em>' class.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>MElement With Font</em>' class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	int MELEMENT_WITH_FONT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MCellEditBehaviorOption <em>MCell Edit Behavior Option</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.MCellEditBehaviorOption
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellEditBehaviorOption()
	 * @generated
	 */
	int MCELL_EDIT_BEHAVIOR_OPTION = 15;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MCellLocateBehaviorOption <em>MCell Locate Behavior Option</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mtableeditor.MCellLocateBehaviorOption
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellLocateBehaviorOption()
	 * @generated
	 */
	int MCELL_LOCATE_BEHAVIOR_OPTION = 16;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MFontSizeAdaptor <em>MFont Size Adaptor</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.MFontSizeAdaptor
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMFontSizeAdaptor()
	 * @generated
	 */
	int MFONT_SIZE_ADAPTOR = 17;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MEditorConfigAction <em>MEditor Config Action</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.MEditorConfigAction
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditorConfigAction()
	 * @generated
	 */
	int MEDITOR_CONFIG_ACTION = 12;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MTableConfigAction <em>MTable Config Action</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.MTableConfigAction
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableConfigAction()
	 * @generated
	 */
	int MTABLE_CONFIG_ACTION = 13;

	/**
	 * The meta object id for the '{@link com.montages.mtableeditor.MColumnConfigAction <em>MColumn Config Action</em>}' enum.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see com.montages.mtableeditor.MColumnConfigAction
	 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMColumnConfigAction()
	 * @generated
	 */
	int MCOLUMN_CONFIG_ACTION = 14;

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MTableEditorElement <em>MTable Editor Element</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>MTable Editor Element</em>'.
	 * @see com.montages.mtableeditor.MTableEditorElement
	 * @generated
	 */
	EClass getMTableEditorElement();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableEditorElement#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see com.montages.mtableeditor.MTableEditorElement#getKind()
	 * @see #getMTableEditorElement()
	 * @generated
	 */
	EAttribute getMTableEditorElement_Kind();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MEditorConfig <em>MEditor Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>MEditor Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig
	 * @generated
	 */
	EClass getMEditorConfig();

	/**
	 * Returns the meta object for the reference '
	 * {@link com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig
	 * <em>Header Table Config</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Header Table Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EReference getMEditorConfig_HeaderTableConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MEditorConfig#getMTableConfig <em>MTable Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>MTable Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getMTableConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EReference getMEditorConfig_MTableConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MEditorConfig#getMColumnConfig <em>MColumn Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>MColumn Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getMColumnConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EReference getMEditorConfig_MColumnConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MEditorConfig#getClassToTableConfig <em>Class To Table Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class To Table Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getClassToTableConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EReference getMEditorConfig_ClassToTableConfig();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MEditorConfig#getSuperConfig <em>Super Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Config</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getSuperConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EReference getMEditorConfig_SuperConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MEditorConfig#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mtableeditor.MEditorConfig#getDoAction()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	EAttribute getMEditorConfig_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MEditorConfig#doAction$Update(com.montages.mtableeditor.MEditorConfigAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mtableeditor.MEditorConfig#doAction$Update(com.montages.mtableeditor.MEditorConfigAction)
	 * @generated
	 */
	EOperation getMEditorConfig__DoAction$Update__MEditorConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MEditorConfig#tableConfigFromClass(com.montages.mcore.MClassifier) <em>Table Config From Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Table Config From Class</em>' operation.
	 * @see com.montages.mtableeditor.MEditorConfig#tableConfigFromClass(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMEditorConfig__TableConfigFromClass__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MEditorConfig#doActionUpdate(com.montages.mtableeditor.MEditorConfigAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mtableeditor.MEditorConfig#doActionUpdate(com.montages.mtableeditor.MEditorConfigAction)
	 * @generated
	 */
	EOperation getMEditorConfig__DoActionUpdate__MEditorConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MEditorConfig#resetEditor() <em>Reset Editor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset Editor</em>' operation.
	 * @see com.montages.mtableeditor.MEditorConfig#resetEditor()
	 * @generated
	 */
	EOperation getMEditorConfig__ResetEditor();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MTableConfig <em>MTable Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>MTable Config</em>'.
	 * @see com.montages.mtableeditor.MTableConfig
	 * @generated
	 */
	EClass getMTableConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getName()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MTableConfig#getColumnConfig <em>Column Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Column Config</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getColumnConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_ColumnConfig();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MTableConfig#getReferenceToTableConfig <em>Reference To Table Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reference To Table Config</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getReferenceToTableConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_ReferenceToTableConfig();

	/**
	 * Returns the meta object for the reference '
	 * {@link com.montages.mtableeditor.MTableConfig#getIntendedPackage
	 * <em>Intended Package</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Intended Package</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getIntendedPackage()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_IntendedPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MTableConfig#getIntendedClass <em>Intended Class</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Intended Class</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getIntendedClass()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_IntendedClass();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getECName <em>EC Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EC Name</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getECName()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_ECName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getECLabel <em>EC Label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EC Label</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getECLabel()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_ECLabel();

	/**
	 * Returns the meta object for the reference list '
	 * {@link com.montages.mtableeditor.MTableConfig#getECColumnConfig
	 * <em>EC Column Config</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference list '<em>EC Column Config</em>
	 *         '.
	 * @see com.montages.mtableeditor.MTableConfig#getECColumnConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_ECColumnConfig();

	/**
	 * Returns the meta object for the container reference '{@link com.montages.mtableeditor.MTableConfig#getContainingEditorConfig <em>Containing Editor Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Editor Config</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getContainingEditorConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_ContainingEditorConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getLabel()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn <em>Display Default Column</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Default Column</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_DisplayDefaultColumn();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getDisplayHeader <em>Display Header</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Display Header</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getDisplayHeader()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_DisplayHeader();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getDoAction()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_DoAction();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mtableeditor.MTableConfig#getIntendedAction <em>Intended Action</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Intended Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getIntendedAction()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_IntendedAction();

	/**
	 * Returns the meta object for the attribute '
	 * {@link com.montages.mtableeditor.MTableConfig#getComplementAction
	 * <em>Complement Action</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Complement Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getComplementAction()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_ComplementAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getSplitChildrenAction <em>Split Children Action</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Split Children Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getSplitChildrenAction()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_SplitChildrenAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MTableConfig#getMergeChildrenAction <em>Merge Children Action</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Merge Children Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getMergeChildrenAction()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EAttribute getMTableConfig_MergeChildrenAction();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mtableeditor.MTableConfig#getComplementActionTable <em>Complement Action Table</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the reference list '<em>Complement Action Table</em>'.
	 * @see com.montages.mtableeditor.MTableConfig#getComplementActionTable()
	 * @see #getMTableConfig()
	 * @generated
	 */
	EReference getMTableConfig_ComplementActionTable();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#doAction$Update(com.montages.mtableeditor.MTableConfigAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#doAction$Update(com.montages.mtableeditor.MTableConfigAction)
	 * @generated
	 */
	EOperation getMTableConfig__DoAction$Update__MTableConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#tableConfigFromReference(com.montages.mcore.MProperty) <em>Table Config From Reference</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Table Config From Reference</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#tableConfigFromReference(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMTableConfig__TableConfigFromReference__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#getAllProperties() <em>Get All Properties</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Get All Properties</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#getAllProperties()
	 * @generated
	 */
	EOperation getMTableConfig__GetAllProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#doActionUpdate(com.montages.mtableeditor.MTableConfigAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#doActionUpdate(com.montages.mtableeditor.MTableConfigAction)
	 * @generated
	 */
	EOperation getMTableConfig__DoActionUpdate__MTableConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#doActionUpdate(org.xocl.semantics.XUpdate, com.montages.mtableeditor.MEditorConfig, com.montages.mtableeditor.MTableConfigAction, org.eclipse.emf.common.util.EList, java.lang.Boolean) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#doActionUpdate(org.xocl.semantics.XUpdate, com.montages.mtableeditor.MEditorConfig, com.montages.mtableeditor.MTableConfigAction, org.eclipse.emf.common.util.EList, java.lang.Boolean)
	 * @generated
	 */
	EOperation getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#doActionUpdate(org.xocl.semantics.XUpdate, com.montages.mtableeditor.MTableConfig, com.montages.mtableeditor.MTableConfigAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#doActionUpdate(org.xocl.semantics.XUpdate, com.montages.mtableeditor.MTableConfig, com.montages.mtableeditor.MTableConfigAction)
	 * @generated
	 */
	EOperation getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MTableConfig#invokeSetDoAction(com.montages.mtableeditor.MTableConfigAction) <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Do Action</em>' operation.
	 * @see com.montages.mtableeditor.MTableConfig#invokeSetDoAction(com.montages.mtableeditor.MTableConfigAction)
	 * @generated
	 */
	EOperation getMTableConfig__InvokeSetDoAction__MTableConfigAction();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MColumnConfig <em>MColumn Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>MColumn Config</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig
	 * @generated
	 */
	EClass getMColumnConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getName <em>Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getName()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_Name();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mtableeditor.MColumnConfig#getClassToCellConfig <em>Class To Cell Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Class To Cell Config</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getClassToCellConfig()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EReference getMColumnConfig_ClassToCellConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getECName <em>EC Name</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EC Name</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getECName()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_ECName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getECLabel <em>EC Label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EC Label</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getECLabel()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_ECLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getECWidth <em>EC Width</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EC Width</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getECWidth()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_ECWidth();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getLabel()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getWidth <em>Width</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Width</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getWidth()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_Width();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getMinimumWidth <em>Minimum Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum Width</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getMinimumWidth()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_MinimumWidth();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getMaximumWidth <em>Maximum Width</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Width</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getMaximumWidth()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_MaximumWidth();

	/**
	 * Returns the meta object for the container reference '{@link com.montages.mtableeditor.MColumnConfig#getContainingTableConfig <em>Containing Table Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Table Config</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getContainingTableConfig()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EReference getMColumnConfig_ContainingTableConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MColumnConfig#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mtableeditor.MColumnConfig#getDoAction()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	EAttribute getMColumnConfig_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MColumnConfig#doAction$Update(com.montages.mtableeditor.MColumnConfigAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mtableeditor.MColumnConfig#doAction$Update(com.montages.mtableeditor.MColumnConfigAction)
	 * @generated
	 */
	EOperation getMColumnConfig__DoAction$Update__MColumnConfigAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MColumnConfig#cellConfigFromClass(com.montages.mcore.MClassifier) <em>Cell Config From Class</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Cell Config From Class</em>' operation.
	 * @see com.montages.mtableeditor.MColumnConfig#cellConfigFromClass(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMColumnConfig__CellConfigFromClass__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mtableeditor.MColumnConfig#doActionUpdate(com.montages.mtableeditor.MColumnConfigAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mtableeditor.MColumnConfig#doActionUpdate(com.montages.mtableeditor.MColumnConfigAction)
	 * @generated
	 */
	EOperation getMColumnConfig__DoActionUpdate__MColumnConfigAction();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MClassToCellConfig <em>MClass To Cell Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>MClass To Cell Config</em>'.
	 * @see com.montages.mtableeditor.MClassToCellConfig
	 * @generated
	 */
	EClass getMClassToCellConfig();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MClassToCellConfig#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class</em>'.
	 * @see com.montages.mtableeditor.MClassToCellConfig#getClass_()
	 * @see #getMClassToCellConfig()
	 * @generated
	 */
	EReference getMClassToCellConfig_Class();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cell Config</em>'.
	 * @see com.montages.mtableeditor.MClassToCellConfig#getCellConfig()
	 * @see #getMClassToCellConfig()
	 * @generated
	 */
	EReference getMClassToCellConfig_CellConfig();

	/**
	 * Returns the meta object for the container reference '{@link com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig <em>Containing Column Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Column Config</em>'.
	 * @see com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig()
	 * @see #getMClassToCellConfig()
	 * @generated
	 */
	EReference getMClassToCellConfig_ContainingColumnConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MClassToCellConfig#getCellKind <em>Cell Kind</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cell Kind</em>'.
	 * @see com.montages.mtableeditor.MClassToCellConfig#getCellKind()
	 * @see #getMClassToCellConfig()
	 * @generated
	 */
	EAttribute getMClassToCellConfig_CellKind();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MCellConfig <em>MCell Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for class '<em>MCell Config</em>'.
	 * @see com.montages.mtableeditor.MCellConfig
	 * @generated
	 */
	EClass getMCellConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Explicit Highlight OCL</em>'.
	 * @see com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL()
	 * @see #getMCellConfig()
	 * @generated
	 */
	EAttribute getMCellConfig_ExplicitHighlightOCL();

	/**
	 * Returns the meta object for the container reference '{@link com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig <em>Containing Class To Cell Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the container reference '<em>Containing Class To Cell Config</em>'.
	 * @see com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig()
	 * @see #getMCellConfig()
	 * @generated
	 */
	EReference getMCellConfig_ContainingClassToCellConfig();

	/**
	 * Returns the meta object for class '
	 * {@link com.montages.mtableeditor.MRowFeatureCell
	 * <em>MRow Feature Cell</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>MRow Feature Cell</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell
	 * @generated
	 */
	EClass getMRowFeatureCell();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MRowFeatureCell#getFeature <em>Feature</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Feature</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getFeature()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EReference getMRowFeatureCell_Feature();

	/**
	 * Returns the meta object for the attribute '
	 * {@link com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior
	 * <em>Cell Edit Behavior</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>Cell Edit Behavior</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getCellEditBehavior()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_CellEditBehavior();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Cell Locate Behavior</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getCellLocateBehavior()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_CellLocateBehavior();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MRowFeatureCell#getMaximumSize <em>Maximum Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Maximum Size</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getMaximumSize()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_MaximumSize();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MRowFeatureCell#getAverageSize <em>Average Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Average Size</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getAverageSize()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_AverageSize();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MRowFeatureCell#getMinimumSize <em>Minimum Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimum Size</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getMinimumSize()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_MinimumSize();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mtableeditor.MRowFeatureCell#getSizeOfPropertyValue <em>Size Of Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Size Of Property Value</em>'.
	 * @see com.montages.mtableeditor.MRowFeatureCell#getSizeOfPropertyValue()
	 * @see #getMRowFeatureCell()
	 * @generated
	 */
	EAttribute getMRowFeatureCell_SizeOfPropertyValue();

	/**
	 * Returns the meta object for class '
	 * {@link com.montages.mtableeditor.MOclCell <em>MOcl Cell</em>}'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the meta object for class '<em>MOcl Cell</em>'.
	 * @see com.montages.mtableeditor.MOclCell
	 * @generated
	 */
	EClass getMOclCell();

	/**
	 * Returns the meta object for the attribute '
	 * {@link com.montages.mtableeditor.MOclCell#getStringRendering
	 * <em>String Rendering</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the attribute '<em>String Rendering</em>'.
	 * @see com.montages.mtableeditor.MOclCell#getStringRendering()
	 * @see #getMOclCell()
	 * @generated
	 */
	EAttribute getMOclCell_StringRendering();

	/**
	 * Returns the meta object for class '
	 * {@link com.montages.mtableeditor.MEditProviderCell
	 * <em>MEdit Provider Cell</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>MEdit Provider Cell</em>'.
	 * @see com.montages.mtableeditor.MEditProviderCell
	 * @generated
	 */
	EClass getMEditProviderCell();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MReferenceToTableConfig <em>MReference To Table Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>MReference To Table Config</em>'.
	 * @see com.montages.mtableeditor.MReferenceToTableConfig
	 * @generated
	 */
	EClass getMReferenceToTableConfig();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MReferenceToTableConfig#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see com.montages.mtableeditor.MReferenceToTableConfig#getReference()
	 * @see #getMReferenceToTableConfig()
	 * @generated
	 */
	EReference getMReferenceToTableConfig_Reference();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig <em>Table Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Table Config</em>'.
	 * @see com.montages.mtableeditor.MReferenceToTableConfig#getTableConfig()
	 * @see #getMReferenceToTableConfig()
	 * @generated
	 */
	EReference getMReferenceToTableConfig_TableConfig();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MReferenceToTableConfig#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.montages.mtableeditor.MReferenceToTableConfig#getLabel()
	 * @see #getMReferenceToTableConfig()
	 * @generated
	 */
	EAttribute getMReferenceToTableConfig_Label();

	/**
	 * Returns the meta object for class '{@link com.montages.mtableeditor.MClassToTableConfig <em>MClass To Table Config</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for class '<em>MClass To Table Config</em>'.
	 * @see com.montages.mtableeditor.MClassToTableConfig
	 * @generated
	 */
	EClass getMClassToTableConfig();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MClassToTableConfig#getClass_ <em>Class</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Class</em>'.
	 * @see com.montages.mtableeditor.MClassToTableConfig#getClass_()
	 * @see #getMClassToTableConfig()
	 * @generated
	 */
	EReference getMClassToTableConfig_Class();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mtableeditor.MClassToTableConfig#getTableConfig <em>Table Config</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Table Config</em>'.
	 * @see com.montages.mtableeditor.MClassToTableConfig#getTableConfig()
	 * @see #getMClassToTableConfig()
	 * @generated
	 */
	EReference getMClassToTableConfig_TableConfig();

	/**
	 * Returns the meta object for the reference '
	 * {@link com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage
	 * <em>Intended Package</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for the reference '<em>Intended Package</em>'.
	 * @see com.montages.mtableeditor.MClassToTableConfig#getIntendedPackage()
	 * @see #getMClassToTableConfig()
	 * @generated
	 */
	EReference getMClassToTableConfig_IntendedPackage();

	/**
	 * Returns the meta object for class '
	 * {@link com.montages.mtableeditor.MElementWithFont
	 * <em>MElement With Font</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for class '<em>MElement With Font</em>'.
	 * @see com.montages.mtableeditor.MElementWithFont
	 * @generated
	 */
	EClass getMElementWithFont();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MElementWithFont#getFontSize <em>Font Size</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Font Size</em>'.
	 * @see com.montages.mtableeditor.MElementWithFont#getFontSize()
	 * @see #getMElementWithFont()
	 * @generated
	 */
	EAttribute getMElementWithFont_FontSize();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MElementWithFont#getBoldFont <em>Bold Font</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bold Font</em>'.
	 * @see com.montages.mtableeditor.MElementWithFont#getBoldFont()
	 * @see #getMElementWithFont()
	 * @generated
	 */
	EAttribute getMElementWithFont_BoldFont();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MElementWithFont#getItalicFont <em>Italic Font</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Italic Font</em>'.
	 * @see com.montages.mtableeditor.MElementWithFont#getItalicFont()
	 * @see #getMElementWithFont()
	 * @generated
	 */
	EAttribute getMElementWithFont_ItalicFont();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mtableeditor.MElementWithFont#getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Font Options Encoding</em>'.
	 * @see com.montages.mtableeditor.MElementWithFont#getDerivedFontOptionsEncoding()
	 * @see #getMElementWithFont()
	 * @generated
	 */
	EAttribute getMElementWithFont_DerivedFontOptionsEncoding();

	/**
	 * Returns the meta object for enum '{@link com.montages.mtableeditor.MCellEditBehaviorOption <em>MCell Edit Behavior Option</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>MCell Edit Behavior Option</em>'.
	 * @see com.montages.mtableeditor.MCellEditBehaviorOption
	 * @generated
	 */
	EEnum getMCellEditBehaviorOption();

	/**
	 * Returns the meta object for enum '{@link com.montages.mtableeditor.MCellLocateBehaviorOption <em>MCell Locate Behavior Option</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>MCell Locate Behavior Option</em>'.
	 * @see com.montages.mtableeditor.MCellLocateBehaviorOption
	 * @generated
	 */
	EEnum getMCellLocateBehaviorOption();

	/**
	 * Returns the meta object for enum '
	 * {@link com.montages.mtableeditor.MFontSizeAdaptor
	 * <em>MFont Size Adaptor</em>}'. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @return the meta object for enum '<em>MFont Size Adaptor</em>'.
	 * @see com.montages.mtableeditor.MFontSizeAdaptor
	 * @generated
	 */
	EEnum getMFontSizeAdaptor();

	/**
	 * Returns the meta object for enum '{@link com.montages.mtableeditor.MEditorConfigAction <em>MEditor Config Action</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>MEditor Config Action</em>'.
	 * @see com.montages.mtableeditor.MEditorConfigAction
	 * @generated
	 */
	EEnum getMEditorConfigAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mtableeditor.MTableConfigAction <em>MTable Config Action</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>MTable Config Action</em>'.
	 * @see com.montages.mtableeditor.MTableConfigAction
	 * @generated
	 */
	EEnum getMTableConfigAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mtableeditor.MColumnConfigAction <em>MColumn Config Action</em>}'.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the meta object for enum '<em>MColumn Config Action</em>'.
	 * @see com.montages.mtableeditor.MColumnConfigAction
	 * @generated
	 */
	EEnum getMColumnConfigAction();

	/**
	 * Returns the factory that creates the instances of the model. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MtableeditorFactory getMtableeditorFactory();

	/**
	 * <!-- begin-user-doc --> Defines literals for the meta objects that
	 * represent
	 * <ul>
	 * <li>each class,</li>
	 * <li>each feature of each class,</li>
	 * <li>each operation of each class,</li>
	 * <li>each enum,</li>
	 * <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MTableEditorElementImpl <em>MTable Editor Element</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MTableEditorElementImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableEditorElement()
		 * @generated
		 */
		EClass MTABLE_EDITOR_ELEMENT = eINSTANCE.getMTableEditorElement();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_EDITOR_ELEMENT__KIND = eINSTANCE.getMTableEditorElement_Kind();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MEditorConfigImpl <em>MEditor Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MEditorConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditorConfig()
		 * @generated
		 */
		EClass MEDITOR_CONFIG = eINSTANCE.getMEditorConfig();

		/**
		 * The meta object literal for the '<em><b>Header Table Config</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDITOR_CONFIG__HEADER_TABLE_CONFIG = eINSTANCE.getMEditorConfig_HeaderTableConfig();

		/**
		 * The meta object literal for the '<em><b>MTable Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MEDITOR_CONFIG__MTABLE_CONFIG = eINSTANCE.getMEditorConfig_MTableConfig();

		/**
		 * The meta object literal for the '<em><b>MColumn Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MEDITOR_CONFIG__MCOLUMN_CONFIG = eINSTANCE.getMEditorConfig_MColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Class To Table Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG = eINSTANCE.getMEditorConfig_ClassToTableConfig();

		/**
		 * The meta object literal for the '<em><b>Super Config</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEDITOR_CONFIG__SUPER_CONFIG = eINSTANCE.getMEditorConfig_SuperConfig();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEDITOR_CONFIG__DO_ACTION = eINSTANCE.getMEditorConfig_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDITOR_CONFIG___DO_ACTION$_UPDATE__MEDITORCONFIGACTION = eINSTANCE
				.getMEditorConfig__DoAction$Update__MEditorConfigAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Table Config From Class</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation MEDITOR_CONFIG___TABLE_CONFIG_FROM_CLASS__MCLASSIFIER = eINSTANCE
				.getMEditorConfig__TableConfigFromClass__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDITOR_CONFIG___DO_ACTION_UPDATE__MEDITORCONFIGACTION = eINSTANCE
				.getMEditorConfig__DoActionUpdate__MEditorConfigAction();

		/**
		 * The meta object literal for the '<em><b>Reset Editor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDITOR_CONFIG___RESET_EDITOR = eINSTANCE.getMEditorConfig__ResetEditor();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MTableConfigImpl <em>MTable Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MTableConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableConfig()
		 * @generated
		 */
		EClass MTABLE_CONFIG = eINSTANCE.getMTableConfig();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__NAME = eINSTANCE.getMTableConfig_Name();

		/**
		 * The meta object literal for the '<em><b>Column Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__COLUMN_CONFIG = eINSTANCE.getMTableConfig_ColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Reference To Table Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG = eINSTANCE.getMTableConfig_ReferenceToTableConfig();

		/**
		 * The meta object literal for the '<em><b>Intended Package</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__INTENDED_PACKAGE = eINSTANCE.getMTableConfig_IntendedPackage();

		/**
		 * The meta object literal for the '<em><b>Intended Class</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__INTENDED_CLASS = eINSTANCE.getMTableConfig_IntendedClass();

		/**
		 * The meta object literal for the '<em><b>EC Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__EC_NAME = eINSTANCE.getMTableConfig_ECName();

		/**
		 * The meta object literal for the '<em><b>EC Label</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__EC_LABEL = eINSTANCE.getMTableConfig_ECLabel();

		/**
		 * The meta object literal for the '<em><b>EC Column Config</b></em>' reference list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__EC_COLUMN_CONFIG = eINSTANCE.getMTableConfig_ECColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Containing Editor Config</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG = eINSTANCE.getMTableConfig_ContainingEditorConfig();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__LABEL = eINSTANCE.getMTableConfig_Label();

		/**
		 * The meta object literal for the '
		 * <em><b>Display Default Column</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN = eINSTANCE.getMTableConfig_DisplayDefaultColumn();

		/**
		 * The meta object literal for the '<em><b>Display Header</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__DISPLAY_HEADER = eINSTANCE.getMTableConfig_DisplayHeader();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__DO_ACTION = eINSTANCE.getMTableConfig_DoAction();

		/**
		 * The meta object literal for the '<em><b>Intended Action</b></em>' attribute list feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__INTENDED_ACTION = eINSTANCE.getMTableConfig_IntendedAction();

		/**
		 * The meta object literal for the '<em><b>Complement Action</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__COMPLEMENT_ACTION = eINSTANCE.getMTableConfig_ComplementAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Split Children Action</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__SPLIT_CHILDREN_ACTION = eINSTANCE.getMTableConfig_SplitChildrenAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Merge Children Action</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MTABLE_CONFIG__MERGE_CHILDREN_ACTION = eINSTANCE.getMTableConfig_MergeChildrenAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Complement Action Table</b></em>' reference list feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EReference MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE = eINSTANCE.getMTableConfig_ComplementActionTable();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___DO_ACTION$_UPDATE__MTABLECONFIGACTION = eINSTANCE
				.getMTableConfig__DoAction$Update__MTableConfigAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Table Config From Reference</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation MTABLE_CONFIG___TABLE_CONFIG_FROM_REFERENCE__MPROPERTY = eINSTANCE
				.getMTableConfig__TableConfigFromReference__MProperty();

		/**
		 * The meta object literal for the '<em><b>Get All Properties</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___GET_ALL_PROPERTIES = eINSTANCE.getMTableConfig__GetAllProperties();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___DO_ACTION_UPDATE__MTABLECONFIGACTION = eINSTANCE
				.getMTableConfig__DoActionUpdate__MTableConfigAction();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MEDITORCONFIG_MTABLECONFIGACTION_ELIST_BOOLEAN = eINSTANCE
				.getMTableConfig__DoActionUpdate__XUpdate_MEditorConfig_MTableConfigAction_EList_Boolean();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___DO_ACTION_UPDATE__XUPDATE_MTABLECONFIG_MTABLECONFIGACTION = eINSTANCE
				.getMTableConfig__DoActionUpdate__XUpdate_MTableConfig_MTableConfigAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Do Action</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTABLE_CONFIG___INVOKE_SET_DO_ACTION__MTABLECONFIGACTION = eINSTANCE
				.getMTableConfig__InvokeSetDoAction__MTableConfigAction();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MColumnConfigImpl <em>MColumn Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MColumnConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMColumnConfig()
		 * @generated
		 */
		EClass MCOLUMN_CONFIG = eINSTANCE.getMColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__NAME = eINSTANCE.getMColumnConfig_Name();

		/**
		 * The meta object literal for the '<em><b>Class To Cell Config</b></em>' containment reference list feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG = eINSTANCE.getMColumnConfig_ClassToCellConfig();

		/**
		 * The meta object literal for the '<em><b>EC Name</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__EC_NAME = eINSTANCE.getMColumnConfig_ECName();

		/**
		 * The meta object literal for the '<em><b>EC Label</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__EC_LABEL = eINSTANCE.getMColumnConfig_ECLabel();

		/**
		 * The meta object literal for the '<em><b>EC Width</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__EC_WIDTH = eINSTANCE.getMColumnConfig_ECWidth();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__LABEL = eINSTANCE.getMColumnConfig_Label();

		/**
		 * The meta object literal for the '<em><b>Width</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__WIDTH = eINSTANCE.getMColumnConfig_Width();

		/**
		 * The meta object literal for the '<em><b>Minimum Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__MINIMUM_WIDTH = eINSTANCE.getMColumnConfig_MinimumWidth();

		/**
		 * The meta object literal for the '<em><b>Maximum Width</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__MAXIMUM_WIDTH = eINSTANCE.getMColumnConfig_MaximumWidth();

		/**
		 * The meta object literal for the '<em><b>Containing Table Config</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG = eINSTANCE.getMColumnConfig_ContainingTableConfig();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOLUMN_CONFIG__DO_ACTION = eINSTANCE.getMColumnConfig_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCOLUMN_CONFIG___DO_ACTION$_UPDATE__MCOLUMNCONFIGACTION = eINSTANCE
				.getMColumnConfig__DoAction$Update__MColumnConfigAction();

		/**
		 * The meta object literal for the '
		 * <em><b>Cell Config From Class</b></em>' operation. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EOperation MCOLUMN_CONFIG___CELL_CONFIG_FROM_CLASS__MCLASSIFIER = eINSTANCE
				.getMColumnConfig__CellConfigFromClass__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCOLUMN_CONFIG___DO_ACTION_UPDATE__MCOLUMNCONFIGACTION = eINSTANCE
				.getMColumnConfig__DoActionUpdate__MColumnConfigAction();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MClassToCellConfigImpl <em>MClass To Cell Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MClassToCellConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMClassToCellConfig()
		 * @generated
		 */
		EClass MCLASS_TO_CELL_CONFIG = eINSTANCE.getMClassToCellConfig();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_CELL_CONFIG__CLASS = eINSTANCE.getMClassToCellConfig_Class();

		/**
		 * The meta object literal for the '<em><b>Cell Config</b></em>' containment reference feature.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_CELL_CONFIG__CELL_CONFIG = eINSTANCE.getMClassToCellConfig_CellConfig();

		/**
		 * The meta object literal for the '<em><b>Containing Column Config</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG = eINSTANCE
				.getMClassToCellConfig_ContainingColumnConfig();

		/**
		 * The meta object literal for the '<em><b>Cell Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASS_TO_CELL_CONFIG__CELL_KIND = eINSTANCE.getMClassToCellConfig_CellKind();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MCellConfigImpl <em>MCell Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MCellConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellConfig()
		 * @generated
		 */
		EClass MCELL_CONFIG = eINSTANCE.getMCellConfig();

		/**
		 * The meta object literal for the '
		 * <em><b>Explicit Highlight OCL</b></em>' attribute feature. <!--
		 * begin-user-doc --> <!-- end-user-doc -->
		 * 
		 * @generated
		 */
		EAttribute MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL = eINSTANCE.getMCellConfig_ExplicitHighlightOCL();

		/**
		 * The meta object literal for the '<em><b>Containing Class To Cell Config</b></em>' container reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG = eINSTANCE
				.getMCellConfig_ContainingClassToCellConfig();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MRowFeatureCellImpl <em>MRow Feature Cell</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MRowFeatureCellImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMRowFeatureCell()
		 * @generated
		 */
		EClass MROW_FEATURE_CELL = eINSTANCE.getMRowFeatureCell();

		/**
		 * The meta object literal for the '<em><b>Feature</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MROW_FEATURE_CELL__FEATURE = eINSTANCE.getMRowFeatureCell_Feature();

		/**
		 * The meta object literal for the '<em><b>Cell Edit Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR = eINSTANCE.getMRowFeatureCell_CellEditBehavior();

		/**
		 * The meta object literal for the '<em><b>Cell Locate Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR = eINSTANCE.getMRowFeatureCell_CellLocateBehavior();

		/**
		 * The meta object literal for the '<em><b>Maximum Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__MAXIMUM_SIZE = eINSTANCE.getMRowFeatureCell_MaximumSize();

		/**
		 * The meta object literal for the '<em><b>Average Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__AVERAGE_SIZE = eINSTANCE.getMRowFeatureCell_AverageSize();

		/**
		 * The meta object literal for the '<em><b>Minimum Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__MINIMUM_SIZE = eINSTANCE.getMRowFeatureCell_MinimumSize();

		/**
		 * The meta object literal for the '<em><b>Size Of Property Value</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MROW_FEATURE_CELL__SIZE_OF_PROPERTY_VALUE = eINSTANCE.getMRowFeatureCell_SizeOfPropertyValue();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MOclCellImpl <em>MOcl Cell</em>}' class.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MOclCellImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMOclCell()
		 * @generated
		 */
		EClass MOCL_CELL = eINSTANCE.getMOclCell();

		/**
		 * The meta object literal for the '<em><b>String Rendering</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOCL_CELL__STRING_RENDERING = eINSTANCE.getMOclCell_StringRendering();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MEditProviderCellImpl <em>MEdit Provider Cell</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MEditProviderCellImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditProviderCell()
		 * @generated
		 */
		EClass MEDIT_PROVIDER_CELL = eINSTANCE.getMEditProviderCell();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MReferenceToTableConfigImpl <em>MReference To Table Config</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MReferenceToTableConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMReferenceToTableConfig()
		 * @generated
		 */
		EClass MREFERENCE_TO_TABLE_CONFIG = eINSTANCE.getMReferenceToTableConfig();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MREFERENCE_TO_TABLE_CONFIG__REFERENCE = eINSTANCE.getMReferenceToTableConfig_Reference();

		/**
		 * The meta object literal for the '<em><b>Table Config</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MREFERENCE_TO_TABLE_CONFIG__TABLE_CONFIG = eINSTANCE.getMReferenceToTableConfig_TableConfig();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREFERENCE_TO_TABLE_CONFIG__LABEL = eINSTANCE.getMReferenceToTableConfig_Label();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MClassToTableConfigImpl <em>MClass To Table Config</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MClassToTableConfigImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMClassToTableConfig()
		 * @generated
		 */
		EClass MCLASS_TO_TABLE_CONFIG = eINSTANCE.getMClassToTableConfig();

		/**
		 * The meta object literal for the '<em><b>Class</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_TABLE_CONFIG__CLASS = eINSTANCE.getMClassToTableConfig_Class();

		/**
		 * The meta object literal for the '<em><b>Table Config</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG = eINSTANCE.getMClassToTableConfig_TableConfig();

		/**
		 * The meta object literal for the '<em><b>Intended Package</b></em>' reference feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE = eINSTANCE.getMClassToTableConfig_IntendedPackage();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.impl.MElementWithFontImpl <em>MElement With Font</em>}' class.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.impl.MElementWithFontImpl
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMElementWithFont()
		 * @generated
		 */
		EClass MELEMENT_WITH_FONT = eINSTANCE.getMElementWithFont();

		/**
		 * The meta object literal for the '<em><b>Font Size</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MELEMENT_WITH_FONT__FONT_SIZE = eINSTANCE.getMElementWithFont_FontSize();

		/**
		 * The meta object literal for the '<em><b>Bold Font</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MELEMENT_WITH_FONT__BOLD_FONT = eINSTANCE.getMElementWithFont_BoldFont();

		/**
		 * The meta object literal for the '<em><b>Italic Font</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MELEMENT_WITH_FONT__ITALIC_FONT = eINSTANCE.getMElementWithFont_ItalicFont();

		/**
		 * The meta object literal for the '<em><b>Derived Font Options Encoding</b></em>' attribute feature.
		 * <!-- begin-user-doc --> <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING = eINSTANCE
				.getMElementWithFont_DerivedFontOptionsEncoding();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MCellEditBehaviorOption <em>MCell Edit Behavior Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mtableeditor.MCellEditBehaviorOption
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellEditBehaviorOption()
		 * @generated
		 */
		EEnum MCELL_EDIT_BEHAVIOR_OPTION = eINSTANCE.getMCellEditBehaviorOption();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MCellLocateBehaviorOption <em>MCell Locate Behavior Option</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mtableeditor.MCellLocateBehaviorOption
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMCellLocateBehaviorOption()
		 * @generated
		 */
		EEnum MCELL_LOCATE_BEHAVIOR_OPTION = eINSTANCE.getMCellLocateBehaviorOption();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MFontSizeAdaptor <em>MFont Size Adaptor</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.MFontSizeAdaptor
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMFontSizeAdaptor()
		 * @generated
		 */
		EEnum MFONT_SIZE_ADAPTOR = eINSTANCE.getMFontSizeAdaptor();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MEditorConfigAction <em>MEditor Config Action</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.MEditorConfigAction
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMEditorConfigAction()
		 * @generated
		 */
		EEnum MEDITOR_CONFIG_ACTION = eINSTANCE.getMEditorConfigAction();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MTableConfigAction <em>MTable Config Action</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.MTableConfigAction
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMTableConfigAction()
		 * @generated
		 */
		EEnum MTABLE_CONFIG_ACTION = eINSTANCE.getMTableConfigAction();

		/**
		 * The meta object literal for the '{@link com.montages.mtableeditor.MColumnConfigAction <em>MColumn Config Action</em>}' enum.
		 * <!-- begin-user-doc --> <!--
		 * end-user-doc -->
		 * @see com.montages.mtableeditor.MColumnConfigAction
		 * @see com.montages.mtableeditor.impl.MtableeditorPackageImpl#getMColumnConfigAction()
		 * @generated
		 */
		EEnum MCOLUMN_CONFIG_ACTION = eINSTANCE.getMColumnConfigAction();

	}

} // MtableeditorPackage
