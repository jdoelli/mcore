/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MEditor;
import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MEditor Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig <em>Header Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getMTableConfig <em>MTable Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getMColumnConfig <em>MColumn Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getClassToTableConfig <em>Class To Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getSuperConfig <em>Super Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MEditorConfig#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig()
 * @model annotation="http://www.xocl.org/OCL label='let fileType: String = \'mtableeditor\' in\nlet fileName: String = if (let chain: mcore::MPackage = if headerTableConfig.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedPackage\nendif in\nif chain <> null then true else false \n  endif) \n  =true \nthen if headerTableConfig.intendedPackage.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedPackage.eName\nendif else if (let chain: mcore::MClassifier = if headerTableConfig.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedClass\nendif in\nif chain <> null then true else false \n  endif)=true then let subchain1 : mcore::MPackage = if headerTableConfig.intendedClass.oclIsUndefined()\n  then null\n  else headerTableConfig.intendedClass.containingPackage\nendif in \n if subchain1 = null \n  then null \n else subchain1.eName endif \n\n  else let subchain1 : mtableeditor::MClassToCellConfig = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = if headerTableConfig.oclIsUndefined()\n  then OrderedSet{}\n  else headerTableConfig.columnConfig.classToCellConfig->asOrderedSet()\nendif in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif in \n if subchain1 = null \n  then null \n else if subchain1.class.containingPackage.oclIsUndefined()\n  then null\n  else subchain1.class.containingPackage.eName\nendif endif \n\nendif endif in\nlet label: String = let e1: String = fileName.concat(\'.\').concat(fileType) in \n if e1.oclIsInvalid() then null else e1 endif in\nlabel\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Editor Config\' in\nconst1\n'"
 * @generated
 */

public interface MEditorConfig extends MEditor, MTableEditorElement {

	/**
	 * Returns the value of the '<em><b>Header Table Config</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Table Config</em>' reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Table Config</em>' reference.
	 * @see #isSetHeaderTableConfig()
	 * @see #unsetHeaderTableConfig()
	 * @see #setHeaderTableConfig(MTableConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_HeaderTableConfig()
	 * @model unsettable="true"
	 * @generated
	 */
	MTableConfig getHeaderTableConfig();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig <em>Header Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Header Table Config</em>' reference.
	 * @see #isSetHeaderTableConfig()
	 * @see #unsetHeaderTableConfig()
	 * @see #getHeaderTableConfig()
	 * @generated
	 */
	void setHeaderTableConfig(MTableConfig value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig <em>Header Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetHeaderTableConfig()
	 * @see #getHeaderTableConfig()
	 * @see #setHeaderTableConfig(MTableConfig)
	 * @generated
	 */
	void unsetHeaderTableConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MEditorConfig#getHeaderTableConfig <em>Header Table Config</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Header Table Config</em>' reference is set.
	 * @see #unsetHeaderTableConfig()
	 * @see #getHeaderTableConfig()
	 * @see #setHeaderTableConfig(MTableConfig)
	 * @generated
	 */
	boolean isSetHeaderTableConfig();

	/**
	 * Returns the value of the '<em><b>MTable Config</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MTableConfig}.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MTableConfig#getContainingEditorConfig <em>Containing Editor Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MTable Config</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MTable Config</em>' containment reference list.
	 * @see #isSetMTableConfig()
	 * @see #unsetMTableConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_MTableConfig()
	 * @see com.montages.mtableeditor.MTableConfig#getContainingEditorConfig
	 * @model opposite="containingEditorConfig" containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='MTableConfig'"
	 * @generated
	 */
	EList<MTableConfig> getMTableConfig();

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getMTableConfig <em>MTable Config</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #isSetMTableConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	void unsetMTableConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MEditorConfig#getMTableConfig
	 * <em>MTable Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>MTable Config</em>' containment
	 *         reference list is set.
	 * @see #unsetMTableConfig()
	 * @see #getMTableConfig()
	 * @generated
	 */
	boolean isSetMTableConfig();

	/**
	 * Returns the value of the '<em><b>MColumn Config</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MColumnConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MColumn Config</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MColumn Config</em>' containment reference list.
	 * @see #isSetMColumnConfig()
	 * @see #unsetMColumnConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_MColumnConfig()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MColumnConfig> getMColumnConfig();

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getMColumnConfig <em>MColumn Config</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #isSetMColumnConfig()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	void unsetMColumnConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MEditorConfig#getMColumnConfig
	 * <em>MColumn Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>MColumn Config</em>' containment
	 *         reference list is set.
	 * @see #unsetMColumnConfig()
	 * @see #getMColumnConfig()
	 * @generated
	 */
	boolean isSetMColumnConfig();

	/**
	 * Returns the value of the '<em><b>Class To Table Config</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link com.montages.mtableeditor.MClassToTableConfig}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class To Table Config</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Class To Table Config</em>' containment
	 *         reference list.
	 * @see #isSetClassToTableConfig()
	 * @see #unsetClassToTableConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_ClassToTableConfig()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG propertyCategory='Additional Concepts' createColumn='false'"
	 * @generated
	 */
	EList<MClassToTableConfig> getClassToTableConfig();

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MEditorConfig#getClassToTableConfig
	 * <em>Class To Table Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isSetClassToTableConfig()
	 * @see #getClassToTableConfig()
	 * @generated
	 */
	void unsetClassToTableConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MEditorConfig#getClassToTableConfig
	 * <em>Class To Table Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>Class To Table Config</em>'
	 *         containment reference list is set.
	 * @see #unsetClassToTableConfig()
	 * @see #getClassToTableConfig()
	 * @generated
	 */
	boolean isSetClassToTableConfig();

	/**
	 * Returns the value of the '<em><b>Super Config</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Super Config</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Super Config</em>' reference.
	 * @see #isSetSuperConfig()
	 * @see #unsetSuperConfig()
	 * @see #setSuperConfig(MEditorConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_SuperConfig()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG propertyCategory='Additional Concepts' createColumn='false'"
	 * @generated
	 */
	MEditorConfig getSuperConfig();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getSuperConfig <em>Super Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Super Config</em>' reference.
	 * @see #isSetSuperConfig()
	 * @see #unsetSuperConfig()
	 * @see #getSuperConfig()
	 * @generated
	 */
	void setSuperConfig(MEditorConfig value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MEditorConfig#getSuperConfig <em>Super Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetSuperConfig()
	 * @see #getSuperConfig()
	 * @see #setSuperConfig(MEditorConfig)
	 * @generated
	 */
	void unsetSuperConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MEditorConfig#getSuperConfig <em>Super Config</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Super Config</em>' reference is set.
	 * @see #unsetSuperConfig()
	 * @see #getSuperConfig()
	 * @see #setSuperConfig(MEditorConfig)
	 * @generated
	 */
	boolean isSetSuperConfig();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute. The
	 * literals are from the enumeration
	 * {@link com.montages.mtableeditor.MEditorConfigAction}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MEditorConfigAction
	 * @see #setDoAction(MEditorConfigAction)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfig_DoAction()
	 * @model transient="true" volatile="true" derived="true" annotation=
	 *        "http://www.montages.com/mCore/MCore mName='do Action'"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='mtableeditor::MEditorConfigAction::Do\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MEditorConfigAction getDoAction();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MEditorConfig#getDoAction
	 * <em>Do Action</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MEditorConfigAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MEditorConfigAction value);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MEditorConfigAction trg);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model mClassRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='let applicableClassToTableConfig: OrderedSet(mtableeditor::MClassToTableConfig)  = classToTableConfig->asOrderedSet()->select(it: mtableeditor::MClassToTableConfig | let e0: Boolean = it.class = mClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(mtableeditor::MTableConfig)  = applicableClassToTableConfig.tableConfig->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional Concepts' createColumn='true'"
	 * @generated
	 */
	MTableConfig tableConfigFromClass(MClassifier mClass);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(MEditorConfigAction mEditorConfigAction);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='resetEditor'"
	 *        annotation="http://www.xocl.org/OCL body='true\n'"
	 * @generated
	 */
	Boolean resetEditor();

} // MEditorConfig
