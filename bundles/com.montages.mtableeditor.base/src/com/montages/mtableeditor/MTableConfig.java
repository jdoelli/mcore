/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;

import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MTable Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getColumnConfig <em>Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getReferenceToTableConfig <em>Reference To Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getIntendedPackage <em>Intended Package</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getIntendedClass <em>Intended Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getECName <em>EC Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getECLabel <em>EC Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getECColumnConfig <em>EC Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getContainingEditorConfig <em>Containing Editor Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getComplementActionTable <em>Complement Action Table</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn <em>Display Default Column</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getDisplayHeader <em>Display Header</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getIntendedAction <em>Intended Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getComplementAction <em>Complement Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getSplitChildrenAction <em>Split Children Action</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MTableConfig#getMergeChildrenAction <em>Merge Children Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig()
 * @model annotation="http://www.xocl.org/OCL label='name\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Table\' in\nconst1\n'"
 * @generated
 */

public interface MTableConfig extends MTableEditorElement {

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear, there really
	 * should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_Name()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	String getName();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Column Config</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MColumnConfig}.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MColumnConfig#getContainingTableConfig <em>Containing Table Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column Config</em>' containment reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column Config</em>' containment reference list.
	 * @see #isSetColumnConfig()
	 * @see #unsetColumnConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ColumnConfig()
	 * @see com.montages.mtableeditor.MColumnConfig#getContainingTableConfig
	 * @model opposite="containingTableConfig" containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MColumnConfig> getColumnConfig();

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getColumnConfig <em>Column Config</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #isSetColumnConfig()
	 * @see #getColumnConfig()
	 * @generated
	 */
	void unsetColumnConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MTableConfig#getColumnConfig
	 * <em>Column Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>Column Config</em>' containment
	 *         reference list is set.
	 * @see #unsetColumnConfig()
	 * @see #getColumnConfig()
	 * @generated
	 */
	boolean isSetColumnConfig();

	/**
	 * Returns the value of the '<em><b>Reference To Table Config</b></em>'
	 * containment reference list. The list contents are of type
	 * {@link com.montages.mtableeditor.MReferenceToTableConfig}. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference To Table Config</em>' containment
	 * reference list isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Reference To Table Config</em>' containment
	 *         reference list.
	 * @see #isSetReferenceToTableConfig()
	 * @see #unsetReferenceToTableConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ReferenceToTableConfig()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MReferenceToTableConfig> getReferenceToTableConfig();

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MTableConfig#getReferenceToTableConfig
	 * <em>Reference To Table Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #isSetReferenceToTableConfig()
	 * @see #getReferenceToTableConfig()
	 * @generated
	 */
	void unsetReferenceToTableConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getReferenceToTableConfig <em>Reference To Table Config</em>}' containment reference list is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reference To Table Config</em>' containment reference list is set.
	 * @see #unsetReferenceToTableConfig()
	 * @see #getReferenceToTableConfig()
	 * @generated
	 */
	boolean isSetReferenceToTableConfig();

	/**
	 * Returns the value of the '<em><b>Intended Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intended Package</em>' reference.
	 * @see #isSetIntendedPackage()
	 * @see #unsetIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_IntendedPackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Entry Filtering' createColumn='false'"
	 * @generated
	 */
	MPackage getIntendedPackage();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedPackage <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Intended Package</em>' reference.
	 * @see #isSetIntendedPackage()
	 * @see #unsetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @generated
	 */
	void setIntendedPackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedPackage <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @generated
	 */
	void unsetIntendedPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedPackage <em>Intended Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Intended Package</em>' reference is set.
	 * @see #unsetIntendedPackage()
	 * @see #getIntendedPackage()
	 * @see #setIntendedPackage(MPackage)
	 * @generated
	 */
	boolean isSetIntendedPackage();

	/**
	 * Returns the value of the '<em><b>Intended Class</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Intended Class</em>' reference.
	 * @see #isSetIntendedClass()
	 * @see #unsetIntendedClass()
	 * @see #setIntendedClass(MClassifier)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_IntendedClass()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/OCL choiceConstraint='let rightPackage: Boolean = if (let chain: mcore::MPackage = intendedPackage in\nif chain <> null then true else false \n  endif) \n  =true \nthen (let e0: Boolean = trg.containingPackage = intendedPackage in \n if e0.oclIsInvalid() then null else e0 endif)\n  else true\nendif in\nlet isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nlet intended: Boolean = let e1: Boolean = if (isClass)= false \n then false \n else if (rightPackage)= false \n then false \nelse if ((isClass)= null or (rightPackage)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nintended\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG propertyCategory='Entry Filtering' createColumn='false'"
	 * @generated
	 */
	MClassifier getIntendedClass();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedClass <em>Intended Class</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Intended Class</em>' reference.
	 * @see #isSetIntendedClass()
	 * @see #unsetIntendedClass()
	 * @see #getIntendedClass()
	 * @generated
	 */
	void setIntendedClass(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedClass <em>Intended Class</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetIntendedClass()
	 * @see #getIntendedClass()
	 * @see #setIntendedClass(MClassifier)
	 * @generated
	 */
	void unsetIntendedClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getIntendedClass <em>Intended Class</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Intended Class</em>' reference is set.
	 * @see #unsetIntendedClass()
	 * @see #getIntendedClass()
	 * @see #setIntendedClass(MClassifier)
	 * @generated
	 */
	boolean isSetIntendedClass();

	/**
	 * Returns the value of the '<em><b>EC Name</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Name</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>EC Name</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ECName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='let resultofecName: String = if (let e0: Boolean = let chain01: String = name in\nif chain01.trim().oclIsUndefined() \n then null \n else chain01.trim()\n  endif = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen if intendedClass.oclIsUndefined()\n  then null\n  else intendedClass.name\nendif\n  else name\nendif in\nresultofecName\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special Editor Config Settings'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getECName();

	/**
	 * Returns the value of the '<em><b>EC Label</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Label</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>EC Label</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ECLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='label\n'" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special Editor Config Settings'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getECLabel();

	/**
	 * Returns the value of the '<em><b>EC Column Config</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MColumnConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EC Column Config</em>' reference list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EC Column Config</em>' reference list.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ECColumnConfig()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='columnConfig->asOrderedSet()\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special Editor Config Settings'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MColumnConfig> getECColumnConfig();

	/**
	 * Returns the value of the '<em><b>Containing Editor Config</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MEditorConfig#getMTableConfig <em>MTable Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Editor Config</em>' container
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Editor Config</em>' container reference.
	 * @see #setContainingEditorConfig(MEditorConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ContainingEditorConfig()
	 * @see com.montages.mtableeditor.MEditorConfig#getMTableConfig
	 * @model opposite="mTableConfig" unsettable="true" transient="false"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structure' createColumn='false'"
	 * @generated
	 */
	MEditorConfig getContainingEditorConfig();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MTableConfig#getContainingEditorConfig
	 * <em>Containing Editor Config</em>}' container reference. <!--
	 * begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Containing Editor Config</em>'
	 *            container reference.
	 * @see #getContainingEditorConfig()
	 * @generated
	 */

	void setContainingEditorConfig(MEditorConfig value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_Label()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Rendering'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getLabel <em>Label</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' attribute is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	boolean isSetLabel();

	/**
	 * Returns the value of the '<em><b>Display Default Column</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Default Column</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Default Column</em>' attribute.
	 * @see #isSetDisplayDefaultColumn()
	 * @see #unsetDisplayDefaultColumn()
	 * @see #setDisplayDefaultColumn(Boolean)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_DisplayDefaultColumn()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Rendering' createColumn='false'"
	 * @generated
	 */
	Boolean getDisplayDefaultColumn();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Display Default Column</em>' attribute.
	 * @see #isSetDisplayDefaultColumn()
	 * @see #unsetDisplayDefaultColumn()
	 * @see #getDisplayDefaultColumn()
	 * @generated
	 */
	void setDisplayDefaultColumn(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetDisplayDefaultColumn()
	 * @see #getDisplayDefaultColumn()
	 * @see #setDisplayDefaultColumn(Boolean)
	 * @generated
	 */
	void unsetDisplayDefaultColumn();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayDefaultColumn <em>Display Default Column</em>}' attribute is set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Display Default Column</em>' attribute is set.
	 * @see #unsetDisplayDefaultColumn()
	 * @see #getDisplayDefaultColumn()
	 * @see #setDisplayDefaultColumn(Boolean)
	 * @generated
	 */
	boolean isSetDisplayDefaultColumn();

	/**
	 * Returns the value of the '<em><b>Display Header</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Header</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Display Header</em>' attribute.
	 * @see #isSetDisplayHeader()
	 * @see #unsetDisplayHeader()
	 * @see #setDisplayHeader(Boolean)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_DisplayHeader()
	 * @model unsettable="true" annotation=
	 *        "http://www.xocl.org/EDITORCONFIG propertyCategory='Rendering' createColumn='false'"
	 * @generated
	 */
	Boolean getDisplayHeader();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayHeader <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Display Header</em>' attribute.
	 * @see #isSetDisplayHeader()
	 * @see #unsetDisplayHeader()
	 * @see #getDisplayHeader()
	 * @generated
	 */
	void setDisplayHeader(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayHeader <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetDisplayHeader()
	 * @see #getDisplayHeader()
	 * @see #setDisplayHeader(Boolean)
	 * @generated
	 */
	void unsetDisplayHeader();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MTableConfig#getDisplayHeader <em>Display Header</em>}' attribute is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Display Header</em>' attribute is set.
	 * @see #unsetDisplayHeader()
	 * @see #getDisplayHeader()
	 * @see #setDisplayHeader(Boolean)
	 * @generated
	 */
	boolean isSetDisplayHeader();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mtableeditor.MTableConfigAction}.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MTableConfigAction
	 * @see #setDoAction(MTableConfigAction)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='do Action'"
	 *        annotation="http://www.xocl.org/OCL derive='mtableeditor::MTableConfigAction::Do\n' choiceConstruction='let var0: OrderedSet(MTableConfigAction) = intendedAction in var0\r\n->append(MTableConfigAction::AddEmptyColumn)\r\n->append(MTableConfigAction::AddColumnWithRow)\r\n->append(MTableConfigAction::AddColumnWithOcl)\r\n->append(MTableConfigAction::AddTableRef)\r\n->prepend(MTableConfigAction::Do)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MTableConfigAction getDoAction();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MTableConfig#getDoAction
	 * <em>Do Action</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @param value
	 *            the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mtableeditor.MTableConfigAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MTableConfigAction value);

	/**
	 * Returns the value of the '<em><b>Intended Action</b></em>' attribute list.
	 * The list contents are of type {@link com.montages.mtableeditor.MTableConfigAction}.
	 * The literals are from the enumeration {@link com.montages.mtableeditor.MTableConfigAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Action</em>' attribute list isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intended Action</em>' attribute list.
	 * @see com.montages.mtableeditor.MTableConfigAction
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_IntendedAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='intended Action'"
	 *        annotation="http://www.xocl.org/OCL derive='if self.intendedClass.oclIsUndefined()\r\n   then if self.intendedPackage.oclIsUndefined()\r\n   \t\t\t\tthen OrderedSet{}\r\n   \t\t\t\telse OrderedSet{MTableConfigAction::UnsetIntendedPackage}\r\n   \t\t\t\tendif\r\n   else\r\n   let e1: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   let e0: OrderedSet(mtableeditor::MTableConfigAction) = if self.intendedPackage.oclIsUndefined()\r\n   \t\t\t\tthen OrderedSet{MTableConfigAction::UnsetIntendedClass}\r\n   \t\t\t\telse OrderedSet{MTableConfigAction::UnsetIntendedClass, MTableConfigAction::UnsetIntendedPackage}\r\n   \t\t\t\tendif   \t\t\t\t\r\n   \t\t\t\tin if self.columnConfig->isEmpty() and self.referenceToTableConfig->isEmpty() and true\r\n   \t\t\t\t\tthen e0\r\n   \t\t\t\t\telse\r\n   \t\t\t\t\t\tlet e6: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   \t\t\t\t\t\tlet e5: OrderedSet(mtableeditor::MTableConfigAction) = \r\n   \t\t\t\t\t\tlet e4: OrderedSet(mtableeditor::MTableConfigAction) = \r\n  \t\t\t\t\t\tlet e3: OrderedSet(mtableeditor::MTableConfigAction) = \r\n  \t\t\t\t\t\tlet e2: OrderedSet(mtableeditor::MTableConfigAction) = e0\r\n  \t\t\t\t\t\tin if self.intendedClass.allDirectSubTypes()->isEmpty()\r\n   \t\t\t\t\t\t\tthen e2\r\n   \t\t\t\t\t\t\telse e2->prepend(MTableConfigAction::SplitSpecializations)\r\n   \t\t\t\t\t\t\tendif\r\n  \t\t\t\t\t\tin if self.intendedClass.allSuperTypes()->isEmpty()\r\n   \t\t\t\t\t\t\tthen e3\r\n   \t\t\t\t\t\t\telse e3->prepend(MTableConfigAction::MergeAbstraction)\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.splitChildrenAction = true\r\n   \t\t\t\t\t\t\tthen e4->prepend(MTableConfigAction::SplitChildren)\r\n   \t\t\t\t\t\t\telse e4\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.mergeChildrenAction = true\r\n   \t\t\t\t\t\t\tthen e5->prepend(MTableConfigAction::MergeChildren)\r\n   \t\t\t\t\t\t\telse e5\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\t\tin if self.intendedClass.classescontainedin->isEmpty()\r\n   \t\t\t\t\t\t\tthen e6\r\n   \t\t\t\t\t\t\telse e6->prepend(MTableConfigAction::MergeContainer)\r\n   \t\t\t\t\t\t\tendif\r\n   \t\t\t\t\tendif   \t\t\t\t\t\r\n   \t\t\t\t\tin if self.complementAction = false\r\n   \t\t\t\t\tthen e1\r\n   \t\t\t\t\telse e1->prepend(MTableConfigAction::ComplementTable)\r\n   \t\t\t\t\tendif\r\n   endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTableConfigAction> getIntendedAction();

	/**
	 * Returns the value of the '<em><b>Complement Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complement Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc --> <!-- begin-model-doc --> False means that the
	 * TableConfig cannot be complemented. <!-- end-model-doc -->
	 * 
	 * @return the value of the '<em>Complement Action</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ComplementAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='let includesAllContainments: Boolean = if (let e0: Boolean = if (let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif)   in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= false \n then false \nelse if ((let e0: Boolean = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet()->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif)   in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen false\n  else true\nendif in\nlet hasContainments: Boolean = if (let e0: Boolean = if (true)= false \n then false \n else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= false \n then false \nelse if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen includesAllContainments\n  else false\nendif in\nlet allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif) in\nlet includesAllProperties: Boolean = if (let e0: Boolean = if (let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \n else if (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= false \n then false \nelse if ((let e0: Boolean = allRowFeatureCell.feature->asOrderedSet()->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->includesAll(if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif->select(notHidden: mcore::MProperty | let e0: Boolean = if (let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= true \n then true \n else if ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= true \n then true \n else if ( notHidden.directSemantics.oclIsUndefined())= true \n then true \nelse if ((let chain01: Boolean = if notHidden.directSemantics.layout.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout.hideInTable\nendif in\nif chain01 = false then true else false \n  endif)= null or ( if notHidden.directSemantics.oclIsUndefined()\n  then null\n  else notHidden.directSemantics.layout\nendif.oclIsUndefined())= null or ( notHidden.directSemantics.oclIsUndefined())= null) = true \n then null \n else false endif endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() )   in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen hasContainments\n  else true\nendif in\nlet hasProperties: Boolean = if (let e0: Boolean = if (true)= false \n then false \n else if (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= false \n then false \nelse if ((true)= null or (let chain02: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allNonContainmentFeatures()\nendif in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen includesAllProperties\n  else hasContainments\nendif in\nlet resultofComplementAction: Boolean = if (let e0: Boolean = if ( intendedClass.oclIsUndefined())= true \n then true \n else if ( intendedClass.oclIsInvalid())= true \n then true \nelse if (( intendedClass.oclIsUndefined())= null or ( intendedClass.oclIsInvalid())= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen false\n  else hasProperties\nendif in\nresultofComplementAction\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getComplementAction();

	/**
	 * Returns the value of the '<em><b>Split Children Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Split Children Action</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Split Children Action</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_SplitChildrenAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let propertiesChildClass: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = intendedClass <> it.containingClassifier in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet allOclCell: OrderedSet(mtableeditor::MOclCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MOclCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MOclCell) then r->including(i.oclAsType(mtableeditor::MOclCell))->asOrderedSet() \n else r endif)->select(it: mtableeditor::MOclCell | let e0: Boolean = it.class <> intendedClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet containsChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen false else if (let e0: Boolean = if (let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= true \n then true \n else if (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= true \n then true \nelse if ((let chain01: OrderedSet(mcore::MProperty)  = propertiesChildClass in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif)= null or (let chain02: OrderedSet(mtableeditor::MOclCell)  = allOclCell in\nif chain02->notEmpty().oclIsUndefined() \n then null \n else chain02->notEmpty()\n  endif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif in\ncontainsChildren\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getSplitChildrenAction();

	/**
	 * Returns the value of the '<em><b>Merge Children Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Merge Children Action</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Merge Children Action</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_MergeChildrenAction()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let containments: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nlet allChildClasses: OrderedSet(mcore::MClassifier)  = containments.type->asOrderedSet() in\nlet propertiesofChildren: OrderedSet(mcore::MProperty)  = getAllProperties()->asOrderedSet()->select(it: mcore::MProperty | let e0: Boolean = it.containingClassifier <> intendedClass in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofMergeChildren: Boolean = if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen false else if (let chain: OrderedSet(mcore::MProperty)  = if intendedClass.oclIsUndefined()\n  then OrderedSet{}\n  else intendedClass.allContainmentReferences()\nendif in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif)=true then true\n  else let e0: Boolean = not(let e0: Boolean = propertiesofChildren->includesAll(allChildClasses.allNonContainmentFeatures()->asOrderedSet())   in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif\nendif endif in\nresultofMergeChildren\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getMergeChildrenAction();

	/**
	 * Returns the value of the '<em><b>Complement Action Table</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mtableeditor.MTableConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Complement Action Table</em>' reference list
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Complement Action Table</em>' reference list.
	 * @see #isSetComplementActionTable()
	 * @see #unsetComplementActionTable()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfig_ComplementActionTable()
	 * @model unsettable="true" transient="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structure'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MTableConfig> getComplementActionTable();

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MTableConfig#getComplementActionTable <em>Complement Action Table</em>}' reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #isSetComplementActionTable()
	 * @see #getComplementActionTable()
	 * @generated
	 */
	void unsetComplementActionTable();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MTableConfig#getComplementActionTable
	 * <em>Complement Action Table</em>}' reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>Complement Action Table</em>'
	 *         reference list is set.
	 * @see #unsetComplementActionTable()
	 * @see #getComplementActionTable()
	 * @generated
	 */
	boolean isSetComplementActionTable();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MTableConfigAction trg);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model referenceRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	MTableConfig tableConfigFromReference(MProperty reference);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='get All Properties'"
	 *        annotation="http://www.xocl.org/OCL body='let allRef: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = referenceToTableConfig.reference->reject(oclIsUndefined())->asOrderedSet() in\nchain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() \n else r endif) in\nlet allRowFeatureCell: OrderedSet(mtableeditor::MRowFeatureCell)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = columnConfig.classToCellConfig->asOrderedSet() in\nchain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() \n else r endif) in\nlet var2: OrderedSet(mcore::MProperty)  = let e1: OrderedSet(mcore::MProperty)  = allRowFeatureCell.feature->asOrderedSet()->union(allRef) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif in\nvar2\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structure' createColumn='true'"
	 * @generated
	 */
	EList<MProperty> getAllProperties();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(MTableConfigAction mTableConfigAction);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model complementTableMany="true"
	 *        complementTableAnnotation="http://www.montages.com/mCore/MCore mName='complement Table'" resetAnnotation="http://www.montages.com/mCore/MCore mName='reset'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(XUpdate xUpdate, MEditorConfig mEditorConfig, MTableConfigAction mTableConfigAction,
			EList<MTableConfig> complementTable, Boolean reset);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @model complementTableMany="true" complementTableAnnotation=
	 *        "http://www.montages.com/mCore/MCore mName='complement Table'"
	 *        resetAnnotation=
	 *        "http://www.montages.com/mCore/MCore mName='reset'" annotation=
	 *        "http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated NOT
	 */
	XUpdate doActionUpdate(XUpdate xUpdate, MEditorConfig mEditorConfig, MTableConfigAction mTableConfigAction,
			List<MTableConfig> complementTable, Boolean reset);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(XUpdate xUpdate, MTableConfig mTableConfig, MTableConfigAction mTableConfigAction);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invoke Set Do Action'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Object invokeSetDoAction(MTableConfigAction mTableConfigAction);

} // MTableConfig
