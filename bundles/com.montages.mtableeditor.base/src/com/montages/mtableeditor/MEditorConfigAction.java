/**
 */
package com.montages.mtableeditor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>MEditor Config Action</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage#getMEditorConfigAction()
 * @model
 * @generated
 */
public enum MEditorConfigAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Add Table</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_TABLE_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_TABLE(1, "AddTable", "+ Table"),
	/**
	 * The '<em><b>Reset Editor</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #RESET_EDITOR_VALUE
	 * @generated
	 * @ordered
	 */
	RESET_EDITOR(2, "ResetEditor", "Reset Editor");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Add Table</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Table</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_TABLE
	 * @model name="AddTable" literal="+ Table"
	 *        annotation="http://www.montages.com/mCore/MCore mName='AddTable'"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_TABLE_VALUE = 1;

	/**
	 * The '<em><b>Reset Editor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Reset Editor</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESET_EDITOR
	 * @model name="ResetEditor" literal="Reset Editor"
	 * @generated
	 * @ordered
	 */
	public static final int RESET_EDITOR_VALUE = 2;

	/**
	 * An array of all the '<em><b>MEditor Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final MEditorConfigAction[] VALUES_ARRAY = new MEditorConfigAction[] { DO, ADD_TABLE,
			RESET_EDITOR, };

	/**
	 * A public read-only list of all the '<em><b>MEditor Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MEditorConfigAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MEditor Config Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MEditorConfigAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MEditorConfigAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MEditor Config Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MEditorConfigAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MEditorConfigAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MEditor Config Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MEditorConfigAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case ADD_TABLE_VALUE:
			return ADD_TABLE;
		case RESET_EDITOR_VALUE:
			return RESET_EDITOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private MEditorConfigAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // MEditorConfigAction
