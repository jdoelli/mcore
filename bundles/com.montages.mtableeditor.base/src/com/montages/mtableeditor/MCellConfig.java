/**
 */
package com.montages.mtableeditor;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MCell Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig <em>Containing Class To Cell Config</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMCellConfig()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Cell\' in\nconst1\n'"
 * @generated
 */

public interface MCellConfig extends MTableEditorElement, MClassToCellConfig, MElementWithFont {

	/**
	 * Returns the value of the '<em><b>Explicit Highlight OCL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Explicit Highlight OCL</em>' attribute isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Explicit Highlight OCL</em>' attribute.
	 * @see #isSetExplicitHighlightOCL()
	 * @see #unsetExplicitHighlightOCL()
	 * @see #setExplicitHighlightOCL(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMCellConfig_ExplicitHighlightOCL()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EXPRESSION_OCL EXPRESSION='OCL'"
	 * @generated
	 */
	String getExplicitHighlightOCL();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>Explicit Highlight OCL</em>' attribute.
	 * @see #isSetExplicitHighlightOCL()
	 * @see #unsetExplicitHighlightOCL()
	 * @see #getExplicitHighlightOCL()
	 * @generated
	 */
	void setExplicitHighlightOCL(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetExplicitHighlightOCL()
	 * @see #getExplicitHighlightOCL()
	 * @see #setExplicitHighlightOCL(String)
	 * @generated
	 */
	void unsetExplicitHighlightOCL();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MCellConfig#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}' attribute is set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Explicit Highlight OCL</em>' attribute is set.
	 * @see #unsetExplicitHighlightOCL()
	 * @see #getExplicitHighlightOCL()
	 * @see #setExplicitHighlightOCL(String)
	 * @generated
	 */
	boolean isSetExplicitHighlightOCL();

	/**
	 * Returns the value of the '<em><b>Containing Class To Cell Config</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Class To Cell Config</em>'
	 * container reference isn't clear, there really should be more of a
	 * description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Class To Cell Config</em>' container reference.
	 * @see #setContainingClassToCellConfig(MClassToCellConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMCellConfig_ContainingClassToCellConfig()
	 * @see com.montages.mtableeditor.MClassToCellConfig#getCellConfig
	 * @model opposite="cellConfig" unsettable="true" transient="false"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structure' createColumn='false'"
	 * @generated
	 */
	MClassToCellConfig getContainingClassToCellConfig();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig
	 * <em>Containing Class To Cell Config</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Containing Class To Cell Config</em>
	 *            ' container reference.
	 * @see #getContainingClassToCellConfig()
	 * @generated
	 */
	void setContainingClassToCellConfig(MClassToCellConfig value);

} // MCellConfig
