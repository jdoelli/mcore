/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MEditor;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MTable Editor</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 * <li>{@link com.montages.mtableeditor.MTableEditor#getMEditorConfig
 * <em>MEditor Config</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableEditor()
 * @model
 * @generated
 */

public interface MTableEditor extends MEditor {

	/**
	 * Returns the value of the '<em><b>MEditor Config</b></em>' containment
	 * reference list. The list contents are of type
	 * {@link com.montages.mtableeditor.MEditorConfig}. <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MEditor Config</em>' containment reference
	 * list isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>MEditor Config</em>' containment reference
	 *         list.
	 * @see #isSetMEditorConfig()
	 * @see #unsetMEditorConfig()
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableEditor_MEditorConfig()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MEditorConfig> getMEditorConfig();

	/**
	 * Unsets the value of the '
	 * {@link com.montages.mtableeditor.MTableEditor#getMEditorConfig
	 * <em>MEditor Config</em>}' containment reference list. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #isSetMEditorConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	void unsetMEditorConfig();

	/**
	 * Returns whether the value of the '
	 * {@link com.montages.mtableeditor.MTableEditor#getMEditorConfig
	 * <em>MEditor Config</em>}' containment reference list is set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return whether the value of the '<em>MEditor Config</em>' containment
	 *         reference list is set.
	 * @see #unsetMEditorConfig()
	 * @see #getMEditorConfig()
	 * @generated
	 */
	boolean isSetMEditorConfig();

} // MTableEditor
