/**
 */
package com.montages.mtableeditor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>MTable Config Action</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage#getMTableConfigAction()
 * @model
 * @generated
 */
public enum MTableConfigAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Unset Intended Package</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #UNSET_INTENDED_PACKAGE_VALUE
	 * @generated
	 * @ordered
	 */
	UNSET_INTENDED_PACKAGE(1, "UnsetIntendedPackage", "-> Unset Intended Package"),

	/**
	 * The '<em><b>Unset Intended Class</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #UNSET_INTENDED_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	UNSET_INTENDED_CLASS(2, "UnsetIntendedClass", "-> Unset Intended Class"),
	/**
	 * The '<em><b>Add Empty Column</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ADD_EMPTY_COLUMN_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_EMPTY_COLUMN(3, "AddEmptyColumn", "+ Empty Column"),
	/**
	 * The '<em><b>Add Column With Row</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ADD_COLUMN_WITH_ROW_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_COLUMN_WITH_ROW(4, "AddColumnWithRow", "+ Column with Property Cell"),
	/**
	 * The '<em><b>Add Column With Ocl</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ADD_COLUMN_WITH_OCL_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_COLUMN_WITH_OCL(5, "AddColumnWithOcl", "+ Column with \'Text\'/Ocl Cell"),
	/**
	 * The '<em><b>Add Table Ref</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #ADD_TABLE_REF_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_TABLE_REF(6, "AddTableRef", "+ Attach Subtable"),
	/**
	 * The '<em><b>Complement Table</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #COMPLEMENT_TABLE_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLEMENT_TABLE(7, "ComplementTable", "Complement Table"),
	/**
	 * The '<em><b>Merge Abstraction</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #MERGE_ABSTRACTION_VALUE
	 * @generated
	 * @ordered
	 */
	MERGE_ABSTRACTION(8, "MergeAbstraction", "Merge in Abstraction"),
	/**
	 * The '<em><b>Split Specializations</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #SPLIT_SPECIALIZATIONS_VALUE
	 * @generated
	 * @ordered
	 */
	SPLIT_SPECIALIZATIONS(9, "SplitSpecializations", "Split in Specializations"),
	/**
	 * The '<em><b>Merge Container</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #MERGE_CONTAINER_VALUE
	 * @generated
	 * @ordered
	 */
	MERGE_CONTAINER(10, "MergeContainer", "Merge in Container"),
	/**
	 * The '<em><b>Split Children</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #SPLIT_CHILDREN_VALUE
	 * @generated
	 * @ordered
	 */
	SPLIT_CHILDREN(11, "SplitChildren", "Split in Children"),
	/**
	 * The '<em><b>Merge Children</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #MERGE_CHILDREN_VALUE
	 * @generated
	 * @ordered
	 */
	MERGE_CHILDREN(12, "MergeChildren", "Merge Children");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Unset Intended Package</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unset Intended Package</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #UNSET_INTENDED_PACKAGE
	 * @model name="UnsetIntendedPackage" literal="-> Unset Intended Package"
	 * @generated
	 * @ordered
	 */
	public static final int UNSET_INTENDED_PACKAGE_VALUE = 1;

	/**
	 * The '<em><b>Unset Intended Class</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unset Intended Class</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #UNSET_INTENDED_CLASS
	 * @model name="UnsetIntendedClass" literal="-> Unset Intended Class"
	 * @generated
	 * @ordered
	 */
	public static final int UNSET_INTENDED_CLASS_VALUE = 2;

	/**
	 * The '<em><b>Add Empty Column</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Add Empty Column</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_EMPTY_COLUMN
	 * @model name="AddEmptyColumn" literal="+ Empty Column"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_EMPTY_COLUMN_VALUE = 3;

	/**
	 * The '<em><b>Add Column With Row</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Column With Row</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ADD_COLUMN_WITH_ROW
	 * @model name="AddColumnWithRow" literal="+ Column with Property Cell"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_COLUMN_WITH_ROW_VALUE = 4;

	/**
	 * The '<em><b>Add Column With Ocl</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Column With Ocl</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ADD_COLUMN_WITH_OCL
	 * @model name="AddColumnWithOcl" literal="+ Column with \'Text\'/Ocl Cell"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_COLUMN_WITH_OCL_VALUE = 5;

	/**
	 * The '<em><b>Add Table Ref</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Add Table Ref</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_TABLE_REF
	 * @model name="AddTableRef" literal="+ Attach Subtable"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_TABLE_REF_VALUE = 6;

	/**
	 * The '<em><b>Complement Table</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Complement Table</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLEMENT_TABLE
	 * @model name="ComplementTable" literal="Complement Table"
	 * @generated
	 * @ordered
	 */
	public static final int COMPLEMENT_TABLE_VALUE = 7;

	/**
	 * The '<em><b>Merge Abstraction</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Merge Abstraction</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #MERGE_ABSTRACTION
	 * @model name="MergeAbstraction" literal="Merge in Abstraction"
	 * @generated
	 * @ordered
	 */
	public static final int MERGE_ABSTRACTION_VALUE = 8;

	/**
	 * The '<em><b>Split Specializations</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Split Specializations</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #SPLIT_SPECIALIZATIONS
	 * @model name="SplitSpecializations" literal="Split in Specializations"
	 * @generated
	 * @ordered
	 */
	public static final int SPLIT_SPECIALIZATIONS_VALUE = 9;

	/**
	 * The '<em><b>Merge Container</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Merge Container</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MERGE_CONTAINER
	 * @model name="MergeContainer" literal="Merge in Container"
	 * @generated
	 * @ordered
	 */
	public static final int MERGE_CONTAINER_VALUE = 10;

	/**
	 * The '<em><b>Split Children</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Split Children</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPLIT_CHILDREN
	 * @model name="SplitChildren" literal="Split in Children"
	 * @generated
	 * @ordered
	 */
	public static final int SPLIT_CHILDREN_VALUE = 11;

	/**
	 * The '<em><b>Merge Children</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Merge Children</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MERGE_CHILDREN
	 * @model name="MergeChildren" literal="Merge Children"
	 * @generated
	 * @ordered
	 */
	public static final int MERGE_CHILDREN_VALUE = 12;

	/**
	 * An array of all the '<em><b>MTable Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final MTableConfigAction[] VALUES_ARRAY = new MTableConfigAction[] { DO, UNSET_INTENDED_PACKAGE,
			UNSET_INTENDED_CLASS, ADD_EMPTY_COLUMN, ADD_COLUMN_WITH_ROW, ADD_COLUMN_WITH_OCL, ADD_TABLE_REF,
			COMPLEMENT_TABLE, MERGE_ABSTRACTION, SPLIT_SPECIALIZATIONS, MERGE_CONTAINER, SPLIT_CHILDREN,
			MERGE_CHILDREN, };

	/**
	 * A public read-only list of all the '<em><b>MTable Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MTableConfigAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MTable Config Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MTableConfigAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MTableConfigAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MTable Config Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MTableConfigAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MTableConfigAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MTable Config Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MTableConfigAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case UNSET_INTENDED_PACKAGE_VALUE:
			return UNSET_INTENDED_PACKAGE;
		case UNSET_INTENDED_CLASS_VALUE:
			return UNSET_INTENDED_CLASS;
		case ADD_EMPTY_COLUMN_VALUE:
			return ADD_EMPTY_COLUMN;
		case ADD_COLUMN_WITH_ROW_VALUE:
			return ADD_COLUMN_WITH_ROW;
		case ADD_COLUMN_WITH_OCL_VALUE:
			return ADD_COLUMN_WITH_OCL;
		case ADD_TABLE_REF_VALUE:
			return ADD_TABLE_REF;
		case COMPLEMENT_TABLE_VALUE:
			return COMPLEMENT_TABLE;
		case MERGE_ABSTRACTION_VALUE:
			return MERGE_ABSTRACTION;
		case SPLIT_SPECIALIZATIONS_VALUE:
			return SPLIT_SPECIALIZATIONS;
		case MERGE_CONTAINER_VALUE:
			return MERGE_CONTAINER;
		case SPLIT_CHILDREN_VALUE:
			return SPLIT_CHILDREN;
		case MERGE_CHILDREN_VALUE:
			return MERGE_CHILDREN;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private MTableConfigAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // MTableConfigAction
