/**
 */
package com.montages.mtableeditor;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MOcl Cell</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MOclCell#getStringRendering <em>String Rendering</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMOclCell()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL cellKindDerive='\'OclCell\'\n\n'"
 * @generated
 */

public interface MOclCell extends MCellConfig {

	/**
	 * Returns the value of the '<em><b>String Rendering</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String Rendering</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String Rendering</em>' attribute.
	 * @see #isSetStringRendering()
	 * @see #unsetStringRendering()
	 * @see #setStringRendering(String)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMOclCell_StringRendering()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	String getStringRendering();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MOclCell#getStringRendering <em>String Rendering</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @param value the new value of the '<em>String Rendering</em>' attribute.
	 * @see #isSetStringRendering()
	 * @see #unsetStringRendering()
	 * @see #getStringRendering()
	 * @generated
	 */
	void setStringRendering(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MOclCell#getStringRendering <em>String Rendering</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isSetStringRendering()
	 * @see #getStringRendering()
	 * @see #setStringRendering(String)
	 * @generated
	 */
	void unsetStringRendering();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MOclCell#getStringRendering <em>String Rendering</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String Rendering</em>' attribute is set.
	 * @see #unsetStringRendering()
	 * @see #getStringRendering()
	 * @see #setStringRendering(String)
	 * @generated
	 */
	boolean isSetStringRendering();

} // MOclCell
