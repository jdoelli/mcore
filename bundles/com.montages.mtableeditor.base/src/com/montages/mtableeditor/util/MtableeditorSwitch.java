/**
 */
package com.montages.mtableeditor.util;

import com.montages.mcore.MEditor;
import com.montages.mtableeditor.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;
import org.langlets.autil.AElement;

/**
 * <!-- begin-user-doc --> The <b>Switch</b> for the model's inheritance
 * hierarchy. It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object and proceeding up the
 * inheritance hierarchy until a non-null result is returned, which is the
 * result of the switch. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage
 * @generated
 */
public class MtableeditorSwitch<T> extends Switch<T> {

	/**
	 * The cached model package
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static MtableeditorPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public MtableeditorSwitch() {
		if (modelPackage == null) {
			modelPackage = MtableeditorPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case MtableeditorPackage.MTABLE_EDITOR_ELEMENT: {
			MTableEditorElement mTableEditorElement = (MTableEditorElement) theEObject;
			T result = caseMTableEditorElement(mTableEditorElement);
			if (result == null)
				result = caseAElement(mTableEditorElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MEDITOR_CONFIG: {
			MEditorConfig mEditorConfig = (MEditorConfig) theEObject;
			T result = caseMEditorConfig(mEditorConfig);
			if (result == null)
				result = caseMEditor(mEditorConfig);
			if (result == null)
				result = caseMTableEditorElement(mEditorConfig);
			if (result == null)
				result = caseAElement(mEditorConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MTABLE_CONFIG: {
			MTableConfig mTableConfig = (MTableConfig) theEObject;
			T result = caseMTableConfig(mTableConfig);
			if (result == null)
				result = caseMTableEditorElement(mTableConfig);
			if (result == null)
				result = caseAElement(mTableConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MCOLUMN_CONFIG: {
			MColumnConfig mColumnConfig = (MColumnConfig) theEObject;
			T result = caseMColumnConfig(mColumnConfig);
			if (result == null)
				result = caseMTableEditorElement(mColumnConfig);
			if (result == null)
				result = caseMElementWithFont(mColumnConfig);
			if (result == null)
				result = caseAElement(mColumnConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MCLASS_TO_CELL_CONFIG: {
			MClassToCellConfig mClassToCellConfig = (MClassToCellConfig) theEObject;
			T result = caseMClassToCellConfig(mClassToCellConfig);
			if (result == null)
				result = caseMTableEditorElement(mClassToCellConfig);
			if (result == null)
				result = caseAElement(mClassToCellConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MCELL_CONFIG: {
			MCellConfig mCellConfig = (MCellConfig) theEObject;
			T result = caseMCellConfig(mCellConfig);
			if (result == null)
				result = caseMClassToCellConfig(mCellConfig);
			if (result == null)
				result = caseMElementWithFont(mCellConfig);
			if (result == null)
				result = caseMTableEditorElement(mCellConfig);
			if (result == null)
				result = caseAElement(mCellConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MROW_FEATURE_CELL: {
			MRowFeatureCell mRowFeatureCell = (MRowFeatureCell) theEObject;
			T result = caseMRowFeatureCell(mRowFeatureCell);
			if (result == null)
				result = caseMCellConfig(mRowFeatureCell);
			if (result == null)
				result = caseMClassToCellConfig(mRowFeatureCell);
			if (result == null)
				result = caseMElementWithFont(mRowFeatureCell);
			if (result == null)
				result = caseMTableEditorElement(mRowFeatureCell);
			if (result == null)
				result = caseAElement(mRowFeatureCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MOCL_CELL: {
			MOclCell mOclCell = (MOclCell) theEObject;
			T result = caseMOclCell(mOclCell);
			if (result == null)
				result = caseMCellConfig(mOclCell);
			if (result == null)
				result = caseMClassToCellConfig(mOclCell);
			if (result == null)
				result = caseMElementWithFont(mOclCell);
			if (result == null)
				result = caseMTableEditorElement(mOclCell);
			if (result == null)
				result = caseAElement(mOclCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MEDIT_PROVIDER_CELL: {
			MEditProviderCell mEditProviderCell = (MEditProviderCell) theEObject;
			T result = caseMEditProviderCell(mEditProviderCell);
			if (result == null)
				result = caseMCellConfig(mEditProviderCell);
			if (result == null)
				result = caseMClassToCellConfig(mEditProviderCell);
			if (result == null)
				result = caseMElementWithFont(mEditProviderCell);
			if (result == null)
				result = caseMTableEditorElement(mEditProviderCell);
			if (result == null)
				result = caseAElement(mEditProviderCell);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG: {
			MReferenceToTableConfig mReferenceToTableConfig = (MReferenceToTableConfig) theEObject;
			T result = caseMReferenceToTableConfig(mReferenceToTableConfig);
			if (result == null)
				result = caseMTableEditorElement(mReferenceToTableConfig);
			if (result == null)
				result = caseAElement(mReferenceToTableConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG: {
			MClassToTableConfig mClassToTableConfig = (MClassToTableConfig) theEObject;
			T result = caseMClassToTableConfig(mClassToTableConfig);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case MtableeditorPackage.MELEMENT_WITH_FONT: {
			MElementWithFont mElementWithFont = (MElementWithFont) theEObject;
			T result = caseMElementWithFont(mElementWithFont);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTable Editor Element</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTable Editor Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTableEditorElement(MTableEditorElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEditor Config</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEditor Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEditorConfig(MEditorConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTable Config</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTable Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTableConfig(MTableConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MColumn Config</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MColumn Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMColumnConfig(MColumnConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MClass To Cell Config</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MClass To Cell Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMClassToCellConfig(MClassToCellConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MCell Config</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MCell Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMCellConfig(MCellConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRow Feature Cell</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRow Feature Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRowFeatureCell(MRowFeatureCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MOcl Cell</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MOcl Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMOclCell(MOclCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEdit Provider Cell</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEdit Provider Cell</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEditProviderCell(MEditProviderCell object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MReference To Table Config</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MReference To Table Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMReferenceToTableConfig(MReferenceToTableConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MClass To Table Config</em>'.
	 * <!-- begin-user-doc --> This
	 * implementation returns null; returning a non-null result will terminate
	 * the switch. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MClass To Table Config</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMClassToTableConfig(MClassToTableConfig object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MElement With Font</em>'.
	 * <!-- begin-user-doc --> This implementation
	 * returns null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MElement With Font</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMElementWithFont(MElementWithFont object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAElement(AElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MEditor</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch. <!--
	 * end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MEditor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMEditor(MEditor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc --> This implementation returns
	 * null; returning a non-null result will terminate the switch, but this is
	 * the last case anyway. <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} // MtableeditorSwitch
