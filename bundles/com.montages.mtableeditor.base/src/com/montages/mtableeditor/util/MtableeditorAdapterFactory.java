/**
 */
package com.montages.mtableeditor.util;

import com.montages.mcore.MEditor;
import com.montages.mtableeditor.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;
import org.langlets.autil.AElement;

/**
 * <!-- begin-user-doc --> The <b>Adapter Factory</b> for the model. It provides
 * an adapter <code>createXXX</code> method for each class of the model. <!--
 * end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage
 * @generated
 */
public class MtableeditorAdapterFactory extends AdapterFactoryImpl {

	/**
	 * The cached model package.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected static MtableeditorPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public MtableeditorAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = MtableeditorPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc --> This implementation returns <code>true</code> if
	 * the object is either the model's package or is an instance object of the
	 * model. <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected MtableeditorSwitch<Adapter> modelSwitch = new MtableeditorSwitch<Adapter>() {
		@Override
		public Adapter caseMTableEditorElement(MTableEditorElement object) {
			return createMTableEditorElementAdapter();
		}

		@Override
		public Adapter caseMEditorConfig(MEditorConfig object) {
			return createMEditorConfigAdapter();
		}

		@Override
		public Adapter caseMTableConfig(MTableConfig object) {
			return createMTableConfigAdapter();
		}

		@Override
		public Adapter caseMColumnConfig(MColumnConfig object) {
			return createMColumnConfigAdapter();
		}

		@Override
		public Adapter caseMClassToCellConfig(MClassToCellConfig object) {
			return createMClassToCellConfigAdapter();
		}

		@Override
		public Adapter caseMCellConfig(MCellConfig object) {
			return createMCellConfigAdapter();
		}

		@Override
		public Adapter caseMRowFeatureCell(MRowFeatureCell object) {
			return createMRowFeatureCellAdapter();
		}

		@Override
		public Adapter caseMOclCell(MOclCell object) {
			return createMOclCellAdapter();
		}

		@Override
		public Adapter caseMEditProviderCell(MEditProviderCell object) {
			return createMEditProviderCellAdapter();
		}

		@Override
		public Adapter caseMReferenceToTableConfig(MReferenceToTableConfig object) {
			return createMReferenceToTableConfigAdapter();
		}

		@Override
		public Adapter caseMClassToTableConfig(MClassToTableConfig object) {
			return createMClassToTableConfigAdapter();
		}

		@Override
		public Adapter caseMElementWithFont(MElementWithFont object) {
			return createMElementWithFontAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseMEditor(MEditor object) {
			return createMEditorAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MTableEditorElement <em>MTable Editor Element</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MTableEditorElement
	 * @generated
	 */
	public Adapter createMTableEditorElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MEditorConfig <em>MEditor Config</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MEditorConfig
	 * @generated
	 */
	public Adapter createMEditorConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MTableConfig <em>MTable Config</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MTableConfig
	 * @generated
	 */
	public Adapter createMTableConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MColumnConfig <em>MColumn Config</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MColumnConfig
	 * @generated
	 */
	public Adapter createMColumnConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MClassToCellConfig <em>MClass To Cell Config</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MClassToCellConfig
	 * @generated
	 */
	public Adapter createMClassToCellConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MCellConfig <em>MCell Config</em>}'.
	 * <!-- begin-user-doc --> This default implementation returns null so that
	 * we can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MCellConfig
	 * @generated
	 */
	public Adapter createMCellConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MRowFeatureCell <em>MRow Feature Cell</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MRowFeatureCell
	 * @generated
	 */
	public Adapter createMRowFeatureCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link com.montages.mtableeditor.MOclCell <em>MOcl Cell</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MOclCell
	 * @generated
	 */
	public Adapter createMOclCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MEditProviderCell <em>MEdit Provider Cell</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MEditProviderCell
	 * @generated
	 */
	public Adapter createMEditProviderCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MReferenceToTableConfig <em>MReference To Table Config</em>}'.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases
	 * anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MReferenceToTableConfig
	 * @generated
	 */
	public Adapter createMReferenceToTableConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MClassToTableConfig <em>MClass To Table Config</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MClassToTableConfig
	 * @generated
	 */
	public Adapter createMClassToTableConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mtableeditor.MElementWithFont <em>MElement With Font</em>}'.
	 * <!-- begin-user-doc --> This default
	 * implementation returns null so that we can easily ignore cases; it's
	 * useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mtableeditor.MElementWithFont
	 * @generated
	 */
	public Adapter createMElementWithFontAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '
	 * {@link org.langlets.autil.AElement <em>AElement</em>}'. <!--
	 * begin-user-doc --> This default implementation returns null so that we
	 * can easily ignore cases; it's useful to ignore a case when inheritance
	 * will catch all the cases anyway. <!-- end-user-doc -->
	 * 
	 * @return the new adapter.
	 * @see org.langlets.autil.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MEditor <em>MEditor</em>}'.
	 * <!-- begin-user-doc
	 * --> This default implementation returns null so that we can easily ignore
	 * cases; it's useful to ignore a case when inheritance will catch all the
	 * cases anyway. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MEditor
	 * @generated
	 */
	public Adapter createMEditorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc --> This
	 * default implementation returns null. <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} // MtableeditorAdapterFactory
