/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mtableeditor.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc --> An implementation of the model <b>Factory</b>. <!--
 * end-user-doc -->
 * @generated
 */
public class MtableeditorFactoryImpl extends EFactoryImpl implements MtableeditorFactory {

	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public static MtableeditorFactory init() {
		try {
			MtableeditorFactory theMtableeditorFactory = (MtableeditorFactory) EPackage.Registry.INSTANCE
					.getEFactory(MtableeditorPackage.eNS_URI);
			if (theMtableeditorFactory != null) {
				return theMtableeditorFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MtableeditorFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	public MtableeditorFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case MtableeditorPackage.MEDITOR_CONFIG:
			return createMEditorConfig();
		case MtableeditorPackage.MTABLE_CONFIG:
			return createMTableConfig();
		case MtableeditorPackage.MCOLUMN_CONFIG:
			return createMColumnConfig();
		case MtableeditorPackage.MCLASS_TO_CELL_CONFIG:
			return createMClassToCellConfig();
		case MtableeditorPackage.MROW_FEATURE_CELL:
			return createMRowFeatureCell();
		case MtableeditorPackage.MOCL_CELL:
			return createMOclCell();
		case MtableeditorPackage.MEDIT_PROVIDER_CELL:
			return createMEditProviderCell();
		case MtableeditorPackage.MREFERENCE_TO_TABLE_CONFIG:
			return createMReferenceToTableConfig();
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG:
			return createMClassToTableConfig();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case MtableeditorPackage.MEDITOR_CONFIG_ACTION:
			return createMEditorConfigActionFromString(eDataType, initialValue);
		case MtableeditorPackage.MTABLE_CONFIG_ACTION:
			return createMTableConfigActionFromString(eDataType, initialValue);
		case MtableeditorPackage.MCOLUMN_CONFIG_ACTION:
			return createMColumnConfigActionFromString(eDataType, initialValue);
		case MtableeditorPackage.MCELL_EDIT_BEHAVIOR_OPTION:
			return createMCellEditBehaviorOptionFromString(eDataType, initialValue);
		case MtableeditorPackage.MCELL_LOCATE_BEHAVIOR_OPTION:
			return createMCellLocateBehaviorOptionFromString(eDataType, initialValue);
		case MtableeditorPackage.MFONT_SIZE_ADAPTOR:
			return createMFontSizeAdaptorFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case MtableeditorPackage.MEDITOR_CONFIG_ACTION:
			return convertMEditorConfigActionToString(eDataType, instanceValue);
		case MtableeditorPackage.MTABLE_CONFIG_ACTION:
			return convertMTableConfigActionToString(eDataType, instanceValue);
		case MtableeditorPackage.MCOLUMN_CONFIG_ACTION:
			return convertMColumnConfigActionToString(eDataType, instanceValue);
		case MtableeditorPackage.MCELL_EDIT_BEHAVIOR_OPTION:
			return convertMCellEditBehaviorOptionToString(eDataType, instanceValue);
		case MtableeditorPackage.MCELL_LOCATE_BEHAVIOR_OPTION:
			return convertMCellLocateBehaviorOptionToString(eDataType, instanceValue);
		case MtableeditorPackage.MFONT_SIZE_ADAPTOR:
			return convertMFontSizeAdaptorToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfig createMEditorConfig() {
		MEditorConfigImpl mEditorConfig = new MEditorConfigImpl();
		return mEditorConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig createMTableConfig() {
		MTableConfigImpl mTableConfig = new MTableConfigImpl();
		return mTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MColumnConfig createMColumnConfig() {
		MColumnConfigImpl mColumnConfig = new MColumnConfigImpl();
		return mColumnConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassToCellConfig createMClassToCellConfig() {
		MClassToCellConfigImpl mClassToCellConfig = new MClassToCellConfigImpl();
		return mClassToCellConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MRowFeatureCell createMRowFeatureCell() {
		MRowFeatureCellImpl mRowFeatureCell = new MRowFeatureCellImpl();
		return mRowFeatureCell;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MOclCell createMOclCell() {
		MOclCellImpl mOclCell = new MOclCellImpl();
		return mOclCell;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditProviderCell createMEditProviderCell() {
		MEditProviderCellImpl mEditProviderCell = new MEditProviderCellImpl();
		return mEditProviderCell;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MReferenceToTableConfig createMReferenceToTableConfig() {
		MReferenceToTableConfigImpl mReferenceToTableConfig = new MReferenceToTableConfigImpl();
		return mReferenceToTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassToTableConfig createMClassToTableConfig() {
		MClassToTableConfigImpl mClassToTableConfig = new MClassToTableConfigImpl();
		return mClassToTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellEditBehaviorOption createMCellEditBehaviorOptionFromString(EDataType eDataType, String initialValue) {
		MCellEditBehaviorOption result = MCellEditBehaviorOption.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMCellEditBehaviorOptionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellLocateBehaviorOption createMCellLocateBehaviorOptionFromString(EDataType eDataType,
			String initialValue) {
		MCellLocateBehaviorOption result = MCellLocateBehaviorOption.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMCellLocateBehaviorOptionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MFontSizeAdaptor createMFontSizeAdaptorFromString(EDataType eDataType, String initialValue) {
		MFontSizeAdaptor result = MFontSizeAdaptor.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMFontSizeAdaptorToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfigAction createMEditorConfigActionFromString(EDataType eDataType, String initialValue) {
		MEditorConfigAction result = MEditorConfigAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMEditorConfigActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfigAction createMTableConfigActionFromString(EDataType eDataType, String initialValue) {
		MTableConfigAction result = MTableConfigAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMTableConfigActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MColumnConfigAction createMColumnConfigActionFromString(EDataType eDataType, String initialValue) {
		MColumnConfigAction result = MColumnConfigAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMColumnConfigActionToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MtableeditorPackage getMtableeditorPackage() {
		return (MtableeditorPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MtableeditorPackage getPackage() {
		return MtableeditorPackage.eINSTANCE;
	}

} // MtableeditorFactoryImpl
