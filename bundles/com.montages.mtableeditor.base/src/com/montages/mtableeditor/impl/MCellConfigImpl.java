/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MClassifier;
import com.montages.mtableeditor.MCellConfig;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MElementWithFont;
import com.montages.mtableeditor.MFontSizeAdaptor;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.langlets.autil.AutilPackage;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MCell Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getCellConfig <em>Cell Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getContainingColumnConfig <em>Containing Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getCellKind <em>Cell Kind</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getBoldFont <em>Bold Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getItalicFont <em>Italic Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getExplicitHighlightOCL <em>Explicit Highlight OCL</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MCellConfigImpl#getContainingClassToCellConfig <em>Containing Class To Cell Config</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MCellConfigImpl extends MTableEditorElementImpl implements MCellConfig, IXoclInitializable {

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected MClassifier class_;

	/**
	 * This is true if the Class reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean classESet;

	/**
	 * The cached value of the '{@link #getCellConfig() <em>Cell Config</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCellConfig()
	 * @generated
	 * @ordered
	 */
	protected MCellConfig cellConfig;

	/**
	 * This is true if the Cell Config containment reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean cellConfigESet;

	/**
	 * The default value of the '{@link #getCellKind() <em>Cell Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCellKind()
	 * @generated
	 * @ordered
	 */
	protected static final String CELL_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BOLD_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean boldFont = BOLD_FONT_EDEFAULT;

	/**
	 * This is true if the Bold Font attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean boldFontESet;

	/**
	 * The default value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ITALIC_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean italicFont = ITALIC_FONT_EDEFAULT;

	/**
	 * This is true if the Italic Font attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean italicFontESet;

	/**
	 * The default value of the '{@link #getFontSize() <em>Font Size</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFontSize()
	 * @generated NOT
	 * @ordered
	 */
	protected static final MFontSizeAdaptor FONT_SIZE_EDEFAULT = MFontSizeAdaptor.ZERO;

	/**
	 * The cached value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected MFontSizeAdaptor fontSize = FONT_SIZE_EDEFAULT;

	/**
	 * This is true if the Font Size attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fontSizeESet;

	/**
	 * The default value of the '{@link #getDerivedFontOptionsEncoding() <em>Derived Font Options Encoding</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getExplicitHighlightOCL() <em>Explicit Highlight OCL</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExplicitHighlightOCL()
	 * @generated
	 * @ordered
	 */
	protected static final String EXPLICIT_HIGHLIGHT_OCL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getExplicitHighlightOCL() <em>Explicit Highlight OCL</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getExplicitHighlightOCL()
	 * @generated
	 * @ordered
	 */
	protected String explicitHighlightOCL = EXPLICIT_HIGHLIGHT_OCL_EDEFAULT;

	/**
	 * This is true if the Explicit Highlight OCL attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean explicitHighlightOCLESet;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getClass_ <em>Class</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClass_
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression class_ChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCellKind
	 * <em>Cell Kind</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getCellKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression cellKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}' property.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedFontOptionsEncodingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase
	 * <em>AKind Base</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getAKindBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MCellConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MCELL_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getClass_() {
		if (class_ != null && class_.eIsProxy()) {
			InternalEObject oldClass = (InternalEObject) class_;
			class_ = (MClassifier) eResolveProxy(oldClass);
			if (class_ != oldClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, MtableeditorPackage.MCELL_CONFIG__CLASS,
							oldClass, class_));
			}
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetClass() {
		return class_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(MClassifier newClass) {
		MClassifier oldClass = class_;
		class_ = newClass;
		boolean oldClassESet = classESet;
		classESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCELL_CONFIG__CLASS, oldClass,
					class_, !oldClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClass() {
		MClassifier oldClass = class_;
		boolean oldClassESet = classESet;
		class_ = null;
		classESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCELL_CONFIG__CLASS, oldClass,
					null, oldClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClass() {
		return classESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Class</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let rightPackage: Boolean = if (let chain: mcore::MPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()
	then null
	else containingColumnConfig.containingTableConfig.intendedPackage
	endif in
	if chain <> null then true else false 
	endif) 
	=true 
	then (let e0: Boolean = trg.containingPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()
	then null
	else containingColumnConfig.containingTableConfig.intendedPackage
	endif in 
	if e0.oclIsInvalid() then null else e0 endif)
	else true
	endif in
	let isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
	if e1.oclIsInvalid() then null else e1 endif in
	let intended: Boolean = let e1: Boolean = if (isClass)= false 
	then false 
	else if (rightPackage)= false 
	then false 
	else if ((isClass)= null or (rightPackage)= null) = true 
	then null 
	else true endif endif endif in 
	if e1.oclIsInvalid() then null else e1 endif in
	intended
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalClassChoiceConstraint(MClassifier trg) {
		EClass eClass = MtableeditorPackage.Literals.MCELL_CONFIG;
		if (class_ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CLASS;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				class_ChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "ClassChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(class_ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "ClassChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellConfig getCellConfig() {
		if (cellConfig != null && cellConfig.eIsProxy()) {
			InternalEObject oldCellConfig = (InternalEObject) cellConfig;
			cellConfig = (MCellConfig) eResolveProxy(oldCellConfig);
			if (cellConfig != oldCellConfig) {
				InternalEObject newCellConfig = (InternalEObject) cellConfig;
				NotificationChain msgs = oldCellConfig.eInverseRemove(this,
						MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, MCellConfig.class, null);
				if (newCellConfig.eInternalContainer() == null) {
					msgs = newCellConfig.eInverseAdd(this,
							MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, MCellConfig.class, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG, oldCellConfig, cellConfig));
			}
		}
		return cellConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellConfig basicGetCellConfig() {
		return cellConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCellConfig(MCellConfig newCellConfig, NotificationChain msgs) {
		MCellConfig oldCellConfig = cellConfig;
		cellConfig = newCellConfig;
		boolean oldCellConfigESet = cellConfigESet;
		cellConfigESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG, oldCellConfig, newCellConfig, !oldCellConfigESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellConfig(MCellConfig newCellConfig) {
		if (newCellConfig != cellConfig) {
			NotificationChain msgs = null;
			if (cellConfig != null)
				msgs = ((InternalEObject) cellConfig).eInverseRemove(this,
						MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, MCellConfig.class, msgs);
			if (newCellConfig != null)
				msgs = ((InternalEObject) newCellConfig).eInverseAdd(this,
						MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, MCellConfig.class, msgs);
			msgs = basicSetCellConfig(newCellConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldCellConfigESet = cellConfigESet;
			cellConfigESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG,
						newCellConfig, newCellConfig, !oldCellConfigESet));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetCellConfig(NotificationChain msgs) {
		MCellConfig oldCellConfig = cellConfig;
		cellConfig = null;
		boolean oldCellConfigESet = cellConfigESet;
		cellConfigESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG, oldCellConfig, null, oldCellConfigESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCellConfig() {
		if (cellConfig != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) cellConfig).eInverseRemove(this,
					MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, MCellConfig.class, msgs);
			msgs = basicUnsetCellConfig(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldCellConfigESet = cellConfigESet;
			cellConfigESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG,
						null, null, oldCellConfigESet));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCellConfig() {
		return cellConfigESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MColumnConfig getContainingColumnConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG)
			return null;
		return (MColumnConfig) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MColumnConfig basicGetContainingColumnConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG)
			return null;
		return (MColumnConfig) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingColumnConfig(MColumnConfig newContainingColumnConfig,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContainingColumnConfig,
				MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingColumnConfig(MColumnConfig newContainingColumnConfig) {
		if (newContainingColumnConfig != eInternalContainer()
				|| (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG
						&& newContainingColumnConfig != null)) {
			if (EcoreUtil.isAncestor(this, newContainingColumnConfig))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingColumnConfig != null)
				msgs = ((InternalEObject) newContainingColumnConfig).eInverseAdd(this,
						MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG, MColumnConfig.class, msgs);
			msgs = basicSetContainingColumnConfig(newContainingColumnConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG, newContainingColumnConfig,
					newContainingColumnConfig));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getCellKind() {
		/**
		 * @OCL if cellConfig.oclIsUndefined()
		then null
		else cellConfig.cellKind
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCELL_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_KIND;

		if (cellKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				cellKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCELL_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(cellKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCELL_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getExplicitHighlightOCL() {
		return explicitHighlightOCL;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setExplicitHighlightOCL(String newExplicitHighlightOCL) {
		String oldExplicitHighlightOCL = explicitHighlightOCL;
		explicitHighlightOCL = newExplicitHighlightOCL;
		boolean oldExplicitHighlightOCLESet = explicitHighlightOCLESet;
		explicitHighlightOCLESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL, oldExplicitHighlightOCL,
					explicitHighlightOCL, !oldExplicitHighlightOCLESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExplicitHighlightOCL() {
		String oldExplicitHighlightOCL = explicitHighlightOCL;
		boolean oldExplicitHighlightOCLESet = explicitHighlightOCLESet;
		explicitHighlightOCL = EXPLICIT_HIGHLIGHT_OCL_EDEFAULT;
		explicitHighlightOCLESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL, oldExplicitHighlightOCL,
					EXPLICIT_HIGHLIGHT_OCL_EDEFAULT, oldExplicitHighlightOCLESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExplicitHighlightOCL() {
		return explicitHighlightOCLESet;
	}

	/**
	 * Returns the OCL expression context for the '
	 * <em><b>Explicit Highlight OCL</b></em>' attribute. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @templateTag GFI03
	 * @generated NOT
	 */
	public OCLExpressionContext getExplicitHighlightOCLOCLExpressionContext() {
		return new OCLExpressionContext(OCLExpressionContext.getEObjectClass(), null, null);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBoldFont() {
		return boldFont;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBoldFont(Boolean newBoldFont) {
		Boolean oldBoldFont = boldFont;
		boldFont = newBoldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCELL_CONFIG__BOLD_FONT,
					oldBoldFont, boldFont, !oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBoldFont() {
		Boolean oldBoldFont = boldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFont = BOLD_FONT_EDEFAULT;
		boldFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCELL_CONFIG__BOLD_FONT,
					oldBoldFont, BOLD_FONT_EDEFAULT, oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBoldFont() {
		return boldFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getItalicFont() {
		return italicFont;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setItalicFont(Boolean newItalicFont) {
		Boolean oldItalicFont = italicFont;
		italicFont = newItalicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT,
					oldItalicFont, italicFont, !oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItalicFont() {
		Boolean oldItalicFont = italicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFont = ITALIC_FONT_EDEFAULT;
		italicFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT,
					oldItalicFont, ITALIC_FONT_EDEFAULT, oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItalicFont() {
		return italicFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MFontSizeAdaptor getFontSize() {
		return fontSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFontSize(MFontSizeAdaptor newFontSize) {
		MFontSizeAdaptor oldFontSize = fontSize;
		fontSize = newFontSize == null ? FONT_SIZE_EDEFAULT : newFontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSizeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCELL_CONFIG__FONT_SIZE,
					oldFontSize, fontSize, !oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFontSize() {
		MFontSizeAdaptor oldFontSize = fontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSize = FONT_SIZE_EDEFAULT;
		fontSizeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCELL_CONFIG__FONT_SIZE,
					oldFontSize, FONT_SIZE_EDEFAULT, oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFontSize() {
		return fontSizeESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedFontOptionsEncoding() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCELL_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING;

		if (derivedFontOptionsEncodingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				derivedFontOptionsEncodingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCELL_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedFontOptionsEncodingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCELL_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassToCellConfig getContainingClassToCellConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG)
			return null;
		return (MClassToCellConfig) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassToCellConfig basicGetContainingClassToCellConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG)
			return null;
		return (MClassToCellConfig) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingClassToCellConfig(MClassToCellConfig newContainingClassToCellConfig,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContainingClassToCellConfig,
				MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingClassToCellConfig(MClassToCellConfig newContainingClassToCellConfig) {
		if (newContainingClassToCellConfig != eInternalContainer()
				|| (eContainerFeatureID() != MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG
						&& newContainingClassToCellConfig != null)) {
			if (EcoreUtil.isAncestor(this, newContainingClassToCellConfig))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingClassToCellConfig != null)
				msgs = ((InternalEObject) newContainingClassToCellConfig).eInverseAdd(this,
						MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_CONFIG, MClassToCellConfig.class, msgs);
			msgs = basicSetContainingClassToCellConfig(newContainingClassToCellConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG, newContainingClassToCellConfig,
					newContainingClassToCellConfig));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			if (cellConfig != null)
				msgs = ((InternalEObject) cellConfig).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG, null, msgs);
			return basicSetCellConfig((MCellConfig) otherEnd, msgs);
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingColumnConfig((MColumnConfig) otherEnd, msgs);
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingClassToCellConfig((MClassToCellConfig) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			return basicUnsetCellConfig(msgs);
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			return basicSetContainingColumnConfig(null, msgs);
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			return basicSetContainingClassToCellConfig(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			return eInternalContainer().eInverseRemove(this, MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
					MColumnConfig.class, msgs);
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			return eInternalContainer().eInverseRemove(this, MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_CONFIG,
					MClassToCellConfig.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CLASS:
			if (resolve)
				return getClass_();
			return basicGetClass();
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			if (resolve)
				return getCellConfig();
			return basicGetCellConfig();
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			if (resolve)
				return getContainingColumnConfig();
			return basicGetContainingColumnConfig();
		case MtableeditorPackage.MCELL_CONFIG__CELL_KIND:
			return getCellKind();
		case MtableeditorPackage.MCELL_CONFIG__BOLD_FONT:
			return getBoldFont();
		case MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT:
			return getItalicFont();
		case MtableeditorPackage.MCELL_CONFIG__FONT_SIZE:
			return getFontSize();
		case MtableeditorPackage.MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
			return getDerivedFontOptionsEncoding();
		case MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL:
			return getExplicitHighlightOCL();
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			if (resolve)
				return getContainingClassToCellConfig();
			return basicGetContainingClassToCellConfig();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CLASS:
			setClass((MClassifier) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			setCellConfig((MCellConfig) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			setContainingColumnConfig((MColumnConfig) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__BOLD_FONT:
			setBoldFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT:
			setItalicFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__FONT_SIZE:
			setFontSize((MFontSizeAdaptor) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL:
			setExplicitHighlightOCL((String) newValue);
			return;
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			setContainingClassToCellConfig((MClassToCellConfig) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CLASS:
			unsetClass();
			return;
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			unsetCellConfig();
			return;
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			setContainingColumnConfig((MColumnConfig) null);
			return;
		case MtableeditorPackage.MCELL_CONFIG__BOLD_FONT:
			unsetBoldFont();
			return;
		case MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT:
			unsetItalicFont();
			return;
		case MtableeditorPackage.MCELL_CONFIG__FONT_SIZE:
			unsetFontSize();
			return;
		case MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL:
			unsetExplicitHighlightOCL();
			return;
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			setContainingClassToCellConfig((MClassToCellConfig) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCELL_CONFIG__CLASS:
			return isSetClass();
		case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
			return isSetCellConfig();
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
			return basicGetContainingColumnConfig() != null;
		case MtableeditorPackage.MCELL_CONFIG__CELL_KIND:
			return CELL_KIND_EDEFAULT == null ? getCellKind() != null : !CELL_KIND_EDEFAULT.equals(getCellKind());
		case MtableeditorPackage.MCELL_CONFIG__BOLD_FONT:
			return isSetBoldFont();
		case MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT:
			return isSetItalicFont();
		case MtableeditorPackage.MCELL_CONFIG__FONT_SIZE:
			return isSetFontSize();
		case MtableeditorPackage.MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
			return DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT == null ? getDerivedFontOptionsEncoding() != null
					: !DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT.equals(getDerivedFontOptionsEncoding());
		case MtableeditorPackage.MCELL_CONFIG__EXPLICIT_HIGHLIGHT_OCL:
			return isSetExplicitHighlightOCL();
		case MtableeditorPackage.MCELL_CONFIG__CONTAINING_CLASS_TO_CELL_CONFIG:
			return basicGetContainingClassToCellConfig() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == MClassToCellConfig.class) {
			switch (derivedFeatureID) {
			case MtableeditorPackage.MCELL_CONFIG__CLASS:
				return MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CLASS;
			case MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG:
				return MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_CONFIG;
			case MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG:
				return MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG;
			case MtableeditorPackage.MCELL_CONFIG__CELL_KIND:
				return MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_KIND;
			default:
				return -1;
			}
		}
		if (baseClass == MElementWithFont.class) {
			switch (derivedFeatureID) {
			case MtableeditorPackage.MCELL_CONFIG__BOLD_FONT:
				return MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT;
			case MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT:
				return MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT;
			case MtableeditorPackage.MCELL_CONFIG__FONT_SIZE:
				return MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE;
			case MtableeditorPackage.MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
				return MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == MClassToCellConfig.class) {
			switch (baseFeatureID) {
			case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CLASS:
				return MtableeditorPackage.MCELL_CONFIG__CLASS;
			case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_CONFIG:
				return MtableeditorPackage.MCELL_CONFIG__CELL_CONFIG;
			case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG:
				return MtableeditorPackage.MCELL_CONFIG__CONTAINING_COLUMN_CONFIG;
			case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_KIND:
				return MtableeditorPackage.MCELL_CONFIG__CELL_KIND;
			default:
				return -1;
			}
		}
		if (baseClass == MElementWithFont.class) {
			switch (baseFeatureID) {
			case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
				return MtableeditorPackage.MCELL_CONFIG__BOLD_FONT;
			case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
				return MtableeditorPackage.MCELL_CONFIG__ITALIC_FONT;
			case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
				return MtableeditorPackage.MCELL_CONFIG__FONT_SIZE;
			case MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING:
				return MtableeditorPackage.MCELL_CONFIG__DERIVED_FONT_OPTIONS_ENCODING;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (boldFont: ");
		if (boldFontESet)
			result.append(boldFont);
		else
			result.append("<unset>");
		result.append(", italicFont: ");
		if (italicFontESet)
			result.append(italicFont);
		else
			result.append("<unset>");
		result.append(", fontSize: ");
		if (fontSizeESet)
			result.append(fontSize);
		else
			result.append("<unset>");
		result.append(", explicitHighlightOCL: ");
		if (explicitHighlightOCLESet)
			result.append(explicitHighlightOCL);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL aKindBase let const1: String = 'Cell' in
	const1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAKindBase() {
		EClass eClass = (MtableeditorPackage.Literals.MCELL_CONFIG);
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CLASS,
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__FONT_SIZE };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCELL_CONFIG,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MCellConfigImpl
