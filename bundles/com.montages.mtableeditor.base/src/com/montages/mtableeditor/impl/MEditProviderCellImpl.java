/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mtableeditor.MEditProviderCell;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.ecore.OCLExpression;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MEdit Provider Cell</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public class MEditProviderCellImpl extends MCellConfigImpl implements MEditProviderCell {

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MEditProviderCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MEDIT_PROVIDER_CELL;
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} // MEditProviderCellImpl
