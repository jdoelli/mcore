/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.PropertyBehavior;
import com.montages.mtableeditor.MCellConfig;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MColumnConfigAction;
import com.montages.mtableeditor.MElementWithFont;
import com.montages.mtableeditor.MFontSizeAdaptor;
import com.montages.mtableeditor.MOclCell;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MtableeditorPackage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.langlets.autil.AutilPackage;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MColumn Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getBoldFont <em>Bold Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getItalicFont <em>Italic Font</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getFontSize <em>Font Size</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getClassToCellConfig <em>Class To Cell Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getECName <em>EC Name</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getECLabel <em>EC Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getECWidth <em>EC Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getMinimumWidth <em>Minimum Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getMaximumWidth <em>Maximum Width</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getContainingTableConfig <em>Containing Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MColumnConfigImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MColumnConfigImpl extends MTableEditorElementImpl implements MColumnConfig, IXoclInitializable {

	/**
	 * The default value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean BOLD_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getBoldFont() <em>Bold Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getBoldFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean boldFont = BOLD_FONT_EDEFAULT;

	/**
	 * This is true if the Bold Font attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean boldFontESet;

	/**
	 * The default value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ITALIC_FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getItalicFont() <em>Italic Font</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getItalicFont()
	 * @generated
	 * @ordered
	 */
	protected Boolean italicFont = ITALIC_FONT_EDEFAULT;

	/**
	 * This is true if the Italic Font attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean italicFontESet;

	/**
	 * The default value of the '{@link #getFontSize() <em>Font Size</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getFontSize()
	 * @generated NOT
	 * @ordered
	 */
	protected static final MFontSizeAdaptor FONT_SIZE_EDEFAULT = MFontSizeAdaptor.ZERO;

	/**
	 * The cached value of the '{@link #getFontSize() <em>Font Size</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFontSize()
	 * @generated
	 * @ordered
	 */
	protected MFontSizeAdaptor fontSize = FONT_SIZE_EDEFAULT;

	/**
	 * This is true if the Font Size attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean fontSizeESet;

	/**
	 * The default value of the '{@link #getDerivedFontOptionsEncoding() <em>Derived Font Options Encoding</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The cached value of the '{@link #getClassToCellConfig()
	 * <em>Class To Cell Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getClassToCellConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MClassToCellConfig> classToCellConfig;

	/**
	 * The default value of the '{@link #getECName() <em>EC Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getECName()
	 * @generated
	 * @ordered
	 */
	protected static final String EC_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getECLabel() <em>EC Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getECLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String EC_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getECWidth() <em>EC Width</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getECWidth()
	 * @generated
	 * @ordered
	 */
	protected static final Integer EC_WIDTH_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * This is true if the Label attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean labelESet;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final Integer WIDTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected Integer width = WIDTH_EDEFAULT;

	/**
	 * This is true if the Width attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean widthESet;

	/**
	 * The default value of the '{@link #getMinimumWidth() <em>Minimum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumWidth()
	 * @generated
	 * @ordered
	 */
	protected static final Integer MINIMUM_WIDTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMinimumWidth() <em>Minimum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinimumWidth()
	 * @generated
	 * @ordered
	 */
	protected Integer minimumWidth = MINIMUM_WIDTH_EDEFAULT;

	/**
	 * This is true if the Minimum Width attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean minimumWidthESet;

	/**
	 * The default value of the '{@link #getMaximumWidth() <em>Maximum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumWidth()
	 * @generated
	 * @ordered
	 */
	protected static final Integer MAXIMUM_WIDTH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getMaximumWidth() <em>Maximum Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaximumWidth()
	 * @generated
	 * @ordered
	 */
	protected Integer maximumWidth = MAXIMUM_WIDTH_EDEFAULT;

	/**
	 * This is true if the Maximum Width attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean maximumWidthESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MColumnConfigAction DO_ACTION_EDEFAULT = MColumnConfigAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemtableeditorMColumnConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #cellConfigFromClass <em>Cell Config From Class</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #cellConfigFromClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression cellConfigFromClassmcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemtableeditorMColumnConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedFontOptionsEncoding <em>Derived Font Options Encoding</em>}' property.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getDerivedFontOptionsEncoding
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedFontOptionsEncodingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getECName
	 * <em>EC Name</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getECName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getECLabel
	 * <em>EC Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getECLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getECWidth
	 * <em>EC Width</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getECWidth
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eCWidthDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase
	 * <em>AKind Base</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getAKindBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MColumnConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MCOLUMN_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__NAME, oldName,
					name, !oldNameESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__NAME, oldName,
					NAME_EDEFAULT, oldNameESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassToCellConfig> getClassToCellConfig() {
		if (classToCellConfig == null) {
			classToCellConfig = new EObjectContainmentWithInverseEList.Unsettable.Resolving<MClassToCellConfig>(
					MClassToCellConfig.class, this, MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
					MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CONTAINING_COLUMN_CONFIG);
		}
		return classToCellConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClassToCellConfig() {
		if (classToCellConfig != null)
			((InternalEList.Unsettable<?>) classToCellConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClassToCellConfig() {
		return classToCellConfig != null && ((InternalEList.Unsettable<?>) classToCellConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getBoldFont() {
		return boldFont;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBoldFont(Boolean newBoldFont) {
		Boolean oldBoldFont = boldFont;
		boldFont = newBoldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT,
					oldBoldFont, boldFont, !oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetBoldFont() {
		Boolean oldBoldFont = boldFont;
		boolean oldBoldFontESet = boldFontESet;
		boldFont = BOLD_FONT_EDEFAULT;
		boldFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT,
					oldBoldFont, BOLD_FONT_EDEFAULT, oldBoldFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetBoldFont() {
		return boldFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getItalicFont() {
		return italicFont;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setItalicFont(Boolean newItalicFont) {
		Boolean oldItalicFont = italicFont;
		italicFont = newItalicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFontESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT,
					oldItalicFont, italicFont, !oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetItalicFont() {
		Boolean oldItalicFont = italicFont;
		boolean oldItalicFontESet = italicFontESet;
		italicFont = ITALIC_FONT_EDEFAULT;
		italicFontESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT,
					oldItalicFont, ITALIC_FONT_EDEFAULT, oldItalicFontESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetItalicFont() {
		return italicFontESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MFontSizeAdaptor getFontSize() {
		return fontSize;
	}

	/**
	 * <!-- begin-user-doc -->
	 * 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFontSize(MFontSizeAdaptor newFontSize) {
		MFontSizeAdaptor oldFontSize = fontSize;
		fontSize = newFontSize == null ? FONT_SIZE_EDEFAULT : newFontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSizeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE,
					oldFontSize, fontSize, !oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFontSize() {
		MFontSizeAdaptor oldFontSize = fontSize;
		boolean oldFontSizeESet = fontSizeESet;
		fontSize = FONT_SIZE_EDEFAULT;
		fontSizeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE,
					oldFontSize, FONT_SIZE_EDEFAULT, oldFontSizeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFontSize() {
		return fontSizeESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedFontOptionsEncoding() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCOLUMN_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING;

		if (derivedFontOptionsEncodingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				derivedFontOptionsEncodingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedFontOptionsEncodingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getECName() {
		/**
		 * @OCL let resultofecName: String = if (let e0: Boolean = let chain01: String = name in
		if chain01.trim().oclIsUndefined() 
		then null 
		else chain01.trim()
		endif = '' in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then if containingTableConfig.intendedClass.oclIsUndefined()
		then null
		else containingTableConfig.intendedClass.name
		endif
		else name
		endif in
		resultofecName
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCOLUMN_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_NAME;

		if (eCNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getECLabel() {
		/**
		 * @OCL label
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCOLUMN_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_LABEL;

		if (eCLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getECWidth() {
		/**
		 * @OCL let rowAverageSize: OrderedSet(Integer)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = classToCellConfig->asOrderedSet() in
		chain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() 
		else r endif)->collect(it: mtableeditor::MRowFeatureCell | it.averageSize)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let rowMaxSize: OrderedSet(Integer)  = let chain: OrderedSet(mtableeditor::MClassToCellConfig)  = classToCellConfig->asOrderedSet() in
		chain->iterate(i:mtableeditor::MClassToCellConfig; r: OrderedSet(mtableeditor::MRowFeatureCell)=OrderedSet{} | if i.oclIsKindOf(mtableeditor::MRowFeatureCell) then r->including(i.oclAsType(mtableeditor::MRowFeatureCell))->asOrderedSet() 
		else r endif)->collect(it: mtableeditor::MRowFeatureCell | it.maximumSize)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let choice: Integer = if (let e0: Boolean = (let e0: Integer = (rowMaxSize->max() / 2).round() in 
		if e0.oclIsInvalid() then null else e0 endif) > rowAverageSize->max() in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then (let e0: Integer = rowAverageSize->max() * 13 in 
		if e0.oclIsInvalid() then null else e0 endif)
		else (let e0: Integer = rowMaxSize->max() * 13 in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif in
		let noMaxandMin: Integer = if (let e0: Boolean = choice <= minimumWidth in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then minimumWidth
		else choice
		endif in
		let maxandNoMin: Integer = if (let e0: Boolean = choice <= maximumWidth in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then choice
		else maximumWidth
		endif in
		let maxandMin: Integer = if (let e0: Boolean = choice >= maximumWidth in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then maximumWidth else if (let e0: Boolean = choice <= minimumWidth in 
		if e0.oclIsInvalid() then null else e0 endif)=true then minimumWidth
		else choice
		endif endif in
		let maxorMin: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= false 
		then false 
		else if (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then maxandNoMin
		else noMaxandMin
		endif in
		let decision: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= false 
		then false 
		else if (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= false 
		then false 
		else if (let e0: Boolean = if (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= null or (let e0: Boolean = if (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then maxandMin
		else maxorMin
		endif in
		let oclDecision: Integer = if (let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= false 
		then false 
		else if (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let e0: Boolean = not(let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then maximumWidth else if (let e0: Boolean = if (let chain01: Integer = minimumWidth in
		if chain01 <> null then true else false 
		endif)= false 
		then false 
		else if (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if ((let chain01: Integer = minimumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let e0: Boolean = not(let chain01: Integer = minimumWidth in
		if chain01 = 0 then true else false 
		endif) in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= null) = true 
		then null 
		else true endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then minimumWidth
		else width
		endif endif in
		let resultofECWidth: Integer = if (let e0: Boolean = if ((let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= true 
		then true 
		else if (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= true 
		then true 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif))= false 
		then false 
		else if (let e0: Boolean = not((let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif)= false 
		then false 
		else if (let chain02: Integer = minimumWidth in
		if chain02 = 0 then true else false 
		endif)= false 
		then false 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif)= null or (let chain02: Integer = minimumWidth in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)) in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= false 
		then false 
		else if (let chain01: OrderedSet(Integer)  = rowMaxSize in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= false 
		then false 
		else if (let chain02: OrderedSet(Integer)  = rowAverageSize in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= false 
		then false 
		else if ((let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= true 
		then true 
		else if (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= true 
		then true 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 <> null then true else false 
		endif)= null or (let chain02: Integer = minimumWidth in
		if chain02 <> null then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = not((let e0: Boolean = if (let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif)= false 
		then false 
		else if (let chain02: Integer = minimumWidth in
		if chain02 = 0 then true else false 
		endif)= false 
		then false 
		else if ((let chain01: Integer = maximumWidth in
		if chain01 = 0 then true else false 
		endif)= null or (let chain02: Integer = minimumWidth in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)) in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = if ( width.oclIsUndefined())= true 
		then true 
		else if (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= true 
		then true 
		else if (( width.oclIsUndefined())= null or (let chain02: Integer = width in
		if chain02 = 0 then true else false 
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif)= null or (let chain01: OrderedSet(Integer)  = rowMaxSize in
		if chain01->notEmpty().oclIsUndefined() 
		then null 
		else chain01->notEmpty()
		endif)= null or (let chain02: OrderedSet(Integer)  = rowAverageSize in
		if chain02->notEmpty().oclIsUndefined() 
		then null 
		else chain02->notEmpty()
		endif)= null) = true 
		then null 
		else true endif endif endif endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then decision else if (let e0: Boolean = classToCellConfig->asOrderedSet()->select(it: mtableeditor::MClassToCellConfig | let e0: Boolean = it.cellKind = 'OclCell' in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() ->notEmpty() in 
		if e0.oclIsInvalid() then null else e0 endif)=true then oclDecision
		else width
		endif endif in
		resultofECWidth
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCOLUMN_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_WIDTH;

		if (eCWidthDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				eCWidthDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eCWidthDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Integer result = (Integer) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		boolean oldLabelESet = labelESet;
		labelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__LABEL, oldLabel,
					label, !oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLabel() {
		String oldLabel = label;
		boolean oldLabelESet = labelESet;
		label = LABEL_EDEFAULT;
		labelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__LABEL, oldLabel,
					LABEL_EDEFAULT, oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLabel() {
		return labelESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(Integer newWidth) {
		Integer oldWidth = width;
		width = newWidth;
		boolean oldWidthESet = widthESet;
		widthESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__WIDTH, oldWidth,
					width, !oldWidthESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetWidth() {
		Integer oldWidth = width;
		boolean oldWidthESet = widthESet;
		width = WIDTH_EDEFAULT;
		widthESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__WIDTH, oldWidth,
					WIDTH_EDEFAULT, oldWidthESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetWidth() {
		return widthESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getMinimumWidth() {
		return minimumWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinimumWidth(Integer newMinimumWidth) {
		Integer oldMinimumWidth = minimumWidth;
		minimumWidth = newMinimumWidth;
		boolean oldMinimumWidthESet = minimumWidthESet;
		minimumWidthESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH,
					oldMinimumWidth, minimumWidth, !oldMinimumWidthESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMinimumWidth() {
		Integer oldMinimumWidth = minimumWidth;
		boolean oldMinimumWidthESet = minimumWidthESet;
		minimumWidth = MINIMUM_WIDTH_EDEFAULT;
		minimumWidthESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH,
					oldMinimumWidth, MINIMUM_WIDTH_EDEFAULT, oldMinimumWidthESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMinimumWidth() {
		return minimumWidthESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getMaximumWidth() {
		return maximumWidth;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaximumWidth(Integer newMaximumWidth) {
		Integer oldMaximumWidth = maximumWidth;
		maximumWidth = newMaximumWidth;
		boolean oldMaximumWidthESet = maximumWidthESet;
		maximumWidthESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH,
					oldMaximumWidth, maximumWidth, !oldMaximumWidthESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMaximumWidth() {
		Integer oldMaximumWidth = maximumWidth;
		boolean oldMaximumWidthESet = maximumWidthESet;
		maximumWidth = MAXIMUM_WIDTH_EDEFAULT;
		maximumWidthESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH,
					oldMaximumWidth, MAXIMUM_WIDTH_EDEFAULT, oldMaximumWidthESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMaximumWidth() {
		return maximumWidthESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig getContainingTableConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG)
			return null;
		return (MTableConfig) eContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig basicGetContainingTableConfig() {
		if (eContainerFeatureID() != MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG)
			return null;
		return (MTableConfig) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingTableConfig(MTableConfig newContainingTableConfig,
			NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newContainingTableConfig,
				MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingTableConfig(MTableConfig newContainingTableConfig) {
		if (newContainingTableConfig != eInternalContainer()
				|| (eContainerFeatureID() != MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG
						&& newContainingTableConfig != null)) {
			if (EcoreUtil.isAncestor(this, newContainingTableConfig))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingTableConfig != null)
				msgs = ((InternalEObject) newContainingTableConfig).eInverseAdd(this,
						MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG, MTableConfig.class, msgs);
			msgs = basicSetContainingTableConfig(newContainingTableConfig, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG, newContainingTableConfig,
					newContainingTableConfig));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MColumnConfigAction getDoAction() {
		/**
		 * @OCL mtableeditor::MColumnConfigAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MCOLUMN_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MCOLUMN_CONFIG__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MColumnConfigAction result = (MColumnConfigAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoAction(MColumnConfigAction newDoAction) {
		if (newDoAction == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MColumnConfigAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE.createXTransition();
		com.montages.mtableeditor.MColumnConfigAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				MtableeditorPackage.eINSTANCE.getMColumnConfig_DoAction(), org.xocl.semantics.XUpdateMode.REDEFINE,
				null, triggerValue, null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MCellConfig cellConfigFromClass(MClassifier mClass) {

		/**
		 * @OCL null
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MtableeditorPackage.Literals.MCOLUMN_CONFIG);
		EOperation eOperation = MtableeditorPackage.Literals.MCOLUMN_CONFIG.getEOperations().get(1);
		if (cellConfigFromClassmcoreMClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				cellConfigFromClassmcoreMClassifierBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(cellConfigFromClassmcoreMClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("mClass", mClass);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MCellConfig) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MColumnConfigAction mColumnConfigAction) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();
		switch (mColumnConfigAction.getValue()) {
		case MColumnConfigAction.ADD_MROW_FEATURE_CELL_VALUE: {

			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMColumnConfigAction()
					.getEEnumLiteral(MColumnConfigAction.ADD_MROW_FEATURE_CELL_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMColumnConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

			MRowFeatureCell newRowCell = (MRowFeatureCell) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMRowFeatureCell());

			newClassCell.setClass(this.getContainingTableConfig().getIntendedClass());
			newClassCell.setCellConfig(newRowCell);
			newRowCell.setClass(this.getContainingTableConfig().getIntendedClass());

			if (this.getContainingTableConfig().getIntendedClass() != null) {
				List<MProperty> allUsableProperties = new ArrayList<MProperty>();
				List<MProperty> allUsedProperties = new ArrayList<MProperty>();
				for (MProperty mprop : this.getContainingTableConfig().getIntendedClass().allProperties()) {
					if (mprop.getPropertyBehavior() != PropertyBehavior.CONTAINED) {
						allUsableProperties.add(mprop);
					}
				}

				// Checks which properties aren't contained and are already in
				// use
				for (MColumnConfig column : this.getContainingTableConfig().getColumnConfig()) {
					for (MClassToCellConfig classToCell : column.getClassToCellConfig()) {
						if (classToCell instanceof MRowFeatureCell) {
							MRowFeatureCell rowCell = (MRowFeatureCell) classToCell;
							if ((!allUsedProperties.contains(rowCell.getFeature())) && rowCell.getFeature() != null
									&& rowCell.getFeature().getPropertyBehavior() != PropertyBehavior.CONTAINED) {
								allUsedProperties.add(rowCell.getFeature());
							}
						}
					}
				}

				for (MProperty mprop : allUsedProperties) {
					allUsableProperties.remove(mprop);
				}
				if (!allUsableProperties.isEmpty()) {
					currentTrigger.addReferenceUpdate(newRowCell,
							MtableeditorPackage.Literals.MROW_FEATURE_CELL__FEATURE, XUpdateMode.REDEFINE, null,
							allUsableProperties.get(0), null, null);
				}
			}

			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newRowCell, null, null);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newRowCell));
			return currentTrigger;
		}

		case MColumnConfigAction.ADD_MOCL_CELL_VALUE: {
			EEnumLiteral triggerLiteral = MtableeditorPackage.eINSTANCE.getMColumnConfigAction()
					.getEEnumLiteral(MColumnConfigAction.ADD_MOCL_CELL_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					MtableeditorPackage.eINSTANCE.getMColumnConfig_DoAction(), XUpdateMode.REDEFINE, null,
					triggerLiteral, null, null);

			MClassToCellConfig newClassCell = (MClassToCellConfig) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMClassToCellConfig());

			MOclCell newOclCell = (MOclCell) currentTrigger
					.createNextStateNewObject(MtableeditorPackage.eINSTANCE.getMOclCell());

			newClassCell.setClass(this.getContainingTableConfig().getIntendedClass());
			newClassCell.setCellConfig(newOclCell);
			newOclCell.setClass(this.getContainingTableConfig().getIntendedClass());

			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newOclCell, null, null);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newOclCell));
			return currentTrigger;
		}

		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getClassToCellConfig()).basicAdd(otherEnd,
					msgs);
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingTableConfig((MTableConfig) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			return ((InternalEList<?>) getClassToCellConfig()).basicRemove(otherEnd, msgs);
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			return basicSetContainingTableConfig(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			return eInternalContainer().eInverseRemove(this, MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG,
					MTableConfig.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
			return getBoldFont();
		case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
			return getItalicFont();
		case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
			return getFontSize();
		case MtableeditorPackage.MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
			return getDerivedFontOptionsEncoding();
		case MtableeditorPackage.MCOLUMN_CONFIG__NAME:
			return getName();
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			return getClassToCellConfig();
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_NAME:
			return getECName();
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_LABEL:
			return getECLabel();
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_WIDTH:
			return getECWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__LABEL:
			return getLabel();
		case MtableeditorPackage.MCOLUMN_CONFIG__WIDTH:
			return getWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH:
			return getMinimumWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH:
			return getMaximumWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			if (resolve)
				return getContainingTableConfig();
			return basicGetContainingTableConfig();
		case MtableeditorPackage.MCOLUMN_CONFIG__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
			setBoldFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
			setItalicFont((Boolean) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
			setFontSize((MFontSizeAdaptor) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__NAME:
			setName((String) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			getClassToCellConfig().clear();
			getClassToCellConfig().addAll((Collection<? extends MClassToCellConfig>) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__LABEL:
			setLabel((String) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__WIDTH:
			setWidth((Integer) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH:
			setMinimumWidth((Integer) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH:
			setMaximumWidth((Integer) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			setContainingTableConfig((MTableConfig) newValue);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__DO_ACTION:
			setDoAction((MColumnConfigAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
			unsetBoldFont();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
			unsetItalicFont();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
			unsetFontSize();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__NAME:
			unsetName();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			unsetClassToCellConfig();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__LABEL:
			unsetLabel();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__WIDTH:
			unsetWidth();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH:
			unsetMinimumWidth();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH:
			unsetMaximumWidth();
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			setContainingTableConfig((MTableConfig) null);
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
			return isSetBoldFont();
		case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
			return isSetItalicFont();
		case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
			return isSetFontSize();
		case MtableeditorPackage.MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
			return DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT == null ? getDerivedFontOptionsEncoding() != null
					: !DERIVED_FONT_OPTIONS_ENCODING_EDEFAULT.equals(getDerivedFontOptionsEncoding());
		case MtableeditorPackage.MCOLUMN_CONFIG__NAME:
			return isSetName();
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			return isSetClassToCellConfig();
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_NAME:
			return EC_NAME_EDEFAULT == null ? getECName() != null : !EC_NAME_EDEFAULT.equals(getECName());
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_LABEL:
			return EC_LABEL_EDEFAULT == null ? getECLabel() != null : !EC_LABEL_EDEFAULT.equals(getECLabel());
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_WIDTH:
			return EC_WIDTH_EDEFAULT == null ? getECWidth() != null : !EC_WIDTH_EDEFAULT.equals(getECWidth());
		case MtableeditorPackage.MCOLUMN_CONFIG__LABEL:
			return isSetLabel();
		case MtableeditorPackage.MCOLUMN_CONFIG__WIDTH:
			return isSetWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH:
			return isSetMinimumWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH:
			return isSetMaximumWidth();
		case MtableeditorPackage.MCOLUMN_CONFIG__CONTAINING_TABLE_CONFIG:
			return basicGetContainingTableConfig() != null;
		case MtableeditorPackage.MCOLUMN_CONFIG__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == MElementWithFont.class) {
			switch (derivedFeatureID) {
			case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
				return MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT;
			case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
				return MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT;
			case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
				return MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE;
			case MtableeditorPackage.MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
				return MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == MElementWithFont.class) {
			switch (baseFeatureID) {
			case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
				return MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT;
			case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
				return MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT;
			case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
				return MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE;
			case MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING:
				return MtableeditorPackage.MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case MtableeditorPackage.MCOLUMN_CONFIG___DO_ACTION$_UPDATE__MCOLUMNCONFIGACTION:
			return doAction$Update((MColumnConfigAction) arguments.get(0));
		case MtableeditorPackage.MCOLUMN_CONFIG___CELL_CONFIG_FROM_CLASS__MCLASSIFIER:
			return cellConfigFromClass((MClassifier) arguments.get(0));
		case MtableeditorPackage.MCOLUMN_CONFIG___DO_ACTION_UPDATE__MCOLUMNCONFIGACTION:
			return doActionUpdate((MColumnConfigAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (boldFont: ");
		if (boldFontESet)
			result.append(boldFont);
		else
			result.append("<unset>");
		result.append(", italicFont: ");
		if (italicFontESet)
			result.append(italicFont);
		else
			result.append("<unset>");
		result.append(", fontSize: ");
		if (fontSizeESet)
			result.append(fontSize);
		else
			result.append("<unset>");
		result.append(", name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", label: ");
		if (labelESet)
			result.append(label);
		else
			result.append("<unset>");
		result.append(", width: ");
		if (widthESet)
			result.append(width);
		else
			result.append("<unset>");
		result.append(", minimumWidth: ");
		if (minimumWidthESet)
			result.append(minimumWidth);
		else
			result.append("<unset>");
		result.append(", maximumWidth: ");
		if (maximumWidthESet)
			result.append(maximumWidth);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL aKindBase let const1: String = 'Column' in
	const1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAKindBase() {
		EClass eClass = (MtableeditorPackage.Literals.MCOLUMN_CONFIG);
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override

	public NotificationChain eBasicSetContainer(InternalEObject newContainer, int newContainerFeatureID,
			NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__FONT_SIZE };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures
					.toArray(new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder",
					"InitOrder", true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures, new Comparator<EStructuralFeature>() {
				public int compare(EStructuralFeature structuralFeature1, EStructuralFeature structuralFeature2) {
					Object comparedObject1 = initOrderMap.get(structuralFeature1);
					Object comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return XoclMutlitypeComparisonUtil.compare(comparedObject1, comparedObject2);
				}
			});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue",
					"InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MCOLUMN_CONFIG,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this, new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap, String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature, eClass(), annotationKey,
				annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, oclText, helper.getProblems(),
						eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MColumnConfigImpl
