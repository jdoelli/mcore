/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;

import com.montages.mtableeditor.MClassToTableConfig;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MtableeditorPackage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MClass To Table Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MClassToTableConfigImpl#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MClassToTableConfigImpl#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MClassToTableConfigImpl#getIntendedPackage <em>Intended Package</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MClassToTableConfigImpl extends MinimalEObjectImpl.Container implements MClassToTableConfig {

	/**
	 * The cached value of the '{@link #getClass_() <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClass_()
	 * @generated
	 * @ordered
	 */
	protected MClassifier class_;

	/**
	 * This is true if the Class reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean classESet;

	/**
	 * The cached value of the '{@link #getTableConfig() <em>Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTableConfig()
	 * @generated
	 * @ordered
	 */
	protected MTableConfig tableConfig;

	/**
	 * This is true if the Table Config reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean tableConfigESet;

	/**
	 * The cached value of the '{@link #getIntendedPackage() <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIntendedPackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage intendedPackage;

	/**
	 * This is true if the Intended Package reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean intendedPackageESet;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getClass_ <em>Class</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClass_
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression class_ChoiceConstraintOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MClassToTableConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getClass_() {
		if (class_ != null && class_.eIsProxy()) {
			InternalEObject oldClass = (InternalEObject) class_;
			class_ = (MClassifier) eResolveProxy(oldClass);
			if (class_ != oldClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS, oldClass, class_));
			}
		}
		return class_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetClass() {
		return class_;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setClass(MClassifier newClass) {
		MClassifier oldClass = class_;
		class_ = newClass;
		boolean oldClassESet = classESet;
		classESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS,
					oldClass, class_, !oldClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClass() {
		MClassifier oldClass = class_;
		boolean oldClassESet = classESet;
		class_ = null;
		classESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS,
					oldClass, null, oldClassESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClass() {
		return classESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Class</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let var1: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
	if e1.oclIsInvalid() then null else e1 endif in
	var1
	
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalClassChoiceConstraint(MClassifier trg) {
		EClass eClass = MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG;
		if (class_ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG__CLASS;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				class_ChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass, "ClassChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(class_ChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "ClassChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig getTableConfig() {
		if (tableConfig != null && tableConfig.eIsProxy()) {
			InternalEObject oldTableConfig = (InternalEObject) tableConfig;
			tableConfig = (MTableConfig) eResolveProxy(oldTableConfig);
			if (tableConfig != oldTableConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, tableConfig));
			}
		}
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig basicGetTableConfig() {
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTableConfig(MTableConfig newTableConfig) {
		MTableConfig oldTableConfig = tableConfig;
		tableConfig = newTableConfig;
		boolean oldTableConfigESet = tableConfigESet;
		tableConfigESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, tableConfig,
					!oldTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTableConfig() {
		MTableConfig oldTableConfig = tableConfig;
		boolean oldTableConfigESet = tableConfigESet;
		tableConfig = null;
		tableConfigESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG, oldTableConfig, null,
					oldTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTableConfig() {
		return tableConfigESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getIntendedPackage() {
		if (intendedPackage != null && intendedPackage.eIsProxy()) {
			InternalEObject oldIntendedPackage = (InternalEObject) intendedPackage;
			intendedPackage = (MPackage) eResolveProxy(oldIntendedPackage);
			if (intendedPackage != oldIntendedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE, oldIntendedPackage,
							intendedPackage));
			}
		}
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetIntendedPackage() {
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntendedPackage(MPackage newIntendedPackage) {
		MPackage oldIntendedPackage = intendedPackage;
		intendedPackage = newIntendedPackage;
		boolean oldIntendedPackageESet = intendedPackageESet;
		intendedPackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE, oldIntendedPackage, intendedPackage,
					!oldIntendedPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetIntendedPackage() {
		MPackage oldIntendedPackage = intendedPackage;
		boolean oldIntendedPackageESet = intendedPackageESet;
		intendedPackage = null;
		intendedPackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE, oldIntendedPackage, null,
					oldIntendedPackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetIntendedPackage() {
		return intendedPackageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS:
			if (resolve)
				return getClass_();
			return basicGetClass();
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG:
			if (resolve)
				return getTableConfig();
			return basicGetTableConfig();
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE:
			if (resolve)
				return getIntendedPackage();
			return basicGetIntendedPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS:
			setClass((MClassifier) newValue);
			return;
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG:
			setTableConfig((MTableConfig) newValue);
			return;
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE:
			setIntendedPackage((MPackage) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS:
			unsetClass();
			return;
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG:
			unsetTableConfig();
			return;
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE:
			unsetIntendedPackage();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__CLASS:
			return isSetClass();
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG:
			return isSetTableConfig();
		case MtableeditorPackage.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE:
			return isSetIntendedPackage();
		}
		return super.eIsSet(featureID);
	}

} // MClassToTableConfigImpl
