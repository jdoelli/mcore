/**
 */
package com.montages.mtableeditor.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.impl.MEditorImpl;
import com.montages.mtableeditor.MClassToTableConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MEditorConfigAction;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MTableConfigAction;
import com.montages.mtableeditor.MTableEditorElement;
import com.montages.mtableeditor.MtableeditorPackage;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.langlets.autil.AElement;
import org.langlets.autil.AutilPackage;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MEditor Config</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getHeaderTableConfig <em>Header Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getMTableConfig <em>MTable Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getMColumnConfig <em>MColumn Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getClassToTableConfig <em>Class To Table Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getSuperConfig <em>Super Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.impl.MEditorConfigImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MEditorConfigImpl extends MEditorImpl implements MEditorConfig {

	private static final XUpdateMode REDEFINE = XUpdateMode.REDEFINE;

	private static final MtableeditorPackage MTE_PACKAGE = MtableeditorPackage.eINSTANCE;

	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final String KIND_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHeaderTableConfig() <em>Header Table Config</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getHeaderTableConfig()
	 * @generated
	 * @ordered
	 */
	protected MTableConfig headerTableConfig;

	/**
	 * This is true if the Header Table Config reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean headerTableConfigESet;

	/**
	 * The cached value of the '{@link #getMTableConfig() <em>MTable Config</em>
	 * }' containment reference list. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getMTableConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MTableConfig> mTableConfig;

	/**
	 * The cached value of the '{@link #getMColumnConfig() <em>MColumn Config</em>}' containment reference list.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getMColumnConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MColumnConfig> mColumnConfig;

	/**
	 * The cached value of the '{@link #getClassToTableConfig()
	 * <em>Class To Table Config</em>}' containment reference list. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getClassToTableConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<MClassToTableConfig> classToTableConfig;

	/**
	 * The cached value of the '{@link #getSuperConfig() <em>Super Config</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSuperConfig()
	 * @generated
	 * @ordered
	 */
	protected MEditorConfig superConfig;

	/**
	 * This is true if the Super Config reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean superConfigESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MEditorConfigAction DO_ACTION_EDEFAULT = MEditorConfigAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemtableeditorMEditorConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #tableConfigFromClass <em>Table Config From Class</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #tableConfigFromClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression tableConfigFromClassmcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemtableeditorMEditorConfigActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #resetEditor <em>Reset Editor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #resetEditor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression resetEditorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentLevel <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #indentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringOrMissing <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #stringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringIsEmpty <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #stringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '
	 * {@link #listOfStringToStringWithSeparator
	 * <em>List Of String To String With Separator</em>}' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '
	 * {@link #listOfStringToStringWithSeparator
	 * <em>List Of String To String With Separator</em>}' operation. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKind <em>Kind</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression kindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase
	 * <em>AKind Base</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getAKindBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MEditorConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MtableeditorPackage.Literals.MEDITOR_CONFIG;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		EStructuralFeature eFeature = AutilPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		EStructuralFeature eOverrideFeature = AutilPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let renderedKind: String = let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif in
		renderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		EStructuralFeature eFeature = AutilPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getKind() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MTABLE_EDITOR_ELEMENT__KIND;

		if (kindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				kindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig getHeaderTableConfig() {
		if (headerTableConfig != null && headerTableConfig.eIsProxy()) {
			InternalEObject oldHeaderTableConfig = (InternalEObject) headerTableConfig;
			headerTableConfig = (MTableConfig) eResolveProxy(oldHeaderTableConfig);
			if (headerTableConfig != oldHeaderTableConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG, oldHeaderTableConfig,
							headerTableConfig));
			}
		}
		return headerTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig basicGetHeaderTableConfig() {
		return headerTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderTableConfig(MTableConfig newHeaderTableConfig) {
		MTableConfig oldHeaderTableConfig = headerTableConfig;
		headerTableConfig = newHeaderTableConfig;
		boolean oldHeaderTableConfigESet = headerTableConfigESet;
		headerTableConfigESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG, oldHeaderTableConfig, headerTableConfig,
					!oldHeaderTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHeaderTableConfig() {
		MTableConfig oldHeaderTableConfig = headerTableConfig;
		boolean oldHeaderTableConfigESet = headerTableConfigESet;
		headerTableConfig = null;
		headerTableConfigESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG, oldHeaderTableConfig, null,
					oldHeaderTableConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHeaderTableConfig() {
		return headerTableConfigESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTableConfig> getMTableConfig() {
		if (mTableConfig == null) {
			mTableConfig = new EObjectContainmentWithInverseEList.Unsettable.Resolving<MTableConfig>(MTableConfig.class,
					this, MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG,
					MtableeditorPackage.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG);
		}
		return mTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMTableConfig() {
		if (mTableConfig != null)
			((InternalEList.Unsettable<?>) mTableConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMTableConfig() {
		return mTableConfig != null && ((InternalEList.Unsettable<?>) mTableConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MColumnConfig> getMColumnConfig() {
		if (mColumnConfig == null) {
			mColumnConfig = new EObjectContainmentEList.Unsettable.Resolving<MColumnConfig>(MColumnConfig.class, this,
					MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG);
		}
		return mColumnConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMColumnConfig() {
		if (mColumnConfig != null)
			((InternalEList.Unsettable<?>) mColumnConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMColumnConfig() {
		return mColumnConfig != null && ((InternalEList.Unsettable<?>) mColumnConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MClassToTableConfig> getClassToTableConfig() {
		if (classToTableConfig == null) {
			classToTableConfig = new EObjectContainmentEList.Unsettable.Resolving<MClassToTableConfig>(
					MClassToTableConfig.class, this, MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG);
		}
		return classToTableConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetClassToTableConfig() {
		if (classToTableConfig != null)
			((InternalEList.Unsettable<?>) classToTableConfig).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetClassToTableConfig() {
		return classToTableConfig != null && ((InternalEList.Unsettable<?>) classToTableConfig).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfig getSuperConfig() {
		if (superConfig != null && superConfig.eIsProxy()) {
			InternalEObject oldSuperConfig = (InternalEObject) superConfig;
			superConfig = (MEditorConfig) eResolveProxy(oldSuperConfig);
			if (superConfig != oldSuperConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG, oldSuperConfig, superConfig));
			}
		}
		return superConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfig basicGetSuperConfig() {
		return superConfig;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperConfig(MEditorConfig newSuperConfig) {
		MEditorConfig oldSuperConfig = superConfig;
		superConfig = newSuperConfig;
		boolean oldSuperConfigESet = superConfigESet;
		superConfigESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG,
					oldSuperConfig, superConfig, !oldSuperConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSuperConfig() {
		MEditorConfig oldSuperConfig = superConfig;
		boolean oldSuperConfigESet = superConfigESet;
		superConfig = null;
		superConfigESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG,
					oldSuperConfig, null, oldSuperConfigESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSuperConfig() {
		return superConfigESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MEditorConfigAction getDoAction() {
		/**
		 * @OCL mtableeditor::MEditorConfigAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		EStructuralFeature eFeature = MtableeditorPackage.Literals.MEDITOR_CONFIG__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, derive, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MEditorConfigAction result = (MEditorConfigAction) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoAction(MEditorConfigAction newDoAction) {
		if (newDoAction == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MEditorConfigAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE.createXTransition();
		com.montages.mtableeditor.MEditorConfigAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				MtableeditorPackage.eINSTANCE.getMEditorConfig_DoAction(), org.xocl.semantics.XUpdateMode.REDEFINE,
				null, triggerValue, null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTableConfig tableConfigFromClass(MClassifier mClass) {

		/**
		 * @OCL let applicableClassToTableConfig: OrderedSet(mtableeditor::MClassToTableConfig)  = classToTableConfig->asOrderedSet()->select(it: mtableeditor::MClassToTableConfig | let e0: Boolean = it.class = mClass in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let chain: OrderedSet(mtableeditor::MTableConfig)  = applicableClassToTableConfig.tableConfig->asOrderedSet() in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (MtableeditorPackage.Literals.MEDITOR_CONFIG);
		EOperation eOperation = MtableeditorPackage.Literals.MEDITOR_CONFIG.getEOperations().get(1);
		if (tableConfigFromClassmcoreMClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				tableConfigFromClassmcoreMClassifierBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(tableConfigFromClassmcoreMClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("mClass", mClass);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MTableConfig) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MEditorConfigAction mEditorConfigAction) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		switch (mEditorConfigAction.getValue()) {
		case MEditorConfigAction.ADD_TABLE_VALUE: {
			int nrOfTables = getMTableConfig().size() + 1;
			String name = "Table ".concat(Integer.toString(nrOfTables)); // default
																			// name
			String label = name;
			MClassifier intendentClass = null;
			MPackage intendedPackage = null;

			EEnumLiteral triggerLiteral = MTE_PACKAGE.getMEditorConfigAction()
					.getEEnumLiteral(MEditorConfigAction.ADD_TABLE_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this, MTE_PACKAGE.getMEditorConfig_DoAction(),
					REDEFINE, null, triggerLiteral, null, null);

			MTableConfig newTable = (MTableConfig) currentTrigger
					.createNextStateNewObject(MTE_PACKAGE.getMTableConfig());

			if (this.getHeaderTableConfig() != null && (this.getHeaderTableConfig().getIntendedPackage() != null
					|| this.getHeaderTableConfig().getIntendedClass() != null)) {
				MPackage mPack = getMPackage();

				// creates an EList
				Set<MClassifier> allUsableClassesSet = new HashSet<MClassifier>();
				// goes through all packages
				for (MPackage findRoot : mPack.getContainingComponent().getOwnedPackage()) {
					for (MClassifier mClass : findRoot.getClassifier()) {
						if (mClass.getNrOfPropertiesAndSignatures() > 0)
							allUsableClassesSet.add(mClass);
					}
					for (MPackage pack : findRoot.getAllSubpackages()) {
						for (MClassifier mClass : pack.getClassifier()) {
							if (mClass.getNrOfPropertiesAndSignatures() > 0)
								allUsableClassesSet.add(mClass);
						}
					}
				}

				List<MClassifier> allUsedClasses = new ArrayList<MClassifier>();

				for (MTableConfig mTab : this.getMTableConfig()) {
					if (mTab.isSetIntendedClass()) {
						allUsedClasses.add(mTab.getIntendedClass());
					}
				}

				allUsableClassesSet.removeAll(allUsedClasses);
				if (!allUsableClassesSet.isEmpty()) {
					MClassifier mClassifier = allUsableClassesSet.iterator().next();
					name = mClassifier.getName();
					label = name;
					intendentClass = mClassifier;
					intendedPackage = mClassifier.getContainingPackage();
				}
			}

			else {

				currentTrigger.addReferenceUpdate(this,
						MtableeditorPackage.Literals.MEDITOR_CONFIG__HEADER_TABLE_CONFIG, REDEFINE, null, newTable,
						null, null);
			}

			newTable.setName(name);
			newTable.setLabel(label);
			newTable.setDisplayHeader(Boolean.TRUE);
			newTable.setDisplayDefaultColumn(Boolean.FALSE);
			newTable.setIntendedClass(intendentClass);
			newTable.setIntendedPackage(intendedPackage);

			transition.getFocusObjects().add(transition.nextStateObjectDefinitionFromObject(newTable));
			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newTable, null, null);

			return currentTrigger;
		}
		case MEditorConfigAction.RESET_EDITOR_VALUE: {
			MPackage mPack = McoreFactory.eINSTANCE.createMPackage();
			if (!this.isSetHeaderTableConfig() || (!(this.getHeaderTableConfig().getIntendedClass() == null)
					|| !(this.getHeaderTableConfig().getIntendedPackage() == null))) {
				for (MTableConfig mTab : getMTableConfig()) {
					if (mTab.isSetIntendedClass()) {
						mPack = mTab.getIntendedClass().getContainingPackage().getContainingComponent()
								.getRootPackage();
						break;
					} else {
						if (mTab.isSetIntendedPackage()) {
							mPack = mTab.getIntendedPackage().getContainingComponent().getRootPackage();
							break;
						}
					}
				}
			} else {
				mPack = getMPackage();
			}

			EEnumLiteral triggerLiteral = MTE_PACKAGE.getMEditorConfigAction()
					.getEEnumLiteral(MEditorConfigAction.ADD_TABLE_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this, MTE_PACKAGE.getMEditorConfig_DoAction(),
					REDEFINE, null, triggerLiteral, null, null);

			ArrayList<MTableConfig> oldTables = new ArrayList<MTableConfig>(getMTableConfig());
			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG,
					XUpdateMode.REMOVE, null, oldTables, null, null);

			MTableConfig newTable = (MTableConfig) currentTrigger
					.createNextStateNewObject(MTE_PACKAGE.getMTableConfig());
			// MTableConfig editor isn't in the a resource so we can edit it
			// directly, without any commands
			newTable.setDisplayHeader(Boolean.TRUE);
			newTable.setDisplayDefaultColumn(Boolean.FALSE);
			if (mPack != null) {
				String name = "";
				if (mPack.getResourceRootClass() != null) {
					name = mPack.getResourceRootClass().getName();
					newTable.setIntendedClass(mPack.getResourceRootClass());
					newTable.setIntendedPackage(mPack.getResourceRootClass().getContainingPackage());
				} else {
					if (!mPack.getClassifier().isEmpty()) {
						System.out.println(mPack.getName());
						System.out.println(mPack.getClassifier().get(0).getName());
						name = mPack.getClassifier().get(0).getName();
						newTable.setIntendedClass(mPack.getClassifier().get(0));
						newTable.setIntendedPackage(mPack.getClassifier().get(0).getContainingPackage());
					}
				}
				newTable.setName(name);
				newTable.setLabel(name);
			}

			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG,
					XUpdateMode.ADD, XAddUpdateMode.LAST, newTable, null, null);
			currentTrigger.addReferenceUpdate(this, MtableeditorPackage.Literals.MEDITOR_CONFIG__HEADER_TABLE_CONFIG,
					REDEFINE, null, newTable, null, null);
			currentTrigger.addReferenceUpdate(newTable,
					MtableeditorPackage.Literals.MTABLE_CONFIG__CONTAINING_EDITOR_CONFIG, REDEFINE, null, this, null,
					null);

			List<MTableConfig> complementTable = new ArrayList<MTableConfig>();
			complementTable.add(newTable);
			currentTrigger = newTable.doActionUpdate(currentTrigger, this, MTableConfigAction.COMPLEMENT_TABLE,
					complementTable, true);
			return currentTrigger;
		}

		}

		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean resetEditor() {
	    XUpdate update = this.doActionUpdate(MEditorConfigAction.RESET_EDITOR);
	    this.unsetMColumnConfig();
	    this.unsetMTableConfig();
	    update.getContainingTransition().interpreteTransitionAsCommand().execute();  
	    this.getMTableConfig().move(0, getHeaderTableConfig());
	     return true;
	}

	/**
	 * 
	 * @return {@link MPackage} of a header table's intended package or a
	 *         containing package of a table's intended class. if both values
	 *         are <code>null</code>, return <code>null</code>
	 */
	private MPackage getMPackage() {
		MTableConfig tableConfig = this.getHeaderTableConfig();
		MPackage intendedPackage = tableConfig.getIntendedPackage();
		return intendedPackage != null ? intendedPackage : tableConfig.getIntendedClass().getContainingPackage();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Integer indentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).indentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(0);
		if (indentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		let result: String = indentationSpaces(numberOfSpaces) in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(1);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer =  size - 1 in
		let result: String = if (let e0: Boolean = size < 1 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then ''
		else (let e0: String = indentationSpaces(sizeMinusOne).concat(' ') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(2);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String stringOrMissing(String p) {

		/**
		 * @OCL let result: String = if (stringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(3);
		if (stringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringIsEmpty(String s) {

		/**
		 * @OCL let result: Boolean = if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif in
		result
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(4);
		if (stringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements, String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(5);
		if (listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		listOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AutilPackage.Literals.AELEMENT);
		EOperation eOperation = AutilPackage.Literals.AELEMENT.getEOperations().get(6);
		if (listOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, body, helper.getProblems(),
						MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query,
					MtableeditorPackage.Literals.MEDITOR_CONFIG, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getMTableConfig()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			return ((InternalEList<?>) getMTableConfig()).basicRemove(otherEnd, msgs);
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
			return ((InternalEList<?>) getMColumnConfig()).basicRemove(otherEnd, msgs);
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			return ((InternalEList<?>) getClassToTableConfig()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__ALABEL:
			return getALabel();
		case MtableeditorPackage.MEDITOR_CONFIG__AKIND_BASE:
			return getAKindBase();
		case MtableeditorPackage.MEDITOR_CONFIG__ARENDERED_KIND:
			return getARenderedKind();
		case MtableeditorPackage.MEDITOR_CONFIG__KIND:
			return getKind();
		case MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG:
			if (resolve)
				return getHeaderTableConfig();
			return basicGetHeaderTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			return getMTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
			return getMColumnConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			return getClassToTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG:
			if (resolve)
				return getSuperConfig();
			return basicGetSuperConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG:
			setHeaderTableConfig((MTableConfig) newValue);
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			getMTableConfig().clear();
			getMTableConfig().addAll((Collection<? extends MTableConfig>) newValue);
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
			getMColumnConfig().clear();
			getMColumnConfig().addAll((Collection<? extends MColumnConfig>) newValue);
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			getClassToTableConfig().clear();
			getClassToTableConfig().addAll((Collection<? extends MClassToTableConfig>) newValue);
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG:
			setSuperConfig((MEditorConfig) newValue);
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__DO_ACTION:
			setDoAction((MEditorConfigAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG:
			unsetHeaderTableConfig();
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			unsetMTableConfig();
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
			unsetMColumnConfig();
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			unsetClassToTableConfig();
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG:
			unsetSuperConfig();
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case MtableeditorPackage.MEDITOR_CONFIG__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null : !ALABEL_EDEFAULT.equals(getALabel());
		case MtableeditorPackage.MEDITOR_CONFIG__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null : !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case MtableeditorPackage.MEDITOR_CONFIG__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		case MtableeditorPackage.MEDITOR_CONFIG__KIND:
			return KIND_EDEFAULT == null ? getKind() != null : !KIND_EDEFAULT.equals(getKind());
		case MtableeditorPackage.MEDITOR_CONFIG__HEADER_TABLE_CONFIG:
			return isSetHeaderTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
			return isSetMTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
			return isSetMColumnConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			return isSetClassToTableConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__SUPER_CONFIG:
			return isSetSuperConfig();
		case MtableeditorPackage.MEDITOR_CONFIG__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (derivedFeatureID) {
			case MtableeditorPackage.MEDITOR_CONFIG__ALABEL:
				return AutilPackage.AELEMENT__ALABEL;
			case MtableeditorPackage.MEDITOR_CONFIG__AKIND_BASE:
				return AutilPackage.AELEMENT__AKIND_BASE;
			case MtableeditorPackage.MEDITOR_CONFIG__ARENDERED_KIND:
				return AutilPackage.AELEMENT__ARENDERED_KIND;
			default:
				return -1;
			}
		}
		if (baseClass == MTableEditorElement.class) {
			switch (derivedFeatureID) {
			case MtableeditorPackage.MEDITOR_CONFIG__KIND:
				return MtableeditorPackage.MTABLE_EDITOR_ELEMENT__KIND;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseFeatureID) {
			case AutilPackage.AELEMENT__ALABEL:
				return MtableeditorPackage.MEDITOR_CONFIG__ALABEL;
			case AutilPackage.AELEMENT__AKIND_BASE:
				return MtableeditorPackage.MEDITOR_CONFIG__AKIND_BASE;
			case AutilPackage.AELEMENT__ARENDERED_KIND:
				return MtableeditorPackage.MEDITOR_CONFIG__ARENDERED_KIND;
			default:
				return -1;
			}
		}
		if (baseClass == MTableEditorElement.class) {
			switch (baseFeatureID) {
			case MtableeditorPackage.MTABLE_EDITOR_ELEMENT__KIND:
				return MtableeditorPackage.MEDITOR_CONFIG__KIND;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseOperationID) {
			case AutilPackage.AELEMENT___INDENT_LEVEL:
				return MtableeditorPackage.MEDITOR_CONFIG___INDENT_LEVEL;
			case AutilPackage.AELEMENT___INDENTATION_SPACES:
				return MtableeditorPackage.MEDITOR_CONFIG___INDENTATION_SPACES;
			case AutilPackage.AELEMENT___INDENTATION_SPACES__INTEGER:
				return MtableeditorPackage.MEDITOR_CONFIG___INDENTATION_SPACES__INTEGER;
			case AutilPackage.AELEMENT___STRING_OR_MISSING__STRING:
				return MtableeditorPackage.MEDITOR_CONFIG___STRING_OR_MISSING__STRING;
			case AutilPackage.AELEMENT___STRING_IS_EMPTY__STRING:
				return MtableeditorPackage.MEDITOR_CONFIG___STRING_IS_EMPTY__STRING;
			case AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
				return MtableeditorPackage.MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;
			case AutilPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
				return MtableeditorPackage.MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;
			default:
				return -1;
			}
		}
		if (baseClass == MTableEditorElement.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case MtableeditorPackage.MEDITOR_CONFIG___DO_ACTION$_UPDATE__MEDITORCONFIGACTION:
			return doAction$Update((MEditorConfigAction) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___TABLE_CONFIG_FROM_CLASS__MCLASSIFIER:
			return tableConfigFromClass((MClassifier) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___DO_ACTION_UPDATE__MEDITORCONFIGACTION:
			return doActionUpdate((MEditorConfigAction) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___INDENT_LEVEL:
			return indentLevel();
		case MtableeditorPackage.MEDITOR_CONFIG___INDENTATION_SPACES:
			return indentationSpaces();
		case MtableeditorPackage.MEDITOR_CONFIG___INDENTATION_SPACES__INTEGER:
			return indentationSpaces((Integer) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___STRING_OR_MISSING__STRING:
			return stringOrMissing((String) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___STRING_IS_EMPTY__STRING:
			return stringIsEmpty((String) arguments.get(0));
		case MtableeditorPackage.MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0), (String) arguments.get(1));
		case MtableeditorPackage.MEDITOR_CONFIG___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL let fileType: String = 'mtableeditor' in let fileName: String = let
	 *      subchain1 : mtableeditor::MClassToCellConfig = let chain:
	 *      OrderedSet(mtableeditor::MClassToCellConfig) = if
	 *      headerTableConfig.oclIsUndefined() then OrderedSet{} else
	 *      headerTableConfig.columnConfig.classToCellConfig->asOrderedSet()
	 *      endif in if chain->first().oclIsUndefined() then null else
	 *      chain->first() endif in if subchain1 = null then null else if
	 *      subchain1.class.containingPackage.oclIsUndefined() then null else
	 *      subchain1.class.containingPackage.eName endif endif in let label:
	 *      String = let e1: String = fileName.concat('.').concat(fileType) in
	 *      if e1.oclIsInvalid() then null else e1 endif in label
	 * 
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = MtableeditorPackage.Literals.MEDITOR_CONFIG;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(MtableeditorPackage.PLUGIN_ID, label, helper.getProblems(), eClass,
						"label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(MtableeditorPackage.PLUGIN_ID, query, eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

} // MEditorConfigImpl
