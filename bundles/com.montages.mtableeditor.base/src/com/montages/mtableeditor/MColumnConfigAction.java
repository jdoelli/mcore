/**
 */
package com.montages.mtableeditor;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc --> A representation of the literals of the enumeration '
 * <em><b>MColumn Config Action</b></em>', and utility methods for working with
 * them. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage#getMColumnConfigAction()
 * @model
 * @generated
 */
public enum MColumnConfigAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Add MRow Feature Cell</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ADD_MROW_FEATURE_CELL_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_MROW_FEATURE_CELL(1, "AddMRowFeatureCell", "+ Cell defined by Property"),

	/**
	 * The '<em><b>Add MOcl Cell</b></em>' literal object.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #ADD_MOCL_CELL_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_MOCL_CELL(2, "AddMOclCell", "+ Cell defined by \'Text\'/Ocl"),

	/**
	 * The '<em><b>Add MEdit Provider Cell</b></em>' literal object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #ADD_MEDIT_PROVIDER_CELL_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_MEDIT_PROVIDER_CELL(3, "AddMEditProviderCell", "+ Edit Provider Cell");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Add MRow Feature Cell</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add MRow Feature Cell</b></em>' literal object
	 * isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ADD_MROW_FEATURE_CELL
	 * @model name="AddMRowFeatureCell" literal="+ Cell defined by Property"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_MROW_FEATURE_CELL_VALUE = 1;

	/**
	 * The '<em><b>Add MOcl Cell</b></em>' literal value.
	 * <!-- begin-user-doc
	 * -->
	 * <p>
	 * If the meaning of '<em><b>Add MOcl Cell</b></em>' literal object isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_MOCL_CELL
	 * @model name="AddMOclCell" literal="+ Cell defined by \'Text\'/Ocl"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_MOCL_CELL_VALUE = 2;

	/**
	 * The '<em><b>Add MEdit Provider Cell</b></em>' literal value. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add MEdit Provider Cell</b></em>' literal
	 * object isn't clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @see #ADD_MEDIT_PROVIDER_CELL
	 * @model name="AddMEditProviderCell" literal="+ Edit Provider Cell"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_MEDIT_PROVIDER_CELL_VALUE = 3;

	/**
	 * An array of all the '<em><b>MColumn Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private static final MColumnConfigAction[] VALUES_ARRAY = new MColumnConfigAction[] { DO, ADD_MROW_FEATURE_CELL,
			ADD_MOCL_CELL, ADD_MEDIT_PROVIDER_CELL, };

	/**
	 * A public read-only list of all the '<em><b>MColumn Config Action</b></em>' enumerators.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MColumnConfigAction> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MColumn Config Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MColumnConfigAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MColumnConfigAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MColumn Config Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MColumnConfigAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MColumnConfigAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MColumn Config Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public static MColumnConfigAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case ADD_MROW_FEATURE_CELL_VALUE:
			return ADD_MROW_FEATURE_CELL;
		case ADD_MOCL_CELL_VALUE:
			return ADD_MOCL_CELL;
		case ADD_MEDIT_PROVIDER_CELL_VALUE:
			return ADD_MEDIT_PROVIDER_CELL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	private MColumnConfigAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} // MColumnConfigAction
