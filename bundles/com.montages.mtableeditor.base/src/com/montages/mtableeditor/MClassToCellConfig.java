/**
 */
package com.montages.mtableeditor;

import com.montages.mcore.MClassifier;

/**
 * <!-- begin-user-doc --> A representation of the model object '
 * <em><b>MClass To Cell Config</b></em>'. <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mtableeditor.MClassToCellConfig#getClass_ <em>Class</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig <em>Containing Column Config</em>}</li>
 *   <li>{@link com.montages.mtableeditor.MClassToCellConfig#getCellKind <em>Cell Kind</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToCellConfig()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL aKindBaseDerive='let const1: String = \'Class -> ...\' in\nconst1\n'"
 * @generated
 */

public interface MClassToCellConfig extends MTableEditorElement {

	/**
	 * Returns the value of the '<em><b>Class</b></em>' reference. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class</em>' reference isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Class</em>' reference.
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #setClass(MClassifier)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToCellConfig_Class()
	 * @model unsettable="true" required="true" annotation=
	 *        "http://www.xocl.org/OCL initValue='if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedClass\nendif\n' choiceConstraint='let rightPackage: Boolean = if (let chain: mcore::MPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedPackage\nendif in\nif chain <> null then true else false \n  endif) \n  =true \nthen (let e0: Boolean = trg.containingPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()\n  then null\n  else containingColumnConfig.containingTableConfig.intendedPackage\nendif in \n if e0.oclIsInvalid() then null else e0 endif)\n  else true\nendif in\nlet isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in \n if e1.oclIsInvalid() then null else e1 endif in\nlet intended: Boolean = let e1: Boolean = if (isClass)= false \n then false \n else if (rightPackage)= false \n then false \nelse if ((isClass)= null or (rightPackage)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nintended\n'"
	 * @generated
	 */
	MClassifier getClass_();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class</em>' reference.
	 * @see #isSetClass()
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @generated
	 */
	void setClass(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getClass_ <em>Class</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #isSetClass()
	 * @see #getClass_()
	 * @see #setClass(MClassifier)
	 * @generated
	 */
	void unsetClass();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getClass_ <em>Class</em>}' reference is set.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @return whether the value of the '<em>Class</em>' reference is set.
	 * @see #unsetClass()
	 * @see #getClass_()
	 * @see #setClass(MClassifier)
	 * @generated
	 */
	boolean isSetClass();

	/**
	 * Returns the value of the '<em><b>Cell Config</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig <em>Containing Class To Cell Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Config</em>' containment reference isn't
	 * clear, there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Config</em>' containment reference.
	 * @see #isSetCellConfig()
	 * @see #unsetCellConfig()
	 * @see #setCellConfig(MCellConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToCellConfig_CellConfig()
	 * @see com.montages.mtableeditor.MCellConfig#getContainingClassToCellConfig
	 * @model opposite="containingClassToCellConfig" containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MCellConfig getCellConfig();

	/** 
	 * Sets the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cell Config</em>' containment reference.
	 * @see #isSetCellConfig()
	 * @see #unsetCellConfig()
	 * @see #getCellConfig()
	 * @generated
	 */
	void setCellConfig(MCellConfig value);

	/**
	 * Unsets the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCellConfig()
	 * @see #getCellConfig()
	 * @see #setCellConfig(MCellConfig)
	 * @generated
	 */
	void unsetCellConfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mtableeditor.MClassToCellConfig#getCellConfig <em>Cell Config</em>}' containment reference is set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return whether the value of the '<em>Cell Config</em>' containment reference is set.
	 * @see #unsetCellConfig()
	 * @see #getCellConfig()
	 * @see #setCellConfig(MCellConfig)
	 * @generated
	 */
	boolean isSetCellConfig();

	/**
	 * Returns the value of the '<em><b>Containing Column Config</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mtableeditor.MColumnConfig#getClassToCellConfig <em>Class To Cell Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Column Config</em>' container
	 * reference isn't clear, there really should be more of a description
	 * here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Column Config</em>' container reference.
	 * @see #setContainingColumnConfig(MColumnConfig)
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToCellConfig_ContainingColumnConfig()
	 * @see com.montages.mtableeditor.MColumnConfig#getClassToCellConfig
	 * @model opposite="classToCellConfig" unsettable="true" transient="false"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structure' createColumn='false'"
	 * @generated
	 */
	MColumnConfig getContainingColumnConfig();

	/**
	 * Sets the value of the '
	 * {@link com.montages.mtableeditor.MClassToCellConfig#getContainingColumnConfig
	 * <em>Containing Column Config</em>}' container reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @param value
	 *            the new value of the '<em>Containing Column Config</em>'
	 *            container reference.
	 * @see #getContainingColumnConfig()
	 * @generated
	 */
	void setContainingColumnConfig(MColumnConfig value);

	/**
	 * Returns the value of the '<em><b>Cell Kind</b></em>' attribute. <!--
	 * begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Kind</em>' attribute isn't clear, there
	 * really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * 
	 * @return the value of the '<em>Cell Kind</em>' attribute.
	 * @see com.montages.mtableeditor.MtableeditorPackage#getMClassToCellConfig_CellKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation=
	 *        "http://www.xocl.org/OCL derive='if cellConfig.oclIsUndefined()\n  then null\n  else cellConfig.cellKind\nendif\n'"
	 *        annotation=
	 *        "http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Kind'"
	 *        annotation=
	 *        "http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getCellKind();

} // MClassToCellConfig
