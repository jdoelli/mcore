/**
 */
package com.montages.mtableeditor;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc --> The <b>Factory</b> for the model. It provides a
 * create method for each non-abstract class of the model. <!-- end-user-doc -->
 * @see com.montages.mtableeditor.MtableeditorPackage
 * @generated
 */
public interface MtableeditorFactory extends EFactory {

	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	MtableeditorFactory eINSTANCE = com.montages.mtableeditor.impl.MtableeditorFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>MEditor Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MEditor Config</em>'.
	 * @generated
	 */
	MEditorConfig createMEditorConfig();

	/**
	 * Returns a new object of class '<em>MTable Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MTable Config</em>'.
	 * @generated
	 */
	MTableConfig createMTableConfig();

	/**
	 * Returns a new object of class '<em>MColumn Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MColumn Config</em>'.
	 * @generated
	 */
	MColumnConfig createMColumnConfig();

	/**
	 * Returns a new object of class '<em>MClass To Cell Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MClass To Cell Config</em>'.
	 * @generated
	 */
	MClassToCellConfig createMClassToCellConfig();

	/**
	 * Returns a new object of class '<em>MRow Feature Cell</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MRow Feature Cell</em>'.
	 * @generated
	 */
	MRowFeatureCell createMRowFeatureCell();

	/**
	 * Returns a new object of class '<em>MOcl Cell</em>'.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @return a new object of class '<em>MOcl Cell</em>'.
	 * @generated
	 */
	MOclCell createMOclCell();

	/**
	 * Returns a new object of class '<em>MEdit Provider Cell</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MEdit Provider Cell</em>'.
	 * @generated
	 */
	MEditProviderCell createMEditProviderCell();

	/**
	 * Returns a new object of class '<em>MReference To Table Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MReference To Table Config</em>'.
	 * @generated
	 */
	MReferenceToTableConfig createMReferenceToTableConfig();

	/**
	 * Returns a new object of class '<em>MClass To Table Config</em>'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @return a new object of class '<em>MClass To Table Config</em>'.
	 * @generated
	 */
	MClassToTableConfig createMClassToTableConfig();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	MtableeditorPackage getMtableeditorPackage();

} // MtableeditorFactory
