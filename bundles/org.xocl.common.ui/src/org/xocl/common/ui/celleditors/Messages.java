package org.xocl.common.ui.celleditors;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.common.ui.celleditors.messages"; //$NON-NLS-1$
	public static String DateTimeCellEditor_DateTimeFormatsHint;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
