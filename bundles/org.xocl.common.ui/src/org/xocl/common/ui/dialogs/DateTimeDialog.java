package org.xocl.common.ui.dialogs;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;

/**
 * A simple input dialog for soliciting an input string from the user.
 * <p>
 * This concrete dialog class can be instantiated as is, or further subclassed as
 * required.
 * </p>
 */
public class DateTimeDialog extends Dialog {
    private String myTitle;
	private Date myDate;
	private DateTime myCalendar;
	private DateTime myTime;

    public DateTimeDialog(Shell parentShell, Date initialDate) {
    	this(parentShell, Messages.DateTimeDialog_DefaultTitle, initialDate);
    }

	
	public DateTimeDialog(Shell parentShell, String dialogTitle, Date initialDate) {
        super(parentShell);
        myTitle = dialogTitle;
		setDate(initialDate);
	}

    /*
     * (non-Javadoc)
     * 
     * @see org.eclipse.jface.window.Window#configureShell(org.eclipse.swt.widgets.Shell)
     */
    protected void configureShell(Shell shell) {
        super.configureShell(shell);
        if (myTitle != null) {
			shell.setText(myTitle);
		}
    }

    /*
     * (non-Javadoc) Method declared on Dialog.
     */
    protected Control createDialogArea(Composite parent) {
        // create composite
        parent = (Composite) super.createDialogArea(parent);

        Composite composite = new Composite(parent, SWT.NULL);

        composite.setLayout(new GridLayout (2, false));

        Label dateLabel = new Label(composite, SWT.WRAP);
        dateLabel.setText(Messages.DateTimeDialog_DateLabel);
        {
        	GridData data = new GridData(GridData.GRAB_HORIZONTAL
        			| GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
        			| GridData.VERTICAL_ALIGN_CENTER);
        	data.widthHint = convertHorizontalDLUsToPixels(IDialogConstants.MINIMUM_MESSAGE_AREA_WIDTH);
        	data.horizontalSpan = 2;
        	dateLabel.setLayoutData(data);
        	dateLabel.setFont(parent.getFont());
        }

        if (myDate == null) {
        	myDate = new Date();
        }

        GregorianCalendar gregorianCalendar = new GregorianCalendar();
        gregorianCalendar.setTime(getDate());

        myCalendar = new DateTime(composite, SWT.CALENDAR | SWT.BORDER);
        myCalendar.setDate(gregorianCalendar.get(Calendar.YEAR), gregorianCalendar.get(Calendar.MONTH), gregorianCalendar.get(Calendar.DAY_OF_MONTH));
        {
        	GridData data = new GridData(GridData.GRAB_HORIZONTAL
        			| GridData.GRAB_VERTICAL | GridData.HORIZONTAL_ALIGN_FILL
        			| GridData.VERTICAL_ALIGN_CENTER);
        	data.horizontalSpan = 2;
        	myCalendar.setLayoutData(data);
        }

        Label timeLabel = new Label(composite, SWT.WRAP);
        timeLabel.setText(Messages.DateTimeDialog_TimeLabel);
        {
        	GridData data = new GridData(GridData.VERTICAL_ALIGN_CENTER);
        	timeLabel.setLayoutData(data);
            timeLabel.setFont(parent.getFont());
        }
        
        
		myTime = new DateTime(composite, SWT.TIME | SWT.LONG | SWT.BORDER);
		myTime.setTime(gregorianCalendar.get(Calendar.HOUR_OF_DAY), gregorianCalendar.get(Calendar.MINUTE), gregorianCalendar.get(Calendar.SECOND));
		{
			GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_CENTER, 
					GridData.VERTICAL_ALIGN_CENTER, true, true);
			myTime.setLayoutData(data);
		}

        applyDialogFont(composite);
        
        return composite;
    }

    /**
     * Returns the string typed into this input dialog.
     * 
     * @return the input string
     */
    public Date getValue() {
    	return myDate;
    }
    
    @Override
    protected void okPressed() {
    	GregorianCalendar gregorianCalendar = new GregorianCalendar(myCalendar.getYear(), myCalendar.getMonth(), myCalendar.getDay(),
    			myTime.getHours(), myTime.getMinutes(), myTime.getSeconds());
    	myDate = gregorianCalendar.getTime();
    	super.okPressed();
    }

	public Date getDate() {
		return myDate;
	}

	public void setDate(Date date) {
		myDate = date;
	}
}