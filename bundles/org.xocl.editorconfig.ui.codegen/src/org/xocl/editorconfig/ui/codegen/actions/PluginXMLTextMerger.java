package org.xocl.editorconfig.ui.codegen.actions;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xocl.editorconfig.ui.codegen.XOCLCodegenUIPlugin;

public class PluginXMLTextMerger implements ITextMerger {
	private static final String NL = System.getProperties().getProperty("line.separator"); //$NON-NLS-1$

	private static final String ATTR_POINT = "point"; //$NON-NLS-1$

	private static final String ELEM_EXTENSION = "extension"; //$NON-NLS-1$
	private static final String ELEM_EXTENSION_START = "<" + ELEM_EXTENSION; //$NON-NLS-1$
	private static final String ELEM_EXTENSION_END = "</" + ELEM_EXTENSION + ">"; //$NON-NLS-1$ //$NON-NLS-2$

	private static final String ELEM_PLUGIN = "plugin"; //$NON-NLS-1$
	private static final String ELEM_PLUGIN_END = "</" + ELEM_PLUGIN + ">"; //$NON-NLS-1$ //$NON-NLS-2$

	public String process(String oldXml, String newXml) {
		try {
			ParsedPluginXml newDoc = new ParsedPluginXml(newXml);

			ParsedPluginXml oldDoc = new ParsedPluginXml(oldXml);

			return mergeDocuments(oldDoc, newDoc);
		} catch (Exception e) {
			XOCLCodegenUIPlugin.INSTANCE.log(e);
			return oldXml;
		}
	}

	private String mergeDocuments(ParsedPluginXml oldDoc, ParsedPluginXml newDoc) {
		ArrayList<ExtensionContributionDescriptor> addedContributions = new ArrayList<ExtensionContributionDescriptor>();
		for (Map.Entry<String, ArrayList<ExtensionContributionDescriptor>> entry : newDoc.getExtensions().entrySet()) {
			ArrayList<ExtensionContributionDescriptor> newContributions = entry.getValue();
			ArrayList<ExtensionContributionDescriptor> oldContributions = oldDoc.getExtensions().get(entry.getKey());
			if (newContributions != null) {
				if (oldContributions == null) {
					addedContributions.addAll(newContributions);
				} else {
					newContributionCycle: for (ExtensionContributionDescriptor newContribution : newContributions) {
						for (ExtensionContributionDescriptor oldContribution : oldContributions) {
							if (newContribution.isEqualTo(oldContribution)) {
								continue newContributionCycle;
							}
						}
						addedContributions.add(newContribution);
					}
				}
			}
		}
		if (addedContributions.isEmpty()) {
			return oldDoc.getXml();
		}
		if (oldDoc.getPluginEnd() < 0) {
			return newDoc.getXml();
		}

		StringBuilder newEntries = new StringBuilder();
		for (ExtensionContributionDescriptor contributionDescriptor : addedContributions) {
			newEntries.append(contributionDescriptor.getText()).append(NL);
		}

		StringBuilder result = new StringBuilder(oldDoc.getXml());
		result.insert(oldDoc.getPluginEnd(), newEntries.toString());
		return result.toString();
	}

	private class ParsedPluginXml {

		private String myXml;
		private int myPluginEnd;
		private Map<String,ArrayList<ExtensionContributionDescriptor>> myExtensions;

		ParsedPluginXml(String xml) throws SAXException, IOException, ParserConfigurationException {
			myXml = xml;
			myExtensions = new HashMap<String, ArrayList<ExtensionContributionDescriptor>>();
			parse(xml);
		}

		public String getXml() {
			return myXml;
		}

		public int getPluginEnd() {
			return myPluginEnd;
		}

		public Map<String, ArrayList<ExtensionContributionDescriptor>> getExtensions() {
			return myExtensions;
		}

		private void parse(String xml) throws SAXException, IOException, ParserConfigurationException {
			myPluginEnd = xml.lastIndexOf(ELEM_PLUGIN_END);

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource(new StringReader(xml));

			Document document = db.parse(is);

			Element documentElement = document.getDocumentElement();

			if (!ELEM_PLUGIN.equals(documentElement.getNodeName())) {
				throw new IOException(Messages.PluginXMLTextMerger_CorruptPluginXmlError);
			}

			NodeList childNodes = documentElement.getChildNodes();
			for (int i = 0, n = childNodes.getLength(); i < n; i ++) {
				Node node = childNodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					if (ELEM_EXTENSION.equals(element.getNodeName())) {
						parseExtension(element);
					}
				}
			}
		}

		private void parseExtension(Element element) {
			String pointName = element.getAttribute(ATTR_POINT);
			ArrayList<ExtensionContributionDescriptor> extensionContributions = myExtensions.get(pointName);
			if (extensionContributions == null) {
				extensionContributions = new ArrayList<ExtensionContributionDescriptor>();
				myExtensions.put(pointName, extensionContributions);
			}
			NodeList childNodes = element.getChildNodes();
			for (int i = 0, n = childNodes.getLength(); i < n; i ++) {
				Node node = childNodes.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					ExtensionContributionDescriptor descriptor = new ExtensionContributionDescriptor(pointName, (Element) node);
					extensionContributions.add(descriptor);
				}
			}			
		}

	}

	private class ExtensionContributionDescriptor {
		private final Element myNode;
		private final String myPointName;

		ExtensionContributionDescriptor(String pointName, Element node) {
			myPointName = pointName;
			myNode = node;
		}

		public Object getText() {
			String text = ELEM_EXTENSION_START + " point=\"" + myPointName + "\">" + NL; //$NON-NLS-1$ //$NON-NLS-2$
			text += getTextContent(myNode) + ELEM_EXTENSION_END + NL;
			return text;
		}

		private String getTextContent(Node node) {
			try {
				TransformerFactory tFactory = TransformerFactory.newInstance();
				Transformer transformer = tFactory.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes"); //$NON-NLS-1$
				transformer.setOutputProperty(OutputKeys.INDENT, "yes"); //$NON-NLS-1$

				Source src = new DOMSource(node);
				StringWriter writer = new StringWriter();
				Result result = new StreamResult(writer);

				transformer.transform(src, result);

				return writer.toString();
			} catch (Exception e) {
				XOCLCodegenUIPlugin.INSTANCE.log(e);
				return ""; //$NON-NLS-1$
			}
		}		

		public boolean isEqualTo(ExtensionContributionDescriptor other) {
			return other.myNode.isEqualNode(myNode);
		}
	}
}