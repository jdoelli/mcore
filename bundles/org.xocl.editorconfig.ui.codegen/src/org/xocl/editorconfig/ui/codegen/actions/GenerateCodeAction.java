package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenClassGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenEnumGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenPackageGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil;
import org.eclipse.emf.codegen.merge.java.JControlModel;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.xocl.editorconfig.ui.codegen.actions.generatoradapters.GenClassGeneratorAdapterExt;
import org.xocl.editorconfig.ui.codegen.actions.generatoradapters.GenEnumGeneratorAdapterExt;
import org.xocl.editorconfig.ui.codegen.actions.generatoradapters.GenModelGeneratorAdapterExt;
import org.xocl.editorconfig.ui.codegen.actions.generatoradapters.GenPackageGeneratorAdapterExt;

public class GenerateCodeAction extends WorkspaceModifyOperation {

	GenModel genModel;
	Generator defaultGenerator;
	private final XoclGenerationSettings myXoclGenerationSettings;

	public GenerateCodeAction(GenModel genModel, XoclGenerationSettings xoclGenerationSettings) {	
		myXoclGenerationSettings = xoclGenerationSettings;
		this.genModel = reread(genModel);
		this.genModel.reconcile();
		this.genModel.setCanGenerate(true);
		defaultGenerator = createGenerator(this.genModel);
	}


	public Generator createGenerator(GenModel genModel) {

		Generator defaultGenerator = GenModelUtil.createGenerator(this.genModel);
		Generator generator = new Generator() {
			@Override
			protected Collection<GeneratorAdapter> getAdapters(Object object) {
				ArrayList<GeneratorAdapter> adapters = (ArrayList<GeneratorAdapter>) super.getAdapters(object);
				for (int i = 0, n = adapters.size(); i < n; i ++) {
					GeneratorAdapter adapter = adapters.get(i);
					if (adapter instanceof GenModelGeneratorAdapter) {
						GenModelGeneratorAdapterExt newAdapter = new GenModelGeneratorAdapterExt(adapter.getAdapterFactory(), myXoclGenerationSettings);
						adapters.set(i, newAdapter);  
					} else if (adapter instanceof GenClassGeneratorAdapter) {
						GenClassGeneratorAdapterExt newAdapter = new GenClassGeneratorAdapterExt(adapter.getAdapterFactory(), myXoclGenerationSettings);
						adapters.set(i, newAdapter); 
					} else if (adapter instanceof GenEnumGeneratorAdapter) {
						GenEnumGeneratorAdapterExt newAdapter = new GenEnumGeneratorAdapterExt(adapter.getAdapterFactory(), myXoclGenerationSettings);
						adapters.set(i, newAdapter); 
					} else if (adapter instanceof GenPackageGeneratorAdapter) {
						GenPackageGeneratorAdapterExt newAdapter = new GenPackageGeneratorAdapterExt(adapter.getAdapterFactory(), myXoclGenerationSettings);
						adapters.set(i, newAdapter);
					}
				}
				return adapters;
			}
		};
		generator.setInput(genModel);
		JControlModel jControlModel = generator.getJControlModel();
		JControlModel defaultControlModel = defaultGenerator.getJControlModel();

		jControlModel.setLeadingTabReplacement(defaultControlModel.getLeadingTabReplacement());
		jControlModel.setConvertToStandardBraceStyle(defaultControlModel.convertToStandardBraceStyle());
		return generator;
	}

	private GenModel reread(GenModel originalGenModel) {
		URI uri = originalGenModel.eResource().getURI();
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.getResource(uri, true);
		GenModel rereadGenModel = (GenModel)resource.getContents().get(0);
		return rereadGenModel;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
	InvocationTargetException, InterruptedException {

		monitor.beginTask(Messages.GenerateCodeAction_GenerateCodeTask, 100);

		final BasicDiagnostic diagnostic = new BasicDiagnostic();
		diagnostic.add(defaultGenerator.generate(genModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, Messages.GenerateCodeAction_model, new BasicMonitor.EclipseSubProgress(monitor, 50)));
		diagnostic.add(defaultGenerator.generate(genModel, GenBaseGeneratorAdapter.EDIT_PROJECT_TYPE, Messages.GenerateCodeAction_edit, new BasicMonitor.EclipseSubProgress(monitor, 25)));
		diagnostic.add(defaultGenerator.generate(genModel, GenBaseGeneratorAdapter.EDITOR_PROJECT_TYPE, Messages.GenerateCodeAction_editor, new BasicMonitor.EclipseSubProgress(monitor, 25)));

		monitor.done();
		if (PlatformUI.isWorkbenchRunning() && diagnostic.getSeverity() == Diagnostic.ERROR) {
			PlatformUI.getWorkbench().getDisplay().asyncExec(
					new Runnable() {
						public void run() {
							DiagnosticDialog.openProblem(
									PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
									Messages.GenerateCodeAction_GenerationError,
									Messages.GenerateCodeAction_GenerationErrorDesc,
									diagnostic
									);
						}
					}
					);
		}	
	}

}
