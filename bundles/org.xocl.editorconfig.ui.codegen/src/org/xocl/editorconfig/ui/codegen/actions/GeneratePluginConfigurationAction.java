package org.xocl.editorconfig.ui.codegen.actions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.BasicExtendedMetaData;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.importer.ecore.EcoreImporter;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.langlets.plugin.DocumentRoot;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginFactory;
import org.langlets.plugin.PluginType;
import org.langlets.plugin.util.PluginResourceFactoryImpl;

public class GeneratePluginConfigurationAction extends WorkspaceModifyOperation{

	public static IPath PLUGIN_CONFIGURATION_PATH = new Path("plugin.xml"); //$NON-NLS-1$

	public static IPath META_INF_FOLDER = new Path("META-INF"); //$NON-NLS-1$

	public static IPath	MANIFEST_PATH = META_INF_FOLDER.append("MANIFEST.MF"); //$NON-NLS-1$

	public static String EDITOR_CONFIG_EXT_POINT_ID = "org.xocl.editorconfig.editorConfigAssociations"; //$NON-NLS-1$

	public static String EDITOR_CONFIG_FILE_EXTENSION = "editorconfig"; //$NON-NLS-1$

	private IFile modelFile;

	private EcoreImporter ecoreImporter;

	private static String MANIFEST_TEMPLATE = 
		"Manifest-Version: 1.0\n" + //$NON-NLS-1$
		"Bundle-ManifestVersion: 2\n" + //$NON-NLS-1$
		"Bundle-Name: %%pluginName\n" + //$NON-NLS-1$
		"Bundle-SymbolicName: %s;singleton:=true\n" + //$NON-NLS-1$
		"Bundle-Version: 1.0.0.qualifier\n" + //$NON-NLS-1$
		"Require-Bundle: org.xocl.editorconfig,\n com.montages.common\n" + //$NON-NLS-1$
		"Bundle-Vendor: %%providerName\n"; //$NON-NLS-1$

	private final XoclGenerationSettings myXoclGenerationSettings;


	/**
	 * @param modelFile
	 * @param ecoreImporter
	 * @param xoclGenerationSettings 
	 */
	public GeneratePluginConfigurationAction(IFile modelFile,
			EcoreImporter ecoreImporter, XoclGenerationSettings xoclGenerationSettings) {
		this.modelFile = modelFile;
		this.ecoreImporter = ecoreImporter;
		myXoclGenerationSettings = xoclGenerationSettings;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
	InvocationTargetException, InterruptedException {
		createPluginConfiguration(monitor);
		createManifestMF(monitor);
	}

	private void createPluginConfiguration(IProgressMonitor monitor) throws CoreException {
		final IFile pluginConfigurationFile = modelFile.getProject().getFile(PLUGIN_CONFIGURATION_PATH);
		if (!myXoclGenerationSettings.isGenerated(pluginConfigurationFile)) {
			return;
		}

		final Map<Object, Object> options = new HashMap<Object, Object>();
		options.put(XMLResource.OPTION_EXTENDED_META_DATA, new BasicExtendedMetaData());
		options.put(XMLResource.OPTION_SUPPRESS_DOCUMENT_ROOT, Boolean.TRUE);

		final URI uri = URI.createPlatformResourceURI(pluginConfigurationFile.getFullPath().toString(), true);
		final Resource resource = new PluginResourceFactoryImpl().createResource(uri);
		if (pluginConfigurationFile.exists()) {
			try {
				resource.load(null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		final PluginType pluginType;
		if (resource.getContents().isEmpty()) {
			DocumentRoot root = PluginFactory.eINSTANCE.createDocumentRoot();
			pluginType = PluginFactory.eINSTANCE.createPluginType();
			root.setPlugin(pluginType);
		} else {
			DocumentRoot root = (DocumentRoot) resource.getContents().get(0);
			if (root.getPlugin() == null) {
				root.setPlugin(PluginFactory.eINSTANCE.createPluginType());
			}
			pluginType = root.getPlugin();
		}
		
		createExtension(pluginType);

		try {
			resource.save(options);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private EObject createExtension(PluginType pluginType) {
		List<ExtensionType> extensions = pluginType.findExtensions(EDITOR_CONFIG_EXT_POINT_ID);
		final ExtensionType extension;
		if (extensions.isEmpty()) {
			extension = PluginFactory.eINSTANCE.createExtensionType();
			extension.setPoint(EDITOR_CONFIG_EXT_POINT_ID);
			pluginType.getExtension().add(extension);
		} else {
			extension = extensions.get(0);
		}

		for (EPackage ePackage : ecoreImporter.getEPackages()) {
			createExtension(ePackage, pluginType, extension);
		}

		return pluginType;
	}
	
	private void createExtension(EPackage ePackage, PluginType pluginType, ExtensionType extension) {
		IFile ecoreFile = modelFile;

		URI ePackageResourceURI = ePackage.eResource().getURI();
		if (ePackageResourceURI.isPlatformResource()) {
			IPath path = new Path(ePackageResourceURI.toPlatformString(true));
			ecoreFile = ResourcesPlugin.getWorkspace().getRoot().getFile(path);
		}

		IFile genModelFile = ResourcesPlugin.getWorkspace().getRoot().getFile(ecoreImporter.getGenModelPath());

		if (ecoreFile.getProject() == genModelFile.getProject()) {			
			createAssociation(extension, ePackage, ecoreFile, pluginType);

			for(Iterator<EObject> iterator = ePackage.eAllContents(); iterator.hasNext();) {
				EObject eObject = iterator.next();
				if (eObject instanceof EPackage) {
					EPackage subPackage = (EPackage) eObject;
					createAssociation(extension, subPackage, ecoreFile, pluginType);
				}
			}
		}
	}

	private String getEditorConfigPath(IFile modelFile, IFile ecoreFile) {
		IPath path = getPath(ecoreFile);
		if (!modelFile.getProject().exists(path)) {
			path = getPath(modelFile);
		}
		
		return path.toString();
	}
	
	private IPath getPath(IFile file) {
		return file.getFullPath()
				.removeFirstSegments(1)
				.makeRelative()
				.removeFileExtension()
				.addFileExtension(EDITOR_CONFIG_FILE_EXTENSION);
	}

	private void createAssociation(ExtensionType extension, EPackage ePackage, IFile ecoreFile, PluginType pluginType) {
		EditorConfigModelAssociationType associationType = pluginType.findEditorConfigAssociation(ePackage.getNsURI());
		if (associationType == null) {
			associationType = PluginFactory.eINSTANCE.createEditorConfigModelAssociationType();
			extension.getEditorConfigModelAssociation().add(associationType);
		}

		String editorConfigPath = getEditorConfigPath(modelFile, ecoreFile);
		associationType.setConfig(editorConfigPath);
		associationType.setUri(ePackage.getNsURI());
	}

	//TODO: more elegant way to generate the manifest.mf (re-use of EMF templates or use EMF text)
	private void createManifestMF(IProgressMonitor monitor) throws CoreException {
		IFile manifestfile = modelFile.getProject().getFile(MANIFEST_PATH);	
		if (!myXoclGenerationSettings.isGenerated(manifestfile)) {
			return;
		}
		String manifestContents = String.format(MANIFEST_TEMPLATE, modelFile.getProject().getName());
		InputStream is = new ByteArrayInputStream(manifestContents.getBytes());
		if (! manifestfile.exists()) {
			IFolder metaInf = modelFile.getProject().getFolder(META_INF_FOLDER);
			if (! metaInf.exists()) {
				metaInf.create(true, true, monitor);
			}
			manifestfile.create(is, true, monitor);
		} else {
			manifestfile.setContents(is, true, true, monitor);
		}
	}

}
