package org.xocl.editorconfig.ui.codegen.actions.generatoradapters;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenClassGeneratorAdapter;
import org.eclipse.emf.codegen.jet.JETEmitter;
import org.eclipse.emf.codegen.util.GIFEmitter;
import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.common.util.URI;
import org.xocl.editorconfig.ui.codegen.actions.XoclGenerationSettings;

public class GenClassGeneratorAdapterExt extends GenClassGeneratorAdapter {
	private final XoclGenerationSettings myXoclGenerationSettings;

	public GenClassGeneratorAdapterExt(
			GeneratorAdapterFactory generatorAdapterFactory, XoclGenerationSettings xoclGenerationSettings) {
		super(generatorAdapterFactory);
		myXoclGenerationSettings = xoclGenerationSettings;
	}

	@Override
	protected void generateGIF(String targetPathName, GIFEmitter gifEmitter,
			String parentKey, String childKey, boolean overwrite,
			Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(targetPathName))) {
			return;
		}
		super.generateGIF(targetPathName, gifEmitter, parentKey, childKey, overwrite,
				monitor);
	}

	@Override
	protected void generateJava(String targetPath, String packageName,
			String className, JETEmitter jetEmitter, Object[] arguments,
			Monitor monitor) {
		URI targetDirectory = toURI(targetPath).appendSegments(packageName.split("\\.")); //$NON-NLS-1$
		URI targetFile = targetDirectory.appendSegment(className + ".java"); //$NON-NLS-1$
		if (!myXoclGenerationSettings.isGenerated(new Path(targetFile.toString()))) {
			return;
		}
		super.generateJava(targetPath, packageName, className, jetEmitter, arguments,
				monitor);
	}

	@Override
	protected void generateProperties(String targetPathName,
			JETEmitter jetEmitter, Object[] arguments, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(targetPathName))) {
			return;
		}
		super.generateProperties(targetPathName, jetEmitter, arguments, monitor);
	}

	@Override
	protected void generateText(String targetPathName, JETEmitter jetEmitter,
			Object[] arguments, boolean overwrite, String encoding,
			Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(targetPathName))) {
			return;
		}
		super.generateText(targetPathName, jetEmitter, arguments, overwrite, encoding,
				monitor);
	}

	@Override
	protected void ensureProjectExists(String workspacePath, Object object,
			Object projectType, boolean force, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(workspacePath))) {
			return;
		}
		super.ensureProjectExists(workspacePath, object, projectType, force, monitor);
	}

	@Override
	protected void ensureContainerExists(URI workspacePath, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(workspacePath.toString()))) {
			return;
		}
		super.ensureContainerExists(workspacePath, monitor);
	}
}