package org.xocl.editorconfig.ui.codegen.actions;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.ui.actions.WorkspaceModifyOperation;

public class GenerateTestWorkspaceAction extends WorkspaceModifyOperation {
	private static final String WORKSPACE_FOLDER_PATH = "workspace"; //$NON-NLS-1$
	private static final String TEST_PROJECT_NAME = "test"; //$NON-NLS-1$
	private IProject myProject;
	private final XoclGenerationSettings myXoclGenerationSettings;

	public GenerateTestWorkspaceAction(IFile file, XoclGenerationSettings xoclGenerationSettings) {
		this(file.getProject(), xoclGenerationSettings);
	}

	public GenerateTestWorkspaceAction(IProject project, XoclGenerationSettings xoclGenerationSettings) {
		myXoclGenerationSettings = xoclGenerationSettings;
		myProject = project;
	}

	@Override
	protected void execute(IProgressMonitor monitor) throws CoreException,
		InvocationTargetException, InterruptedException {
		
		IFolder workspaceFolder = createTestWorkspaceFolder(myProject, monitor);

		if (workspaceFolder != null) {
			createTestProject(workspaceFolder, monitor);
		}
		
		if ((workspaceFolder != null)) {
			myProject.refreshLocal(IResource.DEPTH_INFINITE, monitor);
		}
	}

	private IFolder createTestWorkspaceFolder(IProject project, IProgressMonitor monitor) throws CoreException {
		IFolder folder = project.getFolder(WORKSPACE_FOLDER_PATH);
		if (myXoclGenerationSettings != null && !myXoclGenerationSettings.isGenerated(folder)) {
			return null;
		}
		if (folder.exists()) {
			return null;
		}

		folder.create(false, true, monitor);

		return folder;
	}

	private IFolder createTestProject(IFolder workspace, IProgressMonitor monitor) throws CoreException {
		IFolder folder = workspace.getFolder(TEST_PROJECT_NAME);
		if (!folder.exists()) {
			folder.create(true, true, monitor);
		}

		return folder;
	}
}