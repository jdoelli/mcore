package org.xocl.editorconfig.ui.codegen.actions;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.editorconfig.ui.codegen.actions.messages"; //$NON-NLS-1$
	public static String GenerateCodeAction_edit;
	public static String GenerateCodeAction_editor;
	public static String GenerateCodeAction_GenerateCodeTask;
	public static String GenerateCodeAction_GenerationError;
	public static String GenerateCodeAction_GenerationErrorDesc;
	public static String GenerateCodeAction_model;
	public static String GenerateGenModelAction_ValidationErrorDialogMessage;
	public static String GenerateGenModelAction_ValidationErrorDialogTitle;
	public static String PluginXMLTextMerger_CorruptPluginXmlError;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
