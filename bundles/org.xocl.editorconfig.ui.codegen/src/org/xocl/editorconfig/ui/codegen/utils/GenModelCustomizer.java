package org.xocl.editorconfig.ui.codegen.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.genmodel.GenBase;
import org.eclipse.emf.codegen.ecore.genmodel.GenClassifier;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.codegen.ecore.genmodel.GenResourceKind;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.importer.ecore.EcoreImporter;
import org.xocl.core.util.XoclEmfUtil;

public class GenModelCustomizer {

	public static final String GENMODEL_FILE_EXTENSION = "genmodel"; //$NON-NLS-1$

	public static final String XOCL_TEMPLATE_DIRECTORY = "platform:/plugin/org.xocl.templates/templates"; //$NON-NLS-1$

	public static final String XOCL_GENMODEL_ANNOTATION_SOURCE = "http://www.xocl.org/GENMODEL"; //$NON-NLS-1$

	public static final String GENMODEL_ANNOTATION_PREFIX = "GENMODEL_"; //$NON-NLS-1$

	public static final void adjustSettings(final GenModel genModel, EcoreImporter ecoreImporter) {
		adjustSettings(genModel, ecoreImporter == null ? null : ecoreImporter.getFirstModelLocationURI(false));
	}

	public static final void adjustSettings(final GenModel genModel) {
		adjustSettings(genModel, (URI) null);
	}

	public static final void adjustSettings(final GenModel genModel, URI firstModelLocationURI) {
		adjustGenerationDirectories(genModel);
		adjustPluginIDs(genModel);
		adjustGenerationSettings(genModel);
		neutralizeTestGeneration(genModel);
		addModelContainingProjectDependency(genModel);

		for (GenPackage genPackage : genModel.getGenPackages()) {
			// bug 655: adjust settings for subPackages.
			adjustSettings(genPackage, firstModelLocationURI);

			//bug632 and bug623: Philipp Kutter commented change, as strange behavior resulted.
			//adjustInitializeByLoading(genPackage);
		}

		adjustOperationReflection(genModel);
		adjustComplianceLevel(genModel);

		adjustEcoreAnnotatedGenModelProperties(genModel.getGenPackages());

		genModel.reconcile();
	}

	private static final void adjustSettings(final GenPackage genPackage, URI firstModelLocationURI) {
		adjustGenFeatures(genPackage);
		adjustBasePackage(genPackage, firstModelLocationURI);
		adjustSerialization(genPackage);

		for (GenPackage subGenPackage : genPackage.getSubGenPackages()) {
			adjustSettings(subGenPackage, firstModelLocationURI);
		}
	}

	public static void adjustEcoreAnnotatedGenModelProperties(EList<GenPackage> genPackages) {
		for (GenPackage genPackage : genPackages) {
			adjustEcoreAnnotatedGenModelProperty(genPackage);
			for (GenClassifier genClassifier : genPackage.getGenClassifiers()) {
				adjustEcoreAnnotatedGenModelProperty(genClassifier);
			}
			for (GenFeature genFeature : genPackage.getAllGenFeatures()) {
				adjustEcoreAnnotatedGenModelProperty(genFeature);
			}
			adjustEcoreAnnotatedGenModelProperties(genPackage.getNestedGenPackages());
		}
	}

	private static String getFeatureName(GenBase genBase, String defaultValue) {
		String featureName = defaultValue;
		if ((featureName != null) && featureName.startsWith(GENMODEL_ANNOTATION_PREFIX)) {
			genBase = genBase.getGenModel();
			featureName = featureName.substring(GENMODEL_ANNOTATION_PREFIX.length());
		}
		return featureName;
	}

	private static List<String> parseElementaryAnnotationDetailValue(String value) {
		List<String> result = new ArrayList<String>();
		StringBuilder elementaryValue = new StringBuilder();
		for (int i = 0; i < value.length(); i++) {
			char ch = value.charAt(i);
			if (ch == '|') {
				if ((i + 1 < value.length()) && (value.charAt(i + 1) == '|')) {
					elementaryValue.append('|');
					i++;
				} else {
					result.add(elementaryValue.toString());
					elementaryValue = new StringBuilder();
				}
			} else {
				elementaryValue.append(ch);
			}
		}
		result.add(elementaryValue.toString());
		return result;
	}

	private static void adjustAnnotationDetailEntry(EStructuralFeature eStructuralFeature, EClassifier featureType, List<String> elementaryValues, GenBase genBase) {
		for (String itemValue : elementaryValues) {
			Object value = null;
			if (featureType instanceof EDataType) {
				EDataType eDataType = (EDataType) featureType;
				value = EcoreUtil.createFromString(eDataType, itemValue);
			} else if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (!eReference.isContainment()) {
					URI uri = URI.createURI(itemValue);
					if (uri.isRelative()) {
						uri = uri.resolve(EcoreUtil.getURI(genBase));
					}
					ResourceSet resourceSet = genBase.eResource().getResourceSet();
					value = resourceSet.getEObject(uri, true);
				}
			}
			if (eStructuralFeature.isMany()) {
				Object featureValue = genBase.eGet(eStructuralFeature);
				if (featureValue instanceof Collection<?>) {
					@SuppressWarnings("unchecked")
					Collection<Object> collection = (Collection<Object>) featureValue;
					collection.add(value);
				}
			} else {
				genBase.eSet(eStructuralFeature, value);
			}
		}
	}

	private static void adjustEcoreModelAnnotation(EAnnotation eAnnotation, GenBase genBase) {
		for (Map.Entry<String, String> nextEntry : eAnnotation.getDetails()) {
			String featureName = getFeatureName(genBase, nextEntry.getKey());
			EClass genBaseEClass = genBase.eClass();
			if (genBaseEClass == null) {
				continue;
			}
			EStructuralFeature eStructuralFeature = genBaseEClass.getEStructuralFeature(featureName);
			if (eStructuralFeature == null) {
				continue;
			}
			EClassifier featureType = eStructuralFeature.getEType();
			List<String> elementaryValues = parseElementaryAnnotationDetailValue(nextEntry.getValue());
			adjustAnnotationDetailEntry(eStructuralFeature, featureType, elementaryValues, genBase);
		}
	}

	private static void adjustEcoreAnnotatedGenModelProperty(GenBase genBase) {
		EModelElement ecoreModelElement = genBase.getEcoreModelElement();
		for (EAnnotation eAnnotation : ecoreModelElement.getEAnnotations()) {
			if (false == XOCL_GENMODEL_ANNOTATION_SOURCE.equalsIgnoreCase(eAnnotation.getSource())) {
				continue;
			}
			adjustEcoreModelAnnotation(eAnnotation, genBase);
		}
	}

	private static final void adjustGenerationDirectories(final GenModel genModel) {
		String modelProjectDirectory = genModel.getModelProjectDirectory();
		adjustEditGenerationDirectory(genModel, modelProjectDirectory);
		adjustEditorGenerationDirectory(genModel, modelProjectDirectory);
	}

	private static final void adjustPluginIDs(final GenModel genModel) {
		String modelPluginID = genModel.getModelPluginID();
		adjustEditPluginID(genModel, modelPluginID);
		adjustEditorPluginID(genModel, modelPluginID);
	}

	private static String getDefaultSourceFolder(List<String> sourceFolders) {
		String sourceFolder = sourceFolders.get(0);
		String sourceFolderName = new Path(sourceFolder).removeTrailingSeparator().toString();
		return sourceFolderName;
	}

	private static final void adjustEditGenerationDirectory(final GenModel genModel, final String modelProjectDirectory) {
		String sourceFolder = getDefaultSourceFolder(genModel.getEditSourceFolders());
		String newModelDirectory = GeneratedProjectsNamingConventions.getNewEditGenerationDirectory(modelProjectDirectory, sourceFolder);
		genModel.setEditDirectory(newModelDirectory);
	}

	private static final void adjustEditorGenerationDirectory(final GenModel genModel, final String modelProjectDirectory) {
		String sourceFolder = getDefaultSourceFolder(genModel.getEditorSourceFolders());
		String newModelDirectory = GeneratedProjectsNamingConventions.getNewEditorGenerationDirectory(modelProjectDirectory, sourceFolder);
		genModel.setEditorDirectory(newModelDirectory);
	}

	private static final void adjustEditPluginID(final GenModel genModel, final String modelPluginID) {
		String newEditPluginID = GeneratedProjectsNamingConventions.getNewEditPluginID(modelPluginID);
		genModel.setEditPluginID(newEditPluginID);
	}

	private static final void adjustEditorPluginID(final GenModel genModel, final String modelPluginID) {
		String newEditorPluginID = GeneratedProjectsNamingConventions.getNewEditorPluginID(modelPluginID);
		genModel.setEditorPluginID(newEditorPluginID);
	}

	private static final void adjustSerialization(final GenPackage genPackage) {
		genPackage.setResource(GenResourceKind.XML_LITERAL);
	}

	private static void adjustOperationReflection(GenModel genModel) {
		genModel.setOperationReflection(true);
	}

	private static void adjustComplianceLevel(GenModel genModel) {
		genModel.setComplianceLevel(GenJDKLevel.JDK60_LITERAL);
	}

	private static void adjustGenFeatures(GenPackage genPackage) {
		List<GenFeature> allGenFeatures = genPackage.getAllGenFeatures();
		for (GenFeature genFeature : allGenFeatures) {
			EStructuralFeature eStructuralFeature = genFeature.getEcoreFeature();
			if (eStructuralFeature != null) {
				EClass eClass = eStructuralFeature.getEContainingClass();
				if (eClass != null) {
					String propertyCategory = XoclEmfUtil.findPropertyCategoryAnnotationText(eStructuralFeature, eClass);
					if (propertyCategory != null) {
						genFeature.setPropertyCategory(propertyCategory);
					}
					String propertyMultiline = XoclEmfUtil.findPropertyMultilineAnnotationText(eStructuralFeature, eClass);
					if (propertyMultiline != null) {
						genFeature.setPropertyMultiLine(Boolean.parseBoolean(propertyMultiline));
					}
				}
			}
		}
	}

	private static final void adjustBasePackage(final GenPackage genPackage, URI firstModelLocationURI) {
		String basePackageName = genPackage.getBasePackage();
		if (basePackageName == null || basePackageName.length() == 0) {
			String newbasePackageName = GeneratedProjectsNamingConventions.getNewBasePackage(genPackage.getGenModel().getModelProjectDirectory(), genPackage.getEcorePackage().getName());
			genPackage.setBasePackage(newbasePackageName);
			if (firstModelLocationURI != null) {
				if (!isJavaIdentifier(genPackage.getPrefix())) {
					EPackage ePackage = genPackage.getEcorePackage();
					if (isJavaIdentifier(ePackage.getName())) {
						genPackage.setPrefix(ePackage.getName());
					} else if (isJavaIdentifier(ePackage.getNsPrefix())) {
						genPackage.setPrefix(ePackage.getNsPrefix());
					} else {
						String prefix = firstModelLocationURI.trimFileExtension().lastSegment();
						if (prefix != null) {
							genPackage.setPrefix(prefix);
						}
					}
				}
			}
		}
	}

	public static boolean isJavaIdentifier(String s) {
		if (s == null || s.length() == 0 || !Character.isJavaIdentifierStart(s.charAt(0))) {
			return false;
		}
		for (int i = 1; i < s.length(); i++) {
			if (!Character.isJavaIdentifierPart(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	private static final void neutralizeTestGeneration(final GenModel genModel) {
		genModel.setTestsDirectory(""); //$NON-NLS-1$
	}

	private static final void adjustGenerationSettings(final GenModel genModel) {
		genModel.setDynamicTemplates(true);
		genModel.setTemplateDirectory(XOCL_TEMPLATE_DIRECTORY);
		genModel.setCodeFormatting(true);
		genModel.setContainmentProxies(true);
		genModel.getTemplatePluginVariables().add("org.xocl.core.opensource"); //$NON-NLS-1$
		genModel.setCreationIcons(false);
	}

	private static final void addModelContainingProjectDependency(final GenModel genModel) {
		String modelPluginID = genModel.getModelPluginID();
		String modelContainingProjectID = modelPluginID.substring(0, modelPluginID.lastIndexOf("." + GeneratedProjectsNamingConventions.XOCL_MODEL_PROJECT_NAME)); //$NON-NLS-1$
		genModel.getModelPluginVariables().add(modelContainingProjectID);
	}

}
