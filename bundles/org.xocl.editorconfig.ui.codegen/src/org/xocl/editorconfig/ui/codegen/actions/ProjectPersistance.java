/**
 * <copyright>
 *
 * Copyright (c) 2006-2016 The Voyant Group and A4M applied formal methods AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.editorconfig.ui.codegen.actions;

import java.text.MessageFormat;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileInfo;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;

/**
 * @author Max Stepanov
 *
 */
public final class ProjectPersistance {

	private static final String QUIALIFIER = "org.xocl.codegen";
	/**
	 * 
	 */
	private ProjectPersistance() {
	}
	
	public static void saveModelState(IFile modelFile) throws CoreException {
		IProject project = modelFile.getProject();
		project.setPersistentProperty(getQualifiedName(modelFile), fileHash(modelFile));
	}

	public static void resetModelState(IFile modelFile) throws CoreException {
		IProject project = modelFile.getProject();
		project.setPersistentProperty(getQualifiedName(modelFile), null);
	}

	public static boolean isModelChanged(IFile modelFile) throws CoreException {
		IProject project = modelFile.getProject();
		String value = project.getPersistentProperty(getQualifiedName(modelFile));
		return value == null || !value.equals(fileHash(modelFile));
	}
	
	private static QualifiedName getQualifiedName(IFile file) {
		return new QualifiedName(QUIALIFIER, file.getProjectRelativePath().toPortableString());
	}

	private static String fileHash(IFile file) throws CoreException {
		IFileInfo fileInfo = EFS.getStore(file.getLocationURI()).fetchInfo();
		return MessageFormat.format("{0,number,integer},{1,number,integer}", fileInfo.getLastModified(), fileInfo.getLength());
	}

}
