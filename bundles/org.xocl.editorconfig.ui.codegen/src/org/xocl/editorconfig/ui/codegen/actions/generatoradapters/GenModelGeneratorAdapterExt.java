package org.xocl.editorconfig.ui.codegen.actions.generatoradapters;

import java.io.OutputStream;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.codegen.ecore.CodeGenEcorePlugin;
import org.eclipse.emf.codegen.ecore.generator.GeneratorAdapterFactory;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenModelGeneratorAdapter;
import org.eclipse.emf.codegen.jet.JETEmitter;
import org.eclipse.emf.codegen.util.CodeGenUtil;
import org.eclipse.emf.codegen.util.GIFEmitter;
import org.eclipse.emf.common.util.Monitor;
import org.eclipse.emf.common.util.URI;
import org.xocl.editorconfig.ui.codegen.actions.ITextMerger;
import org.xocl.editorconfig.ui.codegen.actions.ManifestTextMerger;
import org.xocl.editorconfig.ui.codegen.actions.PluginXMLTextMerger;
import org.xocl.editorconfig.ui.codegen.actions.XoclGenerationSettings;

public class GenModelGeneratorAdapterExt extends GenModelGeneratorAdapter {

	private final XoclGenerationSettings myXoclGenerationSettings;

	public GenModelGeneratorAdapterExt(GeneratorAdapterFactory generatorAdapterFactory, XoclGenerationSettings xoclGenerationSettings) {
		super(generatorAdapterFactory);
		myXoclGenerationSettings = xoclGenerationSettings;
	}

	@Override
	protected void generateGIF(String targetPathName, GIFEmitter gifEmitter,
			String parentKey, String childKey, boolean overwrite,
			Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(targetPathName))) {
			return;
		}
		super.generateGIF(targetPathName, gifEmitter, parentKey, childKey, overwrite,
				monitor);
	}

	@Override
	protected void generateJava(String targetPath, String packageName,
			String className, JETEmitter jetEmitter, Object[] arguments,
			Monitor monitor) {
		URI targetDirectory = toURI(targetPath).appendSegments(packageName.split("\\.")); //$NON-NLS-1$
		URI targetFile = targetDirectory.appendSegment(className + ".java"); //$NON-NLS-1$
		if (!myXoclGenerationSettings.isGenerated(new Path(targetFile.toString()))) {
			return;
		}
		super.generateJava(targetPath, packageName, className, jetEmitter, arguments,
				monitor);
	}

	@Override
	protected void generateProperties(String targetPathName,
			JETEmitter jetEmitter, Object[] arguments, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(targetPathName))) {
			return;
		}
		super.generateProperties(targetPathName, jetEmitter, arguments, monitor);
	}

	@Override
	protected void ensureProjectExists(String workspacePath, Object object,
			Object projectType, boolean force, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(workspacePath))) {
			return;
		}
		super.ensureProjectExists(workspacePath, object, projectType, force, monitor);
	}

	@Override
	protected void ensureContainerExists(URI workspacePath, Monitor monitor) {
		if (!myXoclGenerationSettings.isGenerated(new Path(workspacePath.toString()))) {
			return;
		}
		super.ensureContainerExists(workspacePath, monitor);
	}

	@Override
	protected void generateText(String targetPathName, JETEmitter jetEmitter,
			Object[] arguments, boolean overwrite, String encoding,
			Monitor monitor) {
	    IPath path = new Path(targetPathName);
	    if (!myXoclGenerationSettings.isGenerated(path)) {
	    	return;
	    }
		if (targetPathName.endsWith("plugin.xml")) { //$NON-NLS-1$
			generateTextWithMerger(targetPathName, jetEmitter, arguments, overwrite, encoding, monitor, new PluginXMLTextMerger());
		} else if (targetPathName.endsWith("/META-INF/MANIFEST.MF")) { //$NON-NLS-1$
			generateTextWithMerger(targetPathName, jetEmitter, arguments, overwrite, encoding, monitor, new ManifestTextMerger());
		} else {
			super.generateText(targetPathName, jetEmitter, arguments, overwrite, encoding, monitor);
		}
	}

	protected void generateTextWithMerger(String targetPathName, JETEmitter jetEmitter, Object[] arguments, boolean overwrite,
			String encoding, Monitor monitor, ITextMerger textMerger) {
		try {
			monitor.beginTask("", 3); //$NON-NLS-1$

			URI targetFile = toURI(targetPathName);
			monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_GeneratingFile_message", new Object[] { targetFile })); //$NON-NLS-1$

			URI targetDirectory = targetFile.trimSegments(1);
			ensureContainerExists(targetDirectory, createMonitor(monitor, 1));

			boolean exists = exists(targetFile);

			if (!exists || overwrite) {
				super.generateText(targetPathName, jetEmitter, arguments, overwrite, encoding, monitor);
				return;
			}

			if (arguments == null) {
				arguments = new Object[] { generatingObject };
			}
			setLineDelimiter(getLineDelimiter(targetFile, encoding));
			String emitterResult = jetEmitter.generate(createMonitor(monitor, 1), arguments, getLineDelimiter());

			if (PROPERTIES_ENCODING.equals(encoding)) {
				emitterResult = CodeGenUtil.unicodeEscapeEncode(emitterResult);
			}

			if (encoding == null) {
				encoding = getEncoding(targetFile);
			}

			if (exists) {
				monitor.subTask(CodeGenEcorePlugin.INSTANCE.getString("_UI_ExaminingOld_message", new Object[] { targetFile })); //$NON-NLS-1$
				String oldContents = getContents(targetFile, encoding);
				if (!emitterResult.equals(oldContents)) {
					String result = textMerger.process(oldContents, emitterResult);

					if (!result.equals(oldContents)) {
						byte[] bytes = encoding == null ? result.toString().getBytes() : result.toString().getBytes(encoding);
						OutputStream outputStream = createOutputStream(targetFile);
						outputStream.write(bytes);
						outputStream.close();
					}
				}
			}
		} catch (Exception exception) {
			CodeGenEcorePlugin.INSTANCE.log(exception);
		} finally {
			setLineDelimiter(null);
			monitor.done();
		}
	}
}