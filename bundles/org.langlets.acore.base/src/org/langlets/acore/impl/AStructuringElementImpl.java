/**
 */
package org.langlets.acore.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.AAbstractFolder;
import org.langlets.acore.APackage;
import org.langlets.acore.AStructuringElement;
import org.langlets.acore.AcorePackage;

import org.langlets.acore.abstractions.impl.ANamedImpl;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AStructuring Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.impl.AStructuringElementImpl#getAContainingFolder <em>AContaining Folder</em>}</li>
 *   <li>{@link org.langlets.acore.impl.AStructuringElementImpl#getAUri <em>AUri</em>}</li>
 *   <li>{@link org.langlets.acore.impl.AStructuringElementImpl#getAAllPackages <em>AAll Packages</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AStructuringElementImpl extends ANamedImpl implements AStructuringElement {
	/**
	 * The default value of the '{@link #getAUri() <em>AUri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri()
	 * @generated
	 * @ordered
	 */
	protected static final String AURI_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingFolder <em>AContaining Folder</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUri <em>AUri</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUri
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUriDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllPackages <em>AAll Packages</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllPackages
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllPackagesDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AStructuringElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcorePackage.Literals.ASTRUCTURING_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAbstractFolder getAContainingFolder() {
		AAbstractFolder aContainingFolder = basicGetAContainingFolder();
		return aContainingFolder != null && aContainingFolder.eIsProxy()
				? (AAbstractFolder) eResolveProxy((InternalEObject) aContainingFolder) : aContainingFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAbstractFolder basicGetAContainingFolder() {
		/**
		 * @OCL let chain: ecore::EObject = eContainer() in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::AAbstractFolder)
		then chain.oclAsType(acore::AAbstractFolder)
		else null
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ASTRUCTURING_ELEMENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER;

		if (aContainingFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ASTRUCTURING_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ASTRUCTURING_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAbstractFolder result = (AAbstractFolder) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUri() {
		/**
		 * @OCL let nl: String = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ASTRUCTURING_ELEMENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AURI;

		if (aUriDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUriDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ASTRUCTURING_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUriDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ASTRUCTURING_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<APackage> getAAllPackages() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ASTRUCTURING_ELEMENT;
		EStructuralFeature eFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AALL_PACKAGES;

		if (aAllPackagesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllPackagesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ASTRUCTURING_ELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllPackagesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ASTRUCTURING_ELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<APackage> result = (EList<APackage>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AcorePackage.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER:
			if (resolve)
				return getAContainingFolder();
			return basicGetAContainingFolder();
		case AcorePackage.ASTRUCTURING_ELEMENT__AURI:
			return getAUri();
		case AcorePackage.ASTRUCTURING_ELEMENT__AALL_PACKAGES:
			return getAAllPackages();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AcorePackage.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER:
			return basicGetAContainingFolder() != null;
		case AcorePackage.ASTRUCTURING_ELEMENT__AURI:
			return AURI_EDEFAULT == null ? getAUri() != null : !AURI_EDEFAULT.equals(getAUri());
		case AcorePackage.ASTRUCTURING_ELEMENT__AALL_PACKAGES:
			return !getAAllPackages().isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AStructuringElementImpl
