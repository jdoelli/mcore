/**
 */
package org.langlets.acore.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import org.langlets.acore.*;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.langlets.acore.AcorePackage
 * @generated
 */
public class AcoreAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AcorePackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcoreAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = AcorePackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AcoreSwitch<Adapter> modelSwitch = new AcoreSwitch<Adapter>() {
		@Override
		public Adapter caseAStructuringElement(AStructuringElement object) {
			return createAStructuringElementAdapter();
		}

		@Override
		public Adapter caseAAbstractFolder(AAbstractFolder object) {
			return createAAbstractFolderAdapter();
		}

		@Override
		public Adapter caseAComponent(AComponent object) {
			return createAComponentAdapter();
		}

		@Override
		public Adapter caseAFolder(AFolder object) {
			return createAFolderAdapter();
		}

		@Override
		public Adapter caseAPackage(APackage object) {
			return createAPackageAdapter();
		}

		@Override
		public Adapter caseAResource(AResource object) {
			return createAResourceAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseANamed(ANamed object) {
			return createANamedAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.AStructuringElement <em>AStructuring Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.AStructuringElement
	 * @generated
	 */
	public Adapter createAStructuringElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.AAbstractFolder <em>AAbstract Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.AAbstractFolder
	 * @generated
	 */
	public Adapter createAAbstractFolderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.AComponent <em>AComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.AComponent
	 * @generated
	 */
	public Adapter createAComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.AFolder <em>AFolder</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.AFolder
	 * @generated
	 */
	public Adapter createAFolderAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.APackage <em>APackage</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.APackage
	 * @generated
	 */
	public Adapter createAPackageAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.AResource <em>AResource</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.AResource
	 * @generated
	 */
	public Adapter createAResourceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.langlets.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.langlets.acore.abstractions.ANamed
	 * @generated
	 */
	public Adapter createANamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //AcoreAdapterFactory
