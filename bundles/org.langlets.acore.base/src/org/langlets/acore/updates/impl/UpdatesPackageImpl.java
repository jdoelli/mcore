/**
 */
package org.langlets.acore.updates.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.acore.AcorePackage;

import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.abstractions.impl.AbstractionsPackageImpl;

import org.langlets.acore.classifiers.ClassifiersPackage;

import org.langlets.acore.classifiers.impl.ClassifiersPackageImpl;

import org.langlets.acore.impl.AcorePackageImpl;

import org.langlets.acore.updates.AUpdate;
import org.langlets.acore.updates.AUpdateMode;
import org.langlets.acore.updates.UpdatesFactory;
import org.langlets.acore.updates.UpdatesPackage;

import org.langlets.acore.values.ValuesPackage;

import org.langlets.acore.values.impl.ValuesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UpdatesPackageImpl extends EPackageImpl implements UpdatesPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aUpdateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum aUpdateModeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.langlets.acore.updates.UpdatesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UpdatesPackageImpl() {
		super(eNS_URI, UpdatesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UpdatesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UpdatesPackage init() {
		if (isInited)
			return (UpdatesPackage) EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI);

		// Obtain or create and register package
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof UpdatesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new UpdatesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);

		// Create package meta-data objects
		theUpdatesPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theValuesPackage.createPackageContents();

		// Initialize created meta-data
		theUpdatesPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUpdatesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UpdatesPackage.eNS_URI, theUpdatesPackage);
		return theUpdatesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAUpdate() {
		return aUpdateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAUpdate_AObject() {
		return (EReference) aUpdateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAUpdate_AUpdateMode() {
		return (EAttribute) aUpdateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAUpdate_AValue() {
		return (EReference) aUpdateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAUpdate_ADummy2() {
		return (EAttribute) aUpdateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getAUpdateMode() {
		return aUpdateModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpdatesFactory getUpdatesFactory() {
		return (UpdatesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aUpdateEClass = createEClass(AUPDATE);
		createEReference(aUpdateEClass, AUPDATE__AOBJECT);
		createEAttribute(aUpdateEClass, AUPDATE__AUPDATE_MODE);
		createEReference(aUpdateEClass, AUPDATE__AVALUE);
		createEAttribute(aUpdateEClass, AUPDATE__ADUMMY2);

		// Create enums
		aUpdateModeEEnum = createEEnum(AUPDATE_MODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ValuesPackage theValuesPackage = (ValuesPackage) EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(aUpdateEClass, AUpdate.class, "AUpdate", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAUpdate_AObject(), theValuesPackage.getAObject(), null, "aObject", null, 1, 1, AUpdate.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAUpdate_AUpdateMode(), this.getAUpdateMode(), "aUpdateMode", null, 0, 1, AUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAUpdate_AValue(), theValuesPackage.getAValue(), null, "aValue", null, 0, -1, AUpdate.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAUpdate_ADummy2(), ecorePackage.getEString(), "aDummy2", null, 0, 1, AUpdate.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(aUpdateModeEEnum, AUpdateMode.class, "AUpdateMode");
		addEEnumLiteral(aUpdateModeEEnum, AUpdateMode.REDEFINE);
		addEEnumLiteral(aUpdateModeEEnum, AUpdateMode.ADD);
		addEEnumLiteral(aUpdateModeEEnum, AUpdateMode.REMOVE);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getAUpdate_AObject(), source,
				new String[] { "derive", "let nl: acore::values::AObject = null in nl\n" });
		addAnnotation(getAUpdate_AValue(), source, new String[] { "derive", "OrderedSet{}\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getAUpdate_AObject(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core" });
		addAnnotation(getAUpdate_AUpdateMode(), source,
				new String[] { "propertyCategory", "80 A Core", "createColumn", "false" });
		addAnnotation(getAUpdate_AValue(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core" });
		addAnnotation(getAUpdate_ADummy2(), source,
				new String[] { "propertyCategory", "80 A Core", "createColumn", "false" });
	}

} //UpdatesPackageImpl
