/**
 */
package org.langlets.acore.updates.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.updates.AUpdate;
import org.langlets.acore.updates.AUpdateMode;
import org.langlets.acore.updates.UpdatesPackage;

import org.langlets.acore.values.AObject;
import org.langlets.acore.values.AValue;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AUpdate</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.updates.impl.AUpdateImpl#getAObject <em>AObject</em>}</li>
 *   <li>{@link org.langlets.acore.updates.impl.AUpdateImpl#getAUpdateMode <em>AUpdate Mode</em>}</li>
 *   <li>{@link org.langlets.acore.updates.impl.AUpdateImpl#getAValue <em>AValue</em>}</li>
 *   <li>{@link org.langlets.acore.updates.impl.AUpdateImpl#getADummy2 <em>ADummy2</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AUpdateImpl extends MinimalEObjectImpl.Container implements AUpdate {
	/**
	 * The default value of the '{@link #getAUpdateMode() <em>AUpdate Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUpdateMode()
	 * @generated
	 * @ordered
	 */
	protected static final AUpdateMode AUPDATE_MODE_EDEFAULT = AUpdateMode.REDEFINE;

	/**
	 * The cached value of the '{@link #getAUpdateMode() <em>AUpdate Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUpdateMode()
	 * @generated
	 * @ordered
	 */
	protected AUpdateMode aUpdateMode = AUPDATE_MODE_EDEFAULT;

	/**
	 * This is true if the AUpdate Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aUpdateModeESet;

	/**
	 * The default value of the '{@link #getADummy2() <em>ADummy2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADummy2()
	 * @generated
	 * @ordered
	 */
	protected static final String ADUMMY2_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getADummy2() <em>ADummy2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADummy2()
	 * @generated
	 * @ordered
	 */
	protected String aDummy2 = ADUMMY2_EDEFAULT;

	/**
	 * This is true if the ADummy2 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aDummy2ESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAObject <em>AObject</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAValue <em>AValue</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aValueDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AUpdateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UpdatesPackage.Literals.AUPDATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject getAObject() {
		AObject aObject = basicGetAObject();
		return aObject != null && aObject.eIsProxy() ? (AObject) eResolveProxy((InternalEObject) aObject) : aObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject basicGetAObject() {
		/**
		 * @OCL let nl: acore::values::AObject = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = UpdatesPackage.Literals.AUPDATE;
		EStructuralFeature eFeature = UpdatesPackage.Literals.AUPDATE__AOBJECT;

		if (aObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(UpdatesPackage.PLUGIN_ID, derive, helper.getProblems(),
						UpdatesPackage.Literals.AUPDATE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(UpdatesPackage.PLUGIN_ID, query, UpdatesPackage.Literals.AUPDATE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AObject result = (AObject) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AUpdateMode getAUpdateMode() {
		return aUpdateMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAUpdateMode(AUpdateMode newAUpdateMode) {
		AUpdateMode oldAUpdateMode = aUpdateMode;
		aUpdateMode = newAUpdateMode == null ? AUPDATE_MODE_EDEFAULT : newAUpdateMode;
		boolean oldAUpdateModeESet = aUpdateModeESet;
		aUpdateModeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UpdatesPackage.AUPDATE__AUPDATE_MODE, oldAUpdateMode,
					aUpdateMode, !oldAUpdateModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAUpdateMode() {
		AUpdateMode oldAUpdateMode = aUpdateMode;
		boolean oldAUpdateModeESet = aUpdateModeESet;
		aUpdateMode = AUPDATE_MODE_EDEFAULT;
		aUpdateModeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, UpdatesPackage.AUPDATE__AUPDATE_MODE,
					oldAUpdateMode, AUPDATE_MODE_EDEFAULT, oldAUpdateModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAUpdateMode() {
		return aUpdateModeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AValue> getAValue() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = UpdatesPackage.Literals.AUPDATE;
		EStructuralFeature eFeature = UpdatesPackage.Literals.AUPDATE__AVALUE;

		if (aValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(UpdatesPackage.PLUGIN_ID, derive, helper.getProblems(),
						UpdatesPackage.Literals.AUPDATE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(UpdatesPackage.PLUGIN_ID, query, UpdatesPackage.Literals.AUPDATE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AValue> result = (EList<AValue>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getADummy2() {
		return aDummy2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setADummy2(String newADummy2) {
		String oldADummy2 = aDummy2;
		aDummy2 = newADummy2;
		boolean oldADummy2ESet = aDummy2ESet;
		aDummy2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, UpdatesPackage.AUPDATE__ADUMMY2, oldADummy2, aDummy2,
					!oldADummy2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetADummy2() {
		String oldADummy2 = aDummy2;
		boolean oldADummy2ESet = aDummy2ESet;
		aDummy2 = ADUMMY2_EDEFAULT;
		aDummy2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, UpdatesPackage.AUPDATE__ADUMMY2, oldADummy2,
					ADUMMY2_EDEFAULT, oldADummy2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetADummy2() {
		return aDummy2ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case UpdatesPackage.AUPDATE__AOBJECT:
			if (resolve)
				return getAObject();
			return basicGetAObject();
		case UpdatesPackage.AUPDATE__AUPDATE_MODE:
			return getAUpdateMode();
		case UpdatesPackage.AUPDATE__AVALUE:
			return getAValue();
		case UpdatesPackage.AUPDATE__ADUMMY2:
			return getADummy2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case UpdatesPackage.AUPDATE__AUPDATE_MODE:
			setAUpdateMode((AUpdateMode) newValue);
			return;
		case UpdatesPackage.AUPDATE__ADUMMY2:
			setADummy2((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case UpdatesPackage.AUPDATE__AUPDATE_MODE:
			unsetAUpdateMode();
			return;
		case UpdatesPackage.AUPDATE__ADUMMY2:
			unsetADummy2();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case UpdatesPackage.AUPDATE__AOBJECT:
			return basicGetAObject() != null;
		case UpdatesPackage.AUPDATE__AUPDATE_MODE:
			return isSetAUpdateMode();
		case UpdatesPackage.AUPDATE__AVALUE:
			return !getAValue().isEmpty();
		case UpdatesPackage.AUPDATE__ADUMMY2:
			return isSetADummy2();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (aUpdateMode: ");
		if (aUpdateModeESet)
			result.append(aUpdateMode);
		else
			result.append("<unset>");
		result.append(", aDummy2: ");
		if (aDummy2ESet)
			result.append(aDummy2);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //AUpdateImpl
