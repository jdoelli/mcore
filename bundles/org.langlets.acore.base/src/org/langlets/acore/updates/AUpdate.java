/**
 */
package org.langlets.acore.updates;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.langlets.acore.values.AObject;
import org.langlets.acore.values.AValue;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AUpdate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.updates.AUpdate#getAObject <em>AObject</em>}</li>
 *   <li>{@link org.langlets.acore.updates.AUpdate#getAUpdateMode <em>AUpdate Mode</em>}</li>
 *   <li>{@link org.langlets.acore.updates.AUpdate#getAValue <em>AValue</em>}</li>
 *   <li>{@link org.langlets.acore.updates.AUpdate#getADummy2 <em>ADummy2</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.updates.UpdatesPackage#getAUpdate()
 * @model abstract="true"
 * @generated
 */

public interface AUpdate extends EObject {
	/**
	 * Returns the value of the '<em><b>AObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AObject</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AObject</em>' reference.
	 * @see org.langlets.acore.updates.UpdatesPackage#getAUpdate_AObject()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::values::AObject = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 * @generated
	 */
	AObject getAObject();

	/**
	 * Returns the value of the '<em><b>AUpdate Mode</b></em>' attribute.
	 * The literals are from the enumeration {@link org.langlets.acore.updates.AUpdateMode}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUpdate Mode</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUpdate Mode</em>' attribute.
	 * @see org.langlets.acore.updates.AUpdateMode
	 * @see #isSetAUpdateMode()
	 * @see #unsetAUpdateMode()
	 * @see #setAUpdateMode(AUpdateMode)
	 * @see org.langlets.acore.updates.UpdatesPackage#getAUpdate_AUpdateMode()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core' createColumn='false'"
	 * @generated
	 */
	AUpdateMode getAUpdateMode();

	/** 
	 * Sets the value of the '{@link org.langlets.acore.updates.AUpdate#getAUpdateMode <em>AUpdate Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>AUpdate Mode</em>' attribute.
	 * @see org.langlets.acore.updates.AUpdateMode
	 * @see #isSetAUpdateMode()
	 * @see #unsetAUpdateMode()
	 * @see #getAUpdateMode()
	 * @generated
	 */
	void setAUpdateMode(AUpdateMode value);

	/**
	 * Unsets the value of the '{@link org.langlets.acore.updates.AUpdate#getAUpdateMode <em>AUpdate Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAUpdateMode()
	 * @see #getAUpdateMode()
	 * @see #setAUpdateMode(AUpdateMode)
	 * @generated
	 */
	void unsetAUpdateMode();

	/**
	 * Returns whether the value of the '{@link org.langlets.acore.updates.AUpdate#getAUpdateMode <em>AUpdate Mode</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>AUpdate Mode</em>' attribute is set.
	 * @see #unsetAUpdateMode()
	 * @see #getAUpdateMode()
	 * @see #setAUpdateMode(AUpdateMode)
	 * @generated
	 */
	boolean isSetAUpdateMode();

	/**
	 * Returns the value of the '<em><b>AValue</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.values.AValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AValue</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AValue</em>' reference list.
	 * @see org.langlets.acore.updates.UpdatesPackage#getAUpdate_AValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 * @generated
	 */
	EList<AValue> getAValue();

	/**
	 * Returns the value of the '<em><b>ADummy2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ADummy2</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADummy2</em>' attribute.
	 * @see #isSetADummy2()
	 * @see #unsetADummy2()
	 * @see #setADummy2(String)
	 * @see org.langlets.acore.updates.UpdatesPackage#getAUpdate_ADummy2()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core' createColumn='false'"
	 * @generated
	 */
	String getADummy2();

	/** 
	 * Sets the value of the '{@link org.langlets.acore.updates.AUpdate#getADummy2 <em>ADummy2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ADummy2</em>' attribute.
	 * @see #isSetADummy2()
	 * @see #unsetADummy2()
	 * @see #getADummy2()
	 * @generated
	 */
	void setADummy2(String value);

	/**
	 * Unsets the value of the '{@link org.langlets.acore.updates.AUpdate#getADummy2 <em>ADummy2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetADummy2()
	 * @see #getADummy2()
	 * @see #setADummy2(String)
	 * @generated
	 */
	void unsetADummy2();

	/**
	 * Returns whether the value of the '{@link org.langlets.acore.updates.AUpdate#getADummy2 <em>ADummy2</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ADummy2</em>' attribute is set.
	 * @see #unsetADummy2()
	 * @see #getADummy2()
	 * @see #setADummy2(String)
	 * @generated
	 */
	boolean isSetADummy2();

} // AUpdate
