/**
 */
package org.langlets.acore.classifiers;

import org.langlets.acore.abstractions.AElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ATyped</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.ATyped#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.ATyped#getAMandatory <em>AMandatory</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.ATyped#getASingular <em>ASingular</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.ATyped#getATypeLabel <em>AType Label</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.ATyped#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aLabelDerive='aTypeLabel\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aLabelCreateColumn='true'"
 * @generated
 */

public interface ATyped extends AElement {
	/**
	 * Returns the value of the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClassifier</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped_AClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getAClassifier();

	/**
	 * Returns the value of the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AMandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AMandatory</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped_AMandatory()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAMandatory();

	/**
	 * Returns the value of the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASingular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASingular</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped_ASingular()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getASingular();

	/**
	 * Returns the value of the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AType Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AType Label</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped_ATypeLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = aClassifier = null in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in \n if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif\n  else (let e0: String = if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif.concat(\'*\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getATypeLabel();

	/**
	 * Returns the value of the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUndefined Type Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUndefined Type Constant</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getATyped_AUndefinedTypeConstant()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'<A Type Is Undefined>\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUndefinedTypeConstant();

} // ATyped
