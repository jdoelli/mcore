/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.APackage;

import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AClassifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getASpecializedClassifier <em>ASpecialized Classifier</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAActiveDataType <em>AActive Data Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAActiveEnumeration <em>AActive Enumeration</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAActiveClass <em>AActive Class</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAsDataType <em>As Data Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAsClass <em>As Class</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AClassifier#getAIsStringClassifier <em>AIs String Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier()
 * @model abstract="true"
 * @generated
 */

public interface AClassifier extends ANamed {
	/**
	 * Returns the value of the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecialized Classifier</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecialized Classifier</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_ASpecializedClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AClassifier> getASpecializedClassifier();

	/**
	 * Returns the value of the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Data Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Data Type</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveDataType();

	/**
	 * Returns the value of the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Enumeration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Enumeration</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveEnumeration()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveEnumeration();

	/**
	 * Returns the value of the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Class</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AActiveClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveClass();

	/**
	 * Returns the value of the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Package</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AContainingPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::APackage = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getAContainingPackage();

	/**
	 * Returns the value of the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Data Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Data Type</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AsDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = if (aActiveDataType)= true \n then true \n else if (aActiveEnumeration)= true \n then true \nelse if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::ADataType)\n    then chain.oclAsType(acore::classifiers::ADataType)\n    else null\n  endif\n  endif\n  else null\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ADataType getAsDataType();

	/**
	 * Returns the value of the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Class</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AsClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (aActiveClass) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClass)\n    then chain.oclAsType(acore::classifiers::AClass)\n    else null\n  endif\n  endif\n  else null\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClass getAsClass();

	/**
	 * Returns the value of the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AIs String Classifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AIs String Classifier</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAClassifier_AIsStringClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif))= false \n then false \n else if (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= false \n then false \nelse if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Package'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAIsStringClassifier();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if (let e0: Boolean = self = aClassifier in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else let e0: Boolean = aSpecializedClassifier.aAssignableTo(aClassifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in \n if e0.oclIsInvalid() then null else e0 endif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Classifiers' createColumn='true'"
	 * @generated
	 */
	Boolean aAssignableTo(AClassifier aClassifier);

} // AClassifier
