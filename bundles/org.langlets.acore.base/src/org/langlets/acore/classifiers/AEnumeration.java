/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.values.ALiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AEnumeration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AEnumeration#getALiteral <em>ALiteral</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAEnumeration()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aActiveEnumerationDerive='true\n' aActiveDataTypeDerive='false\n'"
 * @generated
 */

public interface AEnumeration extends ADataType {
	/**
	 * Returns the value of the '<em><b>ALiteral</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.values.ALiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALiteral</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALiteral</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAEnumeration_ALiteral()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<ALiteral> getALiteral();

} // AEnumeration
