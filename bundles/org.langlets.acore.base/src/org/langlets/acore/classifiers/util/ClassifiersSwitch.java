/**
 */
package org.langlets.acore.classifiers.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;

import org.langlets.acore.classifiers.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see org.langlets.acore.classifiers.ClassifiersPackage
 * @generated
 */
public class ClassifiersSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ClassifiersPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersSwitch() {
		if (modelPackage == null) {
			modelPackage = ClassifiersPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case ClassifiersPackage.ACLASSIFIER: {
			AClassifier aClassifier = (AClassifier) theEObject;
			T result = caseAClassifier(aClassifier);
			if (result == null)
				result = caseANamed(aClassifier);
			if (result == null)
				result = caseAElement(aClassifier);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.ADATA_TYPE: {
			ADataType aDataType = (ADataType) theEObject;
			T result = caseADataType(aDataType);
			if (result == null)
				result = caseAClassifier(aDataType);
			if (result == null)
				result = caseANamed(aDataType);
			if (result == null)
				result = caseAElement(aDataType);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.AENUMERATION: {
			AEnumeration aEnumeration = (AEnumeration) theEObject;
			T result = caseAEnumeration(aEnumeration);
			if (result == null)
				result = caseADataType(aEnumeration);
			if (result == null)
				result = caseAClassifier(aEnumeration);
			if (result == null)
				result = caseANamed(aEnumeration);
			if (result == null)
				result = caseAElement(aEnumeration);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.ACLASS: {
			AClass aClass = (AClass) theEObject;
			T result = caseAClass(aClass);
			if (result == null)
				result = caseAClassifier(aClass);
			if (result == null)
				result = caseANamed(aClass);
			if (result == null)
				result = caseAElement(aClass);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.ATYPED: {
			ATyped aTyped = (ATyped) theEObject;
			T result = caseATyped(aTyped);
			if (result == null)
				result = caseAElement(aTyped);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.AFEATURE: {
			AFeature aFeature = (AFeature) theEObject;
			T result = caseAFeature(aFeature);
			if (result == null)
				result = caseATyped(aFeature);
			if (result == null)
				result = caseANamed(aFeature);
			if (result == null)
				result = caseAElement(aFeature);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.AATTRIBUTE: {
			AAttribute aAttribute = (AAttribute) theEObject;
			T result = caseAAttribute(aAttribute);
			if (result == null)
				result = caseAFeature(aAttribute);
			if (result == null)
				result = caseATyped(aAttribute);
			if (result == null)
				result = caseANamed(aAttribute);
			if (result == null)
				result = caseAElement(aAttribute);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.AREFERENCE: {
			AReference aReference = (AReference) theEObject;
			T result = caseAReference(aReference);
			if (result == null)
				result = caseAFeature(aReference);
			if (result == null)
				result = caseATyped(aReference);
			if (result == null)
				result = caseANamed(aReference);
			if (result == null)
				result = caseAElement(aReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.AOPERATION: {
			AOperation aOperation = (AOperation) theEObject;
			T result = caseAOperation(aOperation);
			if (result == null)
				result = caseATyped(aOperation);
			if (result == null)
				result = caseANamed(aOperation);
			if (result == null)
				result = caseAElement(aOperation);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ClassifiersPackage.APARAMETER: {
			AParameter aParameter = (AParameter) theEObject;
			T result = caseAParameter(aParameter);
			if (result == null)
				result = caseATyped(aParameter);
			if (result == null)
				result = caseANamed(aParameter);
			if (result == null)
				result = caseAElement(aParameter);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AClassifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AClassifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAClassifier(AClassifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AData Type</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AData Type</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseADataType(ADataType object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AEnumeration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AEnumeration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAEnumeration(AEnumeration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AClass</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AClass</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAClass(AClass object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ATyped</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ATyped</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseATyped(ATyped object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AFeature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AFeature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAFeature(AFeature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AAttribute</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AAttribute</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAAttribute(AAttribute object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AReference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AReference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAReference(AReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AOperation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AOperation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAOperation(AOperation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AParameter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AParameter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAParameter(AParameter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAElement(AElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseANamed(ANamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ClassifiersSwitch
