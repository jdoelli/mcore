/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.APackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AData Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.ADataType#getASpecializedDataType <em>ASpecialized Data Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.ADataType#getADataTypePackage <em>AData Type Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getADataType()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aActiveDataTypeDerive='true\n' aSpecializedClassifierDerive='aSpecializedDataType->asOrderedSet()\n'"
 * @generated
 */

public interface ADataType extends AClassifier {
	/**
	 * Returns the value of the '<em><b>ASpecialized Data Type</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.ADataType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecialized Data Type</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecialized Data Type</em>' reference list.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getADataType_ASpecializedDataType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<ADataType> getASpecializedDataType();

	/**
	 * Returns the value of the '<em><b>AData Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AData Type Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AData Type Package</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getADataType_ADataTypePackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='aContainingPackage\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getADataTypePackage();

} // ADataType
