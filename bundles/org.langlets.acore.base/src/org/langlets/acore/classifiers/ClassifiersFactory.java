/**
 */
package org.langlets.acore.classifiers;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see org.langlets.acore.classifiers.ClassifiersPackage
 * @generated
 */
public interface ClassifiersFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ClassifiersFactory eINSTANCE = org.langlets.acore.classifiers.impl.ClassifiersFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>AReference</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>AReference</em>'.
	 * @generated
	 */
	AReference createAReference();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ClassifiersPackage getClassifiersPackage();

} //ClassifiersFactory
