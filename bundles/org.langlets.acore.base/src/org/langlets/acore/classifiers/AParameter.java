/**
 */
package org.langlets.acore.classifiers;

import org.langlets.acore.abstractions.ANamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AParameter</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAParameter()
 * @model abstract="true"
 * @generated
 */

public interface AParameter extends ATyped, ANamed {
} // AParameter
