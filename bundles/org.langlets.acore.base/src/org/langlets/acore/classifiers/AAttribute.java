/**
 */
package org.langlets.acore.classifiers;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AAttribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.AAttribute#getADataType <em>AData Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.AAttribute#getAActiveAttribute <em>AActive Attribute</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAAttribute()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aDataType\n' aActiveFeatureDerive='aActiveAttribute\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aClassifierCreateColumn='false'"
 * @generated
 */

public interface AAttribute extends AFeature {
	/**
	 * Returns the value of the '<em><b>AData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AData Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AData Type</em>' reference.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAAttribute_ADataType()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::ADataType = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ADataType getADataType();

	/**
	 * Returns the value of the '<em><b>AActive Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Attribute</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Attribute</em>' attribute.
	 * @see org.langlets.acore.classifiers.ClassifiersPackage#getAAttribute_AActiveAttribute()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='80 A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveAttribute();

} // AAttribute
