/**
 */
package org.langlets.acore.classifiers.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.APackage;

import org.langlets.acore.abstractions.impl.ANamedImpl;

import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.ADataType;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AClassifier</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getASpecializedClassifier <em>ASpecialized Classifier</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAActiveDataType <em>AActive Data Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAActiveEnumeration <em>AActive Enumeration</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAActiveClass <em>AActive Class</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAsDataType <em>As Data Type</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAsClass <em>As Class</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.AClassifierImpl#getAIsStringClassifier <em>AIs String Classifier</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AClassifierImpl extends ANamedImpl implements AClassifier {
	/**
	 * The default value of the '{@link #getAActiveDataType() <em>AActive Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveDataType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_DATA_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveEnumeration() <em>AActive Enumeration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveEnumeration()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_ENUMERATION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveClass() <em>AActive Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveClass()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_CLASS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAIsStringClassifier() <em>AIs String Classifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsStringClassifier()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AIS_STRING_CLASSIFIER_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #aAssignableTo <em>AAssignable To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aAssignableTo
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aAssignableToclassifiersAClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASpecializedClassifier <em>ASpecialized Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecializedClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSpecializedClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveDataType <em>AActive Data Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveEnumeration <em>AActive Enumeration</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveEnumeration
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveEnumerationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveClass <em>AActive Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingPackage <em>AContaining Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsDataType <em>As Data Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsClass <em>As Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAIsStringClassifier <em>AIs String Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsStringClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aIsStringClassifierDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AClassifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ACLASSIFIER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassifier> getASpecializedClassifier() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

		if (aSpecializedClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSpecializedClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSpecializedClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassifier> result = (EList<AClassifier>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveDataType() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_DATA_TYPE;

		if (aActiveDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveEnumeration() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_ENUMERATION;

		if (aActiveEnumerationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveEnumerationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveEnumerationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveClass() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_CLASS;

		if (aActiveClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getAContainingPackage() {
		APackage aContainingPackage = basicGetAContainingPackage();
		return aContainingPackage != null && aContainingPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aContainingPackage) : aContainingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetAContainingPackage() {
		/**
		 * @OCL let nl: acore::APackage = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__ACONTAINING_PACKAGE;

		if (aContainingPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType getAsDataType() {
		ADataType asDataType = basicGetAsDataType();
		return asDataType != null && asDataType.eIsProxy() ? (ADataType) eResolveProxy((InternalEObject) asDataType)
				: asDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType basicGetAsDataType() {
		/**
		 * @OCL if (let e0: Boolean = if (aActiveDataType)= true 
		then true 
		else if (aActiveEnumeration)= true 
		then true 
		else if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then let chain: acore::classifiers::AClassifier = self in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::ADataType)
		then chain.oclAsType(acore::classifiers::ADataType)
		else null
		endif
		endif
		else null
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AS_DATA_TYPE;

		if (asDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				asDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ADataType result = (ADataType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass getAsClass() {
		AClass asClass = basicGetAsClass();
		return asClass != null && asClass.eIsProxy() ? (AClass) eResolveProxy((InternalEObject) asClass) : asClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass basicGetAsClass() {
		/**
		 * @OCL if (aActiveClass) 
		=true 
		then let chain: acore::classifiers::AClassifier = self in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClass)
		then chain.oclAsType(acore::classifiers::AClass)
		else null
		endif
		endif
		else null
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AS_CLASS;

		if (asClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				asClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClass result = (AClass) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAIsStringClassifier() {
		/**
		 * @OCL let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aUri
		endif = 'http://www.langlets.org/ACoreC/ACore/Classifiers' in 
		if e2.oclIsInvalid() then null else e2 endif))= false 
		then false 
		else if (let e3: Boolean = aName = 'AString' in 
		if e3.oclIsInvalid() then null else e3 endif)= false 
		then false 
		else if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aUri
		endif = 'http://www.langlets.org/ACoreC/ACore/Classifiers' in 
		if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = 'AString' in 
		if e3.oclIsInvalid() then null else e3 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ACLASSIFIER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AIS_STRING_CLASSIFIER;

		if (aIsStringClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aIsStringClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aIsStringClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean aAssignableTo(AClassifier aClassifier) {

		/**
		 * @OCL if (let e0: Boolean = self = aClassifier in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then true
		else let e0: Boolean = aSpecializedClassifier.aAssignableTo(aClassifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in 
		if e0.oclIsInvalid() then null else e0 endif
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASSIFIER);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASSIFIER.getEOperations().get(0);
		if (aAssignableToclassifiersAClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aAssignableToclassifiersAClassifierBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, body, helper.getProblems(),
						ClassifiersPackage.Literals.ACLASSIFIER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aAssignableToclassifiersAClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ACLASSIFIER,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("aClassifier", aClassifier);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ClassifiersPackage.ACLASSIFIER__ASPECIALIZED_CLASSIFIER:
			return getASpecializedClassifier();
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_DATA_TYPE:
			return getAActiveDataType();
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_ENUMERATION:
			return getAActiveEnumeration();
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_CLASS:
			return getAActiveClass();
		case ClassifiersPackage.ACLASSIFIER__ACONTAINING_PACKAGE:
			if (resolve)
				return getAContainingPackage();
			return basicGetAContainingPackage();
		case ClassifiersPackage.ACLASSIFIER__AS_DATA_TYPE:
			if (resolve)
				return getAsDataType();
			return basicGetAsDataType();
		case ClassifiersPackage.ACLASSIFIER__AS_CLASS:
			if (resolve)
				return getAsClass();
			return basicGetAsClass();
		case ClassifiersPackage.ACLASSIFIER__AIS_STRING_CLASSIFIER:
			return getAIsStringClassifier();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.ACLASSIFIER__ASPECIALIZED_CLASSIFIER:
			return !getASpecializedClassifier().isEmpty();
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_DATA_TYPE:
			return AACTIVE_DATA_TYPE_EDEFAULT == null ? getAActiveDataType() != null
					: !AACTIVE_DATA_TYPE_EDEFAULT.equals(getAActiveDataType());
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_ENUMERATION:
			return AACTIVE_ENUMERATION_EDEFAULT == null ? getAActiveEnumeration() != null
					: !AACTIVE_ENUMERATION_EDEFAULT.equals(getAActiveEnumeration());
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_CLASS:
			return AACTIVE_CLASS_EDEFAULT == null ? getAActiveClass() != null
					: !AACTIVE_CLASS_EDEFAULT.equals(getAActiveClass());
		case ClassifiersPackage.ACLASSIFIER__ACONTAINING_PACKAGE:
			return basicGetAContainingPackage() != null;
		case ClassifiersPackage.ACLASSIFIER__AS_DATA_TYPE:
			return basicGetAsDataType() != null;
		case ClassifiersPackage.ACLASSIFIER__AS_CLASS:
			return basicGetAsClass() != null;
		case ClassifiersPackage.ACLASSIFIER__AIS_STRING_CLASSIFIER:
			return AIS_STRING_CLASSIFIER_EDEFAULT == null ? getAIsStringClassifier() != null
					: !AIS_STRING_CLASSIFIER_EDEFAULT.equals(getAIsStringClassifier());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ClassifiersPackage.ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER:
			return aAssignableTo((AClassifier) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AClassifierImpl
