/**
 */
package org.langlets.acore.classifiers.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.abstractions.impl.AElementImpl;

import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.ATyped;
import org.langlets.acore.classifiers.ClassifiersPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ATyped</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.classifiers.impl.ATypedImpl#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.ATypedImpl#getAMandatory <em>AMandatory</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.ATypedImpl#getASingular <em>ASingular</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.ATypedImpl#getATypeLabel <em>AType Label</em>}</li>
 *   <li>{@link org.langlets.acore.classifiers.impl.ATypedImpl#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class ATypedImpl extends AElementImpl implements ATyped {
	/**
	 * The default value of the '{@link #getAMandatory() <em>AMandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AMANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASingular() <em>ASingular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getATypeLabel() <em>AType Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ATYPE_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedTypeConstant() <em>AUndefined Type Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_TYPE_CONSTANT_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMandatory <em>AMandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASingular <em>ASingular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATypeLabel <em>AType Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTypeLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedTypeConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ATypedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ClassifiersPackage.Literals.ATYPED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAClassifier() {
		AClassifier aClassifier = basicGetAClassifier();
		return aClassifier != null && aClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aClassifier) : aClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAClassifier() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ATYPED;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ATYPED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ATYPED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ATYPED;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ATYPED__AMANDATORY;

		if (aMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ATYPED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ATYPED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getASingular() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ATYPED;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ATYPED__ASINGULAR;

		if (aSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ATYPED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ATYPED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATypeLabel() {
		/**
		 * @OCL if (let e0: Boolean = aClassifier = null in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in 
		if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif
		else (let e0: String = if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif.concat('*') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ATYPED;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ATYPED__ATYPE_LABEL;

		if (aTypeLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTypeLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ATYPED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTypeLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ATYPED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedTypeConstant() {
		/**
		 * @OCL '<A Type Is Undefined>'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ClassifiersPackage.Literals.ATYPED;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT;

		if (aUndefinedTypeConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedTypeConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(),
						ClassifiersPackage.Literals.ATYPED, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedTypeConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, ClassifiersPackage.Literals.ATYPED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ClassifiersPackage.ATYPED__ACLASSIFIER:
			if (resolve)
				return getAClassifier();
			return basicGetAClassifier();
		case ClassifiersPackage.ATYPED__AMANDATORY:
			return getAMandatory();
		case ClassifiersPackage.ATYPED__ASINGULAR:
			return getASingular();
		case ClassifiersPackage.ATYPED__ATYPE_LABEL:
			return getATypeLabel();
		case ClassifiersPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT:
			return getAUndefinedTypeConstant();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ClassifiersPackage.ATYPED__ACLASSIFIER:
			return basicGetAClassifier() != null;
		case ClassifiersPackage.ATYPED__AMANDATORY:
			return AMANDATORY_EDEFAULT == null ? getAMandatory() != null : !AMANDATORY_EDEFAULT.equals(getAMandatory());
		case ClassifiersPackage.ATYPED__ASINGULAR:
			return ASINGULAR_EDEFAULT == null ? getASingular() != null : !ASINGULAR_EDEFAULT.equals(getASingular());
		case ClassifiersPackage.ATYPED__ATYPE_LABEL:
			return ATYPE_LABEL_EDEFAULT == null ? getATypeLabel() != null
					: !ATYPE_LABEL_EDEFAULT.equals(getATypeLabel());
		case ClassifiersPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT:
			return AUNDEFINED_TYPE_CONSTANT_EDEFAULT == null ? getAUndefinedTypeConstant() != null
					: !AUNDEFINED_TYPE_CONSTANT_EDEFAULT.equals(getAUndefinedTypeConstant());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aTypeLabel
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (ClassifiersPackage.Literals.ATYPED);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ClassifiersPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ClassifiersPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //ATypedImpl
