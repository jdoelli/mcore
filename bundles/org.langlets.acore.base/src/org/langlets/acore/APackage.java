/**
 */
package org.langlets.acore;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.classifiers.AClassifier;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>APackage</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.APackage#getARootObjectClass <em>ARoot Object Class</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getASubPackage <em>ASub Package</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getASpecialUri <em>ASpecial Uri</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getAActivePackage <em>AActive Package</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getAActiveRootPackage <em>AActive Root Package</em>}</li>
 *   <li>{@link org.langlets.acore.APackage#getAActiveSubPackage <em>AActive Sub Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.AcorePackage#getAPackage()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aUriDerive='let specialUri: String = aSpecialUri in\nif (let e0: Boolean = not(stringIsEmpty(specialUri)) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen specialUri else if (aActiveRootPackage)=true then (let e0: String = if aContainingFolder.oclIsUndefined()\n  then null\n  else aContainingFolder.aUri\nendif.concat(\'/\').concat(aName) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = if aContainingPackage.oclIsUndefined()\n  then null\n  else aContainingPackage.aUri\nendif.concat(\'/\').concat(aName) in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n' aAllPackagesDerive='let e1: OrderedSet(acore::APackage)  = let chain11: acore::APackage = self in\nif chain11->asOrderedSet()->oclIsUndefined() \n then null \n else chain11->asOrderedSet()\n  endif->union(aSubPackage.aAllPackages->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
 * @generated
 */

public interface APackage extends AStructuringElement {
	/**
	 * Returns the value of the '<em><b>ARoot Object Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ARoot Object Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ARoot Object Class</em>' reference.
	 * @see org.langlets.acore.AcorePackage#getAPackage_ARootObjectClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getARootObjectClass();

	/**
	 * Returns the value of the '<em><b>AClassifier</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClassifier</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClassifier</em>' reference list.
	 * @see org.langlets.acore.AcorePackage#getAPackage_AClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AClassifier> getAClassifier();

	/**
	 * Returns the value of the '<em><b>ASub Package</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.APackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASub Package</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASub Package</em>' reference list.
	 * @see org.langlets.acore.AcorePackage#getAPackage_ASubPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<APackage> getASubPackage();

	/**
	 * Returns the value of the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Package</em>' reference.
	 * @see org.langlets.acore.AcorePackage#getAPackage_AContainingPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::APackage = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getAContainingPackage();

	/**
	 * Returns the value of the '<em><b>ASpecial Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASpecial Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASpecial Uri</em>' attribute.
	 * @see org.langlets.acore.AcorePackage#getAPackage_ASpecialUri()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: String = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getASpecialUri();

	/**
	 * Returns the value of the '<em><b>AActive Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Package</em>' attribute.
	 * @see org.langlets.acore.AcorePackage#getAPackage_AActivePackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActivePackage();

	/**
	 * Returns the value of the '<em><b>AActive Root Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Root Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Root Package</em>' attribute.
	 * @see org.langlets.acore.AcorePackage#getAPackage_AActiveRootPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if (aActivePackage)= false \n then false \n else if (let e2: Boolean = aContainingPackage = null in \n if e2.oclIsInvalid() then null else e2 endif)= false \n then false \nelse if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage = null in \n if e2.oclIsInvalid() then null else e2 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveRootPackage();

	/**
	 * Returns the value of the '<em><b>AActive Sub Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Sub Package</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Sub Package</em>' attribute.
	 * @see org.langlets.acore.AcorePackage#getAPackage_AActiveSubPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if (aActivePackage)= false \n then false \n else if (let e2: Boolean = aContainingPackage <> null in \n if e2.oclIsInvalid() then null else e2 endif)= false \n then false \nelse if ((aActivePackage)= null or (let e2: Boolean = aContainingPackage <> null in \n if e2.oclIsInvalid() then null else e2 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveSubPackage();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let classifiers: OrderedSet(acore::classifiers::AClassifier)  = aClassifier->asOrderedSet()->select(it: acore::classifiers::AClassifier | let e0: Boolean = it.aName = classifierName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(acore::classifiers::AClassifier)  = classifiers in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core' createColumn='true'"
	 * @generated
	 */
	AClassifier aClassifierFromName(String classifierName);

} // APackage
