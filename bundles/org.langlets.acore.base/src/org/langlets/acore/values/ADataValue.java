/**
 */
package org.langlets.acore.values;

import org.langlets.acore.classifiers.ADataType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AData Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.values.ADataValue#getAValue <em>AValue</em>}</li>
 *   <li>{@link org.langlets.acore.values.ADataValue#getADataType <em>AData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.values.ValuesPackage#getADataValue()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aDataType\n'"
 * @generated
 */

public interface ADataValue extends AValue {
	/**
	 * Returns the value of the '<em><b>AValue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AValue</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AValue</em>' attribute.
	 * @see org.langlets.acore.values.ValuesPackage#getADataValue_AValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'TO BE DONE\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Values'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAValue();

	/**
	 * Returns the value of the '<em><b>AData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AData Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AData Type</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getADataValue_ADataType()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::ADataType = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Values'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ADataType getADataType();

} // ADataValue
