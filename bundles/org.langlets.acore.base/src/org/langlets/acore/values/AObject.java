/**
 */
package org.langlets.acore.values;

import org.eclipse.emf.common.util.EList;

import org.langlets.acore.AResource;

import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.AFeature;
import org.langlets.acore.classifiers.AReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AObject</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.values.AObject#getAClass <em>AClass</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getASlot <em>ASlot</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getAContainingObject <em>AContaining Object</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getAContainmentReference <em>AContainment Reference</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getAContainingResource <em>AContaining Resource</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getAFeatureWithoutSlot <em>AFeature Without Slot</em>}</li>
 *   <li>{@link org.langlets.acore.values.AObject#getAAllObject <em>AAll Object</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.values.ValuesPackage#getAObject()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aClass\n'"
 * @generated
 */

public interface AObject extends AValue {
	/**
	 * Returns the value of the '<em><b>AClass</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClass</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClass</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClass = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Values'"
	 * @generated
	 */
	AClass getAClass();

	/**
	 * Returns the value of the '<em><b>ASlot</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.values.ASlot}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASlot</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASlot</em>' reference list.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_ASlot()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Values'"
	 * @generated
	 */
	EList<ASlot> getASlot();

	/**
	 * Returns the value of the '<em><b>AContaining Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Object</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AContainingObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aContainingSlot.oclIsUndefined()\n  then null\n  else aContainingSlot.aContainingObject\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Values' createColumn='false'"
	 * @generated
	 */
	AObject getAContainingObject();

	/**
	 * Returns the value of the '<em><b>AContainment Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContainment Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContainment Reference</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AContainmentReference()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let chain: acore::classifiers::AFeature = if aContainingSlot.oclIsUndefined()\n  then null\n  else aContainingSlot.aInstantiatedFeature\nendif in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AReference)\n    then chain.oclAsType(acore::classifiers::AReference)\n    else null\n  endif\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Values' createColumn='false'"
	 * @generated
	 */
	AReference getAContainmentReference();

	/**
	 * Returns the value of the '<em><b>AContaining Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Resource</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AContainingResource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::AResource = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Values' createColumn='false'"
	 * @generated
	 */
	AResource getAContainingResource();

	/**
	 * Returns the value of the '<em><b>AFeature Without Slot</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.classifiers.AFeature}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature Without Slot</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature Without Slot</em>' reference list.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AFeatureWithoutSlot()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if aClass.oclIsUndefined()\n  then OrderedSet{}\n  else aClass.aAllFeature\nendif->select(it: acore::classifiers::AFeature | let e0: Boolean = aSlot.aInstantiatedFeature->reject(oclIsUndefined())->asOrderedSet()->excludes(it)   in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='91 A Core Helpers' createColumn='false'"
	 * @generated
	 */
	EList<AFeature> getAFeatureWithoutSlot();

	/**
	 * Returns the value of the '<em><b>AAll Object</b></em>' reference list.
	 * The list contents are of type {@link org.langlets.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Object</em>' reference list.
	 * @see org.langlets.acore.values.ValuesPackage#getAObject_AAllObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::values::AObject)  = aSlot.aAllContainedObject->asOrderedSet()->including(self) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='91 A Core Helpers' createColumn='false'"
	 * @generated
	 */
	EList<AObject> getAAllObject();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model aValueMany="true"
	 *        annotation="http://www.xocl.org/OCL body='let nl: acore::values::ASlot = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Values' createColumn='true'"
	 * @generated
	 */
	ASlot aNewSlot(AFeature aFeature, EList<AValue> aValue);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let slots: OrderedSet(acore::values::ASlot)  = aSlot->asOrderedSet()->select(it: acore::values::ASlot | let e0: Boolean = it.aInstantiatedFeature = aFeature in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(acore::values::ASlot)  = slots in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='80 A Core/Values' createColumn='true'"
	 * @generated
	 */
	ASlot aSlotFromFeature(AFeature aFeature);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let e1: Boolean = aClass.aActiveClass = (let e2: Boolean = aClass.aAbstract <> true in \n if e2.oclIsInvalid() then null else e2 endif) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='91 A Core Helpers' createColumn='true'"
	 * @generated
	 */
	Boolean aChoosableForAClass(AClass aClass);

} // AObject
