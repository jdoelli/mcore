/**
 */
package org.langlets.acore.values.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.ADataType;
import org.langlets.acore.values.ADataValue;
import org.langlets.acore.values.ValuesPackage;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AData Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.values.impl.ADataValueImpl#getAValue <em>AValue</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ADataValueImpl#getADataType <em>AData Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class ADataValueImpl extends AValueImpl implements ADataValue {
	/**
	 * The default value of the '{@link #getAValue() <em>AValue</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue()
	 * @generated
	 * @ordered
	 */
	protected static final String AVALUE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAValue <em>AValue</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getADataType <em>AData Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ADataValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValuesPackage.Literals.ADATA_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAValue() {
		/**
		 * @OCL 'TO BE DONE'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ADATA_VALUE;
		EStructuralFeature eFeature = ValuesPackage.Literals.ADATA_VALUE__AVALUE;

		if (aValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ADATA_VALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ADATA_VALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType getADataType() {
		ADataType aDataType = basicGetADataType();
		return aDataType != null && aDataType.eIsProxy() ? (ADataType) eResolveProxy((InternalEObject) aDataType)
				: aDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType basicGetADataType() {
		/**
		 * @OCL let nl: acore::classifiers::ADataType = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ADATA_VALUE;
		EStructuralFeature eFeature = ValuesPackage.Literals.ADATA_VALUE__ADATA_TYPE;

		if (aDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ADATA_VALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ADATA_VALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ADataType result = (ADataType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ValuesPackage.ADATA_VALUE__AVALUE:
			return getAValue();
		case ValuesPackage.ADATA_VALUE__ADATA_TYPE:
			if (resolve)
				return getADataType();
			return basicGetADataType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ValuesPackage.ADATA_VALUE__AVALUE:
			return AVALUE_EDEFAULT == null ? getAValue() != null : !AVALUE_EDEFAULT.equals(getAValue());
		case ValuesPackage.ADATA_VALUE__ADATA_TYPE:
			return basicGetADataType() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aClassifier aDataType
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAClassifier() {
		EClass eClass = (ValuesPackage.Literals.ADATA_VALUE);
		EStructuralFeature eOverrideFeature = ValuesPackage.Literals.AVALUE__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //ADataValueImpl
