/**
 */
package org.langlets.acore.values.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.abstractions.impl.AElementImpl;

import org.langlets.acore.classifiers.AFeature;

import org.langlets.acore.updates.AUpdate;
import org.langlets.acore.updates.AUpdateMode;

import org.langlets.acore.values.AObject;
import org.langlets.acore.values.ASlot;
import org.langlets.acore.values.AValue;
import org.langlets.acore.values.ValuesPackage;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>ASlot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAKey <em>AKey</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAInstantiatedFeature <em>AInstantiated Feature</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAValue <em>AValue</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAIsContainment <em>AIs Containment</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAContainingObject <em>AContaining Object</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAFeatureIsClassTyped <em>AFeature Is Class Typed</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAFeatureIsDataTyped <em>AFeature Is Data Typed</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getADirectlyContainedObject <em>ADirectly Contained Object</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAAllContainedObject <em>AAll Contained Object</em>}</li>
 *   <li>{@link org.langlets.acore.values.impl.ASlotImpl#getAFeatureOfWrongClass <em>AFeature Of Wrong Class</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class ASlotImpl extends AElementImpl implements ASlot {
	/**
	 * The default value of the '{@link #getAKey() <em>AKey</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKey()
	 * @generated
	 * @ordered
	 */
	protected static final String AKEY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAIsContainment() <em>AIs Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsContainment()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AIS_CONTAINMENT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAFeatureIsClassTyped() <em>AFeature Is Class Typed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureIsClassTyped()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AFEATURE_IS_CLASS_TYPED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAFeatureIsDataTyped() <em>AFeature Is Data Typed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureIsDataTyped()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AFEATURE_IS_DATA_TYPED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAFeatureOfWrongClass() <em>AFeature Of Wrong Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureOfWrongClass()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AFEATURE_OF_WRONG_CLASS_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #evaluate <em>Evaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evaluate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression evaluateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #update <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateupdatesAUpdateModevaluesAValueBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKey <em>AKey</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKey
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKeyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAInstantiatedFeature <em>AInstantiated Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAInstantiatedFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aInstantiatedFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAValue <em>AValue</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aValueDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAIsContainment <em>AIs Containment</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsContainment
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aIsContainmentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingObject <em>AContaining Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFeatureIsClassTyped <em>AFeature Is Class Typed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureIsClassTyped
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFeatureIsClassTypedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFeatureIsDataTyped <em>AFeature Is Data Typed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureIsDataTyped
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFeatureIsDataTypedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getADirectlyContainedObject <em>ADirectly Contained Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getADirectlyContainedObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aDirectlyContainedObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllContainedObject <em>AAll Contained Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllContainedObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllContainedObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFeatureOfWrongClass <em>AFeature Of Wrong Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureOfWrongClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFeatureOfWrongClassDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ASlotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValuesPackage.Literals.ASLOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKey() {
		/**
		 * @OCL ''
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AKEY;

		if (aKeyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aKeyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKeyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getAInstantiatedFeature() {
		AFeature aInstantiatedFeature = basicGetAInstantiatedFeature();
		return aInstantiatedFeature != null && aInstantiatedFeature.eIsProxy()
				? (AFeature) eResolveProxy((InternalEObject) aInstantiatedFeature) : aInstantiatedFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetAInstantiatedFeature() {
		/**
		 * @OCL let nl: acore::classifiers::AFeature = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AINSTANTIATED_FEATURE;

		if (aInstantiatedFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aInstantiatedFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aInstantiatedFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AValue> getAValue() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AVALUE;

		if (aValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AValue> result = (EList<AValue>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAIsContainment() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AIS_CONTAINMENT;

		if (aIsContainmentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aIsContainmentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aIsContainmentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject getAContainingObject() {
		AObject aContainingObject = basicGetAContainingObject();
		return aContainingObject != null && aContainingObject.eIsProxy()
				? (AObject) eResolveProxy((InternalEObject) aContainingObject) : aContainingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject basicGetAContainingObject() {
		/**
		 * @OCL let nl: acore::values::AObject = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__ACONTAINING_OBJECT;

		if (aContainingObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AObject result = (AObject) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAFeatureIsClassTyped() {
		/**
		 * @OCL if (if aInstantiatedFeature.aClassifier.oclIsUndefined()
		then null
		else aInstantiatedFeature.aClassifier.aActiveClass
		endif) 
		=true 
		then true
		else false
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AFEATURE_IS_CLASS_TYPED;

		if (aFeatureIsClassTypedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFeatureIsClassTypedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureIsClassTypedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAFeatureIsDataTyped() {
		/**
		 * @OCL if (let e0: Boolean = if (if aInstantiatedFeature.aClassifier.oclIsUndefined()
		then null
		else aInstantiatedFeature.aClassifier.aActiveDataType
		endif)= true 
		then true 
		else if (if aInstantiatedFeature.aClassifier.oclIsUndefined()
		then null
		else aInstantiatedFeature.aClassifier.aActiveEnumeration
		endif)= true 
		then true 
		else if ((if aInstantiatedFeature.aClassifier.oclIsUndefined()
		then null
		else aInstantiatedFeature.aClassifier.aActiveDataType
		endif)= null or (if aInstantiatedFeature.aClassifier.oclIsUndefined()
		then null
		else aInstantiatedFeature.aClassifier.aActiveEnumeration
		endif)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then true
		else false
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AFEATURE_IS_DATA_TYPED;

		if (aFeatureIsDataTypedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFeatureIsDataTypedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureIsDataTypedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getADirectlyContainedObject() {
		/**
		 * @OCL if (aIsContainment) 
		=true 
		then let chain: OrderedSet(acore::values::AValue)  = aValue->asOrderedSet() in
		chain->iterate(i:acore::values::AValue; r: OrderedSet(acore::values::AObject)=OrderedSet{} | if i.oclIsKindOf(acore::values::AObject) then r->including(i.oclAsType(acore::values::AObject))->asOrderedSet() 
		else r endif)
		else OrderedSet{}
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__ADIRECTLY_CONTAINED_OBJECT;

		if (aDirectlyContainedObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aDirectlyContainedObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aDirectlyContainedObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AObject> result = (EList<AObject>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAAllContainedObject() {
		/**
		 * @OCL let e1: OrderedSet(acore::values::AObject)  = aDirectlyContainedObject->asOrderedSet()->union(aDirectlyContainedObject.aSlot.aAllContainedObject->reject(oclIsUndefined())->asOrderedSet()) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AALL_CONTAINED_OBJECT;

		if (aAllContainedObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllContainedObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllContainedObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AObject> result = (EList<AObject>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAFeatureOfWrongClass() {
		/**
		 * @OCL let e1: Boolean = if aContainingObject.aClass.oclIsUndefined()
		then OrderedSet{}
		else aContainingObject.aClass.aAllFeature
		endif->excludes(aInstantiatedFeature)   in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.ASLOT;
		EStructuralFeature eFeature = ValuesPackage.Literals.ASLOT__AFEATURE_OF_WRONG_CLASS;

		if (aFeatureOfWrongClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFeatureOfWrongClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureOfWrongClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AValue> evaluate() {

		/**
		 * @OCL null
		 * @templateTag IGOT01
		 */
		EClass eClass = (ValuesPackage.Literals.ASLOT);
		EOperation eOperation = ValuesPackage.Literals.ASLOT.getEOperations().get(0);
		if (evaluateBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				evaluateBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, body, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(evaluateBodyOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AValue>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AUpdate> update(AUpdateMode aUpdateMode, EList<AValue> trg) {

		/**
		 * @OCL null
		 * @templateTag IGOT01
		 */
		EClass eClass = (ValuesPackage.Literals.ASLOT);
		EOperation eOperation = ValuesPackage.Literals.ASLOT.getEOperations().get(1);
		if (updateupdatesAUpdateModevaluesAValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				updateupdatesAUpdateModevaluesAValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, body, helper.getProblems(),
						ValuesPackage.Literals.ASLOT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(updateupdatesAUpdateModevaluesAValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.ASLOT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("aUpdateMode", aUpdateMode);

			evalEnv.add("trg", trg);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<AUpdate>) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ValuesPackage.ASLOT__AKEY:
			return getAKey();
		case ValuesPackage.ASLOT__AINSTANTIATED_FEATURE:
			if (resolve)
				return getAInstantiatedFeature();
			return basicGetAInstantiatedFeature();
		case ValuesPackage.ASLOT__AVALUE:
			return getAValue();
		case ValuesPackage.ASLOT__AIS_CONTAINMENT:
			return getAIsContainment();
		case ValuesPackage.ASLOT__ACONTAINING_OBJECT:
			if (resolve)
				return getAContainingObject();
			return basicGetAContainingObject();
		case ValuesPackage.ASLOT__AFEATURE_IS_CLASS_TYPED:
			return getAFeatureIsClassTyped();
		case ValuesPackage.ASLOT__AFEATURE_IS_DATA_TYPED:
			return getAFeatureIsDataTyped();
		case ValuesPackage.ASLOT__ADIRECTLY_CONTAINED_OBJECT:
			return getADirectlyContainedObject();
		case ValuesPackage.ASLOT__AALL_CONTAINED_OBJECT:
			return getAAllContainedObject();
		case ValuesPackage.ASLOT__AFEATURE_OF_WRONG_CLASS:
			return getAFeatureOfWrongClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ValuesPackage.ASLOT__AKEY:
			return AKEY_EDEFAULT == null ? getAKey() != null : !AKEY_EDEFAULT.equals(getAKey());
		case ValuesPackage.ASLOT__AINSTANTIATED_FEATURE:
			return basicGetAInstantiatedFeature() != null;
		case ValuesPackage.ASLOT__AVALUE:
			return !getAValue().isEmpty();
		case ValuesPackage.ASLOT__AIS_CONTAINMENT:
			return AIS_CONTAINMENT_EDEFAULT == null ? getAIsContainment() != null
					: !AIS_CONTAINMENT_EDEFAULT.equals(getAIsContainment());
		case ValuesPackage.ASLOT__ACONTAINING_OBJECT:
			return basicGetAContainingObject() != null;
		case ValuesPackage.ASLOT__AFEATURE_IS_CLASS_TYPED:
			return AFEATURE_IS_CLASS_TYPED_EDEFAULT == null ? getAFeatureIsClassTyped() != null
					: !AFEATURE_IS_CLASS_TYPED_EDEFAULT.equals(getAFeatureIsClassTyped());
		case ValuesPackage.ASLOT__AFEATURE_IS_DATA_TYPED:
			return AFEATURE_IS_DATA_TYPED_EDEFAULT == null ? getAFeatureIsDataTyped() != null
					: !AFEATURE_IS_DATA_TYPED_EDEFAULT.equals(getAFeatureIsDataTyped());
		case ValuesPackage.ASLOT__ADIRECTLY_CONTAINED_OBJECT:
			return !getADirectlyContainedObject().isEmpty();
		case ValuesPackage.ASLOT__AALL_CONTAINED_OBJECT:
			return !getAAllContainedObject().isEmpty();
		case ValuesPackage.ASLOT__AFEATURE_OF_WRONG_CLASS:
			return AFEATURE_OF_WRONG_CLASS_EDEFAULT == null ? getAFeatureOfWrongClass() != null
					: !AFEATURE_OF_WRONG_CLASS_EDEFAULT.equals(getAFeatureOfWrongClass());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ValuesPackage.ASLOT___EVALUATE:
			return evaluate();
		case ValuesPackage.ASLOT___UPDATE__AUPDATEMODE_ELIST:
			return update((AUpdateMode) arguments.get(0), (EList<AValue>) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //ASlotImpl
