/**
 */
package org.langlets.acore.values;

import org.langlets.acore.abstractions.ANamed;

import org.langlets.acore.classifiers.AEnumeration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ALiteral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.langlets.acore.values.ALiteral#getAEnumeration <em>AEnumeration</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.langlets.acore.values.ValuesPackage#getALiteral()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aEnumeration\n'"
 * @generated
 */

public interface ALiteral extends AValue, ANamed {
	/**
	 * Returns the value of the '<em><b>AEnumeration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AEnumeration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AEnumeration</em>' reference.
	 * @see org.langlets.acore.values.ValuesPackage#getALiteral_AEnumeration()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AEnumeration = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='80 A Core/Values'"
	 * @generated
	 */
	AEnumeration getAEnumeration();

} // ALiteral
