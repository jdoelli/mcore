/**
 */
package org.langlets.acore.abstractions.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.langlets.acore.AComponent;
import org.langlets.acore.APackage;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.AFeature;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AElement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTPackageUri <em>TPackage Uri</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTClassifierName <em>TClassifier Name</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTFeatureName <em>TFeature Name</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTPackage <em>TPackage</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTClassifier <em>TClassifier</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTFeature <em>TFeature</em>}</li>
 *   <li>{@link org.langlets.acore.abstractions.impl.AElementImpl#getTACoreAStringClass <em>TA Core AString Class</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AElementImpl extends MinimalEObjectImpl.Container implements AElement {
	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getTPackageUri() <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackageUri()
	 * @generated
	 * @ordered
	 */
	protected static final String TPACKAGE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTPackageUri() <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackageUri()
	 * @generated
	 * @ordered
	 */
	protected String tPackageUri = TPACKAGE_URI_EDEFAULT;

	/**
	 * This is true if the TPackage Uri attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tPackageUriESet;

	/**
	 * The default value of the '{@link #getTClassifierName() <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifierName()
	 * @generated
	 * @ordered
	 */
	protected static final String TCLASSIFIER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTClassifierName() <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifierName()
	 * @generated
	 * @ordered
	 */
	protected String tClassifierName = TCLASSIFIER_NAME_EDEFAULT;

	/**
	 * This is true if the TClassifier Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tClassifierNameESet;

	/**
	 * The default value of the '{@link #getTFeatureName() <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeatureName()
	 * @generated
	 * @ordered
	 */
	protected static final String TFEATURE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTFeatureName() <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeatureName()
	 * @generated
	 * @ordered
	 */
	protected String tFeatureName = TFEATURE_NAME_EDEFAULT;

	/**
	 * This is true if the TFeature Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tFeatureNameESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentLevel <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringOrMissing <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringIsEmpty <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aPackageFromUri <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aPackageFromUri
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aPackageFromUriecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aClassifierFromUriAndName <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aClassifierFromUriAndName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aFeatureFromUriAndNames <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aFeatureFromUriAndNames
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAStringClass <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAStringClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAStringClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreARealClass <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreARealClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreARealClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAIntegerClass <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAIntegerClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAIntegerClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAObjectClass <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAObjectClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAObjectClassBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase <em>AKind Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTPackage <em>TPackage</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTClassifier <em>TClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTFeature <em>TFeature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTACoreAStringClass <em>TA Core AString Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTACoreAStringClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tACoreAStringClassDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AElementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbstractionsPackage.Literals.AELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent getAContainingComponent() {
		AComponent aContainingComponent = basicGetAContainingComponent();
		return aContainingComponent != null && aContainingComponent.eIsProxy()
				? (AComponent) eResolveProxy((InternalEObject) aContainingComponent) : aContainingComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent basicGetAContainingComponent() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(acore::AComponent)
		then self.eContainer().oclAsType(acore::AComponent)
		else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)
		then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent
		else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AComponent result = (AComponent) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTPackageUri() {
		return tPackageUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPackageUri(String newTPackageUri) {
		String oldTPackageUri = tPackageUri;
		tPackageUri = newTPackageUri;
		boolean oldTPackageUriESet = tPackageUriESet;
		tPackageUriESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbstractionsPackage.AELEMENT__TPACKAGE_URI,
					oldTPackageUri, tPackageUri, !oldTPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTPackageUri() {
		String oldTPackageUri = tPackageUri;
		boolean oldTPackageUriESet = tPackageUriESet;
		tPackageUri = TPACKAGE_URI_EDEFAULT;
		tPackageUriESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, AbstractionsPackage.AELEMENT__TPACKAGE_URI,
					oldTPackageUri, TPACKAGE_URI_EDEFAULT, oldTPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTPackageUri() {
		return tPackageUriESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTClassifierName() {
		return tClassifierName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTClassifierName(String newTClassifierName) {
		String oldTClassifierName = tClassifierName;
		tClassifierName = newTClassifierName;
		boolean oldTClassifierNameESet = tClassifierNameESet;
		tClassifierNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME,
					oldTClassifierName, tClassifierName, !oldTClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTClassifierName() {
		String oldTClassifierName = tClassifierName;
		boolean oldTClassifierNameESet = tClassifierNameESet;
		tClassifierName = TCLASSIFIER_NAME_EDEFAULT;
		tClassifierNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME,
					oldTClassifierName, TCLASSIFIER_NAME_EDEFAULT, oldTClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTClassifierName() {
		return tClassifierNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTFeatureName() {
		return tFeatureName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTFeatureName(String newTFeatureName) {
		String oldTFeatureName = tFeatureName;
		tFeatureName = newTFeatureName;
		boolean oldTFeatureNameESet = tFeatureNameESet;
		tFeatureNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbstractionsPackage.AELEMENT__TFEATURE_NAME,
					oldTFeatureName, tFeatureName, !oldTFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTFeatureName() {
		String oldTFeatureName = tFeatureName;
		boolean oldTFeatureNameESet = tFeatureNameESet;
		tFeatureName = TFEATURE_NAME_EDEFAULT;
		tFeatureNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, AbstractionsPackage.AELEMENT__TFEATURE_NAME,
					oldTFeatureName, TFEATURE_NAME_EDEFAULT, oldTFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTFeatureName() {
		return tFeatureNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getTPackage() {
		APackage tPackage = basicGetTPackage();
		return tPackage != null && tPackage.eIsProxy() ? (APackage) eResolveProxy((InternalEObject) tPackage)
				: tPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetTPackage() {
		/**
		 * @OCL let p: String = tPackageUri in
		aPackageFromUri(p)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TPACKAGE;

		if (tPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				tPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getTClassifier() {
		AClassifier tClassifier = basicGetTClassifier();
		return tClassifier != null && tClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) tClassifier) : tClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetTClassifier() {
		/**
		 * @OCL let p: String = tPackageUri in
		let c: String = tClassifierName in
		aClassifierFromUriAndName(p, c)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER;

		if (tClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				tClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getTFeature() {
		AFeature tFeature = basicGetTFeature();
		return tFeature != null && tFeature.eIsProxy() ? (AFeature) eResolveProxy((InternalEObject) tFeature)
				: tFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetTFeature() {
		/**
		 * @OCL let p: String = tPackageUri in
		let c: String = tClassifierName in
		let f: String = tFeatureName in
		aFeatureFromUriAndNames(p, c, f)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TFEATURE;

		if (tFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				tFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getTACoreAStringClass() {
		AClassifier tACoreAStringClass = basicGetTACoreAStringClass();
		return tACoreAStringClass != null && tACoreAStringClass.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) tACoreAStringClass) : tACoreAStringClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetTACoreAStringClass() {
		/**
		 * @OCL aCoreAStringClass()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AELEMENT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TA_CORE_ASTRING_CLASS;

		if (tACoreAStringClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				tACoreAStringClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tACoreAStringClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer indentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).indentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(0);
		if (indentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		indentationSpaces(numberOfSpaces)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(1);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer = let e1: Integer = size - 1 in 
		if e1.oclIsInvalid() then null else e1 endif in
		if (let e0: Boolean = size < 1 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then ''
		else (let e0: String = indentationSpaces(sizeMinusOne).concat(' ') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(2);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String stringOrMissing(String p) {

		/**
		 * @OCL if (stringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(3);
		if (stringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringIsEmpty(String s) {

		/**
		 * @OCL if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(4);
		if (stringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				stringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements, String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(5);
		if (listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		listOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(6);
		if (listOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(listOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage aPackageFromUri(String packageUri) {

		/**
		 * @OCL if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aPackageFromUri(packageUri)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(7);
		if (aPackageFromUriecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aPackageFromUriecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageFromUriecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("packageUri", packageUri);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aClassifierFromUriAndName(String uri, String name) {

		/**
		 * @OCL let p: acore::APackage = aPackageFromUri(uri) in
		if p = null
		then null
		else p.aClassifierFromName(name) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(8);
		if (aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("name", name);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature aFeatureFromUriAndNames(String uri, String className, String featureName) {

		/**
		 * @OCL let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in
		let cAsClass: acore::classifiers::AClass = let chain: acore::classifiers::AClassifier = c in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClass)
		then chain.oclAsType(acore::classifiers::AClass)
		else null
		endif
		endif in
		if cAsClass = null
		then null
		else cAsClass.aFeatureFromName(featureName) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(9);
		if (aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("className", className);

			evalEnv.add("featureName", featureName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAStringClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAStringName: String = 'AString' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(10);
		if (aCoreAStringClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAStringClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAStringClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreARealClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreARealName: String = 'AReal' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(11);
		if (aCoreARealClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreARealClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreARealClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAIntegerClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAIntegerName: String = 'AInteger' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(12);
		if (aCoreAIntegerClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAIntegerClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAIntegerClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAObjectClass() {

		/**
		 * @OCL let aCoreValuesPackageUri: String = 'http://www.langlets.org/ACore/ACore/Values' in
		let aCoreAObjectName: String = 'AObject' in
		aClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(13);
		if (aCoreAObjectClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAObjectClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAObjectClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AELEMENT,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbstractionsPackage.AELEMENT__ALABEL:
			return getALabel();
		case AbstractionsPackage.AELEMENT__AKIND_BASE:
			return getAKindBase();
		case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
			return getARenderedKind();
		case AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT:
			if (resolve)
				return getAContainingComponent();
			return basicGetAContainingComponent();
		case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
			return getTPackageUri();
		case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
			return getTClassifierName();
		case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
			return getTFeatureName();
		case AbstractionsPackage.AELEMENT__TPACKAGE:
			if (resolve)
				return getTPackage();
			return basicGetTPackage();
		case AbstractionsPackage.AELEMENT__TCLASSIFIER:
			if (resolve)
				return getTClassifier();
			return basicGetTClassifier();
		case AbstractionsPackage.AELEMENT__TFEATURE:
			if (resolve)
				return getTFeature();
			return basicGetTFeature();
		case AbstractionsPackage.AELEMENT__TA_CORE_ASTRING_CLASS:
			if (resolve)
				return getTACoreAStringClass();
			return basicGetTACoreAStringClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
			setTPackageUri((String) newValue);
			return;
		case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
			setTClassifierName((String) newValue);
			return;
		case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
			setTFeatureName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
			unsetTPackageUri();
			return;
		case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
			unsetTClassifierName();
			return;
		case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
			unsetTFeatureName();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbstractionsPackage.AELEMENT__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null : !ALABEL_EDEFAULT.equals(getALabel());
		case AbstractionsPackage.AELEMENT__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null : !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		case AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT:
			return basicGetAContainingComponent() != null;
		case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
			return isSetTPackageUri();
		case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
			return isSetTClassifierName();
		case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
			return isSetTFeatureName();
		case AbstractionsPackage.AELEMENT__TPACKAGE:
			return basicGetTPackage() != null;
		case AbstractionsPackage.AELEMENT__TCLASSIFIER:
			return basicGetTClassifier() != null;
		case AbstractionsPackage.AELEMENT__TFEATURE:
			return basicGetTFeature() != null;
		case AbstractionsPackage.AELEMENT__TA_CORE_ASTRING_CLASS:
			return basicGetTACoreAStringClass() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AbstractionsPackage.AELEMENT___INDENT_LEVEL:
			return indentLevel();
		case AbstractionsPackage.AELEMENT___INDENTATION_SPACES:
			return indentationSpaces();
		case AbstractionsPackage.AELEMENT___INDENTATION_SPACES__INTEGER:
			return indentationSpaces((Integer) arguments.get(0));
		case AbstractionsPackage.AELEMENT___STRING_OR_MISSING__STRING:
			return stringOrMissing((String) arguments.get(0));
		case AbstractionsPackage.AELEMENT___STRING_IS_EMPTY__STRING:
			return stringIsEmpty((String) arguments.get(0));
		case AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0), (String) arguments.get(1));
		case AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return listOfStringToStringWithSeparator((EList<String>) arguments.get(0));
		case AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING:
			return aPackageFromUri((String) arguments.get(0));
		case AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
			return aClassifierFromUriAndName((String) arguments.get(0), (String) arguments.get(1));
		case AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
			return aFeatureFromUriAndNames((String) arguments.get(0), (String) arguments.get(1),
					(String) arguments.get(2));
		case AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS:
			return aCoreAStringClass();
		case AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS:
			return aCoreARealClass();
		case AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS:
			return aCoreAIntegerClass();
		case AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS:
			return aCoreAObjectClass();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tPackageUri: ");
		if (tPackageUriESet)
			result.append(tPackageUri);
		else
			result.append("<unset>");
		result.append(", tClassifierName: ");
		if (tClassifierNameESet)
			result.append(tClassifierName);
		else
			result.append("<unset>");
		result.append(", tFeatureName: ");
		if (tFeatureNameESet)
			result.append(tFeatureName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //AElementImpl
