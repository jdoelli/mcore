/**
 */
package org.langlets.acore.abstractions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.acore.AcorePackage;

import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;
import org.langlets.acore.abstractions.AbstractionsFactory;
import org.langlets.acore.abstractions.AbstractionsPackage;

import org.langlets.acore.classifiers.ClassifiersPackage;

import org.langlets.acore.classifiers.impl.ClassifiersPackageImpl;

import org.langlets.acore.impl.AcorePackageImpl;

import org.langlets.acore.updates.UpdatesPackage;

import org.langlets.acore.updates.impl.UpdatesPackageImpl;

import org.langlets.acore.values.ValuesPackage;

import org.langlets.acore.values.impl.ValuesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractionsPackageImpl extends EPackageImpl implements AbstractionsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aElementEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.langlets.acore.abstractions.AbstractionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AbstractionsPackageImpl() {
		super(eNS_URI, AbstractionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AbstractionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AbstractionsPackage init() {
		if (isInited)
			return (AbstractionsPackage) EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI);

		// Obtain or create and register package
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AbstractionsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AbstractionsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theAbstractionsPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theAbstractionsPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAbstractionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AbstractionsPackage.eNS_URI, theAbstractionsPackage);
		return theAbstractionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getANamed() {
		return aNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AUndefinedNameConstant() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_ABusinessName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAElement() {
		return aElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ALabel() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_AKindBase() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ARenderedKind() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_AContainingComponent() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_TPackageUri() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_TClassifierName() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_TFeatureName() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_TPackage() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_TClassifier() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_TFeature() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_TACoreAStringClass() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentLevel() {
		return aElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentationSpaces() {
		return aElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__IndentationSpaces__Integer() {
		return aElementEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__StringOrMissing__String() {
		return aElementEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__StringIsEmpty__String() {
		return aElementEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ListOfStringToStringWithSeparator__EList_String() {
		return aElementEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ListOfStringToStringWithSeparator__EList() {
		return aElementEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__APackageFromUri__String() {
		return aElementEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AClassifierFromUriAndName__String_String() {
		return aElementEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AFeatureFromUriAndNames__String_String_String() {
		return aElementEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAStringClass() {
		return aElementEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreARealClass() {
		return aElementEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAIntegerClass() {
		return aElementEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAObjectClass() {
		return aElementEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractionsFactory getAbstractionsFactory() {
		return (AbstractionsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aNamedEClass = createEClass(ANAMED);
		createEAttribute(aNamedEClass, ANAMED__ANAME);
		createEAttribute(aNamedEClass, ANAMED__AUNDEFINED_NAME_CONSTANT);
		createEAttribute(aNamedEClass, ANAMED__ABUSINESS_NAME);

		aElementEClass = createEClass(AELEMENT);
		createEAttribute(aElementEClass, AELEMENT__ALABEL);
		createEAttribute(aElementEClass, AELEMENT__AKIND_BASE);
		createEAttribute(aElementEClass, AELEMENT__ARENDERED_KIND);
		createEReference(aElementEClass, AELEMENT__ACONTAINING_COMPONENT);
		createEAttribute(aElementEClass, AELEMENT__TPACKAGE_URI);
		createEAttribute(aElementEClass, AELEMENT__TCLASSIFIER_NAME);
		createEAttribute(aElementEClass, AELEMENT__TFEATURE_NAME);
		createEReference(aElementEClass, AELEMENT__TPACKAGE);
		createEReference(aElementEClass, AELEMENT__TCLASSIFIER);
		createEReference(aElementEClass, AELEMENT__TFEATURE);
		createEReference(aElementEClass, AELEMENT__TA_CORE_ASTRING_CLASS);
		createEOperation(aElementEClass, AELEMENT___INDENT_LEVEL);
		createEOperation(aElementEClass, AELEMENT___INDENTATION_SPACES);
		createEOperation(aElementEClass, AELEMENT___INDENTATION_SPACES__INTEGER);
		createEOperation(aElementEClass, AELEMENT___STRING_OR_MISSING__STRING);
		createEOperation(aElementEClass, AELEMENT___STRING_IS_EMPTY__STRING);
		createEOperation(aElementEClass, AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING);
		createEOperation(aElementEClass, AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST);
		createEOperation(aElementEClass, AELEMENT___APACKAGE_FROM_URI__STRING);
		createEOperation(aElementEClass, AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING);
		createEOperation(aElementEClass, AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING);
		createEOperation(aElementEClass, AELEMENT___ACORE_ASTRING_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AREAL_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AINTEGER_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AOBJECT_CLASS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AcorePackage theAcorePackage = (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aNamedEClass.getESuperTypes().add(this.getAElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(aNamedEClass, ANamed.class, "ANamed", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getANamed_AName(), ecorePackage.getEString(), "aName", null, 0, 1, ANamed.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_AUndefinedNameConstant(), ecorePackage.getEString(), "aUndefinedNameConstant", null, 0,
				1, ANamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_ABusinessName(), ecorePackage.getEString(), "aBusinessName", null, 0, 1, ANamed.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aElementEClass, AElement.class, "AElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAElement_ALabel(), ecorePackage.getEString(), "aLabel", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_AKindBase(), ecorePackage.getEString(), "aKindBase", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ARenderedKind(), ecorePackage.getEString(), "aRenderedKind", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_AContainingComponent(), theAcorePackage.getAComponent(), null,
				"aContainingComponent", null, 0, 1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_TPackageUri(), ecorePackage.getEString(), "tPackageUri", null, 0, 1, AElement.class,
				IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_TClassifierName(), ecorePackage.getEString(), "tClassifierName", null, 0, 1,
				AElement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_TFeatureName(), ecorePackage.getEString(), "tFeatureName", null, 0, 1,
				AElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_TPackage(), theAcorePackage.getAPackage(), null, "tPackage", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_TClassifier(), theClassifiersPackage.getAClassifier(), null, "tClassifier", null, 0,
				1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_TFeature(), theClassifiersPackage.getAFeature(), null, "tFeature", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_TACoreAStringClass(), theClassifiersPackage.getAClassifier(), null,
				"tACoreAStringClass", null, 0, 1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getAElement__IndentLevel(), ecorePackage.getEIntegerObject(), "indentLevel", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getAElement__IndentationSpaces(), ecorePackage.getEString(), "indentationSpaces", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getAElement__IndentationSpaces__Integer(), ecorePackage.getEString(),
				"indentationSpaces", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__StringOrMissing__String(), ecorePackage.getEString(), "stringOrMissing", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__StringIsEmpty__String(), ecorePackage.getEBooleanObject(), "stringIsEmpty", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__ListOfStringToStringWithSeparator__EList_String(), ecorePackage.getEString(),
				"listOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "separator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__ListOfStringToStringWithSeparator__EList(), ecorePackage.getEString(),
				"listOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__APackageFromUri__String(), theAcorePackage.getAPackage(), "aPackageFromUri", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "packageUri", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AClassifierFromUriAndName__String_String(),
				theClassifiersPackage.getAClassifier(), "aClassifierFromUriAndName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AFeatureFromUriAndNames__String_String_String(),
				theClassifiersPackage.getAFeature(), "aFeatureFromUriAndNames", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "className", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "featureName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAStringClass(), theClassifiersPackage.getAClassifier(), "aCoreAStringClass", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreARealClass(), theClassifiersPackage.getAClassifier(), "aCoreARealClass", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAIntegerClass(), theClassifiersPackage.getAClassifier(), "aCoreAIntegerClass",
				0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAObjectClass(), theClassifiersPackage.getAClassifier(), "aCoreAObjectClass", 0,
				1, IS_UNIQUE, IS_ORDERED);

		// Create annotations
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelDerive", "aName\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelCreateColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getANamed_AName(), source, new String[] { "derive", "aUndefinedNameConstant\n" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "derive", "\'<A Name Is Undefined> \'\n" });
		addAnnotation(getANamed_ABusinessName(), source, new String[] { "derive",
				"let chain : String = aName in\nif chain.oclIsUndefined()\n  then null\n  else chain .camelCaseToBusiness()\n  endif\n" });
		addAnnotation(getAElement__IndentLevel(), source, new String[] { "body",
				"if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).indentLevel() + 1\n  else 0 endif endif\n" });
		addAnnotation(getAElement__IndentationSpaces(), source, new String[] { "body",
				"let numberOfSpaces: Integer = let e1: Integer = indentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\nindentationSpaces(numberOfSpaces)\n" });
		addAnnotation(getAElement__IndentationSpaces__Integer(), source, new String[] { "body",
				"let sizeMinusOne: Integer = let e1: Integer = size - 1 in \n if e1.oclIsInvalid() then null else e1 endif in\nif (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = indentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n" });
		addAnnotation(getAElement__StringOrMissing__String(), source,
				new String[] { "body", "if (stringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif\n" });
		addAnnotation(getAElement__StringIsEmpty__String(), source, new String[] { "body",
				"if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif\n" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList_String(), source, new String[] { "body",
				"let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList(), source, new String[] { "body",
				"let defaultSeparator: String = \', \' in\nlistOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n" });
		addAnnotation(getAElement__APackageFromUri__String(), source, new String[] { "body",
				"if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aPackageFromUri(packageUri)\nendif\n" });
		addAnnotation(getAElement__AClassifierFromUriAndName__String_String(), source, new String[] { "body",
				"let p: acore::APackage = aPackageFromUri(uri) in\nif p = null\n  then null\n  else p.aClassifierFromName(name) endif\n" });
		addAnnotation(getAElement__AFeatureFromUriAndNames__String_String_String(), source, new String[] { "body",
				"let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in\nlet cAsClass: acore::classifiers::AClass = let chain: acore::classifiers::AClassifier = c in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClass)\n    then chain.oclAsType(acore::classifiers::AClass)\n    else null\n  endif\n  endif in\nif cAsClass = null\n  then null\n  else cAsClass.aFeatureFromName(featureName) endif\n" });
		addAnnotation(getAElement__ACoreAStringClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAStringName: String = \'AString\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)\n" });
		addAnnotation(getAElement__ACoreARealClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreARealName: String = \'AReal\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)\n" });
		addAnnotation(getAElement__ACoreAIntegerClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAIntegerName: String = \'AInteger\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)\n" });
		addAnnotation(getAElement__ACoreAObjectClass(), source, new String[] { "body",
				"let aCoreValuesPackageUri: String = \'http://www.langlets.org/ACore/ACore/Values\' in\nlet aCoreAObjectName: String = \'AObject\' in\naClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)\n" });
		addAnnotation(getAElement_ALabel(), source, new String[] { "derive", "aRenderedKind\n" });
		addAnnotation(getAElement_AKindBase(), source, new String[] { "derive", "self.eClass().name\n" });
		addAnnotation(getAElement_ARenderedKind(), source, new String[] { "derive",
				"let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getAElement_AContainingComponent(), source, new String[] { "derive",
				"if self.eContainer().oclIsTypeOf(acore::AComponent)\r\n  then self.eContainer().oclAsType(acore::AComponent)\r\n  else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)\r\n  then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent\r\n  else null endif endif" });
		addAnnotation(getAElement_TPackage(), source,
				new String[] { "derive", "let p: String = tPackageUri in\naPackageFromUri(p)\n" });
		addAnnotation(getAElement_TClassifier(), source, new String[] { "derive",
				"let p: String = tPackageUri in\nlet c: String = tClassifierName in\naClassifierFromUriAndName(p, c)\n" });
		addAnnotation(getAElement_TFeature(), source, new String[] { "derive",
				"let p: String = tPackageUri in\nlet c: String = tClassifierName in\nlet f: String = tFeatureName in\naFeatureFromUriAndNames(p, c, f)\n" });
		addAnnotation(getAElement_TACoreAStringClass(), source, new String[] { "derive", "aCoreAStringClass()\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getANamed_AName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getANamed_ABusinessName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement__IndentLevel(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__IndentationSpaces(), source, new String[] {});
		addAnnotation(getAElement__IndentationSpaces__Integer(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__StringOrMissing__String(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__StringIsEmpty__String(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList_String(), source, new String[] {});
		addAnnotation(getAElement__ListOfStringToStringWithSeparator__EList(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__APackageFromUri__String(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AClassifierFromUriAndName__String_String(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AFeatureFromUriAndNames__String_String_String(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAStringClass(), source,
				new String[] { "propertyCategory", "80 A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreARealClass(), source,
				new String[] { "propertyCategory", "80 A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAIntegerClass(), source,
				new String[] { "propertyCategory", "80 A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAObjectClass(), source,
				new String[] { "propertyCategory", "80 A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "createColumn", "true", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "createColumn", "true", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_AContainingComponent(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_TPackageUri(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_TClassifierName(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_TFeatureName(), source,
				new String[] { "propertyCategory", "80 A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_TPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_TClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_TFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Abstractions" });
		addAnnotation(getAElement_TACoreAStringClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "80 A Core/Package" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getANamed_AName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_ABusinessName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_AContainingComponent(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_TPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_TClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_TFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_TACoreAStringClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation((getAElement__StringOrMissing__String()).getEParameters().get(0), source,
				new String[] { "mName", "p" });
		addAnnotation(getAElement_TACoreAStringClass(), source, new String[] { "mName", "T A Core A String Class" });
	}

} //AbstractionsPackageImpl
