<%if (isImplementation) {
EAnnotation ocl = genClass.getEcoreClass().getEAnnotation(oclNsURI);
String label = null;
if (ocl != null) label = ocl.getDetails().get("label");
if (label != null) { %>
	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL <%=xoclGenerationUtil.embedOclIntoJavaComment(label)%>
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = <%=genClass.getQualifiedClassifierAccessor()%>;
		if (labelOCL == null) {
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper")%> helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			<%=genModel.getImportedName("org.eclipse.emf.ecore.EAnnotation")%> ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return null;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, label, helper.getProblems(), eClass, "label");
			}
		}
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(labelOCL);
		try {
		<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, "label");
		return <%=genModel.getImportedName("org.xocl.core.util.XoclHelper")%>.format(query.evaluate(this));
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return null;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
	}
<%}
}

if (isImplementation) {
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclDerivedGenFeatures(genClass)) {
		if (genClass.getImplementedGenFeatures().contains(genFeature)) {
			continue;
		}		
		String derive = XoclEmfUtil.findDeriveAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass());
		if (!genModel.isReflectiveDelegation() && genFeature.isBasicGet()) {
//@[CopyOf,Class.javajet]
%>
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL <%=genFeature.getName()%> <%=xoclGenerationUtil.embedOclIntoJavaComment(derive)%>
	 * @templateTag INS02
	 * @generated
	 */
<%if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //basicGetGenFeature.annotations.insert.javajetinc%>
<%@ include file="basicGetGenFeature.annotations.insert.javajetinc" fail="silent" %>
<%}%>
<%if (genModel.useClassOverrideAnnotation()) {%>
	@Override
<%}%>
	public <%=genFeature.getImportedType(genClass)%> basicGet<%=genFeature.getAccessorName()%>()
	{
<%@ include file="getGenFeature.pre.insert.javajetinc" fail="silent" %>
<%if (derive.length() == 0) { %>
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
<% } else {
	    final String expr = genFeature.getSafeName() + "DeriveOCL";
	    if (true) { // we will need the feature to create the EcoreEList %>
		    <%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = (<%=genClass.getQualifiedClassifierAccessor()%>);
		    <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> eOverrideFeature = <%=genFeature.getQualifiedFeatureAccessor()%>;
	    <% } %>
		if (<%=expr%> == null) {
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper")%> helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);
			
			String derive = <%=genModel.getImportedName("org.xocl.core.util.XoclEmfUtil")%>.findDeriveAnnotationText(eOverrideFeature, eClass());
			
			try {
				<%=expr%> = helper.createQuery(derive);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return <% if (genFeature.isPrimitiveType()) {%><%=genFeature.getObjectType()%>.valueOf("0").<%=genFeature.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}
		
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(<%=expr%>);
		try {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, eOverrideFeature);
	<% if (genFeature.isListType()) { %>
		<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
		return (<%=genFeature.getObjectType()%>) xoclEval.evaluateElement(eOverrideFeature, query);
	<% } else if (genFeature.isPrimitiveType()) { %>
			return ((<%=genFeature.getObjectType()%>) query.evaluate(this)).<%=genFeature.getPrimitiveValueFunction()%>();
	<% } else { %>
		<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
		return (<%=genFeature.getObjectType()%>) xoclEval.evaluateElement(eOverrideFeature, query);
	<% } %>
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return <% if (genFeature.isPrimitiveType()) {%><%=genFeature.getObjectType()%>.valueOf("0").<%=genFeature.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
<% } %>
	}
<%
//@[EndCopyOf,Class.javajet]				
		} else if (genFeature.isGet() && !genFeature.isSuppressedGetVisibility()) {
//@[CopyOf,Class.javajet]
%>
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL <%=genFeature.getName()%> <%=xoclGenerationUtil.embedOclIntoJavaComment(derive)%>
	 * @templateTag INS03
	 * @generated
	 */
<%if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //getGenFeature.annotations.insert.javajetinc%>
<%@ include file="getGenFeature.annotations.insert.javajetinc" fail="silent" %>
<%}%>
  <%if (genModel.useGenerics() && ((genFeature.isContainer() || genFeature.isResolveProxies()) && !genFeature.isListType() && !genModel.isReflectiveDelegation() && genFeature.isUncheckedCast() || genFeature.isListType() && (genModel.isReflectiveDelegation() || genModel.isVirtualDelegation()) || genFeature.isListDataType() && genFeature.hasDelegateFeature())) {%>
	@SuppressWarnings("unchecked")
  <%}%>
<%if (genModel.useClassOverrideAnnotation() && !genFeature.getGenClass().isInterface()) {%>
	@Override
<%}%>
	public <%=genFeature.getImportedType(genClass)%> <%=genFeature.getGetAccessor()%>()
	{
<%@ include file="getGenFeature.pre.insert.javajetinc" fail="silent" %>
<%if (derive.length() == 0) { %>
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
<% } else {
//@[CopyOf,getGenFeature.TODO.override.javajetinc]
	final String expr = genFeature.getSafeName() + "DeriveOCL";
	    if (true) { // we will need the feature to create the EcoreEList %>
		    <%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = (<%=genClass.getQualifiedClassifierAccessor()%>);
		    <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> eOverrideFeature = <%=genFeature.getQualifiedFeatureAccessor()%>;
	    <% } %>
		if (<%=expr%> == null) { 
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper")%> helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);
			
			String derive = <%=genModel.getImportedName("org.xocl.core.util.XoclEmfUtil")%>.findDeriveAnnotationText(eOverrideFeature, eClass());
			
			try {
				<%=expr%> = helper.createQuery(derive);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return <% if (genFeature.isPrimitiveType()) {%><%=genFeature.getObjectType()%>.valueOf("0").<%=genFeature.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}
		
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(<%=expr%>);
		try {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, eOverrideFeature);
	<% if (genFeature.isListType()) { %>
		<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
		return (<%=genFeature.getObjectType()%>) xoclEval.evaluateElement(eOverrideFeature, query);
	<% } else if (genFeature.isPrimitiveType()) { %>
			return ((<%=genFeature.getObjectType()%>) query.evaluate(this)).<%=genFeature.getPrimitiveValueFunction()%>();
	<% } else { %>
		<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
		return (<%=genFeature.getObjectType()%>) xoclEval.evaluateElement(eOverrideFeature, query);
	<% } %>
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return <% if (genFeature.isPrimitiveType()) {%><%=genFeature.getObjectType()%>.valueOf("0").<%=genFeature.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
<%
//@[EndCopyOf,getGenFeature.TODO.override.javajetinc]
} %>
	}
<%
//@[EndCopyOf,Class.javajet]
		}
	}
EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
if (eAnnotation != null) {
	EMap<String, String> details = eAnnotation.getDetails();
	for (Map.Entry<String, String> entry : details) {
		String detailKey = entry.getKey();
		if (detailKey.endsWith("Body")) {
			String operationName = detailKey.substring(0, detailKey.length()-4);
			String body = entry.getValue();
			for (GenOperation genOperation : genClass.getAllGenOperations()) {
				if (genOperation.getName().equals(operationName)) {
					if (genClass.getImplementedGenOperations().contains(genOperation)) {
						break;
					}
//@[CopyOf,Class.javajet]
%>
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL <%=operationName%> <%=xoclGenerationUtil.embedOclIntoJavaComment(body)%>
	 * @templateTag INS04
	 * @generated
	 */
<%if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //genOperation.annotations.insert.javajetinc%>
<%@ include file="genOperation.annotations.insert.javajetinc" fail="silent" %>
<%}%>
<%if (genModel.useClassOverrideAnnotation()) {%>
	@Override
<%}%>
	public <%=genOperation.getTypeParameters(genClass)%><%=genOperation.getImportedType(genClass)%> <%=genOperation.getName()%>(<%=genOperation.getParameters(genClass)%>)<%=genOperation.getThrows(genClass)%>
	{
<%if (body.length() == 0) { %>
		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
<% } else {
	    final String expr = oclAnnotationsHelper.getOperationBodyVariable(genOperation); %>
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = (<%=genClass.getQualifiedClassifierAccessor()%>);
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EOperation")%> eOverrideOperation = <%=genOperation.getGenClass().getQualifiedClassifierAccessor()%>.getEOperations().get(<%=genOperation.getGenClass().getGenOperations().indexOf(genOperation)%>);
		if (<%=expr%> == null) {
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL")%>.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOverrideOperation);

			<%=genModel.getImportedName("org.eclipse.emf.ecore.EAnnotation")%> ocl = eClass.getEAnnotation(OVERRIDE_OCL_ANNOTATION_SOURCE);
			String body = (String) ocl.getDetails().get(eOverrideOperation.getName()+"Body");<%=genModel.getNonNLS()%>
			
			try {
				<%=expr%> = helper.createQuery(body);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return <% if (genOperation.isPrimitiveReturnType()) {%><%=genOperation.getObjectReturnType()%>.valueOf("0").<%=genOperation.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, body, helper.getProblems(), eClass, eOverrideOperation);
			}
		}
		
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(<%=expr%>);
		try {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, eOverrideOperation);
	<% if (!genOperation.getEcoreOperation().getEParameters().isEmpty()) { %> 
			<%=genModel.getImportedName("org.eclipse.ocl.EvaluationEnvironment")%><?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();
			<% for (EParameter param : genOperation.getEcoreOperation().getEParameters()) { %>
			evalEnv.add("<%=param.getName()%>", <%=param.getName()%>);<%=genModel.getNonNLS()%>
	  <% }
	} 
	
	if (genOperation.isListType()) { %>
			<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
			return (<%=genOperation.getObjectType()%>) xoclEval.evaluateElement(eOverrideOperation, query);
	<% } else if (genOperation.isPrimitiveType()) { %>
			return ((<%=genOperation.getObjectType()%>) query.evaluate(this)).<%=genOperation.getPrimitiveValueFunction()%>();
	<% } else { %>
			<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, cachedValues);
			return (<%=genOperation.getObjectType()%>) xoclEval.evaluateElement(eOverrideOperation, query);
	<% } %>
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return <% if (genOperation.isPrimitiveReturnType()) {%><%=genOperation.getObjectReturnType()%>.valueOf("0").<%=genOperation.getPrimitiveValueFunction()%>()<% } else { %>null<% }%>;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
  <%}%>
	}
<%
//@[EndCopyOf,Class.javajet]
				}
			}
		}
	}
}  
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstraintGenFeatures(genClass)) {
%>
	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b><%=genFeature.getFormattedName()%></b></em>' <%=genFeature.getFeatureKind()%>.
     * The constraint is applied in the context of the source of the reference, and the target of the reference being of type <%=genModel.getImportedName(genFeature.getQualifiedListItemType())%>
     * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL <%=genFeature.getName()%> <%=xoclGenerationUtil.embedOclIntoJavaComment(XoclEmfUtil.findChoiceConstraintAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass()))%>
	 * @templateTag INS05
	 * @generated
	 */
<%if (genModel.useClassOverrideAnnotation()) {
	EAnnotation ocl = genFeature.getEcoreFeature().getEAnnotation(oclNsURI);
	if (ocl != null && ocl.getDetails().get("choiceConstraint") != null) { %>
	@Override
<%}}%>
	public boolean eval<%=genFeature.getAccessorName()%>ChoiceConstraint(<%=genModel.getImportedName(genFeature.getQualifiedListItemType())%> trg){
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = <%=genClass.getQualifiedClassifierAccessor()%>;
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> eOverrideFeature = <%=genFeature.getQualifiedFeatureAccessor()%>;
		if (<%=genFeature.getSafeName()%>ChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			
			addEnvironmentVariable("trg", eOverrideFeature.getEType());

			String choiceConstraint = <%=genModel.getImportedName("org.xocl.core.util.XoclEmfUtil")%>.findChoiceConstraintAnnotationText(eOverrideFeature, eClass());

			try {
				<%=genFeature.getSafeName()%>ChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return false;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, choiceConstraint, helper.getProblems(), eClass, eOverrideFeature);
			}
		}
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(<%=genFeature.getSafeName()%>ChoiceConstraintOCL);
		try {
		<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, eOverrideFeature);
		query.getEvaluationEnvironment().add("trg", trg);
		return ((Boolean) query.evaluate(this)).booleanValue();
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return false;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
	}
<%
	}
//@[EndCopyOf,genFeature.insert.javajetinc]
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstructionGenFeatures(genClass)) {
//@[CopyOf,genFeature.insert.javajetinc]
%>
	/**
	 * Evaluates the OCL defined choice construction for the '<em><b><%=genFeature.getFormattedName()%></b></em>' <%=genFeature.getFeatureKind()%>.
     * The constraint is applied in the context of the source of the reference, and the choice being of type <%=genModel.getImportedName("java.util.ArrayList")%><<%=genModel.getImportedName(genFeature.getQualifiedListItemType())%>>
     * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL <%=genFeature.getName()%> <%=xoclGenerationUtil.embedOclIntoJavaComment(XoclEmfUtil.findChoiceConstructionAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass()))%>
	 * @templateTag INS06
	 * @generated
	 */
	@SuppressWarnings("unchecked")
<%if (genModel.useClassOverrideAnnotation()) {
	GenClass genSuperClass = genClass.getClassExtendsGenClass();
	EClass superClass = genSuperClass == null ? null : genSuperClass.getEcoreClass();
	boolean superClassHasFeature = superClass == null ? false : superClass.getEAllStructuralFeatures().contains(genFeature);
	EAnnotation ocl = genFeature.getEcoreFeature().getEAnnotation(oclNsURI);
	if (superClassHasFeature && ocl != null && ocl.getDetails().get("choiceConstruction") != null) { %>
	@Override
<%}}%>
	public <%=genModel.getImportedName("java.util.List")%><<%=genModel.getImportedName(genFeature.getQualifiedListItemType())%>> eval<%=genFeature.getAccessorName()%>ChoiceConstruction(<%=genModel.getImportedName("java.util.List")%><<%=genModel.getImportedName(genFeature.getQualifiedListItemType())%>> choice){
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EClass")%> eClass = <%=genClass.getQualifiedClassifierAccessor()%>;
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> eOverrideStructuralFeature = <%=genFeature.getQualifiedFeatureAccessor()%>;
		if (<%=genFeature.getSafeName()%>ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			//the actual class.... TODO: is this the right one
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.Variable")%> choiceVar = 
			      <%=genModel.getImportedName("org.eclipse.ocl.ecore.EcoreFactory")%>.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(),
					choiceVar, true);

			String choiceConstruction = <%=genModel.getImportedName("org.xocl.core.util.XoclEmfUtil")%>.findChoiceConstructionAnnotationText(eOverrideStructuralFeature, eClass());

			try {
				<%=genFeature.getSafeName()%>ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return choice;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, choiceConstruction, helper.getProblems(), eClass, eOverrideStructuralFeature);
			}
		}
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(<%=genFeature.getSafeName()%>ChoiceConstructionOCL);
		try {
		<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, eClass, eOverrideStructuralFeature);
		query.getEvaluationEnvironment().add("choice", choice);
		return (ArrayList<<%=genModel.getImportedName(genFeature.getQualifiedListItemType())%>>) query.evaluate(this);
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return choice;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
	}
<%
//@[EndCopyOf,genFeature.insert.javajetinc]
	}%>
<% if (oclAnnotationsHelper.hasImplementedUpdateAnnotations(genClass)) { %>
	
	/**
	 * Returns Update Annotation Meta Info specific for this class.  
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS16
	 * @generated
	 */
	protected <%=genModel.getImportedName("java.lang.Object")%>[][] getUpdateAnnotationMetaInfo() {
		return updateAnnotationMetaInfo;
	}
<%}%>
<% if (oclAnnotationsHelper.isInitializable()) {%>

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public <%=genModel.getImportedName("java.util.Map")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%>> getInitOclExpressionMap() {
		return ourInitOclExpressionMap; 
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public <%=genModel.getImportedName("java.util.Map")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%>> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap; 
	}
  <%} // endif
  if (oclAnnotationsHelper.isDirectInitializable()) {%>

	/**
	 * @templateTag INS09
	 * @generated
	 */
  	<%if (genModel.useClassOverrideAnnotation()) {%>
		@Override
  	<%}%>
    public <%=genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain")%> eBasicSetContainer(<%=genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject")%> newContainer, int newContainerFeatureID,
		      <%=genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain")%> msgs) {  	
  		<%=genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain")%> result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);
		for (<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> eStructuralFeature : eClass().getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof <%=genModel.getImportedName("org.eclipse.emf.ecore.EReference")%>) {
				<%=genModel.getImportedName("org.eclipse.emf.ecore.EReference")%> eReference = (<%=genModel.getImportedName("org.eclipse.emf.ecore.EReference")%>) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		<%=genModel.getImportedName("org.eclipse.emf.ecore.resource.Resource.Internal")%> eInternalResource = eInternalResource();
		ensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}
	
	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new <%=genModel.getImportedName("java.util.HashMap")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("java.lang.Object")%>>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));
			}
		}
	}
	
	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] getInitializedStructuralFeatures() {<%
		Set<String> inititalizedFeatureSet = oclAnnotationsHelper.getAllInitializedFeatures();
  		String[] inititalizedFeatureArray = inititalizedFeatureSet.toArray(new String[inititalizedFeatureSet.size()]);%>
  		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] initializedFeatures = new <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] {
  		<%=inititalizedFeatureArray[0]%><%for (int i = 1; i <  inititalizedFeatureArray.length; i++) {%>, <%=inititalizedFeatureArray[i]%><%}%>
  		};
  		return initializedFeatures;				
	}
	
	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
  		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] initializedFeatures = getInitializedStructuralFeatures();
  		
  		if (isLoadInProgress) {
  			// only transient features are initialized then
  			<%=genModel.getImportedName("java.util.List")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>> filteredInitializedFeatures = new <%=genModel.getImportedName("java.util.ArrayList")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>>();
  			for (<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> initializedFeature : initializedFeatures) {
  				if (initializedFeature.isTransient()) {
  					filteredInitializedFeatures.add(initializedFeature);
  				}
  			}
  			initializedFeatures = filteredInitializedFeatures.toArray(new <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[filteredInitializedFeatures.size()]);
  		}

		final <%=genModel.getImportedName("java.util.Map")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("java.lang.Object")%>> initOrderMap = new <%=genModel.getImportedName("java.util.HashMap")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("java.lang.Object")%>>(); 
		for (<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature : initializedFeatures) {
			<%=genModel.getImportedName("java.lang.Object")%> value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), "initOrder", "InitOrder", true);<%=genModel.getNonNLS(1)%><%=genModel.getNonNLS(2)%>
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}
		
		if (!initOrderMap.isEmpty()) {
			<%=genModel.getImportedName("java.util.Arrays")%>.sort(initializedFeatures, new <%=genModel.getImportedName("java.util.Comparator")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>>() {
				public int compare(<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature1, <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature2) {
					<%=genModel.getImportedName("java.lang.Object")%> comparedObject1 = initOrderMap.get(structuralFeature1);
					<%=genModel.getImportedName("java.lang.Object")%> comparedObject2 = initOrderMap.get(structuralFeature2);
					if (comparedObject1 == null) {
						if (comparedObject2 == null) {
							int index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);
							int index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);
							return index1 - index2;
						} else {
							return 1;
						}
					} else if (comparedObject2 == null) {
						return -1;
					}
					return <%=genModel.getImportedName("org.xocl.core.util.XoclMutlitypeComparisonUtil")%>.compare(comparedObject1, comparedObject2);
				}
			});
		}
	
		for (<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature : initializedFeatures) {
			<%=genModel.getImportedName("java.lang.Object")%> value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), "initValue", "InitValue", false);<%=genModel.getNonNLS(1)%><%=genModel.getNonNLS(2)%>
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature, 
					<%=genModel.getImportedName("java.util.Map")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%>> expressionMap,
					String annotationKey, String annotationOverrideKey, boolean isSimpleEvaluate) {
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%> oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey, annotationOverrideKey);
		
		if (oclExpression == null) {
			return NO_OBJECT;
		}
		
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query")%> query = OCL_ENV.createQuery(oclExpression);
		try {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.enterContext(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, query, <%=genClass.getQualifiedClassifierAccessor()%>, "initOclAnnotation("+structuralFeature.getName()+")");
		
			query.getEvaluationEnvironment().clear();
			<%=genModel.getImportedName("java.lang.Object")%> trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg); <%=genModel.getNonNLS()%>
		
			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			<%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%> xoclEval = new <%=genModel.getImportedName("org.xocl.core.util.XoclEvaluator")%>(this, new <%=genModel.getImportedName("java.util.HashMap")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.ETypedElement")%>, <%=genModel.getImportedName("java.lang.Object")%>>());
			xoclEval.setContainerOnCreation(this);
			
			return xoclEval.evaluateElement(structuralFeature, query);
		} catch(<%=genModel.getImportedName("java.lang.Throwable")%> e) {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleException(e);
			return NO_OBJECT;
		} finally {
			<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.leaveContext();
		}
	}
	
	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected <%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%> getInitOclAnnotationExpression(<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%> structuralFeature, 
					<%=genModel.getImportedName("java.util.Map")%><<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>, <%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%>> expressionMap,
					String annotationKey, String annotationOverrideKey) {
		<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression")%> oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}
		
		String oclText = <%=genModel.getImportedName("org.xocl.core.util.XoclEmfUtil")%>.findAnnotationText(structuralFeature, eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			<%=genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper")%> helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);
			
			<%=genModel.getImportedName("org.eclipse.emf.ecore.EClassifier")%> propertyType = <%=genModel.getImportedName("org.eclipse.ocl.util.TypeUtil")%>.getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType); <%=genModel.getNonNLS()%>
			
			try {
				oclExpression = helper.createQuery(oclText);
			} catch (<%=genModel.getImportedName("org.eclipse.ocl.ParserException")%> e) {
				return null;
			} finally {
				<%=genModel.getImportedName("org.xocl.core.util.XoclErrorHandler")%>.handleQueryProblems(<%=genPackage.getImportedPackageInterfaceName()%>.PLUGIN_ID, oclText, helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}
		
		return oclExpression;
	}	
  <%} else if (oclAnnotationsHelper.isInitializable()) {
	Set<String> inititalizedFeatureSet = oclAnnotationsHelper.getDirectInitializedFeatures();
	String[] inititalizedFeatureArray = inititalizedFeatureSet.toArray(new String[inititalizedFeatureSet.size()]);
	if (inititalizedFeatureArray.length != 0) { %>
 	
	/**
	 * @templateTag INS14
	 * @generated
	 */
  		<%if (genModel.useClassOverrideAnnotation()) {%>
	@Override
  		<%} // endif%>
	protected <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] getInitializedStructuralFeatures() {
		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] baseInititalizedFeatured = super.getInitializedStructuralFeatures();
  		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] initializedFeatures = new <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] {
  		<%=inititalizedFeatureArray[0]%><%for (int i = 1; i <  inititalizedFeatureArray.length; i++) {%>, <%=inititalizedFeatureArray[i]%><%}%>
  		};
  		<%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[] allInitializedFeatures = new <%=genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature")%>[baseInititalizedFeatured.length + initializedFeatures.length];
  		System.arraycopy(baseInititalizedFeatured, 0, allInitializedFeatures, 0, baseInititalizedFeatured.length);
  		System.arraycopy(initializedFeatures, 0, allInitializedFeatures, baseInititalizedFeatured.length, initializedFeatures.length);
  		return allInitializedFeatures;				
	}
 	
 	<%	} // endif
  } // endif

} // endif
%>