
package org.xocl.templates.model;

import java.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.*;
import org.eclipse.emf.ecore.*;
import org.eclipse.emf.common.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.impl.GenClassImpl;
import org.xocl.core.util.*;
import org.eclipse.emf.codegen.ecore.genmodel.impl.Literals;
import org.eclipse.emf.codegen.util.CodeGenUtil;

public class Class
{
  protected static String nl;
  public static synchronized Class create(String lineSeparator)
  {
    nl = lineSeparator;
    Class result = new Class();
    nl = null;
    return result;
  }

  public final String NL = nl == null ? (System.getProperties().getProperty("line.separator")) : nl;
  protected final String TEXT_1 = "/**";
  protected final String TEXT_2 = NL + " * ";
  protected final String TEXT_3 = NL + " */" + NL;
  protected final String TEXT_4 = NL;
  protected final String TEXT_5 = NL + "package ";
  protected final String TEXT_6 = ";";
  protected final String TEXT_7 = NL + "package ";
  protected final String TEXT_8 = ";";
  protected final String TEXT_9 = NL;
  protected final String TEXT_10 = NL;
  protected final String TEXT_11 = NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * A representation of the model object '<em><b>";
  protected final String TEXT_12 = "</b></em>'." + NL + " * <!-- end-user-doc -->";
  protected final String TEXT_13 = NL + " *" + NL + " * <!-- begin-model-doc -->" + NL + " * ";
  protected final String TEXT_14 = NL + " * <!-- end-model-doc -->";
  protected final String TEXT_15 = NL + " *";
  protected final String TEXT_16 = NL + " * <p>" + NL + " * The following features are supported:" + NL + " * <ul>";
  protected final String TEXT_17 = NL + " *   <li>{@link ";
  protected final String TEXT_18 = "#";
  protected final String TEXT_19 = " <em>";
  protected final String TEXT_20 = "</em>}</li>";
  protected final String TEXT_21 = NL + " * </ul>" + NL + " * </p>";
  protected final String TEXT_22 = NL + " *";
  protected final String TEXT_23 = NL + " * @see ";
  protected final String TEXT_24 = "#get";
  protected final String TEXT_25 = "()";
  protected final String TEXT_26 = NL + " * @model ";
  protected final String TEXT_27 = NL + " *        ";
  protected final String TEXT_28 = NL + " * @model";
  protected final String TEXT_29 = NL + " * @extends ";
  protected final String TEXT_30 = NL + " * @generated" + NL + " */";
  protected final String TEXT_31 = NL + "/**" + NL + " * <!-- begin-user-doc -->" + NL + " * An implementation of the model object '<em><b>";
  protected final String TEXT_32 = "</b></em>'." + NL + " * <!-- end-user-doc -->" + NL + " * <p>";
  protected final String TEXT_33 = NL + " * The following features are implemented:" + NL + " * <ul>";
  protected final String TEXT_34 = NL + " *   <li>{@link ";
  protected final String TEXT_35 = "#";
  protected final String TEXT_36 = " <em>";
  protected final String TEXT_37 = "</em>}</li>";
  protected final String TEXT_38 = NL + " * </ul>";
  protected final String TEXT_39 = NL + " * </p>" + NL + " *" + NL + " * @generated" + NL + " */";
  protected final String TEXT_40 = NL;
  protected final String TEXT_41 = NL;
  protected final String TEXT_42 = NL + "public";
  protected final String TEXT_43 = " abstract";
  protected final String TEXT_44 = " class ";
  protected final String TEXT_45 = ",";
  protected final String TEXT_46 = " implements ";
  protected final String TEXT_47 = " ";
  protected final String TEXT_48 = NL + "public interface ";
  protected final String TEXT_49 = NL + "{";
  protected final String TEXT_50 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\t";
  protected final String TEXT_51 = " copyright = ";
  protected final String TEXT_52 = ";";
  protected final String TEXT_53 = NL;
  protected final String TEXT_54 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic static final ";
  protected final String TEXT_55 = " mofDriverNumber = \"";
  protected final String TEXT_56 = "\";";
  protected final String TEXT_57 = NL;
  protected final String TEXT_58 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final long serialVersionUID = 1L;" + NL;
  protected final String TEXT_59 = NL + "\t/**" + NL + "\t * An array of objects representing the values of non-primitive features." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_60 = NL + "\t@";
  protected final String TEXT_61 = NL + "\tprotected Object[] ";
  protected final String TEXT_62 = ";" + NL;
  protected final String TEXT_63 = NL + "\t/**" + NL + "\t * A bit field representing the indices of non-primitive feature values." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_64 = NL + "\t@";
  protected final String TEXT_65 = NL + "\tprotected int ";
  protected final String TEXT_66 = ";" + NL;
  protected final String TEXT_67 = NL + "\t/**" + NL + "\t * A set of bit flags representing the values of boolean attributes and whether unsettable features have been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_68 = NL + "\t@";
  protected final String TEXT_69 = NL + "\tprotected int ";
  protected final String TEXT_70 = " = 0;" + NL;
  protected final String TEXT_71 = NL + "\t/**" + NL + "\t * The cached setting delegate for the '{@link #";
  protected final String TEXT_72 = "() <em>";
  protected final String TEXT_73 = "</em>}' ";
  protected final String TEXT_74 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_75 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_76 = NL + "\t@";
  protected final String TEXT_77 = NL + "\tprotected ";
  protected final String TEXT_78 = ".Internal.SettingDelegate ";
  protected final String TEXT_79 = "__ESETTING_DELEGATE = ((";
  protected final String TEXT_80 = ".Internal)";
  protected final String TEXT_81 = ").getSettingDelegate();" + NL;
  protected final String TEXT_82 = NL + "\t/**" + NL + "\t * The cached value of the '{@link #";
  protected final String TEXT_83 = "() <em>";
  protected final String TEXT_84 = "</em>}' ";
  protected final String TEXT_85 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_86 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_87 = NL + "\t@";
  protected final String TEXT_88 = NL + "\tprotected ";
  protected final String TEXT_89 = " ";
  protected final String TEXT_90 = ";" + NL;
  protected final String TEXT_91 = NL + "\t/**" + NL + "\t * The empty value for the '{@link #";
  protected final String TEXT_92 = "() <em>";
  protected final String TEXT_93 = "</em>}' array accessor." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_94 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_95 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_96 = NL + "\tprotected static final ";
  protected final String TEXT_97 = "[] ";
  protected final String TEXT_98 = "_EEMPTY_ARRAY = new ";
  protected final String TEXT_99 = " [0]";
  protected final String TEXT_100 = ";" + NL;
  protected final String TEXT_101 = NL + "\t/**" + NL + "\t * The default value of the '{@link #";
  protected final String TEXT_102 = "() <em>";
  protected final String TEXT_103 = "</em>}' ";
  protected final String TEXT_104 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_105 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_106 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_107 = NL + "\tprotected static final ";
  protected final String TEXT_108 = " ";
  protected final String TEXT_109 = "; // TODO The default value literal \"";
  protected final String TEXT_110 = "\" is not valid.";
  protected final String TEXT_111 = " = ";
  protected final String TEXT_112 = ";";
  protected final String TEXT_113 = NL;
  protected final String TEXT_114 = NL + "\t/**" + NL + "\t * An additional set of bit flags representing the values of boolean attributes and whether unsettable features have been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_115 = NL + "\t@";
  protected final String TEXT_116 = NL + "\tprotected int ";
  protected final String TEXT_117 = " = 0;" + NL;
  protected final String TEXT_118 = NL + "\t/**" + NL + "\t * The offset of the flags representing the value of the '{@link #";
  protected final String TEXT_119 = "() <em>";
  protected final String TEXT_120 = "</em>}' ";
  protected final String TEXT_121 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_122 = "_EFLAG_OFFSET = ";
  protected final String TEXT_123 = ";" + NL + "" + NL + "\t/**" + NL + "\t * The flags representing the default value of the '{@link #";
  protected final String TEXT_124 = "() <em>";
  protected final String TEXT_125 = "</em>}' ";
  protected final String TEXT_126 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_127 = "_EFLAG_DEFAULT = ";
  protected final String TEXT_128 = ".ordinal()";
  protected final String TEXT_129 = ".VALUES.indexOf(";
  protected final String TEXT_130 = ")";
  protected final String TEXT_131 = " << ";
  protected final String TEXT_132 = "_EFLAG_OFFSET;" + NL + "" + NL + "\t/**" + NL + "\t * The array of enumeration values for '{@link ";
  protected final String TEXT_133 = " ";
  protected final String TEXT_134 = "}'" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprivate static final ";
  protected final String TEXT_135 = "[] ";
  protected final String TEXT_136 = "_EFLAG_VALUES = ";
  protected final String TEXT_137 = ".values()";
  protected final String TEXT_138 = "(";
  protected final String TEXT_139 = "[])";
  protected final String TEXT_140 = ".VALUES.toArray(new ";
  protected final String TEXT_141 = "[";
  protected final String TEXT_142 = ".VALUES.size()])";
  protected final String TEXT_143 = ";" + NL;
  protected final String TEXT_144 = NL + "\t/**" + NL + "\t * The flag";
  protected final String TEXT_145 = " representing the value of the '{@link #";
  protected final String TEXT_146 = "() <em>";
  protected final String TEXT_147 = "</em>}' ";
  protected final String TEXT_148 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_149 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_150 = "_EFLAG = ";
  protected final String TEXT_151 = " << ";
  protected final String TEXT_152 = "_EFLAG_OFFSET";
  protected final String TEXT_153 = ";" + NL;
  protected final String TEXT_154 = NL + "\t/**" + NL + "\t * The cached value of the '{@link #";
  protected final String TEXT_155 = "() <em>";
  protected final String TEXT_156 = "</em>}' ";
  protected final String TEXT_157 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_158 = "()" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_159 = NL + "\t@";
  protected final String TEXT_160 = NL + "\tprotected ";
  protected final String TEXT_161 = " ";
  protected final String TEXT_162 = " = ";
  protected final String TEXT_163 = ";" + NL;
  protected final String TEXT_164 = NL + "\t/**" + NL + "\t * An additional set of bit flags representing the values of boolean attributes and whether unsettable features have been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_165 = NL + "\t@";
  protected final String TEXT_166 = NL + "\tprotected int ";
  protected final String TEXT_167 = " = 0;" + NL;
  protected final String TEXT_168 = NL + "\t/**" + NL + "\t * The flag representing whether the ";
  protected final String TEXT_169 = " ";
  protected final String TEXT_170 = " has been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final int ";
  protected final String TEXT_171 = "_ESETFLAG = 1 << ";
  protected final String TEXT_172 = ";" + NL;
  protected final String TEXT_173 = NL + "\t/**" + NL + "\t * This is true if the ";
  protected final String TEXT_174 = " ";
  protected final String TEXT_175 = " has been set." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */";
  protected final String TEXT_176 = NL + "\t@";
  protected final String TEXT_177 = NL + "\tprotected boolean ";
  protected final String TEXT_178 = "ESet;" + NL;
  protected final String TEXT_179 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the body of the '{@link #";
  protected final String TEXT_180 = " <em>";
  protected final String TEXT_181 = "</em>}' operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_182 = NL + "\t * @templateTag DFGFI01" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_183 = " ";
  protected final String TEXT_184 = ";" + NL + "\t";
  protected final String TEXT_185 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the derivation of '{@link #";
  protected final String TEXT_186 = " <em>";
  protected final String TEXT_187 = "</em>}' property." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_188 = NL + "\t * @templateTag DFGFI02" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_189 = " ";
  protected final String TEXT_190 = "DeriveOCL;" + NL + "\t";
  protected final String TEXT_191 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the constraint of valid choices of '{@link #";
  protected final String TEXT_192 = " <em>";
  protected final String TEXT_193 = "</em>}' property." + NL + "     * Is combined with the choice construction definition." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_194 = NL + "\t * @templateTag DFGFI03" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_195 = " ";
  protected final String TEXT_196 = "ChoiceConstraintOCL;" + NL + "\t";
  protected final String TEXT_197 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the construction of valid choices of '{@link #";
  protected final String TEXT_198 = " <em>";
  protected final String TEXT_199 = "</em>}' property." + NL + "     * Is combined with the choice constraint definition." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_200 = NL + "\t * @templateTag DFGFI04" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_201 = " ";
  protected final String TEXT_202 = "ChoiceConstructionOCL;" + NL + "\t";
  protected final String TEXT_203 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the derivation of '{@link #";
  protected final String TEXT_204 = " <em>";
  protected final String TEXT_205 = "</em>}' property." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_206 = NL + "\t * @templateTag DFGFI05" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_207 = " ";
  protected final String TEXT_208 = "DeriveOCL;" + NL;
  protected final String TEXT_209 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the body of the '{@link #";
  protected final String TEXT_210 = " <em>";
  protected final String TEXT_211 = "</em>}' operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_212 = NL + "\t * @templateTag DFGFI06" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_213 = " ";
  protected final String TEXT_214 = ";" + NL;
  protected final String TEXT_215 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the constraint of valid choices of '{@link #";
  protected final String TEXT_216 = " <em>";
  protected final String TEXT_217 = "</em>}' property." + NL + "     * Is combined with the choice construction definition." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_218 = NL + "\t * @templateTag DFGFI07" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_219 = " ";
  protected final String TEXT_220 = "ChoiceConstraintOCL;" + NL;
  protected final String TEXT_221 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the construction of valid choices of '{@link #";
  protected final String TEXT_222 = " <em>";
  protected final String TEXT_223 = "</em>}' property." + NL + "     * Is combined with the choice constraint definition." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_224 = NL + "\t * @templateTag DFGFI08" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_225 = " ";
  protected final String TEXT_226 = "ChoiceConstructionOCL;" + NL;
  protected final String TEXT_227 = NL + "\t/**" + NL + "\t * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #evalOclLabel" + NL + "\t * @templateTag DFGFI09" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_228 = " labelOCL;" + NL + "\t" + NL + "\t";
  protected final String TEXT_229 = NL + "\t";
  protected final String TEXT_230 = NL + "\t/**" + NL + "\t * Cache for init annotation OCL expressions" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI16" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_231 = "<";
  protected final String TEXT_232 = ", ";
  protected final String TEXT_233 = "> ourInitOclExpressionMap = new ";
  protected final String TEXT_234 = "<";
  protected final String TEXT_235 = ", ";
  protected final String TEXT_236 = ">();" + NL + "" + NL + "\t/**" + NL + "\t * Cache for init order annotation OCL expressions" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI17" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static ";
  protected final String TEXT_237 = "<";
  protected final String TEXT_238 = ", ";
  protected final String TEXT_239 = "> ourInitOrderOclExpressionMap = new ";
  protected final String TEXT_240 = "<";
  protected final String TEXT_241 = ", ";
  protected final String TEXT_242 = ">();" + NL + "" + NL + " \t";
  protected final String TEXT_243 = NL + "\t/**" + NL + "\t * Placeholder object which denotes the absence of a value" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI18" + NL + "\t * @generated" + NL + "\t */" + NL + " \tprivate static final ";
  protected final String TEXT_244 = " NO_OBJECT = new ";
  protected final String TEXT_245 = "();" + NL + " \t" + NL + "\t/**" + NL + "\t * The flag checking whether the class is initialized." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI19" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate boolean _isInitialized = false;" + NL + "\t" + NL + "\t/**" + NL + "\t * The map storing feature values snapshot at allowInitialization() call." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI20" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate ";
  protected final String TEXT_246 = "<";
  protected final String TEXT_247 = ", ";
  protected final String TEXT_248 = "> myInitValueMap;" + NL + "\t" + NL + " \t";
  protected final String TEXT_249 = NL + "\t/**" + NL + "\t * The OCL annotation source." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI10" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final String OCL_ANNOTATION_SOURCE = \"";
  protected final String TEXT_250 = "\";";
  protected final String TEXT_251 = NL + "\t/**" + NL + "\t * The OVERRIDE_OCL annotation source." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI11" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final String OVERRIDE_OCL_ANNOTATION_SOURCE = \"";
  protected final String TEXT_252 = "\";";
  protected final String TEXT_253 = NL + "  \t/**" + NL + "\t * Update Annotations Meta Info." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI21" + NL + "\t * @generated" + NL + "\t */" + NL + "\t private static final ";
  protected final String TEXT_254 = "[][] updateAnnotationMetaInfo = {" + NL + "\t ";
  protected final String TEXT_255 = ", ";
  protected final String TEXT_256 = "{";
  protected final String TEXT_257 = ", ";
  protected final String TEXT_258 = ", ";
  protected final String TEXT_259 = ", null}" + NL + "\t\t\t";
  protected final String TEXT_260 = "};";
  protected final String TEXT_261 = NL + "\t" + NL + "\t/**" + NL + "\t * The OCL environment." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI12" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final ";
  protected final String TEXT_262 = " OCL_ENV = ";
  protected final String TEXT_263 = ".newInstance(new ";
  protected final String TEXT_264 = "());" + NL + "" + NL + "\t/**" + NL + "\t * Set OCL environment options." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI13" + NL + "\t * @generated" + NL + "\t */" + NL + "\tstatic {" + NL + "\t\t";
  protected final String TEXT_265 = ".setOption(OCL_ENV.getEnvironment()," + NL + "\t\t\t\t";
  protected final String TEXT_266 = ".implicitRootClass(OCL_ENV.getEnvironment())," + NL + "\t\t\t\t";
  protected final String TEXT_267 = ".eINSTANCE.getEObject());" + NL + "\t\t";
  protected final String TEXT_268 = ".setOption(OCL_ENV.getEvaluationEnvironment()," + NL + "\t\t\t\t";
  protected final String TEXT_269 = ".DYNAMIC_DISPATCH," + NL + "\t\t\t\ttrue);" + NL + "\t}" + NL + "\t" + NL + "\t/**" + NL + "\t * The cache for OCL expressions." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag DFGFI14" + NL + "\t * @generated" + NL + "\t */" + NL + "\t private ";
  protected final String TEXT_270 = "<";
  protected final String TEXT_271 = ", Object> cachedValues = new ";
  protected final String TEXT_272 = "<";
  protected final String TEXT_273 = ", Object>();" + NL + "  \t " + NL + "\t /**" + NL + "\t * Utility function to safely add a Variable in the global parsing environment." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @param variableName the name of the variable to be added" + NL + "\t * @param variableType the type of the variable to be added" + NL + "\t * @templateTag DFGFI15" + NL + "\t * @generated" + NL + "\t */" + NL + "\t private static void addEnvironmentVariable(String variableName, ";
  protected final String TEXT_274 = " variableType) {" + NL + "\t\tOCL_ENV.getEnvironment().deleteElement(variableName);" + NL + "\t\t";
  protected final String TEXT_275 = " trgVar = ";
  protected final String TEXT_276 = ".eINSTANCE.createVariable();" + NL + "\t\ttrgVar.setName(variableName);" + NL + "\t\ttrgVar.setType(variableType);" + NL + "\t\tOCL_ENV.getEnvironment().addElement(variableName,trgVar, true);" + NL + "\t}" + NL + "\t";
  protected final String TEXT_277 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final int ";
  protected final String TEXT_278 = " = ";
  protected final String TEXT_279 = ".getFeatureID(";
  protected final String TEXT_280 = ") - ";
  protected final String TEXT_281 = ";" + NL;
  protected final String TEXT_282 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final int ";
  protected final String TEXT_283 = " = ";
  protected final String TEXT_284 = ".getFeatureID(";
  protected final String TEXT_285 = ") - ";
  protected final String TEXT_286 = ";" + NL;
  protected final String TEXT_287 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprivate static final int \"EOPERATION_OFFSET_CORRECTION\" = ";
  protected final String TEXT_288 = ".getOperationID(";
  protected final String TEXT_289 = ") - ";
  protected final String TEXT_290 = ";" + NL;
  protected final String TEXT_291 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\t";
  protected final String TEXT_292 = "public";
  protected final String TEXT_293 = "protected";
  protected final String TEXT_294 = " ";
  protected final String TEXT_295 = "()" + NL + "\t{" + NL + "\t\tsuper();";
  protected final String TEXT_296 = NL + "\t\t";
  protected final String TEXT_297 = " |= ";
  protected final String TEXT_298 = "_EFLAG";
  protected final String TEXT_299 = "_DEFAULT";
  protected final String TEXT_300 = ";";
  protected final String TEXT_301 = NL + NL + "\t\teAdapters().add(new ";
  protected final String TEXT_302 = "() {" + NL + "\t\t\t@Override" + NL + "\t\t\tpublic boolean isAdapterForType(";
  protected final String TEXT_303 = " type) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_304 = ".class.isInstance(type);" + NL + "\t\t\t}" + NL + "" + NL + "\t\t\t@Override" + NL + "\t\t\tpublic void notifyChanged(";
  protected final String TEXT_305 = " msg) {\t\t\t" + NL + "\t\t\t\tfor (";
  protected final String TEXT_306 = "[] entry : getUpdateAnnotationMetaInfo()) {" + NL + "\t\t\t\t\tif ((msg.getNotifier() == ";
  protected final String TEXT_307 = ".this)" + NL + "\t\t\t\t\t\t\t&& (msg.getFeature() == entry[0])) {" + NL + "\t\t\t\t\t\texecuteUpdateAnnotation(entry, msg);\t" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\t" + NL + "\t\t\tprotected void executeUpdateAnnotation(";
  protected final String TEXT_308 = "[] entry, ";
  protected final String TEXT_309 = " msg) {" + NL + "\t\t\t\t";
  protected final String TEXT_310 = " structuralFeature = (";
  protected final String TEXT_311 = ") entry[0];" + NL + "\t\t\t\tif (entry[3] == null) {" + NL + "\t\t\t\t\tentry[3] = compileUpdateAnnotationExpression(entry);" + NL + "\t\t\t\t}" + NL + "\t\t\t\t";
  protected final String TEXT_312 = " oclExpression = (";
  protected final String TEXT_313 = ") entry[3];" + NL + "\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_314 = " query = OCL_ENV.createQuery(oclExpression);" + NL + "\t\t\t\ttry {" + NL + "\t\t\t\t\t";
  protected final String TEXT_315 = ".enterContext(";
  protected final String TEXT_316 = ".PLUGIN_ID, query, ";
  protected final String TEXT_317 = ", \"updateAnnotation(\"+structuralFeature.getName()+\")\");" + NL + "\t\t" + NL + "\t\t\t\t\tquery.getEvaluationEnvironment().clear();" + NL + "\t\t\t\t\tquery.getEvaluationEnvironment().add(\"trg\", msg.getNewValue()); ";
  protected final String TEXT_318 = NL + "\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_319 = " xoclEval = new ";
  protected final String TEXT_320 = "(";
  protected final String TEXT_321 = ".this, new ";
  protected final String TEXT_322 = "<";
  protected final String TEXT_323 = ", ";
  protected final String TEXT_324 = ">());" + NL + "\t\t\t\t\txoclEval.setContainerOnCreation(";
  protected final String TEXT_325 = ".this);" + NL + "\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_326 = " updatedFeature = eClass().getEStructuralFeature((String) entry[1]);" + NL + "\t\t\t\t\tObject result = xoclEval.evaluateElement(updatedFeature, query);" + NL + "\t\t\t\t\tif (!xoclEval.isContainerSetOnCreation()) {" + NL + "\t\t\t\t\t\teSet(updatedFeature, result);" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t} catch(";
  protected final String TEXT_327 = " e) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_328 = ".handleException(e);" + NL + "\t\t\t\t} finally {" + NL + "\t\t\t\t\t";
  protected final String TEXT_329 = ".leaveContext();" + NL + "\t\t\t\t}" + NL + "\t\t\t}\t\t\t" + NL + "\t\t\t" + NL + "\t\t\tprotected ";
  protected final String TEXT_330 = " compileUpdateAnnotationExpression(";
  protected final String TEXT_331 = "[] entry) {" + NL + "\t\t\t\t";
  protected final String TEXT_332 = " oclExpression = null;" + NL + "\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_333 = " triggeringFeature = (";
  protected final String TEXT_334 = ") entry[0];" + NL + "\t\t\t\t";
  protected final String TEXT_335 = " updatedFeature = eClass().getEStructuralFeature((";
  protected final String TEXT_336 = ") entry[1]);" + NL + "\t\t\t\t";
  protected final String TEXT_337 = " oclText = (";
  protected final String TEXT_338 = ") entry[2];" + NL + "\t\t" + NL + "\t\t\t\tif (oclText != null) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_339 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\t\t\thelper.setAttributeContext(eClass(), updatedFeature);" + NL + "\t\t\t" + NL + "\t\t\t\t\t";
  protected final String TEXT_340 = " propertyType = ";
  protected final String TEXT_341 = ".getPropertyType(OCL_ENV.getEnvironment(), eClass(), triggeringFeature);" + NL + "\t\t\t\t\taddEnvironmentVariable(\"trg\", propertyType); ";
  protected final String TEXT_342 = NL + "\t\t\t\t" + NL + "\t\t\t\t\ttry {" + NL + "\t\t\t\t\t\toclExpression = helper.createQuery(oclText);" + NL + "\t\t\t\t\t} catch (";
  protected final String TEXT_343 = " e) {" + NL + "\t\t\t\t\t\treturn null;" + NL + "\t\t\t\t\t} finally {" + NL + "\t\t\t\t\t\t";
  protected final String TEXT_344 = ".handleQueryProblems(";
  protected final String TEXT_345 = ".PLUGIN_ID, oclText, helper.getProblems(), eClass(), triggeringFeature);" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t" + NL + "\t\t\t\treturn oclExpression;" + NL + "\t\t\t}\t\t\t" + NL + "\t\t});";
  protected final String TEXT_346 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_347 = NL + "\t@Override";
  protected final String TEXT_348 = NL + "\tprotected ";
  protected final String TEXT_349 = " eStaticClass()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_350 = ";" + NL + "\t}" + NL;
  protected final String TEXT_351 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_352 = NL + "\t@Override";
  protected final String TEXT_353 = NL + "\tprotected int eStaticFeatureCount()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_354 = ";" + NL + "\t}" + NL;
  protected final String TEXT_355 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_356 = NL + "\t";
  protected final String TEXT_357 = "[] ";
  protected final String TEXT_358 = "();" + NL;
  protected final String TEXT_359 = NL + "\tpublic ";
  protected final String TEXT_360 = "[] ";
  protected final String TEXT_361 = "()" + NL + "\t{";
  protected final String TEXT_362 = NL + "\t\t";
  protected final String TEXT_363 = " list = (";
  protected final String TEXT_364 = ")";
  protected final String TEXT_365 = "();" + NL + "\t\tif (list.isEmpty()) return ";
  protected final String TEXT_366 = "(";
  protected final String TEXT_367 = "[])";
  protected final String TEXT_368 = "_EEMPTY_ARRAY;";
  protected final String TEXT_369 = NL + "\t\tif (";
  protected final String TEXT_370 = " == null || ";
  protected final String TEXT_371 = ".isEmpty()) return ";
  protected final String TEXT_372 = "(";
  protected final String TEXT_373 = "[])";
  protected final String TEXT_374 = "_EEMPTY_ARRAY;" + NL + "\t\t";
  protected final String TEXT_375 = " list = (";
  protected final String TEXT_376 = ")";
  protected final String TEXT_377 = ";";
  protected final String TEXT_378 = NL + "\t\tlist.shrink();" + NL + "\t\treturn (";
  protected final String TEXT_379 = "[])list.data();" + NL + "\t}" + NL;
  protected final String TEXT_380 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_381 = NL + "\t";
  protected final String TEXT_382 = " get";
  protected final String TEXT_383 = "(int index);" + NL;
  protected final String TEXT_384 = NL + "\tpublic ";
  protected final String TEXT_385 = " get";
  protected final String TEXT_386 = "(int index)" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_387 = "(";
  protected final String TEXT_388 = ")";
  protected final String TEXT_389 = "().get(index);" + NL + "\t}" + NL;
  protected final String TEXT_390 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_391 = NL + "\tint get";
  protected final String TEXT_392 = "Length();" + NL;
  protected final String TEXT_393 = NL + "\tpublic int get";
  protected final String TEXT_394 = "Length()" + NL + "\t{";
  protected final String TEXT_395 = NL + "\t\treturn ";
  protected final String TEXT_396 = "().size();";
  protected final String TEXT_397 = NL + "\t\treturn ";
  protected final String TEXT_398 = " == null ? 0 : ";
  protected final String TEXT_399 = ".size();";
  protected final String TEXT_400 = NL + "\t}" + NL;
  protected final String TEXT_401 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_402 = NL + "\tvoid set";
  protected final String TEXT_403 = "(";
  protected final String TEXT_404 = "[] new";
  protected final String TEXT_405 = ");" + NL;
  protected final String TEXT_406 = NL + "\tpublic void set";
  protected final String TEXT_407 = "(";
  protected final String TEXT_408 = "[] new";
  protected final String TEXT_409 = ")" + NL + "\t{" + NL + "\t\t((";
  protected final String TEXT_410 = ")";
  protected final String TEXT_411 = "()).setData(new";
  protected final String TEXT_412 = ".length, new";
  protected final String TEXT_413 = ");" + NL + "\t}" + NL;
  protected final String TEXT_414 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_415 = NL + "\tvoid set";
  protected final String TEXT_416 = "(int index, ";
  protected final String TEXT_417 = " element);" + NL;
  protected final String TEXT_418 = NL + "\tpublic void set";
  protected final String TEXT_419 = "(int index, ";
  protected final String TEXT_420 = " element)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_421 = "().set(index, element);" + NL + "\t}" + NL;
  protected final String TEXT_422 = NL + "\t/**" + NL + "\t * Returns the value of the '<em><b>";
  protected final String TEXT_423 = "</b></em>' ";
  protected final String TEXT_424 = ".";
  protected final String TEXT_425 = NL + "\t * The key is of type ";
  protected final String TEXT_426 = "list of {@link ";
  protected final String TEXT_427 = "}";
  protected final String TEXT_428 = "{@link ";
  protected final String TEXT_429 = "}";
  protected final String TEXT_430 = "," + NL + "\t * and the value is of type ";
  protected final String TEXT_431 = "list of {@link ";
  protected final String TEXT_432 = "}";
  protected final String TEXT_433 = "{@link ";
  protected final String TEXT_434 = "}";
  protected final String TEXT_435 = ",";
  protected final String TEXT_436 = NL + "\t * The list contents are of type {@link ";
  protected final String TEXT_437 = "}";
  protected final String TEXT_438 = ".";
  protected final String TEXT_439 = NL + "\t * The default value is <code>";
  protected final String TEXT_440 = "</code>.";
  protected final String TEXT_441 = NL + "\t * The literals are from the enumeration {@link ";
  protected final String TEXT_442 = "}.";
  protected final String TEXT_443 = NL + "\t * It is bidirectional and its opposite is '{@link ";
  protected final String TEXT_444 = "#";
  protected final String TEXT_445 = " <em>";
  protected final String TEXT_446 = "</em>}'.";
  protected final String TEXT_447 = NL + "\t * <!-- begin-user-doc -->";
  protected final String TEXT_448 = NL + "\t * <p>" + NL + "\t * If the meaning of the '<em>";
  protected final String TEXT_449 = "</em>' ";
  protected final String TEXT_450 = " isn't clear," + NL + "\t * there really should be more of a description here..." + NL + "\t * </p>";
  protected final String TEXT_451 = NL + "\t * <!-- end-user-doc -->";
  protected final String TEXT_452 = NL + "\t * <!-- begin-model-doc -->" + NL + "\t * ";
  protected final String TEXT_453 = NL + "\t * <!-- end-model-doc -->";
  protected final String TEXT_454 = NL + "\t * @return the value of the '<em>";
  protected final String TEXT_455 = "</em>' ";
  protected final String TEXT_456 = ".";
  protected final String TEXT_457 = NL + "\t * @see ";
  protected final String TEXT_458 = NL + "\t * @see #isSet";
  protected final String TEXT_459 = "()";
  protected final String TEXT_460 = NL + "\t * @see #unset";
  protected final String TEXT_461 = "()";
  protected final String TEXT_462 = NL + "\t * @see #set";
  protected final String TEXT_463 = "(";
  protected final String TEXT_464 = ")";
  protected final String TEXT_465 = NL + "\t * @see ";
  protected final String TEXT_466 = "#get";
  protected final String TEXT_467 = "()";
  protected final String TEXT_468 = NL + "\t * @see ";
  protected final String TEXT_469 = "#";
  protected final String TEXT_470 = NL + "\t * @model ";
  protected final String TEXT_471 = NL + "\t *        ";
  protected final String TEXT_472 = NL + "\t * @model";
  protected final String TEXT_473 = NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_474 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_475 = NL + "\t";
  protected final String TEXT_476 = " ";
  protected final String TEXT_477 = "();" + NL;
  protected final String TEXT_478 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_479 = NL + "\tpublic ";
  protected final String TEXT_480 = " ";
  protected final String TEXT_481 = "_";
  protected final String TEXT_482 = "()" + NL + "\t{";
  protected final String TEXT_483 = NL + "\t\treturn ";
  protected final String TEXT_484 = "(";
  protected final String TEXT_485 = "(";
  protected final String TEXT_486 = ")eDynamicGet(";
  protected final String TEXT_487 = ", ";
  protected final String TEXT_488 = ", true, ";
  protected final String TEXT_489 = ")";
  protected final String TEXT_490 = ").";
  protected final String TEXT_491 = "()";
  protected final String TEXT_492 = ";";
  protected final String TEXT_493 = NL + "\t\treturn ";
  protected final String TEXT_494 = "(";
  protected final String TEXT_495 = "(";
  protected final String TEXT_496 = ")eGet(";
  protected final String TEXT_497 = ", true)";
  protected final String TEXT_498 = ").";
  protected final String TEXT_499 = "()";
  protected final String TEXT_500 = ";";
  protected final String TEXT_501 = NL + "\t\treturn ";
  protected final String TEXT_502 = "(";
  protected final String TEXT_503 = "(";
  protected final String TEXT_504 = ")";
  protected final String TEXT_505 = "__ESETTING_DELEGATE.dynamicGet(this, null, 0, true, false)";
  protected final String TEXT_506 = ").";
  protected final String TEXT_507 = "()";
  protected final String TEXT_508 = ";";
  protected final String TEXT_509 = NL + "\t\t";
  protected final String TEXT_510 = " ";
  protected final String TEXT_511 = " = (";
  protected final String TEXT_512 = ")eVirtualGet(";
  protected final String TEXT_513 = ");";
  protected final String TEXT_514 = NL + "\t\tif (";
  protected final String TEXT_515 = " == null)" + NL + "\t\t{";
  protected final String TEXT_516 = NL + "\t\t\teVirtualSet(";
  protected final String TEXT_517 = ", ";
  protected final String TEXT_518 = " = new ";
  protected final String TEXT_519 = ");";
  protected final String TEXT_520 = NL + "\t\t\t";
  protected final String TEXT_521 = " = new ";
  protected final String TEXT_522 = ";";
  protected final String TEXT_523 = NL + "\t\t}" + NL + "\t\treturn ";
  protected final String TEXT_524 = ";";
  protected final String TEXT_525 = NL + "\t\tif (eContainerFeatureID() != ";
  protected final String TEXT_526 = ") return null;" + NL + "\t\treturn (";
  protected final String TEXT_527 = ")eContainer();";
  protected final String TEXT_528 = NL + "\t\t";
  protected final String TEXT_529 = " ";
  protected final String TEXT_530 = " = (";
  protected final String TEXT_531 = ")eVirtualGet(";
  protected final String TEXT_532 = ", ";
  protected final String TEXT_533 = ");";
  protected final String TEXT_534 = NL + "\t\tif (";
  protected final String TEXT_535 = " != null && ";
  protected final String TEXT_536 = ".eIsProxy())" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_537 = " old";
  protected final String TEXT_538 = " = (";
  protected final String TEXT_539 = ")";
  protected final String TEXT_540 = ";" + NL + "\t\t\t";
  protected final String TEXT_541 = " = ";
  protected final String TEXT_542 = "eResolveProxy(old";
  protected final String TEXT_543 = ");" + NL + "\t\t\tif (";
  protected final String TEXT_544 = " != old";
  protected final String TEXT_545 = ")" + NL + "\t\t\t{";
  protected final String TEXT_546 = NL + "\t\t\t\t";
  protected final String TEXT_547 = " new";
  protected final String TEXT_548 = " = (";
  protected final String TEXT_549 = ")";
  protected final String TEXT_550 = ";";
  protected final String TEXT_551 = NL + "\t\t\t\t";
  protected final String TEXT_552 = " msgs = old";
  protected final String TEXT_553 = ".eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_554 = ", null, null);";
  protected final String TEXT_555 = NL + "\t\t\t\t";
  protected final String TEXT_556 = " msgs =  old";
  protected final String TEXT_557 = ".eInverseRemove(this, ";
  protected final String TEXT_558 = ", ";
  protected final String TEXT_559 = ".class, null);";
  protected final String TEXT_560 = NL + "\t\t\t\tif (new";
  protected final String TEXT_561 = ".eInternalContainer() == null)" + NL + "\t\t\t\t{";
  protected final String TEXT_562 = NL + "\t\t\t\t\tmsgs = new";
  protected final String TEXT_563 = ".eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_564 = ", null, msgs);";
  protected final String TEXT_565 = NL + "\t\t\t\t\tmsgs =  new";
  protected final String TEXT_566 = ".eInverseAdd(this, ";
  protected final String TEXT_567 = ", ";
  protected final String TEXT_568 = ".class, msgs);";
  protected final String TEXT_569 = NL + "\t\t\t\t}" + NL + "\t\t\t\tif (msgs != null) msgs.dispatch();";
  protected final String TEXT_570 = NL + "\t\t\t\teVirtualSet(";
  protected final String TEXT_571 = ", ";
  protected final String TEXT_572 = ");";
  protected final String TEXT_573 = NL + "\t\t\t\tif (eNotificationRequired())" + NL + "\t\t\t\t\teNotify(new ";
  protected final String TEXT_574 = "(this, ";
  protected final String TEXT_575 = ".RESOLVE, ";
  protected final String TEXT_576 = ", old";
  protected final String TEXT_577 = ", ";
  protected final String TEXT_578 = "));";
  protected final String TEXT_579 = NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_580 = NL + "\t\treturn (";
  protected final String TEXT_581 = ")eVirtualGet(";
  protected final String TEXT_582 = ", ";
  protected final String TEXT_583 = ");";
  protected final String TEXT_584 = NL + "\t\treturn (";
  protected final String TEXT_585 = " & ";
  protected final String TEXT_586 = "_EFLAG) != 0;";
  protected final String TEXT_587 = NL + "\t\treturn ";
  protected final String TEXT_588 = "_EFLAG_VALUES[(";
  protected final String TEXT_589 = " & ";
  protected final String TEXT_590 = "_EFLAG) >>> ";
  protected final String TEXT_591 = "_EFLAG_OFFSET];";
  protected final String TEXT_592 = NL + "\t\treturn ";
  protected final String TEXT_593 = ";";
  protected final String TEXT_594 = NL + "\t\t";
  protected final String TEXT_595 = " ";
  protected final String TEXT_596 = " = basicGet";
  protected final String TEXT_597 = "();" + NL + "\t\treturn ";
  protected final String TEXT_598 = " != null && ";
  protected final String TEXT_599 = ".eIsProxy() ? ";
  protected final String TEXT_600 = "eResolveProxy((";
  protected final String TEXT_601 = ")";
  protected final String TEXT_602 = ") : ";
  protected final String TEXT_603 = ";";
  protected final String TEXT_604 = NL + "\t\treturn new ";
  protected final String TEXT_605 = "((";
  protected final String TEXT_606 = ".Internal)((";
  protected final String TEXT_607 = ".Internal.Wrapper)get";
  protected final String TEXT_608 = "()).featureMap().";
  protected final String TEXT_609 = "list(";
  protected final String TEXT_610 = "));";
  protected final String TEXT_611 = NL + "\t\treturn (";
  protected final String TEXT_612 = ")get";
  protected final String TEXT_613 = "().";
  protected final String TEXT_614 = "list(";
  protected final String TEXT_615 = ");";
  protected final String TEXT_616 = NL + "\t\treturn ((";
  protected final String TEXT_617 = ".Internal.Wrapper)get";
  protected final String TEXT_618 = "()).featureMap().list(";
  protected final String TEXT_619 = ");";
  protected final String TEXT_620 = NL + "\t\treturn get";
  protected final String TEXT_621 = "().list(";
  protected final String TEXT_622 = ");";
  protected final String TEXT_623 = NL + "\t\treturn ";
  protected final String TEXT_624 = "(";
  protected final String TEXT_625 = "(";
  protected final String TEXT_626 = ")";
  protected final String TEXT_627 = "((";
  protected final String TEXT_628 = ".Internal.Wrapper)get";
  protected final String TEXT_629 = "()).featureMap().get(";
  protected final String TEXT_630 = ", true)";
  protected final String TEXT_631 = ").";
  protected final String TEXT_632 = "()";
  protected final String TEXT_633 = ";";
  protected final String TEXT_634 = NL + "\t\treturn ";
  protected final String TEXT_635 = "(";
  protected final String TEXT_636 = "(";
  protected final String TEXT_637 = ")";
  protected final String TEXT_638 = "get";
  protected final String TEXT_639 = "().get(";
  protected final String TEXT_640 = ", true)";
  protected final String TEXT_641 = ").";
  protected final String TEXT_642 = "()";
  protected final String TEXT_643 = ";";
  protected final String TEXT_644 = NL + "\t\t";
  protected final String TEXT_645 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_646 = NL + "\t\t\t/**" + NL + "\t\t\t * @OCL ";
  protected final String TEXT_647 = NL + "\t\t\t * @templateTag GGFT01" + NL + "\t\t\t */";
  protected final String TEXT_648 = NL + "\t\t    ";
  protected final String TEXT_649 = " eClass = ";
  protected final String TEXT_650 = ";" + NL + "\t\t    ";
  protected final String TEXT_651 = " ";
  protected final String TEXT_652 = " = ";
  protected final String TEXT_653 = ";" + NL + "\t    ";
  protected final String TEXT_654 = NL + "\t\tif (";
  protected final String TEXT_655 = " == null) { " + NL + "\t\t\t";
  protected final String TEXT_656 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setAttributeContext(eClass, ";
  protected final String TEXT_657 = ");" + NL + "\t\t\t" + NL + "\t\t\tString derive = ";
  protected final String TEXT_658 = ".findDeriveAnnotationText(";
  protected final String TEXT_659 = ", eClass());" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_660 = " = helper.createQuery(derive);" + NL + "\t\t\t} catch (";
  protected final String TEXT_661 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_662 = ".valueOf(\"0\").";
  protected final String TEXT_663 = "()";
  protected final String TEXT_664 = "null";
  protected final String TEXT_665 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_666 = ".handleQueryProblems(";
  protected final String TEXT_667 = ".PLUGIN_ID, derive, helper.getProblems(), ";
  protected final String TEXT_668 = ", ";
  protected final String TEXT_669 = ");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_670 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_671 = ");" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_672 = ".enterContext(";
  protected final String TEXT_673 = ".PLUGIN_ID, query, ";
  protected final String TEXT_674 = ", ";
  protected final String TEXT_675 = ");" + NL + "\t";
  protected final String TEXT_676 = NL + "\t\t";
  protected final String TEXT_677 = " xoclEval = new ";
  protected final String TEXT_678 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_679 = " result = (";
  protected final String TEXT_680 = ") xoclEval.evaluateElement(";
  protected final String TEXT_681 = ", query);" + NL + "\t\t";
  protected final String TEXT_682 = NL + "\t\tfor(";
  protected final String TEXT_683 = " object : result) {" + NL + "\t\t\tif (object instanceof ";
  protected final String TEXT_684 = ") {" + NL + "\t\t\t\t((";
  protected final String TEXT_685 = ") object).eBasicSetContainer(this, ";
  protected final String TEXT_686 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_687 = ".";
  protected final String TEXT_688 = ", null);" + NL + "\t\t\t}" + NL + "\t\t} " + NL + "\t\t";
  protected final String TEXT_689 = NL + "\t\tif (result instanceof ";
  protected final String TEXT_690 = ") {" + NL + "\t\t\t((";
  protected final String TEXT_691 = ") result).eBasicSetContainer(this, ";
  protected final String TEXT_692 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_693 = ".";
  protected final String TEXT_694 = ", null);" + NL + "\t\t}\t" + NL + "\t\t";
  protected final String TEXT_695 = NL + "\t\treturn result;" + NL + "\t";
  protected final String TEXT_696 = NL + "\t\treturn ((";
  protected final String TEXT_697 = ") query.evaluate(this)).";
  protected final String TEXT_698 = "();" + NL + "\t";
  protected final String TEXT_699 = NL + "\t\t";
  protected final String TEXT_700 = " xoclEval = new ";
  protected final String TEXT_701 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_702 = " result = (";
  protected final String TEXT_703 = ") xoclEval.evaluateElement(";
  protected final String TEXT_704 = ", query);" + NL + "\t\t";
  protected final String TEXT_705 = NL + "\t\tfor(";
  protected final String TEXT_706 = " object : result) {" + NL + "\t\t\tif (object instanceof ";
  protected final String TEXT_707 = ") {" + NL + "\t\t\t\t((";
  protected final String TEXT_708 = ") object).eBasicSetContainer(this, ";
  protected final String TEXT_709 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_710 = ".";
  protected final String TEXT_711 = ", null);" + NL + "\t\t\t}\t" + NL + "\t\t} " + NL + "\t\t";
  protected final String TEXT_712 = NL + "\t\tif (result instanceof ";
  protected final String TEXT_713 = ") {" + NL + "\t\t\t((";
  protected final String TEXT_714 = ") result).eBasicSetContainer(this, ";
  protected final String TEXT_715 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_716 = ".";
  protected final String TEXT_717 = ", null);" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_718 = NL + "\t\t";
  protected final String TEXT_719 = NL + "\t\treturn result;" + NL + "\t";
  protected final String TEXT_720 = NL + "\t\t} catch(";
  protected final String TEXT_721 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_722 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_723 = ".valueOf(\"0\").";
  protected final String TEXT_724 = "()";
  protected final String TEXT_725 = "null";
  protected final String TEXT_726 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_727 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_728 = NL + "\t}" + NL;
  protected final String TEXT_729 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_730 = NL + "\tpublic ";
  protected final String TEXT_731 = " basicGet";
  protected final String TEXT_732 = "()" + NL + "\t{";
  protected final String TEXT_733 = NL + "\t\treturn (";
  protected final String TEXT_734 = ")eDynamicGet(";
  protected final String TEXT_735 = ", ";
  protected final String TEXT_736 = ", false, ";
  protected final String TEXT_737 = ");";
  protected final String TEXT_738 = NL + "\t\treturn ";
  protected final String TEXT_739 = "(";
  protected final String TEXT_740 = "(";
  protected final String TEXT_741 = ")";
  protected final String TEXT_742 = "__ESETTING_DELEGATE.dynamicGet(this, null, 0, false, false)";
  protected final String TEXT_743 = ").";
  protected final String TEXT_744 = "()";
  protected final String TEXT_745 = ";";
  protected final String TEXT_746 = NL + "\t\tif (eContainerFeatureID() != ";
  protected final String TEXT_747 = ") return null;" + NL + "\t\treturn (";
  protected final String TEXT_748 = ")eInternalContainer();";
  protected final String TEXT_749 = NL + "\t\treturn (";
  protected final String TEXT_750 = ")eVirtualGet(";
  protected final String TEXT_751 = ");";
  protected final String TEXT_752 = NL + "\t\treturn ";
  protected final String TEXT_753 = ";";
  protected final String TEXT_754 = NL + "\t\treturn (";
  protected final String TEXT_755 = ")((";
  protected final String TEXT_756 = ".Internal.Wrapper)get";
  protected final String TEXT_757 = "()).featureMap().get(";
  protected final String TEXT_758 = ", false);";
  protected final String TEXT_759 = NL + "\t\treturn (";
  protected final String TEXT_760 = ")get";
  protected final String TEXT_761 = "().get(";
  protected final String TEXT_762 = ", false);";
  protected final String TEXT_763 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_764 = NL + "\t\t\t/**" + NL + "\t\t\t * @OCL ";
  protected final String TEXT_765 = NL + "\t\t\t * @templateTag GGFT01" + NL + "\t\t\t */";
  protected final String TEXT_766 = NL + "\t\t    ";
  protected final String TEXT_767 = " eClass = ";
  protected final String TEXT_768 = ";" + NL + "\t\t    ";
  protected final String TEXT_769 = " ";
  protected final String TEXT_770 = " = ";
  protected final String TEXT_771 = ";" + NL + "\t    ";
  protected final String TEXT_772 = NL + "\t\tif (";
  protected final String TEXT_773 = " == null) { " + NL + "\t\t\t";
  protected final String TEXT_774 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setAttributeContext(eClass, ";
  protected final String TEXT_775 = ");" + NL + "\t\t\t" + NL + "\t\t\tString derive = ";
  protected final String TEXT_776 = ".findDeriveAnnotationText(";
  protected final String TEXT_777 = ", eClass());" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_778 = " = helper.createQuery(derive);" + NL + "\t\t\t} catch (";
  protected final String TEXT_779 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_780 = ".valueOf(\"0\").";
  protected final String TEXT_781 = "()";
  protected final String TEXT_782 = "null";
  protected final String TEXT_783 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_784 = ".handleQueryProblems(";
  protected final String TEXT_785 = ".PLUGIN_ID, derive, helper.getProblems(), ";
  protected final String TEXT_786 = ", ";
  protected final String TEXT_787 = ");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_788 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_789 = ");" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_790 = ".enterContext(";
  protected final String TEXT_791 = ".PLUGIN_ID, query, ";
  protected final String TEXT_792 = ", ";
  protected final String TEXT_793 = ");" + NL + "\t";
  protected final String TEXT_794 = NL + "\t\t";
  protected final String TEXT_795 = " xoclEval = new ";
  protected final String TEXT_796 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_797 = " result = (";
  protected final String TEXT_798 = ") xoclEval.evaluateElement(";
  protected final String TEXT_799 = ", query);" + NL + "\t\t";
  protected final String TEXT_800 = NL + "\t\tfor(";
  protected final String TEXT_801 = " object : result) {" + NL + "\t\t\tif (object instanceof ";
  protected final String TEXT_802 = ") {" + NL + "\t\t\t\t((";
  protected final String TEXT_803 = ") object).eBasicSetContainer(this, ";
  protected final String TEXT_804 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_805 = ".";
  protected final String TEXT_806 = ", null);" + NL + "\t\t\t}" + NL + "\t\t} " + NL + "\t\t";
  protected final String TEXT_807 = NL + "\t\tif (result instanceof ";
  protected final String TEXT_808 = ") {" + NL + "\t\t\t((";
  protected final String TEXT_809 = ") result).eBasicSetContainer(this, ";
  protected final String TEXT_810 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_811 = ".";
  protected final String TEXT_812 = ", null);" + NL + "\t\t}\t" + NL + "\t\t";
  protected final String TEXT_813 = NL + "\t\treturn result;" + NL + "\t";
  protected final String TEXT_814 = NL + "\t\treturn ((";
  protected final String TEXT_815 = ") query.evaluate(this)).";
  protected final String TEXT_816 = "();" + NL + "\t";
  protected final String TEXT_817 = NL + "\t\t";
  protected final String TEXT_818 = " xoclEval = new ";
  protected final String TEXT_819 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_820 = " result = (";
  protected final String TEXT_821 = ") xoclEval.evaluateElement(";
  protected final String TEXT_822 = ", query);" + NL + "\t\t";
  protected final String TEXT_823 = NL + "\t\tfor(";
  protected final String TEXT_824 = " object : result) {" + NL + "\t\t\tif (object instanceof ";
  protected final String TEXT_825 = ") {" + NL + "\t\t\t\t((";
  protected final String TEXT_826 = ") object).eBasicSetContainer(this, ";
  protected final String TEXT_827 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_828 = ".";
  protected final String TEXT_829 = ", null);" + NL + "\t\t\t}\t" + NL + "\t\t} " + NL + "\t\t";
  protected final String TEXT_830 = NL + "\t\tif (result instanceof ";
  protected final String TEXT_831 = ") {" + NL + "\t\t\t((";
  protected final String TEXT_832 = ") result).eBasicSetContainer(this, ";
  protected final String TEXT_833 = ".EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_834 = ".";
  protected final String TEXT_835 = ", null);" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_836 = NL + "\t\t";
  protected final String TEXT_837 = NL + "\t\treturn result;" + NL + "\t";
  protected final String TEXT_838 = NL + "\t\t} catch(";
  protected final String TEXT_839 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_840 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_841 = ".valueOf(\"0\").";
  protected final String TEXT_842 = "()";
  protected final String TEXT_843 = "null";
  protected final String TEXT_844 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_845 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_846 = NL + "\t}" + NL;
  protected final String TEXT_847 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_848 = NL + "\tpublic ";
  protected final String TEXT_849 = " basicSet";
  protected final String TEXT_850 = "(";
  protected final String TEXT_851 = " new";
  protected final String TEXT_852 = ", ";
  protected final String TEXT_853 = " msgs)" + NL + "\t{";
  protected final String TEXT_854 = NL + "\t\tmsgs = eBasicSetContainer((";
  protected final String TEXT_855 = ")new";
  protected final String TEXT_856 = ", ";
  protected final String TEXT_857 = ", msgs);";
  protected final String TEXT_858 = NL + "\t\treturn msgs;";
  protected final String TEXT_859 = NL + "\t\tmsgs = eDynamicInverseAdd((";
  protected final String TEXT_860 = ")new";
  protected final String TEXT_861 = ", ";
  protected final String TEXT_862 = ", msgs);";
  protected final String TEXT_863 = NL + "\t\treturn msgs;";
  protected final String TEXT_864 = NL + "\t\tObject old";
  protected final String TEXT_865 = " = eVirtualSet(";
  protected final String TEXT_866 = ", new";
  protected final String TEXT_867 = ");";
  protected final String TEXT_868 = NL + "\t\t";
  protected final String TEXT_869 = " old";
  protected final String TEXT_870 = " = ";
  protected final String TEXT_871 = ";" + NL + "\t\t";
  protected final String TEXT_872 = " = new";
  protected final String TEXT_873 = ";";
  protected final String TEXT_874 = NL + "\t\tboolean isSetChange = old";
  protected final String TEXT_875 = " == EVIRTUAL_NO_VALUE;";
  protected final String TEXT_876 = NL + "\t\tboolean old";
  protected final String TEXT_877 = "ESet = (";
  protected final String TEXT_878 = " & ";
  protected final String TEXT_879 = "_ESETFLAG) != 0;";
  protected final String TEXT_880 = NL + "\t\t";
  protected final String TEXT_881 = " |= ";
  protected final String TEXT_882 = "_ESETFLAG;";
  protected final String TEXT_883 = NL + "\t\tboolean old";
  protected final String TEXT_884 = "ESet = ";
  protected final String TEXT_885 = "ESet;";
  protected final String TEXT_886 = NL + "\t\t";
  protected final String TEXT_887 = "ESet = true;";
  protected final String TEXT_888 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t{";
  protected final String TEXT_889 = NL + "\t\t\t";
  protected final String TEXT_890 = " notification = new ";
  protected final String TEXT_891 = "(this, ";
  protected final String TEXT_892 = ".SET, ";
  protected final String TEXT_893 = ", ";
  protected final String TEXT_894 = "isSetChange ? null : old";
  protected final String TEXT_895 = "old";
  protected final String TEXT_896 = ", new";
  protected final String TEXT_897 = ", ";
  protected final String TEXT_898 = "isSetChange";
  protected final String TEXT_899 = "!old";
  protected final String TEXT_900 = "ESet";
  protected final String TEXT_901 = ");";
  protected final String TEXT_902 = NL + "\t\t\t";
  protected final String TEXT_903 = " notification = new ";
  protected final String TEXT_904 = "(this, ";
  protected final String TEXT_905 = ".SET, ";
  protected final String TEXT_906 = ", ";
  protected final String TEXT_907 = "old";
  protected final String TEXT_908 = " == EVIRTUAL_NO_VALUE ? null : old";
  protected final String TEXT_909 = "old";
  protected final String TEXT_910 = ", new";
  protected final String TEXT_911 = ");";
  protected final String TEXT_912 = NL + "\t\t\tif (msgs == null) msgs = notification; else msgs.add(notification);" + NL + "\t\t}";
  protected final String TEXT_913 = NL + "\t\treturn msgs;";
  protected final String TEXT_914 = NL + "\t\treturn ((";
  protected final String TEXT_915 = ".Internal)((";
  protected final String TEXT_916 = ".Internal.Wrapper)get";
  protected final String TEXT_917 = "()).featureMap()).basicAdd(";
  protected final String TEXT_918 = ", new";
  protected final String TEXT_919 = ", msgs);";
  protected final String TEXT_920 = NL + "\t\treturn ((";
  protected final String TEXT_921 = ".Internal)get";
  protected final String TEXT_922 = "()).basicAdd(";
  protected final String TEXT_923 = ", new";
  protected final String TEXT_924 = ", msgs);";
  protected final String TEXT_925 = NL + "\t\tif (new";
  protected final String TEXT_926 = " == null) {" + NL + "\t\t\treturn msgs;" + NL + "\t\t}" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_927 = NL + "\t\t// TODO: implement this method to set the contained '";
  protected final String TEXT_928 = "' ";
  protected final String TEXT_929 = NL + "\t\t// -> this method is automatically invoked to keep the containment relationship in synch" + NL + "\t\t// -> do not modify other features" + NL + "\t\t// -> return msgs, after adding any generated Notification to it (if it is null, a NotificationChain object must be created first)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_930 = NL + "\t}" + NL;
  protected final String TEXT_931 = NL + "\t/** " + NL + "\t * Sets the value of the '{@link ";
  protected final String TEXT_932 = "#";
  protected final String TEXT_933 = " <em>";
  protected final String TEXT_934 = "</em>}' ";
  protected final String TEXT_935 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t  ";
  protected final String TEXT_936 = NL + "\t * This feature is generated using custom templates" + NL + "\t * Just for API use. All derived changeable features are handled using the XSemantics framework";
  protected final String TEXT_937 = NL + "\t * <!-- end-user-doc -->" + NL + "\t * @param value the new value of the '<em>";
  protected final String TEXT_938 = "</em>' ";
  protected final String TEXT_939 = ".";
  protected final String TEXT_940 = NL + "\t * @see ";
  protected final String TEXT_941 = NL + "\t * @see #isSet";
  protected final String TEXT_942 = "()";
  protected final String TEXT_943 = NL + "\t * @see #unset";
  protected final String TEXT_944 = "()";
  protected final String TEXT_945 = NL + "\t * @see #";
  protected final String TEXT_946 = "()" + NL + "\t * @generated" + NL + "\t */" + NL;
  protected final String TEXT_947 = NL + "    /**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t ";
  protected final String TEXT_948 = NL + "\t * This feature is generated using custom templates" + NL + "\t * Just for API use. All derived changeable features are handled using the XSemantics framework";
  protected final String TEXT_949 = NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_950 = NL + "\tvoid set";
  protected final String TEXT_951 = "(";
  protected final String TEXT_952 = " value);" + NL;
  protected final String TEXT_953 = NL + "\tpublic void set";
  protected final String TEXT_954 = "_";
  protected final String TEXT_955 = "(";
  protected final String TEXT_956 = " ";
  protected final String TEXT_957 = ")" + NL + "\t{";
  protected final String TEXT_958 = NL + "\t\teDynamicSet(";
  protected final String TEXT_959 = ", ";
  protected final String TEXT_960 = ", ";
  protected final String TEXT_961 = "new ";
  protected final String TEXT_962 = "(";
  protected final String TEXT_963 = "new";
  protected final String TEXT_964 = ")";
  protected final String TEXT_965 = ");";
  protected final String TEXT_966 = NL + "\t\teSet(";
  protected final String TEXT_967 = ", ";
  protected final String TEXT_968 = "new ";
  protected final String TEXT_969 = "(";
  protected final String TEXT_970 = "new";
  protected final String TEXT_971 = ")";
  protected final String TEXT_972 = ");";
  protected final String TEXT_973 = NL + "\t\t";
  protected final String TEXT_974 = "__ESETTING_DELEGATE.dynamicSet(this, null, 0, ";
  protected final String TEXT_975 = "new ";
  protected final String TEXT_976 = "(";
  protected final String TEXT_977 = "new";
  protected final String TEXT_978 = ")";
  protected final String TEXT_979 = ");";
  protected final String TEXT_980 = NL + "\t\tif (new";
  protected final String TEXT_981 = " != eInternalContainer() || (eContainerFeatureID() != ";
  protected final String TEXT_982 = " && new";
  protected final String TEXT_983 = " != null))" + NL + "\t\t{" + NL + "\t\t\tif (";
  protected final String TEXT_984 = ".isAncestor(this, ";
  protected final String TEXT_985 = "new";
  protected final String TEXT_986 = "))" + NL + "\t\t\t\tthrow new ";
  protected final String TEXT_987 = "(\"Recursive containment not allowed for \" + toString());";
  protected final String TEXT_988 = NL + "\t\t\t";
  protected final String TEXT_989 = " msgs = null;" + NL + "\t\t\tif (eInternalContainer() != null)" + NL + "\t\t\t\tmsgs = eBasicRemoveFromContainer(msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_990 = " != null)" + NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_991 = ")new";
  protected final String TEXT_992 = ").eInverseAdd(this, ";
  protected final String TEXT_993 = ", ";
  protected final String TEXT_994 = ".class, msgs);" + NL + "\t\t\tmsgs = basicSet";
  protected final String TEXT_995 = "(";
  protected final String TEXT_996 = "new";
  protected final String TEXT_997 = ", msgs);" + NL + "\t\t\tif (msgs != null) msgs.dispatch();" + NL + "\t\t}";
  protected final String TEXT_998 = NL + "\t\telse if (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_999 = "(this, ";
  protected final String TEXT_1000 = ".SET, ";
  protected final String TEXT_1001 = ", new";
  protected final String TEXT_1002 = ", new";
  protected final String TEXT_1003 = "));";
  protected final String TEXT_1004 = NL + "\t\t";
  protected final String TEXT_1005 = " ";
  protected final String TEXT_1006 = " = (";
  protected final String TEXT_1007 = ")eVirtualGet(";
  protected final String TEXT_1008 = ");";
  protected final String TEXT_1009 = NL + "\t\tif (new";
  protected final String TEXT_1010 = " != ";
  protected final String TEXT_1011 = ")" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_1012 = " msgs = null;" + NL + "\t\t\tif (";
  protected final String TEXT_1013 = " != null)";
  protected final String TEXT_1014 = NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_1015 = ")";
  protected final String TEXT_1016 = ").eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_1017 = ", null, msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_1018 = " != null)" + NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_1019 = ")new";
  protected final String TEXT_1020 = ").eInverseAdd(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_1021 = ", null, msgs);";
  protected final String TEXT_1022 = NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_1023 = ")";
  protected final String TEXT_1024 = ").eInverseRemove(this, ";
  protected final String TEXT_1025 = ", ";
  protected final String TEXT_1026 = ".class, msgs);" + NL + "\t\t\tif (new";
  protected final String TEXT_1027 = " != null)" + NL + "\t\t\t\tmsgs = ((";
  protected final String TEXT_1028 = ")new";
  protected final String TEXT_1029 = ").eInverseAdd(this, ";
  protected final String TEXT_1030 = ", ";
  protected final String TEXT_1031 = ".class, msgs);";
  protected final String TEXT_1032 = NL + "\t\t\tmsgs = basicSet";
  protected final String TEXT_1033 = "(";
  protected final String TEXT_1034 = "new";
  protected final String TEXT_1035 = ", msgs);" + NL + "\t\t\tif (msgs != null) msgs.dispatch();" + NL + "\t\t}";
  protected final String TEXT_1036 = NL + "\t\telse" + NL + "\t\t{";
  protected final String TEXT_1037 = NL + "\t\t\tboolean old";
  protected final String TEXT_1038 = "ESet = eVirtualIsSet(";
  protected final String TEXT_1039 = ");";
  protected final String TEXT_1040 = NL + "\t\t\tboolean old";
  protected final String TEXT_1041 = "ESet = (";
  protected final String TEXT_1042 = " & ";
  protected final String TEXT_1043 = "_ESETFLAG) != 0;";
  protected final String TEXT_1044 = NL + "\t\t\t";
  protected final String TEXT_1045 = " |= ";
  protected final String TEXT_1046 = "_ESETFLAG;";
  protected final String TEXT_1047 = NL + "\t\t\tboolean old";
  protected final String TEXT_1048 = "ESet = ";
  protected final String TEXT_1049 = "ESet;";
  protected final String TEXT_1050 = NL + "\t\t\t";
  protected final String TEXT_1051 = "ESet = true;";
  protected final String TEXT_1052 = NL + "\t\t\tif (eNotificationRequired())" + NL + "\t\t\t\teNotify(new ";
  protected final String TEXT_1053 = "(this, ";
  protected final String TEXT_1054 = ".SET, ";
  protected final String TEXT_1055 = ", new";
  protected final String TEXT_1056 = ", new";
  protected final String TEXT_1057 = ", !old";
  protected final String TEXT_1058 = "ESet));";
  protected final String TEXT_1059 = NL + "\t\t}";
  protected final String TEXT_1060 = NL + "\t\telse if (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_1061 = "(this, ";
  protected final String TEXT_1062 = ".SET, ";
  protected final String TEXT_1063 = ", new";
  protected final String TEXT_1064 = ", new";
  protected final String TEXT_1065 = "));";
  protected final String TEXT_1066 = NL + "\t\t";
  protected final String TEXT_1067 = " old";
  protected final String TEXT_1068 = " = (";
  protected final String TEXT_1069 = " & ";
  protected final String TEXT_1070 = "_EFLAG) != 0;";
  protected final String TEXT_1071 = NL + "\t\t";
  protected final String TEXT_1072 = " old";
  protected final String TEXT_1073 = " = ";
  protected final String TEXT_1074 = "_EFLAG_VALUES[(";
  protected final String TEXT_1075 = " & ";
  protected final String TEXT_1076 = "_EFLAG) >>> ";
  protected final String TEXT_1077 = "_EFLAG_OFFSET];";
  protected final String TEXT_1078 = NL + "\t\tif (new";
  protected final String TEXT_1079 = ") ";
  protected final String TEXT_1080 = " |= ";
  protected final String TEXT_1081 = "_EFLAG; else ";
  protected final String TEXT_1082 = " &= ~";
  protected final String TEXT_1083 = "_EFLAG;";
  protected final String TEXT_1084 = NL + "\t\tif (new";
  protected final String TEXT_1085 = " == null) new";
  protected final String TEXT_1086 = " = ";
  protected final String TEXT_1087 = "_EDEFAULT;" + NL + "\t\t";
  protected final String TEXT_1088 = " = ";
  protected final String TEXT_1089 = " & ~";
  protected final String TEXT_1090 = "_EFLAG | ";
  protected final String TEXT_1091 = "new";
  protected final String TEXT_1092 = ".ordinal()";
  protected final String TEXT_1093 = ".VALUES.indexOf(new";
  protected final String TEXT_1094 = ")";
  protected final String TEXT_1095 = " << ";
  protected final String TEXT_1096 = "_EFLAG_OFFSET;";
  protected final String TEXT_1097 = NL + "\t\t";
  protected final String TEXT_1098 = " old";
  protected final String TEXT_1099 = " = ";
  protected final String TEXT_1100 = ";";
  protected final String TEXT_1101 = NL + "\t\t";
  protected final String TEXT_1102 = " ";
  protected final String TEXT_1103 = " = new";
  protected final String TEXT_1104 = " == null ? ";
  protected final String TEXT_1105 = " : new";
  protected final String TEXT_1106 = ";";
  protected final String TEXT_1107 = NL + "\t\t";
  protected final String TEXT_1108 = " = new";
  protected final String TEXT_1109 = " == null ? ";
  protected final String TEXT_1110 = " : new";
  protected final String TEXT_1111 = ";";
  protected final String TEXT_1112 = NL + "\t\t";
  protected final String TEXT_1113 = " ";
  protected final String TEXT_1114 = " = ";
  protected final String TEXT_1115 = "new";
  protected final String TEXT_1116 = ";";
  protected final String TEXT_1117 = NL + "\t\t";
  protected final String TEXT_1118 = " = ";
  protected final String TEXT_1119 = "new";
  protected final String TEXT_1120 = ";";
  protected final String TEXT_1121 = NL + "\t\tObject old";
  protected final String TEXT_1122 = " = eVirtualSet(";
  protected final String TEXT_1123 = ", ";
  protected final String TEXT_1124 = ");";
  protected final String TEXT_1125 = NL + "\t\tboolean isSetChange = old";
  protected final String TEXT_1126 = " == EVIRTUAL_NO_VALUE;";
  protected final String TEXT_1127 = NL + "\t\tboolean old";
  protected final String TEXT_1128 = "ESet = (";
  protected final String TEXT_1129 = " & ";
  protected final String TEXT_1130 = "_ESETFLAG) != 0;";
  protected final String TEXT_1131 = NL + "\t\t";
  protected final String TEXT_1132 = " |= ";
  protected final String TEXT_1133 = "_ESETFLAG;";
  protected final String TEXT_1134 = NL + "\t\tboolean old";
  protected final String TEXT_1135 = "ESet = ";
  protected final String TEXT_1136 = "ESet;";
  protected final String TEXT_1137 = NL + "\t\t";
  protected final String TEXT_1138 = "ESet = true;";
  protected final String TEXT_1139 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_1140 = "(this, ";
  protected final String TEXT_1141 = ".SET, ";
  protected final String TEXT_1142 = ", ";
  protected final String TEXT_1143 = "isSetChange ? ";
  protected final String TEXT_1144 = " : old";
  protected final String TEXT_1145 = "old";
  protected final String TEXT_1146 = ", ";
  protected final String TEXT_1147 = "new";
  protected final String TEXT_1148 = ", ";
  protected final String TEXT_1149 = "isSetChange";
  protected final String TEXT_1150 = "!old";
  protected final String TEXT_1151 = "ESet";
  protected final String TEXT_1152 = "));";
  protected final String TEXT_1153 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_1154 = "(this, ";
  protected final String TEXT_1155 = ".SET, ";
  protected final String TEXT_1156 = ", ";
  protected final String TEXT_1157 = "old";
  protected final String TEXT_1158 = " == EVIRTUAL_NO_VALUE ? ";
  protected final String TEXT_1159 = " : old";
  protected final String TEXT_1160 = "old";
  protected final String TEXT_1161 = ", ";
  protected final String TEXT_1162 = "new";
  protected final String TEXT_1163 = "));";
  protected final String TEXT_1164 = NL + "\t\t((";
  protected final String TEXT_1165 = ".Internal)((";
  protected final String TEXT_1166 = ".Internal.Wrapper)get";
  protected final String TEXT_1167 = "()).featureMap()).set(";
  protected final String TEXT_1168 = ", ";
  protected final String TEXT_1169 = "new ";
  protected final String TEXT_1170 = "(";
  protected final String TEXT_1171 = "new";
  protected final String TEXT_1172 = ")";
  protected final String TEXT_1173 = ");";
  protected final String TEXT_1174 = NL + "\t\t((";
  protected final String TEXT_1175 = ".Internal)get";
  protected final String TEXT_1176 = "()).set(";
  protected final String TEXT_1177 = ", ";
  protected final String TEXT_1178 = "new ";
  protected final String TEXT_1179 = "(";
  protected final String TEXT_1180 = "new";
  protected final String TEXT_1181 = ")";
  protected final String TEXT_1182 = ");";
  protected final String TEXT_1183 = NL + "\t\t";
  protected final String TEXT_1184 = NL + "\t\tif (new";
  protected final String TEXT_1185 = " == null) {" + NL + "\t\t\treturn;" + NL + "\t\t}" + NL + "\t\t//Todo: Call XUpdate, getEditingDomain  execute XUpdate" + NL + "        //org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);" + NL + "\t\t//domain.getCommandStack().execute(command);" + NL + "\t\t" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_1186 = NL + "\t}" + NL;
  protected final String TEXT_1187 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1188 = NL + "\tpublic ";
  protected final String TEXT_1189 = " basicUnset";
  protected final String TEXT_1190 = "(";
  protected final String TEXT_1191 = " msgs)" + NL + "\t{";
  protected final String TEXT_1192 = NL + "\t\treturn eDynamicInverseRemove((";
  protected final String TEXT_1193 = ")";
  protected final String TEXT_1194 = "basicGet";
  protected final String TEXT_1195 = "(), ";
  protected final String TEXT_1196 = ", msgs);";
  protected final String TEXT_1197 = "Object old";
  protected final String TEXT_1198 = " = ";
  protected final String TEXT_1199 = "eVirtualUnset(";
  protected final String TEXT_1200 = ");";
  protected final String TEXT_1201 = NL + "\t\t";
  protected final String TEXT_1202 = " old";
  protected final String TEXT_1203 = " = ";
  protected final String TEXT_1204 = ";";
  protected final String TEXT_1205 = NL + "\t\t";
  protected final String TEXT_1206 = " = null;";
  protected final String TEXT_1207 = NL + "\t\tboolean isSetChange = old";
  protected final String TEXT_1208 = " != EVIRTUAL_NO_VALUE;";
  protected final String TEXT_1209 = NL + "\t\tboolean old";
  protected final String TEXT_1210 = "ESet = (";
  protected final String TEXT_1211 = " & ";
  protected final String TEXT_1212 = "_ESETFLAG) != 0;";
  protected final String TEXT_1213 = NL + "\t\t";
  protected final String TEXT_1214 = " &= ~";
  protected final String TEXT_1215 = "_ESETFLAG;";
  protected final String TEXT_1216 = NL + "\t\tboolean old";
  protected final String TEXT_1217 = "ESet = ";
  protected final String TEXT_1218 = "ESet;";
  protected final String TEXT_1219 = NL + "\t\t";
  protected final String TEXT_1220 = "ESet = false;";
  protected final String TEXT_1221 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_1222 = " notification = new ";
  protected final String TEXT_1223 = "(this, ";
  protected final String TEXT_1224 = ".UNSET, ";
  protected final String TEXT_1225 = ", ";
  protected final String TEXT_1226 = "isSetChange ? old";
  protected final String TEXT_1227 = " : null";
  protected final String TEXT_1228 = "old";
  protected final String TEXT_1229 = ", null, ";
  protected final String TEXT_1230 = "isSetChange";
  protected final String TEXT_1231 = "old";
  protected final String TEXT_1232 = "ESet";
  protected final String TEXT_1233 = ");" + NL + "\t\t\tif (msgs == null) msgs = notification; else msgs.add(notification);" + NL + "\t\t}" + NL + "\t\treturn msgs;";
  protected final String TEXT_1234 = NL + "\t\t// TODO: implement this method to unset the contained '";
  protected final String TEXT_1235 = "' ";
  protected final String TEXT_1236 = NL + "\t\t// -> this method is automatically invoked to keep the containment relationship in synch" + NL + "\t\t// -> do not modify other features" + NL + "\t\t// -> return msgs, after adding any generated Notification to it (if it is null, a NotificationChain object must be created first)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_1237 = NL + "\t}" + NL;
  protected final String TEXT_1238 = NL + "\t/**" + NL + "\t * Unsets the value of the '{@link ";
  protected final String TEXT_1239 = "#";
  protected final String TEXT_1240 = " <em>";
  protected final String TEXT_1241 = "</em>}' ";
  protected final String TEXT_1242 = ".";
  protected final String TEXT_1243 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->";
  protected final String TEXT_1244 = NL + "\t * @see #isSet";
  protected final String TEXT_1245 = "()";
  protected final String TEXT_1246 = NL + "\t * @see #";
  protected final String TEXT_1247 = "()";
  protected final String TEXT_1248 = NL + "\t * @see #set";
  protected final String TEXT_1249 = "(";
  protected final String TEXT_1250 = ")";
  protected final String TEXT_1251 = NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1252 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1253 = NL + "\tvoid unset";
  protected final String TEXT_1254 = "();" + NL;
  protected final String TEXT_1255 = NL + "\tpublic void unset";
  protected final String TEXT_1256 = "_";
  protected final String TEXT_1257 = "()" + NL + "\t{";
  protected final String TEXT_1258 = NL + "\t\teDynamicUnset(";
  protected final String TEXT_1259 = ", ";
  protected final String TEXT_1260 = ");";
  protected final String TEXT_1261 = NL + "\t\teUnset(";
  protected final String TEXT_1262 = ");";
  protected final String TEXT_1263 = NL + "\t\t";
  protected final String TEXT_1264 = "__ESETTING_DELEGATE.dynamicUnset(this, null, 0);";
  protected final String TEXT_1265 = NL + "\t\t";
  protected final String TEXT_1266 = " ";
  protected final String TEXT_1267 = " = (";
  protected final String TEXT_1268 = ")eVirtualGet(";
  protected final String TEXT_1269 = ");";
  protected final String TEXT_1270 = NL + "\t\tif (";
  protected final String TEXT_1271 = " != null) ((";
  protected final String TEXT_1272 = ".Unsettable";
  protected final String TEXT_1273 = ")";
  protected final String TEXT_1274 = ").unset();";
  protected final String TEXT_1275 = NL + "\t\t";
  protected final String TEXT_1276 = " ";
  protected final String TEXT_1277 = " = (";
  protected final String TEXT_1278 = ")eVirtualGet(";
  protected final String TEXT_1279 = ");";
  protected final String TEXT_1280 = NL + "\t\tif (";
  protected final String TEXT_1281 = " != null)" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_1282 = " msgs = null;";
  protected final String TEXT_1283 = NL + "\t\t\tmsgs = ((";
  protected final String TEXT_1284 = ")";
  protected final String TEXT_1285 = ").eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_1286 = ", null, msgs);";
  protected final String TEXT_1287 = NL + "\t\t\tmsgs = ((";
  protected final String TEXT_1288 = ")";
  protected final String TEXT_1289 = ").eInverseRemove(this, ";
  protected final String TEXT_1290 = ", ";
  protected final String TEXT_1291 = ".class, msgs);";
  protected final String TEXT_1292 = NL + "\t\t\tmsgs = basicUnset";
  protected final String TEXT_1293 = "(msgs);" + NL + "\t\t\tif (msgs != null) msgs.dispatch();" + NL + "\t\t}" + NL + "\t\telse" + NL + "\t\t{";
  protected final String TEXT_1294 = NL + "\t\t\tboolean old";
  protected final String TEXT_1295 = "ESet = eVirtualIsSet(";
  protected final String TEXT_1296 = ");";
  protected final String TEXT_1297 = NL + "\t\t\tboolean old";
  protected final String TEXT_1298 = "ESet = (";
  protected final String TEXT_1299 = " & ";
  protected final String TEXT_1300 = "_ESETFLAG) != 0;";
  protected final String TEXT_1301 = NL + "\t\t\t";
  protected final String TEXT_1302 = " &= ~";
  protected final String TEXT_1303 = "_ESETFLAG;";
  protected final String TEXT_1304 = NL + "\t\t\tboolean old";
  protected final String TEXT_1305 = "ESet = ";
  protected final String TEXT_1306 = "ESet;";
  protected final String TEXT_1307 = NL + "\t\t\t";
  protected final String TEXT_1308 = "ESet = false;";
  protected final String TEXT_1309 = NL + "\t\t\tif (eNotificationRequired())" + NL + "\t\t\t\teNotify(new ";
  protected final String TEXT_1310 = "(this, ";
  protected final String TEXT_1311 = ".UNSET, ";
  protected final String TEXT_1312 = ", null, null, old";
  protected final String TEXT_1313 = "ESet));";
  protected final String TEXT_1314 = NL + "\t\t}";
  protected final String TEXT_1315 = NL + "\t\t";
  protected final String TEXT_1316 = " old";
  protected final String TEXT_1317 = " = (";
  protected final String TEXT_1318 = " & ";
  protected final String TEXT_1319 = "_EFLAG) != 0;";
  protected final String TEXT_1320 = NL + "\t\t";
  protected final String TEXT_1321 = " old";
  protected final String TEXT_1322 = " = ";
  protected final String TEXT_1323 = "_EFLAG_VALUES[(";
  protected final String TEXT_1324 = " & ";
  protected final String TEXT_1325 = "_EFLAG) >>> ";
  protected final String TEXT_1326 = "_EFLAG_OFFSET];";
  protected final String TEXT_1327 = NL + "\t\tObject old";
  protected final String TEXT_1328 = " = eVirtualUnset(";
  protected final String TEXT_1329 = ");";
  protected final String TEXT_1330 = NL + "\t\t";
  protected final String TEXT_1331 = " old";
  protected final String TEXT_1332 = " = ";
  protected final String TEXT_1333 = ";";
  protected final String TEXT_1334 = NL + "\t\tboolean isSetChange = old";
  protected final String TEXT_1335 = " != EVIRTUAL_NO_VALUE;";
  protected final String TEXT_1336 = NL + "\t\tboolean old";
  protected final String TEXT_1337 = "ESet = (";
  protected final String TEXT_1338 = " & ";
  protected final String TEXT_1339 = "_ESETFLAG) != 0;";
  protected final String TEXT_1340 = NL + "\t\tboolean old";
  protected final String TEXT_1341 = "ESet = ";
  protected final String TEXT_1342 = "ESet;";
  protected final String TEXT_1343 = NL + "\t\t";
  protected final String TEXT_1344 = " = null;";
  protected final String TEXT_1345 = NL + "\t\t";
  protected final String TEXT_1346 = " &= ~";
  protected final String TEXT_1347 = "_ESETFLAG;";
  protected final String TEXT_1348 = NL + "\t\t";
  protected final String TEXT_1349 = "ESet = false;";
  protected final String TEXT_1350 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_1351 = "(this, ";
  protected final String TEXT_1352 = ".UNSET, ";
  protected final String TEXT_1353 = ", ";
  protected final String TEXT_1354 = "isSetChange ? old";
  protected final String TEXT_1355 = " : null";
  protected final String TEXT_1356 = "old";
  protected final String TEXT_1357 = ", null, ";
  protected final String TEXT_1358 = "isSetChange";
  protected final String TEXT_1359 = "old";
  protected final String TEXT_1360 = "ESet";
  protected final String TEXT_1361 = "));";
  protected final String TEXT_1362 = NL + "\t\tif (";
  protected final String TEXT_1363 = ") ";
  protected final String TEXT_1364 = " |= ";
  protected final String TEXT_1365 = "_EFLAG; else ";
  protected final String TEXT_1366 = " &= ~";
  protected final String TEXT_1367 = "_EFLAG;";
  protected final String TEXT_1368 = NL + "\t\t";
  protected final String TEXT_1369 = " = ";
  protected final String TEXT_1370 = " & ~";
  protected final String TEXT_1371 = "_EFLAG | ";
  protected final String TEXT_1372 = "_EFLAG_DEFAULT;";
  protected final String TEXT_1373 = NL + "\t\t";
  protected final String TEXT_1374 = " = ";
  protected final String TEXT_1375 = ";";
  protected final String TEXT_1376 = NL + "\t\t";
  protected final String TEXT_1377 = " &= ~";
  protected final String TEXT_1378 = "_ESETFLAG;";
  protected final String TEXT_1379 = NL + "\t\t";
  protected final String TEXT_1380 = "ESet = false;";
  protected final String TEXT_1381 = NL + "\t\tif (eNotificationRequired())" + NL + "\t\t\teNotify(new ";
  protected final String TEXT_1382 = "(this, ";
  protected final String TEXT_1383 = ".UNSET, ";
  protected final String TEXT_1384 = ", ";
  protected final String TEXT_1385 = "isSetChange ? old";
  protected final String TEXT_1386 = " : ";
  protected final String TEXT_1387 = "old";
  protected final String TEXT_1388 = ", ";
  protected final String TEXT_1389 = ", ";
  protected final String TEXT_1390 = "isSetChange";
  protected final String TEXT_1391 = "old";
  protected final String TEXT_1392 = "ESet";
  protected final String TEXT_1393 = "));";
  protected final String TEXT_1394 = NL + "\t\t((";
  protected final String TEXT_1395 = ".Internal)((";
  protected final String TEXT_1396 = ".Internal.Wrapper)get";
  protected final String TEXT_1397 = "()).featureMap()).clear(";
  protected final String TEXT_1398 = ");";
  protected final String TEXT_1399 = NL + "\t\t((";
  protected final String TEXT_1400 = ".Internal)get";
  protected final String TEXT_1401 = "()).clear(";
  protected final String TEXT_1402 = ");";
  protected final String TEXT_1403 = NL + "\t\t";
  protected final String TEXT_1404 = NL + "\t\t// TODO: implement this method to unset the '";
  protected final String TEXT_1405 = "' ";
  protected final String TEXT_1406 = NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_1407 = NL + "\t}" + NL;
  protected final String TEXT_1408 = NL + "\t/**" + NL + "\t * Returns whether the value of the '{@link ";
  protected final String TEXT_1409 = "#";
  protected final String TEXT_1410 = " <em>";
  protected final String TEXT_1411 = "</em>}' ";
  protected final String TEXT_1412 = " is set.";
  protected final String TEXT_1413 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @return whether the value of the '<em>";
  protected final String TEXT_1414 = "</em>' ";
  protected final String TEXT_1415 = " is set.";
  protected final String TEXT_1416 = NL + "\t * @see #unset";
  protected final String TEXT_1417 = "()";
  protected final String TEXT_1418 = NL + "\t * @see #";
  protected final String TEXT_1419 = "()";
  protected final String TEXT_1420 = NL + "\t * @see #set";
  protected final String TEXT_1421 = "(";
  protected final String TEXT_1422 = ")";
  protected final String TEXT_1423 = NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1424 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1425 = NL + "\tboolean isSet";
  protected final String TEXT_1426 = "();" + NL;
  protected final String TEXT_1427 = NL + "\tpublic boolean isSet";
  protected final String TEXT_1428 = "_";
  protected final String TEXT_1429 = "()" + NL + "\t{";
  protected final String TEXT_1430 = NL + "\t\treturn eDynamicIsSet(";
  protected final String TEXT_1431 = ", ";
  protected final String TEXT_1432 = ");";
  protected final String TEXT_1433 = NL + "\t\treturn eIsSet(";
  protected final String TEXT_1434 = ");";
  protected final String TEXT_1435 = NL + "\t\treturn ";
  protected final String TEXT_1436 = "__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);";
  protected final String TEXT_1437 = NL + "\t\t";
  protected final String TEXT_1438 = " ";
  protected final String TEXT_1439 = " = (";
  protected final String TEXT_1440 = ")eVirtualGet(";
  protected final String TEXT_1441 = ");";
  protected final String TEXT_1442 = NL + "\t\treturn ";
  protected final String TEXT_1443 = " != null && ((";
  protected final String TEXT_1444 = ".Unsettable";
  protected final String TEXT_1445 = ")";
  protected final String TEXT_1446 = ").isSet();";
  protected final String TEXT_1447 = NL + "\t\treturn eVirtualIsSet(";
  protected final String TEXT_1448 = ");";
  protected final String TEXT_1449 = NL + "\t\treturn (";
  protected final String TEXT_1450 = " & ";
  protected final String TEXT_1451 = "_ESETFLAG) != 0;";
  protected final String TEXT_1452 = NL + "\t\treturn ";
  protected final String TEXT_1453 = "ESet;";
  protected final String TEXT_1454 = NL + "\t\treturn !((";
  protected final String TEXT_1455 = ".Internal)((";
  protected final String TEXT_1456 = ".Internal.Wrapper)get";
  protected final String TEXT_1457 = "()).featureMap()).isEmpty(";
  protected final String TEXT_1458 = ");";
  protected final String TEXT_1459 = NL + "\t\treturn !((";
  protected final String TEXT_1460 = ".Internal)get";
  protected final String TEXT_1461 = "()).isEmpty(";
  protected final String TEXT_1462 = ");";
  protected final String TEXT_1463 = NL + "\t\t";
  protected final String TEXT_1464 = NL + "\t\t// TODO: implement this method to return whether the '";
  protected final String TEXT_1465 = "' ";
  protected final String TEXT_1466 = " is set" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_1467 = NL + "\t}" + NL;
  protected final String TEXT_1468 = NL + "\t/**" + NL + "\t * Evaluates the OCL defined choice constraint for the '<em><b>";
  protected final String TEXT_1469 = "</b></em>' ";
  protected final String TEXT_1470 = "." + NL + "     * The constraint is applied in the context of the source of the reference, and the target of the reference being of type ";
  protected final String TEXT_1471 = NL + "     * Inside the constraint, the target can be accessed as 'trg'. " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @OCL ";
  protected final String TEXT_1472 = NL + "\t * @templateTag GFI01" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic boolean eval";
  protected final String TEXT_1473 = "ChoiceConstraint(";
  protected final String TEXT_1474 = " trg){" + NL + "\t\t";
  protected final String TEXT_1475 = " eClass = ";
  protected final String TEXT_1476 = ";" + NL + "\t\tif (";
  protected final String TEXT_1477 = "ChoiceConstraintOCL == null) {" + NL + "\t\t\tOCL.Helper helper = OCL_ENV.createOCLHelper();" + NL + "" + NL + "\t\t\thelper.setContext(eClass);" + NL + "\t\t\t" + NL + "\t\t\t//the class of the feature  TODO: is this the right one" + NL + "\t\t\t";
  protected final String TEXT_1478 = " eReference = ";
  protected final String TEXT_1479 = ";" + NL + "\t\t\taddEnvironmentVariable(\"trg\", eReference.getEType());" + NL + "\t\t\t" + NL + "\t\t\tString choiceConstraint = ";
  protected final String TEXT_1480 = ".findChoiceConstraintAnnotationText(eReference, eClass());" + NL + "" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_1481 = "ChoiceConstraintOCL = helper" + NL + "\t\t\t\t\t\t.createQuery(choiceConstraint);" + NL + "\t\t\t} catch (";
  protected final String TEXT_1482 = " e) {" + NL + "\t\t\t\treturn false;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_1483 = ".handleQueryProblems(";
  protected final String TEXT_1484 = ".PLUGIN_ID, choiceConstraint, helper.getProblems(), eClass, \"";
  protected final String TEXT_1485 = "ChoiceConstraint\");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_1486 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_1487 = "ChoiceConstraintOCL);" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_1488 = ".enterContext(";
  protected final String TEXT_1489 = ".PLUGIN_ID, query, eClass, \"";
  protected final String TEXT_1490 = "ChoiceConstraint\");" + NL + "\t\tquery.getEvaluationEnvironment().clear();" + NL + "\t\tquery.getEvaluationEnvironment().add(\"trg\", trg);" + NL + "\t\treturn ((Boolean) query.evaluate(this)).booleanValue();" + NL + "\t\t} catch(";
  protected final String TEXT_1491 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_1492 = ".handleException(e);" + NL + "\t\t\treturn false;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_1493 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_1494 = NL + "\t/**" + NL + "\t * Evaluates the OCL defined choice construction for the '<em><b>";
  protected final String TEXT_1495 = "</b></em>' ";
  protected final String TEXT_1496 = "." + NL + "     * The constraint is applied in the context of the source of the reference, and the choice being of type ";
  protected final String TEXT_1497 = "<";
  protected final String TEXT_1498 = ">" + NL + "     * Inside the constraint, the choice can be accessed as 'choice'. " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @OCL ";
  protected final String TEXT_1499 = NL + "\t * @templateTag GFI02" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@SuppressWarnings(\"unchecked\")" + NL + "\tpublic ";
  protected final String TEXT_1500 = "<";
  protected final String TEXT_1501 = "> eval";
  protected final String TEXT_1502 = "ChoiceConstruction(";
  protected final String TEXT_1503 = "<";
  protected final String TEXT_1504 = "> choice){" + NL + "\t\t";
  protected final String TEXT_1505 = " eClass = ";
  protected final String TEXT_1506 = ";" + NL + "\t\tif (";
  protected final String TEXT_1507 = "ChoiceConstructionOCL == null) {" + NL + "\t\t\tOCL.Helper helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setContext(eClass);" + NL + "\t\t\t// create a variable declaring our global application context object" + NL + "\t\t\t";
  protected final String TEXT_1508 = " choiceVar = " + NL + "\t\t\t      ";
  protected final String TEXT_1509 = ".eINSTANCE.createVariable();" + NL + "\t\t\tchoiceVar.setName(\"choice\");" + NL + "\t\t\tchoiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());" + NL + "\t\t\t// add it to the global OCL environment" + NL + "\t\t\tOCL_ENV.getEnvironment().addElement(choiceVar.getName()," + NL + "\t\t\t\t\tchoiceVar, true);" + NL + "\t\t\t";
  protected final String TEXT_1510 = " eStructuralFeature = ";
  protected final String TEXT_1511 = ";" + NL + "" + NL + "\t\t\tString choiceConstruction = ";
  protected final String TEXT_1512 = ".findChoiceConstructionAnnotationText(eStructuralFeature, eClass());" + NL + "\t\t\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_1513 = "ChoiceConstructionOCL = helper" + NL + "\t\t\t\t\t\t.createQuery(choiceConstruction);" + NL + "\t\t\t} catch (";
  protected final String TEXT_1514 = " e) {" + NL + "\t\t\t\treturn choice;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_1515 = ".handleQueryProblems(";
  protected final String TEXT_1516 = ".PLUGIN_ID, choiceConstruction, helper.getProblems(), eClass, \"";
  protected final String TEXT_1517 = "ChoiceConstruction\");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_1518 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_1519 = "ChoiceConstructionOCL);" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_1520 = ".enterContext(";
  protected final String TEXT_1521 = ".PLUGIN_ID, query, eClass, \"";
  protected final String TEXT_1522 = "ChoiceConstruction\");" + NL + "\t\tquery.getEvaluationEnvironment().add(\"choice\", choice);" + NL + "\t\t";
  protected final String TEXT_1523 = "<";
  protected final String TEXT_1524 = "> result = new ";
  protected final String TEXT_1525 = "<";
  protected final String TEXT_1526 = ">((";
  protected final String TEXT_1527 = "<";
  protected final String TEXT_1528 = ">) query.evaluate(this));" + NL + "\t\t";
  protected final String TEXT_1529 = " result.remove(null); ";
  protected final String TEXT_1530 = NL + "\t\treturn result;" + NL + "\t\t} catch(";
  protected final String TEXT_1531 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_1532 = ".handleException(e);" + NL + "\t\t\treturn choice;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_1533 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_1534 = NL + "\t/**" + NL + "\t * Returns the OCL expression context for the '<em><b>";
  protected final String TEXT_1535 = "</b></em>' ";
  protected final String TEXT_1536 = "." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag GFI03" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_1537 = " get";
  protected final String TEXT_1538 = "OCLExpressionContext() {" + NL + "\t\t// TODO: implement this method (no OCL found!)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();" + NL + "\t}";
  protected final String TEXT_1539 = NL + "\t" + NL + "\t";
  protected final String TEXT_1540 = NL + "\t/**" + NL + "\t * The cached validation expression for the '{@link #";
  protected final String TEXT_1541 = "(";
  protected final String TEXT_1542 = ") <em>";
  protected final String TEXT_1543 = "</em>}' invariant operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_1544 = "(";
  protected final String TEXT_1545 = ")" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final ";
  protected final String TEXT_1546 = " ";
  protected final String TEXT_1547 = "__EEXPRESSION = \"";
  protected final String TEXT_1548 = "\";";
  protected final String TEXT_1549 = NL;
  protected final String TEXT_1550 = NL + "\t/**" + NL + "\t * The cached invocation delegate for the '{@link #";
  protected final String TEXT_1551 = "(";
  protected final String TEXT_1552 = ") <em>";
  protected final String TEXT_1553 = "</em>}' operation." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @see #";
  protected final String TEXT_1554 = "(";
  protected final String TEXT_1555 = ")" + NL + "\t * @generated" + NL + "\t * @ordered" + NL + "\t */" + NL + "\tprotected static final ";
  protected final String TEXT_1556 = ".Internal.InvocationDelegate ";
  protected final String TEXT_1557 = "__EINVOCATION_DELEGATE = ((";
  protected final String TEXT_1558 = ".Internal)";
  protected final String TEXT_1559 = ").getInvocationDelegate();" + NL;
  protected final String TEXT_1560 = NL + "\t/**";
  protected final String TEXT_1561 = NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->";
  protected final String TEXT_1562 = NL + "\t * <!-- begin-model-doc -->";
  protected final String TEXT_1563 = NL + "\t * ";
  protected final String TEXT_1564 = NL + "\t * @param ";
  protected final String TEXT_1565 = NL + "\t *   ";
  protected final String TEXT_1566 = NL + "\t * @param ";
  protected final String TEXT_1567 = " ";
  protected final String TEXT_1568 = NL + "\t * <!-- end-model-doc -->";
  protected final String TEXT_1569 = NL + "\t * @model ";
  protected final String TEXT_1570 = NL + "\t *        ";
  protected final String TEXT_1571 = NL + "\t * @model";
  protected final String TEXT_1572 = NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1573 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1574 = NL + "\t";
  protected final String TEXT_1575 = " ";
  protected final String TEXT_1576 = "(";
  protected final String TEXT_1577 = ")";
  protected final String TEXT_1578 = ";" + NL;
  protected final String TEXT_1579 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_1580 = NL + "\tpublic ";
  protected final String TEXT_1581 = " ";
  protected final String TEXT_1582 = "(";
  protected final String TEXT_1583 = ")";
  protected final String TEXT_1584 = NL + "\t{" + NL + "\t";
  protected final String TEXT_1585 = NL + "\t\t";
  protected final String TEXT_1586 = NL + "\t\treturn" + NL + "\t\t\t";
  protected final String TEXT_1587 = ".validate" + NL + "\t\t\t\t(";
  protected final String TEXT_1588 = "," + NL + "\t\t\t\t this," + NL + "\t\t\t\t ";
  protected final String TEXT_1589 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_1590 = "," + NL + "\t\t\t\t \"";
  protected final String TEXT_1591 = "\",";
  protected final String TEXT_1592 = NL + "\t\t\t\t ";
  protected final String TEXT_1593 = "," + NL + "\t\t\t\t ";
  protected final String TEXT_1594 = "__EEXPRESSION," + NL + "\t\t\t\t ";
  protected final String TEXT_1595 = ".ERROR," + NL + "\t\t\t\t ";
  protected final String TEXT_1596 = ".DIAGNOSTIC_SOURCE," + NL + "\t\t\t\t ";
  protected final String TEXT_1597 = ".";
  protected final String TEXT_1598 = ");";
  protected final String TEXT_1599 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// -> specify the condition that violates the invariant" + NL + "\t\t// -> verify the details of the diagnostic, including severity and message" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tif (false)" + NL + "\t\t{" + NL + "\t\t\tif (";
  protected final String TEXT_1600 = " != null)" + NL + "\t\t\t{" + NL + "\t\t\t\t";
  protected final String TEXT_1601 = ".add" + NL + "\t\t\t\t\t(new ";
  protected final String TEXT_1602 = NL + "\t\t\t\t\t\t(";
  protected final String TEXT_1603 = ".ERROR," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_1604 = ".DIAGNOSTIC_SOURCE," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_1605 = ".";
  protected final String TEXT_1606 = "," + NL + "\t\t\t\t\t\t ";
  protected final String TEXT_1607 = ".INSTANCE.getString(\"_UI_GenericInvariant_diagnostic\", new Object[] { \"";
  protected final String TEXT_1608 = "\", ";
  protected final String TEXT_1609 = ".getObjectLabel(this, ";
  protected final String TEXT_1610 = ") }),";
  protected final String TEXT_1611 = NL + "\t\t\t\t\t\t new Object [] { this }));" + NL + "\t\t\t}" + NL + "\t\t\treturn false;\t" + NL + "\t\t}" + NL + "\t\treturn true;";
  protected final String TEXT_1612 = NL + "\t\ttry" + NL + "\t\t{";
  protected final String TEXT_1613 = NL + "\t\t\t";
  protected final String TEXT_1614 = "__EINVOCATION_DELEGATE.dynamicInvoke(this, ";
  protected final String TEXT_1615 = "new ";
  protected final String TEXT_1616 = ".UnmodifiableEList<Object>(";
  protected final String TEXT_1617 = ", ";
  protected final String TEXT_1618 = ")";
  protected final String TEXT_1619 = "null";
  protected final String TEXT_1620 = ");";
  protected final String TEXT_1621 = NL + "\t\t\treturn ";
  protected final String TEXT_1622 = "(";
  protected final String TEXT_1623 = "(";
  protected final String TEXT_1624 = ")";
  protected final String TEXT_1625 = "__EINVOCATION_DELEGATE.dynamicInvoke(this, ";
  protected final String TEXT_1626 = "new ";
  protected final String TEXT_1627 = ".UnmodifiableEList<Object>(";
  protected final String TEXT_1628 = ", ";
  protected final String TEXT_1629 = ")";
  protected final String TEXT_1630 = "null";
  protected final String TEXT_1631 = ")";
  protected final String TEXT_1632 = ").";
  protected final String TEXT_1633 = "()";
  protected final String TEXT_1634 = ";";
  protected final String TEXT_1635 = NL + "\t\t}" + NL + "\t\tcatch (";
  protected final String TEXT_1636 = " ite)" + NL + "\t\t{" + NL + "\t\t\tthrow new ";
  protected final String TEXT_1637 = "(ite);" + NL + "\t\t}";
  protected final String TEXT_1638 = NL;
  protected final String TEXT_1639 = NL + "\t\t// TODO: implement this method (no OCL found!)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_1640 = NL + "\t   // Auto Generated XSemantics;" + NL + "" + NL + "\t\torg.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE.createXTransition();" + NL + "\t\t";
  protected final String TEXT_1641 = " triggerValue = trg;" + NL + "\t\t" + NL + "\t\t  ";
  protected final String TEXT_1642 = NL + "      ";
  protected final String TEXT_1643 = " src = ";
  protected final String TEXT_1644 = " ; ";
  protected final String TEXT_1645 = NL + "\t " + NL + "\t   XUpdate currentTrigger = transition.\t";
  protected final String TEXT_1646 = "addReferenceUpdate";
  protected final String TEXT_1647 = "addAttributeUpdate";
  protected final String TEXT_1648 = "   " + NL + "\t\t(this," + NL + "\t\t\t\t";
  protected final String TEXT_1649 = "()," + NL + "\t\t\t\torg.xocl.semantics.XUpdateMode.REDEFINE, null," + NL + "\t\t\t\ttriggerValue, null, null);" + NL + "\t\t\t\t" + NL + "\t\t\t\t";
  protected final String TEXT_1650 = NL + "\t currentTrigger.";
  protected final String TEXT_1651 = "addReferenceUpdate(";
  protected final String TEXT_1652 = "addAttributeUpdate(";
  protected final String TEXT_1653 = NL + "    this, ";
  protected final String TEXT_1654 = "(), org.xocl.semantics.XUpdateMode.REDEFINE, null,  " + NL + "    trg, null,null" + NL + "   );";
  protected final String TEXT_1655 = NL + NL + "for ( ";
  protected final String TEXT_1656 = " ";
  protected final String TEXT_1657 = " : ";
  protected final String TEXT_1658 = "(   ";
  protected final String TEXT_1659 = " src,";
  protected final String TEXT_1660 = "trg))" + NL + "   {";
  protected final String TEXT_1661 = NL + "     if (";
  protected final String TEXT_1662 = " ( ";
  protected final String TEXT_1663 = NL + "     src,";
  protected final String TEXT_1664 = NL + "     trg, ";
  protected final String TEXT_1665 = "))";
  protected final String TEXT_1666 = NL + "     ";
  protected final String TEXT_1667 = NL + "   currentTrigger.";
  protected final String TEXT_1668 = "addReferenceUpdate(";
  protected final String TEXT_1669 = "addAttributeUpdate(";
  protected final String TEXT_1670 = NL + "   ";
  protected final String TEXT_1671 = ", ";
  protected final String TEXT_1672 = "(), ";
  protected final String TEXT_1673 = ", ";
  protected final String TEXT_1674 = ",  ";
  protected final String TEXT_1675 = NL + "   ";
  protected final String TEXT_1676 = "(";
  protected final String TEXT_1677 = " src,";
  protected final String TEXT_1678 = "trg" + NL + "    , ";
  protected final String TEXT_1679 = ")" + NL + "   , null" + NL + "   ,null" + NL + "   , ";
  protected final String TEXT_1680 = " null";
  protected final String TEXT_1681 = " ";
  protected final String TEXT_1682 = "(trg, ";
  protected final String TEXT_1683 = " )";
  protected final String TEXT_1684 = NL + "   ,";
  protected final String TEXT_1685 = " null";
  protected final String TEXT_1686 = " ";
  protected final String TEXT_1687 = "(trg, ";
  protected final String TEXT_1688 = " )";
  protected final String TEXT_1689 = NL + "   ,";
  protected final String TEXT_1690 = " null";
  protected final String TEXT_1691 = " ";
  protected final String TEXT_1692 = "(trg, ";
  protected final String TEXT_1693 = " )";
  protected final String TEXT_1694 = NL + "   ,";
  protected final String TEXT_1695 = " null";
  protected final String TEXT_1696 = " ";
  protected final String TEXT_1697 = "(trg, ";
  protected final String TEXT_1698 = " )";
  protected final String TEXT_1699 = NL + "   );" + NL + "   ";
  protected final String TEXT_1700 = NL + "    " + NL + "   currentTrigger = currentTrigger.getContainingTransition().addAndMergeUpdate(";
  protected final String TEXT_1701 = ".";
  protected final String TEXT_1702 = "$Update(";
  protected final String TEXT_1703 = "( ";
  protected final String TEXT_1704 = NL + "    src, ";
  protected final String TEXT_1705 = " " + NL + "    trg, ";
  protected final String TEXT_1706 = ")));" + NL + "    ";
  protected final String TEXT_1707 = NL + "   }" + NL + "   " + NL + "   " + NL + "    ";
  protected final String TEXT_1708 = NL + "  " + NL + "  " + NL + "" + NL + "\t ";
  protected final String TEXT_1709 = " return null; ";
  protected final String TEXT_1710 = NL + "\t\treturn currentTrigger;\t";
  protected final String TEXT_1711 = NL + "\t\t" + NL + "\t   " + NL;
  protected final String TEXT_1712 = NL + NL + NL + "\t\t\t/**" + NL + "\t\t\t * @OCL ";
  protected final String TEXT_1713 = NL + "\t\t\t * @templateTag IGOT01" + NL + "\t\t\t */";
  protected final String TEXT_1714 = NL + "\t\t";
  protected final String TEXT_1715 = " eClass = (";
  protected final String TEXT_1716 = ");" + NL + "\t\t";
  protected final String TEXT_1717 = " ";
  protected final String TEXT_1718 = " = ";
  protected final String TEXT_1719 = ".getEOperations().get(";
  protected final String TEXT_1720 = ");" + NL + "\t\tif (";
  protected final String TEXT_1721 = " == null) {" + NL + "\t\t\t";
  protected final String TEXT_1722 = ".Helper helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setOperationContext(eClass, ";
  protected final String TEXT_1723 = ");" + NL + "" + NL + "\t\t\tString body = ";
  protected final String TEXT_1724 = ".findBodyAnnotationText(";
  protected final String TEXT_1725 = ", eClass());" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_1726 = " = helper.createQuery(body);" + NL + "\t\t\t} catch (";
  protected final String TEXT_1727 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_1728 = ".valueOf(\"0\").";
  protected final String TEXT_1729 = "()";
  protected final String TEXT_1730 = "null";
  protected final String TEXT_1731 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_1732 = ".handleQueryProblems(";
  protected final String TEXT_1733 = ".PLUGIN_ID, body, helper.getProblems(), ";
  protected final String TEXT_1734 = ", ";
  protected final String TEXT_1735 = ");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_1736 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_1737 = ");" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_1738 = ".enterContext(";
  protected final String TEXT_1739 = ".PLUGIN_ID, query, ";
  protected final String TEXT_1740 = ", ";
  protected final String TEXT_1741 = ");" + NL + "\t";
  protected final String TEXT_1742 = " " + NL + "\t\t";
  protected final String TEXT_1743 = "<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();" + NL + "\t\t";
  protected final String TEXT_1744 = NL + "\t\tevalEnv.add(\"";
  protected final String TEXT_1745 = "\", ";
  protected final String TEXT_1746 = ");";
  protected final String TEXT_1747 = NL + "\t  ";
  protected final String TEXT_1748 = NL + "\t\t";
  protected final String TEXT_1749 = " xoclEval = new ";
  protected final String TEXT_1750 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_1751 = "return (";
  protected final String TEXT_1752 = ") ";
  protected final String TEXT_1753 = "xoclEval.evaluateElement(";
  protected final String TEXT_1754 = ", query);" + NL + "\t";
  protected final String TEXT_1755 = NL + "\t\t";
  protected final String TEXT_1756 = "return ((";
  protected final String TEXT_1757 = ") ";
  protected final String TEXT_1758 = "query.evaluate(this)).";
  protected final String TEXT_1759 = "();" + NL + "\t";
  protected final String TEXT_1760 = NL + "\t\t";
  protected final String TEXT_1761 = " xoclEval = new ";
  protected final String TEXT_1762 = "(this, cachedValues);" + NL + "\t\t";
  protected final String TEXT_1763 = "return (";
  protected final String TEXT_1764 = ") ";
  protected final String TEXT_1765 = "xoclEval.evaluateElement(";
  protected final String TEXT_1766 = ", query);" + NL + "\t";
  protected final String TEXT_1767 = NL + "\t\t} catch(";
  protected final String TEXT_1768 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_1769 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_1770 = ".valueOf(\"0\").";
  protected final String TEXT_1771 = "()";
  protected final String TEXT_1772 = "null";
  protected final String TEXT_1773 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_1774 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_1775 = NL + NL;
  protected final String TEXT_1776 = NL + " " + NL + "\t}" + NL;
  protected final String TEXT_1777 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1778 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_1779 = NL + "\t@Override";
  protected final String TEXT_1780 = NL + "\tpublic ";
  protected final String TEXT_1781 = " eInverseAdd(";
  protected final String TEXT_1782 = " otherEnd, int featureID, ";
  protected final String TEXT_1783 = " msgs)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1784 = ")" + NL + "\t\t{";
  protected final String TEXT_1785 = NL + "\t\t\tcase ";
  protected final String TEXT_1786 = ":";
  protected final String TEXT_1787 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_1788 = "(";
  protected final String TEXT_1789 = ".InternalMapView";
  protected final String TEXT_1790 = ")";
  protected final String TEXT_1791 = "()).eMap()).basicAdd(otherEnd, msgs);";
  protected final String TEXT_1792 = NL + "\t\t\t\treturn (";
  protected final String TEXT_1793 = "()).basicAdd(otherEnd, msgs);";
  protected final String TEXT_1794 = NL + "\t\t\t\tif (eInternalContainer() != null)" + NL + "\t\t\t\t\tmsgs = eBasicRemoveFromContainer(msgs);";
  protected final String TEXT_1795 = NL + "\t\t\t\treturn basicSet";
  protected final String TEXT_1796 = "((";
  protected final String TEXT_1797 = ")otherEnd, msgs);";
  protected final String TEXT_1798 = NL + "\t\t\t\treturn eBasicSetContainer(otherEnd, ";
  protected final String TEXT_1799 = ", msgs);";
  protected final String TEXT_1800 = NL + "\t\t\t\t";
  protected final String TEXT_1801 = " ";
  protected final String TEXT_1802 = " = (";
  protected final String TEXT_1803 = ")eVirtualGet(";
  protected final String TEXT_1804 = ");";
  protected final String TEXT_1805 = NL + "\t\t\t\t";
  protected final String TEXT_1806 = " ";
  protected final String TEXT_1807 = " = ";
  protected final String TEXT_1808 = "basicGet";
  protected final String TEXT_1809 = "();";
  protected final String TEXT_1810 = NL + "\t\t\t\tif (";
  protected final String TEXT_1811 = " != null)";
  protected final String TEXT_1812 = NL + "\t\t\t\t\tmsgs = ((";
  protected final String TEXT_1813 = ")";
  protected final String TEXT_1814 = ").eInverseRemove(this, EOPPOSITE_FEATURE_BASE - ";
  protected final String TEXT_1815 = ", null, msgs);";
  protected final String TEXT_1816 = NL + "\t\t\t\t\tmsgs = ((";
  protected final String TEXT_1817 = ")";
  protected final String TEXT_1818 = ").eInverseRemove(this, ";
  protected final String TEXT_1819 = ", ";
  protected final String TEXT_1820 = ".class, msgs);";
  protected final String TEXT_1821 = NL + "\t\t\t\treturn basicSet";
  protected final String TEXT_1822 = "((";
  protected final String TEXT_1823 = ")otherEnd, msgs);";
  protected final String TEXT_1824 = NL + "\t\t}";
  protected final String TEXT_1825 = NL + "\t\treturn super.eInverseAdd(otherEnd, featureID, msgs);";
  protected final String TEXT_1826 = NL + "\t\treturn eDynamicInverseAdd(otherEnd, featureID, msgs);";
  protected final String TEXT_1827 = NL + "\t}" + NL;
  protected final String TEXT_1828 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1829 = NL + "\t@Override";
  protected final String TEXT_1830 = NL + "\tpublic ";
  protected final String TEXT_1831 = " eInverseRemove(";
  protected final String TEXT_1832 = " otherEnd, int featureID, ";
  protected final String TEXT_1833 = " msgs)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1834 = ")" + NL + "\t\t{";
  protected final String TEXT_1835 = NL + "\t\t\tcase ";
  protected final String TEXT_1836 = ":";
  protected final String TEXT_1837 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_1838 = ")((";
  protected final String TEXT_1839 = ".InternalMapView";
  protected final String TEXT_1840 = ")";
  protected final String TEXT_1841 = "()).eMap()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_1842 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_1843 = ")((";
  protected final String TEXT_1844 = ".Internal.Wrapper)";
  protected final String TEXT_1845 = "()).featureMap()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_1846 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_1847 = ")";
  protected final String TEXT_1848 = "()).basicRemove(otherEnd, msgs);";
  protected final String TEXT_1849 = NL + "\t\t\t\treturn eBasicSetContainer(null, ";
  protected final String TEXT_1850 = ", msgs);";
  protected final String TEXT_1851 = NL + "\t\t\t\treturn basicUnset";
  protected final String TEXT_1852 = "(msgs);";
  protected final String TEXT_1853 = NL + "\t\t\t\treturn basicSet";
  protected final String TEXT_1854 = "(null, msgs);";
  protected final String TEXT_1855 = NL + "\t\t}";
  protected final String TEXT_1856 = NL + "\t\treturn super.eInverseRemove(otherEnd, featureID, msgs);";
  protected final String TEXT_1857 = NL + "\t\treturn eDynamicInverseRemove(otherEnd, featureID, msgs);";
  protected final String TEXT_1858 = NL + "\t}" + NL;
  protected final String TEXT_1859 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1860 = NL + "\t@Override";
  protected final String TEXT_1861 = NL + "\tpublic ";
  protected final String TEXT_1862 = " eBasicRemoveFromContainerFeature(";
  protected final String TEXT_1863 = " msgs)" + NL + "\t{" + NL + "\t\tswitch (eContainerFeatureID()";
  protected final String TEXT_1864 = ")" + NL + "\t\t{";
  protected final String TEXT_1865 = NL + "\t\t\tcase ";
  protected final String TEXT_1866 = ":" + NL + "\t\t\t\treturn eInternalContainer().eInverseRemove(this, ";
  protected final String TEXT_1867 = ", ";
  protected final String TEXT_1868 = ".class, msgs);";
  protected final String TEXT_1869 = NL + "\t\t}";
  protected final String TEXT_1870 = NL + "\t\treturn super.eBasicRemoveFromContainerFeature(msgs);";
  protected final String TEXT_1871 = NL + "\t\treturn eDynamicBasicRemoveFromContainer(msgs);";
  protected final String TEXT_1872 = NL + "\t}" + NL;
  protected final String TEXT_1873 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1874 = NL + "\t@Override";
  protected final String TEXT_1875 = NL + "\tpublic Object eGet(int featureID, boolean resolve, boolean coreType)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1876 = ")" + NL + "\t\t{";
  protected final String TEXT_1877 = NL + "\t\t\tcase ";
  protected final String TEXT_1878 = ":";
  protected final String TEXT_1879 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1880 = "();";
  protected final String TEXT_1881 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1882 = "() ? Boolean.TRUE : Boolean.FALSE;";
  protected final String TEXT_1883 = NL + "\t\t\t\treturn new ";
  protected final String TEXT_1884 = "(";
  protected final String TEXT_1885 = "());";
  protected final String TEXT_1886 = NL + "\t\t\t\tif (resolve) return ";
  protected final String TEXT_1887 = "();" + NL + "\t\t\t\treturn basicGet";
  protected final String TEXT_1888 = "();";
  protected final String TEXT_1889 = NL + "\t\t\t\tif (coreType) return ((";
  protected final String TEXT_1890 = ".InternalMapView";
  protected final String TEXT_1891 = ")";
  protected final String TEXT_1892 = "()).eMap();" + NL + "\t\t\t\telse return ";
  protected final String TEXT_1893 = "();";
  protected final String TEXT_1894 = NL + "\t\t\t\tif (coreType) return ";
  protected final String TEXT_1895 = "();" + NL + "\t\t\t\telse return ";
  protected final String TEXT_1896 = "().map();";
  protected final String TEXT_1897 = NL + "\t\t\t\tif (coreType) return ((";
  protected final String TEXT_1898 = ".Internal.Wrapper)";
  protected final String TEXT_1899 = "()).featureMap();" + NL + "\t\t\t\treturn ";
  protected final String TEXT_1900 = "();";
  protected final String TEXT_1901 = NL + "\t\t\t\tif (coreType) return ";
  protected final String TEXT_1902 = "();" + NL + "\t\t\t\treturn ((";
  protected final String TEXT_1903 = ".Internal)";
  protected final String TEXT_1904 = "()).getWrapper();";
  protected final String TEXT_1905 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1906 = "();";
  protected final String TEXT_1907 = NL + "\t\t}";
  protected final String TEXT_1908 = NL + "\t\treturn super.eGet(featureID, resolve, coreType);";
  protected final String TEXT_1909 = NL + "\t\treturn eDynamicGet(featureID, resolve, coreType);";
  protected final String TEXT_1910 = NL + "\t}" + NL;
  protected final String TEXT_1911 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1912 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_1913 = NL + "\t@Override";
  protected final String TEXT_1914 = NL + "\tpublic void eSet(int featureID, Object newValue)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1915 = ")" + NL + "\t\t{";
  protected final String TEXT_1916 = NL + "\t\t\tcase ";
  protected final String TEXT_1917 = ":";
  protected final String TEXT_1918 = NL + "\t\t\t\t((";
  protected final String TEXT_1919 = ".Internal)((";
  protected final String TEXT_1920 = ".Internal.Wrapper)";
  protected final String TEXT_1921 = "()).featureMap()).set(newValue);";
  protected final String TEXT_1922 = NL + "\t\t\t\t((";
  protected final String TEXT_1923 = ".Internal)";
  protected final String TEXT_1924 = "()).set(newValue);";
  protected final String TEXT_1925 = NL + "\t\t\t\t((";
  protected final String TEXT_1926 = ".Setting)((";
  protected final String TEXT_1927 = ".InternalMapView";
  protected final String TEXT_1928 = ")";
  protected final String TEXT_1929 = "()).eMap()).set(newValue);";
  protected final String TEXT_1930 = NL + "\t\t\t\t((";
  protected final String TEXT_1931 = ".Setting)";
  protected final String TEXT_1932 = "()).set(newValue);";
  protected final String TEXT_1933 = NL + "\t\t\t\t";
  protected final String TEXT_1934 = "().clear();" + NL + "\t\t\t\t";
  protected final String TEXT_1935 = "().addAll((";
  protected final String TEXT_1936 = "<? extends ";
  protected final String TEXT_1937 = ">";
  protected final String TEXT_1938 = ")newValue);";
  protected final String TEXT_1939 = NL + "\t\t\t\tset";
  protected final String TEXT_1940 = "(((";
  protected final String TEXT_1941 = ")newValue).";
  protected final String TEXT_1942 = "());";
  protected final String TEXT_1943 = NL + "\t\t\t\tset";
  protected final String TEXT_1944 = "(";
  protected final String TEXT_1945 = "(";
  protected final String TEXT_1946 = ")";
  protected final String TEXT_1947 = "newValue);";
  protected final String TEXT_1948 = NL + "\t\t\t\treturn;";
  protected final String TEXT_1949 = NL + "\t\t}";
  protected final String TEXT_1950 = NL + "\t\tsuper.eSet(featureID, newValue);";
  protected final String TEXT_1951 = NL + "\t\teDynamicSet(featureID, newValue);";
  protected final String TEXT_1952 = NL + "\t}" + NL;
  protected final String TEXT_1953 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1954 = NL + "\t@Override";
  protected final String TEXT_1955 = NL + "\tpublic void eUnset(int featureID)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1956 = ")" + NL + "\t\t{";
  protected final String TEXT_1957 = NL + "\t\t\tcase ";
  protected final String TEXT_1958 = ":";
  protected final String TEXT_1959 = NL + "\t\t\t\t((";
  protected final String TEXT_1960 = ".Internal.Wrapper)";
  protected final String TEXT_1961 = "()).featureMap().clear();";
  protected final String TEXT_1962 = NL + "\t\t\t\t";
  protected final String TEXT_1963 = "().clear();";
  protected final String TEXT_1964 = NL + "\t\t\t\tunset";
  protected final String TEXT_1965 = "();";
  protected final String TEXT_1966 = NL + "\t\t\t\tset";
  protected final String TEXT_1967 = "((";
  protected final String TEXT_1968 = ")null);";
  protected final String TEXT_1969 = NL + "\t\t\t\tset";
  protected final String TEXT_1970 = "(";
  protected final String TEXT_1971 = ");";
  protected final String TEXT_1972 = NL + "\t\t\t\treturn;";
  protected final String TEXT_1973 = NL + "\t\t}";
  protected final String TEXT_1974 = NL + "\t\tsuper.eUnset(featureID);";
  protected final String TEXT_1975 = NL + "\t\teDynamicUnset(featureID);";
  protected final String TEXT_1976 = NL + "\t}" + NL;
  protected final String TEXT_1977 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_1978 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_1979 = NL + "\t@Override";
  protected final String TEXT_1980 = NL + "\tpublic boolean eIsSet(int featureID)" + NL + "\t{" + NL + "\t\tswitch (featureID";
  protected final String TEXT_1981 = ")" + NL + "\t\t{";
  protected final String TEXT_1982 = NL + "\t\t\tcase ";
  protected final String TEXT_1983 = ":";
  protected final String TEXT_1984 = NL + "\t\t\t\treturn isSet";
  protected final String TEXT_1985 = "();";
  protected final String TEXT_1986 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1987 = "__ESETTING_DELEGATE.dynamicIsSet(this, null, 0);";
  protected final String TEXT_1988 = NL + "\t\t\t\treturn !((";
  protected final String TEXT_1989 = ".Internal.Wrapper)";
  protected final String TEXT_1990 = "()).featureMap().isEmpty();";
  protected final String TEXT_1991 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1992 = " != null && !";
  protected final String TEXT_1993 = ".featureMap().isEmpty();";
  protected final String TEXT_1994 = NL + "\t\t\t\treturn ";
  protected final String TEXT_1995 = " != null && !";
  protected final String TEXT_1996 = ".isEmpty();";
  protected final String TEXT_1997 = NL + "\t\t\t\t";
  protected final String TEXT_1998 = " ";
  protected final String TEXT_1999 = " = (";
  protected final String TEXT_2000 = ")eVirtualGet(";
  protected final String TEXT_2001 = ");" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2002 = " != null && !";
  protected final String TEXT_2003 = ".isEmpty();";
  protected final String TEXT_2004 = NL + "\t\t\t\treturn !";
  protected final String TEXT_2005 = "().isEmpty();";
  protected final String TEXT_2006 = NL + "\t\t\t\treturn isSet";
  protected final String TEXT_2007 = "();";
  protected final String TEXT_2008 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2009 = " != null;";
  protected final String TEXT_2010 = NL + "\t\t\t\treturn eVirtualGet(";
  protected final String TEXT_2011 = ") != null;";
  protected final String TEXT_2012 = NL + "\t\t\t\treturn basicGet";
  protected final String TEXT_2013 = "() != null;";
  protected final String TEXT_2014 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2015 = " != null;";
  protected final String TEXT_2016 = NL + "\t\t\t\treturn eVirtualGet(";
  protected final String TEXT_2017 = ") != null;";
  protected final String TEXT_2018 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2019 = "() != null;";
  protected final String TEXT_2020 = NL + "\t\t\t\treturn ((";
  protected final String TEXT_2021 = " & ";
  protected final String TEXT_2022 = "_EFLAG) != 0) != ";
  protected final String TEXT_2023 = ";";
  protected final String TEXT_2024 = NL + "\t\t\t\treturn (";
  protected final String TEXT_2025 = " & ";
  protected final String TEXT_2026 = "_EFLAG) != ";
  protected final String TEXT_2027 = "_EFLAG_DEFAULT;";
  protected final String TEXT_2028 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2029 = " != ";
  protected final String TEXT_2030 = ";";
  protected final String TEXT_2031 = NL + "\t\t\t\treturn eVirtualGet(";
  protected final String TEXT_2032 = ", ";
  protected final String TEXT_2033 = ") != ";
  protected final String TEXT_2034 = ";";
  protected final String TEXT_2035 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2036 = "() != ";
  protected final String TEXT_2037 = ";";
  protected final String TEXT_2038 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2039 = " == null ? ";
  protected final String TEXT_2040 = " != null : !";
  protected final String TEXT_2041 = ".equals(";
  protected final String TEXT_2042 = ");";
  protected final String TEXT_2043 = NL + "\t\t\t\t";
  protected final String TEXT_2044 = " ";
  protected final String TEXT_2045 = " = (";
  protected final String TEXT_2046 = ")eVirtualGet(";
  protected final String TEXT_2047 = ", ";
  protected final String TEXT_2048 = ");" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2049 = " == null ? ";
  protected final String TEXT_2050 = " != null : !";
  protected final String TEXT_2051 = ".equals(";
  protected final String TEXT_2052 = ");";
  protected final String TEXT_2053 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2054 = " == null ? ";
  protected final String TEXT_2055 = "() != null : !";
  protected final String TEXT_2056 = ".equals(";
  protected final String TEXT_2057 = "());";
  protected final String TEXT_2058 = NL + "\t\t}";
  protected final String TEXT_2059 = NL + "\t\treturn super.eIsSet(featureID);";
  protected final String TEXT_2060 = NL + "\t\treturn eDynamicIsSet(featureID);";
  protected final String TEXT_2061 = NL + "\t}" + NL;
  protected final String TEXT_2062 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2063 = NL + "\t@Override";
  protected final String TEXT_2064 = NL + "\tpublic int eBaseStructuralFeatureID(int derivedFeatureID, Class";
  protected final String TEXT_2065 = " baseClass)" + NL + "\t{";
  protected final String TEXT_2066 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2067 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (derivedFeatureID";
  protected final String TEXT_2068 = ")" + NL + "\t\t\t{";
  protected final String TEXT_2069 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2070 = ": return ";
  protected final String TEXT_2071 = ";";
  protected final String TEXT_2072 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2073 = NL + "\t\treturn super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);" + NL + "\t}";
  protected final String TEXT_2074 = NL + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2075 = NL + "\t@Override";
  protected final String TEXT_2076 = NL + "\tpublic int eDerivedStructuralFeatureID(int baseFeatureID, Class";
  protected final String TEXT_2077 = " baseClass)" + NL + "\t{";
  protected final String TEXT_2078 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2079 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseFeatureID)" + NL + "\t\t\t{";
  protected final String TEXT_2080 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2081 = ": return ";
  protected final String TEXT_2082 = ";";
  protected final String TEXT_2083 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2084 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2085 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseFeatureID";
  protected final String TEXT_2086 = ")" + NL + "\t\t\t{";
  protected final String TEXT_2087 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2088 = ": return ";
  protected final String TEXT_2089 = ";";
  protected final String TEXT_2090 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2091 = NL + "\t\treturn super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);" + NL + "\t}" + NL;
  protected final String TEXT_2092 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2093 = NL + "\t@Override";
  protected final String TEXT_2094 = NL + "\tpublic int eDerivedOperationID(int baseOperationID, Class";
  protected final String TEXT_2095 = " baseClass)" + NL + "\t{";
  protected final String TEXT_2096 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2097 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseOperationID)" + NL + "\t\t\t{";
  protected final String TEXT_2098 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2099 = ": return ";
  protected final String TEXT_2100 = ";";
  protected final String TEXT_2101 = NL + "\t\t\t\tdefault: return super.eDerivedOperationID(baseOperationID, baseClass);" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2102 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2103 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseOperationID)" + NL + "\t\t\t{";
  protected final String TEXT_2104 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2105 = ": return ";
  protected final String TEXT_2106 = ";";
  protected final String TEXT_2107 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2108 = NL + "\t\tif (baseClass == ";
  protected final String TEXT_2109 = ".class)" + NL + "\t\t{" + NL + "\t\t\tswitch (baseOperationID";
  protected final String TEXT_2110 = ")" + NL + "\t\t\t{";
  protected final String TEXT_2111 = NL + "\t\t\t\tcase ";
  protected final String TEXT_2112 = ": return ";
  protected final String TEXT_2113 = ";";
  protected final String TEXT_2114 = NL + "\t\t\t\tdefault: return -1;" + NL + "\t\t\t}" + NL + "\t\t}";
  protected final String TEXT_2115 = NL + "\t\treturn super.eDerivedOperationID(baseOperationID, baseClass);" + NL + "\t}" + NL;
  protected final String TEXT_2116 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2117 = NL + "\t@Override";
  protected final String TEXT_2118 = NL + "\tprotected Object[] eVirtualValues()" + NL + "\t{" + NL + "\t\treturn ";
  protected final String TEXT_2119 = ";" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2120 = NL + "\t@Override";
  protected final String TEXT_2121 = NL + "\tprotected void eSetVirtualValues(Object[] newValues)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_2122 = " = newValues;" + NL + "\t}" + NL;
  protected final String TEXT_2123 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2124 = NL + "\t@Override";
  protected final String TEXT_2125 = NL + "\tprotected int eVirtualIndexBits(int offset)" + NL + "\t{" + NL + "\t\tswitch (offset)" + NL + "\t\t{";
  protected final String TEXT_2126 = NL + "\t\t\tcase ";
  protected final String TEXT_2127 = " :" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2128 = ";";
  protected final String TEXT_2129 = NL + "\t\t\tdefault :" + NL + "\t\t\t\tthrow new IndexOutOfBoundsException();" + NL + "\t\t}" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2130 = NL + "\t@Override";
  protected final String TEXT_2131 = NL + "\tprotected void eSetVirtualIndexBits(int offset, int newIndexBits)" + NL + "\t{" + NL + "\t\tswitch (offset)" + NL + "\t\t{";
  protected final String TEXT_2132 = NL + "\t\t\tcase ";
  protected final String TEXT_2133 = " :" + NL + "\t\t\t\t";
  protected final String TEXT_2134 = " = newIndexBits;" + NL + "\t\t\t\tbreak;";
  protected final String TEXT_2135 = NL + "\t\t\tdefault :" + NL + "\t\t\t\tthrow new IndexOutOfBoundsException();" + NL + "\t\t}" + NL + "\t}" + NL;
  protected final String TEXT_2136 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2137 = NL + "\t@Override";
  protected final String TEXT_2138 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_2139 = NL + "\tpublic Object eInvoke(int operationID, ";
  protected final String TEXT_2140 = " arguments) throws ";
  protected final String TEXT_2141 = NL + "\t{" + NL + "\t\tswitch (operationID";
  protected final String TEXT_2142 = ")" + NL + "\t\t{";
  protected final String TEXT_2143 = NL + "\t\t\tcase ";
  protected final String TEXT_2144 = ":";
  protected final String TEXT_2145 = NL + "\t\t\t\t";
  protected final String TEXT_2146 = "(";
  protected final String TEXT_2147 = "(";
  protected final String TEXT_2148 = "(";
  protected final String TEXT_2149 = ")";
  protected final String TEXT_2150 = "arguments.get(";
  protected final String TEXT_2151 = ")";
  protected final String TEXT_2152 = ").";
  protected final String TEXT_2153 = "()";
  protected final String TEXT_2154 = ", ";
  protected final String TEXT_2155 = ");" + NL + "\t\t\t\treturn null;";
  protected final String TEXT_2156 = NL + "\t\t\t\treturn ";
  protected final String TEXT_2157 = "new ";
  protected final String TEXT_2158 = "(";
  protected final String TEXT_2159 = "(";
  protected final String TEXT_2160 = "(";
  protected final String TEXT_2161 = "(";
  protected final String TEXT_2162 = ")";
  protected final String TEXT_2163 = "arguments.get(";
  protected final String TEXT_2164 = ")";
  protected final String TEXT_2165 = ").";
  protected final String TEXT_2166 = "()";
  protected final String TEXT_2167 = ", ";
  protected final String TEXT_2168 = ")";
  protected final String TEXT_2169 = ")";
  protected final String TEXT_2170 = ";";
  protected final String TEXT_2171 = NL + "\t\t}";
  protected final String TEXT_2172 = NL + "\t\treturn super.eInvoke(operationID, arguments);";
  protected final String TEXT_2173 = NL + "\t\treturn eDynamicInvoke(operationID, arguments);";
  protected final String TEXT_2174 = NL + "\t}" + NL;
  protected final String TEXT_2175 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2176 = NL + "\t@Override";
  protected final String TEXT_2177 = NL + "\tpublic String toString()" + NL + "\t{" + NL + "\t\tif (eIsProxy()) return super.toString();" + NL + "" + NL + "\t\tStringBuffer result = new StringBuffer(super.toString());";
  protected final String TEXT_2178 = NL + "\t\tresult.append(\" (";
  protected final String TEXT_2179 = ": \");";
  protected final String TEXT_2180 = NL + "\t\tresult.append(\", ";
  protected final String TEXT_2181 = ": \");";
  protected final String TEXT_2182 = NL + "\t\tif (eVirtualIsSet(";
  protected final String TEXT_2183 = ")) result.append(eVirtualGet(";
  protected final String TEXT_2184 = ")); else result.append(\"<unset>\");";
  protected final String TEXT_2185 = NL + "\t\tif (";
  protected final String TEXT_2186 = "(";
  protected final String TEXT_2187 = " & ";
  protected final String TEXT_2188 = "_ESETFLAG) != 0";
  protected final String TEXT_2189 = "ESet";
  protected final String TEXT_2190 = ") result.append((";
  protected final String TEXT_2191 = " & ";
  protected final String TEXT_2192 = "_EFLAG) != 0); else result.append(\"<unset>\");";
  protected final String TEXT_2193 = NL + "\t\tif (";
  protected final String TEXT_2194 = "(";
  protected final String TEXT_2195 = " & ";
  protected final String TEXT_2196 = "_ESETFLAG) != 0";
  protected final String TEXT_2197 = "ESet";
  protected final String TEXT_2198 = ") result.append(";
  protected final String TEXT_2199 = "_EFLAG_VALUES[(";
  protected final String TEXT_2200 = " & ";
  protected final String TEXT_2201 = "_EFLAG) >>> ";
  protected final String TEXT_2202 = "_EFLAG_OFFSET]); else result.append(\"<unset>\");";
  protected final String TEXT_2203 = NL + "\t\tif (";
  protected final String TEXT_2204 = "(";
  protected final String TEXT_2205 = " & ";
  protected final String TEXT_2206 = "_ESETFLAG) != 0";
  protected final String TEXT_2207 = "ESet";
  protected final String TEXT_2208 = ") result.append(";
  protected final String TEXT_2209 = "); else result.append(\"<unset>\");";
  protected final String TEXT_2210 = NL + "\t\tresult.append(eVirtualGet(";
  protected final String TEXT_2211 = ", ";
  protected final String TEXT_2212 = "));";
  protected final String TEXT_2213 = NL + "\t\tresult.append((";
  protected final String TEXT_2214 = " & ";
  protected final String TEXT_2215 = "_EFLAG) != 0);";
  protected final String TEXT_2216 = NL + "\t\tresult.append(";
  protected final String TEXT_2217 = "_EFLAG_VALUES[(";
  protected final String TEXT_2218 = " & ";
  protected final String TEXT_2219 = "_EFLAG) >>> ";
  protected final String TEXT_2220 = "_EFLAG_OFFSET]);";
  protected final String TEXT_2221 = NL + "\t\tresult.append(";
  protected final String TEXT_2222 = ");";
  protected final String TEXT_2223 = NL + "\t\tresult.append(')');" + NL + "\t\treturn result.toString();" + NL + "\t}" + NL;
  protected final String TEXT_2224 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2225 = NL + "\t@";
  protected final String TEXT_2226 = NL + "\tprotected int hash = -1;" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic int getHash()" + NL + "\t{" + NL + "\t\tif (hash == -1)" + NL + "\t\t{" + NL + "\t\t\t";
  protected final String TEXT_2227 = " theKey = getKey();" + NL + "\t\t\thash = (theKey == null ? 0 : theKey.hashCode());" + NL + "\t\t}" + NL + "\t\treturn hash;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void setHash(int hash)" + NL + "\t{" + NL + "\t\tthis.hash = hash;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_2228 = " getKey()" + NL + "\t{";
  protected final String TEXT_2229 = NL + "\t\treturn new ";
  protected final String TEXT_2230 = "(getTypedKey());";
  protected final String TEXT_2231 = NL + "\t\treturn getTypedKey();";
  protected final String TEXT_2232 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void setKey(";
  protected final String TEXT_2233 = " key)" + NL + "\t{";
  protected final String TEXT_2234 = NL + "\t\tgetTypedKey().addAll(";
  protected final String TEXT_2235 = "(";
  protected final String TEXT_2236 = ")";
  protected final String TEXT_2237 = "key);";
  protected final String TEXT_2238 = NL + "\t\tsetTypedKey(key);";
  protected final String TEXT_2239 = NL + "\t\tsetTypedKey(((";
  protected final String TEXT_2240 = ")key).";
  protected final String TEXT_2241 = "());";
  protected final String TEXT_2242 = NL + "\t\tsetTypedKey((";
  protected final String TEXT_2243 = ")key);";
  protected final String TEXT_2244 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_2245 = " getValue()" + NL + "\t{";
  protected final String TEXT_2246 = NL + "\t\treturn new ";
  protected final String TEXT_2247 = "(getTypedValue());";
  protected final String TEXT_2248 = NL + "\t\treturn getTypedValue();";
  protected final String TEXT_2249 = NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_2250 = " setValue(";
  protected final String TEXT_2251 = " value)" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_2252 = " oldValue = getValue();";
  protected final String TEXT_2253 = NL + "\t\tgetTypedValue().clear();" + NL + "\t\tgetTypedValue().addAll(";
  protected final String TEXT_2254 = "(";
  protected final String TEXT_2255 = ")";
  protected final String TEXT_2256 = "value);";
  protected final String TEXT_2257 = NL + "\t\tsetTypedValue(value);";
  protected final String TEXT_2258 = NL + "\t\tsetTypedValue(((";
  protected final String TEXT_2259 = ")value).";
  protected final String TEXT_2260 = "());";
  protected final String TEXT_2261 = NL + "\t\tsetTypedValue((";
  protected final String TEXT_2262 = ")value);";
  protected final String TEXT_2263 = NL + "\t\treturn oldValue;" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2264 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_2265 = NL + "\tpublic ";
  protected final String TEXT_2266 = " getEMap()" + NL + "\t{" + NL + "\t\t";
  protected final String TEXT_2267 = " container = eContainer();" + NL + "\t\treturn container == null ? null : (";
  protected final String TEXT_2268 = ")container.eGet(eContainmentFeature());" + NL + "\t}" + NL;
  protected final String TEXT_2269 = NL + "\t/**" + NL + "\t * Evaluates the label calculated by OCL 'label' annotation. <!--" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @OCL ";
  protected final String TEXT_2270 = NL + "\t * @templateTag INS01" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic String evalOclLabel() {" + NL + "\t\t";
  protected final String TEXT_2271 = " eClass = ";
  protected final String TEXT_2272 = ";" + NL + "\t\tif (labelOCL == null) {" + NL + "\t\t\t";
  protected final String TEXT_2273 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setContext(eClass);" + NL + "\t\t\t";
  protected final String TEXT_2274 = " ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);" + NL + "\t\t\tString label = (String) ocl.getDetails().get(\"label\");" + NL + "" + NL + "\t\t\ttry {" + NL + "\t\t\t\tlabelOCL = helper.createQuery(label);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2275 = " e) {" + NL + "\t\t\t\treturn null;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2276 = ".handleQueryProblems(";
  protected final String TEXT_2277 = ".PLUGIN_ID, label, helper.getProblems(), eClass, \"label\");" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_2278 = " query = OCL_ENV.createQuery(labelOCL);" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_2279 = ".enterContext(";
  protected final String TEXT_2280 = ".PLUGIN_ID, query, eClass, \"label\");" + NL + "\t\treturn ";
  protected final String TEXT_2281 = ".format(query.evaluate(this));" + NL + "\t\t} catch(";
  protected final String TEXT_2282 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2283 = ".handleException(e);" + NL + "\t\t\treturn null;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2284 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_2285 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @overrideOCL ";
  protected final String TEXT_2286 = " ";
  protected final String TEXT_2287 = NL + "\t * @templateTag INS02" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2288 = NL + "\t@Override";
  protected final String TEXT_2289 = NL + "\tpublic ";
  protected final String TEXT_2290 = " basicGet";
  protected final String TEXT_2291 = "()" + NL + "\t{";
  protected final String TEXT_2292 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_2293 = NL + "\t\t    ";
  protected final String TEXT_2294 = " eClass = (";
  protected final String TEXT_2295 = ");" + NL + "\t\t    ";
  protected final String TEXT_2296 = " eOverrideFeature = ";
  protected final String TEXT_2297 = ";" + NL + "\t    ";
  protected final String TEXT_2298 = NL + "\t\tif (";
  protected final String TEXT_2299 = " == null) {" + NL + "\t\t\t";
  protected final String TEXT_2300 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setAttributeContext(eClass, eOverrideFeature);" + NL + "\t\t\t" + NL + "\t\t\tString derive = ";
  protected final String TEXT_2301 = ".findDeriveAnnotationText(eOverrideFeature, eClass());" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_2302 = " = helper.createQuery(derive);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2303 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2304 = ".valueOf(\"0\").";
  protected final String TEXT_2305 = "()";
  protected final String TEXT_2306 = "null";
  protected final String TEXT_2307 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2308 = ".handleQueryProblems(";
  protected final String TEXT_2309 = ".PLUGIN_ID, derive, helper.getProblems(), eClass, eOverrideFeature);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_2310 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_2311 = ");" + NL + "\t\ttry {" + NL + "\t\t\t";
  protected final String TEXT_2312 = ".enterContext(";
  protected final String TEXT_2313 = ".PLUGIN_ID, query, eClass, eOverrideFeature);" + NL + "\t";
  protected final String TEXT_2314 = NL + "\t\t";
  protected final String TEXT_2315 = " xoclEval = new ";
  protected final String TEXT_2316 = "(this, cachedValues);" + NL + "\t\treturn (";
  protected final String TEXT_2317 = ") xoclEval.evaluateElement(eOverrideFeature, query);" + NL + "\t";
  protected final String TEXT_2318 = NL + "\t\t\treturn ((";
  protected final String TEXT_2319 = ") query.evaluate(this)).";
  protected final String TEXT_2320 = "();" + NL + "\t";
  protected final String TEXT_2321 = NL + "\t\t";
  protected final String TEXT_2322 = " xoclEval = new ";
  protected final String TEXT_2323 = "(this, cachedValues);" + NL + "\t\treturn (";
  protected final String TEXT_2324 = ") xoclEval.evaluateElement(eOverrideFeature, query);" + NL + "\t";
  protected final String TEXT_2325 = NL + "\t\t} catch(";
  protected final String TEXT_2326 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2327 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_2328 = ".valueOf(\"0\").";
  protected final String TEXT_2329 = "()";
  protected final String TEXT_2330 = "null";
  protected final String TEXT_2331 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2332 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_2333 = NL + "\t}";
  protected final String TEXT_2334 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @overrideOCL ";
  protected final String TEXT_2335 = " ";
  protected final String TEXT_2336 = NL + "\t * @templateTag INS03" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2337 = NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_2338 = NL + "\t@Override";
  protected final String TEXT_2339 = NL + "\tpublic ";
  protected final String TEXT_2340 = " ";
  protected final String TEXT_2341 = "()" + NL + "\t{";
  protected final String TEXT_2342 = NL + "\t\t// TODO: implement this method" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_2343 = NL + "\t\t    ";
  protected final String TEXT_2344 = " eClass = (";
  protected final String TEXT_2345 = ");" + NL + "\t\t    ";
  protected final String TEXT_2346 = " eOverrideFeature = ";
  protected final String TEXT_2347 = ";" + NL + "\t    ";
  protected final String TEXT_2348 = NL + "\t\tif (";
  protected final String TEXT_2349 = " == null) { " + NL + "\t\t\t";
  protected final String TEXT_2350 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setAttributeContext(eClass, eOverrideFeature);" + NL + "\t\t\t" + NL + "\t\t\tString derive = ";
  protected final String TEXT_2351 = ".findDeriveAnnotationText(eOverrideFeature, eClass());" + NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_2352 = " = helper.createQuery(derive);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2353 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2354 = ".valueOf(\"0\").";
  protected final String TEXT_2355 = "()";
  protected final String TEXT_2356 = "null";
  protected final String TEXT_2357 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2358 = ".handleQueryProblems(";
  protected final String TEXT_2359 = ".PLUGIN_ID, derive, helper.getProblems(), eClass, eOverrideFeature);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_2360 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_2361 = ");" + NL + "\t\ttry {" + NL + "\t\t\t";
  protected final String TEXT_2362 = ".enterContext(";
  protected final String TEXT_2363 = ".PLUGIN_ID, query, eClass, eOverrideFeature);" + NL + "\t";
  protected final String TEXT_2364 = NL + "\t\t";
  protected final String TEXT_2365 = " xoclEval = new ";
  protected final String TEXT_2366 = "(this, cachedValues);" + NL + "\t\treturn (";
  protected final String TEXT_2367 = ") xoclEval.evaluateElement(eOverrideFeature, query);" + NL + "\t";
  protected final String TEXT_2368 = NL + "\t\t\treturn ((";
  protected final String TEXT_2369 = ") query.evaluate(this)).";
  protected final String TEXT_2370 = "();" + NL + "\t";
  protected final String TEXT_2371 = NL + "\t\t";
  protected final String TEXT_2372 = " xoclEval = new ";
  protected final String TEXT_2373 = "(this, cachedValues);" + NL + "\t\treturn (";
  protected final String TEXT_2374 = ") xoclEval.evaluateElement(eOverrideFeature, query);" + NL + "\t";
  protected final String TEXT_2375 = NL + "\t\t} catch(";
  protected final String TEXT_2376 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2377 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_2378 = ".valueOf(\"0\").";
  protected final String TEXT_2379 = "()";
  protected final String TEXT_2380 = "null";
  protected final String TEXT_2381 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2382 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_2383 = NL + "\t}";
  protected final String TEXT_2384 = NL + "\t/**" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @overrideOCL ";
  protected final String TEXT_2385 = " ";
  protected final String TEXT_2386 = NL + "\t * @templateTag INS04" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2387 = NL + "\t@Override";
  protected final String TEXT_2388 = NL + "\tpublic ";
  protected final String TEXT_2389 = " ";
  protected final String TEXT_2390 = "(";
  protected final String TEXT_2391 = ")";
  protected final String TEXT_2392 = NL + "\t{";
  protected final String TEXT_2393 = NL + "\t\t// TODO: implement this method (no OCL found!)" + NL + "\t\t// Ensure that you remove @generated or mark it @generated NOT" + NL + "\t\tthrow new UnsupportedOperationException();";
  protected final String TEXT_2394 = NL + "\t\t";
  protected final String TEXT_2395 = " eClass = (";
  protected final String TEXT_2396 = ");" + NL + "\t\t";
  protected final String TEXT_2397 = " eOverrideOperation = ";
  protected final String TEXT_2398 = ".getEOperations().get(";
  protected final String TEXT_2399 = ");" + NL + "\t\tif (";
  protected final String TEXT_2400 = " == null) {" + NL + "\t\t\t";
  protected final String TEXT_2401 = ".Helper helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setOperationContext(eClass, eOverrideOperation);" + NL + "" + NL + "\t\t\t";
  protected final String TEXT_2402 = " ocl = eClass.getEAnnotation(OVERRIDE_OCL_ANNOTATION_SOURCE);" + NL + "\t\t\tString body = (String) ocl.getDetails().get(eOverrideOperation.getName()+\"Body\");";
  protected final String TEXT_2403 = NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_2404 = " = helper.createQuery(body);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2405 = " e) {" + NL + "\t\t\t\treturn ";
  protected final String TEXT_2406 = ".valueOf(\"0\").";
  protected final String TEXT_2407 = "()";
  protected final String TEXT_2408 = "null";
  protected final String TEXT_2409 = ";" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2410 = ".handleQueryProblems(";
  protected final String TEXT_2411 = ".PLUGIN_ID, body, helper.getProblems(), eClass, eOverrideOperation);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t\t";
  protected final String TEXT_2412 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_2413 = ");" + NL + "\t\ttry {" + NL + "\t\t\t";
  protected final String TEXT_2414 = ".enterContext(";
  protected final String TEXT_2415 = ".PLUGIN_ID, query, eClass, eOverrideOperation);" + NL + "\t";
  protected final String TEXT_2416 = " " + NL + "\t\t\t";
  protected final String TEXT_2417 = "<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();" + NL + "\t\t\t";
  protected final String TEXT_2418 = NL + "\t\t\tevalEnv.add(\"";
  protected final String TEXT_2419 = "\", ";
  protected final String TEXT_2420 = ");";
  protected final String TEXT_2421 = NL + "\t  ";
  protected final String TEXT_2422 = NL + "\t\t\t";
  protected final String TEXT_2423 = " xoclEval = new ";
  protected final String TEXT_2424 = "(this, cachedValues);" + NL + "\t\t\treturn (";
  protected final String TEXT_2425 = ") xoclEval.evaluateElement(eOverrideOperation, query);" + NL + "\t";
  protected final String TEXT_2426 = NL + "\t\t\treturn ((";
  protected final String TEXT_2427 = ") query.evaluate(this)).";
  protected final String TEXT_2428 = "();" + NL + "\t";
  protected final String TEXT_2429 = NL + "\t\t\t";
  protected final String TEXT_2430 = " xoclEval = new ";
  protected final String TEXT_2431 = "(this, cachedValues);" + NL + "\t\t\treturn (";
  protected final String TEXT_2432 = ") xoclEval.evaluateElement(eOverrideOperation, query);" + NL + "\t";
  protected final String TEXT_2433 = NL + "\t\t} catch(";
  protected final String TEXT_2434 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2435 = ".handleException(e);" + NL + "\t\t\treturn ";
  protected final String TEXT_2436 = ".valueOf(\"0\").";
  protected final String TEXT_2437 = "()";
  protected final String TEXT_2438 = "null";
  protected final String TEXT_2439 = ";" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2440 = ".leaveContext();" + NL + "\t\t}";
  protected final String TEXT_2441 = NL + "\t}";
  protected final String TEXT_2442 = NL + "\t/**" + NL + "\t * Evaluates the OCL defined choice constraint for the '<em><b>";
  protected final String TEXT_2443 = "</b></em>' ";
  protected final String TEXT_2444 = "." + NL + "     * The constraint is applied in the context of the source of the reference, and the target of the reference being of type ";
  protected final String TEXT_2445 = NL + "     * Inside the constraint, the target can be accessed as 'trg'. " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @overrideOCL ";
  protected final String TEXT_2446 = " ";
  protected final String TEXT_2447 = NL + "\t * @templateTag INS05" + NL + "\t * @generated" + NL + "\t */";
  protected final String TEXT_2448 = NL + "\t@Override";
  protected final String TEXT_2449 = NL + "\tpublic boolean eval";
  protected final String TEXT_2450 = "ChoiceConstraint(";
  protected final String TEXT_2451 = " trg){" + NL + "\t\t";
  protected final String TEXT_2452 = " eClass = ";
  protected final String TEXT_2453 = ";" + NL + "\t\t";
  protected final String TEXT_2454 = " eOverrideFeature = ";
  protected final String TEXT_2455 = ";" + NL + "\t\tif (";
  protected final String TEXT_2456 = "ChoiceConstraintOCL == null) {" + NL + "\t\t\tOCL.Helper helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setContext(eClass);" + NL + "\t\t\t" + NL + "\t\t\taddEnvironmentVariable(\"trg\", eOverrideFeature.getEType());" + NL + "" + NL + "\t\t\tString choiceConstraint = ";
  protected final String TEXT_2457 = ".findChoiceConstraintAnnotationText(eOverrideFeature, eClass());" + NL + "" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_2458 = "ChoiceConstraintOCL = helper" + NL + "\t\t\t\t\t\t.createQuery(choiceConstraint);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2459 = " e) {" + NL + "\t\t\t\treturn false;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2460 = ".handleQueryProblems(";
  protected final String TEXT_2461 = ".PLUGIN_ID, choiceConstraint, helper.getProblems(), eClass, eOverrideFeature);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_2462 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_2463 = "ChoiceConstraintOCL);" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_2464 = ".enterContext(";
  protected final String TEXT_2465 = ".PLUGIN_ID, query, eClass, eOverrideFeature);" + NL + "\t\tquery.getEvaluationEnvironment().add(\"trg\", trg);" + NL + "\t\treturn ((Boolean) query.evaluate(this)).booleanValue();" + NL + "\t\t} catch(";
  protected final String TEXT_2466 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2467 = ".handleException(e);" + NL + "\t\t\treturn false;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2468 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_2469 = NL + "\t/**" + NL + "\t * Evaluates the OCL defined choice construction for the '<em><b>";
  protected final String TEXT_2470 = "</b></em>' ";
  protected final String TEXT_2471 = "." + NL + "     * The constraint is applied in the context of the source of the reference, and the choice being of type ";
  protected final String TEXT_2472 = "<";
  protected final String TEXT_2473 = ">" + NL + "     * Inside the constraint, the choice can be accessed as 'choice'. " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @overrideOCL ";
  protected final String TEXT_2474 = " ";
  protected final String TEXT_2475 = NL + "\t * @templateTag INS06" + NL + "\t * @generated" + NL + "\t */" + NL + "\t@SuppressWarnings(\"unchecked\")";
  protected final String TEXT_2476 = NL + "\t@Override";
  protected final String TEXT_2477 = NL + "\tpublic ";
  protected final String TEXT_2478 = "<";
  protected final String TEXT_2479 = "> eval";
  protected final String TEXT_2480 = "ChoiceConstruction(";
  protected final String TEXT_2481 = "<";
  protected final String TEXT_2482 = "> choice){" + NL + "\t\t";
  protected final String TEXT_2483 = " eClass = ";
  protected final String TEXT_2484 = ";" + NL + "\t\t";
  protected final String TEXT_2485 = " eOverrideStructuralFeature = ";
  protected final String TEXT_2486 = ";" + NL + "\t\tif (";
  protected final String TEXT_2487 = "ChoiceConstructionOCL == null) {" + NL + "\t\t\tOCL.Helper helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\t//the actual class.... TODO: is this the right one" + NL + "\t\t\thelper.setContext(eClass);" + NL + "\t\t\t// create a variable declaring our global application context object" + NL + "\t\t\t";
  protected final String TEXT_2488 = " choiceVar = " + NL + "\t\t\t      ";
  protected final String TEXT_2489 = ".eINSTANCE.createVariable();" + NL + "\t\t\tchoiceVar.setName(\"choice\");" + NL + "\t\t\tchoiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary().getSequence());" + NL + "\t\t\t// add it to the global OCL environment" + NL + "\t\t\tOCL_ENV.getEnvironment().addElement(choiceVar.getName()," + NL + "\t\t\t\t\tchoiceVar, true);" + NL + "" + NL + "\t\t\tString choiceConstruction = ";
  protected final String TEXT_2490 = ".findChoiceConstructionAnnotationText(eOverrideStructuralFeature, eClass());" + NL + "" + NL + "\t\t\ttry {" + NL + "\t\t\t\t";
  protected final String TEXT_2491 = "ChoiceConstructionOCL = helper" + NL + "\t\t\t\t\t\t.createQuery(choiceConstruction);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2492 = " e) {" + NL + "\t\t\t\treturn choice;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2493 = ".handleQueryProblems(";
  protected final String TEXT_2494 = ".PLUGIN_ID, choiceConstruction, helper.getProblems(), eClass, eOverrideStructuralFeature);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t";
  protected final String TEXT_2495 = " query = OCL_ENV.createQuery(";
  protected final String TEXT_2496 = "ChoiceConstructionOCL);" + NL + "\t\ttry {" + NL + "\t\t";
  protected final String TEXT_2497 = ".enterContext(";
  protected final String TEXT_2498 = ".PLUGIN_ID, query, eClass, eOverrideStructuralFeature);" + NL + "\t\tquery.getEvaluationEnvironment().add(\"choice\", choice);" + NL + "\t\treturn (ArrayList<";
  protected final String TEXT_2499 = ">) query.evaluate(this);" + NL + "\t\t} catch(";
  protected final String TEXT_2500 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2501 = ".handleException(e);" + NL + "\t\t\treturn choice;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2502 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}";
  protected final String TEXT_2503 = NL + "\t" + NL + "\t/**" + NL + "\t * Returns Update Annotation Meta Info specific for this class.  " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS16" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_2504 = "[][] getUpdateAnnotationMetaInfo() {" + NL + "\t\treturn updateAnnotationMetaInfo;" + NL + "\t}";
  protected final String TEXT_2505 = NL + NL + "\t/**" + NL + "\t * Returns the cache for init annotation OCL expressions" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS07" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_2506 = "<";
  protected final String TEXT_2507 = ", ";
  protected final String TEXT_2508 = "> getInitOclExpressionMap() {" + NL + "\t\treturn ourInitOclExpressionMap; " + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * Returns the cache for init order annotation OCL expressions" + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS08" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic ";
  protected final String TEXT_2509 = "<";
  protected final String TEXT_2510 = ", ";
  protected final String TEXT_2511 = "> getInitOrderOclExpressionMap() {" + NL + "\t\treturn ourInitOrderOclExpressionMap; " + NL + "\t}";
  protected final String TEXT_2512 = NL + NL + "\t/**" + NL + "\t * @templateTag INS09" + NL + "\t * @generated" + NL + "\t */" + NL + "  \t";
  protected final String TEXT_2513 = NL + "\t\t@Override" + NL + "  \t";
  protected final String TEXT_2514 = NL + "    public ";
  protected final String TEXT_2515 = " eBasicSetContainer(";
  protected final String TEXT_2516 = " newContainer, int newContainerFeatureID," + NL + "\t\t      ";
  protected final String TEXT_2517 = " msgs) {  \t" + NL + "  \t\t";
  protected final String TEXT_2518 = " result = super.eBasicSetContainer(newContainer, newContainerFeatureID, msgs);" + NL + "\t\tfor (";
  protected final String TEXT_2519 = " eStructuralFeature : eClass().getEAllStructuralFeatures()) {" + NL + "\t\t\tif (eStructuralFeature instanceof ";
  protected final String TEXT_2520 = ") {" + NL + "\t\t\t\t";
  protected final String TEXT_2521 = " eReference = (";
  protected final String TEXT_2522 = ") eStructuralFeature;" + NL + "\t\t\t\tif (eReference.isContainer()) {" + NL + "\t\t\t\t\tif (eContainmentFeature() == eReference.getEOpposite()) {" + NL + "\t\t\t\t\t\tcontinue;" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t\tif (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {" + NL + "\t\t\t\tif ((myInitValueMap == null) || (myInitValueMap.get(eStructuralFeature) != eGet(eStructuralFeature))) {" + NL + "\t\t\t\t\tmyInitValueMap = null;" + NL + "\t\t\t\t\treturn result;" + NL + "\t\t\t\t}" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\tmyInitValueMap = null;" + NL + "\t\t";
  protected final String TEXT_2523 = " eInternalResource = eInternalResource();" + NL + "\t\tensureClassInitialized((eInternalResource != null) && eInternalResource.isLoading());" + NL + "\t\treturn result;" + NL + "\t}" + NL + "\t" + NL + "\t/**" + NL + "\t * @templateTag INS15" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void allowInitialization() {" + NL + "\t\tif (myInitValueMap == null) {" + NL + "\t\t\tmyInitValueMap = new ";
  protected final String TEXT_2524 = "<";
  protected final String TEXT_2525 = ", ";
  protected final String TEXT_2526 = ">();" + NL + "\t\t}" + NL + "\t\tif (eClass() != null) {" + NL + "\t\t\tfor (EStructuralFeature eStructuralFeature : eClass().getEAllStructuralFeatures()) {" + NL + "\t\t\t\tif (eStructuralFeature.isDerived()) {" + NL + "\t\t\t\t\tcontinue;" + NL + "\t\t\t\t}" + NL + "\t\t\t\tmyInitValueMap.put(eStructuralFeature, eGet(eStructuralFeature));" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t}" + NL + "\t" + NL + "\t/**" + NL + "\t * Returns an array of structural features which are initialized with the init-family annotations " + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS10" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_2527 = "[] getInitializedStructuralFeatures() {";
  protected final String TEXT_2528 = NL + "  \t\t";
  protected final String TEXT_2529 = "[] initializedFeatures = new ";
  protected final String TEXT_2530 = "[] {" + NL + "  \t\t";
  protected final String TEXT_2531 = ", ";
  protected final String TEXT_2532 = NL + "  \t\t};" + NL + "  \t\treturn initializedFeatures;\t\t\t\t" + NL + "\t}" + NL + "\t" + NL + "\t/**" + NL + "\t * This method checks whether the class is initialized." + NL + "\t * If it is not yet initialized then the initialization is performed." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS11" + NL + "\t * @generated" + NL + "\t */" + NL + "\tpublic void ensureClassInitialized(boolean isLoadInProgress) {" + NL + "\t\tif (_isInitialized) {" + NL + "\t\t\treturn;" + NL + "\t\t}" + NL + "\t\t_isInitialized = true;" + NL + "  \t\t";
  protected final String TEXT_2533 = "[] initializedFeatures = getInitializedStructuralFeatures();" + NL + "  \t\t" + NL + "  \t\tif (isLoadInProgress) {" + NL + "  \t\t\t// only transient features are initialized then" + NL + "  \t\t\t";
  protected final String TEXT_2534 = "<";
  protected final String TEXT_2535 = "> filteredInitializedFeatures = new ";
  protected final String TEXT_2536 = "<";
  protected final String TEXT_2537 = ">();" + NL + "  \t\t\tfor (";
  protected final String TEXT_2538 = " initializedFeature : initializedFeatures) {" + NL + "  \t\t\t\tif (initializedFeature.isTransient()) {" + NL + "  \t\t\t\t\tfilteredInitializedFeatures.add(initializedFeature);" + NL + "  \t\t\t\t}" + NL + "  \t\t\t}" + NL + "  \t\t\tinitializedFeatures = filteredInitializedFeatures.toArray(new ";
  protected final String TEXT_2539 = "[filteredInitializedFeatures.size()]);" + NL + "  \t\t}" + NL + "" + NL + "\t\tfinal ";
  protected final String TEXT_2540 = "<";
  protected final String TEXT_2541 = ", ";
  protected final String TEXT_2542 = "> initOrderMap = new ";
  protected final String TEXT_2543 = "<";
  protected final String TEXT_2544 = ", ";
  protected final String TEXT_2545 = ">(); " + NL + "\t\tfor (";
  protected final String TEXT_2546 = " structuralFeature : initializedFeatures) {" + NL + "\t\t\t";
  protected final String TEXT_2547 = " value = evaluateInitOclAnnotation(structuralFeature, getInitOrderOclExpressionMap(), \"initOrder\", \"InitOrder\", true);";
  protected final String TEXT_2548 = NL + "\t\t\tif (value != NO_OBJECT) {" + NL + "\t\t\t\tinitOrderMap.put(structuralFeature, value);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tif (!initOrderMap.isEmpty()) {" + NL + "\t\t\t";
  protected final String TEXT_2549 = ".sort(initializedFeatures, new ";
  protected final String TEXT_2550 = "<";
  protected final String TEXT_2551 = ">() {" + NL + "\t\t\t\tpublic int compare(";
  protected final String TEXT_2552 = " structuralFeature1, ";
  protected final String TEXT_2553 = " structuralFeature2) {" + NL + "\t\t\t\t\t";
  protected final String TEXT_2554 = " comparedObject1 = initOrderMap.get(structuralFeature1);" + NL + "\t\t\t\t\t";
  protected final String TEXT_2555 = " comparedObject2 = initOrderMap.get(structuralFeature2);" + NL + "\t\t\t\t\tif (comparedObject1 == null) {" + NL + "\t\t\t\t\t\tif (comparedObject2 == null) {" + NL + "\t\t\t\t\t\t\tint index1 = eClass().getEAllStructuralFeatures().indexOf(comparedObject1);" + NL + "\t\t\t\t\t\t\tint index2 = eClass().getEAllStructuralFeatures().indexOf(comparedObject2);" + NL + "\t\t\t\t\t\t\treturn index1 - index2;" + NL + "\t\t\t\t\t\t} else {" + NL + "\t\t\t\t\t\t\treturn 1;" + NL + "\t\t\t\t\t\t}" + NL + "\t\t\t\t\t} else if (comparedObject2 == null) {" + NL + "\t\t\t\t\t\treturn -1;" + NL + "\t\t\t\t\t}" + NL + "\t\t\t\t\treturn ";
  protected final String TEXT_2556 = ".compare(comparedObject1, comparedObject2);" + NL + "\t\t\t\t}" + NL + "\t\t\t});" + NL + "\t\t}" + NL + "\t" + NL + "\t\tfor (";
  protected final String TEXT_2557 = " structuralFeature : initializedFeatures) {" + NL + "\t\t\t";
  protected final String TEXT_2558 = " value = evaluateInitOclAnnotation(structuralFeature, getInitOclExpressionMap(), \"initValue\", \"InitValue\", false);";
  protected final String TEXT_2559 = NL + "\t\t\tif (value != NO_OBJECT) {" + NL + "\t\t\t\teSet(structuralFeature, value);" + NL + "\t\t\t}" + NL + "\t\t}" + NL + "\t}" + NL + "" + NL + "\t/**" + NL + "\t * Evaluates the value of an init-family annotation for the property." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS12" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected Object evaluateInitOclAnnotation(";
  protected final String TEXT_2560 = " structuralFeature, " + NL + "\t\t\t\t\t";
  protected final String TEXT_2561 = "<";
  protected final String TEXT_2562 = ", ";
  protected final String TEXT_2563 = "> expressionMap," + NL + "\t\t\t\t\tString annotationKey, String annotationOverrideKey, boolean isSimpleEvaluate) {" + NL + "\t\t";
  protected final String TEXT_2564 = " oclExpression = getInitOclAnnotationExpression(structuralFeature, expressionMap, annotationKey, annotationOverrideKey);" + NL + "\t\t" + NL + "\t\tif (oclExpression == null) {" + NL + "\t\t\treturn NO_OBJECT;" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\t";
  protected final String TEXT_2565 = " query = OCL_ENV.createQuery(oclExpression);" + NL + "\t\ttry {" + NL + "\t\t\t";
  protected final String TEXT_2566 = ".enterContext(";
  protected final String TEXT_2567 = ".PLUGIN_ID, query, ";
  protected final String TEXT_2568 = ", \"initOclAnnotation(\"+structuralFeature.getName()+\")\");" + NL + "\t\t" + NL + "\t\t\tquery.getEvaluationEnvironment().clear();" + NL + "\t\t\t";
  protected final String TEXT_2569 = " trg = eGet(structuralFeature);" + NL + "\t\t\tquery.getEvaluationEnvironment().add(\"trg\", trg); ";
  protected final String TEXT_2570 = NL + "\t\t" + NL + "\t\t\tif (isSimpleEvaluate) {" + NL + "\t\t\t\treturn query.evaluate(this);" + NL + "\t\t\t}" + NL + "\t\t\t";
  protected final String TEXT_2571 = " xoclEval = new ";
  protected final String TEXT_2572 = "(this, new ";
  protected final String TEXT_2573 = "<";
  protected final String TEXT_2574 = ", ";
  protected final String TEXT_2575 = ">());" + NL + "\t\t\txoclEval.setContainerOnCreation(this);" + NL + "\t\t\t" + NL + "\t\t\treturn xoclEval.evaluateElement(structuralFeature, query);" + NL + "\t\t} catch(";
  protected final String TEXT_2576 = " e) {" + NL + "\t\t\t";
  protected final String TEXT_2577 = ".handleException(e);" + NL + "\t\t\treturn NO_OBJECT;" + NL + "\t\t} finally {" + NL + "\t\t\t";
  protected final String TEXT_2578 = ".leaveContext();" + NL + "\t\t}" + NL + "\t}" + NL + "\t" + NL + "\t/**" + NL + "\t * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache." + NL + "\t * <!-- begin-user-doc -->" + NL + "\t * <!-- end-user-doc -->" + NL + "\t * @templateTag INS13" + NL + "\t * @generated" + NL + "\t */" + NL + "\tprotected ";
  protected final String TEXT_2579 = " getInitOclAnnotationExpression(";
  protected final String TEXT_2580 = " structuralFeature, " + NL + "\t\t\t\t\t";
  protected final String TEXT_2581 = "<";
  protected final String TEXT_2582 = ", ";
  protected final String TEXT_2583 = "> expressionMap," + NL + "\t\t\t\t\tString annotationKey, String annotationOverrideKey) {" + NL + "\t\t";
  protected final String TEXT_2584 = " oclExpression = expressionMap.get(structuralFeature);" + NL + "\t\tif (oclExpression != null) {" + NL + "\t\t\treturn oclExpression;" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\tString oclText = ";
  protected final String TEXT_2585 = ".findAnnotationText(structuralFeature, eClass(), annotationKey, annotationOverrideKey);" + NL + "" + NL + "\t\tif (oclText != null) {" + NL + "\t\t\t// Hurray, the expression text is found! Let's compile it" + NL + "\t\t\t";
  protected final String TEXT_2586 = " helper = OCL_ENV.createOCLHelper();" + NL + "\t\t\thelper.setAttributeContext(eClass(), structuralFeature);" + NL + "\t\t\t" + NL + "\t\t\t";
  protected final String TEXT_2587 = " propertyType = ";
  protected final String TEXT_2588 = ".getPropertyType(OCL_ENV.getEnvironment(), eClass(), structuralFeature);" + NL + "\t\t\taddEnvironmentVariable(\"trg\", propertyType); ";
  protected final String TEXT_2589 = NL + "\t\t\t" + NL + "\t\t\ttry {" + NL + "\t\t\t\toclExpression = helper.createQuery(oclText);" + NL + "\t\t\t} catch (";
  protected final String TEXT_2590 = " e) {" + NL + "\t\t\t\treturn null;" + NL + "\t\t\t} finally {" + NL + "\t\t\t\t";
  protected final String TEXT_2591 = ".handleQueryProblems(";
  protected final String TEXT_2592 = ".PLUGIN_ID, oclText, helper.getProblems(), eClass(), structuralFeature);" + NL + "\t\t\t}" + NL + "" + NL + "\t\t\texpressionMap.put(structuralFeature, oclExpression);" + NL + "\t\t}" + NL + "\t\t" + NL + "\t\treturn oclExpression;" + NL + "\t}\t";
  protected final String TEXT_2593 = NL + " \t" + NL + "\t/**" + NL + "\t * @templateTag INS14" + NL + "\t * @generated" + NL + "\t */" + NL + "  \t\t";
  protected final String TEXT_2594 = NL + "\t@Override" + NL + "  \t\t";
  protected final String TEXT_2595 = NL + "\tprotected ";
  protected final String TEXT_2596 = "[] getInitializedStructuralFeatures() {" + NL + "\t\t";
  protected final String TEXT_2597 = "[] baseInititalizedFeatured = super.getInitializedStructuralFeatures();" + NL + "  \t\t";
  protected final String TEXT_2598 = "[] initializedFeatures = new ";
  protected final String TEXT_2599 = "[] {" + NL + "  \t\t";
  protected final String TEXT_2600 = ", ";
  protected final String TEXT_2601 = NL + "  \t\t};" + NL + "  \t\t";
  protected final String TEXT_2602 = "[] allInitializedFeatures = new ";
  protected final String TEXT_2603 = "[baseInititalizedFeatured.length + initializedFeatures.length];" + NL + "  \t\tSystem.arraycopy(baseInititalizedFeatured, 0, allInitializedFeatures, 0, baseInititalizedFeatured.length);" + NL + "  \t\tSystem.arraycopy(initializedFeatures, 0, allInitializedFeatures, baseInititalizedFeatured.length, initializedFeatures.length);" + NL + "  \t\treturn allInitializedFeatures;\t\t\t\t" + NL + "\t}" + NL + " \t" + NL + " \t";
  protected final String TEXT_2604 = NL + "} //";
  protected final String TEXT_2605 = NL;

  public String generate(Object argument)
  {
    final StringBuffer stringBuffer = new StringBuffer();
     final GenClass genClass = (GenClass)((Object[])argument)[0]; final GenPackage genPackage = genClass.getGenPackage(); final GenModel genModel=genPackage.getGenModel();
    stringBuffer.append(TEXT_1);
    {GenBase copyrightHolder = argument instanceof GenBase ? (GenBase)argument : argument instanceof Object[] && ((Object[])argument)[0] instanceof GenBase ? (GenBase)((Object[])argument)[0] : null;
    if (copyrightHolder != null && copyrightHolder.hasCopyright()) {
    stringBuffer.append(TEXT_2);
    stringBuffer.append(copyrightHolder.getCopyright(copyrightHolder.getGenModel().getIndentation(stringBuffer)));
    }}
    stringBuffer.append(TEXT_3);
     final XoclAnnotationHelper oclAnnotationsHelper = new XoclAnnotationHelper(genClass);
     final String oclNsURI = XoclAnnotationHelper.oclNsURI; 
     final String namedOclNsURI = XoclAnnotationHelper.namedOclNsURI; 
     final String overrideOclNsURI = XoclAnnotationHelper.overrideOclNsURI; 
     final String expressionOclNsURI = XoclAnnotationHelper.expressionOclNsURI; 
    stringBuffer.append(TEXT_4);
    final boolean isJDK50 = genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50;
    final boolean isInterface = Boolean.TRUE.equals(((Object[])argument)[1]); final boolean isImplementation = Boolean.TRUE.equals(((Object[])argument)[2]);
    final boolean isGWT = genModel.getRuntimePlatform() == GenRuntimePlatform.GWT;
    final String publicStaticFinalFlag = isImplementation ? "public static final " : "";
    final String singleWildcard = isJDK50 ? "<?>" : "";
    final String negativeOffsetCorrection = genClass.hasOffsetCorrection() ? " - " + genClass.getOffsetCorrectionField(null) : "";
    final String positiveOffsetCorrection = genClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(null) : "";
    final String negativeOperationOffsetCorrection = genClass.hasOffsetCorrection() ? " - EOPERATION_OFFSET_CORRECTION" : "";
    final String positiveOperationOffsetCorrection = genClass.hasOffsetCorrection() ? " + EOPERATION_OFFSET_CORRECTION" : "";
    if (isInterface) {
    stringBuffer.append(TEXT_5);
    stringBuffer.append(genPackage.getInterfacePackageName());
    stringBuffer.append(TEXT_6);
    } else {
    stringBuffer.append(TEXT_7);
    stringBuffer.append(genPackage.getClassPackageName());
    stringBuffer.append(TEXT_8);
    }
    stringBuffer.append(TEXT_9);
    genModel.markImportLocation(stringBuffer, genPackage);
    if (isImplementation) { genClass.addClassPsuedoImports(); }
    stringBuffer.append(TEXT_10);
    if (isInterface) {
    stringBuffer.append(TEXT_11);
    stringBuffer.append(genClass.getFormattedName());
    stringBuffer.append(TEXT_12);
    if (genClass.hasDocumentation()) {
    stringBuffer.append(TEXT_13);
    stringBuffer.append(genClass.getDocumentation(genModel.getIndentation(stringBuffer)));
    stringBuffer.append(TEXT_14);
    }
    stringBuffer.append(TEXT_15);
    if (!genClass.getGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_16);
    for (GenFeature genFeature : genClass.getGenFeatures()) {
    if (!genFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_17);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_18);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_19);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_20);
    }
    }
    stringBuffer.append(TEXT_21);
    }
    stringBuffer.append(TEXT_22);
    if (!genModel.isSuppressEMFMetaData()) {
    stringBuffer.append(TEXT_23);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_24);
    stringBuffer.append(genClass.getClassifierAccessorName());
    stringBuffer.append(TEXT_25);
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genClass.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_26);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_27);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_28);
    }}
    if (genClass.needsRootExtendsInterfaceExtendsTag()) {
    stringBuffer.append(TEXT_29);
    stringBuffer.append(genModel.getImportedName(genModel.getRootExtendsInterface()));
    }
    stringBuffer.append(TEXT_30);
    } else {
    stringBuffer.append(TEXT_31);
    stringBuffer.append(genClass.getFormattedName());
    stringBuffer.append(TEXT_32);
    if (!genClass.getImplementedGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_33);
    for (GenFeature genFeature : genClass.getImplementedGenFeatures()) {
    stringBuffer.append(TEXT_34);
    stringBuffer.append(genClass.getQualifiedClassName());
    stringBuffer.append(TEXT_35);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_36);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_37);
    }
    stringBuffer.append(TEXT_38);
    }
    stringBuffer.append(TEXT_39);
    }
    stringBuffer.append(TEXT_40);
    /*
  Common Utility Class
*/
final class XoclGenerationUtil {
	public String embedOclIntoJavaComment(String ocl) {
		return ocl.replaceAll("[*]/", "*\\\\/");
	}
}
final XoclGenerationUtil xoclGenerationUtil = new XoclGenerationUtil();
    stringBuffer.append(TEXT_41);
    if (isImplementation) {
    stringBuffer.append(TEXT_42);
    if (genClass.isAbstract()) {
    stringBuffer.append(TEXT_43);
    }
    stringBuffer.append(TEXT_44);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(genClass.getTypeParameters().trim());
    stringBuffer.append(genClass.getClassExtends());
    stringBuffer.append(genClass.getClassImplements());
    if (oclAnnotationsHelper.isDirectInitializable()) {
    if (genClass.getClassImplements().trim().length() > 0) {
    stringBuffer.append(TEXT_45);
     } else { 
    stringBuffer.append(TEXT_46);
     } 
    stringBuffer.append(TEXT_47);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.IXoclInitializable"));
    }	
} else {
    stringBuffer.append(TEXT_48);
    stringBuffer.append(genClass.getInterfaceName());
    stringBuffer.append(genClass.getTypeParameters().trim());
    stringBuffer.append(genClass.getInterfaceExtends());
    }
    stringBuffer.append(TEXT_49);
    if (genModel.hasCopyrightField()) {
    stringBuffer.append(TEXT_50);
    stringBuffer.append(publicStaticFinalFlag);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_51);
    stringBuffer.append(genModel.getCopyrightFieldLiteral());
    stringBuffer.append(TEXT_52);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_53);
    }
    if (isImplementation && genModel.getDriverNumber() != null) {
    stringBuffer.append(TEXT_54);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_55);
    stringBuffer.append(genModel.getDriverNumber());
    stringBuffer.append(TEXT_56);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_57);
    }
    if (isImplementation && genClass.isJavaIOSerializable()) {
    stringBuffer.append(TEXT_58);
    }
    if (isImplementation && genModel.isVirtualDelegation()) { String eVirtualValuesField = genClass.getEVirtualValuesField();
    if (eVirtualValuesField != null) {
    stringBuffer.append(TEXT_59);
    if (isGWT) {
    stringBuffer.append(TEXT_60);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_61);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_62);
    }
    { List<String> eVirtualIndexBitFields = genClass.getEVirtualIndexBitFields(new ArrayList<String>());
    if (!eVirtualIndexBitFields.isEmpty()) {
    for (String eVirtualIndexBitField : eVirtualIndexBitFields) {
    stringBuffer.append(TEXT_63);
    if (isGWT) {
    stringBuffer.append(TEXT_64);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_65);
    stringBuffer.append(eVirtualIndexBitField);
    stringBuffer.append(TEXT_66);
    }
    }
    }
    }
    if (isImplementation && genClass.isModelRoot() && genModel.isBooleanFlagsEnabled() && genModel.getBooleanFlagsReservedBits() == -1) {
    stringBuffer.append(TEXT_67);
    if (isGWT) {
    stringBuffer.append(TEXT_68);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_69);
    stringBuffer.append(genModel.getBooleanFlagsField());
    stringBuffer.append(TEXT_70);
    }
    if (isImplementation && !genModel.isReflectiveDelegation()) {
    for (GenFeature genFeature : genClass.getDeclaredFieldGenFeatures()) {
    if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_71);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_72);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_73);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_74);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_75);
    if (isGWT) {
    stringBuffer.append(TEXT_76);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_77);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_78);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_79);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_80);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_81);
    } else if (genFeature.isListType() || genFeature.isReferenceType()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_82);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_83);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_84);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_85);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_86);
    if (isGWT) {
    stringBuffer.append(TEXT_87);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_88);
    stringBuffer.append(genFeature.getImportedInternalType(genClass));
    stringBuffer.append(TEXT_89);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_90);
    }
    if (genModel.isArrayAccessors() && genFeature.isListType() && !genFeature.isFeatureMapType() && !genFeature.isMapType()) { String rawListItemType = genFeature.getRawListItemType(); int index = rawListItemType.indexOf('['); String head = rawListItemType; String tail = ""; if (index != -1) { head = rawListItemType.substring(0, index); tail = rawListItemType.substring(index); } 
    stringBuffer.append(TEXT_91);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_92);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_93);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_94);
    if (genFeature.getQualifiedListItemType(genClass).contains("<")) {
    stringBuffer.append(TEXT_95);
    }
    stringBuffer.append(TEXT_96);
    stringBuffer.append(rawListItemType);
    stringBuffer.append(TEXT_97);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_98);
    stringBuffer.append(head);
    stringBuffer.append(TEXT_99);
    stringBuffer.append(tail);
    stringBuffer.append(TEXT_100);
    }
    } else {
    if (genFeature.hasEDefault() && (!genFeature.isVolatile() || !genModel.isReflectiveDelegation() && (!genFeature.hasDelegateFeature() || !genFeature.isUnsettable()))) { String staticDefaultValue = genFeature.getStaticDefaultValue();
    stringBuffer.append(TEXT_101);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_102);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_103);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_104);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_105);
    if (genModel.useGenerics() && genFeature.isListDataType() && genFeature.isSetDefaultValue()) {
    stringBuffer.append(TEXT_106);
    }
    stringBuffer.append(TEXT_107);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_108);
    stringBuffer.append(genFeature.getEDefault());
    if ("".equals(staticDefaultValue)) {
    stringBuffer.append(TEXT_109);
    stringBuffer.append(genFeature.getEcoreFeature().getDefaultValueLiteral());
    stringBuffer.append(TEXT_110);
    } else {
    stringBuffer.append(TEXT_111);
    stringBuffer.append(staticDefaultValue);
    stringBuffer.append(TEXT_112);
    stringBuffer.append(genModel.getNonNLS(staticDefaultValue));
    }
    stringBuffer.append(TEXT_113);
    }
    if (genClass.isField(genFeature)) {
    if (genClass.isFlag(genFeature)) { int flagIndex = genClass.getFlagIndex(genFeature);
    if (flagIndex > 31 && flagIndex % 32 == 0) {
    stringBuffer.append(TEXT_114);
    if (isGWT) {
    stringBuffer.append(TEXT_115);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_116);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_117);
    }
    if (genFeature.isEnumType()) {
    stringBuffer.append(TEXT_118);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_119);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_120);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_121);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_122);
    stringBuffer.append(flagIndex % 32);
    stringBuffer.append(TEXT_123);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_124);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_125);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_126);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_127);
    if (isJDK50) {
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_128);
    } else {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_129);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_130);
    }
    stringBuffer.append(TEXT_131);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_132);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_133);
    stringBuffer.append(genFeature.getTypeGenClassifier().getFormattedName());
    stringBuffer.append(TEXT_134);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_135);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_136);
    if (isJDK50) {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_137);
    } else {
    stringBuffer.append(TEXT_138);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_139);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_140);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_141);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_142);
    }
    stringBuffer.append(TEXT_143);
    }
    stringBuffer.append(TEXT_144);
    stringBuffer.append(genClass.getFlagSize(genFeature) > 1 ? "s" : "");
    stringBuffer.append(TEXT_145);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_146);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_147);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_148);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_149);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_150);
    stringBuffer.append(genClass.getFlagMask(genFeature));
    stringBuffer.append(TEXT_151);
    if (genFeature.isEnumType()) {
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_152);
    } else {
    stringBuffer.append(flagIndex % 32);
    }
    stringBuffer.append(TEXT_153);
    } else {
    stringBuffer.append(TEXT_154);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_155);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_156);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_157);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_158);
    if (isGWT) {
    stringBuffer.append(TEXT_159);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_160);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_161);
    stringBuffer.append(genFeature.getSafeName());
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_162);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_163);
    }
    }
    }
    if (genClass.isESetField(genFeature)) {
    if (genClass.isESetFlag(genFeature)) { int flagIndex = genClass.getESetFlagIndex(genFeature);
    if (flagIndex > 31 && flagIndex % 32 == 0) {
    stringBuffer.append(TEXT_164);
    if (isGWT) {
    stringBuffer.append(TEXT_165);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_166);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_167);
    }
    stringBuffer.append(TEXT_168);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_169);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_170);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_171);
    stringBuffer.append(flagIndex % 32 );
    stringBuffer.append(TEXT_172);
    } else {
    stringBuffer.append(TEXT_173);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_174);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_175);
    if (isGWT) {
    stringBuffer.append(TEXT_176);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_177);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_178);
    }
    }
    //Class/declaredFieldGenFeature.override.javajetinc
    }
    }
    if (isImplementation) { boolean hasOCL = false; boolean hasOverrideOCL = false; 
    for (GenOperation genOperation : oclAnnotationsHelper.getImplementedGenOperations(genClass)) {
	String body = null;
	EAnnotation ocl = genOperation.getEcoreOperation().getEAnnotation(oclNsURI);
	if (ocl != null) body = ocl.getDetails().get("body");
	boolean override = false; EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	if (eAnnotation != null) {
		if (eAnnotation.getDetails().get(genOperation.getName()+"Body") != null) { override = true; }
	}
	if (!override && body != null) { hasOCL = true; 
    stringBuffer.append(TEXT_179);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_180);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_181);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_182);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_183);
    stringBuffer.append(oclAnnotationsHelper.getOperationBodyVariable(genOperation));
    stringBuffer.append(TEXT_184);
    }
  }
    
  for (GenFeature genFeature : genClass.getImplementedGenFeatures()) {
	EAnnotation ocl = genFeature.getEcoreFeature().getEAnnotation(oclNsURI);
	{
	String derive = null;
	if (ocl != null) derive = ocl.getDetails().get("derive");
	boolean override = false; EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	if (eAnnotation != null) {
		if (eAnnotation.getDetails().get(genFeature.getSafeName()+"Derive") != null) { override = true; }
	}
	if (!override && derive != null) { hasOCL = true; 
    stringBuffer.append(TEXT_185);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_186);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_187);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_188);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_189);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_190);
    }
    } {
    String choiceConstraint = null;
	if (ocl != null) choiceConstraint = ocl.getDetails().get("choiceConstraint");
	boolean override = false; EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	if (eAnnotation != null) {
		if (eAnnotation.getDetails().get(genFeature.getSafeName()+"ChoiceConstraint") != null) { override = true; }
	}
	if (!override && choiceConstraint != null && genFeature.getEcoreFeature().isChangeable()) { hasOCL = true; 
    stringBuffer.append(TEXT_191);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_192);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_193);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_194);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_195);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_196);
    }
    } {
    String choiceConstruction = null;
	if (ocl != null) choiceConstruction = ocl.getDetails().get("choiceConstruction");
	boolean override = false; EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	if (eAnnotation != null) {
		if (eAnnotation.getDetails().get(genFeature.getSafeName()+"ChoiceConstruction") != null) { override = true; }
	}
	if (!override && choiceConstruction != null && genFeature.getEcoreFeature().isChangeable()) { hasOCL = true; 
    stringBuffer.append(TEXT_197);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_198);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_199);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_200);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_201);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_202);
    }
    }
  }
  
  for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclDerivedGenFeatures(genClass)) { 
  	hasOverrideOCL = true; 
    stringBuffer.append(TEXT_203);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_204);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_205);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_206);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_207);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_208);
    
  }
  EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
  if (eAnnotation != null) {
  	EMap<String, String> details = eAnnotation.getDetails();
  	for (Map.Entry<String, String> entry : details) {
  		String detailKey = entry.getKey();
  		if (detailKey.endsWith("Body")) {
			detailKey = detailKey.substring(0, detailKey.length()-4);
			String body = entry.getValue();
			for (GenOperation genOperation : genClass.getAllGenOperations()) {
				if (genOperation.getName().equals(detailKey)) { hasOverrideOCL = true; 
    stringBuffer.append(TEXT_209);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_210);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_211);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_212);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_213);
    stringBuffer.append(oclAnnotationsHelper.getOperationBodyVariable(genOperation));
    stringBuffer.append(TEXT_214);
    
				}
			}
		}
  	}
  }
  
  for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstraintGenFeatures(genClass)) { 
  	hasOverrideOCL = true; 
    stringBuffer.append(TEXT_215);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_216);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_217);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_218);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_219);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_220);
    
  }
  
  for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstructionGenFeatures(genClass)) {
  	hasOverrideOCL = true; 
    stringBuffer.append(TEXT_221);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_222);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_223);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_224);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_225);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_226);
    
  }
      
  EAnnotation ocl = genClass.getEcoreClass().getEAnnotation(oclNsURI);
  String label = null;
	if (ocl != null) label = ocl.getDetails().get("label");
	if (label != null) { hasOCL = true;
    stringBuffer.append(TEXT_227);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_228);
    }
    stringBuffer.append(TEXT_229);
    if (oclAnnotationsHelper.isInitializable()) {
    stringBuffer.append(TEXT_230);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_231);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_232);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_233);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_234);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_235);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_236);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_237);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_238);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_239);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_240);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_241);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_242);
    } // endif
 	if (oclAnnotationsHelper.isDirectInitializable()) {
 		hasOCL = true; 
 		hasOverrideOCL = true;
    stringBuffer.append(TEXT_243);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_244);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_245);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_246);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_247);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_248);
    
 	} // endif 
    if (hasOCL) { 
    stringBuffer.append(TEXT_249);
    stringBuffer.append(oclNsURI);
    stringBuffer.append(TEXT_250);
    stringBuffer.append(genModel.getNonNLS());
     }
  if (hasOverrideOCL) { 
    stringBuffer.append(TEXT_251);
    stringBuffer.append(overrideOclNsURI);
    stringBuffer.append(TEXT_252);
    stringBuffer.append(genModel.getNonNLS());
     }
  if (oclAnnotationsHelper.hasImplementedUpdateAnnotations(genClass)) { hasOCL = true; 
    stringBuffer.append(TEXT_253);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_254);
     boolean isFirstUpdateAnnotation = true;
	 	for (GenFeature genFeature : genClass.getAllGenFeatures()) {
	 		EStructuralFeature eStructuralFeature = genFeature.getEcoreFeature();
			LinkedHashMap<String, String> featureAnnotations = oclAnnotationsHelper.findUpdateAnnotationParams(eStructuralFeature, genClass.getEcoreClass(), false);
			if (!featureAnnotations.isEmpty()) {
				for (Map.Entry<String, String> entry : featureAnnotations.entrySet()) {
					String updatedFeature = entry.getKey();
					String annotationText = entry.getValue();
					if (!isFirstUpdateAnnotation) {
			
    stringBuffer.append(TEXT_255);
    
					}
					isFirstUpdateAnnotation = false;
			
    stringBuffer.append(TEXT_256);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_257);
    stringBuffer.append(Literals.toStringLiteral(updatedFeature, genModel));
    stringBuffer.append(TEXT_258);
    stringBuffer.append(Literals.toStringLiteral(annotationText, genModel));
    stringBuffer.append(TEXT_259);
    
				}
			}
		} 
    stringBuffer.append(TEXT_260);
    }
  if (hasOCL || hasOverrideOCL) { 
    stringBuffer.append(TEXT_261);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL"));
    stringBuffer.append(TEXT_262);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL"));
    stringBuffer.append(TEXT_263);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory"));
    stringBuffer.append(TEXT_264);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.options.ParsingOptions"));
    stringBuffer.append(TEXT_265);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.options.ParsingOptions"));
    stringBuffer.append(TEXT_266);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EcorePackage"));
    stringBuffer.append(TEXT_267);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.options.EvaluationOptions"));
    stringBuffer.append(TEXT_268);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.options.EvaluationOptions"));
    stringBuffer.append(TEXT_269);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_270);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.ETypedElement"));
    stringBuffer.append(TEXT_271);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_272);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.ETypedElement"));
    stringBuffer.append(TEXT_273);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClassifier"));
    stringBuffer.append(TEXT_274);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.Variable"));
    stringBuffer.append(TEXT_275);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.EcoreFactory"));
    stringBuffer.append(TEXT_276);
     }
}
    if (isImplementation && genClass.hasOffsetCorrection() && !genClass.getImplementedGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_277);
    stringBuffer.append(genClass.getOffsetCorrectionField(null));
    stringBuffer.append(TEXT_278);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_279);
    stringBuffer.append(genClass.getImplementedGenFeatures().get(0).getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_280);
    stringBuffer.append(genClass.getQualifiedFeatureID(genClass.getImplementedGenFeatures().get(0)));
    stringBuffer.append(TEXT_281);
    }
    if (isImplementation && !genModel.isReflectiveDelegation()) {
    for (GenFeature genFeature : genClass.getImplementedGenFeatures()) { GenFeature reverseFeature = genFeature.getReverse();
    if (reverseFeature != null && reverseFeature.getGenClass().hasOffsetCorrection()) {
    stringBuffer.append(TEXT_282);
    stringBuffer.append(genClass.getOffsetCorrectionField(genFeature));
    stringBuffer.append(TEXT_283);
    stringBuffer.append(reverseFeature.getGenClass().getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_284);
    stringBuffer.append(reverseFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_285);
    stringBuffer.append(reverseFeature.getGenClass().getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(TEXT_286);
    }
    }
    }
    if (genModel.isOperationReflection() && isImplementation && genClass.hasOffsetCorrection() && !genClass.getImplementedGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_287);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_288);
    stringBuffer.append(genClass.getImplementedGenOperations().get(0).getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_289);
    stringBuffer.append(genClass.getQualifiedOperationID(genClass.getImplementedGenOperations().get(0)));
    stringBuffer.append(TEXT_290);
    }
    if (isImplementation) {
    stringBuffer.append(TEXT_291);
    if (genModel.isPublicConstructors()) {
    stringBuffer.append(TEXT_292);
    } else {
    stringBuffer.append(TEXT_293);
    }
    stringBuffer.append(TEXT_294);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_295);
    for (GenFeature genFeature : genClass.getFlagGenFeaturesWithDefault()) {
    stringBuffer.append(TEXT_296);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_297);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_298);
    if (!genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_299);
    }
    stringBuffer.append(TEXT_300);
    }
    if (oclAnnotationsHelper.isGenerateUpdateAnnotationCommon(genClass)) {
    stringBuffer.append(TEXT_301);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.impl.AdapterImpl"));
    stringBuffer.append(TEXT_302);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_303);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_304);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_305);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_306);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_307);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_308);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_309);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_310);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_311);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_312);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_313);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_314);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_315);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_316);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_317);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_318);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_319);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_320);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_321);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_322);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.ETypedElement"));
    stringBuffer.append(TEXT_323);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_324);
    stringBuffer.append(genClass.getClassName());
    stringBuffer.append(TEXT_325);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_326);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_327);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_328);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_329);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_330);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_331);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_332);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_333);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_334);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_335);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_336);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_337);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_338);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_339);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClassifier"));
    stringBuffer.append(TEXT_340);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.util.TypeUtil"));
    stringBuffer.append(TEXT_341);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_342);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_343);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_344);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_345);
    }
    stringBuffer.append(TEXT_346);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_347);
    }
    stringBuffer.append(TEXT_348);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_349);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_350);
    }
    if (isImplementation && (genModel.getFeatureDelegation() == GenDelegationKind.REFLECTIVE_LITERAL || genModel.isDynamicDelegation()) && (genClass.getClassExtendsGenClass() == null || (genClass.getClassExtendsGenClass().getGenModel().getFeatureDelegation() != GenDelegationKind.REFLECTIVE_LITERAL && !genClass.getClassExtendsGenClass().getGenModel().isDynamicDelegation()))) {
    stringBuffer.append(TEXT_351);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_352);
    }
    stringBuffer.append(TEXT_353);
    stringBuffer.append(genClass.getClassExtendsGenClass() == null ? 0 : genClass.getClassExtendsGenClass().getAllGenFeatures().size());
    stringBuffer.append(TEXT_354);
    }
    //Class/reflectiveDelegation.override.javajetinc
    new Runnable() { public void run() {
    for (GenFeature genFeature : (isImplementation ? genClass.getImplementedGenFeatures() : genClass.getDeclaredGenFeatures())) {
    if (genModel.isArrayAccessors() && genFeature.isListType() && !genFeature.isFeatureMapType() && !genFeature.isMapType()) {
    stringBuffer.append(TEXT_355);
    if (!isImplementation) {
    stringBuffer.append(TEXT_356);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_357);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_358);
    } else {
    stringBuffer.append(TEXT_359);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_360);
    stringBuffer.append(genFeature.getGetArrayAccessor());
    stringBuffer.append(TEXT_361);
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_362);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_363);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_364);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_365);
    if (genModel.useGenerics() && !genFeature.getListItemType(genClass).contains("<") && !genFeature.getListItemType(null).equals(genFeature.getListItemType(genClass))) {
    stringBuffer.append(TEXT_366);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_367);
    }
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_368);
    } else {
    stringBuffer.append(TEXT_369);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_370);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_371);
    if (genModel.useGenerics() && !genFeature.getListItemType(genClass).contains("<") && !genFeature.getListItemType(null).equals(genFeature.getListItemType(genClass))) {
    stringBuffer.append(TEXT_372);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_373);
    }
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_374);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_375);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_376);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_377);
    }
    stringBuffer.append(TEXT_378);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_379);
    }
    stringBuffer.append(TEXT_380);
    if (!isImplementation) {
    stringBuffer.append(TEXT_381);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_382);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_383);
    } else {
    stringBuffer.append(TEXT_384);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_385);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_386);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_387);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_388);
    }
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_389);
    }
    stringBuffer.append(TEXT_390);
    if (!isImplementation) {
    stringBuffer.append(TEXT_391);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_392);
    } else {
    stringBuffer.append(TEXT_393);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_394);
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_395);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_396);
    } else {
    stringBuffer.append(TEXT_397);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_398);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_399);
    }
    stringBuffer.append(TEXT_400);
    }
    stringBuffer.append(TEXT_401);
    if (!isImplementation) {
    stringBuffer.append(TEXT_402);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_403);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_404);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_405);
    } else {
    stringBuffer.append(TEXT_406);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_407);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_408);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_409);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(genFeature.getListTemplateArguments(genClass));
    stringBuffer.append(TEXT_410);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_411);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_412);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_413);
    }
    stringBuffer.append(TEXT_414);
    if (!isImplementation) {
    stringBuffer.append(TEXT_415);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_416);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_417);
    } else {
    stringBuffer.append(TEXT_418);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_419);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_420);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_421);
    }
    }
    if (genFeature.isGet() && (isImplementation || !genFeature.isSuppressedGetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_422);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_423);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_424);
    if (genFeature.isListType()) {
    if (genFeature.isMapType()) { GenFeature keyFeature = genFeature.getMapEntryTypeGenClass().getMapEntryKeyFeature(); GenFeature valueFeature = genFeature.getMapEntryTypeGenClass().getMapEntryValueFeature(); 
    stringBuffer.append(TEXT_425);
    if (keyFeature.isListType()) {
    stringBuffer.append(TEXT_426);
    stringBuffer.append(keyFeature.getQualifiedListItemType(genClass));
    stringBuffer.append(TEXT_427);
    } else {
    stringBuffer.append(TEXT_428);
    stringBuffer.append(keyFeature.getType(genClass));
    stringBuffer.append(TEXT_429);
    }
    stringBuffer.append(TEXT_430);
    if (valueFeature.isListType()) {
    stringBuffer.append(TEXT_431);
    stringBuffer.append(valueFeature.getQualifiedListItemType(genClass));
    stringBuffer.append(TEXT_432);
    } else {
    stringBuffer.append(TEXT_433);
    stringBuffer.append(valueFeature.getType(genClass));
    stringBuffer.append(TEXT_434);
    }
    stringBuffer.append(TEXT_435);
    } else if (!genFeature.isWrappedFeatureMapType() && !(genModel.isSuppressEMFMetaData() && "org.eclipse.emf.ecore.EObject".equals(genFeature.getQualifiedListItemType(genClass)))) {
String typeName = genFeature.getQualifiedListItemType(genClass); String head = typeName; String tail = ""; int index = typeName.indexOf('<'); if (index == -1) { index = typeName.indexOf('['); } 
if (index != -1) { head = typeName.substring(0, index); tail = typeName.substring(index).replaceAll("<", "&lt;"); }

    stringBuffer.append(TEXT_436);
    stringBuffer.append(head);
    stringBuffer.append(TEXT_437);
    stringBuffer.append(tail);
    stringBuffer.append(TEXT_438);
    }
    } else if (genFeature.isSetDefaultValue()) {
    stringBuffer.append(TEXT_439);
    stringBuffer.append(genFeature.getDefaultValue());
    stringBuffer.append(TEXT_440);
    }
    if (genFeature.getTypeGenEnum() != null) {
    stringBuffer.append(TEXT_441);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    stringBuffer.append(TEXT_442);
    }
    if (genFeature.isBidirectional() && !genFeature.getReverse().getGenClass().isMapEntry()) { GenFeature reverseGenFeature = genFeature.getReverse(); 
    if (!reverseGenFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_443);
    stringBuffer.append(reverseGenFeature.getGenClass().getQualifiedInterfaceName());
    stringBuffer.append(TEXT_444);
    stringBuffer.append(reverseGenFeature.getGetAccessor());
    stringBuffer.append(TEXT_445);
    stringBuffer.append(reverseGenFeature.getFormattedName());
    stringBuffer.append(TEXT_446);
    }
    }
    stringBuffer.append(TEXT_447);
    if (!genFeature.hasDocumentation()) {
    stringBuffer.append(TEXT_448);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_449);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_450);
    }
    stringBuffer.append(TEXT_451);
    if (genFeature.hasDocumentation()) {
    stringBuffer.append(TEXT_452);
    stringBuffer.append(genFeature.getDocumentation(genModel.getIndentation(stringBuffer)));
    stringBuffer.append(TEXT_453);
    }
    stringBuffer.append(TEXT_454);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_455);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_456);
    if (genFeature.getTypeGenEnum() != null) {
    stringBuffer.append(TEXT_457);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    }
    if (genFeature.isUnsettable()) {
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_458);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_459);
    }
    if (genFeature.isChangeable() && !genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_460);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_461);
    }
    }
    if (genFeature.isChangeable() && !genFeature.isListType() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_462);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_463);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_464);
    }
    if (!genModel.isSuppressEMFMetaData()) {
    stringBuffer.append(TEXT_465);
    stringBuffer.append(genPackage.getQualifiedPackageInterfaceName());
    stringBuffer.append(TEXT_466);
    stringBuffer.append(genFeature.getFeatureAccessorName());
    stringBuffer.append(TEXT_467);
    }
    if (genFeature.isBidirectional() && !genFeature.getReverse().getGenClass().isMapEntry()) { GenFeature reverseGenFeature = genFeature.getReverse(); 
    if (!reverseGenFeature.isSuppressedGetVisibility()) {
    stringBuffer.append(TEXT_468);
    stringBuffer.append(reverseGenFeature.getGenClass().getQualifiedInterfaceName());
    stringBuffer.append(TEXT_469);
    stringBuffer.append(reverseGenFeature.getGetAccessor());
    }
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genFeature.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_470);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_471);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_472);
    }}
    stringBuffer.append(TEXT_473);
    //Class/getGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_474);
    if (isJDK50) { //Class/getGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_475);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_476);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_477);
    } else {
    if (genModel.useGenerics() && ((genFeature.isContainer() || genFeature.isResolveProxies()) && !genFeature.isListType() && !(genModel.isReflectiveDelegation() && genModel.isDynamicDelegation()) && genFeature.isUncheckedCast(genClass) || genFeature.isListType() && !genFeature.isFeatureMapType() && (genModel.isReflectiveDelegation() || genModel.isVirtualDelegation() || genModel.isDynamicDelegation()) || genFeature.isListDataType() && genFeature.hasDelegateFeature() || genFeature.isListType() && genFeature.hasSettingDelegate())) {
    stringBuffer.append(TEXT_478);
    }
    stringBuffer.append(TEXT_479);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_480);
    stringBuffer.append(genFeature.getGetAccessor());
    if (genClass.hasCollidingGetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_481);
    }
    stringBuffer.append(TEXT_482);
    if (genModel.isDynamicDelegation() && !oclAnnotationsHelper.hasDeriveAnnotation(genFeature)) {
    stringBuffer.append(TEXT_483);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_484);
    }
    stringBuffer.append(TEXT_485);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_486);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_487);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_488);
    stringBuffer.append(!genFeature.isEffectiveSuppressEMFTypes());
    stringBuffer.append(TEXT_489);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_490);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_491);
    }
    stringBuffer.append(TEXT_492);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_493);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_494);
    }
    stringBuffer.append(TEXT_495);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_496);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_497);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_498);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_499);
    }
    stringBuffer.append(TEXT_500);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_501);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_502);
    }
    stringBuffer.append(TEXT_503);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_504);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_505);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_506);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_507);
    }
    stringBuffer.append(TEXT_508);
    } else if (!genFeature.isVolatile() && !oclAnnotationsHelper.hasDeriveAnnotation(genFeature)) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_509);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_510);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_511);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_512);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_513);
    }
    stringBuffer.append(TEXT_514);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_515);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_516);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_517);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_518);
    stringBuffer.append(genClass.getListConstructor(genFeature));
    stringBuffer.append(TEXT_519);
    } else {
    stringBuffer.append(TEXT_520);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_521);
    stringBuffer.append(genClass.getListConstructor(genFeature));
    stringBuffer.append(TEXT_522);
    }
    stringBuffer.append(TEXT_523);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes() ? ".map()" : "");
    stringBuffer.append(TEXT_524);
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_525);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_526);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_527);
    } else {
    if (genFeature.isResolveProxies()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_528);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_529);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_530);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_531);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_532);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_533);
    }
    stringBuffer.append(TEXT_534);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_535);
    stringBuffer.append(genFeature.getSafeNameAsEObject());
    stringBuffer.append(TEXT_536);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_537);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_538);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_539);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_540);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_541);
    stringBuffer.append(genFeature.getNonEObjectInternalTypeCast(genClass));
    stringBuffer.append(TEXT_542);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_543);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_544);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_545);
    if (genFeature.isEffectiveContains()) {
    stringBuffer.append(TEXT_546);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_547);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_548);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_549);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_550);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_551);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_552);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_553);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_554);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_555);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_556);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_557);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_558);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_559);
    }
    stringBuffer.append(TEXT_560);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_561);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_562);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_563);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_564);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_565);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_566);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_567);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_568);
    }
    stringBuffer.append(TEXT_569);
    } else if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_570);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_571);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_572);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_573);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_574);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_575);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_576);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_577);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_578);
    }
    stringBuffer.append(TEXT_579);
    }
    if (!genFeature.isResolveProxies() && genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_580);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_581);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_582);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_583);
    } else if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_584);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_585);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_586);
    } else {
    stringBuffer.append(TEXT_587);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_588);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_589);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_590);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_591);
    }
    } else {
    stringBuffer.append(TEXT_592);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_593);
    }
    }
    } else {//volatile
    if (genFeature.isResolveProxies() && !genFeature.isListType()) {
    stringBuffer.append(TEXT_594);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_595);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_596);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_597);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_598);
    stringBuffer.append(genFeature.getSafeNameAsEObject());
    stringBuffer.append(TEXT_599);
    stringBuffer.append(genFeature.getNonEObjectInternalTypeCast(genClass));
    stringBuffer.append(TEXT_600);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_601);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_602);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_603);
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (genFeature.isFeatureMapType()) {
    String featureMapEntryTemplateArgument = isJDK50 ? "<" + genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap") + ".Entry>" : "";
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_604);
    stringBuffer.append(genFeature.getImportedEffectiveFeatureMapWrapperClass());
    stringBuffer.append(TEXT_605);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_606);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_607);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_608);
    stringBuffer.append(featureMapEntryTemplateArgument);
    stringBuffer.append(TEXT_609);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_610);
    } else {
    stringBuffer.append(TEXT_611);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_612);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_613);
    stringBuffer.append(featureMapEntryTemplateArgument);
    stringBuffer.append(TEXT_614);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_615);
    }
    } else if (genFeature.isListType()) {
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_616);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_617);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_618);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_619);
    } else {
    stringBuffer.append(TEXT_620);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_621);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_622);
    }
    } else {
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_623);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_624);
    }
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType()) {
    stringBuffer.append(TEXT_625);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_626);
    }
    stringBuffer.append(TEXT_627);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_628);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_629);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_630);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_631);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_632);
    }
    stringBuffer.append(TEXT_633);
    } else {
    stringBuffer.append(TEXT_634);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_635);
    }
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType()) {
    stringBuffer.append(TEXT_636);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_637);
    }
    stringBuffer.append(TEXT_638);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_639);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_640);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_641);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_642);
    }
    stringBuffer.append(TEXT_643);
    }
    }
    } else if (genClass.getGetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_644);
    stringBuffer.append(genClass.getGetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    
String derive = null;
EStructuralFeature eFeature = genFeature.getEcoreFeature();
EClassifier eFeatureType = eFeature.getEType();
GenClassifier gFeatureType = genModel.findGenClassifier(eFeatureType);
GenPackage gFeatureTypePackage = gFeatureType.getGenPackage();
EAnnotation ocl = eFeature.getEAnnotation(oclNsURI);
if (ocl != null) derive = (String) ocl.getDetails().get("derive");
if (derive == null) { 
    stringBuffer.append(TEXT_645);
     } else { 
    stringBuffer.append(TEXT_646);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(derive));
    stringBuffer.append(TEXT_647);
    
	    final String expr = genFeature.getSafeName() + "DeriveOCL";
	    boolean override = false;
	    EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	    if (eAnnotation != null) {
	    	if (eAnnotation.getDetails().get(genFeature.getSafeName()+"Derive") != null) {
	    		override = true;
	    	}
	    }
	    
	    final String feature = override ? "eOverrideFeature" : "eFeature";
	    if (true) { // we will need the feature to create the EcoreEList 
    stringBuffer.append(TEXT_648);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_649);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_650);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_651);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_652);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_653);
     } 
    stringBuffer.append(TEXT_654);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_655);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_656);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_657);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_658);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_659);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_660);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_661);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_662);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_663);
     } else { 
    stringBuffer.append(TEXT_664);
     }
    stringBuffer.append(TEXT_665);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_666);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_667);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_668);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_669);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_670);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_671);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_672);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_673);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_674);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_675);
     if (genFeature.isListType()) { 
    stringBuffer.append(TEXT_676);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_677);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_678);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_679);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_680);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_681);
     if (eFeature instanceof org.eclipse.emf.ecore.EReference &&((org.eclipse.emf.ecore.EReference)eFeature).isContainment()) {
		  if (((org.eclipse.emf.ecore.EReference)eFeature).isMany()) { 
    stringBuffer.append(TEXT_682);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_683);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_684);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_685);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_686);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_687);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_688);
     } else {	
    stringBuffer.append(TEXT_689);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_690);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_691);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_692);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_693);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_694);
     } 
		}
    stringBuffer.append(TEXT_695);
     } else if (genFeature.isPrimitiveType()) { 
    stringBuffer.append(TEXT_696);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_697);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_698);
     } else { 
    stringBuffer.append(TEXT_699);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_700);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_701);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_702);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_703);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_704);
     if (eFeature instanceof org.eclipse.emf.ecore.EReference &&((org.eclipse.emf.ecore.EReference)eFeature).isContainment()) {
		     if (((org.eclipse.emf.ecore.EReference)eFeature).isMany()) {
    stringBuffer.append(TEXT_705);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_706);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_707);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_708);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_709);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_710);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_711);
     } else {	
    stringBuffer.append(TEXT_712);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_713);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_714);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_715);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_716);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_717);
     }
    stringBuffer.append(TEXT_718);
     }
    stringBuffer.append(TEXT_719);
     } 
    stringBuffer.append(TEXT_720);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_721);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_722);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_723);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_724);
     } else { 
    stringBuffer.append(TEXT_725);
     }
    stringBuffer.append(TEXT_726);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_727);
     } 
    //Class/getGenFeature.todo.override.javajetinc
    }
    }
    stringBuffer.append(TEXT_728);
    }
    //Class/getGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicGet()) {
    stringBuffer.append(TEXT_729);
    if (isJDK50) { //Class/basicGetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_730);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_731);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_732);
    if (genModel.isDynamicDelegation() && !oclAnnotationsHelper.hasDeriveAnnotation(genFeature)) {
    stringBuffer.append(TEXT_733);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_734);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_735);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_736);
    stringBuffer.append(!genFeature.isEffectiveSuppressEMFTypes());
    stringBuffer.append(TEXT_737);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_738);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_739);
    }
    stringBuffer.append(TEXT_740);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_741);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_742);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_743);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_744);
    }
    stringBuffer.append(TEXT_745);
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_746);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_747);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_748);
    } else if (!genFeature.isVolatile() && !oclAnnotationsHelper.hasDeriveAnnotation(genFeature)) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_749);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_750);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_751);
    } else {
    stringBuffer.append(TEXT_752);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_753);
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_754);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_755);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_756);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_757);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_758);
    } else {
    stringBuffer.append(TEXT_759);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_760);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_761);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_762);
    }
    } else {
    // same body as for multivalued derived feature 
    
String derive = null;
EStructuralFeature eFeature = genFeature.getEcoreFeature();
EClassifier eFeatureType = eFeature.getEType();
GenClassifier gFeatureType = genModel.findGenClassifier(eFeatureType);
GenPackage gFeatureTypePackage = gFeatureType.getGenPackage();
EAnnotation ocl = eFeature.getEAnnotation(oclNsURI);
if (ocl != null) derive = (String) ocl.getDetails().get("derive");
if (derive == null) { 
    stringBuffer.append(TEXT_763);
     } else { 
    stringBuffer.append(TEXT_764);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(derive));
    stringBuffer.append(TEXT_765);
    
	    final String expr = genFeature.getSafeName() + "DeriveOCL";
	    boolean override = false;
	    EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
	    if (eAnnotation != null) {
	    	if (eAnnotation.getDetails().get(genFeature.getSafeName()+"Derive") != null) {
	    		override = true;
	    	}
	    }
	    
	    final String feature = override ? "eOverrideFeature" : "eFeature";
	    if (true) { // we will need the feature to create the EcoreEList 
    stringBuffer.append(TEXT_766);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_767);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_768);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_769);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_770);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_771);
     } 
    stringBuffer.append(TEXT_772);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_773);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_774);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_775);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_776);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_777);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_778);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_779);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_780);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_781);
     } else { 
    stringBuffer.append(TEXT_782);
     }
    stringBuffer.append(TEXT_783);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_784);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_785);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_786);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_787);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_788);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_789);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_790);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_791);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_792);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_793);
     if (genFeature.isListType()) { 
    stringBuffer.append(TEXT_794);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_795);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_796);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_797);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_798);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_799);
     if (eFeature instanceof org.eclipse.emf.ecore.EReference &&((org.eclipse.emf.ecore.EReference)eFeature).isContainment()) {
		  if (((org.eclipse.emf.ecore.EReference)eFeature).isMany()) { 
    stringBuffer.append(TEXT_800);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_801);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_802);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_803);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_804);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_805);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_806);
     } else {	
    stringBuffer.append(TEXT_807);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_808);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_809);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_810);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_811);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_812);
     } 
		}
    stringBuffer.append(TEXT_813);
     } else if (genFeature.isPrimitiveType()) { 
    stringBuffer.append(TEXT_814);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_815);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_816);
     } else { 
    stringBuffer.append(TEXT_817);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_818);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_819);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_820);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_821);
    stringBuffer.append(feature);
    stringBuffer.append(TEXT_822);
     if (eFeature instanceof org.eclipse.emf.ecore.EReference &&((org.eclipse.emf.ecore.EReference)eFeature).isContainment()) {
		     if (((org.eclipse.emf.ecore.EReference)eFeature).isMany()) {
    stringBuffer.append(TEXT_823);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_824);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_825);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_826);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_827);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_828);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_829);
     } else {	
    stringBuffer.append(TEXT_830);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_831);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.BasicEObjectImpl"));
    stringBuffer.append(TEXT_832);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_833);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_834);
    stringBuffer.append(genClass.getFeatureID(genFeature));
    stringBuffer.append(TEXT_835);
     }
    stringBuffer.append(TEXT_836);
     }
    stringBuffer.append(TEXT_837);
     } 
    stringBuffer.append(TEXT_838);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_839);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_840);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_841);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_842);
     } else { 
    stringBuffer.append(TEXT_843);
     }
    stringBuffer.append(TEXT_844);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_845);
     } 
    //Class/basicGetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_846);
    //Class/basicGetGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_847);
    if (isJDK50) { //Class/basicSetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_848);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_849);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_850);
    stringBuffer.append(genFeature.getImportedInternalType(genClass));
    stringBuffer.append(TEXT_851);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_852);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_853);
    if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_854);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_855);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_856);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_857);
    stringBuffer.append(TEXT_858);
    } else if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_859);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_860);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_861);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_862);
    stringBuffer.append(TEXT_863);
    } else if (!genFeature.isVolatile()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_864);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_865);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_866);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_867);
    } else {
    stringBuffer.append(TEXT_868);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_869);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_870);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_871);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_872);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_873);
    }
    if (genFeature.isUnsettable()) {
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_874);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_875);
    }
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_876);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_877);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_878);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_879);
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_880);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_881);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_882);
    }
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_883);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_884);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_885);
    }
    stringBuffer.append(TEXT_886);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_887);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_888);
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_889);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_890);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_891);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_892);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_893);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_894);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_895);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_896);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_897);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_898);
    } else {
    stringBuffer.append(TEXT_899);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_900);
    }
    stringBuffer.append(TEXT_901);
    } else {
    stringBuffer.append(TEXT_902);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_903);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_904);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_905);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_906);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_907);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_908);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_909);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_910);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_911);
    }
    stringBuffer.append(TEXT_912);
    }
    stringBuffer.append(TEXT_913);
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_914);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_915);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_916);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_917);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_918);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_919);
    } else {
    stringBuffer.append(TEXT_920);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_921);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_922);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_923);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_924);
    }
    } else {
    if (genFeature.isDerived()) {
    stringBuffer.append(TEXT_925);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_926);
    } else {
    stringBuffer.append(TEXT_927);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_928);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_929);
    }
    //Class/basicSetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_930);
    //Class/basicSetGenFeature.override.javajetinc
    }
    if (genFeature.isSet() && (isImplementation || !genFeature.isSuppressedSetVisibility())) {
    if (isInterface) { 
    stringBuffer.append(TEXT_931);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_932);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_933);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_934);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_935);
    if (genFeature.isDerived() && genFeature.isChangeable()) {
    stringBuffer.append(TEXT_936);
    }
    stringBuffer.append(TEXT_937);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_938);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_939);
    if (genFeature.isEnumType()) {
    stringBuffer.append(TEXT_940);
    stringBuffer.append(genFeature.getTypeGenEnum().getQualifiedName());
    }
    if (genFeature.isUnsettable()) {
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_941);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_942);
    }
    if (!genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_943);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_944);
    }
    }
    stringBuffer.append(TEXT_945);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_946);
    } else {
    stringBuffer.append(TEXT_947);
    if (genFeature.isDerived() && genFeature.isChangeable()) {
    stringBuffer.append(TEXT_948);
    }
    stringBuffer.append(TEXT_949);
    if (isJDK50) { //Class/setGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) { 
    stringBuffer.append(TEXT_950);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_951);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_952);
    } else { GenOperation setAccessorOperation = genClass.getSetAccessorOperation(genFeature);
    stringBuffer.append(TEXT_953);
    stringBuffer.append(genFeature.getAccessorName());
    if (genClass.hasCollidingSetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_954);
    }
    stringBuffer.append(TEXT_955);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_956);
    stringBuffer.append(setAccessorOperation == null ? "new" + genFeature.getCapName() : setAccessorOperation.getGenParameters().get(0).getName());
    stringBuffer.append(TEXT_957);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_958);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_959);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_960);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_961);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_962);
    }
    stringBuffer.append(TEXT_963);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_964);
    }
    stringBuffer.append(TEXT_965);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_966);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_967);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_968);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_969);
    }
    stringBuffer.append(TEXT_970);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_971);
    }
    stringBuffer.append(TEXT_972);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_973);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_974);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_975);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_976);
    }
    stringBuffer.append(TEXT_977);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_978);
    }
    stringBuffer.append(TEXT_979);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isContainer()) { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_980);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_981);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_982);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_983);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EcoreUtil"));
    stringBuffer.append(TEXT_984);
    stringBuffer.append(genFeature.getEObjectCast());
    stringBuffer.append(TEXT_985);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_986);
    stringBuffer.append(genModel.getImportedName("java.lang.IllegalArgumentException"));
    stringBuffer.append(TEXT_987);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_988);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_989);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_990);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_991);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_992);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_993);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_994);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_995);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_996);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_997);
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_998);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_999);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1000);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1001);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1002);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1003);
    }
    } else if (genFeature.isBidirectional() || genFeature.isEffectiveContains()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1004);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1005);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1006);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1007);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1008);
    }
    stringBuffer.append(TEXT_1009);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1010);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1011);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1012);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1013);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_1014);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1015);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1016);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1017);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1018);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1019);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1020);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1021);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_1022);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1023);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1024);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_1025);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_1026);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1027);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1028);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1029);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_1030);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_1031);
    }
    stringBuffer.append(TEXT_1032);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1033);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_1034);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1035);
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_1036);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1037);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1038);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1039);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1040);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1041);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1042);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1043);
    }
    stringBuffer.append(TEXT_1044);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1045);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1046);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1047);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1048);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1049);
    }
    stringBuffer.append(TEXT_1050);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1051);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1052);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1053);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1054);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1055);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1056);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1057);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1058);
    }
    stringBuffer.append(TEXT_1059);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1060);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1061);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1062);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1063);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1064);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1065);
    }
    }
    } else {
    if (genClass.isFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_1066);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1067);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1068);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1069);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1070);
    } else {
    stringBuffer.append(TEXT_1071);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1072);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1073);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1074);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1075);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1076);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1077);
    }
    }
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_1078);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1079);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1080);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1081);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1082);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1083);
    } else {
    stringBuffer.append(TEXT_1084);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1085);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1086);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1087);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1088);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1089);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1090);
    if (isJDK50) {
    stringBuffer.append(TEXT_1091);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1092);
    } else {
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1093);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1094);
    }
    stringBuffer.append(TEXT_1095);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1096);
    }
    } else {
    if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1097);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1098);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1099);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1100);
    }
    }
    if (genFeature.isEnumType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1101);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1102);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1103);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1104);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1105);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1106);
    } else {
    stringBuffer.append(TEXT_1107);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1108);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1109);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1110);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1111);
    }
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1112);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1113);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1114);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_1115);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1116);
    } else {
    stringBuffer.append(TEXT_1117);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1118);
    stringBuffer.append(genFeature.getInternalTypeCast());
    stringBuffer.append(TEXT_1119);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1120);
    }
    }
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1121);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1122);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1123);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1124);
    }
    }
    if (genFeature.isUnsettable()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1125);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1126);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1127);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1128);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1129);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1130);
    }
    stringBuffer.append(TEXT_1131);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1132);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1133);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1134);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1135);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1136);
    }
    stringBuffer.append(TEXT_1137);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1138);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1139);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1140);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1141);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1142);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1143);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1144);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_1145);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_1146);
    if (genClass.isFlag(genFeature)) {
    stringBuffer.append(TEXT_1147);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(genFeature.getSafeName());
    }
    stringBuffer.append(TEXT_1148);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1149);
    } else {
    stringBuffer.append(TEXT_1150);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1151);
    }
    stringBuffer.append(TEXT_1152);
    }
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1153);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1154);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1155);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1156);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1157);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1158);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1159);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(TEXT_1160);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_1161);
    if (genClass.isFlag(genFeature)) {
    stringBuffer.append(TEXT_1162);
    stringBuffer.append(genFeature.getCapName());
    } else {
    stringBuffer.append(genFeature.getSafeName());
    }
    stringBuffer.append(TEXT_1163);
    }
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1164);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1165);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1166);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1167);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1168);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1169);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_1170);
    }
    stringBuffer.append(TEXT_1171);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1172);
    }
    stringBuffer.append(TEXT_1173);
    } else {
    stringBuffer.append(TEXT_1174);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1175);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1176);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1177);
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1178);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_1179);
    }
    stringBuffer.append(TEXT_1180);
    stringBuffer.append(genFeature.getCapName());
    if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1181);
    }
    stringBuffer.append(TEXT_1182);
    }
    } else if (setAccessorOperation != null) {
    stringBuffer.append(TEXT_1183);
    stringBuffer.append(setAccessorOperation.getBody(genModel.getIndentation(stringBuffer)));
    } else {
    if (genFeature.isDerived() && genFeature.isChangeable()) {
    stringBuffer.append(TEXT_1184);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1185);
    }
    //Class/setGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_1186);
    }
    //Class/setGenFeature.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genFeature.isBasicUnset()) {
    stringBuffer.append(TEXT_1187);
    if (isJDK50) { //Class/basicUnsetGenFeature.annotations.insert.javajetinc
    }
    stringBuffer.append(TEXT_1188);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1189);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1190);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1191);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_1192);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1193);
    if (genFeature.isResolveProxies()) {
    stringBuffer.append(TEXT_1194);
    stringBuffer.append(genFeature.getAccessorName());
    } else {
    stringBuffer.append(genFeature.getGetAccessor());
    }
    stringBuffer.append(TEXT_1195);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1196);
    } else if (!genFeature.isVolatile()) {
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1197);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1198);
    }
    stringBuffer.append(TEXT_1199);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1200);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1201);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1202);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1203);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1204);
    }
    stringBuffer.append(TEXT_1205);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1206);
    }
    if (genModel.isVirtualDelegation()) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1207);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1208);
    }
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1209);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1210);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1211);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1212);
    }
    stringBuffer.append(TEXT_1213);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1214);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1215);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1216);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1217);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1218);
    }
    stringBuffer.append(TEXT_1219);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1220);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1221);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1222);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1223);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1224);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1225);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1226);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1227);
    } else {
    stringBuffer.append(TEXT_1228);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_1229);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1230);
    } else {
    stringBuffer.append(TEXT_1231);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1232);
    }
    stringBuffer.append(TEXT_1233);
    }
    } else {
    stringBuffer.append(TEXT_1234);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1235);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1236);
    //Class/basicUnsetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_1237);
    //Class.basicUnsetGenFeature.override.javajetinc
    }
    if (genFeature.isUnset() && (isImplementation || !genFeature.isSuppressedUnsetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_1238);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_1239);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1240);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1241);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1242);
    stringBuffer.append(TEXT_1243);
    if (!genFeature.isSuppressedIsSetVisibility()) {
    stringBuffer.append(TEXT_1244);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1245);
    }
    stringBuffer.append(TEXT_1246);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1247);
    if (!genFeature.isListType() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_1248);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1249);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_1250);
    }
    stringBuffer.append(TEXT_1251);
    //Class/unsetGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_1252);
    if (isJDK50) { //Class/unsetGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_1253);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1254);
    } else {
    stringBuffer.append(TEXT_1255);
    stringBuffer.append(genFeature.getAccessorName());
    if (genClass.hasCollidingUnsetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_1256);
    }
    stringBuffer.append(TEXT_1257);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_1258);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1259);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1260);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_1261);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1262);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_1263);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1264);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1265);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1266);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1267);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1268);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1269);
    }
    stringBuffer.append(TEXT_1270);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1271);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(TEXT_1272);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_1273);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1274);
    } else if (genFeature.isBidirectional() || genFeature.isEffectiveContains()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1275);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1276);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1277);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1278);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1279);
    }
    stringBuffer.append(TEXT_1280);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1281);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1282);
    if (!genFeature.isBidirectional()) {
    stringBuffer.append(TEXT_1283);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1284);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1285);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1286);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_1287);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1288);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1289);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_1290);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_1291);
    }
    stringBuffer.append(TEXT_1292);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1293);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1294);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1295);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1296);
    } else if (genClass.isESetFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1297);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1298);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1299);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1300);
    }
    stringBuffer.append(TEXT_1301);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1302);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1303);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1304);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1305);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1306);
    }
    stringBuffer.append(TEXT_1307);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1308);
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1309);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1310);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1311);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1312);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1313);
    }
    stringBuffer.append(TEXT_1314);
    } else {
    if (genClass.isFlag(genFeature)) {
    if (!genModel.isSuppressNotification()) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_1315);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1316);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1317);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1318);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1319);
    } else {
    stringBuffer.append(TEXT_1320);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1321);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1322);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1323);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1324);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1325);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1326);
    }
    }
    } else if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1327);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1328);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1329);
    } else {
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1330);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1331);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1332);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1333);
    }
    }
    if (!genModel.isSuppressNotification()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1334);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1335);
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_1336);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1337);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1338);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1339);
    } else {
    stringBuffer.append(TEXT_1340);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1341);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1342);
    }
    }
    if (genFeature.isReferenceType()) {
    stringBuffer.append(TEXT_1343);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1344);
    if (!genModel.isVirtualDelegation()) {
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_1345);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1346);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1347);
    } else {
    stringBuffer.append(TEXT_1348);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1349);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1350);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1351);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1352);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1353);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1354);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1355);
    } else {
    stringBuffer.append(TEXT_1356);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_1357);
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1358);
    } else {
    stringBuffer.append(TEXT_1359);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1360);
    }
    stringBuffer.append(TEXT_1361);
    }
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_1362);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1363);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1364);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1365);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1366);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1367);
    } else {
    stringBuffer.append(TEXT_1368);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1369);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_1370);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1371);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1372);
    }
    } else if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1373);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1374);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1375);
    }
    if (!genModel.isVirtualDelegation() || genFeature.isPrimitiveType()) {
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_1376);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1377);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1378);
    } else {
    stringBuffer.append(TEXT_1379);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1380);
    }
    }
    if (!genModel.isSuppressNotification()) {
    stringBuffer.append(TEXT_1381);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.impl.ENotificationImpl"));
    stringBuffer.append(TEXT_1382);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.Notification"));
    stringBuffer.append(TEXT_1383);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1384);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1385);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1386);
    stringBuffer.append(genFeature.getEDefault());
    } else {
    stringBuffer.append(TEXT_1387);
    stringBuffer.append(genFeature.getCapName());
    }
    stringBuffer.append(TEXT_1388);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1389);
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1390);
    } else {
    stringBuffer.append(TEXT_1391);
    stringBuffer.append(genFeature.getCapName());
    stringBuffer.append(TEXT_1392);
    }
    stringBuffer.append(TEXT_1393);
    }
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1394);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1395);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1396);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1397);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1398);
    } else {
    stringBuffer.append(TEXT_1399);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1400);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1401);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1402);
    }
    } else if (genClass.getUnsetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_1403);
    stringBuffer.append(genClass.getUnsetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_1404);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1405);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1406);
    //Class/unsetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_1407);
    }
    //Class/unsetGenFeature.override.javajetinc
    }
    if (genFeature.isIsSet() && (isImplementation || !genFeature.isSuppressedIsSetVisibility())) {
    if (isInterface) {
    stringBuffer.append(TEXT_1408);
    stringBuffer.append(genClass.getQualifiedInterfaceName());
    stringBuffer.append(TEXT_1409);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1410);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1411);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1412);
    stringBuffer.append(TEXT_1413);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1414);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1415);
    if (genFeature.isChangeable() && !genFeature.isSuppressedUnsetVisibility()) {
    stringBuffer.append(TEXT_1416);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1417);
    }
    stringBuffer.append(TEXT_1418);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1419);
    if (!genFeature.isListType() && genFeature.isChangeable() && !genFeature.isSuppressedSetVisibility()) {
    stringBuffer.append(TEXT_1420);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1421);
    stringBuffer.append(genFeature.getRawImportedBoundType());
    stringBuffer.append(TEXT_1422);
    }
    stringBuffer.append(TEXT_1423);
    //Class/isSetGenFeature.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_1424);
    if (isJDK50) { //Class/isSetGenFeature.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_1425);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1426);
    } else {
    stringBuffer.append(TEXT_1427);
    stringBuffer.append(genFeature.getAccessorName());
    if (genClass.hasCollidingIsSetAccessorOperation(genFeature)) {
    stringBuffer.append(TEXT_1428);
    }
    stringBuffer.append(TEXT_1429);
    if (genModel.isDynamicDelegation()) {
    stringBuffer.append(TEXT_1430);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1431);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1432);
    } else if (genModel.isReflectiveDelegation()) {
    stringBuffer.append(TEXT_1433);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1434);
    } else if (genFeature.hasSettingDelegate()) {
    stringBuffer.append(TEXT_1435);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1436);
    } else if (!genFeature.isVolatile()) {
    if (genFeature.isListType()) {
    if (genModel.isVirtualDelegation()) {
    stringBuffer.append(TEXT_1437);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1438);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1439);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1440);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1441);
    }
    stringBuffer.append(TEXT_1442);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1443);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(TEXT_1444);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_1445);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1446);
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1447);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1448);
    } else if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_1449);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_1450);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1451);
    } else {
    stringBuffer.append(TEXT_1452);
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_1453);
    }
    }
    } else if (genFeature.hasDelegateFeature()) { GenFeature delegateFeature = genFeature.getDelegateFeature();
    if (delegateFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1454);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1455);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1456);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1457);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1458);
    } else {
    stringBuffer.append(TEXT_1459);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1460);
    stringBuffer.append(delegateFeature.getAccessorName());
    stringBuffer.append(TEXT_1461);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1462);
    }
    } else if (genClass.getIsSetAccessorOperation(genFeature) != null) {
    stringBuffer.append(TEXT_1463);
    stringBuffer.append(genClass.getIsSetAccessorOperation(genFeature).getBody(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_1464);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1465);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1466);
    //Class/isSetGenFeature.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_1467);
    }
    //Class/isSetGenFeature.override.javajetinc
    }
    
if (isImplementation) {
//ADDED www.xocl.org
boolean hasChoiceConstraintOCL = false;
boolean hasChoiceConstructionOCL = false;
boolean hasExpressionOCL = false;
String choiceConstraint = null;
String choiceConstruction = null;

EStructuralFeature eReference = genFeature.getEcoreFeature();
EAnnotation ocl= eReference.getEAnnotation(oclNsURI);
if (ocl != null) {
	choiceConstraint = ocl.getDetails().get("choiceConstraint");
	choiceConstruction = ocl.getDetails().get("choiceConstruction");
}
boolean[] override = new boolean[] {false, false}; EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
if (eAnnotation != null) {
	if (eAnnotation.getDetails().get(genFeature.getSafeName()+"ChoiceConstraint") != null) { override[0] = true; }
	if (eAnnotation.getDetails().get(genFeature.getSafeName()+"ChoiceConstruction") != null) { override[1] = true; }
}
if (!override[0] && choiceConstraint != null) {hasChoiceConstraintOCL = true;}
if (!override[1] && choiceConstruction != null) {hasChoiceConstructionOCL = true;}

if (genFeature.getEcoreFeature() instanceof EAttribute) {
    EAnnotation expressionOcl = ((EAttribute)genFeature.getEcoreFeature()).getEAnnotation(expressionOclNsURI);
    hasExpressionOCL = expressionOcl != null;
}

if (hasChoiceConstraintOCL){
    stringBuffer.append(TEXT_1468);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1469);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1470);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1471);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(choiceConstraint));
    stringBuffer.append(TEXT_1472);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1473);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1474);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_1475);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1476);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1477);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EReference"));
    stringBuffer.append(TEXT_1478);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1479);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_1480);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1481);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_1482);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1483);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1484);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1485);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_1486);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1487);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1488);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1489);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1490);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_1491);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1492);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1493);
    
} 

if (hasChoiceConstructionOCL){
    stringBuffer.append(TEXT_1494);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1495);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1496);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_1497);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1498);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(choiceConstruction));
    stringBuffer.append(TEXT_1499);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_1500);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1501);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1502);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_1503);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1504);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_1505);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1506);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1507);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.Variable"));
    stringBuffer.append(TEXT_1508);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.EcoreFactory"));
    stringBuffer.append(TEXT_1509);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_1510);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_1511);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_1512);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1513);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_1514);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1515);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1516);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1517);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_1518);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1519);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1520);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1521);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1522);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_1523);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1524);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_1525);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1526);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_1527);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_1528);
     if (eReference.isRequired()) { 
    stringBuffer.append(TEXT_1529);
     } 
    stringBuffer.append(TEXT_1530);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_1531);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1532);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1533);
    
}
if (hasExpressionOCL){
    stringBuffer.append(TEXT_1534);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_1535);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_1536);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.expr.OCLExpressionContext"));
    stringBuffer.append(TEXT_1537);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1538);
    
}

}

    stringBuffer.append(TEXT_1539);
    //Class/genFeature.override.javajetinc
    }//for
    }}.run();
    for (GenOperation genOperation : (isImplementation ? oclAnnotationsHelper.getImplementedGenOperations(genClass) : genClass.getDeclaredGenOperations())) {
    if (isImplementation) {
    if (genOperation.isInvariant() && genOperation.hasInvariantExpression()) {
    stringBuffer.append(TEXT_1540);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1541);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1542);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_1543);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1544);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1545);
    stringBuffer.append(genModel.getImportedName("java.lang.String"));
    stringBuffer.append(TEXT_1546);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_1547);
    stringBuffer.append(genOperation.getInvariantExpression("\t\t"));
    stringBuffer.append(TEXT_1548);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_1549);
    } else if (genOperation.hasInvocationDelegate()) {
    stringBuffer.append(TEXT_1550);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1551);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1552);
    stringBuffer.append(genOperation.getFormattedName());
    stringBuffer.append(TEXT_1553);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1554);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1555);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_1556);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_1557);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_1558);
    stringBuffer.append(genOperation.getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_1559);
    }
    }
    if (isInterface) {
    stringBuffer.append(TEXT_1560);
    stringBuffer.append(TEXT_1561);
    if (genOperation.hasDocumentation() || genOperation.hasParameterDocumentation()) {
    stringBuffer.append(TEXT_1562);
    if (genOperation.hasDocumentation()) {
    stringBuffer.append(TEXT_1563);
    stringBuffer.append(genOperation.getDocumentation(genModel.getIndentation(stringBuffer)));
    }
    for (GenParameter genParameter : genOperation.getGenParameters()) {
    if (genParameter.hasDocumentation()) { String documentation = genParameter.getDocumentation("");
    if (documentation.contains("\n") || documentation.contains("\r")) {
    stringBuffer.append(TEXT_1564);
    stringBuffer.append(genParameter.getName());
    stringBuffer.append(TEXT_1565);
    stringBuffer.append(genParameter.getDocumentation(genModel.getIndentation(stringBuffer)));
    } else {
    stringBuffer.append(TEXT_1566);
    stringBuffer.append(genParameter.getName());
    stringBuffer.append(TEXT_1567);
    stringBuffer.append(genParameter.getDocumentation(genModel.getIndentation(stringBuffer)));
    }
    }
    }
    stringBuffer.append(TEXT_1568);
    }
    if (!genModel.isSuppressEMFModelTags()) { boolean first = true; for (StringTokenizer stringTokenizer = new StringTokenizer(genOperation.getModelInfo(), "\n\r"); stringTokenizer.hasMoreTokens(); ) { String modelInfo = stringTokenizer.nextToken(); if (first) { first = false;
    stringBuffer.append(TEXT_1569);
    stringBuffer.append(modelInfo);
    } else {
    stringBuffer.append(TEXT_1570);
    stringBuffer.append(modelInfo);
    }} if (first) {
    stringBuffer.append(TEXT_1571);
    }}
    stringBuffer.append(TEXT_1572);
    //Class/genOperation.javadoc.override.javajetinc
    } else {
    stringBuffer.append(TEXT_1573);
    if (isJDK50) { //Class/genOperation.annotations.insert.javajetinc
    }
    }
    if (!isImplementation) {
    stringBuffer.append(TEXT_1574);
    stringBuffer.append(genOperation.getTypeParameters(genClass));
    stringBuffer.append(genOperation.getImportedType(genClass));
    stringBuffer.append(TEXT_1575);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1576);
    stringBuffer.append(genOperation.getParameters(genClass));
    stringBuffer.append(TEXT_1577);
    stringBuffer.append(genOperation.getThrows(genClass));
    stringBuffer.append(TEXT_1578);
    } else {
    if (genModel.useGenerics() && !genOperation.hasBody() && !genOperation.isInvariant() && genOperation.hasInvocationDelegate() && genOperation.isUncheckedCast(genClass)) {
    stringBuffer.append(TEXT_1579);
    }
    stringBuffer.append(TEXT_1580);
    stringBuffer.append(genOperation.getTypeParameters(genClass));
    stringBuffer.append(genOperation.getImportedType(genClass));
    stringBuffer.append(TEXT_1581);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1582);
    stringBuffer.append(genOperation.getParameters(genClass));
    stringBuffer.append(TEXT_1583);
    stringBuffer.append(genOperation.getThrows(genClass));
    stringBuffer.append(TEXT_1584);
    if (genOperation.hasBody()) {
    stringBuffer.append(TEXT_1585);
    stringBuffer.append(genOperation.getBody(genModel.getIndentation(stringBuffer)));
    } else if (genOperation.isInvariant()) {GenClass opClass = genOperation.getGenClass(); String diagnostics = genOperation.getGenParameters().get(0).getName(); String context = genOperation.getGenParameters().get(1).getName();
    if (genOperation.hasInvariantExpression()) {
    stringBuffer.append(TEXT_1586);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_1587);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1588);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_1589);
    stringBuffer.append(context);
    stringBuffer.append(TEXT_1590);
    stringBuffer.append(genOperation.getValidationDelegate());
    stringBuffer.append(TEXT_1591);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_1592);
    stringBuffer.append(genOperation.getQualifiedOperationAccessor());
    stringBuffer.append(TEXT_1593);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_1594);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.Diagnostic"));
    stringBuffer.append(TEXT_1595);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_1596);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_1597);
    stringBuffer.append(opClass.getOperationID(genOperation));
    stringBuffer.append(TEXT_1598);
    } else {
    stringBuffer.append(TEXT_1599);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_1600);
    stringBuffer.append(diagnostics);
    stringBuffer.append(TEXT_1601);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicDiagnostic"));
    stringBuffer.append(TEXT_1602);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.Diagnostic"));
    stringBuffer.append(TEXT_1603);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_1604);
    stringBuffer.append(opClass.getGenPackage().getImportedValidatorClassName());
    stringBuffer.append(TEXT_1605);
    stringBuffer.append(opClass.getOperationID(genOperation));
    stringBuffer.append(TEXT_1606);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.plugin.EcorePlugin"));
    stringBuffer.append(TEXT_1607);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_1608);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.EObjectValidator"));
    stringBuffer.append(TEXT_1609);
    stringBuffer.append(context);
    stringBuffer.append(TEXT_1610);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(TEXT_1611);
    }
    } else if (genOperation.hasInvocationDelegate()) { int size = genOperation.getGenParameters().size();
    stringBuffer.append(TEXT_1612);
    if (genOperation.isVoid()) {
    stringBuffer.append(TEXT_1613);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_1614);
    if (size > 0) {
    stringBuffer.append(TEXT_1615);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_1616);
    stringBuffer.append(size);
    stringBuffer.append(TEXT_1617);
    stringBuffer.append(genOperation.getParametersArray(genClass));
    stringBuffer.append(TEXT_1618);
    } else {
    stringBuffer.append(TEXT_1619);
    }
    stringBuffer.append(TEXT_1620);
    } else {
    stringBuffer.append(TEXT_1621);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_1622);
    }
    stringBuffer.append(TEXT_1623);
    stringBuffer.append(genOperation.getObjectType(genClass));
    stringBuffer.append(TEXT_1624);
    stringBuffer.append(CodeGenUtil.upperName(genClass.getUniqueName(genOperation), genModel.getLocale()));
    stringBuffer.append(TEXT_1625);
    if (size > 0) {
    stringBuffer.append(TEXT_1626);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.BasicEList"));
    stringBuffer.append(TEXT_1627);
    stringBuffer.append(size);
    stringBuffer.append(TEXT_1628);
    stringBuffer.append(genOperation.getParametersArray(genClass));
    stringBuffer.append(TEXT_1629);
    } else {
    stringBuffer.append(TEXT_1630);
    }
    stringBuffer.append(TEXT_1631);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_1632);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_1633);
    }
    stringBuffer.append(TEXT_1634);
    }
    stringBuffer.append(TEXT_1635);
    stringBuffer.append(genModel.getImportedName(isGWT ? "org.eclipse.emf.common.util.InvocationTargetException" : "java.lang.reflect.InvocationTargetException"));
    stringBuffer.append(TEXT_1636);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.WrappedException"));
    stringBuffer.append(TEXT_1637);
    } else {
    stringBuffer.append(TEXT_1638);
    
String body = null;
EOperation eOperation = genOperation.getEcoreOperation();
EAnnotation ocl = eOperation.getEAnnotation(oclNsURI);
if (ocl != null) body = ocl.getDetails().get("body");
boolean override = false;
if (body == null) {
    EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
    if (eAnnotation != null && (body = eAnnotation.getDetails().get(genOperation.getName()+"Body")) != null) { override = true; }
}
if (body == null) { 
    stringBuffer.append(TEXT_1639);
     } else if (genOperation.getImportedType(genClass).equals("XUpdate") && genOperation.getName().contains("$Update")){

String featureName = genOperation.getName().replace("$Update","");
GenFeature generatedDerivedChangeableFeat = null;
GenFeature innerValueOfDerivedFeat = null;

for(GenFeature genFeat : genClass.getAllGenFeatures())
{
	if(genFeat.getName().equals(featureName) && genFeat.isDerived() && genFeat.isChangeable())
	{
		generatedDerivedChangeableFeat = genFeat;
		break;
		}
}

if (generatedDerivedChangeableFeat != null)
{
for(GenFeature genFeat : genClass.getAllGenFeatures())
{
   if((genFeat != generatedDerivedChangeableFeat) && (genFeat.getName().equals(generatedDerivedChangeableFeat.getName().concat("$InnerValue"))))
   {
       if (genFeat.isChangeable() && genFeat.getProperty() == GenPropertyKind.READONLY_LITERAL)
       {
       innerValueOfDerivedFeat = genFeat;
       break;
       }
   }
}
}
   
EAnnotation oclUpdateAnnotation = generatedDerivedChangeableFeat.getEcoreFeature().getEAnnotation(XoclAnnotationHelper.oclNsURI);
ArrayList<String> updateNames = new ArrayList<String>();
for (String keyString : oclUpdateAnnotation.getDetails().keySet())
		if(keyString.endsWith("Object"))
			updateNames.add(keyString.replace("Object", ""));

HashMap<String, String[]> parameterHashMap = new HashMap<String, String[]>();


for (String updateName : updateNames)
{
	parameterHashMap.put(updateName, new String[] {
			oclUpdateAnnotation.getDetails().get(updateName+"Object"),
			oclUpdateAnnotation.getDetails().get(updateName+"Value"),
			oclUpdateAnnotation.getDetails().get(updateName+"Feature"),
			oclUpdateAnnotation.getDetails().get(updateName+"FeaturePackage") == null ? genPackage.getNSURI() : oclUpdateAnnotation.getDetails().get(updateName+"FeaturePackage")  ,
			oclUpdateAnnotation.getDetails().get(updateName+"FeatureClass") == null ? genClass.getName()  : oclUpdateAnnotation.getDetails().get(updateName+"FeatureClass"),
			oclUpdateAnnotation.getDetails().get(updateName+"Value").concat("PersistenceLocation"),
			oclUpdateAnnotation.getDetails().get(updateName+"Value").concat("AlternativePersistencePackage"),
			oclUpdateAnnotation.getDetails().get(updateName+"Value").concat("AlternativePersistenceRoot"),
			oclUpdateAnnotation.getDetails().get(updateName+"Value").concat("AlternativePersistenceReference"),
			oclUpdateAnnotation.getDetails().get(updateName+"Mode"),
			//Todo: Define directly
oclUpdateAnnotation.getDetails().get(updateName+"Value").substring(0,oclUpdateAnnotation.getDetails().get(updateName+"Value").length() -5).concat("Condition")
			});
			
			

}

    stringBuffer.append(TEXT_1640);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1641);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1642);
    stringBuffer.append(genOperation.getParameterTypes(", "));
    stringBuffer.append(TEXT_1643);
    stringBuffer.append(innerValueOfDerivedFeat.getName());
    stringBuffer.append(TEXT_1644);
    }
    stringBuffer.append(TEXT_1645);
    if (generatedDerivedChangeableFeat.isReferenceType()) { 
    stringBuffer.append(TEXT_1646);
      }
         else {
    stringBuffer.append(TEXT_1647);
     }
    stringBuffer.append(TEXT_1648);
    stringBuffer.append(generatedDerivedChangeableFeat.getQualifiedFeatureAccessorName());
    stringBuffer.append(TEXT_1649);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1650);
    if (innerValueOfDerivedFeat.isReferenceType() && !innerValueOfDerivedFeat.isEnumType()) {
    stringBuffer.append(TEXT_1651);
    } else {
    stringBuffer.append(TEXT_1652);
    }
    stringBuffer.append(TEXT_1653);
    stringBuffer.append(innerValueOfDerivedFeat.getQualifiedFeatureAccessorName());
    stringBuffer.append(TEXT_1654);
    }
    for (Map.Entry<String, String[]> pair: parameterHashMap.entrySet()) {
	     	String   update  = pair.getKey();
    String[] details = pair.getValue();
        GenPackage updateGenPack = null;
    GenClass   updateGenClass = null;
    GenFeature updateFeature = null;
    GenOperation updateObject = null;
    GenOperation updateValue = null;
    GenOperation updateValuePersistenceLocation = null;
    GenOperation updateValueAlternativePersistencePackage = null;
    GenOperation updateValueAlternativePersistenceRoot = null;
    GenOperation updateValueAlternativePersistenceReference = null;
    String updateMode = "";
    String updateAddMode = "null";
    GenOperation updateCondition = null;
   
    for (GenOperation genOperations : genClass.getGenOperations())
    {
    	if(genOperations.getName().equals(details[0]))
    		updateObject = genOperations;
    	else if (genOperations.getName().equals(details[1]))
    		updateValue = genOperations;
    	else if (genOperations.getName().equals(details[5]))
    	     updateValuePersistenceLocation = genOperations;
    	else if (genOperations.getName().equals(details[6]))
    	     updateValueAlternativePersistencePackage = genOperations;
    	else if (genOperations.getName().equals(details[7]))
    		updateValueAlternativePersistenceRoot = genOperations;
    	else if (genOperations.getName().equals(details[8]))
    		updateValueAlternativePersistenceReference = genOperations;
    	else if (updateMode.equals(""))
    	{
    	    String mode = details[9].substring(3,details[9].length());
    		updateMode = "org.xocl.semantics.XUpdateMode.".concat(mode.toUpperCase());
    		if(mode.equals("Add"))
    		   updateAddMode = "org.xocl.semantics.XAddUpdateMode.FIRST";
    	}
  
       if (genOperations.getName().equals(details[10]))
            updateCondition = genOperations;

    }
     
		for (GenPackage genPack: genModel.getAllGenAndUsedGenPackagesWithClassifiers())
		{
			if(genPack.getNSURI().equals(details[3]))
			{
				updateGenPack = genPack;
				break;
			}
		}
		for(GenClassifier allGenClass : updateGenPack.getGenClassifiers())
			if (allGenClass.getName().equals(details[4]))
			{
				updateGenClass = (GenClass) allGenClass;
				for(GenFeature genFeature: updateGenClass.getAllGenFeatures())
					if (genFeature.getName().equals(details[2]))
					{
							updateFeature = genFeature;
							break;
					}
			}
		
		
	
    stringBuffer.append(TEXT_1655);
    stringBuffer.append(updateObject.getTypeClassifierAccessorName());
    stringBuffer.append(TEXT_1656);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1657);
    stringBuffer.append(updateObject.getName());
    stringBuffer.append(TEXT_1658);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1659);
    }
    stringBuffer.append(TEXT_1660);
    if (updateCondition != null){
    stringBuffer.append(TEXT_1661);
    stringBuffer.append(updateCondition.getName());
    stringBuffer.append(TEXT_1662);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1663);
    }
    stringBuffer.append(TEXT_1664);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1665);
    }
    stringBuffer.append(TEXT_1666);
    if (!(updateFeature.isDerived() && updateFeature.isChangeable())) {
    stringBuffer.append(TEXT_1667);
    if (updateFeature.isReferenceType()) {
    stringBuffer.append(TEXT_1668);
    } else {
    stringBuffer.append(TEXT_1669);
    }
    stringBuffer.append(TEXT_1670);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1671);
    stringBuffer.append(updateFeature.getQualifiedFeatureAccessorName());
    stringBuffer.append(TEXT_1672);
    stringBuffer.append(updateMode);
    stringBuffer.append(TEXT_1673);
    stringBuffer.append( updateAddMode);
    stringBuffer.append(TEXT_1674);
    stringBuffer.append(TEXT_1675);
    stringBuffer.append(updateValue.getName());
    stringBuffer.append(TEXT_1676);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1677);
    }
    stringBuffer.append(TEXT_1678);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1679);
    if (updateValuePersistenceLocation == null) {
    stringBuffer.append(TEXT_1680);
    } else {
    stringBuffer.append(TEXT_1681);
    stringBuffer.append(updateValuePersistenceLocation.getName());
    stringBuffer.append(TEXT_1682);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1683);
    }
    stringBuffer.append(TEXT_1684);
    if (updateValueAlternativePersistencePackage == null) {
    stringBuffer.append(TEXT_1685);
    } else {
    stringBuffer.append(TEXT_1686);
    stringBuffer.append(updateValueAlternativePersistencePackage.getName());
    stringBuffer.append(TEXT_1687);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1688);
    }
    stringBuffer.append(TEXT_1689);
    if (updateValueAlternativePersistenceRoot == null) {
    stringBuffer.append(TEXT_1690);
    } else {
    stringBuffer.append(TEXT_1691);
    stringBuffer.append(updateValueAlternativePersistenceRoot.getName());
    stringBuffer.append(TEXT_1692);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1693);
    }
    stringBuffer.append(TEXT_1694);
    if (updateValueAlternativePersistenceReference == null) {
    stringBuffer.append(TEXT_1695);
    } else {
    stringBuffer.append(TEXT_1696);
    stringBuffer.append(updateValueAlternativePersistenceReference.getName());
    stringBuffer.append(TEXT_1697);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1698);
    }
    stringBuffer.append(TEXT_1699);
    } else {
    stringBuffer.append(TEXT_1700);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1701);
    stringBuffer.append(updateFeature.getUncapName());
    stringBuffer.append(TEXT_1702);
    stringBuffer.append(updateValue.getName());
    stringBuffer.append(TEXT_1703);
    if (innerValueOfDerivedFeat != null){
    stringBuffer.append(TEXT_1704);
    }
    stringBuffer.append(TEXT_1705);
    stringBuffer.append(updateGenClass.getSafeUncapName());
    stringBuffer.append(TEXT_1706);
    }
    stringBuffer.append(TEXT_1707);
    }
    stringBuffer.append(TEXT_1708);
     if (parameterHashMap.isEmpty()) { 
    stringBuffer.append(TEXT_1709);
    } else { 
    stringBuffer.append(TEXT_1710);
    }
    stringBuffer.append(TEXT_1711);
     } else { 
    stringBuffer.append(TEXT_1712);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(body));
    stringBuffer.append(TEXT_1713);
    
		final String expr = oclAnnotationsHelper.getOperationBodyVariable(genOperation); 
		final String operation = override ? "eOverrideOperation" : "eOperation"; 
    stringBuffer.append(TEXT_1714);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_1715);
    stringBuffer.append((override ? genClass : genOperation.getGenClass()).getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1716);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_1717);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1718);
    stringBuffer.append(genOperation.getGenClass().getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1719);
    stringBuffer.append(genOperation.getGenClass().getGenOperations().indexOf(genOperation));
    stringBuffer.append(TEXT_1720);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_1721);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL"));
    stringBuffer.append(TEXT_1722);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1723);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_1724);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1725);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_1726);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_1727);
     if (genOperation.isPrimitiveReturnType()) {
    stringBuffer.append(genOperation.getObjectReturnType());
    stringBuffer.append(TEXT_1728);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_1729);
     } else if (genOperation.getType(null).equals("void")){} else { 
    stringBuffer.append(TEXT_1730);
     }
    stringBuffer.append(TEXT_1731);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1732);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1733);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1734);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1735);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_1736);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_1737);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1738);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_1739);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_1740);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1741);
     if (!genOperation.getEcoreOperation().getEParameters().isEmpty()) { 
    stringBuffer.append(TEXT_1742);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.EvaluationEnvironment"));
    stringBuffer.append(TEXT_1743);
     for (EParameter param : genOperation.getEcoreOperation().getEParameters()) { 
    stringBuffer.append(TEXT_1744);
    stringBuffer.append(param.getName());
    stringBuffer.append(TEXT_1745);
    stringBuffer.append(param.getName());
    stringBuffer.append(TEXT_1746);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_1747);
     }
	} 

	if (genOperation.isListType()) { 
    stringBuffer.append(TEXT_1748);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_1749);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_1750);
    if (! genOperation.getType(null).equals("void")){
    stringBuffer.append(TEXT_1751);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_1752);
    }
    stringBuffer.append(TEXT_1753);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1754);
     } else if (genOperation.isPrimitiveType()) { 
    stringBuffer.append(TEXT_1755);
    if (! genOperation.getType(null).equals("void")){
    stringBuffer.append(TEXT_1756);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_1757);
    }
    stringBuffer.append(TEXT_1758);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_1759);
     } else { 
    stringBuffer.append(TEXT_1760);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_1761);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_1762);
    if (! genOperation.getType(null).equals("void")){
    stringBuffer.append(TEXT_1763);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_1764);
    }
    stringBuffer.append(TEXT_1765);
    stringBuffer.append(operation);
    stringBuffer.append(TEXT_1766);
     } 
    stringBuffer.append(TEXT_1767);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_1768);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1769);
     if (genOperation.isPrimitiveReturnType()) {
    stringBuffer.append(genOperation.getObjectReturnType());
    stringBuffer.append(TEXT_1770);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_1771);
     } else if (genOperation.getType(null).equals("void")){} else { 
    stringBuffer.append(TEXT_1772);
     }
    stringBuffer.append(TEXT_1773);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_1774);
     } 
    stringBuffer.append(TEXT_1775);
    //Class/implementedGenOperation.todo.override.javajetinc
    }
    stringBuffer.append(TEXT_1776);
    }
    //Class/implementedGenOperation.override.javajetinc
    }//for
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEInverseAddGenFeatures())) {
    stringBuffer.append(TEXT_1777);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : genClass.getEInverseAddGenFeatures()) {
    if (genFeature.isUncheckedCast(genClass)) {
    stringBuffer.append(TEXT_1778);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1779);
    }
    stringBuffer.append(TEXT_1780);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1781);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1782);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1783);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1784);
    for (GenFeature genFeature : genClass.getEInverseAddGenFeatures()) {
    stringBuffer.append(TEXT_1785);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1786);
    if (genFeature.isListType()) { String cast = "("  + genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList") + (!genModel.useGenerics() ? ")" : "<" + genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject") + ">)(" + genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList") + "<?>)");
    if (genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_1787);
    stringBuffer.append(cast);
    stringBuffer.append(TEXT_1788);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_1789);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_1790);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1791);
    } else {
    stringBuffer.append(TEXT_1792);
    stringBuffer.append(cast);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1793);
    }
    } else if (genFeature.isContainer()) {
    stringBuffer.append(TEXT_1794);
    if (genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_1795);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1796);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1797);
    } else {
    stringBuffer.append(TEXT_1798);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1799);
    }
    } else {
    if (genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_1800);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1801);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1802);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1803);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1804);
    } else if (genFeature.isVolatile() || genClass.getImplementingGenModel(genFeature).isDynamicDelegation()) {
    stringBuffer.append(TEXT_1805);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1806);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1807);
    if (genFeature.isResolveProxies()) {
    stringBuffer.append(TEXT_1808);
    stringBuffer.append(genFeature.getAccessorName());
    } else {
    stringBuffer.append(genFeature.getGetAccessor());
    }
    stringBuffer.append(TEXT_1809);
    }
    stringBuffer.append(TEXT_1810);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1811);
    if (genFeature.isEffectiveContains()) {
    stringBuffer.append(TEXT_1812);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1813);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1814);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1815);
    } else { GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_1816);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1817);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_1818);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_1819);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_1820);
    }
    stringBuffer.append(TEXT_1821);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1822);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1823);
    }
    }
    stringBuffer.append(TEXT_1824);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1825);
    } else {
    stringBuffer.append(TEXT_1826);
    }
    stringBuffer.append(TEXT_1827);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEInverseRemoveGenFeatures())) {
    stringBuffer.append(TEXT_1828);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1829);
    }
    stringBuffer.append(TEXT_1830);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1831);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_1832);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1833);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1834);
    for (GenFeature genFeature : genClass.getEInverseRemoveGenFeatures()) {
    stringBuffer.append(TEXT_1835);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1836);
    if (genFeature.isListType()) {
    if (genFeature.isMapType() && genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_1837);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_1838);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_1839);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_1840);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1841);
    } else if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1842);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_1843);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1844);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1845);
    } else {
    stringBuffer.append(TEXT_1846);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.InternalEList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_1847);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1848);
    }
    } else if (genFeature.isContainer() && !genFeature.isBasicSet()) {
    stringBuffer.append(TEXT_1849);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_1850);
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_1851);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1852);
    } else {
    stringBuffer.append(TEXT_1853);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1854);
    }
    }
    stringBuffer.append(TEXT_1855);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1856);
    } else {
    stringBuffer.append(TEXT_1857);
    }
    stringBuffer.append(TEXT_1858);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEBasicRemoveFromContainerGenFeatures())) {
    stringBuffer.append(TEXT_1859);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1860);
    }
    stringBuffer.append(TEXT_1861);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1862);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_1863);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1864);
    for (GenFeature genFeature : genClass.getEBasicRemoveFromContainerGenFeatures()) {
    GenFeature reverseFeature = genFeature.getReverse(); GenClass targetClass = reverseFeature.getGenClass(); String reverseOffsetCorrection = targetClass.hasOffsetCorrection() ? " + " + genClass.getOffsetCorrectionField(genFeature) : "";
    stringBuffer.append(TEXT_1865);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1866);
    stringBuffer.append(targetClass.getQualifiedFeatureID(reverseFeature));
    stringBuffer.append(reverseOffsetCorrection);
    stringBuffer.append(TEXT_1867);
    stringBuffer.append(targetClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_1868);
    }
    stringBuffer.append(TEXT_1869);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1870);
    } else {
    stringBuffer.append(TEXT_1871);
    }
    stringBuffer.append(TEXT_1872);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEGetGenFeatures())) {
    stringBuffer.append(TEXT_1873);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1874);
    }
    stringBuffer.append(TEXT_1875);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1876);
    for (GenFeature genFeature : genClass.getEGetGenFeatures()) {
    stringBuffer.append(TEXT_1877);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1878);
    if (genFeature.isPrimitiveType()) {
    if (isJDK50) {
    stringBuffer.append(TEXT_1879);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1880);
    } else if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_1881);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1882);
    } else {
    stringBuffer.append(TEXT_1883);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_1884);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1885);
    }
    } else if (genFeature.isResolveProxies() && !genFeature.isListType()) {
    stringBuffer.append(TEXT_1886);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1887);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1888);
    } else if (genFeature.isMapType()) {
    if (genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_1889);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_1890);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_1891);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1892);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1893);
    } else {
    stringBuffer.append(TEXT_1894);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1895);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1896);
    }
    } else if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1897);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1898);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1899);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1900);
    } else if (genFeature.isFeatureMapType()) {
    stringBuffer.append(TEXT_1901);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1902);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1903);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1904);
    } else {
    stringBuffer.append(TEXT_1905);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1906);
    }
    }
    stringBuffer.append(TEXT_1907);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1908);
    } else {
    stringBuffer.append(TEXT_1909);
    }
    stringBuffer.append(TEXT_1910);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getESetGenFeatures())) {
    stringBuffer.append(TEXT_1911);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : genClass.getESetGenFeatures()) {
    if (genFeature.isUncheckedCast(genClass) && !genFeature.isFeatureMapType() && !genFeature.isMapType()) {
    stringBuffer.append(TEXT_1912);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1913);
    }
    stringBuffer.append(TEXT_1914);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1915);
    for (GenFeature genFeature : genClass.getESetGenFeatures()) {
    stringBuffer.append(TEXT_1916);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1917);
    if (genFeature.isListType()) {
    if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1918);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1919);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1920);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1921);
    } else if (genFeature.isFeatureMapType()) {
    stringBuffer.append(TEXT_1922);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1923);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1924);
    } else if (genFeature.isMapType()) {
    if (genFeature.isEffectiveSuppressEMFTypes()) {
    stringBuffer.append(TEXT_1925);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_1926);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EMap"));
    stringBuffer.append(TEXT_1927);
    stringBuffer.append(genFeature.getImportedMapTemplateArguments(genClass));
    stringBuffer.append(TEXT_1928);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1929);
    } else {
    stringBuffer.append(TEXT_1930);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_1931);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1932);
    }
    } else {
    stringBuffer.append(TEXT_1933);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1934);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1935);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    if (isJDK50) {
    stringBuffer.append(TEXT_1936);
    stringBuffer.append(genFeature.getListItemType(genClass));
    stringBuffer.append(TEXT_1937);
    }
    stringBuffer.append(TEXT_1938);
    }
    } else if (!isJDK50 && genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_1939);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1940);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_1941);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_1942);
    } else {
    stringBuffer.append(TEXT_1943);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1944);
    if (genFeature.getTypeGenDataType() == null || !genFeature.getTypeGenDataType().isObjectType() || !genFeature.getRawType().equals(genFeature.getType(genClass))) {
    stringBuffer.append(TEXT_1945);
    stringBuffer.append(genFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_1946);
    }
    stringBuffer.append(TEXT_1947);
    }
    stringBuffer.append(TEXT_1948);
    }
    stringBuffer.append(TEXT_1949);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1950);
    } else {
    stringBuffer.append(TEXT_1951);
    }
    stringBuffer.append(TEXT_1952);
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEUnsetGenFeatures())) {
    stringBuffer.append(TEXT_1953);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1954);
    }
    stringBuffer.append(TEXT_1955);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1956);
    for (GenFeature genFeature : genClass.getEUnsetGenFeatures()) {
    stringBuffer.append(TEXT_1957);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1958);
    if (genFeature.isListType() && !genFeature.isUnsettable()) {
    if (genFeature.isWrappedFeatureMapType()) {
    stringBuffer.append(TEXT_1959);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1960);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1961);
    } else {
    stringBuffer.append(TEXT_1962);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1963);
    }
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_1964);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1965);
    } else if (!genFeature.hasEDefault()) {
    stringBuffer.append(TEXT_1966);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1967);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1968);
    } else {
    stringBuffer.append(TEXT_1969);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1970);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_1971);
    }
    stringBuffer.append(TEXT_1972);
    }
    stringBuffer.append(TEXT_1973);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_1974);
    } else {
    stringBuffer.append(TEXT_1975);
    }
    stringBuffer.append(TEXT_1976);
    //Class/eUnset.override.javajetinc
    }
    if (isImplementation && !genModel.isReflectiveDelegation() && genClass.implementsAny(genClass.getEIsSetGenFeatures())) {
    stringBuffer.append(TEXT_1977);
    if (genModel.useGenerics()) {
    for (GenFeature genFeature : genClass.getEIsSetGenFeatures()) {
    if (genFeature.isListType() && !genFeature.isUnsettable() && !genFeature.isWrappedFeatureMapType() && !genClass.isField(genFeature) && genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_1978);
    break; }
    }
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_1979);
    }
    stringBuffer.append(TEXT_1980);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_1981);
    for (GenFeature genFeature : genClass.getEIsSetGenFeatures()) { String safeNameAccessor = genFeature.getSafeName(); if ("featureID".equals(safeNameAccessor)) { safeNameAccessor = "this." + safeNameAccessor; }
    stringBuffer.append(TEXT_1982);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_1983);
    if (genFeature.hasSettingDelegate()) {
    if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_1984);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_1985);
    } else {
    stringBuffer.append(TEXT_1986);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_1987);
    }
    } else if (genFeature.isListType() && !genFeature.isUnsettable()) {
    if (genFeature.isWrappedFeatureMapType()) {
    if (genFeature.isVolatile()) {
    stringBuffer.append(TEXT_1988);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.util.FeatureMap"));
    stringBuffer.append(TEXT_1989);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_1990);
    } else {
    stringBuffer.append(TEXT_1991);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_1992);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_1993);
    }
    } else {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_1994);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_1995);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_1996);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_1997);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_1998);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_1999);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2000);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2001);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2002);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2003);
    } else {
    stringBuffer.append(TEXT_2004);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2005);
    }
    }
    }
    } else if (genFeature.isUnsettable()) {
    stringBuffer.append(TEXT_2006);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_2007);
    } else if (genFeature.isResolveProxies()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_2008);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2009);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_2010);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2011);
    } else {
    stringBuffer.append(TEXT_2012);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_2013);
    }
    }
    } else if (!genFeature.hasEDefault()) {
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_2014);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2015);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_2016);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2017);
    } else {
    stringBuffer.append(TEXT_2018);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2019);
    }
    }
    } else if (genFeature.isPrimitiveType() || genFeature.isEnumType()) {
    if (genClass.isField(genFeature)) {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_2020);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2021);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2022);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2023);
    } else {
    stringBuffer.append(TEXT_2024);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2025);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2026);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2027);
    }
    } else {
    stringBuffer.append(TEXT_2028);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2029);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2030);
    }
    } else {
    if (genFeature.isEnumType() && genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_2031);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2032);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2033);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2034);
    } else {
    stringBuffer.append(TEXT_2035);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2036);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2037);
    }
    }
    } else {//datatype
    if (genClass.isField(genFeature)) {
    stringBuffer.append(TEXT_2038);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2039);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2040);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2041);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2042);
    } else {
    if (genFeature.isField() && genClass.getImplementingGenModel(genFeature).isVirtualDelegation()) {
    stringBuffer.append(TEXT_2043);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2044);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2045);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2046);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2047);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2048);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2049);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2050);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2051);
    stringBuffer.append(safeNameAccessor);
    stringBuffer.append(TEXT_2052);
    } else {
    stringBuffer.append(TEXT_2053);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2054);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2055);
    stringBuffer.append(genFeature.getEDefault());
    stringBuffer.append(TEXT_2056);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2057);
    }
    }
    }
    }
    stringBuffer.append(TEXT_2058);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_2059);
    } else {
    stringBuffer.append(TEXT_2060);
    }
    stringBuffer.append(TEXT_2061);
    //Class/eIsSet.override.javajetinc
    }
    if (isImplementation && (!genClass.getMixinGenFeatures().isEmpty() || genClass.hasOffsetCorrection() && !genClass.getGenFeatures().isEmpty())) {
    if (!genClass.getMixinGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_2062);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2063);
    }
    stringBuffer.append(TEXT_2064);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_2065);
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_2066);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2067);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_2068);
    for (GenFeature genFeature : mixinGenClass.getGenFeatures()) {
    stringBuffer.append(TEXT_2069);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_2070);
    stringBuffer.append(mixinGenClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_2071);
    }
    stringBuffer.append(TEXT_2072);
    }
    stringBuffer.append(TEXT_2073);
    }
    stringBuffer.append(TEXT_2074);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2075);
    }
    stringBuffer.append(TEXT_2076);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_2077);
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_2078);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2079);
    for (GenFeature genFeature : mixinGenClass.getGenFeatures()) {
    stringBuffer.append(TEXT_2080);
    stringBuffer.append(mixinGenClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_2081);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2082);
    }
    stringBuffer.append(TEXT_2083);
    }
    if (genClass.hasOffsetCorrection() && !genClass.getGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_2084);
    stringBuffer.append(genClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2085);
    stringBuffer.append(negativeOffsetCorrection);
    stringBuffer.append(TEXT_2086);
    for (GenFeature genFeature : genClass.getGenFeatures()) {
    stringBuffer.append(TEXT_2087);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(TEXT_2088);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2089);
    }
    stringBuffer.append(TEXT_2090);
    }
    stringBuffer.append(TEXT_2091);
    }
    if (genModel.isOperationReflection() && isImplementation && (!genClass.getMixinGenOperations().isEmpty() || !genClass.getOverrideGenOperations(genClass.getExtendedGenOperations(), genClass.getImplementedGenOperations()).isEmpty() || genClass.hasOffsetCorrection() && !genClass.getGenOperations().isEmpty())) {
    stringBuffer.append(TEXT_2092);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2093);
    }
    stringBuffer.append(TEXT_2094);
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_2095);
    for (GenClass extendedGenClass : genClass.getExtendedGenClasses()) { List<GenOperation> extendedImplementedGenOperations = extendedGenClass.getImplementedGenOperations(); List<GenOperation> implementedGenOperations = genClass.getImplementedGenOperations();
    if (!genClass.getOverrideGenOperations(extendedImplementedGenOperations, implementedGenOperations).isEmpty()) {
    stringBuffer.append(TEXT_2096);
    stringBuffer.append(extendedGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2097);
    for (GenOperation genOperation : extendedImplementedGenOperations) { GenOperation overrideGenOperation = genClass.getOverrideGenOperation(genOperation);
    if (implementedGenOperations.contains(overrideGenOperation)) {
    stringBuffer.append(TEXT_2098);
    stringBuffer.append(extendedGenClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_2099);
    stringBuffer.append(genClass.getQualifiedOperationID(overrideGenOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_2100);
    }
    }
    stringBuffer.append(TEXT_2101);
    }
    }
    for (GenClass mixinGenClass : genClass.getMixinGenClasses()) {
    stringBuffer.append(TEXT_2102);
    stringBuffer.append(mixinGenClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2103);
    for (GenOperation genOperation : mixinGenClass.getGenOperations()) { GenOperation overrideGenOperation = genClass.getOverrideGenOperation(genOperation);
    stringBuffer.append(TEXT_2104);
    stringBuffer.append(mixinGenClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_2105);
    stringBuffer.append(genClass.getQualifiedOperationID(overrideGenOperation != null ? overrideGenOperation : genOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_2106);
    }
    stringBuffer.append(TEXT_2107);
    }
    if (genClass.hasOffsetCorrection() && !genClass.getGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_2108);
    stringBuffer.append(genClass.getRawImportedInterfaceName());
    stringBuffer.append(TEXT_2109);
    stringBuffer.append(negativeOperationOffsetCorrection);
    stringBuffer.append(TEXT_2110);
    for (GenOperation genOperation : genClass.getGenOperations()) {
    stringBuffer.append(TEXT_2111);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_2112);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(positiveOperationOffsetCorrection);
    stringBuffer.append(TEXT_2113);
    }
    stringBuffer.append(TEXT_2114);
    }
    stringBuffer.append(TEXT_2115);
    }
    if (isImplementation && genModel.isVirtualDelegation()) { String eVirtualValuesField = genClass.getEVirtualValuesField();
    if (eVirtualValuesField != null) {
    stringBuffer.append(TEXT_2116);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2117);
    }
    stringBuffer.append(TEXT_2118);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_2119);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2120);
    }
    stringBuffer.append(TEXT_2121);
    stringBuffer.append(eVirtualValuesField);
    stringBuffer.append(TEXT_2122);
    }
    { List<String> eVirtualIndexBitFields = genClass.getEVirtualIndexBitFields(new ArrayList<String>());
    if (!eVirtualIndexBitFields.isEmpty()) { List<String> allEVirtualIndexBitFields = genClass.getAllEVirtualIndexBitFields(new ArrayList<String>());
    stringBuffer.append(TEXT_2123);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2124);
    }
    stringBuffer.append(TEXT_2125);
    for (int i = 0; i < allEVirtualIndexBitFields.size(); i++) {
    stringBuffer.append(TEXT_2126);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_2127);
    stringBuffer.append(allEVirtualIndexBitFields.get(i));
    stringBuffer.append(TEXT_2128);
    }
    stringBuffer.append(TEXT_2129);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2130);
    }
    stringBuffer.append(TEXT_2131);
    for (int i = 0; i < allEVirtualIndexBitFields.size(); i++) {
    stringBuffer.append(TEXT_2132);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_2133);
    stringBuffer.append(allEVirtualIndexBitFields.get(i));
    stringBuffer.append(TEXT_2134);
    }
    stringBuffer.append(TEXT_2135);
    }
    }
    }
    if (genModel.isOperationReflection() && isImplementation && !genClass.getImplementedGenOperations().isEmpty()) {
    stringBuffer.append(TEXT_2136);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2137);
    }
    if (genModel.useGenerics()) {
    LOOP: for (GenOperation genOperation : (genModel.isMinimalReflectiveMethods() ? genClass.getImplementedGenOperations() : genClass.getAllGenOperations())) {
    for (GenParameter genParameter : genOperation.getGenParameters()) {
    if (genParameter.isUncheckedCast()) {
    stringBuffer.append(TEXT_2138);
    break LOOP;}
    }
    }
    }
    stringBuffer.append(TEXT_2139);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.util.EList"));
    stringBuffer.append(singleWildcard);
    stringBuffer.append(TEXT_2140);
    stringBuffer.append(genModel.getImportedName(isGWT ? "org.eclipse.emf.common.util.InvocationTargetException" : "java.lang.reflect.InvocationTargetException"));
    stringBuffer.append(TEXT_2141);
    stringBuffer.append(negativeOperationOffsetCorrection);
    stringBuffer.append(TEXT_2142);
    for (GenOperation genOperation : (genModel.isMinimalReflectiveMethods() ? genClass.getImplementedGenOperations() : genClass.getAllGenOperations())) { List<GenParameter> genParameters = genOperation.getGenParameters(); int size = genParameters.size();
    stringBuffer.append(TEXT_2143);
    stringBuffer.append(genClass.getQualifiedOperationID(genOperation));
    stringBuffer.append(TEXT_2144);
    if (genOperation.isVoid()) {
    stringBuffer.append(TEXT_2145);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_2146);
    for (int i = 0; i < size; i++) { GenParameter genParameter = genParameters.get(i);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_2147);
    }
    if (genParameter.getTypeGenDataType() == null || !genParameter.getTypeGenDataType().isObjectType() || !genParameter.getRawType().equals(genParameter.getType(genClass))) {
    stringBuffer.append(TEXT_2148);
    stringBuffer.append(genParameter.getObjectType(genClass));
    stringBuffer.append(TEXT_2149);
    }
    stringBuffer.append(TEXT_2150);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_2151);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_2152);
    stringBuffer.append(genParameter.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2153);
    }
    if (i < (size - 1)) {
    stringBuffer.append(TEXT_2154);
    }
    }
    stringBuffer.append(TEXT_2155);
    } else {
    stringBuffer.append(TEXT_2156);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_2157);
    stringBuffer.append(genOperation.getObjectType(genClass));
    stringBuffer.append(TEXT_2158);
    }
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_2159);
    for (int i = 0; i < size; i++) { GenParameter genParameter = genParameters.get(i);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_2160);
    }
    if (genParameter.getTypeGenDataType() == null || !genParameter.getTypeGenDataType().isObjectType() || !genParameter.getRawType().equals(genParameter.getType(genClass))) {
    stringBuffer.append(TEXT_2161);
    stringBuffer.append(genParameter.getObjectType(genClass));
    stringBuffer.append(TEXT_2162);
    }
    stringBuffer.append(TEXT_2163);
    stringBuffer.append(i);
    stringBuffer.append(TEXT_2164);
    if (!isJDK50 && genParameter.isPrimitiveType()) {
    stringBuffer.append(TEXT_2165);
    stringBuffer.append(genParameter.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2166);
    }
    if (i < (size - 1)) {
    stringBuffer.append(TEXT_2167);
    }
    }
    stringBuffer.append(TEXT_2168);
    if (!isJDK50 && genOperation.isPrimitiveType()) {
    stringBuffer.append(TEXT_2169);
    }
    stringBuffer.append(TEXT_2170);
    }
    }
    stringBuffer.append(TEXT_2171);
    if (genModel.isMinimalReflectiveMethods()) {
    stringBuffer.append(TEXT_2172);
    } else {
    stringBuffer.append(TEXT_2173);
    }
    stringBuffer.append(TEXT_2174);
    }
    if (!genClass.hasImplementedToStringGenOperation() && isImplementation && !genModel.isReflectiveDelegation() && !genModel.isDynamicDelegation() && !genClass.getToStringGenFeatures().isEmpty()) {
    stringBuffer.append(TEXT_2175);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2176);
    }
    stringBuffer.append(TEXT_2177);
    { boolean first = true;
    for (GenFeature genFeature : genClass.getToStringGenFeatures()) {
    if (first) { first = false;
    stringBuffer.append(TEXT_2178);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2179);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_2180);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2181);
    stringBuffer.append(genModel.getNonNLS());
    }
    if (genFeature.isUnsettable() && !genFeature.isListType()) {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2182);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2183);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    stringBuffer.append(TEXT_2184);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_2185);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_2186);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_2187);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2188);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_2189);
    }
    stringBuffer.append(TEXT_2190);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2191);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2192);
    stringBuffer.append(genModel.getNonNLS());
    } else {
    stringBuffer.append(TEXT_2193);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_2194);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_2195);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2196);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_2197);
    }
    stringBuffer.append(TEXT_2198);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2199);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2200);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2201);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2202);
    stringBuffer.append(genModel.getNonNLS());
    }
    } else {
    stringBuffer.append(TEXT_2203);
    if (genClass.isESetFlag(genFeature)) {
    stringBuffer.append(TEXT_2204);
    stringBuffer.append(genClass.getESetFlagsField(genFeature));
    stringBuffer.append(TEXT_2205);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2206);
    } else {
    stringBuffer.append(genFeature.getUncapName());
    stringBuffer.append(TEXT_2207);
    }
    stringBuffer.append(TEXT_2208);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2209);
    stringBuffer.append(genModel.getNonNLS());
    }
    }
    } else {
    if (genModel.isVirtualDelegation() && !genFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2210);
    stringBuffer.append(genClass.getQualifiedFeatureID(genFeature));
    stringBuffer.append(positiveOffsetCorrection);
    if (!genFeature.isListType() && !genFeature.isReferenceType()){
    stringBuffer.append(TEXT_2211);
    stringBuffer.append(genFeature.getEDefault());
    }
    stringBuffer.append(TEXT_2212);
    } else {
    if (genClass.isFlag(genFeature)) {
    if (genFeature.isBooleanType()) {
    stringBuffer.append(TEXT_2213);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2214);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2215);
    } else {
    stringBuffer.append(TEXT_2216);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2217);
    stringBuffer.append(genClass.getFlagsField(genFeature));
    stringBuffer.append(TEXT_2218);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2219);
    stringBuffer.append(genFeature.getUpperName());
    stringBuffer.append(TEXT_2220);
    }
    } else {
    stringBuffer.append(TEXT_2221);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2222);
    }
    }
    }
    }
    }
    stringBuffer.append(TEXT_2223);
    }
    if (isImplementation && genClass.isMapEntry()) { GenFeature keyFeature = genClass.getMapEntryKeyFeature(); GenFeature valueFeature = genClass.getMapEntryValueFeature();
    String objectType = genModel.getImportedName("java.lang.Object");
    String keyType = isJDK50 ? keyFeature.getObjectType(genClass) : objectType;
    String valueType = isJDK50 ? valueFeature.getObjectType(genClass) : objectType;
    String eMapType = genModel.getImportedName("org.eclipse.emf.common.util.EMap") + (isJDK50 ? "<" + keyType + ", " + valueType + ">" : "");
    stringBuffer.append(TEXT_2224);
    if (isGWT) {
    stringBuffer.append(TEXT_2225);
    stringBuffer.append(genModel.getImportedName("com.google.gwt.user.client.rpc.GwtTransient"));
    }
    stringBuffer.append(TEXT_2226);
    stringBuffer.append(objectType);
    stringBuffer.append(TEXT_2227);
    stringBuffer.append(keyType);
    stringBuffer.append(TEXT_2228);
    if (!isJDK50 && keyFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2229);
    stringBuffer.append(keyFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_2230);
    } else {
    stringBuffer.append(TEXT_2231);
    }
    stringBuffer.append(TEXT_2232);
    stringBuffer.append(keyType);
    stringBuffer.append(TEXT_2233);
    if (keyFeature.isListType()) {
    stringBuffer.append(TEXT_2234);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_2235);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_2236);
    }
    stringBuffer.append(TEXT_2237);
    } else if (isJDK50) {
    stringBuffer.append(TEXT_2238);
    } else if (keyFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2239);
    stringBuffer.append(keyFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_2240);
    stringBuffer.append(keyFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2241);
    } else {
    stringBuffer.append(TEXT_2242);
    stringBuffer.append(keyFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2243);
    }
    stringBuffer.append(TEXT_2244);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_2245);
    if (!isJDK50 && valueFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2246);
    stringBuffer.append(valueFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_2247);
    } else {
    stringBuffer.append(TEXT_2248);
    }
    stringBuffer.append(TEXT_2249);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_2250);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_2251);
    stringBuffer.append(valueType);
    stringBuffer.append(TEXT_2252);
    if (valueFeature.isListType()) {
    stringBuffer.append(TEXT_2253);
    if (!genModel.useGenerics()) {
    stringBuffer.append(TEXT_2254);
    stringBuffer.append(genModel.getImportedName("java.util.Collection"));
    stringBuffer.append(TEXT_2255);
    }
    stringBuffer.append(TEXT_2256);
    } else if (isJDK50) {
    stringBuffer.append(TEXT_2257);
    } else if (valueFeature.isPrimitiveType()) {
    stringBuffer.append(TEXT_2258);
    stringBuffer.append(valueFeature.getObjectType(genClass));
    stringBuffer.append(TEXT_2259);
    stringBuffer.append(valueFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2260);
    } else {
    stringBuffer.append(TEXT_2261);
    stringBuffer.append(valueFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2262);
    }
    stringBuffer.append(TEXT_2263);
    if (genModel.useGenerics()) {
    stringBuffer.append(TEXT_2264);
    }
    stringBuffer.append(TEXT_2265);
    stringBuffer.append(eMapType);
    stringBuffer.append(TEXT_2266);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EObject"));
    stringBuffer.append(TEXT_2267);
    stringBuffer.append(eMapType);
    stringBuffer.append(TEXT_2268);
    }
    if (isImplementation) {
EAnnotation ocl = genClass.getEcoreClass().getEAnnotation(oclNsURI);
String label = null;
if (ocl != null) label = ocl.getDetails().get("label");
if (label != null) { 
    stringBuffer.append(TEXT_2269);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(label));
    stringBuffer.append(TEXT_2270);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2271);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2272);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_2273);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EAnnotation"));
    stringBuffer.append(TEXT_2274);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2275);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2276);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2277);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2278);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2279);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2280);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclHelper"));
    stringBuffer.append(TEXT_2281);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2282);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2283);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2284);
    }
}

if (isImplementation) {
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclDerivedGenFeatures(genClass)) {
		if (genClass.getImplementedGenFeatures().contains(genFeature)) {
			continue;
		}		
		String derive = XoclEmfUtil.findDeriveAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass());
		if (!genModel.isReflectiveDelegation() && genFeature.isBasicGet()) {
//@[CopyOf,Class.javajet]

    stringBuffer.append(TEXT_2285);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2286);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(derive));
    stringBuffer.append(TEXT_2287);
    if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //basicGetGenFeature.annotations.insert.javajetinc
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2288);
    }
    stringBuffer.append(TEXT_2289);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2290);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_2291);
    if (derive.length() == 0) { 
    stringBuffer.append(TEXT_2292);
     } else {
	    final String expr = genFeature.getSafeName() + "DeriveOCL";
	    if (true) { // we will need the feature to create the EcoreEList 
    stringBuffer.append(TEXT_2293);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2294);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2295);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2296);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_2297);
     } 
    stringBuffer.append(TEXT_2298);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2299);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_2300);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_2301);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2302);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2303);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2304);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2305);
     } else { 
    stringBuffer.append(TEXT_2306);
     }
    stringBuffer.append(TEXT_2307);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2308);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2309);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2310);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2311);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2312);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2313);
     if (genFeature.isListType()) { 
    stringBuffer.append(TEXT_2314);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2315);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2316);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2317);
     } else if (genFeature.isPrimitiveType()) { 
    stringBuffer.append(TEXT_2318);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2319);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2320);
     } else { 
    stringBuffer.append(TEXT_2321);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2322);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2323);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2324);
     } 
    stringBuffer.append(TEXT_2325);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2326);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2327);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2328);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2329);
     } else { 
    stringBuffer.append(TEXT_2330);
     }
    stringBuffer.append(TEXT_2331);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2332);
     } 
    stringBuffer.append(TEXT_2333);
    
//@[EndCopyOf,Class.javajet]				
		} else if (genFeature.isGet() && !genFeature.isSuppressedGetVisibility()) {
//@[CopyOf,Class.javajet]

    stringBuffer.append(TEXT_2334);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2335);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(derive));
    stringBuffer.append(TEXT_2336);
    if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //getGenFeature.annotations.insert.javajetinc
    }
    if (genModel.useGenerics() && ((genFeature.isContainer() || genFeature.isResolveProxies()) && !genFeature.isListType() && !genModel.isReflectiveDelegation() && genFeature.isUncheckedCast() || genFeature.isListType() && (genModel.isReflectiveDelegation() || genModel.isVirtualDelegation()) || genFeature.isListDataType() && genFeature.hasDelegateFeature())) {
    stringBuffer.append(TEXT_2337);
    }
    if (genModel.useClassOverrideAnnotation() && !genFeature.getGenClass().isInterface()) {
    stringBuffer.append(TEXT_2338);
    }
    stringBuffer.append(TEXT_2339);
    stringBuffer.append(genFeature.getImportedType(genClass));
    stringBuffer.append(TEXT_2340);
    stringBuffer.append(genFeature.getGetAccessor());
    stringBuffer.append(TEXT_2341);
    if (derive.length() == 0) { 
    stringBuffer.append(TEXT_2342);
     } else {
//@[CopyOf,getGenFeature.TODO.override.javajetinc]
	final String expr = genFeature.getSafeName() + "DeriveOCL";
	    if (true) { // we will need the feature to create the EcoreEList 
    stringBuffer.append(TEXT_2343);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2344);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2345);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2346);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_2347);
     } 
    stringBuffer.append(TEXT_2348);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2349);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_2350);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_2351);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2352);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2353);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2354);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2355);
     } else { 
    stringBuffer.append(TEXT_2356);
     }
    stringBuffer.append(TEXT_2357);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2358);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2359);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2360);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2361);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2362);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2363);
     if (genFeature.isListType()) { 
    stringBuffer.append(TEXT_2364);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2365);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2366);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2367);
     } else if (genFeature.isPrimitiveType()) { 
    stringBuffer.append(TEXT_2368);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2369);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2370);
     } else { 
    stringBuffer.append(TEXT_2371);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2372);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2373);
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2374);
     } 
    stringBuffer.append(TEXT_2375);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2376);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2377);
     if (genFeature.isPrimitiveType()) {
    stringBuffer.append(genFeature.getObjectType());
    stringBuffer.append(TEXT_2378);
    stringBuffer.append(genFeature.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2379);
     } else { 
    stringBuffer.append(TEXT_2380);
     }
    stringBuffer.append(TEXT_2381);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2382);
    
//@[EndCopyOf,getGenFeature.TODO.override.javajetinc]
} 
    stringBuffer.append(TEXT_2383);
    
//@[EndCopyOf,Class.javajet]
		}
	}
EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(overrideOclNsURI);
if (eAnnotation != null) {
	EMap<String, String> details = eAnnotation.getDetails();
	for (Map.Entry<String, String> entry : details) {
		String detailKey = entry.getKey();
		if (detailKey.endsWith("Body")) {
			String operationName = detailKey.substring(0, detailKey.length()-4);
			String body = entry.getValue();
			for (GenOperation genOperation : genClass.getAllGenOperations()) {
				if (genOperation.getName().equals(operationName)) {
					if (genClass.getImplementedGenOperations().contains(genOperation)) {
						break;
					}
//@[CopyOf,Class.javajet]

    stringBuffer.append(TEXT_2384);
    stringBuffer.append(operationName);
    stringBuffer.append(TEXT_2385);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(body));
    stringBuffer.append(TEXT_2386);
    if (genModel.getComplianceLevel().getValue() >= GenJDKLevel.JDK50) { //genOperation.annotations.insert.javajetinc
    }
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2387);
    }
    stringBuffer.append(TEXT_2388);
    stringBuffer.append(genOperation.getTypeParameters(genClass));
    stringBuffer.append(genOperation.getImportedType(genClass));
    stringBuffer.append(TEXT_2389);
    stringBuffer.append(genOperation.getName());
    stringBuffer.append(TEXT_2390);
    stringBuffer.append(genOperation.getParameters(genClass));
    stringBuffer.append(TEXT_2391);
    stringBuffer.append(genOperation.getThrows(genClass));
    stringBuffer.append(TEXT_2392);
    if (body.length() == 0) { 
    stringBuffer.append(TEXT_2393);
     } else {
	    final String expr = oclAnnotationsHelper.getOperationBodyVariable(genOperation); 
    stringBuffer.append(TEXT_2394);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2395);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2396);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EOperation"));
    stringBuffer.append(TEXT_2397);
    stringBuffer.append(genOperation.getGenClass().getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2398);
    stringBuffer.append(genOperation.getGenClass().getGenOperations().indexOf(genOperation));
    stringBuffer.append(TEXT_2399);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2400);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL"));
    stringBuffer.append(TEXT_2401);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EAnnotation"));
    stringBuffer.append(TEXT_2402);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_2403);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2404);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2405);
     if (genOperation.isPrimitiveReturnType()) {
    stringBuffer.append(genOperation.getObjectReturnType());
    stringBuffer.append(TEXT_2406);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2407);
     } else { 
    stringBuffer.append(TEXT_2408);
     }
    stringBuffer.append(TEXT_2409);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2410);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2411);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2412);
    stringBuffer.append(expr);
    stringBuffer.append(TEXT_2413);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2414);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2415);
     if (!genOperation.getEcoreOperation().getEParameters().isEmpty()) { 
    stringBuffer.append(TEXT_2416);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.EvaluationEnvironment"));
    stringBuffer.append(TEXT_2417);
     for (EParameter param : genOperation.getEcoreOperation().getEParameters()) { 
    stringBuffer.append(TEXT_2418);
    stringBuffer.append(param.getName());
    stringBuffer.append(TEXT_2419);
    stringBuffer.append(param.getName());
    stringBuffer.append(TEXT_2420);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_2421);
     }
	} 
	
	if (genOperation.isListType()) { 
    stringBuffer.append(TEXT_2422);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2423);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2424);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_2425);
     } else if (genOperation.isPrimitiveType()) { 
    stringBuffer.append(TEXT_2426);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_2427);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2428);
     } else { 
    stringBuffer.append(TEXT_2429);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2430);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2431);
    stringBuffer.append(genOperation.getObjectType());
    stringBuffer.append(TEXT_2432);
     } 
    stringBuffer.append(TEXT_2433);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2434);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2435);
     if (genOperation.isPrimitiveReturnType()) {
    stringBuffer.append(genOperation.getObjectReturnType());
    stringBuffer.append(TEXT_2436);
    stringBuffer.append(genOperation.getPrimitiveValueFunction());
    stringBuffer.append(TEXT_2437);
     } else { 
    stringBuffer.append(TEXT_2438);
     }
    stringBuffer.append(TEXT_2439);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2440);
    }
    stringBuffer.append(TEXT_2441);
    
//@[EndCopyOf,Class.javajet]
				}
			}
		}
	}
}  
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstraintGenFeatures(genClass)) {

    stringBuffer.append(TEXT_2442);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_2443);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_2444);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2445);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2446);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(XoclEmfUtil.findChoiceConstraintAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass())));
    stringBuffer.append(TEXT_2447);
    if (genModel.useClassOverrideAnnotation()) {
	EAnnotation ocl = genFeature.getEcoreFeature().getEAnnotation(oclNsURI);
	if (ocl != null && ocl.getDetails().get("choiceConstraint") != null) { 
    stringBuffer.append(TEXT_2448);
    }}
    stringBuffer.append(TEXT_2449);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_2450);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2451);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2452);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2453);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2454);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_2455);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2456);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_2457);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2458);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2459);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2460);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2461);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2462);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2463);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2464);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2465);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2466);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2467);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2468);
    
	}
//@[EndCopyOf,genFeature.insert.javajetinc]
	for (GenFeature genFeature : oclAnnotationsHelper.getAdditionalXoclChoiceConstructionGenFeatures(genClass)) {
//@[CopyOf,genFeature.insert.javajetinc]

    stringBuffer.append(TEXT_2469);
    stringBuffer.append(genFeature.getFormattedName());
    stringBuffer.append(TEXT_2470);
    stringBuffer.append(genFeature.getFeatureKind());
    stringBuffer.append(TEXT_2471);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_2472);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2473);
    stringBuffer.append(genFeature.getName());
    stringBuffer.append(TEXT_2474);
    stringBuffer.append(xoclGenerationUtil.embedOclIntoJavaComment(XoclEmfUtil.findChoiceConstructionAnnotationText(genFeature.getEcoreFeature(), genClass.getEcoreClass())));
    stringBuffer.append(TEXT_2475);
    if (genModel.useClassOverrideAnnotation()) {
	GenClass genSuperClass = genClass.getClassExtendsGenClass();
	EClass superClass = genSuperClass == null ? null : genSuperClass.getEcoreClass();
	boolean superClassHasFeature = superClass == null ? false : superClass.getEAllStructuralFeatures().contains(genFeature);
	EAnnotation ocl = genFeature.getEcoreFeature().getEAnnotation(oclNsURI);
	if (superClassHasFeature && ocl != null && ocl.getDetails().get("choiceConstruction") != null) { 
    stringBuffer.append(TEXT_2476);
    }}
    stringBuffer.append(TEXT_2477);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_2478);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2479);
    stringBuffer.append(genFeature.getAccessorName());
    stringBuffer.append(TEXT_2480);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_2481);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2482);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClass"));
    stringBuffer.append(TEXT_2483);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2484);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2485);
    stringBuffer.append(genFeature.getQualifiedFeatureAccessor());
    stringBuffer.append(TEXT_2486);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2487);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.Variable"));
    stringBuffer.append(TEXT_2488);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.EcoreFactory"));
    stringBuffer.append(TEXT_2489);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_2490);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2491);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2492);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2493);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2494);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2495);
    stringBuffer.append(genFeature.getSafeName());
    stringBuffer.append(TEXT_2496);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2497);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2498);
    stringBuffer.append(genModel.getImportedName(genFeature.getQualifiedListItemType()));
    stringBuffer.append(TEXT_2499);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2500);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2501);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2502);
    
//@[EndCopyOf,genFeature.insert.javajetinc]
	}
     if (oclAnnotationsHelper.hasImplementedUpdateAnnotations(genClass)) { 
    stringBuffer.append(TEXT_2503);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2504);
    }
     if (oclAnnotationsHelper.isInitializable()) {
    stringBuffer.append(TEXT_2505);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_2506);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2507);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2508);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_2509);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2510);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2511);
    } // endif
  if (oclAnnotationsHelper.isDirectInitializable()) {
    stringBuffer.append(TEXT_2512);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2513);
    }
    stringBuffer.append(TEXT_2514);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_2515);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.InternalEObject"));
    stringBuffer.append(TEXT_2516);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_2517);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.common.notify.NotificationChain"));
    stringBuffer.append(TEXT_2518);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2519);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EReference"));
    stringBuffer.append(TEXT_2520);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EReference"));
    stringBuffer.append(TEXT_2521);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EReference"));
    stringBuffer.append(TEXT_2522);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.resource.Resource.Internal"));
    stringBuffer.append(TEXT_2523);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_2524);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2525);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2526);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2527);
    
		Set<String> inititalizedFeatureSet = oclAnnotationsHelper.getAllInitializedFeatures();
  		String[] inititalizedFeatureArray = inititalizedFeatureSet.toArray(new String[inititalizedFeatureSet.size()]);
    stringBuffer.append(TEXT_2528);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2529);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2530);
    stringBuffer.append(inititalizedFeatureArray[0]);
    for (int i = 1; i <  inititalizedFeatureArray.length; i++) {
    stringBuffer.append(TEXT_2531);
    stringBuffer.append(inititalizedFeatureArray[i]);
    }
    stringBuffer.append(TEXT_2532);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2533);
    stringBuffer.append(genModel.getImportedName("java.util.List"));
    stringBuffer.append(TEXT_2534);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2535);
    stringBuffer.append(genModel.getImportedName("java.util.ArrayList"));
    stringBuffer.append(TEXT_2536);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2537);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2538);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2539);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_2540);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2541);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2542);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_2543);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2544);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2545);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2546);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2547);
    stringBuffer.append(genModel.getNonNLS(1));
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(TEXT_2548);
    stringBuffer.append(genModel.getImportedName("java.util.Arrays"));
    stringBuffer.append(TEXT_2549);
    stringBuffer.append(genModel.getImportedName("java.util.Comparator"));
    stringBuffer.append(TEXT_2550);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2551);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2552);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2553);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2554);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2555);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclMutlitypeComparisonUtil"));
    stringBuffer.append(TEXT_2556);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2557);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2558);
    stringBuffer.append(genModel.getNonNLS(1));
    stringBuffer.append(genModel.getNonNLS(2));
    stringBuffer.append(TEXT_2559);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2560);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_2561);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2562);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2563);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2564);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Query"));
    stringBuffer.append(TEXT_2565);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2566);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2567);
    stringBuffer.append(genClass.getQualifiedClassifierAccessor());
    stringBuffer.append(TEXT_2568);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2569);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_2570);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2571);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEvaluator"));
    stringBuffer.append(TEXT_2572);
    stringBuffer.append(genModel.getImportedName("java.util.HashMap"));
    stringBuffer.append(TEXT_2573);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.ETypedElement"));
    stringBuffer.append(TEXT_2574);
    stringBuffer.append(genModel.getImportedName("java.lang.Object"));
    stringBuffer.append(TEXT_2575);
    stringBuffer.append(genModel.getImportedName("java.lang.Throwable"));
    stringBuffer.append(TEXT_2576);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2577);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2578);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2579);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2580);
    stringBuffer.append(genModel.getImportedName("java.util.Map"));
    stringBuffer.append(TEXT_2581);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2582);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2583);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCLExpression"));
    stringBuffer.append(TEXT_2584);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclEmfUtil"));
    stringBuffer.append(TEXT_2585);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ecore.OCL.Helper"));
    stringBuffer.append(TEXT_2586);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EClassifier"));
    stringBuffer.append(TEXT_2587);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.util.TypeUtil"));
    stringBuffer.append(TEXT_2588);
    stringBuffer.append(genModel.getNonNLS());
    stringBuffer.append(TEXT_2589);
    stringBuffer.append(genModel.getImportedName("org.eclipse.ocl.ParserException"));
    stringBuffer.append(TEXT_2590);
    stringBuffer.append(genModel.getImportedName("org.xocl.core.util.XoclErrorHandler"));
    stringBuffer.append(TEXT_2591);
    stringBuffer.append(genPackage.getImportedPackageInterfaceName());
    stringBuffer.append(TEXT_2592);
    } else if (oclAnnotationsHelper.isInitializable()) {
	Set<String> inititalizedFeatureSet = oclAnnotationsHelper.getDirectInitializedFeatures();
	String[] inititalizedFeatureArray = inititalizedFeatureSet.toArray(new String[inititalizedFeatureSet.size()]);
	if (inititalizedFeatureArray.length != 0) { 
    stringBuffer.append(TEXT_2593);
    if (genModel.useClassOverrideAnnotation()) {
    stringBuffer.append(TEXT_2594);
    } // endif
    stringBuffer.append(TEXT_2595);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2596);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2597);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2598);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2599);
    stringBuffer.append(inititalizedFeatureArray[0]);
    for (int i = 1; i <  inititalizedFeatureArray.length; i++) {
    stringBuffer.append(TEXT_2600);
    stringBuffer.append(inititalizedFeatureArray[i]);
    }
    stringBuffer.append(TEXT_2601);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2602);
    stringBuffer.append(genModel.getImportedName("org.eclipse.emf.ecore.EStructuralFeature"));
    stringBuffer.append(TEXT_2603);
    	} // endif
  } // endif

} // endif

    stringBuffer.append(TEXT_2604);
    stringBuffer.append(isInterface ? " " + genClass.getInterfaceName() : genClass.getClassName());
    // TODO fix the space above
    genModel.emitSortedImports();
    stringBuffer.append(TEXT_2605);
    return stringBuffer.toString();
  }
}
