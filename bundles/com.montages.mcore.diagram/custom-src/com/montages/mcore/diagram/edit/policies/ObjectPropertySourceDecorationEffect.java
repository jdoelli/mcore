package com.montages.mcore.diagram.edit.policies;

import org.eclipse.ocl.util.Tuple;
import org.eclipse.swt.graphics.Color;

import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.edit.policies.base.ImpactAnalyzerVisualEffectEditPolicyBase;

/**
 * @generated
 */
public class ObjectPropertySourceDecorationEffect extends ImpactAnalyzerVisualEffectEditPolicyBase {

	/**
	 * @generated
	 */
	public static final String KEY = ObjectPropertySourceDecorationEffect.class.getName() + ":KEY";

	/**
	 * @generated
	 */
	@Override
	protected void setVisualEffectValue(Object value) {
		Tuple<?, ?> tupleValue = (Tuple<?, ?>) value;
		Integer red = (Integer) tupleValue.getValue("red");
		Integer green = (Integer) tupleValue.getValue("green");
		Integer blue = (Integer) tupleValue.getValue("blue");
		Color color = new Color(null, red, green, blue);
		getHostImpl().getPrimaryShape().setForegroundColor(color);

	}

	/**
	 * @generated
	 */
	@Override
	protected MPropertyInstanceEditPart getHostImpl() {
		return (MPropertyInstanceEditPart) super.getHostImpl();
	}

	/**
	 * @generated
	 */
	@Override
	protected String getExpressionBody() {
		return "" + //
				" if self.containingObject.type.allProperties()->includes(self.property) " + //
				" then Tuple {red: Integer = 45, green: Integer = 155, blue: Integer = 214} else Tuple {red: Integer = 255, green: Integer = 0, blue: Integer = 0} " + //
				" endif";
	}
}
