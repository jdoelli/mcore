package com.montages.mcore.diagram.edit.policies;

import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;

import com.montages.mcore.MClassifier;
import com.montages.mcore.diagram.commands.AddCustomCanonicalEditPolicy;
import com.montages.mcore.diagram.commands.CreateClassifierViewCommand;
import com.montages.mcore.diagram.commands.CreateObjectViewCommand;
import com.montages.mcore.diagram.commands.RemoveCustomCanonicalCommand;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;

public class CustomMPacakgeCreationEditPolicy extends CreationEditPolicyWithCustomReparent {

	public CustomMPacakgeCreationEditPolicy() {
		super(McoreVisualIDRegistry.TYPED_INSTANCE);
	}

	@Override
	protected Command getCreateElementAndViewCommand(CreateViewAndElementRequest request) {
		Command createCommand = super.getCreateElementAndViewCommand(request);
		if (createCommand != null && getHost().getEditPolicy(CustomMPackageCanonicalEditPolicy.ROLE) != null) {
			// canonical is listening domain and create view for new added elements
			// so we turn it off, create new element and turn it on again
			CompoundCommand result = new CompoundCommand();
			result.add(new RemoveCustomCanonicalCommand(getHost()));
			result.add(createCommand);
			result.add(new AddCustomCanonicalEditPolicy(getHost()));
			createCommand = result;
		}
		return createCommand;
	}

	protected Command getCreateCommand(CreateViewRequest request) {
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();

		CompositeTransactionalCommand result = new CompositeTransactionalCommand(editingDomain, "Create nodes' views");

		List<? extends ViewDescriptor> viewDescriptors = request.getViewDescriptors();
		Iterator<? extends ViewDescriptor> descriptors = viewDescriptors.iterator();

		while (descriptors.hasNext()) {
			CreateViewRequest.ViewDescriptor descriptor = (CreateViewRequest.ViewDescriptor) descriptors.next();
			ICommand createCommand;
			if (descriptor.getElementAdapter() != null) {
				IElementType elementType = descriptor.getElementAdapter().getAdapter(IElementType.class);
				EObject element = descriptor.getElementAdapter().getAdapter(EObject.class);
				if (McoreElementTypes.MClassifier_2001.equals(elementType) || element instanceof MClassifier) {
					createCommand = new CreateClassifierViewCommand(editingDomain, descriptor, (View) getHost().getModel());
				} else if (McoreElementTypes.MObject_2002.equals(elementType) || McoreElementTypes.MObject_2003.equals(elementType) || element instanceof MObject) {
					createCommand = new CreateObjectViewCommand(editingDomain, descriptor, (View) getHost().getModel());
				} else {
					createCommand = new CreateCommand(editingDomain, descriptor, (View) (getHost().getModel()));
				}
			} else {
				createCommand = new CreateCommand(editingDomain, descriptor, (View) (getHost().getModel()));
			}
			result.compose(createCommand);
		}
		return new ICommandProxy(result.reduce());
	}

	@Override
	public MPackageEditPart getHost() {
		return (MPackageEditPart) super.getHost();
	}

}
