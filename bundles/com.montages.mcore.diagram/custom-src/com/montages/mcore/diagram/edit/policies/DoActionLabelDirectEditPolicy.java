package com.montages.mcore.diagram.edit.policies;

import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.LabelDirectEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.l10n.DiagramUIMessages;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.commands.wrappers.EMFtoGMFCommandWrapper;
import com.montages.mcore.diagram.commands.wrappers.GEFtoEMFCommandWrapper;
import com.montages.mcore.diagram.commands.wrappers.GMFtoEMFCommandWrapper;

public class DoActionLabelDirectEditPolicy extends LabelDirectEditPolicy {

	@Override
	public Command getCommand(Request request) {
		Command parserCommand = super.getCommand(request);
		if (parserCommand == null || false == parserCommand instanceof ICommandProxy) {
			return null;
		}
		TransactionalEditingDomain editingDomain = ((IGraphicalEditPart) getHost()).getEditingDomain();
		CompositeTransactionalCommand cc = new CompositeTransactionalCommand(editingDomain, DiagramUIMessages.AddCommand_Label);
		ICommandProxy transactCommand = (ICommandProxy) parserCommand;
		ICommand iCommand = transactCommand.getICommand();
		CompositeCommand composite = new CompositeCommand("");//$NON-NLS-1$
		composite.add(EMFtoGMFCommandWrapper.wrap(GEFtoEMFCommandWrapper.wrap(parserCommand)));
		composite.add(new DoActionResultCreateViewCommand(iCommand, findDiagramEditPart(getHost())));
		cc.compose(composite);
		return new ICommandProxy(cc.reduce());
	}

	public static class DoActionResultCreateViewCommand extends org.eclipse.gmf.runtime.common.core.command.AbstractCommand {

		private final ICommand myPreviousCommand;

		private final IGraphicalEditPart myHost;

		public DoActionResultCreateViewCommand(ICommand previousCommand, IGraphicalEditPart host) {
			super("Create Views for Result");
			myPreviousCommand = previousCommand;
			myHost = host;
		}

		protected ICommand getPreviousCommand() {
			return myPreviousCommand;
		}

		protected Collection<?> getPreviusCommandResult() {
			CommandResult commandResult = myPreviousCommand.getCommandResult();
			IStatus status = commandResult.getStatus();
			if (commandResult == null || !status.isOK()) {
				return Collections.emptyList();
			}
			Object returnValue = commandResult.getReturnValue();
			if (returnValue == null || false == returnValue instanceof Collection) {
				return Collections.emptyList();
			}
			return (Collection<?>) returnValue;
		}

		private CreateCommand createDoActionResultViewCommand(EObject semanticElement, IAdaptable parentAdapter) {
			TransactionalEditingDomain editingDomain = myHost.getEditingDomain();
			ViewDescriptor viewDescriptor = new ViewDescriptor(new EObjectAdapter(semanticElement), Node.class, null, ViewUtil.APPEND, true, myHost.getDiagramPreferencesHint());
			// return new CreateCommand(editingDomain, viewDescriptor, container);
			return new CreateCommand(editingDomain, viewDescriptor, myHost.getNotationView(), parentAdapter);
		}

		private void createViewsCommands(EObject parent, IAdaptable viewAdapter, Collection<?> elements, CompositeCommand result) {
			if (parent == null) {
				return;
			}
			for (Object nextObject : elements) {
				if (nextObject instanceof EObject) {
					EObject child = (EObject)nextObject;
					if (child.eContainer() == parent) {
						CreateCommand createCommand = createDoActionResultViewCommand(child, viewAdapter);
						result.add(createCommand);
						createViewsCommands(createCommand.getViewDescriptor().getElementAdapter().getAdapter(EObject.class), createCommand.getViewDescriptor(),elements, result);
					}
				}
			}
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
			CompositeCommand result = new CompositeCommand("");
			final View hostView = myHost.getNotationView();
			Collection<?> elements = getPreviusCommandResult();
			if (elements != null) {
				createViewsCommands(hostView.getElement(), new IAdaptable() {

					@Override
					public <T> T getAdapter(Class<T> adapter) {
						if (View.class == adapter) {
							return adapter.cast(hostView);
						}
						return null;
					}
				}, elements, result);
			}
			if (result.isEmpty()) { //|| !result.canExecute()) {
				return CommandResult.newOKCommandResult();
			}
			myHost.getEditingDomain().getCommandStack().execute(GMFtoEMFCommandWrapper.wrap(result));
			return CommandResult.newOKCommandResult();
		}

		@Override
		protected CommandResult doRedoWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
			return doExecuteWithResult(progressMonitor, info);
		}

		@Override
		protected CommandResult doUndoWithResult(IProgressMonitor progressMonitor, IAdaptable info) throws ExecutionException {
			//correct behaviour
			return CommandResult.newOKCommandResult();
		}
	}

	private DiagramEditPart findDiagramEditPart(EditPart host) {
		if (host == null) {
			return null;
		}
		if (host instanceof DiagramEditPart) {
			return (DiagramEditPart) host;
		}
		return findDiagramEditPart(host.getParent());
	}

	/**
	 * Some parent views don't exist and we replace parent view in command with it's descriptor
	 * 
	 *
	 */
	private static class CreateCommand extends org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand {

		private IAdaptable viewAdapter;

		public CreateCommand(TransactionalEditingDomain editingDomain, ViewDescriptor viewDescriptor, View containerView, IAdaptable adapter) {
			super(editingDomain, viewDescriptor, containerView);
			viewAdapter = adapter;
		}

		protected View getContainerView() {
			return viewAdapter.getAdapter(View.class);
		}

		public ViewDescriptor getViewDescriptor() {
			return super.getViewDescriptor();
		}

		/**
		 * We allow command execute to let other commands in chain create view on a diagram in case when some views cannot be created
		 * because they restrict executing any command in chain
		 * but we ask super.canExecute before execute to avoid assertion errors and unexpected results
		 */
		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			if (super.canExecute()) {
				return super.doExecuteWithResult(monitor, info);
			}
			return CommandResult.newOKCommandResult();
		}
	}
}
