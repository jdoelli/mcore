package com.montages.mcore.diagram.edit.policies.creators;

import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalCommandStack;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.edit.policies.PolicyUtils;

/**
 * CreateLink class is a command to create link between new node which doesn't exist yet and related element on a diagram.<br>
 * If the related element doesn't have any nodes on the diagram or the new node element is not created then link won't be created.
 */
public abstract class CreateLink extends CreateCommand {

	private final CreateViewRequest.ViewDescriptor myCreatedNodeDescriptor;

	private final View myDiagram;

	/**
	 * Initialize CreateLink with following parameters:<br>
	 * @param domain is a {@link TransactionalCommandStack} for super {@link CreateCommand}
	 * @param label is a simpe description of command
	 * @param descr is a link descriptor
	 * @param createElementDescr is a descriptor for created node
	 * @param containerView is a parent view for created link
	 */
	public CreateLink(TransactionalEditingDomain domain, String label, CreateViewRequest.ViewDescriptor descr, View containerView, CreateViewRequest.ViewDescriptor createElementDescr) {
		super(domain, descr, containerView);
		myCreatedNodeDescriptor = createElementDescr;
		myDiagram = containerView;
	}

	/**
	 * Target view for created link
	 */
	protected abstract View getTarget();

	/**
	 * Source view for created link
	 */
	protected abstract View getSource();

	/**
	 * Setup
	 */
	protected abstract void setup(Edge link);

	/**
	 * Create link and set source and target view
	 * <br>
	 * Link won't be create if source or target is <code>null</code>
	 */
	@Override
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		View sourceView = getSource();
		View targetView = getTarget();

		if (targetView == null || sourceView == null) {
			return CommandResult.newOKCommandResult();
		}
		super.doExecuteWithResult(monitor, info);
		Edge link = getEdgeView();
		link.setSource(sourceView);
		link.setTarget(targetView);
		setup(getEdgeView());
		return CommandResult.newOKCommandResult();
	}

	/**
	 * Get link view from it's descriptor
	 */
	protected Edge getEdgeView() {
		return (Edge) getViewDescriptor().getAdapter(View.class);
	}

	/**
	 * Get new node descriptor
	 */
	protected ViewDescriptor getCreatedElementDescriptor() {
		return myCreatedNodeDescriptor;
	}

	/**
	 * 
	 * @return View from created element view descriptor
	 */
	protected View getCreatedNodeView() {
		return getView(getCreatedElementDescriptor());
	}

	protected static View getView(ViewDescriptor descriptor) {
		return (View)descriptor.getAdapter(View.class);
	}

	/**
	 * 
	 * @param element of a view
	 * 
	 * @return first View which is founded<code>null</code>
	 */
	protected View findViewOnDiagram(EObject element) {
		if (element == null) {
			return null;
		}
		List<View> views = PolicyUtils.findView(element, myDiagram);
		return views.isEmpty() ? null : views.get(0);
	}

	/**
	 * Abstract source link implementation of CreateLink command
	 */
	public abstract static class CreateSourceLink extends CreateLink {

		public CreateSourceLink(TransactionalEditingDomain domain, String label, ViewDescriptor descr, View containerView, ViewDescriptor createElementDescr) {
			super(domain, label, descr, containerView, createElementDescr);
		}

		protected abstract EObject getRelatedElement();

		@Override
		protected View getTarget() {
			EObject sourceSemantic = getRelatedElement();
			if (sourceSemantic == null) {
				return null;
			}
			return findViewOnDiagram(sourceSemantic);
		}

		@Override
		protected View getSource() {
			return getCreatedNodeView();
		}
	}

	/**
	 * Abstract target link implementation of CreateLink command
	 */
	public abstract static class CreateTargetLink extends CreateLink {

		public CreateTargetLink(TransactionalEditingDomain domain, String label, ViewDescriptor descr, View containerView, ViewDescriptor createElementDescr) {
			super(domain, label, descr, containerView, createElementDescr);
		}

		protected abstract EObject getRelatedElement();

		@Override
		protected View getSource() {
			EObject sourceSemantic = getRelatedElement();
			if (sourceSemantic == null) {
				return null;
			}
			return findViewOnDiagram(sourceSemantic);
		}

		@Override
		protected View getTarget() {
			return getCreatedNodeView();
		}
	}
}