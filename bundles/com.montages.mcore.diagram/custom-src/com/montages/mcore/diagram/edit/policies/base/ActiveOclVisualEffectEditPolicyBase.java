package com.montages.mcore.diagram.edit.policies.base;

import org.eclipse.gmf.tooling.runtime.GMFToolingRuntimePlugin;
import org.eclipse.gmf.tooling.runtime.ocl.tracker.OclTrackerFactory;

public abstract class ActiveOclVisualEffectEditPolicyBase extends AbstractOclVisualEffectEditPolicy {

	@Override
	protected OclTrackerFactory getOclTrackerFactory() {
		return GMFToolingRuntimePlugin.getInstance().getOclTrackerFactory(OclTrackerFactory.Type.DEFAULT_GMFT);
	}

}
