package com.montages.mcore.diagram.edit.policies;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.SetBoundsCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.commands.ShowHideElementsRequest;

public class ShowHideSlotEditPolicy extends AbstractEditPolicy {

	public static final String SHOW_HIDE_SLOT_POLICY = "Show/Hide slot policy"; //$NON-NLS-1$

	/**
	 * @see org.eclipse.gmf.runtime.diagram.ui.editpolicies.PropertyHandlerEditPolicy#getCommand(org.eclipse.gef.Request)
	 *
	 * @param request
	 * @return
	 */
	@Override
	public Command getCommand(Request request) {
		if (request instanceof ShowHideElementsRequest) {
			ShowHideElementsRequest req = (ShowHideElementsRequest) request;
			if (req.getHidedEditPart() != null) {
				return getDestroyViewCommand(req.getHidedEditPart());
			} else if (req.getContainer() != null && req.getSemanticElement() != null) {
				return getCreateViewCommand(req.getContainer(), req.getSemanticElement(), req.getLocation());
			}
		}
		return null;
	}

	protected Command getCreateViewCommand(View container, EObject semanticElement, Point location) {
		ViewDescriptor viewDescriptor = new ViewDescriptor(new EObjectAdapter(semanticElement), Node.class, null, ViewUtil.APPEND, true,
				((IGraphicalEditPart) this.getHost()).getDiagramPreferencesHint());
		CompoundCommand compositeCmd = new CompoundCommand("Create View");//$NON-NLS-1$
		CreateCommand cmd = new CreateCommand(getEditingDomain(), viewDescriptor, container);
		if (cmd.canExecute()) {
			compositeCmd.add(new ICommandProxy(cmd));
		}
		if (location != null) {
			SetBoundsCommand setBoundsCommand = new SetBoundsCommand(getEditingDomain(), "move", (IAdaptable) cmd.getCommandResult().getReturnValue(), location); //$NON-NLS-1$
			if (setBoundsCommand.canExecute()) {
				compositeCmd.add(new ICommandProxy(setBoundsCommand));
			}
		}
		return compositeCmd;
	}

	protected Command getDestroyViewCommand(EditPart hidedEditPart) {
		DeleteCommand cmd = new DeleteCommand(getEditingDomain(), (View) hidedEditPart.getModel());
		return new ICommandProxy(cmd);
	}

	protected TransactionalEditingDomain getEditingDomain() {
		return ((IGraphicalEditPart) getHost()).getEditingDomain();
	}
}