package com.montages.mcore.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gef.requests.GroupRequest;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.CompositeCommand;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.core.commands.SetPropertyCommand;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.CommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.DiagramEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.internal.properties.Properties;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateConnectionViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.EdgeImpl;
import org.eclipse.gmf.tooling.runtime.update.UpdaterLinkDescriptor;

import com.montages.mcore.diagram.commands.CreateLinkCommandWithEnds;
import com.montages.mcore.diagram.commands.ShowHideRelatedElementsRequest;
import com.montages.mcore.diagram.commands.ShowHideRelatedElementsRequest.ShowHideKind;
import com.montages.mcore.diagram.edit.policies.MPackageCanonicalEditPolicy.Domain2NotationPublic;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.part.McoreDiagramUpdater;
import com.montages.mcore.diagram.part.McoreLinkDescriptor;
import com.montages.mcore.diagram.tools.DiagramEditPartsUtil;
import com.montages.mcore.diagram.tools.Domain2Notation;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;

/**
 * The editPolicy used to show/hide related elements.
 */
public abstract class AbstractShowHideRelatedLinkEditPolicy extends AbstractEditPolicy {

	public static final String SHOW_HIDE_RELATED_ELEMENTS_ROLE = "ShowHideRelatedElementsEditPolicy"; //$NON-NLS-1$

	public AbstractShowHideRelatedLinkEditPolicy(final DiagramEditPart host) {
		setHost(host);
	}

	private DiagramEditPart findDiagramEditPart(EditPart host) {
		if (false == host instanceof IGraphicalEditPart) {
			return null;
		}
		return (DiagramEditPart) ((IGraphicalEditPart)host).getTopGraphicEditPart().getParent();
	}

	private View getDiagramView() {
		return findDiagramEditPart(getHost()).getNotationView();
	}

	/**
	 * Gets the command.
	 *
	 * @param req
	 *            the req
	 * @return the command
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#getCommand(org.eclipse.gef.Request)
	 */
	@Override
	public Command getCommand(final Request req) {
		if (understandsRequest(req)) {
			final ShowHideRelatedElementsRequest request = (ShowHideRelatedElementsRequest) req;
			// 0. Obtain the required informations
			// --the map between semantic eobjects and existing views
			// final Map<EObject, View> domain2NotationMap = new HashMap<EObject, View>();
			final Domain2Notation domain2NotationMap = new Domain2Notation();
			// -- the map between selected EditPart and the semantic existing links
			final Map<EditPart, Set<EObject>> availableLinks = new HashMap<EditPart, Set<EObject>>();
			// -- the list of the links which are currently visible on the diagram
			final Set<EObject> visibleLinks = new HashSet<EObject>();
			// -- the link descriptors
			final Set<UpdaterLinkDescriptor> linksDescriptors = new HashSet<UpdaterLinkDescriptor>();
			// 1. resolve all to avoid concurrent modification exception during the call of the method collectPartRelatedLinks
			EcoreUtil.resolveAll(getEditingDomain().getResourceSet());
			final ViewAndViewDescriptorWrapper domain2NotationWrapper = new ViewAndViewDescriptorWrapper(domain2NotationMap);
			EditPart diagramEditPart = findDiagramEditPart(getHost());
			// 1. bis To register all EditPart in the global visualIDRegistry
			diagramEditPart.refresh();
			// 2. we associate each view to a semantic element
			domain2NotationMap.mapModel(diagramEditPart.getAdapter(View.class));
			// 3.we collect the link descriptors
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = null;
			Collection<McoreLinkDescriptor> notVisibleConnections = getNotVisibleConnections();
			for (final EditPart currentEp : request.getSelectedEditParts()) {
				final View view = currentEp.getAdapter(View.class);
				if (view == null) {
					continue;
				}
				if (crossReferences == null) {
					crossReferences = EcoreUtil.CrossReferencer.find(view.eResource().getResourceSet().getResources());
				}
				final Collection<? extends UpdaterLinkDescriptor> desc = collectPartRelatedLinks(view, domain2NotationMap, crossReferences);
				linksDescriptors.addAll(desc);
				final Set<EObject> modelLinks = new HashSet<EObject>();
				final Iterator<? extends UpdaterLinkDescriptor> iter = desc.iterator();
				while (iter.hasNext()) {
					final UpdaterLinkDescriptor current = iter.next();
					final EObject link = current.getModelElement();
					EObject target = current.getDestination();
					EObject source = current.getSource();
					if (target != null && source != null) {
						modelLinks.add(link);
					}
					// we build the list of the visible links
					View linkView = findLinkView(view, link, domain2NotationMap);
					if (linkView != null && linkView.isVisible()) {
						visibleLinks.add(link);
					}
				}
				availableLinks.put(currentEp, modelLinks);
			}
			switch (request.getMode()) {
			case OPEN_DIALOG:
				return getShowHideRelatedLinkCommandWithDialog(request.getSelectedEditParts(), availableLinks, visibleLinks, domain2NotationMap, linksDescriptors);
			case SHOW_ALL_LINK_BETWEEN_SELECTED_ELEMENT:
				final Collection<EObject> possibleEnds = new HashSet<EObject>();
				for (final EditPart currentIGraphical : request.getSelectedEditParts()) {
					possibleEnds.add(currentIGraphical.getAdapter(EObject.class));
				}
				final Collection<UpdaterLinkDescriptor> toRemove = new ArrayList<UpdaterLinkDescriptor>();
				for (final UpdaterLinkDescriptor current : linksDescriptors) {
					final EObject source = current.getSource();
					final EObject target = current.getDestination();
					if (!possibleEnds.contains(source) || !possibleEnds.contains(target)) {
						toRemove.add(current);
					}
				}
				linksDescriptors.removeAll(toRemove);
			case SHOW_ALL_LINK_IN_DIAGRAM:
				final List<EObject> allLinks = new ArrayList<EObject>();
				for (final Collection<EObject> currentColl : availableLinks.values()) {
					allLinks.addAll(currentColl);
				}
				return new ICommandProxy(getShowHideRelatedElementsCommand(getEditingDomain(), null, allLinks, Collections.emptyList(), domain2NotationWrapper, notVisibleConnections, linksDescriptors));
			case SHOW_RELATED_ELEMENTS:
				final CompositeCommand compositeCommand = new CompositeCommand(ShowHideKind.SHOW_RELATED_ELEMENTS.toString());// $NON-NLS-1$
				for (EditPart nextEP : availableLinks.keySet()) {
					View nextEPView = nextEP.getAdapter(View.class);
					final Collection<?> res = (Collection<?>) availableLinks.get(nextEP);
					final Set<?> toAdd = new HashSet<Object>(res);
					// toAdd.removeAll(visibleLinks);
					List<EObject> toShow = extractEObjectFromSet(toAdd);
					compositeCommand.add(getShowHideRelatedElementsCommand(getEditingDomain(), nextEPView, toShow, Collections.emptyList(), domain2NotationWrapper, notVisibleConnections, linksDescriptors));
				}
				return new ICommandProxy(compositeCommand);
			default:
				break;
			}
		}
		return null;
	}

	/**
	 * Collects all related links for view.
	 *
	 * @param view
	 *            the view
	 * @param domain2NotationMap
	 *            the domain2 notation map
	 * @return linkdescriptors
	 */
	protected Collection<UpdaterLinkDescriptor> collectPartRelatedLinks(final View view, final Domain2Notation domain2NotationMap, final Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		Collection<UpdaterLinkDescriptor> result = new LinkedList<UpdaterLinkDescriptor>();
		// We must prevent duplicate descriptors
		List<? extends UpdaterLinkDescriptor> outgoingDescriptors = McoreDiagramUpdater.getOutgoingLinks(view);
		cleanAdd(result, view, outgoingDescriptors, domain2NotationMap);
		List<? extends UpdaterLinkDescriptor> incomingDescriptors = McoreDiagramUpdater.getIncomingLinks(view, crossReferences);
		cleanAdd(result, view, incomingDescriptors, domain2NotationMap);
		if (!domain2NotationMap.containsKey(view.getElement()) || view.getEAnnotation("Shortcut") == null) { //$NON-NLS-1$
			domain2NotationMap.putView(view);
		}
		return removeInvalidLinkDescriptor(result);
	}

	/**
	 * Gets the current diagram.
	 *
	 * @return the current diagram
	 *         the current diagram; hosting this edit policy
	 */
	protected Diagram getCurrentDiagram() {
		return getHost().getAdapter(Diagram.class);
	}

	/**
	 * Removes the invalid link descriptor.
	 *
	 * @param descriptors
	 *            the link descriptor
	 * @return the collection
	 *         the collection of link descriptors without some invalid descriptor (we get this case when the link doesn't have source AND target, but
	 *         only ends
	 */
	protected Collection<UpdaterLinkDescriptor> removeInvalidLinkDescriptor(final Collection<UpdaterLinkDescriptor> descriptors) {
		final Collection<UpdaterLinkDescriptor> toRemove = new ArrayList<UpdaterLinkDescriptor>();
		final Collection<UpdaterLinkDescriptor> toAdd = new ArrayList<UpdaterLinkDescriptor>();
		for (final UpdaterLinkDescriptor current : descriptors) {
			if (current.getModelElement() == null) {
				final IElementType elementType = current.getSemanticAdapter().getAdapter(IElementType.class);
				final EdgeWithNoSemanticElementRepresentationImpl noSemantic = new EdgeWithNoSemanticElementRepresentationImpl(current.getSource(), current.getDestination(),
						((IHintedType) elementType).getSemanticHint());
				final UpdaterLinkDescriptor replacement = new UpdaterLinkDescriptor(current.getSource(), current.getDestination(), noSemantic, elementType, current.getVisualID());
				toRemove.add(current);
				toAdd.add(replacement);
			}
		}
		descriptors.removeAll(toRemove);
		descriptors.addAll(toAdd);
		return descriptors;
	}

	/**
	 * Clean add.
	 *
	 * @param result
	 *            the result of the call to this method
	 * @param view
	 *            the current view
	 * @param descriptors
	 *            links descriptors for links related to this view
	 * @param domain2NotationMap
	 *            the map between model element and views
	 */
	protected void cleanAdd(Collection<UpdaterLinkDescriptor> result, View view, List<? extends UpdaterLinkDescriptor> descriptors, final Domain2Notation domain2NotationMap) {
		for (UpdaterLinkDescriptor updaterLinkDescriptor : descriptors) {
			if (cleanContains(result, updaterLinkDescriptor)) {
				continue;
			}
			// check owner
			if (!isOwner(view, updaterLinkDescriptor)) {
				continue;
			}
			result.add(updaterLinkDescriptor);
		}
	}

	private boolean isOwner(View view, UpdaterLinkDescriptor descriptor) {
		return isOwner(view.getElement(), descriptor);
	}
	
	/**
	 * Checks if is owner.
	 *
	 * @param view
	 *            the view
	 * @param descriptor
	 *            the descriptor
	 * @return true, if is owner
	 */
	private boolean isOwner(EObject viewElement, UpdaterLinkDescriptor descriptor) {
		EObject source = descriptor.getSource();
		EObject dest = descriptor.getDestination();
		if (source != null && source.equals(viewElement)) {
			return true;
		}
		if (dest != null && dest.equals(viewElement)) {
			return true;
		}
		return false;
	}

	/**
	 * Detect if similar descriptor already exist in given collection.
	 *
	 * @param collection
	 *            the collection of unique ingoing and outgoing links descriptors
	 * @param umlLinkDescriptor
	 *            the descriptor to search
	 * @return true if already exist
	 */
	private boolean cleanContains(Collection<? extends UpdaterLinkDescriptor> collection, UpdaterLinkDescriptor umlLinkDescriptor) {
		for (UpdaterLinkDescriptor descriptor : collection) {
			if (descriptor.getModelElement() == umlLinkDescriptor.getModelElement() && descriptor.getSource() == umlLinkDescriptor.getSource()
					&& descriptor.getDestination() == umlLinkDescriptor.getDestination() && descriptor.getVisualID() == umlLinkDescriptor.getVisualID()) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Understands request.
	 *
	 * @param request
	 *            the request
	 * @return true, if successful
	 * @see org.eclipse.gef.editpolicies.AbstractEditPolicy#understandsRequest(org.eclipse.gef.Request)
	 */
	@Override
	public boolean understandsRequest(final Request request) {
		return request instanceof ShowHideRelatedElementsRequest;
	}

	/**
	 * Gets the show hide related link command with dialog.
	 *
	 * @param selectedEditParts
	 *            the selected edit parts
	 * @param availableLinks
	 *            a map with the selected edit parts and their available links
	 * @param visibleLinks
	 *            the visible links
	 * @param domain2NotationMap
	 *            a map between existing views and semantic elements
	 * @param linksDescriptors
	 *            the link descriptors
	 * @return the show hide related link command with dialog
	 *         the command to open a dialog and Show/Hide the related links
	 */
	protected abstract Command getShowHideRelatedLinkCommandWithDialog(final Collection<EditPart> selectedEditParts, final Map<EditPart, Set<EObject>> availableLinks, final Set<EObject> visibleLinks,
			final Domain2Notation domain2NotationMap, Collection<UpdaterLinkDescriptor> linksDescriptors);

	protected TransactionalEditingDomain getEditingDomain() {
		return ((IGraphicalEditPart) getHost()).getEditingDomain();
	}

	protected ICommand getShowHideRelatedElementsCommand(final TransactionalEditingDomain domain, final ICommand previousCommand, final Set<EObject> initialSelection,
			final ViewAndViewDescriptorWrapper domain2NotationWrapper, Collection<McoreLinkDescriptor> notVisibleConnections, final Collection<UpdaterLinkDescriptor> linkDescriptor) {
		final ICommand computeCommand = new AbstractTransactionalCommand(domain, "Compute Result", null) {//$NON-NLS-1$

			@Override
			protected CommandResult doExecuteWithResult(final IProgressMonitor monitor, final IAdaptable info) throws ExecutionException {
				final Object returnedValue = previousCommand.getCommandResult().getReturnValue();
				if (!previousCommand.getCommandResult().getStatus().isOK()) {
					return previousCommand.getCommandResult();
				}
				if (returnedValue instanceof Collection<?>) {
					final Collection<?> res = (Collection<?>) returnedValue;
					final Set<?> toAdd = new HashSet<Object>(res);
					toAdd.removeAll(initialSelection);
					final Set<?> toHide = new HashSet<Object>(initialSelection);
					toHide.removeAll(res);
					List<EObject> toShow = extractEObjectFromSet(toAdd);
					final ICommand cmd = getShowHideRelatedElementsCommand(domain, null, toShow, toHide, domain2NotationWrapper, notVisibleConnections, linkDescriptor);
					if (cmd.canExecute()) {
						cmd.execute(monitor, info);
					} else {
						McoreDiagramEditorPlugin.getInstance().logError("The command is not executable", new IllegalStateException("The command is not executable")); //$NON-NLS-1$ )
					}
				}
				return CommandResult.newOKCommandResult(returnedValue);
			}
		};
		return computeCommand;
	}

	private List<EObject> extractEObjectFromSet(Set<?> input) {
		final List<EObject> result = new ArrayList<EObject>();
		Iterator<?> it = input.iterator();
		while (it.hasNext()) {
			Object next = it.next();
			if (next instanceof EObject) {
				result.add((EObject) next);
			}
			if (next instanceof Set) {
				result.addAll(extractEObjectFromSet((Set<?>) next));
			}
		}
		return result;
	}

	/**
	 * Gets the show hide related link command.
	 *
	 * @param domain
	 *            the editing domain
	 * @param toShow
	 *            the list of the link to show
	 * @param toHide
	 *            the list of the link to hide
	 * @param domain2NotationMap
	 *            a map linking the eobject to their view
	 * @param linkDescriptors
	 *            the link descriptor
	 * @return the show hide related link command
	 *         the command to show/hide the links, according to the args of this method
	 */
	protected final ICommand getShowHideRelatedElementsCommand(final TransactionalEditingDomain domain, View selectedView, final List<EObject> toShow, final Collection<?> toHide,
			final ViewAndViewDescriptorWrapper domain2NotationWrapper, Collection<McoreLinkDescriptor> notVisibleConnections,
			final Collection<? extends UpdaterLinkDescriptor> linkDescriptors) {
		final CompositeCommand compositeCommand = new CompositeCommand("Show/Hide Related Link");//$NON-NLS-1$
		Domain2Notation domain2NotationMap = domain2NotationWrapper.getDomain2NotationMap();
		for (EObject current : toShow) {
			if (current.eContainer() == selectedView.getElement() && current instanceof MPropertyInstance) {
				MPropertyInstance propertyInstance = (MPropertyInstance) current;
				for (MObject nextContainedMObject : propertyInstance.getInternalContainedObject()) {
					View nextView = domain2NotationMap.getFirstTopView(nextContainedMObject);
					final ICommand tmp = getShowLinkCommand(domain, current, nextView, nextContainedMObject, domain2NotationWrapper, notVisibleConnections, linkDescriptors);
					if (tmp != null && tmp.canExecute()) {
						compositeCommand.add(tmp);
					}
				}
				for (MObjectReference nextContainedMObject : propertyInstance.getInternalReferencedObject()) {
					MObject refMObject = nextContainedMObject.getReferencedObject();
					View nextView = domain2NotationMap.getFirstTopView(refMObject);
					final ICommand tmp = getShowLinkCommand(domain, current, nextView, refMObject, domain2NotationWrapper, notVisibleConnections, linkDescriptors);
					if (tmp != null && tmp.canExecute()) {
						compositeCommand.add(tmp);
					}
				}
			} else {
				final ICommand tmp = getShowLinkCommand(domain, current, selectedView, selectedView.getElement(), domain2NotationWrapper, notVisibleConnections, linkDescriptors);
				if (tmp != null && tmp.canExecute()) {
					compositeCommand.add(tmp);
				}
			}
		}
		for (final Object current : toHide) {
			if (current instanceof EObject) {
				final ICommand tmp = getHideLinkCommand(domain, (EObject) current, domain2NotationMap, linkDescriptors);
				if (tmp != null && tmp.canExecute()) {
					compositeCommand.add(tmp);
				}
			}
		}
		return compositeCommand;
	}

	/**
	 * Get the hide link command.
	 *
	 * @param domain
	 *            the editing domain
	 * @param linkToHide
	 *            the link to hide
	 * @param domain2NotationMap
	 *            the map between eobjects and views
	 * @param linkDescriptors
	 *            the link descriptors
	 * @return the hide link command
	 *         the command to hide the wanted link
	 */
	protected ICommand getHideLinkCommand(final TransactionalEditingDomain domain, final EObject linkToHide, final Domain2Notation domain2NotationMap,
			final Collection<? extends UpdaterLinkDescriptor> linkDescriptors) {
		final UpdaterLinkDescriptor descriptor = getLinkDescriptor(linkToHide, null, linkDescriptors);
		if (descriptor == null) {
			return null;
		}
		final View view = domain2NotationMap.getFirstTopView(linkToHide);
		final EditPart editPart = DiagramEditPartsUtil.getEditPartFromView(view, getHost());
		return new CommandProxy(editPart.getCommand(new GroupRequest(org.eclipse.gef.RequestConstants.REQ_DELETE)));
	}

	protected final UpdaterLinkDescriptor getLinkDescriptor(final EObject link, EObject currentEPSemantic, final Collection<? extends UpdaterLinkDescriptor> descriptors) {
		final Iterator<? extends UpdaterLinkDescriptor> iter = descriptors.iterator();
		while (iter.hasNext()) {
			final UpdaterLinkDescriptor current = iter.next();
			EObject nextLinkDescriptorSemantic = current.getModelElement();
			if (nextLinkDescriptorSemantic != link) {
				continue;
			}
			if (nextLinkDescriptorSemantic instanceof MPropertyInstance) {
				if (current.getSource() == currentEPSemantic || current.getDestination() == currentEPSemantic) {
					return current;
				}
			} else {
				return current;
			}
		}
		return null;
	}

	/**
	 * Gets the link descriptors.
	 *
	 * @param link
	 *            the link
	 * @param descriptors
	 *            the descriptors
	 * @return the link descriptors
	 */
	protected final List<UpdaterLinkDescriptor> getLinkDescriptors(final EObject link, final Collection<? extends UpdaterLinkDescriptor> descriptors) {
		List<UpdaterLinkDescriptor> updaterLinkDescriptors = new ArrayList<UpdaterLinkDescriptor>();
		final Iterator<? extends UpdaterLinkDescriptor> iter = descriptors.iterator();
		while (iter.hasNext()) {
			final UpdaterLinkDescriptor current = iter.next();
			if (current.getModelElement() == link) {
				updaterLinkDescriptors.add(current);
			}
		}
		return updaterLinkDescriptors;
	}

	/**
	 * Gets the show link command.
	 *
	 * @param domain
	 *            the editing domain to use for this command
	 * @param linkToShow
	 *            a link to show
	 * @param domain2NotationMap
	 *            the domain2 notation map
	 * @param linkDescriptors
	 *            the link descriptors
	 * @return the show link command
	 *         the command to display the link on the diagram
	 */
	private ICommand getShowLinkCommand(final TransactionalEditingDomain domain, final EObject linkToShow, View currentEP, EObject currentEPSemantic, 
			final ViewAndViewDescriptorWrapper domain2NotationWrapper, Collection<McoreLinkDescriptor> notVisibleConnections,
			final Collection<? extends UpdaterLinkDescriptor> linkDescriptors) {
		Domain2Notation domain2NotationMap = domain2NotationWrapper.getDomain2NotationMap();
		domain2NotationMap.mapModel(getHost().getAdapter(View.class));
		final View view = findLinkView(currentEP, linkToShow, domain2NotationMap);
		if (view != null) {
			return new SetPropertyCommand(domain, "Restore related linksCommand show view", new EObjectAdapter(view), Properties.ID_ISVISIBLE, Boolean.TRUE);//$NON-NLS-1$
		}
		// we need to recreate the view. we look for the link descriptor
		UpdaterLinkDescriptor descriptor = getLinkDescriptor(linkToShow, currentEPSemantic, linkDescriptors);
		if (linkToShow instanceof EdgeWithNoSemanticElementRepresentationImpl) {
			// we replace the specific link descriptor by a new one, with no model element (if not the view provider refuse to create the view
			final IElementType elementType = descriptor.getSemanticAdapter().getAdapter(IElementType.class);
			descriptor = new UpdaterLinkDescriptor(descriptor.getSource(), descriptor.getDestination(), elementType, descriptor.getVisualID());
		}
		if (descriptor == null) {
			return null;
		}
		
		CompositeCommand compositeCommand = new CompositeCommand("Restore All Related Elements");
		for (Object source : collectEObjectView(compositeCommand, domain2NotationWrapper, descriptor.getSource())) {
			provideNewLinkCommandsForNewCreatedNode(compositeCommand, source, notVisibleConnections, domain2NotationMap);
			for (Object target : collectEObjectView(compositeCommand, domain2NotationWrapper, descriptor.getDestination())) {
				provideNewLinkCommandsForNewCreatedNode(compositeCommand, target, notVisibleConnections, domain2NotationMap);
			}
		}
		return compositeCommand;
	}
	
	private void provideNewLinkCommandsForNewCreatedNode(CompositeCommand compositeCommand, Object node, Collection<McoreLinkDescriptor> notVisibleConnections, final Domain2Notation domain2NotationMap) {
		if (node instanceof ViewDescriptor) { // new created node view: @see MCore-21
			ViewDescriptor viewDescriptor = (ViewDescriptor) node;
			Optional.ofNullable(viewDescriptor.getElementAdapter())
				.map(adapter -> adapter.getAdapter(EObject.class)).ifPresent(element -> {
					for (Iterator<McoreLinkDescriptor> linksIterator = notVisibleConnections.iterator(); linksIterator.hasNext();) {
						McoreLinkDescriptor nextLinkDescriptor = (McoreLinkDescriptor) linksIterator.next();
						EObject nextLinkSource = nextLinkDescriptor.getSource();
						EObject nextLinkTarget = nextLinkDescriptor.getDestination();
						if (element.equals(nextLinkSource)) {
							createLinkCommand(compositeCommand, nextLinkDescriptor, node, getElementView(nextLinkTarget, domain2NotationMap));
							linksIterator.remove();
						}
						if (element.equals(nextLinkTarget)) {
							createLinkCommand(compositeCommand, nextLinkDescriptor, getElementView(nextLinkSource, domain2NotationMap), node);
							linksIterator.remove();
						}
					}
				});
		}
	}
	
	private void createLinkCommand(CompositeCommand container, UpdaterLinkDescriptor linkDescriptor, Object source, Object target) {
		CreateLinkCommandWithEnds createLinkCommandWithEnds = new CreateLinkCommandWithEnds(getEditingDomain(), createLinkViewDescriptor(linkDescriptor), getDiagramView());
		createLinkCommandWithEnds.setSource(source);
		createLinkCommandWithEnds.setTarget(target);
		container.add(createLinkCommandWithEnds);
	}
	
	protected Collection<McoreLinkDescriptor> getNotVisibleConnections() {
		Diagram diagram = getDiagramView().getDiagram();
		Domain2NotationPublic domain2NotationMap = new Domain2NotationPublic();
		Collection<McoreLinkDescriptor> linkDescriptors = MPackageCanonicalEditPolicy.collectAllViewLinks(diagram, domain2NotationMap);
		Collection existingLinks = new LinkedList(diagram.getEdges());
		MPackageCanonicalEditPolicy.updateExistedLinksCollection(existingLinks, linkDescriptors);
		return linkDescriptors;
	}

	private View findLinkView(View currentEP, EObject linkToShow, Domain2Notation domain2NotationMap) {
		return linkToShow instanceof MPropertyInstance ? findMPropertyInstanceView(currentEP, (MPropertyInstance) linkToShow, domain2NotationMap) : domain2NotationMap.getFirstTopView(linkToShow);
	}

	private View findMPropertyInstanceView(View currentEditPart, MPropertyInstance linkToShow, Domain2Notation domain2NotationMap) {
		for (View nextView : domain2NotationMap.get(linkToShow)) {
			if (false == nextView instanceof EdgeImpl) {
				continue;
			}
			EdgeImpl nextEdge = (EdgeImpl) nextView;
			if (nextEdge.getSource() == currentEditPart || nextEdge.getTarget() == currentEditPart) {
				return nextView;
			}
		}
		return null;
	}

	private Collection<Object> collectEObjectView(CompositeCommand compositeCommand, ViewAndViewDescriptorWrapper viewCollection, EObject eObject) {
		Set<Object> sourceViewList = viewCollection.getViewsOrDescriptor(eObject);
		if (sourceViewList == null || sourceViewList.isEmpty()) {
			ICommand createEndCommand = createLinkEndCommand(getDiagramView(), eObject);
			compositeCommand.add(createEndCommand);
			Object commandReturnValue = createEndCommand.getCommandResult().getReturnValue();
			Optional.ofNullable(commandReturnValue)
				.filter(ViewDescriptor.class::isInstance).map(ViewDescriptor.class::cast)
				.ifPresent(nextViewDescriptor -> viewCollection.registerView(eObject, nextViewDescriptor));
			return Arrays.asList(new Object[] { commandReturnValue });
		}
		return Arrays.asList(sourceViewList.toArray());
	}

	private ViewDescriptor createLinkViewDescriptor(UpdaterLinkDescriptor descriptor) {
		String semanticHint = ((IHintedType) descriptor.getSemanticAdapter().getAdapter(IElementType.class)).getSemanticHint();
		return new CreateConnectionViewRequest.ConnectionViewDescriptor(descriptor.getSemanticAdapter(), semanticHint, ViewUtil.APPEND, false,
				((GraphicalEditPart) getHost()).getDiagramPreferencesHint());
	}

	private ICommand createLinkEndCommand(View container, EObject semanticElement) {
		ViewDescriptor viewDescriptor = new ViewDescriptor(new EObjectAdapter(semanticElement), Node.class, null, ViewUtil.APPEND, true,
				((IGraphicalEditPart) this.getHost()).getDiagramPreferencesHint());
		return new CreateCommand(getEditingDomain(), viewDescriptor, container);
	}

	/**
	 * Retrieves editpart corresponding to model element.
	 *
	 * @param element
	 *            the model element
	 * @param domain2NotationMap
	 *            the domain to notation map
	 * @return the editPart
	 */
	protected EditPart getEditPart(final EObject element, Domain2Notation domain2NotationMap) {
		View view = domain2NotationMap.getFirstTopView(element);
		if (view != null) {
			return (EditPart) getHost().getViewer().getEditPartRegistry().get(view);
		}
		return null;
	}
	
	protected static View getElementView(final EObject element, Domain2Notation domain2NotationMap) {
		return domain2NotationMap.getFirstTopView(element);
	}

	/**
	 * Gets the editpart from view.
	 *
	 * @param view
	 *            the view
	 * @return the editpart from theview
	 */
	protected EditPart getEditPartFromView(View view) {
		if (view != null) {
			return (EditPart) getHost().getViewer().getEditPartRegistry().get(view);
		}
		return null;
	}
	
	public static class ViewAndViewDescriptorWrapper {
		
		private final Domain2Notation myDomain2Notation;
		private final Map<EObject, ViewDescriptor> myEObject2ViewDescriptor;
		
		public ViewAndViewDescriptorWrapper(Domain2Notation domain2Notation) {
			myDomain2Notation = domain2Notation;
			myEObject2ViewDescriptor = new HashMap<>();
		}
		
		public Domain2Notation getDomain2NotationMap() {
			return myDomain2Notation;
		}
		
		public Set<Object> getViewsOrDescriptor(EObject eObject) {
			Set<Object> result = new HashSet<>();
			Set<View> views = myDomain2Notation.get(eObject);
			if (views != null && !views.isEmpty()) {
				result.addAll(views);
			}
			Optional.ofNullable(myEObject2ViewDescriptor.get(eObject)).ifPresent(result::add);
			return result;
		}
		
		public void registerView(EObject eObject, ViewDescriptor viewDescriptor) {
			myEObject2ViewDescriptor.put(eObject, viewDescriptor);
		}
	}
}
