package com.montages.mcore.diagram.edit.policies;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.editpolicies.AbstractEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsPackage;

public class PropertyInstanceRefereshEditPolicy extends AbstractEditPolicy implements Adapter {

	private static final ObjectsPackage OBJECT = ObjectsPackage.eINSTANCE;

	private static final McorePackage MCORE = McorePackage.eINSTANCE;

	public static final String ROLE = "com.montages.mcore.diagram.edit.policies.PropertyInstanceRefereshEditPolicy";

	private Notifier target;

	@Override
	public void activate() {
		super.activate();
		startListening(getAllSemantics());
	}

	@Override
	public void deactivate() {
		stopListening(getAllSemantics());
		super.deactivate();
	}

	protected void doRefresh() {
		getHost().refresh();
	}

	protected void startListening(List<?> semantics) {
		for (Object semantic : semantics) {
			startListening(semantic);
		}
	}

	private void startListening(Object semantic) {
		if (semantic instanceof EObject) {
			((EObject)semantic).eAdapters().add(this);
		}
	}

	protected void stopListening(List<?> semantics) {
		for (Object semantic : semantics) {
			stopListening(semantic);
		}
	}

	protected void stopListening(Object semantic) {
		if (semantic instanceof EObject) {
			((EObject)semantic).eAdapters().remove(this);
		}
	}

	@Override
	public IGraphicalEditPart getHost() {
		return (IGraphicalEditPart) super.getHost();
	}

	protected List<EObject> getAllSemantics() {
		EObject hostEObject = getHost().resolveSemanticElement();
		if (false == hostEObject instanceof MPropertyInstance) {
			return Collections.emptyList();
		}
		MPropertyInstance propertyInstance = (MPropertyInstance) hostEObject;
		List<EObject> semantics = new ArrayList<EObject>();
		add(semantics, propertyInstance);
		add(semantics, propertyInstance.getProperty());
		addDataValues(propertyInstance, semantics);
		return semantics;
	}

	@Override
	public void notifyChanged(Notification notification) {
		if (notification.getNotifier() == getHost().resolveSemanticElement()) {
			if (notification.getFeature() == OBJECT.getMPropertyInstance_Property()) {
				stopListening(notification.getOldValue());
				startListening(notification.getNewValue());
			} else if (notification.getFeature() == OBJECT.getMPropertyInstance_InternalDataValue()) {
				if (notification.getEventType() == Notification.ADD) {
					startListening(notification.getNewValue());
				} else if (notification.getEventType() == Notification.REMOVE) {
					stopListening(notification.getOldValue());
				} else if (notification.getEventType() == Notification.REMOVE_MANY) {
					stopListening((List<?>)notification.getOldValue());
				} else if (notification.getEventType() == Notification.ADD_MANY) {
					startListening((List<?>)notification.getNewValue());
				}
			}
		}
		if (shouldRefresh(notification)) {
			doRefresh();
		}
	}

	protected boolean shouldRefresh(Notification notification) {
		Object feature = notification.getFeature();
		if (notification.getNotifier() instanceof MPropertyInstance) {
			if (feature == OBJECT.getMPropertyInstance_InternalContainedObject() //
					|| feature == OBJECT.getMPropertyInstance_InternalReferencedObject() //
					|| feature == OBJECT.getMPropertyInstance_InternalLiteralValue() //
					|| feature == OBJECT.getMPropertyInstance_InternalDataValue()) {
				return true;
			}
		} else if (notification.getNotifier() instanceof MProperty) {
			if (feature == MCORE.getMNamed_Name() || feature == MCORE.getMNamed_ShortName()) {
				return true;
			}
		} else if (notification.getNotifier() instanceof MDataValue) {
			if (feature == OBJECT.getMDataValue_DataValue()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public Notifier getTarget() {
		return target;
	}

	@Override
	public void setTarget(Notifier newTarget) {
		target = newTarget;
	}

	@Override
	public boolean isAdapterForType(Object type) {
		return false;
	}

	private static void add(List<EObject> list, EObject element) {
		if (element != null) {
			list.add(element);
		}
	}

	private static void addDataValues(MPropertyInstance propertyInstance, List<EObject> semantics) {
		for (EObject eObject: propertyInstance.getInternalDataValue()) {
			add(semantics, eObject);
		}
	}
}
