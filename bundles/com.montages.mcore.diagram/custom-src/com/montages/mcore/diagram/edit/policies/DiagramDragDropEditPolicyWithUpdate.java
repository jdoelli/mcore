package com.montages.mcore.diagram.edit.policies;

import static com.montages.mcore.diagram.commands.CommandUtils.getLinkDescriptor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.commands.CompoundCommand;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.commands.ICommandProxy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.GraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.CanonicalEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.DiagramDragDropEditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.diagram.ui.requests.DropObjectsRequest;
import org.eclipse.gmf.runtime.emf.commands.core.command.AbstractTransactionalCommand;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.commands.CreateLinkCommandWithEnds;
import com.montages.mcore.diagram.edit.commands.McoreCreateShortcutDecorationsCommand;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;

public class DiagramDragDropEditPolicyWithUpdate extends DiagramDragDropEditPolicy {

	@Override
	public GraphicalEditPart getHost() {
		return (GraphicalEditPart) super.getHost();
	}

	@Override
	public Command getDropObjectsCommand(DropObjectsRequest dropRequest) {
		List<CreateViewRequest.ViewDescriptor> viewDescriptors = new ArrayList<CreateViewRequest.ViewDescriptor>();
		// First we create all nodes descriptor 
		// then we create links descriptors with already created nodes descriptors

		// drop nodes
		PreferencesHint hint = getHost().getDiagramPreferencesHint();
		for (Iterator<?> it = dropRequest.getObjects().iterator(); it.hasNext();) {
			Object nextObject = it.next();
			if (false == nextObject instanceof EObject || nextObject instanceof MProperty) {
				continue;
			} else {
				if (nextObject instanceof MObject) {
					MObject mObject = (MObject) nextObject;
					if (mObject.getType() != null && !dropRequest.getObjects().contains(mObject.getType()) && PolicyUtils.findView(mObject.getType(), getHost().getNotationView()).isEmpty()) {
						createNodeDescriptor((EObject) mObject.getType(), viewDescriptors, hint);
					}
				}
				createNodeDescriptor((EObject) nextObject, viewDescriptors, hint);
			}
		}

		// drop links
		CompoundCommand links = new CompoundCommand();
		
		for (Iterator<?> it = dropRequest.getObjects().iterator(); it.hasNext();) {
			Object nextObject = it.next();
			if (nextObject instanceof MProperty) {
				MProperty mProperty = (MProperty) nextObject;
				if (mProperty.getType() != null) {
					IElementType type = mProperty.getContainment() != null && mProperty.getContainment().booleanValue() ? McoreElementTypes.MProperty_4006 : McoreElementTypes.MProperty_4003;
					links.add(getCreateLinkCommand(mProperty.getContainingClassifier(), mProperty.getType(), mProperty, type, viewDescriptors));
				}
			}
		}

		CompoundCommand cc = new CompoundCommand();
		cc.add(createShortcutsCommand(dropRequest, viewDescriptors));
		if (!links.isEmpty()) {
			cc.add(links);
		}
		return cc.isEmpty() ? null : cc;
	}

	protected Command getCreateLinkCommand(EObject sourceEObject, EObject targetEObject, EObject link, IElementType type, List<ViewDescriptor> viewDescriptors) {
		if (sourceEObject == null || targetEObject == null) {
			return null;
		}
		Object source = calculateLinkEnd(sourceEObject, viewDescriptors);
		Object target = calculateLinkEnd(targetEObject, viewDescriptors);
		CreateLinkCommandWithEnds createLinkCommand = new CreateLinkCommandWithEnds(//
				getHost().getEditingDomain()//
				, getLinkDescriptor(getHost().getDiagramPreferencesHint(), link, type)//
				, getHost().getNotationView());
		createLinkCommand.setSource(source);
		createLinkCommand.setTarget(target);
		return new ICommandProxy(createLinkCommand);
	}

	public Object calculateLinkEnd(EObject element, List<ViewDescriptor> viewDescriptors) {
		ViewDescriptor viewDescriptor = findDescriptor(element, viewDescriptors);
		if (viewDescriptor != null) {
			return viewDescriptor;
		}
		List<View> sources = PolicyUtils.findView(element, getHost().getNotationView());
		View source = sources.isEmpty() ? null: sources.get(0);
		if (source != null) {
			return source;
		}
		viewDescriptor = createDescriptor(element, getHost().getDiagramPreferencesHint());
		viewDescriptors.add(viewDescriptor);
		return viewDescriptor;
	}

	private Command createShortcutsCommand(DropObjectsRequest dropRequest, List<CreateViewRequest.ViewDescriptor> viewDescriptors) {
		if (viewDescriptors.isEmpty()) {
			return null;
		}
		Command command = createViewsAndArrangeCommand(dropRequest, viewDescriptors);
		if (command == null) {
			return null;
		}
		TransactionalEditingDomain domain = getHost().getEditingDomain();
		command = command.chain(new ICommandProxy(new McoreCreateShortcutDecorationsCommand(domain, (View) getHost().getModel(), viewDescriptors)));
		command = command.chain(new Command() {

			public void execute() {
				EObject semanticElement = getHost().resolveSemanticElement();
				for (CanonicalEditPolicy next : CanonicalEditPolicy.getRegisteredEditPolicies(semanticElement)) {
					if (next.isEnabled()) {
						next.refresh();
					}
				}
			}
		});
		return wrapIntoTransaction(domain, command);
	}

	private Command wrapIntoTransaction(TransactionalEditingDomain domain, final Command gefCommand) {
		if (false == gefCommand instanceof CompoundCommand) {
			return gefCommand;
		}
		AbstractTransactionalCommand rootTransaction = new AbstractTransactionalCommand(domain, gefCommand.getLabel(), null) {

			@Override
			protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
				gefCommand.execute();
				return CommandResult.newOKCommandResult();
			}

			@Override
			protected IStatus doUndo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
				gefCommand.undo();
				return Status.OK_STATUS;
			}

			@Override
			protected IStatus doRedo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
				gefCommand.redo();
				return Status.OK_STATUS;
			}
		};
		return new ICommandProxy(rootTransaction);
	}

	public static ViewDescriptor findDescriptor(EObject element, List<ViewDescriptor> viewDescriptors) {
		for (ViewDescriptor descr: viewDescriptors) {
			if (descr.getElementAdapter().getAdapter(EObject.class) == element) {
				return descr;
			}
		}
		return null;
	}

	public static void createNodeDescriptor(EObject nextObject, List<CreateViewRequest.ViewDescriptor> viewDescriptors, PreferencesHint hint) {
		ViewDescriptor viewDescriptor = findDescriptor(nextObject, viewDescriptors);
		if (viewDescriptor == null) {
			viewDescriptor = createDescriptor(nextObject, hint);
			viewDescriptors.add(viewDescriptor);
		}
	}

	public static ViewDescriptor createDescriptor(EObject element, PreferencesHint hint) {
		return new CreateViewRequest.ViewDescriptor(new EObjectAdapter(element), Node.class, null, hint);
	}
}
