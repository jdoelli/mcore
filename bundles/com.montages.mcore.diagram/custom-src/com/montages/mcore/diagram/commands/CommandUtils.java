package com.montages.mcore.diagram.commands;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.IHintedType;
import org.eclipse.gmf.runtime.notation.Edge;

public class CommandUtils {

	public static CreateViewRequest.ViewDescriptor getLinkDescriptor(PreferencesHint hint, final EObject element, final IElementType type) {
		return new CreateViewRequest.ViewDescriptor(new IAdaptable() {
			
			@Override
			public <T> T getAdapter(Class<T> adapter) {
				if (IElementType.class == adapter && type instanceof IElementType) {
					return adapter.cast(type);
				}
				if (element instanceof EObject && EObject.class == adapter) {
					return adapter.cast(element);
				}
				return null;
			}
		}, Edge.class, ((IHintedType) type).getSemanticHint(), hint);
	}

}
