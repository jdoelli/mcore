package com.montages.mcore.diagram.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

import com.montages.mcore.diagram.edit.policies.CustomMPackageCanonicalEditPolicy;


public class RemoveCustomCanonicalCommand extends Command {

	private IGraphicalEditPart myDiagramEP;

	public RemoveCustomCanonicalCommand(IGraphicalEditPart diagramEP) {
		myDiagramEP = diagramEP;
	}

	@Override
	public void execute() {
		myDiagramEP.removeEditPolicy(CustomMPackageCanonicalEditPolicy.ROLE);
	}

}
