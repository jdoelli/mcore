package com.montages.mcore.diagram.commands;

import java.util.Collection;

import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;

/**
 *
 * The request used to show/hide related links
 *
 */
public class ShowHideRelatedElementsRequest extends Request {

	/**
	 *
	 * this enumeration describe the several ways to use this request
	 *
	 */
	public static enum ShowHideKind {
		OPEN_DIALOG, SHOW_ALL_LINK_IN_DIAGRAM, SHOW_ALL_LINK_BETWEEN_SELECTED_ELEMENT, SHOW_RELATED_ELEMENTS
	};

	/**
	 * the list of the selected editpart
	 */
	private final Collection<EditPart> selectedEditParts;

	/**
	 * the way to use this request
	 */
	private final ShowHideKind mode;

	/**
	 *
	 * Constructor.
	 *
	 * @param selectedEditPart
	 */
	public ShowHideRelatedElementsRequest(final Collection<EditPart> selectedEditPart, final ShowHideKind mode) {
		super();
		this.selectedEditParts = selectedEditPart;
		this.mode = mode;
	}

	/**
	 *
	 * @return
	 *         the selected edit part
	 */
	public Collection<EditPart> getSelectedEditParts() {
		return selectedEditParts;
	}

	/**
	 *
	 * @return
	 *         the mode to use this request
	 */
	public ShowHideKind getMode() {
		return mode;
	}

}
