package com.montages.mcore.diagram.commands;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;

import com.montages.mcore.diagram.edit.policies.CustomMPackageCanonicalEditPolicy;

public class AddCustomCanonicalEditPolicy extends Command {

	private final IGraphicalEditPart myDiagramEP;

	public AddCustomCanonicalEditPolicy(IGraphicalEditPart diagramEP) {
		myDiagramEP = diagramEP;
	}

	@Override
	public void execute() {
		myDiagramEP.installEditPolicy(CustomMPackageCanonicalEditPolicy.ROLE, new CustomMPackageCanonicalEditPolicy());
	}

}