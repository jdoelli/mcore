package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import com.montages.mcore.diagram.parsers.CompositeSlotProvider.IAdapdableParser;
import com.montages.mcore.diagram.tools.MSlotsUtils;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;

public class ContainingSlotParser implements IAdapdableParser {

	@Override
	public String getEditString(IAdaptable element, int flags) {
		return getPrintString(element, flags);
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable element, String editString) {
		return ParserEditStatus.UNEDITABLE_STATUS;
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		return null;
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());

		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(mPropertyInstance);
		if (editingDomain == null || false == editingDomain instanceof AdapterFactoryEditingDomain) {
			return "";
		}
		IItemLabelProvider labelProvider = (IItemLabelProvider) ((AdapterFactoryEditingDomain) editingDomain).getAdapterFactory().adapt(mPropertyInstance, IItemLabelProvider.class);

		String result = labelProvider.getText(mPropertyInstance);

		List<String> internalContainedValue = processInternalContained(mPropertyInstance.getInternalContainedObject());
		if (internalContainedValue.isEmpty()) {
			return result;
		}
		if (result.contains("=")) {
			result = result.substring(0, result.indexOf("="));
		}
		StringBuilder sb = new StringBuilder(result + " = ");
		sb.append(MSlotsUtils.outputSlotValues(internalContainedValue));
		return sb.toString();
	}

	@Override
	public boolean isAffectingEvent(Object event, int flags) {
		return false;
	}

	@Override
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}

	@Override
	public boolean isAdaptableFor(MPropertyInstance mPropertyInstance) {
		return MSlotsUtils.isContainment(mPropertyInstance);
	}

	private static List<String> processInternalContained(List<MObject> internalObjects) {
		if (internalObjects == null || internalObjects.isEmpty()) {
			return Collections.emptyList();
		}
		List<String> result = new ArrayList<String>();
		Iterator<MObject> iter = internalObjects.iterator();
		while (iter.hasNext()) {
			String nextValue = iter.next().getId();
			if (nextValue == null || nextValue.isEmpty()) {
				continue;
			}
			result.add(nextValue);
		}
		return result;
	}
}
