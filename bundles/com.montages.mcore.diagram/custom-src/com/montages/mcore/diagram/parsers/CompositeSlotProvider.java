package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;
import org.eclipse.gmf.tooling.runtime.directedit.ComboDirectEditManager.IChoiceParser;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import com.montages.mcore.objects.MPropertyInstance;

public class CompositeSlotProvider implements IParser, IChoiceParserEx, ISemanticParser {

	private List<IAdapdableParser> myParsers = new ArrayList<CompositeSlotProvider.IAdapdableParser>();

	public CompositeSlotProvider() {
		myParsers.add(new SlotParser());
		myParsers.add(new EnumParser());
		myParsers.add(new ReferenceSlotParser());
		myParsers.add(new ContainingSlotParser());
	}

	@Override
	public List<String> getEditChoices(IAdaptable element) {
		IParser parser = getParser(element);
		return parser != null ? ((IChoiceParser)getParser(element)).getEditChoices(element) : Collections.<String>emptyList();
	}

	@Override
	public String getEditString(IAdaptable element, int flags) {
		IParser parser = getParser(element);
		return parser != null ? getParser(element).getEditString(element, flags) : "Unsetted property";
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable element, String editString) {
		IParser parser = getParser(element);
		return parser != null ? parser.isValidEditString(element, editString) : ParserEditStatus.UNEDITABLE_STATUS;
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		return getParser(element).getParseCommand(element, newString, flags);
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		IParser parser = getParser(element);
		return parser != null ? parser.getPrintString(element, flags) : "Unsetted property";
	}

	@Override
	public boolean isAffectingEvent(Object event, int flags) {
		return false;
	}

	@Override
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}

	public IParser getParser(IAdaptable element) {
		Object realObject = ((EObjectAdapter) element).getRealObject();
		if (false == realObject instanceof MPropertyInstance) {
			return null;
		}
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) realObject);

		for (IAdapdableParser iAdapdableParser : myParsers) {
			if (iAdapdableParser.isAdaptableFor(mPropertyInstance)) {
				return iAdapdableParser;
			}
		}
		return null;
	}

	@Override
	public List<String> getSelectedChoises(IAdaptable element) {
		IParser parser = getParser(element);
		return parser != null ? ((IChoiceParserEx)parser).getSelectedChoises(element) : Collections.<String>emptyList();
	}

	@Override
	public List<?> getSemanticElementsBeingParsed(EObject element) {
		return Arrays.asList(element);
	}

	@Override
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		return true;
	}

	public static interface IAdapdableParser extends IParser {
		public boolean isAdaptableFor(MPropertyInstance mPropertyInstance);
	}
}
