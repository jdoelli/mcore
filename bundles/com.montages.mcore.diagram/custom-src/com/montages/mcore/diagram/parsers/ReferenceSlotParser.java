package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserEditStatus;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserEditStatus;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.emf.type.core.commands.SetValueCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.SetRequest;
import org.eclipse.jface.text.contentassist.IContentAssistProcessor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.diagram.parsers.CompositeSlotProvider.IAdapdableParser;
import com.montages.mcore.diagram.tools.MSlotsUtils;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

public class ReferenceSlotParser implements IAdapdableParser, IChoiceParserEx {

	@Override
	public String getEditString(IAdaptable element, int flags) {
		return getPrintString(element, flags) + "edit";
	}

	@Override
	public IParserEditStatus isValidEditString(IAdaptable element, String editString) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());
		return MSlotsUtils.isReference(mPropertyInstance) ? ParserEditStatus.EDITABLE_STATUS : ParserEditStatus.UNEDITABLE_STATUS;
	}

	@Override
	public ICommand getParseCommand(IAdaptable element, String newString, int flags) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());

		List<MObjectReference> references = new ArrayList<MObjectReference>();
		List<MObject> mObjects = collectMObjects(mPropertyInstance);
		for (String s : newString.split(",")) {
			if (s.length() > 0) {
				MObjectReference ref = ObjectsFactory.eINSTANCE.createMObjectReference();
				references.add(ref);
				for (MObject mObject : mObjects) {
					if (s.equals(mObject.getId())) {
						ref.setReferencedObject(mObject);
						break;
					}
				}
			}
		}

		return new SetValueCommand(new SetRequest(mPropertyInstance, ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalReferencedObject(), references));
	}

	@Override
	public String getPrintString(IAdaptable element, int flags) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());

		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(mPropertyInstance);
		if (editingDomain == null || false == editingDomain instanceof AdapterFactoryEditingDomain) {
			return "";
		}
		IItemLabelProvider labelProvider = (IItemLabelProvider) ((AdapterFactoryEditingDomain) editingDomain).getAdapterFactory().adapt(mPropertyInstance, IItemLabelProvider.class);
		return labelProvider.getText(mPropertyInstance);
	}

	@Override
	public boolean isAffectingEvent(Object event, int flags) {
		return false;
	}

	@Override
	public IContentAssistProcessor getCompletionProcessor(IAdaptable element) {
		return null;
	}

	@Override
	public boolean isAdaptableFor(MPropertyInstance mPropertyInstance) {
		return MSlotsUtils.isReference(mPropertyInstance);
	}

	@Override
	public List<String> getEditChoices(IAdaptable element) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());
		List<String> result = new ArrayList<String>();
		for (MObject mObject : collectMObjects(mPropertyInstance)) {
			result.add(mObject.getId());
		}
		return result;
	}

	@Override
	public List<String> getSelectedChoises(IAdaptable element) {
		final MPropertyInstance mPropertyInstance = ((MPropertyInstance) ((EObjectAdapter) element).getRealObject());
		List<String> result = new ArrayList<String>();

		for (MObjectReference ref : mPropertyInstance.getInternalReferencedObject()) {
			result.add(ref.getReferencedObject().getId());
		}
		return result;
	}

	private List<MObject> collectMObjects(MPropertyInstance mPropertyInstance) {
		ArrayList<MObject> result = new ArrayList<MObject>();
		MClassifier type = mPropertyInstance.getProperty().getType();
		for (Iterator<EObject> it = mPropertyInstance.getContainingObject().getResource().eAllContents(); it.hasNext();) {
			EObject next = it.next();
			if (false == next instanceof MObject) {
				continue;
			}
			MObject mObject = (MObject) next;
			if (mObject.getType() == type) {
				result.add(mObject);
			}
		}
		return result;
	}
}
