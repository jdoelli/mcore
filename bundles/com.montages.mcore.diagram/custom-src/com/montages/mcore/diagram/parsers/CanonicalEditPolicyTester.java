package com.montages.mcore.diagram.parsers;

import org.eclipse.core.expressions.PropertyTester;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;

import com.montages.mcore.diagram.edit.parts.MPackageEditPart;

public class CanonicalEditPolicyTester extends PropertyTester {

	protected static final String IS_ON = "isOn"; //$NON-NLS-1$

	protected static final String IS_OFF = "isOff"; //$NON-NLS-1$

	@Override
	public boolean test(Object receiver, String property, Object[] args, Object expectedValue) {
		if (false == receiver instanceof MPackageEditPart) {
			return false;
		}
		MPackageEditPart part = (MPackageEditPart) receiver;
		if (IS_ON.equals(property)) {
			return isCanonicalInstalled(part);
		}
		if (IS_OFF.equals(property)) {
			return ! isCanonicalInstalled(part);
		}
		return false;
	}

	private boolean isCanonicalInstalled(MPackageEditPart part) {
		return part.getEditPolicy(EditPolicyRoles.CANONICAL_ROLE) != null;
	}

}
