package com.montages.mcore.diagram.parsers;

import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.tooling.runtime.directedit.ComboDirectEditManager.IChoiceParser;

public interface IChoiceParserEx extends IChoiceParser {
	public List<String> getSelectedChoises(IAdaptable element);
}