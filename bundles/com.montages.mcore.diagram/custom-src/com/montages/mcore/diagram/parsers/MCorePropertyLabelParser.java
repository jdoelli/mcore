package com.montages.mcore.diagram.parsers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ISemanticParser;

import com.montages.mcore.MProperty;

public class MCorePropertyLabelParser extends MessageFormatParser implements ISemanticParser {

	public MCorePropertyLabelParser(EAttribute[] features) {
		super(features);
	}

	@Override
	public String getPrintString(IAdaptable adapter, int flags) {
		return getPrintString((EObject) adapter.getAdapter(EObject.class));
	}

	public String getPrintString(EObject element) {
		TransactionalEditingDomain editingDomain = TransactionUtil.getEditingDomain(element);
		if (editingDomain == null || false == editingDomain instanceof AdapterFactoryEditingDomain) {
			return "";
		}
		IItemLabelProvider labelProvider = (IItemLabelProvider) ((AdapterFactoryEditingDomain) editingDomain).getAdapterFactory().adapt(element, IItemLabelProvider.class);
		return labelProvider.getText(element);
	}

	@Override
	public List<?> getSemanticElementsBeingParsed(EObject element) {
		List<Object> result = new ArrayList<Object>();
		if (false == element instanceof MProperty) {
			return Collections.emptyList();
		}
		MProperty mProperty = (MProperty) element;
		result.add(mProperty);
		if (mProperty.getContainment() && mProperty.getType() != null) {
			result.add(mProperty.getType());
		}
		return result;
	}

	@Override
	public boolean areSemanticElementsAffected(EObject listener, Object notification) {
		if (false == notification instanceof ENotificationImpl) {
			return false;
		}
		return Notification.SET == ((ENotificationImpl) notification).getEventType();
	}

}
