package com.montages.mcore.diagram.actions;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

public abstract class ShowHideHandler extends AbstractHandler {

	private final AbstractShowHideAction myAction;

	private final String myPolicyKey;

	private final List<IGraphicalEditPart> mySelections;

	protected ShowHideHandler(AbstractShowHideAction action, String key) {
		myAction = action;
		myPolicyKey = key;
		mySelections = new ArrayList<IGraphicalEditPart>();
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		myAction.run(null);
		return null;
	}

	@Override
	public boolean isEnabled() {
		collectSelections();
		myAction.setSelection(mySelections);
		return myAction.isEnabled();
	}

	private void collectSelections() {
		mySelections.clear();
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		if (activeWorkbenchWindow == null) {
			return;
		}
		ISelectionService selectionService = activeWorkbenchWindow.getSelectionService();
		ISelection selection = selectionService.getSelection();
		if (selection == null || selection.isEmpty()) {
			return;
		}
		if (false == selection instanceof StructuredSelection) {
			return;
		}
		Iterator<?> it = ((StructuredSelection) selection).iterator();
		while (it.hasNext()) {
			Object nextEditPart = it.next();
			if (false == nextEditPart instanceof IGraphicalEditPart) {
				continue;
			}
			EditPolicy editpolicy = ((IGraphicalEditPart) nextEditPart).getEditPolicy(myPolicyKey);
			if (editpolicy != null) {
				mySelections.add((IGraphicalEditPart) nextEditPart);
			}
		}
	}
}