package com.montages.mcore.diagram.actions;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.jface.dialogs.IDialogSettings;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.dialogs.FilteredItemsSelectionDialog;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.diagram.part.Messages;

public class EObjectSelectionDialog extends FilteredItemsSelectionDialog {

	private static final String ANY_CHAR_PATTERN = "?";

	private final List<?> myContents;

	private final IParser myParser;

	public EObjectSelectionDialog(Shell shell, List<?> contents, IParser parser) {
		this(shell, contents, parser, false);
	}

	private EObjectSelectionDialog(Shell shell, List<?> contents, IParser parser, boolean multi) {
		super(shell, multi);
		myParser = parser;
		myContents = contents;
		setTitle(Messages.EObjectSelectionDialogTitle);
		setSelectionHistory(new ResourceSelectionHistory());
		setInitialPattern(ANY_CHAR_PATTERN);
		setListLabelProvider(new LabelProvider() {

			@Override
			public String getText(Object element) {
				if (false == element instanceof EObject) {
					return super.getText(element);
				}
				EObject eObject = (EObject) element;
				return myParser.getPrintString(new EObjectAdapter(eObject), 0);
			}
		});
	}

	@Override
	protected Control createExtendedContentArea(Composite parent) {
		return null;
	}

	@Override
	protected IDialogSettings getDialogSettings() {
		return McoreDiagramEditorPlugin.getInstance().getDialogSettings();
	}

	@Override
	protected IStatus validateItem(Object item) {
		return Status.OK_STATUS;
	}

	@Override
	protected ItemsFilter createFilter() {
		return new ItemsFilter() {

			public boolean matchItem(Object item) {
				return true;//matches(item.toString());
			}

			public boolean isConsistentItem(Object item) {
				return true;
			}
		};
	}

	@Override
	protected Comparator<Object> getItemsComparator() {
		return new Comparator<Object>() {

			public int compare(Object arg0, Object arg1) {
				return arg0.toString().compareTo(arg1.toString());
			}
		};
	}

	@Override
	protected void fillContentProvider(AbstractContentProvider contentProvider, ItemsFilter itemsFilter, IProgressMonitor progressMonitor) throws CoreException {
		progressMonitor.beginTask("Searching", myContents.size()); //$NON-NLS-1$
		for (Iterator<?> iter = myContents.iterator(); iter.hasNext();) {
			contentProvider.add(iter.next(), itemsFilter);
			progressMonitor.worked(1);
		}
		progressMonitor.done();
	}

	@Override
	public String getElementName(Object item) {
		return item.toString();
	}

	private final class ResourceSelectionHistory extends SelectionHistory {

		protected Object restoreItemFromMemento(IMemento element) {
			return null;
		}

		protected void storeItemToMemento(Object item, IMemento element) {
		}
	}

}
