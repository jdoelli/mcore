package com.montages.mcore.diagram.actions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.core.util.ViewUtil;
import org.eclipse.gmf.runtime.diagram.ui.commands.CreateCommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IResizableCompartmentEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITextAwareEditPart;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewRequest.ViewDescriptor;
import org.eclipse.gmf.runtime.emf.core.util.EObjectAdapter;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.jface.viewers.ILabelProvider;

import com.montages.mcore.diagram.edit.policies.ShowHideSlotEditPolicy;
import com.montages.mcore.objects.MObject;

public class ShowHideSlotAction extends AbstractShowHideAction {

	public ShowHideSlotAction() {
		super("Show/Hide slot items", "Select elements to show", ShowHideSlotEditPolicy.SHOW_HIDE_SLOT_POLICY);
	}

	@Override
	protected boolean canCreateView(EditPartRepresentation rep) {
		return rep instanceof CompartmentChildEditPartRepresentation;
	}

	@Override
	protected boolean canNotAddToDestroyViewList(Object source) {
		return super.canNotAddToDestroyViewList(source) || source instanceof CompartmentEditPartRepresentation;
	}

	@Override
	protected EditPartRepresentation createRootRepresentation(IGraphicalEditPart ep, EObject semantic, ILabelProvider labelProvider) {
		return new SlotCompartmentRootEditPartRepresenation(ep, semantic, labelProvider);
	}

	private static class SlotCompartmentRootEditPartRepresenation extends CompartmentRootEditPartRepresentation {

		public SlotCompartmentRootEditPartRepresenation(IGraphicalEditPart representedEditPart, EObject element, ILabelProvider labelProvider) {
			super(representedEditPart, element, labelProvider);
		}

		@Override
		protected void init() {
			List<IResizableCompartmentEditPart> compartmentChilds = findChildCompartments(getRepresentedEditPart());
			for (IResizableCompartmentEditPart nextChild : compartmentChilds) {
				CompartmentEditPartRepresentation representation = new SlotCompartmentEditPartRepresentation(nextChild, getSemanticElement(), this, getLabelProvider());
				getPossibleElement().add(representation);
			}
		}

		private List<IResizableCompartmentEditPart> findChildCompartments(EditPart editpart) {
			List<IResizableCompartmentEditPart> result = new ArrayList<IResizableCompartmentEditPart>();
			List<?> childrens = editpart.getChildren();
			for (Object nextChildren : childrens) {
				if (nextChildren instanceof IResizableCompartmentEditPart) {
					if (false == nextChildren instanceof ITextAwareEditPart) {
						result.add((IResizableCompartmentEditPart) nextChildren);
					}
				}
			}
			return result;
		}
	}

	private static class SlotCompartmentEditPartRepresentation extends CompartmentEditPartRepresentation {

		public SlotCompartmentEditPartRepresentation(IResizableCompartmentEditPart compartmentEditPart, EObject element, EditPartRepresentation parentRepresentation, ILabelProvider labelProvider) {
			super(compartmentEditPart, null, element, parentRepresentation, labelProvider);
		}

		@Override
		protected void init() {
			for (EObject nextMember : collectMembers()) {
				if (getRepresentedEditPart() == null || false == canContain(nextMember)) {
					continue;
				}
				setupInitialSelections((EObject) nextMember);
			}
		}

		private void setupInitialSelections(EObject member) {
			boolean memberViewPresent = false;
			EList<?> childrenView = getRepresentedEditPart().getNotationView().getVisibleChildren();
			for (Object nextChildrenView : childrenView) {
				if (false == nextChildrenView instanceof View) {
					continue;
				}
				if (((View) nextChildrenView).getElement() != member) {
					continue;
				}
				for (Object nextChildren : getRepresentedEditPart().getChildren()) {
					if (false == nextChildren instanceof IGraphicalEditPart) {
						continue;
					}
					if (((View) nextChildrenView).equals(((IGraphicalEditPart) nextChildren).getNotationView())) {
						CompartmentChildEditPartRepresentation nextMemberEditPart = new CompartmentChildEditPartRepresentation((IGraphicalEditPart) nextChildren, member, this);
						getInitialSelection().add(nextMemberEditPart);
						getPossibleElement().add(nextMemberEditPart);
						memberViewPresent = true;
					}
				}
				break;
			}
			if (!memberViewPresent) {
				CompartmentChildEditPartRepresentation possibleMemberEditPart = new CompartmentChildEditPartRepresentation(null, member, this);
				getPossibleElement().add(possibleMemberEditPart);
			}
		}

		private boolean canContain(Object element) {
			if (false == element instanceof EObject) {
				return false;
			}
			TransactionalEditingDomain domain = getRepresentedEditPart().getEditingDomain();
			ViewDescriptor viewDescriptor = new ViewDescriptor(new EObjectAdapter((EObject) element), Node.class, null, ViewUtil.APPEND, false, getRepresentedEditPart().getDiagramPreferencesHint());
			CreateCommand cmd = new CreateCommand(domain, viewDescriptor, getRepresentedEditPart().getNotationView());
			return cmd.canExecute();
		}

		private List<EObject> collectMembers() {
			EObject semantic = getSemanticElement();
			if (false == semantic instanceof MObject) {
				return Collections.emptyList();
			}
			return new ArrayList<EObject>(((MObject) getSemanticElement()).getPropertyInstance());
		}
	}

	private static class CompartmentChildEditPartRepresentation extends EditPartRepresentation {

		public CompartmentChildEditPartRepresentation(IGraphicalEditPart representedEditPart, EObject element, CompartmentEditPartRepresentation parentRepresentation) {
			super(representedEditPart, element, parentRepresentation, parentRepresentation.getLabelProvider());
		}

		@Override
		public CompartmentEditPartRepresentation getParentRepresentation() {
			return (CompartmentEditPartRepresentation) super.getParentRepresentation();
		}
	}
}