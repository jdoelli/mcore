package com.montages.mcore.diagram.tools;

import java.util.Iterator;
import java.util.List;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MProperty;
import com.montages.mcore.SimpleType;
import com.montages.mcore.objects.MPropertyInstance;

public class MSlotsUtils {

	public static boolean isEnum(MPropertyInstance mPropertyInstance) {
		MProperty mproperty = getMProperty(mPropertyInstance);
		return mproperty != null && mproperty.getType() != null && mproperty.getType().getKind() == ClassifierKind.ENUMERATION;
	}

	public static boolean isBoolean(MPropertyInstance mPropertyInstance) {
		MProperty mproperty = getMProperty(mPropertyInstance);
		return mproperty != null && mproperty.getType() == null && mproperty.getSimpleType() == SimpleType.BOOLEAN;
	}

	public static boolean isReference(MPropertyInstance mPropertyInstance) {
		MProperty mproperty = getMProperty(mPropertyInstance);
		return mproperty != null //
				&& (mproperty.getContainment() == null || mproperty.getContainment() == false)//
				&& mproperty.getType() != null //
				&& mproperty.getType().getKind() == ClassifierKind.CLASS_TYPE;
	}

	public static boolean isContainment(MPropertyInstance mPropertyInstance) {
		MProperty mproperty = getMProperty(mPropertyInstance);
		if (mproperty == null) {
			return !mPropertyInstance.getInternalContainedObject().isEmpty();
		}
		return mproperty.getContainment() != null//
				&& mproperty.getContainment()//
				&& mproperty.getType() != null//
				&& mproperty.getType().getKind() == ClassifierKind.CLASS_TYPE;
	}

	public static boolean isSlotParser(MPropertyInstance mPropertyInstance) {
		MProperty mproperty = getMProperty(mPropertyInstance);
		if (mproperty == null) {
			return !mPropertyInstance.getInternalDataValue().isEmpty();
		}
		return mPropertyInstance.getProperty().getType() == null;
	}

	private static MProperty getMProperty(MPropertyInstance mPropertyInstance) {
		return mPropertyInstance == null ? null : mPropertyInstance.getProperty();
	}

	public static String outputSlotValues(List<String> values) {
		Iterator<String> iter = values.iterator();
		int i = 0;
		String result = "";
		while (iter.hasNext()) {
			i++;
			if (i > 1) {
				result += ", ";
			}
			result += iter.next();
		}
		return result;
	}

}
