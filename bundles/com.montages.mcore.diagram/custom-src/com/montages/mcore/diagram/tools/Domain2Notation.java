package com.montages.mcore.diagram.tools;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.notation.BasicCompartment;
import org.eclipse.gmf.runtime.notation.Connector;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;

import com.montages.mcore.diagram.edit.policies.EdgeWithNoSemanticElementRepresentationImpl;

/**
 * The Class Domain2Notation used for mapping between EObject and views
 */
public class Domain2Notation extends HashMap<EObject, Set<View>> {

	private static final long serialVersionUID = 1L;

	/**
	 * Maps view.
	 *
	 * @param view
	 *            the view from which are mapped all subviews
	 */
	public void mapModel(View view) {
		if ((view instanceof Edge || view instanceof NodeImpl) && !(view instanceof BasicCompartment)) {
			putView(view);
		}
		@SuppressWarnings("unchecked")
		List<View> children = view.getChildren();
		for (View child : children) {
			mapModel(child);
		}
		@SuppressWarnings("unchecked")
		List<View> sourceEdges = view instanceof Diagram ? ((Diagram) view).getEdges() : view.getSourceEdges();
		for (View edge : sourceEdges) {
			mapModel(edge);
		}
	}

	/**
	 * Put view.
	 *
	 * @param view
	 *            the view
	 */
	public void putView(View view) {
		EObject element = view.getElement();
		if (element == null && view instanceof Connector) {
			final EObject source = ((Connector) view).getSource().getElement();
			final EObject target = ((Connector) view).getTarget().getElement();
			element = new EdgeWithNoSemanticElementRepresentationImpl(source, target, view.getType());
		} else if (element == null) {
			return;
		}
		Set<View> set = this.get(element);
		if (set != null) {
			set.add(view);
		} else {
			Set<View> hashSet = new HashSet<View>();
			hashSet.add(view);
			put(element, hashSet);
		}
	}

	/**
	 * Put view.
	 *
	 * @param element
	 *            the element
	 * @param view
	 *            the view
	 */
	public void putView(EObject element, View view) {
		Set<View> set = this.get(element);
		if (set != null) {
			set.add(view);
		} else {
			Set<View> hashSet = new HashSet<View>();
			hashSet.add(view);
			put(element, hashSet);
		}
	}

	public View getFirstTopView(Object key) {
		Set<View> set = get(key);
		if (set == null) {
			return null;
		}
		Iterator<View> it = set.iterator();
		while (it.hasNext()) {
			View nextView = it.next();
			EObject container = nextView.eContainer();
			if (container == null || false == container instanceof Diagram) {
				continue;
			}
			return nextView;
		}
		return null;
	}
}
