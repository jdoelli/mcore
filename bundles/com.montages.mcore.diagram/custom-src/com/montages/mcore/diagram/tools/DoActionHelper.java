package com.montages.mcore.diagram.tools;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gef.DragTracker;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.Request;
import org.eclipse.gef.requests.SelectionRequest;
import org.eclipse.gef.tools.SelectEditPartTracker;
import org.eclipse.gmf.runtime.gef.ui.internal.tools.DelegatingDragEditPartsTracker;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.ObjectsPackage;

public class DoActionHelper {

	public static EAttribute getDoActionFeature(EObject element) {

		if (element instanceof MClassifier) {
			return McorePackage.Literals.MCLASSIFIER__DO_ACTION;
		}
		if (element instanceof MObject) {
			return ObjectsPackage.Literals.MOBJECT__DO_ACTION;
		}
		return null;
	}

	public static DragTracker getDragTracker(Request request, final EditPart delegatingEP, EditPart delegateEP) {
		if (request instanceof SelectionRequest && ((SelectionRequest) request).getLastButtonPressed() == 3)
			return null;
		return new DelegatingDragEditPartsTracker(delegatingEP, delegateEP) {

			@Override
			protected boolean handleButtonDown(int button) {
				setDragTracker(new SelectEditPartTracker(delegatingEP) {

					@Override
					protected boolean handleButtonUp(int button) {
						if (isInState(STATE_DRAG)) {
							//performSelection();
							performDirectEdit();
							if (button == 1 && getSourceEditPart().getSelected() != EditPart.SELECTED_NONE)
								getCurrentViewer().reveal(getSourceEditPart());
							setState(STATE_TERMINAL);
							return true;
						}
						return false;
					}
				});
				lockTargetEditPart(delegatingEP);
				return true;
			}
		};
	}
}
