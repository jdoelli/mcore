package com.montages.mcore.diagram.layout;

import java.util.List;
import java.util.Optional;

import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.ScrollPane;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.Viewport;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.gmf.runtime.diagram.ui.figures.ResizableCompartmentFigure;

public class FlexCompartmentToolbarLayout extends ToolbarLayout {
	
	private final int myFlexChildNum;
	
	public FlexCompartmentToolbarLayout(int flexChildNum) {
		myFlexChildNum = flexChildNum;
	}
	
	@Override
	public void layout(IFigure parent) {
		super.layout(parent);
		flexLayout(parent);
	}
	
	@SuppressWarnings("unchecked")
	private void flexLayout(IFigure parent) {
		List<?> children = parent.getChildren();
		if (children == null || children.size() <= myFlexChildNum) {
			return;
		}
		IFigure flexFigure = Optional.ofNullable(children.get(myFlexChildNum))
			.filter(IFigure.class::isInstance).map(IFigure.class::cast).orElse(null);
		if (flexFigure == null) {
			return;
		}
		((List<Object>)flexFigure.getChildren()).stream()
			.filter(ResizableCompartmentFigure.class::isInstance).map(ResizableCompartmentFigure.class::cast)
			.map(ResizableCompartmentFigure::getScrollPane)
			.map(ScrollPane::getViewport).filter(Viewport::isVisible)
			.findFirst().ifPresent(viewPort -> {
				int parentHeight = parent.getBounds().height();
				int totalHeightDiff = parentHeight - calculateTotalChildrenHeight(children);
				if (totalHeightDiff <= 0) {
					return;
				}
				Rectangle bounds = flexFigure.getBounds();
				bounds.setHeight(bounds.height() + totalHeightDiff);
				
				children.subList(myFlexChildNum + 1, children.size()).stream()
					.filter(IFigure.class::isInstance).map(IFigure.class::cast)
					.forEach(nextFigure ->
						nextFigure.setBounds(nextFigure.getBounds().getTranslated(0, totalHeightDiff))
				);
		});	
	}
	
	private int calculateTotalChildrenHeight(List<?> children) {
		return children.stream()
			.filter(IFigure.class::isInstance).map(IFigure.class::cast)
			.map(nextChild -> getChildPreferredSize(nextChild, -1, -1))
			.map(transposer::t)
			.mapToInt(Dimension::height).sum();
	}
}
