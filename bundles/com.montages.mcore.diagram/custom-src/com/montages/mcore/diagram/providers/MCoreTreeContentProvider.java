package com.montages.mcore.diagram.providers;

import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.montages.mcore.diagram.actions.AbstractShowHideAction.EditPartRepresentation;

public class MCoreTreeContentProvider implements ITreeContentProvider {

	public MCoreTreeContentProvider() {
		super();
	}

	@Override
	public Object[] getElements(Object inputElement) {
		if (inputElement instanceof List) {
			return ((List<?>) inputElement).toArray();
		}
		return new Object[0];
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		if (parentElement instanceof EditPartRepresentation) {
			return ((EditPartRepresentation) parentElement).getPossibleElement().toArray();
		}
		return new Object[0];
	}

	@Override
	public Object getParent(Object element) {
		if (element instanceof EditPartRepresentation) {
			return ((EditPartRepresentation) element).getParentRepresentation();
		}
		return null;
	}

	@Override
	public boolean hasChildren(Object element) {
		return getChildren(element) != null && getChildren(element).length > 0;
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub	
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// TODO Auto-generated method stub
	}
}