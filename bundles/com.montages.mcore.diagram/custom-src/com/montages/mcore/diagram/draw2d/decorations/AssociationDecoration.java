package com.montages.mcore.diagram.draw2d.decorations;

import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.geometry.Point;
import org.eclipse.draw2d.geometry.PointList;

public class AssociationDecoration extends CompositeDecoration {

	private static final PointList RHOMB = new PointList(new int[] { //
			//
					-1, 1, // 
					0, 0, //
					-1, -1, //
					-2, 0, //
					-1, 1, //
			});

	private static final PointList ARROW = new PointList(new int[] { //
			//
					-1, 1, // 
					0, 0, //
					-1, -1, //
					0, 0, // 
					-1, 1, //
			});

	private static final PointList CROSS = new PointList(new int[] { //
			//
					-1, 0, // 
					-2, -1, // 
					-1, 0, //
					-2, 1, //
					-1, 0, // 
					0, 1, //
					-1, 0, // 
					0, -1, //
					-1, 0, // 
			});

	private ComposablePolygonDecoration myCompositeAggrecationDecoration;

	private ComposablePolygonDecoration mySharedAggrecationDecoration;

	private ComposablePolygonDecoration myNavigableDecoration;

	private ComposablePolygonDecoration myNonNavigableDecoration;

	private DotDecoration myOwnedEndDecoration;

	public AssociationDecoration() {
		initAggregationDecorations();
		initNavigabilityDecorations();
		initOwnedEndDecorations();
	}

	private void initAggregationDecorations() {
		myCompositeAggrecationDecoration = new ComposablePolygonDecoration();

		myCompositeAggrecationDecoration.setTemplate(RHOMB.getCopy());
		myCompositeAggrecationDecoration.setBoundPoint(new Point(-2, 0));
		myCompositeAggrecationDecoration.setFill(true);
		if (getParent() != null && getParent().getForegroundColor() != null) {
			myCompositeAggrecationDecoration.setBackgroundColor(getParent().getForegroundColor());
		}
		mySharedAggrecationDecoration = new ComposablePolygonDecoration();
		mySharedAggrecationDecoration.setTemplate(RHOMB.getCopy());
		mySharedAggrecationDecoration.setBoundPoint(new Point(-2, 0));
		mySharedAggrecationDecoration.setFill(true);
		mySharedAggrecationDecoration.setBackgroundColor(ColorConstants.white);
	}

	private void initNavigabilityDecorations() {
		myNavigableDecoration = new ComposablePolygonDecoration();
		myNavigableDecoration.setTemplate(ARROW.getCopy());
		myNavigableDecoration.setBoundPoint(new Point(-1, 0));

		myNonNavigableDecoration = new ComposablePolygonDecoration();
		myNonNavigableDecoration.setScale(4, 3);
		myNonNavigableDecoration.setTemplate(CROSS.getCopy());
		myNonNavigableDecoration.setBoundPoint(new Point(-2, 0));
	}

	private void initOwnedEndDecorations() {
		myOwnedEndDecoration = new DotDecoration();
		myOwnedEndDecoration.setRadius(2);
		myOwnedEndDecoration.setFill(true);
	}

	public void showDiamond(boolean show) {
		if (show) {
			addDecoration(myCompositeAggrecationDecoration);
		} else {
			removeDecoration(myCompositeAggrecationDecoration);
		}
	}

}
