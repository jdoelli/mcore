package com.montages.mcore.diagram.draw2d.figures;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public abstract class HTMLExpression {

	protected static final String EMPTY = "";

	public static final Map<String, String> specialsXml;

	public static final Map<String, String> xmlSpecials;

	static {
		xmlSpecials = new HashMap<String, String>();
		xmlSpecials.put("amp", "&");
		xmlSpecials.put("lt", "<");
		xmlSpecials.put("gt", ">");

		specialsXml = invert(xmlSpecials);
	}

	public static <K, V> Map<V, K> invert(Map<K, V> map) {
		if (map == null) {
			return Collections.emptyMap();
		}
		Map<V, K> result = new HashMap<V, K>();
		for (K key : map.keySet()) {
			V value = map.get(key);
			if (value == null) {
				continue;
			}
			result.put(value, key);
		}
		return result;
	}

	/**
	 * should never be null
	 */
	private final String myValue;

	public HTMLExpression(String value) {
		myValue = value == null ? EMPTY : value;
	}

	protected abstract String getStart();

	protected abstract String getEnd();

	/**
	 * @return string without the Start and the End expression sequences
	 */
	public String getValue() {
		return myValue;
	}

	public String getExpression() {
		return getStart() + myValue + getEnd();
	}

	public String prepareForDocumentBuilder() {
		return getExpression();
	}

	/**
	 * Base tag class
	 */
	public static abstract class Tag extends HTMLExpression {

		private Tag myRelatedTag = null;

		public Tag(String value) {
			super(value);
		}

		protected void addRelatedTag(Tag tag) {
			myRelatedTag = tag;
		}

		public void setRelatedTag(Tag tag) {
			myRelatedTag = tag;
			if (tag != null) {
				tag.addRelatedTag(this);
			}
		}

		public Tag getRelated() {
			return myRelatedTag;
		}

		public boolean isTagValue(String value) {
			if (value == null || getValue() == null) {
				return false;
			}
			return getValue().toLowerCase().equals(value.toLowerCase());
		}

		@Override
		public String prepareForDocumentBuilder() {
			if (getRelated() != null) {
				return super.prepareForDocumentBuilder();
			}
			return new TextExpression(getExpression()).prepareForDocumentBuilder();
		}
	}

	public static class OpenTag extends Tag implements NodeExpression {

		public static final String START_OPEN_TAG = "<";

		public static final String END_OPEN_TAG = ">";

		private List<HTMLExpression> myChildren = null;

		private NodeExpression myParent = null;

		public OpenTag(String value) {
			super(value);
		}

		@Override
		public List<HTMLExpression> getChildren() {
			if (myChildren == null) {
				myChildren = new LinkedList<HTMLExpression>();
			}
			return myChildren;
		}

		@Override
		public void addChild(HTMLExpression content) {
			getChildren().add(content);
			if (content instanceof OpenTag) {
				((OpenTag) content).setParent(this);
			}
		}

		@Override
		protected String getEnd() {
			return END_OPEN_TAG;
		}

		@Override
		protected String getStart() {
			return START_OPEN_TAG;
		}

		@Override
		public void setParent(NodeExpression paretn) {
			myParent = paretn;
		}

		@Override
		public NodeExpression getParent() {
			return myParent;
		}

		@Override
		public String prepareForDocumentBuilder() {
			StringBuilder result = new StringBuilder(super.prepareForDocumentBuilder());
			for (HTMLExpression expr : getChildren()) {
				result.append(expr.prepareForDocumentBuilder());
			}
			return result.toString();
		}
	}

	public static class CloseTag extends Tag {

		public static final String START_CLOSE_TAG = "</";

		public static final String END_CLOSE_TAG = ">";

		public CloseTag(String value) {
			super(value);
		}

		@Override
		protected String getEnd() {
			return END_CLOSE_TAG;
		}

		@Override
		protected String getStart() {
			return START_CLOSE_TAG;
		}
	}

	public static class SpecialXmlExpression extends HTMLExpression {

		public static final String START_SPECIAL = "&";

		public static final String END_SPECIAL = ";";

		public SpecialXmlExpression(String exp) {
			super(exp);
		}

		@Override
		protected String getStart() {
			return START_SPECIAL;
		}

		@Override
		protected String getEnd() {
			return END_SPECIAL;
		}

		@Override
		public String prepareForDocumentBuilder() {
			String value = specialsXml.get(START_SPECIAL);
			if (value != null) {
				SpecialXmlExpression replaceExpr = new SpecialXmlExpression(value);
				return getExpression().replace(START_SPECIAL, replaceExpr.getExpression());
			}
			return super.prepareForDocumentBuilder();
		}
	}

	public static class TextExpression extends HTMLExpression {

		public TextExpression(String text) {
			super(text);
		}

		@Override
		protected String getEnd() {
			return EMPTY;
		}

		@Override
		protected String getStart() {
			return EMPTY;
		}

		@Override
		public String prepareForDocumentBuilder() {
			StringBuilder result = new StringBuilder(getExpression());
			for (String key : specialsXml.keySet()) {
				String value = specialsXml.get(key);
				if (value == null) {
					continue;
				}
				int next = 0;
				SpecialXmlExpression spec = new SpecialXmlExpression(value);
				while ((next = result.indexOf(key, next)) != -1) {
					result.replace(next, next + key.length(), spec.getExpression());
					next += spec.getExpression().length();
				}
			}
			return result.toString();
		}
	}

	public static interface NodeExpression {

		public void addChild(HTMLExpression child);

		public List<HTMLExpression> getChildren();

		public NodeExpression getParent();

		public void setParent(NodeExpression parent);
	}
}