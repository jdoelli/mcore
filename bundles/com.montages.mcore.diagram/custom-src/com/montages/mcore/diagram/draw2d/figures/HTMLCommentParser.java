/*****************************************************************************
 * Copyright (c) 2010 CEA LIST.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *  Remi Schnekenburger (CEA LIST) remi.schnekenburger@cea.fr - Initial API and implementation
 *****************************************************************************/
package com.montages.mcore.diagram.draw2d.figures;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;

/**
 * Parser for simple html comments
 */
public class HTMLCommentParser {

	/**
	 * Parse the given text
	 *
	 * @param text
	 *            the string to parse
	 *
	 * @return the nodes result of the parsing of the text
	 */
	public static NodeList parse(String text) {
		NodeList nodelist = null;
		try {
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
			documentBuilderFactory.setNamespaceAware(true);
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			String prepearedText = HTMLCleaner.prepareForDocumentBuilder(text);
			// fail silently. if not set, default error handler will print an error message in the console
			documentBuilder.setErrorHandler(new DefaultHandler());
			Document document = documentBuilder.parse(new InputSource(new StringReader(prepearedText)));
			nodelist = document.getChildNodes();
		} catch (ParserConfigurationException e) {
			McoreDiagramEditorPlugin.getInstance().logError("Parsing failed.", e);
			nodelist = new EmptyNodeList();
		} catch (IOException e) {
			McoreDiagramEditorPlugin.getInstance().logError("Parsing failed.", e);
			nodelist = new EmptyNodeList();
		} catch (SAXException e) {
			// fail silently
			// Activator.log.debug(e.getMessage());
			nodelist = new EmptyNodeList();
		} catch (Exception e) {
			McoreDiagramEditorPlugin.getInstance().logError("Parsing failed.", e);
			nodelist = new EmptyNodeList();
		}
		return nodelist;
	}

	/**
	 * empty node list implementation
	 */
	public static class EmptyNodeList implements NodeList {

		/**
		 * {@inheritDoc}
		 */
		@Override
		public int getLength() {
			return 0;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public Node item(int index) {
			return null;
		}

	}

}
