package com.montages.mcore.diagram.draw2d.figures;

import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.CloseTag;
import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.OpenTag;
import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.SpecialXmlExpression;
import com.montages.mcore.diagram.draw2d.figures.HTMLExpression.TextExpression;

public abstract class BaseHTMLExpressionParser {

	private final String html;

	private static final String EMPTY = "";

	public BaseHTMLExpressionParser(String text) {
		html = text != null ? text : EMPTY;
	}

	protected abstract boolean isValid(String value);

	protected abstract HTMLExpression buildExpression(String value);

	protected abstract String getStartExpression();

	protected abstract String getEndExpression();

	public final boolean isValid(int start) {
		if (html.length() == 0) {
			return false;
		}
		if (start == -1 || start > html.length()) {
			return false;
		}
		String expresion = getExpression(start);
		if (false == expresion.startsWith(getStartExpression()) || false == expresion.endsWith(getEndExpression())) {
			return false;
		}
		return isValid(getValueFromExpression(expresion));
	}

	public int findNextValidExpression(int pos) {
		int nextValidPos = html.indexOf(getStartExpression(), pos);
		while (nextValidPos != -1) {
			if (isValid(nextValidPos)) {
				break;
			} else {
				nextValidPos = html.indexOf(getStartExpression(), nextValidPos + 1);
			}
		}
		return nextValidPos;
	}

	public final HTMLExpression parse(int start) {
		if (!isValid(start)) {
			return createEmptyTextObject();
		}
		String expression = getExpression(start);
		String value = getValueFromExpression(expression);

		HTMLExpression result = buildExpression(value);
		if (result != null) {
			return result;
		}
		return createEmptyTextObject();
	}

	protected String getExpression(int start) {
		int end = -1;
		int startExtLength = getStartExpression().length();
		if (getEndExpression().isEmpty()) {
			end = start + startExtLength + 1;
		} else {
			end = html.indexOf(getEndExpression(), start + startExtLength);
		}
		if (end == -1) {
			return EMPTY;
		}
		return html.substring(start, end + getEndExpression().length());
	}

	protected String getValueFromExpression(String expresion) {
		return expresion.substring(getStartExpression().length(), expresion.length() - getEndExpression().length());
	}

	private TextExpression createEmptyTextObject() {
		return new TextExpression(EMPTY);
	}

	public static class OpanTagParser extends BaseHTMLExpressionParser {

		public OpanTagParser(String text) {
			super(text);
		}

		@Override
		public HTMLExpression buildExpression(String value) {
			return new OpenTag(value);
		}

		@Override
		protected String getStartExpression() {
			return OpenTag.START_OPEN_TAG;
		}

		@Override
		protected String getEndExpression() {
			return OpenTag.END_OPEN_TAG;
		}

		@Override
		protected boolean isValid(String value) {
			if (value.isEmpty()) {
				return false;
			}
			return value.indexOf('/') == -1;
		}
	}

	public static class CloseTagParser extends BaseHTMLExpressionParser {

		public CloseTagParser(String text) {
			super(text);
		}

		@Override
		public HTMLExpression buildExpression(String value) {
			return new CloseTag(value);
		}

		@Override
		protected String getStartExpression() {
			return CloseTag.START_CLOSE_TAG;
		}

		@Override
		protected String getEndExpression() {
			return CloseTag.END_CLOSE_TAG;
		}

		@Override
		protected boolean isValid(String value) {
			return false == value.isEmpty();
		}
	}

	public static class SpecialXmlExpressionParser extends BaseHTMLExpressionParser {

		public SpecialXmlExpressionParser(String text) {
			super(text);
		}

		@Override
		public HTMLExpression buildExpression(String value) {
			return new SpecialXmlExpression(value);
		}

		@Override
		protected String getStartExpression() {
			return SpecialXmlExpression.START_SPECIAL;
		}

		@Override
		protected String getEndExpression() {
			return SpecialXmlExpression.END_SPECIAL;
		}

		@Override
		protected boolean isValid(String value) {
			return HTMLExpression.xmlSpecials.containsKey(value);
		}
	}
}