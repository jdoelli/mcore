package com.montages.mcore.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.RulerGridPreferencePage;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramRulersAndGridPreferencePage extends RulerGridPreferencePage {

	/**
	* @generated
	*/
	public DiagramRulersAndGridPreferencePage() {
		setPreferenceStore(McoreDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
