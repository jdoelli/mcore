package com.montages.mcore.diagram.preferences;

import org.eclipse.gmf.runtime.diagram.ui.preferences.PrintingPreferencePage;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;

/**
 * @generated
 */
public class DiagramPrintingPreferencePage extends PrintingPreferencePage {

	/**
	* @generated
	*/
	public DiagramPrintingPreferencePage() {
		setPreferenceStore(McoreDiagramEditorPlugin.getInstance().getPreferenceStore());
	}
}
