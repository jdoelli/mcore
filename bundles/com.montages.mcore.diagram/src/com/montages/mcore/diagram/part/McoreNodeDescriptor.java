package com.montages.mcore.diagram.part;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.tooling.runtime.update.UpdaterNodeDescriptor;

/**
* @generated
*/
public class McoreNodeDescriptor extends UpdaterNodeDescriptor {

	/**
	* @generated
	*/
	public McoreNodeDescriptor(EObject modelElement, int visualID) {
		super(modelElement, visualID);
	}

}
