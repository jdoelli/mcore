package com.montages.mcore.diagram.part;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.update.DiagramUpdater;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.MClassiferPropertiesCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierLiteralEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierSuperTypeEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectType2EditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotSlotCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentsLinkEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * @generated
 */
public class McoreDiagramUpdater {

	/**
	 * @generated NOT
	 */
	public static boolean isShortcutOrphaned(View view) {
		String type = view.getType();
		if ("Note".equals(type) || "Text".equals(type)) {
			return false;
		}
		return !view.isSetElement() || view.getElement() == null || view.getElement().eIsProxy() || view.getElement() == ((View) view.eContainer()).getElement()
				|| ((View) view.eContainer()).getElement() == null;
	}

	/**
	 * @generated
	 */
	public static List<McoreNodeDescriptor> getSemanticChildren(View view) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MPackageEditPart.VISUAL_ID:
			return getMPackage_1000SemanticChildren(view);
		case MObjectSlotCompartmentEditPart.VISUAL_ID:
			return getMObjectStatus_7010SemanticChildren(view);
		case MClassiferPropertiesCompartmentEditPart.VISUAL_ID:
			return getMClassifierProperties_7011SemanticChildren(view);
		case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
			return getMObjectStatus_7020SemanticChildren(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated NOT
	 */
	public static List<McoreNodeDescriptor> getMPackage_1000SemanticChildren(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		LinkedList<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		MPackage modelElement = (MPackage) view.getElement();
		MComponent component = modelElement.getContainingComponent();
		for (Iterator<EObject> it = component.eAllContents(); it.hasNext();) {
			EObject next = it.next();
			if (next instanceof MObject) {
				result.addAll(mObject2McoreNodeDescriptor((MObject) next, view));
			}
		}

		result.addAll(getMPackage_1000SemanticChildrenGen(view));
		return result;
	}

	/**
	 * @added
	 */
	private static List<McoreNodeDescriptor> mObject2McoreNodeDescriptor(MObject mObject, View view) {
		List<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		int visualID = McoreVisualIDRegistry.getNodeVisualID(view, mObject);
		if (visualID == MObjectEditPart.VISUAL_ID) {
			result.add(new McoreNodeDescriptor(mObject, visualID));
		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	public static List<McoreNodeDescriptor> getMPackage_1000SemanticChildrenGen(View view) {
		if (!view.isSetElement()) {
			return Collections.emptyList();
		}
		MPackage modelElement = (MPackage) view.getElement();
		LinkedList<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getClassifier().iterator(); it.hasNext();) {
			MClassifier childElement = (MClassifier) it.next();
			int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MClassifierEditPart.VISUAL_ID) {
				result.add(new McoreNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		// for (Iterator<?> it = modelElement.getObject().iterator(); it.hasNext();) {
		// MObject childElement = (MObject) it.next();
		// int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
		// if (visualID == MObjectEditPart.VISUAL_ID) {
		// result.add(new McoreNodeDescriptor(childElement, visualID));
		// continue;
		// }
		// }
		return result;
	}

	/**
	* @generated
	*/
	public static List<McoreNodeDescriptor> getMObjectStatus_7010SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		MObject modelElement = (MObject) containerView.getElement();
		LinkedList<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getPropertyInstance().iterator(); it.hasNext();) {
			MPropertyInstance childElement = (MPropertyInstance) it.next();
			int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MObjectSlotEditPart.VISUAL_ID) {
				result.add(new McoreNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	public static List<McoreNodeDescriptor> getMClassifierProperties_7011SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		MClassifier modelElement = (MClassifier) containerView.getElement();
		LinkedList<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		collectPropertyDescriptors(view, modelElement.getProperty(), result);
		collectPropertyInGroups(view, modelElement.getPropertyGroup(), result);
		for (Iterator<?> it = modelElement.getLiteral().iterator(); it.hasNext();) {
			MLiteral childElement = (MLiteral) it.next();
			int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MClassifierLiteralEditPart.VISUAL_ID) {
				result.add(new McoreNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	public static List<McoreNodeDescriptor> getMObjectStatus_7020SemanticChildren(View view) {
		if (false == view.eContainer() instanceof View) {
			return Collections.emptyList();
		}
		View containerView = (View) view.eContainer();
		if (!containerView.isSetElement()) {
			return Collections.emptyList();
		}
		MObject modelElement = (MObject) containerView.getElement();
		LinkedList<McoreNodeDescriptor> result = new LinkedList<McoreNodeDescriptor>();
		for (Iterator<?> it = modelElement.getPropertyInstance().iterator(); it.hasNext();) {
			MPropertyInstance childElement = (MPropertyInstance) it.next();
			int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MObjectSlotEditPart.VISUAL_ID) {
				result.add(new McoreNodeDescriptor(childElement, visualID));
				continue;
			}
		}
		return result;
	}

	/**
	 * @added
	 */
	private static void collectPropertyInGroups(View view, List<MPropertiesGroup> groups, LinkedList<McoreNodeDescriptor> result) {
		for (MPropertiesGroup group : groups) {
			collectPropertyDescriptors(view, group.getOwnedProperty(), result);
			collectPropertyInGroups(view, group.getPropertyGroup(), result);
		}
	}

	/**
	 * @added
	 */
	private static void collectPropertyDescriptors(View view, List<MProperty> properties, LinkedList<McoreNodeDescriptor> result) {
		for (Iterator<?> it = properties.iterator(); it.hasNext();) {
			MProperty childElement = (MProperty) it.next();
			int visualID = McoreVisualIDRegistry.getNodeVisualID(view, childElement);
			if (visualID == MClassifierPropertyEditPart.VISUAL_ID) {
				result.add(new McoreNodeDescriptor(childElement, visualID));
				continue;
			}
		}
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getContainedLinks(View view) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MPackageEditPart.VISUAL_ID:
			return getMPackage_1000ContainedLinks(view);
		case MClassifierEditPart.VISUAL_ID:
			return getMClassifier_2001ContainedLinks(view);
		case MObjectEditPart.VISUAL_ID:
			return getMObject_2002ContainedLinks(view);
		case MObjectWithSlotEditPart.VISUAL_ID:
			return getMObject_2003ContainedLinks(view);
		case MObjectSlotEditPart.VISUAL_ID:
			return getMPropertyInstance_3003ContainedLinks(view);
		case MClassifierPropertyEditPart.VISUAL_ID:
			return getMProperty_3004ContainedLinks(view);
		case MClassifierLiteralEditPart.VISUAL_ID:
			return getMLiteral_3005ContainedLinks(view);
		case MPropertyEditPart.VISUAL_ID:
			return getMProperty_4003ContainedLinks(view);
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getMPropertyInstance_4004ContainedLinks(view);
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			return getMPropertyInstance_4005ContainedLinks(view);
		case MPropertyContainmentEditPart.VISUAL_ID:
			return getMProperty_4006ContainedLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getIncomingLinks(View view) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MClassifierEditPart.VISUAL_ID:
			return getMClassifier_2001IncomingLinks(view);
		case MObjectEditPart.VISUAL_ID:
			return getMObject_2002IncomingLinks(view);
		case MObjectWithSlotEditPart.VISUAL_ID:
			return getMObject_2003IncomingLinks(view);
		case MObjectSlotEditPart.VISUAL_ID:
			return getMPropertyInstance_3003IncomingLinks(view);
		case MClassifierPropertyEditPart.VISUAL_ID:
			return getMProperty_3004IncomingLinks(view);
		case MClassifierLiteralEditPart.VISUAL_ID:
			return getMLiteral_3005IncomingLinks(view);
		case MPropertyEditPart.VISUAL_ID:
			return getMProperty_4003IncomingLinks(view);
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getMPropertyInstance_4004IncomingLinks(view);
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			return getMPropertyInstance_4005IncomingLinks(view);
		case MPropertyContainmentEditPart.VISUAL_ID:
			return getMProperty_4006IncomingLinks(view);
		}
		return Collections.emptyList();
	}
	
	/**
	 * @generated NOT
	 */
	public static List<McoreLinkDescriptor> getIncomingLinks(View view, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MClassifierEditPart.VISUAL_ID:
			return getMClassifier_2001IncomingLinks(view, crossReferences);
		case MObjectEditPart.VISUAL_ID:
			return getMObject_2002IncomingLinks(view, crossReferences);
		case MObjectWithSlotEditPart.VISUAL_ID:
			return getMObject_2003IncomingLinks(view, crossReferences);
		case MObjectSlotEditPart.VISUAL_ID:
			return getMPropertyInstance_3003IncomingLinks(view);
		case MClassifierPropertyEditPart.VISUAL_ID:
			return getMProperty_3004IncomingLinks(view);
		case MClassifierLiteralEditPart.VISUAL_ID:
			return getMLiteral_3005IncomingLinks(view);
		case MPropertyEditPart.VISUAL_ID:
			return getMProperty_4003IncomingLinks(view);
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getMPropertyInstance_4004IncomingLinks(view);
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			return getMPropertyInstance_4005IncomingLinks(view);
		case MPropertyContainmentEditPart.VISUAL_ID:
			return getMProperty_4006IncomingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getOutgoingLinks(View view) {
		switch (McoreVisualIDRegistry.getVisualID(view)) {
		case MClassifierEditPart.VISUAL_ID:
			return getMClassifier_2001OutgoingLinks(view);
		case MObjectEditPart.VISUAL_ID:
			return getMObject_2002OutgoingLinks(view);
		case MObjectWithSlotEditPart.VISUAL_ID:
			return getMObject_2003OutgoingLinks(view);
		case MObjectSlotEditPart.VISUAL_ID:
			return getMPropertyInstance_3003OutgoingLinks(view);
		case MClassifierPropertyEditPart.VISUAL_ID:
			return getMProperty_3004OutgoingLinks(view);
		case MClassifierLiteralEditPart.VISUAL_ID:
			return getMLiteral_3005OutgoingLinks(view);
		case MPropertyEditPart.VISUAL_ID:
			return getMProperty_4003OutgoingLinks(view);
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getMPropertyInstance_4004OutgoingLinks(view);
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			return getMPropertyInstance_4005OutgoingLinks(view);
		case MPropertyContainmentEditPart.VISUAL_ID:
			return getMProperty_4006OutgoingLinks(view);
		}
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPackage_1000ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMClassifier_2001ContainedLinks(View view) {
		MClassifier modelElement = (MClassifier) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MClassifier_SuperType_4002(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MProperty_4003(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MProperty_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMObject_2002ContainedLinks(View view) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MObject_Type_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4004(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4005(modelElement));
		return result;
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMObject_2003ContainedLinks(View view) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MObject_Type_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4004(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_3003ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_3004ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMLiteral_3005ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_4003ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_4004ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMPropertyInstance_4005ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMProperty_4006ContainedLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMClassifier_2001IncomingLinks(View view) {
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer.find(view.eResource().getResourceSet().getResources());
		return getMClassifier_2001IncomingLinks(view, crossReferences);
	}
	
	/**
	 * @generated NOT
	 */
	public static List<McoreLinkDescriptor> getMClassifier_2001IncomingLinks(View view, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		MClassifier modelElement = (MClassifier) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getIncomingFeatureModelFacetLinks_MObject_Type_4001(modelElement, crossReferences));
		result.addAll(getIncomingFeatureModelFacetLinks_MClassifier_SuperType_4002(modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_MProperty_4003(modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_MProperty_4006(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated NOT
	 */
	public static List<McoreLinkDescriptor> getMObject_2002IncomingLinks(View view) {
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer.find(view.eResource().getResourceSet().getResources());
		return getMObject_2002IncomingLinks(view, crossReferences);
	}
	
	/**
	 * @generated NOT
	 */
	public static List<McoreLinkDescriptor> getMObject_2002IncomingLinks(View view, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_MPropertyInstance_4004(modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_MPropertyInstance_4005(modelElement, crossReferences));
		return result;
	}

	/**
	* @generated NOT
	*/
	public static List<McoreLinkDescriptor> getMObject_2003IncomingLinks(View view) {
		Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences = EcoreUtil.CrossReferencer.find(view.eResource().getResourceSet().getResources());
		return getMObject_2003IncomingLinks(view, crossReferences);
	}
	
	/**
	* @generated NOT
	*/
	public static List<McoreLinkDescriptor> getMObject_2003IncomingLinks(View view, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_MPropertyInstance_4004(modelElement, crossReferences));
		result.addAll(getIncomingTypeModelFacetLinks_MPropertyInstance_4005(modelElement, crossReferences));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_3003IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_3004IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMLiteral_3005IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_4003IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_4004IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMPropertyInstance_4005IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMProperty_4006IncomingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMClassifier_2001OutgoingLinks(View view) {
		MClassifier modelElement = (MClassifier) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MClassifier_SuperType_4002(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MProperty_4003(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MProperty_4006(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMObject_2002OutgoingLinks(View view) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MObject_Type_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4004(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4005(modelElement));
		return result;
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMObject_2003OutgoingLinks(View view) {
		MObject modelElement = (MObject) view.getElement();
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getOutgoingFeatureModelFacetLinks_MObject_Type_4001(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4004(modelElement));
		result.addAll(getContainedTypeModelFacetLinks_MPropertyInstance_4005(modelElement));
		return result;
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_3003OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_3004OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMLiteral_3005OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMProperty_4003OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	public static List<McoreLinkDescriptor> getMPropertyInstance_4004OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMPropertyInstance_4005OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	* @generated
	*/
	public static List<McoreLinkDescriptor> getMProperty_4006OutgoingLinks(View view) {
		return Collections.emptyList();
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getContainedTypeModelFacetLinks_MProperty_4003(MPropertiesContainer container) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		for (Iterator<?> links = container.getProperty().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof MProperty) {
				continue;
			}
			MProperty link = (MProperty) linkObject;
			if (MPropertyEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			MClassifier dst = link.getTypeDefinition();
			result.add(new McoreLinkDescriptor(container, dst, link, McoreElementTypes.MProperty_4003, MPropertyEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	private static Collection<McoreLinkDescriptor> getContainedTypeModelFacetLinks_MPropertyInstance_4004(MObject container) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		for (Iterator<?> links = container.getPropertyInstance().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof MPropertyInstance) {
				continue;
			}
			MPropertyInstance link = (MPropertyInstance) linkObject;
			if (!McoreVisualIDRegistry.getLinkListWithClassVisualID(link).contains(MPropertyInstanceEditPart.VISUAL_ID)) {
				continue;
			}
			for (MObjectReference ref : link.getInternalReferencedObject()) {
				MObject dst = ref.getReferencedObject();
				result.add(new McoreLinkDescriptor(container, dst, link, McoreElementTypes.MPropertyInstance_4004, MPropertyInstanceEditPart.VISUAL_ID));

			}
		}
		return result;
	}

	/**
	* @generated NOT
	*/
	private static Collection<McoreLinkDescriptor> getContainedTypeModelFacetLinks_MPropertyInstance_4005(MObject container) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		for (Iterator<?> links = container.getPropertyInstance().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof MPropertyInstance) {
				continue;
			}
			MPropertyInstance link = (MPropertyInstance) linkObject;
			if (!McoreVisualIDRegistry.getLinkListWithClassVisualID(link).contains(MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID)) {
				continue;
			}
			for (MObject dst : link.getInternalContainedObject()) {
				result.add(new McoreLinkDescriptor(container, dst, link, McoreElementTypes.MPropertyInstance_4005, MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<McoreLinkDescriptor> getContainedTypeModelFacetLinks_MProperty_4006(MPropertiesContainer container) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		for (Iterator<?> links = container.getProperty().iterator(); links.hasNext();) {
			EObject linkObject = (EObject) links.next();
			if (false == linkObject instanceof MProperty) {
				continue;
			}
			MProperty link = (MProperty) linkObject;
			if (MPropertyContainmentEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			MClassifier dst = link.getTypeDefinition();
			result.add(new McoreLinkDescriptor(container, dst, link, McoreElementTypes.MProperty_4006, MPropertyContainmentEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getIncomingFeatureModelFacetLinks_MObject_Type_4001(MClassifier target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == ObjectsPackage.eINSTANCE.getMObject_Type()) {
				result.add(new McoreLinkDescriptor(setting.getEObject(), target, McoreElementTypes.MObjectType_4001, MObjectType2EditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getIncomingFeatureModelFacetLinks_MClassifier_SuperType_4002(MClassifier target,
			Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() == McorePackage.eINSTANCE.getMClassifier_SuperType()) {
				result.add(new McoreLinkDescriptor(setting.getEObject(), target, McoreElementTypes.MClassifierSuperType_4002, MClassifierSuperTypeEditPart.VISUAL_ID));
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getIncomingTypeModelFacetLinks_MProperty_4003(MClassifier target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != McorePackage.eINSTANCE.getMProperty_TypeDefinition() || false == setting.getEObject() instanceof MProperty) {
				continue;
			}
			MProperty link = (MProperty) setting.getEObject();
			if (MPropertyEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			if (false == link.eContainer() instanceof MPropertiesContainer) {
				continue;
			}
			MPropertiesContainer container = (MPropertiesContainer) link.eContainer();
			result.add(new McoreLinkDescriptor(container, target, link, McoreElementTypes.MProperty_4003, MPropertyEditPart.VISUAL_ID));

		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getIncomingTypeModelFacetLinks_MPropertyInstance_4004Gen(MObject target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalContainedObject() || false == setting.getEObject() instanceof MPropertyInstance) {
				continue;
			}
			MPropertyInstance link = (MPropertyInstance) setting.getEObject();
			if (MPropertyInstanceEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			if (false == link.eContainer() instanceof MObject) {
				continue;
			}
			MObject container = (MObject) link.eContainer();
			result.add(new McoreLinkDescriptor(container, target, link, McoreElementTypes.MPropertyInstance_4004, MPropertyInstanceEditPart.VISUAL_ID));

		}
		return result;
	}

	/**
	 * @generated NOT
	 */
	private static Collection<McoreLinkDescriptor> getIncomingTypeModelFacetLinks_MPropertyInstance_4004(MObject target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		result.addAll(getIncomingTypeModelFacetLinks_MPropertyInstance_4004Gen(target, crossReferences));
		EObject link = target.eContainer();
		if (link instanceof MPropertyInstance) {
			if (false == link.eContainer() instanceof MObject) {
				return result;
			}
			MObject container = (MObject) link.eContainer();
			result.add(new McoreLinkDescriptor(container, target, link, McoreElementTypes.MPropertyInstance_4005, MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<McoreLinkDescriptor> getIncomingTypeModelFacetLinks_MPropertyInstance_4005(MObject target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalContainedObject() || false == setting.getEObject() instanceof MPropertyInstance) {
				continue;
			}
			MPropertyInstance link = (MPropertyInstance) setting.getEObject();
			if (MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			if (false == link.eContainer() instanceof MObject) {
				continue;
			}
			MObject container = (MObject) link.eContainer();
			result.add(new McoreLinkDescriptor(container, target, link, McoreElementTypes.MPropertyInstance_4005, MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID));

		}
		return result;
	}

	/**
	* @generated
	*/
	private static Collection<McoreLinkDescriptor> getIncomingTypeModelFacetLinks_MProperty_4006(MClassifier target, Map<EObject, Collection<EStructuralFeature.Setting>> crossReferences) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		Collection<EStructuralFeature.Setting> settings = crossReferences.get(target);
		for (EStructuralFeature.Setting setting : settings) {
			if (setting.getEStructuralFeature() != McorePackage.eINSTANCE.getMProperty_TypeDefinition() || false == setting.getEObject() instanceof MProperty) {
				continue;
			}
			MProperty link = (MProperty) setting.getEObject();
			if (MPropertyContainmentEditPart.VISUAL_ID != McoreVisualIDRegistry.getLinkWithClassVisualID(link)) {
				continue;
			}
			if (false == link.eContainer() instanceof MPropertiesContainer) {
				continue;
			}
			MPropertiesContainer container = (MPropertiesContainer) link.eContainer();
			result.add(new McoreLinkDescriptor(container, target, link, McoreElementTypes.MProperty_4006, MPropertyContainmentEditPart.VISUAL_ID));

		}
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getOutgoingFeatureModelFacetLinks_MObject_Type_4001(MObject source) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		MClassifier destination = source.getType();
		if (destination == null) {
			return result;
		}
		result.add(new McoreLinkDescriptor(source, destination, McoreElementTypes.MObjectType_4001, MObjectType2EditPart.VISUAL_ID));
		return result;
	}

	/**
	 * @generated
	 */
	private static Collection<McoreLinkDescriptor> getOutgoingFeatureModelFacetLinks_MClassifier_SuperType_4002(MClassifier source) {
		LinkedList<McoreLinkDescriptor> result = new LinkedList<McoreLinkDescriptor>();
		for (Iterator<?> destinations = source.getSuperType().iterator(); destinations.hasNext();) {
			MClassifier destination = (MClassifier) destinations.next();
			result.add(new McoreLinkDescriptor(source, destination, McoreElementTypes.MClassifierSuperType_4002, MClassifierSuperTypeEditPart.VISUAL_ID));
		}
		return result;
	}

	/**
	 * @generated
	 */
	public static final DiagramUpdater TYPED_INSTANCE = new DiagramUpdater() {

		/**
		 * @generated
		 */
		@Override
		public List<McoreNodeDescriptor> getSemanticChildren(View view) {
			return McoreDiagramUpdater.getSemanticChildren(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<McoreLinkDescriptor> getContainedLinks(View view) {
			return McoreDiagramUpdater.getContainedLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<McoreLinkDescriptor> getIncomingLinks(View view) {
			return McoreDiagramUpdater.getIncomingLinks(view);
		}

		/**
		 * @generated
		 */
		@Override
		public List<McoreLinkDescriptor> getOutgoingLinks(View view) {
			return McoreDiagramUpdater.getOutgoingLinks(view);
		}
	};

}
