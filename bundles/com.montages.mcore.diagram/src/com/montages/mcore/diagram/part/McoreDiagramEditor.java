package com.montages.mcore.diagram.part;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.IOperationHistoryListener;
import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.commands.operations.OperationHistoryEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.workspace.AbstractEMFOperation;
import org.eclipse.emf.workspace.util.WorkspaceSynchronizer;
import org.eclipse.gef.EditPartViewer;
import org.eclipse.gef.KeyHandler;
import org.eclipse.gef.KeyStroke;
import org.eclipse.gef.palette.PaletteRoot;
import org.eclipse.gmf.runtime.common.ui.services.marker.MarkerNavigationService;
import org.eclipse.gmf.runtime.diagram.core.preferences.PreferencesHint;
import org.eclipse.gmf.runtime.diagram.ui.actions.ActionIds;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.parts.DiagramDropTargetListener;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDiagramDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocument;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.document.IDocumentProvider;
import org.eclipse.gmf.runtime.diagram.ui.resources.editor.parts.DiagramDocumentEditor;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.runtime.notation.impl.DiagramImpl;
import org.eclipse.gmf.runtime.notation.impl.EdgeImpl;
import org.eclipse.gmf.runtime.notation.impl.NodeImpl;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.dnd.TransferData;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorMatchingStrategy;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.dialogs.SaveAsDialog;
import org.eclipse.ui.ide.IGotoMarker;
import org.eclipse.ui.navigator.resources.ProjectExplorer;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.part.IShowInTargetList;
import org.eclipse.ui.part.ShowInContext;

import com.montages.mcore.diagram.navigator.McoreNavigatorItem;
import com.montages.transactions.IMcoreTransactionEventListener;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEvent;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

/**
* @generated NOT
*/
public class McoreDiagramEditor extends DiagramDocumentEditor implements IGotoMarker, IMcoreTransactionEventListener {

	/**
	 * @added
	 */
	private LastClickPositionProvider myLastClickPositionProvider;

	/**
	* @generated
	*/
	public static final String ID = "com.montages.mcore.diagram.part.McoreDiagramEditorID"; //$NON-NLS-1$

	/**
	* @generated
	*/
	public static final String CONTEXT_ID = "com.montages.mcore.diagram.ui.diagramContext"; //$NON-NLS-1$

	/**
	* @generated NOT
	*/
	private IEditorInput myInput = null;

	/**
	* @generated NOT
	*/
	private McoreDiagramEditingDoamin myDomain;

	/**
	* @generated NOT
	*/
	private boolean saveLock = false;

	private boolean changeLock = false;

	/**
	* @generated NOT
	*/
	private List<Object> stopListenSenders = new ArrayList<Object>();

	/**
	* @generated
	*/
	public McoreDiagramEditor() {
		super(true);
	}

	/**
	* @generated
	*/
	protected String getContextID() {
		return CONTEXT_ID;
	}

	/**
	* @generated
	*/
	protected PaletteRoot createPaletteRoot(PaletteRoot existingPaletteRoot) {
		PaletteRoot root = super.createPaletteRoot(existingPaletteRoot);
		new McorePaletteFactory().fillPalette(root);
		return root;
	}

	/**
	* @generated
	*/
	protected PreferencesHint getPreferencesHint() {
		return McoreDiagramEditorPlugin.DIAGRAM_PREFERENCES_HINT;
	}

	/**
	* @generated
	*/
	public String getContributorId() {
		return McoreDiagramEditorPlugin.ID;
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("rawtypes")
	public Object getAdapter(Class type) {
		if (type == IShowInTargetList.class) {
			return new IShowInTargetList() {

				public String[] getShowInTargetIds() {
					return new String[] { ProjectExplorer.VIEW_ID };
				}
			};
		}
		return super.getAdapter(type);
	}

	/**
	* @generated
	*/
	protected IDocumentProvider getDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput || input instanceof URIEditorInput) {
			return McoreDiagramEditorPlugin.getInstance().getDocumentProvider();
		}
		return super.getDocumentProvider(input);
	}

	/**
	* @generated
	*/
	public TransactionalEditingDomain getEditingDomain() {
		IDocument document = getEditorInput() != null ? getDocumentProvider().getDocument(getEditorInput()) : null;
		if (document instanceof IDiagramDocument) {
			return ((IDiagramDocument) document).getEditingDomain();
		}
		return super.getEditingDomain();
	}

	/**
	* @generated
	*/
	protected void setDocumentProvider(IEditorInput input) {
		if (input instanceof IFileEditorInput || input instanceof URIEditorInput) {
			setDocumentProvider(McoreDiagramEditorPlugin.getInstance().getDocumentProvider());
		} else {
			super.setDocumentProvider(input);
		}
	}

	/**
	* @generated
	*/
	public void gotoMarker(IMarker marker) {
		MarkerNavigationService.getInstance().gotoMarker(this, marker);
	}

	/**
	* @generated
	*/
	public boolean isSaveAsAllowed() {
		return true;
	}

	/**
	* @generated
	*/
	public void doSaveAs() {
		performSaveAs(new NullProgressMonitor());
	}

	/**
	* @generated
	*/
	protected void performSaveAs(IProgressMonitor progressMonitor) {
		Shell shell = getSite().getShell();
		IEditorInput input = getEditorInput();
		SaveAsDialog dialog = new SaveAsDialog(shell);
		IFile original = input instanceof IFileEditorInput ? ((IFileEditorInput) input).getFile() : null;
		if (original != null) {
			dialog.setOriginalFile(original);
		}
		dialog.create();
		IDocumentProvider provider = getDocumentProvider();
		if (provider == null) {
			return;
		}
		if (provider.isDeleted(input) && original != null) {
			String message = NLS.bind(Messages.McoreDiagramEditor_SavingDeletedFile, original.getName());
			dialog.setErrorMessage(null);
			dialog.setMessage(message, IMessageProvider.WARNING);
		}
		if (dialog.open() == Window.CANCEL) {
			if (progressMonitor != null) {
				progressMonitor.setCanceled(true);
			}
			return;
		}
		IPath filePath = dialog.getResult();
		if (filePath == null) {
			if (progressMonitor != null) {
				progressMonitor.setCanceled(true);
			}
			return;
		}
		IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
		IFile file = workspaceRoot.getFile(filePath);
		final IEditorInput newInput = new FileEditorInput(file);
		// Check if the editor is already open
		IEditorMatchingStrategy matchingStrategy = getEditorDescriptor().getEditorMatchingStrategy();
		IEditorReference[] editorRefs = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getEditorReferences();
		for (int i = 0; i < editorRefs.length; i++) {
			if (matchingStrategy.matches(editorRefs[i], newInput)) {
				MessageDialog.openWarning(shell, Messages.McoreDiagramEditor_SaveAsErrorTitle, Messages.McoreDiagramEditor_SaveAsErrorMessage);
				return;
			}
		}
		boolean success = false;
		try {
			provider.aboutToChange(newInput);
			getDocumentProvider(newInput).saveDocument(progressMonitor, newInput, getDocumentProvider().getDocument(getEditorInput()), true);
			success = true;
		} catch (CoreException x) {
			IStatus status = x.getStatus();
			if (status == null || status.getSeverity() != IStatus.CANCEL) {
				ErrorDialog.openError(shell, Messages.McoreDiagramEditor_SaveErrorTitle, Messages.McoreDiagramEditor_SaveErrorMessage, x.getStatus());
			}
		} finally {
			provider.changed(newInput);
			if (success) {
				setInput(newInput);
			}
		}
		if (progressMonitor != null) {
			progressMonitor.setCanceled(!success);
		}
	}

	/**
	* @generated NOT
	*/
	protected void performSave(boolean overwrite, IProgressMonitor progressMonitor) {
		if (saveLock) {
			return;
		}
		saveLock = true;
		try {
			myDomain.publish(McoreTransactionEventType.STOP_RS_LISTENING, this);
			super.performSave(overwrite, progressMonitor);
			if (myDomain != null) {
				myDomain.publish(McoreTransactionEventType.SAVE, this);
			}

			myDomain.publish(McoreTransactionEventType.START_RS_LISTENING, this);
			myDomain.doSave();
		} finally {
			firePropertyChange(PROP_DIRTY);
			saveLock = false;
		}
	}

	/**
	* @generated
	*/
	public ShowInContext getShowInContext() {
		return new ShowInContext(getEditorInput(), getNavigatorSelection());
	}

	/**
	* @generated
	*/
	private ISelection getNavigatorSelection() {
		IDiagramDocument document = getDiagramDocument();
		if (document == null) {
			return StructuredSelection.EMPTY;
		}
		Diagram diagram = document.getDiagram();
		if (diagram == null || diagram.eResource() == null) {
			return StructuredSelection.EMPTY;
		}
		IFile file = WorkspaceSynchronizer.getFile(diagram.eResource());
		if (file != null) {
			McoreNavigatorItem item = new McoreNavigatorItem(diagram, file, false);
			return new StructuredSelection(item);
		}
		return StructuredSelection.EMPTY;
	}

	/**
	* @generated
	*/
	protected void configureGraphicalViewer() {
		super.configureGraphicalViewer();
		DiagramEditorContextMenuProvider provider = new DiagramEditorContextMenuProvider(this, getDiagramGraphicalViewer());
		getDiagramGraphicalViewer().setContextMenu(provider);
		getSite().registerContextMenu(ActionIds.DIAGRAM_EDITOR_CONTEXT_MENU, provider, getDiagramGraphicalViewer());
	}

	/**
	* @generated
	*/
	protected void initializeGraphicalViewerGen() {
		super.initializeGraphicalViewer();
		getDiagramGraphicalViewer().addDropTargetListener(new DropTargetListener(getDiagramGraphicalViewer(), LocalSelectionTransfer.getTransfer()) {

			protected Object getJavaObject(TransferData data) {
				if (LocalSelectionTransfer.getTransfer().getSelection() != null) {
					return LocalSelectionTransfer.getTransfer().getSelection();
				}
				return LocalSelectionTransfer.getTransfer().nativeToJava(data);
			}

		});
		getDiagramGraphicalViewer().addDropTargetListener(new DropTargetListener(getDiagramGraphicalViewer(), LocalTransfer.getInstance()) {

			protected Object getJavaObject(TransferData data) {
				return LocalTransfer.getInstance().nativeToJava(data);
			}

		});
	}

	/**
	 * @generated NOT
	 */
	protected void initializeGraphicalViewer() {
		initializeGraphicalViewerGen();
		startupLastClickPositionProvider();
	}

	/**
	 * @added
	 */
	protected void startupLastClickPositionProvider() {
		if (myLastClickPositionProvider == null) {
			myLastClickPositionProvider = new LastClickPositionProvider(this);
			myLastClickPositionProvider.attachToService();
		}
	}

	/**
	 * @added
	 */
	protected void shutDownLastClickPositionProvider() {
		if (myLastClickPositionProvider != null) {
			myLastClickPositionProvider.detachFromService();
			myLastClickPositionProvider.dispose();
			myLastClickPositionProvider = null;
		}
	}

	/**
	 * @generated NOT
	 */
	@Override
	public void dispose() {
		shutDownLastClickPositionProvider();
		if (myDomain != null) {
			myDomain.removeListener(this);
			McoreEditingDomainFactory.getInstance().dispose(myDomain, this);
			myDomain = null;
		}

		myInput = null;
		super.dispose();
	}

	/**
	* @generated
	*/
	private abstract class DropTargetListener extends DiagramDropTargetListener {

		/**
		* @generated
		*/
		public DropTargetListener(EditPartViewer viewer, Transfer xfer) {
			super(viewer, xfer);
		}

		/**
		* @generated
		*/
		protected List getObjectsBeingDropped() {
			TransferData data = getCurrentEvent().currentDataType;
			HashSet<URI> uris = new HashSet<URI>();

			Object transferedObject = getJavaObject(data);
			if (transferedObject instanceof IStructuredSelection) {
				IStructuredSelection selection = (IStructuredSelection) transferedObject;
				for (Iterator<?> it = selection.iterator(); it.hasNext();) {
					Object nextSelectedObject = it.next();
					if (nextSelectedObject instanceof McoreNavigatorItem) {
						View view = ((McoreNavigatorItem) nextSelectedObject).getView();
						nextSelectedObject = view.getElement();
					} else if (nextSelectedObject instanceof IAdaptable) {
						IAdaptable adaptable = (IAdaptable) nextSelectedObject;
						nextSelectedObject = adaptable.getAdapter(EObject.class);
					}

					if (nextSelectedObject instanceof EObject) {
						EObject modelElement = (EObject) nextSelectedObject;
						uris.add(EcoreUtil.getURI(modelElement));
					}
				}
			}

			ArrayList<EObject> result = new ArrayList<EObject>(uris.size());
			for (URI nextURI : uris) {
				EObject modelObject = getEditingDomain().getResourceSet().getEObject(nextURI, true);
				result.add(modelObject);
			}
			return result;
		}

		/**
		* @generated
		*/
		protected abstract Object getJavaObject(TransferData data);

	}

	/**
	* @generated NOT
	*/
	protected TransactionalEditingDomain createEditingDomain() {
		assert myInput == null;
		myDomain = McoreEditingDomainFactory.getInstance().createEditingDomain(getEditingDomainID(), this);
		myDomain.getHistory().addOperationHistoryListener(new IOperationHistoryListener() {

			@Override
			public void historyNotification(OperationHistoryEvent event) {
				firePropertyChange(PROP_DIRTY);
			}
		});
		myDomain.addListener(this);
		return myDomain;
	}

	/**
	* @generated NOT
	*/
	@Override
	protected IUndoContext getUndoContext() {
		if (myDomain != null) {
			return myDomain.getUndoContext();
		}
		return super.getUndoContext();
	}

	/**
	* @generated NOT
	*/
	protected String getEditingDomainID() {
		URI uri = PathUtils.getInputURI(myInput);

		if (uri == null) {
			return super.getEditingDomainID();
		}

		return PathUtils.getDomainIDFromDiagram(uri);
	}

	/**
	* @generated NOT
	*/
	@Override
	public void setInput(IEditorInput input) {
		myInput = input;
		super.setInput(input);
	}

	/**
	* @generated NOT
	*/
	@Override
	public void handleMcoreEvent(McoreTransactionEvent event) {
		if (McoreTransactionEventType.SAVE == event.getType()) {
			this.performSave(true, new NullProgressMonitor());
		} else if (McoreTransactionEventType.STOP_RS_LISTENING == event.getType()) {
			if (stopListenSenders.contains(event.getPublisher())) {
				return;
			}
			stopListenSenders.add(event.getPublisher());
			stopListening();
		} else if (McoreTransactionEventType.START_RS_LISTENING == event.getType()) {
			stopListenSenders.remove(event.getPublisher());
			if (stopListenSenders.isEmpty()) {
				startListening();
			}
		} else if (McoreTransactionEventType.INPUT_CHANGED == event.getType()) {
			if (!changeLock) {
				changeLock = true;
				try {
					handleEditorInputChanged();
				} finally {
					changeLock = false;
				}
			}
		}
	}

	/**
	* @generated NOT
	*/
	@Override
	public boolean isDirty() {
		return super.isDirty() || (myDomain != null && myDomain.isModified());
	}

	protected void inputUpate() {
		Diagram d = getDiagram();
		if (d.eIsProxy()) {
			d = (Diagram) resolveElement(d, myDomain);
			if (d != null) {
				getDocumentProvider().getDocument(getEditorInput()).setContent(d);
			}
		}
		final IGraphicalEditPart part = (IGraphicalEditPart) getDiagramGraphicalViewer().getRootEditPart().getChildren().get(0);
		try {
			myDomain.getHistory().execute(new RecoveryCommand(myDomain, "", part), new NullProgressMonitor(), null);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
	}

	protected static void recoveryEditParts(IGraphicalEditPart part, McoreDiagramEditingDoamin domain) {
		EObject next = part.getNotationView();
		if (next != null) {
			if (next.eIsProxy()) {
				EObject resolved = resolveElement(next, domain);
				if (resolved == null) {
					return;
				}
				part.setModel(resolved);
				Class<? extends EObject> resolverClass = resolved.getClass();
				if ((resolverClass == NodeImpl.class) || (resolverClass == EdgeImpl.class) || (resolverClass == DiagramImpl.class)) {
					View view = (View) resolved;
					EObject element = view.getElement();
					if (element != null && element.eIsProxy()) {
						element = resolveElement(element, domain);
						view.setElement(element);
					}
				}
			}
		}
		for (Object child : part.getChildren()) {
			if (child instanceof IGraphicalEditPart) {
				recoveryEditParts((IGraphicalEditPart) child, domain);
			}
		}
	}

	private static EObject resolveElement(EObject unresolvedElement, McoreDiagramEditingDoamin domain) {
		URI unresolvedElementURI = EcoreUtil.getURI(unresolvedElement);
		Resource diagramResource = domain.getResourceSet().getResource(unresolvedElementURI.trimFragment(), false);
		if (diagramResource == null || diagramResource.getContents().isEmpty()) {
			return null;
		}
		String unresolvedID = unresolvedElementURI.fragment();
		for (Iterator<EObject> it = diagramResource.getAllContents(); it.hasNext();) {
			EObject next = it.next();
			if (unresolvedID.equals(EcoreUtil.getURI(next).fragment())) {
				return next;
			}
		}
		return null;
	}

	protected void releaseDomain() {
		IEditorInput input = myInput;
		dispose();
		myInput = input;
	}

	@Override
	protected KeyHandler getKeyHandler() {
		KeyHandler keyHandler = super.getKeyHandler();
		keyHandler.put(KeyStroke.getPressed(SWT.DEL, SWT.DEL, SWT.CTRL), getActionRegistry().getAction(ActionIds.ACTION_DELETE_FROM_MODEL));
		return keyHandler;
	}

	private static class RecoveryCommand extends AbstractEMFOperation {

		private final McoreDiagramEditingDoamin myDomain;

		private final IGraphicalEditPart myRootEditPart;

		public RecoveryCommand(McoreDiagramEditingDoamin domain, String label, IGraphicalEditPart root) {
			super(domain, label);
			myDomain = domain;
			myRootEditPart = root;
		}

		@Override
		protected IStatus doExecute(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
			recoveryEditParts(myRootEditPart, myDomain);
			myRootEditPart.refresh();
			return Status.OK_STATUS;
		}
	}
}
