package com.montages.mcore.diagram.edit.commands;

import java.util.Objects;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.edit.policies.McoreBaseItemSemanticEditPolicy;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;

/**
 * @generated
 */
public class MPropertyInstanceCreateCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final EObject source;

	/**
	 * @generated
	 */
	private final EObject target;

	/**
	 * @generated
	 */
	public MPropertyInstanceCreateCommand(CreateRelationshipRequest request, EObject source, EObject target) {
		super(request.getLabel(), null, request);
		this.source = source;
		this.target = target;
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (source == null && target == null) {
			return false;
		}
		if (source != null && false == source instanceof MObject) {
			return false;
		}
		if (target != null && false == target instanceof MObject) {
			return false;
		}
		if (getSource() == null) {
			return true; // link creation is in progress; source is not defined yet
		}
		// target may be null here but it's possible to check constraint
		return McoreBaseItemSemanticEditPolicy.getLinkConstraints().canCreateMPropertyInstance_4004(getSource(), getTarget());
	}

	/**
	 * @generated NOT
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException("Invalid arguments in create link command"); //$NON-NLS-1$
		}
		MPropertyInstance propertyInstance = findOrCreateMPropertyInstance();
		
		MObjectReference newReference = ObjectsFactory.eINSTANCE.createMObjectReference();
		newReference.setReferencedObject(getTarget());
		MObject existedInternalReferencedObject = propertyInstance.getInternalReferencedObject().stream()
			.map(MObjectReference::getReferencedObject)
			.filter(mObject -> Objects.equals(mObject, newReference.getReferencedObject()))
			.findFirst().orElse(null);
		if (existedInternalReferencedObject != null) {
			return CommandResult.newCancelledCommandResult();
		}
		propertyInstance.getInternalReferencedObject().add(newReference);
		doConfigure(propertyInstance, monitor, info);
		((CreateElementRequest) getRequest()).setNewElement(propertyInstance);
		return CommandResult.newOKCommandResult(propertyInstance);
	}

	private MPropertyInstance findOrCreateMPropertyInstance() {
		MPropertyInstance existedElement = findSelectedPropertyInstance();
		if (existedElement != null) {
			return existedElement;
		}
		MPropertyInstance propertyInstance = getSource().getPropertyInstance().stream()
			.filter(MPropertyInstanceCreateCommand::isPropertyContainerType)
			.findFirst().orElse(null);
		if (propertyInstance == null) {
			propertyInstance = ObjectsFactory.eINSTANCE.createMPropertyInstance();
			getSource().getPropertyInstance().add(propertyInstance);
		}
		return propertyInstance;
	}
	
	private static boolean isPropertyContainerType(MPropertyInstance propertyInstance) {
		MProperty property = propertyInstance.getProperty();
		return property != null && Objects.equals(property.getContainingClassifier(), property.getType());
	}

	private MPropertyInstance findSelectedPropertyInstance() {
		CreateRelationshipRequest request = (CreateRelationshipRequest) getRequest();
		Object selectionObject = request.getParameter(ISelection.class.getName());
		if (selectionObject == null || false == selectionObject instanceof IStructuredSelection) {
			return null;
		}
		IStructuredSelection selection = (IStructuredSelection) selectionObject;
		if (selection.size() != 1) {
			return null;
		}
		Object selectedObject = selection.getFirstElement();
		if (false == selectedObject instanceof IGraphicalEditPart) {
			return null;
		}
		IGraphicalEditPart editPart = (IGraphicalEditPart) selectedObject;
		if (false == editPart.resolveSemanticElement() instanceof MPropertyInstance) {
			return null;
		}
		MPropertyInstance result = (MPropertyInstance) editPart.resolveSemanticElement();
		if (result.eContainer() != getSource()) {
			return null;
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected void doConfigure(MPropertyInstance newElement, IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IElementType elementType = ((CreateElementRequest) getRequest()).getElementType();
		ConfigureRequest configureRequest = new ConfigureRequest(getEditingDomain(), newElement, elementType);
		configureRequest.setClientContext(((CreateElementRequest) getRequest()).getClientContext());
		configureRequest.addParameters(getRequest().getParameters());
		configureRequest.setParameter(CreateRelationshipRequest.SOURCE, getSource());
		configureRequest.setParameter(CreateRelationshipRequest.TARGET, getTarget());
		ICommand configureCommand = elementType.getEditCommand(configureRequest);
		if (configureCommand != null && configureCommand.canExecute()) {
			configureCommand.execute(monitor, info);
		}
	}

	/**
	 * @generated
	 */
	protected void setElementToEdit(EObject element) {
		throw new UnsupportedOperationException();
	}

	/**
	 * @generated
	 */
	protected MObject getSource() {
		return (MObject) source;
	}

	/**
	 * @generated
	 */
	protected MObject getTarget() {
		return (MObject) target;
	}

}
