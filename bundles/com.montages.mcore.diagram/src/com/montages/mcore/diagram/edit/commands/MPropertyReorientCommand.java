package com.montages.mcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.edit.policies.McoreBaseItemSemanticEditPolicy;

/**
 * @generated
 */
public class MPropertyReorientCommand extends EditElementCommand {

	/**
	* @generated
	*/
	private final int reorientDirection;

	/**
	* @generated
	*/
	private final EObject oldEnd;

	/**
	* @generated
	*/
	private final EObject newEnd;

	/**
	* @generated
	*/
	public MPropertyReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	* @generated
	*/
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof MProperty) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	* @generated
	*/
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof MPropertiesContainer && newEnd instanceof MPropertiesContainer)) {
			return false;
		}
		MClassifier target = getLink().getTypeDefinition();
		return McoreBaseItemSemanticEditPolicy.getLinkConstraints().canExistMProperty_4003(getLink(), getNewSource(), target);
	}

	/**
	* @generated
	*/
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof MClassifier && newEnd instanceof MClassifier)) {
			return false;
		}
		if (!(getLink().eContainer() instanceof MPropertiesContainer)) {
			return false;
		}
		MPropertiesContainer source = (MPropertiesContainer) getLink().eContainer();
		return McoreBaseItemSemanticEditPolicy.getLinkConstraints().canExistMProperty_4003(getLink(), source, getNewTarget());
	}

	/**
	* @generated
	*/
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	* @generated
	*/
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().getProperty().remove(getLink());
		getNewSource().getProperty().add(getLink());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected CommandResult reorientTarget() throws ExecutionException {
		getLink().setTypeDefinition(getNewTarget());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	* @generated
	*/
	protected MProperty getLink() {
		return (MProperty) getElementToEdit();
	}

	/**
	* @generated
	*/
	protected MPropertiesContainer getOldSource() {
		return (MPropertiesContainer) oldEnd;
	}

	/**
	* @generated
	*/
	protected MPropertiesContainer getNewSource() {
		return (MPropertiesContainer) newEnd;
	}

	/**
	* @generated
	*/
	protected MClassifier getOldTarget() {
		return (MClassifier) oldEnd;
	}

	/**
	* @generated
	*/
	protected MClassifier getNewTarget() {
		return (MClassifier) newEnd;
	}
}
