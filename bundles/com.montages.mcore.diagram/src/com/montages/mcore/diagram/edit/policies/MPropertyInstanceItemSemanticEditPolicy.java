package com.montages.mcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MPropertyInstanceItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public MPropertyInstanceItemSemanticEditPolicy() {
		super(McoreElementTypes.MPropertyInstance_4004);
	}

	/**
	* @generated
	*/
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		return getGEFWrapper(new DestroyElementCommand(req));
	}

}
