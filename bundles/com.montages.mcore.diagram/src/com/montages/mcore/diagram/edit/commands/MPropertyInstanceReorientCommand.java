package com.montages.mcore.diagram.edit.commands;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;

import com.montages.mcore.MClassifier;
import com.montages.mcore.diagram.edit.policies.McoreBaseItemSemanticEditPolicy;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;

/**
 * @generated
 */
public class MPropertyInstanceReorientCommand extends EditElementCommand {

	/**
	 * @generated
	 */
	private final int reorientDirection;

	/**
	 * @generated
	 */
	private final EObject oldEnd;

	/**
	 * @generated
	 */
	private final EObject newEnd;

	/**
	 * @generated
	 */
	public MPropertyInstanceReorientCommand(ReorientRelationshipRequest request) {
		super(request.getLabel(), request.getRelationship(), request);
		reorientDirection = request.getDirection();
		oldEnd = request.getOldRelationshipEnd();
		newEnd = request.getNewRelationshipEnd();
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		if (false == getElementToEdit() instanceof MPropertyInstance) {
			return false;
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return canReorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return canReorientTarget();
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean canReorientSource() {
		if (!(oldEnd instanceof MObject && newEnd instanceof MObject)) {
			return false;
		}
		if (getLink().getInternalContainedObject().size() != 1) {
			return false;
		}
		MObject target = (MObject) getLink().getInternalContainedObject().get(0);
		return McoreBaseItemSemanticEditPolicy.getLinkConstraints().canExistMPropertyInstance_4004(getLink(), getNewSource(), target);
	}

	/**
	 * @generated NOT
	 */
	protected boolean canReorientTarget() {
		if (!(oldEnd instanceof MObject && newEnd instanceof MObject)) {
			return false;
		}
		if (!(getLink().eContainer() instanceof MObject)) {
			return false;
		}
		MClassifier propertyInstanceType = getLink().getProperty().getType();
		MClassifier newEndType = ((MObject) newEnd).getType();
		if (propertyInstanceType != newEndType) {
			return false;
		}
		MObject source = (MObject) getLink().eContainer();
		return McoreBaseItemSemanticEditPolicy.getLinkConstraints().canExistMPropertyInstance_4004(getLink(), source, getNewTarget());
	}

	/**
	 * @generated
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		if (!canExecute()) {
			throw new ExecutionException("Invalid arguments in reorient link command"); //$NON-NLS-1$
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_SOURCE) {
			return reorientSource();
		}
		if (reorientDirection == ReorientRelationshipRequest.REORIENT_TARGET) {
			return reorientTarget();
		}
		throw new IllegalStateException();
	}

	/**
	 * @generated
	 */
	protected CommandResult reorientSource() throws ExecutionException {
		getOldSource().getPropertyInstance().remove(getLink());
		getNewSource().getPropertyInstance().add(getLink());
		return CommandResult.newOKCommandResult(getLink());
	}

	/**
	 * @generated NOT
	 */
	protected CommandResult reorientTarget() throws ExecutionException {
		MPropertyInstance property = getLink();
		MObjectReference reference = findObjectReference(property, getOldTarget());
		if (reference != null) {
			reference.setReferencedObject(getNewTarget());
		}
		return CommandResult.newOKCommandResult(getLink());
	}

	private MObjectReference findObjectReference(MPropertyInstance propertyInstance, MObject target) {
		for (MObjectReference next : propertyInstance.getInternalReferencedObject()) {
			if (next.getReferencedObject() == target) {
				return next;
			}
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected MPropertyInstance getLink() {
		return (MPropertyInstance) getElementToEdit();
	}

	/**
	 * @generated
	 */
	protected MObject getOldSource() {
		return (MObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected MObject getNewSource() {
		return (MObject) newEnd;
	}

	/**
	 * @generated
	 */
	protected MObject getOldTarget() {
		return (MObject) oldEnd;
	}

	/**
	 * @generated
	 */
	protected MObject getNewTarget() {
		return (MObject) newEnd;
	}
}
