package com.montages.mcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;

import com.montages.mcore.diagram.edit.commands.MObjectSlotCreateCommand;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MObjectSlotCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MObjectSlotCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2002);
	}

	/**
	 * @generated
	 */
	protected Command getCreateCommand(CreateElementRequest req) {
		if (McoreElementTypes.MPropertyInstance_3003 == req.getElementType()) {
			return getGEFWrapper(new MObjectSlotCreateCommand(req));
		}
		return super.getCreateCommand(req);
	}

}
