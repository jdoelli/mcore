package com.montages.mcore.diagram.edit.policies;

import java.util.Iterator;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.common.core.command.ICompositeCommand;
import org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand;
import org.eclipse.gmf.runtime.emf.commands.core.command.CompositeTransactionalCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyElementRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientReferenceRelationshipRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.ReorientRelationshipRequest;
import org.eclipse.gmf.runtime.notation.Edge;
import org.eclipse.gmf.runtime.notation.Node;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.diagram.edit.commands.MObjectTypeCreateCommand;
import com.montages.mcore.diagram.edit.commands.MObjectTypeReorientCommand;
import com.montages.mcore.diagram.edit.commands.MPropertyInstanceContainmentsLinkCreateCommand;
import com.montages.mcore.diagram.edit.commands.MPropertyInstanceCreateCommand;
import com.montages.mcore.diagram.edit.commands.MPropertyInstanceReorientCommand;
import com.montages.mcore.diagram.edit.parts.MObjectSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectType2EditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotInstanceDerivationsCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotInstanceDescriptionCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotSlotCompartmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentsLinkEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MObjectWithSlotItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public MObjectWithSlotItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2003);
	}

	/**
	 * @generated
	 */
	protected Command getDestroyElementCommand(DestroyElementRequest req) {
		View view = (View) getHost().getModel();
		CompositeTransactionalCommand cmd = new CompositeTransactionalCommand(getEditingDomain(), null);
		cmd.setTransactionNestingEnabled(false);
		for (Iterator<?> it = view.getTargetEdges().iterator(); it.hasNext();) {
			Edge incomingLink = (Edge) it.next();
			if (McoreVisualIDRegistry.getVisualID(incomingLink) == MPropertyInstanceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
			if (McoreVisualIDRegistry.getVisualID(incomingLink) == MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(incomingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), incomingLink));
				continue;
			}
		}
		for (Iterator<?> it = view.getSourceEdges().iterator(); it.hasNext();) {
			Edge outgoingLink = (Edge) it.next();
			if (McoreVisualIDRegistry.getVisualID(outgoingLink) == MObjectType2EditPart.VISUAL_ID) {
				DestroyReferenceRequest r = new DestroyReferenceRequest(outgoingLink.getSource().getElement(), null, outgoingLink.getTarget().getElement(), false);
				cmd.add(new DestroyReferenceCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (McoreVisualIDRegistry.getVisualID(outgoingLink) == MPropertyInstanceEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
			if (McoreVisualIDRegistry.getVisualID(outgoingLink) == MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID) {
				DestroyElementRequest r = new DestroyElementRequest(outgoingLink.getElement(), false);
				cmd.add(new DestroyElementCommand(r));
				cmd.add(new DeleteCommand(getEditingDomain(), outgoingLink));
				continue;
			}
		}
		EAnnotation annotation = view.getEAnnotation("Shortcut"); //$NON-NLS-1$
		if (annotation == null) {
			// there are indirectly referenced children, need extra commands: false
			addDestroyChildNodesCommand(cmd);
			addDestroyShortcutsCommand(cmd, view);
			// delete host element
			cmd.add(new DestroyElementCommand(req));
		} else {
			cmd.add(new DeleteCommand(getEditingDomain(), view));
		}
		return getGEFWrapper(cmd.reduce());
	}

	/**
	 * @generated
	 */
	private void addDestroyChildNodesCommand(ICompositeCommand cmd) {
		View view = (View) getHost().getModel();
		for (Iterator<?> nit = view.getChildren().iterator(); nit.hasNext();) {
			Node node = (Node) nit.next();
			switch (McoreVisualIDRegistry.getVisualID(node)) {
			case MObjectWithSlotInstanceDescriptionCompartmentEditPart.VISUAL_ID:
				for (Iterator<?> cit = node.getChildren().iterator(); cit.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (McoreVisualIDRegistry.getVisualID(cnode)) {
					}
				}
				break;
			case MObjectWithSlotSlotCompartmentEditPart.VISUAL_ID:
				for (Iterator<?> cit = node.getChildren().iterator(); cit.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (McoreVisualIDRegistry.getVisualID(cnode)) {
					case MObjectSlotEditPart.VISUAL_ID:
						cmd.add(new DestroyElementCommand(new DestroyElementRequest(getEditingDomain(), cnode.getElement(), false))); // directlyOwned: true
						// don't need explicit deletion of cnode as parent's view deletion would clean child views as well 
						// cmd.add(new org.eclipse.gmf.runtime.diagram.core.commands.DeleteCommand(getEditingDomain(), cnode));
						break;
					}
				}
				break;
			case MObjectWithSlotInstanceDerivationsCompartmentEditPart.VISUAL_ID:
				for (Iterator<?> cit = node.getChildren().iterator(); cit.hasNext();) {
					Node cnode = (Node) cit.next();
					switch (McoreVisualIDRegistry.getVisualID(cnode)) {
					}
				}
				break;
			}
		}
	}

	/**
	 * @generated
	 */
	protected Command getCreateRelationshipCommand(CreateRelationshipRequest req) {
		Command command = req.getTarget() == null ? getStartCreateRelationshipCommand(req) : getCompleteCreateRelationshipCommand(req);
		return command != null ? command : super.getCreateRelationshipCommand(req);
	}

	/**
	 * @generated
	 */
	protected Command getStartCreateRelationshipCommand(CreateRelationshipRequest req) {
		if (McoreElementTypes.MObjectType_4001 == req.getElementType()) {
			return getGEFWrapper(new MObjectTypeCreateCommand(req, req.getSource(), req.getTarget()));
		}
		if (McoreElementTypes.MPropertyInstance_4004 == req.getElementType()) {
			return getGEFWrapper(new MPropertyInstanceCreateCommand(req, req.getSource(), req.getTarget()));
		}
		if (McoreElementTypes.MPropertyInstance_4005 == req.getElementType()) {
			return getGEFWrapper(new MPropertyInstanceContainmentsLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * @generated
	 */
	protected Command getCompleteCreateRelationshipCommand(CreateRelationshipRequest req) {
		if (McoreElementTypes.MObjectType_4001 == req.getElementType()) {
			return null;
		}
		if (McoreElementTypes.MPropertyInstance_4004 == req.getElementType()) {
			return getGEFWrapper(new MPropertyInstanceCreateCommand(req, req.getSource(), req.getTarget()));
		}
		if (McoreElementTypes.MPropertyInstance_4005 == req.getElementType()) {
			return getGEFWrapper(new MPropertyInstanceContainmentsLinkCreateCommand(req, req.getSource(), req.getTarget()));
		}
		return null;
	}

	/**
	 * Returns command to reorient EClass based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientRelationshipCommand(ReorientRelationshipRequest req) {
		switch (getVisualID(req)) {
		case MPropertyInstanceEditPart.VISUAL_ID:
			return getGEFWrapper(new MPropertyInstanceReorientCommand(req));
		}
		return super.getReorientRelationshipCommand(req);
	}

	/**
	 * Returns command to reorient EReference based link. New link target or source
	 * should be the domain model element associated with this node.
	 * 
	 * @generated
	 */
	protected Command getReorientReferenceRelationshipCommand(ReorientReferenceRelationshipRequest req) {
		switch (getVisualID(req)) {
		case MObjectType2EditPart.VISUAL_ID:
			return getGEFWrapper(new MObjectTypeReorientCommand(req));
		}
		return super.getReorientReferenceRelationshipCommand(req);
	}

}
