package com.montages.mcore.diagram.edit.commands;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.operations.UndoContext;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.transaction.Transaction;
import org.eclipse.emf.workspace.EMFCommandOperation;
import org.eclipse.gmf.runtime.common.core.command.CommandResult;
import org.eclipse.gmf.runtime.common.core.command.ICommand;
import org.eclipse.gmf.runtime.common.ui.util.DisplayUtils;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.type.core.commands.EditElementCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.ConfigureRequest;
import org.eclipse.gmf.runtime.emf.type.core.requests.CreateElementRequest;
import org.eclipse.gmf.runtime.notation.View;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.diagram.actions.EObjectSelectionDialog;
import com.montages.mcore.diagram.parsers.MessageFormatParser;
import com.montages.mcore.diagram.part.Messages;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;

/**
 * @generated
 */
public class MObjectWithSlotCreateCommand extends EditElementCommand {

	/**
	 * @added
	 */
	private UndoContext updateActionCommandContext = new UndoContext();

	/**
	 * @generated
	 */
	public MObjectWithSlotCreateCommand(CreateElementRequest req) {
		super(req.getLabel(), null, req);
	}

	/**
	 * FIXME: replace with setElementToEdit()
	 * @generated
	 */
	protected EObject getElementToEdit() {
		EObject container = ((CreateElementRequest) getRequest()).getContainer();
		if (container instanceof View) {
			container = ((View) container).getElement();
		}
		return container;
	}

	/**
	 * @generated
	 */
	public boolean canExecute() {
		return true;

	}

	/**
	 * @generated NOT
	 */
	protected CommandResult doExecuteWithResult(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		List<MObject> mObjects = collectMObject();
		EAttribute[] features = new EAttribute[] { ObjectsPackage.eINSTANCE.getMObject_Id() };
		EObjectSelectionDialog dialog = new EObjectSelectionDialog(DisplayUtils.getDisplay().getActiveShell(), mObjects, new MessageFormatParser(features));
		dialog.setTitle(Messages.MObjectWithSlotCreateCommanDialogTitle);
		int res = dialog.open();
		if (res != 0) {
			return CommandResult.newCancelledCommandResult();
		}
		Object[] selection = dialog.getResult();
		if (selection == null || selection.length != 1) {
			return CommandResult.newCancelledCommandResult();
		}
		MObject mObject = (MObject) selection[0];
		XUpdate update = mObject.doActionUpdate(MObjectAction.CONTAINMENT_SLOT);
		Command cmd = update.getContainingTransition().interpreteTransitionAsCommand();
		if (cmd == null || !cmd.canExecute()) {
			return CommandResult.newErrorCommandResult("Can't execute " + MClassifierAction.CREATE_INSTANCE + " classifier action.");
		}

		EMFCommandOperation oper = new EMFCommandOperation(((McoreDiagramEditingDoamin) getEditingDomain()), cmd, null);

		// add the appropriate context
		oper.addContext(updateActionCommandContext);
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().execute(oper, monitor, info);

		EList<MPropertyInstance> propertyInstances = mObject.getPropertyInstance();
		MPropertyInstance mPropertyInstance = propertyInstances.get(propertyInstances.size() - 1);
		return CommandResult.newOKCommandResult(mPropertyInstance.getInternalContainedObject().get(0));
	}

	/**
	 * Collect MObjects which can contain the containment slots
	 * 
	 * @added
	 */
	private List<MObject> collectMObject() {
		MPackage mPackage = (MPackage) getElementToEdit();
		MComponent component = mPackage.getContainingComponent();
		List<MResourceFolder> folders = component.getResourceFolder();

		//find all mobjects with containment slots
		List<MObject> result = new ArrayList<MObject>();
		for (MResourceFolder folder : folders) {
			for (Iterator<EObject> it = folder.eAllContents(); it.hasNext();) {
				EObject next = it.next();
				if (next instanceof MObject) {
					MObject mObject = (MObject) next;

					MClassifier type = mObject.getType();
					if (type == null) {
						continue;
					}

					for (MProperty mProperty : type.getProperty()) {
						Boolean containment = mProperty.getContainment();
						if (containment != null && containment && mProperty.getType() != null) {
							result.add(mObject);
							break;
						}
					}
				}
			}
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected void doConfigure(MObject newElement, IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IElementType elementType = ((CreateElementRequest) getRequest()).getElementType();
		ConfigureRequest configureRequest = new ConfigureRequest(getEditingDomain(), newElement, elementType);
		configureRequest.setClientContext(((CreateElementRequest) getRequest()).getClientContext());
		configureRequest.addParameters(getRequest().getParameters());
		ICommand configureCommand = elementType.getEditCommand(configureRequest);
		if (configureCommand != null && configureCommand.canExecute()) {
			configureCommand.execute(monitor, info);
		}
	}

	@Override
	protected IStatus doUndo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		IStatus result = null;
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().undo(updateActionCommandContext, null, null);

		return result;
	}

	protected Transaction createLocalTransaction(Map<?, ?> options) throws InterruptedException {
		return ((McoreDiagramEditingDoamin) getEditingDomain()).startTransaction(false, options);
	}

	@Override
	protected IStatus doRedo(IProgressMonitor monitor, IAdaptable info) throws ExecutionException {
		((McoreDiagramEditingDoamin) getEditingDomain()).getHistory().redo(updateActionCommandContext, monitor, info);
		return null;
	}
}
