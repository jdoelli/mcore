package com.montages.mcore.diagram.edit.policies;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class InstanceDerivationsCompartmentItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	 * @generated
	 */
	public InstanceDerivationsCompartmentItemSemanticEditPolicy() {
		super(McoreElementTypes.MObject_2002);
	}

}
