package com.montages.mcore.diagram.edit.parts;

import org.eclipse.draw2d.Border;
import org.eclipse.draw2d.BorderLayout;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.MarginBorder;
import org.eclipse.draw2d.PositionConstants;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.RoundedRectangle;
import org.eclipse.draw2d.Shape;
import org.eclipse.draw2d.StackLayout;
import org.eclipse.draw2d.ToolbarLayout;
import org.eclipse.draw2d.geometry.Dimension;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.gef.EditPart;
import org.eclipse.gef.EditPolicy;
import org.eclipse.gef.Request;
import org.eclipse.gef.commands.Command;
import org.eclipse.gef.editpolicies.LayoutEditPolicy;
import org.eclipse.gef.editpolicies.NonResizableEditPolicy;
import org.eclipse.gef.requests.CreateRequest;
import org.eclipse.gmf.runtime.diagram.core.edithelpers.CreateElementRequestAdapter;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ShapeNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.diagram.ui.requests.CreateViewAndElementRequest;
import org.eclipse.gmf.runtime.draw2d.ui.figures.ConstrainedToolbarLayout;
import org.eclipse.gmf.runtime.draw2d.ui.figures.OneLineBorder;
import org.eclipse.gmf.runtime.draw2d.ui.figures.WrappingLabel;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.gef.ui.figures.DefaultSizeNodeFigure;
import org.eclipse.gmf.runtime.gef.ui.figures.NodeFigure;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.gmf.tooling.runtime.edit.policies.reparent.CreationEditPolicyWithCustomReparent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Display;

import com.montages.mcore.diagram.draw2d.figures.HTMLWrappingLabel;
import com.montages.mcore.diagram.edit.policies.MObjectItemSemanticEditPolicy;
import com.montages.mcore.diagram.edit.policies.ShowHideCompartmentEditPolicy;
import com.montages.mcore.diagram.edit.policies.ShowHideRelatedElementsEditPolicy;
import com.montages.mcore.diagram.edit.policies.ShowHideSlotEditPolicy;
import com.montages.mcore.diagram.layout.FlexCompartmentToolbarLayout;
import com.montages.mcore.diagram.layout.SubCompartmentLayoutManager;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MObjectEditPart extends ShapeNodeEditPart {

	/**
	 * @generated
	 */
	public static final int VISUAL_ID = 2002;

	/**
	 * @generated
	 */
	protected IFigure contentPane;

	/**
	 * @generated
	 */
	protected IFigure primaryShape;

	/**
	 * @generated
	 */
	public MObjectEditPart(View view) {
		super(view);
	}

	/**
	 * @generated
	 */
	protected void createDefaultEditPolicies() {
		installEditPolicy(EditPolicyRoles.CREATION_ROLE, new CreationEditPolicyWithCustomReparent(McoreVisualIDRegistry.TYPED_INSTANCE));
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new MObjectItemSemanticEditPolicy());
		installEditPolicy(EditPolicy.LAYOUT_ROLE, createLayoutEditPolicy());
		installEditPolicy(ShowHideSlotEditPolicy.SHOW_HIDE_SLOT_POLICY, new ShowHideSlotEditPolicy());
		installEditPolicy(ShowHideCompartmentEditPolicy.SHOW_HIDE_COMPARTMENTS_POLICY, new ShowHideCompartmentEditPolicy());
		installEditPolicy(ShowHideRelatedElementsEditPolicy.SHOW_HIDE_RELATED_ELEMENTS_POLICY, new ShowHideRelatedElementsEditPolicy());
		// XXX need an SCR to runtime to have another abstract superclass that would let children add reasonable editpolicies
		// removeEditPolicy(org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles.CONNECTION_HANDLES_ROLE);
	}

	/**
	 * @generated
	 */
	protected LayoutEditPolicy createLayoutEditPolicy() {
		org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy lep = new org.eclipse.gmf.runtime.diagram.ui.editpolicies.LayoutEditPolicy() {

			protected EditPolicy createChildEditPolicy(EditPart child) {
				EditPolicy result = child.getEditPolicy(EditPolicy.PRIMARY_DRAG_ROLE);
				if (result == null) {
					result = new NonResizableEditPolicy();
				}
				return result;
			}

			protected Command getMoveChildrenCommand(Request request) {
				return null;
			}

			protected Command getCreateCommand(CreateRequest request) {
				return null;
			}
		};
		return lep;
	}

	/**
	 * @generated
	 */
	protected IFigure createNodeShape() {
		return primaryShape = new InstanceFigure();
	}

	/**
	 * @generated
	 */
	public InstanceFigure getPrimaryShape() {
		return (InstanceFigure) primaryShape;
	}

	/**
	* @generated NOT
	*/
	protected boolean addFixedChild(EditPart childEditPart) {
		boolean result = addFixedChildGen(childEditPart);
		if (childEditPart instanceof InstanceDescriptionCompartmentEditPart) {
			// add fixed description label to the compartment view port
			IFigure compartmentFigure = ((InstanceDescriptionCompartmentEditPart) childEditPart).getFigure();
			IFigure children = (IFigure) compartmentFigure.getChildren().get(1);
			IFigure viewPort = (IFigure) children.getChildren().get(0);
			IFigure viewPortChildren = (IFigure) viewPort.getChildren().get(0);
			viewPortChildren.add(getPrimaryShape().getInstanceFigureDescriptionLabel());
			return true;
		}
		if (childEditPart instanceof InstanceDerivationsCompartmentEditPart) {
			// add fixed description label to the compartment view port
			IFigure compartmentFigure = ((InstanceDerivationsCompartmentEditPart) childEditPart).getFigure();
			IFigure children = (IFigure) compartmentFigure.getChildren().get(1);
			IFigure viewPort = (IFigure) children.getChildren().get(0);
			IFigure viewPortChildren = (IFigure) viewPort.getChildren().get(0);
			viewPortChildren.add(getPrimaryShape().getInstanceFigureDerivationsLabel());
			return true;
		}
		return result;
	}

	/**
	 * @generated
	 */
	protected boolean addFixedChildGen(EditPart childEditPart) {
		if (childEditPart instanceof MObjectIdEditPart) {
			((MObjectIdEditPart) childEditPart).setLabel(getPrimaryShape().getFigureInstanceFigure_name());
			return true;
		}
		if (childEditPart instanceof MObjectTypeEditPart) {
			((MObjectTypeEditPart) childEditPart).setLabel(getPrimaryShape().getFigureInstanceFigure_type());
			return true;
		}
		if (childEditPart instanceof MObjectDescriptionLabelEditPart) {
			((MObjectDescriptionLabelEditPart) childEditPart).setLabel(getPrimaryShape().getInstanceFigureDescriptionLabel());
			return true;
		}
		if (childEditPart instanceof MObjectDoActionLabelEditPart) {
			((MObjectDoActionLabelEditPart) childEditPart).setLabel(getPrimaryShape().getDoActionInstanceLabel());
			return true;
		}
		if (childEditPart instanceof InstanceDescriptionCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getInstanceDescriptionCompartment();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((InstanceDescriptionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof MObjectSlotCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getSlotCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((MObjectSlotCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof InstanceDerivationsCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getInstanceDerivationsCompartmentFigure();
			setupContentPane(pane); // FIXME each comparment should handle his content pane in his own way 
			pane.add(((InstanceDerivationsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof MObjectIdEditPart) {
			return true;
		}
		if (childEditPart instanceof MObjectTypeEditPart) {
			return true;
		}
		if (childEditPart instanceof MObjectDescriptionLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof MObjectDoActionLabelEditPart) {
			return true;
		}
		if (childEditPart instanceof InstanceDescriptionCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getInstanceDescriptionCompartment();
			pane.remove(((InstanceDescriptionCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof MObjectSlotCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getSlotCompartmentFigure();
			pane.remove(((MObjectSlotCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		if (childEditPart instanceof InstanceDerivationsCompartmentEditPart) {
			IFigure pane = getPrimaryShape().getInstanceDerivationsCompartmentFigure();
			pane.remove(((InstanceDerivationsCompartmentEditPart) childEditPart).getFigure());
			return true;
		}
		return false;
	}

	/**
	 * @generated
	 */
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, -1);
	}

	/**
	 * @generated
	 */
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	 * @generated
	 */
	protected IFigure getContentPaneFor(IGraphicalEditPart editPart) {
		if (editPart instanceof InstanceDescriptionCompartmentEditPart) {
			return getPrimaryShape().getInstanceDescriptionCompartment();
		}
		if (editPart instanceof MObjectSlotCompartmentEditPart) {
			return getPrimaryShape().getSlotCompartmentFigure();
		}
		if (editPart instanceof InstanceDerivationsCompartmentEditPart) {
			return getPrimaryShape().getInstanceDerivationsCompartmentFigure();
		}
		return getContentPane();
	}

	/**
	 * @generated
	 */
	protected NodeFigure createNodePlate() {
		DefaultSizeNodeFigure result = new DefaultSizeNodeFigure(180, 70);
		return result;
	}

	/**
	 * Creates figure for this edit part.
	 * 
	 * Body of this method does not depend on settings in generation model
	 * so you may safely remove <i>generated</i> tag and modify it.
	 * 
	 * @generated
	 */
	protected NodeFigure createNodeFigure() {
		NodeFigure figure = createNodePlate();
		figure.setLayoutManager(new StackLayout());
		IFigure shape = createNodeShape();
		figure.add(shape);
		contentPane = setupContentPane(shape);
		return figure;
	}

	/**
	 * Default implementation treats passed figure as content pane.
	 * Respects layout one may have set for generated figure.
	 * 
	 * @param nodeShape
	 *            instance of generated figure class
	 * @generated
	 */
	protected IFigure setupContentPane(IFigure nodeShape) {
		if (nodeShape.getLayoutManager() == null) {
			ConstrainedToolbarLayout layout = new ConstrainedToolbarLayout();
			layout.setSpacing(5);
			nodeShape.setLayoutManager(layout);
		}
		return nodeShape; // use nodeShape itself as contentPane
	}

	/**
	 * @generated
	 */
	public IFigure getContentPane() {
		if (contentPane != null) {
			return contentPane;
		}
		return super.getContentPane();
	}

	/**
	 * @generated
	 */
	protected void setForegroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setForegroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setBackgroundColor(Color color) {
		if (primaryShape != null) {
			primaryShape.setBackgroundColor(color);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineWidth(int width) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineWidth(width);
		}
	}

	/**
	 * @generated
	 */
	protected void setLineType(int style) {
		if (primaryShape instanceof Shape) {
			((Shape) primaryShape).setLineStyle(style);
		}
	}

	/**
	 * @generated
	 */
	public EditPart getPrimaryChildEditPart() {
		return getChildBySemanticHint(McoreVisualIDRegistry.getType(MObjectIdEditPart.VISUAL_ID));
	}

	/**
	* @generated
	*/
	public EditPart getTargetEditPart(Request request) {
		if (request instanceof CreateViewAndElementRequest) {
			CreateElementRequestAdapter adapter = ((CreateViewAndElementRequest) request).getViewAndElementDescriptor().getCreateElementRequestAdapter();
			IElementType type = (IElementType) adapter.getAdapter(IElementType.class);
			if (type == McoreElementTypes.MPropertyInstance_3003) {
				return getChildBySemanticHint(McoreVisualIDRegistry.getType(MObjectSlotCompartmentEditPart.VISUAL_ID));
			}
		}
		return super.getTargetEditPart(request);
	}

	/**
	 * @generated
	 */
	protected void handleNotificationEvent(Notification event) {
		if (event.getNotifier() == getModel() && EcorePackage.eINSTANCE.getEModelElement_EAnnotations().equals(event.getFeature())) {
			handleMajorSemanticChange();
		} else {
			super.handleNotificationEvent(event);
		}
	}

	/**
	 * @generated
	 */
	public class InstanceFigure extends RoundedRectangle {

		/**
		* @generated NOT
		*/
		private final boolean DO_NOT_SHOW_DERRIVATIONS_LABEL = true;

		/**
		 * @generated
		 */
		private WrappingLabel myInstanceFigure_type;

		/**
		* @generated
		*/
		private RectangleFigure fSlotCompartmentFigure;

		/**
		* @generated
		*/
		private RectangleFigure fInstanceDescriptionCompartment;

		/**
		   * @generated
		   */
		private WrappingLabel fInstanceFigureDescriptionLabel;

		/**
		* @generated
		*/
		private RectangleFigure fInstanceDerivationsCompartmentFigure;

		/**
		   * @generated
		   */
		private WrappingLabel fInstanceFigureDerivationsLabel;

		/**
		* @generated
		*/
		private WrappingLabel fDoActionInstanceLabel;

		/**
		* @generated
		*/
		private WrappingLabel myInstanceFigure_name;

		/**
		 * @generated
		 */
		public InstanceFigure() {

			ToolbarLayout layoutThis = new ToolbarLayout();
			layoutThis.setStretchMinorAxis(true);
			layoutThis.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);

			layoutThis.setSpacing(0);
			layoutThis.setVertical(true);

			this.setLayoutManager(layoutThis);

			this.setCornerDimensions(new Dimension(8, 8));
			this.setForegroundColor(THIS_FORE);
			createContents();
		}

		/**
		 * @generated
		 */
		private void createContentsGen() {

			RectangleFigure instanceFigure_TypeBlock0 = new RectangleFigure();

			instanceFigure_TypeBlock0.setFill(false);
			instanceFigure_TypeBlock0.setOutline(false);

			instanceFigure_TypeBlock0.setBorder(new MarginBorder(5, 5, 5, 5));

			this.add(instanceFigure_TypeBlock0, BorderLayout.TOP);

			BorderLayout layoutInstanceFigure_TypeBlock0 = new BorderLayout();
			instanceFigure_TypeBlock0.setLayoutManager(layoutInstanceFigure_TypeBlock0);

			myInstanceFigure_type = new WrappingLabel();

			myInstanceFigure_type.setFont(MYINSTANCEFIGURE_TYPE_FONT);

			myInstanceFigure_type.setAlignment(PositionConstants.EAST);

			instanceFigure_TypeBlock0.add(myInstanceFigure_type, BorderLayout.RIGHT);

			RectangleFigure instanceFigure_NameBlock0 = new RectangleFigure();

			instanceFigure_NameBlock0.setFill(false);
			instanceFigure_NameBlock0.setOutline(false);

			this.add(instanceFigure_NameBlock0, BorderLayout.CENTER);

			BorderLayout layoutInstanceFigure_NameBlock0 = new BorderLayout();
			instanceFigure_NameBlock0.setLayoutManager(layoutInstanceFigure_NameBlock0);

			myInstanceFigure_name = new WrappingLabel();

			myInstanceFigure_name.setAlignment(PositionConstants.CENTER);

			instanceFigure_NameBlock0.add(myInstanceFigure_name, BorderLayout.CENTER);

			BorderLayout layoutMyInstanceFigure_name = new BorderLayout();
			myInstanceFigure_name.setLayoutManager(layoutMyInstanceFigure_name);

			RectangleFigure instanceDoActionRectangle0 = new RectangleFigure();

			instanceDoActionRectangle0.setFill(false);
			instanceDoActionRectangle0.setOutline(false);

			this.add(instanceDoActionRectangle0, BorderLayout.RIGHT);

			BorderLayout layoutInstanceDoActionRectangle0 = new BorderLayout();
			instanceDoActionRectangle0.setLayoutManager(layoutInstanceDoActionRectangle0);

			fDoActionInstanceLabel = new WrappingLabel();

			fDoActionInstanceLabel.setText("Do...");

			fDoActionInstanceLabel.setFont(FDOACTIONINSTANCELABEL_FONT);

			instanceDoActionRectangle0.add(fDoActionInstanceLabel, BorderLayout.LEFT);

			fInstanceDescriptionCompartment = new RectangleFigure();

			fInstanceDescriptionCompartment.setFill(false);
			fInstanceDescriptionCompartment.setOutline(false);
			fInstanceDescriptionCompartment.setBorder(createBorder0());

			this.add(fInstanceDescriptionCompartment);

			SubCompartmentLayoutManager layoutFInstanceDescriptionCompartment = new SubCompartmentLayoutManager();
			fInstanceDescriptionCompartment.setLayoutManager(layoutFInstanceDescriptionCompartment);

			fInstanceFigureDescriptionLabel = new WrappingLabel();

			fInstanceFigureDescriptionLabel.setText("<enter description>");

			fInstanceFigureDescriptionLabel.setFont(FINSTANCEFIGUREDESCRIPTIONLABEL_FONT);

			fInstanceDescriptionCompartment.add(fInstanceFigureDescriptionLabel);

			fSlotCompartmentFigure = new RectangleFigure();

			fSlotCompartmentFigure.setFill(false);
			fSlotCompartmentFigure.setOutline(false);
			fSlotCompartmentFigure.setBorder(createBorder1());

			this.add(fSlotCompartmentFigure);

			SubCompartmentLayoutManager layoutFSlotCompartmentFigure = new SubCompartmentLayoutManager();
			fSlotCompartmentFigure.setLayoutManager(layoutFSlotCompartmentFigure);

			fInstanceDerivationsCompartmentFigure = new RectangleFigure();

			fInstanceDerivationsCompartmentFigure.setFill(false);
			fInstanceDerivationsCompartmentFigure.setOutline(false);
			fInstanceDerivationsCompartmentFigure.setBorder(createBorder2());

			this.add(fInstanceDerivationsCompartmentFigure);

			SubCompartmentLayoutManager layoutFInstanceDerivationsCompartmentFigure = new SubCompartmentLayoutManager();
			fInstanceDerivationsCompartmentFigure.setLayoutManager(layoutFInstanceDerivationsCompartmentFigure);

			fInstanceFigureDerivationsLabel = new WrappingLabel();

			fInstanceFigureDerivationsLabel.setText("");

			fInstanceFigureDerivationsLabel.setFont(FINSTANCEFIGUREDERIVATIONSLABEL_FONT);

			fInstanceDerivationsCompartmentFigure.add(fInstanceFigureDerivationsLabel);

		}

		/**
		* @generated NOT
		*/
		private void createContents() {
			setupLayout();
			createContentsGen();
			setupDescription();
			setupDerivations();
		}
		
		/**
		* @generated NOT
		*/
		private void setupLayout() {
			ToolbarLayout layoutThis = new FlexCompartmentToolbarLayout(3);
			layoutThis.setStretchMinorAxis(true);
			layoutThis.setMinorAlignment(ToolbarLayout.ALIGN_TOPLEFT);

			layoutThis.setSpacing(0);
			layoutThis.setVertical(true);

			this.setLayoutManager(layoutThis);
		}

		/**
		* @generated
		*/
		private Border createBorder0() {
			OneLineBorder result = new OneLineBorder();

			return result;
		}

		/**
		   * @generated
		   */
		private Border createBorder1() {
			OneLineBorder result = new OneLineBorder();

			return result;
		}

		/**
		   * @generated
		   */
		private Border createBorder2() {
			OneLineBorder result = new OneLineBorder();

			return result;
		}

		/**
			* @generated NOT
			*/
		private void setupDescription() {
			fInstanceDescriptionCompartment.remove(fInstanceFigureDescriptionLabel);
			fInstanceFigureDescriptionLabel = new HTMLWrappingLabel();
			fInstanceFigureDescriptionLabel.setText("<enter description>");
			fInstanceFigureDescriptionLabel.setFont(FINSTANCEFIGUREDESCRIPTIONLABEL_FONT);
			fInstanceDescriptionCompartment.add(fInstanceFigureDescriptionLabel);
		}

		/**
		* @generated NOT
		*/
		private void setupDerivations() {
			if (DO_NOT_SHOW_DERRIVATIONS_LABEL) {
				fInstanceFigureDerivationsLabel.setVisible(false);
				fInstanceDerivationsCompartmentFigure.setVisible(false);
				fInstanceDerivationsCompartmentFigure.setLayoutManager(null);
				return;
			}
			fInstanceDerivationsCompartmentFigure.remove(fInstanceFigureDerivationsLabel);
			fInstanceFigureDerivationsLabel = new HTMLWrappingLabel();
			fInstanceFigureDerivationsLabel.setText("<...pending...>");
			fInstanceFigureDerivationsLabel.setFont(FINSTANCEFIGUREDERIVATIONSLABEL_FONT);
			fInstanceDerivationsCompartmentFigure.add(fInstanceFigureDerivationsLabel);
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureInstanceFigure_name() {
			return myInstanceFigure_name;
		}

		/**
		 * @generated
		 */
		public WrappingLabel getFigureInstanceFigure_type() {
			return myInstanceFigure_type;
		}

		/**
		   * @generated
		   */
		public RectangleFigure getSlotCompartmentFigure() {
			return fSlotCompartmentFigure;
		}

		/**
		   * @generated
		   */
		public RectangleFigure getInstanceDescriptionCompartment() {
			return fInstanceDescriptionCompartment;
		}

		/**
		   * @generated
		   */
		public WrappingLabel getInstanceFigureDescriptionLabel() {
			return fInstanceFigureDescriptionLabel;
		}

		/**
		   * @generated
		   */
		public RectangleFigure getInstanceDerivationsCompartmentFigure() {
			return fInstanceDerivationsCompartmentFigure;
		}

		/**
		   * @generated
		   */
		public WrappingLabel getInstanceFigureDerivationsLabel() {
			return fInstanceFigureDerivationsLabel;
		}

		/**
		   * @generated
		   */
		public WrappingLabel getDoActionInstanceLabel() {
			return fDoActionInstanceLabel;
		}

	}

	/**
	* @generated
	*/
	static final Color THIS_FORE = new Color(null, 45, 155, 214);

	/**
	* @generated
	*/
	static final Font MYINSTANCEFIGURE_TYPE_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 8, SWT.ITALIC);

	/**
	* @generated
	*/
	static final Font FDOACTIONINSTANCELABEL_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 9, SWT.NORMAL);

	/**
	 * @generated
	 */
	static final Font FINSTANCEFIGUREDESCRIPTIONLABEL_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.NORMAL);

	/**
	 * @generated
	 */
	static final Font FINSTANCEFIGUREDERIVATIONSLABEL_FONT = new Font(Display.getCurrent(), Display.getDefault().getSystemFont().getFontData()[0].getName(), 7, SWT.NORMAL);

}
