package com.montages.mcore.diagram.edit.policies;

import org.eclipse.gef.commands.Command;
import org.eclipse.gmf.runtime.emf.type.core.commands.DestroyReferenceCommand;
import org.eclipse.gmf.runtime.emf.type.core.requests.DestroyReferenceRequest;

import com.montages.mcore.diagram.providers.McoreElementTypes;

/**
 * @generated
 */
public class MClassifierSuperTypeItemSemanticEditPolicy extends McoreBaseItemSemanticEditPolicy {

	/**
	* @generated
	*/
	public MClassifierSuperTypeItemSemanticEditPolicy() {
		super(McoreElementTypes.MClassifierSuperType_4002);
	}

	/**
	* @generated
	*/
	protected Command getDestroyReferenceCommand(DestroyReferenceRequest req) {
		return getGEFWrapper(new DestroyReferenceCommand(req));
	}

}
