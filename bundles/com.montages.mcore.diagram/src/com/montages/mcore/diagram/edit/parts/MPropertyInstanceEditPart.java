package com.montages.mcore.diagram.edit.parts;

import org.eclipse.draw2d.Connection;
import org.eclipse.draw2d.PolylineDecoration;
import org.eclipse.draw2d.RotatableDecoration;
import org.eclipse.draw2d.geometry.PointList;
import org.eclipse.gef.EditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ConnectionNodeEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editparts.ITreeBranchEditPart;
import org.eclipse.gmf.runtime.diagram.ui.editpolicies.EditPolicyRoles;
import org.eclipse.gmf.runtime.draw2d.ui.figures.PolylineConnectionEx;
import org.eclipse.gmf.runtime.notation.View;
import org.eclipse.swt.graphics.Color;

import com.montages.mcore.diagram.edit.policies.MPropertyInstanceItemSemanticEditPolicy;
import com.montages.mcore.diagram.edit.policies.ObjectPropertySourceDecorationEffect;
import com.montages.mcore.diagram.edit.policies.PropertyInstanceRefereshEditPolicy;

/**
 * @generated
 */
public class MPropertyInstanceEditPart extends ConnectionNodeEditPart implements ITreeBranchEditPart {

	/**
	* @generated
	*/
	public static final int VISUAL_ID = 4004;

	/**
	* @generated
	*/
	public MPropertyInstanceEditPart(View view) {
		super(view);
	}

	/**
	* @generated
	*/
	protected void createDefaultEditPolicies() {
		super.createDefaultEditPolicies();
		installEditPolicy(EditPolicyRoles.SEMANTIC_ROLE, new MPropertyInstanceItemSemanticEditPolicy());
		installEditPolicy(ObjectPropertySourceDecorationEffect.KEY, new ObjectPropertySourceDecorationEffect());
		installEditPolicy(PropertyInstanceRefereshEditPolicy.ROLE, new PropertyInstanceRefereshEditPolicy());
	}

	/**
	* @generated
	*/
	protected boolean addFixedChild(EditPart childEditPart) {
		return false;
	}

	/**
	* @generated
	*/
	protected void addChildVisual(EditPart childEditPart, int index) {
		if (addFixedChild(childEditPart)) {
			return;
		}
		super.addChildVisual(childEditPart, index);
	}

	/**
	* @generated
	*/
	protected boolean removeFixedChild(EditPart childEditPart) {
		if (childEditPart instanceof MPropertyInstancePropertyEditPart) {
			return true;
		}
		return false;
	}

	/**
	* @generated
	*/
	protected void removeChildVisual(EditPart childEditPart) {
		if (removeFixedChild(childEditPart)) {
			return;
		}
		super.removeChildVisual(childEditPart);
	}

	/**
	* Creates figure for this edit part.
	* 
	* Body of this method does not depend on settings in generation model
	* so you may safely remove <i>generated</i> tag and modify it.
	* 
	* @generated
	*/

	protected Connection createConnectionFigure() {
		return new PropertyInstanceLinkFigure();
	}

	/**
	* @generated
	*/
	public PropertyInstanceLinkFigure getPrimaryShape() {
		return (PropertyInstanceLinkFigure) getFigure();
	}

	/**
	 * @generated
	 */
	public class PropertyInstanceLinkFigure extends PolylineConnectionEx {

		/**
		 * @generated
		 */
		public PropertyInstanceLinkFigure() {
			this.setForegroundColor(THIS_FORE);

			setTargetDecoration(createTargetDecoration());
		}

		/**
		* @generated
		*/
		private RotatableDecoration createTargetDecoration() {
			PolylineDecoration df = new PolylineDecoration();
			PointList pl = new PointList();
			pl.addPoint(-1, 1);
			pl.addPoint(0, 0);
			pl.addPoint(-1, -1);
			df.setTemplate(pl);
			df.setScale(7, 3);
			return df;
		}

	}

	/**
	* @generated
	*/
	static final Color THIS_FORE = new Color(null, 45, 155, 214);

}
