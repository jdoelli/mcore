package com.montages.mcore.diagram.util;

import java.util.Date;

public class DurationHelper {
	
	public static long getCurrentTime() {
		return new Date().getTime();
	}
	
	private static long getLastTimePointDiff(long lastTimePoint) {
		return getCurrentTime() - lastTimePoint;
	}
	
	public static void printDuration(String comment, long lastTimePoint, boolean minutes) {
		long resMs = getLastTimePointDiff(lastTimePoint);
		long seconds = resMs / 1000;
		boolean isSeconds = minutes && (seconds < 60);
		long duration = minutes ? (isSeconds ? seconds : seconds / 60) : resMs;
		System.out.println(comment + " " + duration + (minutes ? (isSeconds ? " seconds" : " minutes") : ""));
	}
}
