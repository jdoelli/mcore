package com.montages.mcore.diagram.providers.assistants;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.gmf.runtime.diagram.ui.editparts.IGraphicalEditPart;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;

import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotEditPart;
import com.montages.mcore.diagram.providers.McoreElementTypes;
import com.montages.mcore.diagram.providers.McoreModelingAssistantProvider;

/**
	 * @generated
	 */
public class McoreModelingAssistantProviderOfMObjectWithSlotEditPart extends McoreModelingAssistantProvider {

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForPopupBar(IAdaptable host) {
		List<IElementType> types = new ArrayList<IElementType>(1);
		types.add(McoreElementTypes.MPropertyInstance_3003);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSource(IAdaptable source) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSource((MObjectWithSlotEditPart) sourceEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSource(MObjectWithSlotEditPart source) {
		List<IElementType> types = new ArrayList<IElementType>(3);
		types.add(McoreElementTypes.MObjectType_4001);
		types.add(McoreElementTypes.MPropertyInstance_4004);
		types.add(McoreElementTypes.MPropertyInstance_4005);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnSourceAndTarget(IAdaptable source, IAdaptable target) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnSourceAndTarget((MObjectWithSlotEditPart) sourceEditPart, targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnSourceAndTarget(MObjectWithSlotEditPart source, IGraphicalEditPart targetEditPart) {
		List<IElementType> types = new LinkedList<IElementType>();
		if (targetEditPart instanceof MClassifierEditPart) {
			types.add(McoreElementTypes.MObjectType_4001);
		}
		if (targetEditPart instanceof MObjectEditPart) {
			types.add(McoreElementTypes.MPropertyInstance_4004);
		}
		if (targetEditPart instanceof MObjectWithSlotEditPart) {
			types.add(McoreElementTypes.MPropertyInstance_4004);
		}
		if (targetEditPart instanceof MObjectEditPart) {
			types.add(McoreElementTypes.MPropertyInstance_4005);
		}
		if (targetEditPart instanceof MObjectWithSlotEditPart) {
			types.add(McoreElementTypes.MPropertyInstance_4005);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForTarget(IAdaptable source, IElementType relationshipType) {
		IGraphicalEditPart sourceEditPart = (IGraphicalEditPart) source.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForTarget((MObjectWithSlotEditPart) sourceEditPart, relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForTarget(MObjectWithSlotEditPart source, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == McoreElementTypes.MObjectType_4001) {
			types.add(McoreElementTypes.MClassifier_2001);
		} else if (relationshipType == McoreElementTypes.MPropertyInstance_4004) {
			types.add(McoreElementTypes.MObject_2002);
			types.add(McoreElementTypes.MObject_2003);
		} else if (relationshipType == McoreElementTypes.MPropertyInstance_4005) {
			types.add(McoreElementTypes.MObject_2002);
			types.add(McoreElementTypes.MObject_2003);
		}
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getRelTypesOnTarget(IAdaptable target) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetRelTypesOnTarget((MObjectWithSlotEditPart) targetEditPart);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetRelTypesOnTarget(MObjectWithSlotEditPart target) {
		List<IElementType> types = new ArrayList<IElementType>(2);
		types.add(McoreElementTypes.MPropertyInstance_4004);
		types.add(McoreElementTypes.MPropertyInstance_4005);
		return types;
	}

	/**
	 * @generated
	 */
	@Override
	public List<IElementType> getTypesForSource(IAdaptable target, IElementType relationshipType) {
		IGraphicalEditPart targetEditPart = (IGraphicalEditPart) target.getAdapter(IGraphicalEditPart.class);
		return doGetTypesForSource((MObjectWithSlotEditPart) targetEditPart, relationshipType);
	}

	/**
	 * @generated
	 */
	public List<IElementType> doGetTypesForSource(MObjectWithSlotEditPart target, IElementType relationshipType) {
		List<IElementType> types = new ArrayList<IElementType>();
		if (relationshipType == McoreElementTypes.MPropertyInstance_4004) {
			types.add(McoreElementTypes.MObject_2002);
			types.add(McoreElementTypes.MObject_2003);
		} else if (relationshipType == McoreElementTypes.MPropertyInstance_4005) {
			types.add(McoreElementTypes.MObject_2002);
			types.add(McoreElementTypes.MObject_2003);
		}
		return types;
	}

}
