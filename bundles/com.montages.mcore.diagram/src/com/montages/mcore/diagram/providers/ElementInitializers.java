package com.montages.mcore.diagram.providers;

import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;

/**
 * @generated
 */
public class ElementInitializers {

	protected ElementInitializers() {
		// use #getInstance to access cached instance
	}

	/**
	* @generated
	*/
	public static ElementInitializers getInstance() {
		ElementInitializers cached = McoreDiagramEditorPlugin.getInstance().getElementInitializers();
		if (cached == null) {
			McoreDiagramEditorPlugin.getInstance().setElementInitializers(cached = new ElementInitializers());
		}
		return cached;
	}
}
