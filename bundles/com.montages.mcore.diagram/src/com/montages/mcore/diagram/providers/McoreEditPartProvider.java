package com.montages.mcore.diagram.providers;

import org.eclipse.gmf.tooling.runtime.providers.DefaultEditPartProvider;

import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.McoreEditPartFactory;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;

/**
 * @generated
 */
public class McoreEditPartProvider extends DefaultEditPartProvider {

	/**
	* @generated
	*/
	public McoreEditPartProvider() {
		super(new McoreEditPartFactory(), McoreVisualIDRegistry.TYPED_INSTANCE, MPackageEditPart.MODEL_ID);
	}
}
