package com.montages.mcore.diagram.providers;

import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.gmf.runtime.emf.type.core.ElementTypeRegistry;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypeImages;
import org.eclipse.gmf.tooling.runtime.providers.DiagramElementTypes;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;

import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.MClassifierEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierLiteralEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierSuperTypeEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectType2EditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MPackageEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentsLinkEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceEditPart;
import com.montages.mcore.diagram.part.McoreDiagramEditorPlugin;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * @generated
 */
public class McoreElementTypes {

	/**
	 * @generated
	 */
	private McoreElementTypes() {
	}

	/**
	 * @generated
	 */
	private static Map<IElementType, ENamedElement> elements;

	/**
	 * @generated
	 */
	private static DiagramElementTypeImages elementTypeImages = new DiagramElementTypeImages(McoreDiagramEditorPlugin.getInstance().getItemProvidersAdapterFactory());

	/**
	 * @generated
	 */
	private static Set<IElementType> KNOWN_ELEMENT_TYPES;

	/**
	 * @generated
	 */
	public static final IElementType MPackage_1000 = getElementType("com.montages.mcore.diagram.MPackage_1000"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MClassifier_2001 = getElementType("com.montages.mcore.diagram.MClassifier_2001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MObject_2002 = getElementType("com.montages.mcore.diagram.MObject_2002"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static final IElementType MObject_2003 = getElementType("com.montages.mcore.diagram.MObjectWithSlot_2003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MPropertyInstance_3003 = getElementType("com.montages.mcore.diagram.MObjectSlot_3003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MProperty_3004 = getElementType("com.montages.mcore.diagram.MClassifierProperty_3004"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static final IElementType MLiteral_3005 = getElementType("com.montages.mcore.diagram.MClassifierLiteral_3005"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MObjectType_4001 = getElementType("com.montages.mcore.diagram.MObjectType_4001"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MClassifierSuperType_4002 = getElementType("com.montages.mcore.diagram.MClassifierSuperType_4002"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MProperty_4003 = getElementType("com.montages.mcore.diagram.MProperty_4003"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static final IElementType MPropertyInstance_4004 = getElementType("com.montages.mcore.diagram.MPropertyInstance_4004"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static final IElementType MPropertyInstance_4005 = getElementType("com.montages.mcore.diagram.MPropertyInstanceContainmentsLink_4005"); //$NON-NLS-1$

	/**
	* @generated
	*/
	public static final IElementType MProperty_4006 = getElementType("com.montages.mcore.diagram.MPropertyContainment_4006"); //$NON-NLS-1$

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(ENamedElement element) {
		return elementTypeImages.getImageDescriptor(element);
	}

	/**
	 * @generated
	 */
	public static Image getImage(ENamedElement element) {
		return elementTypeImages.getImage(element);
	}

	/**
	 * @generated
	 */
	public static ImageDescriptor getImageDescriptor(IAdaptable hint) {
		return getImageDescriptor(getElement(hint));
	}

	/**
	 * @generated
	 */
	public static Image getImage(IAdaptable hint) {
		return getImage(getElement(hint));
	}

	/**
	 * Returns 'type' of the ecore object associated with the hint.
	 * 
	 * @generated
	 */
	public static ENamedElement getElement(IAdaptable hint) {
		Object type = hint.getAdapter(IElementType.class);
		if (elements == null) {
			elements = new IdentityHashMap<IElementType, ENamedElement>();

			elements.put(MPackage_1000, McorePackage.eINSTANCE.getMPackage());

			elements.put(MClassifier_2001, McorePackage.eINSTANCE.getMClassifier());

			elements.put(MObject_2002, ObjectsPackage.eINSTANCE.getMObject());

			elements.put(MObject_2003, ObjectsPackage.eINSTANCE.getMObject());

			elements.put(MPropertyInstance_3003, ObjectsPackage.eINSTANCE.getMPropertyInstance());

			elements.put(MProperty_3004, McorePackage.eINSTANCE.getMProperty());

			elements.put(MLiteral_3005, McorePackage.eINSTANCE.getMLiteral());

			elements.put(MObjectType_4001, ObjectsPackage.eINSTANCE.getMObject_Type());

			elements.put(MClassifierSuperType_4002, McorePackage.eINSTANCE.getMClassifier_SuperType());

			elements.put(MProperty_4003, McorePackage.eINSTANCE.getMProperty());

			elements.put(MPropertyInstance_4004, ObjectsPackage.eINSTANCE.getMPropertyInstance());

			elements.put(MPropertyInstance_4005, ObjectsPackage.eINSTANCE.getMPropertyInstance());

			elements.put(MProperty_4006, McorePackage.eINSTANCE.getMProperty());
		}
		return (ENamedElement) elements.get(type);
	}

	/**
	 * @generated
	 */
	private static IElementType getElementType(String id) {
		return ElementTypeRegistry.getInstance().getType(id);
	}

	/**
	 * @generated
	 */
	public static boolean isKnownElementType(IElementType elementType) {
		if (KNOWN_ELEMENT_TYPES == null) {
			KNOWN_ELEMENT_TYPES = new HashSet<IElementType>();
			KNOWN_ELEMENT_TYPES.add(MPackage_1000);
			KNOWN_ELEMENT_TYPES.add(MClassifier_2001);
			KNOWN_ELEMENT_TYPES.add(MObject_2002);
			KNOWN_ELEMENT_TYPES.add(MObject_2003);
			KNOWN_ELEMENT_TYPES.add(MPropertyInstance_3003);
			KNOWN_ELEMENT_TYPES.add(MProperty_3004);
			KNOWN_ELEMENT_TYPES.add(MLiteral_3005);
			KNOWN_ELEMENT_TYPES.add(MObjectType_4001);
			KNOWN_ELEMENT_TYPES.add(MClassifierSuperType_4002);
			KNOWN_ELEMENT_TYPES.add(MProperty_4003);
			KNOWN_ELEMENT_TYPES.add(MPropertyInstance_4004);
			KNOWN_ELEMENT_TYPES.add(MPropertyInstance_4005);
			KNOWN_ELEMENT_TYPES.add(MProperty_4006);
		}
		return KNOWN_ELEMENT_TYPES.contains(elementType);
	}

	/**
	 * @generated
	 */
	public static IElementType getElementType(int visualID) {
		switch (visualID) {
		case MPackageEditPart.VISUAL_ID:
			return MPackage_1000;
		case MClassifierEditPart.VISUAL_ID:
			return MClassifier_2001;
		case MObjectEditPart.VISUAL_ID:
			return MObject_2002;
		case MObjectWithSlotEditPart.VISUAL_ID:
			return MObject_2003;
		case MObjectSlotEditPart.VISUAL_ID:
			return MPropertyInstance_3003;
		case MClassifierPropertyEditPart.VISUAL_ID:
			return MProperty_3004;
		case MClassifierLiteralEditPart.VISUAL_ID:
			return MLiteral_3005;
		case MObjectType2EditPart.VISUAL_ID:
			return MObjectType_4001;
		case MClassifierSuperTypeEditPart.VISUAL_ID:
			return MClassifierSuperType_4002;
		case MPropertyEditPart.VISUAL_ID:
			return MProperty_4003;
		case MPropertyInstanceEditPart.VISUAL_ID:
			return MPropertyInstance_4004;
		case MPropertyInstanceContainmentsLinkEditPart.VISUAL_ID:
			return MPropertyInstance_4005;
		case MPropertyContainmentEditPart.VISUAL_ID:
			return MProperty_4006;
		}
		return null;
	}

	/**
	 * @generated
	 */
	public static final DiagramElementTypes TYPED_INSTANCE = new DiagramElementTypes(elementTypeImages) {

		/**
		 * @generated
		 */
		@Override
		public boolean isKnownElementType(IElementType elementType) {
			return com.montages.mcore.diagram.providers.McoreElementTypes.isKnownElementType(elementType);
		}

		/**
		 * @generated
		 */
		@Override
		public IElementType getElementTypeForVisualId(int visualID) {
			return com.montages.mcore.diagram.providers.McoreElementTypes.getElementType(visualID);
		}

		/**
		 * @generated
		 */
		@Override
		public ENamedElement getDefiningNamedElement(IAdaptable elementTypeAdapter) {
			return com.montages.mcore.diagram.providers.McoreElementTypes.getElement(elementTypeAdapter);
		}
	};

}
