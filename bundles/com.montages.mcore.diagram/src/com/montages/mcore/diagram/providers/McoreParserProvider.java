package com.montages.mcore.diagram.providers;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.gmf.runtime.common.core.service.AbstractProvider;
import org.eclipse.gmf.runtime.common.core.service.IOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.GetParserOperation;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParser;
import org.eclipse.gmf.runtime.common.ui.services.parser.IParserProvider;
import org.eclipse.gmf.runtime.common.ui.services.parser.ParserService;
import org.eclipse.gmf.runtime.emf.type.core.IElementType;
import org.eclipse.gmf.runtime.emf.ui.services.parser.ParserHintAdapter;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.McorePackage;
import com.montages.mcore.diagram.edit.parts.MClassifierDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierENameEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierLiteralEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierNameEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierPropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MClassifierShortNameEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectIdEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectTypeEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectTypeWithSlotEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotDescriptionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotDoActionLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MObjectWithSlotIdEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentMultiplicityAsStringSinEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyContainmentNameEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstanceContainmentLinkLabelEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyInstancePropertyEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyMultiplicityAsStringSinEditPart;
import com.montages.mcore.diagram.edit.parts.MPropertyNameEditPart;
import com.montages.mcore.diagram.parsers.DoActionLabelParser;
import com.montages.mcore.diagram.parsers.MCorePropertyLabelParser;
import com.montages.mcore.diagram.parsers.MessageFormatParser;
import com.montages.mcore.diagram.parsers.MultiValuesMessageFormatParser;
import com.montages.mcore.diagram.parsers.PropertyNameMessageFormatParser;
import com.montages.mcore.diagram.parsers.SlotParser;
import com.montages.mcore.diagram.parsers.XUpdateFeatureMessageFormatParser;
import com.montages.mcore.diagram.part.McoreVisualIDRegistry;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * @generated
 */
public class McoreParserProvider extends AbstractProvider implements IParserProvider {

	/**
	* @generated
	*/
	private MultiValuesMessageFormatParser mClassifierName_5005Parser;

	/**
	 * @generated
	 */
	private IParser getMClassifierName_5005Parser() {
		if (mClassifierName_5005Parser == null) {
			mClassifierName_5005Parser = new MultiValuesMessageFormatParser();
		}
		return mClassifierName_5005Parser;
	}

	/**
	 * @generated
	 */
	private IParser mClassifierName_5001Parser;

	/**
	 * @generated
	 */
	private IParser getMClassifierName_5001Parser() {
		if (mClassifierName_5001Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mClassifierName_5001Parser = parser;
		}
		return mClassifierName_5001Parser;
	}

	/**
	 * @generated
	 */
	private IParser mClassifierShortName_5002Parser;

	/**
	 * @generated
	 */
	private IParser getMClassifierShortName_5002Parser() {
		if (mClassifierShortName_5002Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_ShortName() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mClassifierShortName_5002Parser = parser;
		}
		return mClassifierShortName_5002Parser;
	}

	/**
	 * @generated
	 */
	private IParser mClassifierDescription_5006Parser;

	/**
	 * @generated
	 */
	private IParser getMClassifierDescription_5006Parser() {
		if (mClassifierDescription_5006Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMRepositoryElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mClassifierDescription_5006Parser = parser;
		}
		return mClassifierDescription_5006Parser;
	}

	/**
	* @generated
	*/
	private DoActionLabelParser mClassifierLabel_5009Parser;

	/**
	 * @generated
	 */
	private IParser getMClassifierLabel_5009Parser() {
		if (mClassifierLabel_5009Parser == null) {
			mClassifierLabel_5009Parser = new DoActionLabelParser();
		}
		return mClassifierLabel_5009Parser;
	}

	/**
	 * @generated
	 */
	private IParser mObjectId_5003Parser;

	/**
	 * @generated
	 */
	private IParser getMObjectId_5003Parser() {
		if (mObjectId_5003Parser == null) {
			EAttribute[] features = new EAttribute[] { ObjectsPackage.eINSTANCE.getMObject_Id() };
			EAttribute[] editableFeatures = new EAttribute[] { ObjectsPackage.eINSTANCE.getMObject_Id() };
			MessageFormatParser parser = new MessageFormatParser(features, editableFeatures);
			mObjectId_5003Parser = parser;
		}
		return mObjectId_5003Parser;
	}

	/**
	* @generated
	*/
	private XUpdateFeatureMessageFormatParser mObjectClassNameOrNatureDefinition_5004Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMObjectClassNameOrNatureDefinition_5004Parser() {
		if (mObjectClassNameOrNatureDefinition_5004Parser == null) {
			EAttribute[] features = new EAttribute[] { ObjectsPackage.eINSTANCE.getMObject_ClassNameOrNatureDefinition() };
			EAttribute[] editableFeatures = new EAttribute[] { ObjectsPackage.eINSTANCE.getMObject_ClassNameOrNatureDefinition() };
			mObjectClassNameOrNatureDefinition_5004Parser = new XUpdateFeatureMessageFormatParser(features, editableFeatures);
		}
		return mObjectClassNameOrNatureDefinition_5004Parser;
	}

	/**
	 * @generated
	 */
	private IParser mObjectDescription_5007Parser;

	/**
	 * @generated
	 */
	private IParser getMObjectDescription_5007Parser() {
		if (mObjectDescription_5007Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMRepositoryElement_Description() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mObjectDescription_5007Parser = parser;
		}
		return mObjectDescription_5007Parser;
	}

	/**
	* @generated
	*/
	private DoActionLabelParser mObjectLabel_5010Parser;

	/**
	 * @generated
	 */
	private IParser getMObjectLabel_5010Parser() {
		if (mObjectLabel_5010Parser == null) {
			mObjectLabel_5010Parser = new DoActionLabelParser();
		}
		return mObjectLabel_5010Parser;
	}

	/**
	* @generated
	*/
	@SuppressWarnings("unused")
	private IParser mObjectId_5020Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMObjectId_5020Parser() {
		return getMObjectId_5003Parser();
	}

	/**
	 * @generated NOT
	 */
	@SuppressWarnings("unused")
	private XUpdateFeatureMessageFormatParser mObjectClassNameOrNatureDefinition_5021Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMObjectClassNameOrNatureDefinition_5021Parser() {
		return getMObjectClassNameOrNatureDefinition_5004Parser();
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unused")
	private IParser mObjectDescription_5022Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMObjectDescription_5022Parser() {
		return getMObjectDescription_5007Parser();
	}

	/**
	 * @generated
	 */
	@SuppressWarnings("unused")
	private DoActionLabelParser mObjectLabel_5023Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMObjectLabel_5023Parser() {
		return getMObjectLabel_5010Parser();
	}

	/**
	 * @generated
	 */
	private SlotParser mPropertyInstance_3003Parser;

	/**
	 * @generated
	 */
	private IParser getMPropertyInstance_3003Parser() {
		if (mPropertyInstance_3003Parser == null) {
			mPropertyInstance_3003Parser = new SlotParser();
		}
		return mPropertyInstance_3003Parser;
	}

	/**
	 * @generated
	 */
	private MCorePropertyLabelParser mProperty_3004Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMProperty_3004Parser() {
		if (mProperty_3004Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_Name() };
			mProperty_3004Parser = new MCorePropertyLabelParser(features);
		}
		return mProperty_3004Parser;
	}

	/**
	* @generated
	*/
	private IParser mLiteral_3005Parser;

	/**
	 * @generated
	 */
	private IParser getMLiteral_3005Parser() {
		if (mLiteral_3005Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_Name() };
			MessageFormatParser parser = new MessageFormatParser(features);
			mLiteral_3005Parser = parser;
		}
		return mLiteral_3005Parser;
	}

	/**
	 * @generated
	 */
	private PropertyNameMessageFormatParser mPropertyENameNameShortName_6002Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMPropertyENameNameShortName_6002Parser() {
		if (mPropertyENameNameShortName_6002Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_EName(), McorePackage.eINSTANCE.getMNamed_Name(), McorePackage.eINSTANCE.getMNamed_ShortName() };
			EAttribute[] editableFeatures = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_Name() };
			mPropertyENameNameShortName_6002Parser = new PropertyNameMessageFormatParser(features, editableFeatures);
			mPropertyENameNameShortName_6002Parser.setViewPattern("{0}"); //$NON-NLS-1$
			mPropertyENameNameShortName_6002Parser.setEditorPattern("{0}"); //$NON-NLS-1$
			mPropertyENameNameShortName_6002Parser.setEditPattern("{0}"); //$NON-NLS-1$
		}
		return mPropertyENameNameShortName_6002Parser;
	}

	/**
	 * @generated
	 */
	private IParser mPropertyMultiplicityAsStringSingular_6001Parser;

	/**
	 * @generated
	 */
	private IParser getMPropertyMultiplicityAsStringSingular_6001Parser() {
		if (mPropertyMultiplicityAsStringSingular_6001Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMTyped_MultiplicityAsString(), McorePackage.eINSTANCE.getMExplicitlyTyped_Singular() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mPropertyMultiplicityAsStringSingular_6001Parser = parser;
		}
		return mPropertyMultiplicityAsStringSingular_6001Parser;
	}

	/**
	* @generated
	*/
	private XUpdateFeatureMessageFormatParser mPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser() {
		if (mPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser == null) {
			EAttribute[] features = new EAttribute[] { ObjectsPackage.eINSTANCE.getMPropertyInstance_PropertyNameOrLocalKeyDefinition() };
			EAttribute[] editableFeatures = new EAttribute[] { ObjectsPackage.eINSTANCE.getMPropertyInstance_PropertyNameOrLocalKeyDefinition() };
			mPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser = new XUpdateFeatureMessageFormatParser(features, editableFeatures);
		}
		return mPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser;
	}

	/**
	 * @generated
	 */
	private XUpdateFeatureMessageFormatParser mPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser() {
		if (mPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser == null) {
			EAttribute[] features = new EAttribute[] { ObjectsPackage.eINSTANCE.getMPropertyInstance_PropertyNameOrLocalKeyDefinition() };
			EAttribute[] editableFeatures = new EAttribute[] { ObjectsPackage.eINSTANCE.getMPropertyInstance_PropertyNameOrLocalKeyDefinition() };
			mPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser = new XUpdateFeatureMessageFormatParser(features, editableFeatures);
		}
		return mPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser;
	}

	/**
	* @generated
	*/
	private PropertyNameMessageFormatParser mPropertyENameNameShortName_6005Parser;

	/**
	 * @generated NOT
	 */
	private IParser getMPropertyENameNameShortName_6005Parser() {
		if (mPropertyENameNameShortName_6005Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_EName(), McorePackage.eINSTANCE.getMNamed_Name(), McorePackage.eINSTANCE.getMNamed_ShortName() };
			EAttribute[] editableFeatures = new EAttribute[] { McorePackage.eINSTANCE.getMNamed_Name() };
			mPropertyENameNameShortName_6005Parser = new PropertyNameMessageFormatParser(features, editableFeatures);
			mPropertyENameNameShortName_6005Parser.setViewPattern("{0}"); //$NON-NLS-1$
			mPropertyENameNameShortName_6005Parser.setEditorPattern("{0}"); //$NON-NLS-1$
			mPropertyENameNameShortName_6005Parser.setEditPattern("{0}"); //$NON-NLS-1$
		}
		return mPropertyENameNameShortName_6005Parser;
	}

	/**
	 * @generated
	 */
	private IParser mPropertyMultiplicityAsStringSingular_6006Parser;

	/**
	 * @generated
	 */
	private IParser getMPropertyMultiplicityAsStringSingular_6006Parser() {
		if (mPropertyMultiplicityAsStringSingular_6006Parser == null) {
			EAttribute[] features = new EAttribute[] { McorePackage.eINSTANCE.getMTyped_MultiplicityAsString(), McorePackage.eINSTANCE.getMExplicitlyTyped_Singular() };
			MessageFormatParser parser = new MessageFormatParser(features);
			parser.setViewPattern("{0}"); //$NON-NLS-1$
			parser.setEditorPattern("{0}"); //$NON-NLS-1$
			parser.setEditPattern("{0}"); //$NON-NLS-1$
			mPropertyMultiplicityAsStringSingular_6006Parser = parser;
		}
		return mPropertyMultiplicityAsStringSingular_6006Parser;
	}

	/**
	 * @generated
	 */
	protected IParser getParser(int visualID) {
		switch (visualID) {
		case MClassifierENameEditPart.VISUAL_ID:
			return getMClassifierName_5005Parser();
		case MClassifierNameEditPart.VISUAL_ID:
			return getMClassifierName_5001Parser();
		case MClassifierShortNameEditPart.VISUAL_ID:
			return getMClassifierShortName_5002Parser();
		case MClassifierDescriptionLabelEditPart.VISUAL_ID:
			return getMClassifierDescription_5006Parser();
		case MClassifierDoActionLabelEditPart.VISUAL_ID:
			return getMClassifierLabel_5009Parser();
		case MObjectIdEditPart.VISUAL_ID:
			return getMObjectId_5003Parser();
		case MObjectTypeEditPart.VISUAL_ID:
			return getMObjectClassNameOrNatureDefinition_5004Parser();
		case MObjectDescriptionLabelEditPart.VISUAL_ID:
			return getMObjectDescription_5007Parser();
		case MObjectDoActionLabelEditPart.VISUAL_ID:
			return getMObjectLabel_5010Parser();
		case MObjectWithSlotIdEditPart.VISUAL_ID:
			return getMObjectId_5020Parser();
		case MObjectTypeWithSlotEditPart.VISUAL_ID:
			return getMObjectClassNameOrNatureDefinition_5021Parser();
		case MObjectWithSlotDescriptionLabelEditPart.VISUAL_ID:
			return getMObjectDescription_5022Parser();
		case MObjectWithSlotDoActionLabelEditPart.VISUAL_ID:
			return getMObjectLabel_5023Parser();
		case MObjectSlotEditPart.VISUAL_ID:
			return getMPropertyInstance_3003Parser();
		case MClassifierPropertyEditPart.VISUAL_ID:
			return getMProperty_3004Parser();
		case MClassifierLiteralEditPart.VISUAL_ID:
			return getMLiteral_3005Parser();
		case MPropertyNameEditPart.VISUAL_ID:
			return getMPropertyENameNameShortName_6002Parser();
		case MPropertyMultiplicityAsStringSinEditPart.VISUAL_ID:
			return getMPropertyMultiplicityAsStringSingular_6001Parser();
		case MPropertyInstancePropertyEditPart.VISUAL_ID:
			return getMPropertyInstancePropertyNameOrLocalKeyDefinition_6003Parser();
		case MPropertyInstanceContainmentLinkLabelEditPart.VISUAL_ID:
			return getMPropertyInstancePropertyNameOrLocalKeyDefinition_6004Parser();
		case MPropertyContainmentNameEditPart.VISUAL_ID:
			return getMPropertyENameNameShortName_6005Parser();
		case MPropertyContainmentMultiplicityAsStringSinEditPart.VISUAL_ID:
			return getMPropertyMultiplicityAsStringSingular_6006Parser();
		}
		return null;
	}

	/**
	 * Utility method that consults ParserService
	 * 
	 * @generated
	 */
	public static IParser getParser(IElementType type, EObject object, String parserHint) {
		return ParserService.getInstance().getParser(new HintAdapter(type, object, parserHint));
	}

	/**
	 * @generated
	 */
	public IParser getParser(IAdaptable hint) {
		String vid = (String) hint.getAdapter(String.class);
		if (vid != null) {
			return getParser(McoreVisualIDRegistry.getVisualID(vid));
		}
		View view = (View) hint.getAdapter(View.class);
		if (view != null) {
			return getParser(McoreVisualIDRegistry.getVisualID(view));
		}
		return null;
	}

	/**
	 * @generated
	 */
	public boolean provides(IOperation operation) {
		if (operation instanceof GetParserOperation) {
			IAdaptable hint = ((GetParserOperation) operation).getHint();
			if (McoreElementTypes.getElement(hint) == null) {
				return false;
			}
			return getParser(hint) != null;
		}
		return false;
	}

	/**
	 * @generated
	 */
	private static class HintAdapter extends ParserHintAdapter {

		/**
		 * @generated
		 */
		private final IElementType elementType;

		/**
		 * @generated
		 */
		public HintAdapter(IElementType type, EObject object, String parserHint) {
			super(object, parserHint);
			assert type != null;
			elementType = type;
		}

		/**
		 * @generated
		 */
		public Object getAdapter(Class adapter) {
			if (IElementType.class.equals(adapter)) {
				return elementType;
			}
			return super.getAdapter(adapter);
		}
	}

}
