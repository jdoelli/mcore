package com.montages.mcore.diagram.navigator;

import org.eclipse.jface.viewers.ViewerSorter;

import com.montages.mcore.diagram.part.McoreVisualIDRegistry;

/**
* @generated
*/
public class McoreNavigatorSorter extends ViewerSorter {

	/**
	* @generated
	*/
	private static final int GROUP_CATEGORY = 7022;

	/**
	* @generated
	*/
	private static final int SHORTCUTS_CATEGORY = 7021;

	/**
	* @generated
	*/
	public int category(Object element) {
		if (element instanceof McoreNavigatorItem) {
			McoreNavigatorItem item = (McoreNavigatorItem) element;
			if (item.getView().getEAnnotation("Shortcut") != null) { //$NON-NLS-1$
				return SHORTCUTS_CATEGORY;
			}
			return McoreVisualIDRegistry.getVisualID(item.getView());
		}
		return GROUP_CATEGORY;
	}

}
