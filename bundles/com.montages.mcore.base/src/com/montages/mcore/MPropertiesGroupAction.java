/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProperties Group Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMPropertiesGroupAction()
 * @model
 * @generated
 */
public enum MPropertiesGroupAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Constraint</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTRAINT_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRAINT(1, "Constraint", "+ Constraint"),
	/**
	 * The '<em><b>String Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRING_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	STRING_ATTRIBUTE(2, "StringAttribute", "+ String Attribute"),
	/**
	 * The '<em><b>Boolean Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN_ATTRIBUTE(3, "BooleanAttribute", "+ Boolean Attribute"),
	/**
	 * The '<em><b>Integer Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTEGER_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGER_ATTRIBUTE(4, "IntegerAttribute", "+ Integer Attribute"),
	/**
	 * The '<em><b>Real Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REAL_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	REAL_ATTRIBUTE(5, "RealAttribute", "+ Real Atrribute"),
	/**
	 * The '<em><b>Date Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_ATTRIBUTE(6, "DateAttribute", "+ Date Attribute"),
	/**
	 * The '<em><b>Unary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_REFERENCE(7, "UnaryReference", "+ [0..1] Reference"),

	/**
	 * The '<em><b>Nary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	NARY_REFERENCE(8, "NaryReference", "+ [0..*] Reference"),

	/**
	 * The '<em><b>Unary Containment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #UNARY_CONTAINMENT_VALUE
	 * @generated
	 * @ordered
	 */
	UNARY_CONTAINMENT(9, "UnaryContainment", "+ [0..1] Containment"),
	/**
	 * The '<em><b>Nary Containment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NARY_CONTAINMENT_VALUE
	 * @generated
	 * @ordered
	 */
	NARY_CONTAINMENT(10, "NaryContainment", "+ [0..*] Containment"),
	/**
	 * The '<em><b>Operation One Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_ONE_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION_ONE_PARAMETER(11, "OperationOneParameter", "+ Op, 1 Parameter"),

	/**
	 * The '<em><b>Operation Two Parameters</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERATION_TWO_PARAMETERS_VALUE
	 * @generated
	 * @ordered
	 */
	OPERATION_TWO_PARAMETERS(12, "OperationTwoParameters", "+ Op, 2 Parameters"),

	/**
	 * The '<em><b>Group Of Properties</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GROUP_OF_PROPERTIES_VALUE
	 * @generated
	 * @ordered
	 */
	GROUP_OF_PROPERTIES(13, "GroupOfProperties", "+ Group of Properties");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Constraint</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constraint</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRAINT
	 * @model name="Constraint" literal="+ Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRAINT_VALUE = 1;

	/**
	 * The '<em><b>String Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRING_ATTRIBUTE
	 * @model name="StringAttribute" literal="+ String Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_ATTRIBUTE_VALUE = 2;

	/**
	 * The '<em><b>Boolean Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_ATTRIBUTE
	 * @model name="BooleanAttribute" literal="+ Boolean Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_ATTRIBUTE_VALUE = 3;

	/**
	 * The '<em><b>Integer Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Integer Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTEGER_ATTRIBUTE
	 * @model name="IntegerAttribute" literal="+ Integer Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGER_ATTRIBUTE_VALUE = 4;

	/**
	 * The '<em><b>Real Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Real Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REAL_ATTRIBUTE
	 * @model name="RealAttribute" literal="+ Real Atrribute"
	 * @generated
	 * @ordered
	 */
	public static final int REAL_ATTRIBUTE_VALUE = 5;

	/**
	 * The '<em><b>Date Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_ATTRIBUTE
	 * @model name="DateAttribute" literal="+ Date Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_ATTRIBUTE_VALUE = 6;

	/**
	 * The '<em><b>Unary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_REFERENCE
	 * @model name="UnaryReference" literal="+ [0..1] Reference"
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_REFERENCE_VALUE = 7;

	/**
	 * The '<em><b>Nary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NARY_REFERENCE
	 * @model name="NaryReference" literal="+ [0..*] Reference"
	 * @generated
	 * @ordered
	 */
	public static final int NARY_REFERENCE_VALUE = 8;

	/**
	 * The '<em><b>Unary Containment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Unary Containment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #UNARY_CONTAINMENT
	 * @model name="UnaryContainment" literal="+ [0..1] Containment"
	 * @generated
	 * @ordered
	 */
	public static final int UNARY_CONTAINMENT_VALUE = 9;

	/**
	 * The '<em><b>Nary Containment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Nary Containment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NARY_CONTAINMENT
	 * @model name="NaryContainment" literal="+ [0..*] Containment"
	 * @generated
	 * @ordered
	 */
	public static final int NARY_CONTAINMENT_VALUE = 10;

	/**
	 * The '<em><b>Operation One Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operation One Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERATION_ONE_PARAMETER
	 * @model name="OperationOneParameter" literal="+ Op, 1 Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_ONE_PARAMETER_VALUE = 11;

	/**
	 * The '<em><b>Operation Two Parameters</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operation Two Parameters</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERATION_TWO_PARAMETERS
	 * @model name="OperationTwoParameters" literal="+ Op, 2 Parameters"
	 * @generated
	 * @ordered
	 */
	public static final int OPERATION_TWO_PARAMETERS_VALUE = 12;

	/**
	 * The '<em><b>Group Of Properties</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Group Of Properties</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GROUP_OF_PROPERTIES
	 * @model name="GroupOfProperties" literal="+ Group of Properties"
	 * @generated
	 * @ordered
	 */
	public static final int GROUP_OF_PROPERTIES_VALUE = 13;

	/**
	 * An array of all the '<em><b>MProperties Group Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPropertiesGroupAction[] VALUES_ARRAY = new MPropertiesGroupAction[] {
			DO, CONSTRAINT, STRING_ATTRIBUTE, BOOLEAN_ATTRIBUTE,
			INTEGER_ATTRIBUTE, REAL_ATTRIBUTE, DATE_ATTRIBUTE, UNARY_REFERENCE,
			NARY_REFERENCE, UNARY_CONTAINMENT, NARY_CONTAINMENT,
			OPERATION_ONE_PARAMETER, OPERATION_TWO_PARAMETERS,
			GROUP_OF_PROPERTIES, };

	/**
	 * A public read-only list of all the '<em><b>MProperties Group Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPropertiesGroupAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProperties Group Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesGroupAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertiesGroupAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperties Group Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesGroupAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertiesGroupAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperties Group Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesGroupAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case CONSTRAINT_VALUE:
			return CONSTRAINT;
		case STRING_ATTRIBUTE_VALUE:
			return STRING_ATTRIBUTE;
		case BOOLEAN_ATTRIBUTE_VALUE:
			return BOOLEAN_ATTRIBUTE;
		case INTEGER_ATTRIBUTE_VALUE:
			return INTEGER_ATTRIBUTE;
		case REAL_ATTRIBUTE_VALUE:
			return REAL_ATTRIBUTE;
		case DATE_ATTRIBUTE_VALUE:
			return DATE_ATTRIBUTE;
		case UNARY_REFERENCE_VALUE:
			return UNARY_REFERENCE;
		case NARY_REFERENCE_VALUE:
			return NARY_REFERENCE;
		case UNARY_CONTAINMENT_VALUE:
			return UNARY_CONTAINMENT;
		case NARY_CONTAINMENT_VALUE:
			return NARY_CONTAINMENT;
		case OPERATION_ONE_PARAMETER_VALUE:
			return OPERATION_ONE_PARAMETER;
		case OPERATION_TWO_PARAMETERS_VALUE:
			return OPERATION_TWO_PARAMETERS;
		case GROUP_OF_PROPERTIES_VALUE:
			return GROUP_OF_PROPERTIES;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPropertiesGroupAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPropertiesGroupAction
