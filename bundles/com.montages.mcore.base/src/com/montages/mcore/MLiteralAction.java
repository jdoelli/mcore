/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MLiteral Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMLiteralAction()
 * @model
 * @generated
 */
public enum MLiteralAction implements Enumerator {
	/**
	 * The '<em><b>Insert Above Literal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INSERT_ABOVE_LITERAL_VALUE
	 * @generated
	 * @ordered
	 */
	INSERT_ABOVE_LITERAL(0, "InsertAboveLiteral", "+ Above Literal"),

	/**
	 * The '<em><b>Insert Below Literal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INSERT_BELOW_LITERAL_VALUE
	 * @generated
	 * @ordered
	 */
	INSERT_BELOW_LITERAL(1, "InsertBelowLiteral", "+ Below Literal");

	/**
	 * The '<em><b>Insert Above Literal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Insert Above Literal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INSERT_ABOVE_LITERAL
	 * @model name="InsertAboveLiteral" literal="+ Above Literal"
	 * @generated
	 * @ordered
	 */
	public static final int INSERT_ABOVE_LITERAL_VALUE = 0;

	/**
	 * The '<em><b>Insert Below Literal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Insert Below Literal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INSERT_BELOW_LITERAL
	 * @model name="InsertBelowLiteral" literal="+ Below Literal"
	 * @generated
	 * @ordered
	 */
	public static final int INSERT_BELOW_LITERAL_VALUE = 1;

	/**
	 * An array of all the '<em><b>MLiteral Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MLiteralAction[] VALUES_ARRAY = new MLiteralAction[] {
			INSERT_ABOVE_LITERAL, INSERT_BELOW_LITERAL, };

	/**
	 * A public read-only list of all the '<em><b>MLiteral Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MLiteralAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MLiteral Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MLiteralAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MLiteralAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MLiteral Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MLiteralAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MLiteralAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MLiteral Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MLiteralAction get(int value) {
		switch (value) {
		case INSERT_ABOVE_LITERAL_VALUE:
			return INSERT_ABOVE_LITERAL;
		case INSERT_BELOW_LITERAL_VALUE:
			return INSERT_BELOW_LITERAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MLiteralAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MLiteralAction
