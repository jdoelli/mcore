/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Top Level Domain</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getTopLevelDomain()
 * @model
 * @generated
 */
public enum TopLevelDomain implements Enumerator {
	/**
	 * The '<em><b>Com</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COM_VALUE
	 * @generated
	 * @ordered
	 */
	COM(0, "Com", "Com"),

	/**
	 * The '<em><b>Org</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ORG_VALUE
	 * @generated
	 * @ordered
	 */
	ORG(1, "Org", "Org"),

	/**
	 * The '<em><b>Net</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NET_VALUE
	 * @generated
	 * @ordered
	 */
	NET(2, "Net", "Net"),

	/**
	 * The '<em><b>Ch</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CH_VALUE
	 * @generated
	 * @ordered
	 */
	CH(3, "Ch", "Ch"),

	/**
	 * The '<em><b>Fr</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FR_VALUE
	 * @generated
	 * @ordered
	 */
	FR(4, "Fr", "Fr"),

	/**
	 * The '<em><b>Ru</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RU_VALUE
	 * @generated
	 * @ordered
	 */
	RU(5, "Ru", "Ru");

	/**
	 * The '<em><b>Com</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Com</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COM
	 * @model name="Com"
	 * @generated
	 * @ordered
	 */
	public static final int COM_VALUE = 0;

	/**
	 * The '<em><b>Org</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Org</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ORG
	 * @model name="Org"
	 * @generated
	 * @ordered
	 */
	public static final int ORG_VALUE = 1;

	/**
	 * The '<em><b>Net</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Net</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NET
	 * @model name="Net"
	 * @generated
	 * @ordered
	 */
	public static final int NET_VALUE = 2;

	/**
	 * The '<em><b>Ch</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ch</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CH
	 * @model name="Ch"
	 * @generated
	 * @ordered
	 */
	public static final int CH_VALUE = 3;

	/**
	 * The '<em><b>Fr</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Fr</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FR
	 * @model name="Fr"
	 * @generated
	 * @ordered
	 */
	public static final int FR_VALUE = 4;

	/**
	 * The '<em><b>Ru</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Ru</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RU
	 * @model name="Ru"
	 * @generated
	 * @ordered
	 */
	public static final int RU_VALUE = 5;

	/**
	 * An array of all the '<em><b>Top Level Domain</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final TopLevelDomain[] VALUES_ARRAY = new TopLevelDomain[] {
			COM, ORG, NET, CH, FR, RU, };

	/**
	 * A public read-only list of all the '<em><b>Top Level Domain</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<TopLevelDomain> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Top Level Domain</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TopLevelDomain get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TopLevelDomain result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Top Level Domain</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TopLevelDomain getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			TopLevelDomain result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Top Level Domain</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static TopLevelDomain get(int value) {
		switch (value) {
		case COM_VALUE:
			return COM;
		case ORG_VALUE:
			return ORG;
		case NET_VALUE:
			return NET;
		case CH_VALUE:
			return CH;
		case FR_VALUE:
			return FR;
		case RU_VALUE:
			return RU;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private TopLevelDomain(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //TopLevelDomain
