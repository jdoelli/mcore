/**
 */
package com.montages.mcore;

import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTyped</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MTyped#getMultiplicityAsString <em>Multiplicity As String</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getCalculatedType <em>Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getCalculatedMandatory <em>Calculated Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getCalculatedSingular <em>Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getCalculatedTypePackage <em>Calculated Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getVoidTypeAllowed <em>Void Type Allowed</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getCalculatedSimpleType <em>Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.MTyped#getMultiplicityCase <em>Multiplicity Case</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMTyped()
 * @model abstract="true"
 * @generated
 */

public interface MTyped extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity As String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity As String</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMTyped_MultiplicityAsString()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='(if self.calculatedMandatory then \'[1..\' else \'[0..\' endif)\r\n.concat(if self.calculatedSingular then \'1]\' else \'*]\' endif)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getMultiplicityAsString();

	/**
	 * Returns the value of the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Type</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMTyped_CalculatedType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let res:MClassifier=null in res'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getCalculatedType();

	/**
	 * Returns the value of the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Mandatory</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMTyped_CalculatedMandatory()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCalculatedMandatory();

	/**
	 * Returns the value of the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Singular</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMTyped_CalculatedSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCalculatedSingular();

	/**
	 * Returns the value of the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Type Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Type Package</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMTyped_CalculatedTypePackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.calculatedType.oclIsUndefined() then null else self.calculatedType.containingPackage endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getCalculatedTypePackage();

	/**
	 * Returns the value of the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Void Type Allowed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Void Type Allowed</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMTyped_VoidTypeAllowed()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getVoidTypeAllowed();

	/**
	 * Returns the value of the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.McorePackage#getMTyped_CalculatedSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::SimpleType::None\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Information'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	SimpleType getCalculatedSimpleType();

	/**
	 * Returns the value of the '<em><b>Multiplicity Case</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MultiplicityCase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiplicity Case</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiplicity Case</em>' attribute.
	 * @see com.montages.mcore.MultiplicityCase
	 * @see #setMultiplicityCase(MultiplicityCase)
	 * @see com.montages.mcore.McorePackage#getMTyped_MultiplicityCase()
	 * @model required="true" transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.calculatedMandatory and self.calculatedSingular\r\n  then MultiplicityCase::OneOne\r\n  else if (not self.calculatedMandatory) and self.calculatedSingular\r\n    then MultiplicityCase::ZeroOne\r\n    else if self.calculatedMandatory and (not self.calculatedSingular)\r\n      then MultiplicityCase::OneMany\r\n      else MultiplicityCase::ZeroMany endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Behavior' createColumn='false'"
	 * @generated
	 */
	MultiplicityCase getMultiplicityCase();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MTyped#getMultiplicityCase <em>Multiplicity Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Multiplicity Case</em>' attribute.
	 * @see com.montages.mcore.MultiplicityCase
	 * @see #getMultiplicityCase()
	 * @generated
	 */
	void setMultiplicityCase(MultiplicityCase value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" trgRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate multiplicityCase$Update(MultiplicityCase trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if  mClassifier.oclIsUndefined() and (simpleType = SimpleType::None)  \r\n  then \'MISSING TYPE\' \r\n  else let t: String = \r\n      if  simpleType<>SimpleType::None\r\n        then \r\n          if simpleType=SimpleType::Double  then \'Real\' \r\n          else if simpleType=SimpleType::Object then \'ecore::EObject\'\r\n          else if simpleType=SimpleType::Annotation then \'ecore::EAnnotation\'\r\n          else if simpleType=SimpleType::Attribute then \'ecore::EAttribute\'\r\n          else if simpleType=SimpleType::Class then \'ecore::EClass\'\r\n          else if simpleType=SimpleType::Classifier then \'ecore::EClassifier\'\r\n          else if simpleType=SimpleType::DataType then \'ecore::EDataType\'\r\n          else if simpleType=SimpleType::Enumeration then \'ecore::Enumeration\'\r\n          else if simpleType=SimpleType::Feature then \'ecore::EStructuralFeature\'\r\n          else if simpleType=SimpleType::Literal then \'ecore::EEnumLiteral\'\r\n          else if simpleType=SimpleType::KeyValue then \'ecore::EStringToStringMapEntry\'\r\n          else if simpleType=SimpleType::NamedElement then \'ecore::NamedElement\'\r\n          else if simpleType=SimpleType::Operation then \'ecore::EOperation\'\r\n          else if simpleType=SimpleType::Package then \'ecore::EPackage\'\r\n          else if simpleType=SimpleType::Parameter then \'ecore::EParameter\'\r\n           else if simpleType=SimpleType::Reference then \'ecore::EReference\'\r\n           else if simpleType=SimpleType::TypedElement then \'ecore::ETypedElement\'\r\n           else if simpleType = SimpleType::Date then \'ecore::EDate\' \r\n            else simpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif\r\n        else \r\n          /* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified \r\n\t      let pkg: String = \r\n\t        if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat(\'::\')\r\n\t          else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then \'\' else mClassifier.containingPackage.eName.allLowerCase().concat(\'::\') endif \r\n\t        endif\r\n\t      in pkg.concat(mClassifier.eName) \r\n\t      NEW Create string with all package qualifiers \052/\r\n\t      let pkg:String=\r\n\t       if mClassifier.containingClassifier   =null then \'\'\r\n\t       else mClassifier.containingPackage.qualifiedName.concat(\'::\' ) endif                            in\r\n\t       pkg.concat(mClassifier.eName)\r\n      endif\r\n\tin \r\n\r\n    if  singular.oclIsUndefined() then \'MISSING ARITY INFO\' else \r\n      if singular=true then t else \'OrderedSet(\'.concat(t).concat(\') \') endif\r\n    endif\r\nendif\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Type Rendering' createColumn='true'"
	 * @generated
	 */
	String typeAsOcl(MPackage oclPackage, MClassifier mClassifier,
			SimpleType simpleType, Boolean singular);

} // MTyped
