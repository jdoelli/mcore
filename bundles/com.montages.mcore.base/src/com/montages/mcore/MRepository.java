/**
 */
package com.montages.mcore;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MRepository</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MRepository#getComponent <em>Component</em>}</li>
 *   <li>{@link com.montages.mcore.MRepository#getCore <em>Core</em>}</li>
 *   <li>{@link com.montages.mcore.MRepository#getReferencedEPackage <em>Referenced EPackage</em>}</li>
 *   <li>{@link com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMRepository()
 * @model annotation="http://www.xocl.org/OCL label='\'\' '"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Repository\'\n'"
 * @generated
 */

public interface MRepository extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Component</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' containment reference list.
	 * @see #isSetComponent()
	 * @see #unsetComponent()
	 * @see com.montages.mcore.McorePackage#getMRepository_Component()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 *        annotation="http://www.xocl.org/OCL initValue='OrderedSet{Tuple{name=\'Main Component\'}}'"
	 * @generated
	 */
	EList<MComponent> getComponent();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepository#getComponent <em>Component</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetComponent()
	 * @see #getComponent()
	 * @generated
	 */
	void unsetComponent();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepository#getComponent <em>Component</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Component</em>' containment reference list is set.
	 * @see #unsetComponent()
	 * @see #getComponent()
	 * @generated
	 */
	boolean isSetComponent();

	/**
	 * Returns the value of the '<em><b>Core</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Core</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Core</em>' containment reference.
	 * @see #isSetCore()
	 * @see #unsetCore()
	 * @see #setCore(MComponent)
	 * @see com.montages.mcore.McorePackage#getMRepository_Core()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MComponent getCore();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MRepository#getCore <em>Core</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Core</em>' containment reference.
	 * @see #isSetCore()
	 * @see #unsetCore()
	 * @see #getCore()
	 * @generated
	 */
	void setCore(MComponent value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepository#getCore <em>Core</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCore()
	 * @see #getCore()
	 * @see #setCore(MComponent)
	 * @generated
	 */
	void unsetCore();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepository#getCore <em>Core</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Core</em>' containment reference is set.
	 * @see #unsetCore()
	 * @see #getCore()
	 * @see #setCore(MComponent)
	 * @generated
	 */
	boolean isSetCore();

	/**
	 * Returns the value of the '<em><b>Referenced EPackage</b></em>' reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced EPackage</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced EPackage</em>' reference list.
	 * @see #isSetReferencedEPackage()
	 * @see #unsetReferencedEPackage()
	 * @see com.montages.mcore.McorePackage#getMRepository_ReferencedEPackage()
	 * @model unsettable="true"
	 * @generated
	 */
	EList<EPackage> getReferencedEPackage();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepository#getReferencedEPackage <em>Referenced EPackage</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReferencedEPackage()
	 * @see #getReferencedEPackage()
	 * @generated
	 */
	void unsetReferencedEPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepository#getReferencedEPackage <em>Referenced EPackage</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Referenced EPackage</em>' reference list is set.
	 * @see #unsetReferencedEPackage()
	 * @see #getReferencedEPackage()
	 * @generated
	 */
	boolean isSetReferencedEPackage();

	/**
	 * Returns the value of the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute.
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @see com.montages.mcore.McorePackage#getMRepository_AvoidRegenerationOfEditorConfiguration()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='false\n'"
	 * @generated
	 */
	Boolean getAvoidRegenerationOfEditorConfiguration();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute.
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @generated
	 */
	void setAvoidRegenerationOfEditorConfiguration(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @generated
	 */
	void unsetAvoidRegenerationOfEditorConfiguration();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute is set.
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @generated
	 */
	boolean isSetAvoidRegenerationOfEditorConfiguration();

} // MRepository
