/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McoreFactory
 * @model kind="package"
 *        annotation="http://www.montages.com/mCore/MCore mName='MCore'"
 *        annotation="http://www.xocl.org/OCL rootConstraint='trg.name = \'MComponent\''"
 *        annotation="http://www.xocl.org/UUID useUUIDs='true' uuidAttributeName='_uuid'"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages'"
 *        annotation="http://www.xocl.org/EDITORCONFIG hideAdvancedProperties='null'"
 * @generated
 */
public interface McorePackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mcore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mcore";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mCore/MCore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mcore";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	McorePackage eINSTANCE = com.montages.mcore.impl.McorePackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MRepositoryElementImpl <em>MRepository Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MRepositoryElementImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMRepositoryElement()
	 * @generated
	 */
	int MREPOSITORY_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT__KIND_LABEL = 0;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL = 1;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT__INDENT_LEVEL = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT__DESCRIPTION = 3;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT__AS_TEXT = 4;

	/**
	 * The number of structural features of the '<em>MRepository Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT___INDENTATION_SPACES = 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER = 1;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT___NEW_LINE_STRING = 2;

	/**
	 * The number of operations of the '<em>MRepository Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_ELEMENT_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MRepositoryImpl <em>MRepository</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MRepositoryImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMRepository()
	 * @generated
	 */
	int MREPOSITORY = 1;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__KIND_LABEL = MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__RENDERED_KIND_LABEL = MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__INDENT_LEVEL = MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__DESCRIPTION = MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__AS_TEXT = MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__COMPONENT = MREPOSITORY_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Core</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__CORE = MREPOSITORY_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Referenced EPackage</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__REFERENCED_EPACKAGE = MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION = MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 3;

	/**
	 * The number of structural features of the '<em>MRepository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_FEATURE_COUNT = MREPOSITORY_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY___INDENTATION_SPACES = MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY___INDENTATION_SPACES__INTEGER = MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY___NEW_LINE_STRING = MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The number of operations of the '<em>MRepository</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MREPOSITORY_OPERATION_COUNT = MREPOSITORY_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MNamedImpl <em>MNamed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MNamedImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMNamed()
	 * @generated
	 */
	int MNAMED = 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__KIND_LABEL = MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__RENDERED_KIND_LABEL = MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__INDENT_LEVEL = MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__DESCRIPTION = MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__AS_TEXT = MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__SPECIAL_ENAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__SHORT_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__ENAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__FULL_LABEL = MREPOSITORY_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__LOCAL_STRUCTURAL_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__CALCULATED_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__CALCULATED_SHORT_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__CORRECT_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED__CORRECT_SHORT_NAME = MREPOSITORY_ELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>MNamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_FEATURE_COUNT = MREPOSITORY_ELEMENT_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___INDENTATION_SPACES = MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___INDENTATION_SPACES__INTEGER = MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___NEW_LINE_STRING = MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___SAME_NAME__MNAMED = MREPOSITORY_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___SAME_SHORT_NAME__MNAMED = MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___SAME_STRING__STRING_STRING = MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED___STRING_EMPTY__STRING = MREPOSITORY_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>MNamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_OPERATION_COUNT = MREPOSITORY_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MComponentImpl <em>MComponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MComponentImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMComponent()
	 * @generated
	 */
	int MCOMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Owned Package</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__OWNED_PACKAGE = MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Resource Folder</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__RESOURCE_FOLDER = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Semantics</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__SEMANTICS = MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Generated EPackage</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__GENERATED_EPACKAGE = MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Named Editor</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__NAMED_EDITOR = MNAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__NAME_PREFIX = MNAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Derived Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DERIVED_NAME_PREFIX = MNAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Name Prefix For Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__NAME_PREFIX_FOR_FEATURES = MNAMED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Containing Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__CONTAINING_REPOSITORY = MNAMED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Represents Core Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__REPRESENTS_CORE_COMPONENT = MNAMED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Root Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__ROOT_PACKAGE = MNAMED_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Domain Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DOMAIN_TYPE = MNAMED_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Domain Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DOMAIN_NAME = MNAMED_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Derived Domain Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DERIVED_DOMAIN_TYPE = MNAMED_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Derived Domain Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DERIVED_DOMAIN_NAME = MNAMED_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Derived Bundle Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DERIVED_BUNDLE_NAME = MNAMED_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Derived URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DERIVED_URI = MNAMED_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Special Bundle Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__SPECIAL_BUNDLE_NAME = MNAMED_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Preloaded Component</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__PRELOADED_COMPONENT = MNAMED_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Main Named Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__MAIN_NAMED_EDITOR = MNAMED_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Main Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__MAIN_EDITOR = MNAMED_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Hide Advanced Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__HIDE_ADVANCED_PROPERTIES = MNAMED_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>All General Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__ALL_GENERAL_ANNOTATIONS = MNAMED_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Abstract Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__ABSTRACT_COMPONENT = MNAMED_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Count Implementations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__COUNT_IMPLEMENTATIONS = MNAMED_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Disable Default Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT = MNAMED_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__DO_ACTION = MNAMED_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION = MNAMED_FEATURE_COUNT
			+ 27;

	/**
	 * The feature id for the '<em><b>Use Legacy Editorconfig</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT__USE_LEGACY_EDITORCONFIG = MNAMED_FEATURE_COUNT + 28;

	/**
	 * The number of structural features of the '<em>MComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 29;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___DO_ACTION$_UPDATE__MCOMPONENTACTION = MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT___DO_ACTION_UPDATE__MCOMPONENTACTION = MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>MComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCOMPONENT_OPERATION_COUNT = MNAMED_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MModelElementImpl <em>MModel Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MModelElementImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMModelElement()
	 * @generated
	 */
	int MMODEL_ELEMENT = 4;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMODEL_ELEMENT__GENERAL_ANNOTATION = 0;

	/**
	 * The number of structural features of the '<em>MModel Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMODEL_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>MModel Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMODEL_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MPackageImpl <em>MPackage</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MPackageImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPackage()
	 * @generated
	 */
	int MPACKAGE = 5;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__GENERAL_ANNOTATION = MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CLASSIFIER = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub Package</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SUB_PACKAGE = MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Package Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__PACKAGE_ANNOTATIONS = MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Use Package Content Only With Explicit Filter</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER = MNAMED_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Resource Root Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__RESOURCE_ROOT_CLASS = MNAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Special Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SPECIAL_NS_URI = MNAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Special Ns Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SPECIAL_NS_PREFIX = MNAMED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Special File Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__SPECIAL_FILE_NAME = MNAMED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Use UUID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__USE_UUID = MNAMED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Uuid Attribute Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__UUID_ATTRIBUTE_NAME = MNAMED_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__PARENT = MNAMED_FEATURE_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Containing Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CONTAINING_PACKAGE = MNAMED_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Containing Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__CONTAINING_COMPONENT = MNAMED_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Represents Core Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__REPRESENTS_CORE_PACKAGE = MNAMED_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>All Subpackages</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__ALL_SUBPACKAGES = MNAMED_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__QUALIFIED_NAME = MNAMED_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Is Root Package</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__IS_ROOT_PACKAGE = MNAMED_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Internal EPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__INTERNAL_EPACKAGE = MNAMED_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Derived Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_NS_URI = MNAMED_FEATURE_COUNT + 19;

	/**
	 * The feature id for the '<em><b>Derived Standard Ns URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_STANDARD_NS_URI = MNAMED_FEATURE_COUNT + 20;

	/**
	 * The feature id for the '<em><b>Derived Ns Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_NS_PREFIX = MNAMED_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>Derived Java Package Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_JAVA_PACKAGE_NAME = MNAMED_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DO_ACTION = MNAMED_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__NAME_PREFIX = MNAMED_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Derived Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_NAME_PREFIX = MNAMED_FEATURE_COUNT + 25;

	/**
	 * The feature id for the '<em><b>Name Prefix Scope</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__NAME_PREFIX_SCOPE = MNAMED_FEATURE_COUNT + 26;

	/**
	 * The feature id for the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__DERIVED_JSON_SCHEMA = MNAMED_FEATURE_COUNT + 27;

	/**
	 * The feature id for the '<em><b>Generate Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__GENERATE_JSON_SCHEMA = MNAMED_FEATURE_COUNT + 28;

	/**
	 * The feature id for the '<em><b>Generated Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE__GENERATED_JSON_SCHEMA = MNAMED_FEATURE_COUNT + 29;

	/**
	 * The number of structural features of the '<em>MPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 30;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Generate Json Schema$update01 Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_OBJECT__BOOLEAN = MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Generate Json Schema$update01 Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_VALUE__BOOLEAN_MPACKAGE = MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___DO_ACTION$_UPDATE__MPACKAGEACTION = MNAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Generate Json Schema$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___GENERATE_JSON_SCHEMA$_UPDATE__BOOLEAN = MNAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE___DO_ACTION_UPDATE__MPACKAGEACTION = MNAMED_OPERATION_COUNT
			+ 4;

	/**
	 * The number of operations of the '<em>MPackage</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPACKAGE_OPERATION_COUNT = MNAMED_OPERATION_COUNT + 5;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MHasSimpleTypeImpl <em>MHas Simple Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MHasSimpleTypeImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMHasSimpleType()
	 * @generated
	 */
	int MHAS_SIMPLE_TYPE = 6;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING = 0;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT = 3;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE__SIMPLE_TYPE = 4;

	/**
	 * The number of structural features of the '<em>MHas Simple Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE_FEATURE_COUNT = 5;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = 0;

	/**
	 * The number of operations of the '<em>MHas Simple Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHAS_SIMPLE_TYPE_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MPropertiesContainerImpl <em>MProperties Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MPropertiesContainerImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesContainer()
	 * @generated
	 */
	int MPROPERTIES_CONTAINER = 9;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MClassifierImpl <em>MClassifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MClassifierImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMClassifier()
	 * @generated
	 */
	int MCLASSIFIER = 7;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MLiteralImpl <em>MLiteral</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MLiteralImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMLiteral()
	 * @generated
	 */
	int MLITERAL = 8;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__GENERAL_ANNOTATION = MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__PROPERTY = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__ANNOTATIONS = MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Property Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__PROPERTY_GROUP = MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>All Constraint Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS = MNAMED_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER = MNAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Owned Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__OWNED_PROPERTY = MNAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Nr Of Properties And Signatures</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES = MNAMED_FEATURE_COUNT
			+ 7;

	/**
	 * The number of structural features of the '<em>MProperties Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>All Property Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS = MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>All Operation Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS = MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>MProperties Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_CONTAINER_OPERATION_COUNT = MNAMED_OPERATION_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__KIND_LABEL = MPROPERTIES_CONTAINER__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__RENDERED_KIND_LABEL = MPROPERTIES_CONTAINER__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INDENT_LEVEL = MPROPERTIES_CONTAINER__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__DESCRIPTION = MPROPERTIES_CONTAINER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__AS_TEXT = MPROPERTIES_CONTAINER__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SPECIAL_ENAME = MPROPERTIES_CONTAINER__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__NAME = MPROPERTIES_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SHORT_NAME = MPROPERTIES_CONTAINER__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ENAME = MPROPERTIES_CONTAINER__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__FULL_LABEL = MPROPERTIES_CONTAINER__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__LOCAL_STRUCTURAL_NAME = MPROPERTIES_CONTAINER__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CALCULATED_NAME = MPROPERTIES_CONTAINER__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CALCULATED_SHORT_NAME = MPROPERTIES_CONTAINER__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CORRECT_NAME = MPROPERTIES_CONTAINER__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CORRECT_SHORT_NAME = MPROPERTIES_CONTAINER__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__GENERAL_ANNOTATION = MPROPERTIES_CONTAINER__GENERAL_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__PROPERTY = MPROPERTIES_CONTAINER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ANNOTATIONS = MPROPERTIES_CONTAINER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__PROPERTY_GROUP = MPROPERTIES_CONTAINER__PROPERTY_GROUP;

	/**
	 * The feature id for the '<em><b>All Constraint Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ALL_CONSTRAINT_ANNOTATIONS = MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CONTAINING_CLASSIFIER = MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Owned Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OWNED_PROPERTY = MPROPERTIES_CONTAINER__OWNED_PROPERTY;

	/**
	 * The feature id for the '<em><b>Nr Of Properties And Signatures</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__NR_OF_PROPERTIES_AND_SIGNATURES = MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SIMPLE_TYPE_STRING = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__HAS_SIMPLE_DATA_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SIMPLE_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Abstract Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ABSTRACT_CLASS = MPROPERTIES_CONTAINER_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Enforced Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ENFORCED_CLASS = MPROPERTIES_CONTAINER_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Super Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SUPER_TYPE_PACKAGE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Super Type</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SUPER_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__KIND = MPROPERTIES_CONTAINER_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Internal EClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTERNAL_ECLASSIFIER = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Containing Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CONTAINING_PACKAGE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 12;

	/**
	 * The feature id for the '<em><b>Represents Core Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__REPRESENTS_CORE_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 13;

	/**
	 * The feature id for the '<em><b>All Property Groups</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ALL_PROPERTY_GROUPS = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 14;

	/**
	 * The feature id for the '<em><b>Literal</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__LITERAL = MPROPERTIES_CONTAINER_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>EInterface</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__EINTERFACE = MPROPERTIES_CONTAINER_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>Operation As Id Property Error</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 17;

	/**
	 * The feature id for the '<em><b>Is Class By Structure</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__IS_CLASS_BY_STRUCTURE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 18;

	/**
	 * The feature id for the '<em><b>Toggle Super Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__TOGGLE_SUPER_TYPE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>Ordering Strategy</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ORDERING_STRATEGY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 20;

	/**
	 * The feature id for the '<em><b>Test All Properties</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__TEST_ALL_PROPERTIES = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 21;

	/**
	 * The feature id for the '<em><b>Id Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ID_PROPERTY = MPROPERTIES_CONTAINER_FEATURE_COUNT + 22;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__DO_ACTION = MPROPERTIES_CONTAINER_FEATURE_COUNT + 23;

	/**
	 * The feature id for the '<em><b>Resolve Container Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__RESOLVE_CONTAINER_ACTION = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 24;

	/**
	 * The feature id for the '<em><b>Abstract Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__ABSTRACT_DO_ACTION = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 25;

	/**
	 * The feature id for the '<em><b>Object References</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OBJECT_REFERENCES = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 26;

	/**
	 * The feature id for the '<em><b>Nearest Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__NEAREST_INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 27;

	/**
	 * The feature id for the '<em><b>Strict Nearest Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__STRICT_NEAREST_INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 28;

	/**
	 * The feature id for the '<em><b>Strict Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__STRICT_INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT + 29;

	/**
	 * The feature id for the '<em><b>Intelligent Nearest Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 30;

	/**
	 * The feature id for the '<em><b>Object Unreferenced</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OBJECT_UNREFERENCED = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 31;

	/**
	 * The feature id for the '<em><b>Outstanding To Copy Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 32;

	/**
	 * The feature id for the '<em><b>Property Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__PROPERTY_INSTANCES = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 33;

	/**
	 * The feature id for the '<em><b>Object Reference Property Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 34;

	/**
	 * The feature id for the '<em><b>Intelligent Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTELLIGENT_INSTANCE = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 35;

	/**
	 * The feature id for the '<em><b>Containment Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CONTAINMENT_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 36;

	/**
	 * The feature id for the '<em><b>Strict All Classes Contained In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 37;

	/**
	 * The feature id for the '<em><b>Strict Containment Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 38;

	/**
	 * The feature id for the '<em><b>Intelligent All Classes Contained In</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 39;

	/**
	 * The feature id for the '<em><b>Intelligent Containment Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 40;

	/**
	 * The feature id for the '<em><b>Instantiation Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INSTANTIATION_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 41;

	/**
	 * The feature id for the '<em><b>Instantiation Property Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 42;

	/**
	 * The feature id for the '<em><b>Intelligent Instantiation Property Hierarchy</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 43;

	/**
	 * The feature id for the '<em><b>Classescontainedin</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CLASSESCONTAINEDIN = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 44;

	/**
	 * The feature id for the '<em><b>Can Apply Default Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 45;

	/**
	 * The feature id for the '<em><b>Properties As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__PROPERTIES_AS_TEXT = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 46;

	/**
	 * The feature id for the '<em><b>Semantics As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__SEMANTICS_AS_TEXT = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 47;

	/**
	 * The feature id for the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER__DERIVED_JSON_SCHEMA = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 48;

	/**
	 * The number of structural features of the '<em>MClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER_FEATURE_COUNT = MPROPERTIES_CONTAINER_FEATURE_COUNT + 49;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___INDENTATION_SPACES = MPROPERTIES_CONTAINER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___INDENTATION_SPACES__INTEGER = MPROPERTIES_CONTAINER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___NEW_LINE_STRING = MPROPERTIES_CONTAINER___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SAME_NAME__MNAMED = MPROPERTIES_CONTAINER___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SAME_SHORT_NAME__MNAMED = MPROPERTIES_CONTAINER___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SAME_STRING__STRING_STRING = MPROPERTIES_CONTAINER___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___STRING_EMPTY__STRING = MPROPERTIES_CONTAINER___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>All Property Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_PROPERTY_ANNOTATIONS = MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS;

	/**
	 * The operation id for the '<em>All Operation Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_OPERATION_ANNOTATIONS = MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Toggle Super Type$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___TOGGLE_SUPER_TYPE$_UPDATE__MCLASSIFIER = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Ordering Strategy$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ORDERING_STRATEGY$_UPDATE__ORDERINGSTRATEGY = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___DO_ACTION$_UPDATE__MCLASSIFIERACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>All Literals</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_LITERALS = MPROPERTIES_CONTAINER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Selectable Classifier</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SELECTABLE_CLASSIFIER__ELIST_MPACKAGE = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>All Direct Sub Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_DIRECT_SUB_TYPES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>All Sub Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_SUB_TYPES = MPROPERTIES_CONTAINER_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Super Types As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___SUPER_TYPES_AS_STRING = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>All Super Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_SUPER_TYPES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>All Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_PROPERTIES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>All Super Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_SUPER_PROPERTIES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>All Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_FEATURES = MPROPERTIES_CONTAINER_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>All Non Containment Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_NON_CONTAINMENT_FEATURES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>All Stored Non Containment Features</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_STORED_NON_CONTAINMENT_FEATURES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 14;

	/**
	 * The operation id for the '<em>All Features With Storage</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_FEATURES_WITH_STORAGE = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 15;

	/**
	 * The operation id for the '<em>All Containment References</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_CONTAINMENT_REFERENCES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 16;

	/**
	 * The operation id for the '<em>All Operation Signatures</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_OPERATION_SIGNATURES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 17;

	/**
	 * The operation id for the '<em>All Classes Contained In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_CLASSES_CONTAINED_IN = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 18;

	/**
	 * The operation id for the '<em>Default Property Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___DEFAULT_PROPERTY_VALUE = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 19;

	/**
	 * The operation id for the '<em>Default Property Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___DEFAULT_PROPERTY_DESCRIPTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 20;

	/**
	 * The operation id for the '<em>All Id Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_ID_PROPERTIES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 21;

	/**
	 * The operation id for the '<em>All Super Id Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___ALL_SUPER_ID_PROPERTIES = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 22;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___DO_ACTION_UPDATE__MCLASSIFIERACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 23;

	/**
	 * The operation id for the '<em>Invoke Set Do Action</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___INVOKE_SET_DO_ACTION__MCLASSIFIERACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 24;

	/**
	 * The operation id for the '<em>Invoke Toggle Super Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___INVOKE_TOGGLE_SUPER_TYPE__MCLASSIFIER = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 25;

	/**
	 * The operation id for the '<em>Do Super Type Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___DO_SUPER_TYPE_UPDATE__MCLASSIFIER = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 26;

	/**
	 * The operation id for the '<em>Ambiguous Classifier Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER___AMBIGUOUS_CLASSIFIER_NAME = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 27;

	/**
	 * The number of operations of the '<em>MClassifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MCLASSIFIER_OPERATION_COUNT = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 28;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__GENERAL_ANNOTATION = MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Constant Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CONSTANT_LABEL = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__AS_STRING = MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__QUALIFIED_NAME = MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Containing Enumeration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__CONTAINING_ENUMERATION = MNAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Internal EEnum Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL__INTERNAL_EENUM_LITERAL = MNAMED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>MLiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The number of operations of the '<em>MLiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_OPERATION_COUNT = MNAMED_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MPropertiesGroupImpl <em>MProperties Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MPropertiesGroupImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesGroup()
	 * @generated
	 */
	int MPROPERTIES_GROUP = 10;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__KIND_LABEL = MPROPERTIES_CONTAINER__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__RENDERED_KIND_LABEL = MPROPERTIES_CONTAINER__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__INDENT_LEVEL = MPROPERTIES_CONTAINER__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__DESCRIPTION = MPROPERTIES_CONTAINER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__AS_TEXT = MPROPERTIES_CONTAINER__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__SPECIAL_ENAME = MPROPERTIES_CONTAINER__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__NAME = MPROPERTIES_CONTAINER__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__SHORT_NAME = MPROPERTIES_CONTAINER__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__ENAME = MPROPERTIES_CONTAINER__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__FULL_LABEL = MPROPERTIES_CONTAINER__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__LOCAL_STRUCTURAL_NAME = MPROPERTIES_CONTAINER__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__CALCULATED_NAME = MPROPERTIES_CONTAINER__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__CALCULATED_SHORT_NAME = MPROPERTIES_CONTAINER__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__CORRECT_NAME = MPROPERTIES_CONTAINER__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__CORRECT_SHORT_NAME = MPROPERTIES_CONTAINER__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__GENERAL_ANNOTATION = MPROPERTIES_CONTAINER__GENERAL_ANNOTATION;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__PROPERTY = MPROPERTIES_CONTAINER__PROPERTY;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__ANNOTATIONS = MPROPERTIES_CONTAINER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Property Group</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__PROPERTY_GROUP = MPROPERTIES_CONTAINER__PROPERTY_GROUP;

	/**
	 * The feature id for the '<em><b>All Constraint Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__ALL_CONSTRAINT_ANNOTATIONS = MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__CONTAINING_CLASSIFIER = MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Owned Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__OWNED_PROPERTY = MPROPERTIES_CONTAINER__OWNED_PROPERTY;

	/**
	 * The feature id for the '<em><b>Nr Of Properties And Signatures</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__NR_OF_PROPERTIES_AND_SIGNATURES = MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES;

	/**
	 * The feature id for the '<em><b>Order</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__ORDER = MPROPERTIES_CONTAINER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP__DO_ACTION = MPROPERTIES_CONTAINER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>MProperties Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP_FEATURE_COUNT = MPROPERTIES_CONTAINER_FEATURE_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___INDENTATION_SPACES = MPROPERTIES_CONTAINER___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___INDENTATION_SPACES__INTEGER = MPROPERTIES_CONTAINER___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___NEW_LINE_STRING = MPROPERTIES_CONTAINER___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___SAME_NAME__MNAMED = MPROPERTIES_CONTAINER___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___SAME_SHORT_NAME__MNAMED = MPROPERTIES_CONTAINER___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___SAME_STRING__STRING_STRING = MPROPERTIES_CONTAINER___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___STRING_EMPTY__STRING = MPROPERTIES_CONTAINER___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>All Property Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___ALL_PROPERTY_ANNOTATIONS = MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS;

	/**
	 * The operation id for the '<em>All Operation Annotations</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___ALL_OPERATION_ANNOTATIONS = MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___DO_ACTION$_UPDATE__MPROPERTIESGROUPACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___DO_ACTION_UPDATE__MPROPERTIESGROUPACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Invoke Set Do Action</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP___INVOKE_SET_DO_ACTION__MPROPERTIESGROUPACTION = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 2;

	/**
	 * The number of operations of the '<em>MProperties Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTIES_GROUP_OPERATION_COUNT = MPROPERTIES_CONTAINER_OPERATION_COUNT
			+ 3;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl <em>MExplicitly Typed And Named</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMExplicitlyTypedAndNamed()
	 * @generated
	 */
	int MEXPLICITLY_TYPED_AND_NAMED = 16;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MPropertyImpl <em>MProperty</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MPropertyImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMProperty()
	 * @generated
	 */
	int MPROPERTY = 11;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MTypedImpl <em>MTyped</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MTypedImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMTyped()
	 * @generated
	 */
	int MTYPED = 13;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MOperationSignatureImpl <em>MOperation Signature</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MOperationSignatureImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMOperationSignature()
	 * @generated
	 */
	int MOPERATION_SIGNATURE = 12;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MExplicitlyTypedImpl <em>MExplicitly Typed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MExplicitlyTypedImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMExplicitlyTyped()
	 * @generated
	 */
	int MEXPLICITLY_TYPED = 14;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MVariableImpl <em>MVariable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MVariableImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMVariable()
	 * @generated
	 */
	int MVARIABLE = 15;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING = MNAMED_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY = MNAMED_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR = MNAMED_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE = MNAMED_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED = MNAMED_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE = MNAMED_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE = MNAMED_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING = MNAMED_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE = MNAMED_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE = MNAMED_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT = MNAMED_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE = MNAMED_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__TYPE = MNAMED_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE = MNAMED_FEATURE_COUNT + 14;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__MANDATORY = MNAMED_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__SINGULAR = MNAMED_FEATURE_COUNT + 16;

	/**
	 * The feature id for the '<em><b>EType Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME = MNAMED_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>EType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL = MNAMED_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Correctly Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED = MNAMED_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>Append Type Name To Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME = MNAMED_FEATURE_COUNT
			+ 20;

	/**
	 * The number of structural features of the '<em>MExplicitly Typed And Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 21;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = MNAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE = MNAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Short Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE = MNAMED_OPERATION_COUNT
			+ 4;

	/**
	 * The number of operations of the '<em>MExplicitly Typed And Named</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT = MNAMED_OPERATION_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__KIND_LABEL = MEXPLICITLY_TYPED_AND_NAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__RENDERED_KIND_LABEL = MEXPLICITLY_TYPED_AND_NAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__INDENT_LEVEL = MEXPLICITLY_TYPED_AND_NAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__DESCRIPTION = MEXPLICITLY_TYPED_AND_NAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__AS_TEXT = MEXPLICITLY_TYPED_AND_NAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SPECIAL_ENAME = MEXPLICITLY_TYPED_AND_NAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__NAME = MEXPLICITLY_TYPED_AND_NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ENAME = MEXPLICITLY_TYPED_AND_NAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__FULL_LABEL = MEXPLICITLY_TYPED_AND_NAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__LOCAL_STRUCTURAL_NAME = MEXPLICITLY_TYPED_AND_NAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_NAME = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CORRECT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CORRECT_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__MULTIPLICITY_AS_STRING = MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_TYPE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_MANDATORY = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_SINGULAR = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_TYPE_PACKAGE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__VOID_TYPE_ALLOWED = MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CALCULATED_SIMPLE_TYPE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__MULTIPLICITY_CASE = MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SIMPLE_TYPE_STRING = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__HAS_SIMPLE_DATA_TYPE = MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__HAS_SIMPLE_MODELING_TYPE = MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SIMPLE_TYPE_IS_CORRECT = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SIMPLE_TYPE = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__TYPE = MEXPLICITLY_TYPED_AND_NAMED__TYPE;

	/**
	 * The feature id for the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__TYPE_PACKAGE = MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__MANDATORY = MEXPLICITLY_TYPED_AND_NAMED__MANDATORY;

	/**
	 * The feature id for the '<em><b>Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SINGULAR = MEXPLICITLY_TYPED_AND_NAMED__SINGULAR;

	/**
	 * The feature id for the '<em><b>EType Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ETYPE_NAME = MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME;

	/**
	 * The feature id for the '<em><b>EType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ETYPE_LABEL = MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL;

	/**
	 * The feature id for the '<em><b>Correctly Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CORRECTLY_TYPED = MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED;

	/**
	 * The feature id for the '<em><b>Append Type Name To Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__APPEND_TYPE_NAME_TO_NAME = MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__GENERAL_ANNOTATION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Operation Signature</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__OPERATION_SIGNATURE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Opposite Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__OPPOSITE_DEFINITION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Opposite</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__OPPOSITE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CONTAINMENT = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Has Storage</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__HAS_STORAGE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Changeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CHANGEABLE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Persisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__PERSISTED = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Property Behavior</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__PROPERTY_BEHAVIOR = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Is Operation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__IS_OPERATION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>Is Attribute</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__IS_ATTRIBUTE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Is Reference</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__IS_REFERENCE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__KIND = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>Type Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__TYPE_DEFINITION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 13;

	/**
	 * The feature id for the '<em><b>Use Core Types</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__USE_CORE_TYPES = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 14;

	/**
	 * The feature id for the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ELABEL = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 15;

	/**
	 * The feature id for the '<em><b>EName For ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ENAME_FOR_ELABEL = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 16;

	/**
	 * The feature id for the '<em><b>Order Within Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ORDER_WITHIN_GROUP = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 17;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ID = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 18;

	/**
	 * The feature id for the '<em><b>Internal EStructural Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>Direct Semantics</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__DIRECT_SEMANTICS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 20;

	/**
	 * The feature id for the '<em><b>Semantics Specialization</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SEMANTICS_SPECIALIZATION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 21;

	/**
	 * The feature id for the '<em><b>All Signatures</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ALL_SIGNATURES = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 22;

	/**
	 * The feature id for the '<em><b>Direct Operation Semantics</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__DIRECT_OPERATION_SEMANTICS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 23;

	/**
	 * The feature id for the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CONTAINING_CLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 24;

	/**
	 * The feature id for the '<em><b>Containing Properties Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__CONTAINING_PROPERTIES_CONTAINER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 25;

	/**
	 * The feature id for the '<em><b>EKey For Path</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__EKEY_FOR_PATH = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 26;

	/**
	 * The feature id for the '<em><b>Not Unsettable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__NOT_UNSETTABLE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 27;

	/**
	 * The feature id for the '<em><b>Attribute For EId</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ATTRIBUTE_FOR_EID = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 28;

	/**
	 * The feature id for the '<em><b>EDefault Value Literal</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__EDEFAULT_VALUE_LITERAL = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 29;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__DO_ACTION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 30;

	/**
	 * The feature id for the '<em><b>Behavior Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__BEHAVIOR_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 31;

	/**
	 * The feature id for the '<em><b>Layout Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__LAYOUT_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 32;

	/**
	 * The feature id for the '<em><b>Arity Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ARITY_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 33;

	/**
	 * The feature id for the '<em><b>Simple Type Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__SIMPLE_TYPE_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 34;

	/**
	 * The feature id for the '<em><b>Annotation Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__ANNOTATION_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 35;

	/**
	 * The feature id for the '<em><b>Move Actions</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__MOVE_ACTIONS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 36;

	/**
	 * The feature id for the '<em><b>Derived Json Schema</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY__DERIVED_JSON_SCHEMA = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 37;

	/**
	 * The number of structural features of the '<em>MProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_FEATURE_COUNT = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 38;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___INDENTATION_SPACES = MEXPLICITLY_TYPED_AND_NAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___INDENTATION_SPACES__INTEGER = MEXPLICITLY_TYPED_AND_NAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___NEW_LINE_STRING = MEXPLICITLY_TYPED_AND_NAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SAME_NAME__MNAMED = MEXPLICITLY_TYPED_AND_NAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SAME_SHORT_NAME__MNAMED = MEXPLICITLY_TYPED_AND_NAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SAME_STRING__STRING_STRING = MEXPLICITLY_TYPED_AND_NAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___STRING_EMPTY__STRING = MEXPLICITLY_TYPED_AND_NAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MEXPLICITLY_TYPED_AND_NAMED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MEXPLICITLY_TYPED_AND_NAMED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = MEXPLICITLY_TYPED_AND_NAMED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE;

	/**
	 * The operation id for the '<em>Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___NAME_FROM_TYPE = MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE;

	/**
	 * The operation id for the '<em>Short Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SHORT_NAME_FROM_TYPE = MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE;

	/**
	 * The operation id for the '<em>Opposite Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___OPPOSITE_DEFINITION$_UPDATE__MPROPERTY = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Property Behavior$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___PROPERTY_BEHAVIOR$_UPDATE__PROPERTYBEHAVIOR = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Type Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___TYPE_DEFINITION$_UPDATE__MCLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___DO_ACTION$_UPDATE__MPROPERTYACTION = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Selectable Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___SELECTABLE_TYPE__MCLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Ambiguous Property Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___AMBIGUOUS_PROPERTY_NAME = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Ambiguous Property Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___AMBIGUOUS_PROPERTY_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___DO_ACTION_UPDATE__MPROPERTYACTION = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Invoke Set Property Behaviour</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___INVOKE_SET_PROPERTY_BEHAVIOUR__PROPERTYBEHAVIOR = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Invoke Set Do Action</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY___INVOKE_SET_DO_ACTION__MPROPERTYACTION = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 9;

	/**
	 * The number of operations of the '<em>MProperty</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_OPERATION_COUNT = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__KIND_LABEL = MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__RENDERED_KIND_LABEL = MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__INDENT_LEVEL = MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__DESCRIPTION = MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__AS_TEXT = MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__MULTIPLICITY_AS_STRING = MREPOSITORY_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__CALCULATED_TYPE = MREPOSITORY_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__CALCULATED_MANDATORY = MREPOSITORY_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__CALCULATED_SINGULAR = MREPOSITORY_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__CALCULATED_TYPE_PACKAGE = MREPOSITORY_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__VOID_TYPE_ALLOWED = MREPOSITORY_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__CALCULATED_SIMPLE_TYPE = MREPOSITORY_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED__MULTIPLICITY_CASE = MREPOSITORY_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>MTyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED_FEATURE_COUNT = MREPOSITORY_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED___INDENTATION_SPACES = MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED___INDENTATION_SPACES__INTEGER = MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED___NEW_LINE_STRING = MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>MTyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTYPED_OPERATION_COUNT = MREPOSITORY_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__KIND_LABEL = MTYPED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__RENDERED_KIND_LABEL = MTYPED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__INDENT_LEVEL = MTYPED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__DESCRIPTION = MTYPED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__AS_TEXT = MTYPED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__MULTIPLICITY_AS_STRING = MTYPED__MULTIPLICITY_AS_STRING;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CALCULATED_TYPE = MTYPED__CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CALCULATED_MANDATORY = MTYPED__CALCULATED_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CALCULATED_SINGULAR = MTYPED__CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CALCULATED_TYPE_PACKAGE = MTYPED__CALCULATED_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__VOID_TYPE_ALLOWED = MTYPED__VOID_TYPE_ALLOWED;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CALCULATED_SIMPLE_TYPE = MTYPED__CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__MULTIPLICITY_CASE = MTYPED__MULTIPLICITY_CASE;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__GENERAL_ANNOTATION = MTYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Parameter</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__PARAMETER = MTYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__OPERATION = MTYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER = MTYPED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__ELABEL = MTYPED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Internal EOperation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__INTERNAL_EOPERATION = MTYPED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE__DO_ACTION = MTYPED_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>MOperation Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE_FEATURE_COUNT = MTYPED_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___INDENTATION_SPACES = MTYPED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___INDENTATION_SPACES__INTEGER = MTYPED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___NEW_LINE_STRING = MTYPED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___DO_ACTION$_UPDATE__MOPERATIONSIGNATUREACTION = MTYPED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Same Signature</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___SAME_SIGNATURE__MOPERATIONSIGNATURE = MTYPED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Signature As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE___SIGNATURE_AS_STRING = MTYPED_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>MOperation Signature</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOPERATION_SIGNATURE_OPERATION_COUNT = MTYPED_OPERATION_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__KIND_LABEL = MTYPED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__RENDERED_KIND_LABEL = MTYPED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__INDENT_LEVEL = MTYPED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__DESCRIPTION = MTYPED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__AS_TEXT = MTYPED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__MULTIPLICITY_AS_STRING = MTYPED__MULTIPLICITY_AS_STRING;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CALCULATED_TYPE = MTYPED__CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CALCULATED_MANDATORY = MTYPED__CALCULATED_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CALCULATED_SINGULAR = MTYPED__CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CALCULATED_TYPE_PACKAGE = MTYPED__CALCULATED_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__VOID_TYPE_ALLOWED = MTYPED__VOID_TYPE_ALLOWED;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CALCULATED_SIMPLE_TYPE = MTYPED__CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__MULTIPLICITY_CASE = MTYPED__MULTIPLICITY_CASE;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__SIMPLE_TYPE_STRING = MTYPED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__HAS_SIMPLE_DATA_TYPE = MTYPED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__HAS_SIMPLE_MODELING_TYPE = MTYPED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__SIMPLE_TYPE_IS_CORRECT = MTYPED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__SIMPLE_TYPE = MTYPED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__TYPE = MTYPED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__TYPE_PACKAGE = MTYPED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__MANDATORY = MTYPED_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__SINGULAR = MTYPED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>EType Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__ETYPE_NAME = MTYPED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>EType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__ETYPE_LABEL = MTYPED_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>Correctly Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED__CORRECTLY_TYPED = MTYPED_FEATURE_COUNT + 11;

	/**
	 * The number of structural features of the '<em>MExplicitly Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_FEATURE_COUNT = MTYPED_FEATURE_COUNT + 12;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___INDENTATION_SPACES = MTYPED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___INDENTATION_SPACES__INTEGER = MTYPED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___NEW_LINE_STRING = MTYPED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = MTYPED_OPERATION_COUNT
			+ 0;

	/**
	 * The number of operations of the '<em>MExplicitly Typed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEXPLICITLY_TYPED_OPERATION_COUNT = MTYPED_OPERATION_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__KIND_LABEL = MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__RENDERED_KIND_LABEL = MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__INDENT_LEVEL = MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__DESCRIPTION = MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__AS_TEXT = MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__SPECIAL_ENAME = MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__NAME = MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__SHORT_NAME = MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__ENAME = MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__FULL_LABEL = MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__LOCAL_STRUCTURAL_NAME = MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_NAME = MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_SHORT_NAME = MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CORRECT_NAME = MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CORRECT_SHORT_NAME = MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__MULTIPLICITY_AS_STRING = MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_TYPE = MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_MANDATORY = MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_SINGULAR = MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_TYPE_PACKAGE = MNAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__VOID_TYPE_ALLOWED = MNAMED_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__CALCULATED_SIMPLE_TYPE = MNAMED_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE__MULTIPLICITY_CASE = MNAMED_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>MVariable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE_FEATURE_COUNT = MNAMED_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___INDENTATION_SPACES = MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___INDENTATION_SPACES__INTEGER = MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___NEW_LINE_STRING = MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___SAME_NAME__MNAMED = MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___SAME_SHORT_NAME__MNAMED = MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___SAME_STRING__STRING_STRING = MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___STRING_EMPTY__STRING = MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The number of operations of the '<em>MVariable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVARIABLE_OPERATION_COUNT = MNAMED_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MParameterImpl <em>MParameter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MParameterImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMParameter()
	 * @generated
	 */
	int MPARAMETER = 17;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__KIND_LABEL = MEXPLICITLY_TYPED_AND_NAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__RENDERED_KIND_LABEL = MEXPLICITLY_TYPED_AND_NAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__INDENT_LEVEL = MEXPLICITLY_TYPED_AND_NAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__DESCRIPTION = MEXPLICITLY_TYPED_AND_NAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AS_TEXT = MEXPLICITLY_TYPED_AND_NAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SPECIAL_ENAME = MEXPLICITLY_TYPED_AND_NAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__NAME = MEXPLICITLY_TYPED_AND_NAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ENAME = MEXPLICITLY_TYPED_AND_NAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__FULL_LABEL = MEXPLICITLY_TYPED_AND_NAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__LOCAL_STRUCTURAL_NAME = MEXPLICITLY_TYPED_AND_NAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_NAME = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CORRECT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CORRECT_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Multiplicity As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__MULTIPLICITY_AS_STRING = MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING;

	/**
	 * The feature id for the '<em><b>Calculated Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_TYPE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE;

	/**
	 * The feature id for the '<em><b>Calculated Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_MANDATORY = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY;

	/**
	 * The feature id for the '<em><b>Calculated Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_SINGULAR = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR;

	/**
	 * The feature id for the '<em><b>Calculated Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_TYPE_PACKAGE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Void Type Allowed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__VOID_TYPE_ALLOWED = MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED;

	/**
	 * The feature id for the '<em><b>Calculated Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CALCULATED_SIMPLE_TYPE = MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Multiplicity Case</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__MULTIPLICITY_CASE = MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE;

	/**
	 * The feature id for the '<em><b>Simple Type String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SIMPLE_TYPE_STRING = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING;

	/**
	 * The feature id for the '<em><b>Has Simple Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__HAS_SIMPLE_DATA_TYPE = MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE;

	/**
	 * The feature id for the '<em><b>Has Simple Modeling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__HAS_SIMPLE_MODELING_TYPE = MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE;

	/**
	 * The feature id for the '<em><b>Simple Type Is Correct</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SIMPLE_TYPE_IS_CORRECT = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT;

	/**
	 * The feature id for the '<em><b>Simple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SIMPLE_TYPE = MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TYPE = MEXPLICITLY_TYPED_AND_NAMED__TYPE;

	/**
	 * The feature id for the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TYPE_PACKAGE = MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE;

	/**
	 * The feature id for the '<em><b>Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__MANDATORY = MEXPLICITLY_TYPED_AND_NAMED__MANDATORY;

	/**
	 * The feature id for the '<em><b>Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SINGULAR = MEXPLICITLY_TYPED_AND_NAMED__SINGULAR;

	/**
	 * The feature id for the '<em><b>EType Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ETYPE_NAME = MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME;

	/**
	 * The feature id for the '<em><b>EType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ETYPE_LABEL = MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL;

	/**
	 * The feature id for the '<em><b>Correctly Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CORRECTLY_TYPED = MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED;

	/**
	 * The feature id for the '<em><b>Append Type Name To Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__APPEND_TYPE_NAME_TO_NAME = MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME;

	/**
	 * The feature id for the '<em><b>General Annotation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__GENERAL_ANNOTATION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ALABEL = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AKIND_BASE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ARENDERED_KIND = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ACONTAINING_COMPONENT = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>TPackage Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TPACKAGE_URI = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>TClassifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TCLASSIFIER_NAME = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>TFeature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TFEATURE_NAME = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>TPackage</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TPACKAGE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>TClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TCLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>TFeature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TFEATURE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 10;

	/**
	 * The feature id for the '<em><b>TA Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__TA_CORE_ASTRING_CLASS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ANAME = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 12;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AUNDEFINED_NAME_CONSTANT = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 13;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ABUSINESS_NAME = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 14;

	/**
	 * The feature id for the '<em><b>ASpecialized Classifier</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ASPECIALIZED_CLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 15;

	/**
	 * The feature id for the '<em><b>AActive Data Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AACTIVE_DATA_TYPE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 16;

	/**
	 * The feature id for the '<em><b>AActive Enumeration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AACTIVE_ENUMERATION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 17;

	/**
	 * The feature id for the '<em><b>AActive Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AACTIVE_CLASS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 18;

	/**
	 * The feature id for the '<em><b>AContaining Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ACONTAINING_PACKAGE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>As Data Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AS_DATA_TYPE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 20;

	/**
	 * The feature id for the '<em><b>As Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AS_CLASS = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 21;

	/**
	 * The feature id for the '<em><b>AIs String Classifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__AIS_STRING_CLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 22;

	/**
	 * The feature id for the '<em><b>Containing Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__CONTAINING_SIGNATURE = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 23;

	/**
	 * The feature id for the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__ELABEL = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 24;

	/**
	 * The feature id for the '<em><b>Signature As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__SIGNATURE_AS_STRING = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 25;

	/**
	 * The feature id for the '<em><b>Internal EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__INTERNAL_EPARAMETER = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 26;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER__DO_ACTION = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT + 27;

	/**
	 * The number of structural features of the '<em>MParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER_FEATURE_COUNT = MEXPLICITLY_TYPED_AND_NAMED_FEATURE_COUNT
			+ 28;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___NEW_LINE_STRING = MEXPLICITLY_TYPED_AND_NAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___SAME_NAME__MNAMED = MEXPLICITLY_TYPED_AND_NAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___SAME_SHORT_NAME__MNAMED = MEXPLICITLY_TYPED_AND_NAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___SAME_STRING__STRING_STRING = MEXPLICITLY_TYPED_AND_NAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___STRING_EMPTY__STRING = MEXPLICITLY_TYPED_AND_NAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Multiplicity Case$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = MEXPLICITLY_TYPED_AND_NAMED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;

	/**
	 * The operation id for the '<em>Type As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = MEXPLICITLY_TYPED_AND_NAMED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;

	/**
	 * The operation id for the '<em>Simple Type As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = MEXPLICITLY_TYPED_AND_NAMED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE;

	/**
	 * The operation id for the '<em>Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___NAME_FROM_TYPE = MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE;

	/**
	 * The operation id for the '<em>Short Name From Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___SHORT_NAME_FROM_TYPE = MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE;

	/**
	 * The operation id for the '<em>Indent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___INDENT_LEVEL = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___INDENTATION_SPACES = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___INDENTATION_SPACES__INTEGER = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>String Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___STRING_OR_MISSING__STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>String Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___STRING_IS_EMPTY__STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>List Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___APACKAGE_FROM_URI__STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___ACORE_ASTRING_CLASS = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___ACORE_AREAL_CLASS = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___ACORE_AINTEGER_CLASS = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 12;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___ACORE_AOBJECT_CLASS = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>AAssignable To</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___AASSIGNABLE_TO__ACLASSIFIER = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 14;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___DO_ACTION$_UPDATE__MPARAMETERACTION = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 15;

	/**
	 * The operation id for the '<em>Ambiguous Parameter Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___AMBIGUOUS_PARAMETER_NAME = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 16;

	/**
	 * The operation id for the '<em>Ambiguous Parameter Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER___AMBIGUOUS_PARAMETER_SHORT_NAME = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 17;

	/**
	 * The number of operations of the '<em>MParameter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPARAMETER_OPERATION_COUNT = MEXPLICITLY_TYPED_AND_NAMED_OPERATION_COUNT
			+ 18;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MEditorImpl <em>MEditor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MEditorImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMEditor()
	 * @generated
	 */
	int MEDITOR = 18;

	/**
	 * The number of structural features of the '<em>MEditor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Reset Editor</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR___RESET_EDITOR = 0;

	/**
	 * The number of operations of the '<em>MEditor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEDITOR_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link com.montages.mcore.impl.MNamedEditorImpl <em>MNamed Editor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.impl.MNamedEditorImpl
	 * @see com.montages.mcore.impl.McorePackageImpl#getMNamedEditor()
	 * @generated
	 */
	int MNAMED_EDITOR = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR__NAME = 0;

	/**
	 * The feature id for the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR__LABEL = 1;

	/**
	 * The feature id for the '<em><b>User Visible</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR__USER_VISIBLE = 2;

	/**
	 * The feature id for the '<em><b>Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR__EDITOR = 3;

	/**
	 * The number of structural features of the '<em>MNamed Editor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>MNamed Editor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MNAMED_EDITOR_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MComponentAction <em>MComponent Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MComponentAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMComponentAction()
	 * @generated
	 */
	int MCOMPONENT_ACTION = 20;

	/**
	 * The meta object id for the '{@link com.montages.mcore.TopLevelDomain <em>Top Level Domain</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.TopLevelDomain
	 * @see com.montages.mcore.impl.McorePackageImpl#getTopLevelDomain()
	 * @generated
	 */
	int TOP_LEVEL_DOMAIN = 21;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MPackageAction <em>MPackage Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MPackageAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPackageAction()
	 * @generated
	 */
	int MPACKAGE_ACTION = 22;

	/**
	 * The meta object id for the '{@link com.montages.mcore.SimpleType <em>Simple Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.impl.McorePackageImpl#getSimpleType()
	 * @generated
	 */
	int SIMPLE_TYPE = 23;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MClassifierAction <em>MClassifier Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MClassifierAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMClassifierAction()
	 * @generated
	 */
	int MCLASSIFIER_ACTION = 24;

	/**
	 * The meta object id for the '{@link com.montages.mcore.OrderingStrategy <em>Ordering Strategy</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.OrderingStrategy
	 * @see com.montages.mcore.impl.McorePackageImpl#getOrderingStrategy()
	 * @generated
	 */
	int ORDERING_STRATEGY = 25;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MLiteralAction <em>MLiteral Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MLiteralAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMLiteralAction()
	 * @generated
	 */
	int MLITERAL_ACTION = 26;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MPropertiesGroupAction <em>MProperties Group Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MPropertiesGroupAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesGroupAction()
	 * @generated
	 */
	int MPROPERTIES_GROUP_ACTION = 27;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MPropertyAction <em>MProperty Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MPropertyAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertyAction()
	 * @generated
	 */
	int MPROPERTY_ACTION = 28;

	/**
	 * The meta object id for the '{@link com.montages.mcore.PropertyBehavior <em>Property Behavior</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.PropertyBehavior
	 * @see com.montages.mcore.impl.McorePackageImpl#getPropertyBehavior()
	 * @generated
	 */
	int PROPERTY_BEHAVIOR = 29;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MOperationSignatureAction <em>MOperation Signature Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MOperationSignatureAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMOperationSignatureAction()
	 * @generated
	 */
	int MOPERATION_SIGNATURE_ACTION = 30;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MultiplicityCase <em>Multiplicity Case</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MultiplicityCase
	 * @see com.montages.mcore.impl.McorePackageImpl#getMultiplicityCase()
	 * @generated
	 */
	int MULTIPLICITY_CASE = 31;

	/**
	 * The meta object id for the '{@link com.montages.mcore.ClassifierKind <em>Classifier Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.ClassifierKind
	 * @see com.montages.mcore.impl.McorePackageImpl#getClassifierKind()
	 * @generated
	 */
	int CLASSIFIER_KIND = 32;

	/**
	 * The meta object id for the '{@link com.montages.mcore.PropertyKind <em>Property Kind</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.PropertyKind
	 * @see com.montages.mcore.impl.McorePackageImpl#getPropertyKind()
	 * @generated
	 */
	int PROPERTY_KIND = 33;

	/**
	 * The meta object id for the '{@link com.montages.mcore.MParameterAction <em>MParameter Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.MParameterAction
	 * @see com.montages.mcore.impl.McorePackageImpl#getMParameterAction()
	 * @generated
	 */
	int MPARAMETER_ACTION = 34;

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MRepositoryElement <em>MRepository Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MRepository Element</em>'.
	 * @see com.montages.mcore.MRepositoryElement
	 * @generated
	 */
	EClass getMRepositoryElement();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepositoryElement#getKindLabel <em>Kind Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind Label</em>'.
	 * @see com.montages.mcore.MRepositoryElement#getKindLabel()
	 * @see #getMRepositoryElement()
	 * @generated
	 */
	EAttribute getMRepositoryElement_KindLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepositoryElement#getRenderedKindLabel <em>Rendered Kind Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Rendered Kind Label</em>'.
	 * @see com.montages.mcore.MRepositoryElement#getRenderedKindLabel()
	 * @see #getMRepositoryElement()
	 * @generated
	 */
	EAttribute getMRepositoryElement_RenderedKindLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepositoryElement#getIndentLevel <em>Indent Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Indent Level</em>'.
	 * @see com.montages.mcore.MRepositoryElement#getIndentLevel()
	 * @see #getMRepositoryElement()
	 * @generated
	 */
	EAttribute getMRepositoryElement_IndentLevel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepositoryElement#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see com.montages.mcore.MRepositoryElement#getDescription()
	 * @see #getMRepositoryElement()
	 * @generated
	 */
	EAttribute getMRepositoryElement_Description();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepositoryElement#getAsText <em>As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As Text</em>'.
	 * @see com.montages.mcore.MRepositoryElement#getAsText()
	 * @see #getMRepositoryElement()
	 * @generated
	 */
	EAttribute getMRepositoryElement_AsText();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MRepositoryElement#indentationSpaces() <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see com.montages.mcore.MRepositoryElement#indentationSpaces()
	 * @generated
	 */
	EOperation getMRepositoryElement__IndentationSpaces();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MRepositoryElement#indentationSpaces(java.lang.Integer) <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Indentation Spaces</em>' operation.
	 * @see com.montages.mcore.MRepositoryElement#indentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getMRepositoryElement__IndentationSpaces__Integer();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MRepositoryElement#newLineString() <em>New Line String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>New Line String</em>' operation.
	 * @see com.montages.mcore.MRepositoryElement#newLineString()
	 * @generated
	 */
	EOperation getMRepositoryElement__NewLineString();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MRepository <em>MRepository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MRepository</em>'.
	 * @see com.montages.mcore.MRepository
	 * @generated
	 */
	EClass getMRepository();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MRepository#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component</em>'.
	 * @see com.montages.mcore.MRepository#getComponent()
	 * @see #getMRepository()
	 * @generated
	 */
	EReference getMRepository_Component();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mcore.MRepository#getCore <em>Core</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Core</em>'.
	 * @see com.montages.mcore.MRepository#getCore()
	 * @see #getMRepository()
	 * @generated
	 */
	EReference getMRepository_Core();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MRepository#getReferencedEPackage <em>Referenced EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Referenced EPackage</em>'.
	 * @see com.montages.mcore.MRepository#getReferencedEPackage()
	 * @see #getMRepository()
	 * @generated
	 */
	EReference getMRepository_ReferencedEPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avoid Regeneration Of Editor Configuration</em>'.
	 * @see com.montages.mcore.MRepository#getAvoidRegenerationOfEditorConfiguration()
	 * @see #getMRepository()
	 * @generated
	 */
	EAttribute getMRepository_AvoidRegenerationOfEditorConfiguration();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MNamed <em>MNamed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MNamed</em>'.
	 * @see com.montages.mcore.MNamed
	 * @generated
	 */
	EClass getMNamed();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getCalculatedShortName <em>Calculated Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Short Name</em>'.
	 * @see com.montages.mcore.MNamed#getCalculatedShortName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_CalculatedShortName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.montages.mcore.MNamed#getName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getShortName <em>Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Short Name</em>'.
	 * @see com.montages.mcore.MNamed#getShortName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_ShortName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getCorrectName <em>Correct Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct Name</em>'.
	 * @see com.montages.mcore.MNamed#getCorrectName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_CorrectName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getCorrectShortName <em>Correct Short Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct Short Name</em>'.
	 * @see com.montages.mcore.MNamed#getCorrectShortName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_CorrectShortName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getEName <em>EName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EName</em>'.
	 * @see com.montages.mcore.MNamed#getEName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_EName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getSpecialEName <em>Special EName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special EName</em>'.
	 * @see com.montages.mcore.MNamed#getSpecialEName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_SpecialEName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getFullLabel <em>Full Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Full Label</em>'.
	 * @see com.montages.mcore.MNamed#getFullLabel()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_FullLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getLocalStructuralName <em>Local Structural Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Structural Name</em>'.
	 * @see com.montages.mcore.MNamed#getLocalStructuralName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_LocalStructuralName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamed#getCalculatedName <em>Calculated Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Name</em>'.
	 * @see com.montages.mcore.MNamed#getCalculatedName()
	 * @see #getMNamed()
	 * @generated
	 */
	EAttribute getMNamed_CalculatedName();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MNamed#sameName(com.montages.mcore.MNamed) <em>Same Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same Name</em>' operation.
	 * @see com.montages.mcore.MNamed#sameName(com.montages.mcore.MNamed)
	 * @generated
	 */
	EOperation getMNamed__SameName__MNamed();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MNamed#sameShortName(com.montages.mcore.MNamed) <em>Same Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same Short Name</em>' operation.
	 * @see com.montages.mcore.MNamed#sameShortName(com.montages.mcore.MNamed)
	 * @generated
	 */
	EOperation getMNamed__SameShortName__MNamed();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MNamed#sameString(java.lang.String, java.lang.String) <em>Same String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same String</em>' operation.
	 * @see com.montages.mcore.MNamed#sameString(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getMNamed__SameString__String_String();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MNamed#stringEmpty(java.lang.String) <em>String Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>String Empty</em>' operation.
	 * @see com.montages.mcore.MNamed#stringEmpty(java.lang.String)
	 * @generated
	 */
	EOperation getMNamed__StringEmpty__String();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MComponent <em>MComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MComponent</em>'.
	 * @see com.montages.mcore.MComponent
	 * @generated
	 */
	EClass getMComponent();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MComponent#getDoAction()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MComponent#doAction$Update(com.montages.mcore.MComponentAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MComponent#doAction$Update(com.montages.mcore.MComponentAction)
	 * @generated
	 */
	EOperation getMComponent__DoAction$Update__MComponentAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MComponent#doActionUpdate(com.montages.mcore.MComponentAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.MComponent#doActionUpdate(com.montages.mcore.MComponentAction)
	 * @generated
	 */
	EOperation getMComponent__DoActionUpdate__MComponentAction();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MComponent#getOwnedPackage <em>Owned Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Owned Package</em>'.
	 * @see com.montages.mcore.MComponent#getOwnedPackage()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_OwnedPackage();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MComponent#getResourceFolder <em>Resource Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource Folder</em>'.
	 * @see com.montages.mcore.MComponent#getResourceFolder()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_ResourceFolder();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mcore.MComponent#getSemantics <em>Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Semantics</em>'.
	 * @see com.montages.mcore.MComponent#getSemantics()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_Semantics();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MComponent#getPreloadedComponent <em>Preloaded Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Preloaded Component</em>'.
	 * @see com.montages.mcore.MComponent#getPreloadedComponent()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_PreloadedComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MComponent#getGeneratedEPackage <em>Generated EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Generated EPackage</em>'.
	 * @see com.montages.mcore.MComponent#getGeneratedEPackage()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_GeneratedEPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getHideAdvancedProperties <em>Hide Advanced Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hide Advanced Properties</em>'.
	 * @see com.montages.mcore.MComponent#getHideAdvancedProperties()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_HideAdvancedProperties();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getAbstractComponent <em>Abstract Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract Component</em>'.
	 * @see com.montages.mcore.MComponent#getAbstractComponent()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_AbstractComponent();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getCountImplementations <em>Count Implementations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count Implementations</em>'.
	 * @see com.montages.mcore.MComponent#getCountImplementations()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_CountImplementations();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDisableDefaultContainment <em>Disable Default Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Disable Default Containment</em>'.
	 * @see com.montages.mcore.MComponent#getDisableDefaultContainment()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DisableDefaultContainment();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MComponent#getAllGeneralAnnotations <em>All General Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All General Annotations</em>'.
	 * @see com.montages.mcore.MComponent#getAllGeneralAnnotations()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_AllGeneralAnnotations();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MComponent#getNamedEditor <em>Named Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Named Editor</em>'.
	 * @see com.montages.mcore.MComponent#getNamedEditor()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_NamedEditor();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MComponent#getMainEditor <em>Main Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Editor</em>'.
	 * @see com.montages.mcore.MComponent#getMainEditor()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_MainEditor();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Legacy Editorconfig</em>'.
	 * @see com.montages.mcore.MComponent#getUseLegacyEditorconfig()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_UseLegacyEditorconfig();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MComponent#getMainNamedEditor <em>Main Named Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Main Named Editor</em>'.
	 * @see com.montages.mcore.MComponent#getMainNamedEditor()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_MainNamedEditor();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getNamePrefix <em>Name Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Prefix</em>'.
	 * @see com.montages.mcore.MComponent#getNamePrefix()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_NamePrefix();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDerivedNamePrefix <em>Derived Name Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Name Prefix</em>'.
	 * @see com.montages.mcore.MComponent#getDerivedNamePrefix()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DerivedNamePrefix();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getNamePrefixForFeatures <em>Name Prefix For Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Prefix For Features</em>'.
	 * @see com.montages.mcore.MComponent#getNamePrefixForFeatures()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_NamePrefixForFeatures();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDerivedBundleName <em>Derived Bundle Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Bundle Name</em>'.
	 * @see com.montages.mcore.MComponent#getDerivedBundleName()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DerivedBundleName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDerivedURI <em>Derived URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived URI</em>'.
	 * @see com.montages.mcore.MComponent#getDerivedURI()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DerivedURI();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getRepresentsCoreComponent <em>Represents Core Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Represents Core Component</em>'.
	 * @see com.montages.mcore.MComponent#getRepresentsCoreComponent()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_RepresentsCoreComponent();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MComponent#getRootPackage <em>Root Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Package</em>'.
	 * @see com.montages.mcore.MComponent#getRootPackage()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_RootPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Avoid Regeneration Of Editor Configuration</em>'.
	 * @see com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_AvoidRegenerationOfEditorConfiguration();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MComponent#getContainingRepository <em>Containing Repository</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Repository</em>'.
	 * @see com.montages.mcore.MComponent#getContainingRepository()
	 * @see #getMComponent()
	 * @generated
	 */
	EReference getMComponent_ContainingRepository();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDomainType <em>Domain Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Type</em>'.
	 * @see com.montages.mcore.MComponent#getDomainType()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DomainType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDomainName <em>Domain Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Domain Name</em>'.
	 * @see com.montages.mcore.MComponent#getDomainName()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DomainName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDerivedDomainType <em>Derived Domain Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Domain Type</em>'.
	 * @see com.montages.mcore.MComponent#getDerivedDomainType()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DerivedDomainType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getDerivedDomainName <em>Derived Domain Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Domain Name</em>'.
	 * @see com.montages.mcore.MComponent#getDerivedDomainName()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_DerivedDomainName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MComponent#getSpecialBundleName <em>Special Bundle Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special Bundle Name</em>'.
	 * @see com.montages.mcore.MComponent#getSpecialBundleName()
	 * @see #getMComponent()
	 * @generated
	 */
	EAttribute getMComponent_SpecialBundleName();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MModelElement <em>MModel Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MModel Element</em>'.
	 * @see com.montages.mcore.MModelElement
	 * @generated
	 */
	EClass getMModelElement();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MModelElement#getGeneralAnnotation <em>General Annotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>General Annotation</em>'.
	 * @see com.montages.mcore.MModelElement#getGeneralAnnotation()
	 * @see #getMModelElement()
	 * @generated
	 */
	EReference getMModelElement_GeneralAnnotation();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MPackage <em>MPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MPackage</em>'.
	 * @see com.montages.mcore.MPackage
	 * @generated
	 */
	EClass getMPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MPackage#getDoAction()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPackage#doAction$Update(com.montages.mcore.MPackageAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MPackage#doAction$Update(com.montages.mcore.MPackageAction)
	 * @generated
	 */
	EOperation getMPackage__DoAction$Update__MPackageAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPackage#generateJsonSchema$Update(java.lang.Boolean) <em>Generate Json Schema$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Json Schema$ Update</em>' operation.
	 * @see com.montages.mcore.MPackage#generateJsonSchema$Update(java.lang.Boolean)
	 * @generated
	 */
	EOperation getMPackage__GenerateJsonSchema$Update__Boolean();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPackage#doActionUpdate(com.montages.mcore.MPackageAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.MPackage#doActionUpdate(com.montages.mcore.MPackageAction)
	 * @generated
	 */
	EOperation getMPackage__DoActionUpdate__MPackageAction();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MPackage#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Classifier</em>'.
	 * @see com.montages.mcore.MPackage#getClassifier()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_Classifier();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MPackage#getSubPackage <em>Sub Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sub Package</em>'.
	 * @see com.montages.mcore.MPackage#getSubPackage()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_SubPackage();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mcore.MPackage#getPackageAnnotations <em>Package Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Package Annotations</em>'.
	 * @see com.montages.mcore.MPackage#getPackageAnnotations()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_PackageAnnotations();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getNamePrefix <em>Name Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name Prefix</em>'.
	 * @see com.montages.mcore.MPackage#getNamePrefix()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_NamePrefix();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedNamePrefix <em>Derived Name Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Name Prefix</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedNamePrefix()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedNamePrefix();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getNamePrefixScope <em>Name Prefix Scope</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Name Prefix Scope</em>'.
	 * @see com.montages.mcore.MPackage#getNamePrefixScope()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_NamePrefixScope();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedJsonSchema <em>Derived Json Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Json Schema</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedJsonSchema()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedJsonSchema();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getGenerateJsonSchema <em>Generate Json Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generate Json Schema</em>'.
	 * @see com.montages.mcore.MPackage#getGenerateJsonSchema()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_GenerateJsonSchema();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getGeneratedJsonSchema <em>Generated Json Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Generated Json Schema</em>'.
	 * @see com.montages.mcore.MPackage#getGeneratedJsonSchema()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_GeneratedJsonSchema();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPackage#generateJsonSchema$update01Object(java.lang.Boolean) <em>Generate Json Schema$update01 Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Json Schema$update01 Object</em>' operation.
	 * @see com.montages.mcore.MPackage#generateJsonSchema$update01Object(java.lang.Boolean)
	 * @generated
	 */
	EOperation getMPackage__GenerateJsonSchema$update01Object__Boolean();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPackage#generateJsonSchema$update01Value(java.lang.Boolean, com.montages.mcore.MPackage) <em>Generate Json Schema$update01 Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Generate Json Schema$update01 Value</em>' operation.
	 * @see com.montages.mcore.MPackage#generateJsonSchema$update01Value(java.lang.Boolean, com.montages.mcore.MPackage)
	 * @generated
	 */
	EOperation getMPackage__GenerateJsonSchema$update01Value__Boolean_MPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter <em>Use Package Content Only With Explicit Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Package Content Only With Explicit Filter</em>'.
	 * @see com.montages.mcore.MPackage#getUsePackageContentOnlyWithExplicitFilter()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_UsePackageContentOnlyWithExplicitFilter();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getSpecialNsURI <em>Special Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special Ns URI</em>'.
	 * @see com.montages.mcore.MPackage#getSpecialNsURI()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_SpecialNsURI();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getSpecialNsPrefix <em>Special Ns Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special Ns Prefix</em>'.
	 * @see com.montages.mcore.MPackage#getSpecialNsPrefix()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_SpecialNsPrefix();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getSpecialFileName <em>Special File Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Special File Name</em>'.
	 * @see com.montages.mcore.MPackage#getSpecialFileName()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_SpecialFileName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getUseUUID <em>Use UUID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use UUID</em>'.
	 * @see com.montages.mcore.MPackage#getUseUUID()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_UseUUID();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getUuidAttributeName <em>Uuid Attribute Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uuid Attribute Name</em>'.
	 * @see com.montages.mcore.MPackage#getUuidAttributeName()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_UuidAttributeName();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Parent</em>'.
	 * @see com.montages.mcore.MPackage#getParent()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_Parent();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getContainingPackage <em>Containing Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Package</em>'.
	 * @see com.montages.mcore.MPackage#getContainingPackage()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_ContainingPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getContainingComponent <em>Containing Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Component</em>'.
	 * @see com.montages.mcore.MPackage#getContainingComponent()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_ContainingComponent();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getRepresentsCorePackage <em>Represents Core Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Represents Core Package</em>'.
	 * @see com.montages.mcore.MPackage#getRepresentsCorePackage()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_RepresentsCorePackage();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MPackage#getAllSubpackages <em>All Subpackages</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Subpackages</em>'.
	 * @see com.montages.mcore.MPackage#getAllSubpackages()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_AllSubpackages();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getQualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualified Name</em>'.
	 * @see com.montages.mcore.MPackage#getQualifiedName()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_QualifiedName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getIsRootPackage <em>Is Root Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Root Package</em>'.
	 * @see com.montages.mcore.MPackage#getIsRootPackage()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_IsRootPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getInternalEPackage <em>Internal EPackage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EPackage</em>'.
	 * @see com.montages.mcore.MPackage#getInternalEPackage()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_InternalEPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedNsURI <em>Derived Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Ns URI</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedNsURI()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedNsURI();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedStandardNsURI <em>Derived Standard Ns URI</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Standard Ns URI</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedStandardNsURI()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedStandardNsURI();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedNsPrefix <em>Derived Ns Prefix</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Ns Prefix</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedNsPrefix()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedNsPrefix();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPackage#getDerivedJavaPackageName <em>Derived Java Package Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Java Package Name</em>'.
	 * @see com.montages.mcore.MPackage#getDerivedJavaPackageName()
	 * @see #getMPackage()
	 * @generated
	 */
	EAttribute getMPackage_DerivedJavaPackageName();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPackage#getResourceRootClass <em>Resource Root Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Root Class</em>'.
	 * @see com.montages.mcore.MPackage#getResourceRootClass()
	 * @see #getMPackage()
	 * @generated
	 */
	EReference getMPackage_ResourceRootClass();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MHasSimpleType <em>MHas Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MHas Simple Type</em>'.
	 * @see com.montages.mcore.MHasSimpleType
	 * @generated
	 */
	EClass getMHasSimpleType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MHasSimpleType#getSimpleTypeString <em>Simple Type String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simple Type String</em>'.
	 * @see com.montages.mcore.MHasSimpleType#getSimpleTypeString()
	 * @see #getMHasSimpleType()
	 * @generated
	 */
	EAttribute getMHasSimpleType_SimpleTypeString();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MHasSimpleType#getSimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simple Type</em>'.
	 * @see com.montages.mcore.MHasSimpleType#getSimpleType()
	 * @see #getMHasSimpleType()
	 * @generated
	 */
	EAttribute getMHasSimpleType_SimpleType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MHasSimpleType#getHasSimpleDataType <em>Has Simple Data Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Simple Data Type</em>'.
	 * @see com.montages.mcore.MHasSimpleType#getHasSimpleDataType()
	 * @see #getMHasSimpleType()
	 * @generated
	 */
	EAttribute getMHasSimpleType_HasSimpleDataType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MHasSimpleType#getHasSimpleModelingType <em>Has Simple Modeling Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Simple Modeling Type</em>'.
	 * @see com.montages.mcore.MHasSimpleType#getHasSimpleModelingType()
	 * @see #getMHasSimpleType()
	 * @generated
	 */
	EAttribute getMHasSimpleType_HasSimpleModelingType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MHasSimpleType#getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simple Type Is Correct</em>'.
	 * @see com.montages.mcore.MHasSimpleType#getSimpleTypeIsCorrect()
	 * @see #getMHasSimpleType()
	 * @generated
	 */
	EAttribute getMHasSimpleType_SimpleTypeIsCorrect();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MHasSimpleType#simpleTypeAsString(com.montages.mcore.SimpleType) <em>Simple Type As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Simple Type As String</em>' operation.
	 * @see com.montages.mcore.MHasSimpleType#simpleTypeAsString(com.montages.mcore.SimpleType)
	 * @generated
	 */
	EOperation getMHasSimpleType__SimpleTypeAsString__SimpleType();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MClassifier <em>MClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MClassifier</em>'.
	 * @see com.montages.mcore.MClassifier
	 * @generated
	 */
	EClass getMClassifier();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MClassifier#getDoAction()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_DoAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getResolveContainerAction <em>Resolve Container Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resolve Container Action</em>'.
	 * @see com.montages.mcore.MClassifier#getResolveContainerAction()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_ResolveContainerAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getAbstractDoAction <em>Abstract Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract Do Action</em>'.
	 * @see com.montages.mcore.MClassifier#getAbstractDoAction()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_AbstractDoAction();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getObjectReferences <em>Object References</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Object References</em>'.
	 * @see com.montages.mcore.MClassifier#getObjectReferences()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ObjectReferences();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getNearestInstance <em>Nearest Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Nearest Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getNearestInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_NearestInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getStrictNearestInstance <em>Strict Nearest Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Strict Nearest Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getStrictNearestInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_StrictNearestInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getStrictInstance <em>Strict Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Strict Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getStrictInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_StrictInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIntelligentNearestInstance <em>Intelligent Nearest Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intelligent Nearest Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getIntelligentNearestInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IntelligentNearestInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getObjectUnreferenced <em>Object Unreferenced</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Object Unreferenced</em>'.
	 * @see com.montages.mcore.MClassifier#getObjectUnreferenced()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ObjectUnreferenced();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getOutstandingToCopyObjects <em>Outstanding To Copy Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Outstanding To Copy Objects</em>'.
	 * @see com.montages.mcore.MClassifier#getOutstandingToCopyObjects()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_OutstandingToCopyObjects();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getPropertyInstances <em>Property Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Property Instances</em>'.
	 * @see com.montages.mcore.MClassifier#getPropertyInstances()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_PropertyInstances();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getObjectReferencePropertyInstances <em>Object Reference Property Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Object Reference Property Instances</em>'.
	 * @see com.montages.mcore.MClassifier#getObjectReferencePropertyInstances()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ObjectReferencePropertyInstances();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIntelligentInstance <em>Intelligent Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intelligent Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getIntelligentInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IntelligentInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getContainmentHierarchy <em>Containment Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Containment Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getContainmentHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ContainmentHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getStrictAllClassesContainedIn <em>Strict All Classes Contained In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Strict All Classes Contained In</em>'.
	 * @see com.montages.mcore.MClassifier#getStrictAllClassesContainedIn()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_StrictAllClassesContainedIn();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getStrictContainmentHierarchy <em>Strict Containment Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Strict Containment Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getStrictContainmentHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_StrictContainmentHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIntelligentAllClassesContainedIn <em>Intelligent All Classes Contained In</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intelligent All Classes Contained In</em>'.
	 * @see com.montages.mcore.MClassifier#getIntelligentAllClassesContainedIn()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IntelligentAllClassesContainedIn();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIntelligentContainmentHierarchy <em>Intelligent Containment Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intelligent Containment Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getIntelligentContainmentHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IntelligentContainmentHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getInstantiationHierarchy <em>Instantiation Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instantiation Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getInstantiationHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_InstantiationHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getInstantiationPropertyHierarchy <em>Instantiation Property Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instantiation Property Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getInstantiationPropertyHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_InstantiationPropertyHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIntelligentInstantiationPropertyHierarchy <em>Intelligent Instantiation Property Hierarchy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Intelligent Instantiation Property Hierarchy</em>'.
	 * @see com.montages.mcore.MClassifier#getIntelligentInstantiationPropertyHierarchy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IntelligentInstantiationPropertyHierarchy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getClassescontainedin <em>Classescontainedin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Classescontainedin</em>'.
	 * @see com.montages.mcore.MClassifier#getClassescontainedin()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_Classescontainedin();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getCanApplyDefaultContainment <em>Can Apply Default Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Can Apply Default Containment</em>'.
	 * @see com.montages.mcore.MClassifier#getCanApplyDefaultContainment()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_CanApplyDefaultContainment();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getPropertiesAsText <em>Properties As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Properties As Text</em>'.
	 * @see com.montages.mcore.MClassifier#getPropertiesAsText()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_PropertiesAsText();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getSemanticsAsText <em>Semantics As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Semantics As Text</em>'.
	 * @see com.montages.mcore.MClassifier#getSemanticsAsText()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_SemanticsAsText();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getDerivedJsonSchema <em>Derived Json Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Json Schema</em>'.
	 * @see com.montages.mcore.MClassifier#getDerivedJsonSchema()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_DerivedJsonSchema();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#toggleSuperType$Update(com.montages.mcore.MClassifier) <em>Toggle Super Type$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Toggle Super Type$ Update</em>' operation.
	 * @see com.montages.mcore.MClassifier#toggleSuperType$Update(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMClassifier__ToggleSuperType$Update__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#orderingStrategy$Update(com.montages.mcore.OrderingStrategy) <em>Ordering Strategy$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ordering Strategy$ Update</em>' operation.
	 * @see com.montages.mcore.MClassifier#orderingStrategy$Update(com.montages.mcore.OrderingStrategy)
	 * @generated
	 */
	EOperation getMClassifier__OrderingStrategy$Update__OrderingStrategy();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#doAction$Update(com.montages.mcore.MClassifierAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MClassifier#doAction$Update(com.montages.mcore.MClassifierAction)
	 * @generated
	 */
	EOperation getMClassifier__DoAction$Update__MClassifierAction();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MClassifier#getSuperTypePackage <em>Super Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Super Type Package</em>'.
	 * @see com.montages.mcore.MClassifier#getSuperTypePackage()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_SuperTypePackage();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getSuperType <em>Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Super Type</em>'.
	 * @see com.montages.mcore.MClassifier#getSuperType()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_SuperType();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Instance</em>'.
	 * @see com.montages.mcore.MClassifier#getInstance()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_Instance();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see com.montages.mcore.MClassifier#getKind()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_Kind();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getEnforcedClass <em>Enforced Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Enforced Class</em>'.
	 * @see com.montages.mcore.MClassifier#getEnforcedClass()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_EnforcedClass();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getAbstractClass <em>Abstract Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Abstract Class</em>'.
	 * @see com.montages.mcore.MClassifier#getAbstractClass()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_AbstractClass();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MClassifier#getInternalEClassifier <em>Internal EClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EClassifier</em>'.
	 * @see com.montages.mcore.MClassifier#getInternalEClassifier()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_InternalEClassifier();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MClassifier#getContainingPackage <em>Containing Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Package</em>'.
	 * @see com.montages.mcore.MClassifier#getContainingPackage()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ContainingPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getRepresentsCoreType <em>Represents Core Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Represents Core Type</em>'.
	 * @see com.montages.mcore.MClassifier#getRepresentsCoreType()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_RepresentsCoreType();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getAllPropertyGroups <em>All Property Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Property Groups</em>'.
	 * @see com.montages.mcore.MClassifier#getAllPropertyGroups()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_AllPropertyGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MClassifier#getLiteral <em>Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Literal</em>'.
	 * @see com.montages.mcore.MClassifier#getLiteral()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_Literal();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getEInterface <em>EInterface</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EInterface</em>'.
	 * @see com.montages.mcore.MClassifier#getEInterface()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_EInterface();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getOperationAsIdPropertyError <em>Operation As Id Property Error</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operation As Id Property Error</em>'.
	 * @see com.montages.mcore.MClassifier#getOperationAsIdPropertyError()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_OperationAsIdPropertyError();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getIsClassByStructure <em>Is Class By Structure</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Class By Structure</em>'.
	 * @see com.montages.mcore.MClassifier#getIsClassByStructure()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_IsClassByStructure();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MClassifier#getToggleSuperType <em>Toggle Super Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Toggle Super Type</em>'.
	 * @see com.montages.mcore.MClassifier#getToggleSuperType()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_ToggleSuperType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MClassifier#getOrderingStrategy <em>Ordering Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ordering Strategy</em>'.
	 * @see com.montages.mcore.MClassifier#getOrderingStrategy()
	 * @see #getMClassifier()
	 * @generated
	 */
	EAttribute getMClassifier_OrderingStrategy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getTestAllProperties <em>Test All Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Test All Properties</em>'.
	 * @see com.montages.mcore.MClassifier#getTestAllProperties()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_TestAllProperties();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MClassifier#getIdProperty <em>Id Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Id Property</em>'.
	 * @see com.montages.mcore.MClassifier#getIdProperty()
	 * @see #getMClassifier()
	 * @generated
	 */
	EReference getMClassifier_IdProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#defaultPropertyValue() <em>Default Property Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Default Property Value</em>' operation.
	 * @see com.montages.mcore.MClassifier#defaultPropertyValue()
	 * @generated
	 */
	EOperation getMClassifier__DefaultPropertyValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#defaultPropertyDescription() <em>Default Property Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Default Property Description</em>' operation.
	 * @see com.montages.mcore.MClassifier#defaultPropertyDescription()
	 * @generated
	 */
	EOperation getMClassifier__DefaultPropertyDescription();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allLiterals() <em>All Literals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Literals</em>' operation.
	 * @see com.montages.mcore.MClassifier#allLiterals()
	 * @generated
	 */
	EOperation getMClassifier__AllLiterals();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#selectableClassifier(org.eclipse.emf.common.util.EList, com.montages.mcore.MPackage) <em>Selectable Classifier</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Selectable Classifier</em>' operation.
	 * @see com.montages.mcore.MClassifier#selectableClassifier(org.eclipse.emf.common.util.EList, com.montages.mcore.MPackage)
	 * @generated
	 */
	EOperation getMClassifier__SelectableClassifier__EList_MPackage();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allDirectSubTypes() <em>All Direct Sub Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Direct Sub Types</em>' operation.
	 * @see com.montages.mcore.MClassifier#allDirectSubTypes()
	 * @generated
	 */
	EOperation getMClassifier__AllDirectSubTypes();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allSubTypes() <em>All Sub Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Sub Types</em>' operation.
	 * @see com.montages.mcore.MClassifier#allSubTypes()
	 * @generated
	 */
	EOperation getMClassifier__AllSubTypes();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#superTypesAsString() <em>Super Types As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Super Types As String</em>' operation.
	 * @see com.montages.mcore.MClassifier#superTypesAsString()
	 * @generated
	 */
	EOperation getMClassifier__SuperTypesAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allSuperTypes() <em>All Super Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Super Types</em>' operation.
	 * @see com.montages.mcore.MClassifier#allSuperTypes()
	 * @generated
	 */
	EOperation getMClassifier__AllSuperTypes();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allProperties() <em>All Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Properties</em>' operation.
	 * @see com.montages.mcore.MClassifier#allProperties()
	 * @generated
	 */
	EOperation getMClassifier__AllProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allSuperProperties() <em>All Super Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Super Properties</em>' operation.
	 * @see com.montages.mcore.MClassifier#allSuperProperties()
	 * @generated
	 */
	EOperation getMClassifier__AllSuperProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allFeatures() <em>All Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Features</em>' operation.
	 * @see com.montages.mcore.MClassifier#allFeatures()
	 * @generated
	 */
	EOperation getMClassifier__AllFeatures();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allNonContainmentFeatures() <em>All Non Containment Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Non Containment Features</em>' operation.
	 * @see com.montages.mcore.MClassifier#allNonContainmentFeatures()
	 * @generated
	 */
	EOperation getMClassifier__AllNonContainmentFeatures();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allStoredNonContainmentFeatures() <em>All Stored Non Containment Features</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Stored Non Containment Features</em>' operation.
	 * @see com.montages.mcore.MClassifier#allStoredNonContainmentFeatures()
	 * @generated
	 */
	EOperation getMClassifier__AllStoredNonContainmentFeatures();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allFeaturesWithStorage() <em>All Features With Storage</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Features With Storage</em>' operation.
	 * @see com.montages.mcore.MClassifier#allFeaturesWithStorage()
	 * @generated
	 */
	EOperation getMClassifier__AllFeaturesWithStorage();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allContainmentReferences() <em>All Containment References</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Containment References</em>' operation.
	 * @see com.montages.mcore.MClassifier#allContainmentReferences()
	 * @generated
	 */
	EOperation getMClassifier__AllContainmentReferences();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allOperationSignatures() <em>All Operation Signatures</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Operation Signatures</em>' operation.
	 * @see com.montages.mcore.MClassifier#allOperationSignatures()
	 * @generated
	 */
	EOperation getMClassifier__AllOperationSignatures();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allClassesContainedIn() <em>All Classes Contained In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Classes Contained In</em>' operation.
	 * @see com.montages.mcore.MClassifier#allClassesContainedIn()
	 * @generated
	 */
	EOperation getMClassifier__AllClassesContainedIn();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allIdProperties() <em>All Id Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Id Properties</em>' operation.
	 * @see com.montages.mcore.MClassifier#allIdProperties()
	 * @generated
	 */
	EOperation getMClassifier__AllIdProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#allSuperIdProperties() <em>All Super Id Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Super Id Properties</em>' operation.
	 * @see com.montages.mcore.MClassifier#allSuperIdProperties()
	 * @generated
	 */
	EOperation getMClassifier__AllSuperIdProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#doActionUpdate(com.montages.mcore.MClassifierAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.MClassifier#doActionUpdate(com.montages.mcore.MClassifierAction)
	 * @generated
	 */
	EOperation getMClassifier__DoActionUpdate__MClassifierAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#invokeSetDoAction(com.montages.mcore.MClassifierAction) <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Do Action</em>' operation.
	 * @see com.montages.mcore.MClassifier#invokeSetDoAction(com.montages.mcore.MClassifierAction)
	 * @generated
	 */
	EOperation getMClassifier__InvokeSetDoAction__MClassifierAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#invokeToggleSuperType(com.montages.mcore.MClassifier) <em>Invoke Toggle Super Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Toggle Super Type</em>' operation.
	 * @see com.montages.mcore.MClassifier#invokeToggleSuperType(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMClassifier__InvokeToggleSuperType__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#doSuperTypeUpdate(com.montages.mcore.MClassifier) <em>Do Super Type Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Super Type Update</em>' operation.
	 * @see com.montages.mcore.MClassifier#doSuperTypeUpdate(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMClassifier__DoSuperTypeUpdate__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MClassifier#ambiguousClassifierName() <em>Ambiguous Classifier Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ambiguous Classifier Name</em>' operation.
	 * @see com.montages.mcore.MClassifier#ambiguousClassifierName()
	 * @generated
	 */
	EOperation getMClassifier__AmbiguousClassifierName();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MLiteral <em>MLiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MLiteral</em>'.
	 * @see com.montages.mcore.MLiteral
	 * @generated
	 */
	EClass getMLiteral();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MLiteral#getConstantLabel <em>Constant Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Constant Label</em>'.
	 * @see com.montages.mcore.MLiteral#getConstantLabel()
	 * @see #getMLiteral()
	 * @generated
	 */
	EAttribute getMLiteral_ConstantLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MLiteral#getAsString <em>As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>As String</em>'.
	 * @see com.montages.mcore.MLiteral#getAsString()
	 * @see #getMLiteral()
	 * @generated
	 */
	EAttribute getMLiteral_AsString();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MLiteral#getQualifiedName <em>Qualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Qualified Name</em>'.
	 * @see com.montages.mcore.MLiteral#getQualifiedName()
	 * @see #getMLiteral()
	 * @generated
	 */
	EAttribute getMLiteral_QualifiedName();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MLiteral#getContainingEnumeration <em>Containing Enumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Enumeration</em>'.
	 * @see com.montages.mcore.MLiteral#getContainingEnumeration()
	 * @see #getMLiteral()
	 * @generated
	 */
	EReference getMLiteral_ContainingEnumeration();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MLiteral#getInternalEEnumLiteral <em>Internal EEnum Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EEnum Literal</em>'.
	 * @see com.montages.mcore.MLiteral#getInternalEEnumLiteral()
	 * @see #getMLiteral()
	 * @generated
	 */
	EReference getMLiteral_InternalEEnumLiteral();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MPropertiesContainer <em>MProperties Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MProperties Container</em>'.
	 * @see com.montages.mcore.MPropertiesContainer
	 * @generated
	 */
	EClass getMPropertiesContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MPropertiesContainer#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getProperty()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_Property();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mcore.MPropertiesContainer#getAnnotations <em>Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Annotations</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getAnnotations()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_Annotations();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MPropertiesContainer#getPropertyGroup <em>Property Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Group</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getPropertyGroup()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_PropertyGroup();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MPropertiesContainer#getAllConstraintAnnotations <em>All Constraint Annotations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Constraint Annotations</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getAllConstraintAnnotations()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_AllConstraintAnnotations();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MPropertiesContainer#getContainingClassifier <em>Containing Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Classifier</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getContainingClassifier()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_ContainingClassifier();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MPropertiesContainer#getOwnedProperty <em>Owned Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Owned Property</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getOwnedProperty()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EReference getMPropertiesContainer_OwnedProperty();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPropertiesContainer#getNrOfPropertiesAndSignatures <em>Nr Of Properties And Signatures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nr Of Properties And Signatures</em>'.
	 * @see com.montages.mcore.MPropertiesContainer#getNrOfPropertiesAndSignatures()
	 * @see #getMPropertiesContainer()
	 * @generated
	 */
	EAttribute getMPropertiesContainer_NrOfPropertiesAndSignatures();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPropertiesContainer#allPropertyAnnotations() <em>All Property Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Property Annotations</em>' operation.
	 * @see com.montages.mcore.MPropertiesContainer#allPropertyAnnotations()
	 * @generated
	 */
	EOperation getMPropertiesContainer__AllPropertyAnnotations();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPropertiesContainer#allOperationAnnotations() <em>All Operation Annotations</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>All Operation Annotations</em>' operation.
	 * @see com.montages.mcore.MPropertiesContainer#allOperationAnnotations()
	 * @generated
	 */
	EOperation getMPropertiesContainer__AllOperationAnnotations();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MPropertiesGroup <em>MProperties Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MProperties Group</em>'.
	 * @see com.montages.mcore.MPropertiesGroup
	 * @generated
	 */
	EClass getMPropertiesGroup();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPropertiesGroup#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MPropertiesGroup#getDoAction()
	 * @see #getMPropertiesGroup()
	 * @generated
	 */
	EAttribute getMPropertiesGroup_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPropertiesGroup#doAction$Update(com.montages.mcore.MPropertiesGroupAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MPropertiesGroup#doAction$Update(com.montages.mcore.MPropertiesGroupAction)
	 * @generated
	 */
	EOperation getMPropertiesGroup__DoAction$Update__MPropertiesGroupAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPropertiesGroup#doActionUpdate(com.montages.mcore.MPropertiesGroupAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.MPropertiesGroup#doActionUpdate(com.montages.mcore.MPropertiesGroupAction)
	 * @generated
	 */
	EOperation getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MPropertiesGroup#invokeSetDoAction(com.montages.mcore.MPropertiesGroupAction) <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Do Action</em>' operation.
	 * @see com.montages.mcore.MPropertiesGroup#invokeSetDoAction(com.montages.mcore.MPropertiesGroupAction)
	 * @generated
	 */
	EOperation getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MPropertiesGroup#getOrder <em>Order</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order</em>'.
	 * @see com.montages.mcore.MPropertiesGroup#getOrder()
	 * @see #getMPropertiesGroup()
	 * @generated
	 */
	EAttribute getMPropertiesGroup_Order();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MProperty <em>MProperty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MProperty</em>'.
	 * @see com.montages.mcore.MProperty
	 * @generated
	 */
	EClass getMProperty();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MProperty#getDoAction()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_DoAction();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getBehaviorActions <em>Behavior Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Behavior Actions</em>'.
	 * @see com.montages.mcore.MProperty#getBehaviorActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_BehaviorActions();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getLayoutActions <em>Layout Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Layout Actions</em>'.
	 * @see com.montages.mcore.MProperty#getLayoutActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_LayoutActions();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getArityActions <em>Arity Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Arity Actions</em>'.
	 * @see com.montages.mcore.MProperty#getArityActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_ArityActions();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getSimpleTypeActions <em>Simple Type Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Simple Type Actions</em>'.
	 * @see com.montages.mcore.MProperty#getSimpleTypeActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_SimpleTypeActions();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getAnnotationActions <em>Annotation Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Annotation Actions</em>'.
	 * @see com.montages.mcore.MProperty#getAnnotationActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_AnnotationActions();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.MProperty#getMoveActions <em>Move Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Move Actions</em>'.
	 * @see com.montages.mcore.MProperty#getMoveActions()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_MoveActions();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getDerivedJsonSchema <em>Derived Json Schema</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Json Schema</em>'.
	 * @see com.montages.mcore.MProperty#getDerivedJsonSchema()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_DerivedJsonSchema();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#oppositeDefinition$Update(com.montages.mcore.MProperty) <em>Opposite Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Opposite Definition$ Update</em>' operation.
	 * @see com.montages.mcore.MProperty#oppositeDefinition$Update(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMProperty__OppositeDefinition$Update__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#propertyBehavior$Update(com.montages.mcore.PropertyBehavior) <em>Property Behavior$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Behavior$ Update</em>' operation.
	 * @see com.montages.mcore.MProperty#propertyBehavior$Update(com.montages.mcore.PropertyBehavior)
	 * @generated
	 */
	EOperation getMProperty__PropertyBehavior$Update__PropertyBehavior();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#typeDefinition$Update(com.montages.mcore.MClassifier) <em>Type Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Type Definition$ Update</em>' operation.
	 * @see com.montages.mcore.MProperty#typeDefinition$Update(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMProperty__TypeDefinition$Update__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#doAction$Update(com.montages.mcore.MPropertyAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MProperty#doAction$Update(com.montages.mcore.MPropertyAction)
	 * @generated
	 */
	EOperation getMProperty__DoAction$Update__MPropertyAction();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MProperty#getOperationSignature <em>Operation Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Operation Signature</em>'.
	 * @see com.montages.mcore.MProperty#getOperationSignature()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_OperationSignature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getOppositeDefinition <em>Opposite Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite Definition</em>'.
	 * @see com.montages.mcore.MProperty#getOppositeDefinition()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_OppositeDefinition();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getOpposite <em>Opposite</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Opposite</em>'.
	 * @see com.montages.mcore.MProperty#getOpposite()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_Opposite();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getIsOperation <em>Is Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Operation</em>'.
	 * @see com.montages.mcore.MProperty#getIsOperation()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_IsOperation();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getIsAttribute <em>Is Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Attribute</em>'.
	 * @see com.montages.mcore.MProperty#getIsAttribute()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_IsAttribute();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getIsReference <em>Is Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Reference</em>'.
	 * @see com.montages.mcore.MProperty#getIsReference()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_IsReference();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getPropertyBehavior <em>Property Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Behavior</em>'.
	 * @see com.montages.mcore.MProperty#getPropertyBehavior()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_PropertyBehavior();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getKind <em>Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Kind</em>'.
	 * @see com.montages.mcore.MProperty#getKind()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_Kind();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getContainment <em>Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Containment</em>'.
	 * @see com.montages.mcore.MProperty#getContainment()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_Containment();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getHasStorage <em>Has Storage</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Storage</em>'.
	 * @see com.montages.mcore.MProperty#getHasStorage()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_HasStorage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getChangeable <em>Changeable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Changeable</em>'.
	 * @see com.montages.mcore.MProperty#getChangeable()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_Changeable();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getPersisted <em>Persisted</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Persisted</em>'.
	 * @see com.montages.mcore.MProperty#getPersisted()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_Persisted();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getTypeDefinition <em>Type Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Definition</em>'.
	 * @see com.montages.mcore.MProperty#getTypeDefinition()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_TypeDefinition();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getUseCoreTypes <em>Use Core Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Use Core Types</em>'.
	 * @see com.montages.mcore.MProperty#getUseCoreTypes()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_UseCoreTypes();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getELabel <em>ELabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ELabel</em>'.
	 * @see com.montages.mcore.MProperty#getELabel()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_ELabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getENameForELabel <em>EName For ELabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EName For ELabel</em>'.
	 * @see com.montages.mcore.MProperty#getENameForELabel()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_ENameForELabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getOrderWithinGroup <em>Order Within Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Order Within Group</em>'.
	 * @see com.montages.mcore.MProperty#getOrderWithinGroup()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_OrderWithinGroup();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.montages.mcore.MProperty#getId()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_Id();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EStructural Feature</em>'.
	 * @see com.montages.mcore.MProperty#getInternalEStructuralFeature()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_InternalEStructuralFeature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getDirectSemantics <em>Direct Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Semantics</em>'.
	 * @see com.montages.mcore.MProperty#getDirectSemantics()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_DirectSemantics();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MProperty#getSemanticsSpecialization <em>Semantics Specialization</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Semantics Specialization</em>'.
	 * @see com.montages.mcore.MProperty#getSemanticsSpecialization()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_SemanticsSpecialization();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MProperty#getAllSignatures <em>All Signatures</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Signatures</em>'.
	 * @see com.montages.mcore.MProperty#getAllSignatures()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_AllSignatures();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getDirectOperationSemantics <em>Direct Operation Semantics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Direct Operation Semantics</em>'.
	 * @see com.montages.mcore.MProperty#getDirectOperationSemantics()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_DirectOperationSemantics();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getContainingClassifier <em>Containing Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Classifier</em>'.
	 * @see com.montages.mcore.MProperty#getContainingClassifier()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_ContainingClassifier();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MProperty#getContainingPropertiesContainer <em>Containing Properties Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Properties Container</em>'.
	 * @see com.montages.mcore.MProperty#getContainingPropertiesContainer()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_ContainingPropertiesContainer();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.MProperty#getEKeyForPath <em>EKey For Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>EKey For Path</em>'.
	 * @see com.montages.mcore.MProperty#getEKeyForPath()
	 * @see #getMProperty()
	 * @generated
	 */
	EReference getMProperty_EKeyForPath();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getNotUnsettable <em>Not Unsettable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Not Unsettable</em>'.
	 * @see com.montages.mcore.MProperty#getNotUnsettable()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_NotUnsettable();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getAttributeForEId <em>Attribute For EId</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Attribute For EId</em>'.
	 * @see com.montages.mcore.MProperty#getAttributeForEId()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_AttributeForEId();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MProperty#getEDefaultValueLiteral <em>EDefault Value Literal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EDefault Value Literal</em>'.
	 * @see com.montages.mcore.MProperty#getEDefaultValueLiteral()
	 * @see #getMProperty()
	 * @generated
	 */
	EAttribute getMProperty_EDefaultValueLiteral();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#selectableType(com.montages.mcore.MClassifier) <em>Selectable Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Selectable Type</em>' operation.
	 * @see com.montages.mcore.MProperty#selectableType(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMProperty__SelectableType__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#ambiguousPropertyName() <em>Ambiguous Property Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ambiguous Property Name</em>' operation.
	 * @see com.montages.mcore.MProperty#ambiguousPropertyName()
	 * @generated
	 */
	EOperation getMProperty__AmbiguousPropertyName();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#ambiguousPropertyShortName() <em>Ambiguous Property Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ambiguous Property Short Name</em>' operation.
	 * @see com.montages.mcore.MProperty#ambiguousPropertyShortName()
	 * @generated
	 */
	EOperation getMProperty__AmbiguousPropertyShortName();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#doActionUpdate(com.montages.mcore.MPropertyAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.MProperty#doActionUpdate(com.montages.mcore.MPropertyAction)
	 * @generated
	 */
	EOperation getMProperty__DoActionUpdate__MPropertyAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#invokeSetPropertyBehaviour(com.montages.mcore.PropertyBehavior) <em>Invoke Set Property Behaviour</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Property Behaviour</em>' operation.
	 * @see com.montages.mcore.MProperty#invokeSetPropertyBehaviour(com.montages.mcore.PropertyBehavior)
	 * @generated
	 */
	EOperation getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MProperty#invokeSetDoAction(com.montages.mcore.MPropertyAction) <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Do Action</em>' operation.
	 * @see com.montages.mcore.MProperty#invokeSetDoAction(com.montages.mcore.MPropertyAction)
	 * @generated
	 */
	EOperation getMProperty__InvokeSetDoAction__MPropertyAction();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MOperationSignature <em>MOperation Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MOperation Signature</em>'.
	 * @see com.montages.mcore.MOperationSignature
	 * @generated
	 */
	EClass getMOperationSignature();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MOperationSignature#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MOperationSignature#getDoAction()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EAttribute getMOperationSignature_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MOperationSignature#doAction$Update(com.montages.mcore.MOperationSignatureAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MOperationSignature#doAction$Update(com.montages.mcore.MOperationSignatureAction)
	 * @generated
	 */
	EOperation getMOperationSignature__DoAction$Update__MOperationSignatureAction();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.MOperationSignature#getParameter <em>Parameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Parameter</em>'.
	 * @see com.montages.mcore.MOperationSignature#getParameter()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EReference getMOperationSignature_Parameter();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MOperationSignature#getOperation <em>Operation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Operation</em>'.
	 * @see com.montages.mcore.MOperationSignature#getOperation()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EReference getMOperationSignature_Operation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MOperationSignature#getContainingClassifier <em>Containing Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Classifier</em>'.
	 * @see com.montages.mcore.MOperationSignature#getContainingClassifier()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EReference getMOperationSignature_ContainingClassifier();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MOperationSignature#getELabel <em>ELabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ELabel</em>'.
	 * @see com.montages.mcore.MOperationSignature#getELabel()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EAttribute getMOperationSignature_ELabel();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MOperationSignature#getInternalEOperation <em>Internal EOperation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EOperation</em>'.
	 * @see com.montages.mcore.MOperationSignature#getInternalEOperation()
	 * @see #getMOperationSignature()
	 * @generated
	 */
	EReference getMOperationSignature_InternalEOperation();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MOperationSignature#sameSignature(com.montages.mcore.MOperationSignature) <em>Same Signature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Same Signature</em>' operation.
	 * @see com.montages.mcore.MOperationSignature#sameSignature(com.montages.mcore.MOperationSignature)
	 * @generated
	 */
	EOperation getMOperationSignature__SameSignature__MOperationSignature();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MOperationSignature#signatureAsString() <em>Signature As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Signature As String</em>' operation.
	 * @see com.montages.mcore.MOperationSignature#signatureAsString()
	 * @generated
	 */
	EOperation getMOperationSignature__SignatureAsString();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MTyped <em>MTyped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MTyped</em>'.
	 * @see com.montages.mcore.MTyped
	 * @generated
	 */
	EClass getMTyped();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getMultiplicityAsString <em>Multiplicity As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiplicity As String</em>'.
	 * @see com.montages.mcore.MTyped#getMultiplicityAsString()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_MultiplicityAsString();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MTyped#getCalculatedType <em>Calculated Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Calculated Type</em>'.
	 * @see com.montages.mcore.MTyped#getCalculatedType()
	 * @see #getMTyped()
	 * @generated
	 */
	EReference getMTyped_CalculatedType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getCalculatedMandatory <em>Calculated Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Mandatory</em>'.
	 * @see com.montages.mcore.MTyped#getCalculatedMandatory()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_CalculatedMandatory();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getCalculatedSingular <em>Calculated Singular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Singular</em>'.
	 * @see com.montages.mcore.MTyped#getCalculatedSingular()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_CalculatedSingular();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MTyped#getCalculatedTypePackage <em>Calculated Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Calculated Type Package</em>'.
	 * @see com.montages.mcore.MTyped#getCalculatedTypePackage()
	 * @see #getMTyped()
	 * @generated
	 */
	EReference getMTyped_CalculatedTypePackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getVoidTypeAllowed <em>Void Type Allowed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Void Type Allowed</em>'.
	 * @see com.montages.mcore.MTyped#getVoidTypeAllowed()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_VoidTypeAllowed();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getCalculatedSimpleType <em>Calculated Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Simple Type</em>'.
	 * @see com.montages.mcore.MTyped#getCalculatedSimpleType()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_CalculatedSimpleType();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MTyped#getMultiplicityCase <em>Multiplicity Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Multiplicity Case</em>'.
	 * @see com.montages.mcore.MTyped#getMultiplicityCase()
	 * @see #getMTyped()
	 * @generated
	 */
	EAttribute getMTyped_MultiplicityCase();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MTyped#multiplicityCase$Update(com.montages.mcore.MultiplicityCase) <em>Multiplicity Case$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Multiplicity Case$ Update</em>' operation.
	 * @see com.montages.mcore.MTyped#multiplicityCase$Update(com.montages.mcore.MultiplicityCase)
	 * @generated
	 */
	EOperation getMTyped__MultiplicityCase$Update__MultiplicityCase();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MTyped#typeAsOcl(com.montages.mcore.MPackage, com.montages.mcore.MClassifier, com.montages.mcore.SimpleType, java.lang.Boolean) <em>Type As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Type As Ocl</em>' operation.
	 * @see com.montages.mcore.MTyped#typeAsOcl(com.montages.mcore.MPackage, com.montages.mcore.MClassifier, com.montages.mcore.SimpleType, java.lang.Boolean)
	 * @generated
	 */
	EOperation getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MExplicitlyTyped <em>MExplicitly Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MExplicitly Typed</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped
	 * @generated
	 */
	EClass getMExplicitlyTyped();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MExplicitlyTyped#getTypePackage <em>Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Package</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getTypePackage()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EReference getMExplicitlyTyped_TypePackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MExplicitlyTyped#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getType()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EReference getMExplicitlyTyped_Type();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTyped#getMandatory <em>Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Mandatory</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getMandatory()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EAttribute getMExplicitlyTyped_Mandatory();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTyped#getSingular <em>Singular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Singular</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getSingular()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EAttribute getMExplicitlyTyped_Singular();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTyped#getETypeName <em>EType Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EType Name</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getETypeName()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EAttribute getMExplicitlyTyped_ETypeName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTyped#getETypeLabel <em>EType Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>EType Label</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getETypeLabel()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EAttribute getMExplicitlyTyped_ETypeLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTyped#getCorrectlyTyped <em>Correctly Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correctly Typed</em>'.
	 * @see com.montages.mcore.MExplicitlyTyped#getCorrectlyTyped()
	 * @see #getMExplicitlyTyped()
	 * @generated
	 */
	EAttribute getMExplicitlyTyped_CorrectlyTyped();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MVariable <em>MVariable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MVariable</em>'.
	 * @see com.montages.mcore.MVariable
	 * @generated
	 */
	EClass getMVariable();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MExplicitlyTypedAndNamed <em>MExplicitly Typed And Named</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MExplicitly Typed And Named</em>'.
	 * @see com.montages.mcore.MExplicitlyTypedAndNamed
	 * @generated
	 */
	EClass getMExplicitlyTypedAndNamed();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName <em>Append Type Name To Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Append Type Name To Name</em>'.
	 * @see com.montages.mcore.MExplicitlyTypedAndNamed#getAppendTypeNameToName()
	 * @see #getMExplicitlyTypedAndNamed()
	 * @generated
	 */
	EAttribute getMExplicitlyTypedAndNamed_AppendTypeNameToName();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MExplicitlyTypedAndNamed#nameFromType() <em>Name From Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Name From Type</em>' operation.
	 * @see com.montages.mcore.MExplicitlyTypedAndNamed#nameFromType()
	 * @generated
	 */
	EOperation getMExplicitlyTypedAndNamed__NameFromType();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MExplicitlyTypedAndNamed#shortNameFromType() <em>Short Name From Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Short Name From Type</em>' operation.
	 * @see com.montages.mcore.MExplicitlyTypedAndNamed#shortNameFromType()
	 * @generated
	 */
	EOperation getMExplicitlyTypedAndNamed__ShortNameFromType();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MParameter <em>MParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MParameter</em>'.
	 * @see com.montages.mcore.MParameter
	 * @generated
	 */
	EClass getMParameter();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MParameter#getContainingSignature <em>Containing Signature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Signature</em>'.
	 * @see com.montages.mcore.MParameter#getContainingSignature()
	 * @see #getMParameter()
	 * @generated
	 */
	EReference getMParameter_ContainingSignature();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MParameter#getELabel <em>ELabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ELabel</em>'.
	 * @see com.montages.mcore.MParameter#getELabel()
	 * @see #getMParameter()
	 * @generated
	 */
	EAttribute getMParameter_ELabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MParameter#getSignatureAsString <em>Signature As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Signature As String</em>'.
	 * @see com.montages.mcore.MParameter#getSignatureAsString()
	 * @see #getMParameter()
	 * @generated
	 */
	EAttribute getMParameter_SignatureAsString();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MParameter#getInternalEParameter <em>Internal EParameter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Internal EParameter</em>'.
	 * @see com.montages.mcore.MParameter#getInternalEParameter()
	 * @see #getMParameter()
	 * @generated
	 */
	EReference getMParameter_InternalEParameter();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MParameter#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.MParameter#getDoAction()
	 * @see #getMParameter()
	 * @generated
	 */
	EAttribute getMParameter_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MParameter#doAction$Update(com.montages.mcore.MParameterAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.MParameter#doAction$Update(com.montages.mcore.MParameterAction)
	 * @generated
	 */
	EOperation getMParameter__DoAction$Update__MParameterAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MParameter#ambiguousParameterName() <em>Ambiguous Parameter Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ambiguous Parameter Name</em>' operation.
	 * @see com.montages.mcore.MParameter#ambiguousParameterName()
	 * @generated
	 */
	EOperation getMParameter__AmbiguousParameterName();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MParameter#ambiguousParameterShortName() <em>Ambiguous Parameter Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ambiguous Parameter Short Name</em>' operation.
	 * @see com.montages.mcore.MParameter#ambiguousParameterShortName()
	 * @generated
	 */
	EOperation getMParameter__AmbiguousParameterShortName();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MEditor <em>MEditor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MEditor</em>'.
	 * @see com.montages.mcore.MEditor
	 * @generated
	 */
	EClass getMEditor();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.MEditor#resetEditor() <em>Reset Editor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset Editor</em>' operation.
	 * @see com.montages.mcore.MEditor#resetEditor()
	 * @generated
	 */
	EOperation getMEditor__ResetEditor();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.MNamedEditor <em>MNamed Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MNamed Editor</em>'.
	 * @see com.montages.mcore.MNamedEditor
	 * @generated
	 */
	EClass getMNamedEditor();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamedEditor#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see com.montages.mcore.MNamedEditor#getName()
	 * @see #getMNamedEditor()
	 * @generated
	 */
	EAttribute getMNamedEditor_Name();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamedEditor#getLabel <em>Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Label</em>'.
	 * @see com.montages.mcore.MNamedEditor#getLabel()
	 * @see #getMNamedEditor()
	 * @generated
	 */
	EAttribute getMNamedEditor_Label();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.MNamedEditor#getUserVisible <em>User Visible</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>User Visible</em>'.
	 * @see com.montages.mcore.MNamedEditor#getUserVisible()
	 * @see #getMNamedEditor()
	 * @generated
	 */
	EAttribute getMNamedEditor_UserVisible();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.MNamedEditor#getEditor <em>Editor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Editor</em>'.
	 * @see com.montages.mcore.MNamedEditor#getEditor()
	 * @see #getMNamedEditor()
	 * @generated
	 */
	EReference getMNamedEditor_Editor();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MComponentAction <em>MComponent Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MComponent Action</em>'.
	 * @see com.montages.mcore.MComponentAction
	 * @generated
	 */
	EEnum getMComponentAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.TopLevelDomain <em>Top Level Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Top Level Domain</em>'.
	 * @see com.montages.mcore.TopLevelDomain
	 * @generated
	 */
	EEnum getTopLevelDomain();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MPackageAction <em>MPackage Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MPackage Action</em>'.
	 * @see com.montages.mcore.MPackageAction
	 * @generated
	 */
	EEnum getMPackageAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.SimpleType <em>Simple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Simple Type</em>'.
	 * @see com.montages.mcore.SimpleType
	 * @generated
	 */
	EEnum getSimpleType();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MClassifierAction <em>MClassifier Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MClassifier Action</em>'.
	 * @see com.montages.mcore.MClassifierAction
	 * @generated
	 */
	EEnum getMClassifierAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.OrderingStrategy <em>Ordering Strategy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Ordering Strategy</em>'.
	 * @see com.montages.mcore.OrderingStrategy
	 * @generated
	 */
	EEnum getOrderingStrategy();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MLiteralAction <em>MLiteral Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MLiteral Action</em>'.
	 * @see com.montages.mcore.MLiteralAction
	 * @generated
	 */
	EEnum getMLiteralAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MPropertiesGroupAction <em>MProperties Group Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MProperties Group Action</em>'.
	 * @see com.montages.mcore.MPropertiesGroupAction
	 * @generated
	 */
	EEnum getMPropertiesGroupAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MPropertyAction <em>MProperty Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MProperty Action</em>'.
	 * @see com.montages.mcore.MPropertyAction
	 * @generated
	 */
	EEnum getMPropertyAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.PropertyBehavior <em>Property Behavior</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Property Behavior</em>'.
	 * @see com.montages.mcore.PropertyBehavior
	 * @generated
	 */
	EEnum getPropertyBehavior();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MOperationSignatureAction <em>MOperation Signature Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MOperation Signature Action</em>'.
	 * @see com.montages.mcore.MOperationSignatureAction
	 * @generated
	 */
	EEnum getMOperationSignatureAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MultiplicityCase <em>Multiplicity Case</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Multiplicity Case</em>'.
	 * @see com.montages.mcore.MultiplicityCase
	 * @generated
	 */
	EEnum getMultiplicityCase();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.ClassifierKind <em>Classifier Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Classifier Kind</em>'.
	 * @see com.montages.mcore.ClassifierKind
	 * @generated
	 */
	EEnum getClassifierKind();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.PropertyKind <em>Property Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Property Kind</em>'.
	 * @see com.montages.mcore.PropertyKind
	 * @generated
	 */
	EEnum getPropertyKind();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.MParameterAction <em>MParameter Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MParameter Action</em>'.
	 * @see com.montages.mcore.MParameterAction
	 * @generated
	 */
	EEnum getMParameterAction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	McoreFactory getMcoreFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MRepositoryElementImpl <em>MRepository Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MRepositoryElementImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMRepositoryElement()
		 * @generated
		 */
		EClass MREPOSITORY_ELEMENT = eINSTANCE.getMRepositoryElement();

		/**
		 * The meta object literal for the '<em><b>Kind Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY_ELEMENT__KIND_LABEL = eINSTANCE
				.getMRepositoryElement_KindLabel();

		/**
		 * The meta object literal for the '<em><b>Rendered Kind Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL = eINSTANCE
				.getMRepositoryElement_RenderedKindLabel();

		/**
		 * The meta object literal for the '<em><b>Indent Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY_ELEMENT__INDENT_LEVEL = eINSTANCE
				.getMRepositoryElement_IndentLevel();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY_ELEMENT__DESCRIPTION = eINSTANCE
				.getMRepositoryElement_Description();

		/**
		 * The meta object literal for the '<em><b>As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY_ELEMENT__AS_TEXT = eINSTANCE
				.getMRepositoryElement_AsText();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MREPOSITORY_ELEMENT___INDENTATION_SPACES = eINSTANCE
				.getMRepositoryElement__IndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>Indentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER = eINSTANCE
				.getMRepositoryElement__IndentationSpaces__Integer();

		/**
		 * The meta object literal for the '<em><b>New Line String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MREPOSITORY_ELEMENT___NEW_LINE_STRING = eINSTANCE
				.getMRepositoryElement__NewLineString();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MRepositoryImpl <em>MRepository</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MRepositoryImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMRepository()
		 * @generated
		 */
		EClass MREPOSITORY = eINSTANCE.getMRepository();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MREPOSITORY__COMPONENT = eINSTANCE
				.getMRepository_Component();

		/**
		 * The meta object literal for the '<em><b>Core</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MREPOSITORY__CORE = eINSTANCE.getMRepository_Core();

		/**
		 * The meta object literal for the '<em><b>Referenced EPackage</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MREPOSITORY__REFERENCED_EPACKAGE = eINSTANCE
				.getMRepository_ReferencedEPackage();

		/**
		 * The meta object literal for the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MREPOSITORY__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION = eINSTANCE
				.getMRepository_AvoidRegenerationOfEditorConfiguration();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MNamedImpl <em>MNamed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MNamedImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMNamed()
		 * @generated
		 */
		EClass MNAMED = eINSTANCE.getMNamed();

		/**
		 * The meta object literal for the '<em><b>Calculated Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__CALCULATED_SHORT_NAME = eINSTANCE
				.getMNamed_CalculatedShortName();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__NAME = eINSTANCE.getMNamed_Name();

		/**
		 * The meta object literal for the '<em><b>Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__SHORT_NAME = eINSTANCE.getMNamed_ShortName();

		/**
		 * The meta object literal for the '<em><b>Correct Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__CORRECT_NAME = eINSTANCE.getMNamed_CorrectName();

		/**
		 * The meta object literal for the '<em><b>Correct Short Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__CORRECT_SHORT_NAME = eINSTANCE
				.getMNamed_CorrectShortName();

		/**
		 * The meta object literal for the '<em><b>EName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__ENAME = eINSTANCE.getMNamed_EName();

		/**
		 * The meta object literal for the '<em><b>Special EName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__SPECIAL_ENAME = eINSTANCE.getMNamed_SpecialEName();

		/**
		 * The meta object literal for the '<em><b>Full Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__FULL_LABEL = eINSTANCE.getMNamed_FullLabel();

		/**
		 * The meta object literal for the '<em><b>Local Structural Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__LOCAL_STRUCTURAL_NAME = eINSTANCE
				.getMNamed_LocalStructuralName();

		/**
		 * The meta object literal for the '<em><b>Calculated Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED__CALCULATED_NAME = eINSTANCE
				.getMNamed_CalculatedName();

		/**
		 * The meta object literal for the '<em><b>Same Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MNAMED___SAME_NAME__MNAMED = eINSTANCE
				.getMNamed__SameName__MNamed();

		/**
		 * The meta object literal for the '<em><b>Same Short Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MNAMED___SAME_SHORT_NAME__MNAMED = eINSTANCE
				.getMNamed__SameShortName__MNamed();

		/**
		 * The meta object literal for the '<em><b>Same String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MNAMED___SAME_STRING__STRING_STRING = eINSTANCE
				.getMNamed__SameString__String_String();

		/**
		 * The meta object literal for the '<em><b>String Empty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MNAMED___STRING_EMPTY__STRING = eINSTANCE
				.getMNamed__StringEmpty__String();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MComponentImpl <em>MComponent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MComponentImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMComponent()
		 * @generated
		 */
		EClass MCOMPONENT = eINSTANCE.getMComponent();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DO_ACTION = eINSTANCE.getMComponent_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCOMPONENT___DO_ACTION$_UPDATE__MCOMPONENTACTION = eINSTANCE
				.getMComponent__DoAction$Update__MComponentAction();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCOMPONENT___DO_ACTION_UPDATE__MCOMPONENTACTION = eINSTANCE
				.getMComponent__DoActionUpdate__MComponentAction();

		/**
		 * The meta object literal for the '<em><b>Owned Package</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__OWNED_PACKAGE = eINSTANCE
				.getMComponent_OwnedPackage();

		/**
		 * The meta object literal for the '<em><b>Resource Folder</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__RESOURCE_FOLDER = eINSTANCE
				.getMComponent_ResourceFolder();

		/**
		 * The meta object literal for the '<em><b>Semantics</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__SEMANTICS = eINSTANCE.getMComponent_Semantics();

		/**
		 * The meta object literal for the '<em><b>Preloaded Component</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__PRELOADED_COMPONENT = eINSTANCE
				.getMComponent_PreloadedComponent();

		/**
		 * The meta object literal for the '<em><b>Generated EPackage</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__GENERATED_EPACKAGE = eINSTANCE
				.getMComponent_GeneratedEPackage();

		/**
		 * The meta object literal for the '<em><b>Hide Advanced Properties</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__HIDE_ADVANCED_PROPERTIES = eINSTANCE
				.getMComponent_HideAdvancedProperties();

		/**
		 * The meta object literal for the '<em><b>Abstract Component</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__ABSTRACT_COMPONENT = eINSTANCE
				.getMComponent_AbstractComponent();

		/**
		 * The meta object literal for the '<em><b>Count Implementations</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__COUNT_IMPLEMENTATIONS = eINSTANCE
				.getMComponent_CountImplementations();

		/**
		 * The meta object literal for the '<em><b>Disable Default Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT = eINSTANCE
				.getMComponent_DisableDefaultContainment();

		/**
		 * The meta object literal for the '<em><b>All General Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__ALL_GENERAL_ANNOTATIONS = eINSTANCE
				.getMComponent_AllGeneralAnnotations();

		/**
		 * The meta object literal for the '<em><b>Named Editor</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__NAMED_EDITOR = eINSTANCE
				.getMComponent_NamedEditor();

		/**
		 * The meta object literal for the '<em><b>Main Editor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__MAIN_EDITOR = eINSTANCE
				.getMComponent_MainEditor();

		/**
		 * The meta object literal for the '<em><b>Use Legacy Editorconfig</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__USE_LEGACY_EDITORCONFIG = eINSTANCE
				.getMComponent_UseLegacyEditorconfig();

		/**
		 * The meta object literal for the '<em><b>Main Named Editor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__MAIN_NAMED_EDITOR = eINSTANCE
				.getMComponent_MainNamedEditor();

		/**
		 * The meta object literal for the '<em><b>Name Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__NAME_PREFIX = eINSTANCE
				.getMComponent_NamePrefix();

		/**
		 * The meta object literal for the '<em><b>Derived Name Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DERIVED_NAME_PREFIX = eINSTANCE
				.getMComponent_DerivedNamePrefix();

		/**
		 * The meta object literal for the '<em><b>Name Prefix For Features</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__NAME_PREFIX_FOR_FEATURES = eINSTANCE
				.getMComponent_NamePrefixForFeatures();

		/**
		 * The meta object literal for the '<em><b>Derived Bundle Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DERIVED_BUNDLE_NAME = eINSTANCE
				.getMComponent_DerivedBundleName();

		/**
		 * The meta object literal for the '<em><b>Derived URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DERIVED_URI = eINSTANCE
				.getMComponent_DerivedURI();

		/**
		 * The meta object literal for the '<em><b>Represents Core Component</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__REPRESENTS_CORE_COMPONENT = eINSTANCE
				.getMComponent_RepresentsCoreComponent();

		/**
		 * The meta object literal for the '<em><b>Root Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__ROOT_PACKAGE = eINSTANCE
				.getMComponent_RootPackage();

		/**
		 * The meta object literal for the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION = eINSTANCE
				.getMComponent_AvoidRegenerationOfEditorConfiguration();

		/**
		 * The meta object literal for the '<em><b>Containing Repository</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCOMPONENT__CONTAINING_REPOSITORY = eINSTANCE
				.getMComponent_ContainingRepository();

		/**
		 * The meta object literal for the '<em><b>Domain Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DOMAIN_TYPE = eINSTANCE
				.getMComponent_DomainType();

		/**
		 * The meta object literal for the '<em><b>Domain Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DOMAIN_NAME = eINSTANCE
				.getMComponent_DomainName();

		/**
		 * The meta object literal for the '<em><b>Derived Domain Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DERIVED_DOMAIN_TYPE = eINSTANCE
				.getMComponent_DerivedDomainType();

		/**
		 * The meta object literal for the '<em><b>Derived Domain Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__DERIVED_DOMAIN_NAME = eINSTANCE
				.getMComponent_DerivedDomainName();

		/**
		 * The meta object literal for the '<em><b>Special Bundle Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCOMPONENT__SPECIAL_BUNDLE_NAME = eINSTANCE
				.getMComponent_SpecialBundleName();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MModelElementImpl <em>MModel Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MModelElementImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMModelElement()
		 * @generated
		 */
		EClass MMODEL_ELEMENT = eINSTANCE.getMModelElement();

		/**
		 * The meta object literal for the '<em><b>General Annotation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMODEL_ELEMENT__GENERAL_ANNOTATION = eINSTANCE
				.getMModelElement_GeneralAnnotation();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MPackageImpl <em>MPackage</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MPackageImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPackage()
		 * @generated
		 */
		EClass MPACKAGE = eINSTANCE.getMPackage();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DO_ACTION = eINSTANCE.getMPackage_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPACKAGE___DO_ACTION$_UPDATE__MPACKAGEACTION = eINSTANCE
				.getMPackage__DoAction$Update__MPackageAction();

		/**
		 * The meta object literal for the '<em><b>Generate Json Schema$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPACKAGE___GENERATE_JSON_SCHEMA$_UPDATE__BOOLEAN = eINSTANCE
				.getMPackage__GenerateJsonSchema$Update__Boolean();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPACKAGE___DO_ACTION_UPDATE__MPACKAGEACTION = eINSTANCE
				.getMPackage__DoActionUpdate__MPackageAction();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__CLASSIFIER = eINSTANCE.getMPackage_Classifier();

		/**
		 * The meta object literal for the '<em><b>Sub Package</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__SUB_PACKAGE = eINSTANCE.getMPackage_SubPackage();

		/**
		 * The meta object literal for the '<em><b>Package Annotations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__PACKAGE_ANNOTATIONS = eINSTANCE
				.getMPackage_PackageAnnotations();

		/**
		 * The meta object literal for the '<em><b>Name Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__NAME_PREFIX = eINSTANCE.getMPackage_NamePrefix();

		/**
		 * The meta object literal for the '<em><b>Derived Name Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_NAME_PREFIX = eINSTANCE
				.getMPackage_DerivedNamePrefix();

		/**
		 * The meta object literal for the '<em><b>Name Prefix Scope</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__NAME_PREFIX_SCOPE = eINSTANCE
				.getMPackage_NamePrefixScope();

		/**
		 * The meta object literal for the '<em><b>Derived Json Schema</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_JSON_SCHEMA = eINSTANCE
				.getMPackage_DerivedJsonSchema();

		/**
		 * The meta object literal for the '<em><b>Generate Json Schema</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__GENERATE_JSON_SCHEMA = eINSTANCE
				.getMPackage_GenerateJsonSchema();

		/**
		 * The meta object literal for the '<em><b>Generated Json Schema</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__GENERATED_JSON_SCHEMA = eINSTANCE
				.getMPackage_GeneratedJsonSchema();

		/**
		 * The meta object literal for the '<em><b>Generate Json Schema$update01 Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_OBJECT__BOOLEAN = eINSTANCE
				.getMPackage__GenerateJsonSchema$update01Object__Boolean();

		/**
		 * The meta object literal for the '<em><b>Generate Json Schema$update01 Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_VALUE__BOOLEAN_MPACKAGE = eINSTANCE
				.getMPackage__GenerateJsonSchema$update01Value__Boolean_MPackage();

		/**
		 * The meta object literal for the '<em><b>Use Package Content Only With Explicit Filter</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER = eINSTANCE
				.getMPackage_UsePackageContentOnlyWithExplicitFilter();

		/**
		 * The meta object literal for the '<em><b>Special Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__SPECIAL_NS_URI = eINSTANCE
				.getMPackage_SpecialNsURI();

		/**
		 * The meta object literal for the '<em><b>Special Ns Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__SPECIAL_NS_PREFIX = eINSTANCE
				.getMPackage_SpecialNsPrefix();

		/**
		 * The meta object literal for the '<em><b>Special File Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__SPECIAL_FILE_NAME = eINSTANCE
				.getMPackage_SpecialFileName();

		/**
		 * The meta object literal for the '<em><b>Use UUID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__USE_UUID = eINSTANCE.getMPackage_UseUUID();

		/**
		 * The meta object literal for the '<em><b>Uuid Attribute Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__UUID_ATTRIBUTE_NAME = eINSTANCE
				.getMPackage_UuidAttributeName();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__PARENT = eINSTANCE.getMPackage_Parent();

		/**
		 * The meta object literal for the '<em><b>Containing Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__CONTAINING_PACKAGE = eINSTANCE
				.getMPackage_ContainingPackage();

		/**
		 * The meta object literal for the '<em><b>Containing Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__CONTAINING_COMPONENT = eINSTANCE
				.getMPackage_ContainingComponent();

		/**
		 * The meta object literal for the '<em><b>Represents Core Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__REPRESENTS_CORE_PACKAGE = eINSTANCE
				.getMPackage_RepresentsCorePackage();

		/**
		 * The meta object literal for the '<em><b>All Subpackages</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__ALL_SUBPACKAGES = eINSTANCE
				.getMPackage_AllSubpackages();

		/**
		 * The meta object literal for the '<em><b>Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__QUALIFIED_NAME = eINSTANCE
				.getMPackage_QualifiedName();

		/**
		 * The meta object literal for the '<em><b>Is Root Package</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__IS_ROOT_PACKAGE = eINSTANCE
				.getMPackage_IsRootPackage();

		/**
		 * The meta object literal for the '<em><b>Internal EPackage</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__INTERNAL_EPACKAGE = eINSTANCE
				.getMPackage_InternalEPackage();

		/**
		 * The meta object literal for the '<em><b>Derived Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_NS_URI = eINSTANCE
				.getMPackage_DerivedNsURI();

		/**
		 * The meta object literal for the '<em><b>Derived Standard Ns URI</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_STANDARD_NS_URI = eINSTANCE
				.getMPackage_DerivedStandardNsURI();

		/**
		 * The meta object literal for the '<em><b>Derived Ns Prefix</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_NS_PREFIX = eINSTANCE
				.getMPackage_DerivedNsPrefix();

		/**
		 * The meta object literal for the '<em><b>Derived Java Package Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPACKAGE__DERIVED_JAVA_PACKAGE_NAME = eINSTANCE
				.getMPackage_DerivedJavaPackageName();

		/**
		 * The meta object literal for the '<em><b>Resource Root Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPACKAGE__RESOURCE_ROOT_CLASS = eINSTANCE
				.getMPackage_ResourceRootClass();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MHasSimpleTypeImpl <em>MHas Simple Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MHasSimpleTypeImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMHasSimpleType()
		 * @generated
		 */
		EClass MHAS_SIMPLE_TYPE = eINSTANCE.getMHasSimpleType();

		/**
		 * The meta object literal for the '<em><b>Simple Type String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING = eINSTANCE
				.getMHasSimpleType_SimpleTypeString();

		/**
		 * The meta object literal for the '<em><b>Simple Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MHAS_SIMPLE_TYPE__SIMPLE_TYPE = eINSTANCE
				.getMHasSimpleType_SimpleType();

		/**
		 * The meta object literal for the '<em><b>Has Simple Data Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE = eINSTANCE
				.getMHasSimpleType_HasSimpleDataType();

		/**
		 * The meta object literal for the '<em><b>Has Simple Modeling Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE = eINSTANCE
				.getMHasSimpleType_HasSimpleModelingType();

		/**
		 * The meta object literal for the '<em><b>Simple Type Is Correct</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT = eINSTANCE
				.getMHasSimpleType_SimpleTypeIsCorrect();

		/**
		 * The meta object literal for the '<em><b>Simple Type As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MHAS_SIMPLE_TYPE___SIMPLE_TYPE_AS_STRING__SIMPLETYPE = eINSTANCE
				.getMHasSimpleType__SimpleTypeAsString__SimpleType();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MClassifierImpl <em>MClassifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MClassifierImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMClassifier()
		 * @generated
		 */
		EClass MCLASSIFIER = eINSTANCE.getMClassifier();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__DO_ACTION = eINSTANCE.getMClassifier_DoAction();

		/**
		 * The meta object literal for the '<em><b>Resolve Container Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__RESOLVE_CONTAINER_ACTION = eINSTANCE
				.getMClassifier_ResolveContainerAction();

		/**
		 * The meta object literal for the '<em><b>Abstract Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__ABSTRACT_DO_ACTION = eINSTANCE
				.getMClassifier_AbstractDoAction();

		/**
		 * The meta object literal for the '<em><b>Object References</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__OBJECT_REFERENCES = eINSTANCE
				.getMClassifier_ObjectReferences();

		/**
		 * The meta object literal for the '<em><b>Nearest Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__NEAREST_INSTANCE = eINSTANCE
				.getMClassifier_NearestInstance();

		/**
		 * The meta object literal for the '<em><b>Strict Nearest Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__STRICT_NEAREST_INSTANCE = eINSTANCE
				.getMClassifier_StrictNearestInstance();

		/**
		 * The meta object literal for the '<em><b>Strict Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__STRICT_INSTANCE = eINSTANCE
				.getMClassifier_StrictInstance();

		/**
		 * The meta object literal for the '<em><b>Intelligent Nearest Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE = eINSTANCE
				.getMClassifier_IntelligentNearestInstance();

		/**
		 * The meta object literal for the '<em><b>Object Unreferenced</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__OBJECT_UNREFERENCED = eINSTANCE
				.getMClassifier_ObjectUnreferenced();

		/**
		 * The meta object literal for the '<em><b>Outstanding To Copy Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS = eINSTANCE
				.getMClassifier_OutstandingToCopyObjects();

		/**
		 * The meta object literal for the '<em><b>Property Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__PROPERTY_INSTANCES = eINSTANCE
				.getMClassifier_PropertyInstances();

		/**
		 * The meta object literal for the '<em><b>Object Reference Property Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES = eINSTANCE
				.getMClassifier_ObjectReferencePropertyInstances();

		/**
		 * The meta object literal for the '<em><b>Intelligent Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTELLIGENT_INSTANCE = eINSTANCE
				.getMClassifier_IntelligentInstance();

		/**
		 * The meta object literal for the '<em><b>Containment Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__CONTAINMENT_HIERARCHY = eINSTANCE
				.getMClassifier_ContainmentHierarchy();

		/**
		 * The meta object literal for the '<em><b>Strict All Classes Contained In</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN = eINSTANCE
				.getMClassifier_StrictAllClassesContainedIn();

		/**
		 * The meta object literal for the '<em><b>Strict Containment Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY = eINSTANCE
				.getMClassifier_StrictContainmentHierarchy();

		/**
		 * The meta object literal for the '<em><b>Intelligent All Classes Contained In</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN = eINSTANCE
				.getMClassifier_IntelligentAllClassesContainedIn();

		/**
		 * The meta object literal for the '<em><b>Intelligent Containment Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY = eINSTANCE
				.getMClassifier_IntelligentContainmentHierarchy();

		/**
		 * The meta object literal for the '<em><b>Instantiation Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INSTANTIATION_HIERARCHY = eINSTANCE
				.getMClassifier_InstantiationHierarchy();

		/**
		 * The meta object literal for the '<em><b>Instantiation Property Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY = eINSTANCE
				.getMClassifier_InstantiationPropertyHierarchy();

		/**
		 * The meta object literal for the '<em><b>Intelligent Instantiation Property Hierarchy</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY = eINSTANCE
				.getMClassifier_IntelligentInstantiationPropertyHierarchy();

		/**
		 * The meta object literal for the '<em><b>Classescontainedin</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__CLASSESCONTAINEDIN = eINSTANCE
				.getMClassifier_Classescontainedin();

		/**
		 * The meta object literal for the '<em><b>Can Apply Default Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT = eINSTANCE
				.getMClassifier_CanApplyDefaultContainment();

		/**
		 * The meta object literal for the '<em><b>Properties As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__PROPERTIES_AS_TEXT = eINSTANCE
				.getMClassifier_PropertiesAsText();

		/**
		 * The meta object literal for the '<em><b>Semantics As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__SEMANTICS_AS_TEXT = eINSTANCE
				.getMClassifier_SemanticsAsText();

		/**
		 * The meta object literal for the '<em><b>Derived Json Schema</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__DERIVED_JSON_SCHEMA = eINSTANCE
				.getMClassifier_DerivedJsonSchema();

		/**
		 * The meta object literal for the '<em><b>Toggle Super Type$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___TOGGLE_SUPER_TYPE$_UPDATE__MCLASSIFIER = eINSTANCE
				.getMClassifier__ToggleSuperType$Update__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Ordering Strategy$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ORDERING_STRATEGY$_UPDATE__ORDERINGSTRATEGY = eINSTANCE
				.getMClassifier__OrderingStrategy$Update__OrderingStrategy();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___DO_ACTION$_UPDATE__MCLASSIFIERACTION = eINSTANCE
				.getMClassifier__DoAction$Update__MClassifierAction();

		/**
		 * The meta object literal for the '<em><b>Super Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__SUPER_TYPE_PACKAGE = eINSTANCE
				.getMClassifier_SuperTypePackage();

		/**
		 * The meta object literal for the '<em><b>Super Type</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__SUPER_TYPE = eINSTANCE
				.getMClassifier_SuperType();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INSTANCE = eINSTANCE.getMClassifier_Instance();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__KIND = eINSTANCE.getMClassifier_Kind();

		/**
		 * The meta object literal for the '<em><b>Enforced Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__ENFORCED_CLASS = eINSTANCE
				.getMClassifier_EnforcedClass();

		/**
		 * The meta object literal for the '<em><b>Abstract Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__ABSTRACT_CLASS = eINSTANCE
				.getMClassifier_AbstractClass();

		/**
		 * The meta object literal for the '<em><b>Internal EClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__INTERNAL_ECLASSIFIER = eINSTANCE
				.getMClassifier_InternalEClassifier();

		/**
		 * The meta object literal for the '<em><b>Containing Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__CONTAINING_PACKAGE = eINSTANCE
				.getMClassifier_ContainingPackage();

		/**
		 * The meta object literal for the '<em><b>Represents Core Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__REPRESENTS_CORE_TYPE = eINSTANCE
				.getMClassifier_RepresentsCoreType();

		/**
		 * The meta object literal for the '<em><b>All Property Groups</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__ALL_PROPERTY_GROUPS = eINSTANCE
				.getMClassifier_AllPropertyGroups();

		/**
		 * The meta object literal for the '<em><b>Literal</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__LITERAL = eINSTANCE.getMClassifier_Literal();

		/**
		 * The meta object literal for the '<em><b>EInterface</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__EINTERFACE = eINSTANCE
				.getMClassifier_EInterface();

		/**
		 * The meta object literal for the '<em><b>Operation As Id Property Error</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR = eINSTANCE
				.getMClassifier_OperationAsIdPropertyError();

		/**
		 * The meta object literal for the '<em><b>Is Class By Structure</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__IS_CLASS_BY_STRUCTURE = eINSTANCE
				.getMClassifier_IsClassByStructure();

		/**
		 * The meta object literal for the '<em><b>Toggle Super Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__TOGGLE_SUPER_TYPE = eINSTANCE
				.getMClassifier_ToggleSuperType();

		/**
		 * The meta object literal for the '<em><b>Ordering Strategy</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MCLASSIFIER__ORDERING_STRATEGY = eINSTANCE
				.getMClassifier_OrderingStrategy();

		/**
		 * The meta object literal for the '<em><b>Test All Properties</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__TEST_ALL_PROPERTIES = eINSTANCE
				.getMClassifier_TestAllProperties();

		/**
		 * The meta object literal for the '<em><b>Id Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MCLASSIFIER__ID_PROPERTY = eINSTANCE
				.getMClassifier_IdProperty();

		/**
		 * The meta object literal for the '<em><b>Default Property Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___DEFAULT_PROPERTY_VALUE = eINSTANCE
				.getMClassifier__DefaultPropertyValue();

		/**
		 * The meta object literal for the '<em><b>Default Property Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___DEFAULT_PROPERTY_DESCRIPTION = eINSTANCE
				.getMClassifier__DefaultPropertyDescription();

		/**
		 * The meta object literal for the '<em><b>All Literals</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_LITERALS = eINSTANCE
				.getMClassifier__AllLiterals();

		/**
		 * The meta object literal for the '<em><b>Selectable Classifier</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___SELECTABLE_CLASSIFIER__ELIST_MPACKAGE = eINSTANCE
				.getMClassifier__SelectableClassifier__EList_MPackage();

		/**
		 * The meta object literal for the '<em><b>All Direct Sub Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_DIRECT_SUB_TYPES = eINSTANCE
				.getMClassifier__AllDirectSubTypes();

		/**
		 * The meta object literal for the '<em><b>All Sub Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_SUB_TYPES = eINSTANCE
				.getMClassifier__AllSubTypes();

		/**
		 * The meta object literal for the '<em><b>Super Types As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___SUPER_TYPES_AS_STRING = eINSTANCE
				.getMClassifier__SuperTypesAsString();

		/**
		 * The meta object literal for the '<em><b>All Super Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_SUPER_TYPES = eINSTANCE
				.getMClassifier__AllSuperTypes();

		/**
		 * The meta object literal for the '<em><b>All Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_PROPERTIES = eINSTANCE
				.getMClassifier__AllProperties();

		/**
		 * The meta object literal for the '<em><b>All Super Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_SUPER_PROPERTIES = eINSTANCE
				.getMClassifier__AllSuperProperties();

		/**
		 * The meta object literal for the '<em><b>All Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_FEATURES = eINSTANCE
				.getMClassifier__AllFeatures();

		/**
		 * The meta object literal for the '<em><b>All Non Containment Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_NON_CONTAINMENT_FEATURES = eINSTANCE
				.getMClassifier__AllNonContainmentFeatures();

		/**
		 * The meta object literal for the '<em><b>All Stored Non Containment Features</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_STORED_NON_CONTAINMENT_FEATURES = eINSTANCE
				.getMClassifier__AllStoredNonContainmentFeatures();

		/**
		 * The meta object literal for the '<em><b>All Features With Storage</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_FEATURES_WITH_STORAGE = eINSTANCE
				.getMClassifier__AllFeaturesWithStorage();

		/**
		 * The meta object literal for the '<em><b>All Containment References</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_CONTAINMENT_REFERENCES = eINSTANCE
				.getMClassifier__AllContainmentReferences();

		/**
		 * The meta object literal for the '<em><b>All Operation Signatures</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_OPERATION_SIGNATURES = eINSTANCE
				.getMClassifier__AllOperationSignatures();

		/**
		 * The meta object literal for the '<em><b>All Classes Contained In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_CLASSES_CONTAINED_IN = eINSTANCE
				.getMClassifier__AllClassesContainedIn();

		/**
		 * The meta object literal for the '<em><b>All Id Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_ID_PROPERTIES = eINSTANCE
				.getMClassifier__AllIdProperties();

		/**
		 * The meta object literal for the '<em><b>All Super Id Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___ALL_SUPER_ID_PROPERTIES = eINSTANCE
				.getMClassifier__AllSuperIdProperties();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___DO_ACTION_UPDATE__MCLASSIFIERACTION = eINSTANCE
				.getMClassifier__DoActionUpdate__MClassifierAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Do Action</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___INVOKE_SET_DO_ACTION__MCLASSIFIERACTION = eINSTANCE
				.getMClassifier__InvokeSetDoAction__MClassifierAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Toggle Super Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___INVOKE_TOGGLE_SUPER_TYPE__MCLASSIFIER = eINSTANCE
				.getMClassifier__InvokeToggleSuperType__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Do Super Type Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___DO_SUPER_TYPE_UPDATE__MCLASSIFIER = eINSTANCE
				.getMClassifier__DoSuperTypeUpdate__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Ambiguous Classifier Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MCLASSIFIER___AMBIGUOUS_CLASSIFIER_NAME = eINSTANCE
				.getMClassifier__AmbiguousClassifierName();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MLiteralImpl <em>MLiteral</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MLiteralImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMLiteral()
		 * @generated
		 */
		EClass MLITERAL = eINSTANCE.getMLiteral();

		/**
		 * The meta object literal for the '<em><b>Constant Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MLITERAL__CONSTANT_LABEL = eINSTANCE
				.getMLiteral_ConstantLabel();

		/**
		 * The meta object literal for the '<em><b>As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MLITERAL__AS_STRING = eINSTANCE.getMLiteral_AsString();

		/**
		 * The meta object literal for the '<em><b>Qualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MLITERAL__QUALIFIED_NAME = eINSTANCE
				.getMLiteral_QualifiedName();

		/**
		 * The meta object literal for the '<em><b>Containing Enumeration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MLITERAL__CONTAINING_ENUMERATION = eINSTANCE
				.getMLiteral_ContainingEnumeration();

		/**
		 * The meta object literal for the '<em><b>Internal EEnum Literal</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MLITERAL__INTERNAL_EENUM_LITERAL = eINSTANCE
				.getMLiteral_InternalEEnumLiteral();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MPropertiesContainerImpl <em>MProperties Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MPropertiesContainerImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesContainer()
		 * @generated
		 */
		EClass MPROPERTIES_CONTAINER = eINSTANCE.getMPropertiesContainer();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__PROPERTY = eINSTANCE
				.getMPropertiesContainer_Property();

		/**
		 * The meta object literal for the '<em><b>Annotations</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__ANNOTATIONS = eINSTANCE
				.getMPropertiesContainer_Annotations();

		/**
		 * The meta object literal for the '<em><b>Property Group</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__PROPERTY_GROUP = eINSTANCE
				.getMPropertiesContainer_PropertyGroup();

		/**
		 * The meta object literal for the '<em><b>All Constraint Annotations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS = eINSTANCE
				.getMPropertiesContainer_AllConstraintAnnotations();

		/**
		 * The meta object literal for the '<em><b>Containing Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER = eINSTANCE
				.getMPropertiesContainer_ContainingClassifier();

		/**
		 * The meta object literal for the '<em><b>Owned Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTIES_CONTAINER__OWNED_PROPERTY = eINSTANCE
				.getMPropertiesContainer_OwnedProperty();

		/**
		 * The meta object literal for the '<em><b>Nr Of Properties And Signatures</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES = eINSTANCE
				.getMPropertiesContainer_NrOfPropertiesAndSignatures();

		/**
		 * The meta object literal for the '<em><b>All Property Annotations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS = eINSTANCE
				.getMPropertiesContainer__AllPropertyAnnotations();

		/**
		 * The meta object literal for the '<em><b>All Operation Annotations</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS = eINSTANCE
				.getMPropertiesContainer__AllOperationAnnotations();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MPropertiesGroupImpl <em>MProperties Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MPropertiesGroupImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesGroup()
		 * @generated
		 */
		EClass MPROPERTIES_GROUP = eINSTANCE.getMPropertiesGroup();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTIES_GROUP__DO_ACTION = eINSTANCE
				.getMPropertiesGroup_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTIES_GROUP___DO_ACTION$_UPDATE__MPROPERTIESGROUPACTION = eINSTANCE
				.getMPropertiesGroup__DoAction$Update__MPropertiesGroupAction();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTIES_GROUP___DO_ACTION_UPDATE__MPROPERTIESGROUPACTION = eINSTANCE
				.getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Do Action</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTIES_GROUP___INVOKE_SET_DO_ACTION__MPROPERTIESGROUPACTION = eINSTANCE
				.getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction();

		/**
		 * The meta object literal for the '<em><b>Order</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTIES_GROUP__ORDER = eINSTANCE
				.getMPropertiesGroup_Order();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MPropertyImpl <em>MProperty</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MPropertyImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMProperty()
		 * @generated
		 */
		EClass MPROPERTY = eINSTANCE.getMProperty();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__DO_ACTION = eINSTANCE.getMProperty_DoAction();

		/**
		 * The meta object literal for the '<em><b>Behavior Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__BEHAVIOR_ACTIONS = eINSTANCE
				.getMProperty_BehaviorActions();

		/**
		 * The meta object literal for the '<em><b>Layout Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__LAYOUT_ACTIONS = eINSTANCE
				.getMProperty_LayoutActions();

		/**
		 * The meta object literal for the '<em><b>Arity Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ARITY_ACTIONS = eINSTANCE
				.getMProperty_ArityActions();

		/**
		 * The meta object literal for the '<em><b>Simple Type Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__SIMPLE_TYPE_ACTIONS = eINSTANCE
				.getMProperty_SimpleTypeActions();

		/**
		 * The meta object literal for the '<em><b>Annotation Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ANNOTATION_ACTIONS = eINSTANCE
				.getMProperty_AnnotationActions();

		/**
		 * The meta object literal for the '<em><b>Move Actions</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__MOVE_ACTIONS = eINSTANCE
				.getMProperty_MoveActions();

		/**
		 * The meta object literal for the '<em><b>Derived Json Schema</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__DERIVED_JSON_SCHEMA = eINSTANCE
				.getMProperty_DerivedJsonSchema();

		/**
		 * The meta object literal for the '<em><b>Opposite Definition$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___OPPOSITE_DEFINITION$_UPDATE__MPROPERTY = eINSTANCE
				.getMProperty__OppositeDefinition$Update__MProperty();

		/**
		 * The meta object literal for the '<em><b>Property Behavior$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___PROPERTY_BEHAVIOR$_UPDATE__PROPERTYBEHAVIOR = eINSTANCE
				.getMProperty__PropertyBehavior$Update__PropertyBehavior();

		/**
		 * The meta object literal for the '<em><b>Type Definition$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___TYPE_DEFINITION$_UPDATE__MCLASSIFIER = eINSTANCE
				.getMProperty__TypeDefinition$Update__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___DO_ACTION$_UPDATE__MPROPERTYACTION = eINSTANCE
				.getMProperty__DoAction$Update__MPropertyAction();

		/**
		 * The meta object literal for the '<em><b>Operation Signature</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__OPERATION_SIGNATURE = eINSTANCE
				.getMProperty_OperationSignature();

		/**
		 * The meta object literal for the '<em><b>Opposite Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__OPPOSITE_DEFINITION = eINSTANCE
				.getMProperty_OppositeDefinition();

		/**
		 * The meta object literal for the '<em><b>Opposite</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__OPPOSITE = eINSTANCE.getMProperty_Opposite();

		/**
		 * The meta object literal for the '<em><b>Is Operation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__IS_OPERATION = eINSTANCE
				.getMProperty_IsOperation();

		/**
		 * The meta object literal for the '<em><b>Is Attribute</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__IS_ATTRIBUTE = eINSTANCE
				.getMProperty_IsAttribute();

		/**
		 * The meta object literal for the '<em><b>Is Reference</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__IS_REFERENCE = eINSTANCE
				.getMProperty_IsReference();

		/**
		 * The meta object literal for the '<em><b>Property Behavior</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__PROPERTY_BEHAVIOR = eINSTANCE
				.getMProperty_PropertyBehavior();

		/**
		 * The meta object literal for the '<em><b>Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__KIND = eINSTANCE.getMProperty_Kind();

		/**
		 * The meta object literal for the '<em><b>Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__CONTAINMENT = eINSTANCE
				.getMProperty_Containment();

		/**
		 * The meta object literal for the '<em><b>Has Storage</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__HAS_STORAGE = eINSTANCE.getMProperty_HasStorage();

		/**
		 * The meta object literal for the '<em><b>Changeable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__CHANGEABLE = eINSTANCE.getMProperty_Changeable();

		/**
		 * The meta object literal for the '<em><b>Persisted</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__PERSISTED = eINSTANCE.getMProperty_Persisted();

		/**
		 * The meta object literal for the '<em><b>Type Definition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__TYPE_DEFINITION = eINSTANCE
				.getMProperty_TypeDefinition();

		/**
		 * The meta object literal for the '<em><b>Use Core Types</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__USE_CORE_TYPES = eINSTANCE
				.getMProperty_UseCoreTypes();

		/**
		 * The meta object literal for the '<em><b>ELabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ELABEL = eINSTANCE.getMProperty_ELabel();

		/**
		 * The meta object literal for the '<em><b>EName For ELabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ENAME_FOR_ELABEL = eINSTANCE
				.getMProperty_ENameForELabel();

		/**
		 * The meta object literal for the '<em><b>Order Within Group</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ORDER_WITHIN_GROUP = eINSTANCE
				.getMProperty_OrderWithinGroup();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ID = eINSTANCE.getMProperty_Id();

		/**
		 * The meta object literal for the '<em><b>Internal EStructural Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE = eINSTANCE
				.getMProperty_InternalEStructuralFeature();

		/**
		 * The meta object literal for the '<em><b>Direct Semantics</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__DIRECT_SEMANTICS = eINSTANCE
				.getMProperty_DirectSemantics();

		/**
		 * The meta object literal for the '<em><b>Semantics Specialization</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__SEMANTICS_SPECIALIZATION = eINSTANCE
				.getMProperty_SemanticsSpecialization();

		/**
		 * The meta object literal for the '<em><b>All Signatures</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__ALL_SIGNATURES = eINSTANCE
				.getMProperty_AllSignatures();

		/**
		 * The meta object literal for the '<em><b>Direct Operation Semantics</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__DIRECT_OPERATION_SEMANTICS = eINSTANCE
				.getMProperty_DirectOperationSemantics();

		/**
		 * The meta object literal for the '<em><b>Containing Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__CONTAINING_CLASSIFIER = eINSTANCE
				.getMProperty_ContainingClassifier();

		/**
		 * The meta object literal for the '<em><b>Containing Properties Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__CONTAINING_PROPERTIES_CONTAINER = eINSTANCE
				.getMProperty_ContainingPropertiesContainer();

		/**
		 * The meta object literal for the '<em><b>EKey For Path</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY__EKEY_FOR_PATH = eINSTANCE
				.getMProperty_EKeyForPath();

		/**
		 * The meta object literal for the '<em><b>Not Unsettable</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__NOT_UNSETTABLE = eINSTANCE
				.getMProperty_NotUnsettable();

		/**
		 * The meta object literal for the '<em><b>Attribute For EId</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__ATTRIBUTE_FOR_EID = eINSTANCE
				.getMProperty_AttributeForEId();

		/**
		 * The meta object literal for the '<em><b>EDefault Value Literal</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY__EDEFAULT_VALUE_LITERAL = eINSTANCE
				.getMProperty_EDefaultValueLiteral();

		/**
		 * The meta object literal for the '<em><b>Selectable Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___SELECTABLE_TYPE__MCLASSIFIER = eINSTANCE
				.getMProperty__SelectableType__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Ambiguous Property Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___AMBIGUOUS_PROPERTY_NAME = eINSTANCE
				.getMProperty__AmbiguousPropertyName();

		/**
		 * The meta object literal for the '<em><b>Ambiguous Property Short Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___AMBIGUOUS_PROPERTY_SHORT_NAME = eINSTANCE
				.getMProperty__AmbiguousPropertyShortName();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___DO_ACTION_UPDATE__MPROPERTYACTION = eINSTANCE
				.getMProperty__DoActionUpdate__MPropertyAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Property Behaviour</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___INVOKE_SET_PROPERTY_BEHAVIOUR__PROPERTYBEHAVIOR = eINSTANCE
				.getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Do Action</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY___INVOKE_SET_DO_ACTION__MPROPERTYACTION = eINSTANCE
				.getMProperty__InvokeSetDoAction__MPropertyAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MOperationSignatureImpl <em>MOperation Signature</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MOperationSignatureImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMOperationSignature()
		 * @generated
		 */
		EClass MOPERATION_SIGNATURE = eINSTANCE.getMOperationSignature();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOPERATION_SIGNATURE__DO_ACTION = eINSTANCE
				.getMOperationSignature_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOPERATION_SIGNATURE___DO_ACTION$_UPDATE__MOPERATIONSIGNATUREACTION = eINSTANCE
				.getMOperationSignature__DoAction$Update__MOperationSignatureAction();

		/**
		 * The meta object literal for the '<em><b>Parameter</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOPERATION_SIGNATURE__PARAMETER = eINSTANCE
				.getMOperationSignature_Parameter();

		/**
		 * The meta object literal for the '<em><b>Operation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOPERATION_SIGNATURE__OPERATION = eINSTANCE
				.getMOperationSignature_Operation();

		/**
		 * The meta object literal for the '<em><b>Containing Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER = eINSTANCE
				.getMOperationSignature_ContainingClassifier();

		/**
		 * The meta object literal for the '<em><b>ELabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOPERATION_SIGNATURE__ELABEL = eINSTANCE
				.getMOperationSignature_ELabel();

		/**
		 * The meta object literal for the '<em><b>Internal EOperation</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOPERATION_SIGNATURE__INTERNAL_EOPERATION = eINSTANCE
				.getMOperationSignature_InternalEOperation();

		/**
		 * The meta object literal for the '<em><b>Same Signature</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOPERATION_SIGNATURE___SAME_SIGNATURE__MOPERATIONSIGNATURE = eINSTANCE
				.getMOperationSignature__SameSignature__MOperationSignature();

		/**
		 * The meta object literal for the '<em><b>Signature As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOPERATION_SIGNATURE___SIGNATURE_AS_STRING = eINSTANCE
				.getMOperationSignature__SignatureAsString();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MTypedImpl <em>MTyped</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MTypedImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMTyped()
		 * @generated
		 */
		EClass MTYPED = eINSTANCE.getMTyped();

		/**
		 * The meta object literal for the '<em><b>Multiplicity As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__MULTIPLICITY_AS_STRING = eINSTANCE
				.getMTyped_MultiplicityAsString();

		/**
		 * The meta object literal for the '<em><b>Calculated Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTYPED__CALCULATED_TYPE = eINSTANCE
				.getMTyped_CalculatedType();

		/**
		 * The meta object literal for the '<em><b>Calculated Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__CALCULATED_MANDATORY = eINSTANCE
				.getMTyped_CalculatedMandatory();

		/**
		 * The meta object literal for the '<em><b>Calculated Singular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__CALCULATED_SINGULAR = eINSTANCE
				.getMTyped_CalculatedSingular();

		/**
		 * The meta object literal for the '<em><b>Calculated Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTYPED__CALCULATED_TYPE_PACKAGE = eINSTANCE
				.getMTyped_CalculatedTypePackage();

		/**
		 * The meta object literal for the '<em><b>Void Type Allowed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__VOID_TYPE_ALLOWED = eINSTANCE
				.getMTyped_VoidTypeAllowed();

		/**
		 * The meta object literal for the '<em><b>Calculated Simple Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__CALCULATED_SIMPLE_TYPE = eINSTANCE
				.getMTyped_CalculatedSimpleType();

		/**
		 * The meta object literal for the '<em><b>Multiplicity Case</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MTYPED__MULTIPLICITY_CASE = eINSTANCE
				.getMTyped_MultiplicityCase();

		/**
		 * The meta object literal for the '<em><b>Multiplicity Case$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE = eINSTANCE
				.getMTyped__MultiplicityCase$Update__MultiplicityCase();

		/**
		 * The meta object literal for the '<em><b>Type As Ocl</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN = eINSTANCE
				.getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MExplicitlyTypedImpl <em>MExplicitly Typed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MExplicitlyTypedImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMExplicitlyTyped()
		 * @generated
		 */
		EClass MEXPLICITLY_TYPED = eINSTANCE.getMExplicitlyTyped();

		/**
		 * The meta object literal for the '<em><b>Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEXPLICITLY_TYPED__TYPE_PACKAGE = eINSTANCE
				.getMExplicitlyTyped_TypePackage();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEXPLICITLY_TYPED__TYPE = eINSTANCE
				.getMExplicitlyTyped_Type();

		/**
		 * The meta object literal for the '<em><b>Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED__MANDATORY = eINSTANCE
				.getMExplicitlyTyped_Mandatory();

		/**
		 * The meta object literal for the '<em><b>Singular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED__SINGULAR = eINSTANCE
				.getMExplicitlyTyped_Singular();

		/**
		 * The meta object literal for the '<em><b>EType Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED__ETYPE_NAME = eINSTANCE
				.getMExplicitlyTyped_ETypeName();

		/**
		 * The meta object literal for the '<em><b>EType Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED__ETYPE_LABEL = eINSTANCE
				.getMExplicitlyTyped_ETypeLabel();

		/**
		 * The meta object literal for the '<em><b>Correctly Typed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED__CORRECTLY_TYPED = eINSTANCE
				.getMExplicitlyTyped_CorrectlyTyped();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MVariableImpl <em>MVariable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MVariableImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMVariable()
		 * @generated
		 */
		EClass MVARIABLE = eINSTANCE.getMVariable();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl <em>MExplicitly Typed And Named</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMExplicitlyTypedAndNamed()
		 * @generated
		 */
		EClass MEXPLICITLY_TYPED_AND_NAMED = eINSTANCE
				.getMExplicitlyTypedAndNamed();

		/**
		 * The meta object literal for the '<em><b>Append Type Name To Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME = eINSTANCE
				.getMExplicitlyTypedAndNamed_AppendTypeNameToName();

		/**
		 * The meta object literal for the '<em><b>Name From Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE = eINSTANCE
				.getMExplicitlyTypedAndNamed__NameFromType();

		/**
		 * The meta object literal for the '<em><b>Short Name From Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE = eINSTANCE
				.getMExplicitlyTypedAndNamed__ShortNameFromType();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MParameterImpl <em>MParameter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MParameterImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMParameter()
		 * @generated
		 */
		EClass MPARAMETER = eINSTANCE.getMParameter();

		/**
		 * The meta object literal for the '<em><b>Containing Signature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPARAMETER__CONTAINING_SIGNATURE = eINSTANCE
				.getMParameter_ContainingSignature();

		/**
		 * The meta object literal for the '<em><b>ELabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPARAMETER__ELABEL = eINSTANCE.getMParameter_ELabel();

		/**
		 * The meta object literal for the '<em><b>Signature As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPARAMETER__SIGNATURE_AS_STRING = eINSTANCE
				.getMParameter_SignatureAsString();

		/**
		 * The meta object literal for the '<em><b>Internal EParameter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPARAMETER__INTERNAL_EPARAMETER = eINSTANCE
				.getMParameter_InternalEParameter();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPARAMETER__DO_ACTION = eINSTANCE.getMParameter_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPARAMETER___DO_ACTION$_UPDATE__MPARAMETERACTION = eINSTANCE
				.getMParameter__DoAction$Update__MParameterAction();

		/**
		 * The meta object literal for the '<em><b>Ambiguous Parameter Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPARAMETER___AMBIGUOUS_PARAMETER_NAME = eINSTANCE
				.getMParameter__AmbiguousParameterName();

		/**
		 * The meta object literal for the '<em><b>Ambiguous Parameter Short Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPARAMETER___AMBIGUOUS_PARAMETER_SHORT_NAME = eINSTANCE
				.getMParameter__AmbiguousParameterShortName();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MEditorImpl <em>MEditor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MEditorImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMEditor()
		 * @generated
		 */
		EClass MEDITOR = eINSTANCE.getMEditor();

		/**
		 * The meta object literal for the '<em><b>Reset Editor</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MEDITOR___RESET_EDITOR = eINSTANCE.getMEditor__ResetEditor();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.impl.MNamedEditorImpl <em>MNamed Editor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.impl.MNamedEditorImpl
		 * @see com.montages.mcore.impl.McorePackageImpl#getMNamedEditor()
		 * @generated
		 */
		EClass MNAMED_EDITOR = eINSTANCE.getMNamedEditor();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED_EDITOR__NAME = eINSTANCE.getMNamedEditor_Name();

		/**
		 * The meta object literal for the '<em><b>Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED_EDITOR__LABEL = eINSTANCE.getMNamedEditor_Label();

		/**
		 * The meta object literal for the '<em><b>User Visible</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MNAMED_EDITOR__USER_VISIBLE = eINSTANCE
				.getMNamedEditor_UserVisible();

		/**
		 * The meta object literal for the '<em><b>Editor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MNAMED_EDITOR__EDITOR = eINSTANCE.getMNamedEditor_Editor();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MComponentAction <em>MComponent Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MComponentAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMComponentAction()
		 * @generated
		 */
		EEnum MCOMPONENT_ACTION = eINSTANCE.getMComponentAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.TopLevelDomain <em>Top Level Domain</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.TopLevelDomain
		 * @see com.montages.mcore.impl.McorePackageImpl#getTopLevelDomain()
		 * @generated
		 */
		EEnum TOP_LEVEL_DOMAIN = eINSTANCE.getTopLevelDomain();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MPackageAction <em>MPackage Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MPackageAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPackageAction()
		 * @generated
		 */
		EEnum MPACKAGE_ACTION = eINSTANCE.getMPackageAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.SimpleType <em>Simple Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.SimpleType
		 * @see com.montages.mcore.impl.McorePackageImpl#getSimpleType()
		 * @generated
		 */
		EEnum SIMPLE_TYPE = eINSTANCE.getSimpleType();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MClassifierAction <em>MClassifier Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MClassifierAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMClassifierAction()
		 * @generated
		 */
		EEnum MCLASSIFIER_ACTION = eINSTANCE.getMClassifierAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.OrderingStrategy <em>Ordering Strategy</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.OrderingStrategy
		 * @see com.montages.mcore.impl.McorePackageImpl#getOrderingStrategy()
		 * @generated
		 */
		EEnum ORDERING_STRATEGY = eINSTANCE.getOrderingStrategy();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MLiteralAction <em>MLiteral Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MLiteralAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMLiteralAction()
		 * @generated
		 */
		EEnum MLITERAL_ACTION = eINSTANCE.getMLiteralAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MPropertiesGroupAction <em>MProperties Group Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MPropertiesGroupAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertiesGroupAction()
		 * @generated
		 */
		EEnum MPROPERTIES_GROUP_ACTION = eINSTANCE.getMPropertiesGroupAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MPropertyAction <em>MProperty Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MPropertyAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMPropertyAction()
		 * @generated
		 */
		EEnum MPROPERTY_ACTION = eINSTANCE.getMPropertyAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.PropertyBehavior <em>Property Behavior</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.PropertyBehavior
		 * @see com.montages.mcore.impl.McorePackageImpl#getPropertyBehavior()
		 * @generated
		 */
		EEnum PROPERTY_BEHAVIOR = eINSTANCE.getPropertyBehavior();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MOperationSignatureAction <em>MOperation Signature Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MOperationSignatureAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMOperationSignatureAction()
		 * @generated
		 */
		EEnum MOPERATION_SIGNATURE_ACTION = eINSTANCE
				.getMOperationSignatureAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MultiplicityCase <em>Multiplicity Case</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MultiplicityCase
		 * @see com.montages.mcore.impl.McorePackageImpl#getMultiplicityCase()
		 * @generated
		 */
		EEnum MULTIPLICITY_CASE = eINSTANCE.getMultiplicityCase();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.ClassifierKind <em>Classifier Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.ClassifierKind
		 * @see com.montages.mcore.impl.McorePackageImpl#getClassifierKind()
		 * @generated
		 */
		EEnum CLASSIFIER_KIND = eINSTANCE.getClassifierKind();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.PropertyKind <em>Property Kind</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.PropertyKind
		 * @see com.montages.mcore.impl.McorePackageImpl#getPropertyKind()
		 * @generated
		 */
		EEnum PROPERTY_KIND = eINSTANCE.getPropertyKind();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.MParameterAction <em>MParameter Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.MParameterAction
		 * @see com.montages.mcore.impl.McorePackageImpl#getMParameterAction()
		 * @generated
		 */
		EEnum MPARAMETER_ACTION = eINSTANCE.getMParameterAction();

	}

} //McorePackage
