/**
 */
package com.montages.mcore.updates.impl;

import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.langlets.acore.AcorePackage;
import org.langlets.acore.classifiers.ClassifiersPackage;
import org.xocl.semantics.SemanticsPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.impl.AnnotationsPackageImpl;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.ExpressionsPackageImpl;
import com.montages.mcore.impl.McorePackageImpl;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.ObjectsPackageImpl;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.tables.impl.TablesPackageImpl;
import com.montages.mcore.updates.Dummy;
import com.montages.mcore.updates.UpdateMode;
import com.montages.mcore.updates.UpdatesFactory;
import com.montages.mcore.updates.UpdatesPackage;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UpdatesPackageImpl extends EPackageImpl implements UpdatesPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dummyEClass = null;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum updateModeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.updates.UpdatesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private UpdatesPackageImpl() {
		super(eNS_URI, UpdatesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link UpdatesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static UpdatesPackage init() {
		if (isInited)
			return (UpdatesPackage) EPackage.Registry.INSTANCE
					.getEPackage(UpdatesPackage.eNS_URI);

		// Obtain or create and register package
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new UpdatesPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(
								McorePackage.eNS_URI)
						: McorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(AnnotationsPackage.eNS_URI)
								: AnnotationsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ExpressionsPackage.eNS_URI)
								: ExpressionsPackage.eINSTANCE);
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ObjectsPackage.eNS_URI) instanceof ObjectsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ObjectsPackage.eNS_URI)
								: ObjectsPackage.eINSTANCE);
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE
								.getEPackage(TablesPackage.eNS_URI)
						: TablesPackage.eINSTANCE);

		// Create package meta-data objects
		theUpdatesPackage.createPackageContents();
		theMcorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theObjectsPackage.createPackageContents();
		theTablesPackage.createPackageContents();

		// Initialize created meta-data
		theUpdatesPackage.initializePackageContents();
		theMcorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theObjectsPackage.initializePackageContents();
		theTablesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theUpdatesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(UpdatesPackage.eNS_URI,
				theUpdatesPackage);
		return theUpdatesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDummy() {
		return dummyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDummy_Id21() {
		return (EAttribute) dummyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDummy_XUpdate() {
		return (EReference) dummyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDummy_AAttribute() {
		return (EReference) dummyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUpdateMode() {
		return updateModeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpdatesFactory getUpdatesFactory() {
		return (UpdatesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		dummyEClass = createEClass(DUMMY);
		createEAttribute(dummyEClass, DUMMY__ID21);
		createEReference(dummyEClass, DUMMY__XUPDATE);
		createEReference(dummyEClass, DUMMY__AATTRIBUTE);

		// Create enums
		updateModeEEnum = createEEnum(UPDATE_MODE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(dummyEClass, Dummy.class, "Dummy", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDummy_Id21(), ecorePackage.getEString(), "id21", null,
				0, 1, Dummy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDummy_XUpdate(), theSemanticsPackage.getXUpdate(),
				null, "xUpdate", null, 0, 1, Dummy.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDummy_AAttribute(),
				theClassifiersPackage.getAAttribute(), null, "aAttribute", null,
				0, 1, Dummy.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(updateModeEEnum, UpdateMode.class, "UpdateMode");
		addEEnumLiteral(updateModeEEnum, UpdateMode.REDEFINE);
		addEEnumLiteral(updateModeEEnum, UpdateMode.ADD);
		addEEnumLiteral(updateModeEEnum, UpdateMode.REMOVE);
	}

} //UpdatesPackageImpl
