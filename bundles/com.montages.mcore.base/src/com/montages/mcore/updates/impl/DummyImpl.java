/**
 */
package com.montages.mcore.updates.impl;

import com.montages.mcore.updates.Dummy;
import com.montages.mcore.updates.UpdatesPackage;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.langlets.acore.classifiers.AAttribute;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dummy</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.updates.impl.DummyImpl#getId21 <em>Id21</em>}</li>
 *   <li>{@link com.montages.mcore.updates.impl.DummyImpl#getXUpdate <em>XUpdate</em>}</li>
 *   <li>{@link com.montages.mcore.updates.impl.DummyImpl#getAAttribute <em>AAttribute</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class DummyImpl extends MinimalEObjectImpl.Container implements Dummy {
	/**
	 * The default value of the '{@link #getId21() <em>Id21</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId21()
	 * @generated
	 * @ordered
	 */
	protected static final String ID21_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId21() <em>Id21</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId21()
	 * @generated
	 * @ordered
	 */
	protected String id21 = ID21_EDEFAULT;

	/**
	 * This is true if the Id21 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean id21ESet;

	/**
	 * The cached value of the '{@link #getXUpdate() <em>XUpdate</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getXUpdate()
	 * @generated
	 * @ordered
	 */
	protected XUpdate xUpdate;

	/**
	 * This is true if the XUpdate reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean xUpdateESet;

	/**
	 * The cached value of the '{@link #getAAttribute() <em>AAttribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAttribute()
	 * @generated
	 * @ordered
	 */
	protected AAttribute aAttribute;

	/**
	 * This is true if the AAttribute reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aAttributeESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DummyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UpdatesPackage.Literals.DUMMY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId21() {
		return id21;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId21(String newId21) {
		String oldId21 = id21;
		id21 = newId21;
		boolean oldId21ESet = id21ESet;
		id21ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					UpdatesPackage.DUMMY__ID21, oldId21, id21, !oldId21ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetId21() {
		String oldId21 = id21;
		boolean oldId21ESet = id21ESet;
		id21 = ID21_EDEFAULT;
		id21ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					UpdatesPackage.DUMMY__ID21, oldId21, ID21_EDEFAULT,
					oldId21ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetId21() {
		return id21ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate getXUpdate() {
		if (xUpdate != null && xUpdate.eIsProxy()) {
			InternalEObject oldXUpdate = (InternalEObject) xUpdate;
			xUpdate = (XUpdate) eResolveProxy(oldXUpdate);
			if (xUpdate != oldXUpdate) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							UpdatesPackage.DUMMY__XUPDATE, oldXUpdate,
							xUpdate));
			}
		}
		return xUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate basicGetXUpdate() {
		return xUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setXUpdate(XUpdate newXUpdate) {
		XUpdate oldXUpdate = xUpdate;
		xUpdate = newXUpdate;
		boolean oldXUpdateESet = xUpdateESet;
		xUpdateESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					UpdatesPackage.DUMMY__XUPDATE, oldXUpdate, xUpdate,
					!oldXUpdateESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetXUpdate() {
		XUpdate oldXUpdate = xUpdate;
		boolean oldXUpdateESet = xUpdateESet;
		xUpdate = null;
		xUpdateESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					UpdatesPackage.DUMMY__XUPDATE, oldXUpdate, null,
					oldXUpdateESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetXUpdate() {
		return xUpdateESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAttribute getAAttribute() {
		if (aAttribute != null && aAttribute.eIsProxy()) {
			InternalEObject oldAAttribute = (InternalEObject) aAttribute;
			aAttribute = (AAttribute) eResolveProxy(oldAAttribute);
			if (aAttribute != oldAAttribute) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							UpdatesPackage.DUMMY__AATTRIBUTE, oldAAttribute,
							aAttribute));
			}
		}
		return aAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAttribute basicGetAAttribute() {
		return aAttribute;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAAttribute(AAttribute newAAttribute) {
		AAttribute oldAAttribute = aAttribute;
		aAttribute = newAAttribute;
		boolean oldAAttributeESet = aAttributeESet;
		aAttributeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					UpdatesPackage.DUMMY__AATTRIBUTE, oldAAttribute, aAttribute,
					!oldAAttributeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAAttribute() {
		AAttribute oldAAttribute = aAttribute;
		boolean oldAAttributeESet = aAttributeESet;
		aAttribute = null;
		aAttributeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					UpdatesPackage.DUMMY__AATTRIBUTE, oldAAttribute, null,
					oldAAttributeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAAttribute() {
		return aAttributeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case UpdatesPackage.DUMMY__ID21:
			return getId21();
		case UpdatesPackage.DUMMY__XUPDATE:
			if (resolve)
				return getXUpdate();
			return basicGetXUpdate();
		case UpdatesPackage.DUMMY__AATTRIBUTE:
			if (resolve)
				return getAAttribute();
			return basicGetAAttribute();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case UpdatesPackage.DUMMY__ID21:
			setId21((String) newValue);
			return;
		case UpdatesPackage.DUMMY__XUPDATE:
			setXUpdate((XUpdate) newValue);
			return;
		case UpdatesPackage.DUMMY__AATTRIBUTE:
			setAAttribute((AAttribute) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case UpdatesPackage.DUMMY__ID21:
			unsetId21();
			return;
		case UpdatesPackage.DUMMY__XUPDATE:
			unsetXUpdate();
			return;
		case UpdatesPackage.DUMMY__AATTRIBUTE:
			unsetAAttribute();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case UpdatesPackage.DUMMY__ID21:
			return isSetId21();
		case UpdatesPackage.DUMMY__XUPDATE:
			return isSetXUpdate();
		case UpdatesPackage.DUMMY__AATTRIBUTE:
			return isSetAAttribute();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (id21: ");
		if (id21ESet)
			result.append(id21);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //DummyImpl
