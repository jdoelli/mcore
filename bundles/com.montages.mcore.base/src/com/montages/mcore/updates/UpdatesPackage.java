/**
 */
package com.montages.mcore.updates;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mcore.updates.UpdatesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.mcore'"
 * @generated
 */
public interface UpdatesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mcore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "updates";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mCore/MCore/Updates";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mcore.updates";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UpdatesPackage eINSTANCE = com.montages.mcore.updates.impl.UpdatesPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link com.montages.mcore.updates.impl.DummyImpl <em>Dummy</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.updates.impl.DummyImpl
	 * @see com.montages.mcore.updates.impl.UpdatesPackageImpl#getDummy()
	 * @generated
	 */
	int DUMMY = 0;

	/**
	 * The feature id for the '<em><b>Id21</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY__ID21 = 0;

	/**
	 * The feature id for the '<em><b>XUpdate</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY__XUPDATE = 1;

	/**
	 * The feature id for the '<em><b>AAttribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY__AATTRIBUTE = 2;

	/**
	 * The number of structural features of the '<em>Dummy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Dummy</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DUMMY_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.updates.UpdateMode <em>Update Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.updates.UpdateMode
	 * @see com.montages.mcore.updates.impl.UpdatesPackageImpl#getUpdateMode()
	 * @generated
	 */
	int UPDATE_MODE = 1;

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.updates.Dummy <em>Dummy</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dummy</em>'.
	 * @see com.montages.mcore.updates.Dummy
	 * @generated
	 */
	EClass getDummy();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.updates.Dummy#getId21 <em>Id21</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id21</em>'.
	 * @see com.montages.mcore.updates.Dummy#getId21()
	 * @see #getDummy()
	 * @generated
	 */
	EAttribute getDummy_Id21();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.updates.Dummy#getXUpdate <em>XUpdate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>XUpdate</em>'.
	 * @see com.montages.mcore.updates.Dummy#getXUpdate()
	 * @see #getDummy()
	 * @generated
	 */
	EReference getDummy_XUpdate();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.updates.Dummy#getAAttribute <em>AAttribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAttribute</em>'.
	 * @see com.montages.mcore.updates.Dummy#getAAttribute()
	 * @see #getDummy()
	 * @generated
	 */
	EReference getDummy_AAttribute();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.updates.UpdateMode <em>Update Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Update Mode</em>'.
	 * @see com.montages.mcore.updates.UpdateMode
	 * @generated
	 */
	EEnum getUpdateMode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UpdatesFactory getUpdatesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mcore.updates.impl.DummyImpl <em>Dummy</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.updates.impl.DummyImpl
		 * @see com.montages.mcore.updates.impl.UpdatesPackageImpl#getDummy()
		 * @generated
		 */
		EClass DUMMY = eINSTANCE.getDummy();
		/**
		 * The meta object literal for the '<em><b>Id21</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DUMMY__ID21 = eINSTANCE.getDummy_Id21();
		/**
		 * The meta object literal for the '<em><b>XUpdate</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DUMMY__XUPDATE = eINSTANCE.getDummy_XUpdate();
		/**
		 * The meta object literal for the '<em><b>AAttribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DUMMY__AATTRIBUTE = eINSTANCE.getDummy_AAttribute();
		/**
		 * The meta object literal for the '{@link com.montages.mcore.updates.UpdateMode <em>Update Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.updates.UpdateMode
		 * @see com.montages.mcore.updates.impl.UpdatesPackageImpl#getUpdateMode()
		 * @generated
		 */
		EEnum UPDATE_MODE = eINSTANCE.getUpdateMode();

	}

} //UpdatesPackage
