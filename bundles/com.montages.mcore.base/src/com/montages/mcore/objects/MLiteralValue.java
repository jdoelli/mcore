/**
 */
package com.montages.mcore.objects;

import com.montages.mcore.MLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MLiteralValue#getLiteralValue <em>Literal Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMLiteralValue()
 * @model annotation="http://www.xocl.org/OCL label='if self.literalValue.oclIsUndefined() then \'LITERAL VALUE MISSING\' else\r\nself.literalValue.eName endif\r\n\r\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Literal Value\'\n' correctAndDefinedValueDerive='let var1: Boolean = let e1: Boolean = if (let chain11: mcore::MLiteral = literalValue in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctLiteralValue())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: mcore::MLiteral = literalValue in\nif chain11 <> null then true else false \n  endif)= null or (correctLiteralValue())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n'"
 * @generated
 */

public interface MLiteralValue extends MValue {
	/**
	 * Returns the value of the '<em><b>Literal Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Literal Value</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Literal Value</em>' reference.
	 * @see #isSetLiteralValue()
	 * @see #unsetLiteralValue()
	 * @see #setLiteralValue(MLiteral)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMLiteralValue_LiteralValue()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='let p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() \r\n  then  let pos:Integer\r\n                   =self.containingPropertyInstance\r\n                        .internalLiteralValue->indexOf(self) in\r\n    if pos=1 \r\n       then true\r\n    else if trg.containingEnumeration.oclIsUndefined() \r\n       then false\r\n    else let firstLiteralValue:MLiteralValue \r\n                 =  self.containingPropertyInstance \r\n                       .internalLiteralValue->first() in\r\n       if firstLiteralValue.literalValue.oclIsUndefined() \r\n         then false\r\n         else firstLiteralValue.literalValue.containingEnumeration\r\n                     = trg.containingEnumeration endif endif endif\r\nelse if p.type.oclIsUndefined() \r\n  then false \r\n  else p.type.allLiterals()->includes(trg)\r\nendif endif'"
	 * @generated
	 */
	MLiteral getLiteralValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MLiteralValue#getLiteralValue <em>Literal Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Literal Value</em>' reference.
	 * @see #isSetLiteralValue()
	 * @see #unsetLiteralValue()
	 * @see #getLiteralValue()
	 * @generated
	 */
	void setLiteralValue(MLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MLiteralValue#getLiteralValue <em>Literal Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLiteralValue()
	 * @see #getLiteralValue()
	 * @see #setLiteralValue(MLiteral)
	 * @generated
	 */
	void unsetLiteralValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MLiteralValue#getLiteralValue <em>Literal Value</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Literal Value</em>' reference is set.
	 * @see #unsetLiteralValue()
	 * @see #getLiteralValue()
	 * @see #setLiteralValue(MLiteral)
	 * @generated
	 */
	boolean isSetLiteralValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:MProperty = pi.property in\r\nif p.oclIsUndefined() then\r\n    pi.internalDataValue->isEmpty()\r\n    and pi.internalReferencedObject->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\nelse\r\np.kind = PropertyKind::Attribute \r\nand p.type.kind = ClassifierKind::Enumeration\r\nand (if self.literalValue.oclIsUndefined() then true else\r\np.type.allLiterals()->includes(self.literalValue)\r\nendif)\r\nendif'"
	 * @generated
	 */
	Boolean correctLiteralValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalLiteralValue->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif'"
	 * @generated
	 */
	Boolean correctMultiplicity();

} // MLiteralValue
