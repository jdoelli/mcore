/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import com.montages.mcore.McorePackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.mcore'"
 * @generated
 */
public interface ObjectsPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mcore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "objects";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mCore/MCore/Objects";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mcore.objects";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ObjectsPackage eINSTANCE = com.montages.mcore.objects.impl.ObjectsPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MResourceFolderImpl <em>MResource Folder</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MResourceFolderImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceFolder()
	 * @generated
	 */
	int MRESOURCE_FOLDER = 0;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__KIND_LABEL = McorePackage.MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__RENDERED_KIND_LABEL = McorePackage.MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__INDENT_LEVEL = McorePackage.MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__DESCRIPTION = McorePackage.MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__AS_TEXT = McorePackage.MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__SPECIAL_ENAME = McorePackage.MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__NAME = McorePackage.MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__SHORT_NAME = McorePackage.MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__ENAME = McorePackage.MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__FULL_LABEL = McorePackage.MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__LOCAL_STRUCTURAL_NAME = McorePackage.MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__CALCULATED_NAME = McorePackage.MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__CALCULATED_SHORT_NAME = McorePackage.MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__CORRECT_NAME = McorePackage.MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__CORRECT_SHORT_NAME = McorePackage.MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__RESOURCE = McorePackage.MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Folder</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__FOLDER = McorePackage.MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Root Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__ROOT_FOLDER = McorePackage.MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER__DO_ACTION = McorePackage.MNAMED_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MResource Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER_FEATURE_COUNT = McorePackage.MNAMED_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___INDENTATION_SPACES = McorePackage.MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___INDENTATION_SPACES__INTEGER = McorePackage.MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___NEW_LINE_STRING = McorePackage.MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___SAME_NAME__MNAMED = McorePackage.MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___SAME_SHORT_NAME__MNAMED = McorePackage.MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___SAME_STRING__STRING_STRING = McorePackage.MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___STRING_EMPTY__STRING = McorePackage.MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___DO_ACTION$_UPDATE__MRESOURCEFOLDERACTION = McorePackage.MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Update Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY = McorePackage.MNAMED_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Update Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY_MPROPERTY = McorePackage.MNAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The number of operations of the '<em>MResource Folder</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FOLDER_OPERATION_COUNT = McorePackage.MNAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MResourceImpl <em>MResource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MResourceImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResource()
	 * @generated
	 */
	int MRESOURCE = 1;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__KIND_LABEL = McorePackage.MNAMED__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__RENDERED_KIND_LABEL = McorePackage.MNAMED__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__INDENT_LEVEL = McorePackage.MNAMED__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__DESCRIPTION = McorePackage.MNAMED__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__AS_TEXT = McorePackage.MNAMED__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__SPECIAL_ENAME = McorePackage.MNAMED__SPECIAL_ENAME;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__NAME = McorePackage.MNAMED__NAME;

	/**
	 * The feature id for the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__SHORT_NAME = McorePackage.MNAMED__SHORT_NAME;

	/**
	 * The feature id for the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__ENAME = McorePackage.MNAMED__ENAME;

	/**
	 * The feature id for the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__FULL_LABEL = McorePackage.MNAMED__FULL_LABEL;

	/**
	 * The feature id for the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__LOCAL_STRUCTURAL_NAME = McorePackage.MNAMED__LOCAL_STRUCTURAL_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__CALCULATED_NAME = McorePackage.MNAMED__CALCULATED_NAME;

	/**
	 * The feature id for the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__CALCULATED_SHORT_NAME = McorePackage.MNAMED__CALCULATED_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__CORRECT_NAME = McorePackage.MNAMED__CORRECT_NAME;

	/**
	 * The feature id for the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__CORRECT_SHORT_NAME = McorePackage.MNAMED__CORRECT_SHORT_NAME;

	/**
	 * The feature id for the '<em><b>Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__OBJECT = McorePackage.MNAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Root Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__ROOT_OBJECT_PACKAGE = McorePackage.MNAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Containing Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__CONTAINING_FOLDER = McorePackage.MNAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__ID = McorePackage.MNAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE__DO_ACTION = McorePackage.MNAMED_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>MResource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_FEATURE_COUNT = McorePackage.MNAMED_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___INDENTATION_SPACES = McorePackage.MNAMED___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___INDENTATION_SPACES__INTEGER = McorePackage.MNAMED___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___NEW_LINE_STRING = McorePackage.MNAMED___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Same Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___SAME_NAME__MNAMED = McorePackage.MNAMED___SAME_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same Short Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___SAME_SHORT_NAME__MNAMED = McorePackage.MNAMED___SAME_SHORT_NAME__MNAMED;

	/**
	 * The operation id for the '<em>Same String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___SAME_STRING__STRING_STRING = McorePackage.MNAMED___SAME_STRING__STRING_STRING;

	/**
	 * The operation id for the '<em>String Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___STRING_EMPTY__STRING = McorePackage.MNAMED___STRING_EMPTY__STRING;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___DO_ACTION$_UPDATE__MRESOURCEACTION = McorePackage.MNAMED_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___UPDATE__MPROPERTY = McorePackage.MNAMED_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Update Objects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___UPDATE_OBJECTS__MPROPERTY_MPROPERTY = McorePackage.MNAMED_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Update Containment To Stored Feature</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___UPDATE_CONTAINMENT_TO_STORED_FEATURE__MPROPERTY_MOBJECT = McorePackage.MNAMED_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Update Stored To Containment Feature</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___UPDATE_STORED_TO_CONTAINMENT_FEATURE__MPROPERTY_MOBJECT = McorePackage.MNAMED_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Add Objects</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE___ADD_OBJECTS__MPROPERTY_MOBJECT = McorePackage.MNAMED_OPERATION_COUNT
			+ 5;

	/**
	 * The number of operations of the '<em>MResource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MRESOURCE_OPERATION_COUNT = McorePackage.MNAMED_OPERATION_COUNT + 6;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MObjectImpl <em>MObject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MObjectImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObject()
	 * @generated
	 */
	int MOBJECT = 2;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__RENDERED_KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__INDENT_LEVEL = McorePackage.MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__DESCRIPTION = McorePackage.MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__AS_TEXT = McorePackage.MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Property Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__PROPERTY_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Table</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__TABLE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Create Type In Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__CREATE_TYPE_IN_PACKAGE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Has Property Instance Not Of Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__NATURE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Class Name Or Nature Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>Nature Needed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__NATURE_NEEDED = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Nature Missing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__NATURE_MISSING = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__TYPE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__TYPE_PACKAGE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Nature EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__NATURE_ENAME = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Calculated Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__CALCULATED_NATURE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Initialization Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__INITIALIZATION_TYPE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 12;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__ID = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT + 13;

	/**
	 * The feature id for the '<em><b>Numeric Id Of Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__NUMERIC_ID_OF_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 14;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__CONTAINING_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 15;

	/**
	 * The feature id for the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__CONTAINING_PROPERTY_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 16;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__RESOURCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT + 17;

	/**
	 * The feature id for the '<em><b>Referenced By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__REFERENCED_BY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 18;

	/**
	 * The feature id for the '<em><b>Wrong Property Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__WRONG_PROPERTY_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>Initialized Containment Levels</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__INITIALIZED_CONTAINMENT_LEVELS = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 20;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__DO_ACTION = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 21;

	/**
	 * The feature id for the '<em><b>Status As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__STATUS_AS_TEXT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 22;

	/**
	 * The feature id for the '<em><b>Derivations As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT__DERIVATIONS_AS_TEXT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 23;

	/**
	 * The number of structural features of the '<em>MObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_FEATURE_COUNT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 24;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___INDENTATION_SPACES = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___INDENTATION_SPACES__INTEGER = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___NEW_LINE_STRING = McorePackage.MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Create Type In Package$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CREATE_TYPE_IN_PACKAGE$_UPDATE__MPACKAGE = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Class Name Or Nature Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CLASS_NAME_OR_NATURE_DEFINITION$_UPDATE__STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___DO_ACTION$_UPDATE__MOBJECTACTION = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Property Value As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___PROPERTY_VALUE_AS_STRING__MPROPERTY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Property Instance From Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___PROPERTY_INSTANCE_FROM_PROPERTY__MPROPERTY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Is Instance Of</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___IS_INSTANCE_OF__MCLASSIFIER = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Class Name Or Nature Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CLASS_NAME_OR_NATURE_UPDATE__STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Container Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CONTAINER_ID = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Local Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___LOCAL_ID = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Id Property Based Id</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___ID_PROPERTY_BASED_ID = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Id Properties As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___ID_PROPERTIES_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 10;

	/**
	 * The operation id for the '<em>Is Root Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___IS_ROOT_OBJECT = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 11;

	/**
	 * The operation id for the '<em>Correctly Typed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CORRECTLY_TYPED = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 12;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___CORRECT_MULTIPLICITY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 13;

	/**
	 * The operation id for the '<em>Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___DO_ACTION_UPDATE__MOBJECTACTION = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 14;

	/**
	 * The operation id for the '<em>Invoke Set Do Action Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 15;

	/**
	 * The number of operations of the '<em>MObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_OPERATION_COUNT = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 16;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl <em>MProperty Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MPropertyInstanceImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMPropertyInstance()
	 * @generated
	 */
	int MPROPERTY_INSTANCE = 3;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__RENDERED_KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__INDENT_LEVEL = McorePackage.MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DESCRIPTION = McorePackage.MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__AS_TEXT = McorePackage.MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Internal Data Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Internal Literal Value</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Internal Referenced Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 2;

	/**
	 * The feature id for the '<em><b>Internal Contained Object</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 3;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__CONTAINING_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 4;

	/**
	 * The feature id for the '<em><b>Duplicate Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DUPLICATE_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 5;

	/**
	 * The feature id for the '<em><b>All Instances Of This Property</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 6;

	/**
	 * The feature id for the '<em><b>Wrong Internal Contained Object Due To Kind</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 7;

	/**
	 * The feature id for the '<em><b>Wrong Internal Object Reference Due To Kind</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 8;

	/**
	 * The feature id for the '<em><b>Wrong Internal Literal Value Due To Kind</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 9;

	/**
	 * The feature id for the '<em><b>Wrong Internal Data Value Due To Kind</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 10;

	/**
	 * The feature id for the '<em><b>Numeric Id Of Property Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 11;

	/**
	 * The feature id for the '<em><b>Derived Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DERIVED_CONTAINMENT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 12;

	/**
	 * The feature id for the '<em><b>Local Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__LOCAL_KEY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 13;

	/**
	 * The feature id for the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__PROPERTY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 14;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__KEY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 15;

	/**
	 * The feature id for the '<em><b>Property Name Or Local Key Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 16;

	/**
	 * The feature id for the '<em><b>Wrong Local Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__WRONG_LOCAL_KEY = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 17;

	/**
	 * The feature id for the '<em><b>Derived Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DERIVED_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 18;

	/**
	 * The feature id for the '<em><b>Property Kind As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 19;

	/**
	 * The feature id for the '<em><b>Correct Property Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 20;

	/**
	 * The feature id for the '<em><b>Do Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DO_ACTION = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 21;

	/**
	 * The feature id for the '<em><b>Do Add Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__DO_ADD_VALUE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 22;

	/**
	 * The feature id for the '<em><b>Simple Type From Values</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 23;

	/**
	 * The feature id for the '<em><b>Enumeration From Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 24;

	/**
	 * The number of structural features of the '<em>MProperty Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE_FEATURE_COUNT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 25;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___INDENTATION_SPACES = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___INDENTATION_SPACES__INTEGER = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___NEW_LINE_STRING = McorePackage.MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Property Name Or Local Key Definition$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION$_UPDATE__STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Do Action$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___DO_ACTION$_UPDATE__MPROPERTYINSTANCEACTION = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The operation id for the '<em>Do Add Value$ Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___DO_ADD_VALUE$_UPDATE__BOOLEAN = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 2;

	/**
	 * The operation id for the '<em>Correct Property</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___CORRECT_PROPERTY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___CORRECT_MULTIPLICITY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 4;

	/**
	 * The operation id for the '<em>Property Value As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___PROPERTY_VALUE_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 5;

	/**
	 * The operation id for the '<em>Internal Referenced Objects As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___INTERNAL_REFERENCED_OBJECTS_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 6;

	/**
	 * The operation id for the '<em>Internal Data Values As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___INTERNAL_DATA_VALUES_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 7;

	/**
	 * The operation id for the '<em>Internal Literal Values As String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___INTERNAL_LITERAL_VALUES_AS_STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 8;

	/**
	 * The operation id for the '<em>Choosable Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___CHOOSABLE_PROPERTIES = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 9;

	/**
	 * The operation id for the '<em>Property Name Or Local Key Definition Update</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_UPDATE__STRING = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 10;

	/**
	 * The number of operations of the '<em>MProperty Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MPROPERTY_INSTANCE_OPERATION_COUNT = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 11;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MValueImpl <em>MValue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MValueImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMValue()
	 * @generated
	 */
	int MVALUE = 4;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__RENDERED_KIND_LABEL = McorePackage.MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__INDENT_LEVEL = McorePackage.MREPOSITORY_ELEMENT__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__DESCRIPTION = McorePackage.MREPOSITORY_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__AS_TEXT = McorePackage.MREPOSITORY_ELEMENT__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__CONTAINING_PROPERTY_INSTANCE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 0;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__CONTAINING_OBJECT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 1;

	/**
	 * The feature id for the '<em><b>Correct And Defined Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE__CORRECT_AND_DEFINED_VALUE = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 2;

	/**
	 * The number of structural features of the '<em>MValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE_FEATURE_COUNT = McorePackage.MREPOSITORY_ELEMENT_FEATURE_COUNT
			+ 3;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE___INDENTATION_SPACES = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE___INDENTATION_SPACES__INTEGER = McorePackage.MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE___NEW_LINE_STRING = McorePackage.MREPOSITORY_ELEMENT___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE___CORRECT_MULTIPLICITY = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 0;

	/**
	 * The number of operations of the '<em>MValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MVALUE_OPERATION_COUNT = McorePackage.MREPOSITORY_ELEMENT_OPERATION_COUNT
			+ 1;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MObjectReferenceImpl <em>MObject Reference</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MObjectReferenceImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObjectReference()
	 * @generated
	 */
	int MOBJECT_REFERENCE = 5;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__KIND_LABEL = MVALUE__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__RENDERED_KIND_LABEL = MVALUE__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__INDENT_LEVEL = MVALUE__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__DESCRIPTION = MVALUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__AS_TEXT = MVALUE__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__CONTAINING_PROPERTY_INSTANCE = MVALUE__CONTAINING_PROPERTY_INSTANCE;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__CONTAINING_OBJECT = MVALUE__CONTAINING_OBJECT;

	/**
	 * The feature id for the '<em><b>Correct And Defined Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__CORRECT_AND_DEFINED_VALUE = MVALUE__CORRECT_AND_DEFINED_VALUE;

	/**
	 * The feature id for the '<em><b>Referenced Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__REFERENCED_OBJECT = MVALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type Of Referenced Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT = MVALUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Id Of Referenced Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT = MVALUE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Choosable Reference</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE__CHOOSABLE_REFERENCE = MVALUE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>MObject Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE_FEATURE_COUNT = MVALUE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE___INDENTATION_SPACES = MVALUE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE___INDENTATION_SPACES__INTEGER = MVALUE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE___NEW_LINE_STRING = MVALUE___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Correctly Typed Referenced Object</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE___CORRECTLY_TYPED_REFERENCED_OBJECT = MVALUE_OPERATION_COUNT
			+ 0;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE___CORRECT_MULTIPLICITY = MVALUE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>MObject Reference</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_REFERENCE_OPERATION_COUNT = MVALUE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MLiteralValueImpl <em>MLiteral Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MLiteralValueImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMLiteralValue()
	 * @generated
	 */
	int MLITERAL_VALUE = 6;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__KIND_LABEL = MVALUE__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__RENDERED_KIND_LABEL = MVALUE__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__INDENT_LEVEL = MVALUE__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__DESCRIPTION = MVALUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__AS_TEXT = MVALUE__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__CONTAINING_PROPERTY_INSTANCE = MVALUE__CONTAINING_PROPERTY_INSTANCE;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__CONTAINING_OBJECT = MVALUE__CONTAINING_OBJECT;

	/**
	 * The feature id for the '<em><b>Correct And Defined Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__CORRECT_AND_DEFINED_VALUE = MVALUE__CORRECT_AND_DEFINED_VALUE;

	/**
	 * The feature id for the '<em><b>Literal Value</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE__LITERAL_VALUE = MVALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>MLiteral Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE_FEATURE_COUNT = MVALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE___INDENTATION_SPACES = MVALUE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE___INDENTATION_SPACES__INTEGER = MVALUE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE___NEW_LINE_STRING = MVALUE___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Correct Literal Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE___CORRECT_LITERAL_VALUE = MVALUE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE___CORRECT_MULTIPLICITY = MVALUE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>MLiteral Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MLITERAL_VALUE_OPERATION_COUNT = MVALUE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.impl.MDataValueImpl <em>MData Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.impl.MDataValueImpl
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMDataValue()
	 * @generated
	 */
	int MDATA_VALUE = 7;

	/**
	 * The feature id for the '<em><b>Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__KIND_LABEL = MVALUE__KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__RENDERED_KIND_LABEL = MVALUE__RENDERED_KIND_LABEL;

	/**
	 * The feature id for the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__INDENT_LEVEL = MVALUE__INDENT_LEVEL;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__DESCRIPTION = MVALUE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__AS_TEXT = MVALUE__AS_TEXT;

	/**
	 * The feature id for the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__CONTAINING_PROPERTY_INSTANCE = MVALUE__CONTAINING_PROPERTY_INSTANCE;

	/**
	 * The feature id for the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__CONTAINING_OBJECT = MVALUE__CONTAINING_OBJECT;

	/**
	 * The feature id for the '<em><b>Correct And Defined Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__CORRECT_AND_DEFINED_VALUE = MVALUE__CORRECT_AND_DEFINED_VALUE;

	/**
	 * The feature id for the '<em><b>Data Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE__DATA_VALUE = MVALUE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>MData Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE_FEATURE_COUNT = MVALUE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE___INDENTATION_SPACES = MVALUE___INDENTATION_SPACES;

	/**
	 * The operation id for the '<em>Indentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE___INDENTATION_SPACES__INTEGER = MVALUE___INDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>New Line String</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE___NEW_LINE_STRING = MVALUE___NEW_LINE_STRING;

	/**
	 * The operation id for the '<em>Correct Data Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE___CORRECT_DATA_VALUE = MVALUE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Correct Multiplicity</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE___CORRECT_MULTIPLICITY = MVALUE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>MData Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MDATA_VALUE_OPERATION_COUNT = MVALUE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.MResourceFolderAction <em>MResource Folder Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.MResourceFolderAction
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceFolderAction()
	 * @generated
	 */
	int MRESOURCE_FOLDER_ACTION = 8;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.MResourceAction <em>MResource Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.MResourceAction
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceAction()
	 * @generated
	 */
	int MRESOURCE_ACTION = 9;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.MObjectAction <em>MObject Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.MObjectAction
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObjectAction()
	 * @generated
	 */
	int MOBJECT_ACTION = 10;

	/**
	 * The meta object id for the '{@link com.montages.mcore.objects.MPropertyInstanceAction <em>MProperty Instance Action</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.objects.MPropertyInstanceAction
	 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMPropertyInstanceAction()
	 * @generated
	 */
	int MPROPERTY_INSTANCE_ACTION = 11;

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MResourceFolder <em>MResource Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MResource Folder</em>'.
	 * @see com.montages.mcore.objects.MResourceFolder
	 * @generated
	 */
	EClass getMResourceFolder();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MResourceFolder#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource</em>'.
	 * @see com.montages.mcore.objects.MResourceFolder#getResource()
	 * @see #getMResourceFolder()
	 * @generated
	 */
	EReference getMResourceFolder_Resource();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MResourceFolder#getFolder <em>Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Folder</em>'.
	 * @see com.montages.mcore.objects.MResourceFolder#getFolder()
	 * @see #getMResourceFolder()
	 * @generated
	 */
	EReference getMResourceFolder_Folder();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MResourceFolder#getRootFolder <em>Root Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Folder</em>'.
	 * @see com.montages.mcore.objects.MResourceFolder#getRootFolder()
	 * @see #getMResourceFolder()
	 * @generated
	 */
	EReference getMResourceFolder_RootFolder();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MResourceFolder#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.objects.MResourceFolder#getDoAction()
	 * @see #getMResourceFolder()
	 * @generated
	 */
	EAttribute getMResourceFolder_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResourceFolder#doAction$Update(com.montages.mcore.objects.MResourceFolderAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MResourceFolder#doAction$Update(com.montages.mcore.objects.MResourceFolderAction)
	 * @generated
	 */
	EOperation getMResourceFolder__DoAction$Update__MResourceFolderAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResourceFolder#updateResource(com.montages.mcore.MProperty) <em>Update Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Resource</em>' operation.
	 * @see com.montages.mcore.objects.MResourceFolder#updateResource(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMResourceFolder__UpdateResource__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResourceFolder#updateResource(com.montages.mcore.MProperty, com.montages.mcore.MProperty) <em>Update Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Resource</em>' operation.
	 * @see com.montages.mcore.objects.MResourceFolder#updateResource(com.montages.mcore.MProperty, com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMResourceFolder__UpdateResource__MProperty_MProperty();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MResource <em>MResource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MResource</em>'.
	 * @see com.montages.mcore.objects.MResource
	 * @generated
	 */
	EClass getMResource();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MResource#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object</em>'.
	 * @see com.montages.mcore.objects.MResource#getObject()
	 * @see #getMResource()
	 * @generated
	 */
	EReference getMResource_Object();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MResource#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.montages.mcore.objects.MResource#getId()
	 * @see #getMResource()
	 * @generated
	 */
	EAttribute getMResource_Id();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MResource#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.objects.MResource#getDoAction()
	 * @see #getMResource()
	 * @generated
	 */
	EAttribute getMResource_DoAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#doAction$Update(com.montages.mcore.objects.MResourceAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MResource#doAction$Update(com.montages.mcore.objects.MResourceAction)
	 * @generated
	 */
	EOperation getMResource__DoAction$Update__MResourceAction();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MResource#getRootObjectPackage <em>Root Object Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Root Object Package</em>'.
	 * @see com.montages.mcore.objects.MResource#getRootObjectPackage()
	 * @see #getMResource()
	 * @generated
	 */
	EReference getMResource_RootObjectPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MResource#getContainingFolder <em>Containing Folder</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Folder</em>'.
	 * @see com.montages.mcore.objects.MResource#getContainingFolder()
	 * @see #getMResource()
	 * @generated
	 */
	EReference getMResource_ContainingFolder();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#update(com.montages.mcore.MProperty) <em>Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update</em>' operation.
	 * @see com.montages.mcore.objects.MResource#update(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMResource__Update__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#updateObjects(com.montages.mcore.MProperty, com.montages.mcore.MProperty) <em>Update Objects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Objects</em>' operation.
	 * @see com.montages.mcore.objects.MResource#updateObjects(com.montages.mcore.MProperty, com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMResource__UpdateObjects__MProperty_MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#updateContainmentToStoredFeature(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject) <em>Update Containment To Stored Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Containment To Stored Feature</em>' operation.
	 * @see com.montages.mcore.objects.MResource#updateContainmentToStoredFeature(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject)
	 * @generated
	 */
	EOperation getMResource__UpdateContainmentToStoredFeature__MProperty_MObject();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#updateStoredToContainmentFeature(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject) <em>Update Stored To Containment Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Stored To Containment Feature</em>' operation.
	 * @see com.montages.mcore.objects.MResource#updateStoredToContainmentFeature(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject)
	 * @generated
	 */
	EOperation getMResource__UpdateStoredToContainmentFeature__MProperty_MObject();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MResource#addObjects(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject) <em>Add Objects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Objects</em>' operation.
	 * @see com.montages.mcore.objects.MResource#addObjects(com.montages.mcore.MProperty, com.montages.mcore.objects.MObject)
	 * @generated
	 */
	EOperation getMResource__AddObjects__MProperty_MObject();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MObject <em>MObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MObject</em>'.
	 * @see com.montages.mcore.objects.MObject
	 * @generated
	 */
	EClass getMObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getNature <em>Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature</em>'.
	 * @see com.montages.mcore.objects.MObject#getNature()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_Nature();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getClassNameOrNatureDefinition <em>Class Name Or Nature Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Class Name Or Nature Definition</em>'.
	 * @see com.montages.mcore.objects.MObject#getClassNameOrNatureDefinition()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_ClassNameOrNatureDefinition();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getNatureNeeded <em>Nature Needed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature Needed</em>'.
	 * @see com.montages.mcore.objects.MObject#getNatureNeeded()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_NatureNeeded();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getNatureMissing <em>Nature Missing</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature Missing</em>'.
	 * @see com.montages.mcore.objects.MObject#getNatureMissing()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_NatureMissing();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getNatureEName <em>Nature EName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nature EName</em>'.
	 * @see com.montages.mcore.objects.MObject#getNatureEName()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_NatureEName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getCalculatedNature <em>Calculated Nature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Calculated Nature</em>'.
	 * @see com.montages.mcore.objects.MObject#getCalculatedNature()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_CalculatedNature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getTypePackage <em>Type Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Package</em>'.
	 * @see com.montages.mcore.objects.MObject#getTypePackage()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_TypePackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see com.montages.mcore.objects.MObject#getType()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_Type();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getInitializationType <em>Initialization Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Initialization Type</em>'.
	 * @see com.montages.mcore.objects.MObject#getInitializationType()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_InitializationType();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MObject#getPropertyInstance <em>Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Instance</em>'.
	 * @see com.montages.mcore.objects.MObject#getPropertyInstance()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_PropertyInstance();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see com.montages.mcore.objects.MObject#getId()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_Id();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getContainingObject <em>Containing Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Object</em>'.
	 * @see com.montages.mcore.objects.MObject#getContainingObject()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_ContainingObject();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getContainingPropertyInstance <em>Containing Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Property Instance</em>'.
	 * @see com.montages.mcore.objects.MObject#getContainingPropertyInstance()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_ContainingPropertyInstance();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see com.montages.mcore.objects.MObject#getResource()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_Resource();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MObject#getReferencedBy <em>Referenced By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Referenced By</em>'.
	 * @see com.montages.mcore.objects.MObject#getReferencedBy()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_ReferencedBy();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MObject#getWrongPropertyInstance <em>Wrong Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wrong Property Instance</em>'.
	 * @see com.montages.mcore.objects.MObject#getWrongPropertyInstance()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_WrongPropertyInstance();

	/**
	 * Returns the meta object for the containment reference '{@link com.montages.mcore.objects.MObject#getTable <em>Table</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Table</em>'.
	 * @see com.montages.mcore.objects.MObject#getTable()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_Table();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getNumericIdOfObject <em>Numeric Id Of Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Numeric Id Of Object</em>'.
	 * @see com.montages.mcore.objects.MObject#getNumericIdOfObject()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_NumericIdOfObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Initialized Containment Levels</em>'.
	 * @see com.montages.mcore.objects.MObject#getInitializedContainmentLevels()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_InitializedContainmentLevels();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.objects.MObject#getDoAction()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_DoAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getStatusAsText <em>Status As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status As Text</em>'.
	 * @see com.montages.mcore.objects.MObject#getStatusAsText()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_StatusAsText();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getDerivationsAsText <em>Derivations As Text</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derivations As Text</em>'.
	 * @see com.montages.mcore.objects.MObject#getDerivationsAsText()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_DerivationsAsText();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#createTypeInPackage$Update(com.montages.mcore.MPackage) <em>Create Type In Package$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Create Type In Package$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#createTypeInPackage$Update(com.montages.mcore.MPackage)
	 * @generated
	 */
	EOperation getMObject__CreateTypeInPackage$Update__MPackage();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#classNameOrNatureDefinition$Update(java.lang.String) <em>Class Name Or Nature Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Class Name Or Nature Definition$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#classNameOrNatureDefinition$Update(java.lang.String)
	 * @generated
	 */
	EOperation getMObject__ClassNameOrNatureDefinition$Update__String();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#doAction$Update(com.montages.mcore.objects.MObjectAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#doAction$Update(com.montages.mcore.objects.MObjectAction)
	 * @generated
	 */
	EOperation getMObject__DoAction$Update__MObjectAction();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObject#getCreateTypeInPackage <em>Create Type In Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Create Type In Package</em>'.
	 * @see com.montages.mcore.objects.MObject#getCreateTypeInPackage()
	 * @see #getMObject()
	 * @generated
	 */
	EReference getMObject_CreateTypeInPackage();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObject#getHasPropertyInstanceNotOfType <em>Has Property Instance Not Of Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Property Instance Not Of Type</em>'.
	 * @see com.montages.mcore.objects.MObject#getHasPropertyInstanceNotOfType()
	 * @see #getMObject()
	 * @generated
	 */
	EAttribute getMObject_HasPropertyInstanceNotOfType();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#isRootObject() <em>Is Root Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Root Object</em>' operation.
	 * @see com.montages.mcore.objects.MObject#isRootObject()
	 * @generated
	 */
	EOperation getMObject__IsRootObject();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#correctlyTyped() <em>Correctly Typed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correctly Typed</em>' operation.
	 * @see com.montages.mcore.objects.MObject#correctlyTyped()
	 * @generated
	 */
	EOperation getMObject__CorrectlyTyped();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MObject#correctMultiplicity()
	 * @generated
	 */
	EOperation getMObject__CorrectMultiplicity();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#doActionUpdate(com.montages.mcore.objects.MObjectAction) <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#doActionUpdate(com.montages.mcore.objects.MObjectAction)
	 * @generated
	 */
	EOperation getMObject__DoActionUpdate__MObjectAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#invokeSetDoActionUpdate(com.montages.mcore.objects.MObjectAction) <em>Invoke Set Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Invoke Set Do Action Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#invokeSetDoActionUpdate(com.montages.mcore.objects.MObjectAction)
	 * @generated
	 */
	EOperation getMObject__InvokeSetDoActionUpdate__MObjectAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#containerId() <em>Container Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Container Id</em>' operation.
	 * @see com.montages.mcore.objects.MObject#containerId()
	 * @generated
	 */
	EOperation getMObject__ContainerId();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#localId() <em>Local Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Local Id</em>' operation.
	 * @see com.montages.mcore.objects.MObject#localId()
	 * @generated
	 */
	EOperation getMObject__LocalId();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#idPropertyBasedId() <em>Id Property Based Id</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Id Property Based Id</em>' operation.
	 * @see com.montages.mcore.objects.MObject#idPropertyBasedId()
	 * @generated
	 */
	EOperation getMObject__IdPropertyBasedId();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#propertyValueAsString(com.montages.mcore.MProperty) <em>Property Value As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Value As String</em>' operation.
	 * @see com.montages.mcore.objects.MObject#propertyValueAsString(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMObject__PropertyValueAsString__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#propertyInstanceFromProperty(com.montages.mcore.MProperty) <em>Property Instance From Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Instance From Property</em>' operation.
	 * @see com.montages.mcore.objects.MObject#propertyInstanceFromProperty(com.montages.mcore.MProperty)
	 * @generated
	 */
	EOperation getMObject__PropertyInstanceFromProperty__MProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#idPropertiesAsString() <em>Id Properties As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Id Properties As String</em>' operation.
	 * @see com.montages.mcore.objects.MObject#idPropertiesAsString()
	 * @generated
	 */
	EOperation getMObject__IdPropertiesAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#isInstanceOf(com.montages.mcore.MClassifier) <em>Is Instance Of</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Instance Of</em>' operation.
	 * @see com.montages.mcore.objects.MObject#isInstanceOf(com.montages.mcore.MClassifier)
	 * @generated
	 */
	EOperation getMObject__IsInstanceOf__MClassifier();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObject#classNameOrNatureUpdate(java.lang.String) <em>Class Name Or Nature Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Class Name Or Nature Update</em>' operation.
	 * @see com.montages.mcore.objects.MObject#classNameOrNatureUpdate(java.lang.String)
	 * @generated
	 */
	EOperation getMObject__ClassNameOrNatureUpdate__String();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MPropertyInstance <em>MProperty Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MProperty Instance</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance
	 * @generated
	 */
	EClass getMPropertyInstance();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MPropertyInstance#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Property</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getProperty()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_Property();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getPropertyNameOrLocalKeyDefinition <em>Property Name Or Local Key Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Name Or Local Key Definition</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getPropertyNameOrLocalKeyDefinition()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_PropertyNameOrLocalKeyDefinition();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MPropertyInstance#getInternalDataValue <em>Internal Data Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Data Value</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getInternalDataValue()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_InternalDataValue();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MPropertyInstance#getInternalLiteralValue <em>Internal Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Literal Value</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getInternalLiteralValue()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_InternalLiteralValue();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MPropertyInstance#getInternalReferencedObject <em>Internal Referenced Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Referenced Object</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getInternalReferencedObject()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_InternalReferencedObject();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.objects.MPropertyInstance#getInternalContainedObject <em>Internal Contained Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Internal Contained Object</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getInternalContainedObject()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_InternalContainedObject();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MPropertyInstance#getContainingObject <em>Containing Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Object</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getContainingObject()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_ContainingObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getDuplicateInstance <em>Duplicate Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Duplicate Instance</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getDuplicateInstance()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_DuplicateInstance();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MPropertyInstance#getAllInstancesOfThisProperty <em>All Instances Of This Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Instances Of This Property</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getAllInstancesOfThisProperty()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_AllInstancesOfThisProperty();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalContainedObjectDueToKind <em>Wrong Internal Contained Object Due To Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wrong Internal Contained Object Due To Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getWrongInternalContainedObjectDueToKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_WrongInternalContainedObjectDueToKind();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalObjectReferenceDueToKind <em>Wrong Internal Object Reference Due To Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wrong Internal Object Reference Due To Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getWrongInternalObjectReferenceDueToKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_WrongInternalObjectReferenceDueToKind();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalLiteralValueDueToKind <em>Wrong Internal Literal Value Due To Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wrong Internal Literal Value Due To Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getWrongInternalLiteralValueDueToKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_WrongInternalLiteralValueDueToKind();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalDataValueDueToKind <em>Wrong Internal Data Value Due To Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Wrong Internal Data Value Due To Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getWrongInternalDataValueDueToKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_WrongInternalDataValueDueToKind();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getLocalKey <em>Local Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Local Key</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getLocalKey()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_LocalKey();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getWrongLocalKey <em>Wrong Local Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Wrong Local Key</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getWrongLocalKey()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_WrongLocalKey();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getKey <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getKey()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_Key();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getNumericIdOfPropertyInstance <em>Numeric Id Of Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Numeric Id Of Property Instance</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getNumericIdOfPropertyInstance()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_NumericIdOfPropertyInstance();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getDerivedKind <em>Derived Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getDerivedKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_DerivedKind();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getDerivedContainment <em>Derived Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Derived Containment</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getDerivedContainment()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_DerivedContainment();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getPropertyKindAsString <em>Property Kind As String</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Kind As String</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getPropertyKindAsString()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_PropertyKindAsString();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getCorrectPropertyKind <em>Correct Property Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct Property Kind</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getCorrectPropertyKind()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_CorrectPropertyKind();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getDoAction <em>Do Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Action</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getDoAction()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_DoAction();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getDoAddValue <em>Do Add Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Do Add Value</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getDoAddValue()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_DoAddValue();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MPropertyInstance#getSimpleTypeFromValues <em>Simple Type From Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Simple Type From Values</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getSimpleTypeFromValues()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EAttribute getMPropertyInstance_SimpleTypeFromValues();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MPropertyInstance#getEnumerationFromValues <em>Enumeration From Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Enumeration From Values</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstance#getEnumerationFromValues()
	 * @see #getMPropertyInstance()
	 * @generated
	 */
	EReference getMPropertyInstance_EnumerationFromValues();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#propertyNameOrLocalKeyDefinition$Update(java.lang.String) <em>Property Name Or Local Key Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Name Or Local Key Definition$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#propertyNameOrLocalKeyDefinition$Update(java.lang.String)
	 * @generated
	 */
	EOperation getMPropertyInstance__PropertyNameOrLocalKeyDefinition$Update__String();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#doAction$Update(com.montages.mcore.objects.MPropertyInstanceAction) <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Action$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#doAction$Update(com.montages.mcore.objects.MPropertyInstanceAction)
	 * @generated
	 */
	EOperation getMPropertyInstance__DoAction$Update__MPropertyInstanceAction();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#doAddValue$Update(java.lang.Boolean) <em>Do Add Value$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Do Add Value$ Update</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#doAddValue$Update(java.lang.Boolean)
	 * @generated
	 */
	EOperation getMPropertyInstance__DoAddValue$Update__Boolean();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#correctProperty() <em>Correct Property</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Property</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#correctProperty()
	 * @generated
	 */
	EOperation getMPropertyInstance__CorrectProperty();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#correctMultiplicity()
	 * @generated
	 */
	EOperation getMPropertyInstance__CorrectMultiplicity();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#propertyValueAsString() <em>Property Value As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Value As String</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#propertyValueAsString()
	 * @generated
	 */
	EOperation getMPropertyInstance__PropertyValueAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#internalReferencedObjectsAsString() <em>Internal Referenced Objects As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Internal Referenced Objects As String</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#internalReferencedObjectsAsString()
	 * @generated
	 */
	EOperation getMPropertyInstance__InternalReferencedObjectsAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#internalDataValuesAsString() <em>Internal Data Values As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Internal Data Values As String</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#internalDataValuesAsString()
	 * @generated
	 */
	EOperation getMPropertyInstance__InternalDataValuesAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#internalLiteralValuesAsString() <em>Internal Literal Values As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Internal Literal Values As String</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#internalLiteralValuesAsString()
	 * @generated
	 */
	EOperation getMPropertyInstance__InternalLiteralValuesAsString();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#choosableProperties() <em>Choosable Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Choosable Properties</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#choosableProperties()
	 * @generated
	 */
	EOperation getMPropertyInstance__ChoosableProperties();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MPropertyInstance#propertyNameOrLocalKeyDefinitionUpdate(java.lang.String) <em>Property Name Or Local Key Definition Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Property Name Or Local Key Definition Update</em>' operation.
	 * @see com.montages.mcore.objects.MPropertyInstance#propertyNameOrLocalKeyDefinitionUpdate(java.lang.String)
	 * @generated
	 */
	EOperation getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MValue <em>MValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MValue</em>'.
	 * @see com.montages.mcore.objects.MValue
	 * @generated
	 */
	EClass getMValue();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MValue#getContainingPropertyInstance <em>Containing Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Property Instance</em>'.
	 * @see com.montages.mcore.objects.MValue#getContainingPropertyInstance()
	 * @see #getMValue()
	 * @generated
	 */
	EReference getMValue_ContainingPropertyInstance();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MValue#getContainingObject <em>Containing Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Object</em>'.
	 * @see com.montages.mcore.objects.MValue#getContainingObject()
	 * @see #getMValue()
	 * @generated
	 */
	EReference getMValue_ContainingObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MValue#getCorrectAndDefinedValue <em>Correct And Defined Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Correct And Defined Value</em>'.
	 * @see com.montages.mcore.objects.MValue#getCorrectAndDefinedValue()
	 * @see #getMValue()
	 * @generated
	 */
	EAttribute getMValue_CorrectAndDefinedValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MValue#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MValue#correctMultiplicity()
	 * @generated
	 */
	EOperation getMValue__CorrectMultiplicity();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MObjectReference <em>MObject Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MObject Reference</em>'.
	 * @see com.montages.mcore.objects.MObjectReference
	 * @generated
	 */
	EClass getMObjectReference();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObjectReference#getReferencedObject <em>Referenced Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Referenced Object</em>'.
	 * @see com.montages.mcore.objects.MObjectReference#getReferencedObject()
	 * @see #getMObjectReference()
	 * @generated
	 */
	EReference getMObjectReference_ReferencedObject();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MObjectReference#getTypeOfReferencedObject <em>Type Of Referenced Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type Of Referenced Object</em>'.
	 * @see com.montages.mcore.objects.MObjectReference#getTypeOfReferencedObject()
	 * @see #getMObjectReference()
	 * @generated
	 */
	EReference getMObjectReference_TypeOfReferencedObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MObjectReference#getIdOfReferencedObject <em>Id Of Referenced Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id Of Referenced Object</em>'.
	 * @see com.montages.mcore.objects.MObjectReference#getIdOfReferencedObject()
	 * @see #getMObjectReference()
	 * @generated
	 */
	EAttribute getMObjectReference_IdOfReferencedObject();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.objects.MObjectReference#getChoosableReference <em>Choosable Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Choosable Reference</em>'.
	 * @see com.montages.mcore.objects.MObjectReference#getChoosableReference()
	 * @see #getMObjectReference()
	 * @generated
	 */
	EReference getMObjectReference_ChoosableReference();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObjectReference#correctlyTypedReferencedObject() <em>Correctly Typed Referenced Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correctly Typed Referenced Object</em>' operation.
	 * @see com.montages.mcore.objects.MObjectReference#correctlyTypedReferencedObject()
	 * @generated
	 */
	EOperation getMObjectReference__CorrectlyTypedReferencedObject();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MObjectReference#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MObjectReference#correctMultiplicity()
	 * @generated
	 */
	EOperation getMObjectReference__CorrectMultiplicity();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MLiteralValue <em>MLiteral Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MLiteral Value</em>'.
	 * @see com.montages.mcore.objects.MLiteralValue
	 * @generated
	 */
	EClass getMLiteralValue();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.objects.MLiteralValue#getLiteralValue <em>Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Literal Value</em>'.
	 * @see com.montages.mcore.objects.MLiteralValue#getLiteralValue()
	 * @see #getMLiteralValue()
	 * @generated
	 */
	EReference getMLiteralValue_LiteralValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MLiteralValue#correctLiteralValue() <em>Correct Literal Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Literal Value</em>' operation.
	 * @see com.montages.mcore.objects.MLiteralValue#correctLiteralValue()
	 * @generated
	 */
	EOperation getMLiteralValue__CorrectLiteralValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MLiteralValue#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MLiteralValue#correctMultiplicity()
	 * @generated
	 */
	EOperation getMLiteralValue__CorrectMultiplicity();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.objects.MDataValue <em>MData Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MData Value</em>'.
	 * @see com.montages.mcore.objects.MDataValue
	 * @generated
	 */
	EClass getMDataValue();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.mcore.objects.MDataValue#getDataValue <em>Data Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Data Value</em>'.
	 * @see com.montages.mcore.objects.MDataValue#getDataValue()
	 * @see #getMDataValue()
	 * @generated
	 */
	EAttribute getMDataValue_DataValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MDataValue#correctDataValue() <em>Correct Data Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Data Value</em>' operation.
	 * @see com.montages.mcore.objects.MDataValue#correctDataValue()
	 * @generated
	 */
	EOperation getMDataValue__CorrectDataValue();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.objects.MDataValue#correctMultiplicity() <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Correct Multiplicity</em>' operation.
	 * @see com.montages.mcore.objects.MDataValue#correctMultiplicity()
	 * @generated
	 */
	EOperation getMDataValue__CorrectMultiplicity();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.objects.MResourceFolderAction <em>MResource Folder Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MResource Folder Action</em>'.
	 * @see com.montages.mcore.objects.MResourceFolderAction
	 * @generated
	 */
	EEnum getMResourceFolderAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.objects.MResourceAction <em>MResource Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MResource Action</em>'.
	 * @see com.montages.mcore.objects.MResourceAction
	 * @generated
	 */
	EEnum getMResourceAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.objects.MObjectAction <em>MObject Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MObject Action</em>'.
	 * @see com.montages.mcore.objects.MObjectAction
	 * @generated
	 */
	EEnum getMObjectAction();

	/**
	 * Returns the meta object for enum '{@link com.montages.mcore.objects.MPropertyInstanceAction <em>MProperty Instance Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>MProperty Instance Action</em>'.
	 * @see com.montages.mcore.objects.MPropertyInstanceAction
	 * @generated
	 */
	EEnum getMPropertyInstanceAction();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ObjectsFactory getObjectsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MResourceFolderImpl <em>MResource Folder</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MResourceFolderImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceFolder()
		 * @generated
		 */
		EClass MRESOURCE_FOLDER = eINSTANCE.getMResourceFolder();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE_FOLDER__RESOURCE = eINSTANCE
				.getMResourceFolder_Resource();

		/**
		 * The meta object literal for the '<em><b>Folder</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE_FOLDER__FOLDER = eINSTANCE
				.getMResourceFolder_Folder();

		/**
		 * The meta object literal for the '<em><b>Root Folder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE_FOLDER__ROOT_FOLDER = eINSTANCE
				.getMResourceFolder_RootFolder();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRESOURCE_FOLDER__DO_ACTION = eINSTANCE
				.getMResourceFolder_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE_FOLDER___DO_ACTION$_UPDATE__MRESOURCEFOLDERACTION = eINSTANCE
				.getMResourceFolder__DoAction$Update__MResourceFolderAction();

		/**
		 * The meta object literal for the '<em><b>Update Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY = eINSTANCE
				.getMResourceFolder__UpdateResource__MProperty();

		/**
		 * The meta object literal for the '<em><b>Update Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY_MPROPERTY = eINSTANCE
				.getMResourceFolder__UpdateResource__MProperty_MProperty();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MResourceImpl <em>MResource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MResourceImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResource()
		 * @generated
		 */
		EClass MRESOURCE = eINSTANCE.getMResource();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE__OBJECT = eINSTANCE.getMResource_Object();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRESOURCE__ID = eINSTANCE.getMResource_Id();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MRESOURCE__DO_ACTION = eINSTANCE.getMResource_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___DO_ACTION$_UPDATE__MRESOURCEACTION = eINSTANCE
				.getMResource__DoAction$Update__MResourceAction();

		/**
		 * The meta object literal for the '<em><b>Root Object Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE__ROOT_OBJECT_PACKAGE = eINSTANCE
				.getMResource_RootObjectPackage();

		/**
		 * The meta object literal for the '<em><b>Containing Folder</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MRESOURCE__CONTAINING_FOLDER = eINSTANCE
				.getMResource_ContainingFolder();

		/**
		 * The meta object literal for the '<em><b>Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___UPDATE__MPROPERTY = eINSTANCE
				.getMResource__Update__MProperty();

		/**
		 * The meta object literal for the '<em><b>Update Objects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___UPDATE_OBJECTS__MPROPERTY_MPROPERTY = eINSTANCE
				.getMResource__UpdateObjects__MProperty_MProperty();

		/**
		 * The meta object literal for the '<em><b>Update Containment To Stored Feature</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___UPDATE_CONTAINMENT_TO_STORED_FEATURE__MPROPERTY_MOBJECT = eINSTANCE
				.getMResource__UpdateContainmentToStoredFeature__MProperty_MObject();

		/**
		 * The meta object literal for the '<em><b>Update Stored To Containment Feature</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___UPDATE_STORED_TO_CONTAINMENT_FEATURE__MPROPERTY_MOBJECT = eINSTANCE
				.getMResource__UpdateStoredToContainmentFeature__MProperty_MObject();

		/**
		 * The meta object literal for the '<em><b>Add Objects</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MRESOURCE___ADD_OBJECTS__MPROPERTY_MOBJECT = eINSTANCE
				.getMResource__AddObjects__MProperty_MObject();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MObjectImpl <em>MObject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MObjectImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObject()
		 * @generated
		 */
		EClass MOBJECT = eINSTANCE.getMObject();

		/**
		 * The meta object literal for the '<em><b>Nature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__NATURE = eINSTANCE.getMObject_Nature();

		/**
		 * The meta object literal for the '<em><b>Class Name Or Nature Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION = eINSTANCE
				.getMObject_ClassNameOrNatureDefinition();

		/**
		 * The meta object literal for the '<em><b>Nature Needed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__NATURE_NEEDED = eINSTANCE.getMObject_NatureNeeded();

		/**
		 * The meta object literal for the '<em><b>Nature Missing</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__NATURE_MISSING = eINSTANCE
				.getMObject_NatureMissing();

		/**
		 * The meta object literal for the '<em><b>Nature EName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__NATURE_ENAME = eINSTANCE.getMObject_NatureEName();

		/**
		 * The meta object literal for the '<em><b>Calculated Nature</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__CALCULATED_NATURE = eINSTANCE
				.getMObject_CalculatedNature();

		/**
		 * The meta object literal for the '<em><b>Type Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__TYPE_PACKAGE = eINSTANCE.getMObject_TypePackage();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__TYPE = eINSTANCE.getMObject_Type();

		/**
		 * The meta object literal for the '<em><b>Initialization Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__INITIALIZATION_TYPE = eINSTANCE
				.getMObject_InitializationType();

		/**
		 * The meta object literal for the '<em><b>Property Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__PROPERTY_INSTANCE = eINSTANCE
				.getMObject_PropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__ID = eINSTANCE.getMObject_Id();

		/**
		 * The meta object literal for the '<em><b>Containing Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__CONTAINING_OBJECT = eINSTANCE
				.getMObject_ContainingObject();

		/**
		 * The meta object literal for the '<em><b>Containing Property Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__CONTAINING_PROPERTY_INSTANCE = eINSTANCE
				.getMObject_ContainingPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__RESOURCE = eINSTANCE.getMObject_Resource();

		/**
		 * The meta object literal for the '<em><b>Referenced By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__REFERENCED_BY = eINSTANCE.getMObject_ReferencedBy();

		/**
		 * The meta object literal for the '<em><b>Wrong Property Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__WRONG_PROPERTY_INSTANCE = eINSTANCE
				.getMObject_WrongPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Table</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__TABLE = eINSTANCE.getMObject_Table();

		/**
		 * The meta object literal for the '<em><b>Numeric Id Of Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__NUMERIC_ID_OF_OBJECT = eINSTANCE
				.getMObject_NumericIdOfObject();

		/**
		 * The meta object literal for the '<em><b>Initialized Containment Levels</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__INITIALIZED_CONTAINMENT_LEVELS = eINSTANCE
				.getMObject_InitializedContainmentLevels();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__DO_ACTION = eINSTANCE.getMObject_DoAction();

		/**
		 * The meta object literal for the '<em><b>Status As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__STATUS_AS_TEXT = eINSTANCE
				.getMObject_StatusAsText();

		/**
		 * The meta object literal for the '<em><b>Derivations As Text</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__DERIVATIONS_AS_TEXT = eINSTANCE
				.getMObject_DerivationsAsText();

		/**
		 * The meta object literal for the '<em><b>Create Type In Package$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CREATE_TYPE_IN_PACKAGE$_UPDATE__MPACKAGE = eINSTANCE
				.getMObject__CreateTypeInPackage$Update__MPackage();

		/**
		 * The meta object literal for the '<em><b>Class Name Or Nature Definition$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CLASS_NAME_OR_NATURE_DEFINITION$_UPDATE__STRING = eINSTANCE
				.getMObject__ClassNameOrNatureDefinition$Update__String();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___DO_ACTION$_UPDATE__MOBJECTACTION = eINSTANCE
				.getMObject__DoAction$Update__MObjectAction();

		/**
		 * The meta object literal for the '<em><b>Create Type In Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT__CREATE_TYPE_IN_PACKAGE = eINSTANCE
				.getMObject_CreateTypeInPackage();

		/**
		 * The meta object literal for the '<em><b>Has Property Instance Not Of Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE = eINSTANCE
				.getMObject_HasPropertyInstanceNotOfType();

		/**
		 * The meta object literal for the '<em><b>Is Root Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___IS_ROOT_OBJECT = eINSTANCE
				.getMObject__IsRootObject();

		/**
		 * The meta object literal for the '<em><b>Correctly Typed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CORRECTLY_TYPED = eINSTANCE
				.getMObject__CorrectlyTyped();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CORRECT_MULTIPLICITY = eINSTANCE
				.getMObject__CorrectMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___DO_ACTION_UPDATE__MOBJECTACTION = eINSTANCE
				.getMObject__DoActionUpdate__MObjectAction();

		/**
		 * The meta object literal for the '<em><b>Invoke Set Do Action Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION = eINSTANCE
				.getMObject__InvokeSetDoActionUpdate__MObjectAction();

		/**
		 * The meta object literal for the '<em><b>Container Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CONTAINER_ID = eINSTANCE.getMObject__ContainerId();

		/**
		 * The meta object literal for the '<em><b>Local Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___LOCAL_ID = eINSTANCE.getMObject__LocalId();

		/**
		 * The meta object literal for the '<em><b>Id Property Based Id</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___ID_PROPERTY_BASED_ID = eINSTANCE
				.getMObject__IdPropertyBasedId();

		/**
		 * The meta object literal for the '<em><b>Property Value As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___PROPERTY_VALUE_AS_STRING__MPROPERTY = eINSTANCE
				.getMObject__PropertyValueAsString__MProperty();

		/**
		 * The meta object literal for the '<em><b>Property Instance From Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___PROPERTY_INSTANCE_FROM_PROPERTY__MPROPERTY = eINSTANCE
				.getMObject__PropertyInstanceFromProperty__MProperty();

		/**
		 * The meta object literal for the '<em><b>Id Properties As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___ID_PROPERTIES_AS_STRING = eINSTANCE
				.getMObject__IdPropertiesAsString();

		/**
		 * The meta object literal for the '<em><b>Is Instance Of</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___IS_INSTANCE_OF__MCLASSIFIER = eINSTANCE
				.getMObject__IsInstanceOf__MClassifier();

		/**
		 * The meta object literal for the '<em><b>Class Name Or Nature Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT___CLASS_NAME_OR_NATURE_UPDATE__STRING = eINSTANCE
				.getMObject__ClassNameOrNatureUpdate__String();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MPropertyInstanceImpl <em>MProperty Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MPropertyInstanceImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMPropertyInstance()
		 * @generated
		 */
		EClass MPROPERTY_INSTANCE = eINSTANCE.getMPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__PROPERTY = eINSTANCE
				.getMPropertyInstance_Property();

		/**
		 * The meta object literal for the '<em><b>Property Name Or Local Key Definition</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION = eINSTANCE
				.getMPropertyInstance_PropertyNameOrLocalKeyDefinition();

		/**
		 * The meta object literal for the '<em><b>Internal Data Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE = eINSTANCE
				.getMPropertyInstance_InternalDataValue();

		/**
		 * The meta object literal for the '<em><b>Internal Literal Value</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE = eINSTANCE
				.getMPropertyInstance_InternalLiteralValue();

		/**
		 * The meta object literal for the '<em><b>Internal Referenced Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT = eINSTANCE
				.getMPropertyInstance_InternalReferencedObject();

		/**
		 * The meta object literal for the '<em><b>Internal Contained Object</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT = eINSTANCE
				.getMPropertyInstance_InternalContainedObject();

		/**
		 * The meta object literal for the '<em><b>Containing Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__CONTAINING_OBJECT = eINSTANCE
				.getMPropertyInstance_ContainingObject();

		/**
		 * The meta object literal for the '<em><b>Duplicate Instance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__DUPLICATE_INSTANCE = eINSTANCE
				.getMPropertyInstance_DuplicateInstance();

		/**
		 * The meta object literal for the '<em><b>All Instances Of This Property</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY = eINSTANCE
				.getMPropertyInstance_AllInstancesOfThisProperty();

		/**
		 * The meta object literal for the '<em><b>Wrong Internal Contained Object Due To Kind</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND = eINSTANCE
				.getMPropertyInstance_WrongInternalContainedObjectDueToKind();

		/**
		 * The meta object literal for the '<em><b>Wrong Internal Object Reference Due To Kind</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND = eINSTANCE
				.getMPropertyInstance_WrongInternalObjectReferenceDueToKind();

		/**
		 * The meta object literal for the '<em><b>Wrong Internal Literal Value Due To Kind</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND = eINSTANCE
				.getMPropertyInstance_WrongInternalLiteralValueDueToKind();

		/**
		 * The meta object literal for the '<em><b>Wrong Internal Data Value Due To Kind</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND = eINSTANCE
				.getMPropertyInstance_WrongInternalDataValueDueToKind();

		/**
		 * The meta object literal for the '<em><b>Local Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__LOCAL_KEY = eINSTANCE
				.getMPropertyInstance_LocalKey();

		/**
		 * The meta object literal for the '<em><b>Wrong Local Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__WRONG_LOCAL_KEY = eINSTANCE
				.getMPropertyInstance_WrongLocalKey();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__KEY = eINSTANCE
				.getMPropertyInstance_Key();

		/**
		 * The meta object literal for the '<em><b>Numeric Id Of Property Instance</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE = eINSTANCE
				.getMPropertyInstance_NumericIdOfPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Derived Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__DERIVED_KIND = eINSTANCE
				.getMPropertyInstance_DerivedKind();

		/**
		 * The meta object literal for the '<em><b>Derived Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__DERIVED_CONTAINMENT = eINSTANCE
				.getMPropertyInstance_DerivedContainment();

		/**
		 * The meta object literal for the '<em><b>Property Kind As String</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING = eINSTANCE
				.getMPropertyInstance_PropertyKindAsString();

		/**
		 * The meta object literal for the '<em><b>Correct Property Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND = eINSTANCE
				.getMPropertyInstance_CorrectPropertyKind();

		/**
		 * The meta object literal for the '<em><b>Do Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__DO_ACTION = eINSTANCE
				.getMPropertyInstance_DoAction();

		/**
		 * The meta object literal for the '<em><b>Do Add Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__DO_ADD_VALUE = eINSTANCE
				.getMPropertyInstance_DoAddValue();

		/**
		 * The meta object literal for the '<em><b>Simple Type From Values</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES = eINSTANCE
				.getMPropertyInstance_SimpleTypeFromValues();

		/**
		 * The meta object literal for the '<em><b>Enumeration From Values</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES = eINSTANCE
				.getMPropertyInstance_EnumerationFromValues();

		/**
		 * The meta object literal for the '<em><b>Property Name Or Local Key Definition$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION$_UPDATE__STRING = eINSTANCE
				.getMPropertyInstance__PropertyNameOrLocalKeyDefinition$Update__String();

		/**
		 * The meta object literal for the '<em><b>Do Action$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___DO_ACTION$_UPDATE__MPROPERTYINSTANCEACTION = eINSTANCE
				.getMPropertyInstance__DoAction$Update__MPropertyInstanceAction();

		/**
		 * The meta object literal for the '<em><b>Do Add Value$ Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___DO_ADD_VALUE$_UPDATE__BOOLEAN = eINSTANCE
				.getMPropertyInstance__DoAddValue$Update__Boolean();

		/**
		 * The meta object literal for the '<em><b>Correct Property</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___CORRECT_PROPERTY = eINSTANCE
				.getMPropertyInstance__CorrectProperty();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___CORRECT_MULTIPLICITY = eINSTANCE
				.getMPropertyInstance__CorrectMultiplicity();

		/**
		 * The meta object literal for the '<em><b>Property Value As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___PROPERTY_VALUE_AS_STRING = eINSTANCE
				.getMPropertyInstance__PropertyValueAsString();

		/**
		 * The meta object literal for the '<em><b>Internal Referenced Objects As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___INTERNAL_REFERENCED_OBJECTS_AS_STRING = eINSTANCE
				.getMPropertyInstance__InternalReferencedObjectsAsString();

		/**
		 * The meta object literal for the '<em><b>Internal Data Values As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___INTERNAL_DATA_VALUES_AS_STRING = eINSTANCE
				.getMPropertyInstance__InternalDataValuesAsString();

		/**
		 * The meta object literal for the '<em><b>Internal Literal Values As String</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___INTERNAL_LITERAL_VALUES_AS_STRING = eINSTANCE
				.getMPropertyInstance__InternalLiteralValuesAsString();

		/**
		 * The meta object literal for the '<em><b>Choosable Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___CHOOSABLE_PROPERTIES = eINSTANCE
				.getMPropertyInstance__ChoosableProperties();

		/**
		 * The meta object literal for the '<em><b>Property Name Or Local Key Definition Update</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_UPDATE__STRING = eINSTANCE
				.getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MValueImpl <em>MValue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MValueImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMValue()
		 * @generated
		 */
		EClass MVALUE = eINSTANCE.getMValue();

		/**
		 * The meta object literal for the '<em><b>Containing Property Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MVALUE__CONTAINING_PROPERTY_INSTANCE = eINSTANCE
				.getMValue_ContainingPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Containing Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MVALUE__CONTAINING_OBJECT = eINSTANCE
				.getMValue_ContainingObject();

		/**
		 * The meta object literal for the '<em><b>Correct And Defined Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MVALUE__CORRECT_AND_DEFINED_VALUE = eINSTANCE
				.getMValue_CorrectAndDefinedValue();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MVALUE___CORRECT_MULTIPLICITY = eINSTANCE
				.getMValue__CorrectMultiplicity();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MObjectReferenceImpl <em>MObject Reference</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MObjectReferenceImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObjectReference()
		 * @generated
		 */
		EClass MOBJECT_REFERENCE = eINSTANCE.getMObjectReference();

		/**
		 * The meta object literal for the '<em><b>Referenced Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_REFERENCE__REFERENCED_OBJECT = eINSTANCE
				.getMObjectReference_ReferencedObject();

		/**
		 * The meta object literal for the '<em><b>Type Of Referenced Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT = eINSTANCE
				.getMObjectReference_TypeOfReferencedObject();

		/**
		 * The meta object literal for the '<em><b>Id Of Referenced Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT = eINSTANCE
				.getMObjectReference_IdOfReferencedObject();

		/**
		 * The meta object literal for the '<em><b>Choosable Reference</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_REFERENCE__CHOOSABLE_REFERENCE = eINSTANCE
				.getMObjectReference_ChoosableReference();

		/**
		 * The meta object literal for the '<em><b>Correctly Typed Referenced Object</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT_REFERENCE___CORRECTLY_TYPED_REFERENCED_OBJECT = eINSTANCE
				.getMObjectReference__CorrectlyTypedReferencedObject();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT_REFERENCE___CORRECT_MULTIPLICITY = eINSTANCE
				.getMObjectReference__CorrectMultiplicity();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MLiteralValueImpl <em>MLiteral Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MLiteralValueImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMLiteralValue()
		 * @generated
		 */
		EClass MLITERAL_VALUE = eINSTANCE.getMLiteralValue();

		/**
		 * The meta object literal for the '<em><b>Literal Value</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MLITERAL_VALUE__LITERAL_VALUE = eINSTANCE
				.getMLiteralValue_LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Correct Literal Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MLITERAL_VALUE___CORRECT_LITERAL_VALUE = eINSTANCE
				.getMLiteralValue__CorrectLiteralValue();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MLITERAL_VALUE___CORRECT_MULTIPLICITY = eINSTANCE
				.getMLiteralValue__CorrectMultiplicity();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.impl.MDataValueImpl <em>MData Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.impl.MDataValueImpl
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMDataValue()
		 * @generated
		 */
		EClass MDATA_VALUE = eINSTANCE.getMDataValue();

		/**
		 * The meta object literal for the '<em><b>Data Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MDATA_VALUE__DATA_VALUE = eINSTANCE
				.getMDataValue_DataValue();

		/**
		 * The meta object literal for the '<em><b>Correct Data Value</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MDATA_VALUE___CORRECT_DATA_VALUE = eINSTANCE
				.getMDataValue__CorrectDataValue();

		/**
		 * The meta object literal for the '<em><b>Correct Multiplicity</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MDATA_VALUE___CORRECT_MULTIPLICITY = eINSTANCE
				.getMDataValue__CorrectMultiplicity();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.MResourceFolderAction <em>MResource Folder Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.MResourceFolderAction
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceFolderAction()
		 * @generated
		 */
		EEnum MRESOURCE_FOLDER_ACTION = eINSTANCE.getMResourceFolderAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.MResourceAction <em>MResource Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.MResourceAction
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMResourceAction()
		 * @generated
		 */
		EEnum MRESOURCE_ACTION = eINSTANCE.getMResourceAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.MObjectAction <em>MObject Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.MObjectAction
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMObjectAction()
		 * @generated
		 */
		EEnum MOBJECT_ACTION = eINSTANCE.getMObjectAction();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.objects.MPropertyInstanceAction <em>MProperty Instance Action</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.objects.MPropertyInstanceAction
		 * @see com.montages.mcore.objects.impl.ObjectsPackageImpl#getMPropertyInstanceAction()
		 * @generated
		 */
		EEnum MPROPERTY_INSTANCE_ACTION = eINSTANCE
				.getMPropertyInstanceAction();

	}

} //ObjectsPackage
