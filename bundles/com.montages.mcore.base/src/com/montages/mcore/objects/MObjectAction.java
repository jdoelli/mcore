/**
 */
package com.montages.mcore.objects;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MObject Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsPackage#getMObjectAction()
 * @model
 * @generated
 */
public enum MObjectAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Fix Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #FIX_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	FIX_TYPE(1, "FixType", "Fix Type"),

	/**
	 * The '<em><b>Complete Slots</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #COMPLETE_SLOTS_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLETE_SLOTS(2, "CompleteSlots", "Complete Slots"),

	/**
	 * The '<em><b>Clean Slots</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLEAN_SLOTS_VALUE
	 * @generated
	 * @ordered
	 */
	CLEAN_SLOTS(3, "CleanSlots", "Clean Slots"),

	/**
	 * The '<em><b>String Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRING_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	STRING_SLOT(4, "StringSlot", "+ String Slot"),

	/**
	 * The '<em><b>Boolean Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN_SLOT(5, "BooleanSlot", "+ Boolean Slot"),

	/**
	 * The '<em><b>Integer Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTEGER_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGER_SLOT(6, "IntegerSlot", "+ Integer Slot"),

	/**
	 * The '<em><b>Real Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REAL_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	REAL_SLOT(7, "RealSlot", "+ Real Slot"),

	/**
	 * The '<em><b>Date Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_SLOT(8, "DateSlot", "+ Date Slot"),

	/**
	 * The '<em><b>Literal Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL_SLOT(9, "LiteralSlot", "+ Literal Slot"),
	/**
	 * The '<em><b>Reference Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REFERENCE_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	REFERENCE_SLOT(10, "ReferenceSlot", "+ Reference Slot"),

	/**
	 * The '<em><b>Containment Slot</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTAINMENT_SLOT_VALUE
	 * @generated
	 * @ordered
	 */
	CONTAINMENT_SLOT(11, "ContainmentSlot", "+ Containment Slot"),
	/**
	 * The '<em><b>Add Sibling</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ADD_SIBLING_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_SIBLING(12, "AddSibling", "Add Sibling");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Fix Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Fix Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #FIX_TYPE
	 * @model name="FixType" literal="Fix Type"
	 * @generated
	 * @ordered
	 */
	public static final int FIX_TYPE_VALUE = 1;

	/**
	 * The '<em><b>Complete Slots</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Complete Slots</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLETE_SLOTS
	 * @model name="CompleteSlots" literal="Complete Slots"
	 * @generated
	 * @ordered
	 */
	public static final int COMPLETE_SLOTS_VALUE = 2;

	/**
	 * The '<em><b>Clean Slots</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Clean Slots</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLEAN_SLOTS
	 * @model name="CleanSlots" literal="Clean Slots"
	 * @generated
	 * @ordered
	 */
	public static final int CLEAN_SLOTS_VALUE = 3;

	/**
	 * The '<em><b>String Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRING_SLOT
	 * @model name="StringSlot" literal="+ String Slot"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_SLOT_VALUE = 4;

	/**
	 * The '<em><b>Boolean Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_SLOT
	 * @model name="BooleanSlot" literal="+ Boolean Slot"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_SLOT_VALUE = 5;

	/**
	 * The '<em><b>Integer Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Integer Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTEGER_SLOT
	 * @model name="IntegerSlot" literal="+ Integer Slot"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGER_SLOT_VALUE = 6;

	/**
	 * The '<em><b>Real Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Real Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REAL_SLOT
	 * @model name="RealSlot" literal="+ Real Slot"
	 * @generated
	 * @ordered
	 */
	public static final int REAL_SLOT_VALUE = 7;

	/**
	 * The '<em><b>Date Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_SLOT
	 * @model name="DateSlot" literal="+ Date Slot"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_SLOT_VALUE = 8;

	/**
	 * The '<em><b>Literal Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL_SLOT
	 * @model name="LiteralSlot" literal="+ Literal Slot"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_SLOT_VALUE = 9;

	/**
	 * The '<em><b>Reference Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Reference Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REFERENCE_SLOT
	 * @model name="ReferenceSlot" literal="+ Reference Slot"
	 * @generated
	 * @ordered
	 */
	public static final int REFERENCE_SLOT_VALUE = 10;

	/**
	 * The '<em><b>Containment Slot</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Containment Slot</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTAINMENT_SLOT
	 * @model name="ContainmentSlot" literal="+ Containment Slot"
	 * @generated
	 * @ordered
	 */
	public static final int CONTAINMENT_SLOT_VALUE = 11;

	/**
	 * The '<em><b>Add Sibling</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Sibling</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_SIBLING
	 * @model name="AddSibling" literal="Add Sibling"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_SIBLING_VALUE = 12;

	/**
	 * An array of all the '<em><b>MObject Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MObjectAction[] VALUES_ARRAY = new MObjectAction[] {
			DO, FIX_TYPE, COMPLETE_SLOTS, CLEAN_SLOTS, STRING_SLOT,
			BOOLEAN_SLOT, INTEGER_SLOT, REAL_SLOT, DATE_SLOT, LITERAL_SLOT,
			REFERENCE_SLOT, CONTAINMENT_SLOT, ADD_SIBLING, };

	/**
	 * A public read-only list of all the '<em><b>MObject Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MObjectAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MObject Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MObjectAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MObjectAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MObject Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MObjectAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MObjectAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MObject Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MObjectAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case FIX_TYPE_VALUE:
			return FIX_TYPE;
		case COMPLETE_SLOTS_VALUE:
			return COMPLETE_SLOTS;
		case CLEAN_SLOTS_VALUE:
			return CLEAN_SLOTS;
		case STRING_SLOT_VALUE:
			return STRING_SLOT;
		case BOOLEAN_SLOT_VALUE:
			return BOOLEAN_SLOT;
		case INTEGER_SLOT_VALUE:
			return INTEGER_SLOT;
		case REAL_SLOT_VALUE:
			return REAL_SLOT;
		case DATE_SLOT_VALUE:
			return DATE_SLOT;
		case LITERAL_SLOT_VALUE:
			return LITERAL_SLOT;
		case REFERENCE_SLOT_VALUE:
			return REFERENCE_SLOT;
		case CONTAINMENT_SLOT_VALUE:
			return CONTAINMENT_SLOT;
		case ADD_SIBLING_VALUE:
			return ADD_SIBLING;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MObjectAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MObjectAction
