/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.MComponent;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.impl.MNamedImpl;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceAction;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>MResource Folder</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceFolderImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceFolderImpl#getFolder <em>Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceFolderImpl#getRootFolder <em>Root Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MResourceFolderImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MResourceFolderImpl extends MNamedImpl implements MResourceFolder {
	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected EList<MResource> resource;

	/**
	 * The cached value of the '{@link #getFolder() <em>Folder</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFolder()
	 * @generated
	 * @ordered
	 */
	protected EList<MResourceFolder> folder;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MResourceFolderAction DO_ACTION_EDEFAULT = MResourceFolderAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateobjectsMResourceFolderActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #updateResource <em>Update Resource</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #updateResource
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateResourcemcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #updateResource <em>Update Resource</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #updateResource
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression updateResourcemcoreMPropertymcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRootFolder <em>Root Folder</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getRootFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression rootFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getDoAction <em>Do Action</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MResourceFolderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MRESOURCE_FOLDER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MResource> getResource() {
		if (resource == null) {
			resource = new EObjectContainmentEList.Unsettable.Resolving<MResource>(
					MResource.class, this,
					ObjectsPackage.MRESOURCE_FOLDER__RESOURCE);
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResource() {
		if (resource != null)
			((InternalEList.Unsettable<?>) resource).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResource() {
		return resource != null
				&& ((InternalEList.Unsettable<?>) resource).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MResourceFolder> getFolder() {
		if (folder == null) {
			folder = new EObjectContainmentEList.Unsettable.Resolving<MResourceFolder>(
					MResourceFolder.class, this,
					ObjectsPackage.MRESOURCE_FOLDER__FOLDER);
		}
		return folder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetFolder() {
		if (folder != null)
			((InternalEList.Unsettable<?>) folder).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetFolder() {
		return folder != null && ((InternalEList.Unsettable<?>) folder).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolder getRootFolder() {
		MResourceFolder rootFolder = basicGetRootFolder();
		return rootFolder != null && rootFolder.eIsProxy()
				? (MResourceFolder) eResolveProxy((InternalEObject) rootFolder)
				: rootFolder;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolder basicGetRootFolder() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(MResourceFolder) then 
		self.eContainer().oclAsType(MResourceFolder).rootFolder
		else self endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MRESOURCE_FOLDER;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MRESOURCE_FOLDER__ROOT_FOLDER;

		if (rootFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				rootFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MRESOURCE_FOLDER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(rootFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MRESOURCE_FOLDER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MResourceFolder result = (MResourceFolder) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolderAction getDoAction() {
		/**
		 * @OCL let do: mcore::objects::MResourceFolderAction = mcore::objects::MResourceFolderAction::Do in
		do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MRESOURCE_FOLDER;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MRESOURCE_FOLDER__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MRESOURCE_FOLDER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MRESOURCE_FOLDER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MResourceFolderAction result = (MResourceFolderAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MResourceFolderAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MResourceFolderAction.ADD_FOLDER_VALUE:
			MResourceFolder mFolder = ObjectsFactory.eINSTANCE
					.createMResourceFolder();
			mFolder.setName("Folder " + (this.getFolder().size() + 1));
			this.getFolder().add(mFolder);
			break;

		case MResourceFolderAction.ADD_PROJECT_VALUE:
			MResourceFolder pFolder = ObjectsFactory.eINSTANCE
					.createMResourceFolder();
			pFolder.setName("Project " + (this.getFolder().size() + 1));
			this.getFolder().add(pFolder);
			break;

		case MResourceFolderAction.ADD_RESOURCE_VALUE:
			MResource mResource = ObjectsFactory.eINSTANCE.createMResource();
			mResource.setName("My" + (this.getResource().size() + 1));
			MComponent mComp = (MComponent) this.getRootFolder().eContainer();
			mResource.setRootObjectPackage(mComp.getRootPackage());
			int resourceNumber = this.getResource().size() + 1;

			if (resourceNumber < 10) {
				mResource.setId("R0" + resourceNumber);
			} else {
				mResource.setId("R" + resourceNumber);
			}
			mResource.setDoAction(MResourceAction.OBJECT);

			this.getResource().add(mResource);
			break;

		case MResourceFolderAction.OPEN_EDITOR_VALUE:

			break;
		default:
			break;
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MResourceFolderAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.rootFolder = self or self.indentLevel = 1 then OrderedSet{
	mcore::objects::MResourceFolderAction::Do, 
	mcore::objects::MResourceFolderAction::AddProject}
	else OrderedSet{
	mcore::objects::MResourceFolderAction::Do, 
	mcore::objects::MResourceFolderAction::AddFolder,
	mcore::objects::MResourceFolderAction::AddResource}  endif           
	->append(MResourceFolderAction::OpenEditor)                         
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MResourceFolderAction> evalDoActionChoiceConstruction(
			List<MResourceFolderAction> choice) {
		EClass eClass = ObjectsPackage.Literals.MRESOURCE_FOLDER;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ObjectsPackage.Literals.MRESOURCE_FOLDER__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MResourceFolderAction> result = new ArrayList<MResourceFolderAction>(
					(Collection<MResourceFolderAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doAction$Update(MResourceFolderAction obj) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		EEnumLiteral eEnumLiteralTrigger = ObjectsPackage.Literals.MRESOURCE_FOLDER_ACTION
				.getEEnumLiteral(obj.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMResourceFolder_DoAction(),
				XUpdateMode.REDEFINE, null,
				//:=
				eEnumLiteralTrigger, null, null);

		switch (obj.getValue()) {
		case MResourceFolderAction.ADD_FOLDER_VALUE:
			MResourceFolder mFolder = (MResourceFolder) currentTrigger
					.createNextStateNewObject(
							ObjectsPackage.Literals.MRESOURCE_FOLDER);
			mFolder.setName("Folder " + (this.getFolder().size() + 1));

			currentTrigger.addReferenceUpdate(this,
					ObjectsPackage.Literals.MRESOURCE_FOLDER__FOLDER,
					XUpdateMode.ADD, XAddUpdateMode.LAST, mFolder, null, null);

			transition.getFocusObjects().add(currentTrigger
					.nextStateObjectDefinitionFromObject(mFolder));
			return currentTrigger;

		case MResourceFolderAction.ADD_PROJECT_VALUE:
			MResourceFolder pFolder = (MResourceFolder) currentTrigger
					.createNextStateNewObject(
							ObjectsPackage.Literals.MRESOURCE_FOLDER);
			pFolder.setName("Project " + (this.getFolder().size() + 1));
			currentTrigger.addReferenceUpdate(this,
					ObjectsPackage.Literals.MRESOURCE_FOLDER__FOLDER,
					XUpdateMode.ADD, XAddUpdateMode.LAST, pFolder, null, null);

			transition.getFocusObjects().add(currentTrigger
					.nextStateObjectDefinitionFromObject(pFolder));
			return currentTrigger;

		case MResourceFolderAction.ADD_RESOURCE_VALUE:
			MResource mResource = (MResource) currentTrigger
					.createNextStateNewObject(
							ObjectsPackage.Literals.MRESOURCE);
			mResource.setName("My" + (this.getResource().size() + 1));
			MComponent mComp = (MComponent) this.getRootFolder().eContainer();
			mResource.setRootObjectPackage(mComp.getRootPackage());
			int resourceNumber = this.getResource().size() + 1;

			if (resourceNumber < 10) {
				mResource.setId("R0" + resourceNumber);
			} else {
				mResource.setId("R" + resourceNumber);
			}
			mResource.setDoAction(MResourceAction.OBJECT);
			currentTrigger.addReferenceUpdate(mResource,
					ObjectsPackage.Literals.MRESOURCE__OBJECT,
					XUpdateMode.REMOVE, null, mResource.getObject().get(0),
					null, null);

			currentTrigger.addReferenceUpdate(this,
					ObjectsPackage.Literals.MRESOURCE_FOLDER__RESOURCE,
					XUpdateMode.ADD, XAddUpdateMode.LAST, mResource, null,
					null);

			transition.getFocusObjects().add(
					currentTrigger.nextStateObjectDefinitionFromObject(mResource
							.getObject().get(1).getPropertyInstance().get(0)));
			return currentTrigger;

		default:
			// Auto Generated XSemantics;

			transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
					.createXTransition();
			com.montages.mcore.objects.MResourceFolderAction triggerValue = obj;
			currentTrigger = transition.addAttributeUpdate

			(this, ObjectsPackage.eINSTANCE.getMResourceFolder_DoAction(),
					org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
					null, null);

			return null;
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean updateResource(MProperty property) {

		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT
		System.out.println("updating Resources");

		EList<MResource> allResources = new BasicEList<MResource>();

		allResources.addAll(this.resource);

		for (MResourceFolder res : folder) {
			if (res != null)
				allResources.addAll(res.getResource());
		}

		for (MResource allSources : allResources)
			if (allSources != null)
				allSources.update(property);
		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean updateResource(MProperty property,
			MProperty correspondingProperty) {
		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT
		System.out.println("updating Resources");

		EList<MResource> allResources = new BasicEList<MResource>();

		allResources.addAll(this.resource);

		for (MResourceFolder res : folder) {
			if (res != null)
				allResources.addAll(res.getResource());
		}
		Boolean test = null;

		for (MResource allSources : allResources)
			if (allSources != null) {
				test = allSources.updateObjects(property,
						correspondingProperty);

				if (test == null)
					System.out.println();
				else
					allSources.getObject().get(0)
							.setDoAction(MObjectAction.CLEAN_SLOTS);
			}

		return true;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
			return ((InternalEList<?>) getResource()).basicRemove(otherEnd,
					msgs);
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			return ((InternalEList<?>) getFolder()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
			return getResource();
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			return getFolder();
		case ObjectsPackage.MRESOURCE_FOLDER__ROOT_FOLDER:
			if (resolve)
				return getRootFolder();
			return basicGetRootFolder();
		case ObjectsPackage.MRESOURCE_FOLDER__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
			getResource().clear();
			getResource().addAll((Collection<? extends MResource>) newValue);
			return;
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			getFolder().clear();
			getFolder()
					.addAll((Collection<? extends MResourceFolder>) newValue);
			return;
		case ObjectsPackage.MRESOURCE_FOLDER__DO_ACTION:
			setDoAction((MResourceFolderAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
			unsetResource();
			return;
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			unsetFolder();
			return;
		case ObjectsPackage.MRESOURCE_FOLDER__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MRESOURCE_FOLDER__RESOURCE:
			return isSetResource();
		case ObjectsPackage.MRESOURCE_FOLDER__FOLDER:
			return isSetFolder();
		case ObjectsPackage.MRESOURCE_FOLDER__ROOT_FOLDER:
			return basicGetRootFolder() != null;
		case ObjectsPackage.MRESOURCE_FOLDER__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MRESOURCE_FOLDER___DO_ACTION$_UPDATE__MRESOURCEFOLDERACTION:
			return doAction$Update((MResourceFolderAction) arguments.get(0));
		case ObjectsPackage.MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY:
			return updateResource((MProperty) arguments.get(0));
		case ObjectsPackage.MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY_MPROPERTY:
			return updateResource((MProperty) arguments.get(0),
					(MProperty) arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL self.calculatedShortName
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MRESOURCE_FOLDER;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel let resultofKindLabel: String = if (let e0: Boolean = if ((let e0: Boolean = rootFolder = self in 
	if e0.oclIsInvalid() then null else e0 endif))= false 
	then false 
	else if (let e0: Boolean = indentLevel = 1 in 
	if e0.oclIsInvalid() then null else e0 endif)= false 
	then false 
	else if ((let e0: Boolean = rootFolder = self in 
	if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = indentLevel = 1 in 
	if e0.oclIsInvalid() then null else e0 endif)= null) = true 
	then null 
	else true endif endif endif in 
	if e0.oclIsInvalid() then null else e0 endif) 
	=true 
	then 'Workspace' else if (let e0: Boolean = indentLevel = 2 in 
	if e0.oclIsInvalid() then null else e0 endif)=true then 'Project'
	else 'Folder'
	endif endif in
	resultofKindLabel
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MRESOURCE_FOLDER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())
	then self.calculatedShortName
	else self.specialEName.camelCaseLower()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (ObjectsPackage.Literals.MRESOURCE_FOLDER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} // MResourceFolderImpl
