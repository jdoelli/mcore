/**
 */
package com.montages.mcore.objects.impl;

import com.montages.mcore.objects.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectsFactoryImpl extends EFactoryImpl implements ObjectsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ObjectsFactory init() {
		try {
			ObjectsFactory theObjectsFactory = (ObjectsFactory) EPackage.Registry.INSTANCE
					.getEFactory(ObjectsPackage.eNS_URI);
			if (theObjectsFactory != null) {
				return theObjectsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ObjectsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case ObjectsPackage.MRESOURCE_FOLDER:
			return createMResourceFolder();
		case ObjectsPackage.MRESOURCE:
			return createMResource();
		case ObjectsPackage.MOBJECT:
			return createMObject();
		case ObjectsPackage.MPROPERTY_INSTANCE:
			return createMPropertyInstance();
		case ObjectsPackage.MOBJECT_REFERENCE:
			return createMObjectReference();
		case ObjectsPackage.MLITERAL_VALUE:
			return createMLiteralValue();
		case ObjectsPackage.MDATA_VALUE:
			return createMDataValue();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case ObjectsPackage.MRESOURCE_FOLDER_ACTION:
			return createMResourceFolderActionFromString(eDataType,
					initialValue);
		case ObjectsPackage.MRESOURCE_ACTION:
			return createMResourceActionFromString(eDataType, initialValue);
		case ObjectsPackage.MOBJECT_ACTION:
			return createMObjectActionFromString(eDataType, initialValue);
		case ObjectsPackage.MPROPERTY_INSTANCE_ACTION:
			return createMPropertyInstanceActionFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case ObjectsPackage.MRESOURCE_FOLDER_ACTION:
			return convertMResourceFolderActionToString(eDataType,
					instanceValue);
		case ObjectsPackage.MRESOURCE_ACTION:
			return convertMResourceActionToString(eDataType, instanceValue);
		case ObjectsPackage.MOBJECT_ACTION:
			return convertMObjectActionToString(eDataType, instanceValue);
		case ObjectsPackage.MPROPERTY_INSTANCE_ACTION:
			return convertMPropertyInstanceActionToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolder createMResourceFolder() {
		MResourceFolderImpl mResourceFolder = new MResourceFolderImpl();
		return mResourceFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResource createMResource() {
		MResourceImpl mResource = new MResourceImpl();
		return mResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject createMObject() {
		MObjectImpl mObject = new MObjectImpl();
		return mObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance createMPropertyInstance() {
		MPropertyInstanceImpl mPropertyInstance = new MPropertyInstanceImpl();
		return mPropertyInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectReference createMObjectReference() {
		MObjectReferenceImpl mObjectReference = new MObjectReferenceImpl();
		return mObjectReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralValue createMLiteralValue() {
		MLiteralValueImpl mLiteralValue = new MLiteralValueImpl();
		return mLiteralValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MDataValue createMDataValue() {
		MDataValueImpl mDataValue = new MDataValueImpl();
		return mDataValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceFolderAction createMResourceFolderActionFromString(
			EDataType eDataType, String initialValue) {
		MResourceFolderAction result = MResourceFolderAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMResourceFolderActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResourceAction createMResourceActionFromString(EDataType eDataType,
			String initialValue) {
		MResourceAction result = MResourceAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMResourceActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectAction createMObjectActionFromString(EDataType eDataType,
			String initialValue) {
		MObjectAction result = MObjectAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMObjectActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstanceAction createMPropertyInstanceActionFromString(
			EDataType eDataType, String initialValue) {
		MPropertyInstanceAction result = MPropertyInstanceAction
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMPropertyInstanceActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectsPackage getObjectsPackage() {
		return (ObjectsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ObjectsPackage getPackage() {
		return ObjectsPackage.eINSTANCE;
	}

} //ObjectsFactoryImpl
