/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MLiteral;
import com.montages.mcore.McorePackage;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MLiteral Value</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MLiteralValueImpl#getLiteralValue <em>Literal Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MLiteralValueImpl extends MValueImpl implements MLiteralValue {
	/**
	 * The cached value of the '{@link #getLiteralValue() <em>Literal Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiteralValue()
	 * @generated
	 * @ordered
	 */
	protected MLiteral literalValue;

	/**
	 * This is true if the Literal Value reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean literalValueESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctLiteralValue <em>Correct Literal Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctLiteralValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctLiteralValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctMultiplicity <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctMultiplicity
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctMultiplicityBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getLiteralValue <em>Literal Value</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLiteralValue
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression literalValueChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectAndDefinedValue <em>Correct And Defined Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectAndDefinedValue
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctAndDefinedValueDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MLiteralValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MLITERAL_VALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteral getLiteralValue() {
		if (literalValue != null && literalValue.eIsProxy()) {
			InternalEObject oldLiteralValue = (InternalEObject) literalValue;
			literalValue = (MLiteral) eResolveProxy(oldLiteralValue);
			if (literalValue != oldLiteralValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE,
							oldLiteralValue, literalValue));
			}
		}
		return literalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteral basicGetLiteralValue() {
		return literalValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLiteralValue(MLiteral newLiteralValue) {
		MLiteral oldLiteralValue = literalValue;
		literalValue = newLiteralValue;
		boolean oldLiteralValueESet = literalValueESet;
		literalValueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE,
					oldLiteralValue, literalValue, !oldLiteralValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLiteralValue() {
		MLiteral oldLiteralValue = literalValue;
		boolean oldLiteralValueESet = literalValueESet;
		literalValue = null;
		literalValueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE,
					oldLiteralValue, null, oldLiteralValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLiteralValue() {
		return literalValueESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Literal Value</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MLiteral
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let p:MProperty=self.containingPropertyInstance.property in
	if p.oclIsUndefined() 
	then  let pos:Integer
	               =self.containingPropertyInstance
	                    .internalLiteralValue->indexOf(self) in
	if pos=1 
	   then true
	else if trg.containingEnumeration.oclIsUndefined() 
	   then false
	else let firstLiteralValue:MLiteralValue 
	             =  self.containingPropertyInstance 
	                   .internalLiteralValue->first() in
	   if firstLiteralValue.literalValue.oclIsUndefined() 
	     then false
	     else firstLiteralValue.literalValue.containingEnumeration
	                 = trg.containingEnumeration endif endif endif
	else if p.type.oclIsUndefined() 
	then false 
	else p.type.allLiterals()->includes(trg)
	endif endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalLiteralValueChoiceConstraint(MLiteral trg) {
		EClass eClass = ObjectsPackage.Literals.MLITERAL_VALUE;
		if (literalValueChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ObjectsPackage.Literals.MLITERAL_VALUE__LITERAL_VALUE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				literalValueChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"LiteralValueChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(literalValueChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "LiteralValueChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctLiteralValue() {

		/**
		 * @OCL let pi:MPropertyInstance= self.containingPropertyInstance in
		let p:MProperty = pi.property in
		if p.oclIsUndefined() then
		pi.internalDataValue->isEmpty()
		and pi.internalReferencedObject->isEmpty()
		and pi.internalContainedObject->isEmpty()
		else
		p.kind = PropertyKind::Attribute 
		and p.type.kind = ClassifierKind::Enumeration
		and (if self.literalValue.oclIsUndefined() then true else
		p.type.allLiterals()->includes(self.literalValue)
		endif)
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MLITERAL_VALUE);
		EOperation eOperation = ObjectsPackage.Literals.MLITERAL_VALUE
				.getEOperations().get(0);
		if (correctLiteralValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctLiteralValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MLITERAL_VALUE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctLiteralValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MLITERAL_VALUE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctMultiplicity() {

		/**
		 * @OCL let p:MProperty = self.containingPropertyInstance.property in
		if p.oclIsUndefined() then true else
		let pos:Integer = self.containingPropertyInstance.internalLiteralValue->indexOf(self) in 
		if p.singular then pos=1 else true endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MLITERAL_VALUE);
		EOperation eOperation = ObjectsPackage.Literals.MLITERAL_VALUE
				.getEOperations().get(1);
		if (correctMultiplicityBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctMultiplicityBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MLITERAL_VALUE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctMultiplicityBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MLITERAL_VALUE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE:
			if (resolve)
				return getLiteralValue();
			return basicGetLiteralValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE:
			setLiteralValue((MLiteral) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE:
			unsetLiteralValue();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MLITERAL_VALUE__LITERAL_VALUE:
			return isSetLiteralValue();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MLITERAL_VALUE___CORRECT_LITERAL_VALUE:
			return correctLiteralValue();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.literalValue.oclIsUndefined() then 'LITERAL VALUE MISSING' else
	self.literalValue.eName endif
	
	
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MLITERAL_VALUE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Literal Value'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MLITERAL_VALUE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL correctAndDefinedValue let var1: Boolean = let e1: Boolean = if (let chain11: mcore::MLiteral = literalValue in
	if chain11 <> null then true else false 
	endif)= false 
	then false 
	else if (correctLiteralValue())= false 
	then false 
	else if (correctMultiplicity())= false 
	then false 
	else if ((let chain11: mcore::MLiteral = literalValue in
	if chain11 <> null then true else false 
	endif)= null or (correctLiteralValue())= null or (correctMultiplicity())= null) = true 
	then null 
	else true endif endif endif endif in 
	if e1.oclIsInvalid() then null else e1 endif in
	var1
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectAndDefinedValue() {
		EClass eClass = (ObjectsPackage.Literals.MLITERAL_VALUE);
		EStructuralFeature eOverrideFeature = ObjectsPackage.Literals.MVALUE__CORRECT_AND_DEFINED_VALUE;

		if (correctAndDefinedValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctAndDefinedValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctAndDefinedValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MLiteralValueImpl
