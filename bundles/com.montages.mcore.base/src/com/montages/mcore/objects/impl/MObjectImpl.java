/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.impl.MRepositoryElementImpl;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.resolver.TypeResolver;
import com.montages.mcore.tables.MTable;

/**
 * <!-- begin-user-doc --> An implementation of the model object
 * '<em><b>MObject</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getPropertyInstance <em>Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getTable <em>Table</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getCreateTypeInPackage <em>Create Type In Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getHasPropertyInstanceNotOfType <em>Has Property Instance Not Of Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getNature <em>Nature</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getClassNameOrNatureDefinition <em>Class Name Or Nature Definition</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getNatureNeeded <em>Nature Needed</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getNatureMissing <em>Nature Missing</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getTypePackage <em>Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getNatureEName <em>Nature EName</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getCalculatedNature <em>Calculated Nature</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getInitializationType <em>Initialization Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getNumericIdOfObject <em>Numeric Id Of Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getContainingPropertyInstance <em>Containing Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getReferencedBy <em>Referenced By</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getWrongPropertyInstance <em>Wrong Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getStatusAsText <em>Status As Text</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MObjectImpl#getDerivationsAsText <em>Derivations As Text</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MObjectImpl extends MRepositoryElementImpl
		implements MObject, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getPropertyInstance() <em>Property Instance</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getPropertyInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<MPropertyInstance> propertyInstance;

	/**
	 * The cached value of the '{@link #getTable() <em>Table</em>}' containment reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTable()
	 * @generated
	 * @ordered
	 */
	protected MTable table;

	/**
	 * This is true if the Table containment reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean tableESet;

	/**
	 * The default value of the '{@link #getHasPropertyInstanceNotOfType()
	 * <em>Has Property Instance Not Of Type</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHasPropertyInstanceNotOfType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_PROPERTY_INSTANCE_NOT_OF_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected static final String NATURE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNature() <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNature()
	 * @generated
	 * @ordered
	 */
	protected String nature = NATURE_EDEFAULT;

	/**
	 * This is true if the Nature attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean natureESet;

	/**
	 * The default value of the '{@link #getClassNameOrNatureDefinition() <em>Class Name Or Nature Definition</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getClassNameOrNatureDefinition()
	 * @generated
	 * @ordered
	 */
	protected static final String CLASS_NAME_OR_NATURE_DEFINITION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNatureNeeded() <em>Nature Needed</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNatureNeeded()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean NATURE_NEEDED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNatureMissing() <em>Nature Missing</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNatureMissing()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean NATURE_MISSING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier type;

	/**
	 * This is true if the Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The cached value of the '{@link #getTypePackage() <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getTypePackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage typePackage;

	/**
	 * This is true if the Type Package reference has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean typePackageESet;

	/**
	 * The default value of the '{@link #getNatureEName() <em>Nature EName</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNatureEName()
	 * @generated
	 * @ordered
	 */
	protected static final String NATURE_ENAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedNature() <em>Calculated Nature</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCalculatedNature()
	 * @generated
	 * @ordered
	 */
	protected static final String CALCULATED_NATURE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNumericIdOfObject() <em>Numeric Id Of Object</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNumericIdOfObject()
	 * @generated
	 * @ordered
	 */
	protected static final String NUMERIC_ID_OF_OBJECT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getInitializedContainmentLevels() <em>Initialized Containment Levels</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getInitializedContainmentLevels()
	 * @generated
	 * @ordered
	 */
	protected static final Integer INITIALIZED_CONTAINMENT_LEVELS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInitializedContainmentLevels() <em>Initialized Containment Levels</em>}' attribute.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getInitializedContainmentLevels()
	 * @generated
	 * @ordered
	 */
	protected Integer initializedContainmentLevels = INITIALIZED_CONTAINMENT_LEVELS_EDEFAULT;

	/**
	 * This is true if the Initialized Containment Levels attribute has been set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initializedContainmentLevelsESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MObjectAction DO_ACTION_EDEFAULT = MObjectAction.DO;

	/**
	 * The default value of the '{@link #getStatusAsText() <em>Status As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStatusAsText()
	 * @generated
	 * @ordered
	 */
	protected static final String STATUS_AS_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStatusAsText() <em>Status As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getStatusAsText()
	 * @generated
	 * @ordered
	 */
	protected String statusAsText = STATUS_AS_TEXT_EDEFAULT;

	/**
	 * This is true if the Status As Text attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean statusAsTextESet;

	/**
	 * The default value of the '{@link #getDerivationsAsText() <em>Derivations As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDerivationsAsText()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVATIONS_AS_TEXT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDerivationsAsText() <em>Derivations As Text</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDerivationsAsText()
	 * @generated
	 * @ordered
	 */
	protected String derivationsAsText = DERIVATIONS_AS_TEXT_EDEFAULT;

	/**
	 * This is true if the Derivations As Text attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean derivationsAsTextESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #createTypeInPackage$Update <em>Create Type In Package$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #createTypeInPackage$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression createTypeInPackage$UpdatemcoreMPackageBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #classNameOrNatureDefinition$Update <em>Class Name Or Nature Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #classNameOrNatureDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression classNameOrNatureDefinition$UpdateecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateobjectsMObjectActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isRootObject <em>Is Root Object</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isRootObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isRootObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctlyTyped <em>Correctly Typed</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #correctlyTyped
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctlyTypedBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctMultiplicity <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #correctMultiplicity
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctMultiplicityBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdateobjectsMObjectActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetDoActionUpdate <em>Invoke Set Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #invokeSetDoActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetDoActionUpdateobjectsMObjectActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #containerId <em>Container Id</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #containerId
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression containerIdBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #localId <em>Local Id</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #localId
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression localIdBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #idPropertyBasedId <em>Id Property Based Id</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #idPropertyBasedId
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression idPropertyBasedIdBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyValueAsString <em>Property Value As String</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #propertyValueAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyValueAsStringmcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyInstanceFromProperty <em>Property Instance From Property</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #propertyInstanceFromProperty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyInstanceFromPropertymcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isInstanceOf <em>Is Instance Of</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #isInstanceOf
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isInstanceOfmcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #classNameOrNatureUpdate <em>Class Name Or Nature Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #classNameOrNatureUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression classNameOrNatureUpdateecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #idPropertiesAsString <em>Id Properties As String</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #idPropertiesAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression idPropertiesAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNatureEName <em>Nature EName</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getNatureEName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression natureENameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getCalculatedNature <em>Calculated Nature</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCalculatedNature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedNatureDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getType <em>Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression typeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getInitializationType <em>Initialization Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getInitializationType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression initializationTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getId <em>Id</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getId
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression idDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getContainingObject <em>Containing Object</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getContainingObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPropertyInstance <em>Containing Property Instance</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainingPropertyInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPropertyInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getResource
	 * <em>Resource</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression resourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getReferencedBy <em>Referenced By</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getReferencedBy
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression referencedByDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getWrongPropertyInstance <em>Wrong Property Instance</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getWrongPropertyInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression wrongPropertyInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of
	 * '{@link #getDoAction <em>Do Action</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNumericIdOfObject <em>Numeric Id Of Object</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getNumericIdOfObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression numericIdOfObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCreateTypeInPackage <em>Create Type In Package</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCreateTypeInPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression createTypeInPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getHasPropertyInstanceNotOfType <em>Has Property Instance Not Of Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasPropertyInstanceNotOfType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression hasPropertyInstanceNotOfTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getClassNameOrNatureDefinition <em>Class Name Or Nature Definition</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getClassNameOrNatureDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression classNameOrNatureDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNatureNeeded <em>Nature Needed</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getNatureNeeded
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression natureNeededDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getNatureMissing <em>Nature Missing</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getNatureMissing
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression natureMissingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MOBJECT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getNature() {
		return nature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setNature(String newNature) {
		String oldNature = nature;
		nature = newNature;
		boolean oldNatureESet = natureESet;
		natureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__NATURE, oldNature, nature,
					!oldNatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNature() {
		String oldNature = nature;
		boolean oldNatureESet = natureESet;
		nature = NATURE_EDEFAULT;
		natureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__NATURE, oldNature, NATURE_EDEFAULT,
					oldNatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNature() {
		return natureESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getClassNameOrNatureDefinition() {
		/**
		 * @OCL if (natureNeeded) 
		=true 
		then nature
		else if type.oclIsUndefined()
		then null
		else type.calculatedShortName
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION;

		if (classNameOrNatureDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				classNameOrNatureDefinitionDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(classNameOrNatureDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setClassNameOrNatureDefinition(
			String newClassNameOrNatureDefinition) {
		if ((getType() == null) || getHasPropertyInstanceNotOfType()) {
			this.setNature(newClassNameOrNatureDefinition);
		} else {
			String sn = getType().getShortName();
			if ((sn == null) || sn.trim().equals("")) {
				getType().setName(newClassNameOrNatureDefinition);
			} else {
				getType().setShortName(newClassNameOrNatureDefinition);
			}

		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getNatureNeeded() {
		/**
		 * @OCL let e1: Boolean = if ( type.oclIsUndefined())= true 
		then true 
		else if (hasPropertyInstanceNotOfType)= true 
		then true 
		else if (( type.oclIsUndefined())= null or (hasPropertyInstanceNotOfType)= null) = true 
		then null 
		else false endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__NATURE_NEEDED;

		if (natureNeededDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				natureNeededDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(natureNeededDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getNatureMissing() {
		/**
		 * @OCL self.natureNeeded and 
		(if self.nature.oclIsUndefined() 
		then true
		else self.nature.trim() = '' endif)
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__NATURE_MISSING;

		if (natureMissingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				natureMissingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(natureMissingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getNatureEName() {
		/**
		 * @OCL let n:String = calculatedNature in
		if n.oclIsUndefined() 
		then 'TYPE AND NATURE MISSING' 
		else n.camelCaseUpper() endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__NATURE_ENAME;

		if (natureENameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				natureENameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(natureENameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getCalculatedNature() {
		/**
		 * @OCL if (not type.oclIsUndefined()) and (not self.hasPropertyInstanceNotOfType)
		then type.calculatedShortName
		else if not (nature.oclIsUndefined() or nature='') 
		then  nature
		else  if containingPropertyInstance.oclIsUndefined() 
		then null
		else if let l:String = containingPropertyInstance.localKey in 
		    l.oclIsUndefined() or l='' 
		then null
		else  self.containingPropertyInstance.localKey.camelCaseToBusiness()
		endif endif endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__CALCULATED_NATURE;

		if (calculatedNatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedNatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getTypePackage() {
		if (typePackage != null && typePackage.eIsProxy()) {
			InternalEObject oldTypePackage = (InternalEObject) typePackage;
			typePackage = (MPackage) eResolveProxy(oldTypePackage);
			if (typePackage != oldTypePackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MOBJECT__TYPE_PACKAGE,
							oldTypePackage, typePackage));
			}
		}
		return typePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetTypePackage() {
		return typePackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypePackage(MPackage newTypePackage) {
		MPackage oldTypePackage = typePackage;
		typePackage = newTypePackage;
		boolean oldTypePackageESet = typePackageESet;
		typePackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__TYPE_PACKAGE, oldTypePackage,
					typePackage, !oldTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTypePackage() {
		MPackage oldTypePackage = typePackage;
		boolean oldTypePackageESet = typePackageESet;
		typePackage = null;
		typePackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__TYPE_PACKAGE, oldTypePackage, null,
					oldTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTypePackage() {
		return typePackageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject) type;
			type = (MClassifier) eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MOBJECT__TYPE, oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setType(MClassifier newType) {
		MClassifier oldType = type;
		type = newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		// update Nature
		if (newType != null && newType.getName() != null) {
			// reset nature, as it is same as type name
			if (newType.getName() == getNature()) {
				setNature(null);
			}
		} else {
			// set nature, if the type is set to null
			if (oldType != null && oldType.getName() != null) {
				setNature(oldType.getName());
			}
		}
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__TYPE, oldType, type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void unsetType() {
		MClassifier oldType = type;
		boolean oldTypeESet = typeESet;
		type = null;
		typeESet = false;
		// update Nature
		// set nature, if the type is set to null
		if (oldType != null && oldType.getName() != null) {
			setNature(oldType.getName());
		}
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__TYPE, oldType, null, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL trg.kind = ClassifierKind::ClassType
	and
	trg.abstractClass = false
	and 
	if self.isRootObject() 
	then let tP:MPackage=
	 if typePackage.oclIsUndefined()
	  then if resource.oclIsUndefined() 
	    then null
	    else resource.rootObjectPackage endif
	  else typePackage endif in
	trg.selectableClassifier(
	  if type.oclIsUndefined() 
	    then OrderedSet{} else OrderedSet{type} endif,
	  tP)
	else 
	let p:MProperty=self.eContainer().oclAsType(MPropertyInstance).property in
	if p.oclIsUndefined() then true 
	else p.type = trg or 
	        (if p.type.oclIsUndefined() then false 
	              else p.type.allSubTypes()->includes(trg) endif) endif 
	endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		if (typeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ObjectsPackage.Literals.MOBJECT__TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				typeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"TypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(typeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "TypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getInitializationType() {
		MClassifier initializationType = basicGetInitializationType();
		return initializationType != null && initializationType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) initializationType)
				: initializationType;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetInitializationType() {
		/**
		 * @OCL if self.isRootObject() then 
		if self.resource.rootObjectPackage.oclIsUndefined() then null
		else 
		let p:mcore::MPackage=self.resource.rootObjectPackage  in
		if p.resourceRootClass.oclIsUndefined()  then 
		let cs:OrderedSet(MClassifier) =  p.classifier->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.abstractClass=false) in if cs->isEmpty() then null else cs->first() endif
		else p.resourceRootClass
		endif
		endif
		else
		let p:MProperty= self.containingPropertyInstance.property in
		if p.oclIsUndefined() then null else 
		if p.type.oclIsUndefined() then null
		else
		if not p.type.abstractClass then p.type
		else 
		let cs:OrderedSet(mcore::MClassifier)=p.type.allSubTypes()->select(c:mcore::MClassifier| not c.abstractClass) in 
		if cs->isEmpty() then null
		else cs->last()
		endif endif endif
		endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__INITIALIZATION_TYPE;

		if (initializationTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				initializationTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(initializationTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyInstance> getPropertyInstance() {
		if (propertyInstance == null) {
			propertyInstance = new EObjectContainmentEList.Unsettable.Resolving<MPropertyInstance>(
					MPropertyInstance.class, this,
					ObjectsPackage.MOBJECT__PROPERTY_INSTANCE);
		}
		return propertyInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPropertyInstance() {
		if (propertyInstance != null)
			((InternalEList.Unsettable<?>) propertyInstance).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPropertyInstance() {
		return propertyInstance != null
				&& ((InternalEList.Unsettable<?>) propertyInstance).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		/**
		 * @OCL if not type.oclIsUndefined() 
		then if  type.allIdProperties()->notEmpty() 
		then idPropertyBasedId() 
		else numericIdOfObject endif
		else numericIdOfObject endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__ID;

		if (idDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				idDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(idDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getContainingObject() {
		MObject containingObject = basicGetContainingObject();
		return containingObject != null && containingObject.eIsProxy()
				? (MObject) eResolveProxy((InternalEObject) containingObject)
				: containingObject;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetContainingObject() {
		/**
		 * @OCL if self.isRootObject() then null
		else self.containingPropertyInstance.containingObject endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__CONTAINING_OBJECT;

		if (containingObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MObject result = (MObject) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance getContainingPropertyInstance() {
		MPropertyInstance containingPropertyInstance = basicGetContainingPropertyInstance();
		return containingPropertyInstance != null
				&& containingPropertyInstance.eIsProxy()
						? (MPropertyInstance) eResolveProxy(
								(InternalEObject) containingPropertyInstance)
						: containingPropertyInstance;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance basicGetContainingPropertyInstance() {
		/**
		 * @OCL if self.isRootObject() then null
		else self.eContainer().oclAsType(MPropertyInstance) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__CONTAINING_PROPERTY_INSTANCE;

		if (containingPropertyInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPropertyInstanceDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingPropertyInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyInstance result = (MPropertyInstance) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResource getResource() {
		MResource resource = basicGetResource();
		return resource != null && resource.eIsProxy()
				? (MResource) eResolveProxy((InternalEObject) resource)
				: resource;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MResource basicGetResource() {
		/**
		 * @OCL if self.isRootObject() then 
		if self.eContainer().oclIsKindOf(MResource) then
		self.eContainer().oclAsType(MResource) else null endif
		else self.containingObject.resource endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__RESOURCE;

		if (resourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				resourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(resourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MResource result = (MResource) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReference> getReferencedBy() {
		/**
		 * @OCL MObjectReference.allInstances()->select(referencedObject= self)
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__REFERENCED_BY;

		if (referencedByDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				referencedByDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(referencedByDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReference> result = (EList<MObjectReference>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyInstance> getWrongPropertyInstance() {
		/**
		 * @OCL 
		propertyInstance->reject(
		i:MPropertyInstance | (  let correctInternalValue: Boolean = i.wrongInternalContainedObjectDueToKind->isEmpty() and i.wrongInternalDataValueDueToKind->isEmpty() and
		i.wrongInternalLiteralValueDueToKind->isEmpty() and
		i.wrongInternalObjectReferenceDueToKind->isEmpty() in
		
		i.correctMultiplicity() and i.correctProperty() and  not(i.wrongLocalKey) and correctInternalValue))
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__WRONG_PROPERTY_INSTANCE;

		if (wrongPropertyInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				wrongPropertyInstanceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(wrongPropertyInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyInstance> result = (EList<MPropertyInstance>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTable getTable() {
		if (table != null && table.eIsProxy()) {
			InternalEObject oldTable = (InternalEObject) table;
			table = (MTable) eResolveProxy(oldTable);
			if (table != oldTable) {
				InternalEObject newTable = (InternalEObject) table;
				NotificationChain msgs = oldTable.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ObjectsPackage.MOBJECT__TABLE,
						null, null);
				if (newTable.eInternalContainer() == null) {
					msgs = newTable.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ObjectsPackage.MOBJECT__TABLE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ObjectsPackage.MOBJECT__TABLE, oldTable, table));
			}
		}
		return table;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MTable basicGetTable() {
		return table;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTable(MTable newTable,
			NotificationChain msgs) {
		MTable oldTable = table;
		table = newTable;
		boolean oldTableESet = tableESet;
		tableESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, ObjectsPackage.MOBJECT__TABLE, oldTable,
					newTable, !oldTableESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setTable(MTable newTable) {
		if (newTable != table) {
			NotificationChain msgs = null;
			if (table != null)
				msgs = ((InternalEObject) table).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - ObjectsPackage.MOBJECT__TABLE,
						null, msgs);
			if (newTable != null)
				msgs = ((InternalEObject) newTable).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - ObjectsPackage.MOBJECT__TABLE,
						null, msgs);
			msgs = basicSetTable(newTable, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldTableESet = tableESet;
			tableESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ObjectsPackage.MOBJECT__TABLE, newTable, newTable,
						!oldTableESet));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetTable(NotificationChain msgs) {
		MTable oldTable = table;
		table = null;
		boolean oldTableESet = tableESet;
		tableESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET, ObjectsPackage.MOBJECT__TABLE, oldTable,
					null, oldTableESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTable() {
		if (table != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) table).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - ObjectsPackage.MOBJECT__TABLE,
					null, msgs);
			msgs = basicUnsetTable(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldTableESet = tableESet;
			tableESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ObjectsPackage.MOBJECT__TABLE, null, null,
						oldTableESet));
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTable() {
		return tableESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getNumericIdOfObject() {
		/**
		 * @OCL containerId().concat('_').concat(
		if isRootObject() 
		then localId()
		else
		let p:MProperty=self.containingPropertyInstance.property in
		if p.oclIsUndefined() then 
		self.containingPropertyInstance.numericIdOfPropertyInstance
		.concat('.').concat(self.localId()) else
		if p.singular then p.id else
		p.id.concat('.').concat(self.localId())
		endif endif endif)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__NUMERIC_ID_OF_OBJECT;

		if (numericIdOfObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				numericIdOfObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(numericIdOfObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getInitializedContainmentLevels() {
		return initializedContainmentLevels;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitializedContainmentLevels(
			Integer newInitializedContainmentLevels) {
		Integer oldInitializedContainmentLevels = initializedContainmentLevels;
		initializedContainmentLevels = newInitializedContainmentLevels;
		boolean oldInitializedContainmentLevelsESet = initializedContainmentLevelsESet;
		initializedContainmentLevelsESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS,
					oldInitializedContainmentLevels,
					initializedContainmentLevels,
					!oldInitializedContainmentLevelsESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitializedContainmentLevels() {
		Integer oldInitializedContainmentLevels = initializedContainmentLevels;
		boolean oldInitializedContainmentLevelsESet = initializedContainmentLevelsESet;
		initializedContainmentLevels = INITIALIZED_CONTAINMENT_LEVELS_EDEFAULT;
		initializedContainmentLevelsESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS,
					oldInitializedContainmentLevels,
					INITIALIZED_CONTAINMENT_LEVELS_EDEFAULT,
					oldInitializedContainmentLevelsESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitializedContainmentLevels() {
		return initializedContainmentLevelsESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectAction getDoAction() {
		/**
		 * @OCL let do: mcore::objects::MObjectAction = mcore::objects::MObjectAction::Do in
		do
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MObjectAction result = (MObjectAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setDoAction(MObjectAction newDoAction) {
		MComponent c = (MComponent) this.eResource().getContents().get(0);
		switch (newDoAction.getValue()) {
		case MObjectAction.FIX_TYPE_VALUE:
			/*
			 * ATTENTION: REPEATED IN: MComponent do...CLASS MObject do... FIX
			 * TYPE MResource do... FIX ALL TYPES
			 */
			MPackage p = null;
			if (this.getType() == null) {
				if (c.getOwnedPackage().isEmpty()) {
					p = McoreFactory.eINSTANCE.createMPackage();
					c.getOwnedPackage().add(p);
					p.setName(c.getCalculatedName());
					this.getResource().setRootObjectPackage(p);
				} else {
					if (this.getResource().getRootObjectPackage() == null) {
						if (c.getOwnedPackage().isEmpty()) {
							p = McoreFactory.eINSTANCE.createMPackage();
							c.getOwnedPackage().add(p);
						} else {
							p = c.getOwnedPackage().get(0);
						}
						this.getResource().setRootObjectPackage(p);
					} else {
						p = this.getResource().getRootObjectPackage();
					}
				}
			} else {
				p = this.getType().getContainingPackage();
			}
			this.setCreateTypeInPackage(p);
			if (this.isRootObject()) {
				this.getResource().setRootObjectPackage(p);
				p.setResourceRootClass(this.getType());
			}
			break;
		case MObjectAction.COMPLETE_SLOTS_VALUE:
			if (this.getType() != null) {
				for (Iterator iterator = this.getType().allFeaturesWithStorage()
						.iterator(); iterator.hasNext();) {
					MProperty mProperty = (MProperty) iterator.next();

					getPropertyInstance().removeAll(getWrongPropertyInstance());
					MPropertyInstance existingSlot = this
							.propertyInstanceFromProperty(mProperty);
					if (existingSlot == null) {
						MPropertyInstance completedSlot = ObjectsFactory.eINSTANCE
								.createMPropertyInstance();
						completedSlot.setProperty(mProperty);
						this.getPropertyInstance().add(completedSlot);

						if (completedSlot.getProperty()
								.getCalculatedSingular()) {
							if (completedSlot.getProperty().getType() != null
									&& completedSlot.getProperty().getType()
											.getAbstractClass())
								break;
							completedSlot.setDoAddValue(true);
						}
					}
				}
			}
			break;
		case MObjectAction.CLEAN_SLOTS_VALUE:
			if (this.getType() != null) {
				for (Iterator<MPropertyInstance> iterator = getPropertyInstance()
						.iterator(); iterator.hasNext();) {
					MPropertyInstance mPropertyInstance = (MPropertyInstance) iterator
							.next();
					if (mPropertyInstance.getProperty() == null) {
						this.getPropertyInstance().remove(mPropertyInstance);
					} else {

						if (!this.getType().allFeaturesWithStorage()
								.contains(mPropertyInstance.getProperty())) {
							// this.getPropertyInstance()
							iterator.remove();
						}
					}
				}
			}
			break;

		case MObjectAction.ADD_SIBLING_VALUE:
			getContainingPropertyInstance()
					.setDoAction(MPropertyInstanceAction.CONTAINED_OBJECT);
			EList<MObject> internalObjects = getContainingPropertyInstance()
					.getInternalContainedObject();
			if (getContainingPropertyInstance().getProperty() == null) {
				internalObjects.get(internalObjects.size() - 1)
						.setClassNameOrNatureDefinition(
								this.getClassNameOrNatureDefinition());
			}

			internalObjects.get(internalObjects.size() - 1)
					.getPropertyInstance().clear();
			internalObjects.get(internalObjects.size() - 1)
					.getPropertyInstance()
					.addAll(EcoreUtil.copyAll(this.getPropertyInstance()));

			break;
		default:
			// if (getType() != null) {
			// setDoAction(MObjectAction.COMPLETE_SLOTS);
			// }

			MPropertyInstance newSlot = null;
			MDataValue newDataValue = null;
			newSlot = ObjectsFactory.eINSTANCE.createMPropertyInstance();
			this.getPropertyInstance().add(newSlot);
			if (getNature() == null || getNature().trim().equals("")) {
				if (getType() != null) {
					setNature(getType().getCalculatedShortName()
							.concat(" Subtype ").concat(Integer.toString(
									getType().allSubTypes().size() + 1)));
				} else {
					if (getResource().getRootObjectPackage() != null) {
						int nrOfClasses = 0;
						for (MClassifier mClassifier : getResource()
								.getRootObjectPackage().getClassifier()) {
							if (mClassifier
									.getKind() == ClassifierKind.CLASS_TYPE) {
								nrOfClasses += 1;
							}
						}
						setNature(
								"Class ".concat(Integer.toString(nrOfClasses)));
					} else {
						setNature("Class ?");
					}
				}
			}
			// undo what initialization does:
			newSlot.setProperty(null);

			switch (newDoAction.getValue()) {
			case MObjectAction.STRING_SLOT_VALUE:
				newSlot.setLocalKey("Text ".concat(Integer.toString(
						numberOfExistingFeaturesAndSlots(SimpleType.STRING) + 1)
						.toString()));
				newDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
				newSlot.getInternalDataValue().add(newDataValue);
				newDataValue.setDataValue("ENTER VALUE");
				break;
			case MObjectAction.BOOLEAN_SLOT_VALUE:
				newSlot.setLocalKey("Flag ".concat(Integer.toString(
						numberOfExistingFeaturesAndSlots(SimpleType.BOOLEAN)
								+ 1)
						.toString()));
				newDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
				newSlot.getInternalDataValue().add(newDataValue);
				newDataValue.setDataValue("false");
				break;
			case MObjectAction.INTEGER_SLOT_VALUE:
				newSlot.setLocalKey(
						"Number "
								.concat(Integer
										.toString(
												numberOfExistingFeaturesAndSlots(
														SimpleType.INTEGER) + 1)
										.toString()));
				newDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
				newSlot.getInternalDataValue().add(newDataValue);
				newDataValue.setDataValue("0");
				break;
			case MObjectAction.REAL_SLOT_VALUE:
				newSlot.setLocalKey("Factor ".concat(Integer.toString(
						numberOfExistingFeaturesAndSlots(SimpleType.DOUBLE) + 1)
						.toString()));
				newDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
				newSlot.getInternalDataValue().add(newDataValue);
				newDataValue.setDataValue("3.5");
				break;
			case MObjectAction.DATE_SLOT_VALUE:
				newSlot.setLocalKey("Date ".concat(Integer.toString(
						numberOfExistingFeaturesAndSlots(SimpleType.DATE) + 1)
						.toString()));
				newDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
				newSlot.getInternalDataValue().add(newDataValue);
				newDataValue.setDataValue("1983-02-14T06:55:19.000+0200");
				break;
			case MObjectAction.LITERAL_SLOT_VALUE:
				MLiteralValue newLiteralValue = ObjectsFactory.eINSTANCE
						.createMLiteralValue();
				newSlot.getInternalLiteralValue().add(newLiteralValue);
				if (this.getType() != null) {
					for (Iterator iterator = this.getType()
							.getContainingPackage().getClassifier()
							.iterator(); iterator.hasNext();) {
						MClassifier enumeration = (MClassifier) iterator.next();
						if (enumeration
								.getKind() == ClassifierKind.ENUMERATION) {
							if (!enumeration.getLiteral().isEmpty()) {
								newLiteralValue.setLiteralValue(
										enumeration.getLiteral().get(0));
								break;
							}
						}

					}
				} else {
					if (!c.getOwnedPackage().isEmpty()) {
						for (Iterator iterator = c.getOwnedPackage().get(0)
								.getClassifier().iterator(); iterator
										.hasNext();) {
							MClassifier enumeration = (MClassifier) iterator
									.next();
							if (enumeration
									.getKind() == ClassifierKind.ENUMERATION) {
								if (!enumeration.getLiteral().isEmpty()) {
									newLiteralValue.setLiteralValue(
											enumeration.getLiteral().get(0));
									break;
								}
							}

						}
					}

				}
				break;
			case MObjectAction.REFERENCE_SLOT_VALUE:
				int counter = 0;

				for (MPropertyInstance propIni : getPropertyInstance())
					if (propIni.getPropertyKindAsString().equals("Reference"))
						counter++;
				newSlot.setLocalKey("reference" + counter);
				MObjectReference newReferenceValue = ObjectsFactory.eINSTANCE
						.createMObjectReference();
				newSlot.getInternalReferencedObject().add(newReferenceValue);
				MObject initialReferencedObject = null;
				if (this.getContainingPropertyInstance() != null) {
					/*
					 * TODO: go backward, from that property instance to
					 * previous, and find object, and then reference it!
					 */

					int i = this.getContainingPropertyInstance()
							.getInternalContainedObject().indexOf(this);
					if (i > 0) {
						initialReferencedObject = this
								.getContainingPropertyInstance()
								.getInternalContainedObject().get(i - 1);
						newReferenceValue.setReferencedObject(this);
						break;
					} else {
						initialReferencedObject = this;
						newReferenceValue.setReferencedObject(this);
						break;
					}

				} else {
					initialReferencedObject = this;
					newReferenceValue.setReferencedObject(this);
					break;
				}
			case MObjectAction.CONTAINMENT_SLOT_VALUE:
				Set<String> allCont = new HashSet<String>();

				for (MObject mOb : this.getResource().getObject()) {
					if (mOb.isRootObject()) {
						for (MPropertyInstance propIni : mOb
								.getPropertyInstance()) {
							System.out.println(propIni);
							if (propIni.getPropertyKindAsString()
									.equals("Containment")) {
								allCont.add(propIni.getKey());
								allCont = getAllContainments(allCont, propIni);
							}
						}
						break;
					}
				}
				counter = allCont.size() + 1;
				MObject newObject = ObjectsFactory.eINSTANCE.createMObject();
				newSlot.getInternalContainedObject().add(newObject);
				newObject.setNature("Class" + counter);
				break;

			default:
				break;
			}
			break;
		}
	}

	/**
	 * This recursive function saves all keys in a Set With the Set it avoids
	 * that the same containment of more than one object is added more than one
	 * time. At the end the size of the Set is the amount of existing and in
	 * future created classes minus the root class.
	 * 
	 * @generate NOT
	 * @return
	 */
	public Set<String> getAllContainments(Set<String> allCont,
			MPropertyInstance propIni) {

		for (MObject mOb : propIni.getInternalContainedObject()) {
			for (MPropertyInstance propInstance : mOb.getPropertyInstance()) {
				if (propInstance.getPropertyKindAsString()
						.equals("Containment")) {
					allCont.add(propInstance.getKey());
					allCont = getAllContainments(allCont, propInstance);
				}
			}
		}

		return allCont;
	}

	private int numberOfExistingFeaturesAndSlots(SimpleType st) {
		int count = 0;
		if (this.getType() != null) {
			for (MProperty mProperty : this.getType().allFeatures()) {
				if (mProperty.getCalculatedSimpleType() == st) {
					count += 1;
				}
			}
		}
		for (MPropertyInstance mPropertyInstance : propertyInstance) {
			if (mPropertyInstance
					.getProperty() == null) /* otherwise counted above! */ {
				if (mPropertyInstance.getSimpleTypeFromValues() == st) {
					count += 1;
				}
			}
		}
		return count;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MObjectAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL let instantiatedProperties:OrderedSet(MProperty) = self.propertyInstance.property->excluding(null)->asOrderedSet() in 
	let nonInstantiatedProperties:OrderedSet(MProperty) =
	if type.oclIsUndefined() then OrderedSet{} else
	 type.allFeaturesWithStorage()->select(p:MProperty | instantiatedProperties->excludes(p)) endif in
	let start:OrderedSet(MObjectAction) = OrderedSet{MObjectAction::Do} in start->append(
	if type.oclIsUndefined() or hasPropertyInstanceNotOfType then 
	MObjectAction::FixType else null endif)->append(
	if not nonInstantiatedProperties->isEmpty() then 
	MObjectAction::CompleteSlots else null endif)->append(
	if type.oclIsUndefined() then null else
	if  not self.hasPropertyInstanceNotOfType
	then null else
	MObjectAction::CleanSlots endif endif)->append(
	if true then
	MObjectAction::StringSlot else null endif)->append(
	if true then 
	MObjectAction::BooleanSlot else null endif)->append(
	if true then 
	MObjectAction::IntegerSlot else null endif)->append(
	if true then 
	MObjectAction::RealSlot else null endif)->append(
	if true then 
	MObjectAction::DateSlot else null endif)->append(
	if true then 
	MObjectAction::LiteralSlot else null endif)->append(
	if true then 
	MObjectAction::ReferenceSlot else null endif)->append(
	if true then 
	MObjectAction::ContainmentSlot else null endif)->append(MObjectAction::AddSibling)->excluding(null)
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MObjectAction> evalDoActionChoiceConstruction(
			List<MObjectAction> choice) {
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ObjectsPackage.Literals.MOBJECT__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MObjectAction> result = new ArrayList<MObjectAction>(
					(Collection<MObjectAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getStatusAsText() {
		return statusAsText;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatusAsText(String newStatusAsText) {
		String oldStatusAsText = statusAsText;
		statusAsText = newStatusAsText;
		boolean oldStatusAsTextESet = statusAsTextESet;
		statusAsTextESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__STATUS_AS_TEXT, oldStatusAsText,
					statusAsText, !oldStatusAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStatusAsText() {
		String oldStatusAsText = statusAsText;
		boolean oldStatusAsTextESet = statusAsTextESet;
		statusAsText = STATUS_AS_TEXT_EDEFAULT;
		statusAsTextESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__STATUS_AS_TEXT, oldStatusAsText,
					STATUS_AS_TEXT_EDEFAULT, oldStatusAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStatusAsText() {
		return statusAsTextESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivationsAsText() {
		return derivationsAsText;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setDerivationsAsText(String newDerivationsAsText) {
		String oldDerivationsAsText = derivationsAsText;
		derivationsAsText = newDerivationsAsText;
		boolean oldDerivationsAsTextESet = derivationsAsTextESet;
		derivationsAsTextESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT,
					oldDerivationsAsText, derivationsAsText,
					!oldDerivationsAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDerivationsAsText() {
		String oldDerivationsAsText = derivationsAsText;
		boolean oldDerivationsAsTextESet = derivationsAsTextESet;
		derivationsAsText = DERIVATIONS_AS_TEXT_EDEFAULT;
		derivationsAsTextESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT,
					oldDerivationsAsText, DERIVATIONS_AS_TEXT_EDEFAULT,
					oldDerivationsAsTextESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDerivationsAsText() {
		return derivationsAsTextESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate createTypeInPackage$Update(MPackage trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MPackage triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				ObjectsPackage.eINSTANCE.getMObject_CreateTypeInPackage(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate classNameOrNatureDefinition$Update(String trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		java.lang.String triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE
						.getMObject_ClassNameOrNatureDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MObjectAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.objects.MObjectAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMObject_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getCreateTypeInPackage() {
		MPackage createTypeInPackage = basicGetCreateTypeInPackage();
		return createTypeInPackage != null && createTypeInPackage.eIsProxy()
				? (MPackage) eResolveProxy(
						(InternalEObject) createTypeInPackage)
				: createTypeInPackage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetCreateTypeInPackage() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__CREATE_TYPE_IN_PACKAGE;

		if (createTypeInPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				createTypeInPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(createTypeInPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setCreateTypeInPackage(MPackage targetPackage) {
		MClassifier newType = TypeResolver.alignClassifierWithObject(this,
				targetPackage);
		if (getResource().getRootObjectPackage() == null) {
			getResource().setRootObjectPackage(newType.getContainingPackage());
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasPropertyInstanceNotOfType() {
		/**
		 * @OCL if self.propertyInstance->isEmpty()
		then false
		else not (self.propertyInstance.correctProperty()->excludes(false)) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE;

		if (hasPropertyInstanceNotOfTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				hasPropertyInstanceNotOfTypeDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(hasPropertyInstanceNotOfTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isRootObject() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then true 
		else not eContainer().oclIsTypeOf(MPropertyInstance) endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(11);
		if (isRootObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isRootObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isRootObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctlyTyped() {

		/**
		 * @OCL let t:MClassifier = self.type in
		if t.oclIsUndefined() then false
		else 
		/*typed but wrongly:*\/
		if not (t.kind = mcore::ClassifierKind::ClassType) then false
		else
		if t.abstractClass then false else
		if self.isRootObject() then true else
		let p:MProperty = self.eContainer().oclAsType(MPropertyInstance).property in
		if p.oclIsUndefined() then true else
		/*wrongly typed based on type of property:*\/
		if p.type.oclIsUndefined() then false else
		if p.type= self.type or p.type.allSubTypes()->includes(self.type) then true else false 
		endif endif endif endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(12);
		if (correctlyTypedBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctlyTypedBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctlyTypedBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctMultiplicity() {

		/**
		 * @OCL if self.isRootObject() then true else
		let pI:MPropertyInstance= self.containingPropertyInstance in
		if pI.property.oclIsUndefined() then false else
		if pI.property.singular then
		let pos:Integer=pI.internalContainedObject->indexOf(self) in
		pos=1
		else true
		endif 
		endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(13);
		if (correctMultiplicityBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctMultiplicityBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctMultiplicityBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MObjectAction trg) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();
		EEnumLiteral eEnumLiteralTrigger = ObjectsPackage.Literals.MOBJECT_ACTION
				.getEEnumLiteral(trg.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ObjectsPackage.eINSTANCE.getMObject_DoAction(),
				XUpdateMode.REDEFINE, null,
				// :=
				eEnumLiteralTrigger, null, null);

		switch (trg) {
		case BOOLEAN_SLOT:
			break;
		case CLEAN_SLOTS:
			break;
		case COMPLETE_SLOTS:
			break;
		case CONTAINMENT_SLOT:
			break;
		case DATE_SLOT:
			break;
		case DO:
			break;
		case FIX_TYPE:
			break;
		case INTEGER_SLOT:
			break;
		case LITERAL_SLOT:
			break;
		case REAL_SLOT:
			break;
		case REFERENCE_SLOT:
			break;
		case STRING_SLOT:
			break;
		default:

			currentTrigger.invokeOperationCall(this,
					ObjectsPackage.Literals.MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION,
					trg, null, null);

			// Operations return the Object that shall be focussed
			// -> Put focus command on that
			return currentTrigger;
		}
		currentTrigger.invokeOperationCall(this,
				ObjectsPackage.Literals.MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION,
				trg, null, null);

		// Operations return the Object that shall be focussed
		// -> Put focus command on that
		return currentTrigger;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object invokeSetDoActionUpdate(MObjectAction trg) {

		this.setDoAction(trg);
		switch (trg) {
		case BOOLEAN_SLOT:
		case DATE_SLOT:
		case INTEGER_SLOT:
		case REAL_SLOT:
		case STRING_SLOT:
			return this.getPropertyInstance().get(propertyInstance.size() - 1)
					.getInternalDataValue();
		case LITERAL_SLOT:
			return this.getPropertyInstance().get(propertyInstance.size() - 1)
					.getInternalLiteralValue();
		case CONTAINMENT_SLOT:
			return this.getPropertyInstance().get(propertyInstance.size() - 1)
					.getInternalContainedObject();
		case REFERENCE_SLOT:
			return this.getPropertyInstance().get(propertyInstance.size() - 1)
					.getInternalReferencedObject();
		case FIX_TYPE:
			return this.getResource().getRootObjectPackage()
					.getResourceRootClass();
		default:
			break;

		}

		return this;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String containerId() {

		/**
		 * @OCL if self.isRootObject() then 
		let resourceId:String=self.eContainer().oclAsType(MResource).id in
		if resourceId.oclIsUndefined() then 'MISSING RESOURCE ID' else resourceId endif
		else self.containingObject.id endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(7);
		if (containerIdBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				containerIdBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(containerIdBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String localId() {

		/**
		 * @OCL if isRootObject() 
		then if resource.oclIsUndefined()
		then 'GA'
		else  
		let pos:Integer=self.resource.object->indexOf(self) in
		if pos<10 
		then '0'.concat(pos.toString()) 
		else pos.toString() endif endif
		else let pos:Integer = containingPropertyInstance 
		  .internalContainedObject
		     ->indexOf(self) in
		if pos<10 then '0'.concat(pos.toString()) else pos.toString() endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(8);
		if (localIdBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				localIdBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(localIdBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String idPropertyBasedId() {

		/**
		 * @OCL if self.type.oclIsUndefined() then '' else
		if self.type.allIdProperties()->isEmpty() then '' else
		if self.type.allIdProperties()->size()=1 then
		self.propertyValueAsString(self.type.allIdProperties()->first()) else
		self.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=''|
		(if res='' then '' else res.concat('|') endif).concat(
		let v:String=self.propertyValueAsString(p) in if v='' then '_' else v endif
		))
		endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(9);
		if (idPropertyBasedIdBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				idPropertyBasedIdBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(idPropertyBasedIdBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String propertyValueAsString(MProperty p) {

		/**
		 * @OCL let pi:objects::MPropertyInstance = self.propertyInstanceFromProperty(p) in
		if pi = null then '' else
		pi.propertyValueAsString()
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(3);
		if (propertyValueAsStringmcoreMPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				propertyValueAsStringmcoreMPropertyBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(propertyValueAsStringmcoreMPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance propertyInstanceFromProperty(MProperty p) {

		/**
		 * @OCL if self.propertyInstance->isEmpty() then null
		else 
		let pis:OrderedSet(objects::MPropertyInstance) = self.propertyInstance->select(pi:objects::MPropertyInstance|pi.property=p) in
		if pis->isEmpty() then null else
		pis->first()
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(4);
		if (propertyInstanceFromPropertymcoreMPropertyBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				propertyInstanceFromPropertymcoreMPropertyBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(propertyInstanceFromPropertymcoreMPropertyBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MPropertyInstance) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String idPropertiesAsString() {

		/**
		 * @OCL if self.type.oclIsUndefined() then '' else
		if self.type.allIdProperties()->isEmpty() then '' else
		self.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=' '|
		res.concat(p.eName).concat('="').concat(self.propertyValueAsString(p)).concat('" ')
		)
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EOperation eOperation = ObjectsPackage.Literals.MOBJECT.getEOperations()
				.get(10);
		if (idPropertiesAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				idPropertiesAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(idPropertiesAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Boolean isInstanceOf(MClassifier mClassifier) {
		MClassifier type = getType();
		if (type == null || type.getKind() != ClassifierKind.CLASS_TYPE)
			return false;
		else if (mClassifier.equals(type))
			return true;
		else {
			List<MClassifier> superTypes = type.getSuperType();
			if (superTypes.contains(mClassifier)) {
				return true;
			} else {
				return type.allSuperTypes().contains(mClassifier);
			}
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate classNameOrNatureUpdate(String classNameOrNatureDefinition) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		XUpdate currentTrigger = null;

		if ((getType() == null) || getHasPropertyInstanceNotOfType()) {
			currentTrigger = transition.addAttributeUpdate(this,
					ObjectsPackage.eINSTANCE.getMObject_Nature(),
					XUpdateMode.REDEFINE, null,
					// :=
					classNameOrNatureDefinition, null, null);

		} else {
			String sn = getType().getShortName();
			if ((sn == null) || sn.trim().equals("")) {
				currentTrigger = transition.addAttributeUpdate(getType(),
						McorePackage.Literals.MNAMED__NAME,
						XUpdateMode.REDEFINE, null,
						// :=
						classNameOrNatureDefinition, null, null);
			} else {
				currentTrigger = transition.addAttributeUpdate(getType(),
						McorePackage.Literals.MNAMED__SHORT_NAME,
						XUpdateMode.REDEFINE, null,
						// :=
						classNameOrNatureDefinition, null, null);
			}

		}

		return currentTrigger;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
			return ((InternalEList<?>) getPropertyInstance())
					.basicRemove(otherEnd, msgs);
		case ObjectsPackage.MOBJECT__TABLE:
			return basicUnsetTable(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
			return getPropertyInstance();
		case ObjectsPackage.MOBJECT__TABLE:
			if (resolve)
				return getTable();
			return basicGetTable();
		case ObjectsPackage.MOBJECT__CREATE_TYPE_IN_PACKAGE:
			if (resolve)
				return getCreateTypeInPackage();
			return basicGetCreateTypeInPackage();
		case ObjectsPackage.MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE:
			return getHasPropertyInstanceNotOfType();
		case ObjectsPackage.MOBJECT__NATURE:
			return getNature();
		case ObjectsPackage.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION:
			return getClassNameOrNatureDefinition();
		case ObjectsPackage.MOBJECT__NATURE_NEEDED:
			return getNatureNeeded();
		case ObjectsPackage.MOBJECT__NATURE_MISSING:
			return getNatureMissing();
		case ObjectsPackage.MOBJECT__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case ObjectsPackage.MOBJECT__TYPE_PACKAGE:
			if (resolve)
				return getTypePackage();
			return basicGetTypePackage();
		case ObjectsPackage.MOBJECT__NATURE_ENAME:
			return getNatureEName();
		case ObjectsPackage.MOBJECT__CALCULATED_NATURE:
			return getCalculatedNature();
		case ObjectsPackage.MOBJECT__INITIALIZATION_TYPE:
			if (resolve)
				return getInitializationType();
			return basicGetInitializationType();
		case ObjectsPackage.MOBJECT__ID:
			return getId();
		case ObjectsPackage.MOBJECT__NUMERIC_ID_OF_OBJECT:
			return getNumericIdOfObject();
		case ObjectsPackage.MOBJECT__CONTAINING_OBJECT:
			if (resolve)
				return getContainingObject();
			return basicGetContainingObject();
		case ObjectsPackage.MOBJECT__CONTAINING_PROPERTY_INSTANCE:
			if (resolve)
				return getContainingPropertyInstance();
			return basicGetContainingPropertyInstance();
		case ObjectsPackage.MOBJECT__RESOURCE:
			if (resolve)
				return getResource();
			return basicGetResource();
		case ObjectsPackage.MOBJECT__REFERENCED_BY:
			return getReferencedBy();
		case ObjectsPackage.MOBJECT__WRONG_PROPERTY_INSTANCE:
			return getWrongPropertyInstance();
		case ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS:
			return getInitializedContainmentLevels();
		case ObjectsPackage.MOBJECT__DO_ACTION:
			return getDoAction();
		case ObjectsPackage.MOBJECT__STATUS_AS_TEXT:
			return getStatusAsText();
		case ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT:
			return getDerivationsAsText();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
			getPropertyInstance().clear();
			getPropertyInstance()
					.addAll((Collection<? extends MPropertyInstance>) newValue);
			return;
		case ObjectsPackage.MOBJECT__TABLE:
			setTable((MTable) newValue);
			return;
		case ObjectsPackage.MOBJECT__CREATE_TYPE_IN_PACKAGE:
			setCreateTypeInPackage((MPackage) newValue);
			return;
		case ObjectsPackage.MOBJECT__NATURE:
			setNature((String) newValue);
			return;
		case ObjectsPackage.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION:
			setClassNameOrNatureDefinition((String) newValue);
			return;
		case ObjectsPackage.MOBJECT__TYPE:
			setType((MClassifier) newValue);
			return;
		case ObjectsPackage.MOBJECT__TYPE_PACKAGE:
			setTypePackage((MPackage) newValue);
			return;
		case ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS:
			setInitializedContainmentLevels((Integer) newValue);
			return;
		case ObjectsPackage.MOBJECT__DO_ACTION:
			setDoAction((MObjectAction) newValue);
			return;
		case ObjectsPackage.MOBJECT__STATUS_AS_TEXT:
			setStatusAsText((String) newValue);
			return;
		case ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT:
			setDerivationsAsText((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
			unsetPropertyInstance();
			return;
		case ObjectsPackage.MOBJECT__TABLE:
			unsetTable();
			return;
		case ObjectsPackage.MOBJECT__CREATE_TYPE_IN_PACKAGE:
			setCreateTypeInPackage((MPackage) null);
			return;
		case ObjectsPackage.MOBJECT__NATURE:
			unsetNature();
			return;
		case ObjectsPackage.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION:
			setClassNameOrNatureDefinition(
					CLASS_NAME_OR_NATURE_DEFINITION_EDEFAULT);
			return;
		case ObjectsPackage.MOBJECT__TYPE:
			unsetType();
			return;
		case ObjectsPackage.MOBJECT__TYPE_PACKAGE:
			unsetTypePackage();
			return;
		case ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS:
			unsetInitializedContainmentLevels();
			return;
		case ObjectsPackage.MOBJECT__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		case ObjectsPackage.MOBJECT__STATUS_AS_TEXT:
			unsetStatusAsText();
			return;
		case ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT:
			unsetDerivationsAsText();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
			return isSetPropertyInstance();
		case ObjectsPackage.MOBJECT__TABLE:
			return isSetTable();
		case ObjectsPackage.MOBJECT__CREATE_TYPE_IN_PACKAGE:
			return basicGetCreateTypeInPackage() != null;
		case ObjectsPackage.MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE:
			return HAS_PROPERTY_INSTANCE_NOT_OF_TYPE_EDEFAULT == null
					? getHasPropertyInstanceNotOfType() != null
					: !HAS_PROPERTY_INSTANCE_NOT_OF_TYPE_EDEFAULT
							.equals(getHasPropertyInstanceNotOfType());
		case ObjectsPackage.MOBJECT__NATURE:
			return isSetNature();
		case ObjectsPackage.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION:
			return CLASS_NAME_OR_NATURE_DEFINITION_EDEFAULT == null
					? getClassNameOrNatureDefinition() != null
					: !CLASS_NAME_OR_NATURE_DEFINITION_EDEFAULT
							.equals(getClassNameOrNatureDefinition());
		case ObjectsPackage.MOBJECT__NATURE_NEEDED:
			return NATURE_NEEDED_EDEFAULT == null ? getNatureNeeded() != null
					: !NATURE_NEEDED_EDEFAULT.equals(getNatureNeeded());
		case ObjectsPackage.MOBJECT__NATURE_MISSING:
			return NATURE_MISSING_EDEFAULT == null ? getNatureMissing() != null
					: !NATURE_MISSING_EDEFAULT.equals(getNatureMissing());
		case ObjectsPackage.MOBJECT__TYPE:
			return isSetType();
		case ObjectsPackage.MOBJECT__TYPE_PACKAGE:
			return isSetTypePackage();
		case ObjectsPackage.MOBJECT__NATURE_ENAME:
			return NATURE_ENAME_EDEFAULT == null ? getNatureEName() != null
					: !NATURE_ENAME_EDEFAULT.equals(getNatureEName());
		case ObjectsPackage.MOBJECT__CALCULATED_NATURE:
			return CALCULATED_NATURE_EDEFAULT == null
					? getCalculatedNature() != null
					: !CALCULATED_NATURE_EDEFAULT.equals(getCalculatedNature());
		case ObjectsPackage.MOBJECT__INITIALIZATION_TYPE:
			return basicGetInitializationType() != null;
		case ObjectsPackage.MOBJECT__ID:
			return ID_EDEFAULT == null ? getId() != null
					: !ID_EDEFAULT.equals(getId());
		case ObjectsPackage.MOBJECT__NUMERIC_ID_OF_OBJECT:
			return NUMERIC_ID_OF_OBJECT_EDEFAULT == null
					? getNumericIdOfObject() != null
					: !NUMERIC_ID_OF_OBJECT_EDEFAULT
							.equals(getNumericIdOfObject());
		case ObjectsPackage.MOBJECT__CONTAINING_OBJECT:
			return basicGetContainingObject() != null;
		case ObjectsPackage.MOBJECT__CONTAINING_PROPERTY_INSTANCE:
			return basicGetContainingPropertyInstance() != null;
		case ObjectsPackage.MOBJECT__RESOURCE:
			return basicGetResource() != null;
		case ObjectsPackage.MOBJECT__REFERENCED_BY:
			return !getReferencedBy().isEmpty();
		case ObjectsPackage.MOBJECT__WRONG_PROPERTY_INSTANCE:
			return !getWrongPropertyInstance().isEmpty();
		case ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS:
			return isSetInitializedContainmentLevels();
		case ObjectsPackage.MOBJECT__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case ObjectsPackage.MOBJECT__STATUS_AS_TEXT:
			return isSetStatusAsText();
		case ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT:
			return isSetDerivationsAsText();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MOBJECT___CREATE_TYPE_IN_PACKAGE$_UPDATE__MPACKAGE:
			return createTypeInPackage$Update((MPackage) arguments.get(0));
		case ObjectsPackage.MOBJECT___CLASS_NAME_OR_NATURE_DEFINITION$_UPDATE__STRING:
			return classNameOrNatureDefinition$Update(
					(String) arguments.get(0));
		case ObjectsPackage.MOBJECT___DO_ACTION$_UPDATE__MOBJECTACTION:
			return doAction$Update((MObjectAction) arguments.get(0));
		case ObjectsPackage.MOBJECT___PROPERTY_VALUE_AS_STRING__MPROPERTY:
			return propertyValueAsString((MProperty) arguments.get(0));
		case ObjectsPackage.MOBJECT___PROPERTY_INSTANCE_FROM_PROPERTY__MPROPERTY:
			return propertyInstanceFromProperty((MProperty) arguments.get(0));
		case ObjectsPackage.MOBJECT___IS_INSTANCE_OF__MCLASSIFIER:
			return isInstanceOf((MClassifier) arguments.get(0));
		case ObjectsPackage.MOBJECT___CLASS_NAME_OR_NATURE_UPDATE__STRING:
			return classNameOrNatureUpdate((String) arguments.get(0));
		case ObjectsPackage.MOBJECT___CONTAINER_ID:
			return containerId();
		case ObjectsPackage.MOBJECT___LOCAL_ID:
			return localId();
		case ObjectsPackage.MOBJECT___ID_PROPERTY_BASED_ID:
			return idPropertyBasedId();
		case ObjectsPackage.MOBJECT___ID_PROPERTIES_AS_STRING:
			return idPropertiesAsString();
		case ObjectsPackage.MOBJECT___IS_ROOT_OBJECT:
			return isRootObject();
		case ObjectsPackage.MOBJECT___CORRECTLY_TYPED:
			return correctlyTyped();
		case ObjectsPackage.MOBJECT___CORRECT_MULTIPLICITY:
			return correctMultiplicity();
		case ObjectsPackage.MOBJECT___DO_ACTION_UPDATE__MOBJECTACTION:
			return doActionUpdate((MObjectAction) arguments.get(0));
		case ObjectsPackage.MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION:
			return invokeSetDoActionUpdate((MObjectAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nature: ");
		if (natureESet)
			result.append(nature);
		else
			result.append("<unset>");
		result.append(", initializedContainmentLevels: ");
		if (initializedContainmentLevelsESet)
			result.append(initializedContainmentLevels);
		else
			result.append("<unset>");
		result.append(", statusAsText: ");
		if (statusAsTextESet)
			result.append(statusAsText);
		else
			result.append("<unset>");
		result.append(", derivationsAsText: ");
		if (derivationsAsTextESet)
			result.append(derivationsAsText);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL let rootString:String= if self.type.oclIsUndefined() then 'root'
	 *      else self.type.eName.camelCaseLower() endif in let idString:String=
	 *      if self.type.oclIsUndefined() then '
	 *      id=\"'.concat(self.id).concat('\"') else if
	 *      self.type.idProperty->isEmpty() then '
	 *      id=\"'.concat(self.id).concat('\"') else self.idPropertiesAsString()
	 *      endif endif in '<' .concat( if self.isRootObject() then rootString
	 *      else (let pi:MPropertyInstance =
	 *      self.eContainer().oclAsType(MPropertyInstance) in let p:MProperty =
	 *      pi.property in pi.key) endif) .concat(idString) .concat('>')
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ObjectsPackage.Literals.MOBJECT;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel if (isRootObject()) 
	=true 
	then 'Root Object'
	else 'Object'
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ObjectsPackage.Literals.MOBJECT);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ObjectsPackage.Literals.MOBJECT__PROPERTY_INSTANCE,
				ObjectsPackage.Literals.MOBJECT__TYPE,
				ObjectsPackage.Literals.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MOBJECT,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} // MObjectImpl
