/**
 */
package com.montages.mcore.objects.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.impl.MRepositoryElementImpl;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MValue;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MValue</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.impl.MValueImpl#getContainingPropertyInstance <em>Containing Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MValueImpl#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.impl.MValueImpl#getCorrectAndDefinedValue <em>Correct And Defined Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MValueImpl extends MRepositoryElementImpl
		implements MValue {
	/**
	 * The default value of the '{@link #getCorrectAndDefinedValue() <em>Correct And Defined Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectAndDefinedValue()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECT_AND_DEFINED_VALUE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #correctMultiplicity <em>Correct Multiplicity</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #correctMultiplicity
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression correctMultiplicityBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPropertyInstance <em>Containing Property Instance</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingPropertyInstance
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPropertyInstanceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingObject <em>Containing Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectAndDefinedValue <em>Correct And Defined Value</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectAndDefinedValue
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctAndDefinedValueDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MValueImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ObjectsPackage.Literals.MVALUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance getContainingPropertyInstance() {
		MPropertyInstance containingPropertyInstance = basicGetContainingPropertyInstance();
		return containingPropertyInstance != null
				&& containingPropertyInstance.eIsProxy()
						? (MPropertyInstance) eResolveProxy(
								(InternalEObject) containingPropertyInstance)
						: containingPropertyInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyInstance basicGetContainingPropertyInstance() {
		/**
		 * @OCL self.eContainer().oclAsType(MPropertyInstance)
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MVALUE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MVALUE__CONTAINING_PROPERTY_INSTANCE;

		if (containingPropertyInstanceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPropertyInstanceDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingPropertyInstanceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MVALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyInstance result = (MPropertyInstance) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getContainingObject() {
		MObject containingObject = basicGetContainingObject();
		return containingObject != null && containingObject.eIsProxy()
				? (MObject) eResolveProxy((InternalEObject) containingObject)
				: containingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetContainingObject() {
		/**
		 * @OCL self.containingPropertyInstance.containingObject
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MVALUE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MVALUE__CONTAINING_OBJECT;

		if (containingObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MVALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MObject result = (MObject) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectAndDefinedValue() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ObjectsPackage.Literals.MVALUE;
		EStructuralFeature eFeature = ObjectsPackage.Literals.MVALUE__CORRECT_AND_DEFINED_VALUE;

		if (correctAndDefinedValueDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				correctAndDefinedValueDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						derive, helper.getProblems(),
						ObjectsPackage.Literals.MVALUE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctAndDefinedValueDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MVALUE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean correctMultiplicity() {

		/**
		 * @OCL false
		 * @templateTag IGOT01
		 */
		EClass eClass = (ObjectsPackage.Literals.MVALUE);
		EOperation eOperation = ObjectsPackage.Literals.MVALUE.getEOperations()
				.get(0);
		if (correctMultiplicityBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				correctMultiplicityBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ObjectsPackage.PLUGIN_ID,
						body, helper.getProblems(),
						ObjectsPackage.Literals.MVALUE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(correctMultiplicityBodyOCL);
		try {
			XoclErrorHandler.enterContext(ObjectsPackage.PLUGIN_ID, query,
					ObjectsPackage.Literals.MVALUE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ObjectsPackage.MVALUE__CONTAINING_PROPERTY_INSTANCE:
			if (resolve)
				return getContainingPropertyInstance();
			return basicGetContainingPropertyInstance();
		case ObjectsPackage.MVALUE__CONTAINING_OBJECT:
			if (resolve)
				return getContainingObject();
			return basicGetContainingObject();
		case ObjectsPackage.MVALUE__CORRECT_AND_DEFINED_VALUE:
			return getCorrectAndDefinedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ObjectsPackage.MVALUE__CONTAINING_PROPERTY_INSTANCE:
			return basicGetContainingPropertyInstance() != null;
		case ObjectsPackage.MVALUE__CONTAINING_OBJECT:
			return basicGetContainingObject() != null;
		case ObjectsPackage.MVALUE__CORRECT_AND_DEFINED_VALUE:
			return CORRECT_AND_DEFINED_VALUE_EDEFAULT == null
					? getCorrectAndDefinedValue() != null
					: !CORRECT_AND_DEFINED_VALUE_EDEFAULT
							.equals(getCorrectAndDefinedValue());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ObjectsPackage.MVALUE___CORRECT_MULTIPLICITY:
			return correctMultiplicity();
		}
		return super.eInvoke(operationID, arguments);
	}

} //MValueImpl
