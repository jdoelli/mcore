/**
 */
package com.montages.mcore.objects.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.langlets.acore.AcorePackage;
import org.xocl.semantics.SemanticsPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.impl.AnnotationsPackageImpl;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.ExpressionsPackageImpl;
import com.montages.mcore.impl.McorePackageImpl;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MPropertyInstanceAction;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceAction;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.MValue;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.tables.impl.TablesPackageImpl;
import com.montages.mcore.updates.UpdatesPackage;
import com.montages.mcore.updates.impl.UpdatesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectsPackageImpl extends EPackageImpl implements ObjectsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mResourceFolderEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mResourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPropertyInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLiteralValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mDataValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mResourceFolderActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mResourceActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mObjectActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mPropertyInstanceActionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.objects.ObjectsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ObjectsPackageImpl() {
		super(eNS_URI, ObjectsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ObjectsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ObjectsPackage init() {
		if (isInited)
			return (ObjectsPackage) EPackage.Registry.INSTANCE
					.getEPackage(ObjectsPackage.eNS_URI);

		// Obtain or create and register package
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ObjectsPackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ObjectsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(
								McorePackage.eNS_URI)
						: McorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(AnnotationsPackage.eNS_URI)
								: AnnotationsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ExpressionsPackage.eNS_URI)
								: ExpressionsPackage.eINSTANCE);
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE
								.getEPackage(TablesPackage.eNS_URI)
						: TablesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(UpdatesPackage.eNS_URI)
								: UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theObjectsPackage.createPackageContents();
		theMcorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theTablesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theObjectsPackage.initializePackageContents();
		theMcorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theTablesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theObjectsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ObjectsPackage.eNS_URI,
				theObjectsPackage);
		return theObjectsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMResourceFolder() {
		return mResourceFolderEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResourceFolder_Resource() {
		return (EReference) mResourceFolderEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResourceFolder_Folder() {
		return (EReference) mResourceFolderEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResourceFolder_RootFolder() {
		return (EReference) mResourceFolderEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMResourceFolder_DoAction() {
		return (EAttribute) mResourceFolderEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResourceFolder__DoAction$Update__MResourceFolderAction() {
		return mResourceFolderEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResourceFolder__UpdateResource__MProperty() {
		return mResourceFolderEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResourceFolder__UpdateResource__MProperty_MProperty() {
		return mResourceFolderEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMResource() {
		return mResourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResource_Object() {
		return (EReference) mResourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMResource_Id() {
		return (EAttribute) mResourceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMResource_DoAction() {
		return (EAttribute) mResourceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__DoAction$Update__MResourceAction() {
		return mResourceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResource_RootObjectPackage() {
		return (EReference) mResourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMResource_ContainingFolder() {
		return (EReference) mResourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__Update__MProperty() {
		return mResourceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__UpdateObjects__MProperty_MProperty() {
		return mResourceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__UpdateContainmentToStoredFeature__MProperty_MObject() {
		return mResourceEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__UpdateStoredToContainmentFeature__MProperty_MObject() {
		return mResourceEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMResource__AddObjects__MProperty_MObject() {
		return mResourceEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObject() {
		return mObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_Nature() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_ClassNameOrNatureDefinition() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_NatureNeeded() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_NatureMissing() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_NatureEName() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_CalculatedNature() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_TypePackage() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_Type() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_InitializationType() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_PropertyInstance() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_Id() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_ContainingObject() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_ContainingPropertyInstance() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_Resource() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_ReferencedBy() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_WrongPropertyInstance() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_Table() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_NumericIdOfObject() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_InitializedContainmentLevels() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_DoAction() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_StatusAsText() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_DerivationsAsText() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__CreateTypeInPackage$Update__MPackage() {
		return mObjectEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__ClassNameOrNatureDefinition$Update__String() {
		return mObjectEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__DoAction$Update__MObjectAction() {
		return mObjectEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObject_CreateTypeInPackage() {
		return (EReference) mObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObject_HasPropertyInstanceNotOfType() {
		return (EAttribute) mObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__IsRootObject() {
		return mObjectEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__CorrectlyTyped() {
		return mObjectEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__CorrectMultiplicity() {
		return mObjectEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__DoActionUpdate__MObjectAction() {
		return mObjectEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__InvokeSetDoActionUpdate__MObjectAction() {
		return mObjectEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__ContainerId() {
		return mObjectEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__LocalId() {
		return mObjectEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__IdPropertyBasedId() {
		return mObjectEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__PropertyValueAsString__MProperty() {
		return mObjectEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__PropertyInstanceFromProperty__MProperty() {
		return mObjectEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__IdPropertiesAsString() {
		return mObjectEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__IsInstanceOf__MClassifier() {
		return mObjectEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObject__ClassNameOrNatureUpdate__String() {
		return mObjectEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPropertyInstance() {
		return mPropertyInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_Property() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_PropertyNameOrLocalKeyDefinition() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_InternalDataValue() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_InternalLiteralValue() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_InternalReferencedObject() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_InternalContainedObject() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_ContainingObject() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_DuplicateInstance() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_AllInstancesOfThisProperty() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_WrongInternalContainedObjectDueToKind() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_WrongInternalObjectReferenceDueToKind() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_WrongInternalLiteralValueDueToKind() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_WrongInternalDataValueDueToKind() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_LocalKey() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_WrongLocalKey() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_Key() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_NumericIdOfPropertyInstance() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_DerivedKind() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_DerivedContainment() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_PropertyKindAsString() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_CorrectPropertyKind() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_DoAction() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_DoAddValue() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertyInstance_SimpleTypeFromValues() {
		return (EAttribute) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertyInstance_EnumerationFromValues() {
		return (EReference) mPropertyInstanceEClass.getEStructuralFeatures()
				.get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__PropertyNameOrLocalKeyDefinition$Update__String() {
		return mPropertyInstanceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__DoAction$Update__MPropertyInstanceAction() {
		return mPropertyInstanceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__DoAddValue$Update__Boolean() {
		return mPropertyInstanceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__CorrectProperty() {
		return mPropertyInstanceEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__CorrectMultiplicity() {
		return mPropertyInstanceEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__PropertyValueAsString() {
		return mPropertyInstanceEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__InternalReferencedObjectsAsString() {
		return mPropertyInstanceEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__InternalDataValuesAsString() {
		return mPropertyInstanceEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__InternalLiteralValuesAsString() {
		return mPropertyInstanceEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__ChoosableProperties() {
		return mPropertyInstanceEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String() {
		return mPropertyInstanceEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMValue() {
		return mValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMValue_ContainingPropertyInstance() {
		return (EReference) mValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMValue_ContainingObject() {
		return (EReference) mValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMValue_CorrectAndDefinedValue() {
		return (EAttribute) mValueEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMValue__CorrectMultiplicity() {
		return mValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObjectReference() {
		return mObjectReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReference_ReferencedObject() {
		return (EReference) mObjectReferenceEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReference_TypeOfReferencedObject() {
		return (EReference) mObjectReferenceEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMObjectReference_IdOfReferencedObject() {
		return (EAttribute) mObjectReferenceEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReference_ChoosableReference() {
		return (EReference) mObjectReferenceEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObjectReference__CorrectlyTypedReferencedObject() {
		return mObjectReferenceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMObjectReference__CorrectMultiplicity() {
		return mObjectReferenceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLiteralValue() {
		return mLiteralValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralValue_LiteralValue() {
		return (EReference) mLiteralValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMLiteralValue__CorrectLiteralValue() {
		return mLiteralValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMLiteralValue__CorrectMultiplicity() {
		return mLiteralValueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMDataValue() {
		return mDataValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMDataValue_DataValue() {
		return (EAttribute) mDataValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMDataValue__CorrectDataValue() {
		return mDataValueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMDataValue__CorrectMultiplicity() {
		return mDataValueEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMResourceFolderAction() {
		return mResourceFolderActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMResourceAction() {
		return mResourceActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMObjectAction() {
		return mObjectActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMPropertyInstanceAction() {
		return mPropertyInstanceActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectsFactory getObjectsFactory() {
		return (ObjectsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mResourceFolderEClass = createEClass(MRESOURCE_FOLDER);
		createEReference(mResourceFolderEClass, MRESOURCE_FOLDER__RESOURCE);
		createEReference(mResourceFolderEClass, MRESOURCE_FOLDER__FOLDER);
		createEReference(mResourceFolderEClass, MRESOURCE_FOLDER__ROOT_FOLDER);
		createEAttribute(mResourceFolderEClass, MRESOURCE_FOLDER__DO_ACTION);
		createEOperation(mResourceFolderEClass,
				MRESOURCE_FOLDER___DO_ACTION$_UPDATE__MRESOURCEFOLDERACTION);
		createEOperation(mResourceFolderEClass,
				MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY);
		createEOperation(mResourceFolderEClass,
				MRESOURCE_FOLDER___UPDATE_RESOURCE__MPROPERTY_MPROPERTY);

		mResourceEClass = createEClass(MRESOURCE);
		createEReference(mResourceEClass, MRESOURCE__OBJECT);
		createEReference(mResourceEClass, MRESOURCE__ROOT_OBJECT_PACKAGE);
		createEReference(mResourceEClass, MRESOURCE__CONTAINING_FOLDER);
		createEAttribute(mResourceEClass, MRESOURCE__ID);
		createEAttribute(mResourceEClass, MRESOURCE__DO_ACTION);
		createEOperation(mResourceEClass,
				MRESOURCE___DO_ACTION$_UPDATE__MRESOURCEACTION);
		createEOperation(mResourceEClass, MRESOURCE___UPDATE__MPROPERTY);
		createEOperation(mResourceEClass,
				MRESOURCE___UPDATE_OBJECTS__MPROPERTY_MPROPERTY);
		createEOperation(mResourceEClass,
				MRESOURCE___UPDATE_CONTAINMENT_TO_STORED_FEATURE__MPROPERTY_MOBJECT);
		createEOperation(mResourceEClass,
				MRESOURCE___UPDATE_STORED_TO_CONTAINMENT_FEATURE__MPROPERTY_MOBJECT);
		createEOperation(mResourceEClass,
				MRESOURCE___ADD_OBJECTS__MPROPERTY_MOBJECT);

		mObjectEClass = createEClass(MOBJECT);
		createEReference(mObjectEClass, MOBJECT__PROPERTY_INSTANCE);
		createEReference(mObjectEClass, MOBJECT__TABLE);
		createEReference(mObjectEClass, MOBJECT__CREATE_TYPE_IN_PACKAGE);
		createEAttribute(mObjectEClass,
				MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE);
		createEAttribute(mObjectEClass, MOBJECT__NATURE);
		createEAttribute(mObjectEClass,
				MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION);
		createEAttribute(mObjectEClass, MOBJECT__NATURE_NEEDED);
		createEAttribute(mObjectEClass, MOBJECT__NATURE_MISSING);
		createEReference(mObjectEClass, MOBJECT__TYPE);
		createEReference(mObjectEClass, MOBJECT__TYPE_PACKAGE);
		createEAttribute(mObjectEClass, MOBJECT__NATURE_ENAME);
		createEAttribute(mObjectEClass, MOBJECT__CALCULATED_NATURE);
		createEReference(mObjectEClass, MOBJECT__INITIALIZATION_TYPE);
		createEAttribute(mObjectEClass, MOBJECT__ID);
		createEAttribute(mObjectEClass, MOBJECT__NUMERIC_ID_OF_OBJECT);
		createEReference(mObjectEClass, MOBJECT__CONTAINING_OBJECT);
		createEReference(mObjectEClass, MOBJECT__CONTAINING_PROPERTY_INSTANCE);
		createEReference(mObjectEClass, MOBJECT__RESOURCE);
		createEReference(mObjectEClass, MOBJECT__REFERENCED_BY);
		createEReference(mObjectEClass, MOBJECT__WRONG_PROPERTY_INSTANCE);
		createEAttribute(mObjectEClass,
				MOBJECT__INITIALIZED_CONTAINMENT_LEVELS);
		createEAttribute(mObjectEClass, MOBJECT__DO_ACTION);
		createEAttribute(mObjectEClass, MOBJECT__STATUS_AS_TEXT);
		createEAttribute(mObjectEClass, MOBJECT__DERIVATIONS_AS_TEXT);
		createEOperation(mObjectEClass,
				MOBJECT___CREATE_TYPE_IN_PACKAGE$_UPDATE__MPACKAGE);
		createEOperation(mObjectEClass,
				MOBJECT___CLASS_NAME_OR_NATURE_DEFINITION$_UPDATE__STRING);
		createEOperation(mObjectEClass,
				MOBJECT___DO_ACTION$_UPDATE__MOBJECTACTION);
		createEOperation(mObjectEClass,
				MOBJECT___PROPERTY_VALUE_AS_STRING__MPROPERTY);
		createEOperation(mObjectEClass,
				MOBJECT___PROPERTY_INSTANCE_FROM_PROPERTY__MPROPERTY);
		createEOperation(mObjectEClass, MOBJECT___IS_INSTANCE_OF__MCLASSIFIER);
		createEOperation(mObjectEClass,
				MOBJECT___CLASS_NAME_OR_NATURE_UPDATE__STRING);
		createEOperation(mObjectEClass, MOBJECT___CONTAINER_ID);
		createEOperation(mObjectEClass, MOBJECT___LOCAL_ID);
		createEOperation(mObjectEClass, MOBJECT___ID_PROPERTY_BASED_ID);
		createEOperation(mObjectEClass, MOBJECT___ID_PROPERTIES_AS_STRING);
		createEOperation(mObjectEClass, MOBJECT___IS_ROOT_OBJECT);
		createEOperation(mObjectEClass, MOBJECT___CORRECTLY_TYPED);
		createEOperation(mObjectEClass, MOBJECT___CORRECT_MULTIPLICITY);
		createEOperation(mObjectEClass,
				MOBJECT___DO_ACTION_UPDATE__MOBJECTACTION);
		createEOperation(mObjectEClass,
				MOBJECT___INVOKE_SET_DO_ACTION_UPDATE__MOBJECTACTION);

		mPropertyInstanceEClass = createEClass(MPROPERTY_INSTANCE);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__INTERNAL_DATA_VALUE);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__INTERNAL_LITERAL_VALUE);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__INTERNAL_REFERENCED_OBJECT);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__INTERNAL_CONTAINED_OBJECT);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__CONTAINING_OBJECT);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__DUPLICATE_INSTANCE);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__ALL_INSTANCES_OF_THIS_PROPERTY);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__WRONG_INTERNAL_CONTAINED_OBJECT_DUE_TO_KIND);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__WRONG_INTERNAL_OBJECT_REFERENCE_DUE_TO_KIND);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__WRONG_INTERNAL_LITERAL_VALUE_DUE_TO_KIND);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__WRONG_INTERNAL_DATA_VALUE_DUE_TO_KIND);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__NUMERIC_ID_OF_PROPERTY_INSTANCE);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__DERIVED_CONTAINMENT);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__LOCAL_KEY);
		createEReference(mPropertyInstanceEClass, MPROPERTY_INSTANCE__PROPERTY);
		createEAttribute(mPropertyInstanceEClass, MPROPERTY_INSTANCE__KEY);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__WRONG_LOCAL_KEY);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__DERIVED_KIND);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__PROPERTY_KIND_AS_STRING);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__CORRECT_PROPERTY_KIND);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__DO_ACTION);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__DO_ADD_VALUE);
		createEAttribute(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__SIMPLE_TYPE_FROM_VALUES);
		createEReference(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE__ENUMERATION_FROM_VALUES);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION$_UPDATE__STRING);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___DO_ACTION$_UPDATE__MPROPERTYINSTANCEACTION);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___DO_ADD_VALUE$_UPDATE__BOOLEAN);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___CORRECT_PROPERTY);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___CORRECT_MULTIPLICITY);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___PROPERTY_VALUE_AS_STRING);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___INTERNAL_REFERENCED_OBJECTS_AS_STRING);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___INTERNAL_DATA_VALUES_AS_STRING);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___INTERNAL_LITERAL_VALUES_AS_STRING);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___CHOOSABLE_PROPERTIES);
		createEOperation(mPropertyInstanceEClass,
				MPROPERTY_INSTANCE___PROPERTY_NAME_OR_LOCAL_KEY_DEFINITION_UPDATE__STRING);

		mValueEClass = createEClass(MVALUE);
		createEReference(mValueEClass, MVALUE__CONTAINING_PROPERTY_INSTANCE);
		createEReference(mValueEClass, MVALUE__CONTAINING_OBJECT);
		createEAttribute(mValueEClass, MVALUE__CORRECT_AND_DEFINED_VALUE);
		createEOperation(mValueEClass, MVALUE___CORRECT_MULTIPLICITY);

		mObjectReferenceEClass = createEClass(MOBJECT_REFERENCE);
		createEReference(mObjectReferenceEClass,
				MOBJECT_REFERENCE__REFERENCED_OBJECT);
		createEReference(mObjectReferenceEClass,
				MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT);
		createEAttribute(mObjectReferenceEClass,
				MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT);
		createEReference(mObjectReferenceEClass,
				MOBJECT_REFERENCE__CHOOSABLE_REFERENCE);
		createEOperation(mObjectReferenceEClass,
				MOBJECT_REFERENCE___CORRECTLY_TYPED_REFERENCED_OBJECT);
		createEOperation(mObjectReferenceEClass,
				MOBJECT_REFERENCE___CORRECT_MULTIPLICITY);

		mLiteralValueEClass = createEClass(MLITERAL_VALUE);
		createEReference(mLiteralValueEClass, MLITERAL_VALUE__LITERAL_VALUE);
		createEOperation(mLiteralValueEClass,
				MLITERAL_VALUE___CORRECT_LITERAL_VALUE);
		createEOperation(mLiteralValueEClass,
				MLITERAL_VALUE___CORRECT_MULTIPLICITY);

		mDataValueEClass = createEClass(MDATA_VALUE);
		createEAttribute(mDataValueEClass, MDATA_VALUE__DATA_VALUE);
		createEOperation(mDataValueEClass, MDATA_VALUE___CORRECT_DATA_VALUE);
		createEOperation(mDataValueEClass, MDATA_VALUE___CORRECT_MULTIPLICITY);

		// Create enums
		mResourceFolderActionEEnum = createEEnum(MRESOURCE_FOLDER_ACTION);
		mResourceActionEEnum = createEEnum(MRESOURCE_ACTION);
		mObjectActionEEnum = createEEnum(MOBJECT_ACTION);
		mPropertyInstanceActionEEnum = createEEnum(MPROPERTY_INSTANCE_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		McorePackage theMcorePackage = (McorePackage) EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI);
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);
		TablesPackage theTablesPackage = (TablesPackage) EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mResourceFolderEClass.getESuperTypes().add(theMcorePackage.getMNamed());
		mResourceEClass.getESuperTypes().add(theMcorePackage.getMNamed());
		mObjectEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mPropertyInstanceEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mValueEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mObjectReferenceEClass.getESuperTypes().add(this.getMValue());
		mLiteralValueEClass.getESuperTypes().add(this.getMValue());
		mDataValueEClass.getESuperTypes().add(this.getMValue());

		// Initialize classes, features, and operations; add parameters
		initEClass(mResourceFolderEClass, MResourceFolder.class,
				"MResourceFolder", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMResourceFolder_Resource(), this.getMResource(), null,
				"resource", null, 0, -1, MResourceFolder.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMResourceFolder_Folder(), this.getMResourceFolder(),
				null, "folder", null, 0, -1, MResourceFolder.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		getMResourceFolder_Folder().getEKeys()
				.add(theMcorePackage.getMNamed_Name());
		initEReference(getMResourceFolder_RootFolder(),
				this.getMResourceFolder(), null, "rootFolder", null, 0, 1,
				MResourceFolder.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMResourceFolder_DoAction(),
				this.getMResourceFolderAction(), "doAction", null, 0, 1,
				MResourceFolder.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(
				getMResourceFolder__DoAction$Update__MResourceFolderAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMResourceFolderAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMResourceFolder__UpdateResource__MProperty(),
				ecorePackage.getEBooleanObject(), "updateResource", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMResourceFolder__UpdateResource__MProperty_MProperty(),
				ecorePackage.getEBooleanObject(), "updateResource", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(),
				"correspondingProperty", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mResourceEClass, MResource.class, "MResource", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMResource_Object(), this.getMObject(), null, "object",
				null, 0, -1, MResource.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMResource_RootObjectPackage(),
				theMcorePackage.getMPackage(), null, "rootObjectPackage", null,
				0, 1, MResource.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMResource_ContainingFolder(),
				this.getMResourceFolder(), null, "containingFolder", null, 0, 1,
				MResource.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMResource_Id(), ecorePackage.getEString(), "id", null,
				0, 1, MResource.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMResource_DoAction(), this.getMResourceAction(),
				"doAction", null, 0, 1, MResource.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMResource__DoAction$Update__MResourceAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMResourceAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMResource__Update__MProperty(),
				ecorePackage.getEBooleanObject(), "update", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMResource__UpdateObjects__MProperty_MProperty(),
				ecorePackage.getEBooleanObject(), "updateObjects", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(),
				"correspondingProperty", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMResource__UpdateContainmentToStoredFeature__MProperty_MObject(),
				ecorePackage.getEBooleanObject(),
				"updateContainmentToStoredFeature", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObject(), "rootObj", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMResource__UpdateStoredToContainmentFeature__MProperty_MObject(),
				ecorePackage.getEBooleanObject(),
				"updateStoredToContainmentFeature", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(),
				"correspondingProperty", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObject(), "rootObj", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMResource__AddObjects__MProperty_MObject(),
				ecorePackage.getEBooleanObject(), "addObjects", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "property", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObject(), "rootObj", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mObjectEClass, MObject.class, "MObject", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMObject_PropertyInstance(),
				this.getMPropertyInstance(), null, "propertyInstance", null, 0,
				-1, MObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_Table(), theTablesPackage.getMTable(), null,
				"table", null, 0, 1, MObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_CreateTypeInPackage(),
				theMcorePackage.getMPackage(), null, "createTypeInPackage",
				null, 0, 1, MObject.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_HasPropertyInstanceNotOfType(),
				ecorePackage.getEBooleanObject(),
				"hasPropertyInstanceNotOfType", null, 0, 1, MObject.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_Nature(), ecorePackage.getEString(), "nature",
				null, 0, 1, MObject.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMObject_ClassNameOrNatureDefinition(),
				ecorePackage.getEString(), "classNameOrNatureDefinition", null,
				0, 1, MObject.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_NatureNeeded(),
				ecorePackage.getEBooleanObject(), "natureNeeded", null, 0, 1,
				MObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_NatureMissing(),
				ecorePackage.getEBooleanObject(), "natureMissing", null, 0, 1,
				MObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_Type(), theMcorePackage.getMClassifier(),
				null, "type", null, 0, 1, MObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_TypePackage(), theMcorePackage.getMPackage(),
				null, "typePackage", null, 0, 1, MObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_NatureEName(), ecorePackage.getEString(),
				"natureEName", null, 0, 1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_CalculatedNature(), ecorePackage.getEString(),
				"calculatedNature", null, 0, 1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_InitializationType(),
				theMcorePackage.getMClassifier(), null, "initializationType",
				null, 0, 1, MObject.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_Id(), ecorePackage.getEString(), "id", null,
				0, 1, MObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_NumericIdOfObject(),
				ecorePackage.getEString(), "numericIdOfObject", null, 1, 1,
				MObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_ContainingObject(), this.getMObject(), null,
				"containingObject", null, 0, 1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_ContainingPropertyInstance(),
				this.getMPropertyInstance(), null, "containingPropertyInstance",
				null, 0, 1, MObject.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_Resource(), this.getMResource(), null,
				"resource", null, 0, 1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_ReferencedBy(), this.getMObjectReference(),
				null, "referencedBy", null, 0, -1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMObject_WrongPropertyInstance(),
				this.getMPropertyInstance(), null, "wrongPropertyInstance",
				null, 0, -1, MObject.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_InitializedContainmentLevels(),
				ecorePackage.getEIntegerObject(),
				"initializedContainmentLevels", null, 0, 1, MObject.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_DoAction(), this.getMObjectAction(),
				"doAction", null, 0, 1, MObject.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_StatusAsText(), ecorePackage.getEString(),
				"statusAsText", null, 0, 1, MObject.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMObject_DerivationsAsText(),
				ecorePackage.getEString(), "derivationsAsText", null, 0, 1,
				MObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMObject__CreateTypeInPackage$Update__MPackage(),
				theSemanticsPackage.getXUpdate(), "createTypeInPackage$Update",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMPackage(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMObject__ClassNameOrNatureDefinition$Update__String(),
				theSemanticsPackage.getXUpdate(),
				"classNameOrNatureDefinition$Update", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMObject__DoAction$Update__MObjectAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObjectAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMObject__PropertyValueAsString__MProperty(),
				ecorePackage.getEString(), "propertyValueAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "p", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMObject__PropertyInstanceFromProperty__MProperty(),
				this.getMPropertyInstance(), "propertyInstanceFromProperty", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMProperty(), "p", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMObject__IsInstanceOf__MClassifier(),
				ecorePackage.getEBooleanObject(), "isInstanceOf", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theMcorePackage.getMClassifier(), "mClassifier", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMObject__ClassNameOrNatureUpdate__String(),
				theSemanticsPackage.getXUpdate(), "classNameOrNatureUpdate", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(),
				"classNameOrNatureDefinition", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__ContainerId(), ecorePackage.getEString(),
				"containerId", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__LocalId(), ecorePackage.getEString(),
				"localId", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__IdPropertyBasedId(),
				ecorePackage.getEString(), "idPropertyBasedId", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMObject__IdPropertiesAsString(),
				ecorePackage.getEString(), "idPropertiesAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__IsRootObject(),
				ecorePackage.getEBooleanObject(), "isRootObject", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__CorrectlyTyped(),
				ecorePackage.getEBooleanObject(), "correctlyTyped", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObject__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMObject__DoActionUpdate__MObjectAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObjectAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMObject__InvokeSetDoActionUpdate__MObjectAction(),
				ecorePackage.getEJavaObject(), "invokeSetDoActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMObjectAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mPropertyInstanceEClass, MPropertyInstance.class,
				"MPropertyInstance", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMPropertyInstance_InternalDataValue(),
				this.getMDataValue(), null, "internalDataValue", null, 0, -1,
				MPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyInstance_InternalLiteralValue(),
				this.getMLiteralValue(), null, "internalLiteralValue", null, 0,
				-1, MPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyInstance_InternalReferencedObject(),
				this.getMObjectReference(), null, "internalReferencedObject",
				null, 0, -1, MPropertyInstance.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyInstance_InternalContainedObject(),
				this.getMObject(), null, "internalContainedObject", null, 0, -1,
				MPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyInstance_ContainingObject(),
				this.getMObject(), null, "containingObject", null, 0, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyInstance_DuplicateInstance(),
				ecorePackage.getEBooleanObject(), "duplicateInstance", null, 0,
				1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyInstance_AllInstancesOfThisProperty(),
				this.getMPropertyInstance(), null, "allInstancesOfThisProperty",
				null, 0, -1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMPropertyInstance_WrongInternalContainedObjectDueToKind(),
				this.getMObject(), null,
				"wrongInternalContainedObjectDueToKind", null, 0, -1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMPropertyInstance_WrongInternalObjectReferenceDueToKind(),
				this.getMObjectReference(), null,
				"wrongInternalObjectReferenceDueToKind", null, 0, -1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMPropertyInstance_WrongInternalLiteralValueDueToKind(),
				this.getMLiteralValue(), null,
				"wrongInternalLiteralValueDueToKind", null, 0, -1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertyInstance_WrongInternalDataValueDueToKind(),
				this.getMDataValue(), null, "wrongInternalDataValueDueToKind",
				null, 0, -1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyInstance_NumericIdOfPropertyInstance(),
				ecorePackage.getEString(), "numericIdOfPropertyInstance", null,
				1, 1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_DerivedContainment(),
				ecorePackage.getEBooleanObject(), "derivedContainment", null, 0,
				1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_LocalKey(),
				ecorePackage.getEString(), "localKey", null, 0, 1,
				MPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyInstance_Property(),
				theMcorePackage.getMProperty(), null, "property", null, 0, 1,
				MPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyInstance_Key(), ecorePackage.getEString(),
				"key", null, 1, 1, MPropertyInstance.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertyInstance_PropertyNameOrLocalKeyDefinition(),
				ecorePackage.getEString(), "propertyNameOrLocalKeyDefinition",
				null, 0, 1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_WrongLocalKey(),
				ecorePackage.getEBooleanObject(), "wrongLocalKey", null, 1, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_DerivedKind(),
				theMcorePackage.getPropertyKind(), "derivedKind", null, 1, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_PropertyKindAsString(),
				ecorePackage.getEString(), "propertyKindAsString", null, 1, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_CorrectPropertyKind(),
				ecorePackage.getEBooleanObject(), "correctPropertyKind", null,
				1, 1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_DoAction(),
				this.getMPropertyInstanceAction(), "doAction", null, 0, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_DoAddValue(),
				ecorePackage.getEBooleanObject(), "doAddValue", null, 0, 1,
				MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertyInstance_SimpleTypeFromValues(),
				theMcorePackage.getSimpleType(), "simpleTypeFromValues", null,
				0, 1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertyInstance_EnumerationFromValues(),
				theMcorePackage.getMClassifier(), null, "enumerationFromValues",
				null, 0, 1, MPropertyInstance.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinition$Update__String(),
				theSemanticsPackage.getXUpdate(),
				"propertyNameOrLocalKeyDefinition$Update", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMPropertyInstance__DoAction$Update__MPropertyInstanceAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertyInstanceAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMPropertyInstance__DoAddValue$Update__Boolean(),
				theSemanticsPackage.getXUpdate(), "doAddValue$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__CorrectProperty(),
				ecorePackage.getEBooleanObject(), "correctProperty", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__PropertyValueAsString(),
				ecorePackage.getEString(), "propertyValueAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(
				getMPropertyInstance__InternalReferencedObjectsAsString(),
				ecorePackage.getEString(), "internalReferencedObjectsAsString",
				0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__InternalDataValuesAsString(),
				ecorePackage.getEString(), "internalDataValuesAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__InternalLiteralValuesAsString(),
				ecorePackage.getEString(), "internalLiteralValuesAsString", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertyInstance__ChoosableProperties(),
				theMcorePackage.getMProperty(), "choosableProperties", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String(),
				theSemanticsPackage.getXUpdate(),
				"propertyNameOrLocalKeyDefinitionUpdate", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(),
				"propertyNameOrLocalKeyDefinition", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mValueEClass, MValue.class, "MValue", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMValue_ContainingPropertyInstance(),
				this.getMPropertyInstance(), null, "containingPropertyInstance",
				null, 0, 1, MValue.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMValue_ContainingObject(), this.getMObject(), null,
				"containingObject", null, 0, 1, MValue.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMValue_CorrectAndDefinedValue(),
				ecorePackage.getEBooleanObject(), "correctAndDefinedValue",
				null, 0, 1, MValue.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		initEOperation(getMValue__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mObjectReferenceEClass, MObjectReference.class,
				"MObjectReference", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMObjectReference_ReferencedObject(),
				this.getMObject(), null, "referencedObject", null, 1, 1,
				MObjectReference.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectReference_TypeOfReferencedObject(),
				theMcorePackage.getMClassifier(), null,
				"typeOfReferencedObject", null, 0, 1, MObjectReference.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMObjectReference_IdOfReferencedObject(),
				ecorePackage.getEString(), "idOfReferencedObject", null, 0, 1,
				MObjectReference.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMObjectReference_ChoosableReference(),
				theMcorePackage.getMProperty(), null, "choosableReference",
				null, 0, -1, MObjectReference.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getMObjectReference__CorrectlyTypedReferencedObject(),
				ecorePackage.getEBooleanObject(),
				"correctlyTypedReferencedObject", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMObjectReference__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mLiteralValueEClass, MLiteralValue.class, "MLiteralValue",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMLiteralValue_LiteralValue(),
				theMcorePackage.getMLiteral(), null, "literalValue", null, 1, 1,
				MLiteralValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getMLiteralValue__CorrectLiteralValue(),
				ecorePackage.getEBooleanObject(), "correctLiteralValue", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMLiteralValue__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mDataValueEClass, MDataValue.class, "MDataValue",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMDataValue_DataValue(), ecorePackage.getEString(),
				"dataValue", null, 1, 1, MDataValue.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getMDataValue__CorrectDataValue(),
				ecorePackage.getEBooleanObject(), "correctDataValue", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMDataValue__CorrectMultiplicity(),
				ecorePackage.getEBooleanObject(), "correctMultiplicity", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(mResourceFolderActionEEnum, MResourceFolderAction.class,
				"MResourceFolderAction");
		addEEnumLiteral(mResourceFolderActionEEnum, MResourceFolderAction.DO);
		addEEnumLiteral(mResourceFolderActionEEnum,
				MResourceFolderAction.ADD_PROJECT);
		addEEnumLiteral(mResourceFolderActionEEnum,
				MResourceFolderAction.ADD_FOLDER);
		addEEnumLiteral(mResourceFolderActionEEnum,
				MResourceFolderAction.ADD_RESOURCE);
		addEEnumLiteral(mResourceFolderActionEEnum,
				MResourceFolderAction.OPEN_EDITOR);

		initEEnum(mResourceActionEEnum, MResourceAction.class,
				"MResourceAction");
		addEEnumLiteral(mResourceActionEEnum, MResourceAction.DO);
		addEEnumLiteral(mResourceActionEEnum, MResourceAction.OPEN_EDITOR);
		addEEnumLiteral(mResourceActionEEnum, MResourceAction.FIX_ALL_TYPES);
		addEEnumLiteral(mResourceActionEEnum, MResourceAction.OBJECT);

		initEEnum(mObjectActionEEnum, MObjectAction.class, "MObjectAction");
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.DO);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.FIX_TYPE);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.COMPLETE_SLOTS);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.CLEAN_SLOTS);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.STRING_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.BOOLEAN_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.INTEGER_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.REAL_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.DATE_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.LITERAL_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.REFERENCE_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.CONTAINMENT_SLOT);
		addEEnumLiteral(mObjectActionEEnum, MObjectAction.ADD_SIBLING);

		initEEnum(mPropertyInstanceActionEEnum, MPropertyInstanceAction.class,
				"MPropertyInstanceAction");
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.DO);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.ADD_PROPERTY_TO_TYPE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.CLEAR_VALUES);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.INTO_UNARY);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.INTO_NARY);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.INTO_CONTAINMENT);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.STRING_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.BOOLEAN_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.INTEGER_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.REAL_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.DATE_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.LITERAL_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.REFERENCED_OBJECT_VALUE);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.CONTAINED_OBJECT);
		addEEnumLiteral(mPropertyInstanceActionEEnum,
				MPropertyInstanceAction.CLASS_WITH_STRING_ATTRIBUTE);

		// Create annotations
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(mResourceFolderEClass, source,
				new String[] { "label", "self.calculatedShortName" });
		addAnnotation(
				getMResourceFolder__DoAction$Update__MResourceFolderAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMResourceFolder__UpdateResource__MProperty(), source,
				new String[] { "body", "null" });
		addAnnotation(getMResourceFolder__UpdateResource__MProperty_MProperty(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMResourceFolder_RootFolder(), source, new String[] {
				"derive",
				"if self.eContainer().oclIsTypeOf(MResourceFolder) then \r\nself.eContainer().oclAsType(MResourceFolder).rootFolder\r\nelse self endif" });
		addAnnotation(getMResourceFolder_DoAction(), source, new String[] {
				"derive",
				"let do: mcore::objects::MResourceFolderAction = mcore::objects::MResourceFolderAction::Do in\ndo\n",
				"choiceConstruction",
				"if self.rootFolder = self or self.indentLevel = 1 then OrderedSet{\r\nmcore::objects::MResourceFolderAction::Do, \r\nmcore::objects::MResourceFolderAction::AddProject}\r\nelse OrderedSet{\r\nmcore::objects::MResourceFolderAction::Do, \r\nmcore::objects::MResourceFolderAction::AddFolder,\r\nmcore::objects::MResourceFolderAction::AddResource}  endif           \r\n->append(MResourceFolderAction::OpenEditor)                         " });
		addAnnotation(mResourceEClass, source, new String[] { "label",
				"(if self.name.oclIsUndefined() \r\n  then \'NAME MISSING\' \r\n  else self.calculatedShortName endif)\r\n.concat(\'.\')\r\n.concat(if self.object->isEmpty() then \'OBJECT MISSING\' else\r\nif self.object->first().type.oclIsUndefined() then \'TYPE OF OBJECT MISSING\' else\r\nself.object->first().type.containingPackage.eName endif endif)" });
		addAnnotation(getMResource__DoAction$Update__MResourceAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMResource__Update__MProperty(), source,
				new String[] { "body", "null" });
		addAnnotation(getMResource__UpdateObjects__MProperty_MProperty(),
				source, new String[] { "body", "null\n" });
		addAnnotation(
				getMResource__UpdateContainmentToStoredFeature__MProperty_MObject(),
				source, new String[] { "body", "null\n" });
		addAnnotation(
				getMResource__UpdateStoredToContainmentFeature__MProperty_MObject(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMResource__AddObjects__MProperty_MObject(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getMResource_Object(), source, new String[] { "initOrder",
				"2", "initValue",
				"let p:MPackage=self.rootObjectPackage in\r\nif p.oclIsUndefined() \r\n  then OrderedSet{Tuple{type=let t:MClassifier=null in t}}\r\n  else OrderedSet{Tuple{type=p.resourceRootClass}} endif" });
		addAnnotation(getMResource_RootObjectPackage(), source, new String[] {
				"initOrder", "1", "initValue",
				"let f:MResourceFolder = self.containingFolder in\r\nif f.oclIsUndefined() then\r\n  null\r\nelse\r\n  let c:MComponent = self.containingFolder.rootFolder.eContainer().oclAsType(MComponent)   \r\n    in   if c.ownedPackage->isEmpty() \r\n        then null \r\n        else c.ownedPackage->first() endif endif" });
		addAnnotation(getMResource_ContainingFolder(), source, new String[] {
				"derive",
				"if   self.eContainer().oclIsKindOf(MResourceFolder) \r\n  then self.eContainer().oclAsType(MResourceFolder)\r\n  else null\r\n  endif" });
		addAnnotation(getMResource_Id(), source, new String[] { "initValue",
				"let f:MResourceFolder= self.containingFolder in\r\nif f.oclIsUndefined()\r\n  then \'R00\'\r\n  else let i:Integer=f.resource->size() in\r\n                    if i <10 \r\n                     then \'R0\'.concat(i.toString()) \r\n                     else \'R\'.concat(i.toString()) endif endif" });
		addAnnotation(getMResource_DoAction(), source, new String[] { "derive",
				"let do: mcore::objects::MResourceAction = mcore::objects::MResourceAction::Do in\ndo",
				"choiceConstruction",
				"if self.object->isEmpty() then OrderedSet{\nmcore::objects::MResourceAction::Do, \nmcore::objects::MResourceAction::OpenEditor,\nmcore::objects::MResourceAction::FixAllTypes,\nmcore::objects::MResourceAction::Object\n} else OrderedSet{\nmcore::objects::MResourceAction::Do, \nmcore::objects::MResourceAction::OpenEditor,\nmcore::objects::MResourceAction::FixAllTypes\n}  endif                           " });
		addAnnotation(mObjectEClass, source, new String[] { "label",
				"let rootString:String= if self.type.oclIsUndefined() then \'root\' else self.type.eName.camelCaseLower() endif in\r\nlet idString:String= if self.type.oclIsUndefined()\r\n      then \' id=\\\"\'.concat(self.id).concat(\'\\\"\') \r\nelse if self.type.idProperty->isEmpty()\r\n      then  \' id=\\\"\'.concat(self.id).concat(\'\\\"\') \r\n      else  self.idPropertiesAsString() endif endif\r\nin\r\n \'<\'\r\n.concat(\r\nif self.isRootObject() then\r\nrootString\r\nelse\r\n(let pi:MPropertyInstance = self.eContainer().oclAsType(MPropertyInstance) in\r\nlet p:MProperty = pi.property in\r\npi.key)\r\nendif)\r\n.concat(idString)\r\n.concat(\'>\')" });
		addAnnotation(getMObject__CreateTypeInPackage$Update__MPackage(),
				source, new String[] { "body", "null" });
		addAnnotation(getMObject__ClassNameOrNatureDefinition$Update__String(),
				source, new String[] { "body", "null" });
		addAnnotation(getMObject__DoAction$Update__MObjectAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMObject__PropertyValueAsString__MProperty(), source,
				new String[] { "body",
						"let pi:objects::MPropertyInstance = self.propertyInstanceFromProperty(p) in\r\nif pi = null then \'\' else\r\npi.propertyValueAsString()\r\nendif" });
		addAnnotation(getMObject__PropertyInstanceFromProperty__MProperty(),
				source, new String[] { "body",
						"if self.propertyInstance->isEmpty() then null\r\nelse \r\nlet pis:OrderedSet(objects::MPropertyInstance) = self.propertyInstance->select(pi:objects::MPropertyInstance|pi.property=p) in\r\nif pis->isEmpty() then null else\r\npis->first()\r\nendif endif" });
		addAnnotation(getMObject__IsInstanceOf__MClassifier(), source,
				new String[] { "body", "null" });
		addAnnotation(getMObject__ClassNameOrNatureUpdate__String(), source,
				new String[] { "body", "null" });
		addAnnotation(getMObject__ContainerId(), source, new String[] { "body",
				"if self.isRootObject() then \r\nlet resourceId:String=self.eContainer().oclAsType(MResource).id in\r\nif resourceId.oclIsUndefined() then \'MISSING RESOURCE ID\' else resourceId endif\r\nelse self.containingObject.id endif\r\n" });
		addAnnotation(getMObject__LocalId(), source, new String[] { "body",
				"if isRootObject() \r\n  then if resource.oclIsUndefined()\r\n    then \'GA\'\r\n   else  \r\n      let pos:Integer=self.resource.object->indexOf(self) in\r\n          if pos<10 \r\n            then \'0\'.concat(pos.toString()) \r\n            else pos.toString() endif endif\r\n  else let pos:Integer = containingPropertyInstance \r\n              .internalContainedObject\r\n                 ->indexOf(self) in\r\nif pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif\r\nendif" });
		addAnnotation(getMObject__IdPropertyBasedId(), source, new String[] {
				"body",
				"if self.type.oclIsUndefined() then \'\' else\r\nif self.type.allIdProperties()->isEmpty() then \'\' else\r\nif self.type.allIdProperties()->size()=1 then\r\nself.propertyValueAsString(self.type.allIdProperties()->first()) else\r\nself.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=\'\'|\r\n(if res=\'\' then \'\' else res.concat(\'|\') endif).concat(\r\nlet v:String=self.propertyValueAsString(p) in if v=\'\' then \'_\' else v endif\r\n))\r\nendif endif endif" });
		addAnnotation(getMObject__IdPropertiesAsString(), source, new String[] {
				"body",
				"if self.type.oclIsUndefined() then \'\' else\r\nif self.type.allIdProperties()->isEmpty() then \'\' else\r\nself.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=\' \'|\r\nres.concat(p.eName).concat(\'=\"\').concat(self.propertyValueAsString(p)).concat(\'\" \')\r\n)\r\nendif endif" });
		addAnnotation(getMObject__IsRootObject(), source, new String[] { "body",
				"if eContainer().oclIsUndefined() \r\n  then true \r\n  else not eContainer().oclIsTypeOf(MPropertyInstance) endif " });
		addAnnotation(getMObject__CorrectlyTyped(), source, new String[] {
				"body",
				"let t:MClassifier = self.type in\r\nif t.oclIsUndefined() then false\r\nelse \r\n/*typed but wrongly:*/\r\nif not (t.kind = mcore::ClassifierKind::ClassType) then false\r\nelse\r\nif t.abstractClass then false else\r\nif self.isRootObject() then true else\r\nlet p:MProperty = self.eContainer().oclAsType(MPropertyInstance).property in\r\nif p.oclIsUndefined() then true else\r\n/*wrongly typed based on type of property:*/\r\nif p.type.oclIsUndefined() then false else\r\nif p.type= self.type or p.type.allSubTypes()->includes(self.type) then true else false \r\nendif endif endif endif endif endif endif" });
		addAnnotation(getMObject__CorrectMultiplicity(), source, new String[] {
				"body",
				"if self.isRootObject() then true else\r\nlet pI:MPropertyInstance= self.containingPropertyInstance in\r\nif pI.property.oclIsUndefined() then false else\r\nif pI.property.singular then\r\nlet pos:Integer=pI.internalContainedObject->indexOf(self) in\r\npos=1\r\nelse true\r\nendif \r\nendif\r\nendif" });
		addAnnotation(getMObject__DoActionUpdate__MObjectAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMObject__InvokeSetDoActionUpdate__MObjectAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMObject_PropertyInstance(), source, new String[] {
				"initOrder", "2\n", "initValue",
				"/*initializationType.allFeaturesWithStorage()->collect(p|Tuple{property=p})*/\r\nif self.type.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.type.allFeaturesWithStorage()->isEmpty() then OrderedSet{}\r\nelse self.type.allFeaturesWithStorage()->collect(p:mcore::MProperty|Tuple{property=p})->asOrderedSet()\r\nendif endif\r\n" });
		addAnnotation(getMObject_CreateTypeInPackage(), source,
				new String[] { "derive", "null" });
		addAnnotation(getMObject_HasPropertyInstanceNotOfType(), source,
				new String[] { "derive",
						"if self.propertyInstance->isEmpty()\n   then false\n   else not (self.propertyInstance.correctProperty()->excludes(false)) endif" });
		addAnnotation(getMObject_ClassNameOrNatureDefinition(), source,
				new String[] { "derive",
						"if (natureNeeded) \n  =true \nthen nature\n  else if type.oclIsUndefined()\n  then null\n  else type.calculatedShortName\nendif\nendif\n" });
		addAnnotation(getMObject_NatureNeeded(), source, new String[] {
				"derive",
				"let e1: Boolean = if ( type.oclIsUndefined())= true \n then true \n else if (hasPropertyInstanceNotOfType)= true \n then true \nelse if (( type.oclIsUndefined())= null or (hasPropertyInstanceNotOfType)= null) = true \n then null \n else false endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getMObject_NatureMissing(), source, new String[] {
				"derive",
				"self.natureNeeded and \n(if self.nature.oclIsUndefined() \n    then true\n    else self.nature.trim() = \'\' endif)" });
		addAnnotation(getMObject_Type(), source, new String[] { "initOrder",
				"1\n", "initValue",
				"if self.type=null\r\n then self.initializationType\r\n else self.type endif",
				"choiceConstraint",
				"trg.kind = ClassifierKind::ClassType\r\nand\r\n trg.abstractClass = false\r\nand \r\nif self.isRootObject() \r\n  then let tP:MPackage=\r\n     if typePackage.oclIsUndefined()\r\n      then if resource.oclIsUndefined() \r\n        then null\r\n        else resource.rootObjectPackage endif\r\n      else typePackage endif in\r\n    trg.selectableClassifier(\r\n      if type.oclIsUndefined() \r\n        then OrderedSet{} else OrderedSet{type} endif,\r\n      tP)\r\nelse \r\nlet p:MProperty=self.eContainer().oclAsType(MPropertyInstance).property in\r\n  if p.oclIsUndefined() then true \r\n    else p.type = trg or \r\n            (if p.type.oclIsUndefined() then false \r\n                  else p.type.allSubTypes()->includes(trg) endif) endif \r\nendif" });
		addAnnotation(getMObject_NatureEName(), source, new String[] { "derive",
				"let n:String = calculatedNature in\r\nif n.oclIsUndefined() \r\n  then \'TYPE AND NATURE MISSING\' \r\n  else n.camelCaseUpper() endif" });
		addAnnotation(getMObject_CalculatedNature(), source, new String[] {
				"derive",
				"if (not type.oclIsUndefined()) and (not self.hasPropertyInstanceNotOfType)\r\n  then type.calculatedShortName\r\nelse if not (nature.oclIsUndefined() or nature=\'\') \r\n  then  nature\r\nelse  if containingPropertyInstance.oclIsUndefined() \r\n  then null\r\nelse if let l:String = containingPropertyInstance.localKey in \r\n                l.oclIsUndefined() or l=\'\' \r\n   then null\r\nelse  self.containingPropertyInstance.localKey.camelCaseToBusiness()\r\n  endif endif endif\r\nendif" });
		addAnnotation(getMObject_InitializationType(), source, new String[] {
				"derive",
				"if self.isRootObject() then \r\n if self.resource.rootObjectPackage.oclIsUndefined() then null\r\n else \r\n    let p:mcore::MPackage=self.resource.rootObjectPackage  in\r\n    if p.resourceRootClass.oclIsUndefined()  then \r\n      let cs:OrderedSet(MClassifier) =  p.classifier->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.abstractClass=false) in if cs->isEmpty() then null else cs->first() endif\r\n    else p.resourceRootClass\r\n    endif\r\n  endif\r\nelse\r\nlet p:MProperty= self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then null else \r\nif p.type.oclIsUndefined() then null\r\nelse\r\nif not p.type.abstractClass then p.type\r\n else \r\n let cs:OrderedSet(mcore::MClassifier)=p.type.allSubTypes()->select(c:mcore::MClassifier| not c.abstractClass) in \r\n if cs->isEmpty() then null\r\n else cs->last()\r\nendif endif endif\r\nendif endif" });
		addAnnotation(getMObject_Id(), source, new String[] { "derive",
				"if not type.oclIsUndefined() \r\n  then if  type.allIdProperties()->notEmpty() \r\n    then idPropertyBasedId() \r\n    else numericIdOfObject endif\r\n  else numericIdOfObject endif" });
		addAnnotation(getMObject_NumericIdOfObject(), source, new String[] {
				"derive",
				"containerId().concat(\'_\').concat(\r\n if isRootObject() \r\n   then localId()\r\n  else\r\nlet p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then \r\nself.containingPropertyInstance.numericIdOfPropertyInstance\r\n.concat(\'.\').concat(self.localId()) else\r\nif p.singular then p.id else\r\np.id.concat(\'.\').concat(self.localId())\r\nendif endif endif)\r\n" });
		addAnnotation(getMObject_ContainingObject(), source, new String[] {
				"derive",
				"if self.isRootObject() then null\r\nelse self.containingPropertyInstance.containingObject endif" });
		addAnnotation(getMObject_ContainingPropertyInstance(), source,
				new String[] { "derive",
						"if self.isRootObject() then null\r\nelse self.eContainer().oclAsType(MPropertyInstance) endif" });
		addAnnotation(getMObject_Resource(), source, new String[] { "derive",
				"if self.isRootObject() then \r\nif self.eContainer().oclIsKindOf(MResource) then\r\nself.eContainer().oclAsType(MResource) else null endif\r\nelse self.containingObject.resource endif" });
		addAnnotation(getMObject_ReferencedBy(), source, new String[] {
				"derive",
				"MObjectReference.allInstances()->select(referencedObject= self)" });
		addAnnotation(getMObject_WrongPropertyInstance(), source, new String[] {
				"derive",
				"\r\npropertyInstance->reject(\r\ni:MPropertyInstance | (  let correctInternalValue: Boolean = i.wrongInternalContainedObjectDueToKind->isEmpty() and i.wrongInternalDataValueDueToKind->isEmpty() and\r\ni.wrongInternalLiteralValueDueToKind->isEmpty() and\r\ni.wrongInternalObjectReferenceDueToKind->isEmpty() in\r\n\r\n i.correctMultiplicity() and i.correctProperty() and  not(i.wrongLocalKey) and correctInternalValue))" });
		addAnnotation(getMObject_InitializedContainmentLevels(), source,
				new String[] { "initValue",
						"let levelsToInitializeForRootObjects: Integer = 3 in\nif  containingObject = null\n  then levelsToInitializeForRootObjects\nelse if containingObject.initializedContainmentLevels=null then\n     levelsToInitializeForRootObjects\nelse   \n   containingObject.initializedContainmentLevels - 1 \n endif endif\n" });
		addAnnotation(getMObject_DoAction(), source, new String[] { "derive",
				"let do: mcore::objects::MObjectAction = mcore::objects::MObjectAction::Do in\ndo",
				"choiceConstruction",
				"let instantiatedProperties:OrderedSet(MProperty) = self.propertyInstance.property->excluding(null)->asOrderedSet() in \nlet nonInstantiatedProperties:OrderedSet(MProperty) =\n  if type.oclIsUndefined() then OrderedSet{} else\n     type.allFeaturesWithStorage()->select(p:MProperty | instantiatedProperties->excludes(p)) endif in\nlet start:OrderedSet(MObjectAction) = OrderedSet{MObjectAction::Do} in start->append(\nif type.oclIsUndefined() or hasPropertyInstanceNotOfType then \n   MObjectAction::FixType else null endif)->append(\nif not nonInstantiatedProperties->isEmpty() then \n   MObjectAction::CompleteSlots else null endif)->append(\nif type.oclIsUndefined() then null else\n  if  not self.hasPropertyInstanceNotOfType\n   then null else\n   MObjectAction::CleanSlots endif endif)->append(\nif true then\n   MObjectAction::StringSlot else null endif)->append(\nif true then \n   MObjectAction::BooleanSlot else null endif)->append(\nif true then \n   MObjectAction::IntegerSlot else null endif)->append(\nif true then \n   MObjectAction::RealSlot else null endif)->append(\nif true then \n   MObjectAction::DateSlot else null endif)->append(\nif true then \n   MObjectAction::LiteralSlot else null endif)->append(\nif true then \n   MObjectAction::ReferenceSlot else null endif)->append(\nif true then \n   MObjectAction::ContainmentSlot else null endif)->append(MObjectAction::AddSibling)->excluding(null)\n" });
		addAnnotation(mPropertyInstanceEClass, source, new String[] { "label",
				"if self.derivedContainment\r\n    /*then  \'<...\'.concat(\'\').concat(\'(\'.concat(self.internalContainedObject->size().toString())).concat(\' \').concat(self.key).concat(\'(s))...>\') */\r\n    then  \'<\'.concat(self.key).concat(\'>\') \r\n    else self.key.concat(\'=\"\').concat(\r\nif self.derivedKind=mcore::PropertyKind::Attribute \r\n   then\r\n     if self.property.oclIsUndefined() \r\n       then  self.internalDataValuesAsString()\r\n     else if self.property.type.oclIsUndefined() \r\n       then  self.internalDataValuesAsString()  \r\n       else  if self.property.type.kind=mcore::ClassifierKind::DataType \r\n           then  self.internalDataValuesAsString()    \r\n           else self.internalLiteralValuesAsString() endif endif endif\r\n  else self.internalReferencedObjectsAsString() endif \r\n .concat(\'\"\')\r\n )\r\n endif" });
		addAnnotation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinition$Update__String(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMPropertyInstance__DoAction$Update__MPropertyInstanceAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMPropertyInstance__DoAddValue$Update__Boolean(),
				source, new String[] { "body", "null" });
		addAnnotation(getMPropertyInstance__CorrectProperty(), source,
				new String[] { "body",
						"let t:MClassifier=self.containingObject.type in\r\nlet p:MProperty=self.property in\r\nif t.oclIsUndefined() or p.oclIsUndefined() then false\r\nelse t.kind=ClassifierKind::ClassType and t.allProperties()->includes(p) \r\nand not(p.kind = PropertyKind::Operation) endif" });
		addAnnotation(getMPropertyInstance__CorrectMultiplicity(), source,
				new String[] { "body",
						"let p:MProperty=self.property in\r\nif p.oclIsUndefined() then false\r\nelse if p.kind = PropertyKind::Attribute then\r\n  self.internalContainedObject->size()=0 \r\n  and\r\n  (if p.singular then self.internalDataValue->size() <2 else\r\n  if p.mandatory then self.internalDataValue->size() > 0 else true endif endif)\r\n  and \r\n  self.internalReferencedObject->size()=0\r\nelse if p.kind = PropertyKind::Reference then \r\n  if p.containment = false then\r\n    self.internalContainedObject->size()=0 \r\n    and\r\n   self.internalDataValue->size() = 0 \r\n    and \r\n    (if p.singular then self.internalReferencedObject->size() <2 else\r\n    if p.mandatory then self.internalReferencedObject->size() > 0 else true \r\n    endif   endif)\r\n else\r\n    (if p.singular then self.internalContainedObject->size() <2 else\r\n    if p.mandatory then self.internalContainedObject->size() > 0 else true \r\n    endif   endif)\r\n    and\r\n   self.internalDataValue->size() = 0 \r\n    and \r\n    self.internalReferencedObject->size() = 0\r\n endif\r\n else false\r\n endif endif endif" });
		addAnnotation(getMPropertyInstance__PropertyValueAsString(), source,
				new String[] { "body",
						"if self.property.oclIsUndefined() then \'\' else\r\n  if self.property.kind=mcore::PropertyKind::Reference then\r\n    self.internalReferencedObjectsAsString()\r\nelse\r\n    if self.property.kind=mcore::PropertyKind::Attribute then\r\n      if self.property.type.oclIsUndefined() then\r\n         if not (self.property.simpleType =SimpleType::None) then \r\n            self.internalDataValuesAsString() else \'ERROR3\' endif\r\n      else      \r\n        if self.property.type.kind=mcore::ClassifierKind::DataType then\r\n          self.internalDataValuesAsString() else\r\n          if self.property.type.kind=mcore::ClassifierKind::Enumeration then\r\n             self.internalLiteralValuesAsString() else\r\n               \'ERROR1\' endif endif endif\r\n    else \'ERROR2\' \r\n  endif \r\nendif endif\r\n/*\r\nif self.type.oclIsUndefined() then \'\' else\r\nif self.type.idProperty->isEmpty() then \'\' else\r\nself.type.idProperty->iterate(p:mcore::MProperty;res:String=\'\'|\r\n(if res=\'\' then \'\' else res.concat(\'|\') endif).concat(self.propertyValueAsString(p)))\r\nendif endif*/" });
		addAnnotation(getMPropertyInstance__InternalReferencedObjectsAsString(),
				source, new String[] { "body",
						"let vs:OrderedSet(objects::MObjectReference) = self.internalReferencedObject in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MObjectReference;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.referencedObject.oclIsUndefined() then \r\n                \'OBJECT MISSING\' else \r\n                if r.referencedObject=self.containingObject then \'self\' else\r\n                let c:mcore::MClassifier=r.typeOfReferencedObject in \r\n                if c.oclIsUndefined() then r.referencedObject.id else\r\n                  if c.allIdProperties()->isEmpty() then\r\n                    r.referencedObject.id \r\n                  else if c.allIdProperties()->size()>1 then\r\n                   \'(\'.concat(r.referencedObject.id ).concat(\')\')\r\n                   else  r.referencedObject.id\r\n                  endif  endif endif endif endif))\r\nendif " });
		addAnnotation(getMPropertyInstance__InternalDataValuesAsString(),
				source, new String[] { "body",
						"let vs:OrderedSet(objects::MDataValue) = self.internalDataValue in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MDataValue;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.dataValue.oclIsUndefined() then \r\n                               \'VALUE MISSING\' else r.dataValue endif))\r\nendif " });
		addAnnotation(getMPropertyInstance__InternalLiteralValuesAsString(),
				source, new String[] { "body",
						"let vs:OrderedSet(objects::MLiteralValue) = self.internalLiteralValue in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MLiteralValue;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.literalValue.oclIsUndefined() then \r\n                                \'LITERAL MISSING\' else \r\n                                r.literalValue.eName endif))\r\nendif " });
		addAnnotation(getMPropertyInstance__ChoosableProperties(), source,
				new String[] { "body",
						"/*see as well allFeaturesWithStorage of type.*/\r\nlet t:MClassifier =containingObject.type in\r\nif t.oclIsUndefined() \r\n then if self.property.oclIsUndefined() then OrderedSet{} else self.property->asOrderedSet() endif\r\n else\r\ncontainingObject.type.allProperties()->\r\nreject(p:MProperty| \r\n  p.kind=PropertyKind::Operation or \r\n not( p.hasStorage=true) or\r\n  (not (p = self.property) and self.containingObject.propertyInstance.property->includes(p)))\r\nendif" });
		addAnnotation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String(),
				source, new String[] { "body", "null" });
		addAnnotation(getMPropertyInstance_InternalDataValue(), source,
				new String[] { "initValue",
						"if not self.internalDataValue->isEmpty()\r\nthen self.internalDataValue->collect(dv:MDataValue|Tuple{dataValue=dv.dataValue})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then \r\n    if self.property.simpleType=SimpleType::None then OrderedSet{} else\r\n      if self.property.hasSimpleModelingType \r\n        then OrderedSet{} else\r\n         OrderedSet{Tuple{dataValue=\'\'}}\r\n    endif endif\r\n  else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then \r\n               OrderedSet{Tuple{dataValue=\'\'}}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     OrderedSet{}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_InternalLiteralValue(), source,
				new String[] { "initValue",
						"if not self.internalLiteralValue->isEmpty()\r\n  then self.internalLiteralValue->collect(lv:MLiteralValue|Tuple{literalValue=lv.literalValue})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then OrderedSet{} else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then OrderedSet{}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     let nullLit:mcore::MLiteral=null in\r\n     OrderedSet{Tuple{literalValue=nullLit}}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_InternalReferencedObject(), source,
				new String[] { "initValue",
						"if not self.internalReferencedObject->isEmpty()\r\nthen\r\n\tself.internalReferencedObject->collect(oRef:MObjectReference|Tuple{referencedObject=oRef.referencedObject})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nlet nullObj:objects::MObject=null in\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{Tuple{referencedObject=nullObj}} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then OrderedSet{} else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then OrderedSet{}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     OrderedSet{}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_InternalContainedObject(), source,
				new String[] { "initValue",
						"if true then OrderedSet{} else\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif  not self.property.containment then OrderedSet{} else\r\nif not (self.property.kind = mcore::PropertyKind::Reference) then  OrderedSet{} else\r\nif self.property.type.oclIsUndefined() then OrderedSet{} else\r\nif self.property.type.hasSimpleModelingType then OrderedSet{} else\r\nif \r\nfalse\r\n then OrderedSet{} else\r\nlet types:OrderedSet(MClassifier) = \r\nself.property.type.allSubTypes()->prepend(self.property.type)\r\n->reject(abstractClass)->excluding(null)->asOrderedSet()\r\nin\r\nif types->size()=0 then OrderedSet{} else\r\nif self.property.singular then\r\n  OrderedSet{Tuple{type=types->first()}}\r\n  else types->collect(c:MClassifier|Tuple{type=c})\r\nendif endif endif endif endif endif endif endif\r\nendif\r\n" });
		addAnnotation(getMPropertyInstance_ContainingObject(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(MObject)" });
		addAnnotation(getMPropertyInstance_DuplicateInstance(), source,
				new String[] { "derive",
						"self.allInstancesOfThisProperty->size()>1" });
		addAnnotation(getMPropertyInstance_AllInstancesOfThisProperty(), source,
				new String[] { "derive",
						"if property.oclIsUndefined() then OrderedSet{}\r\nelse self.containingObject.propertyInstance->select(p:MPropertyInstance|p.property=property) endif" });
		addAnnotation(
				getMPropertyInstance_WrongInternalContainedObjectDueToKind(),
				source, new String[] { "derive",
						"if property.oclIsUndefined() then OrderedSet{}\r\nelse if property.containment then OrderedSet{}\r\nelse self.internalContainedObject endif endif" });
		addAnnotation(
				getMPropertyInstance_WrongInternalObjectReferenceDueToKind(),
				source, new String[] { "derive",
						"if self.property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.containment \r\n              or (self.property.kind <> PropertyKind::Reference) \r\n      then self.internalReferencedObject\r\n                 ->reject(r:MObjectReference|r.oclIsUndefined()) \r\n      else OrderedSet{} endif endif\r\n" });
		addAnnotation(getMPropertyInstance_WrongInternalLiteralValueDueToKind(),
				source, new String[] { "derive",
						"if property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if self.property.type.kind <> ClassifierKind::Enumeration\r\n      then self.internalLiteralValue->reject(l:MLiteralValue| l.literalValue.oclIsUndefined()) \r\n      else OrderedSet{} endif endif endif\r\n" });
		addAnnotation(getMPropertyInstance_WrongInternalDataValueDueToKind(),
				source, new String[] { "derive",
						"if property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if self.property.type.kind <> ClassifierKind::DataType\r\n      then self.internalDataValue->reject(d:MDataValue| d.dataValue.oclIsUndefined() or ( d.dataValue=\'\')) \r\n      else OrderedSet{} endif endif endif\r\n" });
		addAnnotation(getMPropertyInstance_NumericIdOfPropertyInstance(),
				source, new String[] { "derive",
						"if self.eContainer().oclIsKindOf(MObject) \r\n  then let o:MObject=self.eContainer().oclAsType(MObject) in\r\n     let pos:Integer= o.propertyInstance->indexOf(self) in\r\n       if pos.oclIsUndefined() \r\n           then \'POS NOT CALCULATABLE\'  \r\n       else if pos<10 \r\n          then \'0\'.concat(pos.toString()) \r\n          else pos.toString() endif  endif\r\n   else \'PROPERTY INSTANCE NOT CONTAINED IN OBJECT\' endif " });
		addAnnotation(getMPropertyInstance_DerivedContainment(), source,
				new String[] { "derive",
						"if not self.property.oclIsUndefined() \r\n  then self.property.containment\r\nelse if self.internalContainedObject->notEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then true\r\n  else false endif endif" });
		addAnnotation(getMPropertyInstance_Property(), source,
				new String[] { "initValue",
						"if not self.property.oclIsUndefined() then self.property else\r\nlet ps:OrderedSet(MProperty)=self.choosableProperties() in\r\nif ps->size() > 0 then ps->first() else null endif endif",
						"choiceConstruction", "self.choosableProperties()" });
		addAnnotation(getMPropertyInstance_Key(), source, new String[] {
				"derive",
				"/*todo: check for trimmed versions of empty strings...SYSTEM*/\r\nif  not property.oclIsUndefined()\r\n   then self.property.eName \r\n   else if not ((self.localKey=\'\') or self.localKey.oclIsUndefined())\r\n                then self.localKey.camelCaseLower()\r\n                else \tif self.derivedKind<>PropertyKind::Reference\r\n                \t             then \'KEY MISSING\'\r\n                \t         else if self.internalReferencedObject->notEmpty()\r\n                \t              then let o:MObject = \r\n                \t                            self.internalReferencedObject->first().referencedObject in\r\n                \t                       if o.oclIsUndefined()\r\n                \t                               then \'KEY MISSING\'\r\n                \t                       else if o.nature.oclIsUndefined() or o.nature=\'\'\r\n                \t                                then if not o.type.oclIsUndefined()\r\n                \t                                             then o.type.eName\r\n                \t                                             else\' KEY MISSING\' endif\r\n                \t                       else o.nature.camelCaseLower() endif endif\r\n                \t         else if self.internalContainedObject->notEmpty()  \r\n                \t               then let o:MObject = \r\n                \t                            self.internalContainedObject->first() in\r\n               \t                       if o.nature.oclIsUndefined() or o.nature=\'\'\r\n                \t                                then if not o.type.oclIsUndefined()\r\n                \t                                             then o.type.eName\r\n                \t                                             else\' KEY MISSING\' endif\r\n                \t                       else o.nature.camelCaseLower() endif\r\n                \t         else \'KEY MISSING\' endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_PropertyNameOrLocalKeyDefinition(),
				source, new String[] { "derive",
						"if ( property.oclIsUndefined()) \n  =true \nthen localKey\n  else if property.oclIsUndefined()\n  then null\n  else property.calculatedShortName\nendif\nendif\n" });
		addAnnotation(getMPropertyInstance_WrongLocalKey(), source,
				new String[] { "derive",
						"let localKeyNotSet:Boolean=self.localKey.oclIsUndefined() or (self.localKey=\'\') in\r\nif self.property.oclIsUndefined()\r\n  then if internalReferencedObject->notEmpty() \r\n             or internalContainedObject->notEmpty()\r\n             then false\r\n             else  localKeyNotSet endif\r\nelse if localKeyNotSet\r\n  then false\r\n  else self.property.eName <> self.localKey.camelCaseLower() \r\nendif endif" });
		addAnnotation(getMPropertyInstance_DerivedKind(), source, new String[] {
				"derive",
				"if not self.property.oclIsUndefined() \r\n  then self.property.kind\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Undefined\r\nelse if self.internalContainedObject->notEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Reference\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->notEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Attribute\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->notEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Attribute\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->notEmpty()\r\n  then PropertyKind::Reference\r\n  else PropertyKind::Ambiguous endif endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_PropertyKindAsString(), source,
				new String[] { "derive",
						"let notStorable:Boolean =\r\nif self.property.oclIsUndefined()=false\r\n  then not (self.property.hasStorage=true) else false endif in\r\nif notStorable\r\n  then  \'ERROR: PROPERTY NOT STORABLE\'\r\nelse  if self.derivedKind=PropertyKind::Attribute \r\n  then \'Attribute\'\r\nelse if self.derivedKind=PropertyKind::Reference\r\n  then if self.derivedContainment \r\n    then \'Containment\'\r\n    else  \'Reference\' endif\r\nelse if self.derivedKind=PropertyKind::Operation\r\n   then \'ERROR:PROPERTY IS OPERATION\' else if self.derivedKind=PropertyKind::Ambiguous\r\n   then \'ERROR:VALUES AMBIGUOUS\' \r\n else if self.derivedKind = PropertyKind::TypeMissing\r\n   then \'Untyped Property\'\r\nelse \'ERROR: SET VALUE OR PROPERTY\' \r\nendif endif endif endif endif endif" });
		addAnnotation(getMPropertyInstance_CorrectPropertyKind(), source,
				new String[] { "derive",
						"self.derivedKind=PropertyKind::Attribute \r\n    or self.derivedKind=PropertyKind::Reference" });
		addAnnotation(getMPropertyInstance_DoAction(), source, new String[] {
				"derive",
				"let do: mcore::objects::MPropertyInstanceAction = mcore::objects::MPropertyInstanceAction::Do in\ndo",
				"choiceConstruction",
				"let onlyDataValues:Boolean = internalLiteralValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in\nlet onlyLiteralValues:Boolean = internalDataValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in\nlet onlyReferencedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalContainedObject->isEmpty()  in\nlet onlyContainedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalReferencedObject->isEmpty()   in\n\nlet start:OrderedSet(MPropertyInstanceAction) = OrderedSet{MPropertyInstanceAction::Do} in start->append(\nif containingObject.type.oclIsUndefined() then null else\n   if property.oclIsUndefined() then  \n        MPropertyInstanceAction::AddPropertyToType else\n   if containingObject.type.allFeaturesWithStorage()->excludes(property) then\n        MPropertyInstanceAction::AddPropertyToType else null endif endif endif)->append(\nif not property.oclIsUndefined() then \n   if property.calculatedSingular then\n        MPropertyInstanceAction::IntoNary else MPropertyInstanceAction::IntoUnary endif else null endif)->append(\nif property.oclIsUndefined() then \n     if self.derivedKind = PropertyKind::Reference and self.derivedContainment = false then     \n           MPropertyInstanceAction::IntoContainment else null endif\nelse if property.kind = PropertyKind::Reference and property.containment = false then \n           MPropertyInstanceAction::IntoContainment else null endif endif)->append(\nif ((not property.oclIsUndefined()) and property.calculatedSimpleType = SimpleType::String) or self.simpleTypeFromValues = SimpleType::String\n  \tthen MPropertyInstanceAction::ClassWithStringAttribute\n \telse null endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::StringValue else null endif else\n   if property.calculatedSimpleType = SimpleType::String then\n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::StringValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::BooleanValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Boolean then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::BooleanValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::IntegerValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Integer then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::IntegerValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::RealValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Double then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::RealValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::DateValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Date then \n         if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::DateValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyReferencedObjects then\n        MPropertyInstanceAction::ReferencedObjectValue else null endif else\n   if property.kind = PropertyKind::Reference and property.containment = false then \n         if (not property.singular) or internalReferencedObject->isEmpty() then \n        MPropertyInstanceAction::ReferencedObjectValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyContainedObjects then\n        MPropertyInstanceAction::ContainedObject else null endif else\n   if property.kind = PropertyKind::Reference and property.containment = true then \n        MPropertyInstanceAction::ContainedObject else null endif endif)->append(\n        if internalDataValue->notEmpty() or internalLiteralValue->notEmpty() or internalReferencedObject->notEmpty() or internalContainedObject->notEmpty() then \n   MPropertyInstanceAction::ClearValues else null endif)->excluding(null)" });
		addAnnotation(getMPropertyInstance_DoAddValue(), source,
				new String[] { "derive", "null\n" });
		addAnnotation(getMPropertyInstance_SimpleTypeFromValues(), source,
				new String[] { "derive", "/*overwritten in Java*/ null" });
		addAnnotation(getMPropertyInstance_EnumerationFromValues(), source,
				new String[] { "derive", "/*Overwritten in Java */ null" });
		addAnnotation(getMValue__CorrectMultiplicity(), source,
				new String[] { "body", "false" });
		addAnnotation(getMValue_ContainingPropertyInstance(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(MPropertyInstance)" });
		addAnnotation(getMValue_ContainingObject(), source, new String[] {
				"derive", "self.containingPropertyInstance.containingObject" });
		addAnnotation(getMValue_CorrectAndDefinedValue(), source,
				new String[] { "derive", "false\n" });
		addAnnotation(mObjectReferenceEClass, source, new String[] { "label",
				"let o:MObject=self.referencedObject in\r\nif o.oclIsUndefined() then \'OBJECT REFERENCE MISSING\' \r\nelse o.id.concat(\r\nif o.type.oclIsUndefined() then \': TYPE MISSING\' \r\nelse \': \'.concat(o.type.name) endif\r\n)\r\nendif" });
		addAnnotation(getMObjectReference__CorrectlyTypedReferencedObject(),
				source, new String[] { "body",
						"let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:MProperty = pi.property in\r\nif p.oclIsUndefined() then \r\n   pi.internalLiteralValue->isEmpty()\r\n    and pi.internalDataValue->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\nelse\r\nlet o:MObject = self.referencedObject in\r\nif p.oclIsUndefined() or o.oclIsUndefined() then false else\r\nif p.kind = mcore::PropertyKind::Reference\r\nthen p.type = o.type or p.type.allSubTypes()->includes(o.type)\r\nelse false endif\r\nendif endif" });
		addAnnotation(getMObjectReference__CorrectMultiplicity(), source,
				new String[] { "body",
						"let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalReferencedObject->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif" });
		addAnnotation(getMObjectReference_ReferencedObject(), source,
				new String[] { "choiceConstraint",
						"let p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() \r\n  then true \r\n  else if p.type.oclIsUndefined() \r\n     then if p.simpleType = SimpleType::Object \r\n        then true \r\n        else false endif\r\n     else if p.type.simpleType = SimpleType::Object\r\n       then true\r\n       else (trg.type = p.type) \r\n           or p.type.allSubTypes()->includes(trg.type)\r\nendif endif endif" });
		addAnnotation(getMObjectReference_TypeOfReferencedObject(), source,
				new String[] { "derive",
						"if self.referencedObject.oclIsUndefined() then null else\r\nself.referencedObject.type endif" });
		addAnnotation(getMObjectReference_IdOfReferencedObject(), source,
				new String[] { "derive",
						"if self.referencedObject.oclIsUndefined() then \'OBJECT REFERENCE MISSING\' else\r\nself.referencedObject.id endif" });
		addAnnotation(getMObjectReference_ChoosableReference(), source,
				new String[] { "derive",
						"let o:MObject = self.referencedObject in\r\nlet source:MObject = self.containingObject in\r\nif o.oclIsUndefined() \r\n  or source.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if o.type.oclIsUndefined()\r\n      or source.type.oclIsUndefined()\r\n     then OrderedSet{} \r\n     else source.type.allFeaturesWithStorage()\r\n           ->select(p:MProperty|\r\n                 (p.containment = false) and\r\n                 ( if p.type.oclIsUndefined() \r\n                    then false \r\n                    else ( p.type = o.type) \r\n                       or p.type.allSubTypes()->includes(o.type) endif)\r\n                )\r\nendif endif\r\n" });
		addAnnotation(mLiteralValueEClass, source, new String[] { "label",
				"if self.literalValue.oclIsUndefined() then \'LITERAL VALUE MISSING\' else\r\nself.literalValue.eName endif\r\n\r\n" });
		addAnnotation(getMLiteralValue__CorrectLiteralValue(), source,
				new String[] { "body",
						"let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:MProperty = pi.property in\r\nif p.oclIsUndefined() then\r\n    pi.internalDataValue->isEmpty()\r\n    and pi.internalReferencedObject->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\nelse\r\np.kind = PropertyKind::Attribute \r\nand p.type.kind = ClassifierKind::Enumeration\r\nand (if self.literalValue.oclIsUndefined() then true else\r\np.type.allLiterals()->includes(self.literalValue)\r\nendif)\r\nendif" });
		addAnnotation(getMLiteralValue__CorrectMultiplicity(), source,
				new String[] { "body",
						"let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalLiteralValue->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif" });
		addAnnotation(getMLiteralValue_LiteralValue(), source, new String[] {
				"choiceConstraint",
				"let p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() \r\n  then  let pos:Integer\r\n                   =self.containingPropertyInstance\r\n                        .internalLiteralValue->indexOf(self) in\r\n    if pos=1 \r\n       then true\r\n    else if trg.containingEnumeration.oclIsUndefined() \r\n       then false\r\n    else let firstLiteralValue:MLiteralValue \r\n                 =  self.containingPropertyInstance \r\n                       .internalLiteralValue->first() in\r\n       if firstLiteralValue.literalValue.oclIsUndefined() \r\n         then false\r\n         else firstLiteralValue.literalValue.containingEnumeration\r\n                     = trg.containingEnumeration endif endif endif\r\nelse if p.type.oclIsUndefined() \r\n  then false \r\n  else p.type.allLiterals()->includes(trg)\r\nendif endif" });
		addAnnotation(mDataValueEClass, source,
				new String[] { "label", "self.dataValue\r\n\r\n" });
		addAnnotation(getMDataValue__CorrectDataValue(), source, new String[] {
				"body",
				"let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:mcore::MProperty = pi.property in\r\nif p.oclIsUndefined() \r\n  then pi.internalLiteralValue->isEmpty()\r\n    and pi.internalReferencedObject->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\n  else (p.kind = mcore::PropertyKind::Attribute) \r\n       and p.correctlyTyped endif" });
		addAnnotation(getMDataValue__CorrectMultiplicity(), source,
				new String[] { "body",
						"let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalDataValue->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mResourceFolderEClass, source, new String[] {
				"kindLabelDerive",
				"let resultofKindLabel: String = if (let e0: Boolean = if ((let e0: Boolean = rootFolder = self in \n if e0.oclIsInvalid() then null else e0 endif))= false \n then false \n else if (let e0: Boolean = indentLevel = 1 in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = rootFolder = self in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = indentLevel = 1 in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'Workspace\' else if (let e0: Boolean = indentLevel = 2 in \n if e0.oclIsInvalid() then null else e0 endif)=true then \'Project\'\n  else \'Folder\'\nendif endif in\nresultofKindLabel\n",
				"eNameDerive",
				"if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())\r\nthen self.calculatedShortName\r\nelse self.specialEName.camelCaseLower()\r\nendif" });
		addAnnotation(mResourceEClass, source, new String[] { "kindLabelDerive",
				"\'Resource\'\n", "eNameDerive",
				"let nSPrefix: String = if (let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif) \n  =true \nthen let subchain1 : mcore::objects::MObject = let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif in \n if subchain1 = null \n  then null \n else if subchain1.type.containingPackage.oclIsUndefined()\n  then null\n  else subchain1.type.containingPackage.derivedNsPrefix\nendif endif \n else if (let chain: mcore::MPackage = rootObjectPackage in\nif chain <> null then true else false \n  endif)=true then if rootObjectPackage.oclIsUndefined()\n  then null\n  else rootObjectPackage.derivedNsPrefix\nendif\n  else \'ROOT OBJECT PACKAGE MISSING\'\nendif endif in\nlet namePlusNSPrefix: String = let e1: String = calculatedShortName.concat(\'.\').concat(nSPrefix) in \n if e1.oclIsInvalid() then null else e1 endif in\nnamePlusNSPrefix\n" });
		addAnnotation(mObjectEClass, source, new String[] { "kindLabelDerive",
				"if (isRootObject()) \n  =true \nthen \'Root Object\'\n  else \'Object\'\nendif\n" });
		addAnnotation(mPropertyInstanceEClass, source,
				new String[] { "kindLabelDerive", "propertyKindAsString\n" });
		addAnnotation(mObjectReferenceEClass, source, new String[] {
				"kindLabelDerive", "\'Object Reference\'\n",
				"correctAndDefinedValueDerive",
				"let var1: Boolean = let e1: Boolean = if (let chain11: mcore::objects::MObject = referencedObject in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctlyTypedReferencedObject())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: mcore::objects::MObject = referencedObject in\nif chain11 <> null then true else false \n  endif)= null or (correctlyTypedReferencedObject())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n" });
		addAnnotation(mLiteralValueEClass, source, new String[] {
				"kindLabelDerive", "\'Literal Value\'\n",
				"correctAndDefinedValueDerive",
				"let var1: Boolean = let e1: Boolean = if (let chain11: mcore::MLiteral = literalValue in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctLiteralValue())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: mcore::MLiteral = literalValue in\nif chain11 <> null then true else false \n  endif)= null or (correctLiteralValue())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n" });
		addAnnotation(mDataValueEClass, source, new String[] {
				"kindLabelDerive", "\'Data Value\'\n",
				"correctAndDefinedValueDerive",
				"let var1: Boolean = let e1: Boolean = if (let chain11: String = dataValue in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctDataValue())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: String = dataValue in\nif chain11 <> null then true else false \n  endif)= null or (correctDataValue())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(getMResourceFolder__UpdateResource__MProperty(), source,
				new String[] { "mName", "updateResource" });
		addAnnotation(
				(getMResourceFolder__UpdateResource__MProperty())
						.getEParameters().get(0),
				source, new String[] { "mName", "property" });
		addAnnotation(getMResourceFolder__UpdateResource__MProperty_MProperty(),
				source, new String[] { "mName", "updateResource" });
		addAnnotation(
				(getMResourceFolder__UpdateResource__MProperty_MProperty())
						.getEParameters().get(0),
				source, new String[] { "mName", "property" });
		addAnnotation(
				(getMResourceFolder__UpdateResource__MProperty_MProperty())
						.getEParameters().get(1),
				source, new String[] { "mName", "corresponding Property" });
		addAnnotation(getMResource__Update__MProperty(), source,
				new String[] { "mName", "update" });
		addAnnotation(
				(getMResource__Update__MProperty()).getEParameters().get(0),
				source, new String[] { "mName", "property" });
		addAnnotation(getMResource__UpdateObjects__MProperty_MProperty(),
				source, new String[] { "mName", "updateObjects" });
		addAnnotation(
				(getMResource__UpdateObjects__MProperty_MProperty())
						.getEParameters().get(0),
				source, new String[] { "mName", "property" });
		addAnnotation(
				(getMResource__UpdateObjects__MProperty_MProperty())
						.getEParameters().get(1),
				source, new String[] { "mName", "corresponding Property" });
		addAnnotation(
				getMResource__UpdateContainmentToStoredFeature__MProperty_MObject(),
				source,
				new String[] { "mName", "updateContainmentToStoredFeature" });
		addAnnotation(
				(getMResource__UpdateContainmentToStoredFeature__MProperty_MObject())
						.getEParameters().get(0),
				source, new String[] { "mName", "property" });
		addAnnotation(
				(getMResource__UpdateContainmentToStoredFeature__MProperty_MObject())
						.getEParameters().get(1),
				source, new String[] { "mName", "rootObj" });
		addAnnotation(
				getMResource__UpdateStoredToContainmentFeature__MProperty_MObject(),
				source,
				new String[] { "mName", "updateStoredToContainmentFeature" });
		addAnnotation(
				(getMResource__UpdateStoredToContainmentFeature__MProperty_MObject())
						.getEParameters().get(0),
				source, new String[] { "mName", "corresponding Property" });
		addAnnotation(
				(getMResource__UpdateStoredToContainmentFeature__MProperty_MObject())
						.getEParameters().get(1),
				source, new String[] { "mName", "rootObj" });
		addAnnotation(getMResource__AddObjects__MProperty_MObject(), source,
				new String[] { "mName", "addObjects" });
		addAnnotation((getMResource__AddObjects__MProperty_MObject())
				.getEParameters().get(0), source,
				new String[] { "mName", "property" });
		addAnnotation((getMResource__AddObjects__MProperty_MObject())
				.getEParameters().get(1), source,
				new String[] { "mName", "rootObj" });
		addAnnotation(getMObject__IsInstanceOf__MClassifier(), source,
				new String[] { "mName", "isInstanceOf" });
		addAnnotation(getMObject__ClassNameOrNatureUpdate__String(), source,
				new String[] { "mName", "ClassNameOrNatureUpdate" });
		addAnnotation(
				(getMObject__ClassNameOrNatureUpdate__String()).getEParameters()
						.get(0),
				source,
				new String[] { "mName", "classNameOrNatureDefinition" });
		addAnnotation(getMObject__DoActionUpdate__MObjectAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation((getMObject__DoActionUpdate__MObjectAction())
				.getEParameters().get(0), source,
				new String[] { "mName", "trg" });
		addAnnotation(getMObject__InvokeSetDoActionUpdate__MObjectAction(),
				source,
				new String[] { "mName", "invoke Set Do Action Update" });
		addAnnotation(
				(getMObject__InvokeSetDoActionUpdate__MObjectAction())
						.getEParameters().get(0),
				source, new String[] { "mName", "trg" });
		addAnnotation(getMObject_ReferencedBy(), source,
				new String[] { "mName", "referenced By" });
		addAnnotation(getMObject_WrongPropertyInstance(), source,
				new String[] { "mName", "wrong Property Instance" });
		addAnnotation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String(),
				source, new String[] { "mName",
						"propertyNameOrLocalKeyDefinitionUpdate" });
		addAnnotation(
				(getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String())
						.getEParameters().get(0),
				source,
				new String[] { "mName", "propertyNameOrLocalKeyDefinition" });
		addAnnotation(getMValue_CorrectAndDefinedValue(), source,
				new String[] { "mName", "CorrectAndDefinedValue" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getMResourceFolder_RootFolder(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMResourceFolder_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMResource__UpdateObjects__MProperty_MProperty(),
				source,
				new String[] { "propertyCategory",
						"Default Containment Operations", "createColumn",
						"true" });
		addAnnotation(
				getMResource__UpdateContainmentToStoredFeature__MProperty_MObject(),
				source,
				new String[] { "propertyCategory",
						"Default Containment Operations", "createColumn",
						"true" });
		addAnnotation(
				getMResource__UpdateStoredToContainmentFeature__MProperty_MObject(),
				source,
				new String[] { "propertyCategory",
						"Default Containment Operations", "createColumn",
						"true" });
		addAnnotation(getMResource__AddObjects__MProperty_MObject(), source,
				new String[] { "propertyCategory",
						"Default Containment Operations", "createColumn",
						"true" });
		addAnnotation(getMResource_RootObjectPackage(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMResource_ContainingFolder(), source, new String[] {
				"propertyCategory", "Structural", "createColumn", "false" });
		addAnnotation(getMResource_Id(), source, new String[] { "createColumn",
				"false", "propertyCategory", "Object Id" });
		addAnnotation(getMResource_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMObject__ClassNameOrNatureUpdate__String(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "true" });
		addAnnotation(getMObject__ContainerId(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "true" });
		addAnnotation(getMObject__LocalId(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "true" });
		addAnnotation(getMObject__IdPropertyBasedId(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "true" });
		addAnnotation(getMObject__IdPropertiesAsString(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "true" });
		addAnnotation(getMObject__IsRootObject(), source, new String[] {
				"propertyCategory", "Structural", "createColumn", "true" });
		addAnnotation(getMObject__CorrectlyTyped(), source, new String[] {
				"propertyCategory", "Validation", "createColumn", "true" });
		addAnnotation(getMObject__CorrectMultiplicity(), source, new String[] {
				"propertyCategory", "Validation", "createColumn", "true" });
		addAnnotation(getMObject__DoActionUpdate__MObjectAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn",
						"true" });
		addAnnotation(getMObject__InvokeSetDoActionUpdate__MObjectAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMObject_CreateTypeInPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Derivation" });
		addAnnotation(getMObject_HasPropertyInstanceNotOfType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Derivation" });
		addAnnotation(getMObject_Nature(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "false" });
		addAnnotation(getMObject_ClassNameOrNatureDefinition(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Nature and Type" });
		addAnnotation(getMObject_NatureNeeded(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Nature and Type" });
		addAnnotation(getMObject_NatureMissing(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "false" });
		addAnnotation(getMObject_Type(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "false" });
		addAnnotation(getMObject_TypePackage(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "false" });
		addAnnotation(getMObject_NatureEName(), source,
				new String[] { "propertyCategory", "Nature and Type",
						"createColumn", "false" });
		addAnnotation(getMObject_CalculatedNature(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Nature and Type" });
		addAnnotation(getMObject_InitializationType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Nature and Type" });
		addAnnotation(getMObject_Id(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "false" });
		addAnnotation(getMObject_NumericIdOfObject(), source, new String[] {
				"propertyCategory", "Object Id", "createColumn", "false" });
		addAnnotation(getMObject_ContainingObject(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Structural" });
		addAnnotation(getMObject_ContainingPropertyInstance(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Structural" });
		addAnnotation(getMObject_Resource(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Structural" });
		addAnnotation(getMObject_ReferencedBy(), source, new String[] {
				"propertyCategory", "Structural", "createColumn", "false" });
		addAnnotation(getMObject_WrongPropertyInstance(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Validation" });
		addAnnotation(getMObject_InitializedContainmentLevels(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Initialization" });
		addAnnotation(getMObject_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMObject_StatusAsText(), source,
				new String[] { "propertyCategory", "Summary As Text",
						"createColumn", "false" });
		addAnnotation(getMObject_DerivationsAsText(), source,
				new String[] { "propertyCategory", "Summary As Text",
						"createColumn", "false" });
		addAnnotation(
				getMPropertyInstance__PropertyNameOrLocalKeyDefinitionUpdate__String(),
				source, new String[] { "propertyCategory", "Key and Property",
						"createColumn", "true" });
		addAnnotation(getMPropertyInstance_LocalKey(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_Property(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_Key(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_PropertyNameOrLocalKeyDefinition(),
				source, new String[] { "createColumn", "false",
						"propertyCategory", "Key and Property" });
		addAnnotation(getMPropertyInstance_WrongLocalKey(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_DerivedKind(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_PropertyKindAsString(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_CorrectPropertyKind(), source,
				new String[] { "propertyCategory", "Key and Property",
						"createColumn", "false" });
		addAnnotation(getMPropertyInstance_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMPropertyInstance_DoAddValue(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMPropertyInstance_SimpleTypeFromValues(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Typing" });
		addAnnotation(getMPropertyInstance_EnumerationFromValues(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"false" });
		addAnnotation(getMValue_ContainingPropertyInstance(), source,
				new String[] { "createColumn", "true" });
		addAnnotation(getMValue_CorrectAndDefinedValue(), source, new String[] {
				"propertyCategory", "Validation", "createColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMResourceFolder_RootFolder(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMResourceFolder_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMResource_RootObjectPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMResource_Id(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMResource_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMObject_CreateTypeInPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_HasPropertyInstanceNotOfType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_ClassNameOrNatureDefinition(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_NatureNeeded(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_CalculatedNature(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_InitializationType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_WrongPropertyInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_InitializedContainmentLevels(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMObject_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMPropertyInstance_Property(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMPropertyInstance_PropertyNameOrLocalKeyDefinition(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertyInstance_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMPropertyInstance_DoAddValue(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertyInstance_SimpleTypeFromValues(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
	}

} //ObjectsPackageImpl
