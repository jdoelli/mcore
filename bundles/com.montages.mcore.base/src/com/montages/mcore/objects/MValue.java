/**
 */
package com.montages.mcore.objects;

import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MValue</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MValue#getContainingPropertyInstance <em>Containing Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MValue#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MValue#getCorrectAndDefinedValue <em>Correct And Defined Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMValue()
 * @model abstract="true"
 * @generated
 */

public interface MValue extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Property Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Property Instance</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMValue_ContainingPropertyInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MPropertyInstance)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true'"
	 * @generated
	 */
	MPropertyInstance getContainingPropertyInstance();

	/**
	 * Returns the value of the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Object</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMValue_ContainingObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.containingPropertyInstance.containingObject'"
	 * @generated
	 */
	MObject getContainingObject();

	/**
	 * Returns the value of the '<em><b>Correct And Defined Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correct And Defined Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correct And Defined Value</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMValue_CorrectAndDefinedValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='CorrectAndDefinedValue'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Validation' createColumn='false'"
	 * @generated
	 */
	Boolean getCorrectAndDefinedValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='false'"
	 * @generated
	 */
	Boolean correctMultiplicity();

} // MValue
