/**
 */
package com.montages.mcore.objects;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MResource Folder Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolderAction()
 * @model
 * @generated
 */
public enum MResourceFolderAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Add Project</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_PROJECT_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_PROJECT(1, "AddProject", "+ Project"),
	/**
	 * The '<em><b>Add Folder</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ADD_FOLDER_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_FOLDER(2, "AddFolder", "+ Folder"),

	/**
	 * The '<em><b>Add Resource</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_RESOURCE_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_RESOURCE(3, "AddResource", "+ Resource"),
	/**
	 * The '<em><b>Open Editor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #OPEN_EDITOR_VALUE
	 * @generated
	 * @ordered
	 */
	OPEN_EDITOR(4, "OpenEditor", "Open Dynamic Editor");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Add Project</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Project</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_PROJECT
	 * @model name="AddProject" literal="+ Project"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_PROJECT_VALUE = 1;

	/**
	 * The '<em><b>Add Folder</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Folder</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_FOLDER
	 * @model name="AddFolder" literal="+ Folder"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_FOLDER_VALUE = 2;

	/**
	 * The '<em><b>Add Resource</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Resource</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_RESOURCE
	 * @model name="AddResource" literal="+ Resource"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_RESOURCE_VALUE = 3;

	/**
	 * The '<em><b>Open Editor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Open Editor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPEN_EDITOR
	 * @model name="OpenEditor" literal="Open Dynamic Editor"
	 * @generated
	 * @ordered
	 */
	public static final int OPEN_EDITOR_VALUE = 4;

	/**
	 * An array of all the '<em><b>MResource Folder Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MResourceFolderAction[] VALUES_ARRAY = new MResourceFolderAction[] {
			DO, ADD_PROJECT, ADD_FOLDER, ADD_RESOURCE, OPEN_EDITOR, };

	/**
	 * A public read-only list of all the '<em><b>MResource Folder Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MResourceFolderAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MResource Folder Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MResourceFolderAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MResourceFolderAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MResource Folder Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MResourceFolderAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MResourceFolderAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MResource Folder Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MResourceFolderAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case ADD_PROJECT_VALUE:
			return ADD_PROJECT;
		case ADD_FOLDER_VALUE:
			return ADD_FOLDER;
		case ADD_RESOURCE_VALUE:
			return ADD_RESOURCE;
		case OPEN_EDITOR_VALUE:
			return OPEN_EDITOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MResourceFolderAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MResourceFolderAction
