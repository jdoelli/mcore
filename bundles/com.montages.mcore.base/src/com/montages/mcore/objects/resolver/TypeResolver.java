package com.montages.mcore.objects.resolver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.xocl.core.util.XoclHelper;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;

public class TypeResolver {

	/**
	 * Aligns the given object with its type: property in the type which are
	 * mandatory that are not in the object are added; property instances in the
	 * object that have no assigned property, will trigger creation of
	 * corresponding properties in the classifier. PWK: Actually, a subtype of
	 * the classifier is created! If the object has not type, a new classifier
	 * is created in given package. The process is applied to the object and any
	 * other object it contains or references.
	 * 
	 * @param pkg
	 *            The package where newly created classifiers are added.
	 * @param obj
	 *            The MObject to be aligned with the classifier.
	 * @return Classifier for the object, possibly newly created.
	 * 
	 * @added
	 */
	public static MClassifier alignClassifierWithObject(MObject obj,
			MPackage pkg) {
		List<MClassifier> newClassifiers = new ArrayList<MClassifier>();
		HashMap<String, MClassifier> createdNatures = new HashMap<String, MClassifier>();
		List<MProperty> untypedProperties = new ArrayList<MProperty>();

		MClassifier newType = assignClassifierToObject(obj, pkg,
				new ArrayList<MObject>(), createdNatures, newClassifiers);
		alignPropertiesWithClassifier(obj, new ArrayList<MObject>(),
				createdNatures, newClassifiers, untypedProperties);
		return newType;
	}

	/**
	 * Check that given object has an assigned classifier. If not, a new
	 * classifier is created in given package and assigned to it. The process is
	 * repeated for all objects contained or referenced in given object. Note
	 * that object in the same feature will be all assigned the same type.
	 *
	 * PWK20121028 Try to make sure, that for the defintion of the resulting
	 * classifier, the "natureEName" is used, since I added some intelligence to
	 * that.
	 * 
	 * @param pkg
	 *            The package where newly created classifiers are added.
	 * @param obj
	 *            The MObject to be aligned with the classifier.
	 * @param visited
	 *            The list of objects already visited, to avoid loops if objects
	 *            have circular references.
	 * @param createdNature
	 *            For newly created type, if the type was created from
	 *            MObject.nature, the type is stored here so we will use same
	 *            type for all objects with same nature.
	 * @param newClassifiers
	 *            Contains the list of classifiers that will be created as
	 *            result of calling this method.
	 * @return Classifier for the object, possibly newly created.
	 * 
	 * @added
	 */
	private static MClassifier assignClassifierToObject(MObject obj,
			MPackage pkg, List<MObject> visited,
			Map<String, MClassifier> createdNature,
			List<MClassifier> newClassifiers) {

		if (visited.contains(obj)) {
			return obj.getType();
		}

		// find out current classifier, if any
		MClassifier classifier = obj.getType();
		if (classifier == null) {
			classifier = createNewClassifier(obj, pkg, null, createdNature,
					newClassifiers);
			obj.setType(classifier);
		}

		// mark this as visited, before triggering any recursion
		visited.add(obj);

		// recourse into referenced objects
		for (MPropertyInstance p : obj.getPropertyInstance()) {

			// let's get all objects referenced/contained in this property
			List<MObject> contained = collectObjects(p);
			if (contained.size() == 0)
				continue;

			MClassifier propertyType = (p.getProperty() == null) ? null
					: p.getProperty().getType();

			if (propertyType != null) {
				// property has a type, we use it for all untyped objects
				for (MObject o : contained) {
					if (o.getType() == null)
						// TODO if object has nature, what should we do?
						// PWK: ??
						o.setType(propertyType);
				}
				continue; // go to next property
			} else {
				// things get difficult here, as we must consider sub/super
				// typing

				// first let's create a classifier we will assign to all untyped
				// object with no nature
				MClassifier defaultType = createNewClassifier(
						(p.getProperty() == null) ? p.getLocalKey()
								: p.getProperty().getName(),
						pkg, true, getSuperType(p), newClassifiers);
				defaultType.setAbstractClass(true);
				boolean defaultTypeUsed = false;

				for (MObject o : contained) {
					if (o.getType() == null) {
						if (o.getNatureEName() == null) {
							// totally untyped, use default
							o.setType(defaultType);
							defaultType.setAbstractClass(false);
							defaultType.setEnforcedClass(defaultType
									.getEnforcedClass()
									&& (o.getPropertyInstance().size() == 0));
							defaultTypeUsed = true;
						} else {
							// use nature
							o.setType(createNewClassifier(o, pkg, null,
									createdNature, newClassifiers));
						}
					}
				}

				// Figure out a common super type
				MClassifier superType = null;
				MClassifier defaultSuperType = null;
				for (MObject o : contained) {
					MClassifier t = o.getType();
					if (superType == null)
						superType = t;
					else if (superType == t)
						continue;
					else if (superType.getSuperType().contains(t))
						superType = t;
					else if (!t.getSuperType().contains(superType)) {
						// we have two types with no type in common, me make all
						// sub types of defaultSuperType
						if (defaultSuperType == null) {
							if (!defaultTypeUsed) {
								// default type was not used, let's recycle it
								// for this
								defaultSuperType = defaultType;
								defaultTypeUsed = true;
							} else {
								// default type is used as classifier for one or
								// more object, create another abstract
								// classifier
								// to contain all referenced objects
								defaultSuperType = createNewClassifier(
										"Abstract " + defaultType.getName(),
										defaultType.getContainingPackage(),
										true, defaultType, newClassifiers);
								defaultSuperType.setAbstractClass(true);
							}
						}

						if (t != defaultSuperType)
							t.getSuperType().add(defaultSuperType);
						if (superType != defaultSuperType)
							superType.getSuperType().add(defaultSuperType);
						superType = defaultSuperType;
					}
				}

				// remove default type, if not necessary
				if (!defaultTypeUsed) {
					pkg.getClassifier().remove(defaultType);
					newClassifiers.remove(defaultType);
				}

				// NOTE: the property type is assigned later....

				// Recourse into contained objects....
				for (MObject o : contained) {
					assignClassifierToObject(o, pkg, visited, createdNature,
							newClassifiers);
				}

			} // property type == null
		} // for each property

		return classifier;
	}

	/**
	 * Create new classifier in given package.
	 * 
	 * @param obj
	 * @param name
	 * @param pkg
	 * @return
	 */
	private static MClassifier createNewClassifier(MObject obj, MPackage pkg,
			MClassifier superType, Map<String, MClassifier> createdNature,
			List<MClassifier> newClassifiers) {

		MClassifier classifier = null;

		String name = (obj.getNatureEName() == null) ? null
				: obj.getNatureEName().trim();
		if ((name != null) && (!name.equals(""))) {
			// see if we have a classifier for this nature
			if (createdNature.containsKey(name)) {
				classifier = createdNature.get(name);
			} else {
				// PWK20121028 here the problem was using getNature instead of
				// getNatureEName.
				// SOLUTION PROPOSED: use new function calculatedNature()
				classifier = createNewClassifier(obj.getCalculatedNature(), pkg,
						(obj.getPropertyInstance().size() == 0), superType,
						newClassifiers);
				createdNature.put(name, classifier);
			}
			obj.setNature(null);
		} else {
			name = "New Type from " + obj.getNumericIdOfObject()
					.replace("MISSING RESOURCE", "");
			classifier = createNewClassifier(name, pkg,
					(obj.getPropertyInstance().size() == 0), superType,
					newClassifiers);
		}

		return classifier;
	}

	/**
	 * Create new classifier in given package.
	 * 
	 * @param obj
	 * @param name
	 * @param pkg
	 * @return
	 */
	private static MClassifier createNewClassifier(String name, MPackage pkg,
			boolean forceClassBehavior, MClassifier superType,
			List<MClassifier> newClassifiers) {
		MClassifier classifier = McoreFactory.eINSTANCE.createMClassifier();
		classifier
				.setName((name == null) ? "NEW CLASSIFIER WITH NO NAME" : name);
		classifier.setEnforcedClass(forceClassBehavior);
		if ((superType != null)
				&& !classifier.getSuperType().contains(superType))
			classifier.getSuperType().add(superType);

		// store it in pkg
		pkg.getClassifier().add(classifier);
		if (pkg.getResourceRootClass() == null)
			pkg.setResourceRootClass(classifier);

		newClassifiers.add(classifier);
		return classifier;
	}

	/**
	 * Aligns the given object with its type: property in the type which are
	 * mandatory that are not in the object are added; property instances in the
	 * object that have no assigned property, will trigger creation of
	 * corresponding properties in the classifier. The process is applied to the
	 * object and any other object it contains or references.
	 * 
	 * @param obj
	 *            The MObject to be aligned with the classifier.
	 * @param visited
	 *            The list of objects already visited, to avoid loops if objects
	 *            have circular references.
	 * @param createdNatures
	 * @param newClassifiers
	 *            Contains the list of classifiers that are newly created, this
	 *            might be treated differently than ones already existing.
	 * @param untypedProperties
	 *            Contains the list of properties added so far for which a type
	 *            could not be found (corresponding property instance was
	 *            empty).
	 * 
	 * @added
	 */
	private static void alignPropertiesWithClassifier(MObject obj,
			List<MObject> visited, HashMap<String, MClassifier> createdNatures,
			List<MClassifier> newClassifiers,
			List<MProperty> untypedProperties) {

		if (obj == null || visited.contains(obj)) {
			return;
		}

		addPropertiesToClassifier(obj, createdNatures, newClassifiers,
				untypedProperties);
		addPropertiesFromClassifier(obj, createdNatures, newClassifiers,
				untypedProperties);
		visited.add(obj);

		for (MObject o : collectObjects(obj)) {
			alignPropertiesWithClassifier(o, visited, createdNatures,
					newClassifiers, untypedProperties);
		}
	}

	/**
	 * For newly created classifiers, property instances in the object that have
	 * no assigned property, will trigger creation of corresponding properties
	 * in the classifier. For objects of an already existing type, new property
	 * instances not mapped to any existing property, will trigger creation of a
	 * sub type for the object type, with new properties. The process is applied
	 * to the object and any other object it contains or references.
	 * 
	 * @param obj
	 *            The MObject to be aligned with the classifier.
	 * @param createdNatures
	 * @param newClassifiers
	 *            Contains the list of classifiers that are newly created, this
	 *            might be treated differently than ones already existing.
	 * @param untypedProperties
	 *            Contains the list of properties added so far for which a type
	 *            could not be found (corresponding property instance was
	 *            empty).
	 * 
	 * @added
	 */
	private static void addPropertiesToClassifier(MObject obj,
			HashMap<String, MClassifier> createdNatures,
			List<MClassifier> newClassifiers,
			List<MProperty> untypedProperties) {

		if (obj == null) {
			System.out.println("null object");
			return;
		}

		MClassifier classifier = obj.getType();

		// add properties to the classifier
		for (MPropertyInstance pi : obj.getPropertyInstance()) {

			if (pi.getProperty() != null)
				continue;

			// If local key is null, name will be derived
			// PWK: key, being derived from localKey, property, or referenced
			// object type should be used.
			// OLD: String name = p.getLocalKey();
			String eNameFromKey = "NOTSET";
			if (pi.getLocalKey() != null
					&& !pi.getLocalKey().trim().equals("")) {
				eNameFromKey = XoclHelper.camelCaseLower(pi.getLocalKey());
			} else {

				if (pi.getDerivedKind() == PropertyKind.REFERENCE) {
					MObject o = null;
					if (pi.getDerivedContainment()) {
						o = pi.getInternalContainedObject().get(0);
					} else {
						try {
							o = pi.getInternalReferencedObject().get(0)
									.getReferencedObject();
						} catch (Exception e) {
							// No problem
						}
					}
					if (o != null) {
						if (o.getType() != null) {
							eNameFromKey = XoclHelper.camelCaseLower(
									o.getType().getCalculatedShortName());
						} else {
							if (o.getNature() != null
									&& !o.getNature().trim().equals("")) {
								eNameFromKey = XoclHelper
										.camelCaseLower(o.getNature());
							}

						}
					}
				}
			}

			// Check if any property with that name exists, if so, assign it.
			// PWK: the eName should be used, not the name! The eName matching
			// the key,
			// indicates it already exists..

			boolean found = false;
			for (MProperty property : classifier.allFeatures()) {

				if ((property.getName() != null)
						&& XoclHelper.camelCaseLower(property.getName())
								.equalsIgnoreCase(eNameFromKey)) {
					pi.setLocalKey(null);
					pi.setProperty(property);
					found = true;
					break;
				}
			}
			// TODO: even for found properties, if they are newly created
			// update their arity and value based on the common supertype of
			// referenced objects,
			// and the found arity.
			if (found) {
				continue;
			}

			// If we found new property instances in objects that were already
			// typed, we create a sub type.
			if (!newClassifiers.contains(classifier)) {
				classifier = createNewClassifier(obj,
						classifier.getContainingPackage(), classifier,
						createdNatures, newClassifiers);
				obj.setType(classifier);
			}

			// Now we add a new property to the type, if required
			// PWK: now the BusinessCase version of the name has to be used.
			MProperty property = McoreFactory.eINSTANCE.createMProperty();

			// PWK: what about not setting the property name, as it is the same
			// as the type name?
			property.setName(XoclHelper.camelCaseToBusiness(eNameFromKey));

			// arity
			// use new derived arity info.
			int valuesArity = pi.getInternalContainedObject().size()
					+ pi.getInternalDataValue().size()
					+ pi.getInternalLiteralValue().size()
					+ pi.getInternalReferencedObject().size();
			property.setMandatory(valuesArity > 0);
			property.setSingular(valuesArity <= 1);

			if (!setTypeForProperty(property, pi)) {
				// remember the type was guessed b/c the propety instance was
				// empty.
				untypedProperties.add(property);
			}

			// not set by default
			property.setPersisted(true);
			property.setChangeable(true);
			property.setHasStorage(true);

			classifier.getProperty().add(property);
			if ((classifier.getIdProperty().size() == 0)
					&& (property.getSimpleType() == SimpleType.STRING)) {
				classifier.getIdProperty().add(property);
			}
			pi.setLocalKey(null);
			pi.setProperty(property);
		} // for each property
	}

	/**
	 * Set type for MProperty, based on the data in corresponding property
	 * instance.
	 * 
	 * @param property
	 * @param p
	 * @return true if a type could be defined, false otherwise.
	 */
	private static boolean setTypeForProperty(MProperty property,
			MPropertyInstance p) {

		// Note: for references we will always have a common super type,
		// because of what we did while assigning types to objects
		if (p.getInternalReferencedObject().size() > 0) {
			// type is reference to type
			property.setType(getSuperType(p));
			property.setSimpleType(SimpleType.NONE);
			property.setContainment(false);
			return true;
		}
		if (p.getInternalContainedObject().size() > 0) {
			// type is reference to type
			property.setType(getSuperType(p));
			property.setSimpleType(SimpleType.NONE);
			property.setContainment(true);
			return true;
		}
		if (p.getInternalLiteralValue().size() > 0) {
			// enumeration
			property.setType(p.getInternalLiteralValue().get(0)
					.getLiteralValue().getContainingEnumeration());
			property.setSimpleType(SimpleType.NONE);
			return true;
		}
		if (p.getInternalDataValue().size() > 0) {
			// basic type
			property.setType(null);
			property.setSimpleType(SimpleType.STRING);
			return true;
		}

		// gotta do this, b/c otherwise property will not appear in
		// getAllFeatures() this will cause duplicated properties.
		property.setType(null);
		property.setSimpleType(SimpleType.STRING);
		return false;
	}

	/**
	 * For newly created classifiers, properties that are in the type which do
	 * not have corresponding property instances in the object, will be made
	 * optional. For already existing types, nothing is changed.
	 * 
	 * @param obj
	 *            The MObject to be aligned with the classifier.
	 * @param createdNatures
	 * @param untypedProperties
	 * 
	 * @added
	 */
	private static void addPropertiesFromClassifier(MObject obj,
			HashMap<String, MClassifier> createdNatures,
			List<MClassifier> newClassifiers,
			List<MProperty> untypedProperties) {

		MClassifier classifier = obj.getType();
		if (!newClassifiers.contains(classifier))
			return;

		for (MProperty property : classifier.allFeatures()) {

			// Check if the object contains the property
			MPropertyInstance found = null;
			for (MPropertyInstance p : obj.getPropertyInstance()) {
				if (p.getProperty() == property) {
					found = p;
					break;
				}
			}

			if (found == null) {
				// the object has no corresponding property instance, make
				// property optional.
				property.setMandatory(false);
			} else {
				// if property is untyped (maybe because the first object that
				// had it, had no data), try to add type.
				if (untypedProperties.contains(property)) {
					if (setTypeForProperty(property, found))
						untypedProperties.remove(property);
				}

				// else set it to multiple if there is more than one value
				// provided
				int valuesArity = found.getInternalContainedObject().size()
						+ found.getInternalDataValue().size()
						+ found.getInternalLiteralValue().size()
						+ found.getInternalReferencedObject().size();
				property.setSingular(
						property.isSetSingular() && (valuesArity <= 1));
			}
		} // for each property
	}

	/**
	 * Returns all objects directly contained or referenced in the given object.
	 * 
	 * @added
	 */
	private static List<MObject> collectObjects(MObject obj) {
		List<MObject> contained = new ArrayList<MObject>();
		for (MPropertyInstance p : obj.getPropertyInstance()) {
			contained.addAll(collectObjects(p));
		}
		return contained;
	}

	/**
	 * Returns one common supertype for all objects in given reference. It
	 * returns null if no supertype is defined, or return the frist type which
	 * is super type for all referenced objects..
	 * 
	 * @added
	 */
	private static MClassifier getSuperType(MPropertyInstance p) {
		MClassifier superType = null;
		for (MObject o : collectObjects(p)) {
			MClassifier t = o.getType();
			if (t != null) {
				if (superType == null)
					superType = t;
				else if (superType.getSuperType().contains(t))
					superType = t;
				else if (t.getSuperType().contains(superType))
					continue;
				else {
					for (MClassifier c : t.getSuperType()) {
						if (superType.getSuperType().contains(c)) {
							// TODO: Cover more sophisticated inheritance.
							// we assume here there is only one common super
							// type
							superType = c;
							break;
						}
					}
				}
			}
		}
		return superType;
	}

	/**
	 * Returns common supertype for all objects contained or referenced by given
	 * property instance.
	 * 
	 * @added
	 */
	private static List<MObject> collectObjects(MPropertyInstance p) {
		List<MObject> contained = new ArrayList<MObject>(
				p.getInternalContainedObject());
		for (MObjectReference ref : p.getInternalReferencedObject()) {
			if (ref.getReferencedObject() != null) {
				contained.add(ref.getReferencedObject());
			}
		}
		return contained;
	}

}
