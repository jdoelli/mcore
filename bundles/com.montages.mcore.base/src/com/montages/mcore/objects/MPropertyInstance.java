/**
 */
package com.montages.mcore.objects;

import com.montages.mcore.MClassifier;
import org.eclipse.emf.common.util.EList;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProperty Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getInternalDataValue <em>Internal Data Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getInternalLiteralValue <em>Internal Literal Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getInternalReferencedObject <em>Internal Referenced Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getInternalContainedObject <em>Internal Contained Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getDuplicateInstance <em>Duplicate Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getAllInstancesOfThisProperty <em>All Instances Of This Property</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalContainedObjectDueToKind <em>Wrong Internal Contained Object Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalObjectReferenceDueToKind <em>Wrong Internal Object Reference Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalLiteralValueDueToKind <em>Wrong Internal Literal Value Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getWrongInternalDataValueDueToKind <em>Wrong Internal Data Value Due To Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getNumericIdOfPropertyInstance <em>Numeric Id Of Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getDerivedContainment <em>Derived Containment</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getLocalKey <em>Local Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getKey <em>Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getPropertyNameOrLocalKeyDefinition <em>Property Name Or Local Key Definition</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getWrongLocalKey <em>Wrong Local Key</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getDerivedKind <em>Derived Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getPropertyKindAsString <em>Property Kind As String</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getCorrectPropertyKind <em>Correct Property Kind</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getDoAddValue <em>Do Add Value</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getSimpleTypeFromValues <em>Simple Type From Values</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MPropertyInstance#getEnumerationFromValues <em>Enumeration From Values</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance()
 * @model annotation="http://www.xocl.org/OCL label='if self.derivedContainment\r\n    /*then  \'<...\'.concat(\'\').concat(\'(\'.concat(self.internalContainedObject->size().toString())).concat(\' \').concat(self.key).concat(\'(s))...>\') \052/\r\n    then  \'<\'.concat(self.key).concat(\'>\') \r\n    else self.key.concat(\'=\"\').concat(\r\nif self.derivedKind=mcore::PropertyKind::Attribute \r\n   then\r\n     if self.property.oclIsUndefined() \r\n       then  self.internalDataValuesAsString()\r\n     else if self.property.type.oclIsUndefined() \r\n       then  self.internalDataValuesAsString()  \r\n       else  if self.property.type.kind=mcore::ClassifierKind::DataType \r\n           then  self.internalDataValuesAsString()    \r\n           else self.internalLiteralValuesAsString() endif endif endif\r\n  else self.internalReferencedObjectsAsString() endif \r\n .concat(\'\"\')\r\n )\r\n endif'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='propertyKindAsString\n'"
 * @generated
 */

public interface MPropertyInstance extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #isSetProperty()
	 * @see #unsetProperty()
	 * @see #setProperty(MProperty)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_Property()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='if not self.property.oclIsUndefined() then self.property else\r\nlet ps:OrderedSet(MProperty)=self.choosableProperties() in\r\nif ps->size() > 0 then ps->first() else null endif endif' choiceConstruction='self.choosableProperties()'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	MProperty getProperty();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #isSetProperty()
	 * @see #unsetProperty()
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProperty()
	 * @see #getProperty()
	 * @see #setProperty(MProperty)
	 * @generated
	 */
	void unsetProperty();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getProperty <em>Property</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property</em>' reference is set.
	 * @see #unsetProperty()
	 * @see #getProperty()
	 * @see #setProperty(MProperty)
	 * @generated
	 */
	boolean isSetProperty();

	/**
	 * Returns the value of the '<em><b>Property Name Or Local Key Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Name Or Local Key Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Name Or Local Key Definition</em>' attribute.
	 * @see #setPropertyNameOrLocalKeyDefinition(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_PropertyNameOrLocalKeyDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if ( property.oclIsUndefined()) \n  =true \nthen localKey\n  else if property.oclIsUndefined()\n  then null\n  else property.calculatedShortName\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Key and Property'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getPropertyNameOrLocalKeyDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getPropertyNameOrLocalKeyDefinition <em>Property Name Or Local Key Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Name Or Local Key Definition</em>' attribute.
	 * @see #getPropertyNameOrLocalKeyDefinition()
	 * @generated
	 */
	void setPropertyNameOrLocalKeyDefinition(String value);

	/**
	 * Returns the value of the '<em><b>Internal Data Value</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MDataValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Data Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Data Value</em>' containment reference list.
	 * @see #isSetInternalDataValue()
	 * @see #unsetInternalDataValue()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_InternalDataValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='if not self.internalDataValue->isEmpty()\r\nthen self.internalDataValue->collect(dv:MDataValue|Tuple{dataValue=dv.dataValue})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then \r\n    if self.property.simpleType=SimpleType::None then OrderedSet{} else\r\n      if self.property.hasSimpleModelingType \r\n        then OrderedSet{} else\r\n         OrderedSet{Tuple{dataValue=\'\'}}\r\n    endif endif\r\n  else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then \r\n               OrderedSet{Tuple{dataValue=\'\'}}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     OrderedSet{}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif'"
	 * @generated
	 */
	EList<MDataValue> getInternalDataValue();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalDataValue <em>Internal Data Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalDataValue()
	 * @see #getInternalDataValue()
	 * @generated
	 */
	void unsetInternalDataValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalDataValue <em>Internal Data Value</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal Data Value</em>' containment reference list is set.
	 * @see #unsetInternalDataValue()
	 * @see #getInternalDataValue()
	 * @generated
	 */
	boolean isSetInternalDataValue();

	/**
	 * Returns the value of the '<em><b>Internal Literal Value</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MLiteralValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Literal Value</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Literal Value</em>' containment reference list.
	 * @see #isSetInternalLiteralValue()
	 * @see #unsetInternalLiteralValue()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_InternalLiteralValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='if not self.internalLiteralValue->isEmpty()\r\n  then self.internalLiteralValue->collect(lv:MLiteralValue|Tuple{literalValue=lv.literalValue})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then OrderedSet{} else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then OrderedSet{}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     let nullLit:mcore::MLiteral=null in\r\n     OrderedSet{Tuple{literalValue=nullLit}}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif'"
	 * @generated
	 */
	EList<MLiteralValue> getInternalLiteralValue();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalLiteralValue <em>Internal Literal Value</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalLiteralValue()
	 * @see #getInternalLiteralValue()
	 * @generated
	 */
	void unsetInternalLiteralValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalLiteralValue <em>Internal Literal Value</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal Literal Value</em>' containment reference list is set.
	 * @see #unsetInternalLiteralValue()
	 * @see #getInternalLiteralValue()
	 * @generated
	 */
	boolean isSetInternalLiteralValue();

	/**
	 * Returns the value of the '<em><b>Internal Referenced Object</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObjectReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Referenced Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Referenced Object</em>' containment reference list.
	 * @see #isSetInternalReferencedObject()
	 * @see #unsetInternalReferencedObject()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_InternalReferencedObject()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='if not self.internalReferencedObject->isEmpty()\r\nthen\r\n\tself.internalReferencedObject->collect(oRef:MObjectReference|Tuple{referencedObject=oRef.referencedObject})\r\nelse\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif self.property.containment then OrderedSet{} else\r\nlet nullObj:objects::MObject=null in\r\nif self.property.kind = mcore::PropertyKind::Reference then OrderedSet{Tuple{referencedObject=nullObj}} else \r\nif self.property.kind = mcore::PropertyKind::Attribute then\r\n  if self.property.type.oclIsUndefined() then OrderedSet{} else\r\n     if self.property.type.kind = mcore::ClassifierKind::DataType then OrderedSet{}\r\n     else if self.property.type.kind = mcore::ClassifierKind::Enumeration then\r\n     OrderedSet{}\r\n     else OrderedSet{}\r\n     endif endif endif\r\n else OrderedSet{} \r\n endif endif endif endif endif'"
	 * @generated
	 */
	EList<MObjectReference> getInternalReferencedObject();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalReferencedObject <em>Internal Referenced Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalReferencedObject()
	 * @see #getInternalReferencedObject()
	 * @generated
	 */
	void unsetInternalReferencedObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalReferencedObject <em>Internal Referenced Object</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal Referenced Object</em>' containment reference list is set.
	 * @see #unsetInternalReferencedObject()
	 * @see #getInternalReferencedObject()
	 * @generated
	 */
	boolean isSetInternalReferencedObject();

	/**
	 * Returns the value of the '<em><b>Internal Contained Object</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal Contained Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal Contained Object</em>' containment reference list.
	 * @see #isSetInternalContainedObject()
	 * @see #unsetInternalContainedObject()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_InternalContainedObject()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='if true then OrderedSet{} else\r\nif self.property.oclIsUndefined() then OrderedSet{} else\r\nif  not self.property.containment then OrderedSet{} else\r\nif not (self.property.kind = mcore::PropertyKind::Reference) then  OrderedSet{} else\r\nif self.property.type.oclIsUndefined() then OrderedSet{} else\r\nif self.property.type.hasSimpleModelingType then OrderedSet{} else\r\nif \r\nfalse\r\n then OrderedSet{} else\r\nlet types:OrderedSet(MClassifier) = \r\nself.property.type.allSubTypes()->prepend(self.property.type)\r\n->reject(abstractClass)->excluding(null)->asOrderedSet()\r\nin\r\nif types->size()=0 then OrderedSet{} else\r\nif self.property.singular then\r\n  OrderedSet{Tuple{type=types->first()}}\r\n  else types->collect(c:MClassifier|Tuple{type=c})\r\nendif endif endif endif endif endif endif endif\r\nendif\r\n'"
	 * @generated
	 */
	EList<MObject> getInternalContainedObject();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalContainedObject <em>Internal Contained Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalContainedObject()
	 * @see #getInternalContainedObject()
	 * @generated
	 */
	void unsetInternalContainedObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getInternalContainedObject <em>Internal Contained Object</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal Contained Object</em>' containment reference list is set.
	 * @see #unsetInternalContainedObject()
	 * @see #getInternalContainedObject()
	 * @generated
	 */
	boolean isSetInternalContainedObject();

	/**
	 * Returns the value of the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Object</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_ContainingObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MObject)'"
	 * @generated
	 */
	MObject getContainingObject();

	/**
	 * Returns the value of the '<em><b>Duplicate Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Duplicate Instance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Duplicate Instance</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_DuplicateInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.allInstancesOfThisProperty->size()>1'"
	 * @generated
	 */
	Boolean getDuplicateInstance();

	/**
	 * Returns the value of the '<em><b>All Instances Of This Property</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MPropertyInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Instances Of This Property</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Instances Of This Property</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_AllInstancesOfThisProperty()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if property.oclIsUndefined() then OrderedSet{}\r\nelse self.containingObject.propertyInstance->select(p:MPropertyInstance|p.property=property) endif'"
	 * @generated
	 */
	EList<MPropertyInstance> getAllInstancesOfThisProperty();

	/**
	 * Returns the value of the '<em><b>Wrong Internal Contained Object Due To Kind</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Internal Contained Object Due To Kind</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Internal Contained Object Due To Kind</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_WrongInternalContainedObjectDueToKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if property.oclIsUndefined() then OrderedSet{}\r\nelse if property.containment then OrderedSet{}\r\nelse self.internalContainedObject endif endif'"
	 * @generated
	 */
	EList<MObject> getWrongInternalContainedObjectDueToKind();

	/**
	 * Returns the value of the '<em><b>Wrong Internal Object Reference Due To Kind</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObjectReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Internal Object Reference Due To Kind</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Internal Object Reference Due To Kind</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_WrongInternalObjectReferenceDueToKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.containment \r\n              or (self.property.kind <> PropertyKind::Reference) \r\n      then self.internalReferencedObject\r\n                 ->reject(r:MObjectReference|r.oclIsUndefined()) \r\n      else OrderedSet{} endif endif\r\n'"
	 * @generated
	 */
	EList<MObjectReference> getWrongInternalObjectReferenceDueToKind();

	/**
	 * Returns the value of the '<em><b>Wrong Internal Literal Value Due To Kind</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MLiteralValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Internal Literal Value Due To Kind</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Internal Literal Value Due To Kind</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_WrongInternalLiteralValueDueToKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if self.property.type.kind <> ClassifierKind::Enumeration\r\n      then self.internalLiteralValue->reject(l:MLiteralValue| l.literalValue.oclIsUndefined()) \r\n      else OrderedSet{} endif endif endif\r\n'"
	 * @generated
	 */
	EList<MLiteralValue> getWrongInternalLiteralValueDueToKind();

	/**
	 * Returns the value of the '<em><b>Wrong Internal Data Value Due To Kind</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MDataValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Internal Data Value Due To Kind</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Internal Data Value Due To Kind</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_WrongInternalDataValueDueToKind()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if property.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.property.type.oclIsUndefined()\r\n    then OrderedSet{}\r\n    else if self.property.type.kind <> ClassifierKind::DataType\r\n      then self.internalDataValue->reject(d:MDataValue| d.dataValue.oclIsUndefined() or ( d.dataValue=\'\')) \r\n      else OrderedSet{} endif endif endif\r\n'"
	 * @generated
	 */
	EList<MDataValue> getWrongInternalDataValueDueToKind();

	/**
	 * Returns the value of the '<em><b>Local Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Key</em>' attribute.
	 * @see #isSetLocalKey()
	 * @see #unsetLocalKey()
	 * @see #setLocalKey(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_LocalKey()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	String getLocalKey();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getLocalKey <em>Local Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Local Key</em>' attribute.
	 * @see #isSetLocalKey()
	 * @see #unsetLocalKey()
	 * @see #getLocalKey()
	 * @generated
	 */
	void setLocalKey(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getLocalKey <em>Local Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLocalKey()
	 * @see #getLocalKey()
	 * @see #setLocalKey(String)
	 * @generated
	 */
	void unsetLocalKey();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getLocalKey <em>Local Key</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Local Key</em>' attribute is set.
	 * @see #unsetLocalKey()
	 * @see #getLocalKey()
	 * @see #setLocalKey(String)
	 * @generated
	 */
	boolean isSetLocalKey();

	/**
	 * Returns the value of the '<em><b>Wrong Local Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Local Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Local Key</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_WrongLocalKey()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let localKeyNotSet:Boolean=self.localKey.oclIsUndefined() or (self.localKey=\'\') in\r\nif self.property.oclIsUndefined()\r\n  then if internalReferencedObject->notEmpty() \r\n             or internalContainedObject->notEmpty()\r\n             then false\r\n             else  localKeyNotSet endif\r\nelse if localKeyNotSet\r\n  then false\r\n  else self.property.eName <> self.localKey.camelCaseLower() \r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	Boolean getWrongLocalKey();

	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_Key()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*todo: check for trimmed versions of empty strings...SYSTEM\052/\r\nif  not property.oclIsUndefined()\r\n   then self.property.eName \r\n   else if not ((self.localKey=\'\') or self.localKey.oclIsUndefined())\r\n                then self.localKey.camelCaseLower()\r\n                else \tif self.derivedKind<>PropertyKind::Reference\r\n                \t             then \'KEY MISSING\'\r\n                \t         else if self.internalReferencedObject->notEmpty()\r\n                \t              then let o:MObject = \r\n                \t                            self.internalReferencedObject->first().referencedObject in\r\n                \t                       if o.oclIsUndefined()\r\n                \t                               then \'KEY MISSING\'\r\n                \t                       else if o.nature.oclIsUndefined() or o.nature=\'\'\r\n                \t                                then if not o.type.oclIsUndefined()\r\n                \t                                             then o.type.eName\r\n                \t                                             else\' KEY MISSING\' endif\r\n                \t                       else o.nature.camelCaseLower() endif endif\r\n                \t         else if self.internalContainedObject->notEmpty()  \r\n                \t               then let o:MObject = \r\n                \t                            self.internalContainedObject->first() in\r\n               \t                       if o.nature.oclIsUndefined() or o.nature=\'\'\r\n                \t                                then if not o.type.oclIsUndefined()\r\n                \t                                             then o.type.eName\r\n                \t                                             else\' KEY MISSING\' endif\r\n                \t                       else o.nature.camelCaseLower() endif\r\n                \t         else \'KEY MISSING\' endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	String getKey();

	/**
	 * Returns the value of the '<em><b>Numeric Id Of Property Instance</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Numeric Id Of Property Instance</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numeric Id Of Property Instance</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_NumericIdOfPropertyInstance()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsKindOf(MObject) \r\n  then let o:MObject=self.eContainer().oclAsType(MObject) in\r\n     let pos:Integer= o.propertyInstance->indexOf(self) in\r\n       if pos.oclIsUndefined() \r\n           then \'POS NOT CALCULATABLE\'  \r\n       else if pos<10 \r\n          then \'0\'.concat(pos.toString()) \r\n          else pos.toString() endif  endif\r\n   else \'PROPERTY INSTANCE NOT CONTAINED IN OBJECT\' endif '"
	 * @generated
	 */
	String getNumericIdOfPropertyInstance();

	/**
	 * Returns the value of the '<em><b>Derived Kind</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.PropertyKind}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Kind</em>' attribute.
	 * @see com.montages.mcore.PropertyKind
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_DerivedKind()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.property.oclIsUndefined() \r\n  then self.property.kind\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Undefined\r\nelse if self.internalContainedObject->notEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Reference\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->notEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Attribute\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->notEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then PropertyKind::Attribute\r\nelse if self.internalContainedObject->isEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->notEmpty()\r\n  then PropertyKind::Reference\r\n  else PropertyKind::Ambiguous endif endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	PropertyKind getDerivedKind();

	/**
	 * Returns the value of the '<em><b>Derived Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Containment</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_DerivedContainment()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not self.property.oclIsUndefined() \r\n  then self.property.containment\r\nelse if self.internalContainedObject->notEmpty()\r\n             and self.internalDataValue->isEmpty()\r\n             and self.internalLiteralValue->isEmpty()\r\n             and self.internalReferencedObject->isEmpty()\r\n  then true\r\n  else false endif endif'"
	 * @generated
	 */
	Boolean getDerivedContainment();

	/**
	 * Returns the value of the '<em><b>Property Kind As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Kind As String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Kind As String</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_PropertyKindAsString()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let notStorable:Boolean =\r\nif self.property.oclIsUndefined()=false\r\n  then not (self.property.hasStorage=true) else false endif in\r\nif notStorable\r\n  then  \'ERROR: PROPERTY NOT STORABLE\'\r\nelse  if self.derivedKind=PropertyKind::Attribute \r\n  then \'Attribute\'\r\nelse if self.derivedKind=PropertyKind::Reference\r\n  then if self.derivedContainment \r\n    then \'Containment\'\r\n    else  \'Reference\' endif\r\nelse if self.derivedKind=PropertyKind::Operation\r\n   then \'ERROR:PROPERTY IS OPERATION\' else if self.derivedKind=PropertyKind::Ambiguous\r\n   then \'ERROR:VALUES AMBIGUOUS\' \r\n else if self.derivedKind = PropertyKind::TypeMissing\r\n   then \'Untyped Property\'\r\nelse \'ERROR: SET VALUE OR PROPERTY\' \r\nendif endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	String getPropertyKindAsString();

	/**
	 * Returns the value of the '<em><b>Correct Property Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correct Property Kind</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correct Property Kind</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_CorrectPropertyKind()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.derivedKind=PropertyKind::Attribute \r\n    or self.derivedKind=PropertyKind::Reference'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='false'"
	 * @generated
	 */
	Boolean getCorrectPropertyKind();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.objects.MPropertyInstanceAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MPropertyInstanceAction
	 * @see #setDoAction(MPropertyInstanceAction)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let do: mcore::objects::MPropertyInstanceAction = mcore::objects::MPropertyInstanceAction::Do in\ndo' choiceConstruction='let onlyDataValues:Boolean = internalLiteralValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in\nlet onlyLiteralValues:Boolean = internalDataValue->isEmpty() and internalReferencedObject->isEmpty() and internalContainedObject->isEmpty() in\nlet onlyReferencedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalContainedObject->isEmpty()  in\nlet onlyContainedObjects:Boolean = internalLiteralValue->isEmpty() and internalDataValue->isEmpty() and internalReferencedObject->isEmpty()   in\n\nlet start:OrderedSet(MPropertyInstanceAction) = OrderedSet{MPropertyInstanceAction::Do} in start->append(\nif containingObject.type.oclIsUndefined() then null else\n   if property.oclIsUndefined() then  \n        MPropertyInstanceAction::AddPropertyToType else\n   if containingObject.type.allFeaturesWithStorage()->excludes(property) then\n        MPropertyInstanceAction::AddPropertyToType else null endif endif endif)->append(\nif not property.oclIsUndefined() then \n   if property.calculatedSingular then\n        MPropertyInstanceAction::IntoNary else MPropertyInstanceAction::IntoUnary endif else null endif)->append(\nif property.oclIsUndefined() then \n     if self.derivedKind = PropertyKind::Reference and self.derivedContainment = false then     \n           MPropertyInstanceAction::IntoContainment else null endif\nelse if property.kind = PropertyKind::Reference and property.containment = false then \n           MPropertyInstanceAction::IntoContainment else null endif endif)->append(\nif ((not property.oclIsUndefined()) and property.calculatedSimpleType = SimpleType::String) or self.simpleTypeFromValues = SimpleType::String\n  \tthen MPropertyInstanceAction::ClassWithStringAttribute\n \telse null endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::StringValue else null endif else\n   if property.calculatedSimpleType = SimpleType::String then\n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::StringValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::BooleanValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Boolean then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::BooleanValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::IntegerValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Integer then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::IntegerValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::RealValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Double then \n        if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::RealValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyDataValues then\n        MPropertyInstanceAction::DateValue else null endif else\n   if property.calculatedSimpleType = SimpleType::Date then \n         if (not property.singular) or internalDataValue->isEmpty() then \n        MPropertyInstanceAction::DateValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyReferencedObjects then\n        MPropertyInstanceAction::ReferencedObjectValue else null endif else\n   if property.kind = PropertyKind::Reference and property.containment = false then \n         if (not property.singular) or internalReferencedObject->isEmpty() then \n        MPropertyInstanceAction::ReferencedObjectValue else null endif else null endif endif)->append(\nif property.oclIsUndefined() then \n   if onlyContainedObjects then\n        MPropertyInstanceAction::ContainedObject else null endif else\n   if property.kind = PropertyKind::Reference and property.containment = true then \n        MPropertyInstanceAction::ContainedObject else null endif endif)->append(\n        if internalDataValue->notEmpty() or internalLiteralValue->notEmpty() or internalReferencedObject->notEmpty() or internalContainedObject->notEmpty() then \n   MPropertyInstanceAction::ClearValues else null endif)->excluding(null)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MPropertyInstanceAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MPropertyInstanceAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MPropertyInstanceAction value);

	/**
	 * Returns the value of the '<em><b>Do Add Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Add Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Add Value</em>' attribute.
	 * @see #setDoAddValue(Boolean)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_DoAddValue()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getDoAddValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MPropertyInstance#getDoAddValue <em>Do Add Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Add Value</em>' attribute.
	 * @see #getDoAddValue()
	 * @generated
	 */
	void setDoAddValue(Boolean value);

	/**
	 * Returns the value of the '<em><b>Simple Type From Values</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type From Values</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type From Values</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_SimpleTypeFromValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*overwritten in Java\052/ null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	SimpleType getSimpleTypeFromValues();

	/**
	 * Returns the value of the '<em><b>Enumeration From Values</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Enumeration From Values</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Enumeration From Values</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstance_EnumerationFromValues()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*Overwritten in Java \052/ null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Typing' createColumn='false'"
	 * @generated
	 */
	MClassifier getEnumerationFromValues();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate propertyNameOrLocalKeyDefinition$Update(String trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MPropertyInstanceAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAddValue$Update(Boolean trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let t:MClassifier=self.containingObject.type in\r\nlet p:MProperty=self.property in\r\nif t.oclIsUndefined() or p.oclIsUndefined() then false\r\nelse t.kind=ClassifierKind::ClassType and t.allProperties()->includes(p) \r\nand not(p.kind = PropertyKind::Operation) endif'"
	 * @generated
	 */
	Boolean correctProperty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let p:MProperty=self.property in\r\nif p.oclIsUndefined() then false\r\nelse if p.kind = PropertyKind::Attribute then\r\n  self.internalContainedObject->size()=0 \r\n  and\r\n  (if p.singular then self.internalDataValue->size() <2 else\r\n  if p.mandatory then self.internalDataValue->size() > 0 else true endif endif)\r\n  and \r\n  self.internalReferencedObject->size()=0\r\nelse if p.kind = PropertyKind::Reference then \r\n  if p.containment = false then\r\n    self.internalContainedObject->size()=0 \r\n    and\r\n   self.internalDataValue->size() = 0 \r\n    and \r\n    (if p.singular then self.internalReferencedObject->size() <2 else\r\n    if p.mandatory then self.internalReferencedObject->size() > 0 else true \r\n    endif   endif)\r\n else\r\n    (if p.singular then self.internalContainedObject->size() <2 else\r\n    if p.mandatory then self.internalContainedObject->size() > 0 else true \r\n    endif   endif)\r\n    and\r\n   self.internalDataValue->size() = 0 \r\n    and \r\n    self.internalReferencedObject->size() = 0\r\n endif\r\n else false\r\n endif endif endif'"
	 * @generated
	 */
	Boolean correctMultiplicity();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.property.oclIsUndefined() then \'\' else\r\n  if self.property.kind=mcore::PropertyKind::Reference then\r\n    self.internalReferencedObjectsAsString()\r\nelse\r\n    if self.property.kind=mcore::PropertyKind::Attribute then\r\n      if self.property.type.oclIsUndefined() then\r\n         if not (self.property.simpleType =SimpleType::None) then \r\n            self.internalDataValuesAsString() else \'ERROR3\' endif\r\n      else      \r\n        if self.property.type.kind=mcore::ClassifierKind::DataType then\r\n          self.internalDataValuesAsString() else\r\n          if self.property.type.kind=mcore::ClassifierKind::Enumeration then\r\n             self.internalLiteralValuesAsString() else\r\n               \'ERROR1\' endif endif endif\r\n    else \'ERROR2\' \r\n  endif \r\nendif endif\r\n/*\r\nif self.type.oclIsUndefined() then \'\' else\r\nif self.type.idProperty->isEmpty() then \'\' else\r\nself.type.idProperty->iterate(p:mcore::MProperty;res:String=\'\'|\r\n(if res=\'\' then \'\' else res.concat(\'|\') endif).concat(self.propertyValueAsString(p)))\r\nendif endif\052/'"
	 * @generated
	 */
	String propertyValueAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let vs:OrderedSet(objects::MObjectReference) = self.internalReferencedObject in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MObjectReference;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.referencedObject.oclIsUndefined() then \r\n                \'OBJECT MISSING\' else \r\n                if r.referencedObject=self.containingObject then \'self\' else\r\n                let c:mcore::MClassifier=r.typeOfReferencedObject in \r\n                if c.oclIsUndefined() then r.referencedObject.id else\r\n                  if c.allIdProperties()->isEmpty() then\r\n                    r.referencedObject.id \r\n                  else if c.allIdProperties()->size()>1 then\r\n                   \'(\'.concat(r.referencedObject.id ).concat(\')\')\r\n                   else  r.referencedObject.id\r\n                  endif  endif endif endif endif))\r\nendif '"
	 * @generated
	 */
	String internalReferencedObjectsAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let vs:OrderedSet(objects::MDataValue) = self.internalDataValue in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MDataValue;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.dataValue.oclIsUndefined() then \r\n                               \'VALUE MISSING\' else r.dataValue endif))\r\nendif '"
	 * @generated
	 */
	String internalDataValuesAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let vs:OrderedSet(objects::MLiteralValue) = self.internalLiteralValue in\r\nif vs->isEmpty() then \'\' else\r\n    vs->iterate(r:objects::MLiteralValue;res:String=\'\'|\r\n            (if res=\'\' then \'\' else res.concat(\',\') endif)\r\n              .concat(if r.literalValue.oclIsUndefined() then \r\n                                \'LITERAL MISSING\' else \r\n                                r.literalValue.eName endif))\r\nendif '"
	 * @generated
	 */
	String internalLiteralValuesAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='/*see as well allFeaturesWithStorage of type.\052/\r\nlet t:MClassifier =containingObject.type in\r\nif t.oclIsUndefined() \r\n then if self.property.oclIsUndefined() then OrderedSet{} else self.property->asOrderedSet() endif\r\n else\r\ncontainingObject.type.allProperties()->\r\nreject(p:MProperty| \r\n  p.kind=PropertyKind::Operation or \r\n not( p.hasStorage=true) or\r\n  (not (p = self.property) and self.containingObject.propertyInstance.property->includes(p)))\r\nendif'"
	 * @generated
	 */
	EList<MProperty> choosableProperties();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyNameOrLocalKeyDefinitionAnnotation="http://www.montages.com/mCore/MCore mName='propertyNameOrLocalKeyDefinition'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='propertyNameOrLocalKeyDefinitionUpdate'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Key and Property' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate propertyNameOrLocalKeyDefinitionUpdate(
			String propertyNameOrLocalKeyDefinition);

} // MPropertyInstance
