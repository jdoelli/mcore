/**
 */
package com.montages.mcore.objects.util;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

import com.montages.mcore.objects.ObjectsPackage;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ObjectsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		ObjectsPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the ObjectsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new ObjectsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new ObjectsResourceFactoryImpl());
		}
		return registrations;
	}

} //ObjectsXMLProcessor
