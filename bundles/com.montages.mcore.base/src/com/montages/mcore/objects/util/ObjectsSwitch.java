/**
 */
package com.montages.mcore.objects.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import com.montages.mcore.MNamed;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.MValue;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsPackage
 * @generated
 */
public class ObjectsSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ObjectsPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectsSwitch() {
		if (modelPackage == null) {
			modelPackage = ObjectsPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case ObjectsPackage.MRESOURCE_FOLDER: {
			MResourceFolder mResourceFolder = (MResourceFolder) theEObject;
			T result = caseMResourceFolder(mResourceFolder);
			if (result == null)
				result = caseMNamed(mResourceFolder);
			if (result == null)
				result = caseMRepositoryElement(mResourceFolder);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MRESOURCE: {
			MResource mResource = (MResource) theEObject;
			T result = caseMResource(mResource);
			if (result == null)
				result = caseMNamed(mResource);
			if (result == null)
				result = caseMRepositoryElement(mResource);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MOBJECT: {
			MObject mObject = (MObject) theEObject;
			T result = caseMObject(mObject);
			if (result == null)
				result = caseMRepositoryElement(mObject);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MPROPERTY_INSTANCE: {
			MPropertyInstance mPropertyInstance = (MPropertyInstance) theEObject;
			T result = caseMPropertyInstance(mPropertyInstance);
			if (result == null)
				result = caseMRepositoryElement(mPropertyInstance);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MVALUE: {
			MValue mValue = (MValue) theEObject;
			T result = caseMValue(mValue);
			if (result == null)
				result = caseMRepositoryElement(mValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MOBJECT_REFERENCE: {
			MObjectReference mObjectReference = (MObjectReference) theEObject;
			T result = caseMObjectReference(mObjectReference);
			if (result == null)
				result = caseMValue(mObjectReference);
			if (result == null)
				result = caseMRepositoryElement(mObjectReference);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MLITERAL_VALUE: {
			MLiteralValue mLiteralValue = (MLiteralValue) theEObject;
			T result = caseMLiteralValue(mLiteralValue);
			if (result == null)
				result = caseMValue(mLiteralValue);
			if (result == null)
				result = caseMRepositoryElement(mLiteralValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case ObjectsPackage.MDATA_VALUE: {
			MDataValue mDataValue = (MDataValue) theEObject;
			T result = caseMDataValue(mDataValue);
			if (result == null)
				result = caseMValue(mDataValue);
			if (result == null)
				result = caseMRepositoryElement(mDataValue);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MResource Folder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MResource Folder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMResourceFolder(MResourceFolder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MResource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MResource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMResource(MResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObject(MObject object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MProperty Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MProperty Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMPropertyInstance(MPropertyInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MValue</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MValue</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMValue(MValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject Reference</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject Reference</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObjectReference(MObjectReference object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MLiteral Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MLiteral Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMLiteralValue(MLiteralValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MData Value</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MData Value</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMDataValue(MDataValue object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MRepository Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMRepositoryElement(MRepositoryElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MNamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMNamed(MNamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ObjectsSwitch
