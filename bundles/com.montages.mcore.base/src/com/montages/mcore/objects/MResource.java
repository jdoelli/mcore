/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.MNamed;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MResource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MResource#getObject <em>Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResource#getRootObjectPackage <em>Root Object Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResource#getContainingFolder <em>Containing Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResource#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResource#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMResource()
 * @model annotation="http://www.xocl.org/OCL label='(if self.name.oclIsUndefined() \r\n  then \'NAME MISSING\' \r\n  else self.calculatedShortName endif)\r\n.concat(\'.\')\r\n.concat(if self.object->isEmpty() then \'OBJECT MISSING\' else\r\nif self.object->first().type.oclIsUndefined() then \'TYPE OF OBJECT MISSING\' else\r\nself.object->first().type.containingPackage.eName endif endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Resource\'\n' eNameDerive='let nSPrefix: String = if (let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif) \n  =true \nthen let subchain1 : mcore::objects::MObject = let chain: OrderedSet(mcore::objects::MObject)  = object->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif in \n if subchain1 = null \n  then null \n else if subchain1.type.containingPackage.oclIsUndefined()\n  then null\n  else subchain1.type.containingPackage.derivedNsPrefix\nendif endif \n else if (let chain: mcore::MPackage = rootObjectPackage in\nif chain <> null then true else false \n  endif)=true then if rootObjectPackage.oclIsUndefined()\n  then null\n  else rootObjectPackage.derivedNsPrefix\nendif\n  else \'ROOT OBJECT PACKAGE MISSING\'\nendif endif in\nlet namePlusNSPrefix: String = let e1: String = calculatedShortName.concat(\'.\').concat(nSPrefix) in \n if e1.oclIsInvalid() then null else e1 endif in\nnamePlusNSPrefix\n'"
 * @generated
 */

public interface MResource extends MNamed {
	/**
	 * Returns the value of the '<em><b>Object</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' containment reference list.
	 * @see #isSetObject()
	 * @see #unsetObject()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResource_Object()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initOrder='2' initValue='let p:MPackage=self.rootObjectPackage in\r\nif p.oclIsUndefined() \r\n  then OrderedSet{Tuple{type=let t:MClassifier=null in t}}\r\n  else OrderedSet{Tuple{type=p.resourceRootClass}} endif'"
	 * @generated
	 */
	EList<MObject> getObject();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MResource#getObject <em>Object</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObject()
	 * @see #getObject()
	 * @generated
	 */
	void unsetObject();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MResource#getObject <em>Object</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object</em>' containment reference list is set.
	 * @see #unsetObject()
	 * @see #getObject()
	 * @generated
	 */
	boolean isSetObject();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #setId(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResource_Id()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let f:MResourceFolder= self.containingFolder in\r\nif f.oclIsUndefined()\r\n  then \'R00\'\r\n  else let i:Integer=f.resource->size() in\r\n                    if i <10 \r\n                     then \'R0\'.concat(i.toString()) \r\n                     else \'R\'.concat(i.toString()) endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Object Id'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getId();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MResource#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #isSetId()
	 * @see #unsetId()
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MResource#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetId()
	 * @see #getId()
	 * @see #setId(String)
	 * @generated
	 */
	void unsetId();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MResource#getId <em>Id</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Id</em>' attribute is set.
	 * @see #unsetId()
	 * @see #getId()
	 * @see #setId(String)
	 * @generated
	 */
	boolean isSetId();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.objects.MResourceAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MResourceAction
	 * @see #setDoAction(MResourceAction)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResource_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let do: mcore::objects::MResourceAction = mcore::objects::MResourceAction::Do in\ndo' choiceConstruction='if self.object->isEmpty() then OrderedSet{\nmcore::objects::MResourceAction::Do, \nmcore::objects::MResourceAction::OpenEditor,\nmcore::objects::MResourceAction::FixAllTypes,\nmcore::objects::MResourceAction::Object\n} else OrderedSet{\nmcore::objects::MResourceAction::Do, \nmcore::objects::MResourceAction::OpenEditor,\nmcore::objects::MResourceAction::FixAllTypes\n}  endif                           '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MResourceAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MResource#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MResourceAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MResourceAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MResourceAction trg);

	/**
	 * Returns the value of the '<em><b>Root Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Object Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Object Package</em>' reference.
	 * @see #isSetRootObjectPackage()
	 * @see #unsetRootObjectPackage()
	 * @see #setRootObjectPackage(MPackage)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResource_RootObjectPackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initOrder='1' initValue='let f:MResourceFolder = self.containingFolder in\r\nif f.oclIsUndefined() then\r\n  null\r\nelse\r\n  let c:MComponent = self.containingFolder.rootFolder.eContainer().oclAsType(MComponent)   \r\n    in   if c.ownedPackage->isEmpty() \r\n        then null \r\n        else c.ownedPackage->first() endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getRootObjectPackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MResource#getRootObjectPackage <em>Root Object Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Root Object Package</em>' reference.
	 * @see #isSetRootObjectPackage()
	 * @see #unsetRootObjectPackage()
	 * @see #getRootObjectPackage()
	 * @generated
	 */
	void setRootObjectPackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MResource#getRootObjectPackage <em>Root Object Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetRootObjectPackage()
	 * @see #getRootObjectPackage()
	 * @see #setRootObjectPackage(MPackage)
	 * @generated
	 */
	void unsetRootObjectPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MResource#getRootObjectPackage <em>Root Object Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Root Object Package</em>' reference is set.
	 * @see #unsetRootObjectPackage()
	 * @see #getRootObjectPackage()
	 * @see #setRootObjectPackage(MPackage)
	 * @generated
	 */
	boolean isSetRootObjectPackage();

	/**
	 * Returns the value of the '<em><b>Containing Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Folder</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Folder</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResource_ContainingFolder()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if   self.eContainer().oclIsKindOf(MResourceFolder) \r\n  then self.eContainer().oclAsType(MResourceFolder)\r\n  else null\r\n  endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	MResourceFolder getContainingFolder();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='update'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Boolean update(MProperty property);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'" correspondingPropertyAnnotation="http://www.montages.com/mCore/MCore mName='corresponding Property'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updateObjects'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Default Containment Operations' createColumn='true'"
	 * @generated
	 */
	Boolean updateObjects(MProperty property, MProperty correspondingProperty);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'" rootObjAnnotation="http://www.montages.com/mCore/MCore mName='rootObj'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updateContainmentToStoredFeature'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Default Containment Operations' createColumn='true'"
	 * @generated
	 */
	Boolean updateContainmentToStoredFeature(MProperty property,
			MObject rootObj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model correspondingPropertyAnnotation="http://www.montages.com/mCore/MCore mName='corresponding Property'" rootObjAnnotation="http://www.montages.com/mCore/MCore mName='rootObj'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updateStoredToContainmentFeature'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Default Containment Operations' createColumn='true'"
	 * @generated
	 */
	Boolean updateStoredToContainmentFeature(MProperty correspondingProperty,
			MObject rootObj);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'" rootObjAnnotation="http://www.montages.com/mCore/MCore mName='rootObj'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='addObjects'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Default Containment Operations' createColumn='true'"
	 * @generated
	 */
	Boolean addObjects(MProperty property, MObject rootObj);

} // MResource
