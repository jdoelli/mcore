/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.tables.MTable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MObject#getPropertyInstance <em>Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getTable <em>Table</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getCreateTypeInPackage <em>Create Type In Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getHasPropertyInstanceNotOfType <em>Has Property Instance Not Of Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getNature <em>Nature</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getClassNameOrNatureDefinition <em>Class Name Or Nature Definition</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getNatureNeeded <em>Nature Needed</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getNatureMissing <em>Nature Missing</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getTypePackage <em>Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getNatureEName <em>Nature EName</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getCalculatedNature <em>Calculated Nature</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getInitializationType <em>Initialization Type</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getNumericIdOfObject <em>Numeric Id Of Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getContainingObject <em>Containing Object</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getContainingPropertyInstance <em>Containing Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getResource <em>Resource</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getReferencedBy <em>Referenced By</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getWrongPropertyInstance <em>Wrong Property Instance</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getStatusAsText <em>Status As Text</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MObject#getDerivationsAsText <em>Derivations As Text</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMObject()
 * @model annotation="http://www.xocl.org/OCL label='let rootString:String= if self.type.oclIsUndefined() then \'root\' else self.type.eName.camelCaseLower() endif in\r\nlet idString:String= if self.type.oclIsUndefined()\r\n      then \' id=\\\"\'.concat(self.id).concat(\'\\\"\') \r\nelse if self.type.idProperty->isEmpty()\r\n      then  \' id=\\\"\'.concat(self.id).concat(\'\\\"\') \r\n      else  self.idPropertiesAsString() endif endif\r\nin\r\n \'<\'\r\n.concat(\r\nif self.isRootObject() then\r\nrootString\r\nelse\r\n(let pi:MPropertyInstance = self.eContainer().oclAsType(MPropertyInstance) in\r\nlet p:MProperty = pi.property in\r\npi.key)\r\nendif)\r\n.concat(idString)\r\n.concat(\'>\')'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='if (isRootObject()) \n  =true \nthen \'Root Object\'\n  else \'Object\'\nendif\n'"
 * @generated
 */

public interface MObject extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This attribute is a name used to classify an object which has not a type yet. Objects with same nature are assumed to be of same type, even when the type is not yet defined. This attribute is used by the tool when creating classifiers from a set of instances.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Nature</em>' attribute.
	 * @see #isSetNature()
	 * @see #unsetNature()
	 * @see #setNature(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_Nature()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='false'"
	 * @generated
	 */
	String getNature();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getNature <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nature</em>' attribute.
	 * @see #isSetNature()
	 * @see #unsetNature()
	 * @see #getNature()
	 * @generated
	 */
	void setNature(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getNature <em>Nature</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNature()
	 * @see #getNature()
	 * @see #setNature(String)
	 * @generated
	 */
	void unsetNature();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getNature <em>Nature</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Nature</em>' attribute is set.
	 * @see #unsetNature()
	 * @see #getNature()
	 * @see #setNature(String)
	 * @generated
	 */
	boolean isSetNature();

	/**
	 * Returns the value of the '<em><b>Class Name Or Nature Definition</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class Name Or Nature Definition</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class Name Or Nature Definition</em>' attribute.
	 * @see #setClassNameOrNatureDefinition(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_ClassNameOrNatureDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (natureNeeded) \n  =true \nthen nature\n  else if type.oclIsUndefined()\n  then null\n  else type.calculatedShortName\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Nature and Type'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getClassNameOrNatureDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getClassNameOrNatureDefinition <em>Class Name Or Nature Definition</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Class Name Or Nature Definition</em>' attribute.
	 * @see #getClassNameOrNatureDefinition()
	 * @generated
	 */
	void setClassNameOrNatureDefinition(String value);

	/**
	 * Returns the value of the '<em><b>Nature Needed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature Needed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature Needed</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_NatureNeeded()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if ( type.oclIsUndefined())= true \n then true \n else if (hasPropertyInstanceNotOfType)= true \n then true \nelse if (( type.oclIsUndefined())= null or (hasPropertyInstanceNotOfType)= null) = true \n then null \n else false endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Nature and Type'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getNatureNeeded();

	/**
	 * Returns the value of the '<em><b>Nature Missing</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nature Missing</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nature Missing</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_NatureMissing()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.natureNeeded and \n(if self.nature.oclIsUndefined() \n    then true\n    else self.nature.trim() = \'\' endif)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='false'"
	 * @generated
	 */
	Boolean getNatureMissing();

	/**
	 * Returns the value of the '<em><b>Nature EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This attribute is a name used to classify an object which has not a type yet. Objects with same nature are assumed to be of same type, even when the type is not yet defined. This attribute is used by the tool when creating classifiers from a set of instances.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Nature EName</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_NatureEName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let n:String = calculatedNature in\r\nif n.oclIsUndefined() \r\n  then \'TYPE AND NATURE MISSING\' \r\n  else n.camelCaseUpper() endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='false'"
	 * @generated
	 */
	String getNatureEName();

	/**
	 * Returns the value of the '<em><b>Calculated Nature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Nature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Nature</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_CalculatedNature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (not type.oclIsUndefined()) and (not self.hasPropertyInstanceNotOfType)\r\n  then type.calculatedShortName\r\nelse if not (nature.oclIsUndefined() or nature=\'\') \r\n  then  nature\r\nelse  if containingPropertyInstance.oclIsUndefined() \r\n  then null\r\nelse if let l:String = containingPropertyInstance.localKey in \r\n                l.oclIsUndefined() or l=\'\' \r\n   then null\r\nelse  self.containingPropertyInstance.localKey.camelCaseToBusiness()\r\n  endif endif endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Nature and Type'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getCalculatedNature();

	/**
	 * Returns the value of the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Package</em>' reference.
	 * @see #isSetTypePackage()
	 * @see #unsetTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_TypePackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='false'"
	 * @generated
	 */
	MPackage getTypePackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getTypePackage <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Package</em>' reference.
	 * @see #isSetTypePackage()
	 * @see #unsetTypePackage()
	 * @see #getTypePackage()
	 * @generated
	 */
	void setTypePackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getTypePackage <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTypePackage()
	 * @see #getTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @generated
	 */
	void unsetTypePackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getTypePackage <em>Type Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type Package</em>' reference is set.
	 * @see #unsetTypePackage()
	 * @see #getTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @generated
	 */
	boolean isSetTypePackage();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(MClassifier)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_Type()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initOrder='1\n' initValue='if self.type=null\r\n then self.initializationType\r\n else self.type endif' choiceConstraint='trg.kind = ClassifierKind::ClassType\r\nand\r\n trg.abstractClass = false\r\nand \r\nif self.isRootObject() \r\n  then let tP:MPackage=\r\n     if typePackage.oclIsUndefined()\r\n      then if resource.oclIsUndefined() \r\n        then null\r\n        else resource.rootObjectPackage endif\r\n      else typePackage endif in\r\n    trg.selectableClassifier(\r\n      if type.oclIsUndefined() \r\n        then OrderedSet{} else OrderedSet{type} endif,\r\n      tP)\r\nelse \r\nlet p:MProperty=self.eContainer().oclAsType(MPropertyInstance).property in\r\n  if p.oclIsUndefined() then true \r\n    else p.type = trg or \r\n            (if p.type.oclIsUndefined() then false \r\n                  else p.type.allSubTypes()->includes(trg) endif) endif \r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='false'"
	 * @generated
	 */
	MClassifier getType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getType <em>Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' reference is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Initialization Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialization Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialization Type</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_InitializationType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootObject() then \r\n if self.resource.rootObjectPackage.oclIsUndefined() then null\r\n else \r\n    let p:mcore::MPackage=self.resource.rootObjectPackage  in\r\n    if p.resourceRootClass.oclIsUndefined()  then \r\n      let cs:OrderedSet(MClassifier) =  p.classifier->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.abstractClass=false) in if cs->isEmpty() then null else cs->first() endif\r\n    else p.resourceRootClass\r\n    endif\r\n  endif\r\nelse\r\nlet p:MProperty= self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then null else \r\nif p.type.oclIsUndefined() then null\r\nelse\r\nif not p.type.abstractClass then p.type\r\n else \r\n let cs:OrderedSet(mcore::MClassifier)=p.type.allSubTypes()->select(c:mcore::MClassifier| not c.abstractClass) in \r\n if cs->isEmpty() then null\r\n else cs->last()\r\nendif endif endif\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Nature and Type'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getInitializationType();

	/**
	 * Returns the value of the '<em><b>Property Instance</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MPropertyInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Instance</em>' containment reference list.
	 * @see #isSetPropertyInstance()
	 * @see #unsetPropertyInstance()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_PropertyInstance()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initOrder='2\n' initValue='/*initializationType.allFeaturesWithStorage()->collect(p|Tuple{property=p})\052/\r\nif self.type.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if self.type.allFeaturesWithStorage()->isEmpty() then OrderedSet{}\r\nelse self.type.allFeaturesWithStorage()->collect(p:mcore::MProperty|Tuple{property=p})->asOrderedSet()\r\nendif endif\r\n'"
	 * @generated
	 */
	EList<MPropertyInstance> getPropertyInstance();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getPropertyInstance <em>Property Instance</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPropertyInstance()
	 * @see #getPropertyInstance()
	 * @generated
	 */
	void unsetPropertyInstance();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getPropertyInstance <em>Property Instance</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property Instance</em>' containment reference list is set.
	 * @see #unsetPropertyInstance()
	 * @see #getPropertyInstance()
	 * @generated
	 */
	boolean isSetPropertyInstance();

	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_Id()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if not type.oclIsUndefined() \r\n  then if  type.allIdProperties()->notEmpty() \r\n    then idPropertyBasedId() \r\n    else numericIdOfObject endif\r\n  else numericIdOfObject endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='false'"
	 * @generated
	 */
	String getId();

	/**
	 * Returns the value of the '<em><b>Containing Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Object</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_ContainingObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootObject() then null\r\nelse self.containingPropertyInstance.containingObject endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Structural'"
	 * @generated
	 */
	MObject getContainingObject();

	/**
	 * Returns the value of the '<em><b>Containing Property Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Property Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Property Instance</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_ContainingPropertyInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootObject() then null\r\nelse self.eContainer().oclAsType(MPropertyInstance) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Structural'"
	 * @generated
	 */
	MPropertyInstance getContainingPropertyInstance();

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_Resource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.isRootObject() then \r\nif self.eContainer().oclIsKindOf(MResource) then\r\nself.eContainer().oclAsType(MResource) else null endif\r\nelse self.containingObject.resource endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Structural'"
	 * @generated
	 */
	MResource getResource();

	/**
	 * Returns the value of the '<em><b>Referenced By</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MObjectReference}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Referenced By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Referenced By</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_ReferencedBy()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='referenced By'"
	 *        annotation="http://www.xocl.org/OCL derive='MObjectReference.allInstances()->select(referencedObject= self)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='false'"
	 * @generated
	 */
	EList<MObjectReference> getReferencedBy();

	/**
	 * Returns the value of the '<em><b>Wrong Property Instance</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MPropertyInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Wrong Property Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Wrong Property Instance</em>' reference list.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_WrongPropertyInstance()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='wrong Property Instance'"
	 *        annotation="http://www.xocl.org/OCL derive='\r\npropertyInstance->reject(\r\ni:MPropertyInstance | (  let correctInternalValue: Boolean = i.wrongInternalContainedObjectDueToKind->isEmpty() and i.wrongInternalDataValueDueToKind->isEmpty() and\r\ni.wrongInternalLiteralValueDueToKind->isEmpty() and\r\ni.wrongInternalObjectReferenceDueToKind->isEmpty() in\r\n\r\n i.correctMultiplicity() and i.correctProperty() and  not(i.wrongLocalKey) and correctInternalValue))'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyInstance> getWrongPropertyInstance();

	/**
	 * Returns the value of the '<em><b>Table</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' containment reference.
	 * @see #isSetTable()
	 * @see #unsetTable()
	 * @see #setTable(MTable)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_Table()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MTable getTable();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getTable <em>Table</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' containment reference.
	 * @see #isSetTable()
	 * @see #unsetTable()
	 * @see #getTable()
	 * @generated
	 */
	void setTable(MTable value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getTable <em>Table</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTable()
	 * @see #getTable()
	 * @see #setTable(MTable)
	 * @generated
	 */
	void unsetTable();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getTable <em>Table</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Table</em>' containment reference is set.
	 * @see #unsetTable()
	 * @see #getTable()
	 * @see #setTable(MTable)
	 * @generated
	 */
	boolean isSetTable();

	/**
	 * Returns the value of the '<em><b>Numeric Id Of Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Numeric Id Of Object</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Numeric Id Of Object</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_NumericIdOfObject()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='containerId().concat(\'_\').concat(\r\n if isRootObject() \r\n   then localId()\r\n  else\r\nlet p:MProperty=self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then \r\nself.containingPropertyInstance.numericIdOfPropertyInstance\r\n.concat(\'.\').concat(self.localId()) else\r\nif p.singular then p.id else\r\np.id.concat(\'.\').concat(self.localId())\r\nendif endif endif)\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='false'"
	 * @generated
	 */
	String getNumericIdOfObject();

	/**
	 * Returns the value of the '<em><b>Initialized Containment Levels</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Initialized Containment Levels</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Initialized Containment Levels</em>' attribute.
	 * @see #isSetInitializedContainmentLevels()
	 * @see #unsetInitializedContainmentLevels()
	 * @see #setInitializedContainmentLevels(Integer)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_InitializedContainmentLevels()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='let levelsToInitializeForRootObjects: Integer = 3 in\nif  containingObject = null\n  then levelsToInitializeForRootObjects\nelse if containingObject.initializedContainmentLevels=null then\n     levelsToInitializeForRootObjects\nelse   \n   containingObject.initializedContainmentLevels - 1 \n endif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Initialization'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getInitializedContainmentLevels();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Initialized Containment Levels</em>' attribute.
	 * @see #isSetInitializedContainmentLevels()
	 * @see #unsetInitializedContainmentLevels()
	 * @see #getInitializedContainmentLevels()
	 * @generated
	 */
	void setInitializedContainmentLevels(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitializedContainmentLevels()
	 * @see #getInitializedContainmentLevels()
	 * @see #setInitializedContainmentLevels(Integer)
	 * @generated
	 */
	void unsetInitializedContainmentLevels();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getInitializedContainmentLevels <em>Initialized Containment Levels</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Initialized Containment Levels</em>' attribute is set.
	 * @see #unsetInitializedContainmentLevels()
	 * @see #getInitializedContainmentLevels()
	 * @see #setInitializedContainmentLevels(Integer)
	 * @generated
	 */
	boolean isSetInitializedContainmentLevels();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.objects.MObjectAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MObjectAction
	 * @see #setDoAction(MObjectAction)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let do: mcore::objects::MObjectAction = mcore::objects::MObjectAction::Do in\ndo' choiceConstruction='let instantiatedProperties:OrderedSet(MProperty) = self.propertyInstance.property->excluding(null)->asOrderedSet() in \nlet nonInstantiatedProperties:OrderedSet(MProperty) =\n  if type.oclIsUndefined() then OrderedSet{} else\n     type.allFeaturesWithStorage()->select(p:MProperty | instantiatedProperties->excludes(p)) endif in\nlet start:OrderedSet(MObjectAction) = OrderedSet{MObjectAction::Do} in start->append(\nif type.oclIsUndefined() or hasPropertyInstanceNotOfType then \n   MObjectAction::FixType else null endif)->append(\nif not nonInstantiatedProperties->isEmpty() then \n   MObjectAction::CompleteSlots else null endif)->append(\nif type.oclIsUndefined() then null else\n  if  not self.hasPropertyInstanceNotOfType\n   then null else\n   MObjectAction::CleanSlots endif endif)->append(\nif true then\n   MObjectAction::StringSlot else null endif)->append(\nif true then \n   MObjectAction::BooleanSlot else null endif)->append(\nif true then \n   MObjectAction::IntegerSlot else null endif)->append(\nif true then \n   MObjectAction::RealSlot else null endif)->append(\nif true then \n   MObjectAction::DateSlot else null endif)->append(\nif true then \n   MObjectAction::LiteralSlot else null endif)->append(\nif true then \n   MObjectAction::ReferenceSlot else null endif)->append(\nif true then \n   MObjectAction::ContainmentSlot else null endif)->append(MObjectAction::AddSibling)->excluding(null)\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MObjectAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MObjectAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MObjectAction value);

	/**
	 * Returns the value of the '<em><b>Status As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status As Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status As Text</em>' attribute.
	 * @see #isSetStatusAsText()
	 * @see #unsetStatusAsText()
	 * @see #setStatusAsText(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_StatusAsText()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Summary As Text' createColumn='false'"
	 * @generated
	 */
	String getStatusAsText();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getStatusAsText <em>Status As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status As Text</em>' attribute.
	 * @see #isSetStatusAsText()
	 * @see #unsetStatusAsText()
	 * @see #getStatusAsText()
	 * @generated
	 */
	void setStatusAsText(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getStatusAsText <em>Status As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStatusAsText()
	 * @see #getStatusAsText()
	 * @see #setStatusAsText(String)
	 * @generated
	 */
	void unsetStatusAsText();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getStatusAsText <em>Status As Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Status As Text</em>' attribute is set.
	 * @see #unsetStatusAsText()
	 * @see #getStatusAsText()
	 * @see #setStatusAsText(String)
	 * @generated
	 */
	boolean isSetStatusAsText();

	/**
	 * Returns the value of the '<em><b>Derivations As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derivations As Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derivations As Text</em>' attribute.
	 * @see #isSetDerivationsAsText()
	 * @see #unsetDerivationsAsText()
	 * @see #setDerivationsAsText(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_DerivationsAsText()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Summary As Text' createColumn='false'"
	 * @generated
	 */
	String getDerivationsAsText();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getDerivationsAsText <em>Derivations As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Derivations As Text</em>' attribute.
	 * @see #isSetDerivationsAsText()
	 * @see #unsetDerivationsAsText()
	 * @see #getDerivationsAsText()
	 * @generated
	 */
	void setDerivationsAsText(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MObject#getDerivationsAsText <em>Derivations As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDerivationsAsText()
	 * @see #getDerivationsAsText()
	 * @see #setDerivationsAsText(String)
	 * @generated
	 */
	void unsetDerivationsAsText();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MObject#getDerivationsAsText <em>Derivations As Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Derivations As Text</em>' attribute is set.
	 * @see #unsetDerivationsAsText()
	 * @see #getDerivationsAsText()
	 * @see #setDerivationsAsText(String)
	 * @generated
	 */
	boolean isSetDerivationsAsText();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate createTypeInPackage$Update(MPackage trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate classNameOrNatureDefinition$Update(String trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MObjectAction trg);

	/**
	 * Returns the value of the '<em><b>Create Type In Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Create Type In Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Create Type In Package</em>' reference.
	 * @see #setCreateTypeInPackage(MPackage)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_CreateTypeInPackage()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Derivation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getCreateTypeInPackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MObject#getCreateTypeInPackage <em>Create Type In Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Create Type In Package</em>' reference.
	 * @see #getCreateTypeInPackage()
	 * @generated
	 */
	void setCreateTypeInPackage(MPackage value);

	/**
	 * Returns the value of the '<em><b>Has Property Instance Not Of Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Property Instance Not Of Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Property Instance Not Of Type</em>' attribute.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMObject_HasPropertyInstanceNotOfType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.propertyInstance->isEmpty()\n   then false\n   else not (self.propertyInstance.correctProperty()->excludes(false)) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Type Derivation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getHasPropertyInstanceNotOfType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.xocl.org/OCL body='if eContainer().oclIsUndefined() \r\n  then true \r\n  else not eContainer().oclIsTypeOf(MPropertyInstance) endif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Structural' createColumn='true'"
	 * @generated
	 */
	Boolean isRootObject();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let t:MClassifier = self.type in\r\nif t.oclIsUndefined() then false\r\nelse \r\n/*typed but wrongly:\052/\r\nif not (t.kind = mcore::ClassifierKind::ClassType) then false\r\nelse\r\nif t.abstractClass then false else\r\nif self.isRootObject() then true else\r\nlet p:MProperty = self.eContainer().oclAsType(MPropertyInstance).property in\r\nif p.oclIsUndefined() then true else\r\n/*wrongly typed based on type of property:\052/\r\nif p.type.oclIsUndefined() then false else\r\nif p.type= self.type or p.type.allSubTypes()->includes(self.type) then true else false \r\nendif endif endif endif endif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Validation' createColumn='true'"
	 * @generated
	 */
	Boolean correctlyTyped();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.isRootObject() then true else\r\nlet pI:MPropertyInstance= self.containingPropertyInstance in\r\nif pI.property.oclIsUndefined() then false else\r\nif pI.property.singular then\r\nlet pos:Integer=pI.internalContainedObject->indexOf(self) in\r\npos=1\r\nelse true\r\nendif \r\nendif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Validation' createColumn='true'"
	 * @generated
	 */
	Boolean correctMultiplicity();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(MObjectAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgAnnotation="http://www.montages.com/mCore/MCore mName='trg'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='invoke Set Do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Object invokeSetDoActionUpdate(MObjectAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.isRootObject() then \r\nlet resourceId:String=self.eContainer().oclAsType(MResource).id in\r\nif resourceId.oclIsUndefined() then \'MISSING RESOURCE ID\' else resourceId endif\r\nelse self.containingObject.id endif\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='true'"
	 * @generated
	 */
	String containerId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if isRootObject() \r\n  then if resource.oclIsUndefined()\r\n    then \'GA\'\r\n   else  \r\n      let pos:Integer=self.resource.object->indexOf(self) in\r\n          if pos<10 \r\n            then \'0\'.concat(pos.toString()) \r\n            else pos.toString() endif endif\r\n  else let pos:Integer = containingPropertyInstance \r\n              .internalContainedObject\r\n                 ->indexOf(self) in\r\nif pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='true'"
	 * @generated
	 */
	String localId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.type.oclIsUndefined() then \'\' else\r\nif self.type.allIdProperties()->isEmpty() then \'\' else\r\nif self.type.allIdProperties()->size()=1 then\r\nself.propertyValueAsString(self.type.allIdProperties()->first()) else\r\nself.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=\'\'|\r\n(if res=\'\' then \'\' else res.concat(\'|\') endif).concat(\r\nlet v:String=self.propertyValueAsString(p) in if v=\'\' then \'_\' else v endif\r\n))\r\nendif endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='true'"
	 * @generated
	 */
	String idPropertyBasedId();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let pi:objects::MPropertyInstance = self.propertyInstanceFromProperty(p) in\r\nif pi = null then \'\' else\r\npi.propertyValueAsString()\r\nendif'"
	 * @generated
	 */
	String propertyValueAsString(MProperty p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.propertyInstance->isEmpty() then null\r\nelse \r\nlet pis:OrderedSet(objects::MPropertyInstance) = self.propertyInstance->select(pi:objects::MPropertyInstance|pi.property=p) in\r\nif pis->isEmpty() then null else\r\npis->first()\r\nendif endif'"
	 * @generated
	 */
	MPropertyInstance propertyInstanceFromProperty(MProperty p);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if self.type.oclIsUndefined() then \'\' else\r\nif self.type.allIdProperties()->isEmpty() then \'\' else\r\nself.type.allIdProperties()->iterate(p:mcore::MProperty;res:String=\' \'|\r\nres.concat(p.eName).concat(\'=\"\').concat(self.propertyValueAsString(p)).concat(\'\" \')\r\n)\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Object Id' createColumn='true'"
	 * @generated
	 */
	String idPropertiesAsString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='isInstanceOf'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Boolean isInstanceOf(MClassifier mClassifier);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model classNameOrNatureDefinitionAnnotation="http://www.montages.com/mCore/MCore mName='classNameOrNatureDefinition'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='ClassNameOrNatureUpdate'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Nature and Type' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate classNameOrNatureUpdate(String classNameOrNatureDefinition);

} // MObject
