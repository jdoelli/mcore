/**
 */
package com.montages.mcore.objects;

import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.MNamed;
import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MResource Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MResourceFolder#getResource <em>Resource</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResourceFolder#getFolder <em>Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResourceFolder#getRootFolder <em>Root Folder</em>}</li>
 *   <li>{@link com.montages.mcore.objects.MResourceFolder#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolder()
 * @model annotation="http://www.xocl.org/OCL label='self.calculatedShortName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let resultofKindLabel: String = if (let e0: Boolean = if ((let e0: Boolean = rootFolder = self in \n if e0.oclIsInvalid() then null else e0 endif))= false \n then false \n else if (let e0: Boolean = indentLevel = 1 in \n if e0.oclIsInvalid() then null else e0 endif)= false \n then false \nelse if ((let e0: Boolean = rootFolder = self in \n if e0.oclIsInvalid() then null else e0 endif)= null or (let e0: Boolean = indentLevel = 1 in \n if e0.oclIsInvalid() then null else e0 endif)= null) = true \n then null \n else true endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'Workspace\' else if (let e0: Boolean = indentLevel = 2 in \n if e0.oclIsInvalid() then null else e0 endif)=true then \'Project\'\n  else \'Folder\'\nendif endif in\nresultofKindLabel\n' eNameDerive='if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())\r\nthen self.calculatedShortName\r\nelse self.specialEName.camelCaseLower()\r\nendif'"
 * @generated
 */

public interface MResourceFolder extends MNamed {
	/**
	 * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' containment reference list.
	 * @see #isSetResource()
	 * @see #unsetResource()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolder_Resource()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MResource> getResource();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MResourceFolder#getResource <em>Resource</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResource()
	 * @see #getResource()
	 * @generated
	 */
	void unsetResource();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MResourceFolder#getResource <em>Resource</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Resource</em>' containment reference list is set.
	 * @see #unsetResource()
	 * @see #getResource()
	 * @generated
	 */
	boolean isSetResource();

	/**
	 * Returns the value of the '<em><b>Folder</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MResourceFolder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Folder</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Folder</em>' containment reference list.
	 * @see #isSetFolder()
	 * @see #unsetFolder()
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolder_Folder()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 * @generated
	 */
	EList<MResourceFolder> getFolder();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MResourceFolder#getFolder <em>Folder</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetFolder()
	 * @see #getFolder()
	 * @generated
	 */
	void unsetFolder();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MResourceFolder#getFolder <em>Folder</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Folder</em>' containment reference list is set.
	 * @see #unsetFolder()
	 * @see #getFolder()
	 * @generated
	 */
	boolean isSetFolder();

	/**
	 * Returns the value of the '<em><b>Root Folder</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Folder</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Folder</em>' reference.
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolder_RootFolder()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsTypeOf(MResourceFolder) then \r\nself.eContainer().oclAsType(MResourceFolder).rootFolder\r\nelse self endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MResourceFolder getRootFolder();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.objects.MResourceFolderAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MResourceFolderAction
	 * @see #setDoAction(MResourceFolderAction)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMResourceFolder_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let do: mcore::objects::MResourceFolderAction = mcore::objects::MResourceFolderAction::Do in\ndo\n' choiceConstruction='if self.rootFolder = self or self.indentLevel = 1 then OrderedSet{\r\nmcore::objects::MResourceFolderAction::Do, \r\nmcore::objects::MResourceFolderAction::AddProject}\r\nelse OrderedSet{\r\nmcore::objects::MResourceFolderAction::Do, \r\nmcore::objects::MResourceFolderAction::AddFolder,\r\nmcore::objects::MResourceFolderAction::AddResource}  endif           \r\n->append(MResourceFolderAction::OpenEditor)                         '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MResourceFolderAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MResourceFolder#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.objects.MResourceFolderAction
	 * @see #getDoAction()
	 * @generated
	 */

	void setDoAction(MResourceFolderAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MResourceFolderAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updateResource'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	Boolean updateResource(MProperty property);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model propertyAnnotation="http://www.montages.com/mCore/MCore mName='property'" correspondingPropertyAnnotation="http://www.montages.com/mCore/MCore mName='corresponding Property'"
	 *        annotation="http://www.montages.com/mCore/MCore mName='updateResource'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	Boolean updateResource(MProperty property, MProperty correspondingProperty);

} // MResourceFolder
