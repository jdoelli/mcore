/**
 */
package com.montages.mcore.objects;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProperty Instance Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.objects.ObjectsPackage#getMPropertyInstanceAction()
 * @model
 * @generated
 */
public enum MPropertyInstanceAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Add Property To Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ADD_PROPERTY_TO_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_PROPERTY_TO_TYPE(1, "AddPropertyToType", "Add Property to Type"),

	/**
	 * The '<em><b>Clear Values</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLEAR_VALUES_VALUE
	 * @generated
	 * @ordered
	 */
	CLEAR_VALUES(2, "ClearValues", "Clear Value(s)"),
	/**
	 * The '<em><b>Into Unary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_UNARY(3, "IntoUnary", "-> [0..1]"),
	/**
	 * The '<em><b>Into Nary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_NARY(4, "IntoNary", "-> [0..*]"),
	/**
	 * The '<em><b>Into Containment</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_CONTAINMENT_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_CONTAINMENT(5, "IntoContainment", "-> Containment"),
	/**
	 * The '<em><b>String Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #STRING_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	STRING_VALUE(6, "StringValue", "+ String Value"),

	/**
	 * The '<em><b>Boolean Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	BOOLEAN_VALUE(7, "BooleanValue", "+ Boolean Value"),

	/**
	 * The '<em><b>Integer Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTEGER_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	INTEGER_VALUE(8, "IntegerValue", "+ Integer Value"),

	/**
	 * The '<em><b>Real Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REAL_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	REAL_VALUE(9, "RealValue", "+ Real Value"),

	/**
	 * The '<em><b>Date Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATE_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	DATE_VALUE(10, "DateValue", "+ Date Value"),

	/**
	 * The '<em><b>Literal Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #LITERAL_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	LITERAL_VALUE(11, "LiteralValue", "+ Literal Value"),
	/**
	 * The '<em><b>Referenced Object Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #REFERENCED_OBJECT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	REFERENCED_OBJECT_VALUE(12, "ReferencedObjectValue", "+ Reference"),

	/**
	 * The '<em><b>Contained Object</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONTAINED_OBJECT_VALUE
	 * @generated
	 * @ordered
	 */
	CONTAINED_OBJECT(13, "ContainedObject", "+ Object"),
	/**
	 * The '<em><b>Class With String Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CLASS_WITH_STRING_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	CLASS_WITH_STRING_ATTRIBUTE(14, "ClassWithStringAttribute", "Move to New Contained Class");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Add Property To Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Property To Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_PROPERTY_TO_TYPE
	 * @model name="AddPropertyToType" literal="Add Property to Type"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_PROPERTY_TO_TYPE_VALUE = 1;

	/**
	 * The '<em><b>Clear Values</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Clear Values</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLEAR_VALUES
	 * @model name="ClearValues" literal="Clear Value(s)"
	 * @generated
	 * @ordered
	 */
	public static final int CLEAR_VALUES_VALUE = 2;

	/**
	 * The '<em><b>Into Unary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Unary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY
	 * @model name="IntoUnary" literal="-> [0..1]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_UNARY_VALUE = 3;

	/**
	 * The '<em><b>Into Nary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Nary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY
	 * @model name="IntoNary" literal="-> [0..*]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_NARY_VALUE = 4;

	/**
	 * The '<em><b>Into Containment</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Containment</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_CONTAINMENT
	 * @model name="IntoContainment" literal="-> Containment"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_CONTAINMENT_VALUE = 5;

	/**
	 * The '<em><b>String Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>String Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #STRING_VALUE
	 * @model name="StringValue" literal="+ String Value"
	 * @generated
	 * @ordered
	 */
	public static final int STRING_VALUE_VALUE = 6;

	/**
	 * The '<em><b>Boolean Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Boolean Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #BOOLEAN_VALUE
	 * @model name="BooleanValue" literal="+ Boolean Value"
	 * @generated
	 * @ordered
	 */
	public static final int BOOLEAN_VALUE_VALUE = 7;

	/**
	 * The '<em><b>Integer Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Integer Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTEGER_VALUE
	 * @model name="IntegerValue" literal="+ Integer Value"
	 * @generated
	 * @ordered
	 */
	public static final int INTEGER_VALUE_VALUE = 8;

	/**
	 * The '<em><b>Real Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Real Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REAL_VALUE
	 * @model name="RealValue" literal="+ Real Value"
	 * @generated
	 * @ordered
	 */
	public static final int REAL_VALUE_VALUE = 9;

	/**
	 * The '<em><b>Date Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Date Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATE_VALUE
	 * @model name="DateValue" literal="+ Date Value"
	 * @generated
	 * @ordered
	 */
	public static final int DATE_VALUE_VALUE = 10;

	/**
	 * The '<em><b>Literal Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Literal Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #LITERAL_VALUE
	 * @model name="LiteralValue" literal="+ Literal Value"
	 * @generated
	 * @ordered
	 */
	public static final int LITERAL_VALUE_VALUE = 11;

	/**
	 * The '<em><b>Referenced Object Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Referenced Object Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REFERENCED_OBJECT_VALUE
	 * @model name="ReferencedObjectValue" literal="+ Reference"
	 * @generated
	 * @ordered
	 */
	public static final int REFERENCED_OBJECT_VALUE_VALUE = 12;

	/**
	 * The '<em><b>Contained Object</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Contained Object</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONTAINED_OBJECT
	 * @model name="ContainedObject" literal="+ Object"
	 * @generated
	 * @ordered
	 */
	public static final int CONTAINED_OBJECT_VALUE = 13;

	/**
	 * The '<em><b>Class With String Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Class With String Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLASS_WITH_STRING_ATTRIBUTE
	 * @model name="ClassWithStringAttribute" literal="Move to New Contained Class"
	 * @generated
	 * @ordered
	 */
	public static final int CLASS_WITH_STRING_ATTRIBUTE_VALUE = 14;

	/**
	 * An array of all the '<em><b>MProperty Instance Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPropertyInstanceAction[] VALUES_ARRAY = new MPropertyInstanceAction[] {
			DO, ADD_PROPERTY_TO_TYPE, CLEAR_VALUES, INTO_UNARY, INTO_NARY,
			INTO_CONTAINMENT, STRING_VALUE, BOOLEAN_VALUE, INTEGER_VALUE,
			REAL_VALUE, DATE_VALUE, LITERAL_VALUE, REFERENCED_OBJECT_VALUE,
			CONTAINED_OBJECT, CLASS_WITH_STRING_ATTRIBUTE, };

	/**
	 * A public read-only list of all the '<em><b>MProperty Instance Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPropertyInstanceAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProperty Instance Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyInstanceAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyInstanceAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Instance Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyInstanceAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyInstanceAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Instance Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyInstanceAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case ADD_PROPERTY_TO_TYPE_VALUE:
			return ADD_PROPERTY_TO_TYPE;
		case CLEAR_VALUES_VALUE:
			return CLEAR_VALUES;
		case INTO_UNARY_VALUE:
			return INTO_UNARY;
		case INTO_NARY_VALUE:
			return INTO_NARY;
		case INTO_CONTAINMENT_VALUE:
			return INTO_CONTAINMENT;
		case STRING_VALUE_VALUE:
			return STRING_VALUE;
		case BOOLEAN_VALUE_VALUE:
			return BOOLEAN_VALUE;
		case INTEGER_VALUE_VALUE:
			return INTEGER_VALUE;
		case REAL_VALUE_VALUE:
			return REAL_VALUE;
		case DATE_VALUE_VALUE:
			return DATE_VALUE;
		case LITERAL_VALUE_VALUE:
			return LITERAL_VALUE;
		case REFERENCED_OBJECT_VALUE_VALUE:
			return REFERENCED_OBJECT_VALUE;
		case CONTAINED_OBJECT_VALUE:
			return CONTAINED_OBJECT;
		case CLASS_WITH_STRING_ATTRIBUTE_VALUE:
			return CLASS_WITH_STRING_ATTRIBUTE;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPropertyInstanceAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPropertyInstanceAction
