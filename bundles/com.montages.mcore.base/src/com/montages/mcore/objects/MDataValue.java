/**
 */
package com.montages.mcore.objects;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MData Value</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.objects.MDataValue#getDataValue <em>Data Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.objects.ObjectsPackage#getMDataValue()
 * @model annotation="http://www.xocl.org/OCL label='self.dataValue\r\n\r\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Data Value\'\n' correctAndDefinedValueDerive='let var1: Boolean = let e1: Boolean = if (let chain11: String = dataValue in\nif chain11 <> null then true else false \n  endif)= false \n then false \n else if (correctDataValue())= false \n then false \n else if (correctMultiplicity())= false \n then false \nelse if ((let chain11: String = dataValue in\nif chain11 <> null then true else false \n  endif)= null or (correctDataValue())= null or (correctMultiplicity())= null) = true \n then null \n else true endif endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif in\nvar1\n'"
 * @generated
 */

public interface MDataValue extends MValue {
	/**
	 * Returns the value of the '<em><b>Data Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Value</em>' attribute.
	 * @see #isSetDataValue()
	 * @see #unsetDataValue()
	 * @see #setDataValue(String)
	 * @see com.montages.mcore.objects.ObjectsPackage#getMDataValue_DataValue()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	String getDataValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.objects.MDataValue#getDataValue <em>Data Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Value</em>' attribute.
	 * @see #isSetDataValue()
	 * @see #unsetDataValue()
	 * @see #getDataValue()
	 * @generated
	 */
	void setDataValue(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.objects.MDataValue#getDataValue <em>Data Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDataValue()
	 * @see #getDataValue()
	 * @see #setDataValue(String)
	 * @generated
	 */
	void unsetDataValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.objects.MDataValue#getDataValue <em>Data Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Data Value</em>' attribute is set.
	 * @see #unsetDataValue()
	 * @see #getDataValue()
	 * @see #setDataValue(String)
	 * @generated
	 */
	boolean isSetDataValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let pi:MPropertyInstance= self.containingPropertyInstance in\r\nlet p:mcore::MProperty = pi.property in\r\nif p.oclIsUndefined() \r\n  then pi.internalLiteralValue->isEmpty()\r\n    and pi.internalReferencedObject->isEmpty()\r\n    and pi.internalContainedObject->isEmpty()\r\n  else (p.kind = mcore::PropertyKind::Attribute) \r\n       and p.correctlyTyped endif'"
	 * @generated
	 */
	Boolean correctDataValue();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let p:MProperty = self.containingPropertyInstance.property in\r\nif p.oclIsUndefined() then true else\r\nlet pos:Integer = self.containingPropertyInstance.internalDataValue->indexOf(self) in \r\nif p.singular then pos=1 else true endif endif'"
	 * @generated
	 */
	Boolean correctMultiplicity();

} // MDataValue
