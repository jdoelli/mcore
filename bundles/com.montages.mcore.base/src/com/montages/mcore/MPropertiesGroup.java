/**
 */
package com.montages.mcore;

import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProperties Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MPropertiesGroup#getOrder <em>Order</em>}</li>
 *   <li>{@link com.montages.mcore.MPropertiesGroup#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMPropertiesGroup()
 * @model annotation="http://www.xocl.org/OCL label='self.eName.concat(\'(\').concat(self.nrOfPropertiesAndSignatures.toString()).concat(\'):\')'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Group\'\n' containingClassifierDerive='if self.eContainer().oclIsUndefined() then null else\r\n  self.eContainer().oclAsType(MPropertiesContainer).containingClassifier\r\nendif' eNameDerive='if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseUpper()\r\nelse self.specialEName.camelCaseUpper()\r\nendif'"
 * @generated
 */

public interface MPropertiesGroup
		extends MPropertiesContainer, MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MPropertiesGroupAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPropertiesGroupAction
	 * @see #setDoAction(MPropertiesGroupAction)
	 * @see com.montages.mcore.McorePackage#getMPropertiesGroup_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MPropertiesGroupAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertiesGroupAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPropertiesGroup#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MPropertiesGroupAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MPropertiesGroupAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MPropertiesGroupAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='doActionUpdate'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	XUpdate doActionUpdate(MPropertiesGroupAction mPropertiesGroupAction);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='invokeSetDoAction'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 * @generated
	 */
	Object invokeSetDoAction(MPropertiesGroupAction mPropertiesGroupAction);

	/**
	 * Returns the value of the '<em><b>Order</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Order</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Order</em>' attribute.
	 * @see #isSetOrder()
	 * @see #unsetOrder()
	 * @see #setOrder(Integer)
	 * @see com.montages.mcore.McorePackage#getMPropertiesGroup_Order()
	 * @model default="0" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Order' createColumn='false'"
	 * @generated
	 */
	Integer getOrder();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MPropertiesGroup#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Order</em>' attribute.
	 * @see #isSetOrder()
	 * @see #unsetOrder()
	 * @see #getOrder()
	 * @generated
	 */
	void setOrder(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MPropertiesGroup#getOrder <em>Order</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOrder()
	 * @see #getOrder()
	 * @see #setOrder(Integer)
	 * @generated
	 */
	void unsetOrder();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MPropertiesGroup#getOrder <em>Order</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Order</em>' attribute is set.
	 * @see #unsetOrder()
	 * @see #getOrder()
	 * @see #setOrder(Integer)
	 * @generated
	 */
	boolean isSetOrder();

} // MPropertiesGroup
