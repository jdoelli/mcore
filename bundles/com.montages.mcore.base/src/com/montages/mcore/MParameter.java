/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EParameter;
import org.langlets.acore.classifiers.AClassifier;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MParameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MParameter#getContainingSignature <em>Containing Signature</em>}</li>
 *   <li>{@link com.montages.mcore.MParameter#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.MParameter#getSignatureAsString <em>Signature As String</em>}</li>
 *   <li>{@link com.montages.mcore.MParameter#getInternalEParameter <em>Internal EParameter</em>}</li>
 *   <li>{@link com.montages.mcore.MParameter#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMParameter()
 * @model annotation="http://www.xocl.org/OCL label='eLabel'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Parameter\'\n' correctNameDerive='(not ambiguousParameterName()) and \r\nif self.hasSimpleModelingType then not self.stringEmpty(self.name) else true endif' correctShortNameDerive='not ambiguousParameterShortName()' calculatedMandatoryDerive='self.mandatory' calculatedTypeDerive='self.type' calculatedSingularDerive='self.singular' simpleTypeIsCorrectDerive='if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... \052/' calculatedNameDerive='(if not stringEmpty(name) then name else  \r\nif hasSimpleDataType then \'NAMEMISSING\' else nameFromType() endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   nameFromType().camelCaseUpper()\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if ambiguousParameterName() then \'  (AMBIGUOUS)\' else \'\' endif)' calculatedShortNameDerive='(if stringEmpty(name) then shortNameFromType()\r\nelse\r\n  if stringEmpty(shortName) then name\r\n  else shortName\r\n endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   shortNameFromType().camelCaseUpper()\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if self.ambiguousParameterShortName() then \'( AMBIGUOUS)\' else \'\' endif)'"
 * @generated
 */

public interface MParameter extends MExplicitlyTypedAndNamed, MVariable,
		MModelElement, AClassifier {
	/**
	 * Returns the value of the '<em><b>Containing Signature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Signature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Signature</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMParameter_ContainingSignature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MOperationSignature)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MOperationSignature getContainingSignature();

	/**
	 * Returns the value of the '<em><b>ELabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ELabel</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ELabel</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMParameter_ELabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if stringEmpty(name)\r\n  then self.eTypeLabel \r\n  else eName\r\n                     .concat(if self.eTypeLabel=\'\'\r\n                             then \'\' else \':\'.concat(self.eTypeLabel) endif) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getELabel();

	/**
	 * Returns the value of the '<em><b>Signature As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Signature As String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Signature As String</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMParameter_SignatureAsString()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.type.oclIsUndefined() then \'TYPE_MISSING\' else self.type.eName.concat(self.multiplicityAsString) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getSignatureAsString();

	/**
	 * Returns the value of the '<em><b>Internal EParameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EParameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EParameter</em>' reference.
	 * @see #isSetInternalEParameter()
	 * @see #unsetInternalEParameter()
	 * @see #setInternalEParameter(EParameter)
	 * @see com.montages.mcore.McorePackage#getMParameter_InternalEParameter()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relation to ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EParameter getInternalEParameter();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MParameter#getInternalEParameter <em>Internal EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EParameter</em>' reference.
	 * @see #isSetInternalEParameter()
	 * @see #unsetInternalEParameter()
	 * @see #getInternalEParameter()
	 * @generated
	 */
	void setInternalEParameter(EParameter value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MParameter#getInternalEParameter <em>Internal EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEParameter()
	 * @see #getInternalEParameter()
	 * @see #setInternalEParameter(EParameter)
	 * @generated
	 */
	void unsetInternalEParameter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MParameter#getInternalEParameter <em>Internal EParameter</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EParameter</em>' reference is set.
	 * @see #unsetInternalEParameter()
	 * @see #getInternalEParameter()
	 * @see #setInternalEParameter(EParameter)
	 * @generated
	 */
	boolean isSetInternalEParameter();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MParameterAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MParameterAction
	 * @see #setDoAction(MParameterAction)
	 * @see com.montages.mcore.McorePackage#getMParameter_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let do: mcore::MParameterAction = mcore::MParameterAction::Do in\ndo\n\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MParameterAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MParameter#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MParameterAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MParameterAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MParameterAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if true then false else\r\nif self.containingSignature.oclIsUndefined() then false else\r\nif self.containingSignature.parameter->isEmpty() then false else\r\nself.containingSignature.parameter\r\n  ->exists(p:MParameter|not (p=self) \r\n       and p.sameName(self)\r\n       and (if stringEmpty(p.name) then \r\n                    p.type = self.type else true endif))\r\nendif endif\r\nendif'"
	 * @generated
	 */
	Boolean ambiguousParameterName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if true then false else \r\nif not self.ambiguousParameterName() then false else\r\nif self.containingSignature.oclIsUndefined() then false else\r\nif self.containingSignature.parameter->isEmpty() then false else\r\nself.containingSignature.parameter\r\n  ->exists(p:MParameter|not (p=self) \r\n       and p.sameShortName(self))\r\nendif endif endif\r\nendif'"
	 * @generated
	 */
	Boolean ambiguousParameterShortName();

} // MParameter
