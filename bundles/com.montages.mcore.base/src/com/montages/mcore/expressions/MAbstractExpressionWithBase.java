/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MVariable;
import com.montages.mcore.SimpleType;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Expression With Base</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseAsCode <em>Base As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseDefinition <em>Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseVar <em>Base Var</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseExitType <em>Base Exit Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseExitSimpleType <em>Base Exit Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseExitMandatory <em>Base Exit Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseExitSingular <em>Base Exit Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase()
 * @model
 * @generated
 */

public interface MAbstractExpressionWithBase extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Base As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base As Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base As Code</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseAsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'\'\n  else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)\n  then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)\n  else baseDefinition.calculatedAsCode\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getBaseAsCode();

	/**
	 * Returns the value of the '<em><b>Base</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.ExpressionBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionBase
	 * @see #isSetBase()
	 * @see #unsetBase()
	 * @see #setBase(ExpressionBase)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_Base()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	ExpressionBase getBase();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionBase
	 * @see #isSetBase()
	 * @see #unsetBase()
	 * @see #getBase()
	 * @generated
	 */
	void setBase(ExpressionBase value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBase()
	 * @see #getBase()
	 * @see #setBase(ExpressionBase)
	 * @generated
	 */
	void unsetBase();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBase <em>Base</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Base</em>' attribute is set.
	 * @see #unsetBase()
	 * @see #getBase()
	 * @see #setBase(ExpressionBase)
	 * @generated
	 */
	boolean isSetBase();

	/**
	 * Returns the value of the '<em><b>Base Definition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Definition</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Definition</em>' reference.
	 * @see #setBaseDefinition(MAbstractBaseDefinition)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseDefinition()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let r: mcore::expressions::MAbstractBaseDefinition =\r\n\r\nif base=mcore::expressions::ExpressionBase::Undefined \r\n  then null\r\nelse if base=mcore::expressions::ExpressionBase::SelfObject\r\n  then scopeSelf->first() \r\n else if base=mcore::expressions::ExpressionBase::EContainer\r\n  then scopeContainerBase->first() \r\nelse if base=mcore::expressions::ExpressionBase::Target\r\n  then scopeTrg->first() \r\nelse if base=mcore::expressions::ExpressionBase::ObjectObject\r\n  then scopeObj->first() \r\n  else if base=mcore::expressions::ExpressionBase::Source\r\n  then scopeSource->first() \r\nelse if base=mcore::expressions::ExpressionBase::ConstantValue \r\n  then scopeSimpleTypeConstants->select(c:mcore::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::ConstantLiteralValue\r\n  then scopeLiteralConstants->select(c:mcore::expressions::MLiteralConstantBaseDefinition | c.namedConstant = baseVar)->first() \r\nelse if base=mcore::expressions::ExpressionBase::ConstantObjectReference\r\n  then scopeObjectReferenceConstants->select(c:mcore::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = baseVar)->first()\r\nelse if base=mcore::expressions::ExpressionBase::Variable\r\n  then scopeVariables->select(c:mcore::expressions::MVariableBaseDefinition | c.namedExpression = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Parameter\r\n  then scopeParameters->select(c:mcore::expressions::MParameterBaseDefinition | c.parameter = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Iterator\r\n  then scopeIterator->select(c:mcore::expressions::MIteratorBaseDefinition | c.iterator = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Accumulator\r\n  then scopeAccumulator->select(c:mcore::expressions::MAccumulatorBaseDefinition | c.accumulator = baseVar)->first()  \r\n else if base=mcore::expressions::ExpressionBase::OneValue or base=mcore::expressions::ExpressionBase::ZeroValue  or base = mcore::expressions::ExpressionBase::MinusOneValue\r\n then scopeNumberBase->select(c:mcore::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()\r\nelse scopeBase->select(c:mcore::expressions::MBaseDefinition | c.expressionBase=base)->first() \r\nendif endif endif endif endif endif endif endif endif endif endif endif endif endif\r\nin\r\nif r.oclIsUndefined() then null else r endif' choiceConstruction='self.getScope()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MAbstractBaseDefinition getBaseDefinition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseDefinition <em>Base Definition</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Definition</em>' reference.
	 * @see #getBaseDefinition()
	 * @generated
	 */
	void setBaseDefinition(MAbstractBaseDefinition value);

	/**
	 * Returns the value of the '<em><b>Base Var</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Var</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Var</em>' reference.
	 * @see #isSetBaseVar()
	 * @see #unsetBaseVar()
	 * @see #setBaseVar(MVariable)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseVar()
	 * @model unsettable="true"
	 * @generated
	 */
	MVariable getBaseVar();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseVar <em>Base Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Base Var</em>' reference.
	 * @see #isSetBaseVar()
	 * @see #unsetBaseVar()
	 * @see #getBaseVar()
	 * @generated
	 */
	void setBaseVar(MVariable value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseVar <em>Base Var</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetBaseVar()
	 * @see #getBaseVar()
	 * @see #setBaseVar(MVariable)
	 * @generated
	 */
	void unsetBaseVar();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase#getBaseVar <em>Base Var</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Base Var</em>' reference is set.
	 * @see #unsetBaseVar()
	 * @see #getBaseVar()
	 * @see #setBaseVar(MVariable)
	 * @generated
	 */
	boolean isSetBaseVar();

	/**
	 * Returns the value of the '<em><b>Base Exit Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Type</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if baseDefinition.oclIsUndefined() \r\nthen null\r\nelse \r\n  /* Trick b/c we need to reference the container here and baseDefinition is generated on the fly \052/\r\n  if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::SelfObject\r\n    then self.selfObjectType\r\n\telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target\r\n    \tthen self.targetObjectType\r\n\telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::ObjectObject\r\n    \tthen self.objectObjectType\r\n    \telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Source\r\n    \tthen self.srcObjectType\r\n   else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::EContainer\r\n    \tthen  if self.selfObjectType.allClassesContainedIn()->size() = 1 then self.selfObjectType.allClassesContainedIn()->first() else null endif\r\n    \telse baseDefinition.calculatedType\r\n  \tendif endif endif endif\r\n  endif\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getBaseExitType();

	/**
	 * Returns the value of the '<em><b>Base Exit Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitSimpleType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if baseDefinition.oclIsUndefined()  or not (self.baseExitType.oclIsUndefined())\r\nthen mcore::SimpleType::None\r\nelse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target\r\nthen self.targetSimpleType\r\nelse self.baseDefinition.calculatedSimpleType\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	SimpleType getBaseExitSimpleType();

	/**
	 * Returns the value of the '<em><b>Base Exit Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Mandatory</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitMandatory()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.calculatedMandatory\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getBaseExitMandatory();

	/**
	 * Returns the value of the '<em><b>Base Exit Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Base Exit Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Base Exit Singular</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractExpressionWithBase_BaseExitSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen true\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.calculatedSingular\nendif\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getBaseExitSingular();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate baseDefinition$Update(MAbstractBaseDefinition trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='baseAsCode\n'"
	 * @generated
	 */
	String asCodeForBuiltIn();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='if baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition)\r\nthen let b: mcore::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MSimpleTypeConstantBaseDefinition) in\r\nb.namedConstant.eName\r\nelse if baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)\r\nthen let b: mcore::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MLiteralConstantBaseDefinition) in\r\nb.namedConstant.eName\r\nelse null endif endif'"
	 * @generated
	 */
	String asCodeForConstants();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in\r\nlet vName: String = v.namedExpression.eName in\r\nbaseAsCode'"
	 * @generated
	 */
	String asCodeForVariables();

} // MAbstractExpressionWithBase
