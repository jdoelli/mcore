/**
 */
package com.montages.mcore.expressions;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MApplication Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMApplicationAction()
 * @model
 * @generated
 */
public enum MApplicationAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),
	/**
	 * The '<em><b>Operand Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERAND_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	OPERAND_CHAIN(1, "OperandChain", "+ Operand: Chain"),
	/**
	 * The '<em><b>Operand Apply</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERAND_APPLY_VALUE
	 * @generated
	 * @ordered
	 */
	OPERAND_APPLY(2, "OperandApply", "+ Operand: Apply"),
	/**
	 * The '<em><b>Operand Data Constant</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERAND_DATA_CONSTANT_VALUE
	 * @generated
	 * @ordered
	 */
	OPERAND_DATA_CONSTANT(3, "OperandDataConstant", "+ Operand: Data Constant"),
	/**
	 * The '<em><b>Operand Literal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #OPERAND_LITERAL_VALUE
	 * @generated
	 * @ordered
	 */
	OPERAND_LITERAL(4, "OperandLiteral", "+ Operand: Literal"),
	/**
	 * The '<em><b>Into Chain</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_CHAIN_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_CHAIN(5, "IntoChain", "-> Chain"),
	/**
	 * The '<em><b>Into Cond Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_COND_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_COND_OF_IF_THEN_ELSE(6, "IntoCondOfIfThenElse", "-> Cond of If-Then-Else\n\n   Constraint"),
	/**
	 * The '<em><b>Into Then Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_THEN_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_THEN_OF_IF_THEN_ELSE(7, "IntoThenOfIfThenElse", "-> Then of If-Then-Else\n\n   Constraint"),
	/**
	 * The '<em><b>Into Else Of If Then Else</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_ELSE_OF_IF_THEN_ELSE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_ELSE_OF_IF_THEN_ELSE(8, "IntoElseOfIfThenElse", "-> Else of If-Then-Else\n\n   Constraint"),
	/**
	 * The '<em><b>Remove Apply</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #REMOVE_APPLY_VALUE
	 * @generated
	 * @ordered
	 */
	REMOVE_APPLY(9, "RemoveApply", "-> Remove Apply");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Operand Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operand Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERAND_CHAIN
	 * @model name="OperandChain" literal="+ Operand: Chain"
	 * @generated
	 * @ordered
	 */
	public static final int OPERAND_CHAIN_VALUE = 1;

	/**
	 * The '<em><b>Operand Apply</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operand Apply</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERAND_APPLY
	 * @model name="OperandApply" literal="+ Operand: Apply"
	 * @generated
	 * @ordered
	 */
	public static final int OPERAND_APPLY_VALUE = 2;

	/**
	 * The '<em><b>Operand Data Constant</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operand Data Constant</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERAND_DATA_CONSTANT
	 * @model name="OperandDataConstant" literal="+ Operand: Data Constant"
	 * @generated
	 * @ordered
	 */
	public static final int OPERAND_DATA_CONSTANT_VALUE = 3;

	/**
	 * The '<em><b>Operand Literal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Operand Literal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPERAND_LITERAL
	 * @model name="OperandLiteral" literal="+ Operand: Literal"
	 * @generated
	 * @ordered
	 */
	public static final int OPERAND_LITERAL_VALUE = 4;

	/**
	 * The '<em><b>Into Chain</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Chain</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_CHAIN
	 * @model name="IntoChain" literal="-> Chain"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_CHAIN_VALUE = 5;

	/**
	 * The '<em><b>Into Cond Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Cond Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_COND_OF_IF_THEN_ELSE
	 * @model name="IntoCondOfIfThenElse" literal="-> Cond of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_COND_OF_IF_THEN_ELSE_VALUE = 6;

	/**
	 * The '<em><b>Into Then Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Then Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_THEN_OF_IF_THEN_ELSE
	 * @model name="IntoThenOfIfThenElse" literal="-> Then of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_THEN_OF_IF_THEN_ELSE_VALUE = 7;

	/**
	 * The '<em><b>Into Else Of If Then Else</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Else Of If Then Else</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_ELSE_OF_IF_THEN_ELSE
	 * @model name="IntoElseOfIfThenElse" literal="-> Else of If-Then-Else\n\n   Constraint"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_ELSE_OF_IF_THEN_ELSE_VALUE = 8;

	/**
	 * The '<em><b>Remove Apply</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Remove Apply</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REMOVE_APPLY
	 * @model name="RemoveApply" literal="-> Remove Apply"
	 * @generated
	 * @ordered
	 */
	public static final int REMOVE_APPLY_VALUE = 9;

	/**
	 * An array of all the '<em><b>MApplication Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MApplicationAction[] VALUES_ARRAY = new MApplicationAction[] {
			DO, OPERAND_CHAIN, OPERAND_APPLY, OPERAND_DATA_CONSTANT,
			OPERAND_LITERAL, INTO_CHAIN, INTO_COND_OF_IF_THEN_ELSE,
			INTO_THEN_OF_IF_THEN_ELSE, INTO_ELSE_OF_IF_THEN_ELSE,
			REMOVE_APPLY, };

	/**
	 * A public read-only list of all the '<em><b>MApplication Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MApplicationAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MApplication Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MApplicationAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MApplicationAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MApplication Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MApplicationAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MApplicationAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MApplication Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MApplicationAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case OPERAND_CHAIN_VALUE:
			return OPERAND_CHAIN;
		case OPERAND_APPLY_VALUE:
			return OPERAND_APPLY;
		case OPERAND_DATA_CONSTANT_VALUE:
			return OPERAND_DATA_CONSTANT;
		case OPERAND_LITERAL_VALUE:
			return OPERAND_LITERAL;
		case INTO_CHAIN_VALUE:
			return INTO_CHAIN;
		case INTO_COND_OF_IF_THEN_ELSE_VALUE:
			return INTO_COND_OF_IF_THEN_ELSE;
		case INTO_THEN_OF_IF_THEN_ELSE_VALUE:
			return INTO_THEN_OF_IF_THEN_ELSE;
		case INTO_ELSE_OF_IF_THEN_ELSE_VALUE:
			return INTO_ELSE_OF_IF_THEN_ELSE;
		case REMOVE_APPLY_VALUE:
			return REMOVE_APPLY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MApplicationAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MApplicationAction
