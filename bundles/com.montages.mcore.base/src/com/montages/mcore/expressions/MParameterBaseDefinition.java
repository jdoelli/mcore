/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MParameter;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MParameter Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MParameterBaseDefinition#getParameter <em>Parameter</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMParameterBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<par> \'.concat(if parameter.oclIsUndefined() then \'\' else parameter.eName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='mcore::expressions::ExpressionBase::Parameter\n' calculatedAsCodeDerive='if parameter.oclIsUndefined()then \'MISSING PARAMETER\' else parameter.eName endif' calculatedMandatoryDerive='if parameter.oclIsUndefined() then true\r\nelse parameter.calculatedMandatory endif' calculatedSingularDerive='if parameter.oclIsUndefined() then true\r\nelse parameter.calculatedSingular endif' calculatedSimpleTypeDerive='if parameter.oclIsUndefined() then SimpleType::None\r\nelse parameter.calculatedSimpleType\r\nendif' calculatedTypeDerive='if parameter.oclIsUndefined() then null\r\nelse parameter.calculatedType\r\nendif'"
 * @generated
 */

public interface MParameterBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Parameter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parameter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parameter</em>' reference.
	 * @see #isSetParameter()
	 * @see #unsetParameter()
	 * @see #setParameter(MParameter)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMParameterBaseDefinition_Parameter()
	 * @model unsettable="true"
	 * @generated
	 */
	MParameter getParameter();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MParameterBaseDefinition#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parameter</em>' reference.
	 * @see #isSetParameter()
	 * @see #unsetParameter()
	 * @see #getParameter()
	 * @generated
	 */
	void setParameter(MParameter value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MParameterBaseDefinition#getParameter <em>Parameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetParameter()
	 * @see #getParameter()
	 * @see #setParameter(MParameter)
	 * @generated
	 */
	void unsetParameter();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MParameterBaseDefinition#getParameter <em>Parameter</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Parameter</em>' reference is set.
	 * @see #unsetParameter()
	 * @see #getParameter()
	 * @see #setParameter(MParameter)
	 * @generated
	 */
	boolean isSetParameter();

} // MParameterBaseDefinition
