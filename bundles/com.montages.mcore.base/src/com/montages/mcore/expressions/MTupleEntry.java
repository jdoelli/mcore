/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MNamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MTuple Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMTupleEntry()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MTupleEntry'"
 * @generated
 */

public interface MTupleEntry extends MNamed, MAbstractTupleEntry {
} // MTupleEntry
