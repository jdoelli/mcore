/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MBase Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * This is used for all simple cases of base definition that do no require additional settings (e.g. NullValue, ZeroValue, etc.)
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MBaseDefinition#getExpressionBase <em>Expression Base</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<builtin> \'.concat(calculatedAsCode)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedBaseDerive='expressionBase\n' calculatedAsCodeDerive='if expressionBase=ExpressionBase::NullValue then \'null\'\r\nelse if expressionBase=ExpressionBase::ZeroValue then \'0\'\r\nelse if expressionBase=ExpressionBase::OneValue then \'1\'\r\nelse if expressionBase=ExpressionBase::MinusOneValue then \'-1\'\r\nelse if expressionBase=ExpressionBase::FalseValue then \'false\'\r\nelse if expressionBase=ExpressionBase::TrueValue then \'true\'\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then \'\\\'\\\'\'\r\nelse if expressionBase=ExpressionBase::EmptyCollection then \'OrderedSet{}\'\r\nelse \'<?>\' endif endif endif endif endif endif endif endif' calculatedMandatoryDerive='if expressionBase=ExpressionBase::EmptyCollection then false\r\nelse true endif' calculatedSingularDerive='if expressionBase=ExpressionBase::EmptyCollection then false\r\nelse true endif' calculatedSimpleTypeDerive='if expressionBase=ExpressionBase::NullValue or expressionBase=ExpressionBase::EmptyCollection then SimpleType::None\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then SimpleType::String\r\nelse if expressionBase=ExpressionBase::ZeroValue \r\n  or expressionBase=ExpressionBase::OneValue \r\n  or expressionBase=ExpressionBase::MinusOneValue then SimpleType::Integer\r\nelse SimpleType::Boolean endif endif endif'"
 * @generated
 */

public interface MBaseDefinition extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Expression Base</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.ExpressionBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression Base</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionBase
	 * @see #isSetExpressionBase()
	 * @see #unsetExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMBaseDefinition_ExpressionBase()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	ExpressionBase getExpressionBase();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression Base</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionBase
	 * @see #isSetExpressionBase()
	 * @see #unsetExpressionBase()
	 * @see #getExpressionBase()
	 * @generated
	 */
	void setExpressionBase(ExpressionBase value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpressionBase()
	 * @see #getExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @generated
	 */
	void unsetExpressionBase();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MBaseDefinition#getExpressionBase <em>Expression Base</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression Base</em>' attribute is set.
	 * @see #unsetExpressionBase()
	 * @see #getExpressionBase()
	 * @see #setExpressionBase(ExpressionBase)
	 * @generated
	 */
	boolean isSetExpressionBase();

} // MBaseDefinition
