/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MContainer Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMContainerBaseDefinition()
 * @model annotation="http://www.montages.com/mCore/MCore mName='MContainerBaseDefinition'"
 *        annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'eContainer()\'\n' calculatedBaseDerive='ExpressionBase::EContainer' calculatedMandatoryDerive='true\n' calculatedSingularDerive='true\n' calculatedSimpleTypeDerive='SimpleType::Object'"
 * @generated
 */

public interface MContainerBaseDefinition extends MAbstractBaseDefinition {

} // MContainerBaseDefinition
