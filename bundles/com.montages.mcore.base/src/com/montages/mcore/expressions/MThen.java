/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MThen</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MThen#getExpression <em>Expression</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMThen()
 * @model annotation="http://www.xocl.org/OCL label='\'then \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse expression.getShortCode() endif\r\n)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let kind: String = \'THEN\' in\nkind\n' calculatedOwnMandatoryDerive='if expression.oclIsUndefined() then false\r\nelse expression.calculatedMandatory endif' calculatedOwnTypeDerive='if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif' calculatedOwnSingularDerive='if expression.oclIsUndefined() then true\r\nelse expression.calculatedSingular endif' calculatedOwnSimpleTypeDerive='if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif' asBasicCodeDerive='\'then \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse \r\n\t  if expression.isComplexExpression.oclIsUndefined() then \'ERRORisComplexExpressionUndefined\' \r\n\t  else if expression.asCode.oclIsUndefined() then \r\n\t  \'ERRORasCodeUndefined\' else\r\n\t\r\n\tif expression.isComplexExpression\r\n\t  then \'(\'.concat(expression.asCode).concat(\')\')\r\n\t  else expression.asCode endif endif\r\n\t  \r\n\t  endif endif\r\n)'"
 * @generated
 */

public interface MThen extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Expression</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Expression</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMThen_Expression()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 * @generated
	 */
	MChainOrApplication getExpression();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MThen#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Expression</em>' containment reference.
	 * @see #isSetExpression()
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @generated
	 */
	void setExpression(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MThen#getExpression <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	void unsetExpression();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MThen#getExpression <em>Expression</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Expression</em>' containment reference is set.
	 * @see #unsetExpression()
	 * @see #getExpression()
	 * @see #setExpression(MChainOrApplication)
	 * @generated
	 */
	boolean isSetExpression();

} // MThen
