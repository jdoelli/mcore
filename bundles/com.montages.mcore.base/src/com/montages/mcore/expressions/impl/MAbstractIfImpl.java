/**
 */
package com.montages.mcore.expressions.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractIf;
import com.montages.mcore.expressions.MChainOrApplication;
import com.montages.mcore.expressions.MElse;
import com.montages.mcore.expressions.MElseIf;
import com.montages.mcore.expressions.MThen;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract If</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractIfImpl#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractIfImpl#getThenPart <em>Then Part</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractIfImpl#getElseifPart <em>Elseif Part</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractIfImpl#getElsePart <em>Else Part</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractIfImpl extends MAbstractExpressionImpl
		implements MAbstractIf {
	/**
	 * The cached value of the '{@link #getCondition() <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCondition()
	 * @generated
	 * @ordered
	 */
	protected MChainOrApplication condition;

	/**
	 * This is true if the Condition containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean conditionESet;

	/**
	 * The cached value of the '{@link #getThenPart() <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getThenPart()
	 * @generated
	 * @ordered
	 */
	protected MThen thenPart;

	/**
	 * This is true if the Then Part containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean thenPartESet;

	/**
	 * The cached value of the '{@link #getElseifPart() <em>Elseif Part</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElseifPart()
	 * @generated
	 * @ordered
	 */
	protected EList<MElseIf> elseifPart;

	/**
	 * The cached value of the '{@link #getElsePart() <em>Else Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElsePart()
	 * @generated
	 * @ordered
	 */
	protected MElse elsePart;

	/**
	 * This is true if the Else Part containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean elsePartESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractIfImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MABSTRACT_IF;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication getCondition() {
		if (condition != null && condition.eIsProxy()) {
			InternalEObject oldCondition = (InternalEObject) condition;
			condition = (MChainOrApplication) eResolveProxy(oldCondition);
			if (condition != oldCondition) {
				InternalEObject newCondition = (InternalEObject) condition;
				NotificationChain msgs = oldCondition.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__CONDITION,
						null, null);
				if (newCondition.eInternalContainer() == null) {
					msgs = newCondition.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MABSTRACT_IF__CONDITION,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MABSTRACT_IF__CONDITION,
							oldCondition, condition));
			}
		}
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainOrApplication basicGetCondition() {
		return condition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCondition(MChainOrApplication newCondition,
			NotificationChain msgs) {
		MChainOrApplication oldCondition = condition;
		condition = newCondition;
		boolean oldConditionESet = conditionESet;
		conditionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MABSTRACT_IF__CONDITION, oldCondition,
					newCondition, !oldConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCondition(MChainOrApplication newCondition) {
		if (newCondition != condition) {
			NotificationChain msgs = null;
			if (condition != null)
				msgs = ((InternalEObject) condition).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__CONDITION,
						null, msgs);
			if (newCondition != null)
				msgs = ((InternalEObject) newCondition).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__CONDITION,
						null, msgs);
			msgs = basicSetCondition(newCondition, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldConditionESet = conditionESet;
			conditionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MABSTRACT_IF__CONDITION,
						newCondition, newCondition, !oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetCondition(NotificationChain msgs) {
		MChainOrApplication oldCondition = condition;
		condition = null;
		boolean oldConditionESet = conditionESet;
		conditionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MABSTRACT_IF__CONDITION, oldCondition,
					null, oldConditionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCondition() {
		if (condition != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) condition).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MABSTRACT_IF__CONDITION,
					null, msgs);
			msgs = basicUnsetCondition(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldConditionESet = conditionESet;
			conditionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MABSTRACT_IF__CONDITION, null, null,
						oldConditionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCondition() {
		return conditionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MThen getThenPart() {
		if (thenPart != null && thenPart.eIsProxy()) {
			InternalEObject oldThenPart = (InternalEObject) thenPart;
			thenPart = (MThen) eResolveProxy(oldThenPart);
			if (thenPart != oldThenPart) {
				InternalEObject newThenPart = (InternalEObject) thenPart;
				NotificationChain msgs = oldThenPart.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__THEN_PART,
						null, null);
				if (newThenPart.eInternalContainer() == null) {
					msgs = newThenPart.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MABSTRACT_IF__THEN_PART,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MABSTRACT_IF__THEN_PART,
							oldThenPart, thenPart));
			}
		}
		return thenPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MThen basicGetThenPart() {
		return thenPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetThenPart(MThen newThenPart,
			NotificationChain msgs) {
		MThen oldThenPart = thenPart;
		thenPart = newThenPart;
		boolean oldThenPartESet = thenPartESet;
		thenPartESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MABSTRACT_IF__THEN_PART, oldThenPart,
					newThenPart, !oldThenPartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThenPart(MThen newThenPart) {
		if (newThenPart != thenPart) {
			NotificationChain msgs = null;
			if (thenPart != null)
				msgs = ((InternalEObject) thenPart).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__THEN_PART,
						null, msgs);
			if (newThenPart != null)
				msgs = ((InternalEObject) newThenPart).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__THEN_PART,
						null, msgs);
			msgs = basicSetThenPart(newThenPart, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldThenPartESet = thenPartESet;
			thenPartESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MABSTRACT_IF__THEN_PART, newThenPart,
						newThenPart, !oldThenPartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetThenPart(NotificationChain msgs) {
		MThen oldThenPart = thenPart;
		thenPart = null;
		boolean oldThenPartESet = thenPartESet;
		thenPartESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MABSTRACT_IF__THEN_PART, oldThenPart,
					null, oldThenPartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetThenPart() {
		if (thenPart != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) thenPart).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MABSTRACT_IF__THEN_PART,
					null, msgs);
			msgs = basicUnsetThenPart(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldThenPartESet = thenPartESet;
			thenPartESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MABSTRACT_IF__THEN_PART, null, null,
						oldThenPartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetThenPart() {
		return thenPartESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MElseIf> getElseifPart() {
		if (elseifPart == null) {
			elseifPart = new EObjectContainmentEList.Unsettable.Resolving<MElseIf>(
					MElseIf.class, this,
					ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART);
		}
		return elseifPart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElseifPart() {
		if (elseifPart != null)
			((InternalEList.Unsettable<?>) elseifPart).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElseifPart() {
		return elseifPart != null
				&& ((InternalEList.Unsettable<?>) elseifPart).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MElse getElsePart() {
		if (elsePart != null && elsePart.eIsProxy()) {
			InternalEObject oldElsePart = (InternalEObject) elsePart;
			elsePart = (MElse) eResolveProxy(oldElsePart);
			if (elsePart != oldElsePart) {
				InternalEObject newElsePart = (InternalEObject) elsePart;
				NotificationChain msgs = oldElsePart.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
						null, null);
				if (newElsePart.eInternalContainer() == null) {
					msgs = newElsePart.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
							oldElsePart, elsePart));
			}
		}
		return elsePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MElse basicGetElsePart() {
		return elsePart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetElsePart(MElse newElsePart,
			NotificationChain msgs) {
		MElse oldElsePart = elsePart;
		elsePart = newElsePart;
		boolean oldElsePartESet = elsePartESet;
		elsePartESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MABSTRACT_IF__ELSE_PART, oldElsePart,
					newElsePart, !oldElsePartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElsePart(MElse newElsePart) {
		if (newElsePart != elsePart) {
			NotificationChain msgs = null;
			if (elsePart != null)
				msgs = ((InternalEObject) elsePart).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
						null, msgs);
			if (newElsePart != null)
				msgs = ((InternalEObject) newElsePart).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
						null, msgs);
			msgs = basicSetElsePart(newElsePart, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldElsePartESet = elsePartESet;
			elsePartESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MABSTRACT_IF__ELSE_PART, newElsePart,
						newElsePart, !oldElsePartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetElsePart(NotificationChain msgs) {
		MElse oldElsePart = elsePart;
		elsePart = null;
		boolean oldElsePartESet = elsePartESet;
		elsePartESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MABSTRACT_IF__ELSE_PART, oldElsePart,
					null, oldElsePartESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElsePart() {
		if (elsePart != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) elsePart).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MABSTRACT_IF__ELSE_PART,
					null, msgs);
			msgs = basicUnsetElsePart(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldElsePartESet = elsePartESet;
			elsePartESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MABSTRACT_IF__ELSE_PART, null, null,
						oldElsePartESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElsePart() {
		return elsePartESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_IF__CONDITION:
			return basicUnsetCondition(msgs);
		case ExpressionsPackage.MABSTRACT_IF__THEN_PART:
			return basicUnsetThenPart(msgs);
		case ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART:
			return ((InternalEList<?>) getElseifPart()).basicRemove(otherEnd,
					msgs);
		case ExpressionsPackage.MABSTRACT_IF__ELSE_PART:
			return basicUnsetElsePart(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_IF__CONDITION:
			if (resolve)
				return getCondition();
			return basicGetCondition();
		case ExpressionsPackage.MABSTRACT_IF__THEN_PART:
			if (resolve)
				return getThenPart();
			return basicGetThenPart();
		case ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART:
			return getElseifPart();
		case ExpressionsPackage.MABSTRACT_IF__ELSE_PART:
			if (resolve)
				return getElsePart();
			return basicGetElsePart();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_IF__CONDITION:
			setCondition((MChainOrApplication) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_IF__THEN_PART:
			setThenPart((MThen) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART:
			getElseifPart().clear();
			getElseifPart().addAll((Collection<? extends MElseIf>) newValue);
			return;
		case ExpressionsPackage.MABSTRACT_IF__ELSE_PART:
			setElsePart((MElse) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_IF__CONDITION:
			unsetCondition();
			return;
		case ExpressionsPackage.MABSTRACT_IF__THEN_PART:
			unsetThenPart();
			return;
		case ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART:
			unsetElseifPart();
			return;
		case ExpressionsPackage.MABSTRACT_IF__ELSE_PART:
			unsetElsePart();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_IF__CONDITION:
			return isSetCondition();
		case ExpressionsPackage.MABSTRACT_IF__THEN_PART:
			return isSetThenPart();
		case ExpressionsPackage.MABSTRACT_IF__ELSEIF_PART:
			return isSetElseifPart();
		case ExpressionsPackage.MABSTRACT_IF__ELSE_PART:
			return isSetElsePart();
		}
		return super.eIsSet(featureID);
	}

} //MAbstractIfImpl
