/**
 */
package com.montages.mcore.expressions.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractChain;
import com.montages.mcore.expressions.MBaseChain;
import com.montages.mcore.expressions.MCallArgument;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MProcessor;
import com.montages.mcore.expressions.MProcessorDefinition;
import com.montages.mcore.expressions.MSubChain;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MBase Chain</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainEntryType <em>Chain Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainAsCode <em>Chain As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement1 <em>Element1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement1Correct <em>Element1 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement2EntryType <em>Element2 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement2 <em>Element2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement2Correct <em>Element2 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement3EntryType <em>Element3 Entry Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement3 <em>Element3</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getElement3Correct <em>Element3 Correct</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getCastType <em>Cast Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getLastElement <em>Last Element</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainCalculatedType <em>Chain Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainCalculatedSimpleType <em>Chain Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainCalculatedSingular <em>Chain Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getProcessor <em>Processor</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getProcessorDefinition <em>Processor Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getTypeMismatch <em>Type Mismatch</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getCallArgument <em>Call Argument</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getSubExpression <em>Sub Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getContainedCollector <em>Contained Collector</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getChainCodeforSubchains <em>Chain Codefor Subchains</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MBaseChainImpl#getIsOwnXOCLOp <em>Is Own XOCL Op</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MBaseChainImpl extends MAbstractExpressionWithBaseImpl
		implements MBaseChain, IXoclInitializable {
	/**
	 * The default value of the '{@link #getChainAsCode() <em>Chain As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAIN_AS_CODE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForOthers <em>As Code For Others</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForOthers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForOthersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #asCodeForVariables <em>As Code For Variables</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #asCodeForVariables
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression asCodeForVariablesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorDefinition$Update <em>Processor Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorDefinition$UpdateexpressionsMProcessorDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #autoCastWithProc <em>Auto Cast With Proc</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #autoCastWithProc
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression autoCastWithProcBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getBaseAsCode <em>Base As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBaseAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression baseAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getCastType <em>Cast Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCastType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression castTypeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainAsCode <em>Chain As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSimpleType <em>Chain Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCalculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedSingular <em>Chain Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCalculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCalculatedType <em>Chain Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainCalculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainCodeforSubchains <em>Chain Codefor Subchains</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCodeforSubchains
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression chainCodeforSubchainsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getChainEntryType <em>Chain Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainEntryType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression chainEntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength1 <em>Code For Length1</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength1
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength1BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength2 <em>Code For Length2</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength2
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength2BodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #codeForLength3 <em>Code For Length3</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #codeForLength3
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression codeForLength3BodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #createProcessorDefinition <em>Create Processor Definition</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #createProcessorDefinition
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression createProcessorDefinitionBodyOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement1 <em>Element1</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1
	 * @templateTag DFGFI08
	 * @generated
	 */
	private static OCLExpression element1ChoiceConstructionOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement1Correct <em>Element1 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression element1CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement2 <em>Element2</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression element2ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement2Correct <em>Element2 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression element2CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement2EntryType <em>Element2 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element2EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getElement3 <em>Element3</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression element3ChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement3Correct <em>Element3 Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression element3CorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getElement3EntryType <em>Element3 Entry Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3EntryType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression element3EntryTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isCustomCodeProcessor <em>Is Custom Code Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isCustomCodeProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isCustomCodeProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsOwnXOCLOp <em>Is Own XOCL Op</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsOwnXOCLOp
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOpDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isOwnXOCLOperator <em>Is Own XOCL Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isOwnXOCLOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isOwnXOCLOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isPostfixProcessor <em>Is Postfix Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPostfixProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isPostfixProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isPrefixProcessor <em>Is Prefix Processor</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPrefixProcessor
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isPrefixProcessorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isProcessorCheckEqualOperator <em>Is Processor Check Equal Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProcessorCheckEqualOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isProcessorCheckEqualOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #isProcessorSetOperator <em>Is Processor Set Operator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isProcessorSetOperator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression isProcessorSetOperatorBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLastElement <em>Last Element</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastElement
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression lastElementDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #length <em>Length</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #length
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression lengthBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ownToApplyMismatch <em>Own To Apply Mismatch</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ownToApplyMismatch
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ownToApplyMismatchBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procAsCode <em>Proc As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procAsCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBoolean <em>Proc Def Choices For Boolean</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBoolean
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleanBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForBooleans <em>Proc Def Choices For Booleans</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForBooleans
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForBooleansBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDate <em>Proc Def Choices For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDateBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForDates <em>Proc Def Choices For Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForDates
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForDatesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForInteger <em>Proc Def Choices For Integer</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForInteger
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegerBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForIntegers <em>Proc Def Choices For Integers</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForIntegers
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForIntegersBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObject <em>Proc Def Choices For Object</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObject
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForObjects <em>Proc Def Choices For Objects</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForObjects
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForObjectsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReal <em>Proc Def Choices For Real</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReal
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForReals <em>Proc Def Choices For Reals</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForReals
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForRealsBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForString <em>Proc Def Choices For String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #procDefChoicesForStrings <em>Proc Def Choices For Strings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #procDefChoicesForStrings
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression procDefChoicesForStringsBodyOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression processorDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getProcessorDefinition <em>Processor Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessorDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression processorDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorIsSet <em>Processor Is Set</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorIsSet
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorIsSetBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #processorReturnsSingular <em>Processor Returns Singular</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #processorReturnsSingular
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression processorReturnsSingularBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTypeMismatch <em>Type Mismatch</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMismatch
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression typeMismatchDeriveOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #uniqueChainNumber <em>Unique Chain Number</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #uniqueChainNumber
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression uniqueChainNumberBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #reuseFromOtherNoMoreUsedChain <em>Reuse From Other No More Used Chain</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #reuseFromOtherNoMoreUsedChain
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression reuseFromOtherNoMoreUsedChainexpressionsMBaseChainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #resetToBase <em>Reset To Base</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #resetToBase
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression resetToBaseexpressionsExpressionBasemcoreMVariableBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #reuseFromOtherNoMoreUsedChainAsUpdate <em>Reuse From Other No More Used Chain As Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #reuseFromOtherNoMoreUsedChainAsUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression reuseFromOtherNoMoreUsedChainAsUpdateexpressionsMBaseChainBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainAsCode <em>Unsafe Chain As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeChainStepAsCode <em>Unsafe Chain Step As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeChainStepAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #unsafeElementAsCode <em>Unsafe Element As Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #unsafeElementAsCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression unsafeElementAsCodeecoreEIntegerObjectBodyOCL;

	/**
	 * The cached value of the '{@link #getElement1() <em>Element1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1()
	 * @generated
	 * @ordered
	 */
	protected MProperty element1;

	/**
	 * This is true if the Element1 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element1ESet;

	/**
	 * The default value of the '{@link #getElement1Correct() <em>Element1 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement1Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT1_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElement2() <em>Element2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2()
	 * @generated
	 * @ordered
	 */
	protected MProperty element2;

	/**
	 * This is true if the Element2 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element2ESet;

	/**
	 * The default value of the '{@link #getElement2Correct() <em>Element2 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement2Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT2_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getElement3() <em>Element3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3()
	 * @generated
	 * @ordered
	 */
	protected MProperty element3;

	/**
	 * This is true if the Element3 reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean element3ESet;

	/**
	 * The default value of the '{@link #getElement3Correct() <em>Element3 Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getElement3Correct()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ELEMENT3_CORRECT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCastType() <em>Cast Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCastType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier castType;

	/**
	 * This is true if the Cast Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean castTypeESet;

	/**
	 * The default value of the '{@link #getChainCalculatedSimpleType() <em>Chain Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getChainCalculatedSingular() <em>Chain Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CHAIN_CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected static final MProcessor PROCESSOR_EDEFAULT = MProcessor.NONE;

	/**
	 * The cached value of the '{@link #getProcessor() <em>Processor</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProcessor()
	 * @generated
	 * @ordered
	 */
	protected MProcessor processor = PROCESSOR_EDEFAULT;

	/**
	 * This is true if the Processor attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean processorESet;

	/**
	 * The default value of the '{@link #getTypeMismatch() <em>Type Mismatch</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeMismatch()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean TYPE_MISMATCH_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCallArgument() <em>Call Argument</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCallArgument()
	 * @generated
	 * @ordered
	 */
	protected EList<MCallArgument> callArgument;

	/**
	 * The cached value of the '{@link #getSubExpression() <em>Sub Expression</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSubExpression()
	 * @generated
	 * @ordered
	 */
	protected EList<MSubChain> subExpression;

	/**
	 * The cached value of the '{@link #getContainedCollector() <em>Contained Collector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainedCollector()
	 * @generated
	 * @ordered
	 */
	protected MCollectionExpression containedCollector;

	/**
	 * This is true if the Contained Collector containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean containedCollectorESet;

	/**
	 * The default value of the '{@link #getChainCodeforSubchains() <em>Chain Codefor Subchains</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChainCodeforSubchains()
	 * @generated
	 * @ordered
	 */
	protected static final String CHAIN_CODEFOR_SUBCHAINS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsOwnXOCLOp() <em>Is Own XOCL Op</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsOwnXOCLOp()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_OWN_XOCL_OP_EDEFAULT = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MBaseChainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MBASE_CHAIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_ENTRY_TYPE:
			if (resolve)
				return getChainEntryType();
			return basicGetChainEntryType();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE:
			return getChainAsCode();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1:
			if (resolve)
				return getElement1();
			return basicGetElement1();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT:
			return getElement1Correct();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_ENTRY_TYPE:
			if (resolve)
				return getElement2EntryType();
			return basicGetElement2EntryType();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2:
			if (resolve)
				return getElement2();
			return basicGetElement2();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT:
			return getElement2Correct();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_ENTRY_TYPE:
			if (resolve)
				return getElement3EntryType();
			return basicGetElement3EntryType();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3:
			if (resolve)
				return getElement3();
			return basicGetElement3();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT:
			return getElement3Correct();
		case ExpressionsPackage.MBASE_CHAIN__CAST_TYPE:
			if (resolve)
				return getCastType();
			return basicGetCastType();
		case ExpressionsPackage.MBASE_CHAIN__LAST_ELEMENT:
			if (resolve)
				return getLastElement();
			return basicGetLastElement();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_TYPE:
			if (resolve)
				return getChainCalculatedType();
			return basicGetChainCalculatedType();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
			return getChainCalculatedSimpleType();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR:
			return getChainCalculatedSingular();
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
			return getProcessor();
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION:
			if (resolve)
				return getProcessorDefinition();
			return basicGetProcessorDefinition();
		case ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH:
			return getTypeMismatch();
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			return getCallArgument();
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
			return getSubExpression();
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			if (resolve)
				return getContainedCollector();
			return basicGetContainedCollector();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS:
			return getChainCodeforSubchains();
		case ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP:
			return getIsOwnXOCLOp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1:
			setElement1((MProperty) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2:
			setElement2((MProperty) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3:
			setElement3((MProperty) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__CAST_TYPE:
			setCastType((MClassifier) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
			setProcessor((MProcessor) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION:
			setProcessorDefinition((MProcessorDefinition) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			getCallArgument().clear();
			getCallArgument()
					.addAll((Collection<? extends MCallArgument>) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
			getSubExpression().clear();
			getSubExpression()
					.addAll((Collection<? extends MSubChain>) newValue);
			return;
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			setContainedCollector((MCollectionExpression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1:
			unsetElement1();
			return;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2:
			unsetElement2();
			return;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3:
			unsetElement3();
			return;
		case ExpressionsPackage.MBASE_CHAIN__CAST_TYPE:
			unsetCastType();
			return;
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
			unsetProcessor();
			return;
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION:
			setProcessorDefinition((MProcessorDefinition) null);
			return;
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			unsetCallArgument();
			return;
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
			unsetSubExpression();
			return;
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			unsetContainedCollector();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_ENTRY_TYPE:
			return basicGetChainEntryType() != null;
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE:
			return CHAIN_AS_CODE_EDEFAULT == null ? getChainAsCode() != null
					: !CHAIN_AS_CODE_EDEFAULT.equals(getChainAsCode());
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1:
			return isSetElement1();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT:
			return ELEMENT1_CORRECT_EDEFAULT == null
					? getElement1Correct() != null
					: !ELEMENT1_CORRECT_EDEFAULT.equals(getElement1Correct());
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_ENTRY_TYPE:
			return basicGetElement2EntryType() != null;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2:
			return isSetElement2();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT:
			return ELEMENT2_CORRECT_EDEFAULT == null
					? getElement2Correct() != null
					: !ELEMENT2_CORRECT_EDEFAULT.equals(getElement2Correct());
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_ENTRY_TYPE:
			return basicGetElement3EntryType() != null;
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3:
			return isSetElement3();
		case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT:
			return ELEMENT3_CORRECT_EDEFAULT == null
					? getElement3Correct() != null
					: !ELEMENT3_CORRECT_EDEFAULT.equals(getElement3Correct());
		case ExpressionsPackage.MBASE_CHAIN__CAST_TYPE:
			return isSetCastType();
		case ExpressionsPackage.MBASE_CHAIN__LAST_ELEMENT:
			return basicGetLastElement() != null;
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_TYPE:
			return basicGetChainCalculatedType() != null;
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
			return getChainCalculatedSimpleType() != CHAIN_CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR:
			return CHAIN_CALCULATED_SINGULAR_EDEFAULT == null
					? getChainCalculatedSingular() != null
					: !CHAIN_CALCULATED_SINGULAR_EDEFAULT
							.equals(getChainCalculatedSingular());
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
			return isSetProcessor();
		case ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION:
			return basicGetProcessorDefinition() != null;
		case ExpressionsPackage.MBASE_CHAIN__TYPE_MISMATCH:
			return TYPE_MISMATCH_EDEFAULT == null ? getTypeMismatch() != null
					: !TYPE_MISMATCH_EDEFAULT.equals(getTypeMismatch());
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			return isSetCallArgument();
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
			return isSetSubExpression();
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			return isSetContainedCollector();
		case ExpressionsPackage.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS:
			return CHAIN_CODEFOR_SUBCHAINS_EDEFAULT == null
					? getChainCodeforSubchains() != null
					: !CHAIN_CODEFOR_SUBCHAINS_EDEFAULT
							.equals(getChainCodeforSubchains());
		case ExpressionsPackage.MBASE_CHAIN__IS_OWN_XOCL_OP:
			return IS_OWN_XOCL_OP_EDEFAULT == null ? getIsOwnXOCLOp() != null
					: !IS_OWN_XOCL_OP_EDEFAULT.equals(getIsOwnXOCLOp());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (derivedFeatureID) {
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT1:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT2:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_ENTRY_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT3:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3;
			case ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT:
				return ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT;
			case ExpressionsPackage.MBASE_CHAIN__CAST_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CAST_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__LAST_ELEMENT:
				return ExpressionsPackage.MABSTRACT_CHAIN__LAST_ELEMENT;
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE;
			case ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR:
				return ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;
			case ExpressionsPackage.MBASE_CHAIN__PROCESSOR:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR;
			case ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION:
				return ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (baseFeatureID) {
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_AS_CODE:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT1;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT1_CORRECT:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT1_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT2_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT2;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT2_CORRECT:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT2_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT3_ENTRY_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT3;
			case ExpressionsPackage.MABSTRACT_CHAIN__ELEMENT3_CORRECT:
				return ExpressionsPackage.MBASE_CHAIN__ELEMENT3_CORRECT;
			case ExpressionsPackage.MABSTRACT_CHAIN__CAST_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__CAST_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__LAST_ELEMENT:
				return ExpressionsPackage.MBASE_CHAIN__LAST_ELEMENT;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE;
			case ExpressionsPackage.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR:
				return ExpressionsPackage.MBASE_CHAIN__CHAIN_CALCULATED_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR:
				return ExpressionsPackage.MBASE_CHAIN__PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN__PROCESSOR_DEFINITION:
				return ExpressionsPackage.MBASE_CHAIN__PROCESSOR_DEFINITION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MAbstractChain.class) {
			switch (baseOperationID) {
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION:
				return ExpressionsPackage.MBASE_CHAIN___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION;
			case ExpressionsPackage.MABSTRACT_CHAIN___LENGTH:
				return ExpressionsPackage.MBASE_CHAIN___LENGTH;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
				return ExpressionsPackage.MBASE_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
				return ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER:
				return ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
				return ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___AS_CODE_FOR_OTHERS:
				return ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_OTHERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH1:
				return ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH1;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH2:
				return ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH2;
			case ExpressionsPackage.MABSTRACT_CHAIN___CODE_FOR_LENGTH3:
				return ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH3;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_AS_CODE:
				return ExpressionsPackage.MBASE_CHAIN___PROC_AS_CODE;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_CUSTOM_CODE_PROCESSOR:
				return ExpressionsPackage.MBASE_CHAIN___IS_CUSTOM_CODE_PROCESSOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_PROCESSOR_SET_OPERATOR:
				return ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_SET_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___IS_OWN_XOCL_OPERATOR:
				return ExpressionsPackage.MBASE_CHAIN___IS_OWN_XOCL_OPERATOR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_RETURNS_SINGULAR:
				return ExpressionsPackage.MBASE_CHAIN___PROCESSOR_RETURNS_SINGULAR;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROCESSOR_IS_SET:
				return ExpressionsPackage.MBASE_CHAIN___PROCESSOR_IS_SET;
			case ExpressionsPackage.MABSTRACT_CHAIN___CREATE_PROCESSOR_DEFINITION:
				return ExpressionsPackage.MBASE_CHAIN___CREATE_PROCESSOR_DEFINITION;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REAL:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REAL;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REALS:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REALS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRING:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRING;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATE:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATE;
			case ExpressionsPackage.MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATES:
				return ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATES;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MBASE_CHAIN___AUTO_CAST_WITH_PROC:
			return autoCastWithProc();
		case ExpressionsPackage.MBASE_CHAIN___OWN_TO_APPLY_MISMATCH:
			return ownToApplyMismatch();
		case ExpressionsPackage.MBASE_CHAIN___UNIQUE_CHAIN_NUMBER:
			return uniqueChainNumber();
		case ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN:
			return reuseFromOtherNoMoreUsedChain((MBaseChain) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___RESET_TO_BASE__EXPRESSIONBASE_MVARIABLE:
			return resetToBase((ExpressionBase) arguments.get(0),
					(MVariable) arguments.get(1));
		case ExpressionsPackage.MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN:
			return reuseFromOtherNoMoreUsedChainAsUpdate(
					(MBaseChain) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_CHECK_EQUAL_OPERATOR:
			return isProcessorCheckEqualOperator();
		case ExpressionsPackage.MBASE_CHAIN___IS_PREFIX_PROCESSOR:
			return isPrefixProcessor();
		case ExpressionsPackage.MBASE_CHAIN___IS_POSTFIX_PROCESSOR:
			return isPostfixProcessor();
		case ExpressionsPackage.MBASE_CHAIN___AS_CODE_FOR_OTHERS:
			return asCodeForOthers();
		case ExpressionsPackage.MBASE_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER:
			return unsafeElementAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH1:
			return codeForLength1();
		case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH2:
			return codeForLength2();
		case ExpressionsPackage.MBASE_CHAIN___CODE_FOR_LENGTH3:
			return codeForLength3();
		case ExpressionsPackage.MBASE_CHAIN___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION:
			return processorDefinition$Update(
					(MProcessorDefinition) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___LENGTH:
			return length();
		case ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER:
			return unsafeChainStepAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0));
		case ExpressionsPackage.MBASE_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER:
			return unsafeChainAsCode((Integer) arguments.get(0),
					(Integer) arguments.get(1));
		case ExpressionsPackage.MBASE_CHAIN___PROC_AS_CODE:
			return procAsCode();
		case ExpressionsPackage.MBASE_CHAIN___IS_CUSTOM_CODE_PROCESSOR:
			return isCustomCodeProcessor();
		case ExpressionsPackage.MBASE_CHAIN___IS_PROCESSOR_SET_OPERATOR:
			return isProcessorSetOperator();
		case ExpressionsPackage.MBASE_CHAIN___IS_OWN_XOCL_OPERATOR:
			return isOwnXOCLOperator();
		case ExpressionsPackage.MBASE_CHAIN___PROCESSOR_RETURNS_SINGULAR:
			return processorReturnsSingular();
		case ExpressionsPackage.MBASE_CHAIN___PROCESSOR_IS_SET:
			return processorIsSet();
		case ExpressionsPackage.MBASE_CHAIN___CREATE_PROCESSOR_DEFINITION:
			return createProcessorDefinition();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT:
			return procDefChoicesForObject();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS:
			return procDefChoicesForObjects();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN:
			return procDefChoicesForBoolean();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS:
			return procDefChoicesForBooleans();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER:
			return procDefChoicesForInteger();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS:
			return procDefChoicesForIntegers();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REAL:
			return procDefChoicesForReal();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_REALS:
			return procDefChoicesForReals();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRING:
			return procDefChoicesForString();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS:
			return procDefChoicesForStrings();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATE:
			return procDefChoicesForDate();
		case ExpressionsPackage.MBASE_CHAIN___PROC_DEF_CHOICES_FOR_DATES:
			return procDefChoicesForDates();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (processor: ");
		if (processorESet)
			result.append(processor);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForOthers() {

		/**
		 * @OCL if length()=0 then baseAsCode
		else if length()=1 then codeForLength1() 
		else if length()=2 then codeForLength2() 
		else if length()=3 then codeForLength3() 
		else 'ERROR' endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(9);
		if (asCodeForOthersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForOthersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForOthersBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String asCodeForVariables() {

		/**
		 * @OCL let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in
		let vName: String = v.namedExpression.eName in
		
		if v.calculatedSingular and (length() > 0)
		then 
		'if '.concat(vName).concat(' = null\n  then null\n  else ').concat(asCodeForOthers()).concat(' endif')
		else asCodeForOthers()
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(14);
		if (asCodeForVariablesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				asCodeForVariablesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeForVariablesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate processorDefinition$Update(MProcessorDefinition trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.expressions.MProcessorDefinition triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE
						.getMAbstractChain_ProcessorDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String autoCastWithProc() {

		/**
		 * @OCL  let code: String = if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) 
		then asCodeForBuiltIn()
		else if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) then asCodeForConstants()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) then asCodeForVariables()
		else asCodeForOthers() endif endif endif in
		let apply: mcore::expressions::MApplication= if self.eContainer().oclIsTypeOf(mcore::expressions::MApplication ) then self.eContainer().oclAsType(mcore::expressions::MApplication) else null endif in
		let chainTypeString: String =typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in
		let chainTypeStringSingular: String = typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in
		
		--let castType: MClassifier =  if apply.oclIsUndefined() then self.expectedReturnType else if self.castType.oclIsUndefined() then apply.operands->first().calculatedOwnType else self.castType endif endif
		let castType: mcore::MClassifier =  if self.castType.oclIsUndefined() then (if apply.oclIsUndefined() then self.expectedReturnType else apply.operands->first().calculatedOwnType  endif) else self.castType endif  -- CastType has to be preferred to autocast
		
		in
		let castTypeString: String = typeAsOcl(selfObjectPackage, castType, SimpleType::None, chainCalculatedSingular) in
		let castTypeStringSingular: String = typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in
		
		let opChangesReturn : Boolean = apply.operands->first().calculatedSimpleType <> apply.calculatedSimpleType or apply.operands->first().calculatedType <> apply.calculatedType
		in
		    let variableName : String = self.uniqueChainNumber() in  
		    
		    -- chain name
		    let chainName: String = 
		    if self.processor= mcore::expressions::MProcessor::None then '' else
		'let '.concat(variableName).concat(': ').concat(if self.chainCalculatedSingular then castTypeStringSingular else castTypeString endif).concat(' = ') endif
		in
		-- chainname end	
		
		if  
		
		not(self.castType.oclIsUndefined()) or 
		--new
		(if (not(apply.oclIsUndefined()) and (apply.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean  or (apply.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)))then 
		if apply.operands->first().calculatedOwnType.oclIsUndefined() then false
		else    apply.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and apply.operands->first().calculatedOwnType<> self.calculatedOwnType
		endif else false endif)
		--new     
		or
		
		( if apply.oclIsUndefined() then self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType               --   check is dont have an apply but chain only    :::::::.changed OwnType for Collection
		else ((apply.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and apply.calculatedOwnSimpleType= mcore::SimpleType::None endif)      --check if Applytype differs from chain and type is a Classifier
		
		then
		chainName.concat('let chain: ').concat(chainTypeString).concat(' = ').concat(code).concat(' in\n').concat(
		  if chainCalculatedSingular then 'if chain.oclIsUndefined()'.concat('\n').concat('  then null\n  else ') else '' endif
		).concat(
		  if chainCalculatedSingular
		    then
		      'if chain.oclIsKindOf('.concat(castTypeStringSingular).concat(')\n    then chain.oclAsType(').concat(castTypeStringSingular).concat(')\n    else null\n  endif')
		    else 
		
		'chain->iterate(i:'.concat(chainTypeStringSingular).concat('; r: OrderedSet(').concat(castTypeStringSingular).concat(')=OrderedSet{} | if i.oclIsKindOf(').concat(castTypeStringSingular).concat(') then r->including(i.oclAsType(').concat(castTypeStringSingular).concat(')').concat(')->asOrderedSet() \n else r endif)')
		     endif
		 .concat(
		  if chainCalculatedSingular then '\n  endif' else '' endif
		).concat(if self.processor= MProcessor::None then '' else ' in\n'.concat(    -- Accessing feature is not unary
		
		'if ').concat(variableName).concat(if self.isOwnXOCLOperator() then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(if self.isProcessorSetOperator() then '->' else if not(self.isProcessorCheckEqualOperator()) then '.' else '' endif endif).concat(self.procAsCode())
		.concat(if self.isProcessorCheckEqualOperator() then ' then true else false ' 
		
		
		else (if processor=mcore::expressions::MProcessor::AsOrderedSet then '->' else '.' endif).concat('oclIsUndefined() \n then null \n else '  -- todo   processor does not influence calculatedSingular
		.concat(variableName).concat(
		  if self.isProcessorSetOperator() then '->' else '.' endif --  "->" for Set , "." for unary ,  nothing if we check for a value
		  ).concat(self.procAsCode())) endif) 
		  
		  .concat( 
		 '\n  endif' )  
		 endif))
		else
		
		let procString : String =  if self.processor <> mcore::expressions::MProcessor::None then
		--  let a: String =  '->iterate(i:'.concat(chainTypeStringSingular).concat('; r: String = \'\' | r.concat(i.toString()) )')  in
		'let '.concat(variableName).concat(': ').concat(chainTypeString).concat(' = ').concat(code).concat(' in\n').concat(    -- Accessing feature is not unary
		
		'if ').concat(variableName).concat(if self.isOwnXOCLOperator() then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(if self.isProcessorSetOperator() then '->' else if not(self.isProcessorCheckEqualOperator()) then '.' else '' endif endif).concat(self.procAsCode())
		.concat(if self.isProcessorCheckEqualOperator() then ' then true else false ' 
		
		
		else (if processor=mcore::expressions::MProcessor::AsOrderedSet then '->' else '.' endif).concat('oclIsUndefined() \n then null \n else '  -- todo   processor does not influence calculatedSingular
		.concat(variableName).concat(
		  if self.isProcessorSetOperator() then '->' else '.' endif --  "->" for Set , "." for unary ,  nothing if we check for a value
		  ).concat(self.procAsCode())) endif) 
		  
		  .concat( 
		 '\n  endif'  )  else code endif in
		 procString.concat
		 (if self.chainCalculatedSingular or self.processor <> mcore::expressions::MProcessor::None then '.toString()' else  '->iterate(i:'.concat(chainTypeStringSingular).concat('; r: String = \'\' | r.concat(i.toString()) )')
		  endif)
		
		 
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(0);
		if (autoCastWithProcBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				autoCastWithProcBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(autoCastWithProcBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnType if calculatedOwnSimpleType = mcore::SimpleType::None or calculatedOwnSimpleType = mcore::SimpleType::Object then
	if castType.oclIsUndefined() 
	then chainCalculatedType
	else castType
	endif
	else 
	null
	endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedType if collector.oclIsUndefined() and self.subExpression->isEmpty()
	then calculatedOwnType
	else if collector.oclIsUndefined()
	then subExpression->last().calculatedOwnType
	else collector.calculatedType
	endif
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCastType() {
		return castType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetChainCalculatedType() {
		/**
		 * @OCL let nl: mcore::MClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE;

		if (chainCalculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCalculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetChainEntryType() {
		/**
		 * @OCL let c:MClassifier=null in c
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE;

		if (chainEntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainEntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainEntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL collector containedCollector
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MCollectionExpression basicGetCollector() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MCollectionExpression) xoclEval
					.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetContainedCollector() {
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement1() {
		return element1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement2() {
		return element2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetElement2EntryType() {
		/**
		 * @OCL if not self.element1Correct then null
		else 
		if self.element1.oclIsUndefined() then null 
		else self.element1.type endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE;

		if (element2EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element2EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element2EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetElement3() {
		return element3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetElement3EntryType() {
		/**
		 * @OCL if not self.element2Correct then null
		else 
		if self.element2.oclIsUndefined() then null
		else self.element2.type endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE;

		if (element3EntryTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				element3EntryTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element3EntryTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetLastElement() {
		/**
		 * @OCL if not self.element3.oclIsUndefined() then self.element3 else
		if not self.element2.oclIsUndefined() then self.element2 else
		if not self.element1.oclIsUndefined() then self.element1 else
		null endif endif endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__LAST_ELEMENT;

		if (lastElementDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				lastElementDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(lastElementDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition basicGetProcessorDefinition() {
		/**
		 * @OCL if (let e0: Boolean = processor = mcore::expressions::MProcessor::None in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then null
		else createProcessorDefinition()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

		if (processorDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				processorDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(processorDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProcessorDefinition result = (MProcessorDefinition) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainedCollector(
			MCollectionExpression newContainedCollector,
			NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = newContainedCollector;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
					oldContainedCollector, newContainedCollector,
					!oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetContainedCollector(
			NotificationChain msgs) {
		MCollectionExpression oldContainedCollector = containedCollector;
		containedCollector = null;
		boolean oldContainedCollectorESet = containedCollectorESet;
		containedCollectorESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
					oldContainedCollector, null, oldContainedCollectorESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength1() {

		/**
		 * @OCL let chain:mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		
		let b: String = if (chain.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else chain.baseAsCode.concat('.') endif in 
		b.concat(unsafeChainStepAsCode(1)).concat(if chain.chainCalculatedSingular then '' else '->asOrderedSet()' endif)
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(11);
		if (codeForLength1BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength1BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength1BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength2() {

		/**
		 * @OCL 
		let chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		let abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in
		let b0: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else abstract.baseAsCode.concat('.') endif in 
		let b:String = if b0.oclIsUndefined() then 'PROBLEM WITH BASE' else b0 endif in
		if element1.calculatedSingular and element2.calculatedSingular then
		let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,2)).concat('\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,2).concat('\nendif'))
		
		else  if (not element1.calculatedSingular) and element2.calculatedSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else  if (not element1.calculatedSingular) and (not element2.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->asOrderedSet()')
		
		else null
		
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(12);
		if (codeForLength2BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength2BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength2BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String codeForLength3() {

		/**
		 * @OCL let chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in
		let abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in
		
		
		let b: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then '' else abstract.baseAsCode.concat('.') endif in 
		
		if element1.calculatedSingular and element2.calculatedSingular and element3.calculatedSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then null\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('\nendif')
		
		else if element1.calculatedSingular and element2.calculatedSingular and (not element3.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,2)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) and element3.calculatedSingular then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()\nendif')
		
		else if element1.calculatedSingular and (not element2.calculatedSingular) and (not element3.calculatedSingular) then
		'if '.concat(b).concat(unsafeChainAsCode(1,1)).concat('.oclIsUndefined()\n  then OrderedSet{}\n  else ').concat(b).concat(unsafeChainAsCode(1,3)).concat('->asOrderedSet()\nendif')
		
		else if (not element1.calculatedSingular) and element2.calculatedSingular and element3.calculatedSingular then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else if (not element1.calculatedSingular) and element2.calculatedSingular and (not element3.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,2)).concat('->reject(oclIsUndefined()).').concat(unsafeChainAsCode(3,3)).concat(if chain.subExpression->isEmpty() and not(self.processorIsSet()) then '' else '->asOrderedSet()' endif)
		
		else if (not element1.calculatedSingular) and (not element2.calculatedSingular) and element3.calculatedSingular then
		b.concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else if (not element1.calculatedSingular) and (not element2.calculatedSingular) and (not element3.calculatedSingular) then
		b.concat(unsafeChainAsCode(1,3)).concat('->reject(oclIsUndefined())->asOrderedSet()')
		
		else null
		endif endif endif endif endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(13);
		if (codeForLength3BodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				codeForLength3BodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(codeForLength3BodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition createProcessorDefinition() {

		/**
		 * @OCL Tuple{processor=processor}
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(16);
		if (createProcessorDefinitionBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				createProcessorDefinitionBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(createProcessorDefinitionBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProcessorDefinition) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Cast Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.kind = mcore::ClassifierKind::ClassType 
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalCastTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		if (castTypeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CAST_TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				castTypeChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"CastTypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(castTypeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "CastTypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element1</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL element1 
	let annotatedProp: mcore::MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	if self.chainEntryType.oclIsUndefined() then 
	Sequence{} else
	self.chainEntryType.allProperties()->excluding(if annotatedProp.oclIsUndefined() or (not self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.oclIsTypeOf(mcore::annotations::MResult)) then null else annotatedProp endif
	)->asSequence()endif
	
	--
	 * @templateTag INS06
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement1ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1;
		if (element1ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			//the actual class.... TODO: is this the right one
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(
							eOverrideStructuralFeature, eClass());

			try {
				element1ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						eOverrideStructuralFeature);
			}
		}
		Query query = OCL_ENV.createQuery(element1ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideStructuralFeature);
			query.getEvaluationEnvironment().add("choice", choice);
			return (ArrayList<MProperty>) query.evaluate(this);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element2</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	
	if element1.oclIsUndefined() 
	then OrderedSet{}
	else if element1.isOperation
	then OrderedSet{} 
	else if element2EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else element2EntryType.allProperties() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement2ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		if (element2ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				element2ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"Element2ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(element2ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "Element2ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Element3</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let annotatedProp: mcore::MProperty = 
	self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
	in
	
	if element2.oclIsUndefined() 
	then OrderedSet{}
	else if element2.isOperation
	then OrderedSet{} 
	else if element3EntryType.oclIsUndefined() 
	  then OrderedSet{}
	  else element3EntryType.allProperties() endif
	 endif
	endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalElement3ChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		if (element3ChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				element3ChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"Element3ChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(element3ChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "Element3ChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Processor Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProcessorDefinition>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 
	let s:SimpleType = self.chainCalculatedSimpleType in
	let t:MClassifier = self.chainCalculatedType in
	let res:OrderedSet(mcore::expressions::MProcessorDefinition) =
	if s = mcore::SimpleType::Boolean
	then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForBoolean()
	             else self.procDefChoicesForBooleans() endif
	else if s = mcore::SimpleType::Integer 
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForInteger()
	             else self.procDefChoicesForIntegers() endif
	else if   s = mcore::SimpleType::Double
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForReal()
	             else self.procDefChoicesForReals() endif
	else if   s = mcore::SimpleType::String
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForString()
	             else self.procDefChoicesForStrings() endif
	else if   s = mcore::SimpleType::Date
	 then if self.chainCalculatedSingular = true
	             then self.procDefChoicesForDate()
	             else self.procDefChoicesForDates() endif
	else if s = mcore::SimpleType::None or 
	      s = mcore::SimpleType::Annotation or 
	      s = mcore::SimpleType::Attribute or 
	      s = mcore::SimpleType::Class or 
	      s = mcore::SimpleType::Classifier or 
	      s = mcore::SimpleType::DataType or 
	      s = mcore::SimpleType::Enumeration or 
	      s = mcore::SimpleType::Feature or 
	      s = mcore::SimpleType::KeyValue or 
	      s = mcore::SimpleType::Literal or 
	      s = mcore::SimpleType::NamedElement or 
	      s = mcore::SimpleType::Object or 
	      s = mcore::SimpleType::Operation or 
	      s = mcore::SimpleType::Package or 
	      s = mcore::SimpleType::Parameter or 
	      s = mcore::SimpleType::Reference or 
	      s = mcore::SimpleType::TypedElement 
	 then if self.chainCalculatedSingular 
	             then self.procDefChoicesForObject()
	             else self.procDefChoicesForObjects()
	                    --OrderedSet{Tuple{processor=MProcessor::Head},
	                    --                   Tuple{processor=MProcessor::Tail}} 
	                    endif
	 else OrderedSet{} endif endif endif endif endif endif
	in res->prepend(null)
	                                       
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProcessorDefinition> evalProcessorDefinitionChoiceConstruction(
			List<MProcessorDefinition> choice) {
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		if (processorDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				processorDefinitionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"ProcessorDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV
				.createQuery(processorDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "ProcessorDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProcessorDefinition> result = new ArrayList<MProcessorDefinition>(
					(Collection<MProcessorDefinition>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode if (not self.subExpression->isEmpty()) 
	 then let mirrorOrder:OrderedSet(mcore::expressions::MSubChain) = self.subExpression->iterate( sub:mcore::expressions::MSubChain; d: OrderedSet(mcore::expressions::MSubChain) = OrderedSet{} | d->prepend(sub)) in
	          let subString:String = self.subExpression
	                    ->iterate(x:mcore::expressions::MSubChain ; s: String = '' | 
	                              let t: String = typeAsOcl( x.previousExpression.selfObjectPackage, x.previousExpression.calculatedOwnType, 
	                                                                     x.previousExpression.calculatedOwnSimpleType, x.previousExpression.calculatedOwnSingular ) in
	                              'let '.concat(x.uniqueSubchainName()).concat(' : ').concat(t).concat(' = ').concat(s) )  in 
	          let codeString:String =
	                  chainCodeforSubchains
	                  .concat(subExpression->iterate(x:mcore::expressions::MSubChain ; subStr: String ='' | subStr.concat(x.asCode))) in  
	          subString.concat(codeString)
	else chainCodeforSubchains endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL baseAsCode if (let e: Boolean = baseDefinition.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then ''
	else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)
	then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)
	else if baseDefinition.oclIsTypeOf(mcore::expressions::MContainerBaseDefinition)
	then baseDefinition.calculatedAsCode.concat(if not self.baseExitType.oclIsUndefined() then '.oclAsType('.concat(self.baseExitType.eName).concat(')')else '' endif)
	else baseDefinition.calculatedAsCode
	endif
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getBaseAsCode() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE;

		if (baseAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				baseAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(baseAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory let m1: Boolean = 
	if element1.oclIsUndefined() then true else element1.calculatedMandatory endif in
	let m2: Boolean =
	if element2.oclIsUndefined() then true else element2.calculatedMandatory endif in
	let m3: Boolean = 
	if element3.oclIsUndefined() then true else element3.calculatedMandatory endif in
	baseExitMandatory and m1 and m2 and m3
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSimpleType let p:mcore::expressions::MProcessor = processor in
	if p <> mcore::expressions::MProcessor::None
	then
	 if      p = mcore::expressions::MProcessor::ToInteger or
	         p = mcore::expressions::MProcessor::Size  or
	         p = mcore::expressions::MProcessor::Round or
	         p = mcore::expressions::MProcessor::Floor or
	         p = mcore::expressions::MProcessor::Year or
	         p = mcore::expressions::MProcessor::Minute or
	         p = mcore::expressions::MProcessor::Month or
	         p = mcore::expressions::MProcessor::Second or
	         p = mcore::expressions::MProcessor::Day or
	         p = mcore::expressions::MProcessor::Hour
	   then mcore::SimpleType::Integer
	 else if p = mcore::expressions::MProcessor::ToString or  
	         p = mcore::expressions::MProcessor::ToYyyyMmDd or
	         p = mcore::expressions::MProcessor::ToHhMm or
	         p = mcore::expressions::MProcessor::AllLowerCase or
	         p = mcore::expressions::MProcessor::AllUpperCase or
	         p = mcore::expressions::MProcessor::CamelCaseLower or
	         p = mcore::expressions::MProcessor::CamelCaseToBusiness or
	         p = mcore::expressions::MProcessor::CamelCaseUpper or
	         p = mcore::expressions::MProcessor::FirstUpperCase or
	         p = mcore::expressions::MProcessor::Trim
	   then mcore::SimpleType::String
	 else if p = mcore::expressions::MProcessor::ToReal or
	         p = mcore::expressions::MProcessor::OneDividedBy or
	         p = mcore::expressions::MProcessor::Sum
	   then mcore::SimpleType::Double
	 else if p = mcore::expressions::MProcessor::ToBoolean or
	         p = mcore::expressions::MProcessor::IsEmpty or
	         p = mcore::expressions::MProcessor::NotEmpty or
	         p = mcore::expressions::MProcessor::Not  or
	         p = mcore::expressions::MProcessor::IsFalse or
	         p = mcore::expressions::MProcessor::IsTrue or
	         p = mcore::expressions::MProcessor::IsZero or
	         p = mcore::expressions::MProcessor::IsOne or
	         p = mcore::expressions::MProcessor::NotNull or
	         p = mcore::expressions::MProcessor::IsNull or
	         p = mcore::expressions::MProcessor::IsInvalid or
	         p = mcore::expressions::MProcessor::And or
	         p= mcore::expressions::MProcessor::Or
	   then mcore::SimpleType::Boolean
	  else if p = mcore::expressions::MProcessor::ToDate 
	   then mcore::SimpleType::Date
	 else if self.lastElement=null
	   then self.baseExitSimpleType
	 else   self.lastElement.simpleType
	 endif endif endif endif endif endif
	   
	else if self.lastElement=null
	 then self.baseExitSimpleType
	else
	      self.lastElement.simpleType
	endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular if self.processorIsSet() 
	 then  self.processorReturnsSingular()
	 else self.chainCalculatedSingular endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSimpleType if self.collector.oclIsUndefined() and self.subExpression->isEmpty() 
	then calculatedOwnSimpleType
	else if collector.oclIsUndefined()
	then subExpression->last().calculatedOwnSimpleType
	else collector.calculatedSimpleType
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular if self.subExpression->isEmpty() and collector.oclIsUndefined()
	then calculatedOwnSingular
	else if collector.oclIsUndefined()
	then subExpression->last().calculatedOwnSingular
	else collector.calculatedSingular
	endif
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MCallArgument> getCallArgument() {
		if (callArgument == null) {
			callArgument = new EObjectContainmentWithInverseEList.Unsettable.Resolving<MCallArgument>(
					MCallArgument.class, this,
					ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT,
					ExpressionsPackage.MCALL_ARGUMENT__CALL);
		}
		return callArgument;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCastType() {
		if (castType != null && castType.eIsProxy()) {
			InternalEObject oldCastType = (InternalEObject) castType;
			castType = (MClassifier) eResolveProxy(oldCastType);
			if (castType != oldCastType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MBASE_CHAIN__CAST_TYPE,
							oldCastType, castType));
			}
		}
		return castType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChainAsCode() {
		/**
		 * @OCL unsafeChainAsCode(1)
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE;

		if (chainAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getChainCalculatedSimpleType() {
		/**
		 * @OCL let nl: mcore::SimpleType = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE;

		if (chainCalculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCalculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getChainCalculatedSingular() {
		/**
		 * @OCL let nl: Boolean = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR;

		if (chainCalculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				chainCalculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCalculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getChainCalculatedType() {
		MClassifier chainCalculatedType = basicGetChainCalculatedType();
		return chainCalculatedType != null && chainCalculatedType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) chainCalculatedType)
				: chainCalculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getChainCodeforSubchains() {
		/**
		 * @OCL let code: String = 
		if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) 
		then asCodeForBuiltIn()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MContainerBaseDefinition) 
		then asCodeForOthers()
		else if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) 
		then asCodeForConstants()
		else if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) 
		then asCodeForVariables()
		else asCodeForOthers() endif endif endif endif in
		let res: String = if calculatedSingular then 'null' else 'OrderedSet{}' endif in
		let chainTypeString: String = 
		typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in
		let chainTypeStringSingular: String = 
		typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in
		--if (castType.oclIsUndefined() or chainCalculatedType.oclIsUndefined())
		--then 
		if self.typeMismatch or not(self.castType.oclIsUndefined())-- and self.containedCollector.oclIsUndefined() and
		--(if self.castType.oclIsUndefined() then true else castType.allSubTypes()->excludes(chainCalculatedType)endif)
		then self.autoCastWithProc()
		else if self.processor=mcore::expressions::MProcessor::None 
		then code 
		else if self.isPrefixProcessor() or self.isPostfixProcessor() 
		then if self.processor = mcore::expressions::MProcessor::Not 
		      then let businessLog : String = ' '  in
		              businessLog.concat('if (').concat(code).concat(')= true \n then false \n else if (').concat(code).concat(')= false \n then true \n else null endif endif \n ')
		 else if self.processor = mcore::expressions::MProcessor::OneDividedBy 
		       then let businessLog : String = ' '  in
		               businessLog.concat('(1 / (').concat(code).concat('))')
		 else if self.isPostfixProcessor() 
		        then let businessLog : String = ' '  in
		                businessLog.concat(code).concat(self.procAsCode())
		        else code endif endif endif
		else let variableName : String = 
		 self.uniqueChainNumber() in
		'let '.concat(variableName).concat(': ').concat(chainTypeString).concat(' = ').concat(
		code).concat(' in\n').concat(    -- Accessing feature is not unary:
		'if ').concat(variableName).concat(
		if self.isOwnXOCLOperator() 
		     then '.oclIsUndefined() or '.concat(variableName) else ''endif).concat(
		if self.isProcessorSetOperator() 
		     then '->'
		else if not(self.isProcessorCheckEqualOperator()) 
		     then '.' else '' endif endif).concat(
		if self.isCustomCodeProcessor() 
		     then 'isEmpty()' 
		     else self.procAsCode() endif).concat
		(
		if self.isProcessorCheckEqualOperator() 
		     then ' then true else false ' 
		else           
		        if self.isCustomCodeProcessor()  
		             then '' 
		             else if not(self.processorReturnsSingular())  then '->' else '.' endif.concat(
		                    'oclIsUndefined() \n ') endif.concat( 
		        'then null \n else ').concat(-- todo   processor does not influence calculatedSingular
		        variableName).concat(
		        if self.isProcessorSetOperator() then '->' else '.' endif).concat
		        ( 
		        --  "->" for Set , "." for unary ,  nothing if we check for a value
		        if self.isCustomCodeProcessor() 
		           then let checkPart : String = if  processor= mcore::expressions::MProcessor::And then 'false' else 'true' endif in
		                   let elsePart : String = if processor= mcore::expressions::MProcessor::And then 'true' else 'false' endif in
		                   let iterateBase: String = 'iterate( x:'.concat(chainTypeStringSingular).concat('; s:') in
		                   if self.processor = mcore::expressions::MProcessor::And or processor = mcore::expressions::MProcessor::Or 
		                        then iterateBase.concat(chainTypeStringSingular).concat(
		                                '= ').concat(elsePart).concat(
		                               '|  if ( x) = ').concat(checkPart).concat( ' \n then ').concat(checkPart).concat('\n').concat(
		                                'else if (s)=').concat(checkPart).concat('\n').concat('then ').concat(checkPart).concat('\n').concat(
		                                ' else if x =null then null \n').concat('else if s =null then null').concat(
		                                ' else ').concat(elsePart).concat(' endif endif endif endif)')
		                       else iterateBase.concat(chainTypeString).concat('= ').concat('OrderedSet{} | if x.oclIsUndefined() then s else if x=').concat(
		                               --CHANGE ->first and ->last 
		                               --.concat(code).concat(if processor=MProcessor::Head then '->first() ' else '->last() ' endif).concat('then s else s->including(x)->asOrderedSet() endif endif)'))
		                               code).concat(if processor=mcore::expressions::MProcessor::Head then '->last() ' else '->first() ' endif).concat(
		                               'then s else s->including(x)->asOrderedSet() endif endif)') endif
		          else self.procAsCode() endif
		          ) endif
		  ).concat( 
		 '\n  endif') endif endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS;

		if (chainCodeforSubchainsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				chainCodeforSubchainsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(chainCodeforSubchainsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getChainEntryType() {
		MClassifier chainEntryType = basicGetChainEntryType();
		return chainEntryType != null && chainEntryType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) chainEntryType)
				: chainEntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getContainedCollector() {
		if (containedCollector != null && containedCollector.eIsProxy()) {
			InternalEObject oldContainedCollector = (InternalEObject) containedCollector;
			containedCollector = (MCollectionExpression) eResolveProxy(
					oldContainedCollector);
			if (containedCollector != oldContainedCollector) {
				InternalEObject newContainedCollector = (InternalEObject) containedCollector;
				NotificationChain msgs = oldContainedCollector.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
						null, null);
				if (newContainedCollector.eInternalContainer() == null) {
					msgs = newContainedCollector.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
							oldContainedCollector, containedCollector));
			}
		}
		return containedCollector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement1() {
		if (element1 != null && element1.eIsProxy()) {
			InternalEObject oldElement1 = (InternalEObject) element1;
			element1 = (MProperty) eResolveProxy(oldElement1);
			if (element1 != oldElement1) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MBASE_CHAIN__ELEMENT1,
							oldElement1, element1));
			}
		}
		return element1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement1Correct() {
		/**
		 * @OCL if element1.oclIsUndefined() then true else
		--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else
		if chainEntryType.oclIsUndefined() then false 
		else chainEntryType.allProperties()->includes(element1)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT;

		if (element1CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				element1CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element1CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement2() {
		if (element2 != null && element2.eIsProxy()) {
			InternalEObject oldElement2 = (InternalEObject) element2;
			element2 = (MProperty) eResolveProxy(oldElement2);
			if (element2 != oldElement2) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MBASE_CHAIN__ELEMENT2,
							oldElement2, element2));
			}
		}
		return element2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement2Correct() {
		/**
		 * @OCL if element2.oclIsUndefined() then true else
		--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else
		if element2EntryType.oclIsUndefined() then false 
		else element2EntryType.allProperties()->includes(self.element2)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT;

		if (element2CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				element2CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element2CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getElement2EntryType() {
		MClassifier element2EntryType = basicGetElement2EntryType();
		return element2EntryType != null && element2EntryType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) element2EntryType)
				: element2EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getElement3() {
		if (element3 != null && element3.eIsProxy()) {
			InternalEObject oldElement3 = (InternalEObject) element3;
			element3 = (MProperty) eResolveProxy(oldElement3);
			if (element3 != oldElement3) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MBASE_CHAIN__ELEMENT3,
							oldElement3, element3));
			}
		}
		return element3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getElement3Correct() {
		/**
		 * @OCL if element3.oclIsUndefined() then true else
		--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else
		if element3EntryType.oclIsUndefined() then false
		else element3EntryType.allProperties()->includes(self.element3)
		endif endif 
		--endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT;

		if (element3CorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				element3CorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(element3CorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval
					.evaluateElement(eOverrideFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getElement3EntryType() {
		MClassifier element3EntryType = basicGetElement3EntryType();
		return element3EntryType != null && element3EntryType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) element3EntryType)
				: element3EntryType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL isComplexExpression false
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getIsComplexExpression() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsOwnXOCLOp() {
		/**
		 * @OCL processor = expressions::MProcessor::CamelCaseLower or
		processor = expressions::MProcessor::CamelCaseToBusiness or
		processor = mcore::expressions::MProcessor::CamelCaseUpper
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__IS_OWN_XOCL_OP;

		if (isOwnXOCLOpDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isOwnXOCLOpDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOpDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel let kind: String = 'CHAIN' in
	kind
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getLastElement() {
		MProperty lastElement = basicGetLastElement();
		return lastElement != null && lastElement.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) lastElement)
				: lastElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessor getProcessor() {
		return processor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProcessorDefinition getProcessorDefinition() {
		MProcessorDefinition processorDefinition = basicGetProcessorDefinition();
		return processorDefinition != null && processorDefinition.eIsProxy()
				? (MProcessorDefinition) eResolveProxy(
						(InternalEObject) processorDefinition)
				: processorDefinition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSubChain> getSubExpression() {
		if (subExpression == null) {
			subExpression = new EObjectContainmentEList.Unsettable.Resolving<MSubChain>(
					MSubChain.class, this,
					ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION);
		}
		return subExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getTypeMismatch() {
		/**
		 * @OCL self.ownToApplyMismatch()
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MBASE_CHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MBASE_CHAIN__TYPE_MISMATCH;

		if (typeMismatchDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				typeMismatchDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(typeMismatchDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isCustomCodeProcessor() {

		/**
		 * @OCL if self.processorIsSet().oclIsUndefined() then null
		else 
		processor = mcore::expressions::MProcessor::Head or
		processor = mcore::expressions::MProcessor::Tail or
		processor = mcore::expressions::MProcessor::And or
		processor = mcore::expressions::MProcessor::Or  
		endif
		
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(11);
		if (isCustomCodeProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isCustomCodeProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isCustomCodeProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isOwnXOCLOperator() {

		/**
		 * @OCL processor =mcore::expressions::MProcessor::CamelCaseLower or
		processor =mcore::expressions::MProcessor::CamelCaseToBusiness or
		processor =mcore::expressions::MProcessor::CamelCaseUpper 
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(13);
		if (isOwnXOCLOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isOwnXOCLOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isOwnXOCLOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isPostfixProcessor() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::PlusOne or
		processor=expressions::MProcessor::MinusOne or
		processor=expressions::MProcessor::TimesMinusOne or
		processor=expressions::MProcessor::IsNull or
		processor=expressions::MProcessor::IsInvalid
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(8);
		if (isPostfixProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isPostfixProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isPostfixProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isPrefixProcessor() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::Not or
		processor = expressions::MProcessor::OneDividedBy
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(7);
		if (isPrefixProcessorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isPrefixProcessorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isPrefixProcessorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isProcessorCheckEqualOperator() {

		/**
		 * @OCL if processor.oclIsUndefined() then false 
		else
		processor=expressions::MProcessor::IsOne or
		processor=expressions::MProcessor::IsZero or
		processor = expressions::MProcessor::IsFalse or
		processor = expressions::MProcessor::IsTrue or
		processor = expressions::MProcessor::NotNull 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(6);
		if (isProcessorCheckEqualOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isProcessorCheckEqualOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isProcessorCheckEqualOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isProcessorSetOperator() {

		/**
		 * @OCL if processor = mcore::expressions::MProcessor::None then false 
		else
		processor=mcore::expressions::MProcessor::AsOrderedSet or
		processor=mcore::expressions::MProcessor::First or
		processor=mcore::expressions::MProcessor::IsEmpty or
		processor=mcore::expressions::MProcessor::Last or
		processor=mcore::expressions::MProcessor::NotEmpty or
		processor=mcore::expressions::MProcessor::Size or
		processor=mcore::expressions::MProcessor::Sum or
		processor=mcore::expressions::MProcessor::Head or
		processor=mcore::expressions::MProcessor::Tail or
		processor=mcore::expressions::MProcessor::And or
		processor=mcore::expressions::MProcessor::Or
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(12);
		if (isProcessorSetOperatorBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				isProcessorSetOperatorBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(isProcessorSetOperatorBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCallArgument() {
		return callArgument != null
				&& ((InternalEList.Unsettable<?>) callArgument).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCastType() {
		return castTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainedCollector() {
		return containedCollectorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement1() {
		return element1ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement2() {
		return element2ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetElement3() {
		return element3ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProcessor() {
		return processorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSubExpression() {
		return subExpression != null
				&& ((InternalEList.Unsettable<?>) subExpression).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer length() {

		/**
		 * @OCL if not element3.oclIsUndefined() then 3
		else if not element2.oclIsUndefined() then 2
		else if not element1.oclIsUndefined() then 1
		else 0 endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(1);
		if (lengthBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				lengthBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(lengthBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ownToApplyMismatch() {

		/**
		 * @OCL if self.eContainer().oclIsTypeOf(mcore::expressions::MIf) then false   -- Implement IF typemismatch
		else
		if self.eContainer().oclIsTypeOf(mcore::expressions::MApplication) and self.containedCollector.oclIsUndefined()
		then 
		let app: mcore::expressions::MApplication = self.eContainer().oclAsType(mcore::expressions::MApplication) in
		let opChangesReturn : Boolean = app.operands->first().calculatedSimpleType <> app.calculatedSimpleType or app.operands->first().calculatedType <> app.calculatedType
		--let opChangesReturn : Boolean = app.operands->first().calculatedOwnSimpleType <> app.calculatedOwnSimpleType or app.operands->first().calculatedOwnType <> app.calculatedOwnType
		in
		
		if self.base = mcore::expressions::ExpressionBase::SelfObject or  self.base = mcore::expressions::ExpressionBase::Variable
		then -- builtin been casted  : TODO  add Parameter,Iterator etc...
		
		if (app.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean) or (app.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)  then 
		if app.operands->first().calculatedOwnType.oclIsUndefined() then (self.calculatedOwnSimpleType <>  app.operands->first().calculatedOwnSimpleType and app.calculatedSimpleType<> mcore::SimpleType::Double)
		else    app.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType
		endif
		
		else
		
		
		if ((app.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and (app.calculatedOwnSimpleType= SimpleType::None)-- CHanged to App.calcOwnType
		then
		app.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType
		else if  ((app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) or opChangesReturn) and  (app.calculatedSimpleType <> SimpleType::None and (app.calculatedSimpleType <> SimpleType::Double))
		-- (app.calculatedSimpleType <> SimpleType::None  and app.calculatedSimpleType <> SimpleType::Boolean)
		then true else false endif
		
		endif
		
		endif
		
		else
		
		if self.base = ExpressionBase::SelfObject 
		then
		(app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) and opChangesReturn
		else 
		false
		endif
		endif
		
		
		else if not(self.eContainer().oclIsTypeOf(MApplication)) and self.containedCollector.oclIsUndefined() 
		and self.base = ExpressionBase::SelfObject then     --TODO  add parameter,iterator etc
		--self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType or     FOR TYPE
		if self.eContainer().oclIsTypeOf(MNamedExpression) and self.eContainer().oclIsTypeOf(MNamedExpression).oclIsTypeOf(mcore::annotations::MResult) and
		self.eContainer().oclAsType(MNamedExpression).eContainer().oclAsType(mcore::annotations::MExprAnnotation).namedExpression->asSequence()->last() = self.eContainer() 
		--   or self.eContainer().oclIsTypeOf(MCollectionExpression)
		then
		if self.expectedReturnType.oclIsUndefined() then self.expectedReturnType = self.calculatedOwnType and self.expectedReturnSimpleType <> self.calculatedOwnSimpleType 
		else
		self.expectedReturnType.allSubTypes()->excludes(self.calculatedType) and  self.expectedReturnType <>self.calculatedOwnType and self.expectedReturnSimpleType = self.calculatedOwnSimpleType 
		endif 
		else false endif
		
		
		else
		
		false endif endif
		endif
		
		--if (app.calculatedOwnSimpleType = SimpleType::Double and self.calculatedOwnSimpleType= SimpleType::Integer) then false
		--else
		--    app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType
		--endif
		--endif
		--else
		--null
		-- endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(1);
		if (ownToApplyMismatchBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ownToApplyMismatchBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ownToApplyMismatchBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String procAsCode() {

		/**
		 * @OCL if self.processor= mcore::expressions::MProcessor::IsNull then '.oclIsUndefined()' 
		else if self.processor = mcore::expressions::MProcessor::AllUpperCase then 'toUpperCase()'
		else if self.processor = mcore::expressions::MProcessor::IsInvalid then '.oclIsInvalid()'
		else if self.processor = mcore::expressions::MProcessor::Container then 'eContainer()'
		else  self.processor.toString()
		endif endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(10);
		if (procAsCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procAsCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procAsCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBoolean() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsFalse},
		Tuple{processor=mcore::expressions::MProcessor::IsTrue},
		Tuple{processor=mcore::expressions::MProcessor::Not},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(19);
		if (procDefChoicesForBooleanBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForBooleanBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleanBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForBooleans() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::And},
		Tuple{processor=mcore::expressions::MProcessor::Or},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(20);
		if (procDefChoicesForBooleansBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForBooleansBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForBooleansBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDate() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Year},
		Tuple{processor=mcore::expressions::MProcessor::Month},
		Tuple{processor=mcore::expressions::MProcessor::Day},
		Tuple{processor=mcore::expressions::MProcessor::Hour},
		Tuple{processor=mcore::expressions::MProcessor::Minute},
		Tuple{processor=mcore::expressions::MProcessor::Second},
		Tuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mcore::expressions::MProcessor::ToHhMm},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(27);
		if (procDefChoicesForDateBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForDateBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDateBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForDates() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Year},
		Tuple{processor=mcore::expressions::MProcessor::Month},
		Tuple{processor=mcore::expressions::MProcessor::Day},
		Tuple{processor=mcore::expressions::MProcessor::Hour},
		Tuple{processor=mcore::expressions::MProcessor::Minute},
		Tuple{processor=mcore::expressions::MProcessor::Second},
		Tuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},
		Tuple{processor=mcore::expressions::MProcessor::ToHhMm},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(28);
		if (procDefChoicesForDatesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForDatesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForDatesBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getCallArgument())
					.basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MBASE_CHAIN__CALL_ARGUMENT:
			return ((InternalEList<?>) getCallArgument()).basicRemove(otherEnd,
					msgs);
		case ExpressionsPackage.MBASE_CHAIN__SUB_EXPRESSION:
			return ((InternalEList<?>) getSubExpression()).basicRemove(otherEnd,
					msgs);
		case ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR:
			return basicUnsetContainedCollector(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForInteger() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsZero},
		Tuple{processor=mcore::expressions::MProcessor::IsOne},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(21);
		if (procDefChoicesForIntegerBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForIntegerBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegerBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForIntegers() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Sum},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(22);
		if (procDefChoicesForIntegersBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForIntegersBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForIntegersBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObject() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::Container},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(17);
		if (procDefChoicesForObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForObjects() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail},
		Tuple{processor=mcore::expressions::MProcessor::Container}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(18);
		if (procDefChoicesForObjectsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForObjectsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForObjectsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReal() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Round},
		Tuple{processor=mcore::expressions::MProcessor::Floor},
		Tuple{processor=mcore::expressions::MProcessor::IsZero},
		Tuple{processor=mcore::expressions::MProcessor::IsOne},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::ToString},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(23);
		if (procDefChoicesForRealBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForRealBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForReals() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Round},
		Tuple{processor=mcore::expressions::MProcessor::Floor},
		Tuple{processor=mcore::expressions::MProcessor::Sum},
		Tuple{processor=mcore::expressions::MProcessor::PlusOne},
		Tuple{processor=mcore::expressions::MProcessor::MinusOne},
		Tuple{processor=mcore::expressions::MProcessor::TimesMinusOne},
		Tuple{processor=mcore::expressions::MProcessor::Absolute},
		Tuple{processor=mcore::expressions::MProcessor::OneDividedBy},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(24);
		if (procDefChoicesForRealsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForRealsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForRealsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForString() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Trim},
		Tuple{processor=mcore::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mcore::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mcore::expressions::MProcessor::ToBoolean},
		Tuple{processor=mcore::expressions::MProcessor::ToInteger},
		Tuple{processor=mcore::expressions::MProcessor::ToReal},
		Tuple{processor=mcore::expressions::MProcessor::ToDate},
		Tuple{processor=mcore::expressions::MProcessor::IsNull},
		Tuple{processor=mcore::expressions::MProcessor::NotNull},
		Tuple{processor=mcore::expressions::MProcessor::AsOrderedSet},
		Tuple{processor=mcore::expressions::MProcessor::IsInvalid}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(25);
		if (procDefChoicesForStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProcessorDefinition> procDefChoicesForStrings() {

		/**
		 * @OCL OrderedSet{
		Tuple{processor=mcore::expressions::MProcessor::Trim},
		Tuple{processor=mcore::expressions::MProcessor::AllLowerCase},
		Tuple{processor=mcore::expressions::MProcessor::AllUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::FirstUpperCase},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseLower},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},
		Tuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},
		Tuple{processor=mcore::expressions::MProcessor::ToBoolean},
		Tuple{processor=mcore::expressions::MProcessor::ToInteger},
		Tuple{processor=mcore::expressions::MProcessor::ToReal},
		Tuple{processor=mcore::expressions::MProcessor::ToDate},
		Tuple{processor=mcore::expressions::MProcessor::IsEmpty},
		Tuple{processor=mcore::expressions::MProcessor::NotEmpty},
		Tuple{processor=mcore::expressions::MProcessor::Size},
		Tuple{processor=mcore::expressions::MProcessor::First},
		Tuple{processor=mcore::expressions::MProcessor::Last},
		Tuple{processor=mcore::expressions::MProcessor::Head},
		Tuple{processor=mcore::expressions::MProcessor::Tail}}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(26);
		if (procDefChoicesForStringsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				procDefChoicesForStringsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(procDefChoicesForStringsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MProcessorDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorIsSet() {

		/**
		 * @OCL self.processor <> mcore::expressions::MProcessor::None
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(15);
		if (processorIsSetBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				processorIsSetBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorIsSetBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean processorReturnsSingular() {

		/**
		 * @OCL if self.processor = mcore::expressions::MProcessor::None then null
		else if
		self.processor = mcore::expressions::MProcessor::AsOrderedSet or
		processor = mcore::expressions::MProcessor::Head or
		processor= mcore::expressions::MProcessor::Tail
		
		then 
		false
		else true
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(14);
		if (processorReturnsSingularBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				processorReturnsSingularBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(processorReturnsSingularBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCastType(MClassifier newCastType) {
		MClassifier oldCastType = castType;
		castType = newCastType;
		boolean oldCastTypeESet = castTypeESet;
		castTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__CAST_TYPE, oldCastType,
					castType, !oldCastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainedCollector(
			MCollectionExpression newContainedCollector) {
		if (newContainedCollector != containedCollector) {
			NotificationChain msgs = null;
			if (containedCollector != null)
				msgs = ((InternalEObject) containedCollector).eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
						null, msgs);
			if (newContainedCollector != null)
				msgs = ((InternalEObject) newContainedCollector).eInverseAdd(
						this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
						null, msgs);
			msgs = basicSetContainedCollector(newContainedCollector, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
						newContainedCollector, newContainedCollector,
						!oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement1(MProperty newElement1) {
		MProperty oldElement1 = element1;
		element1 = newElement1;
		boolean oldElement1ESet = element1ESet;
		element1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT1, oldElement1,
					element1, !oldElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement2(MProperty newElement2) {
		MProperty oldElement2 = element2;
		element2 = newElement2;
		boolean oldElement2ESet = element2ESet;
		element2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT2, oldElement2,
					element2, !oldElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElement3(MProperty newElement3) {
		MProperty oldElement3 = element3;
		element3 = newElement3;
		boolean oldElement3ESet = element3ESet;
		element3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT3, oldElement3,
					element3, !oldElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProcessor(MProcessor newProcessor) {
		MProcessor oldProcessor = processor;
		processor = newProcessor == null ? PROCESSOR_EDEFAULT : newProcessor;
		boolean oldProcessorESet = processorESet;
		processorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MBASE_CHAIN__PROCESSOR, oldProcessor,
					processor, !oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setProcessorDefinition(
			MProcessorDefinition newProcessorDefinition) {
		/* NOTE: Repeated from MAbstractChainImpl
		/* NOTE: After you changed this you may also have to change basicGetProcessorDefinition() as in basicGetBaseDefinition() where you do it in order to retrieve the selected base variable. */
		if (newProcessorDefinition == null) {
			setProcessor(MProcessor.NONE);
		} else {
			setProcessor(newProcessorDefinition.getProcessor());
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String uniqueChainNumber() {

		/**
		 * @OCL 		        let variableName : String = 'chain'.concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then '' else  self.eContainer().oclAsType(mcore::expressions::MApplication).uniqueApplyNumber().toString() endif).concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then '' else
		    
		     if (self.eContainer().oclIsTypeOf(mcore::expressions::MApplication))  then 
		      if (self.eContainer().oclAsType(mcore::expressions::MApplication).operands->isEmpty()) and self.oclIsTypeOf(mcore::expressions::MChain) then ' '  else 
		       self.eContainer().oclAsType(mcore::expressions::MApplication).operands->iterate(i:mcore::expressions::MChainOrApplication; r: OrderedSet(MBaseChain)=OrderedSet{} | if i.oclIsKindOf(MBaseChain) then r->including(i.oclAsType(MBaseChain))->asOrderedSet() else r endif )->indexOf(self).toString()
		       endif else '' 
		       endif endif ) in variableName
		       
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(2);
		if (uniqueChainNumberBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				uniqueChainNumberBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(uniqueChainNumberBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean reuseFromOtherNoMoreUsedChain(MBaseChain archetype) {
		//ATTENTION: Duplicated in MExprAnnotation and MBaseChain
		//Add all operations of expr annotation, base, element 1-3, cast, processor
		this.setBaseDefinition(archetype.getBaseDefinition());
		this.setElement1(archetype.getElement1());
		this.setElement2(archetype.getElement2());
		this.setElement3(archetype.getElement3());
		this.setCastType(archetype.getCastType());
		this.setProcessor(archetype.getProcessor());
		this.setContainedCollector(archetype.getContainedCollector());
		if (!(archetype.getSubExpression() == null)
				&& !archetype.getSubExpression().isEmpty()) {
			this.getSubExpression().addAll(archetype.getSubExpression());
		}
		if (!(archetype.getCallArgument() == null)
				&& !archetype.getCallArgument().isEmpty()) {
			this.getCallArgument().addAll(archetype.getCallArgument());
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Boolean resetToBase(ExpressionBase base, MVariable baseVar) {
		//ATTENTION: Duplicated in MExprAnnotation and MBaseChain

		this.setBase(base);
		this.setBaseVar(baseVar);
		this.setElement1(null);
		this.setElement2(null);
		this.setElement3(null);
		this.setCastType(null);
		this.setProcessor(null);
		this.setContainedCollector(null);
		if (!(getCallArgument() == null) && !getCallArgument().isEmpty()) {
			this.getCallArgument().clear();
		}
		if (!(getSubExpression() == null) && !getSubExpression().isEmpty()) {
			this.getSubExpression().clear();
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate reuseFromOtherNoMoreUsedChainAsUpdate(MBaseChain archetype) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		//1. Set currenttrigger :=  base
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				ExpressionsPackage.eINSTANCE
						.getMAbstractExpressionWithBase_Base(),
				XUpdateMode.REDEFINE, null,
				ExpressionsPackage.Literals.EXPRESSION_BASE
						.getEEnumLiteralByLiteral(
								archetype.getBase().getLiteral()),
				null, null);

		//2. element1 :=   archetype element1 
		currentTrigger.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element1(),
				XUpdateMode.REDEFINE, null, archetype.getElement1(), null,
				null);

		//2.1  archetype element1 = null
		currentTrigger.addReferenceUpdate(archetype,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element1(),
				XUpdateMode.REMOVE, null, archetype.getElement1(), null, null);

		// 3. element2 :=   archetype element2
		currentTrigger.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element2(),
				XUpdateMode.REDEFINE, null, archetype.getElement2(), null,
				null);
		//3.1  archetype element2 = null
		currentTrigger.addReferenceUpdate(archetype,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element2(),
				XUpdateMode.REMOVE, null, archetype.getElement2(), null, null);

		//4. element3 :=   archetype element3
		currentTrigger.addReferenceUpdate(this,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element3(),
				XUpdateMode.REDEFINE, null, archetype.getElement3(), null,
				null);

		//4.1  archetype element3 = null
		currentTrigger.addReferenceUpdate(archetype,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Element3(),
				XUpdateMode.REMOVE, null, archetype.getElement3(), null, null);

		//5. processor :=   archetype processor
		currentTrigger.addAttributeUpdate(this,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Processor(),
				XUpdateMode.REDEFINE, null,
				ExpressionsPackage.Literals.MPROCESSOR.getEEnumLiteralByLiteral(
						archetype.getProcessor().getLiteral()),
				null, null);

		//5.1 archetype processor = null
		currentTrigger.addAttributeUpdate(archetype,
				ExpressionsPackage.eINSTANCE.getMAbstractChain_Processor(),
				XUpdateMode.REMOVE, null,
				ExpressionsPackage.Literals.MPROCESSOR.getEEnumLiteralByLiteral(
						archetype.getProcessor().getLiteral()),
				null, null);

		if (!archetype.getSubExpression().isEmpty()) {
			//6. archetype subexpression :=  null

			currentTrigger.addReferenceUpdate(archetype,
					ExpressionsPackage.eINSTANCE.getMBaseChain_SubExpression(),
					XUpdateMode.REMOVE, null, archetype.getSubExpression(),
					null, null);
			//7. subexpression :=   archetype subexpression
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE.getMBaseChain_SubExpression(),
					XUpdateMode.ADD, XAddUpdateMode.FIRST,
					archetype.getSubExpression(), null, null);
		}
		if (archetype.getContainedCollector() != null) {
			//8.  archetype containedcollector := null
			currentTrigger.addReferenceUpdate(archetype,
					ExpressionsPackage.eINSTANCE
							.getMBaseChain_ContainedCollector(),
					XUpdateMode.REMOVE, null, archetype.getContainedCollector(),
					null, null);
			//9. containedcollector :=   archetype contained collector
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE
							.getMBaseChain_ContainedCollector(),
					XUpdateMode.ADD, XAddUpdateMode.FIRST,
					archetype.getCollector(), null, null);
		}

		// return XUpdate
		return currentTrigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep) {

		/**
		 * @OCL unsafeChainAsCode(fromStep, length())
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(4);
		if (unsafeChainAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeChainAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainAsCode(Integer fromStep, Integer toStep) {

		/**
		 * @OCL let end: Integer = if (length() > toStep) then toStep else length() endif in
		
		if fromStep=1 then
		if end=3 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2)).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then
		unsafeChainStepAsCode(1).concat('.').concat(unsafeChainStepAsCode(2))
		else if end=1 then unsafeChainStepAsCode(1) else '' endif
		endif endif
		else if fromStep=2 then
		if end=3 then
		unsafeChainStepAsCode(2).concat('.').concat(unsafeChainStepAsCode(3)) 
		else if end=2 then unsafeChainStepAsCode(2) else '' endif
		endif
		else if fromStep=3 then
		if end=3 then unsafeChainStepAsCode(3) else '' endif
		else 'ERROR'
		endif endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(5);
		if (unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				unsafeChainAsCodeecoreEIntegerObjectecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("fromStep", fromStep);

			evalEnv.add("toStep", toStep);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeChainStepAsCode(Integer step) {

		/**
		 * @OCL if step=1 then
		if element1.oclIsUndefined() then 'MISSING ELEMENT 1'
		else unsafeElementAsCode(1) endif
		else if step=2 then
		if element2.oclIsUndefined() then 'MISSING ELEMENT 2'
		else unsafeElementAsCode(2) endif
		else if step=3 then
		if element3.oclIsUndefined() then 'MISSING ELEMENT 3'
		else unsafeElementAsCode(3) endif
		else 'ERROR'
		endif endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_CHAIN
				.getEOperations().get(3);
		if (unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeChainStepAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String unsafeElementAsCode(Integer step) {

		/**
		 * @OCL let element: mcore::MProperty = if step=1 then element1
		else if step=2 then element2 
		else if step=3 then element3
		else null endif endif endif in
		
		if element.oclIsUndefined() then 'ERROR' else 
		if element.isOperation
		then if step=length()
		then
		let p: String = let pp: String = callArgument->iterate(
		x: mcore::expressions::MCallArgument; s: String = '' | 
		s.concat(', ').concat(x.asCode)
		) in if pp.size()>2 then pp.substring(3,pp.size()) else pp endif in
		element.eName.concat('(').concat(p).concat(')') 
		else		
		element.eName.concat('()') endif		
		else 
		element.eName 
		endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MBASE_CHAIN);
		EOperation eOperation = ExpressionsPackage.Literals.MBASE_CHAIN
				.getEOperations().get(10);
		if (unsafeElementAsCodeecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				unsafeElementAsCodeecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(unsafeElementAsCodeecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MBASE_CHAIN, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("step", step);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCallArgument() {
		if (callArgument != null)
			((InternalEList.Unsettable<?>) callArgument).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCastType() {
		MClassifier oldCastType = castType;
		boolean oldCastTypeESet = castTypeESet;
		castType = null;
		castTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__CAST_TYPE, oldCastType,
					null, oldCastTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainedCollector() {
		if (containedCollector != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) containedCollector).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
					null, msgs);
			msgs = basicUnsetContainedCollector(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldContainedCollectorESet = containedCollectorESet;
			containedCollectorESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MBASE_CHAIN__CONTAINED_COLLECTOR,
						null, null, oldContainedCollectorESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement1() {
		MProperty oldElement1 = element1;
		boolean oldElement1ESet = element1ESet;
		element1 = null;
		element1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT1, oldElement1, null,
					oldElement1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement2() {
		MProperty oldElement2 = element2;
		boolean oldElement2ESet = element2ESet;
		element2 = null;
		element2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT2, oldElement2, null,
					oldElement2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetElement3() {
		MProperty oldElement3 = element3;
		boolean oldElement3ESet = element3ESet;
		element3 = null;
		element3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__ELEMENT3, oldElement3, null,
					oldElement3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProcessor() {
		MProcessor oldProcessor = processor;
		boolean oldProcessorESet = processorESet;
		processor = PROCESSOR_EDEFAULT;
		processorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MBASE_CHAIN__PROCESSOR, oldProcessor,
					PROCESSOR_EDEFAULT, oldProcessorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSubExpression() {
		if (subExpression != null)
			((InternalEList.Unsettable<?>) subExpression).unset();
	}

} //MBaseChainImpl
