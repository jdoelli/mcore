/**
 */
package com.montages.mcore.expressions.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractBaseDefinition;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.expressions.MAccumulatorBaseDefinition;
import com.montages.mcore.expressions.MBaseDefinition;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MContainerBaseDefinition;
import com.montages.mcore.expressions.MIteratorBaseDefinition;
import com.montages.mcore.expressions.MLiteralConstantBaseDefinition;
import com.montages.mcore.expressions.MNumberBaseDefinition;
import com.montages.mcore.expressions.MObjectBaseDefinition;
import com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mcore.expressions.MParameterBaseDefinition;
import com.montages.mcore.expressions.MSelfBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mcore.expressions.MSourceBaseDefinition;
import com.montages.mcore.expressions.MTargetBaseDefinition;
import com.montages.mcore.expressions.MVariableBaseDefinition;
import com.montages.mcore.impl.MTypedImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getAsCode <em>As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getAsBasicCode <em>As Basic Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getCollector <em>Collector</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getEntireScope <em>Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeBase <em>Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeSelf <em>Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeTrg <em>Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeObj <em>Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeLiteralConstants <em>Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeVariables <em>Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeIterator <em>Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeAccumulator <em>Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeParameters <em>Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeContainerBase <em>Scope Container Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeNumberBase <em>Scope Number Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getScopeSource <em>Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalEntireScope <em>Local Entire Scope</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeBase <em>Local Scope Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeSelf <em>Local Scope Self</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeTrg <em>Local Scope Trg</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeObj <em>Local Scope Obj</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeSource <em>Local Scope Source</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeVariables <em>Local Scope Variables</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeIterator <em>Local Scope Iterator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeAccumulator <em>Local Scope Accumulator</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeParameters <em>Local Scope Parameters</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getLocalScopeContainer <em>Local Scope Container</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getContainingAnnotation <em>Containing Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getContainingExpression <em>Containing Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getIsComplexExpression <em>Is Complex Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getIsSubExpression <em>Is Sub Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnType <em>Calculated Own Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnSingular <em>Calculated Own Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getSelfObjectType <em>Self Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getSelfObjectPackage <em>Self Object Package</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getTargetObjectType <em>Target Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getTargetSimpleType <em>Target Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getObjectObjectType <em>Object Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getExpectedReturnType <em>Expected Return Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getExpectedReturnSimpleType <em>Expected Return Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getIsReturnValueSingular <em>Is Return Value Singular</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getSrcObjectType <em>Src Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MAbstractExpressionImpl#getSrcObjectSimpleType <em>Src Object Simple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractExpressionImpl extends MTypedImpl
		implements MAbstractExpression {
	/**
	 * The default value of the '{@link #getAsCode() <em>As Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAsBasicCode() <em>As Basic Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode()
	 * @generated
	 * @ordered
	 */
	protected static final String AS_BASIC_CODE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsComplexExpression() <em>Is Complex Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_COMPLEX_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsSubExpression() <em>Is Sub Expression</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_SUB_EXPRESSION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnMandatory() <em>Calculated Own Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnSingular() <em>Calculated Own Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_OWN_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedOwnSimpleType() <em>Calculated Own Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CALCULATED_OWN_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getTargetSimpleType() <em>Target Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType TARGET_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getExpectedReturnSimpleType() <em>Expected Return Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType EXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getIsReturnValueMandatory() <em>Is Return Value Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReturnValueSingular() <em>Is Return Value Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_RETURN_VALUE_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSrcObjectSimpleType() <em>Src Object Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SRC_OBJECT_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The parsed OCL expression for the body of the '{@link #getShortCode <em>Get Short Code</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getShortCode
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getShortCodeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #getScope <em>Get Scope</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScope
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression getScopeBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsCode <em>As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCollector <em>Collector</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCollector
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression collectorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEntireScope <em>Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression entireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeBase <em>Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSelf <em>Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeTrg <em>Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObj <em>Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSimpleTypeConstants <em>Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeLiteralConstants <em>Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeObjectReferenceConstants <em>Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeVariables <em>Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeIterator <em>Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeAccumulator <em>Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeParameters <em>Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeContainerBase <em>Scope Container Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeContainerBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeContainerBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeNumberBase <em>Scope Number Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeNumberBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeNumberBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getScopeSource <em>Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression scopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalEntireScope <em>Local Entire Scope</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalEntireScope
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localEntireScopeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeBase <em>Local Scope Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSelf <em>Local Scope Self</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSelf
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSelfDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeTrg <em>Local Scope Trg</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeTrg
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeTrgDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObj <em>Local Scope Obj</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObj
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSource <em>Local Scope Source</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeSimpleTypeConstants <em>Local Scope Simple Type Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeSimpleTypeConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeSimpleTypeConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeLiteralConstants <em>Local Scope Literal Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeLiteralConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeLiteralConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeObjectReferenceConstants <em>Local Scope Object Reference Constants</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeObjectReferenceConstants
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeObjectReferenceConstantsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeVariables <em>Local Scope Variables</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeVariables
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeVariablesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeIterator <em>Local Scope Iterator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeIterator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeIteratorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeAccumulator <em>Local Scope Accumulator</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeAccumulator
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeAccumulatorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeParameters <em>Local Scope Parameters</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeParameters
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeParametersDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeNumberBaseDefinition <em>Local Scope Number Base Definition</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeNumberBaseDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeNumberBaseDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalScopeContainer <em>Local Scope Container</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocalScopeContainer
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression localScopeContainerDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAnnotation <em>Containing Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingExpression <em>Containing Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsComplexExpression <em>Is Complex Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsComplexExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isComplexExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsSubExpression <em>Is Sub Expression</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsSubExpression
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isSubExpressionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSelfObjectType <em>Self Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression selfObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSelfObjectPackage <em>Self Object Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression selfObjectPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetObjectType <em>Target Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetSimpleType <em>Target Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectType <em>Object Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnType <em>Expected Return Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleType <em>Expected Return Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueMandatory <em>Is Return Value Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReturnValueSingular <em>Is Return Value Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIsReturnValueSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReturnValueSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSrcObjectType <em>Src Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression srcObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSrcObjectSimpleType <em>Src Object Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSrcObjectSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression srcObjectSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsCode() {
		/**
		 * @OCL if (let e: Boolean = collector.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then asBasicCode
		else if collector.oclIsUndefined()
		then null
		else collector.asBasicCode
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE;

		if (asCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAsBasicCode() {
		/**
		 * @OCL eClass().name.concat(' : <?>')
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression getCollector() {
		MCollectionExpression collector = basicGetCollector();
		return collector != null && collector.eIsProxy()
				? (MCollectionExpression) eResolveProxy(
						(InternalEObject) collector)
				: collector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MCollectionExpression basicGetCollector() {
		/**
		 * @OCL let nl: mcore::expressions::MCollectionExpression = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR;

		if (collectorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				collectorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(collectorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MCollectionExpression result = (MCollectionExpression) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getEntireScope() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localEntireScope 
		else /* Note this is necessary for some cases like scopeIterator that otherwise won't work *\/ 
		scopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(
		scopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeNumberBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeContainerBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		scopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE;

		if (entireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				entireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(entireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getScopeBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeBase 
		else containingExpression.scopeBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE;

		if (scopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getScopeSelf() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSelf
		else containingExpression.scopeSelf
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF;

		if (scopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getScopeTrg() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeTrg
		else containingExpression.scopeTrg
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG;

		if (scopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getScopeObj() {
		/**
		 * @OCL --todo: only for updates...
		if containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ;

		if (scopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getScopeSimpleTypeConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeSimpleTypeConstants
		else containingExpression.scopeSimpleTypeConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (scopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSimpleTypeConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getScopeLiteralConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeLiteralConstants
		else containingExpression.scopeLiteralConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS;

		if (scopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeLiteralConstantsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getScopeObjectReferenceConstants() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeObjectReferenceConstants
		else containingExpression.scopeObjectReferenceConstants
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (scopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeObjectReferenceConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(scopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getScopeVariables() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeVariables
		else containingExpression.scopeVariables
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES;

		if (scopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getScopeIterator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeIterator
		else containingExpression.scopeIterator->union(localScopeIterator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR;

		if (scopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getScopeAccumulator() {
		/**
		 * @OCL /* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr *\/
		
		if containingExpression.oclIsUndefined()
		then localScopeAccumulator
		else containingExpression.scopeAccumulator->union(localScopeAccumulator)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR;

		if (scopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getScopeParameters() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then localScopeParameters
		else containingExpression.scopeParameters
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS;

		if (scopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getScopeContainerBase() {
		/**
		 * @OCL localScopeContainer
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE;

		if (scopeContainerBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeContainerBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeContainerBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getScopeNumberBase() {
		/**
		 * @OCL if containingExpression.oclIsUndefined()
		then self.localScopeNumberBaseDefinition
		else containingExpression.scopeNumberBase
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE;

		if (scopeNumberBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeNumberBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeNumberBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getScopeSource() {
		/**
		 * @OCL 
		if containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE;

		if (scopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				scopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(scopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getLocalEntireScope() {
		/**
		 * @OCL localScopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(
		localScopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeNumberBaseDefinition->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeContainer->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(
		localScopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE;

		if (localEntireScopeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localEntireScopeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localEntireScopeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAbstractBaseDefinition> result = (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MBaseDefinition> getLocalScopeBase() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mcore::expressions::ExpressionBase::NullValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::FalseValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::TrueValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyStringValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyCollection}  
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE;

		if (localScopeBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MBaseDefinition> result = (EList<MBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSelfBaseDefinition> getLocalScopeSelf() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF;

		if (localScopeSelfDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSelfDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSelfDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSelfBaseDefinition> result = (EList<MSelfBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MTargetBaseDefinition> getLocalScopeTrg() {
		/**
		 * @OCL if targetObjectType.oclIsUndefined() and self.targetSimpleType=mcore::SimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG;

		if (localScopeTrgDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeTrgDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeTrgDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MTargetBaseDefinition> result = (EList<MTargetBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectBaseDefinition> getLocalScopeObj() {
		/**
		 * @OCL if objectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ;

		if (localScopeObjDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeObjDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeObjDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectBaseDefinition> result = (EList<MObjectBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSourceBaseDefinition> getLocalScopeSource() {
		/**
		 * @OCL if srcObjectType.oclIsUndefined() and self.srcObjectSimpleType=mcore::SimpleType::None
		then OrderedSet{}
		else OrderedSet{Tuple{debug=''}} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE;

		if (localScopeSourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeSourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSourceBaseDefinition> result = (EList<MSourceBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MSimpleTypeConstantBaseDefinition> getLocalScopeSimpleTypeConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in*\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MSimpleTypeConstantLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS;

		if (localScopeSimpleTypeConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeSimpleTypeConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeSimpleTypeConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MSimpleTypeConstantBaseDefinition> result = (EList<MSimpleTypeConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MLiteralConstantBaseDefinition> getLocalScopeLiteralConstants() {
		/**
		 * @OCL /* if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in *\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MLiteralLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/*
		else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS;

		if (localScopeLiteralConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeLiteralConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeLiteralConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MLiteralConstantBaseDefinition> result = (EList<MLiteralConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObjectReferenceConstantBaseDefinition> getLocalScopeObjectReferenceConstants() {
		/**
		 * @OCL /*if eContainer().oclIsUndefined() then OrderedSet{} 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)
		then let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in *\/
		let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then OrderedSet{} 
		else
		
		a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if i.expression.oclIsKindOf(mcore::expressions::MObjectReferenceLet) and (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedConstant=i}) 
		else r 
		endif
		endif
		)
		endif
		/* else OrderedSet{} endif
		endif *\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS;

		if (localScopeObjectReferenceConstantsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeObjectReferenceConstantsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeObjectReferenceConstantsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MObjectReferenceConstantBaseDefinition> result = (EList<MObjectReferenceConstantBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MVariableBaseDefinition> getLocalScopeVariables() {
		/**
		 * @OCL let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined()
		then  OrderedSet{} 
		else
		a.namedExpression->iterate (i: mcore::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mcore::expressions::MNamedExpression))=OrderedSet{} |
		if i.expression.oclIsUndefined() then r
		else if  (not i.name.oclIsUndefined()) and (i.name<>'')
		then r->append(Tuple{namedExpression=i}) 
		else r 
		endif
		endif
		) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES;

		if (localScopeVariablesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeVariablesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeVariablesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MVariableBaseDefinition> result = (EList<MVariableBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MIteratorBaseDefinition> getLocalScopeIterator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)
		then let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in
		if c.iteratorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{iterator=c.iteratorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR;

		if (localScopeIteratorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeIteratorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeIteratorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MIteratorBaseDefinition> result = (EList<MIteratorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAccumulatorBaseDefinition> getLocalScopeAccumulator() {
		/**
		 * @OCL if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)
		then let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in
		if c.accumulatorVar.oclIsUndefined() then OrderedSet{}
		else OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif
		else OrderedSet{} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR;

		if (localScopeAccumulatorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeAccumulatorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeAccumulatorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MAccumulatorBaseDefinition> result = (EList<MAccumulatorBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameterBaseDefinition> getLocalScopeParameters() {
		/**
		 * @OCL if self.containingAnnotation=null 
		then OrderedSet{}
		else 
		let e:MModelElement= self.containingAnnotation.annotatedElement in
		if e=null 
		then OrderedSet{}
		else if not e.oclIsKindOf(mcore::MOperationSignature)
		then OrderedSet{}
		else e.oclAsType(mcore::MOperationSignature).parameter
		 ->iterate (i: mcore::MParameter; r: OrderedSet(Tuple(parameter:mcore::MParameter))=OrderedSet{} 
		                |r->append(Tuple{parameter=i}) )
		endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS;

		if (localScopeParametersDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeParametersDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeParametersDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MParameterBaseDefinition> result = (EList<MParameterBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNumberBaseDefinition> getLocalScopeNumberBaseDefinition() {
		/**
		 * @OCL OrderedSet{
		Tuple{expressionBase = mcore::expressions::ExpressionBase::ZeroValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::OneValue},
		Tuple{expressionBase = mcore::expressions::ExpressionBase::MinusOneValue}
		}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION;

		if (localScopeNumberBaseDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeNumberBaseDefinitionDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(localScopeNumberBaseDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MNumberBaseDefinition> result = (EList<MNumberBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MContainerBaseDefinition> getLocalScopeContainer() {
		/**
		 * @OCL OrderedSet{Tuple{debug=''}}
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER;

		if (localScopeContainerDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				localScopeContainerDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localScopeContainerDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MContainerBaseDefinition> result = (EList<MContainerBaseDefinition>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotation getContainingAnnotation() {
		MExprAnnotation containingAnnotation = basicGetContainingAnnotation();
		return containingAnnotation != null && containingAnnotation.eIsProxy()
				? (MExprAnnotation) eResolveProxy(
						(InternalEObject) containingAnnotation)
				: containingAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotation basicGetContainingAnnotation() {
		/**
		 * @OCL -- expressions can now be as well annotations, so the "containing" annotation can be self.
		if self.oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then self.oclAsType(mcore::annotations::MExprAnnotation) 
		else if eContainer().oclIsUndefined() 
		then null 
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation) 
		else if eContainer().oclIsTypeOf(mcore::expressions::MNamedExpression) 
		then self.eContainer().oclAsType(mcore::expressions::MNamedExpression).containingAnnotation
		else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue) 
		then eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue)
		.eContainer().oclAsType(mcore::expressions::MNewObjecct).containingAnnotation
		else if containingExpression.oclIsUndefined() 
		then null 
		else containingExpression.containingAnnotation endif endif endif endif endif	endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION;

		if (containingAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingAnnotationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MExprAnnotation result = (MExprAnnotation) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression getContainingExpression() {
		MAbstractExpression containingExpression = basicGetContainingExpression();
		return containingExpression != null && containingExpression.eIsProxy()
				? (MAbstractExpression) eResolveProxy(
						(InternalEObject) containingExpression)
				: containingExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpression basicGetContainingExpression() {
		/**
		 * @OCL if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression)
		then eContainer().oclAsType(mcore::expressions::MAbstractExpression)
		
		else null endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION;

		if (containingExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractExpression result = (MAbstractExpression) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsComplexExpression() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION;

		if (isComplexExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isComplexExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isComplexExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsSubExpression() {
		/**
		 * @OCL if containingExpression.oclIsUndefined() then false
		else not (containingExpression.oclIsKindOf(mcore::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mcore::expressions::MNamedExpression)) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION;

		if (isSubExpressionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isSubExpressionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isSubExpressionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSelfObjectType() {
		MClassifier selfObjectType = basicGetSelfObjectType();
		return selfObjectType != null && selfObjectType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) selfObjectType)
				: selfObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSelfObjectType() {
		/**
		 * @OCL let a:mcore::annotations::MExprAnnotation = self.containingAnnotation in
		if a.oclIsUndefined() then null
		else a.selfObjectTypeOfAnnotation endif
		
		/*if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation
		--else if self.oclsKindOf(mcore::annotations::MExprAnnotation)
		--   then self.oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation
		
		--else  if  eContainer().oclIsTypeOf(mcore::expressions::MAbstractExpression) then
		-- eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType
		else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue)
		then 
		if eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mcore::expressions::MNewObjecct)
		then  eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mcore::expressions::MNewObjecct).selfObjectType
		else 
		null
		endif
		else eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType
		endif
		--endif
		endif
		endif 
		--endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE;

		if (selfObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				selfObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(selfObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getSelfObjectPackage() {
		MPackage selfObjectPackage = basicGetSelfObjectPackage();
		return selfObjectPackage != null && selfObjectPackage.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) selfObjectPackage)
				: selfObjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetSelfObjectPackage() {
		/**
		 * @OCL if (let e: Boolean = selfObjectType.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if selfObjectType.oclIsUndefined()
		then null
		else selfObjectType.containingPackage
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE;

		if (selfObjectPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				selfObjectPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(selfObjectPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getTargetObjectType() {
		MClassifier targetObjectType = basicGetTargetObjectType();
		return targetObjectType != null && targetObjectType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) targetObjectType)
				: targetObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetTargetObjectType() {
		/**
		 * @OCL if self.containingAnnotation.oclIsUndefined()
		then null
		else self.containingAnnotation.targetObjectTypeOfAnnotation endif
		/*   
		if eContainer().oclIsUndefined() then null
		else if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) 
		then eContainer().oclAsType(mcore::annotations::MExprAnnotation).targetObjectTypeOfAnnotation
		else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) then
		eContainer().oclAsType(mcore::expressions::MAbstractExpression).targetObjectType else null endif
		endif
		endif *\/
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE;

		if (targetObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(targetObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getTargetSimpleType() {
		/**
		 * @OCL if self.containingAnnotation.oclIsUndefined()
		then null
		else self.containingAnnotation.targetSimpleTypeOfAnnotation endif
		/*
		if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.targetSimpleTypeOfAnnotation
		endif
		endif
		*\/
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE;

		if (targetSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(targetSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getObjectObjectType() {
		MClassifier objectObjectType = basicGetObjectObjectType();
		return objectObjectType != null && objectObjectType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) objectObjectType)
				: objectObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetObjectObjectType() {
		/**
		 * @OCL if self.oclIsKindOf(mcore::annotations::MUpdateValue)  
		--replaced with self.
		then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation
		else if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)
		then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation
		else if eContainer().oclIsUndefined()  then null 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  
		then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation 
		else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  
		then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   
		else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) 
		then eContainer().oclAsType(mcore::expressions::MAbstractExpression).objectObjectType 
		else null endif endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE;

		if (objectObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getExpectedReturnType() {
		MClassifier expectedReturnType = basicGetExpectedReturnType();
		return expectedReturnType != null && expectedReturnType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) expectedReturnType)
				: expectedReturnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetExpectedReturnType() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then null
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.expectedReturnTypeOfAnnotation
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE;

		if (expectedReturnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(expectedReturnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getExpectedReturnSimpleType() {
		/**
		 * @OCL if containingAnnotation.oclIsUndefined() 
		then mcore::SimpleType::None
		else containingAnnotation.expectedReturnSimpleTypeOfAnnotation endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE;

		if (expectedReturnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				expectedReturnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(expectedReturnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueMandatory() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationMandatory
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY;

		if (isReturnValueMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReturnValueSingular() {
		/**
		 * @OCL if (let e: Boolean = containingAnnotation.oclIsUndefined() in 
		if e.oclIsInvalid() then null else e endif) 
		=true 
		then false
		else if containingAnnotation.oclIsUndefined()
		then null
		else containingAnnotation.isReturnValueOfAnnotationSingular
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR;

		if (isReturnValueSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReturnValueSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReturnValueSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSrcObjectType() {
		MClassifier srcObjectType = basicGetSrcObjectType();
		return srcObjectType != null && srcObjectType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) srcObjectType)
				: srcObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSrcObjectType() {
		/**
		 * @OCL let srcType : MClassifier =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type
		else null 
		endif
		endif
		in
		if srcType.oclIsUndefined() then null else srcType endif
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE;

		if (srcObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				srcObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(srcObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSrcObjectSimpleType() {
		/**
		 * @OCL let srcType : SimpleType =
		let srcClassifier:MProperty = 
		self.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty
		in 
		if srcClassifier.oclIsUndefined() then null
		else if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType
		else null
		endif endif
		in
		if srcType.oclIsUndefined() then SimpleType::None else srcType endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE;

		if (srcObjectSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				srcObjectSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(srcObjectSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCalculatedOwnType() {
		MClassifier calculatedOwnType = basicGetCalculatedOwnType();
		return calculatedOwnType != null && calculatedOwnType.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) calculatedOwnType)
				: calculatedOwnType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCalculatedOwnType() {
		/**
		 * @OCL let nl: mcore::MClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedOwnSingular() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getCalculatedOwnSimpleType() {
		/**
		 * @OCL mcore::SimpleType::None
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getShortCode() {

		/**
		 * @OCL if asCode.oclIsUndefined() then ''
		else let s: String = asCode in
		if s.size() > 30 then s.substring(1, 27).concat('...')
		else s endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION
				.getEOperations().get(0);
		if (getShortCodeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				getShortCodeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getShortCodeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAbstractBaseDefinition> getScope() {

		/**
		 * @OCL entireScope
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION
				.getEOperations().get(1);
		if (getScopeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				getScopeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(getScopeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MABSTRACT_EXPRESSION,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MAbstractBaseDefinition>) xoclEval
					.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
			return getAsCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
			return getAsBasicCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR:
			if (resolve)
				return getCollector();
			return basicGetCollector();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE:
			return getEntireScope();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE:
			return getScopeBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF:
			return getScopeSelf();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG:
			return getScopeTrg();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ:
			return getScopeObj();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getScopeSimpleTypeConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS:
			return getScopeLiteralConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getScopeObjectReferenceConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES:
			return getScopeVariables();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR:
			return getScopeIterator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR:
			return getScopeAccumulator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS:
			return getScopeParameters();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE:
			return getScopeContainerBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE:
			return getScopeNumberBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE:
			return getScopeSource();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE:
			return getLocalEntireScope();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE:
			return getLocalScopeBase();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF:
			return getLocalScopeSelf();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG:
			return getLocalScopeTrg();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ:
			return getLocalScopeObj();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE:
			return getLocalScopeSource();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return getLocalScopeSimpleTypeConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return getLocalScopeLiteralConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return getLocalScopeObjectReferenceConstants();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES:
			return getLocalScopeVariables();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR:
			return getLocalScopeIterator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR:
			return getLocalScopeAccumulator();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS:
			return getLocalScopeParameters();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return getLocalScopeNumberBaseDefinition();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER:
			return getLocalScopeContainer();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION:
			if (resolve)
				return getContainingAnnotation();
			return basicGetContainingAnnotation();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION:
			if (resolve)
				return getContainingExpression();
			return basicGetContainingExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
			return getIsComplexExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
			return getIsSubExpression();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE:
			if (resolve)
				return getCalculatedOwnType();
			return basicGetCalculatedOwnType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
			return getCalculatedOwnMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
			return getCalculatedOwnSingular();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE:
			return getCalculatedOwnSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE:
			if (resolve)
				return getSelfObjectType();
			return basicGetSelfObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE:
			if (resolve)
				return getSelfObjectPackage();
			return basicGetSelfObjectPackage();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE:
			if (resolve)
				return getTargetObjectType();
			return basicGetTargetObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE:
			return getTargetSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE:
			if (resolve)
				return getObjectObjectType();
			return basicGetObjectObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE:
			if (resolve)
				return getExpectedReturnType();
			return basicGetExpectedReturnType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE:
			return getExpectedReturnSimpleType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
			return getIsReturnValueMandatory();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
			return getIsReturnValueSingular();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE:
			if (resolve)
				return getSrcObjectType();
			return basicGetSrcObjectType();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE:
			return getSrcObjectSimpleType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
			return AS_CODE_EDEFAULT == null ? getAsCode() != null
					: !AS_CODE_EDEFAULT.equals(getAsCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
			return AS_BASIC_CODE_EDEFAULT == null ? getAsBasicCode() != null
					: !AS_BASIC_CODE_EDEFAULT.equals(getAsBasicCode());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__COLLECTOR:
			return basicGetCollector() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__ENTIRE_SCOPE:
			return !getEntireScope().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_BASE:
			return !getScopeBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SELF:
			return !getScopeSelf().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_TRG:
			return !getScopeTrg().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJ:
			return !getScopeObj().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getScopeSimpleTypeConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS:
			return !getScopeLiteralConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getScopeObjectReferenceConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_VARIABLES:
			return !getScopeVariables().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ITERATOR:
			return !getScopeIterator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR:
			return !getScopeAccumulator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS:
			return !getScopeParameters().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE:
			return !getScopeContainerBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE:
			return !getScopeNumberBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SCOPE_SOURCE:
			return !getScopeSource().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE:
			return !getLocalEntireScope().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE:
			return !getLocalScopeBase().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF:
			return !getLocalScopeSelf().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG:
			return !getLocalScopeTrg().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ:
			return !getLocalScopeObj().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE:
			return !getLocalScopeSource().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS:
			return !getLocalScopeSimpleTypeConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS:
			return !getLocalScopeLiteralConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS:
			return !getLocalScopeObjectReferenceConstants().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES:
			return !getLocalScopeVariables().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR:
			return !getLocalScopeIterator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR:
			return !getLocalScopeAccumulator().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS:
			return !getLocalScopeParameters().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION:
			return !getLocalScopeNumberBaseDefinition().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER:
			return !getLocalScopeContainer().isEmpty();
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION:
			return basicGetContainingAnnotation() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION:
			return basicGetContainingExpression() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
			return IS_COMPLEX_EXPRESSION_EDEFAULT == null
					? getIsComplexExpression() != null
					: !IS_COMPLEX_EXPRESSION_EDEFAULT
							.equals(getIsComplexExpression());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
			return IS_SUB_EXPRESSION_EDEFAULT == null
					? getIsSubExpression() != null
					: !IS_SUB_EXPRESSION_EDEFAULT.equals(getIsSubExpression());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE:
			return basicGetCalculatedOwnType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
			return CALCULATED_OWN_MANDATORY_EDEFAULT == null
					? getCalculatedOwnMandatory() != null
					: !CALCULATED_OWN_MANDATORY_EDEFAULT
							.equals(getCalculatedOwnMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
			return CALCULATED_OWN_SINGULAR_EDEFAULT == null
					? getCalculatedOwnSingular() != null
					: !CALCULATED_OWN_SINGULAR_EDEFAULT
							.equals(getCalculatedOwnSingular());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE:
			return getCalculatedOwnSimpleType() != CALCULATED_OWN_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE:
			return basicGetSelfObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE:
			return basicGetSelfObjectPackage() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE:
			return basicGetTargetObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE:
			return getTargetSimpleType() != TARGET_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE:
			return basicGetObjectObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE:
			return basicGetExpectedReturnType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE:
			return getExpectedReturnSimpleType() != EXPECTED_RETURN_SIMPLE_TYPE_EDEFAULT;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
			return IS_RETURN_VALUE_MANDATORY_EDEFAULT == null
					? getIsReturnValueMandatory() != null
					: !IS_RETURN_VALUE_MANDATORY_EDEFAULT
							.equals(getIsReturnValueMandatory());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
			return IS_RETURN_VALUE_SINGULAR_EDEFAULT == null
					? getIsReturnValueSingular() != null
					: !IS_RETURN_VALUE_SINGULAR_EDEFAULT
							.equals(getIsReturnValueSingular());
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE:
			return basicGetSrcObjectType() != null;
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE:
			return getSrcObjectSimpleType() != SRC_OBJECT_SIMPLE_TYPE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SHORT_CODE:
			return getShortCode();
		case ExpressionsPackage.MABSTRACT_EXPRESSION___GET_SCOPE:
			return getScope();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'OVERRIDE IN SUBCLASS'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedType if (let e: Boolean = collector.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then calculatedOwnType
	else if collector.oclIsUndefined()
	then null
	else collector.calculatedType
	endif
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedMandatory if (let e: Boolean = collector.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then calculatedOwnMandatory
	else if collector.oclIsUndefined()
	then null
	else collector.calculatedMandatory
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular if (let e: Boolean = collector.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then calculatedOwnSingular
	else if collector.oclIsUndefined()
	then null
	else collector.calculatedSingular
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSimpleType if (let e: Boolean = collector.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then calculatedOwnSimpleType
	else if collector.oclIsUndefined()
	then null
	else collector.calculatedSimpleType
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MABSTRACT_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MAbstractExpressionImpl
