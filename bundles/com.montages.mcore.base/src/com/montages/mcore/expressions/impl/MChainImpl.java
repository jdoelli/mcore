/**
 */
package com.montages.mcore.expressions.impl;

import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAccumulator;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MCallArgument;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MChainAction;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MDataValueExpr;
import com.montages.mcore.expressions.MElse;
import com.montages.mcore.expressions.MElseIf;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MLiteralValueExpr;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MSubChain;
import com.montages.mcore.expressions.MThen;

import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MChain</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MChainImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MChainImpl extends MBaseChainImpl implements MChain {
	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MChainAction DO_ACTION_EDEFAULT = MChainAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateexpressionsMChainActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MChainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MCHAIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChainAction getDoAction() {
		/**
		 * @OCL mcore::expressions::MChainAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MCHAIN;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MCHAIN__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MCHAIN, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MCHAIN, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MChainAction result = (MChainAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoAction(MChainAction newDoAction) {
		if (newDoAction == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doAction$Update(MChainAction obj) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		EEnumLiteral eEnumLiteralTrigger = McorePackage.Literals.MPROPERTY_ACTION
				.getEEnumLiteral(obj.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_DoAction(),
				XUpdateMode.REDEFINE, null,
				//:=
				eEnumLiteralTrigger, null, null);

		switch (obj.getValue()) {
		case MChainAction.SUB_CHAIN_VALUE:
			MSubChain subChain = ExpressionsFactory.eINSTANCE.createMSubChain();
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.Literals.MBASE_CHAIN__SUB_EXPRESSION,
					XUpdateMode.ADD, XAddUpdateMode.LAST, subChain, null, null);
			return currentTrigger;

		case MChainAction.COLLECTION_OP_VALUE:
			MCollectionExpression collector = (MCollectionExpression) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.Literals.MBASE_CHAIN__CONTAINED_COLLECTOR,
					XUpdateMode.REDEFINE, null, collector, null, null);
			transition.getFocusObjects()
					.add(currentTrigger.nextStateObjectDefinitionFromObject(
							collector.getExpression()));

			return currentTrigger;

		case MChainAction.ARGUMENT_VALUE:
			MCallArgument newArg = (MCallArgument) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCALL_ARGUMENT);
			currentTrigger.addReferenceUpdate(this,
					ExpressionsPackage.eINSTANCE.getMBaseChain_CallArgument(),
					XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null, null);
			return currentTrigger;

		case MChainAction.INTO_APPLY_VALUE:
			MApplication newApply = (MApplication) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MAPPLICATION);

			if (this.eContainer() instanceof MApplication) {
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE.getMApplication_Operands(),
						XUpdateMode.REMOVE, null, this, null, null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
						XUpdateMode.ADD, XAddUpdateMode.LAST, newApply, null,
						null);

			} else {
				if (this.eContainer() instanceof MNamedExpression) {
					currentTrigger.addReferenceUpdate(this.eContainer(),
							ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
							XUpdateMode.REDEFINE, null, newApply, null, null);

				} else {
					if (this.eContainer() instanceof MIf) {
						currentTrigger.addReferenceUpdate(this.eContainer(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractIf_Condition(),
								XUpdateMode.REDEFINE, null, newApply, null,
								null);

					} else {
						if (this.eContainer() instanceof MThen) {
							currentTrigger.addReferenceUpdate(this.eContainer(),
									ExpressionsPackage.eINSTANCE
											.getMThen_Expression(),
									XUpdateMode.REDEFINE, null, newApply, null,
									null);

						} else {
							if (this.eContainer() instanceof MElse) {
								currentTrigger.addReferenceUpdate(
										this.eContainer(),
										ExpressionsPackage.eINSTANCE
												.getMElse_Expression(),
										XUpdateMode.REDEFINE, null, newApply,
										null, null);

							} else {
								if (this.eContainer() instanceof MElseIf) {
									currentTrigger.addReferenceUpdate(
											this.eContainer(),
											ExpressionsPackage.eINSTANCE
													.getMElseIf_Condition(),
											XUpdateMode.REDEFINE, null,
											newApply, null, null);

								} else {
									if (this.eContainer() instanceof MCollectionExpression) {
										currentTrigger.addReferenceUpdate(
												this.eContainer(),
												ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
												XUpdateMode.REDEFINE, null,
												newApply, null, null);

									} else {
										if (this.eContainer() instanceof MAccumulator) {
											currentTrigger.addReferenceUpdate(
													this.eContainer(),
													ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
													XUpdateMode.REDEFINE, null,
													newApply, null, null);

										}
									}
								}
							}
						}
					}
				}
			}

			currentTrigger.addReferenceUpdate(newApply,
					ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
					XUpdateMode.ADD, XAddUpdateMode.LAST, this, null, null);

			return currentTrigger;

		case MChainAction.INTO_DATA_CONSTANT_VALUE:
			MDataValueExpr newData = (MDataValueExpr) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MDATA_VALUE_EXPR);
			if (this.getLastElement() != null && this.getLastElement()
					.getSimpleType() != SimpleType.NONE) {
				newData.setSimpleType(this.getLastElement().getSimpleType());
			}

			if (this.eContainer() instanceof MApplication) {
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
						XUpdateMode.ADD, XAddUpdateMode.LAST, newData, null,
						null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
						XUpdateMode.REMOVE, null, this, null, null);

			} else {
				if (this.eContainer() instanceof MNamedExpression) {
					currentTrigger.addReferenceUpdate(this.eContainer(),
							ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
							XUpdateMode.REDEFINE, null, newData, null, null);

				} else {
					if (this.eContainer() instanceof MIf) {
						currentTrigger.addReferenceUpdate(this.eContainer(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractIf_Condition(),
								XUpdateMode.REDEFINE, null, newData, null,
								null);

					} else {
						if (this.eContainer() instanceof MThen) {
							currentTrigger.addReferenceUpdate(this.eContainer(),
									ExpressionsPackage.eINSTANCE
											.getMThen_Expression(),
									XUpdateMode.REDEFINE, null, newData, null,
									null);

						} else {
							if (this.eContainer() instanceof MElse) {
								currentTrigger.addReferenceUpdate(
										this.eContainer(),
										ExpressionsPackage.eINSTANCE
												.getMElse_Expression(),
										XUpdateMode.REDEFINE, null, newData,
										null, null);

							} else {
								if (this.eContainer() instanceof MElseIf) {
									currentTrigger.addReferenceUpdate(
											this.eContainer(),
											ExpressionsPackage.eINSTANCE
													.getMElseIf_Condition(),
											XUpdateMode.REDEFINE, null, newData,
											null, null);

								} else {
									if (this.eContainer() instanceof MCollectionExpression) {
										currentTrigger.addReferenceUpdate(
												this.eContainer(),
												ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
												XUpdateMode.REDEFINE, null,
												newData, null, null);

									} else {
										if (this.eContainer() instanceof MAccumulator) {
											currentTrigger.addReferenceUpdate(
													this.eContainer(),
													ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
													XUpdateMode.REDEFINE, null,
													newData, null, null);

										}
									}
								}
							}
						}
					}
				}
			}

			return currentTrigger;

		case MChainAction.INTO_LITERAL_CONSTANT_VALUE:
			MLiteralValueExpr newLiteral = (MLiteralValueExpr) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MLITERAL_VALUE_EXPR);

			if (this.eContainer() instanceof MApplication) {
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
						XUpdateMode.ADD, XAddUpdateMode.LAST, newLiteral, null,
						null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
						XUpdateMode.REMOVE, null, this, null, null);

			} else {
				if (this.eContainer() instanceof MNamedExpression) {
					currentTrigger.addReferenceUpdate(this.eContainer(),
							ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
							XUpdateMode.REDEFINE, null, newLiteral, null, null);

				} else {
					if (this.eContainer() instanceof MIf) {
						currentTrigger.addReferenceUpdate(this.eContainer(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractIf_Condition(),
								XUpdateMode.REDEFINE, null, newLiteral, null,
								null);

					} else {
						if (this.eContainer() instanceof MThen) {
							currentTrigger.addReferenceUpdate(this.eContainer(),
									ExpressionsPackage.eINSTANCE
											.getMThen_Expression(),
									XUpdateMode.REDEFINE, null, newLiteral,
									null, null);

						} else {
							if (this.eContainer() instanceof MElse) {
								currentTrigger.addReferenceUpdate(
										this.eContainer(),
										ExpressionsPackage.eINSTANCE
												.getMElse_Expression(),
										XUpdateMode.REDEFINE, null, newLiteral,
										null, null);

							} else {
								if (this.eContainer() instanceof MElseIf) {
									currentTrigger.addReferenceUpdate(
											this.eContainer(),
											ExpressionsPackage.eINSTANCE
													.getMElseIf_Condition(),
											XUpdateMode.REDEFINE, null,
											newLiteral, null, null);

								} else {
									if (this.eContainer() instanceof MCollectionExpression) {
										currentTrigger.addReferenceUpdate(
												this.eContainer(),
												ExpressionsPackage.Literals.MCOLLECTION_EXPRESSION__EXPRESSION,
												XUpdateMode.REDEFINE, null,
												newLiteral, null, null);

									} else {
										if (this.eContainer() instanceof MAccumulator) {
											currentTrigger.addReferenceUpdate(
													this.eContainer(),
													ExpressionsPackage.Literals.MACCUMULATOR__ACC_DEFINITION,
													XUpdateMode.REDEFINE, null,
													newLiteral, null, null);

										}
									}
								}
							}
						}
					}
				}
			}

			return currentTrigger;

		case MChainAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE:
		case MChainAction.INTO_ELSE_OF_IF_THEN_ELSE_VALUE:
		case MChainAction.INTO_COND_OF_IF_THEN_ELSE_VALUE:
			//Creates if-then-else statement
			MIf newIf = (MIf) currentTrigger
					.createNextStateNewObject(ExpressionsPackage.Literals.MIF);

			MThen thenPart = (MThen) currentTrigger.createNextStateNewObject(
					ExpressionsPackage.Literals.MTHEN);
			currentTrigger.addReferenceUpdate(newIf,
					ExpressionsPackage.eINSTANCE.getMAbstractIf_ThenPart(),
					XUpdateMode.REDEFINE, null, thenPart, null, null);

			MElse elsePart = (MElse) currentTrigger.createNextStateNewObject(
					ExpressionsPackage.Literals.MELSE);
			currentTrigger.addReferenceUpdate(newIf,
					ExpressionsPackage.eINSTANCE.getMAbstractIf_ElsePart(),
					XUpdateMode.REDEFINE, null, elsePart, null, null);

			if (obj.getValue() == MChainAction.INTO_COND_OF_IF_THEN_ELSE_VALUE) {
				currentTrigger.addReferenceUpdate(newIf,
						ExpressionsPackage.eINSTANCE.getMAbstractIf_Condition(),
						XUpdateMode.REDEFINE, null, this, null, null);

				MChain thenChain = (MChain) transition.createNextStateNewObject(
						ExpressionsPackage.Literals.MCHAIN);

				currentTrigger.addReferenceUpdate(thenPart,
						ExpressionsPackage.eINSTANCE.getMThen_Expression(),
						XUpdateMode.REDEFINE, null, thenChain, null, null);

				MChain elseChain = (MChain) transition.createNextStateNewObject(
						ExpressionsPackage.Literals.MCHAIN);

				currentTrigger.addReferenceUpdate(elsePart,
						ExpressionsPackage.eINSTANCE.getMElse_Expression(),
						XUpdateMode.REDEFINE, null, elseChain, null, null);
			} else {
				if (obj.getValue() == MChainAction.INTO_THEN_OF_IF_THEN_ELSE_VALUE) {
					MChain ifChain = (MChain) transition
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);

					currentTrigger.addReferenceUpdate(newIf,
							ExpressionsPackage.eINSTANCE
									.getMAbstractIf_Condition(),
							XUpdateMode.REDEFINE, null, ifChain, null, null);

					currentTrigger.addReferenceUpdate(thenPart,
							ExpressionsPackage.eINSTANCE.getMThen_Expression(),
							XUpdateMode.REDEFINE, null, this, null, null);

					MChain elseChain = (MChain) transition
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);

					currentTrigger.addReferenceUpdate(elsePart,
							ExpressionsPackage.eINSTANCE.getMElse_Expression(),
							XUpdateMode.REDEFINE, null, elseChain, null, null);
				} else {
					MChain ifChain = (MChain) transition
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);

					currentTrigger.addReferenceUpdate(newIf,
							ExpressionsPackage.eINSTANCE
									.getMAbstractIf_Condition(),
							XUpdateMode.REDEFINE, null, ifChain, null, null);

					MChain thenChain = (MChain) transition
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);

					currentTrigger.addReferenceUpdate(thenPart,
							ExpressionsPackage.eINSTANCE.getMThen_Expression(),
							XUpdateMode.REDEFINE, null, thenChain, null, null);

					currentTrigger.addReferenceUpdate(elsePart,
							ExpressionsPackage.eINSTANCE.getMElse_Expression(),
							XUpdateMode.REDEFINE, null, this, null, null);
				}
			}
			transition.getFocusObjects().add(currentTrigger
					.nextStateObjectDefinitionFromObject(newIf.getCondition()));

			//Checks where the application is contained
			if (this.eContainer() instanceof MNamedExpression) {
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
						XUpdateMode.REDEFINE, null, newIf, null, null);
			} else {
				MNamedExpression newWhere = (MNamedExpression) currentTrigger
						.createNextStateNewObject(
								ExpressionsPackage.Literals.MNAMED_EXPRESSION);

				currentTrigger.addReferenceUpdate(
						this.getContainingAnnotation(),
						AnnotationsPackage.Literals.MEXPR_ANNOTATION__NAMED_EXPRESSION,
						XUpdateMode.ADD, XAddUpdateMode.LAST, newWhere, null,
						null);
				currentTrigger.addAttributeUpdate(newWhere,
						McorePackage.Literals.MNAMED__NAME,
						XUpdateMode.REDEFINE, null,
						"Var ".concat(
								String.valueOf(this.getContainingAnnotation()
										.getNamedExpression().size())),
						null, null);
				currentTrigger.addReferenceUpdate(newWhere,
						ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION,
						XUpdateMode.REDEFINE, null, newIf, null, null);
				MChain newChain = (MChain) currentTrigger
						.createNextStateNewObject(
								ExpressionsPackage.Literals.MCHAIN);

				currentTrigger.addAttributeUpdate(newChain,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						XUpdateMode.REDEFINE, null,
						ExpressionsPackage.Literals.EXPRESSION_BASE
								.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
						null, null);

				currentTrigger.addReferenceUpdate(newChain,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						XUpdateMode.REDEFINE, null, newWhere, null, null);

				if (this.eContainer() instanceof MIf) {
					currentTrigger.addReferenceUpdate(this.eContainer(),
							ExpressionsPackage.eINSTANCE
									.getMAbstractIf_Condition(),
							XUpdateMode.REDEFINE, null, newChain, null, null);
				} else {
					if (this.eContainer() instanceof MThen) {
						currentTrigger.addReferenceUpdate(this.eContainer(),
								ExpressionsPackage.eINSTANCE
										.getMThen_Expression(),
								XUpdateMode.REDEFINE, null, newChain, null,
								null);
					} else {
						if (this.eContainer() instanceof MElse) {
							currentTrigger.addReferenceUpdate(this.eContainer(),
									ExpressionsPackage.eINSTANCE
											.getMElse_Expression(),
									XUpdateMode.REDEFINE, null, newChain, null,
									null);
						} else
							currentTrigger.addReferenceUpdate(this.eContainer,
									ExpressionsPackage.eINSTANCE
											.getMApplication_Operands(),
									XUpdateMode.ADD, XAddUpdateMode.LAST,
									newChain, null, null);
					}
				}

			}

			return currentTrigger;

		default: {
			com.montages.mcore.expressions.MChainAction triggerValue = obj;
			currentTrigger = transition.addAttributeUpdate

			(this, ExpressionsPackage.eINSTANCE.getMChain_DoAction(),
					org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
					null, null);

			return null;
		}
		}
		// Auto Generated XSemantics;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MCHAIN__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MCHAIN__DO_ACTION:
			setDoAction((MChainAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCHAIN__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MCHAIN__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MCHAIN___DO_ACTION$_UPDATE__MCHAINACTION:
			return doAction$Update((MChainAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

} //MChainImpl
