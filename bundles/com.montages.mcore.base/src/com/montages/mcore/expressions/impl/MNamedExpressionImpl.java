/**
 */
package com.montages.mcore.expressions.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MCallArgument;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MNamedExpressionAction;
import com.montages.mcore.expressions.MToplevelExpression;
import com.montages.mcore.expressions.MVariableBaseDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MNamed Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MNamedExpressionImpl#getExpression <em>Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MNamedExpressionImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MNamedExpressionImpl extends MAbstractLetImpl
		implements MNamedExpression, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getExpression() <em>Expression</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpression()
	 * @generated
	 * @ordered
	 */
	protected MToplevelExpression expression;

	/**
	 * This is true if the Expression containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean expressionESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MNamedExpressionAction DO_ACTION_EDEFAULT = MNamedExpressionAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateexpressionsMNamedExpressionActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #defaultValue <em>Default Value</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #defaultValue
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression defaultValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #allApplications <em>All Applications</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #allApplications
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression allApplicationsBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnType <em>Calculated Own Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnTypeDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNamedExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MNAMED_EXPRESSION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MToplevelExpression getExpression() {
		if (expression != null && expression.eIsProxy()) {
			InternalEObject oldExpression = (InternalEObject) expression;
			expression = (MToplevelExpression) eResolveProxy(oldExpression);
			if (expression != oldExpression) {
				InternalEObject newExpression = (InternalEObject) expression;
				NotificationChain msgs = oldExpression.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
						null, null);
				if (newExpression.eInternalContainer() == null) {
					msgs = newExpression.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
							oldExpression, expression));
			}
		}
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MToplevelExpression basicGetExpression() {
		return expression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetExpression(
			MToplevelExpression newExpression, NotificationChain msgs) {
		MToplevelExpression oldExpression = expression;
		expression = newExpression;
		boolean oldExpressionESet = expressionESet;
		expressionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
					oldExpression, newExpression, !oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExpression(MToplevelExpression newExpression) {
		if (newExpression != expression) {
			NotificationChain msgs = null;
			if (expression != null)
				msgs = ((InternalEObject) expression).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
						null, msgs);
			if (newExpression != null)
				msgs = ((InternalEObject) newExpression).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
						null, msgs);
			msgs = basicSetExpression(newExpression, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
						newExpression, newExpression, !oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetExpression(NotificationChain msgs) {
		MToplevelExpression oldExpression = expression;
		expression = null;
		boolean oldExpressionESet = expressionESet;
		expressionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
					oldExpression, null, oldExpressionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetExpression() {
		if (expression != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) expression).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION,
					null, msgs);
			msgs = basicUnsetExpression(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldExpressionESet = expressionESet;
			expressionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION, null,
						null, oldExpressionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetExpression() {
		return expressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedExpressionAction getDoAction() {
		/**
		 * @OCL mcore::expressions::MNamedExpressionAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MNAMED_EXPRESSION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MNAMED_EXPRESSION__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MNAMED_EXPRESSION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MNamedExpressionAction result = (MNamedExpressionAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoAction(MNamedExpressionAction newDoAction) {
		if (newDoAction == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doAction$Update(MNamedExpressionAction obj) {

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();

		EEnumLiteral eEnumLiteralTrigger = McorePackage.Literals.MPROPERTY_ACTION
				.getEEnumLiteral(obj.getValue());
		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_DoAction(),
				XUpdateMode.REDEFINE, null,
				//:=
				eEnumLiteralTrigger, null, null);

		switch (obj.getValue()) {
		case MNamedExpressionAction.AS_BASE_IN_MAIN_CHAIN_VALUE: {
			currentTrigger.addAttributeUpdate(this.eContainer(),
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					null, ExpressionsPackage.Literals.EXPRESSION_BASE
							.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
					null, null);

			currentTrigger.addReferenceUpdate(this.eContainer(),
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, null, this, null, null);
			return currentTrigger;
		}
		case MNamedExpressionAction.AS_ARGUMENT_IN_MAIN_CHAIN_VALUE: {
			MCallArgument newArg = (MCallArgument) currentTrigger
					.createNextStateNewObject(
							ExpressionsPackage.Literals.MCALL_ARGUMENT);

			currentTrigger.addReferenceUpdate(this.eContainer(),
					ExpressionsPackage.eINSTANCE.getMBaseChain_CallArgument(),
					XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null, null);

			currentTrigger.addAttributeUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE,
					null, ExpressionsPackage.Literals.EXPRESSION_BASE
							.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
					null, null);

			currentTrigger.addReferenceUpdate(newArg,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_BaseVar(),
					XUpdateMode.REDEFINE, null, this, null, null);

			return currentTrigger;
		}
		case MNamedExpressionAction.AS_BASE_IN_PREVIOUS_CHAIN_VALUE:
		case MNamedExpressionAction.AS_ARGUMENT_IN_PREVIOUS_CHAIN_VALUE: {
			int number = 0;
			ArrayList<MNamedExpression> allExpressions = new ArrayList<MNamedExpression>();
			for (MVariableBaseDefinition mBase : this.getContainingAnnotation()
					.getLocalScopeVariables()) {
				allExpressions.add(mBase.getNamedExpression());
				if (mBase.getNamedExpression().equals(this)) {
					number = allExpressions.size() - 1;
					break;
				}
			}

			for (int i = number - 1; i == 0; i--) {
				if (allExpressions.get(i).getExpression() instanceof MChain) {
					if (obj.getValue() == MNamedExpressionAction.AS_BASE_IN_PREVIOUS_CHAIN_VALUE) {
						currentTrigger.addAttributeUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractExpressionWithBase_Base(),
								XUpdateMode.REDEFINE, null,
								ExpressionsPackage.Literals.EXPRESSION_BASE
										.getEEnumLiteral(
												ExpressionBase.VARIABLE_VALUE),
								null, null);
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractExpressionWithBase_BaseVar(),
								XUpdateMode.REDEFINE, null, this, null, null);

						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractChain_Element1(),
								XUpdateMode.REDEFINE, null, "", null, null);
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractChain_Element2(),
								XUpdateMode.REDEFINE, null, "", null, null);
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractChain_Element3(),
								XUpdateMode.REDEFINE, null, "", null, null);
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractChain_CastType(),
								XUpdateMode.REDEFINE, null, "", null, null);
						return currentTrigger;
					} else {
						MCallArgument newArg = (MCallArgument) currentTrigger
								.createNextStateNewObject(
										ExpressionsPackage.Literals.MCALL_ARGUMENT);
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMBaseChain_CallArgument(),
								XUpdateMode.ADD, XAddUpdateMode.LAST, newArg,
								null, null);
						currentTrigger.addAttributeUpdate(newArg,
								ExpressionsPackage.eINSTANCE
										.getMAbstractExpressionWithBase_Base(),
								XUpdateMode.REDEFINE, null,
								ExpressionsPackage.Literals.EXPRESSION_BASE
										.getEEnumLiteral(
												ExpressionBase.VARIABLE_VALUE),
								null, null);

						currentTrigger.addReferenceUpdate(newArg,
								ExpressionsPackage.eINSTANCE
										.getMAbstractExpressionWithBase_BaseVar(),
								XUpdateMode.REDEFINE, null, this, null, null);
						return currentTrigger;
					}
				}
			}

			//No MNamedExpression has just a chain as base so it will be set in the main chain
			if (obj.getValue() == MNamedExpressionAction.AS_BASE_IN_PREVIOUS_CHAIN_VALUE) {
				currentTrigger.addAttributeUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						XUpdateMode.REDEFINE, null,
						ExpressionsPackage.Literals.EXPRESSION_BASE
								.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
						null, null);

				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						XUpdateMode.REDEFINE, null, this, null, null);

				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractChain_Element1(),
						XUpdateMode.REDEFINE, null, "", null, null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractChain_Element2(),
						XUpdateMode.REDEFINE, null, "", null, null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractChain_Element3(),
						XUpdateMode.REDEFINE, null, "", null, null);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMAbstractChain_CastType(),
						XUpdateMode.REDEFINE, null, "", null, null);
				return currentTrigger;
			} else {
				MCallArgument newArg = (MCallArgument) currentTrigger
						.createNextStateNewObject(
								ExpressionsPackage.Literals.MCALL_ARGUMENT);
				currentTrigger.addReferenceUpdate(this.eContainer(),
						ExpressionsPackage.eINSTANCE
								.getMBaseChain_CallArgument(),
						XUpdateMode.ADD, XAddUpdateMode.LAST, newArg, null,
						null);
				currentTrigger.addAttributeUpdate(newArg,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_Base(),
						XUpdateMode.REDEFINE, null,
						ExpressionsPackage.Literals.EXPRESSION_BASE
								.getEEnumLiteral(ExpressionBase.VARIABLE_VALUE),
						null, null);

				currentTrigger.addReferenceUpdate(newArg,
						ExpressionsPackage.eINSTANCE
								.getMAbstractExpressionWithBase_BaseVar(),
						XUpdateMode.REDEFINE, null, this, null, null);
				return currentTrigger;
			}
		}
		case MNamedExpressionAction.AS_OPERAND_IN_PREVIOUS_APPLY_VALUE: {
			int number = 0;
			ArrayList<MNamedExpression> allExpressions = new ArrayList<MNamedExpression>();
			for (MVariableBaseDefinition mBase : this.getContainingAnnotation()
					.getLocalScopeVariables()) {
				allExpressions.add(mBase.getNamedExpression());
				if (mBase.getNamedExpression().equals(this)) {
					number = allExpressions.size() - 1;
					break;
				}
			}

			for (int i = number - 1; i == 0; i--) {
				if (allExpressions.get(i)
						.getExpression() instanceof MApplication) {
					MChain newChain = (MChain) currentTrigger
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);
					currentTrigger.addReferenceUpdate(
							allExpressions.get(i).getExpression(),
							ExpressionsPackage.eINSTANCE
									.getMApplication_Operands(),
							XUpdateMode.ADD, XAddUpdateMode.LAST, newChain,
							null, null);
					currentTrigger.addAttributeUpdate(newChain,
							ExpressionsPackage.eINSTANCE
									.getMAbstractExpressionWithBase_Base(),
							XUpdateMode.REDEFINE, null,
							ExpressionsPackage.Literals.EXPRESSION_BASE
									.getEEnumLiteral(
											ExpressionBase.VARIABLE_VALUE),
							null, null);
					currentTrigger.addReferenceUpdate(newChain,
							ExpressionsPackage.eINSTANCE
									.getMAbstractExpressionWithBase_BaseVar(),
							XUpdateMode.REDEFINE, null, this, null, null);
					return currentTrigger;
				}
			}
			return currentTrigger;
		}

		case MNamedExpressionAction.AS_COND_IN_PREVIOUS_IF_VALUE:
		case MNamedExpressionAction.AS_THEN_IN_PREVIOUS_IF_VALUE:
		case MNamedExpressionAction.AS_ELSE_IN_PREVIOUS_IF_VALUE: {
			int number = 0;
			ArrayList<MNamedExpression> allExpressions = new ArrayList<MNamedExpression>();
			for (MVariableBaseDefinition mBase : this.getContainingAnnotation()
					.getLocalScopeVariables()) {
				allExpressions.add(mBase.getNamedExpression());
				if (mBase.getNamedExpression().equals(this)) {
					number = allExpressions.size() - 1;
					break;
				}
			}

			for (int i = number - 1; i == 0; i--) {
				if (allExpressions.get(i).getExpression() instanceof MIf) {
					MChain newChain = (MChain) currentTrigger
							.createNextStateNewObject(
									ExpressionsPackage.Literals.MCHAIN);

					currentTrigger.addAttributeUpdate(newChain,
							ExpressionsPackage.eINSTANCE
									.getMAbstractExpressionWithBase_Base(),
							XUpdateMode.REDEFINE, null,
							ExpressionsPackage.Literals.EXPRESSION_BASE
									.getEEnumLiteral(
											ExpressionBase.VARIABLE_VALUE),
							null, null);
					currentTrigger.addReferenceUpdate(newChain,
							ExpressionsPackage.eINSTANCE
									.getMAbstractExpressionWithBase_BaseVar(),
							XUpdateMode.REDEFINE, null, this, null, null);

					if (obj.getValue() == MNamedExpressionAction.AS_COND_IN_PREVIOUS_IF_VALUE) {
						currentTrigger.addReferenceUpdate(
								allExpressions.get(i).getExpression(),
								ExpressionsPackage.eINSTANCE
										.getMAbstractIf_Condition(),
								XUpdateMode.REDEFINE, null, newChain, null,
								null);
						return currentTrigger;
					} else {
						if (obj.getValue() == MNamedExpressionAction.AS_THEN_IN_PREVIOUS_IF_VALUE) {
							currentTrigger.addReferenceUpdate(
									((MIf) allExpressions.get(i)
											.getExpression()).getThenPart(),
									ExpressionsPackage.eINSTANCE
											.getMThen_Expression(),
									XUpdateMode.REDEFINE, null, newChain, null,
									null);
							return currentTrigger;
						} else {
							currentTrigger.addReferenceUpdate(
									((MIf) allExpressions.get(i)
											.getExpression()).getElsePart(),
									ExpressionsPackage.eINSTANCE
											.getMElse_Expression(),
									XUpdateMode.REDEFINE, null, newChain, null,
									null);
							return currentTrigger;
						}
					}
				}
			}
			return currentTrigger;
		}

		default:
			// Auto Generated XSemantics;

			transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
					.createXTransition();
			com.montages.mcore.expressions.MNamedExpressionAction triggerValue = obj;
			currentTrigger = transition.addAttributeUpdate

			(this, ExpressionsPackage.eINSTANCE.getMNamedExpression_DoAction(),
					org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
					null, null);

			return currentTrigger;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChain defaultValue() {

		/**
		 * @OCL Tuple{base=ExpressionBase::SelfObject}
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MNAMED_EXPRESSION
				.getEOperations().get(1);
		if (defaultValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				defaultValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MNAMED_EXPRESSION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(defaultValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MChain) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MApplication> allApplications() {

		/**
		 * @OCL let all: OrderedSet(MApplication) = 
		self.expression->asOrderedSet()->first().oclAsType(MApplication)->asSequence()->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))->union(self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication)->asSequence())->asOrderedSet()
		
		--self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication) ->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))
		--->reject(oclIsUndefined())->asOrderedSet() 
		--->append(self.expression->asOrderedSet()->first().oclAsType(MApplication))->asOrderedSet()
		-->reject(oclIsUndefined())->asOrderedSet()
		in if all->oclIsUndefined() then null else all endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EOperation eOperation = ExpressionsPackage.Literals.MNAMED_EXPRESSION
				.getEOperations().get(2);
		if (allApplicationsBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				allApplicationsBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, body,
						helper.getProblems(),
						ExpressionsPackage.Literals.MNAMED_EXPRESSION,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(allApplicationsBodyOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<MApplication>) xoclEval.evaluateElement(eOperation,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION:
			return basicUnsetExpression(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION:
			if (resolve)
				return getExpression();
			return basicGetExpression();
		case ExpressionsPackage.MNAMED_EXPRESSION__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION:
			setExpression((MToplevelExpression) newValue);
			return;
		case ExpressionsPackage.MNAMED_EXPRESSION__DO_ACTION:
			setDoAction((MNamedExpressionAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION:
			unsetExpression();
			return;
		case ExpressionsPackage.MNAMED_EXPRESSION__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MNAMED_EXPRESSION__EXPRESSION:
			return isSetExpression();
		case ExpressionsPackage.MNAMED_EXPRESSION__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case ExpressionsPackage.MNAMED_EXPRESSION___DO_ACTION$_UPDATE__MNAMEDEXPRESSIONACTION:
			return doAction$Update((MNamedExpressionAction) arguments.get(0));
		case ExpressionsPackage.MNAMED_EXPRESSION___DEFAULT_VALUE:
			return defaultValue();
		case ExpressionsPackage.MNAMED_EXPRESSION___ALL_APPLICATIONS:
			return allApplications();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Where'
	/*
	let c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in
	if c.oclIsUndefined() then 'Let'
	else if c.namedExpression->last() = self
		then 'Definition'
		else 'Let' endif
	endif
	*\/
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode if expression.oclIsUndefined() then 'MISSING EXPRESSION' else
	
	let c: String = expression.asCode in
	let t: String = 
	if   expression.oclIsKindOf(expressions::MChain) then
	if  expression.oclAsType(expressions::MChain).base = ExpressionBase::EContainer and expression.oclAsType(expressions::MChain).baseExitSimpleType = SimpleType::Object then 
	typeAsOcl( selfObjectPackage, expression.calculatedType, SimpleType::None, expression.calculatedSingular ) else
	
	typeAsOcl( selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular )
	endif
	else
	typeAsOcl( selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular )
	
	
	endif
	in
	if name.oclIsUndefined() or name='' 
	then /* The expression shoudl be returned here, as this is the final result *\/
	if expression.oclIsKindOf(MAbstractExpressionWithBase)
	then /* We do a special case for null...since it cannot simply be returned as such *\/
		let chain: MAbstractExpressionWithBase = expression.oclAsType(MAbstractExpressionWithBase) in
		if chain.base = ExpressionBase::NullValue
			then 'let nl: '.concat(  typeAsOcl(selfObjectPackage, containingAnnotation.expectedReturnTypeOfAnnotation, containingAnnotation.expectedReturnSimpleTypeOfAnnotation, self.isReturnValueSingular) ).concat(' = null in nl')
			else c endif
	else c endif
	else 'let '.concat(eName).concat(': ').concat(t).concat(' = ').concat(c).concat(' in')
	endif
	
	endif 
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory if expression.oclIsUndefined() then false
	else expression.calculatedMandatory endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular 
	if expression.oclIsUndefined() then true
	else if expression.oclIsTypeOf(expressions::MChain) 
	then  if expression.oclAsType(expressions::MChain).processor <> MProcessor::None then true else
	expression.calculatedSingular endif 
	else 
	expression.calculatedSingular
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSimpleType if expression.oclIsUndefined() then SimpleType::None
	else expression.calculatedSimpleType endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnType if expression.oclIsUndefined() then null
	else expression.calculatedType endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedOwnType() {
		EClass eClass = (ExpressionsPackage.Literals.MNAMED_EXPRESSION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE;

		if (calculatedOwnTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MNAMED_EXPRESSION__EXPRESSION };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MNAMED_EXPRESSION,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MNamedExpressionImpl
