/**
 */
package com.montages.mcore.expressions.impl;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MToplevelExpression;
import com.montages.mcore.expressions.MVariableBaseDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MVariable Base Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MVariableBaseDefinitionImpl#getNamedExpression <em>Named Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MVariableBaseDefinitionImpl#getVariableLet <em>Variable Let</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MVariableBaseDefinitionImpl extends MAbstractBaseDefinitionImpl
		implements MVariableBaseDefinition {
	/**
	 * The cached value of the '{@link #getNamedExpression() <em>Named Expression</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedExpression()
	 * @generated
	 * @ordered
	 */
	protected MNamedExpression namedExpression;

	/**
	 * This is true if the Named Expression reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean namedExpressionESet;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getVariableLet <em>Variable Let</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVariableLet
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression variableLetDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedBase <em>Calculated Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedBase
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedAsCode <em>Calculated As Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedAsCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedAsCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MVariableBaseDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedExpression getNamedExpression() {
		if (namedExpression != null && namedExpression.eIsProxy()) {
			InternalEObject oldNamedExpression = (InternalEObject) namedExpression;
			namedExpression = (MNamedExpression) eResolveProxy(
					oldNamedExpression);
			if (namedExpression != oldNamedExpression) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION,
							oldNamedExpression, namedExpression));
			}
		}
		return namedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedExpression basicGetNamedExpression() {
		return namedExpression;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamedExpression(MNamedExpression newNamedExpression) {
		MNamedExpression oldNamedExpression = namedExpression;
		namedExpression = newNamedExpression;
		boolean oldNamedExpressionESet = namedExpressionESet;
		namedExpressionESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION,
					oldNamedExpression, namedExpression,
					!oldNamedExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedExpression() {
		MNamedExpression oldNamedExpression = namedExpression;
		boolean oldNamedExpressionESet = namedExpressionESet;
		namedExpression = null;
		namedExpressionESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION,
					oldNamedExpression, null, oldNamedExpressionESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedExpression() {
		return namedExpressionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MToplevelExpression getVariableLet() {
		MToplevelExpression variableLet = basicGetVariableLet();
		return variableLet != null && variableLet.eIsProxy()
				? (MToplevelExpression) eResolveProxy(
						(InternalEObject) variableLet)
				: variableLet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MToplevelExpression basicGetVariableLet() {
		/**
		 * @OCL if namedExpression.expression.oclIsUndefined() then  null
		else if namedExpression.expression.oclIsKindOf(MToplevelExpression) 
		then namedExpression.expression.oclAsType(MToplevelExpression)
		else null endif 
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION;
		EStructuralFeature eFeature = ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION__VARIABLE_LET;

		if (variableLetDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				variableLetDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(variableLetDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MToplevelExpression result = (MToplevelExpression) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION:
			if (resolve)
				return getNamedExpression();
			return basicGetNamedExpression();
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__VARIABLE_LET:
			if (resolve)
				return getVariableLet();
			return basicGetVariableLet();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION:
			setNamedExpression((MNamedExpression) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION:
			unsetNamedExpression();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION:
			return isSetNamedExpression();
		case ExpressionsPackage.MVARIABLE_BASE_DEFINITION__VARIABLE_LET:
			return basicGetVariableLet() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL '<var> '.concat(if namedExpression.oclIsUndefined() then '' else namedExpression.eName endif)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedBase ExpressionBase::Variable
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public ExpressionBase getCalculatedBase() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_BASE;

		if (calculatedBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ExpressionBase) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedAsCode if (let e: Boolean = namedExpression.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then 'MISSING EXPRESSION'
	else if namedExpression.oclIsUndefined()
	then null
	else namedExpression.eName
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedAsCode() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_AS_CODE;

		if (calculatedAsCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedAsCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedAsCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedMandatory if (let e: Boolean = variableLet.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then true
	else if variableLet.oclIsUndefined()
	then null
	else variableLet.calculatedMandatory
	endif
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular if namedExpression.oclIsUndefined() then null
	else namedExpression.calculatedSingular
	endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSimpleType if variableLet.oclIsUndefined() then SimpleType::None
	else variableLet.calculatedSimpleType
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedType if (let e: Boolean = variableLet.oclIsUndefined() in 
	if e.oclIsInvalid() then null else e endif) 
	=true 
	then null
	else if variableLet.oclIsUndefined()
	then null
	else variableLet.calculatedType
	endif
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (ExpressionsPackage.Literals.MVARIABLE_BASE_DEFINITION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MVariableBaseDefinitionImpl
