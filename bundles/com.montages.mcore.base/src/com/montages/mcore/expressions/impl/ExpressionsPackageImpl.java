/**
 */
package com.montages.mcore.expressions.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.langlets.acore.AcorePackage;
import org.xocl.semantics.SemanticsPackage;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.impl.AnnotationsPackageImpl;
import com.montages.mcore.expressions.CollectionOperator;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsFactory;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractBaseDefinition;
import com.montages.mcore.expressions.MAbstractChain;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MAbstractIf;
import com.montages.mcore.expressions.MAbstractLet;
import com.montages.mcore.expressions.MAbstractNamedTuple;
import com.montages.mcore.expressions.MAbstractTupleEntry;
import com.montages.mcore.expressions.MAccumulator;
import com.montages.mcore.expressions.MAccumulatorBaseDefinition;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MApplicationAction;
import com.montages.mcore.expressions.MBaseChain;
import com.montages.mcore.expressions.MBaseDefinition;
import com.montages.mcore.expressions.MCallArgument;
import com.montages.mcore.expressions.MChain;
import com.montages.mcore.expressions.MChainAction;
import com.montages.mcore.expressions.MChainOrApplication;
import com.montages.mcore.expressions.MCollectionExpression;
import com.montages.mcore.expressions.MCollectionVar;
import com.montages.mcore.expressions.MConstantLet;
import com.montages.mcore.expressions.MContainerBaseDefinition;
import com.montages.mcore.expressions.MDataValueExpr;
import com.montages.mcore.expressions.MElse;
import com.montages.mcore.expressions.MElseIf;
import com.montages.mcore.expressions.MIf;
import com.montages.mcore.expressions.MIterator;
import com.montages.mcore.expressions.MIteratorBaseDefinition;
import com.montages.mcore.expressions.MLiteralConstantBaseDefinition;
import com.montages.mcore.expressions.MLiteralLet;
import com.montages.mcore.expressions.MLiteralValueExpr;
import com.montages.mcore.expressions.MNamedConstant;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MNamedExpressionAction;
import com.montages.mcore.expressions.MNewObjecct;
import com.montages.mcore.expressions.MNewObjectFeatureValue;
import com.montages.mcore.expressions.MNumberBaseDefinition;
import com.montages.mcore.expressions.MObjectBaseDefinition;
import com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition;
import com.montages.mcore.expressions.MObjectReferenceLet;
import com.montages.mcore.expressions.MOperator;
import com.montages.mcore.expressions.MOperatorDefinition;
import com.montages.mcore.expressions.MParameterBaseDefinition;
import com.montages.mcore.expressions.MProcessor;
import com.montages.mcore.expressions.MProcessorDefinition;
import com.montages.mcore.expressions.MSelfBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition;
import com.montages.mcore.expressions.MSimpleTypeConstantLet;
import com.montages.mcore.expressions.MSourceBaseDefinition;
import com.montages.mcore.expressions.MSubChain;
import com.montages.mcore.expressions.MSubChainAction;
import com.montages.mcore.expressions.MTargetBaseDefinition;
import com.montages.mcore.expressions.MThen;
import com.montages.mcore.expressions.MToplevelExpression;
import com.montages.mcore.expressions.MTuple;
import com.montages.mcore.expressions.MTupleEntry;
import com.montages.mcore.expressions.MVariableBaseDefinition;
import com.montages.mcore.impl.McorePackageImpl;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.ObjectsPackageImpl;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.tables.impl.TablesPackageImpl;
import com.montages.mcore.updates.UpdatesPackage;
import com.montages.mcore.updates.impl.UpdatesPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExpressionsPackageImpl extends EPackageImpl
		implements ExpressionsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractExpressionWithBaseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mContainerBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNumberBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mSelfBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTargetBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mSourceBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mSimpleTypeConstantBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLiteralConstantBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectReferenceConstantBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mVariableBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mParameterBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mIteratorBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAccumulatorBaseDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractLetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractNamedTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractTupleEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTupleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTupleEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNewObjecctEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNewObjectFeatureValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNamedExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mToplevelExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mBaseChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mProcessorDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mCallArgumentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mChainOrApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mDataValueExprEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLiteralValueExprEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAbstractIfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mIfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mThenEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mElseIfEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mElseEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mSubChainEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mCollectionExpressionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mCollectionVarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mIteratorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mAccumulatorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNamedConstantEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mConstantLetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mSimpleTypeConstantLetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLiteralLetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mObjectReferenceLetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mApplicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mOperatorDefinitionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum expressionBaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mNamedExpressionActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mProcessorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mChainActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mSubChainActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum collectionOperatorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mApplicationActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mOperatorEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.expressions.ExpressionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ExpressionsPackageImpl() {
		super(eNS_URI, ExpressionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ExpressionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ExpressionsPackage init() {
		if (isInited)
			return (ExpressionsPackage) EPackage.Registry.INSTANCE
					.getEPackage(ExpressionsPackage.eNS_URI);

		// Obtain or create and register package
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ExpressionsPackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ExpressionsPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(
								McorePackage.eNS_URI)
						: McorePackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(AnnotationsPackage.eNS_URI)
								: AnnotationsPackage.eINSTANCE);
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ObjectsPackage.eNS_URI) instanceof ObjectsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ObjectsPackage.eNS_URI)
								: ObjectsPackage.eINSTANCE);
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE
								.getEPackage(TablesPackage.eNS_URI)
						: TablesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(UpdatesPackage.eNS_URI)
								: UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theExpressionsPackage.createPackageContents();
		theMcorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theObjectsPackage.createPackageContents();
		theTablesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theExpressionsPackage.initializePackageContents();
		theMcorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theObjectsPackage.initializePackageContents();
		theTablesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theExpressionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ExpressionsPackage.eNS_URI,
				theExpressionsPackage);
		return theExpressionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractExpression() {
		return mAbstractExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_AsCode() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_AsBasicCode() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_Collector() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_EntireScope() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeBase() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeSelf() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeTrg() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeObj() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeSimpleTypeConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeLiteralConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeObjectReferenceConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeVariables() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeIterator() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeAccumulator() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeParameters() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeContainerBase() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeNumberBase() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ScopeSource() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalEntireScope() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeBase() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeSelf() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeTrg() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeObj() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeSource() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeSimpleTypeConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeLiteralConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeObjectReferenceConstants() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeVariables() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeIterator() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeAccumulator() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeParameters() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeNumberBaseDefinition() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_LocalScopeContainer() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ContainingAnnotation() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ContainingExpression() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_IsComplexExpression() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_IsSubExpression() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(36);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_SelfObjectType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(41);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_SelfObjectPackage() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(42);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_TargetObjectType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(43);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_TargetSimpleType() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(44);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ObjectObjectType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(45);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_ExpectedReturnType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(46);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_ExpectedReturnSimpleType() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(47);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_IsReturnValueMandatory() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(48);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_IsReturnValueSingular() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(49);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_SrcObjectType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(50);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_SrcObjectSimpleType() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(51);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpression_CalculatedOwnType() {
		return (EReference) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(37);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_CalculatedOwnMandatory() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(38);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_CalculatedOwnSingular() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(39);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpression_CalculatedOwnSimpleType() {
		return (EAttribute) mAbstractExpressionEClass.getEStructuralFeatures()
				.get(40);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpression__GetShortCode() {
		return mAbstractExpressionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpression__GetScope() {
		return mAbstractExpressionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractExpressionWithBase() {
		return mAbstractExpressionWithBaseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpressionWithBase_BaseAsCode() {
		return (EAttribute) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpressionWithBase_Base() {
		return (EAttribute) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpressionWithBase_BaseDefinition() {
		return (EReference) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpressionWithBase_BaseVar() {
		return (EReference) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractExpressionWithBase_BaseExitType() {
		return (EReference) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpressionWithBase_BaseExitSimpleType() {
		return (EAttribute) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpressionWithBase_BaseExitMandatory() {
		return (EAttribute) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractExpressionWithBase_BaseExitSingular() {
		return (EAttribute) mAbstractExpressionWithBaseEClass
				.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpressionWithBase__BaseDefinition$Update__MAbstractBaseDefinition() {
		return mAbstractExpressionWithBaseEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpressionWithBase__AsCodeForBuiltIn() {
		return mAbstractExpressionWithBaseEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpressionWithBase__AsCodeForConstants() {
		return mAbstractExpressionWithBaseEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractExpressionWithBase__AsCodeForVariables() {
		return mAbstractExpressionWithBaseEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractBaseDefinition() {
		return mAbstractBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractBaseDefinition_CalculatedBase() {
		return (EAttribute) mAbstractBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractBaseDefinition_CalculatedAsCode() {
		return (EAttribute) mAbstractBaseDefinitionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractBaseDefinition_Debug() {
		return (EAttribute) mAbstractBaseDefinitionEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMContainerBaseDefinition() {
		return mContainerBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMBaseDefinition() {
		return mBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMBaseDefinition_ExpressionBase() {
		return (EAttribute) mBaseDefinitionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNumberBaseDefinition() {
		return mNumberBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNumberBaseDefinition_ExpressionBase() {
		return (EAttribute) mNumberBaseDefinitionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNumberBaseDefinition__CalculateAsCode__MAbstractExpression() {
		return mNumberBaseDefinitionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSelfBaseDefinition() {
		return mSelfBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTargetBaseDefinition() {
		return mTargetBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSourceBaseDefinition() {
		return mSourceBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObjectBaseDefinition() {
		return mObjectBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSimpleTypeConstantBaseDefinition() {
		return mSimpleTypeConstantBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSimpleTypeConstantBaseDefinition_NamedConstant() {
		return (EReference) mSimpleTypeConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSimpleTypeConstantBaseDefinition_ConstantLet() {
		return (EReference) mSimpleTypeConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLiteralConstantBaseDefinition() {
		return mLiteralConstantBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralConstantBaseDefinition_NamedConstant() {
		return (EReference) mLiteralConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralConstantBaseDefinition_ConstantLet() {
		return (EReference) mLiteralConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObjectReferenceConstantBaseDefinition() {
		return mObjectReferenceConstantBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceConstantBaseDefinition_NamedConstant() {
		return (EReference) mObjectReferenceConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceConstantBaseDefinition_ConstantLet() {
		return (EReference) mObjectReferenceConstantBaseDefinitionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMVariableBaseDefinition() {
		return mVariableBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMVariableBaseDefinition_NamedExpression() {
		return (EReference) mVariableBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMVariableBaseDefinition_VariableLet() {
		return (EReference) mVariableBaseDefinitionEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMParameterBaseDefinition() {
		return mParameterBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMParameterBaseDefinition_Parameter() {
		return (EReference) mParameterBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMIteratorBaseDefinition() {
		return mIteratorBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMIteratorBaseDefinition_Iterator() {
		return (EReference) mIteratorBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAccumulatorBaseDefinition() {
		return mAccumulatorBaseDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAccumulatorBaseDefinition_Accumulator() {
		return (EReference) mAccumulatorBaseDefinitionEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractLet() {
		return mAbstractLetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractNamedTuple() {
		return mAbstractNamedTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractNamedTuple_AbstractEntry() {
		return (EReference) mAbstractNamedTupleEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractTupleEntry() {
		return mAbstractTupleEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractTupleEntry_Value() {
		return (EReference) mAbstractTupleEntryEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTuple() {
		return mTupleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTuple_Entry() {
		return (EReference) mTupleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTupleEntry() {
		return mTupleEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNewObjecct() {
		return mNewObjecctEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNewObjecct_NewType() {
		return (EReference) mNewObjecctEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNewObjecct_Entry() {
		return (EReference) mNewObjecctEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNewObjectFeatureValue() {
		return mNewObjectFeatureValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNewObjectFeatureValue_Feature() {
		return (EReference) mNewObjectFeatureValueEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNamedExpression() {
		return mNamedExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNamedExpression_Expression() {
		return (EReference) mNamedExpressionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamedExpression_DoAction() {
		return (EAttribute) mNamedExpressionEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamedExpression__DoAction$Update__MNamedExpressionAction() {
		return mNamedExpressionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamedExpression__DefaultValue() {
		return mNamedExpressionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamedExpression__AllApplications() {
		return mNamedExpressionEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMToplevelExpression() {
		return mToplevelExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractChain() {
		return mAbstractChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_ChainEntryType() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_ChainAsCode() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_Element1() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_Element1Correct() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_Element2EntryType() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_Element2() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_Element2Correct() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_Element3EntryType() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_Element3() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_Element3Correct() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_CastType() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_LastElement() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_Processor() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_ProcessorDefinition() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcessorDefinition$Update__MProcessorDefinition() {
		return mAbstractChainEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractChain_ChainCalculatedType() {
		return (EReference) mAbstractChainEClass.getEStructuralFeatures()
				.get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_ChainCalculatedSimpleType() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAbstractChain_ChainCalculatedSingular() {
		return (EAttribute) mAbstractChainEClass.getEStructuralFeatures()
				.get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__Length() {
		return mAbstractChainEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__UnsafeElementAsCode__Integer() {
		return mAbstractChainEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__UnsafeChainStepAsCode__Integer() {
		return mAbstractChainEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__UnsafeChainAsCode__Integer() {
		return mAbstractChainEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__UnsafeChainAsCode__Integer_Integer() {
		return mAbstractChainEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__AsCodeForOthers() {
		return mAbstractChainEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__CodeForLength1() {
		return mAbstractChainEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__CodeForLength2() {
		return mAbstractChainEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__CodeForLength3() {
		return mAbstractChainEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcAsCode() {
		return mAbstractChainEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__IsProcessorSetOperator() {
		return mAbstractChainEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__IsOwnXOCLOperator() {
		return mAbstractChainEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcessorReturnsSingular() {
		return mAbstractChainEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcessorIsSet() {
		return mAbstractChainEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__CreateProcessorDefinition() {
		return mAbstractChainEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForObject() {
		return mAbstractChainEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForObjects() {
		return mAbstractChainEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForBoolean() {
		return mAbstractChainEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForBooleans() {
		return mAbstractChainEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForInteger() {
		return mAbstractChainEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForIntegers() {
		return mAbstractChainEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForReal() {
		return mAbstractChainEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForReals() {
		return mAbstractChainEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForString() {
		return mAbstractChainEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForStrings() {
		return mAbstractChainEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForDate() {
		return mAbstractChainEClass.getEOperations().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__ProcDefChoicesForDates() {
		return mAbstractChainEClass.getEOperations().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMBaseChain() {
		return mBaseChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMBaseChain_TypeMismatch() {
		return (EAttribute) mBaseChainEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMBaseChain_CallArgument() {
		return (EReference) mBaseChainEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMBaseChain_SubExpression() {
		return (EReference) mBaseChainEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMBaseChain_ContainedCollector() {
		return (EReference) mBaseChainEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMBaseChain_ChainCodeforSubchains() {
		return (EAttribute) mBaseChainEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMBaseChain_IsOwnXOCLOp() {
		return (EAttribute) mBaseChainEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__AutoCastWithProc() {
		return mBaseChainEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__AsCodeForVariables() {
		return mBaseChainEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__OwnToApplyMismatch() {
		return mBaseChainEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__UniqueChainNumber() {
		return mBaseChainEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__ReuseFromOtherNoMoreUsedChain__MBaseChain() {
		return mBaseChainEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__ResetToBase__ExpressionBase_MVariable() {
		return mBaseChainEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__ReuseFromOtherNoMoreUsedChainAsUpdate__MBaseChain() {
		return mBaseChainEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__IsProcessorCheckEqualOperator() {
		return mBaseChainEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__IsPrefixProcessor() {
		return mBaseChainEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__IsPostfixProcessor() {
		return mBaseChainEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__AsCodeForOthers() {
		return mBaseChainEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__UnsafeElementAsCode__Integer() {
		return mBaseChainEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__CodeForLength1() {
		return mBaseChainEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__CodeForLength3() {
		return mBaseChainEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMBaseChain__CodeForLength2() {
		return mBaseChainEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMAbstractChain__IsCustomCodeProcessor() {
		return mAbstractChainEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMProcessorDefinition() {
		return mProcessorDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProcessorDefinition_Processor() {
		return (EAttribute) mProcessorDefinitionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMChain() {
		return mChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMChain_DoAction() {
		return (EAttribute) mChainEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMChain__DoAction$Update__MChainAction() {
		return mChainEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMCallArgument() {
		return mCallArgumentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMCallArgument_First() {
		return (EAttribute) mCallArgumentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMCallArgument_Last() {
		return (EAttribute) mCallArgumentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCallArgument_Call() {
		return (EReference) mCallArgumentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMChainOrApplication() {
		return mChainOrApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMDataValueExpr() {
		return mDataValueExprEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMDataValueExpr_SimpleType() {
		return (EAttribute) mDataValueExprEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMDataValueExpr_Value() {
		return (EAttribute) mDataValueExprEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLiteralValueExpr() {
		return mLiteralValueExprEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralValueExpr_EnumerationType() {
		return (EReference) mLiteralValueExprEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralValueExpr_Value() {
		return (EReference) mLiteralValueExprEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMLiteralValueExpr__GetAllowedLiterals() {
		return mLiteralValueExprEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAbstractIf() {
		return mAbstractIfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractIf_Condition() {
		return (EReference) mAbstractIfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractIf_ThenPart() {
		return (EReference) mAbstractIfEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractIf_ElseifPart() {
		return (EReference) mAbstractIfEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAbstractIf_ElsePart() {
		return (EReference) mAbstractIfEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMIf() {
		return mIfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMIf_ContainedCollector() {
		return (EReference) mIfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMIf__DefaultValue() {
		return mIfEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMThen() {
		return mThenEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMThen_Expression() {
		return (EReference) mThenEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMElseIf() {
		return mElseIfEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMElseIf_Condition() {
		return (EReference) mElseIfEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMElseIf_ThenPart() {
		return (EReference) mElseIfEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMElseIf__DefaultValue() {
		return mElseIfEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMElse() {
		return mElseEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMElse_Expression() {
		return (EReference) mElseEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSubChain() {
		return mSubChainEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSubChain_PreviousExpression() {
		return (EReference) mSubChainEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMSubChain_NextExpression() {
		return (EReference) mSubChainEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSubChain_DoAction() {
		return (EAttribute) mSubChainEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__DoAction$Update__MSubChainAction() {
		return mSubChainEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__IsLastSubchain() {
		return mSubChainEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__UniqueSubchainName() {
		return mSubChainEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__AsCodeForOthers() {
		return mSubChainEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__ReturnSingularPureChain() {
		return mSubChainEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__CodeForLength1() {
		return mSubChainEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__CodeForLength2() {
		return mSubChainEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMSubChain__CodeForLength3() {
		return mSubChainEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMCollectionExpression() {
		return mCollectionExpressionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMCollectionExpression_CollectionOperator() {
		return (EAttribute) mCollectionExpressionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCollectionExpression_Collection() {
		return (EReference) mCollectionExpressionEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCollectionExpression_IteratorVar() {
		return (EReference) mCollectionExpressionEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCollectionExpression_AccumulatorVar() {
		return (EReference) mCollectionExpressionEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCollectionExpression_Expression() {
		return (EReference) mCollectionExpressionEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMCollectionExpression__CollectionOperatorAsCode__CollectionOperator() {
		return mCollectionExpressionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMCollectionExpression__CollectionOperatorAsCode() {
		return mCollectionExpressionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMCollectionExpression__DefaultValue() {
		return mCollectionExpressionEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMCollectionVar() {
		return mCollectionVarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMCollectionVar_ContainingCollection() {
		return (EReference) mCollectionVarEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMIterator() {
		return mIteratorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMAccumulator() {
		return mAccumulatorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAccumulator_AccDefinition() {
		return (EReference) mAccumulatorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAccumulator_SimpleType() {
		return (EAttribute) mAccumulatorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMAccumulator_Type() {
		return (EReference) mAccumulatorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAccumulator_Mandatory() {
		return (EAttribute) mAccumulatorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMAccumulator_Singular() {
		return (EAttribute) mAccumulatorEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNamedConstant() {
		return mNamedConstantEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNamedConstant_Expression() {
		return (EReference) mNamedConstantEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamedConstant__DefaultValue() {
		return mNamedConstantEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMConstantLet() {
		return mConstantLetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMConstantLet__AsSetString__String_String_String() {
		return mConstantLetEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMConstantLet__InQuotes__String() {
		return mConstantLetEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMSimpleTypeConstantLet() {
		return mSimpleTypeConstantLetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSimpleTypeConstantLet_SimpleType() {
		return (EAttribute) mSimpleTypeConstantLetEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSimpleTypeConstantLet_Constant1() {
		return (EAttribute) mSimpleTypeConstantLetEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSimpleTypeConstantLet_Constant2() {
		return (EAttribute) mSimpleTypeConstantLetEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMSimpleTypeConstantLet_Constant3() {
		return (EAttribute) mSimpleTypeConstantLetEClass
				.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLiteralLet() {
		return mLiteralLetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralLet_EnumerationType() {
		return (EReference) mLiteralLetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralLet_Constant1() {
		return (EReference) mLiteralLetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralLet_Constant2() {
		return (EReference) mLiteralLetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteralLet_Constant3() {
		return (EReference) mLiteralLetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMLiteralLet__GetAllowedLiterals() {
		return mLiteralLetEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMObjectReferenceLet() {
		return mObjectReferenceLetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceLet_ObjectType() {
		return (EReference) mObjectReferenceLetEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceLet_Constant1() {
		return (EReference) mObjectReferenceLetEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceLet_Constant2() {
		return (EReference) mObjectReferenceLetEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMObjectReferenceLet_Constant3() {
		return (EReference) mObjectReferenceLetEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMApplication() {
		return mApplicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMApplication_Operator() {
		return (EAttribute) mApplicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMApplication_Operands() {
		return (EReference) mApplicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMApplication_ContainedCollector() {
		return (EReference) mApplicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMApplication_DoAction() {
		return (EAttribute) mApplicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__DoAction$Update__MApplicationAction() {
		return mApplicationEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__OperatorAsCode() {
		return mApplicationEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorPrefix() {
		return mApplicationEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorInfix() {
		return mApplicationEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorPostfix() {
		return mApplicationEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorUnaryFunction() {
		return mApplicationEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorSetOperator() {
		return mApplicationEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__DefaultValue() {
		return mApplicationEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorUnaryFunctionTwoParam() {
		return mApplicationEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__IsOperatorParameterUnary() {
		return mApplicationEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__UniqueApplyNumber() {
		return mApplicationEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__DefinesOperatorBusinessLogic() {
		return mApplicationEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__CodeForLogicOperator() {
		return mApplicationEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMApplication__GetCalculatedSimpleDifferentTypes() {
		return mApplicationEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMOperatorDefinition() {
		return mOperatorDefinitionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMOperatorDefinition_MOperator() {
		return (EAttribute) mOperatorDefinitionEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getExpressionBase() {
		return expressionBaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMNamedExpressionAction() {
		return mNamedExpressionActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMProcessor() {
		return mProcessorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMChainAction() {
		return mChainActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMSubChainAction() {
		return mSubChainActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCollectionOperator() {
		return collectionOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMApplicationAction() {
		return mApplicationActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMOperator() {
		return mOperatorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsFactory getExpressionsFactory() {
		return (ExpressionsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mAbstractExpressionEClass = createEClass(MABSTRACT_EXPRESSION);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__AS_CODE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__AS_BASIC_CODE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__COLLECTOR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__ENTIRE_SCOPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_BASE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_SELF);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_TRG);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_OBJ);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_VARIABLES);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_ITERATOR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_PARAMETERS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SCOPE_SOURCE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR);
		createEReference(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE);
		createEAttribute(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE);
		createEOperation(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION___GET_SHORT_CODE);
		createEOperation(mAbstractExpressionEClass,
				MABSTRACT_EXPRESSION___GET_SCOPE);

		mAbstractExpressionWithBaseEClass = createEClass(
				MABSTRACT_EXPRESSION_WITH_BASE);
		createEAttribute(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE);
		createEAttribute(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE);
		createEReference(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION);
		createEReference(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_VAR);
		createEReference(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_TYPE);
		createEAttribute(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SIMPLE_TYPE);
		createEAttribute(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY);
		createEAttribute(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR);
		createEOperation(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE___BASE_DEFINITION$_UPDATE__MABSTRACTBASEDEFINITION);
		createEOperation(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_BUILT_IN);
		createEOperation(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_CONSTANTS);
		createEOperation(mAbstractExpressionWithBaseEClass,
				MABSTRACT_EXPRESSION_WITH_BASE___AS_CODE_FOR_VARIABLES);

		mAbstractBaseDefinitionEClass = createEClass(MABSTRACT_BASE_DEFINITION);
		createEAttribute(mAbstractBaseDefinitionEClass,
				MABSTRACT_BASE_DEFINITION__CALCULATED_BASE);
		createEAttribute(mAbstractBaseDefinitionEClass,
				MABSTRACT_BASE_DEFINITION__CALCULATED_AS_CODE);
		createEAttribute(mAbstractBaseDefinitionEClass,
				MABSTRACT_BASE_DEFINITION__DEBUG);

		mContainerBaseDefinitionEClass = createEClass(
				MCONTAINER_BASE_DEFINITION);

		mBaseDefinitionEClass = createEClass(MBASE_DEFINITION);
		createEAttribute(mBaseDefinitionEClass,
				MBASE_DEFINITION__EXPRESSION_BASE);

		mNumberBaseDefinitionEClass = createEClass(MNUMBER_BASE_DEFINITION);
		createEAttribute(mNumberBaseDefinitionEClass,
				MNUMBER_BASE_DEFINITION__EXPRESSION_BASE);
		createEOperation(mNumberBaseDefinitionEClass,
				MNUMBER_BASE_DEFINITION___CALCULATE_AS_CODE__MABSTRACTEXPRESSION);

		mSelfBaseDefinitionEClass = createEClass(MSELF_BASE_DEFINITION);

		mTargetBaseDefinitionEClass = createEClass(MTARGET_BASE_DEFINITION);

		mSourceBaseDefinitionEClass = createEClass(MSOURCE_BASE_DEFINITION);

		mObjectBaseDefinitionEClass = createEClass(MOBJECT_BASE_DEFINITION);

		mSimpleTypeConstantBaseDefinitionEClass = createEClass(
				MSIMPLE_TYPE_CONSTANT_BASE_DEFINITION);
		createEReference(mSimpleTypeConstantBaseDefinitionEClass,
				MSIMPLE_TYPE_CONSTANT_BASE_DEFINITION__NAMED_CONSTANT);
		createEReference(mSimpleTypeConstantBaseDefinitionEClass,
				MSIMPLE_TYPE_CONSTANT_BASE_DEFINITION__CONSTANT_LET);

		mLiteralConstantBaseDefinitionEClass = createEClass(
				MLITERAL_CONSTANT_BASE_DEFINITION);
		createEReference(mLiteralConstantBaseDefinitionEClass,
				MLITERAL_CONSTANT_BASE_DEFINITION__NAMED_CONSTANT);
		createEReference(mLiteralConstantBaseDefinitionEClass,
				MLITERAL_CONSTANT_BASE_DEFINITION__CONSTANT_LET);

		mObjectReferenceConstantBaseDefinitionEClass = createEClass(
				MOBJECT_REFERENCE_CONSTANT_BASE_DEFINITION);
		createEReference(mObjectReferenceConstantBaseDefinitionEClass,
				MOBJECT_REFERENCE_CONSTANT_BASE_DEFINITION__NAMED_CONSTANT);
		createEReference(mObjectReferenceConstantBaseDefinitionEClass,
				MOBJECT_REFERENCE_CONSTANT_BASE_DEFINITION__CONSTANT_LET);

		mVariableBaseDefinitionEClass = createEClass(MVARIABLE_BASE_DEFINITION);
		createEReference(mVariableBaseDefinitionEClass,
				MVARIABLE_BASE_DEFINITION__NAMED_EXPRESSION);
		createEReference(mVariableBaseDefinitionEClass,
				MVARIABLE_BASE_DEFINITION__VARIABLE_LET);

		mIteratorBaseDefinitionEClass = createEClass(MITERATOR_BASE_DEFINITION);
		createEReference(mIteratorBaseDefinitionEClass,
				MITERATOR_BASE_DEFINITION__ITERATOR);

		mParameterBaseDefinitionEClass = createEClass(
				MPARAMETER_BASE_DEFINITION);
		createEReference(mParameterBaseDefinitionEClass,
				MPARAMETER_BASE_DEFINITION__PARAMETER);

		mAccumulatorBaseDefinitionEClass = createEClass(
				MACCUMULATOR_BASE_DEFINITION);
		createEReference(mAccumulatorBaseDefinitionEClass,
				MACCUMULATOR_BASE_DEFINITION__ACCUMULATOR);

		mAbstractLetEClass = createEClass(MABSTRACT_LET);

		mAbstractNamedTupleEClass = createEClass(MABSTRACT_NAMED_TUPLE);
		createEReference(mAbstractNamedTupleEClass,
				MABSTRACT_NAMED_TUPLE__ABSTRACT_ENTRY);

		mAbstractTupleEntryEClass = createEClass(MABSTRACT_TUPLE_ENTRY);
		createEReference(mAbstractTupleEntryEClass,
				MABSTRACT_TUPLE_ENTRY__VALUE);

		mTupleEClass = createEClass(MTUPLE);
		createEReference(mTupleEClass, MTUPLE__ENTRY);

		mTupleEntryEClass = createEClass(MTUPLE_ENTRY);

		mNewObjecctEClass = createEClass(MNEW_OBJECCT);
		createEReference(mNewObjecctEClass, MNEW_OBJECCT__NEW_TYPE);
		createEReference(mNewObjecctEClass, MNEW_OBJECCT__ENTRY);

		mNewObjectFeatureValueEClass = createEClass(MNEW_OBJECT_FEATURE_VALUE);
		createEReference(mNewObjectFeatureValueEClass,
				MNEW_OBJECT_FEATURE_VALUE__FEATURE);

		mNamedExpressionEClass = createEClass(MNAMED_EXPRESSION);
		createEReference(mNamedExpressionEClass, MNAMED_EXPRESSION__EXPRESSION);
		createEAttribute(mNamedExpressionEClass, MNAMED_EXPRESSION__DO_ACTION);
		createEOperation(mNamedExpressionEClass,
				MNAMED_EXPRESSION___DO_ACTION$_UPDATE__MNAMEDEXPRESSIONACTION);
		createEOperation(mNamedExpressionEClass,
				MNAMED_EXPRESSION___DEFAULT_VALUE);
		createEOperation(mNamedExpressionEClass,
				MNAMED_EXPRESSION___ALL_APPLICATIONS);

		mToplevelExpressionEClass = createEClass(MTOPLEVEL_EXPRESSION);

		mAbstractChainEClass = createEClass(MABSTRACT_CHAIN);
		createEReference(mAbstractChainEClass,
				MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE);
		createEAttribute(mAbstractChainEClass, MABSTRACT_CHAIN__CHAIN_AS_CODE);
		createEReference(mAbstractChainEClass, MABSTRACT_CHAIN__ELEMENT1);
		createEAttribute(mAbstractChainEClass,
				MABSTRACT_CHAIN__ELEMENT1_CORRECT);
		createEReference(mAbstractChainEClass,
				MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE);
		createEReference(mAbstractChainEClass, MABSTRACT_CHAIN__ELEMENT2);
		createEAttribute(mAbstractChainEClass,
				MABSTRACT_CHAIN__ELEMENT2_CORRECT);
		createEReference(mAbstractChainEClass,
				MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE);
		createEReference(mAbstractChainEClass, MABSTRACT_CHAIN__ELEMENT3);
		createEAttribute(mAbstractChainEClass,
				MABSTRACT_CHAIN__ELEMENT3_CORRECT);
		createEReference(mAbstractChainEClass, MABSTRACT_CHAIN__CAST_TYPE);
		createEReference(mAbstractChainEClass, MABSTRACT_CHAIN__LAST_ELEMENT);
		createEReference(mAbstractChainEClass,
				MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE);
		createEAttribute(mAbstractChainEClass,
				MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE);
		createEAttribute(mAbstractChainEClass,
				MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR);
		createEAttribute(mAbstractChainEClass, MABSTRACT_CHAIN__PROCESSOR);
		createEReference(mAbstractChainEClass,
				MABSTRACT_CHAIN__PROCESSOR_DEFINITION);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROCESSOR_DEFINITION$_UPDATE__MPROCESSORDEFINITION);
		createEOperation(mAbstractChainEClass, MABSTRACT_CHAIN___LENGTH);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___UNSAFE_CHAIN_STEP_AS_CODE__INTEGER);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___UNSAFE_CHAIN_AS_CODE__INTEGER_INTEGER);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___AS_CODE_FOR_OTHERS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___CODE_FOR_LENGTH1);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___CODE_FOR_LENGTH2);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___CODE_FOR_LENGTH3);
		createEOperation(mAbstractChainEClass, MABSTRACT_CHAIN___PROC_AS_CODE);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___IS_CUSTOM_CODE_PROCESSOR);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___IS_PROCESSOR_SET_OPERATOR);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___IS_OWN_XOCL_OPERATOR);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROCESSOR_RETURNS_SINGULAR);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROCESSOR_IS_SET);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___CREATE_PROCESSOR_DEFINITION);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECT);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_OBJECTS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEAN);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_BOOLEANS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGER);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_INTEGERS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REAL);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_REALS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRING);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_STRINGS);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATE);
		createEOperation(mAbstractChainEClass,
				MABSTRACT_CHAIN___PROC_DEF_CHOICES_FOR_DATES);

		mBaseChainEClass = createEClass(MBASE_CHAIN);
		createEAttribute(mBaseChainEClass, MBASE_CHAIN__TYPE_MISMATCH);
		createEReference(mBaseChainEClass, MBASE_CHAIN__CALL_ARGUMENT);
		createEReference(mBaseChainEClass, MBASE_CHAIN__SUB_EXPRESSION);
		createEReference(mBaseChainEClass, MBASE_CHAIN__CONTAINED_COLLECTOR);
		createEAttribute(mBaseChainEClass,
				MBASE_CHAIN__CHAIN_CODEFOR_SUBCHAINS);
		createEAttribute(mBaseChainEClass, MBASE_CHAIN__IS_OWN_XOCL_OP);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___AUTO_CAST_WITH_PROC);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___OWN_TO_APPLY_MISMATCH);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___UNIQUE_CHAIN_NUMBER);
		createEOperation(mBaseChainEClass,
				MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN__MBASECHAIN);
		createEOperation(mBaseChainEClass,
				MBASE_CHAIN___RESET_TO_BASE__EXPRESSIONBASE_MVARIABLE);
		createEOperation(mBaseChainEClass,
				MBASE_CHAIN___REUSE_FROM_OTHER_NO_MORE_USED_CHAIN_AS_UPDATE__MBASECHAIN);
		createEOperation(mBaseChainEClass,
				MBASE_CHAIN___IS_PROCESSOR_CHECK_EQUAL_OPERATOR);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___IS_PREFIX_PROCESSOR);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___IS_POSTFIX_PROCESSOR);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___AS_CODE_FOR_OTHERS);
		createEOperation(mBaseChainEClass,
				MBASE_CHAIN___UNSAFE_ELEMENT_AS_CODE__INTEGER);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___CODE_FOR_LENGTH1);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___CODE_FOR_LENGTH2);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___CODE_FOR_LENGTH3);
		createEOperation(mBaseChainEClass, MBASE_CHAIN___AS_CODE_FOR_VARIABLES);

		mProcessorDefinitionEClass = createEClass(MPROCESSOR_DEFINITION);
		createEAttribute(mProcessorDefinitionEClass,
				MPROCESSOR_DEFINITION__PROCESSOR);

		mChainEClass = createEClass(MCHAIN);
		createEAttribute(mChainEClass, MCHAIN__DO_ACTION);
		createEOperation(mChainEClass,
				MCHAIN___DO_ACTION$_UPDATE__MCHAINACTION);

		mCallArgumentEClass = createEClass(MCALL_ARGUMENT);
		createEAttribute(mCallArgumentEClass, MCALL_ARGUMENT__FIRST);
		createEAttribute(mCallArgumentEClass, MCALL_ARGUMENT__LAST);
		createEReference(mCallArgumentEClass, MCALL_ARGUMENT__CALL);

		mSubChainEClass = createEClass(MSUB_CHAIN);
		createEReference(mSubChainEClass, MSUB_CHAIN__PREVIOUS_EXPRESSION);
		createEReference(mSubChainEClass, MSUB_CHAIN__NEXT_EXPRESSION);
		createEAttribute(mSubChainEClass, MSUB_CHAIN__DO_ACTION);
		createEOperation(mSubChainEClass,
				MSUB_CHAIN___DO_ACTION$_UPDATE__MSUBCHAINACTION);
		createEOperation(mSubChainEClass, MSUB_CHAIN___IS_LAST_SUBCHAIN);
		createEOperation(mSubChainEClass, MSUB_CHAIN___UNIQUE_SUBCHAIN_NAME);
		createEOperation(mSubChainEClass,
				MSUB_CHAIN___RETURN_SINGULAR_PURE_CHAIN);
		createEOperation(mSubChainEClass, MSUB_CHAIN___AS_CODE_FOR_OTHERS);
		createEOperation(mSubChainEClass, MSUB_CHAIN___CODE_FOR_LENGTH1);
		createEOperation(mSubChainEClass, MSUB_CHAIN___CODE_FOR_LENGTH2);
		createEOperation(mSubChainEClass, MSUB_CHAIN___CODE_FOR_LENGTH3);

		mChainOrApplicationEClass = createEClass(MCHAIN_OR_APPLICATION);

		mDataValueExprEClass = createEClass(MDATA_VALUE_EXPR);
		createEAttribute(mDataValueExprEClass, MDATA_VALUE_EXPR__SIMPLE_TYPE);
		createEAttribute(mDataValueExprEClass, MDATA_VALUE_EXPR__VALUE);

		mLiteralValueExprEClass = createEClass(MLITERAL_VALUE_EXPR);
		createEReference(mLiteralValueExprEClass,
				MLITERAL_VALUE_EXPR__ENUMERATION_TYPE);
		createEReference(mLiteralValueExprEClass, MLITERAL_VALUE_EXPR__VALUE);
		createEOperation(mLiteralValueExprEClass,
				MLITERAL_VALUE_EXPR___GET_ALLOWED_LITERALS);

		mAbstractIfEClass = createEClass(MABSTRACT_IF);
		createEReference(mAbstractIfEClass, MABSTRACT_IF__CONDITION);
		createEReference(mAbstractIfEClass, MABSTRACT_IF__THEN_PART);
		createEReference(mAbstractIfEClass, MABSTRACT_IF__ELSEIF_PART);
		createEReference(mAbstractIfEClass, MABSTRACT_IF__ELSE_PART);

		mIfEClass = createEClass(MIF);
		createEReference(mIfEClass, MIF__CONTAINED_COLLECTOR);
		createEOperation(mIfEClass, MIF___DEFAULT_VALUE);

		mThenEClass = createEClass(MTHEN);
		createEReference(mThenEClass, MTHEN__EXPRESSION);

		mElseIfEClass = createEClass(MELSE_IF);
		createEReference(mElseIfEClass, MELSE_IF__CONDITION);
		createEReference(mElseIfEClass, MELSE_IF__THEN_PART);
		createEOperation(mElseIfEClass, MELSE_IF___DEFAULT_VALUE);

		mElseEClass = createEClass(MELSE);
		createEReference(mElseEClass, MELSE__EXPRESSION);

		mCollectionExpressionEClass = createEClass(MCOLLECTION_EXPRESSION);
		createEAttribute(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION__COLLECTION_OPERATOR);
		createEReference(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION__COLLECTION);
		createEReference(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION__ITERATOR_VAR);
		createEReference(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION__ACCUMULATOR_VAR);
		createEReference(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION__EXPRESSION);
		createEOperation(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION___COLLECTION_OPERATOR_AS_CODE__COLLECTIONOPERATOR);
		createEOperation(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION___COLLECTION_OPERATOR_AS_CODE);
		createEOperation(mCollectionExpressionEClass,
				MCOLLECTION_EXPRESSION___DEFAULT_VALUE);

		mCollectionVarEClass = createEClass(MCOLLECTION_VAR);
		createEReference(mCollectionVarEClass,
				MCOLLECTION_VAR__CONTAINING_COLLECTION);

		mIteratorEClass = createEClass(MITERATOR);

		mAccumulatorEClass = createEClass(MACCUMULATOR);
		createEReference(mAccumulatorEClass, MACCUMULATOR__ACC_DEFINITION);
		createEAttribute(mAccumulatorEClass, MACCUMULATOR__SIMPLE_TYPE);
		createEReference(mAccumulatorEClass, MACCUMULATOR__TYPE);
		createEAttribute(mAccumulatorEClass, MACCUMULATOR__MANDATORY);
		createEAttribute(mAccumulatorEClass, MACCUMULATOR__SINGULAR);

		mNamedConstantEClass = createEClass(MNAMED_CONSTANT);
		createEReference(mNamedConstantEClass, MNAMED_CONSTANT__EXPRESSION);
		createEOperation(mNamedConstantEClass, MNAMED_CONSTANT___DEFAULT_VALUE);

		mConstantLetEClass = createEClass(MCONSTANT_LET);
		createEOperation(mConstantLetEClass,
				MCONSTANT_LET___AS_SET_STRING__STRING_STRING_STRING);
		createEOperation(mConstantLetEClass, MCONSTANT_LET___IN_QUOTES__STRING);

		mSimpleTypeConstantLetEClass = createEClass(MSIMPLE_TYPE_CONSTANT_LET);
		createEAttribute(mSimpleTypeConstantLetEClass,
				MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE);
		createEAttribute(mSimpleTypeConstantLetEClass,
				MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1);
		createEAttribute(mSimpleTypeConstantLetEClass,
				MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2);
		createEAttribute(mSimpleTypeConstantLetEClass,
				MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3);

		mLiteralLetEClass = createEClass(MLITERAL_LET);
		createEReference(mLiteralLetEClass, MLITERAL_LET__ENUMERATION_TYPE);
		createEReference(mLiteralLetEClass, MLITERAL_LET__CONSTANT1);
		createEReference(mLiteralLetEClass, MLITERAL_LET__CONSTANT2);
		createEReference(mLiteralLetEClass, MLITERAL_LET__CONSTANT3);
		createEOperation(mLiteralLetEClass,
				MLITERAL_LET___GET_ALLOWED_LITERALS);

		mObjectReferenceLetEClass = createEClass(MOBJECT_REFERENCE_LET);
		createEReference(mObjectReferenceLetEClass,
				MOBJECT_REFERENCE_LET__OBJECT_TYPE);
		createEReference(mObjectReferenceLetEClass,
				MOBJECT_REFERENCE_LET__CONSTANT1);
		createEReference(mObjectReferenceLetEClass,
				MOBJECT_REFERENCE_LET__CONSTANT2);
		createEReference(mObjectReferenceLetEClass,
				MOBJECT_REFERENCE_LET__CONSTANT3);

		mApplicationEClass = createEClass(MAPPLICATION);
		createEAttribute(mApplicationEClass, MAPPLICATION__OPERATOR);
		createEReference(mApplicationEClass, MAPPLICATION__OPERANDS);
		createEReference(mApplicationEClass, MAPPLICATION__CONTAINED_COLLECTOR);
		createEAttribute(mApplicationEClass, MAPPLICATION__DO_ACTION);
		createEOperation(mApplicationEClass,
				MAPPLICATION___DO_ACTION$_UPDATE__MAPPLICATIONACTION);
		createEOperation(mApplicationEClass, MAPPLICATION___OPERATOR_AS_CODE);
		createEOperation(mApplicationEClass, MAPPLICATION___IS_OPERATOR_PREFIX);
		createEOperation(mApplicationEClass, MAPPLICATION___IS_OPERATOR_INFIX);
		createEOperation(mApplicationEClass,
				MAPPLICATION___IS_OPERATOR_POSTFIX);
		createEOperation(mApplicationEClass,
				MAPPLICATION___IS_OPERATOR_UNARY_FUNCTION);
		createEOperation(mApplicationEClass,
				MAPPLICATION___IS_OPERATOR_SET_OPERATOR);
		createEOperation(mApplicationEClass, MAPPLICATION___DEFAULT_VALUE);
		createEOperation(mApplicationEClass,
				MAPPLICATION___IS_OPERATOR_UNARY_FUNCTION_TWO_PARAM);
		createEOperation(mApplicationEClass,
				MAPPLICATION___IS_OPERATOR_PARAMETER_UNARY);
		createEOperation(mApplicationEClass,
				MAPPLICATION___UNIQUE_APPLY_NUMBER);
		createEOperation(mApplicationEClass,
				MAPPLICATION___DEFINES_OPERATOR_BUSINESS_LOGIC);
		createEOperation(mApplicationEClass,
				MAPPLICATION___CODE_FOR_LOGIC_OPERATOR);
		createEOperation(mApplicationEClass,
				MAPPLICATION___GET_CALCULATED_SIMPLE_DIFFERENT_TYPES);

		mOperatorDefinitionEClass = createEClass(MOPERATOR_DEFINITION);
		createEAttribute(mOperatorDefinitionEClass,
				MOPERATOR_DEFINITION__MOPERATOR);

		// Create enums
		expressionBaseEEnum = createEEnum(EXPRESSION_BASE);
		mNamedExpressionActionEEnum = createEEnum(MNAMED_EXPRESSION_ACTION);
		mProcessorEEnum = createEEnum(MPROCESSOR);
		mChainActionEEnum = createEEnum(MCHAIN_ACTION);
		mSubChainActionEEnum = createEEnum(MSUB_CHAIN_ACTION);
		collectionOperatorEEnum = createEEnum(COLLECTION_OPERATOR);
		mApplicationActionEEnum = createEEnum(MAPPLICATION_ACTION);
		mOperatorEEnum = createEEnum(MOPERATOR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		McorePackage theMcorePackage = (McorePackage) EPackage.Registry.INSTANCE
				.getEPackage(McorePackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI);
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);
		ObjectsPackage theObjectsPackage = (ObjectsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ObjectsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mAbstractExpressionEClass.getESuperTypes()
				.add(theMcorePackage.getMTyped());
		mAbstractExpressionWithBaseEClass.getESuperTypes()
				.add(this.getMAbstractExpression());
		mAbstractBaseDefinitionEClass.getESuperTypes()
				.add(theMcorePackage.getMTyped());
		mContainerBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mNumberBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mSelfBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mTargetBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mSourceBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mObjectBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mSimpleTypeConstantBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mLiteralConstantBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mObjectReferenceConstantBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mVariableBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mIteratorBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mParameterBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mAccumulatorBaseDefinitionEClass.getESuperTypes()
				.add(this.getMAbstractBaseDefinition());
		mAbstractLetEClass.getESuperTypes().add(theMcorePackage.getMVariable());
		mAbstractLetEClass.getESuperTypes().add(this.getMAbstractExpression());
		mAbstractNamedTupleEClass.getESuperTypes().add(this.getMAbstractLet());
		mAbstractTupleEntryEClass.getESuperTypes()
				.add(theMcorePackage.getMRepositoryElement());
		mTupleEClass.getESuperTypes().add(this.getMAbstractNamedTuple());
		mTupleEntryEClass.getESuperTypes().add(theMcorePackage.getMNamed());
		mTupleEntryEClass.getESuperTypes().add(this.getMAbstractTupleEntry());
		mNewObjecctEClass.getESuperTypes().add(this.getMAbstractNamedTuple());
		mNewObjectFeatureValueEClass.getESuperTypes()
				.add(this.getMAbstractTupleEntry());
		mNamedExpressionEClass.getESuperTypes().add(this.getMAbstractLet());
		mToplevelExpressionEClass.getESuperTypes()
				.add(this.getMAbstractExpression());
		mBaseChainEClass.getESuperTypes()
				.add(this.getMAbstractExpressionWithBase());
		mBaseChainEClass.getESuperTypes().add(this.getMAbstractChain());
		mChainEClass.getESuperTypes().add(this.getMBaseChain());
		mChainEClass.getESuperTypes().add(this.getMChainOrApplication());
		mCallArgumentEClass.getESuperTypes()
				.add(this.getMAbstractExpressionWithBase());
		mSubChainEClass.getESuperTypes().add(this.getMAbstractExpression());
		mSubChainEClass.getESuperTypes().add(this.getMAbstractChain());
		mChainOrApplicationEClass.getESuperTypes()
				.add(this.getMToplevelExpression());
		mDataValueExprEClass.getESuperTypes()
				.add(this.getMChainOrApplication());
		mLiteralValueExprEClass.getESuperTypes()
				.add(this.getMChainOrApplication());
		mAbstractIfEClass.getESuperTypes().add(this.getMAbstractExpression());
		mIfEClass.getESuperTypes().add(this.getMAbstractIf());
		mIfEClass.getESuperTypes().add(this.getMToplevelExpression());
		mThenEClass.getESuperTypes().add(this.getMAbstractExpression());
		mElseIfEClass.getESuperTypes().add(this.getMAbstractExpression());
		mElseEClass.getESuperTypes().add(this.getMAbstractExpression());
		mCollectionExpressionEClass.getESuperTypes()
				.add(this.getMAbstractExpression());
		mCollectionVarEClass.getESuperTypes()
				.add(this.getMAbstractExpression());
		mCollectionVarEClass.getESuperTypes()
				.add(theMcorePackage.getMVariable());
		mIteratorEClass.getESuperTypes().add(this.getMCollectionVar());
		mAccumulatorEClass.getESuperTypes().add(this.getMCollectionVar());
		mNamedConstantEClass.getESuperTypes().add(this.getMAbstractLet());
		mConstantLetEClass.getESuperTypes().add(this.getMAbstractExpression());
		mSimpleTypeConstantLetEClass.getESuperTypes()
				.add(this.getMConstantLet());
		mLiteralLetEClass.getESuperTypes().add(this.getMConstantLet());
		mObjectReferenceLetEClass.getESuperTypes().add(this.getMConstantLet());
		mApplicationEClass.getESuperTypes().add(this.getMChainOrApplication());

		// Initialize classes, features, and operations; add parameters
		initEClass(mAbstractExpressionEClass, MAbstractExpression.class,
				"MAbstractExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMAbstractExpression_AsCode(),
				ecorePackage.getEString(), "asCode", null, 0, 1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpression_AsBasicCode(),
				ecorePackage.getEString(), "asBasicCode", null, 0, 1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_Collector(),
				this.getMCollectionExpression(), null, "collector", null, 0, 1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_EntireScope(),
				this.getMAbstractBaseDefinition(), null, "entireScope", null, 0,
				-1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeBase(),
				this.getMBaseDefinition(), null, "scopeBase", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeSelf(),
				this.getMSelfBaseDefinition(), null, "scopeSelf", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeTrg(),
				this.getMTargetBaseDefinition(), null, "scopeTrg", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeObj(),
				this.getMObjectBaseDefinition(), null, "scopeObj", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeSimpleTypeConstants(),
				this.getMSimpleTypeConstantBaseDefinition(), null,
				"scopeSimpleTypeConstants", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeLiteralConstants(),
				this.getMLiteralConstantBaseDefinition(), null,
				"scopeLiteralConstants", null, 0, -1, MAbstractExpression.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeObjectReferenceConstants(),
				this.getMObjectReferenceConstantBaseDefinition(), null,
				"scopeObjectReferenceConstants", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeVariables(),
				this.getMVariableBaseDefinition(), null, "scopeVariables", null,
				0, -1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeIterator(),
				this.getMIteratorBaseDefinition(), null, "scopeIterator", null,
				0, -1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeAccumulator(),
				this.getMAccumulatorBaseDefinition(), null, "scopeAccumulator",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeParameters(),
				this.getMParameterBaseDefinition(), null, "scopeParameters",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeContainerBase(),
				this.getMContainerBaseDefinition(), null, "scopeContainerBase",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeNumberBase(),
				this.getMNumberBaseDefinition(), null, "scopeNumberBase", null,
				0, -1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ScopeSource(),
				this.getMSourceBaseDefinition(), null, "scopeSource", null, 0,
				-1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalEntireScope(),
				this.getMAbstractBaseDefinition(), null, "localEntireScope",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeBase(),
				this.getMBaseDefinition(), null, "localScopeBase", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeSelf(),
				this.getMSelfBaseDefinition(), null, "localScopeSelf", null, 0,
				-1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeTrg(),
				this.getMTargetBaseDefinition(), null, "localScopeTrg", null, 0,
				-1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeObj(),
				this.getMObjectBaseDefinition(), null, "localScopeObj", null, 0,
				-1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeSource(),
				this.getMSourceBaseDefinition(), null, "localScopeSource", null,
				0, -1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeSimpleTypeConstants(),
				this.getMSimpleTypeConstantBaseDefinition(), null,
				"localScopeSimpleTypeConstants", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeLiteralConstants(),
				this.getMLiteralConstantBaseDefinition(), null,
				"localScopeLiteralConstants", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMAbstractExpression_LocalScopeObjectReferenceConstants(),
				this.getMObjectReferenceConstantBaseDefinition(), null,
				"localScopeObjectReferenceConstants", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeVariables(),
				this.getMVariableBaseDefinition(), null, "localScopeVariables",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeIterator(),
				this.getMIteratorBaseDefinition(), null, "localScopeIterator",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeAccumulator(),
				this.getMAccumulatorBaseDefinition(), null,
				"localScopeAccumulator", null, 0, -1, MAbstractExpression.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeParameters(),
				this.getMParameterBaseDefinition(), null,
				"localScopeParameters", null, 0, -1, MAbstractExpression.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeNumberBaseDefinition(),
				this.getMNumberBaseDefinition(), null,
				"localScopeNumberBaseDefinition", null, 0, -1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_LocalScopeContainer(),
				this.getMContainerBaseDefinition(), null, "localScopeContainer",
				null, 0, -1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ContainingAnnotation(),
				theAnnotationsPackage.getMExprAnnotation(), null,
				"containingAnnotation", null, 0, 1, MAbstractExpression.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_ContainingExpression(),
				this.getMAbstractExpression(), null, "containingExpression",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_IsComplexExpression(),
				ecorePackage.getEBooleanObject(), "isComplexExpression", null,
				1, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpression_IsSubExpression(),
				ecorePackage.getEBooleanObject(), "isSubExpression", null, 1, 1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_CalculatedOwnType(),
				theMcorePackage.getMClassifier(), null, "calculatedOwnType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_CalculatedOwnMandatory(),
				ecorePackage.getEBooleanObject(), "calculatedOwnMandatory",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_CalculatedOwnSingular(),
				ecorePackage.getEBooleanObject(), "calculatedOwnSingular", null,
				0, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpression_CalculatedOwnSimpleType(),
				theMcorePackage.getSimpleType(), "calculatedOwnSimpleType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_SelfObjectType(),
				theMcorePackage.getMClassifier(), null, "selfObjectType", null,
				0, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_SelfObjectPackage(),
				theMcorePackage.getMPackage(), null, "selfObjectPackage", null,
				0, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_TargetObjectType(),
				theMcorePackage.getMClassifier(), null, "targetObjectType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_TargetSimpleType(),
				theMcorePackage.getSimpleType(), "targetSimpleType", null, 0, 1,
				MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_ObjectObjectType(),
				theMcorePackage.getMClassifier(), null, "objectObjectType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpression_ExpectedReturnType(),
				theMcorePackage.getMClassifier(), null, "expectedReturnType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_ExpectedReturnSimpleType(),
				theMcorePackage.getSimpleType(), "expectedReturnSimpleType",
				null, 0, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_IsReturnValueMandatory(),
				ecorePackage.getEBooleanObject(), "isReturnValueMandatory",
				null, 1, 1, MAbstractExpression.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_IsReturnValueSingular(),
				ecorePackage.getEBooleanObject(), "isReturnValueSingular", null,
				1, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpression_SrcObjectType(),
				theMcorePackage.getMClassifier(), null, "srcObjectType", null,
				0, 1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpression_SrcObjectSimpleType(),
				theMcorePackage.getSimpleType(), "srcObjectSimpleType", null, 0,
				1, MAbstractExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		initEOperation(getMAbstractExpression__GetShortCode(),
				ecorePackage.getEString(), "getShortCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractExpression__GetScope(),
				this.getMAbstractBaseDefinition(), "getScope", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mAbstractExpressionWithBaseEClass,
				MAbstractExpressionWithBase.class,
				"MAbstractExpressionWithBase", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMAbstractExpressionWithBase_BaseAsCode(),
				ecorePackage.getEString(), "baseAsCode", null, 0, 1,
				MAbstractExpressionWithBase.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpressionWithBase_Base(),
				this.getExpressionBase(), "base", null, 1, 1,
				MAbstractExpressionWithBase.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMAbstractExpressionWithBase_BaseDefinition(),
				this.getMAbstractBaseDefinition(), null, "baseDefinition", null,
				0, 1, MAbstractExpressionWithBase.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpressionWithBase_BaseVar(),
				theMcorePackage.getMVariable(), null, "baseVar", null, 0, 1,
				MAbstractExpressionWithBase.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractExpressionWithBase_BaseExitType(),
				theMcorePackage.getMClassifier(), null, "baseExitType", null, 0,
				1, MAbstractExpressionWithBase.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractExpressionWithBase_BaseExitSimpleType(),
				theMcorePackage.getSimpleType(), "baseExitSimpleType", null, 0,
				1, MAbstractExpressionWithBase.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpressionWithBase_BaseExitMandatory(),
				ecorePackage.getEBooleanObject(), "baseExitMandatory", null, 0,
				1, MAbstractExpressionWithBase.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractExpressionWithBase_BaseExitSingular(),
				ecorePackage.getEBooleanObject(), "baseExitSingular", null, 0,
				1, MAbstractExpressionWithBase.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		EOperation op = initEOperation(
				getMAbstractExpressionWithBase__BaseDefinition$Update__MAbstractBaseDefinition(),
				theSemanticsPackage.getXUpdate(), "baseDefinition$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMAbstractBaseDefinition(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractExpressionWithBase__AsCodeForBuiltIn(),
				ecorePackage.getEString(), "asCodeForBuiltIn", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractExpressionWithBase__AsCodeForConstants(),
				ecorePackage.getEString(), "asCodeForConstants", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractExpressionWithBase__AsCodeForVariables(),
				ecorePackage.getEString(), "asCodeForVariables", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mAbstractBaseDefinitionEClass, MAbstractBaseDefinition.class,
				"MAbstractBaseDefinition", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMAbstractBaseDefinition_CalculatedBase(),
				this.getExpressionBase(), "calculatedBase", null, 1, 1,
				MAbstractBaseDefinition.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractBaseDefinition_CalculatedAsCode(),
				ecorePackage.getEString(), "calculatedAsCode", null, 0, 1,
				MAbstractBaseDefinition.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractBaseDefinition_Debug(),
				ecorePackage.getEString(), "debug", null, 0, 1,
				MAbstractBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mContainerBaseDefinitionEClass,
				MContainerBaseDefinition.class, "MContainerBaseDefinition",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mBaseDefinitionEClass, MBaseDefinition.class,
				"MBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMBaseDefinition_ExpressionBase(),
				this.getExpressionBase(), "expressionBase", null, 1, 1,
				MBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mNumberBaseDefinitionEClass, MNumberBaseDefinition.class,
				"MNumberBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMNumberBaseDefinition_ExpressionBase(),
				this.getExpressionBase(), "expressionBase", null, 1, 1,
				MNumberBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(
				getMNumberBaseDefinition__CalculateAsCode__MAbstractExpression(),
				ecorePackage.getEString(), "calculateAsCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getMAbstractExpression(), "caller", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mSelfBaseDefinitionEClass, MSelfBaseDefinition.class,
				"MSelfBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mTargetBaseDefinitionEClass, MTargetBaseDefinition.class,
				"MTargetBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mSourceBaseDefinitionEClass, MSourceBaseDefinition.class,
				"MSourceBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mObjectBaseDefinitionEClass, MObjectBaseDefinition.class,
				"MObjectBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mSimpleTypeConstantBaseDefinitionEClass,
				MSimpleTypeConstantBaseDefinition.class,
				"MSimpleTypeConstantBaseDefinition", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSimpleTypeConstantBaseDefinition_NamedConstant(),
				this.getMNamedConstant(), null, "namedConstant", null, 0, 1,
				MSimpleTypeConstantBaseDefinition.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMSimpleTypeConstantBaseDefinition_ConstantLet(),
				this.getMSimpleTypeConstantLet(), null, "constantLet", null, 0,
				1, MSimpleTypeConstantBaseDefinition.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mLiteralConstantBaseDefinitionEClass,
				MLiteralConstantBaseDefinition.class,
				"MLiteralConstantBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMLiteralConstantBaseDefinition_NamedConstant(),
				this.getMNamedConstant(), null, "namedConstant", null, 0, 1,
				MLiteralConstantBaseDefinition.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteralConstantBaseDefinition_ConstantLet(),
				this.getMLiteralLet(), null, "constantLet", null, 0, 1,
				MLiteralConstantBaseDefinition.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mObjectReferenceConstantBaseDefinitionEClass,
				MObjectReferenceConstantBaseDefinition.class,
				"MObjectReferenceConstantBaseDefinition", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(
				getMObjectReferenceConstantBaseDefinition_NamedConstant(),
				this.getMNamedConstant(), null, "namedConstant", null, 0, 1,
				MObjectReferenceConstantBaseDefinition.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectReferenceConstantBaseDefinition_ConstantLet(),
				this.getMObjectReferenceLet(), null, "constantLet", null, 0, 1,
				MObjectReferenceConstantBaseDefinition.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mVariableBaseDefinitionEClass, MVariableBaseDefinition.class,
				"MVariableBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMVariableBaseDefinition_NamedExpression(),
				this.getMNamedExpression(), null, "namedExpression", null, 0, 1,
				MVariableBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMVariableBaseDefinition_VariableLet(),
				this.getMToplevelExpression(), null, "variableLet", null, 0, 1,
				MVariableBaseDefinition.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mIteratorBaseDefinitionEClass, MIteratorBaseDefinition.class,
				"MIteratorBaseDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMIteratorBaseDefinition_Iterator(),
				this.getMIterator(), null, "iterator", null, 0, 1,
				MIteratorBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mParameterBaseDefinitionEClass,
				MParameterBaseDefinition.class, "MParameterBaseDefinition",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMParameterBaseDefinition_Parameter(),
				theMcorePackage.getMParameter(), null, "parameter", null, 0, 1,
				MParameterBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mAccumulatorBaseDefinitionEClass,
				MAccumulatorBaseDefinition.class, "MAccumulatorBaseDefinition",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAccumulatorBaseDefinition_Accumulator(),
				this.getMAccumulator(), null, "accumulator", null, 0, 1,
				MAccumulatorBaseDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mAbstractLetEClass, MAbstractLet.class, "MAbstractLet",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mAbstractNamedTupleEClass, MAbstractNamedTuple.class,
				"MAbstractNamedTuple", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractNamedTuple_AbstractEntry(),
				this.getMAbstractTupleEntry(), null, "abstractEntry", null, 0,
				-1, MAbstractNamedTuple.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mAbstractTupleEntryEClass, MAbstractTupleEntry.class,
				"MAbstractTupleEntry", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractTupleEntry_Value(),
				this.getMToplevelExpression(), null, "value", null, 0, 1,
				MAbstractTupleEntry.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mTupleEClass, MTuple.class, "MTuple", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMTuple_Entry(), this.getMTupleEntry(), null, "entry",
				null, 0, -1, MTuple.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mTupleEntryEClass, MTupleEntry.class, "MTupleEntry",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mNewObjecctEClass, MNewObjecct.class, "MNewObjecct",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMNewObjecct_NewType(),
				theMcorePackage.getMClassifier(), null, "newType", null, 0, 1,
				MNewObjecct.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMNewObjecct_Entry(), this.getMNewObjectFeatureValue(),
				null, "entry", null, 0, -1, MNewObjecct.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mNewObjectFeatureValueEClass, MNewObjectFeatureValue.class,
				"MNewObjectFeatureValue", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMNewObjectFeatureValue_Feature(),
				theMcorePackage.getMProperty(), null, "feature", null, 0, 1,
				MNewObjectFeatureValue.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mNamedExpressionEClass, MNamedExpression.class,
				"MNamedExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMNamedExpression_Expression(),
				this.getMToplevelExpression(), null, "expression", null, 0, 1,
				MNamedExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamedExpression_DoAction(),
				this.getMNamedExpressionAction(), "doAction", null, 0, 1,
				MNamedExpression.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(
				getMNamedExpression__DoAction$Update__MNamedExpressionAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMNamedExpressionAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMNamedExpression__DefaultValue(), this.getMChain(),
				"defaultValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMNamedExpression__AllApplications(),
				this.getMApplication(), "allApplications", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mToplevelExpressionEClass, MToplevelExpression.class,
				"MToplevelExpression", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mAbstractChainEClass, MAbstractChain.class, "MAbstractChain",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractChain_ChainEntryType(),
				theMcorePackage.getMClassifier(), null, "chainEntryType", null,
				0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractChain_ChainAsCode(),
				ecorePackage.getEString(), "chainAsCode", null, 0, 1,
				MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_Element1(),
				theMcorePackage.getMProperty(), null, "element1", null, 0, 1,
				MAbstractChain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractChain_Element1Correct(),
				ecorePackage.getEBooleanObject(), "element1Correct", null, 0, 1,
				MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_Element2EntryType(),
				theMcorePackage.getMClassifier(), null, "element2EntryType",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_Element2(),
				theMcorePackage.getMProperty(), null, "element2", null, 0, 1,
				MAbstractChain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractChain_Element2Correct(),
				ecorePackage.getEBooleanObject(), "element2Correct", null, 0, 1,
				MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_Element3EntryType(),
				theMcorePackage.getMClassifier(), null, "element3EntryType",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_Element3(),
				theMcorePackage.getMProperty(), null, "element3", null, 0, 1,
				MAbstractChain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractChain_Element3Correct(),
				ecorePackage.getEBooleanObject(), "element3Correct", null, 0, 1,
				MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_CastType(),
				theMcorePackage.getMClassifier(), null, "castType", null, 0, 1,
				MAbstractChain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_LastElement(),
				theMcorePackage.getMProperty(), null, "lastElement", null, 0, 1,
				MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_ChainCalculatedType(),
				theMcorePackage.getMClassifier(), null, "chainCalculatedType",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAbstractChain_ChainCalculatedSimpleType(),
				theMcorePackage.getSimpleType(), "chainCalculatedSimpleType",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractChain_ChainCalculatedSingular(),
				ecorePackage.getEBooleanObject(), "chainCalculatedSingular",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMAbstractChain_Processor(), this.getMProcessor(),
				"processor", null, 0, 1, MAbstractChain.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractChain_ProcessorDefinition(),
				this.getMProcessorDefinition(), null, "processorDefinition",
				null, 0, 1, MAbstractChain.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMAbstractChain__ProcessorDefinition$Update__MProcessorDefinition(),
				theSemanticsPackage.getXUpdate(), "processorDefinition$Update",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMProcessorDefinition(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__Length(),
				ecorePackage.getEIntegerObject(), "length", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMAbstractChain__UnsafeElementAsCode__Integer(),
				ecorePackage.getEString(), "unsafeElementAsCode", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "step", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMAbstractChain__UnsafeChainStepAsCode__Integer(),
				ecorePackage.getEString(), "unsafeChainStepAsCode", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "step", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMAbstractChain__UnsafeChainAsCode__Integer(),
				ecorePackage.getEString(), "unsafeChainAsCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "fromStep", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMAbstractChain__UnsafeChainAsCode__Integer_Integer(),
				ecorePackage.getEString(), "unsafeChainAsCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "fromStep", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "toStep", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__AsCodeForOthers(),
				ecorePackage.getEString(), "asCodeForOthers", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractChain__CodeForLength1(),
				ecorePackage.getEString(), "codeForLength1", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractChain__CodeForLength2(),
				ecorePackage.getEString(), "codeForLength2", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractChain__CodeForLength3(),
				ecorePackage.getEString(), "codeForLength3", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractChain__ProcAsCode(),
				ecorePackage.getEString(), "procAsCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMAbstractChain__IsCustomCodeProcessor(),
				ecorePackage.getEBooleanObject(), "isCustomCodeProcessor", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__IsProcessorSetOperator(),
				ecorePackage.getEBooleanObject(), "isProcessorSetOperator", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__IsOwnXOCLOperator(),
				ecorePackage.getEBooleanObject(), "isOwnXOCLOperator", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcessorReturnsSingular(),
				ecorePackage.getEBooleanObject(), "processorReturnsSingular", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcessorIsSet(),
				ecorePackage.getEBooleanObject(), "processorIsSet", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__CreateProcessorDefinition(),
				this.getMProcessorDefinition(), "createProcessorDefinition", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForObject(),
				this.getMProcessorDefinition(), "procDefChoicesForObject", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForObjects(),
				this.getMProcessorDefinition(), "procDefChoicesForObjects", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForBoolean(),
				this.getMProcessorDefinition(), "procDefChoicesForBoolean", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForBooleans(),
				this.getMProcessorDefinition(), "procDefChoicesForBooleans", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForInteger(),
				this.getMProcessorDefinition(), "procDefChoicesForInteger", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForIntegers(),
				this.getMProcessorDefinition(), "procDefChoicesForIntegers", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForReal(),
				this.getMProcessorDefinition(), "procDefChoicesForReal", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForReals(),
				this.getMProcessorDefinition(), "procDefChoicesForReals", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForString(),
				this.getMProcessorDefinition(), "procDefChoicesForString", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForStrings(),
				this.getMProcessorDefinition(), "procDefChoicesForStrings", 0,
				-1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForDate(),
				this.getMProcessorDefinition(), "procDefChoicesForDate", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMAbstractChain__ProcDefChoicesForDates(),
				this.getMProcessorDefinition(), "procDefChoicesForDates", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mBaseChainEClass, MBaseChain.class, "MBaseChain",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMBaseChain_TypeMismatch(),
				ecorePackage.getEBooleanObject(), "typeMismatch", null, 0, 1,
				MBaseChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMBaseChain_CallArgument(), this.getMCallArgument(),
				this.getMCallArgument_Call(), "callArgument", null, 0, -1,
				MBaseChain.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMBaseChain_SubExpression(), this.getMSubChain(), null,
				"subExpression", null, 0, -1, MBaseChain.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMBaseChain_ContainedCollector(),
				this.getMCollectionExpression(), null, "containedCollector",
				null, 0, 1, MBaseChain.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMBaseChain_ChainCodeforSubchains(),
				ecorePackage.getEString(), "chainCodeforSubchains", null, 0, 1,
				MBaseChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMBaseChain_IsOwnXOCLOp(),
				ecorePackage.getEBooleanObject(), "isOwnXOCLOp", null, 0, 1,
				MBaseChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getMBaseChain__AutoCastWithProc(),
				ecorePackage.getEString(), "autoCastWithProc", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMBaseChain__OwnToApplyMismatch(),
				ecorePackage.getEBooleanObject(), "ownToApplyMismatch", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMBaseChain__UniqueChainNumber(),
				ecorePackage.getEString(), "uniqueChainNumber", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMBaseChain__ReuseFromOtherNoMoreUsedChain__MBaseChain(),
				ecorePackage.getEBooleanObject(),
				"reuseFromOtherNoMoreUsedChain", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMBaseChain(), "archetype", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMBaseChain__ResetToBase__ExpressionBase_MVariable(),
				ecorePackage.getEBooleanObject(), "resetToBase", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getExpressionBase(), "base", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, theMcorePackage.getMVariable(), "baseVar", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMBaseChain__ReuseFromOtherNoMoreUsedChainAsUpdate__MBaseChain(),
				theSemanticsPackage.getXUpdate(),
				"reuseFromOtherNoMoreUsedChainAsUpdate", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getMBaseChain(), "archetype", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMBaseChain__IsProcessorCheckEqualOperator(),
				ecorePackage.getEBooleanObject(),
				"isProcessorCheckEqualOperator", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMBaseChain__IsPrefixProcessor(),
				ecorePackage.getEBooleanObject(), "isPrefixProcessor", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMBaseChain__IsPostfixProcessor(),
				ecorePackage.getEBooleanObject(), "isPostfixProcessor", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMBaseChain__AsCodeForOthers(),
				ecorePackage.getEString(), "asCodeForOthers", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMBaseChain__UnsafeElementAsCode__Integer(),
				ecorePackage.getEString(), "unsafeElementAsCode", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "step", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMBaseChain__CodeForLength1(),
				ecorePackage.getEString(), "codeForLength1", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMBaseChain__CodeForLength2(),
				ecorePackage.getEString(), "codeForLength2", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMBaseChain__CodeForLength3(),
				ecorePackage.getEString(), "codeForLength3", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMBaseChain__AsCodeForVariables(),
				ecorePackage.getEString(), "asCodeForVariables", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mProcessorDefinitionEClass, MProcessorDefinition.class,
				"MProcessorDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMProcessorDefinition_Processor(),
				this.getMProcessor(), "processor", null, 0, 1,
				MProcessorDefinition.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mChainEClass, MChain.class, "MChain", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMChain_DoAction(), this.getMChainAction(), "doAction",
				null, 0, 1, MChain.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(getMChain__DoAction$Update__MChainAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMChainAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mCallArgumentEClass, MCallArgument.class, "MCallArgument",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMCallArgument_First(),
				ecorePackage.getEBooleanObject(), "first", null, 0, 1,
				MCallArgument.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMCallArgument_Last(),
				ecorePackage.getEBooleanObject(), "last", null, 0, 1,
				MCallArgument.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMCallArgument_Call(), this.getMBaseChain(),
				this.getMBaseChain_CallArgument(), "call", null, 0, 1,
				MCallArgument.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mSubChainEClass, MSubChain.class, "MSubChain", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMSubChain_PreviousExpression(),
				this.getMAbstractExpression(), null, "previousExpression", null,
				0, 1, MSubChain.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMSubChain_NextExpression(),
				this.getMAbstractExpression(), null, "nextExpression", null, 1,
				1, MSubChain.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMSubChain_DoAction(), this.getMSubChainAction(),
				"doAction", null, 0, 1, MSubChain.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMSubChain__DoAction$Update__MSubChainAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMSubChainAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMSubChain__IsLastSubchain(),
				ecorePackage.getEBooleanObject(), "isLastSubchain", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMSubChain__UniqueSubchainName(),
				ecorePackage.getEString(), "uniqueSubchainName", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMSubChain__ReturnSingularPureChain(),
				ecorePackage.getEBooleanObject(), "returnSingularPureChain", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMSubChain__AsCodeForOthers(),
				ecorePackage.getEString(), "asCodeForOthers", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMSubChain__CodeForLength1(),
				ecorePackage.getEString(), "codeForLength1", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMSubChain__CodeForLength2(),
				ecorePackage.getEString(), "codeForLength2", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMSubChain__CodeForLength3(),
				ecorePackage.getEString(), "codeForLength3", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mChainOrApplicationEClass, MChainOrApplication.class,
				"MChainOrApplication", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		initEClass(mDataValueExprEClass, MDataValueExpr.class, "MDataValueExpr",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMDataValueExpr_SimpleType(),
				theMcorePackage.getSimpleType(), "simpleType", null, 0, 1,
				MDataValueExpr.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMDataValueExpr_Value(), ecorePackage.getEString(),
				"value", null, 0, 1, MDataValueExpr.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(mLiteralValueExprEClass, MLiteralValueExpr.class,
				"MLiteralValueExpr", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMLiteralValueExpr_EnumerationType(),
				theMcorePackage.getMClassifier(), null, "enumerationType", null,
				1, 1, MLiteralValueExpr.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteralValueExpr_Value(),
				theMcorePackage.getMLiteral(), null, "value", null, 1, 1,
				MLiteralValueExpr.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMLiteralValueExpr__GetAllowedLiterals(),
				theMcorePackage.getMLiteral(), "getAllowedLiterals", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mAbstractIfEClass, MAbstractIf.class, "MAbstractIf",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAbstractIf_Condition(),
				this.getMChainOrApplication(), null, "condition", null, 1, 1,
				MAbstractIf.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractIf_ThenPart(), this.getMThen(), null,
				"thenPart", null, 1, 1, MAbstractIf.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractIf_ElseifPart(), this.getMElseIf(), null,
				"elseifPart", null, 0, -1, MAbstractIf.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAbstractIf_ElsePart(), this.getMElse(), null,
				"elsePart", null, 1, 1, MAbstractIf.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mIfEClass, MIf.class, "MIf", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMIf_ContainedCollector(),
				this.getMCollectionExpression(), null, "containedCollector",
				null, 0, 1, MIf.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMIf__DefaultValue(), this.getMChain(), "defaultValue",
				1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mThenEClass, MThen.class, "MThen", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMThen_Expression(), this.getMChainOrApplication(),
				null, "expression", null, 1, 1, MThen.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mElseIfEClass, MElseIf.class, "MElseIf", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMElseIf_Condition(), this.getMChainOrApplication(),
				null, "condition", null, 1, 1, MElseIf.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMElseIf_ThenPart(), this.getMThen(), null, "thenPart",
				null, 1, 1, MElseIf.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEOperation(getMElseIf__DefaultValue(), this.getMChain(),
				"defaultValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mElseEClass, MElse.class, "MElse", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMElse_Expression(), this.getMChainOrApplication(),
				null, "expression", null, 1, 1, MElse.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mCollectionExpressionEClass, MCollectionExpression.class,
				"MCollectionExpression", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMCollectionExpression_CollectionOperator(),
				this.getCollectionOperator(), "collectionOperator", null, 0, 1,
				MCollectionExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMCollectionExpression_Collection(),
				this.getMAbstractExpression(), null, "collection", null, 0, 1,
				MCollectionExpression.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMCollectionExpression_IteratorVar(),
				this.getMIterator(), null, "iteratorVar", null, 1, 1,
				MCollectionExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMCollectionExpression_AccumulatorVar(),
				this.getMAccumulator(), null, "accumulatorVar", null, 0, 1,
				MCollectionExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMCollectionExpression_Expression(),
				this.getMChainOrApplication(), null, "expression", null, 0, 1,
				MCollectionExpression.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMCollectionExpression__CollectionOperatorAsCode__CollectionOperator(),
				ecorePackage.getEString(), "collectionOperatorAsCode", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCollectionOperator(), "operator", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMCollectionExpression__CollectionOperatorAsCode(),
				ecorePackage.getEString(), "collectionOperatorAsCode", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMCollectionExpression__DefaultValue(),
				this.getMChain(), "defaultValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mCollectionVarEClass, MCollectionVar.class, "MCollectionVar",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMCollectionVar_ContainingCollection(),
				this.getMCollectionExpression(), null, "containingCollection",
				null, 0, 1, MCollectionVar.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(mIteratorEClass, MIterator.class, "MIterator", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mAccumulatorEClass, MAccumulator.class, "MAccumulator",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMAccumulator_AccDefinition(),
				this.getMChainOrApplication(), null, "accDefinition", null, 0,
				1, MAccumulator.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAccumulator_SimpleType(),
				theMcorePackage.getSimpleType(), "simpleType", null, 0, 1,
				MAccumulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMAccumulator_Type(), theMcorePackage.getMClassifier(),
				null, "type", null, 0, 1, MAccumulator.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAccumulator_Mandatory(),
				ecorePackage.getEBooleanObject(), "mandatory", "false", 0, 1,
				MAccumulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMAccumulator_Singular(),
				ecorePackage.getEBooleanObject(), "singular", "false", 0, 1,
				MAccumulator.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mNamedConstantEClass, MNamedConstant.class, "MNamedConstant",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMNamedConstant_Expression(), this.getMConstantLet(),
				null, "expression", null, 0, 1, MNamedConstant.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEOperation(getMNamedConstant__DefaultValue(), this.getMChain(),
				"defaultValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mConstantLetEClass, MConstantLet.class, "MConstantLet",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(
				getMConstantLet__AsSetString__String_String_String(),
				ecorePackage.getEString(), "asSetString", 1, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p1", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p2", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p3", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMConstantLet__InQuotes__String(),
				ecorePackage.getEString(), "inQuotes", 1, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p1", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mSimpleTypeConstantLetEClass, MSimpleTypeConstantLet.class,
				"MSimpleTypeConstantLet", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMSimpleTypeConstantLet_SimpleType(),
				theMcorePackage.getSimpleType(), "simpleType", null, 0, 1,
				MSimpleTypeConstantLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMSimpleTypeConstantLet_Constant1(),
				ecorePackage.getEString(), "constant1", null, 0, 1,
				MSimpleTypeConstantLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMSimpleTypeConstantLet_Constant2(),
				ecorePackage.getEString(), "constant2", null, 0, 1,
				MSimpleTypeConstantLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMSimpleTypeConstantLet_Constant3(),
				ecorePackage.getEString(), "constant3", null, 0, 1,
				MSimpleTypeConstantLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mLiteralLetEClass, MLiteralLet.class, "MLiteralLet",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMLiteralLet_EnumerationType(),
				theMcorePackage.getMClassifier(), null, "enumerationType", null,
				1, 1, MLiteralLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteralLet_Constant1(),
				theMcorePackage.getMLiteral(), null, "constant1", null, 0, 1,
				MLiteralLet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteralLet_Constant2(),
				theMcorePackage.getMLiteral(), null, "constant2", null, 0, 1,
				MLiteralLet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteralLet_Constant3(),
				theMcorePackage.getMLiteral(), null, "constant3", null, 0, 1,
				MLiteralLet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getMLiteralLet__GetAllowedLiterals(),
				theMcorePackage.getMLiteral(), "getAllowedLiterals", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mObjectReferenceLetEClass, MObjectReferenceLet.class,
				"MObjectReferenceLet", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMObjectReferenceLet_ObjectType(),
				theMcorePackage.getMClassifier(), null, "objectType", null, 1,
				1, MObjectReferenceLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectReferenceLet_Constant1(),
				theObjectsPackage.getMObject(), null, "constant1", null, 0, 1,
				MObjectReferenceLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectReferenceLet_Constant2(),
				theObjectsPackage.getMObject(), null, "constant2", null, 0, 1,
				MObjectReferenceLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMObjectReferenceLet_Constant3(),
				theObjectsPackage.getMObject(), null, "constant3", null, 0, 1,
				MObjectReferenceLet.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mApplicationEClass, MApplication.class, "MApplication",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMApplication_Operator(), this.getMOperator(),
				"operator", null, 0, 1, MApplication.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMApplication_Operands(),
				this.getMChainOrApplication(), null, "operands", null, 0, -1,
				MApplication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMApplication_ContainedCollector(),
				this.getMCollectionExpression(), null, "containedCollector",
				null, 0, 1, MApplication.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMApplication_DoAction(), this.getMApplicationAction(),
				"doAction", null, 0, 1, MApplication.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMApplication__DoAction$Update__MApplicationAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMApplicationAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMApplication__OperatorAsCode(),
				ecorePackage.getEString(), "operatorAsCode", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMApplication__IsOperatorPrefix(),
				ecorePackage.getEBooleanObject(), "isOperatorPrefix", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorInfix(),
				ecorePackage.getEBooleanObject(), "isOperatorInfix", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorPostfix(),
				ecorePackage.getEBooleanObject(), "isOperatorPostfix", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorUnaryFunction(),
				ecorePackage.getEBooleanObject(), "isOperatorUnaryFunction", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorSetOperator(),
				ecorePackage.getEBooleanObject(), "isOperatorSetOperator", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__DefaultValue(), this.getMChain(),
				"defaultValue", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorUnaryFunctionTwoParam(),
				ecorePackage.getEBooleanObject(),
				"isOperatorUnaryFunctionTwoParam", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__IsOperatorParameterUnary(),
				ecorePackage.getEBooleanObject(), "isOperatorParameterUnary", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__UniqueApplyNumber(),
				ecorePackage.getEIntegerObject(), "uniqueApplyNumber", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__DefinesOperatorBusinessLogic(),
				ecorePackage.getEBooleanObject(),
				"definesOperatorBusinessLogic", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__CodeForLogicOperator(),
				ecorePackage.getEString(), "codeForLogicOperator", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMApplication__GetCalculatedSimpleDifferentTypes(),
				theMcorePackage.getSimpleType(),
				"getCalculatedSimpleDifferentTypes", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mOperatorDefinitionEClass, MOperatorDefinition.class,
				"MOperatorDefinition", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMOperatorDefinition_MOperator(), this.getMOperator(),
				"mOperator", null, 0, 1, MOperatorDefinition.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(expressionBaseEEnum, ExpressionBase.class, "ExpressionBase");
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.UNDEFINED);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.SELF_OBJECT);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.NULL_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.ZERO_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.ONE_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.MINUS_ONE_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.FALSE_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.TRUE_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.EMPTY_STRING_VALUE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.CONSTANT_VALUE);
		addEEnumLiteral(expressionBaseEEnum,
				ExpressionBase.CONSTANT_LITERAL_VALUE);
		addEEnumLiteral(expressionBaseEEnum,
				ExpressionBase.CONSTANT_OBJECT_REFERENCE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.TARGET);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.SYSTEM);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.VARIABLE);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.ITERATOR);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.PARAMETER);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.EMPTY_COLLECTION);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.ACCUMULATOR);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.OBJECT_OBJECT);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.ECONTAINER);
		addEEnumLiteral(expressionBaseEEnum, ExpressionBase.SOURCE);

		initEEnum(mNamedExpressionActionEEnum, MNamedExpressionAction.class,
				"MNamedExpressionAction");
		addEEnumLiteral(mNamedExpressionActionEEnum, MNamedExpressionAction.DO);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_BASE_IN_MAIN_CHAIN);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_ARGUMENT_IN_MAIN_CHAIN);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_BASE_IN_PREVIOUS_CHAIN);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_ARGUMENT_IN_PREVIOUS_CHAIN);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_OPERAND_IN_PREVIOUS_APPLY);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_COND_IN_PREVIOUS_IF);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_THEN_IN_PREVIOUS_IF);
		addEEnumLiteral(mNamedExpressionActionEEnum,
				MNamedExpressionAction.AS_ELSE_IN_PREVIOUS_IF);

		initEEnum(mProcessorEEnum, MProcessor.class, "MProcessor");
		addEEnumLiteral(mProcessorEEnum, MProcessor.NONE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_NULL);
		addEEnumLiteral(mProcessorEEnum, MProcessor.NOT_NULL);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_BOOLEAN);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_FALSE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_TRUE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.NOT);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_INTEGER);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_ZERO);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_ONE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.PLUS_ONE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.MINUS_ONE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TIMES_MINUS_ONE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.ABSOLUTE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.ROUND);
		addEEnumLiteral(mProcessorEEnum, MProcessor.FLOOR);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_REAL);
		addEEnumLiteral(mProcessorEEnum, MProcessor.ONE_DIVIDED_BY);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_STRING);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TRIM);
		addEEnumLiteral(mProcessorEEnum, MProcessor.ALL_LOWER_CASE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.ALL_UPPER_CASE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.FIRST_UPPER_CASE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.CAMEL_CASE_LOWER);
		addEEnumLiteral(mProcessorEEnum, MProcessor.CAMEL_CASE_UPPER);
		addEEnumLiteral(mProcessorEEnum, MProcessor.CAMEL_CASE_TO_BUSINESS);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_DATE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.YEAR);
		addEEnumLiteral(mProcessorEEnum, MProcessor.MONTH);
		addEEnumLiteral(mProcessorEEnum, MProcessor.DAY);
		addEEnumLiteral(mProcessorEEnum, MProcessor.HOUR);
		addEEnumLiteral(mProcessorEEnum, MProcessor.MINUTE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.SECOND);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_YYYY_MM_DD);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TO_HH_MM);
		addEEnumLiteral(mProcessorEEnum, MProcessor.AS_ORDERED_SET);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_EMPTY);
		addEEnumLiteral(mProcessorEEnum, MProcessor.NOT_EMPTY);
		addEEnumLiteral(mProcessorEEnum, MProcessor.SIZE);
		addEEnumLiteral(mProcessorEEnum, MProcessor.AND);
		addEEnumLiteral(mProcessorEEnum, MProcessor.OR);
		addEEnumLiteral(mProcessorEEnum, MProcessor.SUM);
		addEEnumLiteral(mProcessorEEnum, MProcessor.FIRST);
		addEEnumLiteral(mProcessorEEnum, MProcessor.LAST);
		addEEnumLiteral(mProcessorEEnum, MProcessor.HEAD);
		addEEnumLiteral(mProcessorEEnum, MProcessor.TAIL);
		addEEnumLiteral(mProcessorEEnum, MProcessor.IS_INVALID);
		addEEnumLiteral(mProcessorEEnum, MProcessor.CONTAINER);

		initEEnum(mChainActionEEnum, MChainAction.class, "MChainAction");
		addEEnumLiteral(mChainActionEEnum, MChainAction.DO);
		addEEnumLiteral(mChainActionEEnum, MChainAction.INTO_DATA_CONSTANT);
		addEEnumLiteral(mChainActionEEnum, MChainAction.INTO_LITERAL_CONSTANT);
		addEEnumLiteral(mChainActionEEnum, MChainAction.INTO_APPLY);
		addEEnumLiteral(mChainActionEEnum,
				MChainAction.INTO_COND_OF_IF_THEN_ELSE);
		addEEnumLiteral(mChainActionEEnum,
				MChainAction.INTO_THEN_OF_IF_THEN_ELSE);
		addEEnumLiteral(mChainActionEEnum,
				MChainAction.INTO_ELSE_OF_IF_THEN_ELSE);
		addEEnumLiteral(mChainActionEEnum, MChainAction.ARGUMENT);
		addEEnumLiteral(mChainActionEEnum,
				MChainAction.DATA_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mChainActionEEnum,
				MChainAction.LITERAL_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mChainActionEEnum, MChainAction.SUB_CHAIN);
		addEEnumLiteral(mChainActionEEnum, MChainAction.COLLECTION_OP);

		initEEnum(mSubChainActionEEnum, MSubChainAction.class,
				"MSubChainAction");
		addEEnumLiteral(mSubChainActionEEnum, MSubChainAction.DO);
		addEEnumLiteral(mSubChainActionEEnum, MSubChainAction.ARGUMENT);
		addEEnumLiteral(mSubChainActionEEnum, MSubChainAction.SUB_CHAIN);
		addEEnumLiteral(mSubChainActionEEnum,
				MSubChainAction.DATA_CONSTANT_AS_ARGUMENT);
		addEEnumLiteral(mSubChainActionEEnum,
				MSubChainAction.LITERAL_CONSTANT_AS_ARGUMENT);

		initEEnum(collectionOperatorEEnum, CollectionOperator.class,
				"CollectionOperator");
		addEEnumLiteral(collectionOperatorEEnum, CollectionOperator.COLLECT);
		addEEnumLiteral(collectionOperatorEEnum, CollectionOperator.SELECT);
		addEEnumLiteral(collectionOperatorEEnum, CollectionOperator.ITERATE);
		addEEnumLiteral(collectionOperatorEEnum, CollectionOperator.CLOSURE);

		initEEnum(mApplicationActionEEnum, MApplicationAction.class,
				"MApplicationAction");
		addEEnumLiteral(mApplicationActionEEnum, MApplicationAction.DO);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.OPERAND_CHAIN);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.OPERAND_APPLY);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.OPERAND_DATA_CONSTANT);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.OPERAND_LITERAL);
		addEEnumLiteral(mApplicationActionEEnum, MApplicationAction.INTO_CHAIN);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.INTO_COND_OF_IF_THEN_ELSE);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.INTO_THEN_OF_IF_THEN_ELSE);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.INTO_ELSE_OF_IF_THEN_ELSE);
		addEEnumLiteral(mApplicationActionEEnum,
				MApplicationAction.REMOVE_APPLY);

		initEEnum(mOperatorEEnum, MOperator.class, "MOperator");
		addEEnumLiteral(mOperatorEEnum, MOperator.EQUAL);
		addEEnumLiteral(mOperatorEEnum, MOperator.NOT_EQUAL);
		addEEnumLiteral(mOperatorEEnum, MOperator.GREATER);
		addEEnumLiteral(mOperatorEEnum, MOperator.SMALLER);
		addEEnumLiteral(mOperatorEEnum, MOperator.GREATER_EQUAL);
		addEEnumLiteral(mOperatorEEnum, MOperator.SMALLER_EQUAL);
		addEEnumLiteral(mOperatorEEnum, MOperator.NOT_OP);
		addEEnumLiteral(mOperatorEEnum, MOperator.OCL_AND);
		addEEnumLiteral(mOperatorEEnum, MOperator.OCL_OR);
		addEEnumLiteral(mOperatorEEnum, MOperator.XOR_OP);
		addEEnumLiteral(mOperatorEEnum, MOperator.IMPLIES_OP);
		addEEnumLiteral(mOperatorEEnum, MOperator.PLUS);
		addEEnumLiteral(mOperatorEEnum, MOperator.MINUS);
		addEEnumLiteral(mOperatorEEnum, MOperator.TIMES);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIVIDE);
		addEEnumLiteral(mOperatorEEnum, MOperator.MOD);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIV);
		addEEnumLiteral(mOperatorEEnum, MOperator.CONCAT);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_INCLUDING);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_EXCLUDING);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_UNION);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_EXCLUDES);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_INCLUDES);
		addEEnumLiteral(mOperatorEEnum, MOperator.INDEX_OF);
		addEEnumLiteral(mOperatorEEnum, MOperator.AT);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_AS_ORDERED_SET);
		addEEnumLiteral(mOperatorEEnum, MOperator.TO_STRING);
		addEEnumLiteral(mOperatorEEnum, MOperator.TRIM);
		addEEnumLiteral(mOperatorEEnum, MOperator.SUB_STRING);
		addEEnumLiteral(mOperatorEEnum, MOperator.AND_OP);
		addEEnumLiteral(mOperatorEEnum, MOperator.OR_OP);
		addEEnumLiteral(mOperatorEEnum, MOperator.BUSINESS_NOT);
		addEEnumLiteral(mOperatorEEnum, MOperator.BUSINESS_IMPLIES);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_EXCLUDES_ALL);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_INCLUDES_ALL);
		addEEnumLiteral(mOperatorEEnum, MOperator.INTERSECTION);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_YEARS);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_DAYS);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_SECONDS);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_MINUTES);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_MONTH);
		addEEnumLiteral(mOperatorEEnum, MOperator.DIFF_IN_HRS);
		addEEnumLiteral(mOperatorEEnum, MOperator.OCL_IS_INVALID);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_FIRST);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_IS_EMPTY);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_LAST);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_NOT_EMPTY);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_SIZE);
		addEEnumLiteral(mOperatorEEnum, MOperator.SET_SUM);
		addEEnumLiteral(mOperatorEEnum, MOperator.OCL_IS_UNDEFINED);

		// Create annotations
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mAbstractExpressionEClass, source, new String[] {
				"kindLabelDerive", "\'OVERRIDE IN SUBCLASS\'\n",
				"calculatedTypeDerive",
				"if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedType\nendif\nendif\n",
				"calculatedMandatoryDerive",
				"if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnMandatory\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedMandatory\nendif\nendif\n",
				"calculatedSingularDerive",
				"if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnSingular\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedSingular\nendif\nendif\n",
				"calculatedSimpleTypeDerive",
				"if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen calculatedOwnSimpleType\n  else if collector.oclIsUndefined()\n  then null\n  else collector.calculatedSimpleType\nendif\nendif\n" });
		addAnnotation(mAbstractBaseDefinitionEClass, source, new String[] {
				"calculatedSimpleTypeDerive", "SimpleType::None" });
		addAnnotation(mContainerBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive", "\'eContainer()\'\n",
						"calculatedBaseDerive", "ExpressionBase::EContainer",
						"calculatedMandatoryDerive", "true\n",
						"calculatedSingularDerive", "true\n",
						"calculatedSimpleTypeDerive", "SimpleType::Object" });
		addAnnotation(mBaseDefinitionEClass, source, new String[] {
				"calculatedBaseDerive", "expressionBase\n",
				"calculatedAsCodeDerive",
				"if expressionBase=ExpressionBase::NullValue then \'null\'\r\nelse if expressionBase=ExpressionBase::ZeroValue then \'0\'\r\nelse if expressionBase=ExpressionBase::OneValue then \'1\'\r\nelse if expressionBase=ExpressionBase::MinusOneValue then \'-1\'\r\nelse if expressionBase=ExpressionBase::FalseValue then \'false\'\r\nelse if expressionBase=ExpressionBase::TrueValue then \'true\'\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then \'\\\'\\\'\'\r\nelse if expressionBase=ExpressionBase::EmptyCollection then \'OrderedSet{}\'\r\nelse \'<?>\' endif endif endif endif endif endif endif endif",
				"calculatedMandatoryDerive",
				"if expressionBase=ExpressionBase::EmptyCollection then false\r\nelse true endif",
				"calculatedSingularDerive",
				"if expressionBase=ExpressionBase::EmptyCollection then false\r\nelse true endif",
				"calculatedSimpleTypeDerive",
				"if expressionBase=ExpressionBase::NullValue or expressionBase=ExpressionBase::EmptyCollection then SimpleType::None\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then SimpleType::String\r\nelse if expressionBase=ExpressionBase::ZeroValue \r\n  or expressionBase=ExpressionBase::OneValue \r\n  or expressionBase=ExpressionBase::MinusOneValue then SimpleType::Integer\r\nelse SimpleType::Boolean endif endif endif" });
		addAnnotation(mNumberBaseDefinitionEClass, source,
				new String[] { "calculatedBaseDerive", "expressionBase\n",
						"calculatedAsCodeDerive",
						" if expressionBase=ExpressionBase::ZeroValue then \'0\'\r\nelse if expressionBase=ExpressionBase::OneValue then \'1\'\r\nelse if expressionBase=ExpressionBase::MinusOneValue then \'-1\'\r\nelse \'\' endif endif endif",
						"calculatedSimpleTypeDerive",
						"if expressionBase=ExpressionBase::NullValue or expressionBase=ExpressionBase::EmptyCollection then SimpleType::None\r\nelse if expressionBase=ExpressionBase::EmptyStringValue then SimpleType::String\r\nelse if expressionBase=ExpressionBase::ZeroValue \r\n  or expressionBase=ExpressionBase::OneValue \r\n  or expressionBase=ExpressionBase::MinusOneValue then SimpleType::Integer\r\nelse SimpleType::Boolean endif endif endif",
						"calculatedSingularDerive", "true\n" });
		addAnnotation(mSelfBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive", "\'self\'\n",
						"calculatedBaseDerive", "ExpressionBase::SelfObject",
						"calculatedMandatoryDerive", "true\n",
						"calculatedTypeDerive",
						"if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).selfObjectType\r\nelse null\r\nendif\r\n",
						"calculatedSingularDerive", "true\n" });
		addAnnotation(mTargetBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive", "\'trg\'\n",
						"calculatedBaseDerive", "ExpressionBase::Target",
						"calculatedMandatoryDerive", "true\n",
						"calculatedTypeDerive",
						"if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).targetObjectType\r\nelse null\r\nendif\r\n",
						"calculatedSingularDerive", "true\n" });
		addAnnotation(mSourceBaseDefinitionEClass, source,
				new String[] { "calculatedSimpleTypeDerive",
						"let srcSimple:SimpleType =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).srcObjectSimpleType\r\nelse null\r\nendif\r\nin \r\nif srcSimple.oclIsUndefined() then SimpleType::None else srcSimple endif ",
						"calculatedBaseDerive", "ExpressionBase::Source",
						"calculatedAsCodeDerive", "\'src\'",
						"calculatedTypeDerive",
						"let srcType:MClassifier =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).srcObjectType\r\nelse null\r\nendif\r\nin \r\nif srcType.oclIsUndefined() then null else srcType endif",
						"calculatedSingularDerive", "true\n" });
		addAnnotation(mObjectBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive", "\'obj\'\n",
						"calculatedBaseDerive", "ExpressionBase::ObjectObject",
						"calculatedMandatoryDerive", "true",
						"calculatedTypeDerive",
						"if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).objectObjectType\r\nelse null\r\nendif\r\n",
						"calculatedSingularDerive", "true\n" });
		addAnnotation(mSimpleTypeConstantBaseDefinitionEClass, source,
				new String[] { "calculatedBaseDerive",
						"ExpressionBase::ConstantValue",
						"calculatedAsCodeDerive",
						"if namedConstant.oclIsUndefined() then \'MISSING EXPRESSION\' else namedConstant.eName endif",
						"calculatedMandatoryDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedMandatory endif",
						"calculatedSingularDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedSingular endif",
						"calculatedSimpleTypeDerive",
						"if constantLet.oclIsUndefined() then SimpleType::None\r\nelse constantLet.calculatedSimpleType\r\nendif" });
		addAnnotation(mLiteralConstantBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive",
						"if namedConstant.oclIsUndefined() then \'MISSING EXPRESSION\' else namedConstant.eName endif",
						"calculatedBaseDerive",
						"ExpressionBase::ConstantLiteralValue",
						"calculatedMandatoryDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedMandatory endif",
						"calculatedTypeDerive",
						"if constantLet.oclIsUndefined() then null\r\nelse constantLet.calculatedType endif",
						"calculatedSingularDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedSingular endif" });
		addAnnotation(mObjectReferenceConstantBaseDefinitionEClass, source,
				new String[] { "calculatedAsCodeDerive",
						"if namedConstant.oclIsUndefined() then \'MISSING EXPRESSION\' else namedConstant.eName endif",
						"calculatedBaseDerive",
						"ExpressionBase::ConstantObjectReference",
						"calculatedMandatoryDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedMandatory endif",
						"calculatedTypeDerive",
						"if constantLet.oclIsUndefined() then null\r\nelse constantLet.calculatedType endif",
						"calculatedSingularDerive",
						"if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedSingular endif" });
		addAnnotation(mVariableBaseDefinitionEClass, source, new String[] {
				"calculatedBaseDerive", "ExpressionBase::Variable",
				"calculatedAsCodeDerive",
				"if (let e: Boolean = namedExpression.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'MISSING EXPRESSION\'\n  else if namedExpression.oclIsUndefined()\n  then null\n  else namedExpression.eName\nendif\nendif\n",
				"calculatedMandatoryDerive",
				"if (let e: Boolean = variableLet.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen true\n  else if variableLet.oclIsUndefined()\n  then null\n  else variableLet.calculatedMandatory\nendif\nendif\n",
				"calculatedSingularDerive",
				"if namedExpression.oclIsUndefined() then null\nelse namedExpression.calculatedSingular\nendif\n",
				"calculatedSimpleTypeDerive",
				"if variableLet.oclIsUndefined() then SimpleType::None\r\nelse variableLet.calculatedSimpleType\r\nendif",
				"calculatedTypeDerive",
				"if (let e: Boolean = variableLet.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if variableLet.oclIsUndefined()\n  then null\n  else variableLet.calculatedType\nendif\nendif\n" });
		addAnnotation(mIteratorBaseDefinitionEClass, source, new String[] {
				"calculatedBaseDerive", "ExpressionBase::Iterator",
				"calculatedAsCodeDerive",
				"if iterator.oclIsUndefined() then \'MISSING ITERATOR\' else iterator.eName endif",
				"calculatedMandatoryDerive", "true", "calculatedSingularDerive",
				"true", "calculatedSimpleTypeDerive",
				"if iterator.oclIsUndefined() then SimpleType::None\r\nelse iterator.calculatedSimpleType\r\nendif",
				"calculatedTypeDerive",
				"if iterator.oclIsUndefined() then null\r\nelse iterator.calculatedType endif" });
		addAnnotation(mParameterBaseDefinitionEClass, source, new String[] {
				"calculatedBaseDerive",
				"mcore::expressions::ExpressionBase::Parameter\n",
				"calculatedAsCodeDerive",
				"if parameter.oclIsUndefined()then \'MISSING PARAMETER\' else parameter.eName endif",
				"calculatedMandatoryDerive",
				"if parameter.oclIsUndefined() then true\r\nelse parameter.calculatedMandatory endif",
				"calculatedSingularDerive",
				"if parameter.oclIsUndefined() then true\r\nelse parameter.calculatedSingular endif",
				"calculatedSimpleTypeDerive",
				"if parameter.oclIsUndefined() then SimpleType::None\r\nelse parameter.calculatedSimpleType\r\nendif",
				"calculatedTypeDerive",
				"if parameter.oclIsUndefined() then null\r\nelse parameter.calculatedType\r\nendif" });
		addAnnotation(mAccumulatorBaseDefinitionEClass, source, new String[] {
				"calculatedBaseDerive", "ExpressionBase::Accumulator",
				"calculatedAsCodeDerive",
				"if accumulator.oclIsUndefined() then \'MISSING ACCUMULATOR\' else accumulator.eName endif",
				"calculatedMandatoryDerive",
				"if accumulator.oclIsUndefined() then false\r\nelse accumulator.calculatedMandatory endif",
				"calculatedSingularDerive",
				"if accumulator.oclIsUndefined() then true\r\nelse accumulator.calculatedSingular endif",
				"calculatedSimpleTypeDerive",
				"if accumulator.oclIsUndefined() then SimpleType::None\r\nelse accumulator.calculatedSimpleType\r\nendif",
				"calculatedTypeDerive",
				"if accumulator.oclIsUndefined() then null\r\nelse accumulator.calculatedType endif" });
		addAnnotation(mAbstractTupleEntryEClass, source,
				new String[] { "kindLabelDerive", "\'Entry\'\n" });
		addAnnotation(mTupleEClass, source, new String[] {
				"abstractEntryDerive", "entry->asOrderedSet()\n",
				"kindLabelDerive", "\'Tuple\'\n", "asBasicCodeDerive",
				"let c:String= \'Tuple{TODOENTRIES)\' in\r\n\tif name.oclIsUndefined() or name=\'\' \r\nthen /* The expression shoudl be returned here, as this is the final result */\r\n c\r\nelse \'let \'.concat(eName).concat(\': \').concat(\'TYPE TODO\').concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n" });
		addAnnotation(mNewObjecctEClass, source,
				new String[] { "abstractEntryDerive", "entry->asOrderedSet()\n",
						"asBasicCodeDerive",
						"let c:String= \'Tuple{\' in\r\n c.concat(\r\n    if entry->isEmpty() then \'}\' else  self.entry->iterate(\r\n\t\t  temp1 : MNewObjectFeatureValue; s: String = \'\' | \r\n\t\t  s.concat(if temp1.feature.eName.oclIsUndefined() then \'\' else temp1.feature.eName endif).concat(\'=\').concat(if temp1.value.asCode.oclIsUndefined() then \'\' else temp1.value.asCode endif).concat(if entry->at(entry->indexOf(temp1)+1).oclIsUndefined() then \'\' else \',\' endif )).concat(\'}\') endif)\r\n",
						"kindLabelDerive", "\'New Object\'\n" });
		addAnnotation(mNamedExpressionEClass, source, new String[] {
				"kindLabelDerive",
				"\'Where\'\r\n/*\r\nlet c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in\r\nif c.oclIsUndefined() then \'Let\'\r\n\telse if c.namedExpression->last() = self\r\n\t\tthen \'Definition\'\r\n\t\telse \'Let\' endif\r\nendif\r\n*/",
				"asBasicCodeDerive",
				"if expression.oclIsUndefined() then \'MISSING EXPRESSION\' else\r\n\r\nlet c: String = expression.asCode in\r\nlet t: String = \r\nif   expression.oclIsKindOf(expressions::MChain) then\r\nif  expression.oclAsType(expressions::MChain).base = ExpressionBase::EContainer and expression.oclAsType(expressions::MChain).baseExitSimpleType = SimpleType::Object then \r\ntypeAsOcl( selfObjectPackage, expression.calculatedType, SimpleType::None, expression.calculatedSingular ) else\r\n\r\ntypeAsOcl( selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular )\r\nendif\r\nelse\r\ntypeAsOcl( selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular )\r\n\r\n\r\n  endif\r\nin\r\nif name.oclIsUndefined() or name=\'\' \r\nthen /* The expression shoudl be returned here, as this is the final result */\r\n  if expression.oclIsKindOf(MAbstractExpressionWithBase)\r\n\tthen /* We do a special case for null...since it cannot simply be returned as such */\r\n\t\tlet chain: MAbstractExpressionWithBase = expression.oclAsType(MAbstractExpressionWithBase) in\r\n\t\tif chain.base = ExpressionBase::NullValue\r\n\t\t\tthen \'let nl: \'.concat(  typeAsOcl(selfObjectPackage, containingAnnotation.expectedReturnTypeOfAnnotation, containingAnnotation.expectedReturnSimpleTypeOfAnnotation, self.isReturnValueSingular) ).concat(\' = null in nl\')\r\n\t\t\telse c endif\r\n  \telse c endif\r\nelse \'let \'.concat(eName).concat(\': \').concat(t).concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n\r\nendif ",
				"calculatedOwnMandatoryDerive",
				"if expression.oclIsUndefined() then false\r\nelse expression.calculatedMandatory endif",
				"calculatedOwnSingularDerive",
				"\r\nif expression.oclIsUndefined() then true\r\nelse if expression.oclIsTypeOf(expressions::MChain) \r\nthen  if expression.oclAsType(expressions::MChain).processor <> MProcessor::None then true else\r\nexpression.calculatedSingular endif \r\nelse \r\nexpression.calculatedSingular\r\nendif\r\nendif\r\n",
				"calculatedOwnSimpleTypeDerive",
				"if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif",
				"calculatedOwnTypeDerive",
				"if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif" });
		addAnnotation(mBaseChainEClass, source, new String[] {
				"chainEntryTypeDerive", "self.baseExitType",
				"calculatedOwnMandatoryDerive",
				"let m1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedMandatory endif in\r\nlet m2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedMandatory endif in\r\nlet m3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedMandatory endif in\r\nbaseExitMandatory and m1 and m2 and m3",
				"calculatedOwnSingularDerive",
				"if self.processorIsSet() \r\n     then  self.processorReturnsSingular()\r\n     else self.chainCalculatedSingular endif",
				"calculatedOwnTypeDerive",
				"if calculatedOwnSimpleType = mcore::SimpleType::None or calculatedOwnSimpleType = mcore::SimpleType::Object then\r\nif castType.oclIsUndefined() \r\n  then chainCalculatedType\r\n  else castType\r\nendif\r\nelse \r\nnull\r\nendif",
				"calculatedOwnSimpleTypeDerive",
				"let p:mcore::expressions::MProcessor = processor in\nif p <> mcore::expressions::MProcessor::None\n  then\n     if      p = mcore::expressions::MProcessor::ToInteger or\n             p = mcore::expressions::MProcessor::Size  or\n             p = mcore::expressions::MProcessor::Round or\n             p = mcore::expressions::MProcessor::Floor or\n             p = mcore::expressions::MProcessor::Year or\n             p = mcore::expressions::MProcessor::Minute or\n             p = mcore::expressions::MProcessor::Month or\n             p = mcore::expressions::MProcessor::Second or\n             p = mcore::expressions::MProcessor::Day or\n             p = mcore::expressions::MProcessor::Hour\n       then mcore::SimpleType::Integer\n     else if p = mcore::expressions::MProcessor::ToString or  \n             p = mcore::expressions::MProcessor::ToYyyyMmDd or\n             p = mcore::expressions::MProcessor::ToHhMm or\n             p = mcore::expressions::MProcessor::AllLowerCase or\n             p = mcore::expressions::MProcessor::AllUpperCase or\n             p = mcore::expressions::MProcessor::CamelCaseLower or\n             p = mcore::expressions::MProcessor::CamelCaseToBusiness or\n             p = mcore::expressions::MProcessor::CamelCaseUpper or\n             p = mcore::expressions::MProcessor::FirstUpperCase or\n             p = mcore::expressions::MProcessor::Trim\n       then mcore::SimpleType::String\n     else if p = mcore::expressions::MProcessor::ToReal or\n             p = mcore::expressions::MProcessor::OneDividedBy or\n             p = mcore::expressions::MProcessor::Sum\n       then mcore::SimpleType::Double\n     else if p = mcore::expressions::MProcessor::ToBoolean or\n             p = mcore::expressions::MProcessor::IsEmpty or\n             p = mcore::expressions::MProcessor::NotEmpty or\n             p = mcore::expressions::MProcessor::Not  or\n             p = mcore::expressions::MProcessor::IsFalse or\n             p = mcore::expressions::MProcessor::IsTrue or\n             p = mcore::expressions::MProcessor::IsZero or\n             p = mcore::expressions::MProcessor::IsOne or\n             p = mcore::expressions::MProcessor::NotNull or\n             p = mcore::expressions::MProcessor::IsNull or\n             p = mcore::expressions::MProcessor::IsInvalid or\n             p = mcore::expressions::MProcessor::And or\n             p= mcore::expressions::MProcessor::Or\n       then mcore::SimpleType::Boolean\n      else if p = mcore::expressions::MProcessor::ToDate \n       then mcore::SimpleType::Date\n     else if self.lastElement=null\n       then self.baseExitSimpleType\n     else   self.lastElement.simpleType\n     endif endif endif endif endif endif\n       \n  else if self.lastElement=null\n     then self.baseExitSimpleType\n  else\n          self.lastElement.simpleType\n  endif endif",
				"isComplexExpressionDerive", "false", "asBasicCodeDerive",
				"if (not self.subExpression->isEmpty()) \r\n     then let mirrorOrder:OrderedSet(mcore::expressions::MSubChain) = self.subExpression->iterate( sub:mcore::expressions::MSubChain; d: OrderedSet(mcore::expressions::MSubChain) = OrderedSet{} | d->prepend(sub)) in\r\n              let subString:String = self.subExpression\r\n                        ->iterate(x:mcore::expressions::MSubChain ; s: String = \'\' | \r\n                                  let t: String = typeAsOcl( x.previousExpression.selfObjectPackage, x.previousExpression.calculatedOwnType, \r\n                                                                         x.previousExpression.calculatedOwnSimpleType, x.previousExpression.calculatedOwnSingular ) in\r\n                                  \'let \'.concat(x.uniqueSubchainName()).concat(\' : \').concat(t).concat(\' = \').concat(s) )  in \r\n              let codeString:String =\r\n                      chainCodeforSubchains\r\n                      .concat(subExpression->iterate(x:mcore::expressions::MSubChain ; subStr: String =\'\' | subStr.concat(x.asCode))) in  \r\n              subString.concat(codeString)\r\n  else chainCodeforSubchains endif",
				"chainCalculatedTypeDerive",
				"if self.lastElement=null then self.baseExitType else\r\nself.lastElement.type endif",
				"chainCalculatedSimpleTypeDerive",
				"if self.lastElement=null then self.baseExitSimpleType\r\nelse self.lastElement.simpleType endif",
				"element1CorrectDerive",
				"if element1.oclIsUndefined() then true else\r\n--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else\r\nif chainEntryType.oclIsUndefined() then false \r\n  else chainEntryType.allProperties()->includes(element1)\r\n    and  \r\n   (if not (element1.kind=mcore::PropertyKind::Operation)\r\n      then  true\r\n      else if element2.oclIsUndefined()\r\n            then element1.allSignatures\r\n                        ->exists(s:mcore::MOperationSignature|\r\n                                s.parameter->size()=self.callArgument->size())\r\n             else element1.allSignatures\r\n                         ->exists(s:mcore::MOperationSignature|s.parameter->size()=0) endif endif)\r\nendif endif \r\n--endif",
				"element2CorrectDerive",
				"if element2.oclIsUndefined() then true else\r\n--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else\r\nif element2EntryType.oclIsUndefined() then false \r\n  else element2EntryType.allProperties()->includes(self.element2)\r\n      and  \r\n   (if not (element2.kind=mcore::PropertyKind::Operation)\r\n      then  true\r\n      else if element3.oclIsUndefined()\r\n            then element2.allSignatures\r\n                        ->exists(s:mcore::MOperationSignature|\r\n                                s.parameter->size()=self.callArgument->size())\r\n             else element2.allSignatures\r\n                         ->exists(s:mcore::MOperationSignature|s.parameter->size()=0) endif endif)\r\nendif endif \r\n--endif",
				"element3CorrectDerive",
				"if element3.oclIsUndefined() then true else\r\n--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else\r\nif element3EntryType.oclIsUndefined() then false\r\n  else element3EntryType.allProperties()->includes(self.element3)\r\n     and  \r\n   (if not (element3.kind=mcore::PropertyKind::Operation)\r\n      then  true\r\n      else element3.allSignatures\r\n                        ->exists(s:mcore::MOperationSignature|\r\n                                s.parameter->size()=self.callArgument->size()) endif)\r\nendif endif \r\n--endif",
				"kindLabelDerive", "let kind: String = \'CHAIN\' in\nkind\n",
				"collectorDerive", "containedCollector", "baseAsCodeDerive",
				"if (let e: Boolean = baseDefinition.oclIsUndefined() in \r\n    if e.oclIsInvalid() then null else e endif) \r\n  =true \r\nthen \'\'\r\n  else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)\r\n  then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)\r\n  else if baseDefinition.oclIsTypeOf(mcore::expressions::MContainerBaseDefinition)\r\n  then baseDefinition.calculatedAsCode.concat(if not self.baseExitType.oclIsUndefined() then \'.oclAsType(\'.concat(self.baseExitType.eName).concat(\')\')else \'\' endif)\r\n  else baseDefinition.calculatedAsCode\r\nendif\r\nendif\r\nendif\r\n",
				"chainCalculatedSingularDerive",
				"let s1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedSingular endif in\r\nlet s2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedSingular endif in\r\nlet s3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedSingular endif in\r\n\r\n(baseExitSingular and s1 and s2 and s3) ",
				"calculatedTypeDerive",
				"if collector.oclIsUndefined() and self.subExpression->isEmpty()\r\nthen calculatedOwnType\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().calculatedOwnType\r\n  else collector.calculatedType\r\nendif\r\nendif\r\n",
				"calculatedSingularDerive",
				"if self.subExpression->isEmpty() and collector.oclIsUndefined()\r\nthen calculatedOwnSingular\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().calculatedOwnSingular\r\n  else collector.calculatedSingular\r\nendif\r\nendif",
				"calculatedSimpleTypeDerive",
				"if self.collector.oclIsUndefined() and self.subExpression->isEmpty() \r\nthen calculatedOwnSimpleType\r\n  else if collector.oclIsUndefined()\r\n  then subExpression->last().calculatedOwnSimpleType\r\n  else collector.calculatedSimpleType\r\nendif\r\nendif\r\n",
				"processorInitValue", "mcore::expressions::MProcessor::None\n",
				"element1ChoiceConstruction",
				"\r\nlet annotatedProp: mcore::MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\nif self.chainEntryType.oclIsUndefined() then \r\nSequence{} else\r\nself.chainEntryType.allProperties()->excluding(if annotatedProp.oclIsUndefined() or (not self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.oclIsTypeOf(mcore::annotations::MResult)) then null else annotatedProp endif\r\n)->asSequence()endif\r\n\r\n--" });
		addAnnotation(mCallArgumentEClass, source,
				new String[] { "asCodeDerive",
						"let s:String = baseAsCode in\n\nif\n(self.call.lastElement.operationSignature.parameter->at(self.call.lastElement.operationSignature.parameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).parameter))\n.calculatedSingular).oclIsUndefined()\nthen s\nelse if\n (self.call.lastElement.operationSignature.parameter->at(self.call.lastElement.operationSignature.parameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).parameter))\n.calculatedSingular) \n then s\n else\ns.concat(\'->asOrderedSet()\')\nendif\nendif",
						"kindLabelDerive", "\'Argument\'\n" });
		addAnnotation(mSubChainEClass, source, new String[] { "kindLabelDerive",
				"\'   ...   \'.concat(\r\nlet c:String=self.chainAsCode in\r\nif c=\'\' then \'\' else \'.\'.concat(c) endif\r\n)",
				"chainEntryTypeDerive",
				"self.previousExpression.calculatedOwnType",
				"calculatedOwnSimpleTypeDerive",
				"let p:MProcessor = processor in\r\nif p <> MProcessor::None\r\n  then\r\n     if      p = MProcessor::ToInteger or\r\n             p = MProcessor::Size  or\r\n             p = MProcessor::Round or\r\n             p = MProcessor::Floor or\r\n             p = MProcessor::Year or\r\n             p = MProcessor::Minute or\r\n             p = MProcessor::Month or\r\n             p = MProcessor::Second or\r\n             p = MProcessor::Day or\r\n             p = MProcessor::Hour\r\n       then SimpleType::Integer\r\n     else if p = MProcessor::ToString or  \r\n             p = MProcessor::ToYyyyMmDd or\r\n             p = MProcessor::ToHhMm or\r\n             p = MProcessor::AllLowerCase or\r\n             p = MProcessor::AllUpperCase or\r\n             p = MProcessor::CamelCaseLower or\r\n             p = MProcessor::CamelCaseToBusiness or\r\n             p = MProcessor::CamelCaseUpper or\r\n             p = MProcessor::FirstUpperCase or\r\n             p = MProcessor::Trim\r\n       then SimpleType::String\r\n     else if p = MProcessor::ToReal or\r\n             p = MProcessor::OneDividedBy or\r\n             p = MProcessor::Sum\r\n       then SimpleType::Double\r\n     else if p = MProcessor::ToBoolean or\r\n             p = MProcessor::IsEmpty or\r\n             p = MProcessor::NotEmpty or\r\n             p = MProcessor::Not  or\r\n             p = MProcessor::IsFalse or\r\n             p = MProcessor::IsTrue or\r\n             p = MProcessor::IsZero or\r\n             p = MProcessor::IsOne or\r\n             p = MProcessor::NotNull or\r\n             p = MProcessor::IsNull or\r\n             p = MProcessor::IsInvalid or\r\n             p = MProcessor::And or\r\n             p= MProcessor::Or\r\n       then SimpleType::Boolean\r\n      else if p = MProcessor::ToDate \r\n       then SimpleType::Date\r\n      else if  self.lastElement.oclIsUndefined() \r\n       then  self.previousExpression.calculatedOwnSimpleType\r\n     else self.lastElement.simpleType\r\n      endif endif endif endif endif endif\r\n    else\r\n if\r\n       self.lastElement.oclIsUndefined() then self.previousExpression.calculatedOwnSimpleType\r\n       else self.lastElement.simpleType\r\n       endif endif\r\n\r\n\r\n",
				"calculatedOwnMandatoryDerive",
				"if self.lastElement=null then self.previousExpression.calculatedMandatory else\r\nself.lastElement.mandatory endif",
				"calculatedOwnTypeDerive",
				"if self.calculatedOwnSimpleType = SimpleType::None then\r\n if self.castType.oclIsUndefined() then\r\n  if self.lastElement=null then self.previousExpression.calculatedOwnType else\r\nself.lastElement.type  endif\r\nelse self.castType\r\nendif\r\nelse null endif",
				"calculatedOwnSingularDerive",
				"if self.processorIsSet() \r\n     then self.processorReturnsSingular()\r\n     else self.chainCalculatedSingular endif",
				"asBasicCodeDerive",
				"-- chainType: the type coming out of the chain\r\nlet chainTypeString:String =\r\n     if self.lastElement.oclIsUndefined()  then \r\n       typeAsOcl( self.selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  \r\n     else\r\n       typeAsOcl( self.selfObjectPackage, self.chainCalculatedType, self.chainCalculatedSimpleType, self.chainCalculatedSingular )  endif  in\r\n -- singularVersion of chainType\r\nlet chainTypeStringSingular: String =\r\n\t  if self.lastElement.oclIsUndefined() then \r\n\t\t  typeAsOcl(selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, true)\r\n\t  else\r\n\t\t   typeAsOcl(selfObjectPackage, self.lastElement.calculatedType, self.lastElement.calculatedSimpleType, true) endif in\r\n-- castType: the type to which we cast, if we cast or chainType otherwise\r\nlet castTypeString: String = \r\n      if castType.oclIsUndefined()\r\n         then chainTypeString\r\n         else typeAsOcl(selfObjectPackage, castType, SimpleType::None, self.chainCalculatedSingular) endif in\r\n-- singular version of castType\r\nlet castTypeStringSingular: String = \r\n     if castType.oclIsUndefined()\r\n       then chainTypeStringSingular\r\n       else typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) endif in\r\n--let name for the input to cast\r\nlet castName: String = \r\n     self.uniqueSubchainName().concat(\'cast\') in\t \r\n--let name for the input to proc\r\nlet procName: String = \r\n     self.uniqueSubchainName().concat(\'proc\') in\t \r\n--code for chained properties/operation:\r\nlet code: String = \r\n     self.asCodeForOthers() in\r\n \r\n\' in \\n\'\r\n.concat(if self.processor = MProcessor::None  \r\n     then \'\' \r\n     else \'let \'.concat(procName).concat(\':\').concat(castTypeString).concat(\' = \') endif)\r\n.concat(if self.castType.oclIsUndefined()\r\n     then  \' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \').concat(code).concat( \' endif \\n\')\r\n     else  \'let \'.concat(castName).concat(\':\').concat(chainTypeString).concat(\' =\').concat(\' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \')).concat(code).concat( \' endif \\n\')\r\n              .concat(\' in \\n\')\r\n              .concat(if self.chainCalculatedSingular \r\n                   then \'if \'.concat(castName).concat(\'.oclIsUndefined() then null else \') \r\n                   else \'\' endif)\r\n              .concat(if self.chainCalculatedSingular \r\n                   then \'if \'.concat(castName).concat(\'.oclIsKindOf(\').concat(castTypeStringSingular).concat(\')\\n then \'.concat(castName).concat(\'.oclAsType(\').concat(castTypeStringSingular)\r\n                            .concat(\') else null endif endif \'))\r\n                   else  castName.concat(\'->iterate(i:\').concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular)\r\n                           .concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\')\r\n                           .concat(\')->asOrderedSet() \\n else r endif)\') endif) endif)\r\n .concat(if self.processor = MProcessor::None \r\n      then \'\' \r\n      else \' in \\n if \'.concat(procName)\r\n              .concat(if self.isProcessorSetOperator() \r\n                      then \'->\' \r\n                      else \'.\' endif)\r\n              .concat(if self.isCustomCodeProcessor() \r\n                       then \'isEmpty()\' \r\n                       else procAsCode().concat(\'.oclIsUndefined()\') endif)\r\n               .concat(\' then null \\n else \').concat(procName)\r\n               .concat( if self.isProcessorSetOperator() \r\n                       then \'->\' \r\n                       else \'.\' endif)\r\n               .concat(if self.isCustomCodeProcessor() \r\n                      then   let iterateBase: String = \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n                                if self.processor = MProcessor::And or processor = MProcessor::Or \r\n                                       then let checkPart : String = if  processor= MProcessor::And  then \'false\' else \'true\' endif in \r\n                                               let elsePart : String = if processor= MProcessor::And then \'true\' else \'false\' endif in\r\n                                               iterateBase.concat(chainTypeStringSingular).concat(\'= \').concat(elsePart).concat(\'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\')\r\n                                               .concat(\'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\')\r\n                                               .concat(\' else if x =null then null \\n\'\r\n                                               .concat(\'else if s =null then null\'))\r\n                                               .concat(\' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n                                       else  let elementToExclude: String = \r\n                                                   --QUESTION why code, and not procName?? ProcName seems much better... I do the change, change back if problems appear...\r\n                                                   --code\r\n                                                   procName\r\n                                                   .concat(if processor=MProcessor::Head \r\n                                                   -- CHANGE turned around as head and tail delivered the wrong code.\r\n                                                        then \'->last() \' \r\n                                                        else \'->first() \' endif) in\r\n                                                 iterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\'\r\n                                                .concat(elementToExclude)\r\n                                                .concat(\'then s else s->including(x)->asOrderedSet() endif endif)\')) endif\r\n                       else self.procAsCode() endif)\r\n               .concat( \'\\n  endif\' ) endif  ) \r\n\t\t      \r\n\r\n/*\r\n-- chainType: the type coming out of the chain\r\nlet chainTypeString:String =\r\n     if self.lastElement.oclIsUndefined()  then \r\n       typeAsOcl( self.selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  \r\n     else\r\n       typeAsOcl( self.selfObjectPackage, self.chainCalculatedType, self.chainCalculatedSimpleType, self.chainCalculatedSingular )  \r\n     endif  in\r\nlet chainTypeStringSingular: String =\r\n\t  if self.lastElement.oclIsUndefined() then \r\n\t\t  typeAsOcl(selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, true)\r\n\t  else\r\n\t\t   typeAsOcl(selfObjectPackage, self.lastElement.calculatedType, self.lastElement.calculatedSimpleType, true) endif in\r\n-- castType: the type to which we cast.\r\nlet castTypeString: String = \r\n      typeAsOcl(selfObjectPackage, castType, SimpleType::None, self.chainCalculatedSingular) in\r\nlet castTypeStringSingular: String = \r\n     typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in\r\n--let name for the input to cast\r\nlet castName: String = \r\n     self.uniqueSubchainName().concat(\'cast\') in\t \r\n--let name for the input to proc\r\nlet procName: String = \r\n     self.uniqueSubchainName().concat(\'proc\') in\t \r\n--code for chained properties/operation:\r\nlet code: String = \r\n     self.asCodeForOthers() in\r\n \r\n\' in \\n\'\r\n.concat(if self.processor = MProcessor::None  \r\n  then \'\' \r\n  else \'let \'.concat(procName.concat(\':\').concat(if self.castType.oclIsUndefined() then chainTypeString else castTypeString endif).concat(\' = \')) endif)\r\n.concat(if self.castType.oclIsUndefined()then  \' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \')\r\n.concat(code).concat( \' endif \\n\')\r\n else  \'let \'.concat(castName).concat(\':\').concat(chainTypeString).concat(\' =\')\r\n.concat(\' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \'))\r\n.concat(code).concat( \' endif \\n\')\r\n.concat(\' in \\n\').concat(if self.chainCalculatedSingular then \'if \'.concat(castName).concat(\'.oclIsUndefined() then null else \') else \'\' endif)\r\n.concat(if self.chainCalculatedSingular then \'if \'.concat(castName).concat(\'.oclIsKindOf(\').concat(castTypeStringSingular).concat(\')\\n then \'.concat(castName).concat(\'.oclAsType(\').concat(castTypeStringSingular).concat(\') else null endif endif \'))\r\nelse  \r\n\r\ncastName.concat(\'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular).concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\').concat(\')->asOrderedSet() \\n else r endif)\'))\r\n endif) endif)\r\n \r\n .concat(if self.processor = MProcessor::None then \'\' else \' in \\n if \'.concat(procName.concat(if self.isProcessorSetOperator() then \'->\' else \'.\' endif.concat(if self.isCustomCodeProcessor() then \'isEmpty()\' else self.procAsCode().concat(\'.oclIsUndefined()\')endif))).concat(\' then null \\n else \').concat(procName).concat( if self.isProcessorSetOperator() then \'->\' else \'.\' endif\r\n \r\n .concat(if self.isCustomCodeProcessor() then\r\n\t\t   let checkPart : String = if  processor= MProcessor::And then \'false\' else \'true\' endif\r\nin\r\nlet elsePart : String = if processor= MProcessor::And then \'true\' else \'false\' endif in\r\n\r\nlet iterateBase: String = \r\n \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n \r\n if self.processor = MProcessor::And or processor = MProcessor::Or then\r\niterateBase.concat(chainTypeStringSingular).concat(\'= \').concat(elsePart)\r\n.concat(\'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\').concat(\'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\').concat(\' else if x =null then null \\n\'.concat(\'else if s =null then null\')\r\n).concat(\' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n\r\nelse\r\niterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\'.concat(code).concat(if processor=MProcessor::Head then \'->first() \' else \'->last() \' endif).concat(\'then s else s->including(x)->asOrderedSet() endif endif)\'))\r\nendif\r\n else self.procAsCode() endif).concat( \r\n\t\t\t \'\\n  endif\' )) endif  ) \r\n\t\t\t */\r\n\r\n\r\n\r\n\r\n",
				"chainCalculatedSingularDerive",
				"let s1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedSingular endif in\r\nlet s2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedSingular endif in\r\nlet s3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedSingular endif in\r\n\r\n(self.previousExpression.calculatedOwnSingular and s1 and s2 and s3) ",
				"chainCalculatedTypeDerive",
				"if self.lastElement=null then self.previousExpression.calculatedOwnType else\r\nself.lastElement.type endif",
				"chainCalculatedSimpleTypeDerive",
				"if self.lastElement.oclIsUndefined() \r\n      then self.previousExpression.calculatedOwnSimpleType\r\n      else self.lastElement.simpleType endif" });
		addAnnotation(mDataValueExprEClass, source, new String[] {
				"kindLabelDerive", "\'Value\'\n", "isComplexExpressionDerive",
				"false", "calculatedOwnMandatoryDerive", "true",
				"calculatedOwnSingularDerive", "true",
				"calculatedOwnTypeDerive", "null",
				"calculatedOwnSimpleTypeDerive", "simpleType",
				"asBasicCodeDerive",
				"let v:String =if value=null then \'Data Value MISSING\' else value endif in\r\nif simpleType=SimpleType::String\r\nthen \'\\\'\'.concat(v).concat(\'\\\'\') else v endif " });
		addAnnotation(mLiteralValueExprEClass, source, new String[] {
				"kindLabelDerive", "\'Literal\'\n", "isComplexExpressionDerive",
				"false", "calculatedOwnMandatoryDerive", "true",
				"calculatedOwnSingularDerive", "true",
				"calculatedOwnTypeDerive", "enumerationType",
				"calculatedOwnSimpleTypeDerive", "SimpleType::None",
				"asBasicCodeDerive",
				"if value.oclIsUndefined() then \'null\'\r\nelse value.qualifiedName endif" });
		addAnnotation(mIfEClass, source,
				new String[] { "kindLabelDerive",
						"let kind: String = \'IF\' in\nkind\n",
						"calculatedOwnMandatoryDerive",
						"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedMandatory endif",
						"calculatedOwnSingularDerive",
						"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedSingular endif",
						"calculatedOwnTypeDerive",
						"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then null\r\nelse thenPart.calculatedType endif",
						"calculatedOwnSimpleTypeDerive",
						"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then SimpleType::None\r\nelse thenPart.calculatedSimpleType endif",
						"asBasicCodeDerive",
						"let c: String = if condition.oclIsUndefined() then \'MISSING CONDITION\' \r\n  else condition.asCode endif in\r\nlet t: String = if thenPart.oclIsUndefined() then \'MISSING THEN\' \r\n  else thenPart.asCode endif in\r\nlet e: String = if elsePart.oclIsUndefined() then \'MISSING ELSE\' \r\n  else elsePart.asCode endif in\r\n\'if (\'.concat(c).concat(\') \\n  =true \\n\').concat(\r\nt).concat(\r\n\telseifPart->iterate(it: MElseIf; s: String=\'\' | s.concat(\' \').concat(it.asCode))\r\n).concat(\'\\n  \').concat(e).concat(\'\\nendif\').concat(\r\n\telseifPart->iterate(it: MElseIf; s: String=\'\' | s.concat(\' endif\'))\r\n)",
						"collectorDerive", "containedCollector",
						"elseifPartInitValue", "OrderedSet{}" });
		addAnnotation(mThenEClass, source, new String[] { "kindLabelDerive",
				"let kind: String = \'THEN\' in\nkind\n",
				"calculatedOwnMandatoryDerive",
				"if expression.oclIsUndefined() then false\r\nelse expression.calculatedMandatory endif",
				"calculatedOwnTypeDerive",
				"if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif",
				"calculatedOwnSingularDerive",
				"if expression.oclIsUndefined() then true\r\nelse expression.calculatedSingular endif",
				"calculatedOwnSimpleTypeDerive",
				"if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif",
				"asBasicCodeDerive",
				"\'then \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse \r\n\t  if expression.isComplexExpression.oclIsUndefined() then \'ERRORisComplexExpressionUndefined\' \r\n\t  else if expression.asCode.oclIsUndefined() then \r\n\t  \'ERRORasCodeUndefined\' else\r\n\t\r\n\tif expression.isComplexExpression\r\n\t  then \'(\'.concat(expression.asCode).concat(\')\')\r\n\t  else expression.asCode endif endif\r\n\t  \r\n\t  endif endif\r\n)" });
		addAnnotation(mElseIfEClass, source, new String[] { "kindLabelDerive",
				"let kind: String = \'ELSE IF\' in\nkind\n",
				"calculatedOwnMandatoryDerive",
				"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedMandatory endif",
				"calculatedOwnTypeDerive",
				"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then null\r\nelse thenPart.calculatedType endif",
				"calculatedOwnSingularDerive",
				"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedSingular endif",
				"calculatedOwnSimpleTypeDerive",
				"/* TODO: better typing and verification */\r\nif thenPart.oclIsUndefined() then SimpleType::None\r\nelse thenPart.calculatedSimpleType endif",
				"asBasicCodeDerive",
				"\'else if (\'.concat(\r\n\tif condition.oclIsUndefined() then \'MISSING IF CONDITION\'\r\n\telse condition.asCode endif\r\n).concat(\')=true \').concat(\r\n\tif thenPart.oclIsUndefined() then \'MISSING THEN\'\r\n\telse thenPart.asCode endif\r\n)" });
		addAnnotation(mElseEClass, source, new String[] { "kindLabelDerive",
				"let kind: String = \'ELSE\' in\nkind\n",
				"calculatedOwnMandatoryDerive",
				"if expression.oclIsUndefined() then true\r\nelse expression.calculatedMandatory endif",
				"calculatedOwnTypeDerive",
				"if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif",
				"calculatedOwnSingularDerive",
				"if expression.oclIsUndefined() then true\r\nelse expression.calculatedSingular endif",
				"calculatedOwnSimpleTypeDerive",
				"if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif",
				"asBasicCodeDerive",
				"\'else \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse if expression.isComplexExpression\r\n\t  then \'(\'.concat(expression.asCode).concat(\')\')\r\n\t  else expression.asCode endif endif\r\n)" });
		addAnnotation(mCollectionExpressionEClass, source, new String[] {
				"kindLabelDerive",
				"let prefix: String = if collection.oclIsUndefined() then \'\'\r\nelse if collection.oclIsKindOf(MBaseChain) then \'\'\r\nelse if collection.oclIsKindOf(MApplication) then \')\'\r\nelse if collection.oclIsKindOf(MIf) then \'ENDIF\' else \'\'\r\nendif endif endif endif in \r\nprefix.concat(\'->\').concat(collectionOperatorAsCode()).concat(\'(\')",
				"isComplexExpressionDerive",
				"/* if it returns a real collection, then it\'s a single OCL item, otherwise it\'s a let with if */\r\ncalculatedSingular",
				"asBasicCodeDerive",
				"if collection.oclIsUndefined() then \'MISSING COLLECTION\'\r\nelse\r\n\r\nlet hasIterator: Boolean = not iteratorVar.oclIsUndefined() in\r\nlet hasAccumulator: Boolean=not accumulatorVar.oclIsUndefined() in\r\nlet  ct: String = typeAsOcl(selfObjectPackage, collection.calculatedOwnType, collection.calculatedOwnSimpleType , true) in\r\n\t\r\nlet exp: String = (if collection.isComplexExpression then \'(\' else \'\' endif).concat(collection.asBasicCode).concat(if collection.isComplexExpression then \')\' else \'\' endif).concat(\r\n\'->\').concat(collectionOperatorAsCode()).concat(\'(\').concat(\r\n\tif hasIterator then iteratorVar.eName.concat(\': \').concat(ct)\r\n\t\telse \'\' endif\r\n).concat(\t\t\r\n\tif hasIterator and hasAccumulator then \'; \' else \'\' endif\r\n).concat(\t\t\r\n\tif hasAccumulator then \r\n\t  accumulatorVar.eName.concat(\': \').concat(typeAsOcl(selfObjectPackage, accumulatorVar.calculatedOwnType, accumulatorVar.calculatedOwnSimpleType , accumulatorVar.calculatedOwnSingular))\r\n\t  .concat(\' = \').concat(if self.accumulatorVar.accDefinition.oclIsUndefined() then if self.calculatedOwnSimpleType= SimpleType::String then \'  \\\' \' \'\\\' \' else if self.calculatedOwnSimpleType = SimpleType::Integer or self.calculatedOwnSimpleType = SimpleType::Double then \'0\' else \'null \' endif endif\r\n\t  else\r\n\t  self.accumulatorVar.accDefinition.asBasicCode\r\n\t  endif\r\n\t  ) \r\n\t\r\n\telse \'\' endif\r\n).concat(\t\t\r\n\tif hasIterator or hasAccumulator then \' | \' else \'\' endif\r\n). concat (\r\n\tif expression.oclIsUndefined() then \'true\'\r\n\telse expression.asCode endif\r\n).concat(\')\') in\r\n\r\nif calculatedOwnSingular then\r\n  \'let c: \'.concat(typeAsOcl(selfObjectPackage,  calculatedType,  calculatedOwnSimpleType , true)).concat(\' = \').concat(exp).concat(\' in if c.oclIsUndefined() then null else c endif\')\r\nelse \r\n  exp.concat(\'->asOrderedSet()->excluding(null)->asOrderedSet() \')\t\r\nendif\r\n\r\nendif\r\n",
				"calculatedOwnMandatoryDerive",
				"if collectionOperator = CollectionOperator::Iterate then\r\n\tif iteratorVar.oclIsUndefined() then false\r\n\telse iteratorVar.calculatedMandatory endif\r\nelse false endif",
				"calculatedOwnSingularDerive",
				"if collectionOperator = CollectionOperator::Iterate then\r\n\tif iteratorVar.oclIsUndefined() then false\r\n\telse iteratorVar.calculatedSingular endif\r\nelse false endif",
				"calculatedOwnSimpleTypeDerive",
				"let ct: SimpleType = if collection.oclIsUndefined()\r\n\tthen SimpleType::None\r\n\telse collection.calculatedOwnSimpleType endif in\r\nlet et: SimpleType = if expression.oclIsUndefined()\r\n\tthen SimpleType::None\r\n\telse expression.calculatedSimpleType endif in\r\nlet at: SimpleType = if accumulatorVar.oclIsUndefined()\r\n\tthen SimpleType::None\r\n\telse accumulatorVar.calculatedSimpleType endif in\r\n\t\t\r\nif collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure\r\n  then ct \r\n  else if collectionOperator = CollectionOperator::Collect \r\n    then  et \r\n      else if collectionOperator = CollectionOperator::Iterate\r\n    \tthen  at\r\n    \t  else SimpleType::None endif\r\n    endif \r\n  endif \r\n",
				"calculatedTypeDerive",
				"let ct: MClassifier = if collection.oclIsUndefined()\r\n\tthen null\r\n\telse collection.calculatedOwnType endif in\r\nlet et: MClassifier = if expression.oclIsUndefined()\r\n\tthen null\r\n\telse expression.calculatedType endif in\r\nlet at: MClassifier = if accumulatorVar.oclIsUndefined()\r\n\tthen null\r\n\telse accumulatorVar.calculatedType endif in\r\n\t\t\r\nif collectionOperator = CollectionOperator::Select or collectionOperator = CollectionOperator::Closure\r\n  then ct \r\n  else if collectionOperator = CollectionOperator::Collect \r\n    then  et \r\n      else if collectionOperator = CollectionOperator::Iterate\r\n    \tthen  at\r\n    \t  else null endif\r\n    endif \r\n  endif " });
		addAnnotation(mIteratorEClass, source, new String[] { "kindLabelDerive",
				"\'Iterator\'\n", "calculatedOwnMandatoryDerive", "true",
				"calculatedOwnSingularDerive", "true",
				"calculatedOwnSimpleTypeDerive",
				"if containingCollection.collection.calculatedOwnSimpleType.oclIsInvalid()\r\nthen SimpleType::None\r\nelse containingCollection.collection.calculatedOwnSimpleType endif",
				"calculatedOwnTypeDerive",
				"if containingCollection.collection.calculatedOwnType.oclIsUndefined() then null\r\nelse containingCollection.collection.calculatedOwnType endif" });
		addAnnotation(mAccumulatorEClass, source,
				new String[] { "kindLabelDerive", "\'Accumulator\'\n",
						"calculatedOwnMandatoryDerive",
						"if mandatory.oclIsUndefined() then false\r\nelse mandatory endif",
						"calculatedOwnSingularDerive",
						"if singular.oclIsUndefined() then true\r\nelse singular endif",
						"calculatedOwnSimpleTypeDerive", "simpleType",
						"calculatedOwnTypeDerive", "type" });
		addAnnotation(mNamedConstantEClass, source, new String[] {
				"kindLabelDerive",
				"\'Where\'\r\n/*\r\nlet c: annotations::MExprAnnotation = self.eContainer().oclAsType(annotations::MExprAnnotation) in\r\nif c.oclIsUndefined() then \'Let\'\r\n\telse if c.namedExpression->notEmpty()\r\n\t\tthen \'Let\'\r\n\t\telse if c.namedConstant->last()=self\r\n\t\t    then \'Definition\'\r\n\t\t    else \'Let\' endif\r\n\t\tendif\r\nendif\r\n*/\r\n",
				"asBasicCodeDerive",
				"if expression.oclIsUndefined() then \'MISSING EXPRESSION\' else\r\n\r\nlet c: String = expression.asCode in\r\nlet t: String = if  expression.calculatedType.oclIsUndefined()\r\n\tthen expression.calculatedSimpleType.toString()\r\n\telse expression.calculatedType.eName endif in\r\n\t\r\n if name.oclIsUndefined() or name=\'\' \r\n then c\r\n else \'let \'.concat(eName).concat(\': \').concat( typeAsOcl(selfObjectPackage, expression.calculatedType, expression.calculatedSimpleType, expression.calculatedSingular) ).concat(\' = \').concat(c).concat(\' in\')\r\nendif\r\n\r\nendif",
				"calculatedOwnMandatoryDerive",
				"if expression.oclIsUndefined() then false\r\nelse expression.calculatedMandatory endif",
				"calculatedOwnSingularDerive",
				"if expression.oclIsUndefined() then true\r\nelse expression.calculatedSingular endif",
				"calculatedOwnSimpleTypeDerive",
				"if expression.oclIsUndefined() then SimpleType::None\r\nelse expression.calculatedSimpleType endif",
				"calculatedOwnTypeDerive",
				"if expression.oclIsUndefined() then null\r\nelse expression.calculatedType endif" });
		addAnnotation(mSimpleTypeConstantLetEClass, source,
				new String[] { "kindLabelDerive", "\'Value Constant\'\n",
						"asBasicCodeDerive",
						"let a: String = if constant1.oclIsUndefined() then \'\' else if constant1=\'\' then \'\' else constant1 endif endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else if constant2=\'\' then \'\' else constant2 endif endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else if constant3=\'\' then \'\' else constant3 endif endif in\r\n\r\nif calculatedSingular\r\n    then if calculatedSimpleType = SimpleType::String\r\n    \tthen inQuotes(a.concat(b).concat(c))\r\n    \telse a.concat(b).concat(c) endif\r\n    else if calculatedSimpleType = SimpleType::String\r\n    \tthen asSetString(inQuotes(constant1), inQuotes(constant2), inQuotes(constant3))\r\n    \telse asSetString(constant1, constant2, constant3)\r\n    \tendif\r\n    endif",
						"calculatedOwnMandatoryDerive", "true",
						"calculatedOwnSingularDerive",
						"let a: Integer = if constant1.oclIsUndefined() then 0\r\n\telse if constant1=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\nlet b: Integer = if constant2.oclIsUndefined() then 0\r\n\telse if constant2=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\nlet c: Integer = if constant3.oclIsUndefined() then 0\r\n\telse if constant3=\'\' then 0 else 1 endif\r\n\tendif in\r\n\t\r\n(a+b+c) <= 1",
						"calculatedOwnSimpleTypeDerive", "simpleType" });
		addAnnotation(mLiteralLetEClass, source,
				new String[] { "kindLabelDerive", "\'Literal Constant\'\n",
						"asBasicCodeDerive",
						"let a: String = if constant1.oclIsUndefined() then \'\' else constant1.qualifiedName endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else constant2.qualifiedName endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else constant3.qualifiedName endif in\r\n\r\n if calculatedSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(a, b, c)\r\nendif\r\n",
						"calculatedOwnSingularDerive",
						"let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1",
						"calculatedOwnMandatoryDerive", "true",
						"calculatedOwnTypeDerive", "enumerationType" });
		addAnnotation(mObjectReferenceLetEClass, source,
				new String[] { "kindLabelDerive", "\'Object Reference\'\n",
						"asBasicCodeDerive",
						"let t: String =  if objectType.oclIsUndefined() then \'MISSING TYPE\' else\r\n\tobjectType.calculatedShortName endif in\r\nlet a: String = if constant1.oclIsUndefined() then \'\' else \'<obj \'.concat(constant1.id).concat(\'>\') endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else \'<obj \'.concat(constant2.id).concat(\'>\') endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else \'<obj \'.concat(constant3.id).concat(\'>\')  endif in\r\n\r\n if calculatedSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(\r\n  \t\'<obj \'.concat(constant1.id).concat(\'>\'),\r\n  \t\'<obj \'.concat(constant2.id).concat(\'>\'),\r\n  \t\'<obj \'.concat(constant3.id).concat(\'>\')\r\n  )\r\n  endif\r\n",
						"calculatedOwnMandatoryDerive",
						"let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) > 0",
						"calculatedOwnSingularDerive",
						"let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1",
						"calculatedOwnTypeDerive", "objectType" });
		addAnnotation(mApplicationEClass, source,
				new String[] { "kindLabelDerive",
						"let kind: String = \'APPLY\' in\nkind\n",
						"calculatedOwnTypeDerive",
						"if\r\n\toperator=MOperator::SetFirst  or operator=MOperator::SetLast\r\n\tor operator=MOperator::SetIncluding or operator=MOperator::SetExcluding\r\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet\t\r\n    or operator=MOperator::At  or operator=MOperator::Intersection then \r\n\tlet x: MClassifier = self.operands->first().calculatedType in \r\n\tif x.oclIsUndefined() then null else x endif\r\nelse null\r\nendif",
						"calculatedOwnSimpleTypeDerive",
						"if\r\n   operator=MOperator::Concat or operator=MOperator::ToString or operator=MOperator::Trim or operator=MOperator::SubString\r\nthen SimpleType::String\r\nelse if \r\n\toperator=MOperator::Equal  or operator=MOperator::NotEqual  or operator=MOperator::Greater\r\n\tor operator=MOperator::Smaller  or operator=MOperator::GreaterEqual or operator=MOperator::SmallerEqual\r\n\tor operator=MOperator::NotOp  or operator=MOperator::OclAnd  or operator=MOperator::OclOr\r\n\tor operator=MOperator::XorOp or operator=MOperator::ImpliesOp or operator=MOperator::SetIsEmpty \r\n\tor operator=MOperator::SetNotEmpty  or operator=MOperator::SetIncludes or operator=MOperator::SetExcludes\r\n\tor operator=MOperator::ImpliesOp or self.definesOperatorBusinessLogic() or operator=MOperator::SetIncludesAll or operator=MOperator::SetExcludesAll\r\n\t\r\nthen SimpleType::Boolean\r\nelse if \r\n\toperator=MOperator::SetSize or operator=MOperator::IndexOf or \r\n\t(operator = MOperator::DiffInDays) or\r\n (operator = MOperator::DiffInHrs) or\r\n  (operator = MOperator::DiffInMinutes) or\r\n   (operator = MOperator::DiffInMonth) or\r\n    (operator = MOperator::DiffInSeconds) or\r\n     (operator = MOperator::DiffInYears)\r\nthen SimpleType::Integer\r\nelse if \r\n\tself.operator=MOperator::Plus  or operator=MOperator::Minus or operator= MOperator::Times \r\n\tor operator= MOperator::Divide  or operator= MOperator::Div or operator= MOperator::Mod\r\n\tor operator=MOperator::SetFirst  or operator=MOperator::SetLast or operator=MOperator::SetSum\r\n\tor operator=MOperator::SetIncluding or operator=MOperator::SetExcluding\r\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet or operator=MOperator::At  or operator=MOperator::Intersection\r\nthen \r\n   self.getCalculatedSimpleDifferentTypes()\r\n--\tlet x: SimpleType = self.operands->first().calculatedSimpleType in \r\n--\tif x.oclIsUndefined() then SimpleType::None else x endif\r\nelse if \r\n\t(operator= MOperator::OclIsInvalid) or (operator= MOperator::OclIsUndefined) then SimpleType::Boolean\r\nelse SimpleType::None endif endif endif endif endif",
						"calculatedOwnSingularDerive",
						"if\n\toperator=MOperator::SetIncluding or operator=MOperator::SetExcluding\n\tor operator=MOperator::SetUnion or operator=MOperator::SetAsOrderedSet\t\n\tor operator=MOperator::Intersection\nthen false\nelse true\nendif",
						"isComplexExpressionDerive", "isOperatorInfix()",
						"asBasicCodeDerive",
						"let varName : String = \'e\'.concat(self.uniqueApplyNumber().toString()) in\r\n\r\n\'let \'.concat(varName).concat(\': \').concat( typeAsOcl(selfObjectPackage, calculatedType, calculatedSimpleType, calculatedSingular) )\r\n.concat(\' = \').concat(\r\nif operands->size() = 0 then \'\' else\r\nlet frst: MChainOrApplication = operands->first() in\r\nlet frstCode: String = (if frst.isComplexExpression then \'(\' else \'\' endif).concat(frst.asCode).concat(if frst.isComplexExpression then \')\' else \'\' endif) in\r\n\r\n\r\nif self.definesOperatorBusinessLogic() then self.codeForLogicOperator()\r\nelse\r\nif isOperatorUnaryFunction()\r\nthen\r\n\tfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\'.\').concat(operatorAsCode()).concat(\'(\').concat(x.asCode).concat(\')\')\r\n\t\t ) )\r\n\t\r\nelse if isOperatorSetOperator()\r\nthen\r\nlet appCode : String = if self.operands->size() = 1 then frstCode.concat(\'->\').concat(operatorAsCode()).concat(\'()\') else \r\nfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\nx: MChainOrApplication; s: String = \'\' | \r\n\t s.concat(\'->\').concat(operatorAsCode())\r\n\t.concat(\'(\').concat(x.asCode).concat(\')\')\r\n\t) ).concat(if self.calculatedSingular then \'  \' else \' ->asOrderedSet()  \'  endif)\r\n\tendif in appCode\r\n\t\r\nelse if self.isOperatorUnaryFunctionTwoParam()\r\nthen\r\n\tfrstCode.concat(\'.\').concat(operatorAsCode()).concat(\'(\').concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(x.asCode).concat( if (operands->indexOf(x) = operands->size()) then \'\' else \', \' endif)\r\n\t\t--  s.concat(x.asCode).concat(\',\').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined() then \'1\' else operands->at(operands->indexOf(x)+1).asCode endif)\r\n\t\t ) ).concat(\')\')\r\nelse if isOperatorInfix() \r\nthen\r\n\tfrstCode.concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\' \').concat(operatorAsCode()).concat(\' \').concat(\r\n\t\t    (if x.isComplexExpression then \'(\' else \'\' endif).concat(x.asCode).concat(if x.isComplexExpression then \')\' else \'\' endif)\r\n\t\t  )\r\n\t\t) \r\n\t)\r\nelse if isOperatorPrefix() \r\nthen\r\n\toperatorAsCode().concat(\'(\').concat(frstCode).concat(\r\n\t\toperands->excluding(frst)->iterate(\r\n\t\t  x: MChainOrApplication; s: String = \'\' | \r\n\t\t  s.concat(\', \').concat(\r\n\t\t    (if x.isComplexExpression then \'(\' else \'\' endif).concat(x.asCode).concat(if x.isComplexExpression then \')\' else \'\' endif)\r\n\t\t  )\r\n\t\t) \r\n\t).concat(\')\')\r\nelse\r\n\tfrstCode.concat(\'.\').concat(operatorAsCode()).concat(\'()\')\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\nendif\r\n).concat(if self.calculatedSingular then \' in \\n if \'.concat(varName).concat(\'.oclIsInvalid() \').concat(\'then \') else \' in \\n    if \'.concat(varName).concat(\'->oclIsInvalid() then \') endif)\r\n.concat(if self.calculatedSingular then \'null\' else \'OrderedSet{}\' endif)\r\n.concat(\' else \').concat(varName).concat(\' endif\')",
						"collectorDerive", "containedCollector" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getMAbstractExpression__GetShortCode(), source,
				new String[] { "body",
						"if asCode.oclIsUndefined() then \'\'\r\nelse let s: String = asCode in\r\n\tif s.size() > 30 then s.substring(1, 27).concat(\'...\')\r\n\telse s endif\r\nendif" });
		addAnnotation(getMAbstractExpression__GetScope(), source,
				new String[] { "body", "entireScope" });
		addAnnotation(getMAbstractExpression_AsCode(), source, new String[] {
				"derive",
				"if (let e: Boolean = collector.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen asBasicCode\n  else if collector.oclIsUndefined()\n  then null\n  else collector.asBasicCode\nendif\nendif\n" });
		addAnnotation(getMAbstractExpression_AsBasicCode(), source,
				new String[] { "derive", "eClass().name.concat(\' : <?>\')" });
		addAnnotation(getMAbstractExpression_Collector(), source, new String[] {
				"derive",
				"let nl: mcore::expressions::MCollectionExpression = null in nl\n" });
		addAnnotation(getMAbstractExpression_EntireScope(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localEntireScope \r\nelse /* Note this is necessary for some cases like scopeIterator that otherwise won\'t work */ \r\nscopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(\r\nscopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeNumberBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeContainerBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nscopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeBase(), source, new String[] {
				"derive",
				"if containingExpression.oclIsUndefined()\r\nthen localScopeBase \r\nelse containingExpression.scopeBase\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeSelf(), source, new String[] {
				"derive",
				"if containingExpression.oclIsUndefined()\r\nthen localScopeSelf\r\nelse containingExpression.scopeSelf\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeTrg(), source, new String[] {
				"derive",
				"if containingExpression.oclIsUndefined()\r\nthen localScopeTrg\r\nelse containingExpression.scopeTrg\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeObj(), source, new String[] {
				"derive",
				"--todo: only for updates...\r\nif containingExpression.oclIsUndefined() then localScopeObj else containingExpression.scopeObj endif" });
		addAnnotation(getMAbstractExpression_ScopeSimpleTypeConstants(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localScopeSimpleTypeConstants\r\nelse containingExpression.scopeSimpleTypeConstants\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeLiteralConstants(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localScopeLiteralConstants\r\nelse containingExpression.scopeLiteralConstants\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeObjectReferenceConstants(),
				source, new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localScopeObjectReferenceConstants\r\nelse containingExpression.scopeObjectReferenceConstants\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeVariables(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localScopeVariables\r\nelse containingExpression.scopeVariables\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeIterator(), source,
				new String[] { "derive",
						"/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr */\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeIterator\r\nelse containingExpression.scopeIterator->union(localScopeIterator)\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeAccumulator(), source,
				new String[] { "derive",
						"/* NOTE this is a bit of a special case, since we have iterators only if inside a collection expr */\r\n\r\nif containingExpression.oclIsUndefined()\r\nthen localScopeAccumulator\r\nelse containingExpression.scopeAccumulator->union(localScopeAccumulator)\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeParameters(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen localScopeParameters\r\nelse containingExpression.scopeParameters\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeContainerBase(), source,
				new String[] { "derive", "localScopeContainer" });
		addAnnotation(getMAbstractExpression_ScopeNumberBase(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined()\r\nthen self.localScopeNumberBaseDefinition\r\nelse containingExpression.scopeNumberBase\r\nendif" });
		addAnnotation(getMAbstractExpression_ScopeSource(), source,
				new String[] { "derive",
						"\r\nif containingExpression.oclIsUndefined() then localScopeSource else containingExpression.scopeSource endif" });
		addAnnotation(getMAbstractExpression_LocalEntireScope(), source,
				new String[] { "derive",
						"localScopeBase->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition))->union(\r\nlocalScopeSelf->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObj->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeTrg->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSimpleTypeConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeLiteralConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeObjectReferenceConstants->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeVariables->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeParameters->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeIterator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeAccumulator->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeNumberBaseDefinition->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeContainer->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))->union(\r\nlocalScopeSource->collect(oclAsType(mcore::expressions::MAbstractBaseDefinition)))\r\n" });
		addAnnotation(getMAbstractExpression_LocalScopeBase(), source,
				new String[] { "derive",
						"OrderedSet{\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::NullValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::FalseValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::TrueValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyStringValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::EmptyCollection}  \r\n}" });
		addAnnotation(getMAbstractExpression_LocalScopeSelf(), source,
				new String[] { "derive", "OrderedSet{Tuple{debug=\'\'}}" });
		addAnnotation(getMAbstractExpression_LocalScopeTrg(), source,
				new String[] { "derive",
						"if targetObjectType.oclIsUndefined() and self.targetSimpleType=mcore::SimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif" });
		addAnnotation(getMAbstractExpression_LocalScopeObj(), source,
				new String[] { "derive",
						"if objectObjectType.oclIsUndefined() then OrderedSet{} else OrderedSet{Tuple{debug=\'\'}} endif" });
		addAnnotation(getMAbstractExpression_LocalScopeSource(), source,
				new String[] { "derive",
						"if srcObjectType.oclIsUndefined() and self.srcObjectSimpleType=mcore::SimpleType::None\r\n\tthen OrderedSet{}\r\n\telse OrderedSet{Tuple{debug=\'\'}} endif" });
		addAnnotation(getMAbstractExpression_LocalScopeSimpleTypeConstants(),
				source, new String[] { "derive",
						"/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in*/\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MSimpleTypeConstantLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif*/" });
		addAnnotation(getMAbstractExpression_LocalScopeLiteralConstants(),
				source, new String[] { "derive",
						"/* if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in */\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MLiteralLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\n  endif\r\n/*\r\nelse OrderedSet{} endif\r\nendif */" });
		addAnnotation(
				getMAbstractExpression_LocalScopeObjectReferenceConstants(),
				source, new String[] { "derive",
						"/*if eContainer().oclIsUndefined() then OrderedSet{} \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation)\r\nthen let a: mcore::annotations::MExprAnnotation = eContainer().oclAsType(mcore::annotations::MExprAnnotation) in */\r\nlet a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then OrderedSet{} \r\n  else\r\n\r\n  a.namedConstant->iterate (i: mcore::expressions::MNamedConstant; r: OrderedSet(Tuple(namedConstant:mcore::expressions::MNamedConstant))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if i.expression.oclIsKindOf(mcore::expressions::MObjectReferenceLet) and (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedConstant=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  )\r\nendif\r\n/* else OrderedSet{} endif\r\nendif */" });
		addAnnotation(getMAbstractExpression_LocalScopeVariables(), source,
				new String[] { "derive",
						"let a: mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined()\r\n  then  OrderedSet{} \r\n  else\r\n  a.namedExpression->iterate (i: mcore::expressions::MNamedExpression; r: OrderedSet(Tuple(namedExpression:mcore::expressions::MNamedExpression))=OrderedSet{} |\r\n    if i.expression.oclIsUndefined() then r\r\n    \telse if  (not i.name.oclIsUndefined()) and (i.name<>\'\')\r\n    \t\tthen r->append(Tuple{namedExpression=i}) \r\n    \t\telse r \r\n    \tendif\r\n    endif\r\n  ) endif" });
		addAnnotation(getMAbstractExpression_LocalScopeIterator(), source,
				new String[] { "derive",
						"if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)\r\nthen let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in\r\n  if c.iteratorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{iterator=c.iteratorVar}} endif\r\nelse OrderedSet{} endif" });
		addAnnotation(getMAbstractExpression_LocalScopeAccumulator(), source,
				new String[] { "derive",
						"if eContainer().oclIsKindOf(mcore::expressions::MCollectionExpression)\r\nthen let c: mcore::expressions::MCollectionExpression = eContainer().oclAsType(mcore::expressions::MCollectionExpression) in\r\n  if c.accumulatorVar.oclIsUndefined() then OrderedSet{}\r\n  \telse OrderedSet{Tuple{accumulator=c.accumulatorVar}} endif\r\nelse OrderedSet{} endif" });
		addAnnotation(getMAbstractExpression_LocalScopeParameters(), source,
				new String[] { "derive",
						"if self.containingAnnotation=null \r\n  then OrderedSet{}\r\n  else \r\n  let e:MModelElement= self.containingAnnotation.annotatedElement in\r\n    if e=null \r\n      then OrderedSet{}\r\n      else if not e.oclIsKindOf(mcore::MOperationSignature)\r\n        then OrderedSet{}\r\n        else e.oclAsType(mcore::MOperationSignature).parameter\r\n             ->iterate (i: mcore::MParameter; r: OrderedSet(Tuple(parameter:mcore::MParameter))=OrderedSet{} \r\n                            |r->append(Tuple{parameter=i}) )\r\n    \tendif endif endif" });
		addAnnotation(getMAbstractExpression_LocalScopeNumberBaseDefinition(),
				source, new String[] { "derive",
						"OrderedSet{\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::ZeroValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::OneValue},\r\n  Tuple{expressionBase = mcore::expressions::ExpressionBase::MinusOneValue}\r\n}" });
		addAnnotation(getMAbstractExpression_LocalScopeContainer(), source,
				new String[] { "derive", "OrderedSet{Tuple{debug=\'\'}}" });
		addAnnotation(getMAbstractExpression_ContainingAnnotation(), source,
				new String[] { "derive",
						"-- expressions can now be as well annotations, so the \"containing\" annotation can be self.\r\nif self.oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n\tthen self.oclAsType(mcore::annotations::MExprAnnotation) \r\nelse if eContainer().oclIsUndefined() \r\n\tthen null \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n\tthen eContainer().oclAsType(mcore::annotations::MExprAnnotation) \r\nelse if eContainer().oclIsTypeOf(mcore::expressions::MNamedExpression) \r\n   then self.eContainer().oclAsType(mcore::expressions::MNamedExpression).containingAnnotation\r\nelse if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue) \r\n   then eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue)\r\n   \t\t\t.eContainer().oclAsType(mcore::expressions::MNewObjecct).containingAnnotation\r\nelse if containingExpression.oclIsUndefined() \r\n\tthen null \r\n\telse containingExpression.containingAnnotation endif endif endif endif endif\tendif" });
		addAnnotation(getMAbstractExpression_ContainingExpression(), source,
				new String[] { "derive",
						"if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression)\r\nthen eContainer().oclAsType(mcore::expressions::MAbstractExpression)\r\n\r\n else null endif endif\r\n" });
		addAnnotation(getMAbstractExpression_IsComplexExpression(), source,
				new String[] { "derive", "true\n" });
		addAnnotation(getMAbstractExpression_IsSubExpression(), source,
				new String[] { "derive",
						"if containingExpression.oclIsUndefined() then false\r\nelse not (containingExpression.oclIsKindOf(mcore::expressions::MNamedConstant) or containingExpression.oclIsKindOf(mcore::expressions::MNamedExpression)) endif" });
		addAnnotation(getMAbstractExpression_CalculatedOwnType(), source,
				new String[] { "derive",
						"let nl: mcore::MClassifier = null in nl\n" });
		addAnnotation(getMAbstractExpression_CalculatedOwnMandatory(), source,
				new String[] { "derive", "false\n" });
		addAnnotation(getMAbstractExpression_CalculatedOwnSingular(), source,
				new String[] { "derive", "true\n" });
		addAnnotation(getMAbstractExpression_CalculatedOwnSimpleType(), source,
				new String[] { "derive", "mcore::SimpleType::None" });
		addAnnotation(getMAbstractExpression_SelfObjectType(), source,
				new String[] { "derive",
						"let a:mcore::annotations::MExprAnnotation = self.containingAnnotation in\r\nif a.oclIsUndefined() then null\r\n  else a.selfObjectTypeOfAnnotation endif\r\n\r\n/*if eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n  then eContainer().oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation\r\n--else if self.oclsKindOf(mcore::annotations::MExprAnnotation)\r\n--   then self.oclAsType(mcore::annotations::MExprAnnotation).selfObjectTypeOfAnnotation\r\n\r\n    --else  if  eContainer().oclIsTypeOf(mcore::expressions::MAbstractExpression) then\r\n  -- eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType\r\n   else if eContainer().oclIsKindOf(mcore::expressions::MNewObjectFeatureValue)\r\n      then \r\n       if eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclIsKindOf(mcore::expressions::MNewObjecct)\r\n       then  eContainer().oclAsType(mcore::expressions::MNewObjectFeatureValue).eContainer().oclAsType(mcore::expressions::MNewObjecct).selfObjectType\r\n       else \r\n       null\r\n     endif\r\n       else eContainer().oclAsType(mcore::expressions::MAbstractExpression).selfObjectType\r\n       endif\r\n  --endif\r\nendif\r\nendif \r\n--endif\r\n*/" });
		addAnnotation(getMAbstractExpression_SelfObjectPackage(), source,
				new String[] { "derive",
						"if (let e: Boolean = selfObjectType.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if selfObjectType.oclIsUndefined()\n  then null\n  else selfObjectType.containingPackage\nendif\nendif\n" });
		addAnnotation(getMAbstractExpression_TargetObjectType(), source,
				new String[] { "derive",
						"if self.containingAnnotation.oclIsUndefined()\r\n   then null\r\n   else self.containingAnnotation.targetObjectTypeOfAnnotation endif\r\n/*   \r\nif eContainer().oclIsUndefined() then null\r\nelse if eContainer().oclIsKindOf(mcore::annotations::MExprAnnotation) \r\n  then eContainer().oclAsType(mcore::annotations::MExprAnnotation).targetObjectTypeOfAnnotation\r\n  else  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) then\r\n   eContainer().oclAsType(mcore::expressions::MAbstractExpression).targetObjectType else null endif\r\nendif\r\nendif */\r\n" });
		addAnnotation(getMAbstractExpression_TargetSimpleType(), source,
				new String[] { "derive",
						"if self.containingAnnotation.oclIsUndefined()\n   then null\n   else self.containingAnnotation.targetSimpleTypeOfAnnotation endif\n/*\nif (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.targetSimpleTypeOfAnnotation\nendif\nendif\n*/" });
		addAnnotation(getMAbstractExpression_ObjectObjectType(), source,
				new String[] { "derive",
						"if self.oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n  --replaced with self.\r\n     then self.oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation\r\nelse if self.oclIsKindOf(mcore::annotations::MUpdatePersistence)\r\n    then self.oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation\r\nelse if eContainer().oclIsUndefined()  then null \r\nelse if eContainer().oclIsKindOf(mcore::annotations::MUpdateValue)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdateValue).objectObjectTypeOfAnnotation \r\n  else if eContainer().oclIsKindOf(mcore::annotations::MUpdatePersistence)  \r\n     then eContainer().oclAsType(mcore::annotations::MUpdatePersistence).objectObjectTypeOfAnnotation   \r\nelse  if eContainer().oclIsKindOf(mcore::expressions::MAbstractExpression) \r\n     then eContainer().oclAsType(mcore::expressions::MAbstractExpression).objectObjectType \r\n     else null endif endif endif endif endif endif" });
		addAnnotation(getMAbstractExpression_ExpectedReturnType(), source,
				new String[] { "derive",
						"if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.expectedReturnTypeOfAnnotation\nendif\nendif\n" });
		addAnnotation(getMAbstractExpression_ExpectedReturnSimpleType(), source,
				new String[] { "derive",
						"if containingAnnotation.oclIsUndefined() \r\n     then mcore::SimpleType::None\r\n     else containingAnnotation.expectedReturnSimpleTypeOfAnnotation endif" });
		addAnnotation(getMAbstractExpression_IsReturnValueMandatory(), source,
				new String[] { "derive",
						"if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationMandatory\nendif\nendif\n" });
		addAnnotation(getMAbstractExpression_IsReturnValueSingular(), source,
				new String[] { "derive",
						"if (let e: Boolean = containingAnnotation.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if containingAnnotation.oclIsUndefined()\n  then null\n  else containingAnnotation.isReturnValueOfAnnotationSingular\nendif\nendif\n" });
		addAnnotation(getMAbstractExpression_SrcObjectType(), source,
				new String[] { "derive",
						"let srcType : MClassifier =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  ((srcClassifier.hasStorage) and (srcClassifier.changeable)) then srcClassifier.type\r\nelse null \r\nendif\r\nendif\r\nin\r\nif srcType.oclIsUndefined() then null else srcType endif\r\n\r\n" });
		addAnnotation(getMAbstractExpression_SrcObjectSimpleType(), source,
				new String[] { "derive",
						"let srcType : SimpleType =\r\nlet srcClassifier:MProperty = \r\nself.containingAnnotation.containingAbstractPropertyAnnotations.annotatedProperty\r\nin \r\nif srcClassifier.oclIsUndefined() then null\r\nelse if  (srcClassifier.hasStorage) and ( srcClassifier.changeable) then srcClassifier.simpleType\r\nelse null\r\nendif endif\r\nin\r\nif srcType.oclIsUndefined() then SimpleType::None else srcType endif\r\n" });
		addAnnotation(
				getMAbstractExpressionWithBase__BaseDefinition$Update__MAbstractBaseDefinition(),
				source, new String[] { "body", "null" });
		addAnnotation(getMAbstractExpressionWithBase__AsCodeForBuiltIn(),
				source, new String[] { "body", "baseAsCode\n" });
		addAnnotation(getMAbstractExpressionWithBase__AsCodeForConstants(),
				source, new String[] { "body",
						"if baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition)\r\nthen let b: mcore::expressions::MSimpleTypeConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MSimpleTypeConstantBaseDefinition) in\r\nb.namedConstant.eName\r\nelse if baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)\r\nthen let b: mcore::expressions::MLiteralConstantBaseDefinition =  baseDefinition.oclAsType(mcore::expressions::MLiteralConstantBaseDefinition) in\r\nb.namedConstant.eName\r\nelse null endif endif" });
		addAnnotation(getMAbstractExpressionWithBase__AsCodeForVariables(),
				source, new String[] { "body",
						"let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in\r\nlet vName: String = v.namedExpression.eName in\r\nbaseAsCode" });
		addAnnotation(getMAbstractExpressionWithBase_BaseAsCode(), source,
				new String[] { "derive",
						"if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen \'\'\n  else if baseDefinition.oclIsTypeOf(mcore::expressions::MNumberBaseDefinition)\n  then  baseDefinition.oclAsType(mcore::expressions::MNumberBaseDefinition).calculateAsCode(self)\n  else baseDefinition.calculatedAsCode\nendif\nendif\n" });
		addAnnotation(getMAbstractExpressionWithBase_BaseDefinition(), source,
				new String[] { "derive",
						"let r: mcore::expressions::MAbstractBaseDefinition =\r\n\r\nif base=mcore::expressions::ExpressionBase::Undefined \r\n  then null\r\nelse if base=mcore::expressions::ExpressionBase::SelfObject\r\n  then scopeSelf->first() \r\n else if base=mcore::expressions::ExpressionBase::EContainer\r\n  then scopeContainerBase->first() \r\nelse if base=mcore::expressions::ExpressionBase::Target\r\n  then scopeTrg->first() \r\nelse if base=mcore::expressions::ExpressionBase::ObjectObject\r\n  then scopeObj->first() \r\n  else if base=mcore::expressions::ExpressionBase::Source\r\n  then scopeSource->first() \r\nelse if base=mcore::expressions::ExpressionBase::ConstantValue \r\n  then scopeSimpleTypeConstants->select(c:mcore::expressions::MSimpleTypeConstantBaseDefinition | c.namedConstant = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::ConstantLiteralValue\r\n  then scopeLiteralConstants->select(c:mcore::expressions::MLiteralConstantBaseDefinition | c.namedConstant = baseVar)->first() \r\nelse if base=mcore::expressions::ExpressionBase::ConstantObjectReference\r\n  then scopeObjectReferenceConstants->select(c:mcore::expressions::MObjectReferenceConstantBaseDefinition | c.namedConstant = baseVar)->first()\r\nelse if base=mcore::expressions::ExpressionBase::Variable\r\n  then scopeVariables->select(c:mcore::expressions::MVariableBaseDefinition | c.namedExpression = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Parameter\r\n  then scopeParameters->select(c:mcore::expressions::MParameterBaseDefinition | c.parameter = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Iterator\r\n  then scopeIterator->select(c:mcore::expressions::MIteratorBaseDefinition | c.iterator = baseVar)->first()  \r\nelse if base=mcore::expressions::ExpressionBase::Accumulator\r\n  then scopeAccumulator->select(c:mcore::expressions::MAccumulatorBaseDefinition | c.accumulator = baseVar)->first()  \r\n else if base=mcore::expressions::ExpressionBase::OneValue or base=mcore::expressions::ExpressionBase::ZeroValue  or base = mcore::expressions::ExpressionBase::MinusOneValue\r\n then scopeNumberBase->select(c:mcore::expressions::MNumberBaseDefinition|c.expressionBase = base)->first()\r\nelse scopeBase->select(c:mcore::expressions::MBaseDefinition | c.expressionBase=base)->first() \r\nendif endif endif endif endif endif endif endif endif endif endif endif endif endif\r\nin\r\nif r.oclIsUndefined() then null else r endif",
						"choiceConstruction", "self.getScope()" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitType(), source,
				new String[] { "derive",
						"if baseDefinition.oclIsUndefined() \r\nthen null\r\nelse \r\n  /* Trick b/c we need to reference the container here and baseDefinition is generated on the fly */\r\n  if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::SelfObject\r\n    then self.selfObjectType\r\n\telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target\r\n    \tthen self.targetObjectType\r\n\telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::ObjectObject\r\n    \tthen self.objectObjectType\r\n    \telse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Source\r\n    \tthen self.srcObjectType\r\n   else if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::EContainer\r\n    \tthen  if self.selfObjectType.allClassesContainedIn()->size() = 1 then self.selfObjectType.allClassesContainedIn()->first() else null endif\r\n    \telse baseDefinition.calculatedType\r\n  \tendif endif endif endif\r\n  endif\r\nendif" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSimpleType(),
				source, new String[] { "derive",
						"if baseDefinition.oclIsUndefined()  or not (self.baseExitType.oclIsUndefined())\r\nthen mcore::SimpleType::None\r\nelse if baseDefinition.calculatedBase=mcore::expressions::ExpressionBase::Target\r\nthen self.targetSimpleType\r\nelse self.baseDefinition.calculatedSimpleType\r\nendif endif" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitMandatory(),
				source, new String[] { "derive",
						"if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen false\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.calculatedMandatory\nendif\nendif\n" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSingular(), source,
				new String[] { "derive",
						"if (let e: Boolean = baseDefinition.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen true\n  else if baseDefinition.oclIsUndefined()\n  then null\n  else baseDefinition.calculatedSingular\nendif\nendif\n" });
		addAnnotation(mAbstractBaseDefinitionEClass, source,
				new String[] { "label", "calculatedBase.toString()" });
		addAnnotation(getMAbstractBaseDefinition_CalculatedBase(), source,
				new String[] { "derive", "ExpressionBase::Undefined" });
		addAnnotation(getMAbstractBaseDefinition_CalculatedAsCode(), source,
				new String[] { "derive", "\'\'\n" });
		addAnnotation(mContainerBaseDefinitionEClass, source,
				new String[] { "label", "calculatedAsCode\n" });
		addAnnotation(mBaseDefinitionEClass, source, new String[] { "label",
				"\'<builtin> \'.concat(calculatedAsCode)" });
		addAnnotation(mNumberBaseDefinitionEClass, source, new String[] {
				"label", "\'<builtin> \'.concat(calculatedAsCode)" });
		addAnnotation(
				getMNumberBaseDefinition__CalculateAsCode__MAbstractExpression(),
				source, new String[] { "body",
						"\r\nself.calculatedAsCode.concat(if caller.expectedReturnSimpleType = SimpleType::Double then \'.0\' else \'\'endif)\r\n\r\n\r\n\r\n" });
		addAnnotation(mSelfBaseDefinitionEClass, source,
				new String[] { "label", "calculatedAsCode\n" });
		addAnnotation(mTargetBaseDefinitionEClass, source,
				new String[] { "label", "calculatedAsCode\n" });
		addAnnotation(mSourceBaseDefinitionEClass, source,
				new String[] { "label", "calculatedAsCode\n" });
		addAnnotation(mObjectBaseDefinitionEClass, source,
				new String[] { "label", "calculatedAsCode\n" });
		addAnnotation(mSimpleTypeConstantBaseDefinitionEClass, source,
				new String[] { "label",
						"\'<const> \'.concat(if namedConstant.oclIsUndefined() then \'\' else namedConstant.eName endif)" });
		addAnnotation(getMSimpleTypeConstantBaseDefinition_ConstantLet(),
				source, new String[] { "derive",
						"if namedConstant.expression.oclIsUndefined() then  null\r\nelse if namedConstant.expression.oclIsKindOf(MSimpleTypeConstantLet) \r\n\tthen namedConstant.expression.oclAsType(MSimpleTypeConstantLet)\r\n\telse null endif \r\nendif" });
		addAnnotation(mLiteralConstantBaseDefinitionEClass, source,
				new String[] { "label",
						"\'<const> \'.concat(if namedConstant.oclIsUndefined() then \'\' else namedConstant.eName endif)" });
		addAnnotation(getMLiteralConstantBaseDefinition_ConstantLet(), source,
				new String[] { "derive",
						"if namedConstant.expression.oclIsUndefined() then  null\r\nelse if namedConstant.expression.oclIsKindOf(MLiteralLet) \r\n\tthen namedConstant.expression.oclAsType(MLiteralLet)\r\n\telse null endif \r\nendif" });
		addAnnotation(mObjectReferenceConstantBaseDefinitionEClass, source,
				new String[] { "label",
						"\'<const> \'.concat(if namedConstant.oclIsUndefined() then \'\' else namedConstant.eName endif)" });
		addAnnotation(getMObjectReferenceConstantBaseDefinition_ConstantLet(),
				source, new String[] { "derive",
						"if namedConstant.expression.oclIsUndefined() then  null\r\nelse if namedConstant.expression.oclIsKindOf(MObjectReferenceLet) \r\n\tthen namedConstant.expression.oclAsType(MObjectReferenceLet)\r\n\telse null endif \r\nendif" });
		addAnnotation(mVariableBaseDefinitionEClass, source, new String[] {
				"label",
				"\'<var> \'.concat(if namedExpression.oclIsUndefined() then \'\' else namedExpression.eName endif)" });
		addAnnotation(getMVariableBaseDefinition_VariableLet(), source,
				new String[] { "derive",
						"if namedExpression.expression.oclIsUndefined() then  null\r\nelse if namedExpression.expression.oclIsKindOf(MToplevelExpression) \r\n\tthen namedExpression.expression.oclAsType(MToplevelExpression)\r\n\telse null endif \r\nendif" });
		addAnnotation(mIteratorBaseDefinitionEClass, source, new String[] {
				"label",
				"\'<iterator> \'.concat(if iterator.oclIsUndefined() then \'\' else iterator.eName endif)" });
		addAnnotation(mParameterBaseDefinitionEClass, source, new String[] {
				"label",
				"\'<par> \'.concat(if parameter.oclIsUndefined() then \'\' else parameter.eName endif)" });
		addAnnotation(mAccumulatorBaseDefinitionEClass, source, new String[] {
				"label",
				"\'<accumulator> \'.concat(if accumulator.oclIsUndefined() then \'\' else accumulator.eName endif)" });
		addAnnotation(getMAbstractNamedTuple_AbstractEntry(), source,
				new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getMNewObjectFeatureValue_Feature(), source,
				new String[] { "choiceConstruction",
						"if self.eContainer().oclAsType(expressions::MNewObjecct).newType.oclIsUndefined() then \r\nOrderedSet{} else\r\nlet type : MClassifier = \r\nself.eContainer().oclAsType(expressions::MNewObjecct).newType in\r\ntype.allFeaturesWithStorage()->union(type.allContainmentReferences())  endif" });
		addAnnotation(
				getMNamedExpression__DoAction$Update__MNamedExpressionAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMNamedExpression__DefaultValue(), source,
				new String[] { "body",
						"Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMNamedExpression__AllApplications(), source,
				new String[] { "body",
						"let all: OrderedSet(MApplication) = \r\nself.expression->asOrderedSet()->first().oclAsType(MApplication)->asSequence()->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))->union(self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication)->asSequence())->asOrderedSet()\r\n\r\n --self.expression->closure(oclAsType(MApplication).operands).oclAsType(MApplication) ->union(  self.expression->closure(oclAsType(MApplication).operands->select(oclIsTypeOf(MApplication))).oclAsType(MApplication))\r\n --->reject(oclIsUndefined())->asOrderedSet() \r\n --->append(self.expression->asOrderedSet()->first().oclAsType(MApplication))->asOrderedSet()\r\n -->reject(oclIsUndefined())->asOrderedSet()\r\n in if all->oclIsUndefined() then null else all endif" });
		addAnnotation(getMNamedExpression_Expression(), source,
				new String[] { "initValue", "defaultValue()" });
		addAnnotation(getMNamedExpression_DoAction(), source, new String[] {
				"derive", "mcore::expressions::MNamedExpressionAction::Do\n" });
		addAnnotation(
				getMAbstractChain__ProcessorDefinition$Update__MProcessorDefinition(),
				source, new String[] { "body", "null" });
		addAnnotation(getMAbstractChain__Length(), source, new String[] {
				"body",
				"if not element3.oclIsUndefined() then 3\r\nelse if not element2.oclIsUndefined() then 2\r\nelse if not element1.oclIsUndefined() then 1\r\nelse 0 endif endif endif" });
		addAnnotation(getMAbstractChain__UnsafeElementAsCode__Integer(), source,
				new String[] { "body",
						"let element: MProperty = if step=1 then element1\r\n\telse if step=2 then element2 \r\n\t\telse if step=3 then element3\r\n\t\t\telse null endif endif endif in\r\n\t\r\nif element.oclIsUndefined() then \'ERROR\' else element.eName endif" });
		addAnnotation(getMAbstractChain__UnsafeChainStepAsCode__Integer(),
				source, new String[] { "body",
						"if step=1 then\r\n\tif element1.oclIsUndefined() then \'MISSING ELEMENT 1\'\r\n\telse unsafeElementAsCode(1) endif\r\nelse if step=2 then\r\n\tif element2.oclIsUndefined() then \'MISSING ELEMENT 2\'\r\n\telse unsafeElementAsCode(2) endif\r\nelse if step=3 then\r\n\tif element3.oclIsUndefined() then \'MISSING ELEMENT 3\'\r\n\telse unsafeElementAsCode(3) endif\r\nelse \'ERROR\'\r\nendif endif endif" });
		addAnnotation(getMAbstractChain__UnsafeChainAsCode__Integer(), source,
				new String[] { "body",
						"unsafeChainAsCode(fromStep, length())" });
		addAnnotation(getMAbstractChain__UnsafeChainAsCode__Integer_Integer(),
				source, new String[] { "body",
						"let end: Integer = if (length() > toStep) then toStep else length() endif in\r\n\r\nif fromStep=1 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2)).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then\r\n  \t\tunsafeChainStepAsCode(1).concat(\'.\').concat(unsafeChainStepAsCode(2))\r\n\telse if end=1 then unsafeChainStepAsCode(1) else \'\' endif\r\n\tendif endif\r\nelse if fromStep=2 then\r\n\tif end=3 then\r\n\t\tunsafeChainStepAsCode(2).concat(\'.\').concat(unsafeChainStepAsCode(3)) \r\n\telse if end=2 then unsafeChainStepAsCode(2) else \'\' endif\r\n\tendif\r\nelse if fromStep=3 then\r\n\tif end=3 then unsafeChainStepAsCode(3) else \'\' endif\r\n\telse \'ERROR\'\r\nendif endif endif\r\n" });
		addAnnotation(getMAbstractChain__AsCodeForOthers(), source,
				new String[] { "body", "null" });
		addAnnotation(getMAbstractChain__CodeForLength1(), source,
				new String[] { "body", "null" });
		addAnnotation(getMAbstractChain__CodeForLength2(), source,
				new String[] { "body", "null" });
		addAnnotation(getMAbstractChain__CodeForLength3(), source,
				new String[] { "body", "null" });
		addAnnotation(getMAbstractChain__ProcAsCode(), source, new String[] {
				"body",
				"if self.processor= mcore::expressions::MProcessor::IsNull then \'.oclIsUndefined()\' \r\nelse if self.processor = mcore::expressions::MProcessor::AllUpperCase then \'toUpperCase()\'\r\nelse if self.processor = mcore::expressions::MProcessor::IsInvalid then \'.oclIsInvalid()\'\r\nelse if self.processor = mcore::expressions::MProcessor::Container then \'eContainer()\'\r\nelse  self.processor.toString()\r\nendif endif endif endif" });
		addAnnotation(getMAbstractChain__IsCustomCodeProcessor(), source,
				new String[] { "body",
						"if self.processorIsSet().oclIsUndefined() then null\r\nelse \r\nprocessor = mcore::expressions::MProcessor::Head or\r\nprocessor = mcore::expressions::MProcessor::Tail or\r\nprocessor = mcore::expressions::MProcessor::And or\r\nprocessor = mcore::expressions::MProcessor::Or  \r\nendif\r\n\r\n\r\n" });
		addAnnotation(getMAbstractChain__IsProcessorSetOperator(), source,
				new String[] { "body",
						"if processor = mcore::expressions::MProcessor::None then false \r\nelse\r\nprocessor=mcore::expressions::MProcessor::AsOrderedSet or\r\nprocessor=mcore::expressions::MProcessor::First or\r\nprocessor=mcore::expressions::MProcessor::IsEmpty or\r\nprocessor=mcore::expressions::MProcessor::Last or\r\nprocessor=mcore::expressions::MProcessor::NotEmpty or\r\nprocessor=mcore::expressions::MProcessor::Size or\r\nprocessor=mcore::expressions::MProcessor::Sum or\r\nprocessor=mcore::expressions::MProcessor::Head or\r\nprocessor=mcore::expressions::MProcessor::Tail or\r\nprocessor=mcore::expressions::MProcessor::And or\r\nprocessor=mcore::expressions::MProcessor::Or\r\nendif" });
		addAnnotation(getMAbstractChain__IsOwnXOCLOperator(), source,
				new String[] { "body",
						"processor =mcore::expressions::MProcessor::CamelCaseLower or\r\nprocessor =mcore::expressions::MProcessor::CamelCaseToBusiness or\r\nprocessor =mcore::expressions::MProcessor::CamelCaseUpper " });
		addAnnotation(getMAbstractChain__ProcessorReturnsSingular(), source,
				new String[] { "body",
						"if self.processor = mcore::expressions::MProcessor::None then null\r\nelse if\r\n self.processor = mcore::expressions::MProcessor::AsOrderedSet or\r\n processor = mcore::expressions::MProcessor::Head or\r\n processor= mcore::expressions::MProcessor::Tail\r\n\r\nthen \r\nfalse\r\nelse true\r\nendif endif" });
		addAnnotation(getMAbstractChain__ProcessorIsSet(), source,
				new String[] { "body",
						"self.processor <> mcore::expressions::MProcessor::None" });
		addAnnotation(getMAbstractChain__CreateProcessorDefinition(), source,
				new String[] { "body", "Tuple{processor=processor}\n" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObject(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::Container},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObjects(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail},\nTuple{processor=mcore::expressions::MProcessor::Container}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBoolean(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsFalse},\nTuple{processor=mcore::expressions::MProcessor::IsTrue},\nTuple{processor=mcore::expressions::MProcessor::Not},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBooleans(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::And},\nTuple{processor=mcore::expressions::MProcessor::Or},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForInteger(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::IsZero},\nTuple{processor=mcore::expressions::MProcessor::IsOne},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForIntegers(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Sum},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReal(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Round},\nTuple{processor=mcore::expressions::MProcessor::Floor},\nTuple{processor=mcore::expressions::MProcessor::IsZero},\nTuple{processor=mcore::expressions::MProcessor::IsOne},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReals(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Round},\nTuple{processor=mcore::expressions::MProcessor::Floor},\nTuple{processor=mcore::expressions::MProcessor::Sum},\nTuple{processor=mcore::expressions::MProcessor::PlusOne},\nTuple{processor=mcore::expressions::MProcessor::MinusOne},\nTuple{processor=mcore::expressions::MProcessor::TimesMinusOne},\nTuple{processor=mcore::expressions::MProcessor::Absolute},\nTuple{processor=mcore::expressions::MProcessor::OneDividedBy},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForString(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Trim},\nTuple{processor=mcore::expressions::MProcessor::AllLowerCase},\nTuple{processor=mcore::expressions::MProcessor::AllUpperCase},\nTuple{processor=mcore::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mcore::expressions::MProcessor::ToBoolean},\nTuple{processor=mcore::expressions::MProcessor::ToInteger},\nTuple{processor=mcore::expressions::MProcessor::ToReal},\nTuple{processor=mcore::expressions::MProcessor::ToDate},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForStrings(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Trim},\nTuple{processor=mcore::expressions::MProcessor::AllLowerCase},\nTuple{processor=mcore::expressions::MProcessor::AllUpperCase},\nTuple{processor=mcore::expressions::MProcessor::FirstUpperCase},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseLower},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseUpper},\nTuple{processor=mcore::expressions::MProcessor::CamelCaseToBusiness},\nTuple{processor=mcore::expressions::MProcessor::ToBoolean},\nTuple{processor=mcore::expressions::MProcessor::ToInteger},\nTuple{processor=mcore::expressions::MProcessor::ToReal},\nTuple{processor=mcore::expressions::MProcessor::ToDate},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDate(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Year},\nTuple{processor=mcore::expressions::MProcessor::Month},\nTuple{processor=mcore::expressions::MProcessor::Day},\nTuple{processor=mcore::expressions::MProcessor::Hour},\nTuple{processor=mcore::expressions::MProcessor::Minute},\nTuple{processor=mcore::expressions::MProcessor::Second},\nTuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mcore::expressions::MProcessor::ToHhMm},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::IsNull},\nTuple{processor=mcore::expressions::MProcessor::NotNull},\nTuple{processor=mcore::expressions::MProcessor::AsOrderedSet},\nTuple{processor=mcore::expressions::MProcessor::IsInvalid}}" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDates(), source,
				new String[] { "body",
						"OrderedSet{\nTuple{processor=mcore::expressions::MProcessor::Year},\nTuple{processor=mcore::expressions::MProcessor::Month},\nTuple{processor=mcore::expressions::MProcessor::Day},\nTuple{processor=mcore::expressions::MProcessor::Hour},\nTuple{processor=mcore::expressions::MProcessor::Minute},\nTuple{processor=mcore::expressions::MProcessor::Second},\nTuple{processor=mcore::expressions::MProcessor::ToYyyyMmDd},\nTuple{processor=mcore::expressions::MProcessor::ToHhMm},\nTuple{processor=mcore::expressions::MProcessor::ToString},\nTuple{processor=mcore::expressions::MProcessor::IsEmpty},\nTuple{processor=mcore::expressions::MProcessor::NotEmpty},\nTuple{processor=mcore::expressions::MProcessor::Size},\nTuple{processor=mcore::expressions::MProcessor::First},\nTuple{processor=mcore::expressions::MProcessor::Last},\nTuple{processor=mcore::expressions::MProcessor::Head},\nTuple{processor=mcore::expressions::MProcessor::Tail}}" });
		addAnnotation(getMAbstractChain_ChainEntryType(), source,
				new String[] { "derive", "let c:MClassifier=null in c" });
		addAnnotation(getMAbstractChain_ChainAsCode(), source,
				new String[] { "derive", "unsafeChainAsCode(1)" });
		addAnnotation(getMAbstractChain_Element1(), source, new String[] {
				"choiceConstruction",
				"\r\nlet annotatedProp: mcore::MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\nif self.chainEntryType.oclIsUndefined() then \r\nOrderedSet{} else\r\nself.chainEntryType.allProperties()\r\nendif\r\n\r\n--" });
		addAnnotation(getMAbstractChain_Element1Correct(), source,
				new String[] { "derive",
						"if element1.oclIsUndefined() then true else\r\n--if element1.type.oclIsUndefined() and (not element1.simpleTypeIsCorrect) then false else\r\nif chainEntryType.oclIsUndefined() then false \r\n  else chainEntryType.allProperties()->includes(element1)\r\nendif endif \r\n--endif" });
		addAnnotation(getMAbstractChain_Element2EntryType(), source,
				new String[] { "derive",
						"if not self.element1Correct then null\r\nelse \r\nif self.element1.oclIsUndefined() then null \r\nelse self.element1.type endif endif" });
		addAnnotation(getMAbstractChain_Element2(), source, new String[] {
				"choiceConstruction",
				"let annotatedProp: MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\n\r\nif element1.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if element1.isOperation\r\n    then OrderedSet{} \r\n    else if element2EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else element2EntryType.allProperties() endif\r\n  \t endif\r\n  endif\r\n" });
		addAnnotation(getMAbstractChain_Element2Correct(), source,
				new String[] { "derive",
						"if element2.oclIsUndefined() then true else\r\n--if element2.type.oclIsUndefined() and (not element2.simpleTypeIsCorrect) then false else\r\nif element2EntryType.oclIsUndefined() then false \r\n  else element2EntryType.allProperties()->includes(self.element2)\r\nendif endif \r\n--endif" });
		addAnnotation(getMAbstractChain_Element3EntryType(), source,
				new String[] { "derive",
						"if not self.element2Correct then null\r\nelse \r\nif self.element2.oclIsUndefined() then null\r\nelse self.element2.type endif endif" });
		addAnnotation(getMAbstractChain_Element3(), source, new String[] {
				"choiceConstruction",
				"let annotatedProp: mcore::MProperty = \r\nself.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)\r\nin\r\n\r\nif element2.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else if element2.isOperation\r\n    then OrderedSet{} \r\n    else if element3EntryType.oclIsUndefined() \r\n\t  then OrderedSet{}\r\n  \t  else element3EntryType.allProperties() endif\r\n  \t endif\r\n  endif\r\n" });
		addAnnotation(getMAbstractChain_Element3Correct(), source,
				new String[] { "derive",
						"if element3.oclIsUndefined() then true else\r\n--if element3.type.oclIsUndefined() and (not element3.simpleTypeIsCorrect) then false else\r\nif element3EntryType.oclIsUndefined() then false\r\n  else element3EntryType.allProperties()->includes(self.element3)\r\nendif endif \r\n--endif" });
		addAnnotation(getMAbstractChain_CastType(), source,
				new String[] { "choiceConstraint",
						"trg.kind = mcore::ClassifierKind::ClassType " });
		addAnnotation(getMAbstractChain_LastElement(), source, new String[] {
				"derive",
				"if not self.element3.oclIsUndefined() then self.element3 else\r\nif not self.element2.oclIsUndefined() then self.element2 else\r\nif not self.element1.oclIsUndefined() then self.element1 else\r\nnull endif endif endif " });
		addAnnotation(getMAbstractChain_ChainCalculatedType(), source,
				new String[] { "derive",
						"let nl: mcore::MClassifier = null in nl\n" });
		addAnnotation(getMAbstractChain_ChainCalculatedSimpleType(), source,
				new String[] { "derive",
						"let nl: mcore::SimpleType = null in nl\n" });
		addAnnotation(getMAbstractChain_ChainCalculatedSingular(), source,
				new String[] { "derive", "let nl: Boolean = null in nl\n" });
		addAnnotation(getMAbstractChain_ProcessorDefinition(), source,
				new String[] { "derive",
						"if (let e0: Boolean = processor = mcore::expressions::MProcessor::None in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen null\n  else createProcessorDefinition()\nendif\n",
						"choiceConstruction",
						"\nlet s:SimpleType = self.chainCalculatedSimpleType in\nlet t:MClassifier = self.chainCalculatedType in\nlet res:OrderedSet(mcore::expressions::MProcessorDefinition) =\nif s = mcore::SimpleType::Boolean\n    then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForBoolean()\n                 else self.procDefChoicesForBooleans() endif\nelse if s = mcore::SimpleType::Integer \n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForInteger()\n                 else self.procDefChoicesForIntegers() endif\nelse if   s = mcore::SimpleType::Double\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForReal()\n                 else self.procDefChoicesForReals() endif\nelse if   s = mcore::SimpleType::String\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForString()\n                 else self.procDefChoicesForStrings() endif\nelse if   s = mcore::SimpleType::Date\n     then if self.chainCalculatedSingular = true\n                 then self.procDefChoicesForDate()\n                 else self.procDefChoicesForDates() endif\nelse if s = mcore::SimpleType::None or \n          s = mcore::SimpleType::Annotation or \n          s = mcore::SimpleType::Attribute or \n          s = mcore::SimpleType::Class or \n          s = mcore::SimpleType::Classifier or \n          s = mcore::SimpleType::DataType or \n          s = mcore::SimpleType::Enumeration or \n          s = mcore::SimpleType::Feature or \n          s = mcore::SimpleType::KeyValue or \n          s = mcore::SimpleType::Literal or \n          s = mcore::SimpleType::NamedElement or \n          s = mcore::SimpleType::Object or \n          s = mcore::SimpleType::Operation or \n          s = mcore::SimpleType::Package or \n          s = mcore::SimpleType::Parameter or \n          s = mcore::SimpleType::Reference or \n          s = mcore::SimpleType::TypedElement \n     then if self.chainCalculatedSingular \n                 then self.procDefChoicesForObject()\n                 else self.procDefChoicesForObjects()\n                        --OrderedSet{Tuple{processor=MProcessor::Head},\n                        --                   Tuple{processor=MProcessor::Tail}} \n                        endif\n     else OrderedSet{} endif endif endif endif endif endif\nin res->prepend(null)\n                                           \n  " });
		addAnnotation(getMBaseChain__AutoCastWithProc(), source, new String[] {
				"body",
				" let code: String = if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) \r\n\tthen asCodeForBuiltIn()\r\n\telse if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) then asCodeForConstants()\r\n\telse if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) then asCodeForVariables()\r\n\telse asCodeForOthers() endif endif endif in\r\nlet apply: mcore::expressions::MApplication= if self.eContainer().oclIsTypeOf(mcore::expressions::MApplication ) then self.eContainer().oclAsType(mcore::expressions::MApplication) else null endif in\r\nlet chainTypeString: String =typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in\r\nlet chainTypeStringSingular: String = typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in\r\n\r\n --let castType: MClassifier =  if apply.oclIsUndefined() then self.expectedReturnType else if self.castType.oclIsUndefined() then apply.operands->first().calculatedOwnType else self.castType endif endif\r\n  let castType: mcore::MClassifier =  if self.castType.oclIsUndefined() then (if apply.oclIsUndefined() then self.expectedReturnType else apply.operands->first().calculatedOwnType  endif) else self.castType endif  -- CastType has to be preferred to autocast\r\n \r\n in\r\n   let castTypeString: String = typeAsOcl(selfObjectPackage, castType, SimpleType::None, chainCalculatedSingular) in\r\n\t\t  let castTypeStringSingular: String = typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in\r\n\t\r\n\tlet opChangesReturn : Boolean = apply.operands->first().calculatedSimpleType <> apply.calculatedSimpleType or apply.operands->first().calculatedType <> apply.calculatedType\r\nin\r\n\t\t        let variableName : String = self.uniqueChainNumber() in  \r\n\t\t        \r\n\t\t        -- chain name\r\n\t\t        let chainName: String = \r\n\t\t        if self.processor= mcore::expressions::MProcessor::None then \'\' else\r\n\t\t\'let \'.concat(variableName).concat(\': \').concat(if self.chainCalculatedSingular then castTypeStringSingular else castTypeString endif).concat(\' = \') endif\r\n\t\t   in\r\n\t-- chainname end\t\r\n\r\nif  \r\n\r\nnot(self.castType.oclIsUndefined()) or \r\n--new\r\n(if (not(apply.oclIsUndefined()) and (apply.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean  or (apply.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)))then \r\n   if apply.operands->first().calculatedOwnType.oclIsUndefined() then false\r\n    else    apply.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and apply.operands->first().calculatedOwnType<> self.calculatedOwnType\r\n        endif else false endif)\r\n   --new     \r\n         or\r\n\r\n( if apply.oclIsUndefined() then self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType               --   check is dont have an apply but chain only    :::::::.changed OwnType for Collection\r\n else ((apply.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and apply.calculatedOwnSimpleType= mcore::SimpleType::None endif)      --check if Applytype differs from chain and type is a Classifier\r\n  \r\nthen\r\n\t\tchainName.concat(\'let chain: \').concat(chainTypeString).concat(\' = \').concat(code).concat(\' in\\n\').concat(\r\n\t\t\t  if chainCalculatedSingular then \'if chain.oclIsUndefined()\'.concat(\'\\n\').concat(\'  then null\\n  else \') else \'\' endif\r\n\t\t    ).concat(\r\n\t\t\t  if chainCalculatedSingular\r\n\t\t\t    then\r\n\t\t\t      \'if chain.oclIsKindOf(\'.concat(castTypeStringSingular).concat(\')\\n    then chain.oclAsType(\').concat(castTypeStringSingular).concat(\')\\n    else null\\n  endif\')\r\n\t\t\t    else \r\n\r\n \'chain->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular).concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\').concat(\')->asOrderedSet() \\n else r endif)\')\r\n\t\t\t     endif\r\n\t\t\t .concat(\r\n\t\t\t  if chainCalculatedSingular then \'\\n  endif\' else \'\' endif\r\n\t\t    ).concat(if self.processor= MProcessor::None then \'\' else \' in\\n\'.concat(    -- Accessing feature is not unary\r\n\t\t\r\n\t\t\'if \').concat(variableName).concat(if self.isOwnXOCLOperator() then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(if self.isProcessorSetOperator() then \'->\' else if not(self.isProcessorCheckEqualOperator()) then \'.\' else \'\' endif endif).concat(self.procAsCode())\r\n\t\t.concat(if self.isProcessorCheckEqualOperator() then \' then true else false \' \r\n\t\t\r\n\t\t\r\n\t\telse (if processor=mcore::expressions::MProcessor::AsOrderedSet then \'->\' else \'.\' endif).concat(\'oclIsUndefined() \\n then null \\n else \'  -- todo   processor does not influence calculatedSingular\r\n.concat(variableName).concat(\r\n\t\t      if self.isProcessorSetOperator() then \'->\' else \'.\' endif --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t      ).concat(self.procAsCode())) endif) \r\n\t\t      \r\n\t\t      .concat( \r\n\t\t\t \'\\n  endif\' )  \r\n\t\t     endif))\r\n\t\t    else\r\n\t\t    \r\n\t\t    let procString : String =  if self.processor <> mcore::expressions::MProcessor::None then\r\n\t\t --  let a: String =  \'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: String = \\\'\\\' | r.concat(i.toString()) )\')  in\r\n\t\t  \t\'let \'.concat(variableName).concat(\': \').concat(chainTypeString).concat(\' = \').concat(code).concat(\' in\\n\').concat(    -- Accessing feature is not unary\r\n\t\t\r\n\t\t\'if \').concat(variableName).concat(if self.isOwnXOCLOperator() then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(if self.isProcessorSetOperator() then \'->\' else if not(self.isProcessorCheckEqualOperator()) then \'.\' else \'\' endif endif).concat(self.procAsCode())\r\n\t\t.concat(if self.isProcessorCheckEqualOperator() then \' then true else false \' \r\n\t\t\r\n\t\t\r\n\t\telse (if processor=mcore::expressions::MProcessor::AsOrderedSet then \'->\' else \'.\' endif).concat(\'oclIsUndefined() \\n then null \\n else \'  -- todo   processor does not influence calculatedSingular\r\n.concat(variableName).concat(\r\n\t\t      if self.isProcessorSetOperator() then \'->\' else \'.\' endif --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t      ).concat(self.procAsCode())) endif) \r\n\t\t      \r\n\t\t      .concat( \r\n\t\t\t \'\\n  endif\'  )  else code endif in\r\n\t\t\t procString.concat\r\n\t\t\t (if self.chainCalculatedSingular or self.processor <> mcore::expressions::MProcessor::None then \'.toString()\' else  \'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: String = \\\'\\\' | r.concat(i.toString()) )\')\r\n\t\t\t  endif)\r\n\t\t\t\r\n\t\t     \r\n\t\tendif\r\n" });
		addAnnotation(getMBaseChain__OwnToApplyMismatch(), source,
				new String[] { "body",
						"if self.eContainer().oclIsTypeOf(mcore::expressions::MIf) then false   -- Implement IF typemismatch\r\nelse\r\nif self.eContainer().oclIsTypeOf(mcore::expressions::MApplication) and self.containedCollector.oclIsUndefined()\r\nthen \r\nlet app: mcore::expressions::MApplication = self.eContainer().oclAsType(mcore::expressions::MApplication) in\r\nlet opChangesReturn : Boolean = app.operands->first().calculatedSimpleType <> app.calculatedSimpleType or app.operands->first().calculatedType <> app.calculatedType\r\n--let opChangesReturn : Boolean = app.operands->first().calculatedOwnSimpleType <> app.calculatedOwnSimpleType or app.operands->first().calculatedOwnType <> app.calculatedOwnType\r\nin\r\n\r\nif self.base = mcore::expressions::ExpressionBase::SelfObject or  self.base = mcore::expressions::ExpressionBase::Variable\r\nthen -- builtin been casted  : TODO  add Parameter,Iterator etc...\r\n\r\nif (app.calculatedOwnSimpleType= SimpleType::Boolean and self.calculatedSimpleType <> mcore::SimpleType::Boolean) or (app.calculatedOwnSimpleType= mcore::SimpleType::Integer and self.calculatedSimpleType <> mcore::SimpleType::Integer)  then \r\n   if app.operands->first().calculatedOwnType.oclIsUndefined() then (self.calculatedOwnSimpleType <>  app.operands->first().calculatedOwnSimpleType and app.calculatedSimpleType<> mcore::SimpleType::Double)\r\n    else    app.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType\r\n        endif\r\n\r\nelse\r\n\r\n\r\nif ((app.calculatedOwnSimpleType = self.calculatedOwnSimpleType) or opChangesReturn) and (app.calculatedOwnSimpleType= SimpleType::None)-- CHanged to App.calcOwnType\r\nthen\r\napp.operands->first().calculatedOwnType.allSubTypes()->excludes(self.calculatedOwnType) and app.operands->first().calculatedOwnType<> self.calculatedOwnType\r\nelse if  ((app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) or opChangesReturn) and  (app.calculatedSimpleType <> SimpleType::None and (app.calculatedSimpleType <> SimpleType::Double))\r\n-- (app.calculatedSimpleType <> SimpleType::None  and app.calculatedSimpleType <> SimpleType::Boolean)\r\nthen true else false endif\r\n\r\n endif\r\n \r\nendif\r\n\r\nelse\r\n\r\nif self.base = ExpressionBase::SelfObject \r\nthen\r\n(app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType) and opChangesReturn\r\nelse \r\nfalse\r\nendif\r\nendif\r\n\r\n\r\nelse if not(self.eContainer().oclIsTypeOf(MApplication)) and self.containedCollector.oclIsUndefined() \r\nand self.base = ExpressionBase::SelfObject then     --TODO  add parameter,iterator etc\r\n--self.expectedReturnType <> self.calculatedType and self.expectedReturnSimpleType = self.calculatedSimpleType or     FOR TYPE\r\nif self.eContainer().oclIsTypeOf(MNamedExpression) and self.eContainer().oclIsTypeOf(MNamedExpression).oclIsTypeOf(mcore::annotations::MResult) and\r\nself.eContainer().oclAsType(MNamedExpression).eContainer().oclAsType(mcore::annotations::MExprAnnotation).namedExpression->asSequence()->last() = self.eContainer() \r\n --   or self.eContainer().oclIsTypeOf(MCollectionExpression)\r\n then\r\nif self.expectedReturnType.oclIsUndefined() then self.expectedReturnType = self.calculatedOwnType and self.expectedReturnSimpleType <> self.calculatedOwnSimpleType \r\nelse\r\nself.expectedReturnType.allSubTypes()->excludes(self.calculatedType) and  self.expectedReturnType <>self.calculatedOwnType and self.expectedReturnSimpleType = self.calculatedOwnSimpleType \r\nendif \r\nelse false endif\r\n\r\n\r\nelse\r\n\r\nfalse endif endif\r\nendif\r\n\r\n--if (app.calculatedOwnSimpleType = SimpleType::Double and self.calculatedOwnSimpleType= SimpleType::Integer) then false\r\n--else\r\n --    app.calculatedOwnSimpleType <> self.calculatedOwnSimpleType\r\n--endif\r\n--endif\r\n--else\r\n --null\r\n-- endif" });
		addAnnotation(getMBaseChain__UniqueChainNumber(), source, new String[] {
				"body",
				"\t\t        let variableName : String = \'chain\'.concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then \'\' else  self.eContainer().oclAsType(mcore::expressions::MApplication).uniqueApplyNumber().toString() endif).concat(if self.eContainer().oclAsType(mcore::expressions::MApplication).oclIsUndefined() then \'\' else\r\n\t\t        \r\n\t\t         if (self.eContainer().oclIsTypeOf(mcore::expressions::MApplication))  then \r\n\t\t          if (self.eContainer().oclAsType(mcore::expressions::MApplication).operands->isEmpty()) and self.oclIsTypeOf(mcore::expressions::MChain) then \' \'  else \r\n\t\t           self.eContainer().oclAsType(mcore::expressions::MApplication).operands->iterate(i:mcore::expressions::MChainOrApplication; r: OrderedSet(MBaseChain)=OrderedSet{} | if i.oclIsKindOf(MBaseChain) then r->including(i.oclAsType(MBaseChain))->asOrderedSet() else r endif )->indexOf(self).toString()\r\n\t\t           endif else \'\' \r\n\t\t           endif endif ) in variableName\r\n\t\t           " });
		addAnnotation(
				getMBaseChain__ReuseFromOtherNoMoreUsedChain__MBaseChain(),
				source, new String[] { "body", "false\n" });
		addAnnotation(getMBaseChain__ResetToBase__ExpressionBase_MVariable(),
				source, new String[] { "body", "false\n" });
		addAnnotation(
				getMBaseChain__ReuseFromOtherNoMoreUsedChainAsUpdate__MBaseChain(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMBaseChain__IsProcessorCheckEqualOperator(), source,
				new String[] { "body",
						"if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::IsOne or\r\nprocessor=expressions::MProcessor::IsZero or\r\nprocessor = expressions::MProcessor::IsFalse or\r\nprocessor = expressions::MProcessor::IsTrue or\r\nprocessor = expressions::MProcessor::NotNull \r\nendif" });
		addAnnotation(getMBaseChain__IsPrefixProcessor(), source, new String[] {
				"body",
				"if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::Not or\r\nprocessor = expressions::MProcessor::OneDividedBy\r\nendif" });
		addAnnotation(getMBaseChain__IsPostfixProcessor(), source,
				new String[] { "body",
						"if processor.oclIsUndefined() then false \r\nelse\r\nprocessor=expressions::MProcessor::PlusOne or\r\nprocessor=expressions::MProcessor::MinusOne or\r\nprocessor=expressions::MProcessor::TimesMinusOne or\r\nprocessor=expressions::MProcessor::IsNull or\r\nprocessor=expressions::MProcessor::IsInvalid\r\nendif" });
		addAnnotation(getMBaseChain__AsCodeForOthers(), source, new String[] {
				"body",
				"if length()=0 then baseAsCode\r\nelse if length()=1 then codeForLength1() \r\nelse if length()=2 then codeForLength2() \r\nelse if length()=3 then codeForLength3() \r\nelse \'ERROR\' endif endif endif endif " });
		addAnnotation(getMBaseChain__UnsafeElementAsCode__Integer(), source,
				new String[] { "body",
						"let element: mcore::MProperty = if step=1 then element1\r\n\telse if step=2 then element2 \r\n\t\telse if step=3 then element3\r\n\t\t\telse null endif endif endif in\r\n\t\r\nif element.oclIsUndefined() then \'ERROR\' else \r\nif element.isOperation\r\nthen if step=length()\r\n\tthen\r\n\t\tlet p: String = let pp: String = callArgument->iterate(\r\n\t\t\tx: mcore::expressions::MCallArgument; s: String = \'\' | \r\n\t\t  \ts.concat(\', \').concat(x.asCode)\r\n\t\t  ) in if pp.size()>2 then pp.substring(3,pp.size()) else pp endif in\r\n\t\telement.eName.concat(\'(\').concat(p).concat(\')\') \r\n\telse\t\t\r\n\t\telement.eName.concat(\'()\') endif\t\t\r\nelse \r\n\telement.eName \r\nendif endif" });
		addAnnotation(getMBaseChain__CodeForLength1(), source, new String[] {
				"body",
				"let chain:mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in\r\n\r\nlet b: String = if (chain.base = mcore::expressions::ExpressionBase::SelfObject ) then \'\' else chain.baseAsCode.concat(\'.\') endif in \r\nb.concat(unsafeChainStepAsCode(1)).concat(if chain.chainCalculatedSingular then \'\' else \'->asOrderedSet()\' endif)" });
		addAnnotation(getMBaseChain__CodeForLength2(), source, new String[] {
				"body",
				"\r\nlet chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in\r\nlet abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in\r\nlet b0: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then \'\' else abstract.baseAsCode.concat(\'.\') endif in \r\nlet b:String = if b0.oclIsUndefined() then \'PROBLEM WITH BASE\' else b0 endif in\r\nif element1.calculatedSingular and element2.calculatedSingular then\r\n  let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,2).concat(\'\\nendif\'))\r\n\r\nelse  if (not element1.calculatedSingular) and element2.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse  if (not element1.calculatedSingular) and (not element2.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->asOrderedSet()\')\r\n\r\nelse null\r\n\r\nendif endif endif endif" });
		addAnnotation(getMBaseChain__CodeForLength3(), source, new String[] {
				"body",
				"let chain: mcore::expressions::MBaseChain = self.oclAsType(mcore::expressions::MBaseChain) in\r\nlet abstract:  mcore::expressions::MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in\r\n\r\n\r\nlet b: String = if (abstract.base = mcore::expressions::ExpressionBase::SelfObject ) then \'\' else abstract.baseAsCode.concat(\'.\') endif in \r\n\r\nif element1.calculatedSingular and element2.calculatedSingular and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->asOrderedSet()\\nendif\')\r\n\r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n  \r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if chain.subExpression->isEmpty() and not(self.processorIsSet()) then \'\' else \'->asOrderedSet()\' endif)\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse null\r\nendif endif endif endif endif endif endif endif " });
		addAnnotation(getMBaseChain__AsCodeForVariables(), source,
				new String[] { "body",
						"let v: mcore::expressions::MVariableBaseDefinition = baseDefinition.oclAsType(mcore::expressions::MVariableBaseDefinition) in\r\nlet vName: String = v.namedExpression.eName in\r\n\r\nif v.calculatedSingular and (length() > 0)\r\n  then \r\n    \'if \'.concat(vName).concat(\' = null\\n  then null\\n  else \').concat(asCodeForOthers()).concat(\' endif\')\r\n  else asCodeForOthers()\r\nendif" });
		addAnnotation(getMBaseChain_TypeMismatch(), source,
				new String[] { "derive", "self.ownToApplyMismatch()" });
		addAnnotation(getMBaseChain_ContainedCollector(), source,
				new String[] { "initValue", "null" });
		addAnnotation(getMBaseChain_ChainCodeforSubchains(), source,
				new String[] { "derive",
						"let code: String = \r\n     if baseDefinition.oclIsKindOf(mcore::expressions::MBaseDefinition) \r\n\t     then asCodeForBuiltIn()\r\n\t else if baseDefinition.oclIsKindOf(mcore::expressions::MContainerBaseDefinition) \r\n\t     then asCodeForOthers()\r\n\t else if (baseDefinition.oclIsKindOf(mcore::expressions::MSimpleTypeConstantBaseDefinition) or baseDefinition.oclIsKindOf(mcore::expressions::MLiteralConstantBaseDefinition)) \r\n\t     then asCodeForConstants()\r\n\t else if baseDefinition.oclIsKindOf(mcore::expressions::MVariableBaseDefinition) \r\n\t     then asCodeForVariables()\r\n\t     else asCodeForOthers() endif endif endif endif in\r\nlet res: String = if calculatedSingular then \'null\' else \'OrderedSet{}\' endif in\r\nlet chainTypeString: String = \r\n     typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, chainCalculatedSingular) in\r\nlet chainTypeStringSingular: String = \r\n     typeAsOcl(selfObjectPackage, chainCalculatedType, chainCalculatedSimpleType, true) in\r\n--if (castType.oclIsUndefined() or chainCalculatedType.oclIsUndefined())\r\n\t--then \r\nif self.typeMismatch or not(self.castType.oclIsUndefined())-- and self.containedCollector.oclIsUndefined() and\r\n\t --(if self.castType.oclIsUndefined() then true else castType.allSubTypes()->excludes(chainCalculatedType)endif)\r\n     then self.autoCastWithProc()\r\nelse if self.processor=mcore::expressions::MProcessor::None \r\n     then code \r\nelse if self.isPrefixProcessor() or self.isPostfixProcessor() \r\n     then if self.processor = mcore::expressions::MProcessor::Not \r\n                  then let businessLog : String = \' \'  in\r\n\t\t                  businessLog.concat(\'if (\').concat(code).concat(\')= true \\n then false \\n else if (\').concat(code).concat(\')= false \\n then true \\n else null endif endif \\n \')\r\n\t\t     else if self.processor = mcore::expressions::MProcessor::OneDividedBy \r\n\t\t           then let businessLog : String = \' \'  in\r\n\t\t                   businessLog.concat(\'(1 / (\').concat(code).concat(\'))\')\r\n\t\t     else if self.isPostfixProcessor() \r\n\t\t            then let businessLog : String = \' \'  in\r\n\t\t                    businessLog.concat(code).concat(self.procAsCode())\r\n\t\t            else code endif endif endif\r\n     else let variableName : String = \r\n             self.uniqueChainNumber() in\r\n\t\t    \'let \'.concat(variableName).concat(\': \').concat(chainTypeString).concat(\' = \').concat(\r\n\t\t    code).concat(\' in\\n\').concat(    -- Accessing feature is not unary:\r\n            \'if \').concat(variableName).concat(\r\n            if self.isOwnXOCLOperator() \r\n                 then \'.oclIsUndefined() or \'.concat(variableName) else \'\'endif).concat(\r\n            if self.isProcessorSetOperator() \r\n                 then \'->\'\r\n            else if not(self.isProcessorCheckEqualOperator()) \r\n                 then \'.\' else \'\' endif endif).concat(\r\n            if self.isCustomCodeProcessor() \r\n                 then \'isEmpty()\' \r\n                 else self.procAsCode() endif).concat\r\n            (\r\n            if self.isProcessorCheckEqualOperator() \r\n\t\t         then \' then true else false \' \r\n\t\t    else           \r\n\t\t            if self.isCustomCodeProcessor()  \r\n\t\t                 then \'\' \r\n\t\t                 else if not(self.processorReturnsSingular())  then \'->\' else \'.\' endif.concat(\r\n\t\t                        \'oclIsUndefined() \\n \') endif.concat( \r\n\t\t            \'then null \\n else \').concat(-- todo   processor does not influence calculatedSingular\r\n\t\t            variableName).concat(\r\n\t\t            if self.isProcessorSetOperator() then \'->\' else \'.\' endif).concat\r\n\t\t            ( \r\n\t\t            --  \"->\" for Set , \".\" for unary ,  nothing if we check for a value\r\n\t\t            if self.isCustomCodeProcessor() \r\n\t\t               then let checkPart : String = if  processor= mcore::expressions::MProcessor::And then \'false\' else \'true\' endif in\r\n                               let elsePart : String = if processor= mcore::expressions::MProcessor::And then \'true\' else \'false\' endif in\r\n                               let iterateBase: String = \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n                               if self.processor = mcore::expressions::MProcessor::And or processor = mcore::expressions::MProcessor::Or \r\n                                    then iterateBase.concat(chainTypeStringSingular).concat(\r\n                                            \'= \').concat(elsePart).concat(\r\n                                           \'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\').concat(\r\n                                            \'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\').concat(\r\n                                            \' else if x =null then null \\n\').concat(\'else if s =null then null\').concat(\r\n                                            \' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n                                   else iterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\').concat(\r\n                                           --CHANGE ->first and ->last \r\n                                           --.concat(code).concat(if processor=MProcessor::Head then \'->first() \' else \'->last() \' endif).concat(\'then s else s->including(x)->asOrderedSet() endif endif)\'))\r\n                                           code).concat(if processor=mcore::expressions::MProcessor::Head then \'->last() \' else \'->first() \' endif).concat(\r\n                                           \'then s else s->including(x)->asOrderedSet() endif endif)\') endif\r\n                      else self.procAsCode() endif\r\n                      ) endif\r\n              ).concat( \r\n             \'\\n  endif\') endif endif endif\r\n" });
		addAnnotation(getMBaseChain_IsOwnXOCLOp(), source, new String[] {
				"derive",
				"processor = expressions::MProcessor::CamelCaseLower or\r\nprocessor = expressions::MProcessor::CamelCaseToBusiness or\r\nprocessor = mcore::expressions::MProcessor::CamelCaseUpper" });
		addAnnotation(mProcessorDefinitionEClass, source, new String[] {
				"label",
				"if self.processor.oclIsUndefined() \n      then \'\'\n      else self.processor.toString()\n         endif" });
		addAnnotation(getMChain__DoAction$Update__MChainAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMChain_DoAction(), source, new String[] { "derive",
				"mcore::expressions::MChainAction::Do\n" });
		addAnnotation(getMCallArgument_First(), source, new String[] { "derive",
				"if self.call.oclIsUndefined() \r\n  then false\r\n  else self.call.callArgument->first()=self endif" });
		addAnnotation(getMCallArgument_Last(), source, new String[] { "derive",
				"if self.call.oclIsUndefined() \r\n  then false\r\n  else self.call.callArgument->last()=self endif" });
		addAnnotation(mSubChainEClass, source, new String[] { "label",
				"\'   ...   \'.concat(\r\nlet c:String=self.chainAsCode in\r\nif c=\'\' then \'\' else \'.\'.concat(c) endif\r\n)" });
		addAnnotation(getMSubChain__DoAction$Update__MSubChainAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMSubChain__IsLastSubchain(), source, new String[] {
				"body",
				"self.eContainer().oclAsType(MBaseChain).subExpression->last() = self" });
		addAnnotation(getMSubChain__UniqueSubchainName(), source, new String[] {
				"body",
				"\r\nlet chainNumber:String = \r\n(self.eContainer().oclAsType(MBaseChain).subExpression->size()+1 - self.eContainer().oclAsType(MBaseChain).subExpression->indexOf(self)).toString() in\r\nlet vName: String = \'sub\'.concat(if self.eContainer().oclAsType(MBaseChain).uniqueChainNumber().oclIsUndefined() then \'\' else self.eContainer().oclAsType(MBaseChain).uniqueChainNumber() endif).concat(chainNumber) in \r\nvName" });
		addAnnotation(getMSubChain__ReturnSingularPureChain(), source,
				new String[] { "body",
						"let previousSingular: Boolean =  \r\nif self.previousExpression.oclAsType(MAbstractChain).processor <> MProcessor::None then self.previousExpression.oclAsType(MAbstractChain).processorReturnsSingular() else self.previousExpression.calculatedSingular endif in\r\n\r\nlet s1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedSingular endif in\r\nlet s2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedSingular endif in\r\nlet s3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedSingular endif in\r\n\r\npreviousSingular and s1 and s2 and s3 \r\n" });
		addAnnotation(getMSubChain__AsCodeForOthers(), source, new String[] {
				"body",
				"\r\n if length()=1 then codeForLength1() \r\nelse if length()=2 then codeForLength2() \r\nelse if length()=3 then codeForLength3() \r\nelse self.uniqueSubchainName() endif endif endif " });
		addAnnotation(getMSubChain__CodeForLength1(), source, new String[] {
				"body",
				"let chain: MSubChain = self.oclAsType(MSubChain) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and \r\nchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None) in\r\n\r\n\r\nlet b: String =  self.oclAsType(MSubChain).uniqueSubchainName().concat(\'.\') in \r\nb.concat(unsafeChainStepAsCode(1)).concat(if self.oclAsType(MSubChain).chainCalculatedSingular then \'\' else\r\nif noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\'endif.concat(\'->asOrderedSet()\') endif)" });
		addAnnotation(getMSubChain__CodeForLength2(), source, new String[] {
				"body",
				"\r\nlet chain: MSubChain = self.oclAsType(MSubChain) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and \r\nchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)\r\n\r\nin\r\n\r\nlet b: String = self.oclAsType(MSubChain).uniqueSubchainName().concat(\'.\') in \r\n\r\nif  chain.previousExpression.calculatedOwnSingular then\r\n\r\nif element1.calculatedSingular and element2.calculatedSingular then\r\n  let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse  if (not element1.calculatedSingular) and element2.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse  if (not element1.calculatedSingular) and (not element2.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->asOrderedSet()\')\r\n\r\n\r\nelse null\r\n\r\nendif  endif endif endif\r\n\r\nelse\r\n\r\n  b.concat(unsafeChainAsCode(1,1)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(2,2)).concat(if noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\' endif.concat(\r\n  \'->asOrderedSet()\'))\r\n\r\nendif" });
		addAnnotation(getMSubChain__CodeForLength3(), source, new String[] {
				"body",
				"let subchain: MSubChain = self.oclAsType(MSubChain ) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (subchain.nextExpression.oclIsUndefined()) and \r\nsubchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and subchain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)\r\nin\r\nlet abstract:  MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in\r\n\r\n\r\nlet b: String = subchain.uniqueSubchainName().concat(\'.\')  in \r\n\r\nif  subchain.previousExpression.calculatedOwnSingular then\r\n\r\n\r\nif element1.calculatedSingular and element2.calculatedSingular and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(if subchain.nextExpression.oclIsUndefined() then \'\' else \'->asOrderedSet()\' endif).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->asOrderedSet()\\nendif\')\r\n\r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(--\'->reject(oclIsUndefined()) \r\n  \'->asOrderedSet()\')\r\n  \r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if subchain.nextExpression.oclIsUndefined() then \'\' else \'->asOrderedSet()\' endif)\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\')\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\')\r\n\r\nelse null\r\nendif endif endif endif endif endif endif endif \r\n\r\nelse\r\n\r\n  b.concat(unsafeChainAsCode(1,1)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(2,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\' endif.concat(\r\n  \'->asOrderedSet()\'))\r\n\r\nendif" });
		addAnnotation(getMSubChain_PreviousExpression(), source, new String[] {
				"derive",
				"if containingExpression.oclIsUndefined() then null\r\nelse if not containingExpression.oclIsKindOf(MBaseChain) then null\r\nelse let chain: MBaseChain = containingExpression.oclAsType(MBaseChain) in\r\n\tlet pos: Integer = chain.subExpression->indexOf(self) in \r\n\r\n\tif pos=1\tthen chain.oclAsType(MAbstractExpression)\r\n\telse chain.subExpression->at(pos-1).oclAsType(MAbstractExpression) endif\r\nendif endif" });
		addAnnotation(getMSubChain_NextExpression(), source, new String[] {
				"derive",
				" let chain: MBaseChain = eContainer().oclAsType(MBaseChain) in\r\n\tlet pos: Integer = chain.subExpression->indexOf(self) in \r\n\r\n\t if chain.subExpression->at(pos+1).oclIsUndefined() then null\r\n else chain.subExpression->at(pos+1).oclAsType(MAbstractExpression)\r\n endif" });
		addAnnotation(getMSubChain_DoAction(), source, new String[] { "derive",
				"mcore::expressions::MSubChainAction::Do\n" });
		addAnnotation(getMDataValueExpr_SimpleType(), source,
				new String[] { "initValue", "SimpleType::String" });
		addAnnotation(getMLiteralValueExpr__GetAllowedLiterals(), source,
				new String[] { "body",
						"if self.enumerationType.oclIsUndefined() then OrderedSet{}\r\nelse if self.enumerationType.kind = ClassifierKind::Enumeration then enumerationType.allLiterals() else OrderedSet{} endif endif" });
		addAnnotation(getMLiteralValueExpr_EnumerationType(), source,
				new String[] { "choiceConstraint",
						"trg.allLiterals()->size() > 0" });
		addAnnotation(getMLiteralValueExpr_Value(), source,
				new String[] { "choiceConstruction", "getAllowedLiterals()" });
		addAnnotation(mIfEClass, source, new String[] { "label",
				"let c: String = if condition.oclIsUndefined() then \'MISSING CONDITION\' \r\n  else condition.getShortCode() endif in\r\n    \r\n\'if \'.concat(c)" });
		addAnnotation(getMIf__DefaultValue(), source, new String[] { "body",
				"Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMIf_ContainedCollector(), source,
				new String[] { "initValue", "null" });
		addAnnotation(mThenEClass, source, new String[] { "label",
				"\'then \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse expression.getShortCode() endif\r\n)" });
		addAnnotation(mElseIfEClass, source, new String[] { "label",
				"\'else if \'.concat(\r\n\tif condition.oclIsUndefined() then \'MISSING CONDITION\'\r\n\telse condition.getShortCode() endif\r\n).concat(\' \').concat(\r\n\tif thenPart.oclIsUndefined() then \'MISSING THEN\'\r\n\telse thenPart.getShortCode() endif\r\n)" });
		addAnnotation(getMElseIf__DefaultValue(), source, new String[] { "body",
				"Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMElseIf_Condition(), source,
				new String[] { "initValue", "defaultValue()" });
		addAnnotation(getMElseIf_ThenPart(), source, new String[] { "initValue",
				"Tuple{expression = defaultValue()}" });
		addAnnotation(mElseEClass, source, new String[] { "label",
				"\'else \'.concat(\r\n\tif expression.oclIsUndefined() then \'MISSING EXPRESSION\'\r\n\telse expression.getShortCode() endif\r\n)" });
		addAnnotation(mCollectionExpressionEClass, source, new String[] {
				"label", "collectionOperatorAsCode(self.collectionOperator)" });
		addAnnotation(
				getMCollectionExpression__CollectionOperatorAsCode__CollectionOperator(),
				source, new String[] { "body",
						"if operator= CollectionOperator::Collect then \'collect\' else\r\nif operator= CollectionOperator::Select then \'select\' else\r\nif operator= CollectionOperator::Iterate then \'iterate\' else\r\nif operator= CollectionOperator::Closure then \'closure\' else\r\n\'ERROR\' \r\nendif endif endif endif" });
		addAnnotation(getMCollectionExpression__CollectionOperatorAsCode(),
				source, new String[] { "body",
						"collectionOperatorAsCode(collectionOperator)" });
		addAnnotation(getMCollectionExpression__DefaultValue(), source,
				new String[] { "body",
						"Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMCollectionExpression_Collection(), source,
				new String[] { "derive",
						"if eContainer().oclIsKindOf(MAbstractExpression)\r\n  then eContainer().oclAsType(MAbstractExpression)\r\n  else null \r\nendif" });
		addAnnotation(getMCollectionExpression_IteratorVar(), source,
				new String[] { "initValue", "Tuple{name=\'it\'}" });
		addAnnotation(getMCollectionExpression_Expression(), source,
				new String[] { "initValue", "defaultValue()" });
		addAnnotation(getMCollectionVar_ContainingCollection(), source,
				new String[] { "derive",
						"if eContainer().oclIsKindOf(MCollectionExpression)\r\nthen eContainer().oclAsType(MCollectionExpression)\r\nelse null endif" });
		addAnnotation(mIteratorEClass, source,
				new String[] { "label", "eName" });
		addAnnotation(mAccumulatorEClass, source,
				new String[] { "label", "eName" });
		addAnnotation(getMAccumulator_SimpleType(), source,
				new String[] { "initValue", "SimpleType::None" });
		addAnnotation(getMAccumulator_Mandatory(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMAccumulator_Singular(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMNamedConstant__DefaultValue(), source, new String[] {
				"body", "Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMConstantLet__AsSetString__String_String_String(),
				source, new String[] { "body",
						"let s1: String = if p1.oclIsUndefined() then \'\' else if p1=\'\' then \'\' else p1 endif endif in\r\nlet s2: String = if p2.oclIsUndefined() then \'\' else if p2=\'\' then \'\' else p2 endif endif in\r\nlet s3: String = if p3.oclIsUndefined() then \'\' else if p3=\'\' then \'\' else p3 endif endif in\r\n\r\n\'OrderedSet {\'.concat(\r\n\tif s1=\'\'\r\n\tthen \r\n\t\tif s2=\'\'\r\n\t\tthen \r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse s3\r\n\t\t\tendif\r\n\t\telse s2.concat (\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\t\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif)\r\n\t\tendif\r\n\telse s1.concat (\r\n\t\tif s2=\'\'\r\n\t\tthen\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif\r\n\t\telse \', \'.concat(s2).concat (\r\n\t\t\tif s3=\'\'\r\n\t\t\tthen \'\'\r\n\t\t\telse \', \'.concat(s3)\r\n\t\t\tendif)\r\n\t\tendif)\r\n\tendif\r\n).concat(\'}\')" });
		addAnnotation(getMConstantLet__InQuotes__String(), source,
				new String[] { "body",
						"if p1.oclIsUndefined() then null\r\nelse \'\\\'\'.concat(p1).concat(\'\\\'\') endif" });
		addAnnotation(mSimpleTypeConstantLetEClass, source,
				new String[] { "label", "asCode" });
		addAnnotation(getMSimpleTypeConstantLet_SimpleType(), source,
				new String[] { "initValue", "SimpleType::String",
						"choiceConstruction",
						"OrderedSet{SimpleType::Boolean, SimpleType::Double, SimpleType::Integer, SimpleType::String}" });
		addAnnotation(mLiteralLetEClass, source,
				new String[] { "label", "asCode" });
		addAnnotation(getMLiteralLet__GetAllowedLiterals(), source,
				new String[] { "body",
						"if self.enumerationType.oclIsUndefined() then OrderedSet{}\r\nelse if self.enumerationType.kind = ClassifierKind::Enumeration then enumerationType.allLiterals() else OrderedSet{} endif endif" });
		addAnnotation(getMLiteralLet_EnumerationType(), source, new String[] {
				"choiceConstraint", "trg.allLiterals()->size() > 0" });
		addAnnotation(getMLiteralLet_Constant1(), source,
				new String[] { "choiceConstruction", "getAllowedLiterals()" });
		addAnnotation(getMLiteralLet_Constant2(), source,
				new String[] { "choiceConstruction", "getAllowedLiterals()" });
		addAnnotation(getMLiteralLet_Constant3(), source,
				new String[] { "choiceConstruction", "getAllowedLiterals()" });
		addAnnotation(mObjectReferenceLetEClass, source,
				new String[] { "label", "asCode" });
		addAnnotation(getMObjectReferenceLet_Constant1(), source, new String[] {
				"choiceConstraint",
				"if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif" });
		addAnnotation(getMObjectReferenceLet_Constant2(), source, new String[] {
				"choiceConstraint",
				"if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif" });
		addAnnotation(getMObjectReferenceLet_Constant3(), source, new String[] {
				"choiceConstraint",
				"if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif" });
		addAnnotation(mApplicationEClass, source, new String[] { "label",
				"if operands->forAll(oclIsKindOf(MChain))\r\nthen /* the operand applies only to chains, we show the entire text */\r\n\tasCode\r\nelse /* we show only the operator */\r\n\toperatorAsCode()\r\nendif" });
		addAnnotation(getMApplication__DoAction$Update__MApplicationAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMApplication__OperatorAsCode(), source, new String[] {
				"body",
				"if self.operator=MOperator::Equal then \'=\' \r\nelse if self.operator=MOperator::NotEqual then \'<>\'\r\nelse if self.operator=MOperator::Greater then \'>\'\r\nelse if self.operator=MOperator::Smaller then \'<\'\r\nelse if self.operator=MOperator::GreaterEqual then \'>=\'\r\nelse if self.operator=MOperator::SmallerEqual then \'<=\'\r\nelse if self.operator=MOperator::NotOp then \'not\'\r\nelse if self.operator=MOperator::OclAnd then \'and\'\r\nelse if self.operator=MOperator::OclOr then \'or\'\r\nelse if self.operator=MOperator::XorOp then \'xor\'\r\nelse if self.operator=MOperator::ImpliesOp then \'implies\'\r\nelse if self.operator=MOperator::Plus then \'+\'\r\nelse if self.operator=MOperator::Minus then \'-\'\r\nelse if self .operator= MOperator::Times then \'*\'\r\nelse if self .operator= MOperator::Divide then \'/\'\r\nelse if self .operator= MOperator::Div then \'div\'\r\nelse if self .operator= MOperator::Mod then \'mod\'\r\nelse if self .operator= MOperator::Concat then \'concat\'\r\nelse if self .operator= MOperator::OclIsInvalid then \'oclIsInvalid\'\r\nelse if self .operator= MOperator::OclIsUndefined then \'oclIsUndefined\'\r\nelse if self .operator= MOperator::SetFirst then \'first\'\r\nelse if self .operator= MOperator::SetIsEmpty then \'isEmpty\'\r\nelse if self .operator= MOperator::SetLast then \'last\'\r\nelse if self .operator= MOperator::SetNotEmpty then \'notEmpty\'\r\nelse if self .operator= MOperator::SetSize then \'size\'\r\nelse if self .operator= MOperator::SetSum then \'sum\'\r\nelse if self .operator= MOperator::SetIncluding then \'including\'\r\n\r\nelse if self .operator= MOperator::SetIncludesAll then \'includesAll\'\r\nelse if self .operator= MOperator::SetExcludesAll then \'excludesAll\'\r\nelse if self .operator= MOperator::Intersection then \'intersection\'\r\nelse if (operator = MOperator::DiffInDays) then \'diffInDays\'\r\n else if (operator = MOperator::DiffInHrs)   then \'diffInHours\'\r\n else if  (operator = MOperator::DiffInMinutes) then \'diffInMinutes\'\r\n else if  (operator = MOperator::DiffInMonth) then \'diffInMonths\'\r\n else if   (operator = MOperator::DiffInSeconds) then \'diffInSeconds\'\r\nelse if   (operator = MOperator::DiffInYears) then \'diffInYears\'\r\n\r\nelse if self .operator= MOperator::SetExcluding then \'excluding\'\r\nelse if self .operator= MOperator::SetUnion then \'union\'\r\nelse if self .operator= MOperator::SetIncludes then \'includes\'\r\nelse if self .operator= MOperator::SetExcludes then \'excludes\'\r\nelse if self .operator= MOperator::IndexOf then \'indexOf\'\r\nelse if self .operator= MOperator::At then \'at\'\r\nelse if self.operator=MOperator::SetAsOrderedSet then \'asOrderedSet\'\r\nelse if self. operator=MOperator::ImpliesOp then \'implies\' \r\nelse if self. operator=MOperator::ToString then \'toString\'\r\nelse if self. operator=MOperator::Trim then \'trim\'\r\nelse if self.operator= MOperator::SubString then \'substring\'\r\n\r\nelse \'ERROR\'  endif endif endif endif endif endif endif endif endif\r\nendif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif" });
		addAnnotation(getMApplication__IsOperatorPrefix(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false \r\nelse (operator=MOperator::NotOp) endif" });
		addAnnotation(getMApplication__IsOperatorInfix(), source, new String[] {
				"body",
				"(not isOperatorPrefix()) and (not isOperatorPostfix()) and (not isOperatorSetOperator())\r\n" });
		addAnnotation(getMApplication__IsOperatorPostfix(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false else\r\n(operator = MOperator::OclIsInvalid) or (operator = MOperator::OclIsUndefined) \r\nor (operator = MOperator::ToString) or (operator = MOperator::Trim) or isOperatorSetOperator() \r\nendif" });
		addAnnotation(getMApplication__IsOperatorUnaryFunction(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false else\r\n (operator = MOperator::Concat) or\r\n (operator = MOperator::DiffInDays) or\r\n (operator = MOperator::DiffInHrs) or\r\n  (operator = MOperator::DiffInMinutes) or\r\n   (operator = MOperator::DiffInMonth) or\r\n    (operator = MOperator::DiffInSeconds) or\r\n     (operator = MOperator::DiffInYears)\r\n   endif" });
		addAnnotation(getMApplication__IsOperatorSetOperator(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false \r\nelse\r\noperator= MOperator::SetFirst or\r\noperator= MOperator::SetIsEmpty or\r\noperator= MOperator::SetLast or\r\noperator= MOperator::SetNotEmpty or\r\noperator= MOperator::SetSize or\r\noperator= MOperator::SetSum or\r\noperator= MOperator::SetIncluding or\r\noperator= MOperator::SetExcluding or\r\noperator= MOperator::SetUnion or\r\noperator= MOperator::SetIncludes or\r\noperator= MOperator::SetExcludes or\r\noperator= MOperator::IndexOf or\r\noperator= MOperator::At or \r\noperator=MOperator::SetAsOrderedSet or\r\noperator=MOperator::SetExcludesAll or\r\noperator=MOperator::SetIncludesAll or\r\noperator=MOperator::Intersection \r\nendif" });
		addAnnotation(getMApplication__DefaultValue(), source, new String[] {
				"body", "Tuple{base=ExpressionBase::SelfObject}" });
		addAnnotation(getMApplication__IsOperatorUnaryFunctionTwoParam(),
				source, new String[] { "body",
						"if operator.oclIsUndefined() then false else\r\n (operator = MOperator::SubString)  endif" });
		addAnnotation(getMApplication__IsOperatorParameterUnary(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false else\r\n (operator = MOperator::SetExcluding)  or \r\n  (operator = MOperator::SetIncluding) or  \r\n  (operator = MOperator::At) or\r\n  (operator = MOperator::IndexOf) or\r\n  (operator = MOperator::SubString) or\r\n  (operator = MOperator::SetExcludes) or\r\n  (operator = MOperator::SetIncludes)\r\n  \r\n  endif" });
		addAnnotation(getMApplication__UniqueApplyNumber(), source,
				new String[] { "body",
						"let chain : Integer =\r\nif self.eContainer().oclAsType(MNamedExpression).oclIsUndefined() then \r\nself.eContainer()->closure(eContainer())->select(oclIsTypeOf(MNamedExpression)).oclAsType(MNamedExpression)->asOrderedSet()->first().allApplications()->indexOf(self)\r\nelse\r\nself.eContainer().oclAsType(MNamedExpression).allApplications()->indexOf(self)\r\nendif\r\nin if chain.oclIsUndefined() then 0 else chain endif" });
		addAnnotation(getMApplication__DefinesOperatorBusinessLogic(), source,
				new String[] { "body",
						"if operator.oclIsUndefined() then false else\r\n (operator = MOperator::AndOp) or\r\n  (operator = MOperator::OrOp) or\r\n    (operator = MOperator::BusinessNOT) or\r\n    (operator = MOperator::BusinessImplies)\r\n  endif" });
		addAnnotation(getMApplication__CodeForLogicOperator(), source,
				new String[] { "body",
						"if operands->first().oclIsUndefined() then \' \'\r\n\r\nelse\r\n\r\nlet frst: MChainOrApplication =  operands->first() in\r\nlet frstCode: String = (if frst.isComplexExpression then \'(\' else \'\' endif).concat(frst.asCode).concat(if frst.isComplexExpression then \')\' else \'\' endif) in\r\nlet businessExpr : String = \'\' in\r\nif operator= MOperator::AndOp or operator = MOperator::OrOp\r\nthen\r\n\r\nlet checkPart : String = if  operator= MOperator::AndOp then \'false\' else \'true\' endif\r\nin\r\nlet elsePart : String = if operator = MOperator::AndOp then \'true\' else \'false\' endif in\r\nbusinessExpr.concat(\'if (\').concat(frstCode).concat(\')= \').concat(checkPart).concat(\' \\n then \').concat(checkPart).concat(\' \\n\').concat(operands->excluding(frst)->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\' else if (\').concat(x.asCode).concat(\')= \' ).concat(checkPart).concat(\' \\n\').concat(\' then \').concat(checkPart).concat(\' \\n\'))).concat(\'else if (\')\r\n.concat(operands->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\'(\').concat(x.asCode).concat(\')= null\').concat(if operands->at(operands->indexOf(x)+1).oclIsUndefined()  then \'\' else \' or \' endif))).concat(\') = true \\n then null \\n else \').concat(elsePart).concat(\' \')\r\n.concat(operands->iterate(x: MChainOrApplication; s:String = \'\' | s.concat(\'endif \'))).concat(\'endif\')\r\n\r\nelse if operator = MOperator::BusinessNOT\t\r\nthen\r\n\r\nbusinessExpr.concat(\'if (\').concat(frst.asCode).concat(\')= true \\n then false \\n else if (\').concat(frst.asCode).concat(\')= false \\n then true \\n else null endif endif \\n \')\r\n\t\t \r\nelse \r\n\'\'\r\n\r\nendif\r\nendif\r\nendif" });
		addAnnotation(getMApplication__GetCalculatedSimpleDifferentTypes(),
				source, new String[] { "body",
						"\tlet x: OrderedSet(SimpleType) = self.operands->collect(calculatedSimpleType)->asOrderedSet() in \r\n\t\r\n\tif x->oclIsUndefined() then SimpleType::None \r\n\telse if x->includesAll(OrderedSet{SimpleType::Integer,SimpleType::Double}) then SimpleType::Double\r\n\t--else if x->size() > 1 then SimpleType::None\r\n\telse\r\n\tx->first() endif endif--endif" });
		addAnnotation(getMApplication_ContainedCollector(), source,
				new String[] { "initValue", "null" });
		addAnnotation(getMApplication_DoAction(), source, new String[] {
				"derive", "mcore::expressions::MApplicationAction::Do\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getMAbstractExpression__GetScope(), source, new String[] {
				"propertyCategory", "Base Scope", "createColumn", "true" });
		addAnnotation(getMAbstractExpression_EntireScope(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeBase(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeSelf(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeTrg(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeObj(), source, new String[] {
				"createColumn", "true", "propertyCategory", "Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeSimpleTypeConstants(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeLiteralConstants(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeObjectReferenceConstants(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeVariables(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeIterator(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeAccumulator(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeParameters(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeContainerBase(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeNumberBase(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_ScopeSource(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Base Scope" });
		addAnnotation(getMAbstractExpression_LocalEntireScope(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeBase(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeSelf(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeTrg(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeObj(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeSource(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeSimpleTypeConstants(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeLiteralConstants(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Base Scope/Local Base Scope" });
		addAnnotation(
				getMAbstractExpression_LocalScopeObjectReferenceConstants(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeVariables(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeIterator(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeAccumulator(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeParameters(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeNumberBaseDefinition(),
				source, new String[] { "createColumn", "true",
						"propertyCategory", "Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_LocalScopeContainer(), source,
				new String[] { "createColumn", "true", "propertyCategory",
						"Base Scope/Local Base Scope" });
		addAnnotation(getMAbstractExpression_ContainingAnnotation(), source,
				new String[] { "propertyCategory", "Structural", "createColumn",
						"false" });
		addAnnotation(getMAbstractExpression_ContainingExpression(), source,
				new String[] { "propertyCategory", "Structural", "createColumn",
						"false" });
		addAnnotation(getMAbstractExpression_IsComplexExpression(), source,
				new String[] { "propertyCategory", "Structural", "createColumn",
						"false" });
		addAnnotation(getMAbstractExpression_IsSubExpression(), source,
				new String[] { "propertyCategory", "Structural", "createColumn",
						"false" });
		addAnnotation(getMAbstractExpression_CalculatedOwnType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_CalculatedOwnMandatory(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_CalculatedOwnSingular(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_CalculatedOwnSimpleType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_SelfObjectType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_SelfObjectPackage(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_TargetObjectType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_TargetSimpleType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_ObjectObjectType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_ExpectedReturnType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_ExpectedReturnSimpleType(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_IsReturnValueMandatory(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_IsReturnValueSingular(), source,
				new String[] { "propertyCategory", "Type Information",
						"createColumn", "false" });
		addAnnotation(getMAbstractExpression_SrcObjectType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMAbstractExpression_SrcObjectSimpleType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMAbstractExpressionWithBase_BaseAsCode(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_Base(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseDefinition(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitType(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSimpleType(),
				source, new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitMandatory(),
				source, new String[] { "createColumn", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSingular(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMNamedExpression_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMAbstractChain__ProcAsCode(), source, new String[] {
				"propertyCategory", "Processor", "createColumn", "true" });
		addAnnotation(getMAbstractChain__IsCustomCodeProcessor(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMAbstractChain__IsProcessorSetOperator(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMAbstractChain__IsOwnXOCLOperator(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMAbstractChain__ProcessorReturnsSingular(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMAbstractChain__ProcessorIsSet(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMAbstractChain__CreateProcessorDefinition(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObject(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObjects(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBoolean(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBooleans(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForInteger(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForIntegers(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReal(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReals(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForString(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForStrings(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDate(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDates(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "true" });
		addAnnotation(getMAbstractChain_LastElement(), source,
				new String[] { "createColumn", "true" });
		addAnnotation(getMAbstractChain_Processor(), source, new String[] {
				"propertyCategory", "Processor", "createColumn", "false" });
		addAnnotation(getMAbstractChain_ProcessorDefinition(), source,
				new String[] { "propertyCategory", "Processor/Definition",
						"createColumn", "false" });
		addAnnotation(getMBaseChain__IsProcessorCheckEqualOperator(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMBaseChain__IsPrefixProcessor(), source, new String[] {
				"propertyCategory", "Processor", "createColumn", "true" });
		addAnnotation(getMBaseChain__IsPostfixProcessor(), source,
				new String[] { "propertyCategory", "Processor", "createColumn",
						"true" });
		addAnnotation(getMBaseChain_IsOwnXOCLOp(), source, new String[] {
				"propertyCategory", "Processor", "createColumn", "false" });
		addAnnotation(getMChain_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMSubChain_PreviousExpression(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMSubChain_NextExpression(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMSubChain_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMAccumulator_SimpleType(), source, new String[] {
				"propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMAccumulator_Type(), source, new String[] {
				"propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMAccumulator_Mandatory(), source, new String[] {
				"propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMAccumulator_Singular(), source, new String[] {
				"propertyCategory", "Typing", "createColumn", "false" });
		addAnnotation(getMApplication_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(getMAbstractExpression_ScopeContainerBase(), source,
				new String[] { "mName", "scopeContainerBase" });
		addAnnotation(getMAbstractExpression_ScopeNumberBase(), source,
				new String[] { "mName", "scopeNumberBase" });
		addAnnotation(getMAbstractExpression_ScopeSource(), source,
				new String[] { "mName", "scopeSource" });
		addAnnotation(getMAbstractExpression_LocalScopeNumberBaseDefinition(),
				source,
				new String[] { "mName", "Local Scope NumberBase Definition" });
		addAnnotation(getMAbstractExpression_ExpectedReturnType(), source,
				new String[] { "mName", "Expected ReturnType" });
		addAnnotation(getMAbstractExpression_SrcObjectSimpleType(), source,
				new String[] { "mName", "src Object SimpleType" });
		addAnnotation(expressionBaseEEnum.getELiterals().get(20), source,
				new String[] { "mName", "EContainer" });
		addAnnotation(mContainerBaseDefinitionEClass, source,
				new String[] { "mName", "MContainerBaseDefinition" });
		addAnnotation(
				getMNumberBaseDefinition__CalculateAsCode__MAbstractExpression(),
				source, new String[] { "mName", "calculateAsCode" });
		addAnnotation(
				(getMNumberBaseDefinition__CalculateAsCode__MAbstractExpression())
						.getEParameters().get(0),
				source, new String[] { "mName", "caller" });
		addAnnotation(mTargetBaseDefinitionEClass, source,
				new String[] { "mName", "MTargetBaseDefinition" });
		addAnnotation(mSourceBaseDefinitionEClass, source,
				new String[] { "mName", "M SourceBaseDefinition" });
		addAnnotation(getMIteratorBaseDefinition_Iterator(), source,
				new String[] { "mName", "iterator" });
		addAnnotation(getMAccumulatorBaseDefinition_Accumulator(), source,
				new String[] { "mName", "accumulator" });
		addAnnotation(mTupleEntryEClass, source,
				new String[] { "mName", "MTupleEntry" });
		addAnnotation(mNamedExpressionEClass, source,
				new String[] { "mName", "MNamedExpression" });
		addAnnotation(getMNamedExpression__AllApplications(), source,
				new String[] { "mName", "allApplications" });
		addAnnotation(
				(getMAbstractChain__UnsafeElementAsCode__Integer())
						.getEParameters().get(0),
				source, new String[] { "mName", "step" });
		addAnnotation(getMAbstractChain__CodeForLength1(), source,
				new String[] { "mName", "Code For Length1" });
		addAnnotation(getMAbstractChain__CodeForLength2(), source,
				new String[] { "mName", "Code For Length2" });
		addAnnotation(getMAbstractChain__CodeForLength3(), source,
				new String[] { "mName", "Code For Length3" });
		addAnnotation(getMAbstractChain__ProcAsCode(), source,
				new String[] { "mName", "procAsCode" });
		addAnnotation(getMAbstractChain__IsCustomCodeProcessor(), source,
				new String[] { "mName", "Is CustomCode Processor" });
		addAnnotation(getMAbstractChain__IsOwnXOCLOperator(), source,
				new String[] { "mName", "IsOwnXOCLOperator" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObject(), source,
				new String[] { "mShortName", "Proc Def Choices For Object",
						"mName",
						"Processor Definition Choices For Singular Object" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForObjects(), source,
				new String[] { "mShortName", "Proc Def Choices For Objects",
						"mName", "Processor Definition Choices For Objects" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBoolean(), source,
				new String[] { "mShortName", "Proc Def Choices For Boolean",
						"mName", "Processor Definition Choices For Boolean" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForBooleans(), source,
				new String[] { "mShortName", "Proc Def Choices For Booleans",
						"mName", "Processor Definition Choices For Booleans" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForInteger(), source,
				new String[] { "mShortName", "Proc Def Choices For Integer",
						"mName", "Processor Definition Choices For Integer" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForIntegers(), source,
				new String[] { "mShortName", "Proc Def Choices For Integers",
						"mName", "Processor Definition Choices For Integers" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReal(), source,
				new String[] { "mShortName", "Proc Def Choices For Real",
						"mName", "Processor Definition Choices For Real" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForReals(), source,
				new String[] { "mShortName", "Proc Def Choices For Reals",
						"mName", "Processor Definition Choices For Reals" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForString(), source,
				new String[] { "mShortName", "Proc Def Choices For String",
						"mName", "Processor Definition Choices For String" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForStrings(), source,
				new String[] { "mShortName", "Proc Def Choices For Strings",
						"mName", "Processor Definition Choices For Strings" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDate(), source,
				new String[] { "mShortName", "Proc Def Choices For Date",
						"mName", "Processor Definition Choices For Date" });
		addAnnotation(getMAbstractChain__ProcDefChoicesForDates(), source,
				new String[] { "mShortName", "Proc Def Choices For Dates",
						"mName", "Processor Definition Choices For Dates" });
		addAnnotation(getMAbstractChain_ChainCalculatedSimpleType(), source,
				new String[] { "mName", "Chain Calculated SimpleType" });
		addAnnotation(getMBaseChain__AutoCastWithProc(), source,
				new String[] { "mName", "autoCastWithProc" });
		addAnnotation(getMBaseChain__OwnToApplyMismatch(), source,
				new String[] { "mName", "OwnToApplyMismatch" });
		addAnnotation(
				(getMBaseChain__ReuseFromOtherNoMoreUsedChainAsUpdate__MBaseChain())
						.getEParameters().get(0),
				source, new String[] { "mName", "archetype" });
		addAnnotation(getMBaseChain__IsPostfixProcessor(), source,
				new String[] { "mName", "isPostfixProcessor" });
		addAnnotation((getMBaseChain__UnsafeElementAsCode__Integer())
				.getEParameters().get(0), source,
				new String[] { "mName", "step" });
		addAnnotation(getMBaseChain__CodeForLength1(), source,
				new String[] { "mName", "Code For Length1" });
		addAnnotation(getMBaseChain__CodeForLength2(), source,
				new String[] { "mName", "Code For Length2" });
		addAnnotation(getMBaseChain__CodeForLength3(), source,
				new String[] { "mName", "Code For Length3" });
		addAnnotation(getMBaseChain_TypeMismatch(), source,
				new String[] { "mName", "typeMismatch" });
		addAnnotation(getMBaseChain_ChainCodeforSubchains(), source,
				new String[] { "mName", "Chain Code for Subchains" });
		addAnnotation(getMBaseChain_IsOwnXOCLOp(), source,
				new String[] { "mName", "IsOwnXOCLOp" });
		addAnnotation(getMSubChain__IsLastSubchain(), source,
				new String[] { "mName", "is Last Subchain" });
		addAnnotation(getMSubChain__CodeForLength1(), source,
				new String[] { "mName", "Code For Length1" });
		addAnnotation(getMSubChain__CodeForLength2(), source,
				new String[] { "mName", "Code For Length2" });
		addAnnotation(getMSubChain__CodeForLength3(), source,
				new String[] { "mName", "Code For Length3" });
		addAnnotation(getMSubChain_NextExpression(), source,
				new String[] { "mName", "nextExpression" });
		addAnnotation(
				(getMCollectionExpression__CollectionOperatorAsCode__CollectionOperator())
						.getEParameters().get(0),
				source, new String[] { "mName", "operator" });
		addAnnotation(getMCollectionExpression_AccumulatorVar(), source,
				new String[] { "mName", "accumulatorVar" });
		addAnnotation(getMApplication__UniqueApplyNumber(), source,
				new String[] { "mName", "UniqueApplyNumber" });
		addAnnotation(getMApplication__DefinesOperatorBusinessLogic(), source,
				new String[] { "mName", "Defines Operator BusinessLogic" });
		addAnnotation(getMApplication__CodeForLogicOperator(), source,
				new String[] { "mName", "codeForLogicOperator" });
		addAnnotation(getMApplication__GetCalculatedSimpleDifferentTypes(),
				source,
				new String[] { "mName", "getCalculatedSimpleDifferentTypes" });
		addAnnotation(mOperatorEEnum.getELiterals().get(7), source,
				new String[] { "mName", "oclAnd" });
		addAnnotation(mOperatorEEnum.getELiterals().get(8), source,
				new String[] { "mName", "OclOr" });
		addAnnotation(mOperatorEEnum.getELiterals().get(28), source,
				new String[] { "mName", "SubString" });
		addAnnotation(mOperatorEEnum.getELiterals().get(29), source,
				new String[] { "mName", "and Op" });
		addAnnotation(mOperatorEEnum.getELiterals().get(30), source,
				new String[] { "mName", "or Op" });
		addAnnotation(mOperatorEEnum.getELiterals().get(31), source,
				new String[] { "mName", "BusinessNOT" });
		addAnnotation(mOperatorEEnum.getELiterals().get(32), source,
				new String[] { "mName", "BusinessImplies" });
		addAnnotation(mOperatorEEnum.getELiterals().get(36), source,
				new String[] { "mName", "DiffInYears" });
		addAnnotation(mOperatorEEnum.getELiterals().get(37), source,
				new String[] { "mName", "DiffInDays" });
		addAnnotation(mOperatorEEnum.getELiterals().get(38), source,
				new String[] { "mName", "DiffInSeconds" });
		addAnnotation(mOperatorEEnum.getELiterals().get(39), source,
				new String[] { "mName", "DiffInMinutes" });
		addAnnotation(mOperatorEEnum.getELiterals().get(40), source,
				new String[] { "mName", "DiffInMonth" });
		addAnnotation(mOperatorEEnum.getELiterals().get(41), source,
				new String[] { "mName", "DiffInHrs" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMAbstractExpression_EntireScope(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeBase(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeSelf(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeTrg(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeObj(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeSimpleTypeConstants(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeLiteralConstants(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeObjectReferenceConstants(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeVariables(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeIterator(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeAccumulator(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeParameters(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeContainerBase(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeNumberBase(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_ScopeSource(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalEntireScope(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeBase(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeSelf(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeTrg(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeObj(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeSource(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeSimpleTypeConstants(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeLiteralConstants(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(
				getMAbstractExpression_LocalScopeObjectReferenceConstants(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeVariables(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeIterator(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeAccumulator(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeParameters(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeNumberBaseDefinition(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_LocalScopeContainer(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_SrcObjectType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpression_SrcObjectSimpleType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_BaseAsCode(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_Base(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_BaseDefinition(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSimpleType(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitMandatory(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractExpressionWithBase_BaseExitSingular(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNewObjectFeatureValue_Feature(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMNamedExpression_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMAbstractChain_Element1(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMAbstractChain_Element2(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMAbstractChain_Element3(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMAbstractChain_ProcessorDefinition(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMChain_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMSubChain_PreviousExpression(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMSubChain_NextExpression(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMSubChain_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteralValueExpr_Value(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMSimpleTypeConstantLet_SimpleType(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMLiteralLet_Constant1(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMLiteralLet_Constant2(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMLiteralLet_Constant3(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMApplication_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
	}

} //ExpressionsPackageImpl
