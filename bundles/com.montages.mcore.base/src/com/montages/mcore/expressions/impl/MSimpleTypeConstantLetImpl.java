/**
 */
package com.montages.mcore.expressions.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MSimpleTypeConstantLet;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MSimple Type Constant Let</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MSimpleTypeConstantLetImpl#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MSimpleTypeConstantLetImpl#getConstant1 <em>Constant1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MSimpleTypeConstantLetImpl#getConstant2 <em>Constant2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.impl.MSimpleTypeConstantLetImpl#getConstant3 <em>Constant3</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MSimpleTypeConstantLetImpl extends MConstantLetImpl
		implements MSimpleTypeConstantLet, IXoclInitializable {
	/**
	 * The default value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The cached value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected SimpleType simpleType = SIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the Simple Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean simpleTypeESet;

	/**
	 * The default value of the '{@link #getConstant1() <em>Constant1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant1()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSTANT1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConstant1() <em>Constant1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant1()
	 * @generated
	 * @ordered
	 */
	protected String constant1 = CONSTANT1_EDEFAULT;

	/**
	 * This is true if the Constant1 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant1ESet;

	/**
	 * The default value of the '{@link #getConstant2() <em>Constant2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant2()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSTANT2_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConstant2() <em>Constant2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant2()
	 * @generated
	 * @ordered
	 */
	protected String constant2 = CONSTANT2_EDEFAULT;

	/**
	 * This is true if the Constant2 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant2ESet;

	/**
	 * The default value of the '{@link #getConstant3() <em>Constant3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant3()
	 * @generated
	 * @ordered
	 */
	protected static final String CONSTANT3_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getConstant3() <em>Constant3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstant3()
	 * @generated
	 * @ordered
	 */
	protected String constant3 = CONSTANT3_EDEFAULT;

	/**
	 * This is true if the Constant3 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean constant3ESet;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getSimpleType <em>Simple Type</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression simpleTypeChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsBasicCode <em>As Basic Code</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsBasicCode
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression asBasicCodeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnMandatory <em>Calculated Own Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSingular <em>Calculated Own Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedOwnSimpleType <em>Calculated Own Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedOwnSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedOwnSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MSimpleTypeConstantLetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSimpleType() {
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSimpleType(SimpleType newSimpleType) {
		SimpleType oldSimpleType = simpleType;
		simpleType = newSimpleType == null ? SIMPLE_TYPE_EDEFAULT
				: newSimpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE,
					oldSimpleType, simpleType, !oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSimpleType() {
		SimpleType oldSimpleType = simpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleType = SIMPLE_TYPE_EDEFAULT;
		simpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE,
					oldSimpleType, SIMPLE_TYPE_EDEFAULT, oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSimpleType() {
		return simpleTypeESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Simple Type</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<SimpleType>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL OrderedSet{SimpleType::Boolean, SimpleType::Double, SimpleType::Integer, SimpleType::String}
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<SimpleType> evalSimpleTypeChoiceConstruction(
			List<SimpleType> choice) {
		EClass eClass = ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET;
		if (simpleTypeChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				simpleTypeChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"SimpleTypeChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(simpleTypeChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "SimpleTypeChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<SimpleType> result = new ArrayList<SimpleType>(
					(Collection<SimpleType>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConstant1() {
		return constant1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant1(String newConstant1) {
		String oldConstant1 = constant1;
		constant1 = newConstant1;
		boolean oldConstant1ESet = constant1ESet;
		constant1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1,
					oldConstant1, constant1, !oldConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant1() {
		String oldConstant1 = constant1;
		boolean oldConstant1ESet = constant1ESet;
		constant1 = CONSTANT1_EDEFAULT;
		constant1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1,
					oldConstant1, CONSTANT1_EDEFAULT, oldConstant1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant1() {
		return constant1ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConstant2() {
		return constant2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant2(String newConstant2) {
		String oldConstant2 = constant2;
		constant2 = newConstant2;
		boolean oldConstant2ESet = constant2ESet;
		constant2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2,
					oldConstant2, constant2, !oldConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant2() {
		String oldConstant2 = constant2;
		boolean oldConstant2ESet = constant2ESet;
		constant2 = CONSTANT2_EDEFAULT;
		constant2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2,
					oldConstant2, CONSTANT2_EDEFAULT, oldConstant2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant2() {
		return constant2ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConstant3() {
		return constant3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstant3(String newConstant3) {
		String oldConstant3 = constant3;
		constant3 = newConstant3;
		boolean oldConstant3ESet = constant3ESet;
		constant3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3,
					oldConstant3, constant3, !oldConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetConstant3() {
		String oldConstant3 = constant3;
		boolean oldConstant3ESet = constant3ESet;
		constant3 = CONSTANT3_EDEFAULT;
		constant3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3,
					oldConstant3, CONSTANT3_EDEFAULT, oldConstant3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConstant3() {
		return constant3ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE:
			return getSimpleType();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1:
			return getConstant1();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2:
			return getConstant2();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3:
			return getConstant3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE:
			setSimpleType((SimpleType) newValue);
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1:
			setConstant1((String) newValue);
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2:
			setConstant2((String) newValue);
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3:
			setConstant3((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE:
			unsetSimpleType();
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1:
			unsetConstant1();
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2:
			unsetConstant2();
			return;
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3:
			unsetConstant3();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE:
			return isSetSimpleType();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1:
			return isSetConstant1();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2:
			return isSetConstant2();
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3:
			return isSetConstant3();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (simpleType: ");
		if (simpleTypeESet)
			result.append(simpleType);
		else
			result.append("<unset>");
		result.append(", constant1: ");
		if (constant1ESet)
			result.append(constant1);
		else
			result.append("<unset>");
		result.append(", constant2: ");
		if (constant2ESet)
			result.append(constant2);
		else
			result.append("<unset>");
		result.append(", constant3: ");
		if (constant3ESet)
			result.append(constant3);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL asCode
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Value Constant'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL asBasicCode let a: String = if constant1.oclIsUndefined() then '' else if constant1='' then '' else constant1 endif endif in
	let b: String = if constant2.oclIsUndefined() then '' else if constant2='' then '' else constant2 endif endif in
	let c: String = if constant3.oclIsUndefined() then '' else if constant3='' then '' else constant3 endif endif in
	
	if calculatedSingular
	then if calculatedSimpleType = SimpleType::String
		then inQuotes(a.concat(b).concat(c))
		else a.concat(b).concat(c) endif
	else if calculatedSimpleType = SimpleType::String
		then asSetString(inQuotes(constant1), inQuotes(constant2), inQuotes(constant3))
		else asSetString(constant1, constant2, constant3)
		endif
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getAsBasicCode() {
		EClass eClass = (ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE;

		if (asBasicCodeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				asBasicCodeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asBasicCodeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnMandatory true
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnMandatory() {
		EClass eClass = (ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY;

		if (calculatedOwnMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSingular let a: Integer = if constant1.oclIsUndefined() then 0
	else if constant1='' then 0 else 1 endif
	endif in
	
	let b: Integer = if constant2.oclIsUndefined() then 0
	else if constant2='' then 0 else 1 endif
	endif in
	
	let c: Integer = if constant3.oclIsUndefined() then 0
	else if constant3='' then 0 else 1 endif
	endif in
	
	(a+b+c) <= 1
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedOwnSingular() {
		EClass eClass = (ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR;

		if (calculatedOwnSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedOwnSimpleType simpleType
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedOwnSimpleType() {
		EClass eClass = (ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE;

		if (calculatedOwnSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedOwnSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedOwnSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(ExpressionsPackage.PLUGIN_ID, query,
					ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						ExpressionsPackage.PLUGIN_ID, oclText,
						helper.getProblems(), eClass(), structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MSimpleTypeConstantLetImpl
