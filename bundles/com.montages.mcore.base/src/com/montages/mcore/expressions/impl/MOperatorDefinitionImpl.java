/**
 */
package com.montages.mcore.expressions.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MOperator;
import com.montages.mcore.expressions.MOperatorDefinition;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MOperator Definition</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.impl.MOperatorDefinitionImpl#getMOperator <em>MOperator</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MOperatorDefinitionImpl extends MinimalEObjectImpl.Container
		implements MOperatorDefinition {
	/**
	 * The default value of the '{@link #getMOperator() <em>MOperator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMOperator()
	 * @generated
	 * @ordered
	 */
	protected static final MOperator MOPERATOR_EDEFAULT = MOperator.EQUAL;

	/**
	 * The cached value of the '{@link #getMOperator() <em>MOperator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMOperator()
	 * @generated
	 * @ordered
	 */
	protected MOperator mOperator = MOPERATOR_EDEFAULT;

	/**
	 * This is true if the MOperator attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mOperatorESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MOperatorDefinitionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MOPERATOR_DEFINITION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperator getMOperator() {
		return mOperator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMOperator(MOperator newMOperator) {
		MOperator oldMOperator = mOperator;
		mOperator = newMOperator == null ? MOPERATOR_EDEFAULT : newMOperator;
		boolean oldMOperatorESet = mOperatorESet;
		mOperatorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR,
					oldMOperator, mOperator, !oldMOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMOperator() {
		MOperator oldMOperator = mOperator;
		boolean oldMOperatorESet = mOperatorESet;
		mOperator = MOPERATOR_EDEFAULT;
		mOperatorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR,
					oldMOperator, MOPERATOR_EDEFAULT, oldMOperatorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMOperator() {
		return mOperatorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR:
			return getMOperator();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR:
			setMOperator((MOperator) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR:
			unsetMOperator();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ExpressionsPackage.MOPERATOR_DEFINITION__MOPERATOR:
			return isSetMOperator();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (mOperator: ");
		if (mOperatorESet)
			result.append(mOperator);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MOperatorDefinitionImpl
