/**
 */
package com.montages.mcore.expressions.impl;

import org.eclipse.emf.ecore.EClass;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MToplevelExpression;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MToplevel Expression</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public abstract class MToplevelExpressionImpl extends MAbstractExpressionImpl
		implements MToplevelExpression {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MToplevelExpressionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExpressionsPackage.Literals.MTOPLEVEL_EXPRESSION;
	}

} //MToplevelExpressionImpl
