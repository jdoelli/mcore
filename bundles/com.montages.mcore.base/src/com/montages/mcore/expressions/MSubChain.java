/**
 */
package com.montages.mcore.expressions;

import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSub Chain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MSubChain#getPreviousExpression <em>Previous Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MSubChain#getNextExpression <em>Next Expression</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MSubChain#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSubChain()
 * @model annotation="http://www.xocl.org/OCL label='\'   ...   \'.concat(\r\nlet c:String=self.chainAsCode in\r\nif c=\'\' then \'\' else \'.\'.concat(c) endif\r\n)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'   ...   \'.concat(\r\nlet c:String=self.chainAsCode in\r\nif c=\'\' then \'\' else \'.\'.concat(c) endif\r\n)' chainEntryTypeDerive='self.previousExpression.calculatedOwnType' calculatedOwnSimpleTypeDerive='let p:MProcessor = processor in\r\nif p <> MProcessor::None\r\n  then\r\n     if      p = MProcessor::ToInteger or\r\n             p = MProcessor::Size  or\r\n             p = MProcessor::Round or\r\n             p = MProcessor::Floor or\r\n             p = MProcessor::Year or\r\n             p = MProcessor::Minute or\r\n             p = MProcessor::Month or\r\n             p = MProcessor::Second or\r\n             p = MProcessor::Day or\r\n             p = MProcessor::Hour\r\n       then SimpleType::Integer\r\n     else if p = MProcessor::ToString or  \r\n             p = MProcessor::ToYyyyMmDd or\r\n             p = MProcessor::ToHhMm or\r\n             p = MProcessor::AllLowerCase or\r\n             p = MProcessor::AllUpperCase or\r\n             p = MProcessor::CamelCaseLower or\r\n             p = MProcessor::CamelCaseToBusiness or\r\n             p = MProcessor::CamelCaseUpper or\r\n             p = MProcessor::FirstUpperCase or\r\n             p = MProcessor::Trim\r\n       then SimpleType::String\r\n     else if p = MProcessor::ToReal or\r\n             p = MProcessor::OneDividedBy or\r\n             p = MProcessor::Sum\r\n       then SimpleType::Double\r\n     else if p = MProcessor::ToBoolean or\r\n             p = MProcessor::IsEmpty or\r\n             p = MProcessor::NotEmpty or\r\n             p = MProcessor::Not  or\r\n             p = MProcessor::IsFalse or\r\n             p = MProcessor::IsTrue or\r\n             p = MProcessor::IsZero or\r\n             p = MProcessor::IsOne or\r\n             p = MProcessor::NotNull or\r\n             p = MProcessor::IsNull or\r\n             p = MProcessor::IsInvalid or\r\n             p = MProcessor::And or\r\n             p= MProcessor::Or\r\n       then SimpleType::Boolean\r\n      else if p = MProcessor::ToDate \r\n       then SimpleType::Date\r\n      else if  self.lastElement.oclIsUndefined() \r\n       then  self.previousExpression.calculatedOwnSimpleType\r\n     else self.lastElement.simpleType\r\n      endif endif endif endif endif endif\r\n    else\r\n if\r\n       self.lastElement.oclIsUndefined() then self.previousExpression.calculatedOwnSimpleType\r\n       else self.lastElement.simpleType\r\n       endif endif\r\n\r\n\r\n' calculatedOwnMandatoryDerive='if self.lastElement=null then self.previousExpression.calculatedMandatory else\r\nself.lastElement.mandatory endif' calculatedOwnTypeDerive='if self.calculatedOwnSimpleType = SimpleType::None then\r\n if self.castType.oclIsUndefined() then\r\n  if self.lastElement=null then self.previousExpression.calculatedOwnType else\r\nself.lastElement.type  endif\r\nelse self.castType\r\nendif\r\nelse null endif' calculatedOwnSingularDerive='if self.processorIsSet() \r\n     then self.processorReturnsSingular()\r\n     else self.chainCalculatedSingular endif' asBasicCodeDerive='-- chainType: the type coming out of the chain\r\nlet chainTypeString:String =\r\n     if self.lastElement.oclIsUndefined()  then \r\n       typeAsOcl( self.selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  \r\n     else\r\n       typeAsOcl( self.selfObjectPackage, self.chainCalculatedType, self.chainCalculatedSimpleType, self.chainCalculatedSingular )  endif  in\r\n -- singularVersion of chainType\r\nlet chainTypeStringSingular: String =\r\n\t  if self.lastElement.oclIsUndefined() then \r\n\t\t  typeAsOcl(selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, true)\r\n\t  else\r\n\t\t   typeAsOcl(selfObjectPackage, self.lastElement.calculatedType, self.lastElement.calculatedSimpleType, true) endif in\r\n-- castType: the type to which we cast, if we cast or chainType otherwise\r\nlet castTypeString: String = \r\n      if castType.oclIsUndefined()\r\n         then chainTypeString\r\n         else typeAsOcl(selfObjectPackage, castType, SimpleType::None, self.chainCalculatedSingular) endif in\r\n-- singular version of castType\r\nlet castTypeStringSingular: String = \r\n     if castType.oclIsUndefined()\r\n       then chainTypeStringSingular\r\n       else typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) endif in\r\n--let name for the input to cast\r\nlet castName: String = \r\n     self.uniqueSubchainName().concat(\'cast\') in\t \r\n--let name for the input to proc\r\nlet procName: String = \r\n     self.uniqueSubchainName().concat(\'proc\') in\t \r\n--code for chained properties/operation:\r\nlet code: String = \r\n     self.asCodeForOthers() in\r\n \r\n\' in \\n\'\r\n.concat(if self.processor = MProcessor::None  \r\n     then \'\' \r\n     else \'let \'.concat(procName).concat(\':\').concat(castTypeString).concat(\' = \') endif)\r\n.concat(if self.castType.oclIsUndefined()\r\n     then  \' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \').concat(code).concat( \' endif \\n\')\r\n     else  \'let \'.concat(castName).concat(\':\').concat(chainTypeString).concat(\' =\').concat(\' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \')).concat(code).concat( \' endif \\n\')\r\n              .concat(\' in \\n\')\r\n              .concat(if self.chainCalculatedSingular \r\n                   then \'if \'.concat(castName).concat(\'.oclIsUndefined() then null else \') \r\n                   else \'\' endif)\r\n              .concat(if self.chainCalculatedSingular \r\n                   then \'if \'.concat(castName).concat(\'.oclIsKindOf(\').concat(castTypeStringSingular).concat(\')\\n then \'.concat(castName).concat(\'.oclAsType(\').concat(castTypeStringSingular)\r\n                            .concat(\') else null endif endif \'))\r\n                   else  castName.concat(\'->iterate(i:\').concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular)\r\n                           .concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\')\r\n                           .concat(\')->asOrderedSet() \\n else r endif)\') endif) endif)\r\n .concat(if self.processor = MProcessor::None \r\n      then \'\' \r\n      else \' in \\n if \'.concat(procName)\r\n              .concat(if self.isProcessorSetOperator() \r\n                      then \'->\' \r\n                      else \'.\' endif)\r\n              .concat(if self.isCustomCodeProcessor() \r\n                       then \'isEmpty()\' \r\n                       else procAsCode().concat(\'.oclIsUndefined()\') endif)\r\n               .concat(\' then null \\n else \').concat(procName)\r\n               .concat( if self.isProcessorSetOperator() \r\n                       then \'->\' \r\n                       else \'.\' endif)\r\n               .concat(if self.isCustomCodeProcessor() \r\n                      then   let iterateBase: String = \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n                                if self.processor = MProcessor::And or processor = MProcessor::Or \r\n                                       then let checkPart : String = if  processor= MProcessor::And  then \'false\' else \'true\' endif in \r\n                                               let elsePart : String = if processor= MProcessor::And then \'true\' else \'false\' endif in\r\n                                               iterateBase.concat(chainTypeStringSingular).concat(\'= \').concat(elsePart).concat(\'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\')\r\n                                               .concat(\'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\')\r\n                                               .concat(\' else if x =null then null \\n\'\r\n                                               .concat(\'else if s =null then null\'))\r\n                                               .concat(\' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n                                       else  let elementToExclude: String = \r\n                                                   --QUESTION why code, and not procName?? ProcName seems much better... I do the change, change back if problems appear...\r\n                                                   --code\r\n                                                   procName\r\n                                                   .concat(if processor=MProcessor::Head \r\n                                                   -- CHANGE turned around as head and tail delivered the wrong code.\r\n                                                        then \'->last() \' \r\n                                                        else \'->first() \' endif) in\r\n                                                 iterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\'\r\n                                                .concat(elementToExclude)\r\n                                                .concat(\'then s else s->including(x)->asOrderedSet() endif endif)\')) endif\r\n                       else self.procAsCode() endif)\r\n               .concat( \'\\n  endif\' ) endif  ) \r\n\t\t      \r\n\r\n/*\r\n-- chainType: the type coming out of the chain\r\nlet chainTypeString:String =\r\n     if self.lastElement.oclIsUndefined()  then \r\n       typeAsOcl( self.selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, self.previousExpression.calculatedOwnSingular )  \r\n     else\r\n       typeAsOcl( self.selfObjectPackage, self.chainCalculatedType, self.chainCalculatedSimpleType, self.chainCalculatedSingular )  \r\n     endif  in\r\nlet chainTypeStringSingular: String =\r\n\t  if self.lastElement.oclIsUndefined() then \r\n\t\t  typeAsOcl(selfObjectPackage, self.previousExpression.calculatedOwnType, self.previousExpression.calculatedOwnSimpleType, true)\r\n\t  else\r\n\t\t   typeAsOcl(selfObjectPackage, self.lastElement.calculatedType, self.lastElement.calculatedSimpleType, true) endif in\r\n-- castType: the type to which we cast.\r\nlet castTypeString: String = \r\n      typeAsOcl(selfObjectPackage, castType, SimpleType::None, self.chainCalculatedSingular) in\r\nlet castTypeStringSingular: String = \r\n     typeAsOcl(selfObjectPackage,castType, SimpleType::None, true) in\r\n--let name for the input to cast\r\nlet castName: String = \r\n     self.uniqueSubchainName().concat(\'cast\') in\t \r\n--let name for the input to proc\r\nlet procName: String = \r\n     self.uniqueSubchainName().concat(\'proc\') in\t \r\n--code for chained properties/operation:\r\nlet code: String = \r\n     self.asCodeForOthers() in\r\n \r\n\' in \\n\'\r\n.concat(if self.processor = MProcessor::None  \r\n  then \'\' \r\n  else \'let \'.concat(procName.concat(\':\').concat(if self.castType.oclIsUndefined() then chainTypeString else castTypeString endif).concat(\' = \')) endif)\r\n.concat(if self.castType.oclIsUndefined()then  \' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \')\r\n.concat(code).concat( \' endif \\n\')\r\n else  \'let \'.concat(castName).concat(\':\').concat(chainTypeString).concat(\' =\')\r\n.concat(\' if \'.concat(self.uniqueSubchainName()).concat( \' = null \\n  then null \\n else \'))\r\n.concat(code).concat( \' endif \\n\')\r\n.concat(\' in \\n\').concat(if self.chainCalculatedSingular then \'if \'.concat(castName).concat(\'.oclIsUndefined() then null else \') else \'\' endif)\r\n.concat(if self.chainCalculatedSingular then \'if \'.concat(castName).concat(\'.oclIsKindOf(\').concat(castTypeStringSingular).concat(\')\\n then \'.concat(castName).concat(\'.oclAsType(\').concat(castTypeStringSingular).concat(\') else null endif endif \'))\r\nelse  \r\n\r\ncastName.concat(\'->iterate(i:\'.concat(chainTypeStringSingular).concat(\'; r: OrderedSet(\').concat(castTypeStringSingular).concat(\')=OrderedSet{} | if i.oclIsKindOf(\').concat(castTypeStringSingular).concat(\') then r->including(i.oclAsType(\').concat(castTypeStringSingular).concat(\')\').concat(\')->asOrderedSet() \\n else r endif)\'))\r\n endif) endif)\r\n \r\n .concat(if self.processor = MProcessor::None then \'\' else \' in \\n if \'.concat(procName.concat(if self.isProcessorSetOperator() then \'->\' else \'.\' endif.concat(if self.isCustomCodeProcessor() then \'isEmpty()\' else self.procAsCode().concat(\'.oclIsUndefined()\')endif))).concat(\' then null \\n else \').concat(procName).concat( if self.isProcessorSetOperator() then \'->\' else \'.\' endif\r\n \r\n .concat(if self.isCustomCodeProcessor() then\r\n\t\t   let checkPart : String = if  processor= MProcessor::And then \'false\' else \'true\' endif\r\nin\r\nlet elsePart : String = if processor= MProcessor::And then \'true\' else \'false\' endif in\r\n\r\nlet iterateBase: String = \r\n \'iterate( x:\'.concat(chainTypeStringSingular).concat(\'; s:\') in\r\n \r\n if self.processor = MProcessor::And or processor = MProcessor::Or then\r\niterateBase.concat(chainTypeStringSingular).concat(\'= \').concat(elsePart)\r\n.concat(\'|  if ( x) = \').concat(checkPart).concat( \' \\n then \').concat(checkPart).concat(\'\\n\').concat(\'else if (s)=\').concat(checkPart).concat(\'\\n\').concat(\'then \').concat(checkPart).concat(\'\\n\').concat(\' else if x =null then null \\n\'.concat(\'else if s =null then null\')\r\n).concat(\' else \').concat(elsePart).concat(\' endif endif endif endif)\')\r\n\r\nelse\r\niterateBase.concat(chainTypeString).concat(\'= \').concat(\'OrderedSet{} | if x.oclIsUndefined() then s else if x=\'.concat(code).concat(if processor=MProcessor::Head then \'->first() \' else \'->last() \' endif).concat(\'then s else s->including(x)->asOrderedSet() endif endif)\'))\r\nendif\r\n else self.procAsCode() endif).concat( \r\n\t\t\t \'\\n  endif\' )) endif  ) \r\n\t\t\t \052/\r\n\r\n\r\n\r\n\r\n' chainCalculatedSingularDerive='let s1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedSingular endif in\r\nlet s2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedSingular endif in\r\nlet s3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedSingular endif in\r\n\r\n(self.previousExpression.calculatedOwnSingular and s1 and s2 and s3) ' chainCalculatedTypeDerive='if self.lastElement=null then self.previousExpression.calculatedOwnType else\r\nself.lastElement.type endif' chainCalculatedSimpleTypeDerive='if self.lastElement.oclIsUndefined() \r\n      then self.previousExpression.calculatedOwnSimpleType\r\n      else self.lastElement.simpleType endif'"
 * @generated
 */

public interface MSubChain extends MAbstractExpression, MAbstractChain {
	/**
	 * Returns the value of the '<em><b>Previous Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Expression</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Expression</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSubChain_PreviousExpression()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if containingExpression.oclIsUndefined() then null\r\nelse if not containingExpression.oclIsKindOf(MBaseChain) then null\r\nelse let chain: MBaseChain = containingExpression.oclAsType(MBaseChain) in\r\n\tlet pos: Integer = chain.subExpression->indexOf(self) in \r\n\r\n\tif pos=1\tthen chain.oclAsType(MAbstractExpression)\r\n\telse chain.subExpression->at(pos-1).oclAsType(MAbstractExpression) endif\r\nendif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MAbstractExpression getPreviousExpression();

	/**
	 * Returns the value of the '<em><b>Next Expression</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Next Expression</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Next Expression</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSubChain_NextExpression()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='nextExpression'"
	 *        annotation="http://www.xocl.org/OCL derive=' let chain: MBaseChain = eContainer().oclAsType(MBaseChain) in\r\n\tlet pos: Integer = chain.subExpression->indexOf(self) in \r\n\r\n\t if chain.subExpression->at(pos+1).oclIsUndefined() then null\r\n else chain.subExpression->at(pos+1).oclAsType(MAbstractExpression)\r\n endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MAbstractExpression getNextExpression();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.MSubChainAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.expressions.MSubChainAction
	 * @see #setDoAction(MSubChainAction)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSubChain_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::expressions::MSubChainAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MSubChainAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MSubChain#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.expressions.MSubChainAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MSubChainAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MSubChainAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 *        annotation="http://www.montages.com/mCore/MCore mName='is Last Subchain'"
	 *        annotation="http://www.xocl.org/OCL body='self.eContainer().oclAsType(MBaseChain).subExpression->last() = self'"
	 * @generated
	 */
	Boolean isLastSubchain();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\r\nlet chainNumber:String = \r\n(self.eContainer().oclAsType(MBaseChain).subExpression->size()+1 - self.eContainer().oclAsType(MBaseChain).subExpression->indexOf(self)).toString() in\r\nlet vName: String = \'sub\'.concat(if self.eContainer().oclAsType(MBaseChain).uniqueChainNumber().oclIsUndefined() then \'\' else self.eContainer().oclAsType(MBaseChain).uniqueChainNumber() endif).concat(chainNumber) in \r\nvName'"
	 * @generated
	 */
	String uniqueSubchainName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='\r\n if length()=1 then codeForLength1() \r\nelse if length()=2 then codeForLength2() \r\nelse if length()=3 then codeForLength3() \r\nelse self.uniqueSubchainName() endif endif endif '"
	 * @generated
	 */
	String asCodeForOthers();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='let previousSingular: Boolean =  \r\nif self.previousExpression.oclAsType(MAbstractChain).processor <> MProcessor::None then self.previousExpression.oclAsType(MAbstractChain).processorReturnsSingular() else self.previousExpression.calculatedSingular endif in\r\n\r\nlet s1: Boolean = \r\n  if element1.oclIsUndefined() then true else element1.calculatedSingular endif in\r\nlet s2: Boolean =\r\n   if element2.oclIsUndefined() then true else element2.calculatedSingular endif in\r\nlet s3: Boolean = \r\n  if element3.oclIsUndefined() then true else element3.calculatedSingular endif in\r\n\r\npreviousSingular and s1 and s2 and s3 \r\n'"
	 * @generated
	 */
	Boolean returnSingularPureChain();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length1'"
	 *        annotation="http://www.xocl.org/OCL body='let chain: MSubChain = self.oclAsType(MSubChain) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and \r\nchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None) in\r\n\r\n\r\nlet b: String =  self.oclAsType(MSubChain).uniqueSubchainName().concat(\'.\') in \r\nb.concat(unsafeChainStepAsCode(1)).concat(if self.oclAsType(MSubChain).chainCalculatedSingular then \'\' else\r\nif noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\'endif.concat(\'->asOrderedSet()\') endif)'"
	 * @generated
	 */
	String codeForLength1();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length2'"
	 *        annotation="http://www.xocl.org/OCL body='\r\nlet chain: MSubChain = self.oclAsType(MSubChain) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (chain.nextExpression.oclIsUndefined()) and \r\nchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and chain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)\r\n\r\nin\r\n\r\nlet b: String = self.oclAsType(MSubChain).uniqueSubchainName().concat(\'.\') in \r\n\r\nif  chain.previousExpression.calculatedOwnSingular then\r\n\r\nif element1.calculatedSingular and element2.calculatedSingular then\r\n  let unsafe: String = b.concat(unsafeChainAsCode(1,2)) in\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,2)).concat(\'\\nendif\')\r\n\r\nelse  if (not element1.calculatedSingular) and element2.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined())->asOrderedSet()\')\r\n\r\nelse  if (not element1.calculatedSingular) and (not element2.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->asOrderedSet()\')\r\n\r\n\r\nelse null\r\n\r\nendif  endif endif endif\r\n\r\nelse\r\n\r\n  b.concat(unsafeChainAsCode(1,1)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(2,2)).concat(if noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\' endif.concat(\r\n  \'->asOrderedSet()\'))\r\n\r\nendif'"
	 * @generated
	 */
	String codeForLength2();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Code For Length3'"
	 *        annotation="http://www.xocl.org/OCL body='let subchain: MSubChain = self.oclAsType(MSubChain ) in\r\n\r\nlet noRejectNeeded: Boolean = self.processor <> MProcessor::None or (not (subchain.nextExpression.oclIsUndefined()) and \r\nsubchain.nextExpression.oclAsType(MAbstractChain).lastElement.oclIsUndefined() and subchain.nextExpression.oclAsType(MAbstractChain).processor <> MProcessor::None)\r\nin\r\nlet abstract:  MAbstractExpressionWithBase = self.oclAsType(mcore::expressions::MAbstractExpressionWithBase) in\r\n\r\n\r\nlet b: String = subchain.uniqueSubchainName().concat(\'.\')  in \r\n\r\nif  subchain.previousExpression.calculatedOwnSingular then\r\n\r\n\r\nif element1.calculatedSingular and element2.calculatedSingular and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then null\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,2)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(if subchain.nextExpression.oclIsUndefined() then \'\' else \'->asOrderedSet()\' endif).concat(\'\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\\nendif\')\r\n\r\nelse if element1.calculatedSingular and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  \'if \'.concat(b).concat(unsafeChainAsCode(1,1)).concat(\'.oclIsUndefined()\\n  then OrderedSet{}\\n  else \').concat(b).concat(unsafeChainAsCode(1,3)).concat(\'->asOrderedSet()\\nendif\')\r\n\r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(--\'->reject(oclIsUndefined()) \r\n  \'->asOrderedSet()\')\r\n  \r\nelse if (not element1.calculatedSingular) and element2.calculatedSingular and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if subchain.nextExpression.oclIsUndefined() then \'\' else \'->asOrderedSet()\' endif)\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and element3.calculatedSingular then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\')\r\n\r\nelse if (not element1.calculatedSingular) and (not element2.calculatedSingular) and (not element3.calculatedSingular) then\r\n  b.concat(unsafeChainAsCode(1,3)).concat(--\'->reject(oclIsUndefined())\r\n  \'->asOrderedSet()\')\r\n\r\nelse null\r\nendif endif endif endif endif endif endif endif \r\n\r\nelse\r\n\r\n  b.concat(unsafeChainAsCode(1,1)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(2,2)).concat(\'->reject(oclIsUndefined()).\').concat(unsafeChainAsCode(3,3)).concat(if noRejectNeeded then \'\' else \'->reject(oclIsUndefined())\' endif.concat(\r\n  \'->asOrderedSet()\'))\r\n\r\nendif'"
	 * @generated
	 */
	String codeForLength3();

} // MSubChain
