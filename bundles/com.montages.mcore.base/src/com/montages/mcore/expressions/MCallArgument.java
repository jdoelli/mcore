/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MCall Argument</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MCallArgument#getFirst <em>First</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MCallArgument#getLast <em>Last</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MCallArgument#getCall <em>Call</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCallArgument()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL asCodeDerive='let s:String = baseAsCode in\n\nif\n(self.call.lastElement.operationSignature.parameter->at(self.call.lastElement.operationSignature.parameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).parameter))\n.calculatedSingular).oclIsUndefined()\nthen s\nelse if\n (self.call.lastElement.operationSignature.parameter->at(self.call.lastElement.operationSignature.parameter->indexOf(self.baseDefinition.oclAsType(expressions::MParameterBaseDefinition).parameter))\n.calculatedSingular) \n then s\n else\ns.concat(\'->asOrderedSet()\')\nendif\nendif' kindLabelDerive='\'Argument\'\n'"
 * @generated
 */

public interface MCallArgument extends MAbstractExpressionWithBase {
	/**
	 * Returns the value of the '<em><b>First</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCallArgument_First()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.call.oclIsUndefined() \r\n  then false\r\n  else self.call.callArgument->first()=self endif'"
	 * @generated
	 */
	Boolean getFirst();

	/**
	 * Returns the value of the '<em><b>Last</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCallArgument_Last()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.call.oclIsUndefined() \r\n  then false\r\n  else self.call.callArgument->last()=self endif'"
	 * @generated
	 */
	Boolean getLast();

	/**
	 * Returns the value of the '<em><b>Call</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link com.montages.mcore.expressions.MBaseChain#getCallArgument <em>Call Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Call</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Call</em>' container reference.
	 * @see #setCall(MBaseChain)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMCallArgument_Call()
	 * @see com.montages.mcore.expressions.MBaseChain#getCallArgument
	 * @model opposite="callArgument" unsettable="true" transient="false"
	 * @generated
	 */
	MBaseChain getCall();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MCallArgument#getCall <em>Call</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Call</em>' container reference.
	 * @see #getCall()
	 * @generated
	 */
	void setCall(MBaseChain value);

} // MCallArgument
