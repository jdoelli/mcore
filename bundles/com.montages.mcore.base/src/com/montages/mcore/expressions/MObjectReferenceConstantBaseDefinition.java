/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Reference Constant Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition#getConstantLet <em>Constant Let</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceConstantBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='\'<const> \'.concat(if namedConstant.oclIsUndefined() then \'\' else namedConstant.eName endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='if namedConstant.oclIsUndefined() then \'MISSING EXPRESSION\' else namedConstant.eName endif' calculatedBaseDerive='ExpressionBase::ConstantObjectReference' calculatedMandatoryDerive='if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedMandatory endif' calculatedTypeDerive='if constantLet.oclIsUndefined() then null\r\nelse constantLet.calculatedType endif' calculatedSingularDerive='if constantLet.oclIsUndefined() then true\r\nelse constantLet.calculatedSingular endif'"
 * @generated
 */

public interface MObjectReferenceConstantBaseDefinition
		extends MAbstractBaseDefinition {
	/**
	 * Returns the value of the '<em><b>Named Constant</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Constant</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Constant</em>' reference.
	 * @see #isSetNamedConstant()
	 * @see #unsetNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceConstantBaseDefinition_NamedConstant()
	 * @model unsettable="true"
	 * @generated
	 */
	MNamedConstant getNamedConstant();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Named Constant</em>' reference.
	 * @see #isSetNamedConstant()
	 * @see #unsetNamedConstant()
	 * @see #getNamedConstant()
	 * @generated
	 */
	void setNamedConstant(MNamedConstant value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedConstant()
	 * @see #getNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @generated
	 */
	void unsetNamedConstant();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition#getNamedConstant <em>Named Constant</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Constant</em>' reference is set.
	 * @see #unsetNamedConstant()
	 * @see #getNamedConstant()
	 * @see #setNamedConstant(MNamedConstant)
	 * @generated
	 */
	boolean isSetNamedConstant();

	/**
	 * Returns the value of the '<em><b>Constant Let</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Let</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Let</em>' reference.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceConstantBaseDefinition_ConstantLet()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if namedConstant.expression.oclIsUndefined() then  null\r\nelse if namedConstant.expression.oclIsKindOf(MObjectReferenceLet) \r\n\tthen namedConstant.expression.oclAsType(MObjectReferenceLet)\r\n\telse null endif \r\nendif'"
	 * @generated
	 */
	MObjectReferenceLet getConstantLet();

} // MObjectReferenceConstantBaseDefinition
