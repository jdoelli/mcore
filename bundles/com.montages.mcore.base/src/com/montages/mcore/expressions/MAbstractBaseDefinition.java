/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MTyped;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getCalculatedBase <em>Calculated Base</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getCalculatedAsCode <em>Calculated As Code</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getDebug <em>Debug</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractBaseDefinition()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OCL label='calculatedBase.toString()'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedSimpleTypeDerive='SimpleType::None'"
 * @generated
 */

public interface MAbstractBaseDefinition extends MTyped {
	/**
	 * Returns the value of the '<em><b>Calculated Base</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.expressions.ExpressionBase}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Base</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Base</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionBase
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractBaseDefinition_CalculatedBase()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='ExpressionBase::Undefined'"
	 * @generated
	 */
	ExpressionBase getCalculatedBase();

	/**
	 * Returns the value of the '<em><b>Calculated As Code</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated As Code</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated As Code</em>' attribute.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractBaseDefinition_CalculatedAsCode()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'\'\n'"
	 * @generated
	 */
	String getCalculatedAsCode();

	/**
	 * Returns the value of the '<em><b>Debug</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Debug</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Debug</em>' attribute.
	 * @see #isSetDebug()
	 * @see #unsetDebug()
	 * @see #setDebug(String)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractBaseDefinition_Debug()
	 * @model unsettable="true"
	 * @generated
	 */
	String getDebug();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getDebug <em>Debug</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Debug</em>' attribute.
	 * @see #isSetDebug()
	 * @see #unsetDebug()
	 * @see #getDebug()
	 * @generated
	 */
	void setDebug(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getDebug <em>Debug</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDebug()
	 * @see #getDebug()
	 * @see #setDebug(String)
	 * @generated
	 */
	void unsetDebug();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractBaseDefinition#getDebug <em>Debug</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Debug</em>' attribute is set.
	 * @see #unsetDebug()
	 * @see #getDebug()
	 * @see #setDebug(String)
	 * @generated
	 */
	boolean isSetDebug();

} // MAbstractBaseDefinition
