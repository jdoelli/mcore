/**
 */
package com.montages.mcore.expressions;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Named Tuple</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractNamedTuple#getAbstractEntry <em>Abstract Entry</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractNamedTuple()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractNamedTuple extends MAbstractLet {
	/**
	 * Returns the value of the '<em><b>Abstract Entry</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.expressions.MAbstractTupleEntry}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Entry</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Entry</em>' reference list.
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractNamedTuple_AbstractEntry()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 * @generated
	 */
	EList<MAbstractTupleEntry> getAbstractEntry();

} // MAbstractNamedTuple
