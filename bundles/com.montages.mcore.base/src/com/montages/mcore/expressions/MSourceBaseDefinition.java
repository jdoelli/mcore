
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MSource Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMSourceBaseDefinition()
 * @model annotation="http://www.montages.com/mCore/MCore mName='M SourceBaseDefinition'"
 *        annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedSimpleTypeDerive='let srcSimple:SimpleType =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).srcObjectSimpleType\r\nelse null\r\nendif\r\nin \r\nif srcSimple.oclIsUndefined() then SimpleType::None else srcSimple endif ' calculatedBaseDerive='ExpressionBase::Source' calculatedAsCodeDerive='\'src\'' calculatedTypeDerive='let srcType:MClassifier =\r\nif eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).srcObjectType\r\nelse null\r\nendif\r\nin \r\nif srcType.oclIsUndefined() then null else srcType endif' calculatedSingularDerive='true\n'"
 * @generated
 */

public interface MSourceBaseDefinition extends MAbstractBaseDefinition {
} // MSourceBaseDefinition
