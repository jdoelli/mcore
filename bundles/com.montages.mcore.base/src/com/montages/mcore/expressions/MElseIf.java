/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MElse If</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MElseIf#getCondition <em>Condition</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MElseIf#getThenPart <em>Then Part</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMElseIf()
 * @model annotation="http://www.xocl.org/OCL label='\'else if \'.concat(\r\n\tif condition.oclIsUndefined() then \'MISSING CONDITION\'\r\n\telse condition.getShortCode() endif\r\n).concat(\' \').concat(\r\n\tif thenPart.oclIsUndefined() then \'MISSING THEN\'\r\n\telse thenPart.getShortCode() endif\r\n)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='let kind: String = \'ELSE IF\' in\nkind\n' calculatedOwnMandatoryDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedMandatory endif' calculatedOwnTypeDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then null\r\nelse thenPart.calculatedType endif' calculatedOwnSingularDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then true\r\nelse thenPart.calculatedSingular endif' calculatedOwnSimpleTypeDerive='/* TODO: better typing and verification \052/\r\nif thenPart.oclIsUndefined() then SimpleType::None\r\nelse thenPart.calculatedSimpleType endif' asBasicCodeDerive='\'else if (\'.concat(\r\n\tif condition.oclIsUndefined() then \'MISSING IF CONDITION\'\r\n\telse condition.asCode endif\r\n).concat(\')=true \').concat(\r\n\tif thenPart.oclIsUndefined() then \'MISSING THEN\'\r\n\telse thenPart.asCode endif\r\n)'"
 * @generated
 */

public interface MElseIf extends MAbstractExpression {
	/**
	 * Returns the value of the '<em><b>Condition</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Condition</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMElseIf_Condition()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='defaultValue()'"
	 * @generated
	 */
	MChainOrApplication getCondition();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MElseIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Condition</em>' containment reference.
	 * @see #isSetCondition()
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @generated
	 */
	void setCondition(MChainOrApplication value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MElseIf#getCondition <em>Condition</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCondition()
	 * @see #getCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @generated
	 */
	void unsetCondition();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MElseIf#getCondition <em>Condition</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Condition</em>' containment reference is set.
	 * @see #unsetCondition()
	 * @see #getCondition()
	 * @see #setCondition(MChainOrApplication)
	 * @generated
	 */
	boolean isSetCondition();

	/**
	 * Returns the value of the '<em><b>Then Part</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then Part</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then Part</em>' containment reference.
	 * @see #isSetThenPart()
	 * @see #unsetThenPart()
	 * @see #setThenPart(MThen)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMElseIf_ThenPart()
	 * @model containment="true" resolveProxies="true" unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='Tuple{expression = defaultValue()}'"
	 * @generated
	 */
	MThen getThenPart();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MElseIf#getThenPart <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then Part</em>' containment reference.
	 * @see #isSetThenPart()
	 * @see #unsetThenPart()
	 * @see #getThenPart()
	 * @generated
	 */
	void setThenPart(MThen value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MElseIf#getThenPart <em>Then Part</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetThenPart()
	 * @see #getThenPart()
	 * @see #setThenPart(MThen)
	 * @generated
	 */
	void unsetThenPart();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MElseIf#getThenPart <em>Then Part</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Then Part</em>' containment reference is set.
	 * @see #unsetThenPart()
	 * @see #getThenPart()
	 * @see #setThenPart(MThen)
	 * @generated
	 */
	boolean isSetThenPart();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='Tuple{base=ExpressionBase::SelfObject}'"
	 * @generated
	 */
	MChain defaultValue();

} // MElseIf
