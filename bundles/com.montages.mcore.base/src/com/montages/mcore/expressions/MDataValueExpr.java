/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.SimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MData Value Expr</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MDataValueExpr#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MDataValueExpr#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMDataValueExpr()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Value\'\n' isComplexExpressionDerive='false' calculatedOwnMandatoryDerive='true' calculatedOwnSingularDerive='true' calculatedOwnTypeDerive='null' calculatedOwnSimpleTypeDerive='simpleType' asBasicCodeDerive='let v:String =if value=null then \'Data Value MISSING\' else value endif in\r\nif simpleType=SimpleType::String\r\nthen \'\\\'\'.concat(v).concat(\'\\\'\') else v endif '"
 * @generated
 */

public interface MDataValueExpr extends MChainOrApplication {
	/**
	 * Returns the value of the '<em><b>Simple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.SimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Simple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMDataValueExpr_SimpleType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='SimpleType::String'"
	 * @generated
	 */
	SimpleType getSimpleType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Simple Type</em>' attribute.
	 * @see com.montages.mcore.SimpleType
	 * @see #isSetSimpleType()
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @generated
	 */
	void setSimpleType(SimpleType value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getSimpleType <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	void unsetSimpleType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getSimpleType <em>Simple Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Simple Type</em>' attribute is set.
	 * @see #unsetSimpleType()
	 * @see #getSimpleType()
	 * @see #setSimpleType(SimpleType)
	 * @generated
	 */
	boolean isSetSimpleType();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(String)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMDataValueExpr_Value()
	 * @model unsettable="true"
	 * @generated
	 */
	String getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MDataValueExpr#getValue <em>Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' attribute is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	boolean isSetValue();

} // MDataValueExpr
