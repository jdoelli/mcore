/**
 */
package com.montages.mcore.expressions;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Base Definition</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectBaseDefinition()
 * @model annotation="http://www.xocl.org/OCL label='calculatedAsCode\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedAsCodeDerive='\'obj\'\n' calculatedBaseDerive='ExpressionBase::ObjectObject' calculatedMandatoryDerive='true' calculatedTypeDerive='if eContainer().oclIsKindOf(MBaseChain)\r\nthen eContainer().oclAsType(MBaseChain).objectObjectType\r\nelse null\r\nendif\r\n' calculatedSingularDerive='true\n'"
 * @generated
 */

public interface MObjectBaseDefinition extends MAbstractBaseDefinition {
} // MObjectBaseDefinition
