/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Tuple Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MAbstractTupleEntry#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractTupleEntry()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Entry\'\n'"
 * @generated
 */

public interface MAbstractTupleEntry extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(MToplevelExpression)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMAbstractTupleEntry_Value()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	MToplevelExpression getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MAbstractTupleEntry#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(MToplevelExpression value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MAbstractTupleEntry#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(MToplevelExpression)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MAbstractTupleEntry#getValue <em>Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' containment reference is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(MToplevelExpression)
	 * @generated
	 */
	boolean isSetValue();

} // MAbstractTupleEntry
