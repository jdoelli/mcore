/**
 */
package com.montages.mcore.expressions.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EObject;
import com.montages.mcore.MNamed;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.expressions.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.expressions.ExpressionsPackage
 * @generated
 */
public class ExpressionsAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExpressionsPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExpressionsAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ExpressionsPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExpressionsSwitch<Adapter> modelSwitch = new ExpressionsSwitch<Adapter>() {
		@Override
		public Adapter caseMAbstractExpression(MAbstractExpression object) {
			return createMAbstractExpressionAdapter();
		}

		@Override
		public Adapter caseMAbstractExpressionWithBase(
				MAbstractExpressionWithBase object) {
			return createMAbstractExpressionWithBaseAdapter();
		}

		@Override
		public Adapter caseMAbstractBaseDefinition(
				MAbstractBaseDefinition object) {
			return createMAbstractBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMContainerBaseDefinition(
				MContainerBaseDefinition object) {
			return createMContainerBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMBaseDefinition(MBaseDefinition object) {
			return createMBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMNumberBaseDefinition(MNumberBaseDefinition object) {
			return createMNumberBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMSelfBaseDefinition(MSelfBaseDefinition object) {
			return createMSelfBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMTargetBaseDefinition(MTargetBaseDefinition object) {
			return createMTargetBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMSourceBaseDefinition(MSourceBaseDefinition object) {
			return createMSourceBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMObjectBaseDefinition(MObjectBaseDefinition object) {
			return createMObjectBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMSimpleTypeConstantBaseDefinition(
				MSimpleTypeConstantBaseDefinition object) {
			return createMSimpleTypeConstantBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMLiteralConstantBaseDefinition(
				MLiteralConstantBaseDefinition object) {
			return createMLiteralConstantBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMObjectReferenceConstantBaseDefinition(
				MObjectReferenceConstantBaseDefinition object) {
			return createMObjectReferenceConstantBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMVariableBaseDefinition(
				MVariableBaseDefinition object) {
			return createMVariableBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMIteratorBaseDefinition(
				MIteratorBaseDefinition object) {
			return createMIteratorBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMParameterBaseDefinition(
				MParameterBaseDefinition object) {
			return createMParameterBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMAccumulatorBaseDefinition(
				MAccumulatorBaseDefinition object) {
			return createMAccumulatorBaseDefinitionAdapter();
		}

		@Override
		public Adapter caseMAbstractLet(MAbstractLet object) {
			return createMAbstractLetAdapter();
		}

		@Override
		public Adapter caseMAbstractNamedTuple(MAbstractNamedTuple object) {
			return createMAbstractNamedTupleAdapter();
		}

		@Override
		public Adapter caseMAbstractTupleEntry(MAbstractTupleEntry object) {
			return createMAbstractTupleEntryAdapter();
		}

		@Override
		public Adapter caseMTuple(MTuple object) {
			return createMTupleAdapter();
		}

		@Override
		public Adapter caseMTupleEntry(MTupleEntry object) {
			return createMTupleEntryAdapter();
		}

		@Override
		public Adapter caseMNewObjecct(MNewObjecct object) {
			return createMNewObjecctAdapter();
		}

		@Override
		public Adapter caseMNewObjectFeatureValue(
				MNewObjectFeatureValue object) {
			return createMNewObjectFeatureValueAdapter();
		}

		@Override
		public Adapter caseMNamedExpression(MNamedExpression object) {
			return createMNamedExpressionAdapter();
		}

		@Override
		public Adapter caseMToplevelExpression(MToplevelExpression object) {
			return createMToplevelExpressionAdapter();
		}

		@Override
		public Adapter caseMAbstractChain(MAbstractChain object) {
			return createMAbstractChainAdapter();
		}

		@Override
		public Adapter caseMBaseChain(MBaseChain object) {
			return createMBaseChainAdapter();
		}

		@Override
		public Adapter caseMProcessorDefinition(MProcessorDefinition object) {
			return createMProcessorDefinitionAdapter();
		}

		@Override
		public Adapter caseMChain(MChain object) {
			return createMChainAdapter();
		}

		@Override
		public Adapter caseMCallArgument(MCallArgument object) {
			return createMCallArgumentAdapter();
		}

		@Override
		public Adapter caseMSubChain(MSubChain object) {
			return createMSubChainAdapter();
		}

		@Override
		public Adapter caseMChainOrApplication(MChainOrApplication object) {
			return createMChainOrApplicationAdapter();
		}

		@Override
		public Adapter caseMDataValueExpr(MDataValueExpr object) {
			return createMDataValueExprAdapter();
		}

		@Override
		public Adapter caseMLiteralValueExpr(MLiteralValueExpr object) {
			return createMLiteralValueExprAdapter();
		}

		@Override
		public Adapter caseMAbstractIf(MAbstractIf object) {
			return createMAbstractIfAdapter();
		}

		@Override
		public Adapter caseMIf(MIf object) {
			return createMIfAdapter();
		}

		@Override
		public Adapter caseMThen(MThen object) {
			return createMThenAdapter();
		}

		@Override
		public Adapter caseMElseIf(MElseIf object) {
			return createMElseIfAdapter();
		}

		@Override
		public Adapter caseMElse(MElse object) {
			return createMElseAdapter();
		}

		@Override
		public Adapter caseMCollectionExpression(MCollectionExpression object) {
			return createMCollectionExpressionAdapter();
		}

		@Override
		public Adapter caseMCollectionVar(MCollectionVar object) {
			return createMCollectionVarAdapter();
		}

		@Override
		public Adapter caseMIterator(MIterator object) {
			return createMIteratorAdapter();
		}

		@Override
		public Adapter caseMAccumulator(MAccumulator object) {
			return createMAccumulatorAdapter();
		}

		@Override
		public Adapter caseMNamedConstant(MNamedConstant object) {
			return createMNamedConstantAdapter();
		}

		@Override
		public Adapter caseMConstantLet(MConstantLet object) {
			return createMConstantLetAdapter();
		}

		@Override
		public Adapter caseMSimpleTypeConstantLet(
				MSimpleTypeConstantLet object) {
			return createMSimpleTypeConstantLetAdapter();
		}

		@Override
		public Adapter caseMLiteralLet(MLiteralLet object) {
			return createMLiteralLetAdapter();
		}

		@Override
		public Adapter caseMObjectReferenceLet(MObjectReferenceLet object) {
			return createMObjectReferenceLetAdapter();
		}

		@Override
		public Adapter caseMApplication(MApplication object) {
			return createMApplicationAdapter();
		}

		@Override
		public Adapter caseMOperatorDefinition(MOperatorDefinition object) {
			return createMOperatorDefinitionAdapter();
		}

		@Override
		public Adapter caseMRepositoryElement(MRepositoryElement object) {
			return createMRepositoryElementAdapter();
		}

		@Override
		public Adapter caseMTyped(MTyped object) {
			return createMTypedAdapter();
		}

		@Override
		public Adapter caseMNamed(MNamed object) {
			return createMNamedAdapter();
		}

		@Override
		public Adapter caseMVariable(MVariable object) {
			return createMVariableAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractExpression <em>MAbstract Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractExpression
	 * @generated
	 */
	public Adapter createMAbstractExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractExpressionWithBase <em>MAbstract Expression With Base</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractExpressionWithBase
	 * @generated
	 */
	public Adapter createMAbstractExpressionWithBaseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractBaseDefinition <em>MAbstract Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractBaseDefinition
	 * @generated
	 */
	public Adapter createMAbstractBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MContainerBaseDefinition <em>MContainer Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MContainerBaseDefinition
	 * @generated
	 */
	public Adapter createMContainerBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MBaseDefinition <em>MBase Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MBaseDefinition
	 * @generated
	 */
	public Adapter createMBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MNumberBaseDefinition <em>MNumber Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MNumberBaseDefinition
	 * @generated
	 */
	public Adapter createMNumberBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MSelfBaseDefinition <em>MSelf Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MSelfBaseDefinition
	 * @generated
	 */
	public Adapter createMSelfBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MTargetBaseDefinition <em>MTarget Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MTargetBaseDefinition
	 * @generated
	 */
	public Adapter createMTargetBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MSourceBaseDefinition <em>MSource Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MSourceBaseDefinition
	 * @generated
	 */
	public Adapter createMSourceBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MObjectBaseDefinition <em>MObject Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MObjectBaseDefinition
	 * @generated
	 */
	public Adapter createMObjectBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition <em>MSimple Type Constant Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MSimpleTypeConstantBaseDefinition
	 * @generated
	 */
	public Adapter createMSimpleTypeConstantBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition <em>MLiteral Constant Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MLiteralConstantBaseDefinition
	 * @generated
	 */
	public Adapter createMLiteralConstantBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition <em>MObject Reference Constant Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MObjectReferenceConstantBaseDefinition
	 * @generated
	 */
	public Adapter createMObjectReferenceConstantBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MVariableBaseDefinition <em>MVariable Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MVariableBaseDefinition
	 * @generated
	 */
	public Adapter createMVariableBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MParameterBaseDefinition <em>MParameter Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MParameterBaseDefinition
	 * @generated
	 */
	public Adapter createMParameterBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MIteratorBaseDefinition <em>MIterator Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MIteratorBaseDefinition
	 * @generated
	 */
	public Adapter createMIteratorBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAccumulatorBaseDefinition <em>MAccumulator Base Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAccumulatorBaseDefinition
	 * @generated
	 */
	public Adapter createMAccumulatorBaseDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractLet <em>MAbstract Let</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractLet
	 * @generated
	 */
	public Adapter createMAbstractLetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractNamedTuple <em>MAbstract Named Tuple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractNamedTuple
	 * @generated
	 */
	public Adapter createMAbstractNamedTupleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractTupleEntry <em>MAbstract Tuple Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractTupleEntry
	 * @generated
	 */
	public Adapter createMAbstractTupleEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MTuple <em>MTuple</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MTuple
	 * @generated
	 */
	public Adapter createMTupleAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MTupleEntry <em>MTuple Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MTupleEntry
	 * @generated
	 */
	public Adapter createMTupleEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MNewObjecct <em>MNew Objecct</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MNewObjecct
	 * @generated
	 */
	public Adapter createMNewObjecctAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MNewObjectFeatureValue <em>MNew Object Feature Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MNewObjectFeatureValue
	 * @generated
	 */
	public Adapter createMNewObjectFeatureValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MNamedExpression <em>MNamed Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MNamedExpression
	 * @generated
	 */
	public Adapter createMNamedExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MToplevelExpression <em>MToplevel Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MToplevelExpression
	 * @generated
	 */
	public Adapter createMToplevelExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractChain <em>MAbstract Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractChain
	 * @generated
	 */
	public Adapter createMAbstractChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MBaseChain <em>MBase Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MBaseChain
	 * @generated
	 */
	public Adapter createMBaseChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MProcessorDefinition <em>MProcessor Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MProcessorDefinition
	 * @generated
	 */
	public Adapter createMProcessorDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MChain <em>MChain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MChain
	 * @generated
	 */
	public Adapter createMChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MCallArgument <em>MCall Argument</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MCallArgument
	 * @generated
	 */
	public Adapter createMCallArgumentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MChainOrApplication <em>MChain Or Application</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MChainOrApplication
	 * @generated
	 */
	public Adapter createMChainOrApplicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MDataValueExpr <em>MData Value Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MDataValueExpr
	 * @generated
	 */
	public Adapter createMDataValueExprAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MLiteralValueExpr <em>MLiteral Value Expr</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MLiteralValueExpr
	 * @generated
	 */
	public Adapter createMLiteralValueExprAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAbstractIf <em>MAbstract If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAbstractIf
	 * @generated
	 */
	public Adapter createMAbstractIfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MIf <em>MIf</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MIf
	 * @generated
	 */
	public Adapter createMIfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MThen <em>MThen</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MThen
	 * @generated
	 */
	public Adapter createMThenAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MElseIf <em>MElse If</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MElseIf
	 * @generated
	 */
	public Adapter createMElseIfAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MElse <em>MElse</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MElse
	 * @generated
	 */
	public Adapter createMElseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MSubChain <em>MSub Chain</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MSubChain
	 * @generated
	 */
	public Adapter createMSubChainAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MCollectionExpression <em>MCollection Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MCollectionExpression
	 * @generated
	 */
	public Adapter createMCollectionExpressionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MCollectionVar <em>MCollection Var</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MCollectionVar
	 * @generated
	 */
	public Adapter createMCollectionVarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MIterator <em>MIterator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MIterator
	 * @generated
	 */
	public Adapter createMIteratorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MAccumulator <em>MAccumulator</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MAccumulator
	 * @generated
	 */
	public Adapter createMAccumulatorAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MNamedConstant <em>MNamed Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MNamedConstant
	 * @generated
	 */
	public Adapter createMNamedConstantAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MConstantLet <em>MConstant Let</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MConstantLet
	 * @generated
	 */
	public Adapter createMConstantLetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MSimpleTypeConstantLet <em>MSimple Type Constant Let</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MSimpleTypeConstantLet
	 * @generated
	 */
	public Adapter createMSimpleTypeConstantLetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MLiteralLet <em>MLiteral Let</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MLiteralLet
	 * @generated
	 */
	public Adapter createMLiteralLetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MObjectReferenceLet <em>MObject Reference Let</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MObjectReferenceLet
	 * @generated
	 */
	public Adapter createMObjectReferenceLetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MApplication <em>MApplication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MApplication
	 * @generated
	 */
	public Adapter createMApplicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.expressions.MOperatorDefinition <em>MOperator Definition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.expressions.MOperatorDefinition
	 * @generated
	 */
	public Adapter createMOperatorDefinitionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MRepositoryElement <em>MRepository Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MRepositoryElement
	 * @generated
	 */
	public Adapter createMRepositoryElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MTyped <em>MTyped</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MTyped
	 * @generated
	 */
	public Adapter createMTypedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MNamed <em>MNamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MNamed
	 * @generated
	 */
	public Adapter createMNamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.mcore.MVariable <em>MVariable</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.mcore.MVariable
	 * @generated
	 */
	public Adapter createMVariableAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ExpressionsAdapterFactory
