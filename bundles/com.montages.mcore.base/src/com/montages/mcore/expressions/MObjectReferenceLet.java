/**
 */
package com.montages.mcore.expressions;

import com.montages.mcore.MClassifier;
import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MObject Reference Let</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceLet#getObjectType <em>Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant1 <em>Constant1</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant2 <em>Constant2</em>}</li>
 *   <li>{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant3 <em>Constant3</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceLet()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OCL label='asCode'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Object Reference\'\n' asBasicCodeDerive='let t: String =  if objectType.oclIsUndefined() then \'MISSING TYPE\' else\r\n\tobjectType.calculatedShortName endif in\r\nlet a: String = if constant1.oclIsUndefined() then \'\' else \'<obj \'.concat(constant1.id).concat(\'>\') endif in\r\nlet b: String = if constant2.oclIsUndefined() then \'\' else \'<obj \'.concat(constant2.id).concat(\'>\') endif in\r\nlet c: String = if constant3.oclIsUndefined() then \'\' else \'<obj \'.concat(constant3.id).concat(\'>\')  endif in\r\n\r\n if calculatedSingular\r\n  then a.concat(b).concat(c)\r\n  else asSetString(\r\n  \t\'<obj \'.concat(constant1.id).concat(\'>\'),\r\n  \t\'<obj \'.concat(constant2.id).concat(\'>\'),\r\n  \t\'<obj \'.concat(constant3.id).concat(\'>\')\r\n  )\r\n  endif\r\n' calculatedOwnMandatoryDerive='let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) > 0' calculatedOwnSingularDerive='let a: Integer = if constant1.oclIsUndefined() then 0 else 1 endif in\r\nlet b: Integer = if constant2.oclIsUndefined() then 0 else 1 endif in\r\nlet c: Integer = if constant3.oclIsUndefined() then 0 else 1 endif in\r\n\t\r\n(a+b+c) <= 1' calculatedOwnTypeDerive='objectType'"
 * @generated
 */

public interface MObjectReferenceLet extends MConstantLet {
	/**
	 * Returns the value of the '<em><b>Object Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Type</em>' reference.
	 * @see #isSetObjectType()
	 * @see #unsetObjectType()
	 * @see #setObjectType(MClassifier)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceLet_ObjectType()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	MClassifier getObjectType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getObjectType <em>Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Type</em>' reference.
	 * @see #isSetObjectType()
	 * @see #unsetObjectType()
	 * @see #getObjectType()
	 * @generated
	 */
	void setObjectType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getObjectType <em>Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObjectType()
	 * @see #getObjectType()
	 * @see #setObjectType(MClassifier)
	 * @generated
	 */
	void unsetObjectType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getObjectType <em>Object Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object Type</em>' reference is set.
	 * @see #unsetObjectType()
	 * @see #getObjectType()
	 * @see #setObjectType(MClassifier)
	 * @generated
	 */
	boolean isSetObjectType();

	/**
	 * Returns the value of the '<em><b>Constant1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant1</em>' reference.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #setConstant1(MObject)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceLet_Constant1()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif'"
	 * @generated
	 */
	MObject getConstant1();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant1 <em>Constant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant1</em>' reference.
	 * @see #isSetConstant1()
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @generated
	 */
	void setConstant1(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant1 <em>Constant1</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(MObject)
	 * @generated
	 */
	void unsetConstant1();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant1 <em>Constant1</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant1</em>' reference is set.
	 * @see #unsetConstant1()
	 * @see #getConstant1()
	 * @see #setConstant1(MObject)
	 * @generated
	 */
	boolean isSetConstant1();

	/**
	 * Returns the value of the '<em><b>Constant2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant2</em>' reference.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #setConstant2(MObject)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceLet_Constant2()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif'"
	 * @generated
	 */
	MObject getConstant2();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant2 <em>Constant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant2</em>' reference.
	 * @see #isSetConstant2()
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @generated
	 */
	void setConstant2(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant2 <em>Constant2</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(MObject)
	 * @generated
	 */
	void unsetConstant2();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant2 <em>Constant2</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant2</em>' reference is set.
	 * @see #unsetConstant2()
	 * @see #getConstant2()
	 * @see #setConstant2(MObject)
	 * @generated
	 */
	boolean isSetConstant2();

	/**
	 * Returns the value of the '<em><b>Constant3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant3</em>' reference.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #setConstant3(MObject)
	 * @see com.montages.mcore.expressions.ExpressionsPackage#getMObjectReferenceLet_Constant3()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='if objectType.oclIsUndefined() then false\r\nelse trg.type = objectType endif'"
	 * @generated
	 */
	MObject getConstant3();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant3 <em>Constant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant3</em>' reference.
	 * @see #isSetConstant3()
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @generated
	 */
	void setConstant3(MObject value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant3 <em>Constant3</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(MObject)
	 * @generated
	 */
	void unsetConstant3();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.expressions.MObjectReferenceLet#getConstant3 <em>Constant3</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant3</em>' reference is set.
	 * @see #unsetConstant3()
	 * @see #getConstant3()
	 * @see #setConstant3(MObject)
	 * @generated
	 */
	boolean isSetConstant3();

} // MObjectReferenceLet
