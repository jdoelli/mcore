/**
 */
package com.montages.mcore;

import com.montages.mcore.annotations.MGeneralAnnotation;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EPackage;
import org.xocl.semantics.XSemantics;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.objects.MResourceFolder;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MComponent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MComponent#getOwnedPackage <em>Owned Package</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getResourceFolder <em>Resource Folder</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getSemantics <em>Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getGeneratedEPackage <em>Generated EPackage</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getNamedEditor <em>Named Editor</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getNamePrefix <em>Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDerivedNamePrefix <em>Derived Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getNamePrefixForFeatures <em>Name Prefix For Features</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getContainingRepository <em>Containing Repository</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getRepresentsCoreComponent <em>Represents Core Component</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getRootPackage <em>Root Package</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDomainType <em>Domain Type</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDomainName <em>Domain Name</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDerivedDomainType <em>Derived Domain Type</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDerivedDomainName <em>Derived Domain Name</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDerivedBundleName <em>Derived Bundle Name</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDerivedURI <em>Derived URI</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getSpecialBundleName <em>Special Bundle Name</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getPreloadedComponent <em>Preloaded Component</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getMainNamedEditor <em>Main Named Editor</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getMainEditor <em>Main Editor</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getHideAdvancedProperties <em>Hide Advanced Properties</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getAllGeneralAnnotations <em>All General Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getCountImplementations <em>Count Implementations</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDisableDefaultContainment <em>Disable Default Containment</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}</li>
 *   <li>{@link com.montages.mcore.MComponent#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMComponent()
 * @model annotation="http://www.xocl.org/OCL label='self.derivedBundleName'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL fullLabelDerive='\'[component] \'.concat(self.calculatedName)' kindLabelDerive='\'Component\'\n' nameInitValue='\'My Component\''"
 * @generated
 */

public interface MComponent extends MNamed {
	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.MComponentAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MComponentAction
	 * @see #setDoAction(MComponentAction)
	 * @see com.montages.mcore.McorePackage#getMComponent_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::MComponentAction::Do\n' choiceConstruction='let e1:OrderedSet(MComponentAction)=OrderedSet{MComponentAction::Do, MComponentAction::Object, MComponentAction::Class,MComponentAction::FixDocumentation,MComponentAction::OpenEditor} in\r\nif self.abstractComponent = true\r\nthen e1->append(MComponentAction::Implement)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)\r\nelse e1->append(MComponentAction::Abstract)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert' propertySortChoices='false'"
	 * @generated
	 */
	MComponentAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.MComponentAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MComponentAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MComponentAction trg);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='do Action Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doActionUpdate(MComponentAction mComponentAction);

	/**
	 * Returns the value of the '<em><b>Owned Package</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Owned Package</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Owned Package</em>' containment reference list.
	 * @see #isSetOwnedPackage()
	 * @see #unsetOwnedPackage()
	 * @see com.montages.mcore.McorePackage#getMComponent_OwnedPackage()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 *        annotation="http://www.xocl.org/OCL initOrder='1' initValue='OrderedSet{Tuple{name=\'My Package\'}}'"
	 * @generated
	 */
	EList<MPackage> getOwnedPackage();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getOwnedPackage <em>Owned Package</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOwnedPackage()
	 * @see #getOwnedPackage()
	 * @generated
	 */
	void unsetOwnedPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getOwnedPackage <em>Owned Package</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Owned Package</em>' containment reference list is set.
	 * @see #unsetOwnedPackage()
	 * @see #getOwnedPackage()
	 * @generated
	 */
	boolean isSetOwnedPackage();

	/**
	 * Returns the value of the '<em><b>Resource Folder</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.objects.MResourceFolder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Folder</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Folder</em>' containment reference list.
	 * @see #isSetResourceFolder()
	 * @see #unsetResourceFolder()
	 * @see com.montages.mcore.McorePackage#getMComponent_ResourceFolder()
	 * @model containment="true" resolveProxies="true" unsettable="true" keys="name"
	 *        annotation="http://www.xocl.org/OCL initOrder='2' initValue='OrderedSet{\r\n  Tuple{\r\n        name=\'workspace\', \r\n        folder=\r\n                OrderedSet{\r\n                       Tuple{\r\n                             name=\'test\',\r\n                             resource=\r\n                                 OrderedSet{Tuple{name=\'My\'}}}}}}'"
	 * @generated
	 */
	EList<MResourceFolder> getResourceFolder();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getResourceFolder <em>Resource Folder</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResourceFolder()
	 * @see #getResourceFolder()
	 * @generated
	 */
	void unsetResourceFolder();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getResourceFolder <em>Resource Folder</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Resource Folder</em>' containment reference list is set.
	 * @see #unsetResourceFolder()
	 * @see #getResourceFolder()
	 * @generated
	 */
	boolean isSetResourceFolder();

	/**
	 * Returns the value of the '<em><b>Semantics</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Semantics</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Semantics</em>' containment reference.
	 * @see #isSetSemantics()
	 * @see #unsetSemantics()
	 * @see #setSemantics(XSemantics)
	 * @see com.montages.mcore.McorePackage#getMComponent_Semantics()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	XSemantics getSemantics();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getSemantics <em>Semantics</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Semantics</em>' containment reference.
	 * @see #isSetSemantics()
	 * @see #unsetSemantics()
	 * @see #getSemantics()
	 * @generated
	 */
	void setSemantics(XSemantics value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getSemantics <em>Semantics</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSemantics()
	 * @see #getSemantics()
	 * @see #setSemantics(XSemantics)
	 * @generated
	 */
	void unsetSemantics();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getSemantics <em>Semantics</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Semantics</em>' containment reference is set.
	 * @see #unsetSemantics()
	 * @see #getSemantics()
	 * @see #setSemantics(XSemantics)
	 * @generated
	 */
	boolean isSetSemantics();

	/**
	 * Returns the value of the '<em><b>Preloaded Component</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.MComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Preloaded Component</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Preloaded Component</em>' reference list.
	 * @see #isSetPreloadedComponent()
	 * @see #unsetPreloadedComponent()
	 * @see com.montages.mcore.McorePackage#getMComponent_PreloadedComponent()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MComponent> getPreloadedComponent();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getPreloadedComponent <em>Preloaded Component</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetPreloadedComponent()
	 * @see #getPreloadedComponent()
	 * @generated
	 */
	void unsetPreloadedComponent();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getPreloadedComponent <em>Preloaded Component</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Preloaded Component</em>' reference list is set.
	 * @see #unsetPreloadedComponent()
	 * @see #getPreloadedComponent()
	 * @generated
	 */
	boolean isSetPreloadedComponent();

	/**
	 * Returns the value of the '<em><b>Generated EPackage</b></em>' containment reference list.
	 * The list contents are of type {@link org.eclipse.emf.ecore.EPackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generated EPackage</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generated EPackage</em>' containment reference list.
	 * @see #isSetGeneratedEPackage()
	 * @see #unsetGeneratedEPackage()
	 * @see com.montages.mcore.McorePackage#getMComponent_GeneratedEPackage()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<EPackage> getGeneratedEPackage();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getGeneratedEPackage <em>Generated EPackage</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetGeneratedEPackage()
	 * @see #getGeneratedEPackage()
	 * @generated
	 */
	void unsetGeneratedEPackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getGeneratedEPackage <em>Generated EPackage</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Generated EPackage</em>' containment reference list is set.
	 * @see #unsetGeneratedEPackage()
	 * @see #getGeneratedEPackage()
	 * @generated
	 */
	boolean isSetGeneratedEPackage();

	/**
	 * Returns the value of the '<em><b>Hide Advanced Properties</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hide Advanced Properties</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hide Advanced Properties</em>' attribute.
	 * @see #isSetHideAdvancedProperties()
	 * @see #unsetHideAdvancedProperties()
	 * @see #setHideAdvancedProperties(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_HideAdvancedProperties()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getHideAdvancedProperties();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getHideAdvancedProperties <em>Hide Advanced Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hide Advanced Properties</em>' attribute.
	 * @see #isSetHideAdvancedProperties()
	 * @see #unsetHideAdvancedProperties()
	 * @see #getHideAdvancedProperties()
	 * @generated
	 */
	void setHideAdvancedProperties(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getHideAdvancedProperties <em>Hide Advanced Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHideAdvancedProperties()
	 * @see #getHideAdvancedProperties()
	 * @see #setHideAdvancedProperties(Boolean)
	 * @generated
	 */
	void unsetHideAdvancedProperties();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getHideAdvancedProperties <em>Hide Advanced Properties</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Hide Advanced Properties</em>' attribute is set.
	 * @see #unsetHideAdvancedProperties()
	 * @see #getHideAdvancedProperties()
	 * @see #setHideAdvancedProperties(Boolean)
	 * @generated
	 */
	boolean isSetHideAdvancedProperties();

	/**
	 * Returns the value of the '<em><b>Abstract Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Abstract Component</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Abstract Component</em>' attribute.
	 * @see #isSetAbstractComponent()
	 * @see #unsetAbstractComponent()
	 * @see #setAbstractComponent(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_AbstractComponent()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special Component Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getAbstractComponent();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getAbstractComponent <em>Abstract Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Abstract Component</em>' attribute.
	 * @see #isSetAbstractComponent()
	 * @see #unsetAbstractComponent()
	 * @see #getAbstractComponent()
	 * @generated
	 */

	void setAbstractComponent(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getAbstractComponent <em>Abstract Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAbstractComponent()
	 * @see #getAbstractComponent()
	 * @see #setAbstractComponent(Boolean)
	 * @generated
	 */
	void unsetAbstractComponent();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getAbstractComponent <em>Abstract Component</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Abstract Component</em>' attribute is set.
	 * @see #unsetAbstractComponent()
	 * @see #getAbstractComponent()
	 * @see #setAbstractComponent(Boolean)
	 * @generated
	 */
	boolean isSetAbstractComponent();

	/**
	 * Returns the value of the '<em><b>Count Implementations</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count Implementations</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count Implementations</em>' attribute.
	 * @see #isSetCountImplementations()
	 * @see #unsetCountImplementations()
	 * @see #setCountImplementations(Integer)
	 * @see com.montages.mcore.McorePackage#getMComponent_CountImplementations()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='1\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special Component Settings'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getCountImplementations();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getCountImplementations <em>Count Implementations</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count Implementations</em>' attribute.
	 * @see #isSetCountImplementations()
	 * @see #unsetCountImplementations()
	 * @see #getCountImplementations()
	 * @generated
	 */

	void setCountImplementations(Integer value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getCountImplementations <em>Count Implementations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetCountImplementations()
	 * @see #getCountImplementations()
	 * @see #setCountImplementations(Integer)
	 * @generated
	 */
	void unsetCountImplementations();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getCountImplementations <em>Count Implementations</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Count Implementations</em>' attribute is set.
	 * @see #unsetCountImplementations()
	 * @see #getCountImplementations()
	 * @see #setCountImplementations(Integer)
	 * @generated
	 */
	boolean isSetCountImplementations();

	/**
	 * Returns the value of the '<em><b>Disable Default Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Disable Default Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Disable Default Containment</em>' attribute.
	 * @see #isSetDisableDefaultContainment()
	 * @see #unsetDisableDefaultContainment()
	 * @see #setDisableDefaultContainment(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_DisableDefaultContainment()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='disableDefaultContainment'"
	 *        annotation="http://www.xocl.org/OCL initValue='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special Component Settings' createColumn='false'"
	 * @generated
	 */
	Boolean getDisableDefaultContainment();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getDisableDefaultContainment <em>Disable Default Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Disable Default Containment</em>' attribute.
	 * @see #isSetDisableDefaultContainment()
	 * @see #unsetDisableDefaultContainment()
	 * @see #getDisableDefaultContainment()
	 * @generated
	 */

	void setDisableDefaultContainment(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getDisableDefaultContainment <em>Disable Default Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDisableDefaultContainment()
	 * @see #getDisableDefaultContainment()
	 * @see #setDisableDefaultContainment(Boolean)
	 * @generated
	 */
	void unsetDisableDefaultContainment();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getDisableDefaultContainment <em>Disable Default Containment</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Disable Default Containment</em>' attribute is set.
	 * @see #unsetDisableDefaultContainment()
	 * @see #getDisableDefaultContainment()
	 * @see #setDisableDefaultContainment(Boolean)
	 * @generated
	 */
	boolean isSetDisableDefaultContainment();

	/**
	 * Returns the value of the '<em><b>All General Annotations</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MGeneralAnnotation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All General Annotations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All General Annotations</em>' reference list.
	 * @see com.montages.mcore.McorePackage#getMComponent_AllGeneralAnnotations()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::annotations::MGeneralAnnotation.allInstances()'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MGeneralAnnotation> getAllGeneralAnnotations();

	/**
	 * Returns the value of the '<em><b>Named Editor</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.MNamedEditor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Named Editor</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Named Editor</em>' containment reference list.
	 * @see #isSetNamedEditor()
	 * @see #unsetNamedEditor()
	 * @see com.montages.mcore.McorePackage#getMComponent_NamedEditor()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MNamedEditor> getNamedEditor();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getNamedEditor <em>Named Editor</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamedEditor()
	 * @see #getNamedEditor()
	 * @generated
	 */
	void unsetNamedEditor();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getNamedEditor <em>Named Editor</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Named Editor</em>' containment reference list is set.
	 * @see #unsetNamedEditor()
	 * @see #getNamedEditor()
	 * @generated
	 */
	boolean isSetNamedEditor();

	/**
	 * Returns the value of the '<em><b>Main Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Editor</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Editor</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMComponent_MainEditor()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if mainNamedEditor.oclIsUndefined()\n  then null\n  else mainNamedEditor.editor\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MEditor getMainEditor();

	/**
	 * Returns the value of the '<em><b>Use Legacy Editorconfig</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Use Legacy Editorconfig</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Use Legacy Editorconfig</em>' attribute.
	 * @see #isSetUseLegacyEditorconfig()
	 * @see #unsetUseLegacyEditorconfig()
	 * @see #setUseLegacyEditorconfig(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_UseLegacyEditorconfig()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Editor Configuration' createColumn='false'"
	 * @generated
	 */
	Boolean getUseLegacyEditorconfig();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Use Legacy Editorconfig</em>' attribute.
	 * @see #isSetUseLegacyEditorconfig()
	 * @see #unsetUseLegacyEditorconfig()
	 * @see #getUseLegacyEditorconfig()
	 * @generated
	 */
	void setUseLegacyEditorconfig(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUseLegacyEditorconfig()
	 * @see #getUseLegacyEditorconfig()
	 * @see #setUseLegacyEditorconfig(Boolean)
	 * @generated
	 */
	void unsetUseLegacyEditorconfig();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Use Legacy Editorconfig</em>' attribute is set.
	 * @see #unsetUseLegacyEditorconfig()
	 * @see #getUseLegacyEditorconfig()
	 * @see #setUseLegacyEditorconfig(Boolean)
	 * @generated
	 */
	boolean isSetUseLegacyEditorconfig();

	/**
	 * Returns the value of the '<em><b>Main Named Editor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Main Named Editor</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Main Named Editor</em>' reference.
	 * @see #isSetMainNamedEditor()
	 * @see #unsetMainNamedEditor()
	 * @see #setMainNamedEditor(MNamedEditor)
	 * @see com.montages.mcore.McorePackage#getMComponent_MainNamedEditor()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MNamedEditor getMainNamedEditor();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getMainNamedEditor <em>Main Named Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Main Named Editor</em>' reference.
	 * @see #isSetMainNamedEditor()
	 * @see #unsetMainNamedEditor()
	 * @see #getMainNamedEditor()
	 * @generated
	 */
	void setMainNamedEditor(MNamedEditor value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getMainNamedEditor <em>Main Named Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMainNamedEditor()
	 * @see #getMainNamedEditor()
	 * @see #setMainNamedEditor(MNamedEditor)
	 * @generated
	 */
	void unsetMainNamedEditor();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getMainNamedEditor <em>Main Named Editor</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Main Named Editor</em>' reference is set.
	 * @see #unsetMainNamedEditor()
	 * @see #getMainNamedEditor()
	 * @see #setMainNamedEditor(MNamedEditor)
	 * @generated
	 */
	boolean isSetMainNamedEditor();

	/**
	 * Returns the value of the '<em><b>Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Prefix</em>' attribute.
	 * @see #isSetNamePrefix()
	 * @see #unsetNamePrefix()
	 * @see #setNamePrefix(String)
	 * @see com.montages.mcore.McorePackage#getMComponent_NamePrefix()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	String getNamePrefix();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getNamePrefix <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Prefix</em>' attribute.
	 * @see #isSetNamePrefix()
	 * @see #unsetNamePrefix()
	 * @see #getNamePrefix()
	 * @generated
	 */
	void setNamePrefix(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getNamePrefix <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamePrefix()
	 * @see #getNamePrefix()
	 * @see #setNamePrefix(String)
	 * @generated
	 */
	void unsetNamePrefix();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getNamePrefix <em>Name Prefix</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name Prefix</em>' attribute is set.
	 * @see #unsetNamePrefix()
	 * @see #getNamePrefix()
	 * @see #setNamePrefix(String)
	 * @generated
	 */
	boolean isSetNamePrefix();

	/**
	 * Returns the value of the '<em><b>Derived Name Prefix</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Name Prefix</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Name Prefix</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_DerivedNamePrefix()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let chain: String = namePrefix in\nif chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() \n then null \n else chain.camelCaseUpper().trim()\n  endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedNamePrefix();

	/**
	 * Returns the value of the '<em><b>Name Prefix For Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name Prefix For Features</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name Prefix For Features</em>' attribute.
	 * @see #isSetNamePrefixForFeatures()
	 * @see #unsetNamePrefixForFeatures()
	 * @see #setNamePrefixForFeatures(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_NamePrefixForFeatures()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	Boolean getNamePrefixForFeatures();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getNamePrefixForFeatures <em>Name Prefix For Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name Prefix For Features</em>' attribute.
	 * @see #isSetNamePrefixForFeatures()
	 * @see #unsetNamePrefixForFeatures()
	 * @see #getNamePrefixForFeatures()
	 * @generated
	 */

	void setNamePrefixForFeatures(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getNamePrefixForFeatures <em>Name Prefix For Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetNamePrefixForFeatures()
	 * @see #getNamePrefixForFeatures()
	 * @see #setNamePrefixForFeatures(Boolean)
	 * @generated
	 */
	void unsetNamePrefixForFeatures();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getNamePrefixForFeatures <em>Name Prefix For Features</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name Prefix For Features</em>' attribute is set.
	 * @see #unsetNamePrefixForFeatures()
	 * @see #getNamePrefixForFeatures()
	 * @see #setNamePrefixForFeatures(Boolean)
	 * @generated
	 */
	boolean isSetNamePrefixForFeatures();

	/**
	 * Returns the value of the '<em><b>Derived Bundle Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Bundle Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Bundle Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_DerivedBundleName()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='(\nif domainType.oclIsUndefined() \n   then \'MISSING domainType\' \n   else derivedDomainType endif).concat(\n\'.\').concat(\nif domainName.oclIsUndefined() \n   then  \'MISSING domainName\' \n   else derivedDomainName endif).concat(\n\'.\').concat(\ncalculatedShortName.camelCaseLower().allLowerCase())'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Bundle Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedBundleName();

	/**
	 * Returns the value of the '<em><b>Derived URI</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived URI</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived URI</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_DerivedURI()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'http://www.\'.concat(\nif domainName.oclIsUndefined() \n   then  \'MISSING domainName\' \n   else derivedDomainName endif).concat(\n\'.\').concat(\nif domainType.oclIsUndefined() \n   then \'MISSING domainType\' \n   else derivedDomainType endif).concat(\n\'/\').concat(\neName)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Bundle Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedURI();

	/**
	 * Returns the value of the '<em><b>Represents Core Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Represents Core Component</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Represents Core Component</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_RepresentsCoreComponent()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainmentFeature().oclIsUndefined() \r\n  then false\r\n  else eContainmentFeature().name=\'core\' endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getRepresentsCoreComponent();

	/**
	 * Returns the value of the '<em><b>Root Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Root Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Root Package</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMComponent_RootPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='ownedPackage->asOrderedSet()->select(it: mcore::MPackage | let chain: Boolean = it.isRootPackage in\r\nif chain = true then true else false \r\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet()->at(1)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPackage getRootPackage();

	/**
	 * Returns the value of the '<em><b>Avoid Regeneration Of Editor Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute.
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @see com.montages.mcore.McorePackage#getMComponent_AvoidRegenerationOfEditorConfiguration()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Editor Configuration' createColumn='false'"
	 * @generated
	 */
	Boolean getAvoidRegenerationOfEditorConfiguration();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute.
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @generated
	 */
	void setAvoidRegenerationOfEditorConfiguration(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @generated
	 */
	void unsetAvoidRegenerationOfEditorConfiguration();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Avoid Regeneration Of Editor Configuration</em>' attribute is set.
	 * @see #unsetAvoidRegenerationOfEditorConfiguration()
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @see #setAvoidRegenerationOfEditorConfiguration(Boolean)
	 * @generated
	 */
	boolean isSetAvoidRegenerationOfEditorConfiguration();

	/**
	 * Returns the value of the '<em><b>Containing Repository</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Repository</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Repository</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMComponent_ContainingRepository()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else if self.eContainer().oclIsKindOf(MRepository)\r\n    then self.eContainer().oclAsType(MRepository)\r\n    else null endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MRepository getContainingRepository();

	/**
	 * Returns the value of the '<em><b>Domain Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Type</em>' attribute.
	 * @see #isSetDomainType()
	 * @see #unsetDomainType()
	 * @see #setDomainType(String)
	 * @see com.montages.mcore.McorePackage#getMComponent_DomainType()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='\'org\''"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Bundle Naming' createColumn='false'"
	 * @generated
	 */
	String getDomainType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getDomainType <em>Domain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Type</em>' attribute.
	 * @see #isSetDomainType()
	 * @see #unsetDomainType()
	 * @see #getDomainType()
	 * @generated
	 */
	void setDomainType(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getDomainType <em>Domain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDomainType()
	 * @see #getDomainType()
	 * @see #setDomainType(String)
	 * @generated
	 */
	void unsetDomainType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getDomainType <em>Domain Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domain Type</em>' attribute is set.
	 * @see #unsetDomainType()
	 * @see #getDomainType()
	 * @see #setDomainType(String)
	 * @generated
	 */
	boolean isSetDomainType();

	/**
	 * Returns the value of the '<em><b>Domain Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain Name</em>' attribute.
	 * @see #isSetDomainName()
	 * @see #unsetDomainName()
	 * @see #setDomainName(String)
	 * @see com.montages.mcore.McorePackage#getMComponent_DomainName()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL initValue='\'langlets\''"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Bundle Naming' createColumn='false'"
	 * @generated
	 */
	String getDomainName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getDomainName <em>Domain Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain Name</em>' attribute.
	 * @see #isSetDomainName()
	 * @see #unsetDomainName()
	 * @see #getDomainName()
	 * @generated
	 */
	void setDomainName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getDomainName <em>Domain Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDomainName()
	 * @see #getDomainName()
	 * @see #setDomainName(String)
	 * @generated
	 */
	void unsetDomainName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getDomainName <em>Domain Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Domain Name</em>' attribute is set.
	 * @see #unsetDomainName()
	 * @see #getDomainName()
	 * @see #setDomainName(String)
	 * @generated
	 */
	boolean isSetDomainName();

	/**
	 * Returns the value of the '<em><b>Derived Domain Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Domain Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Domain Type</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_DerivedDomainType()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let derivedOne:String = self.domainType.allLowerCase().trim() in\r\nSequence{1..(derivedOne.tokenize(\'.\')->size())}->iterate (\r\ni:Integer; c: String =\'\'|\r\nif i=1\r\nthen c.concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())))\r\nelse c.concat(\'.\').concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())-i+1))\r\nendif\r\n)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Bundle Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedDomainType();

	/**
	 * Returns the value of the '<em><b>Derived Domain Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Derived Domain Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Derived Domain Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMComponent_DerivedDomainName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let derivedOne:String = self.domainName.allLowerCase().trim() in\r\nSequence{1..(derivedOne.tokenize(\'.\')->size())}->iterate (\r\ni:Integer; c: String =\'\'|\r\nif i=1\r\nthen c.concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())))\r\nelse c.concat(\'.\').concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())-i+1))\r\nendif\r\n)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Bundle Naming'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getDerivedDomainName();

	/**
	 * Returns the value of the '<em><b>Special Bundle Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * This fields stores file name for the ECore file containing th epackage (only for root packages), in case this name does not follow standard naming.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Special Bundle Name</em>' attribute.
	 * @see #isSetSpecialBundleName()
	 * @see #unsetSpecialBundleName()
	 * @see #setSpecialBundleName(String)
	 * @see com.montages.mcore.McorePackage#getMComponent_SpecialBundleName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special ECore Settings' createColumn='false'"
	 * @generated
	 */
	String getSpecialBundleName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MComponent#getSpecialBundleName <em>Special Bundle Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special Bundle Name</em>' attribute.
	 * @see #isSetSpecialBundleName()
	 * @see #unsetSpecialBundleName()
	 * @see #getSpecialBundleName()
	 * @generated
	 */
	void setSpecialBundleName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MComponent#getSpecialBundleName <em>Special Bundle Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpecialBundleName()
	 * @see #getSpecialBundleName()
	 * @see #setSpecialBundleName(String)
	 * @generated
	 */
	void unsetSpecialBundleName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MComponent#getSpecialBundleName <em>Special Bundle Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Special Bundle Name</em>' attribute is set.
	 * @see #unsetSpecialBundleName()
	 * @see #getSpecialBundleName()
	 * @see #setSpecialBundleName(String)
	 * @generated
	 */
	boolean isSetSpecialBundleName();

} // MComponent
