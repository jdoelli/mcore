/**
 */
package com.montages.mcore;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MExplicitly Typed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getTypePackage <em>Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getSingular <em>Singular</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getETypeName <em>EType Name</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getETypeLabel <em>EType Label</em>}</li>
 *   <li>{@link com.montages.mcore.MExplicitlyTyped#getCorrectlyTyped <em>Correctly Typed</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL calculatedMandatoryDerive='self.mandatory' calculatedTypeDerive='self.type' calculatedSingularDerive='self.singular' simpleTypeIsCorrectDerive='if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* does not work because of bug, is repeated in MProperty and MParameter... \052/' calculatedSimpleTypeDerive='simpleType\n'"
 * @generated
 */

public interface MExplicitlyTyped extends MTyped, MHasSimpleType {
	/**
	 * Returns the value of the '<em><b>Type Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Package</em>' reference.
	 * @see #isSetTypePackage()
	 * @see #unsetTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_TypePackage()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Additional' createColumn='false'"
	 * @generated
	 */
	MPackage getTypePackage();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getTypePackage <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type Package</em>' reference.
	 * @see #isSetTypePackage()
	 * @see #unsetTypePackage()
	 * @see #getTypePackage()
	 * @generated
	 */
	void setTypePackage(MPackage value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getTypePackage <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetTypePackage()
	 * @see #getTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @generated
	 */
	void unsetTypePackage();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MExplicitlyTyped#getTypePackage <em>Type Package</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type Package</em>' reference is set.
	 * @see #unsetTypePackage()
	 * @see #getTypePackage()
	 * @see #setTypePackage(MPackage)
	 * @generated
	 */
	boolean isSetTypePackage();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(MClassifier)
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_Type()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstraint='trg.selectableClassifier(\r\n  if self.type.oclIsUndefined() then OrderedSet{} else OrderedSet{self.type} endif,\r\n  self.typePackage)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 * @generated
	 */
	MClassifier getType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MExplicitlyTyped#getType <em>Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' reference is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Mandatory</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Mandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Mandatory</em>' attribute.
	 * @see #isSetMandatory()
	 * @see #unsetMandatory()
	 * @see #setMandatory(Boolean)
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_Mandatory()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getMandatory();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Mandatory</em>' attribute.
	 * @see #isSetMandatory()
	 * @see #unsetMandatory()
	 * @see #getMandatory()
	 * @generated
	 */
	void setMandatory(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getMandatory <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMandatory()
	 * @see #getMandatory()
	 * @see #setMandatory(Boolean)
	 * @generated
	 */
	void unsetMandatory();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MExplicitlyTyped#getMandatory <em>Mandatory</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Mandatory</em>' attribute is set.
	 * @see #unsetMandatory()
	 * @see #getMandatory()
	 * @see #setMandatory(Boolean)
	 * @generated
	 */
	boolean isSetMandatory();

	/**
	 * Returns the value of the '<em><b>Singular</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Singular</em>' attribute.
	 * @see #isSetSingular()
	 * @see #unsetSingular()
	 * @see #setSingular(Boolean)
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_Singular()
	 * @model default="false" unsettable="true"
	 *        annotation="http://www.xocl.org/OCL initValue='true'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Typing'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getSingular();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getSingular <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Singular</em>' attribute.
	 * @see #isSetSingular()
	 * @see #unsetSingular()
	 * @see #getSingular()
	 * @generated
	 */
	void setSingular(Boolean value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MExplicitlyTyped#getSingular <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSingular()
	 * @see #getSingular()
	 * @see #setSingular(Boolean)
	 * @generated
	 */
	void unsetSingular();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MExplicitlyTyped#getSingular <em>Singular</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Singular</em>' attribute is set.
	 * @see #unsetSingular()
	 * @see #getSingular()
	 * @see #setSingular(Boolean)
	 * @generated
	 */
	boolean isSetSingular();

	/**
	 * Returns the value of the '<em><b>EType Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EType Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EType Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_ETypeName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if type.oclIsUndefined() then\r\n  if  simpleType=SimpleType::None then \r\n    \'TYPE_MISSING\'\r\n  else  simpleTypeString endif\r\nelse type.eName endif '"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getETypeName();

	/**
	 * Returns the value of the '<em><b>EType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EType Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EType Label</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_ETypeLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.type.oclIsUndefined() and self.simpleType=SimpleType::None and self.voidTypeAllowed then \'\' else eTypeName.concat(if self.singular then \'\' else \'*\' endif) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getETypeLabel();

	/**
	 * Returns the value of the '<em><b>Correctly Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correctly Typed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correctly Typed</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMExplicitlyTyped_CorrectlyTyped()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='simpleTypeIsCorrect'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCorrectlyTyped();

} // MExplicitlyTyped
