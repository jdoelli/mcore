/**
 */
package com.montages.mcore.impl;

import com.montages.mcore.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class McoreFactoryImpl extends EFactoryImpl implements McoreFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static McoreFactory init() {
		try {
			McoreFactory theMcoreFactory = (McoreFactory) EPackage.Registry.INSTANCE
					.getEFactory(McorePackage.eNS_URI);
			if (theMcoreFactory != null) {
				return theMcoreFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new McoreFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McoreFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case McorePackage.MREPOSITORY:
			return createMRepository();
		case McorePackage.MCOMPONENT:
			return createMComponent();
		case McorePackage.MPACKAGE:
			return createMPackage();
		case McorePackage.MCLASSIFIER:
			return createMClassifier();
		case McorePackage.MLITERAL:
			return createMLiteral();
		case McorePackage.MPROPERTIES_GROUP:
			return createMPropertiesGroup();
		case McorePackage.MPROPERTY:
			return createMProperty();
		case McorePackage.MOPERATION_SIGNATURE:
			return createMOperationSignature();
		case McorePackage.MPARAMETER:
			return createMParameter();
		case McorePackage.MNAMED_EDITOR:
			return createMNamedEditor();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case McorePackage.MCOMPONENT_ACTION:
			return createMComponentActionFromString(eDataType, initialValue);
		case McorePackage.TOP_LEVEL_DOMAIN:
			return createTopLevelDomainFromString(eDataType, initialValue);
		case McorePackage.MPACKAGE_ACTION:
			return createMPackageActionFromString(eDataType, initialValue);
		case McorePackage.SIMPLE_TYPE:
			return createSimpleTypeFromString(eDataType, initialValue);
		case McorePackage.MCLASSIFIER_ACTION:
			return createMClassifierActionFromString(eDataType, initialValue);
		case McorePackage.ORDERING_STRATEGY:
			return createOrderingStrategyFromString(eDataType, initialValue);
		case McorePackage.MLITERAL_ACTION:
			return createMLiteralActionFromString(eDataType, initialValue);
		case McorePackage.MPROPERTIES_GROUP_ACTION:
			return createMPropertiesGroupActionFromString(eDataType,
					initialValue);
		case McorePackage.MPROPERTY_ACTION:
			return createMPropertyActionFromString(eDataType, initialValue);
		case McorePackage.PROPERTY_BEHAVIOR:
			return createPropertyBehaviorFromString(eDataType, initialValue);
		case McorePackage.MOPERATION_SIGNATURE_ACTION:
			return createMOperationSignatureActionFromString(eDataType,
					initialValue);
		case McorePackage.MULTIPLICITY_CASE:
			return createMultiplicityCaseFromString(eDataType, initialValue);
		case McorePackage.CLASSIFIER_KIND:
			return createClassifierKindFromString(eDataType, initialValue);
		case McorePackage.PROPERTY_KIND:
			return createPropertyKindFromString(eDataType, initialValue);
		case McorePackage.MPARAMETER_ACTION:
			return createMParameterActionFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case McorePackage.MCOMPONENT_ACTION:
			return convertMComponentActionToString(eDataType, instanceValue);
		case McorePackage.TOP_LEVEL_DOMAIN:
			return convertTopLevelDomainToString(eDataType, instanceValue);
		case McorePackage.MPACKAGE_ACTION:
			return convertMPackageActionToString(eDataType, instanceValue);
		case McorePackage.SIMPLE_TYPE:
			return convertSimpleTypeToString(eDataType, instanceValue);
		case McorePackage.MCLASSIFIER_ACTION:
			return convertMClassifierActionToString(eDataType, instanceValue);
		case McorePackage.ORDERING_STRATEGY:
			return convertOrderingStrategyToString(eDataType, instanceValue);
		case McorePackage.MLITERAL_ACTION:
			return convertMLiteralActionToString(eDataType, instanceValue);
		case McorePackage.MPROPERTIES_GROUP_ACTION:
			return convertMPropertiesGroupActionToString(eDataType,
					instanceValue);
		case McorePackage.MPROPERTY_ACTION:
			return convertMPropertyActionToString(eDataType, instanceValue);
		case McorePackage.PROPERTY_BEHAVIOR:
			return convertPropertyBehaviorToString(eDataType, instanceValue);
		case McorePackage.MOPERATION_SIGNATURE_ACTION:
			return convertMOperationSignatureActionToString(eDataType,
					instanceValue);
		case McorePackage.MULTIPLICITY_CASE:
			return convertMultiplicityCaseToString(eDataType, instanceValue);
		case McorePackage.CLASSIFIER_KIND:
			return convertClassifierKindToString(eDataType, instanceValue);
		case McorePackage.PROPERTY_KIND:
			return convertPropertyKindToString(eDataType, instanceValue);
		case McorePackage.MPARAMETER_ACTION:
			return convertMParameterActionToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRepository createMRepository() {
		MRepositoryImpl mRepository = new MRepositoryImpl();
		return mRepository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MComponent createMComponent() {
		MComponentImpl mComponent = new MComponentImpl();
		return mComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage createMPackage() {
		MPackageImpl mPackage = new MPackageImpl();
		return mPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier createMClassifier() {
		MClassifierImpl mClassifier = new MClassifierImpl();
		return mClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteral createMLiteral() {
		MLiteralImpl mLiteral = new MLiteralImpl();
		return mLiteral;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesGroup createMPropertiesGroup() {
		MPropertiesGroupImpl mPropertiesGroup = new MPropertiesGroupImpl();
		return mPropertiesGroup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty createMProperty() {
		MPropertyImpl mProperty = new MPropertyImpl();
		return mProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature createMOperationSignature() {
		MOperationSignatureImpl mOperationSignature = new MOperationSignatureImpl();
		return mOperationSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MParameter createMParameter() {
		MParameterImpl mParameter = new MParameterImpl();
		return mParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedEditor createMNamedEditor() {
		MNamedEditorImpl mNamedEditor = new MNamedEditorImpl();
		return mNamedEditor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MComponentAction createMComponentActionFromString(
			EDataType eDataType, String initialValue) {
		MComponentAction result = MComponentAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMComponentActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TopLevelDomain createTopLevelDomainFromString(EDataType eDataType,
			String initialValue) {
		TopLevelDomain result = TopLevelDomain.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTopLevelDomainToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackageAction createMPackageActionFromString(EDataType eDataType,
			String initialValue) {
		MPackageAction result = MPackageAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMPackageActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType createSimpleTypeFromString(EDataType eDataType,
			String initialValue) {
		SimpleType result = SimpleType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertSimpleTypeToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAction createMClassifierActionFromString(
			EDataType eDataType, String initialValue) {
		MClassifierAction result = MClassifierAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMClassifierActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrderingStrategy createOrderingStrategyFromString(
			EDataType eDataType, String initialValue) {
		OrderingStrategy result = OrderingStrategy.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertOrderingStrategyToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralAction createMLiteralActionFromString(EDataType eDataType,
			String initialValue) {
		MLiteralAction result = MLiteralAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMLiteralActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesGroupAction createMPropertiesGroupActionFromString(
			EDataType eDataType, String initialValue) {
		MPropertiesGroupAction result = MPropertiesGroupAction
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMPropertiesGroupActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAction createMPropertyActionFromString(EDataType eDataType,
			String initialValue) {
		MPropertyAction result = MPropertyAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMPropertyActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyBehavior createPropertyBehaviorFromString(
			EDataType eDataType, String initialValue) {
		PropertyBehavior result = PropertyBehavior.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPropertyBehaviorToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignatureAction createMOperationSignatureActionFromString(
			EDataType eDataType, String initialValue) {
		MOperationSignatureAction result = MOperationSignatureAction
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMOperationSignatureActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityCase createMultiplicityCaseFromString(
			EDataType eDataType, String initialValue) {
		MultiplicityCase result = MultiplicityCase.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMultiplicityCaseToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifierKind createClassifierKindFromString(EDataType eDataType,
			String initialValue) {
		ClassifierKind result = ClassifierKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertClassifierKindToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyKind createPropertyKindFromString(EDataType eDataType,
			String initialValue) {
		PropertyKind result = PropertyKind.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPropertyKindToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MParameterAction createMParameterActionFromString(
			EDataType eDataType, String initialValue) {
		MParameterAction result = MParameterAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMParameterActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McorePackage getMcorePackage() {
		return (McorePackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static McorePackage getPackage() {
		return McorePackage.eINSTANCE;
	}

} //McoreFactoryImpl
