
/**
 */
package com.montages.mcore.impl;

import com.montages.mcore.MEditor;
import com.montages.mcore.MNamedEditor;
import com.montages.mcore.McorePackage;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MNamed Editor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MNamedEditorImpl#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MNamedEditorImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MNamedEditorImpl#getUserVisible <em>User Visible</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MNamedEditorImpl#getEditor <em>Editor</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MNamedEditorImpl extends MinimalEObjectImpl.Container
		implements MNamedEditor {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * This is true if the Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean nameESet;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * This is true if the Label attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean labelESet;

	/**
	 * The default value of the '{@link #getUserVisible() <em>User Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserVisible()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USER_VISIBLE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUserVisible() <em>User Visible</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUserVisible()
	 * @generated
	 * @ordered
	 */
	protected Boolean userVisible = USER_VISIBLE_EDEFAULT;

	/**
	 * This is true if the User Visible attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean userVisibleESet;

	/**
	 * The cached value of the '{@link #getEditor() <em>Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEditor()
	 * @generated
	 * @ordered
	 */
	protected MEditor editor;

	/**
	 * This is true if the Editor reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean editorESet;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MNamedEditorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MNAMED_EDITOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		boolean oldNameESet = nameESet;
		nameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MNAMED_EDITOR__NAME, oldName, name,
					!oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetName() {
		String oldName = name;
		boolean oldNameESet = nameESet;
		name = NAME_EDEFAULT;
		nameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MNAMED_EDITOR__NAME, oldName, NAME_EDEFAULT,
					oldNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetName() {
		return nameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		boolean oldLabelESet = labelESet;
		labelESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MNAMED_EDITOR__LABEL, oldLabel, label,
					!oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLabel() {
		String oldLabel = label;
		boolean oldLabelESet = labelESet;
		label = LABEL_EDEFAULT;
		labelESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MNAMED_EDITOR__LABEL, oldLabel, LABEL_EDEFAULT,
					oldLabelESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLabel() {
		return labelESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUserVisible() {
		return userVisible;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUserVisible(Boolean newUserVisible) {
		Boolean oldUserVisible = userVisible;
		userVisible = newUserVisible;
		boolean oldUserVisibleESet = userVisibleESet;
		userVisibleESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MNAMED_EDITOR__USER_VISIBLE, oldUserVisible,
					userVisible, !oldUserVisibleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUserVisible() {
		Boolean oldUserVisible = userVisible;
		boolean oldUserVisibleESet = userVisibleESet;
		userVisible = USER_VISIBLE_EDEFAULT;
		userVisibleESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MNAMED_EDITOR__USER_VISIBLE, oldUserVisible,
					USER_VISIBLE_EDEFAULT, oldUserVisibleESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUserVisible() {
		return userVisibleESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MEditor getEditor() {
		if (editor != null && editor.eIsProxy()) {
			InternalEObject oldEditor = (InternalEObject) editor;
			editor = (MEditor) eResolveProxy(oldEditor);
			if (editor != oldEditor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MNAMED_EDITOR__EDITOR, oldEditor,
							editor));
			}
		}
		return editor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MEditor basicGetEditor() {
		return editor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEditor(MEditor newEditor) {
		MEditor oldEditor = editor;
		editor = newEditor;
		boolean oldEditorESet = editorESet;
		editorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MNAMED_EDITOR__EDITOR, oldEditor, editor,
					!oldEditorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEditor() {
		MEditor oldEditor = editor;
		boolean oldEditorESet = editorESet;
		editor = null;
		editorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MNAMED_EDITOR__EDITOR, oldEditor, null,
					oldEditorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEditor() {
		return editorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MNAMED_EDITOR__NAME:
			return getName();
		case McorePackage.MNAMED_EDITOR__LABEL:
			return getLabel();
		case McorePackage.MNAMED_EDITOR__USER_VISIBLE:
			return getUserVisible();
		case McorePackage.MNAMED_EDITOR__EDITOR:
			if (resolve)
				return getEditor();
			return basicGetEditor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MNAMED_EDITOR__NAME:
			setName((String) newValue);
			return;
		case McorePackage.MNAMED_EDITOR__LABEL:
			setLabel((String) newValue);
			return;
		case McorePackage.MNAMED_EDITOR__USER_VISIBLE:
			setUserVisible((Boolean) newValue);
			return;
		case McorePackage.MNAMED_EDITOR__EDITOR:
			setEditor((MEditor) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MNAMED_EDITOR__NAME:
			unsetName();
			return;
		case McorePackage.MNAMED_EDITOR__LABEL:
			unsetLabel();
			return;
		case McorePackage.MNAMED_EDITOR__USER_VISIBLE:
			unsetUserVisible();
			return;
		case McorePackage.MNAMED_EDITOR__EDITOR:
			unsetEditor();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MNAMED_EDITOR__NAME:
			return isSetName();
		case McorePackage.MNAMED_EDITOR__LABEL:
			return isSetLabel();
		case McorePackage.MNAMED_EDITOR__USER_VISIBLE:
			return isSetUserVisible();
		case McorePackage.MNAMED_EDITOR__EDITOR:
			return isSetEditor();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		if (nameESet)
			result.append(name);
		else
			result.append("<unset>");
		result.append(", label: ");
		if (labelESet)
			result.append(label);
		else
			result.append("<unset>");
		result.append(", userVisible: ");
		if (userVisibleESet)
			result.append(userVisible);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MNamedEditorImpl
