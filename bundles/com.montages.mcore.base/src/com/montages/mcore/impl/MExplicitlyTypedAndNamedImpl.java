/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.MExplicitlyTypedAndNamed;
import com.montages.mcore.MHasSimpleType;
import com.montages.mcore.MPackage;
import com.montages.mcore.MTyped;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.SimpleType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MExplicitly Typed And Named</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getMultiplicityAsString <em>Multiplicity As String</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCalculatedType <em>Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCalculatedMandatory <em>Calculated Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCalculatedSingular <em>Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCalculatedTypePackage <em>Calculated Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getVoidTypeAllowed <em>Void Type Allowed</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCalculatedSimpleType <em>Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getMultiplicityCase <em>Multiplicity Case</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getSimpleTypeString <em>Simple Type String</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getHasSimpleDataType <em>Has Simple Data Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getHasSimpleModelingType <em>Has Simple Modeling Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getSimpleType <em>Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getTypePackage <em>Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getMandatory <em>Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getSingular <em>Singular</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getETypeName <em>EType Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getETypeLabel <em>EType Label</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getCorrectlyTyped <em>Correctly Typed</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MExplicitlyTypedAndNamedImpl#getAppendTypeNameToName <em>Append Type Name To Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MExplicitlyTypedAndNamedImpl extends MNamedImpl
		implements MExplicitlyTypedAndNamed, IXoclInitializable {
	/**
	 * The default value of the '{@link #getMultiplicityAsString() <em>Multiplicity As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString()
	 * @generated
	 * @ordered
	 */
	protected static final String MULTIPLICITY_AS_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedMandatory() <em>Calculated Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSingular() <em>Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getVoidTypeAllowed() <em>Void Type Allowed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean VOID_TYPE_ALLOWED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSimpleType() <em>Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CALCULATED_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getMultiplicityCase() <em>Multiplicity Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase()
	 * @generated
	 * @ordered
	 */
	protected static final MultiplicityCase MULTIPLICITY_CASE_EDEFAULT = MultiplicityCase.ZERO_ONE;

	/**
	 * The default value of the '{@link #getSimpleTypeString() <em>Simple Type String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeString()
	 * @generated
	 * @ordered
	 */
	protected static final String SIMPLE_TYPE_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getHasSimpleDataType() <em>Has Simple Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSimpleDataType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_SIMPLE_DATA_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getHasSimpleModelingType() <em>Has Simple Modeling Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSimpleModelingType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_SIMPLE_MODELING_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSimpleTypeIsCorrect() <em>Simple Type Is Correct</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeIsCorrect()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SIMPLE_TYPE_IS_CORRECT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The cached value of the '{@link #getSimpleType() <em>Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleType()
	 * @generated
	 * @ordered
	 */
	protected SimpleType simpleType = SIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the Simple Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean simpleTypeESet;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier type;

	/**
	 * This is true if the Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typeESet;

	/**
	 * The cached value of the '{@link #getTypePackage() <em>Type Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypePackage()
	 * @generated
	 * @ordered
	 */
	protected MPackage typePackage;

	/**
	 * This is true if the Type Package reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean typePackageESet;

	/**
	 * The default value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean MANDATORY_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getMandatory() <em>Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMandatory()
	 * @generated
	 * @ordered
	 */
	protected Boolean mandatory = MANDATORY_EDEFAULT;

	/**
	 * This is true if the Mandatory attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mandatoryESet;

	/**
	 * The default value of the '{@link #getSingular() <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SINGULAR_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getSingular() <em>Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSingular()
	 * @generated
	 * @ordered
	 */
	protected Boolean singular = SINGULAR_EDEFAULT;

	/**
	 * This is true if the Singular attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean singularESet;

	/**
	 * The default value of the '{@link #getETypeName() <em>EType Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getETypeName()
	 * @generated
	 * @ordered
	 */
	protected static final String ETYPE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getETypeLabel() <em>EType Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getETypeLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ETYPE_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCorrectlyTyped() <em>Correctly Typed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectlyTyped()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CORRECTLY_TYPED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAppendTypeNameToName() <em>Append Type Name To Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppendTypeNameToName()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean APPEND_TYPE_NAME_TO_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAppendTypeNameToName() <em>Append Type Name To Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAppendTypeNameToName()
	 * @generated
	 * @ordered
	 */
	protected Boolean appendTypeNameToName = APPEND_TYPE_NAME_TO_NAME_EDEFAULT;

	/**
	 * This is true if the Append Type Name To Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean appendTypeNameToNameESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #nameFromType <em>Name From Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #nameFromType
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression nameFromTypeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #shortNameFromType <em>Short Name From Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #shortNameFromType
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression shortNameFromTypeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #simpleTypeAsString <em>Simple Type As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #simpleTypeAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression simpleTypeAsStringmcoreSimpleTypeBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #multiplicityCase$Update <em>Multiplicity Case$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #multiplicityCase$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression multiplicityCase$UpdatemcoreMultiplicityCaseBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #typeAsOcl <em>Type As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #typeAsOcl
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityAsString <em>Multiplicity As String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityAsStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedTypePackage <em>Calculated Type Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedTypePackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypePackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getVoidTypeAllowed <em>Void Type Allowed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression voidTypeAllowedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityCase <em>Multiplicity Case</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityCaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeString <em>Simple Type String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression simpleTypeStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getHasSimpleDataType <em>Has Simple Data Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSimpleDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression hasSimpleDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getHasSimpleModelingType <em>Has Simple Modeling Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHasSimpleModelingType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression hasSimpleModelingTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeIsCorrect
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression simpleTypeIsCorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getType <em>Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression typeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getETypeName <em>EType Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getETypeName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eTypeNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getETypeLabel <em>EType Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getETypeLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eTypeLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectlyTyped <em>Correctly Typed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectlyTyped
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression correctlyTypedDeriveOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MExplicitlyTypedAndNamedImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMultiplicityAsString() {
		/**
		 * @OCL (if self.calculatedMandatory then '[1..' else '[0..' endif)
		.concat(if self.calculatedSingular then '1]' else '*]' endif)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_AS_STRING;

		if (multiplicityAsStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityAsStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityAsStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCalculatedType() {
		MClassifier calculatedType = basicGetCalculatedType();
		return calculatedType != null && calculatedType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) calculatedType)
				: calculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCalculatedType() {
		/**
		 * @OCL let res:MClassifier=null in res
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedMandatory() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedSingular() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getCalculatedTypePackage() {
		MPackage calculatedTypePackage = basicGetCalculatedTypePackage();
		return calculatedTypePackage != null && calculatedTypePackage.eIsProxy()
				? (MPackage) eResolveProxy(
						(InternalEObject) calculatedTypePackage)
				: calculatedTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetCalculatedTypePackage() {
		/**
		 * @OCL if self.calculatedType.oclIsUndefined() then null else self.calculatedType.containingPackage endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE_PACKAGE;

		if (calculatedTypePackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypePackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypePackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getVoidTypeAllowed() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED;

		if (voidTypeAllowedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				voidTypeAllowedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(voidTypeAllowedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getCalculatedSimpleType() {
		/**
		 * @OCL mcore::SimpleType::None
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityCase getMultiplicityCase() {
		/**
		 * @OCL if self.calculatedMandatory and self.calculatedSingular
		then MultiplicityCase::OneOne
		else if (not self.calculatedMandatory) and self.calculatedSingular
		then MultiplicityCase::ZeroOne
		else if self.calculatedMandatory and (not self.calculatedSingular)
		then MultiplicityCase::OneMany
		else MultiplicityCase::ZeroMany endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_CASE;

		if (multiplicityCaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityCaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityCaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MultiplicityCase result = (MultiplicityCase) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setMultiplicityCase(MultiplicityCase newMultiplicityCase) {
		// overridden from MTyped class. Added to both MExplicitlyTypedImple AND 
		// MExplicitlyTypedAndNamedImpl, as the second does not directly inherit...
		switch (newMultiplicityCase) {
		case ONE_MANY:
			this.setMandatory(true);
			this.setSingular(false);
			break;
		case ONE_ONE:
			this.setMandatory(true);
			this.setSingular(true);
			break;
		case ZERO_MANY:
			this.setMandatory(false);
			this.setSingular(false);
			break;
		case ZERO_ONE:
			this.setMandatory(false);
			this.setSingular(true);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSimpleTypeString() {
		/**
		 * @OCL self.simpleTypeAsString(self.simpleType)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING;

		if (simpleTypeStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				simpleTypeStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getSimpleType() {
		return simpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setSimpleType(SimpleType newSimpleType) {
		SimpleType oldSimpleType = simpleType;
		simpleType = newSimpleType == null ? SIMPLE_TYPE_EDEFAULT
				: newSimpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE,
					oldSimpleType, simpleType, !oldSimpleTypeESet));
		//added by PWK to unset type, if new simpleType != SimpleType.NONE
		if (newSimpleType != SimpleType.NONE)
			this.unsetType();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSimpleType() {
		SimpleType oldSimpleType = simpleType;
		boolean oldSimpleTypeESet = simpleTypeESet;
		simpleType = SIMPLE_TYPE_EDEFAULT;
		simpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE,
					oldSimpleType, SIMPLE_TYPE_EDEFAULT, oldSimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSimpleType() {
		return simpleTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasSimpleDataType() {
		/**
		 * @OCL  simpleType=SimpleType::String or
		simpleType=SimpleType::Boolean or
		simpleType=SimpleType::Date or
		simpleType=SimpleType::Double or
		simpleType=SimpleType::Integer or 
		simpleType= SimpleType::Any
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE;

		if (hasSimpleDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				hasSimpleDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(hasSimpleDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasSimpleModelingType() {
		/**
		 * @OCL  not (simpleType=SimpleType::None or hasSimpleDataType)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE;

		if (hasSimpleModelingTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				hasSimpleModelingTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(hasSimpleModelingTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSimpleTypeIsCorrect() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;

		if (simpleTypeIsCorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				simpleTypeIsCorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeIsCorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getTypePackage() {
		if (typePackage != null && typePackage.eIsProxy()) {
			InternalEObject oldTypePackage = (InternalEObject) typePackage;
			typePackage = (MPackage) eResolveProxy(oldTypePackage);
			if (typePackage != oldTypePackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE,
							oldTypePackage, typePackage));
			}
		}
		return typePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetTypePackage() {
		return typePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypePackage(MPackage newTypePackage) {
		MPackage oldTypePackage = typePackage;
		typePackage = newTypePackage;
		boolean oldTypePackageESet = typePackageESet;
		typePackageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE,
					oldTypePackage, typePackage, !oldTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTypePackage() {
		MPackage oldTypePackage = typePackage;
		boolean oldTypePackageESet = typePackageESet;
		typePackage = null;
		typePackageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE,
					oldTypePackage, null, oldTypePackageESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTypePackage() {
		return typePackageESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getType() {
		if (type != null && type.eIsProxy()) {
			InternalEObject oldType = (InternalEObject) type;
			type = (MClassifier) eResolveProxy(oldType);
			if (type != oldType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE,
							oldType, type));
			}
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetType() {
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MClassifier newType) {
		MClassifier oldType = type;
		type = newType;
		boolean oldTypeESet = typeESet;
		typeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE, oldType,
					type, !oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetType() {
		MClassifier oldType = type;
		boolean oldTypeESet = typeESet;
		type = null;
		typeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE, oldType,
					null, oldTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetType() {
		return typeESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Type</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg.selectableClassifier(
	if self.type.oclIsUndefined() then OrderedSet{} else OrderedSet{self.type} endif,
	self.typePackage)
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		if (typeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MEXPLICITLY_TYPED__TYPE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				typeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"TypeChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(typeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"TypeChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getMandatory() {
		return mandatory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMandatory(Boolean newMandatory) {
		Boolean oldMandatory = mandatory;
		mandatory = newMandatory;
		boolean oldMandatoryESet = mandatoryESet;
		mandatoryESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY,
					oldMandatory, mandatory, !oldMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMandatory() {
		Boolean oldMandatory = mandatory;
		boolean oldMandatoryESet = mandatoryESet;
		mandatory = MANDATORY_EDEFAULT;
		mandatoryESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY,
					oldMandatory, MANDATORY_EDEFAULT, oldMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMandatory() {
		return mandatoryESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSingular() {
		return singular;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSingular(Boolean newSingular) {
		Boolean oldSingular = singular;
		singular = newSingular;
		boolean oldSingularESet = singularESet;
		singularESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR,
					oldSingular, singular, !oldSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSingular() {
		Boolean oldSingular = singular;
		boolean oldSingularESet = singularESet;
		singular = SINGULAR_EDEFAULT;
		singularESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR,
					oldSingular, SINGULAR_EDEFAULT, oldSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSingular() {
		return singularESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getETypeName() {
		/**
		 * @OCL if type.oclIsUndefined() then
		if  simpleType=SimpleType::None then 
		'TYPE_MISSING'
		else  simpleTypeString endif
		else type.eName endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MEXPLICITLY_TYPED__ETYPE_NAME;

		if (eTypeNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eTypeNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eTypeNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getETypeLabel() {
		/**
		 * @OCL if self.type.oclIsUndefined() and self.simpleType=SimpleType::None and self.voidTypeAllowed then '' else eTypeName.concat(if self.singular then '' else '*' endif) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MEXPLICITLY_TYPED__ETYPE_LABEL;

		if (eTypeLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eTypeLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eTypeLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCorrectlyTyped() {
		/**
		 * @OCL simpleTypeIsCorrect
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED;
		EStructuralFeature eFeature = McorePackage.Literals.MEXPLICITLY_TYPED__CORRECTLY_TYPED;

		if (correctlyTypedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				correctlyTypedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctlyTypedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAppendTypeNameToName() {
		return appendTypeNameToName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAppendTypeNameToName(Boolean newAppendTypeNameToName) {
		Boolean oldAppendTypeNameToName = appendTypeNameToName;
		appendTypeNameToName = newAppendTypeNameToName;
		boolean oldAppendTypeNameToNameESet = appendTypeNameToNameESet;
		appendTypeNameToNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME,
					oldAppendTypeNameToName, appendTypeNameToName,
					!oldAppendTypeNameToNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAppendTypeNameToName() {
		Boolean oldAppendTypeNameToName = appendTypeNameToName;
		boolean oldAppendTypeNameToNameESet = appendTypeNameToNameESet;
		appendTypeNameToName = APPEND_TYPE_NAME_TO_NAME_EDEFAULT;
		appendTypeNameToNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME,
					oldAppendTypeNameToName, APPEND_TYPE_NAME_TO_NAME_EDEFAULT,
					oldAppendTypeNameToNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAppendTypeNameToName() {
		return appendTypeNameToNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String nameFromType() {

		/**
		 * @OCL if not type.oclIsUndefined() 
		then type.calculatedName
		else if  self.simpleType <> SimpleType::None
		then self.simpleType.toString().concat(' Property')
		else ' ERROR UNNAMED UNTYPED' endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED);
		EOperation eOperation = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED
				.getEOperations().get(0);
		if (nameFromTypeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				nameFromTypeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(nameFromTypeBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String shortNameFromType() {

		/**
		 * @OCL if not type.oclIsUndefined() 
		then type.calculatedShortName
		else if  self.simpleType <> SimpleType::None
		then self.simpleType.toString().concat(' Property')
		else ' ERROR UNNAMED UNTYPED' endif endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED);
		EOperation eOperation = McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED
				.getEOperations().get(1);
		if (shortNameFromTypeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				shortNameFromTypeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(shortNameFromTypeBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String simpleTypeAsString(SimpleType simpleType) {

		/**
		 * @OCL if  simpleType=SimpleType::None then '-' else
		if  simpleType=SimpleType::String then 'String' else
		if simpleType=SimpleType::Boolean then 'Boolean' else
		if  simpleType=SimpleType::Date then 'Date' else
		if  simpleType=SimpleType::Double then 'Double' else
		if  simpleType=SimpleType::Integer then 'Integer' else
		if  simpleType=SimpleType::Annotation then 'Annotation' else
		if  simpleType=SimpleType::Attribute then 'Attribute' else
		if  simpleType=SimpleType::Class then 'Class' else
		if  simpleType=SimpleType::Classifier then 'Classifier' else
		if  simpleType=SimpleType::DataType then 'Data Type' else
		if simpleType=SimpleType::Enumeration then 'Enumeration' else
		if  simpleType=SimpleType::Feature then 'Feature' else
		if  simpleType=SimpleType::KeyValue then 'KeyValue' else
		if  simpleType=SimpleType::Literal then 'Literal' else
		if  simpleType=SimpleType::Object then 'EObject' else
		if simpleType= SimpleType::Any then 'Object' else
		if  simpleType=SimpleType::Operation then 'Operation' else
		if simpleType=SimpleType::Package then 'Package' else
		if  simpleType=SimpleType::Parameter then 'Parameter' else
		if  simpleType=SimpleType::Reference then 'Reference' else
		if simpleType=SimpleType::NamedElement then 'Named Element' else
		if simpleType=SimpleType::TypedElement then 'Typed Element' else 
		'ERROR' endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif 
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MHAS_SIMPLE_TYPE);
		EOperation eOperation = McorePackage.Literals.MHAS_SIMPLE_TYPE
				.getEOperations().get(0);
		if (simpleTypeAsStringmcoreSimpleTypeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				simpleTypeAsStringmcoreSimpleTypeBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(simpleTypeAsStringmcoreSimpleTypeBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("simpleType", simpleType);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate multiplicityCase$Update(MultiplicityCase trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MultiplicityCase triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMTyped_MultiplicityCase(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String typeAsOcl(MPackage oclPackage, MClassifier mClassifier,
			SimpleType simpleType, Boolean singular) {

		/**
		 * @OCL if  mClassifier.oclIsUndefined() and (simpleType = SimpleType::None)  
		then 'MISSING TYPE' 
		else let t: String = 
		if  simpleType<>SimpleType::None
		then 
		if simpleType=SimpleType::Double  then 'Real' 
		else if simpleType=SimpleType::Object then 'ecore::EObject'
		else if simpleType=SimpleType::Annotation then 'ecore::EAnnotation'
		else if simpleType=SimpleType::Attribute then 'ecore::EAttribute'
		else if simpleType=SimpleType::Class then 'ecore::EClass'
		else if simpleType=SimpleType::Classifier then 'ecore::EClassifier'
		else if simpleType=SimpleType::DataType then 'ecore::EDataType'
		else if simpleType=SimpleType::Enumeration then 'ecore::Enumeration'
		else if simpleType=SimpleType::Feature then 'ecore::EStructuralFeature'
		else if simpleType=SimpleType::Literal then 'ecore::EEnumLiteral'
		else if simpleType=SimpleType::KeyValue then 'ecore::EStringToStringMapEntry'
		else if simpleType=SimpleType::NamedElement then 'ecore::NamedElement'
		else if simpleType=SimpleType::Operation then 'ecore::EOperation'
		else if simpleType=SimpleType::Package then 'ecore::EPackage'
		else if simpleType=SimpleType::Parameter then 'ecore::EParameter'
		else if simpleType=SimpleType::Reference then 'ecore::EReference'
		else if simpleType=SimpleType::TypedElement then 'ecore::ETypedElement'
		else if simpleType = SimpleType::Date then 'ecore::EDate' 
		else simpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif
		else 
		/* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified 
		let pkg: String = 
		if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat('::')
		  else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then '' else mClassifier.containingPackage.eName.allLowerCase().concat('::') endif 
		endif
		in pkg.concat(mClassifier.eName) 
		NEW Create string with all package qualifiers *\/
		let pkg:String=
		if mClassifier.containingClassifier   =null then ''
		else mClassifier.containingPackage.qualifiedName.concat('::' ) endif                            in
		pkg.concat(mClassifier.eName)
		endif
		in 
		
		if  singular.oclIsUndefined() then 'MISSING ARITY INFO' else 
		if singular=true then t else 'OrderedSet('.concat(t).concat(') ') endif
		endif
		endif
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MTYPED);
		EOperation eOperation = McorePackage.Literals.MTYPED.getEOperations()
				.get(1);
		if (typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
						eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("oclPackage", oclPackage);

			evalEnv.add("mClassifier", mClassifier);

			evalEnv.add("simpleType", simpleType);

			evalEnv.add("singular", singular);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING:
			return getMultiplicityAsString();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE:
			if (resolve)
				return getCalculatedType();
			return basicGetCalculatedType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY:
			return getCalculatedMandatory();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR:
			return getCalculatedSingular();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE:
			if (resolve)
				return getCalculatedTypePackage();
			return basicGetCalculatedTypePackage();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED:
			return getVoidTypeAllowed();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE:
			return getMultiplicityCase();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING:
			return getSimpleTypeString();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE:
			return getHasSimpleDataType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE:
			return getHasSimpleModelingType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT:
			return getSimpleTypeIsCorrect();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE:
			return getSimpleType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE:
			if (resolve)
				return getType();
			return basicGetType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE:
			if (resolve)
				return getTypePackage();
			return basicGetTypePackage();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY:
			return getMandatory();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR:
			return getSingular();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME:
			return getETypeName();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL:
			return getETypeLabel();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED:
			return getCorrectlyTyped();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME:
			return getAppendTypeNameToName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE:
			setMultiplicityCase((MultiplicityCase) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE:
			setSimpleType((SimpleType) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE:
			setType((MClassifier) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE:
			setTypePackage((MPackage) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY:
			setMandatory((Boolean) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR:
			setSingular((Boolean) newValue);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME:
			setAppendTypeNameToName((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE:
			setMultiplicityCase(MULTIPLICITY_CASE_EDEFAULT);
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE:
			unsetSimpleType();
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE:
			unsetType();
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE:
			unsetTypePackage();
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY:
			unsetMandatory();
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR:
			unsetSingular();
			return;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME:
			unsetAppendTypeNameToName();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING:
			return MULTIPLICITY_AS_STRING_EDEFAULT == null
					? getMultiplicityAsString() != null
					: !MULTIPLICITY_AS_STRING_EDEFAULT
							.equals(getMultiplicityAsString());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE:
			return basicGetCalculatedType() != null;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY:
			return CALCULATED_MANDATORY_EDEFAULT == null
					? getCalculatedMandatory() != null
					: !CALCULATED_MANDATORY_EDEFAULT
							.equals(getCalculatedMandatory());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR:
			return CALCULATED_SINGULAR_EDEFAULT == null
					? getCalculatedSingular() != null
					: !CALCULATED_SINGULAR_EDEFAULT
							.equals(getCalculatedSingular());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE:
			return basicGetCalculatedTypePackage() != null;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED:
			return VOID_TYPE_ALLOWED_EDEFAULT == null
					? getVoidTypeAllowed() != null
					: !VOID_TYPE_ALLOWED_EDEFAULT.equals(getVoidTypeAllowed());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType() != CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE:
			return getMultiplicityCase() != MULTIPLICITY_CASE_EDEFAULT;
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING:
			return SIMPLE_TYPE_STRING_EDEFAULT == null
					? getSimpleTypeString() != null
					: !SIMPLE_TYPE_STRING_EDEFAULT
							.equals(getSimpleTypeString());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE:
			return HAS_SIMPLE_DATA_TYPE_EDEFAULT == null
					? getHasSimpleDataType() != null
					: !HAS_SIMPLE_DATA_TYPE_EDEFAULT
							.equals(getHasSimpleDataType());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE:
			return HAS_SIMPLE_MODELING_TYPE_EDEFAULT == null
					? getHasSimpleModelingType() != null
					: !HAS_SIMPLE_MODELING_TYPE_EDEFAULT
							.equals(getHasSimpleModelingType());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT:
			return SIMPLE_TYPE_IS_CORRECT_EDEFAULT == null
					? getSimpleTypeIsCorrect() != null
					: !SIMPLE_TYPE_IS_CORRECT_EDEFAULT
							.equals(getSimpleTypeIsCorrect());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE:
			return isSetSimpleType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE:
			return isSetType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE:
			return isSetTypePackage();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY:
			return isSetMandatory();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR:
			return isSetSingular();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME:
			return ETYPE_NAME_EDEFAULT == null ? getETypeName() != null
					: !ETYPE_NAME_EDEFAULT.equals(getETypeName());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL:
			return ETYPE_LABEL_EDEFAULT == null ? getETypeLabel() != null
					: !ETYPE_LABEL_EDEFAULT.equals(getETypeLabel());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED:
			return CORRECTLY_TYPED_EDEFAULT == null
					? getCorrectlyTyped() != null
					: !CORRECTLY_TYPED_EDEFAULT.equals(getCorrectlyTyped());
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME:
			return isSetAppendTypeNameToName();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (derivedFeatureID) {
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING:
				return McorePackage.MTYPED__MULTIPLICITY_AS_STRING;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE:
				return McorePackage.MTYPED__CALCULATED_TYPE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY:
				return McorePackage.MTYPED__CALCULATED_MANDATORY;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR:
				return McorePackage.MTYPED__CALCULATED_SINGULAR;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE:
				return McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED:
				return McorePackage.MTYPED__VOID_TYPE_ALLOWED;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE:
				return McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE:
				return McorePackage.MTYPED__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		if (baseClass == MHasSimpleType.class) {
			switch (derivedFeatureID) {
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE:
				return McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == MExplicitlyTyped.class) {
			switch (derivedFeatureID) {
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE:
				return McorePackage.MEXPLICITLY_TYPED__TYPE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE:
				return McorePackage.MEXPLICITLY_TYPED__TYPE_PACKAGE;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY:
				return McorePackage.MEXPLICITLY_TYPED__MANDATORY;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR:
				return McorePackage.MEXPLICITLY_TYPED__SINGULAR;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME:
				return McorePackage.MEXPLICITLY_TYPED__ETYPE_NAME;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL:
				return McorePackage.MEXPLICITLY_TYPED__ETYPE_LABEL;
			case McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED:
				return McorePackage.MEXPLICITLY_TYPED__CORRECTLY_TYPED;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseFeatureID) {
			case McorePackage.MTYPED__MULTIPLICITY_AS_STRING:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_AS_STRING;
			case McorePackage.MTYPED__CALCULATED_TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE;
			case McorePackage.MTYPED__CALCULATED_MANDATORY:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_MANDATORY;
			case McorePackage.MTYPED__CALCULATED_SINGULAR:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SINGULAR;
			case McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_TYPE_PACKAGE;
			case McorePackage.MTYPED__VOID_TYPE_ALLOWED:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__VOID_TYPE_ALLOWED;
			case McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CALCULATED_SIMPLE_TYPE;
			case McorePackage.MTYPED__MULTIPLICITY_CASE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		if (baseClass == MHasSimpleType.class) {
			switch (baseFeatureID) {
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_STRING;
			case McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_DATA_TYPE;
			case McorePackage.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__HAS_SIMPLE_MODELING_TYPE;
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE_IS_CORRECT;
			case McorePackage.MHAS_SIMPLE_TYPE__SIMPLE_TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SIMPLE_TYPE;
			default:
				return -1;
			}
		}
		if (baseClass == MExplicitlyTyped.class) {
			switch (baseFeatureID) {
			case McorePackage.MEXPLICITLY_TYPED__TYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE;
			case McorePackage.MEXPLICITLY_TYPED__TYPE_PACKAGE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__TYPE_PACKAGE;
			case McorePackage.MEXPLICITLY_TYPED__MANDATORY:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__MANDATORY;
			case McorePackage.MEXPLICITLY_TYPED__SINGULAR:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__SINGULAR;
			case McorePackage.MEXPLICITLY_TYPED__ETYPE_NAME:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_NAME;
			case McorePackage.MEXPLICITLY_TYPED__ETYPE_LABEL:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__ETYPE_LABEL;
			case McorePackage.MEXPLICITLY_TYPED__CORRECTLY_TYPED:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED__CORRECTLY_TYPED;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseOperationID) {
			case McorePackage.MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;
			case McorePackage.MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;
			default:
				return -1;
			}
		}
		if (baseClass == MHasSimpleType.class) {
			switch (baseOperationID) {
			case McorePackage.MHAS_SIMPLE_TYPE___SIMPLE_TYPE_AS_STRING__SIMPLETYPE:
				return McorePackage.MEXPLICITLY_TYPED_AND_NAMED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE;
			default:
				return -1;
			}
		}
		if (baseClass == MExplicitlyTyped.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE:
			return nameFromType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE:
			return shortNameFromType();
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED___SIMPLE_TYPE_AS_STRING__SIMPLETYPE:
			return simpleTypeAsString((SimpleType) arguments.get(0));
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
			return multiplicityCase$Update((MultiplicityCase) arguments.get(0));
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
			return typeAsOcl((MPackage) arguments.get(0),
					(MClassifier) arguments.get(1),
					(SimpleType) arguments.get(2), (Boolean) arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (simpleType: ");
		if (simpleTypeESet)
			result.append(simpleType);
		else
			result.append("<unset>");
		result.append(", mandatory: ");
		if (mandatoryESet)
			result.append(mandatory);
		else
			result.append("<unset>");
		result.append(", singular: ");
		if (singularESet)
			result.append(singular);
		else
			result.append("<unset>");
		result.append(", appendTypeNameToName: ");
		if (appendTypeNameToNameESet)
			result.append(appendTypeNameToName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE,
				McorePackage.Literals.MEXPLICITLY_TYPED__SINGULAR,
				McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MEXPLICITLY_TYPED_AND_NAMED,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MExplicitlyTypedAndNamedImpl
