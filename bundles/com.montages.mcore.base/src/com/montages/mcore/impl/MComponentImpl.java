/**
 */
package com.montages.mcore.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource.Internal;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.util.TypeUtil;
import org.xocl.core.util.IXoclInitializable;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.core.util.XoclMutlitypeComparisonUtil;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XSemantics;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MComponentAction;
import com.montages.mcore.MEditor;
import com.montages.mcore.MNamedEditor;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepository;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.ObjectsFactory;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MComponent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getOwnedPackage <em>Owned Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getResourceFolder <em>Resource Folder</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getSemantics <em>Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getGeneratedEPackage <em>Generated EPackage</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getNamedEditor <em>Named Editor</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getNamePrefix <em>Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDerivedNamePrefix <em>Derived Name Prefix</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getNamePrefixForFeatures <em>Name Prefix For Features</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getContainingRepository <em>Containing Repository</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getRepresentsCoreComponent <em>Represents Core Component</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getRootPackage <em>Root Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDomainType <em>Domain Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDomainName <em>Domain Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDerivedDomainType <em>Derived Domain Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDerivedDomainName <em>Derived Domain Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDerivedBundleName <em>Derived Bundle Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDerivedURI <em>Derived URI</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getSpecialBundleName <em>Special Bundle Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getPreloadedComponent <em>Preloaded Component</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getMainNamedEditor <em>Main Named Editor</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getMainEditor <em>Main Editor</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getHideAdvancedProperties <em>Hide Advanced Properties</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getAllGeneralAnnotations <em>All General Annotations</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getAbstractComponent <em>Abstract Component</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getCountImplementations <em>Count Implementations</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDisableDefaultContainment <em>Disable Default Containment</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getAvoidRegenerationOfEditorConfiguration <em>Avoid Regeneration Of Editor Configuration</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MComponentImpl#getUseLegacyEditorconfig <em>Use Legacy Editorconfig</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MComponentImpl extends MNamedImpl
		implements MComponent, IXoclInitializable {
	/**
	 * The cached value of the '{@link #getOwnedPackage() <em>Owned Package</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOwnedPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<MPackage> ownedPackage;

	/**
	 * The cached value of the '{@link #getResourceFolder() <em>Resource Folder</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResourceFolder()
	 * @generated
	 * @ordered
	 */
	protected EList<MResourceFolder> resourceFolder;

	/**
	 * The cached value of the '{@link #getSemantics() <em>Semantics</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSemantics()
	 * @generated
	 * @ordered
	 */
	protected XSemantics semantics;

	/**
	 * This is true if the Semantics containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean semanticsESet;

	/**
	 * The cached value of the '{@link #getGeneratedEPackage() <em>Generated EPackage</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneratedEPackage()
	 * @generated
	 * @ordered
	 */
	protected EList<EPackage> generatedEPackage;

	/**
	 * The cached value of the '{@link #getNamedEditor() <em>Named Editor</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamedEditor()
	 * @generated
	 * @ordered
	 */
	protected EList<MNamedEditor> namedEditor;

	/**
	 * The default value of the '{@link #getNamePrefix() <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_PREFIX_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNamePrefix() <em>Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected String namePrefix = NAME_PREFIX_EDEFAULT;

	/**
	 * This is true if the Name Prefix attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean namePrefixESet;

	/**
	 * The default value of the '{@link #getDerivedNamePrefix() <em>Derived Name Prefix</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNamePrefix()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_NAME_PREFIX_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getNamePrefixForFeatures() <em>Name Prefix For Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefixForFeatures()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean NAME_PREFIX_FOR_FEATURES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getNamePrefixForFeatures() <em>Name Prefix For Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNamePrefixForFeatures()
	 * @generated
	 * @ordered
	 */
	protected Boolean namePrefixForFeatures = NAME_PREFIX_FOR_FEATURES_EDEFAULT;

	/**
	 * This is true if the Name Prefix For Features attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean namePrefixForFeaturesESet;

	/**
	 * The default value of the '{@link #getRepresentsCoreComponent() <em>Represents Core Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentsCoreComponent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean REPRESENTS_CORE_COMPONENT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDomainType() <em>Domain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainType()
	 * @generated
	 * @ordered
	 */
	protected static final String DOMAIN_TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomainType() <em>Domain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainType()
	 * @generated
	 * @ordered
	 */
	protected String domainType = DOMAIN_TYPE_EDEFAULT;

	/**
	 * This is true if the Domain Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainTypeESet;

	/**
	 * The default value of the '{@link #getDomainName() <em>Domain Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainName()
	 * @generated
	 * @ordered
	 */
	protected static final String DOMAIN_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDomainName() <em>Domain Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainName()
	 * @generated
	 * @ordered
	 */
	protected String domainName = DOMAIN_NAME_EDEFAULT;

	/**
	 * This is true if the Domain Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainNameESet;

	/**
	 * The default value of the '{@link #getDerivedDomainType() <em>Derived Domain Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedDomainType()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_DOMAIN_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedDomainName() <em>Derived Domain Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedDomainName()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_DOMAIN_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedBundleName() <em>Derived Bundle Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedBundleName()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_BUNDLE_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getDerivedURI() <em>Derived URI</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedURI()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_URI_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSpecialBundleName() <em>Special Bundle Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialBundleName()
	 * @generated
	 * @ordered
	 */
	protected static final String SPECIAL_BUNDLE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSpecialBundleName() <em>Special Bundle Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialBundleName()
	 * @generated
	 * @ordered
	 */
	protected String specialBundleName = SPECIAL_BUNDLE_NAME_EDEFAULT;

	/**
	 * This is true if the Special Bundle Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean specialBundleNameESet;

	/**
	 * The cached value of the '{@link #getPreloadedComponent() <em>Preloaded Component</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPreloadedComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<MComponent> preloadedComponent;

	/**
	 * The cached value of the '{@link #getMainNamedEditor() <em>Main Named Editor</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainNamedEditor()
	 * @generated
	 * @ordered
	 */
	protected MNamedEditor mainNamedEditor;

	/**
	 * This is true if the Main Named Editor reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean mainNamedEditorESet;

	/**
	 * The default value of the '{@link #getHideAdvancedProperties() <em>Hide Advanced Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHideAdvancedProperties()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HIDE_ADVANCED_PROPERTIES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHideAdvancedProperties() <em>Hide Advanced Properties</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHideAdvancedProperties()
	 * @generated
	 * @ordered
	 */
	protected Boolean hideAdvancedProperties = HIDE_ADVANCED_PROPERTIES_EDEFAULT;

	/**
	 * This is true if the Hide Advanced Properties attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean hideAdvancedPropertiesESet;

	/**
	 * The default value of the '{@link #getAbstractComponent() <em>Abstract Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractComponent()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ABSTRACT_COMPONENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAbstractComponent() <em>Abstract Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAbstractComponent()
	 * @generated
	 * @ordered
	 */
	protected Boolean abstractComponent = ABSTRACT_COMPONENT_EDEFAULT;

	/**
	 * This is true if the Abstract Component attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean abstractComponentESet;

	/**
	 * The default value of the '{@link #getCountImplementations() <em>Count Implementations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountImplementations()
	 * @generated
	 * @ordered
	 */
	protected static final Integer COUNT_IMPLEMENTATIONS_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getCountImplementations() <em>Count Implementations</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCountImplementations()
	 * @generated
	 * @ordered
	 */
	protected Integer countImplementations = COUNT_IMPLEMENTATIONS_EDEFAULT;

	/**
	 * This is true if the Count Implementations attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean countImplementationsESet;

	/**
	 * The default value of the '{@link #getDisableDefaultContainment() <em>Disable Default Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisableDefaultContainment()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean DISABLE_DEFAULT_CONTAINMENT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDisableDefaultContainment() <em>Disable Default Containment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDisableDefaultContainment()
	 * @generated
	 * @ordered
	 */
	protected Boolean disableDefaultContainment = DISABLE_DEFAULT_CONTAINMENT_EDEFAULT;

	/**
	 * This is true if the Disable Default Containment attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean disableDefaultContainmentESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MComponentAction DO_ACTION_EDEFAULT = MComponentAction.DO;

	/**
	 * The default value of the '{@link #getAvoidRegenerationOfEditorConfiguration() <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AVOID_REGENERATION_OF_EDITOR_CONFIGURATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAvoidRegenerationOfEditorConfiguration() <em>Avoid Regeneration Of Editor Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAvoidRegenerationOfEditorConfiguration()
	 * @generated
	 * @ordered
	 */
	protected Boolean avoidRegenerationOfEditorConfiguration = AVOID_REGENERATION_OF_EDITOR_CONFIGURATION_EDEFAULT;

	/**
	 * This is true if the Avoid Regeneration Of Editor Configuration attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean avoidRegenerationOfEditorConfigurationESet;

	/**
	 * The default value of the '{@link #getUseLegacyEditorconfig() <em>Use Legacy Editorconfig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseLegacyEditorconfig()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_LEGACY_EDITORCONFIG_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUseLegacyEditorconfig() <em>Use Legacy Editorconfig</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUseLegacyEditorconfig()
	 * @generated
	 * @ordered
	 */
	protected Boolean useLegacyEditorconfig = USE_LEGACY_EDITORCONFIG_EDEFAULT;

	/**
	 * This is true if the Use Legacy Editorconfig attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean useLegacyEditorconfigESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMComponentActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemcoreMComponentActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedNamePrefix <em>Derived Name Prefix</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedNamePrefix
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedNamePrefixDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getDoAction <em>Do Action</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedBundleName <em>Derived Bundle Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedBundleName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedBundleNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedURI <em>Derived URI</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedURI
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedURIDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMainEditor <em>Main Editor</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMainEditor
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression mainEditorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAllGeneralAnnotations <em>All General Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllGeneralAnnotations
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allGeneralAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRepresentsCoreComponent <em>Represents Core Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRepresentsCoreComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression representsCoreComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getRootPackage <em>Root Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRootPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression rootPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedDomainType <em>Derived Domain Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedDomainType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedDomainTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedDomainName <em>Derived Domain Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedDomainName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedDomainNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingRepository <em>Containing Repository</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingRepository
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingRepositoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Placeholder object which denotes the absence of a value
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI18
	 * @generated
	 */
	private static final Object NO_OBJECT = new Object();

	/**
	 * The flag checking whether the class is initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI19
	 * @generated
	 */
	private boolean _isInitialized = false;

	/**
	 * The map storing feature values snapshot at allowInitialization() call.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI20
	 * @generated
	 */
	private Map<EStructuralFeature, Object> myInitValueMap;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MCOMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MComponentAction getDoAction() {
		/**
		 * @OCL mcore::MComponentAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MComponentAction result = (MComponentAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MComponentAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MComponentAction.OBJECT_VALUE:
			MResourceFolder f1;
			if (this.getResourceFolder().isEmpty()) {
				f1 = ObjectsFactory.eINSTANCE.createMResourceFolder();
				this.getResourceFolder().add(f1);
				f1.setName("workspace");
			} else {
				f1 = this.getResourceFolder().get(0);
			}
			MResourceFolder f2;
			if (f1.getFolder().isEmpty()) {
				f2 = ObjectsFactory.eINSTANCE.createMResourceFolder();
				f1.getFolder().add(f2);
				f2.setName("test");
			} else {
				f2 = f1.getFolder().get(0);
			}
			MResource r;
			if (f2.getResource().isEmpty()) {
				r = ObjectsFactory.eINSTANCE.createMResource();
				f2.getResource().add(r);
				r.setName("My Resource");
				r.setId("R");
				/*Initialization of MResource already created first object, thus break */
				break;
			} else {
				r = f2.getResource().get(0);
			}
			MObject rootObject;
			if (r.getObject().isEmpty()) {
				rootObject = ObjectsFactory.eINSTANCE.createMObject();
				r.getObject().add(rootObject);
				if (r.getRootObjectPackage() == null) {
					rootObject.setNature("Class 0");
				} else {
					if (r.getRootObjectPackage().getClassifier().isEmpty()) {
						rootObject.setNature("Class 0");
					} else {
						MClassifier c;
						if (r.getRootObjectPackage()
								.getResourceRootClass() == null) {
							c = r.getRootObjectPackage().getClassifier().get(0);
						} else {
							c = r.getRootObjectPackage().getResourceRootClass();
						}
						if (c.getKind() == ClassifierKind.CLASS_TYPE) {
							rootObject.setType(c);
						} else {
							rootObject.setNature("Class 0");
						}

					}
				}
			} else {
				rootObject = r.getObject().get(0);
				if (rootObject.getType() == null) {
					addNewObjectToLastContainmentSlot(rootObject);

				} else {
					MProperty lastContainmentProperty = null;
					EList<MProperty> containments = rootObject.getType()
							.allContainmentReferences();
					for (Iterator iterator = containments.iterator(); iterator
							.hasNext();) {
						MProperty mProperty = (MProperty) iterator.next();
						if (!mProperty.getSingular()) {
							lastContainmentProperty = mProperty;
						}
					}

					if (lastContainmentProperty == null) {
						addNewObjectToLastContainmentSlot(rootObject);

					} else {
						MPropertyInstance lastContainmentPropertySlot = null;
						for (Iterator iterator = rootObject
								.getPropertyInstance().iterator(); iterator
										.hasNext();) {
							MPropertyInstance s = (MPropertyInstance) iterator
									.next();
							if (s.getProperty() == lastContainmentProperty) {
								lastContainmentPropertySlot = s;
								break;
							}
						}
						if (lastContainmentPropertySlot == null) {
							lastContainmentPropertySlot = ObjectsFactory.eINSTANCE
									.createMPropertyInstance();
							rootObject.getPropertyInstance()
									.add(lastContainmentPropertySlot);
							lastContainmentPropertySlot
									.setProperty(lastContainmentProperty);
						}
						addNewObjectToLastContainmentSlot(
								lastContainmentPropertySlot);
					}
				}
				/*TODO: if root object has type, try to create an object of the last n-ary containment property in its class. Results in nice default containment behavior.*/
				/*if only 1ary exist, go to the last unary, and add an object there. Thus create the structure of the unary containments, and add object to last one having n-ar containment*/
				/*if root object has no type, add object to last property-Instance which already contains object, clone those objects, if found.*/
			}
			break;

		case MComponentAction.OPEN_EDITOR_VALUE:

			if (!this.getResourceFolder().isEmpty())
				getResourceFolder().get(0)
						.setDoAction(MResourceFolderAction.OPEN_EDITOR);

			break;
		case MComponentAction.CLASS_VALUE:
			/*create minimal package stuff, see whether class exists, if not create one and make it root class. If one exists, add it, and set default containment...*/
			/*ATTENTION: REPEATED IN: 
			 * MComponent do...CLASS
			 * MObject do... FIX TYPE
			 * MResource do... FIX ALL TYPES
			 */
			MPackage mainPackage = null;
			if (getOwnedPackage().isEmpty()) {
				mainPackage = McoreFactory.eINSTANCE.createMPackage();
				getOwnedPackage().add(mainPackage);
			} else {
				mainPackage = getOwnedPackage().get(0);
			}
			MClassifier newClassifier = McoreFactory.eINSTANCE
					.createMClassifier();
			mainPackage.getClassifier().add(newClassifier);
			newClassifier.setName("Class ".concat(
					Integer.toString(mainPackage.getClassifier().size() - 1)));
			break;

		case MComponentAction.GENERATE_PROPERTY_GROUPS_VALUE:
			this.getRootPackage()
					.setDoAction(MPackageAction.GENERATE_PROPERTY_GROUPS);
			break;

		case MComponentAction.REMOVE_PROPERTY_GROUPS_VALUE:
			this.getRootPackage()
					.setDoAction(MPackageAction.REMOVE_PROPERTY_GROUPS);
			break;

		case MComponentAction.COMPLETE_SEMANTICS_VALUE:
			this.getRootPackage()
					.setDoAction(MPackageAction.COMPLETE_SEMANTICS);
			break;

		default:
			break;
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MComponentAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let e1:OrderedSet(MComponentAction)=OrderedSet{MComponentAction::Do, MComponentAction::Object, MComponentAction::Class,MComponentAction::FixDocumentation,MComponentAction::OpenEditor} in
	if self.abstractComponent = true
	then e1->append(MComponentAction::Implement)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)
	else e1->append(MComponentAction::Abstract)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)
	endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MComponentAction> evalDoActionChoiceConstruction(
			List<MComponentAction> choice) {
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MCOMPONENT__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MComponentAction> result = new ArrayList<MComponentAction>(
					(Collection<MComponentAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MComponentAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MComponentAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMComponent_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MComponentAction mComponentAction) {

		switch (mComponentAction) {
		case CLASS: {

			if (!this.getOwnedPackage().isEmpty())
				if (this.getOwnedPackage().get(0) != null)
					if (!this.getOwnedPackage().get(0).getClassifier()
							.isEmpty())
						return getOwnedPackage().get(0)
								.doActionUpdate(MPackageAction.CLASS);

			//1. Create XTransition
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			//2. Define literal that triggers the action
			EEnumLiteral eEnumLiteralTRIGGER = McorePackage.Literals.MCOMPONENT_ACTION
					.getEEnumLiteral(mComponentAction.getValue());

			//3. currentTrigger := Class Action
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_DoAction(),
					XUpdateMode.REDEFINE, null,
					//:=
					eEnumLiteralTRIGGER, null, null);

			// Check whether package exists
			MPackage mPackage = null;
			if (getOwnedPackage().isEmpty()) {
				//4. Create MPackage Object     
				// EClass is gathered from Package that contains the EClass  : 
				//	- AnnotationsPackage for package Annotations
				//  -ExpressionsPackage for package Expressions
				//   etc....
				mPackage = (MPackage) currentTrigger.createNextStateNewObject(
						McorePackage.Literals.MPACKAGE);

				//5. Add mPackage object to reference of 
				//  Parameter 1 : Owner object of the property
				//  Parameter 2:  EReference of property 
				//  Parameter 3:  XUpdateMode  Redefine = Change,   Add = Add
				//  Parameter 4:  XAddUpdateMode   ( position we want to add an object)  null for "redefine"
				//  Parameter 5:  object we want to add to given EReference(Property)
				//  Parameter 6+7:  null by default right now
				currentTrigger.addReferenceUpdate(this,
						McorePackage.Literals.MCOMPONENT__OWNED_PACKAGE,
						XUpdateMode.REDEFINE, null, mPackage, null, null);
			} else {
				mPackage = getOwnedPackage().get(0);
			}
			//6. Create newClassifier object, we work mostly with "EObject" so we don't have to inherit all kinds of interfaces
			//  Only cast the EObject to a specific  classifier if you will need to work with features on that level
			EObject newClassifier = currentTrigger.createNextStateNewObject(
					McorePackage.Literals.MCLASSIFIER);

			//7 Add newClassifier object to reference of MPackage 
			//  Parameter 1 : Owner object of the property
			//  Parameter 2:  EReference of property 
			//  Parameter 3:  XUpdateMode   Add = Add
			//  Parameter 4:  XAddUpdateMode.LAST   add at last position
			//  Parameter 5:  object we want to add to given EReference(Property)
			//  Parameter 6+7:  null by default right now
			currentTrigger.addReferenceUpdate(mPackage,
					McorePackage.Literals.MPACKAGE__CLASSIFIER, XUpdateMode.ADD,
					XAddUpdateMode.LAST, newClassifier, null, null);

			//8. Change name of newClassifier 
			//  Parameter 1 : Owner object of the property
			//  Parameter 2:  EAttribute of Property (Simpletype)
			//  Parameter 3:  XUpdateMode   Redefne
			//  Parameter 4:  null
			//  Parameter 5:  object we want the EAttribute to be 
			//  Parameter 6+7:  null by default right now
			currentTrigger.addAttributeUpdate(newClassifier,
					McorePackage.Literals.MNAMED__NAME, XUpdateMode.REDEFINE,
					null,
					"Class ".concat(
							Integer.toString(mPackage.getClassifier().size())),
					null, null);

			//9. Set focus to newClassifier
			transition.getFocusObjects().add(transition
					.nextStateObjectDefinitionFromObject(newClassifier));

			//10. Return xUpdate
			return currentTrigger;

		}
		case OBJECT:
			break;

		case FIX_DOCUMENTATION:

			//1. Create XTransition
			XTransition transition = SemanticsFactory.eINSTANCE
					.createXTransition();

			//2. Define literal that triggers the action
			EEnumLiteral eEnumLiteralTRIGGER = McorePackage.Literals.MCOMPONENT_ACTION
					.getEEnumLiteral(mComponentAction.getValue());

			//3. currentTrigger := Class Action
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_DoAction(),
					XUpdateMode.REDEFINE, null,
					//:=
					eEnumLiteralTRIGGER, null, null);

			for (MGeneralAnnotation annotation : getAllGeneralAnnotations())
				if (annotation.getSource() != null
						&& annotation.getSource().contains("GenModel")
						&& !annotation.getMGeneralDetail().isEmpty()) {
					EList<MGeneralDetail> detailList = annotation
							.getMGeneralDetail();

					for (MGeneralDetail detail : detailList) {
						String value = detail.getValue();
						if (value != null && (value.equals("documentation")
								|| value.equals("Documentation") && !detail
										.getKey().equals("documentation"))) {
							String aux = detail.getKey();
							currentTrigger.addAttributeUpdate(detail,
									AnnotationsPackage.eINSTANCE
											.getMGeneralDetail_Key(),
									XUpdateMode.REDEFINE, XAddUpdateMode.FIRST,
									value.toLowerCase(), null, null);
							currentTrigger.addAttributeUpdate(detail,
									AnnotationsPackage.eINSTANCE
											.getMGeneralDetail_Value(),
									XUpdateMode.REDEFINE, XAddUpdateMode.FIRST,
									aux, null, null);
						}
					}
				}
			return currentTrigger;
		default:
			break;

		}

		return null;
	}

	private void addNewObjectToLastContainmentSlot(MObject rootObject) {
		MPropertyInstance lastContainmentSlot = null;
		for (Iterator iterator = rootObject.getPropertyInstance()
				.iterator(); iterator.hasNext();) {
			MPropertyInstance slot = (MPropertyInstance) iterator.next();
			if (slot.getProperty() == null) {
				if (slot.getInternalDataValue().isEmpty()
						&& slot.getInternalLiteralValue().isEmpty()
						&& slot.getInternalReferencedObject().isEmpty()) {
					lastContainmentSlot = slot;
				}
			} else {
				if (slot.getProperty().getContainment()) {
					lastContainmentSlot = slot;
				}
			}
		}
		if (lastContainmentSlot == null) {
			lastContainmentSlot = ObjectsFactory.eINSTANCE
					.createMPropertyInstance();
			rootObject.getPropertyInstance().add(lastContainmentSlot);
		}
		addNewObjectToLastContainmentSlot(lastContainmentSlot);
	}

	private void addNewObjectToLastContainmentSlot(
			MPropertyInstance lastContainmentSlot) {
		/*add object to last slot, create type as in last instance in this slot.*/
		MObject newObject = ObjectsFactory.eINSTANCE.createMObject();
		lastContainmentSlot.getInternalContainedObject().add(newObject);
		if (lastContainmentSlot.getProperty() != null && lastContainmentSlot
				.getProperty().getCalculatedType() != null) {
			MClassifier lastContainmentSlotType = lastContainmentSlot
					.getProperty().getCalculatedType();
			if (!lastContainmentSlotType.getAbstractClass()) {
				newObject.setType(lastContainmentSlotType);
			} else {
				MClassifier nonAbstract = null;
				for (Iterator iterator = lastContainmentSlotType.allSubTypes()
						.iterator(); iterator.hasNext();) {
					MClassifier mClassifier = (MClassifier) iterator.next();
					if (!mClassifier.getAbstractClass()) {
						nonAbstract = mClassifier;
						break;
					}

				}
				if (nonAbstract != null) {
					newObject.setType(nonAbstract);
				} else {
					newObject.setType(lastContainmentSlotType);
					newObject.setNature(lastContainmentSlotType
							.getCalculatedName().concat(" Subtype"));
				}
			}
		} else {
			if (lastContainmentSlot.getKey() != null
					&& !lastContainmentSlot.getKey().trim().equals("")) {
				newObject.setNature(XoclHelper
						.camelCaseToBusiness(lastContainmentSlot.getKey()));
			} else {
				newObject.setNature("Child Object");
			}

		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPackage> getOwnedPackage() {
		if (ownedPackage == null) {
			ownedPackage = new EObjectContainmentEList.Unsettable.Resolving<MPackage>(
					MPackage.class, this,
					McorePackage.MCOMPONENT__OWNED_PACKAGE);
		}
		return ownedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOwnedPackage() {
		if (ownedPackage != null)
			((InternalEList.Unsettable<?>) ownedPackage).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOwnedPackage() {
		return ownedPackage != null
				&& ((InternalEList.Unsettable<?>) ownedPackage).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MResourceFolder> getResourceFolder() {
		if (resourceFolder == null) {
			resourceFolder = new EObjectContainmentEList.Unsettable.Resolving<MResourceFolder>(
					MResourceFolder.class, this,
					McorePackage.MCOMPONENT__RESOURCE_FOLDER);
		}
		return resourceFolder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResourceFolder() {
		if (resourceFolder != null)
			((InternalEList.Unsettable<?>) resourceFolder).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResourceFolder() {
		return resourceFolder != null
				&& ((InternalEList.Unsettable<?>) resourceFolder).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XSemantics getSemantics() {
		if (semantics != null && semantics.eIsProxy()) {
			InternalEObject oldSemantics = (InternalEObject) semantics;
			semantics = (XSemantics) eResolveProxy(oldSemantics);
			if (semantics != oldSemantics) {
				InternalEObject newSemantics = (InternalEObject) semantics;
				NotificationChain msgs = oldSemantics.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MCOMPONENT__SEMANTICS,
						null, null);
				if (newSemantics.eInternalContainer() == null) {
					msgs = newSemantics.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- McorePackage.MCOMPONENT__SEMANTICS,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MCOMPONENT__SEMANTICS, oldSemantics,
							semantics));
			}
		}
		return semantics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XSemantics basicGetSemantics() {
		return semantics;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSemantics(XSemantics newSemantics,
			NotificationChain msgs) {
		XSemantics oldSemantics = semantics;
		semantics = newSemantics;
		boolean oldSemanticsESet = semanticsESet;
		semanticsESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET, McorePackage.MCOMPONENT__SEMANTICS,
					oldSemantics, newSemantics, !oldSemanticsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSemantics(XSemantics newSemantics) {
		if (newSemantics != semantics) {
			NotificationChain msgs = null;
			if (semantics != null)
				msgs = ((InternalEObject) semantics).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MCOMPONENT__SEMANTICS,
						null, msgs);
			if (newSemantics != null)
				msgs = ((InternalEObject) newSemantics).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- McorePackage.MCOMPONENT__SEMANTICS,
						null, msgs);
			msgs = basicSetSemantics(newSemantics, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldSemanticsESet = semanticsESet;
			semanticsESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						McorePackage.MCOMPONENT__SEMANTICS, newSemantics,
						newSemantics, !oldSemanticsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetSemantics(NotificationChain msgs) {
		XSemantics oldSemantics = semantics;
		semantics = null;
		boolean oldSemanticsESet = semanticsESet;
		semanticsESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET, McorePackage.MCOMPONENT__SEMANTICS,
					oldSemantics, null, oldSemanticsESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSemantics() {
		if (semantics != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) semantics).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE - McorePackage.MCOMPONENT__SEMANTICS,
					null, msgs);
			msgs = basicUnsetSemantics(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldSemanticsESet = semanticsESet;
			semanticsESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						McorePackage.MCOMPONENT__SEMANTICS, null, null,
						oldSemanticsESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSemantics() {
		return semanticsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MComponent> getPreloadedComponent() {
		if (preloadedComponent == null) {
			preloadedComponent = new EObjectResolvingEList.Unsettable<MComponent>(
					MComponent.class, this,
					McorePackage.MCOMPONENT__PRELOADED_COMPONENT);
		}
		return preloadedComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPreloadedComponent() {
		if (preloadedComponent != null)
			((InternalEList.Unsettable<?>) preloadedComponent).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPreloadedComponent() {
		return preloadedComponent != null
				&& ((InternalEList.Unsettable<?>) preloadedComponent).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EPackage> getGeneratedEPackage() {
		if (generatedEPackage == null) {
			generatedEPackage = new EObjectContainmentEList.Unsettable.Resolving<EPackage>(
					EPackage.class, this,
					McorePackage.MCOMPONENT__GENERATED_EPACKAGE);
		}
		return generatedEPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneratedEPackage() {
		if (generatedEPackage != null)
			((InternalEList.Unsettable<?>) generatedEPackage).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneratedEPackage() {
		return generatedEPackage != null
				&& ((InternalEList.Unsettable<?>) generatedEPackage).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHideAdvancedProperties() {
		return hideAdvancedProperties;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHideAdvancedProperties(Boolean newHideAdvancedProperties) {
		Boolean oldHideAdvancedProperties = hideAdvancedProperties;
		hideAdvancedProperties = newHideAdvancedProperties;
		boolean oldHideAdvancedPropertiesESet = hideAdvancedPropertiesESet;
		hideAdvancedPropertiesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES,
					oldHideAdvancedProperties, hideAdvancedProperties,
					!oldHideAdvancedPropertiesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHideAdvancedProperties() {
		Boolean oldHideAdvancedProperties = hideAdvancedProperties;
		boolean oldHideAdvancedPropertiesESet = hideAdvancedPropertiesESet;
		hideAdvancedProperties = HIDE_ADVANCED_PROPERTIES_EDEFAULT;
		hideAdvancedPropertiesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES,
					oldHideAdvancedProperties,
					HIDE_ADVANCED_PROPERTIES_EDEFAULT,
					oldHideAdvancedPropertiesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHideAdvancedProperties() {
		return hideAdvancedPropertiesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAbstractComponent() {
		return abstractComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAbstractComponent(Boolean newAbstractComponent) {
		Boolean oldAbstractComponent = abstractComponent;
		abstractComponent = newAbstractComponent;
		boolean oldAbstractComponentESet = abstractComponentESet;
		abstractComponentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__ABSTRACT_COMPONENT,
					oldAbstractComponent, abstractComponent,
					!oldAbstractComponentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAbstractComponent() {
		Boolean oldAbstractComponent = abstractComponent;
		boolean oldAbstractComponentESet = abstractComponentESet;
		abstractComponent = ABSTRACT_COMPONENT_EDEFAULT;
		abstractComponentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__ABSTRACT_COMPONENT,
					oldAbstractComponent, ABSTRACT_COMPONENT_EDEFAULT,
					oldAbstractComponentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAbstractComponent() {
		return abstractComponentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getCountImplementations() {
		return countImplementations;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * This feature is generated using custom templates
	 * Just for API use. All derived changeable features are handled using the XSemantics framework
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCountImplementations(Integer newCountImplementations) {
		Integer oldCountImplementations = countImplementations;
		countImplementations = newCountImplementations;
		boolean oldCountImplementationsESet = countImplementationsESet;
		countImplementationsESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS,
					oldCountImplementations, countImplementations,
					!oldCountImplementationsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetCountImplementations() {
		Integer oldCountImplementations = countImplementations;
		boolean oldCountImplementationsESet = countImplementationsESet;
		countImplementations = COUNT_IMPLEMENTATIONS_EDEFAULT;
		countImplementationsESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS,
					oldCountImplementations, COUNT_IMPLEMENTATIONS_EDEFAULT,
					oldCountImplementationsESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetCountImplementations() {
		return countImplementationsESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getDisableDefaultContainment() {
		return disableDefaultContainment;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisableDefaultContainment(
			Boolean newDisableDefaultContainment) {
		Boolean oldDisableDefaultContainment = disableDefaultContainment;
		disableDefaultContainment = newDisableDefaultContainment;
		boolean oldDisableDefaultContainmentESet = disableDefaultContainmentESet;
		disableDefaultContainmentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT,
					oldDisableDefaultContainment, disableDefaultContainment,
					!oldDisableDefaultContainmentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDisableDefaultContainment() {
		Boolean oldDisableDefaultContainment = disableDefaultContainment;
		boolean oldDisableDefaultContainmentESet = disableDefaultContainmentESet;
		disableDefaultContainment = DISABLE_DEFAULT_CONTAINMENT_EDEFAULT;
		disableDefaultContainmentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT,
					oldDisableDefaultContainment,
					DISABLE_DEFAULT_CONTAINMENT_EDEFAULT,
					oldDisableDefaultContainmentESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDisableDefaultContainment() {
		return disableDefaultContainmentESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getAllGeneralAnnotations() {
		/**
		 * @OCL mcore::annotations::MGeneralAnnotation.allInstances()
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__ALL_GENERAL_ANNOTATIONS;

		if (allGeneralAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allGeneralAnnotationsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allGeneralAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MGeneralAnnotation> result = (EList<MGeneralAnnotation>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MNamedEditor> getNamedEditor() {
		if (namedEditor == null) {
			namedEditor = new EObjectContainmentEList.Unsettable.Resolving<MNamedEditor>(
					MNamedEditor.class, this,
					McorePackage.MCOMPONENT__NAMED_EDITOR);
		}
		return namedEditor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamedEditor() {
		if (namedEditor != null)
			((InternalEList.Unsettable<?>) namedEditor).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamedEditor() {
		return namedEditor != null
				&& ((InternalEList.Unsettable<?>) namedEditor).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MEditor getMainEditor() {
		MEditor mainEditor = basicGetMainEditor();
		return mainEditor != null && mainEditor.eIsProxy()
				? (MEditor) eResolveProxy((InternalEObject) mainEditor)
				: mainEditor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MEditor basicGetMainEditor() {
		/**
		 * @OCL if mainNamedEditor.oclIsUndefined()
		then null
		else mainNamedEditor.editor
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__MAIN_EDITOR;

		if (mainEditorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				mainEditorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(mainEditorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MEditor result = (MEditor) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUseLegacyEditorconfig() {
		return useLegacyEditorconfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUseLegacyEditorconfig(Boolean newUseLegacyEditorconfig) {
		Boolean oldUseLegacyEditorconfig = useLegacyEditorconfig;
		useLegacyEditorconfig = newUseLegacyEditorconfig;
		boolean oldUseLegacyEditorconfigESet = useLegacyEditorconfigESet;
		useLegacyEditorconfigESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG,
					oldUseLegacyEditorconfig, useLegacyEditorconfig,
					!oldUseLegacyEditorconfigESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUseLegacyEditorconfig() {
		Boolean oldUseLegacyEditorconfig = useLegacyEditorconfig;
		boolean oldUseLegacyEditorconfigESet = useLegacyEditorconfigESet;
		useLegacyEditorconfig = USE_LEGACY_EDITORCONFIG_EDEFAULT;
		useLegacyEditorconfigESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG,
					oldUseLegacyEditorconfig, USE_LEGACY_EDITORCONFIG_EDEFAULT,
					oldUseLegacyEditorconfigESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUseLegacyEditorconfig() {
		return useLegacyEditorconfigESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedEditor getMainNamedEditor() {
		if (mainNamedEditor != null && mainNamedEditor.eIsProxy()) {
			InternalEObject oldMainNamedEditor = (InternalEObject) mainNamedEditor;
			mainNamedEditor = (MNamedEditor) eResolveProxy(oldMainNamedEditor);
			if (mainNamedEditor != oldMainNamedEditor) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR,
							oldMainNamedEditor, mainNamedEditor));
			}
		}
		return mainNamedEditor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MNamedEditor basicGetMainNamedEditor() {
		return mainNamedEditor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMainNamedEditor(MNamedEditor newMainNamedEditor) {
		MNamedEditor oldMainNamedEditor = mainNamedEditor;
		mainNamedEditor = newMainNamedEditor;
		boolean oldMainNamedEditorESet = mainNamedEditorESet;
		mainNamedEditorESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR,
					oldMainNamedEditor, mainNamedEditor,
					!oldMainNamedEditorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMainNamedEditor() {
		MNamedEditor oldMainNamedEditor = mainNamedEditor;
		boolean oldMainNamedEditorESet = mainNamedEditorESet;
		mainNamedEditor = null;
		mainNamedEditorESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR,
					oldMainNamedEditor, null, oldMainNamedEditorESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMainNamedEditor() {
		return mainNamedEditorESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getNamePrefix() {
		return namePrefix;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamePrefix(String newNamePrefix) {
		String oldNamePrefix = namePrefix;
		namePrefix = newNamePrefix;
		boolean oldNamePrefixESet = namePrefixESet;
		namePrefixESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__NAME_PREFIX, oldNamePrefix,
					namePrefix, !oldNamePrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamePrefix() {
		String oldNamePrefix = namePrefix;
		boolean oldNamePrefixESet = namePrefixESet;
		namePrefix = NAME_PREFIX_EDEFAULT;
		namePrefixESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__NAME_PREFIX, oldNamePrefix,
					NAME_PREFIX_EDEFAULT, oldNamePrefixESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamePrefix() {
		return namePrefixESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedNamePrefix() {
		/**
		 * @OCL let chain: String = namePrefix in
		if chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() 
		then null 
		else chain.camelCaseUpper().trim()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DERIVED_NAME_PREFIX;

		if (derivedNamePrefixDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedNamePrefixDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedNamePrefixDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getNamePrefixForFeatures() {
		return namePrefixForFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNamePrefixForFeatures(Boolean newNamePrefixForFeatures) {
		Boolean oldNamePrefixForFeatures = namePrefixForFeatures;
		namePrefixForFeatures = newNamePrefixForFeatures;
		boolean oldNamePrefixForFeaturesESet = namePrefixForFeaturesESet;
		namePrefixForFeaturesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES,
					oldNamePrefixForFeatures, namePrefixForFeatures,
					!oldNamePrefixForFeaturesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNamePrefixForFeatures() {
		Boolean oldNamePrefixForFeatures = namePrefixForFeatures;
		boolean oldNamePrefixForFeaturesESet = namePrefixForFeaturesESet;
		namePrefixForFeatures = NAME_PREFIX_FOR_FEATURES_EDEFAULT;
		namePrefixForFeaturesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES,
					oldNamePrefixForFeatures, NAME_PREFIX_FOR_FEATURES_EDEFAULT,
					oldNamePrefixForFeaturesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNamePrefixForFeatures() {
		return namePrefixForFeaturesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedBundleName() {
		/**
		 * @OCL (
		if domainType.oclIsUndefined() 
		then 'MISSING domainType' 
		else derivedDomainType endif).concat(
		'.').concat(
		if domainName.oclIsUndefined() 
		then  'MISSING domainName' 
		else derivedDomainName endif).concat(
		'.').concat(
		calculatedShortName.camelCaseLower().allLowerCase())
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DERIVED_BUNDLE_NAME;

		if (derivedBundleNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedBundleNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedBundleNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedURI() {
		/**
		 * @OCL 'http://www.'.concat(
		if domainName.oclIsUndefined() 
		then  'MISSING domainName' 
		else derivedDomainName endif).concat(
		'.').concat(
		if domainType.oclIsUndefined() 
		then 'MISSING domainType' 
		else derivedDomainType endif).concat(
		'/').concat(
		eName)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DERIVED_URI;

		if (derivedURIDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedURIDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedURIDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getRepresentsCoreComponent() {
		/**
		 * @OCL if self.eContainmentFeature().oclIsUndefined() 
		then false
		else eContainmentFeature().name='core' endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__REPRESENTS_CORE_COMPONENT;

		if (representsCoreComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				representsCoreComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(representsCoreComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getRootPackage() {
		MPackage rootPackage = basicGetRootPackage();
		return rootPackage != null && rootPackage.eIsProxy()
				? (MPackage) eResolveProxy((InternalEObject) rootPackage)
				: rootPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetRootPackage() {
		/**
		 * @OCL ownedPackage->asOrderedSet()->select(it: mcore::MPackage | let chain: Boolean = it.isRootPackage in
		if chain = true then true else false 
		endif)->asOrderedSet()->excluding(null)->asOrderedSet()->at(1)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__ROOT_PACKAGE;

		if (rootPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				rootPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(rootPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAvoidRegenerationOfEditorConfiguration() {
		return avoidRegenerationOfEditorConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAvoidRegenerationOfEditorConfiguration(
			Boolean newAvoidRegenerationOfEditorConfiguration) {
		Boolean oldAvoidRegenerationOfEditorConfiguration = avoidRegenerationOfEditorConfiguration;
		avoidRegenerationOfEditorConfiguration = newAvoidRegenerationOfEditorConfiguration;
		boolean oldAvoidRegenerationOfEditorConfigurationESet = avoidRegenerationOfEditorConfigurationESet;
		avoidRegenerationOfEditorConfigurationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION,
					oldAvoidRegenerationOfEditorConfiguration,
					avoidRegenerationOfEditorConfiguration,
					!oldAvoidRegenerationOfEditorConfigurationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAvoidRegenerationOfEditorConfiguration() {
		Boolean oldAvoidRegenerationOfEditorConfiguration = avoidRegenerationOfEditorConfiguration;
		boolean oldAvoidRegenerationOfEditorConfigurationESet = avoidRegenerationOfEditorConfigurationESet;
		avoidRegenerationOfEditorConfiguration = AVOID_REGENERATION_OF_EDITOR_CONFIGURATION_EDEFAULT;
		avoidRegenerationOfEditorConfigurationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION,
					oldAvoidRegenerationOfEditorConfiguration,
					AVOID_REGENERATION_OF_EDITOR_CONFIGURATION_EDEFAULT,
					oldAvoidRegenerationOfEditorConfigurationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAvoidRegenerationOfEditorConfiguration() {
		return avoidRegenerationOfEditorConfigurationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRepository getContainingRepository() {
		MRepository containingRepository = basicGetContainingRepository();
		return containingRepository != null && containingRepository.eIsProxy()
				? (MRepository) eResolveProxy(
						(InternalEObject) containingRepository)
				: containingRepository;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRepository basicGetContainingRepository() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() 
		then null
		else if self.eContainer().oclIsKindOf(MRepository)
		then self.eContainer().oclAsType(MRepository)
		else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__CONTAINING_REPOSITORY;

		if (containingRepositoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingRepositoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingRepositoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MRepository result = (MRepository) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomainType() {
		return domainType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainType(String newDomainType) {
		String oldDomainType = domainType;
		domainType = newDomainType;
		boolean oldDomainTypeESet = domainTypeESet;
		domainTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__DOMAIN_TYPE, oldDomainType,
					domainType, !oldDomainTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDomainType() {
		String oldDomainType = domainType;
		boolean oldDomainTypeESet = domainTypeESet;
		domainType = DOMAIN_TYPE_EDEFAULT;
		domainTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__DOMAIN_TYPE, oldDomainType,
					DOMAIN_TYPE_EDEFAULT, oldDomainTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainType() {
		return domainTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDomainName() {
		return domainName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainName(String newDomainName) {
		String oldDomainName = domainName;
		domainName = newDomainName;
		boolean oldDomainNameESet = domainNameESet;
		domainNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__DOMAIN_NAME, oldDomainName,
					domainName, !oldDomainNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDomainName() {
		String oldDomainName = domainName;
		boolean oldDomainNameESet = domainNameESet;
		domainName = DOMAIN_NAME_EDEFAULT;
		domainNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__DOMAIN_NAME, oldDomainName,
					DOMAIN_NAME_EDEFAULT, oldDomainNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainName() {
		return domainNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedDomainType() {
		/**
		 * @OCL let derivedOne:String = self.domainType.allLowerCase().trim() in
		Sequence{1..(derivedOne.tokenize('.')->size())}->iterate (
		i:Integer; c: String =''|
		if i=1
		then c.concat(derivedOne.tokenize('.')->at((derivedOne.tokenize('.')->size())))
		else c.concat('.').concat(derivedOne.tokenize('.')->at((derivedOne.tokenize('.')->size())-i+1))
		endif
		)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DERIVED_DOMAIN_TYPE;

		if (derivedDomainTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedDomainTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedDomainTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedDomainName() {
		/**
		 * @OCL let derivedOne:String = self.domainName.allLowerCase().trim() in
		Sequence{1..(derivedOne.tokenize('.')->size())}->iterate (
		i:Integer; c: String =''|
		if i=1
		then c.concat(derivedOne.tokenize('.')->at((derivedOne.tokenize('.')->size())))
		else c.concat('.').concat(derivedOne.tokenize('.')->at((derivedOne.tokenize('.')->size())-i+1))
		endif
		)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		EStructuralFeature eFeature = McorePackage.Literals.MCOMPONENT__DERIVED_DOMAIN_NAME;

		if (derivedDomainNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedDomainNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MCOMPONENT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedDomainNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSpecialBundleName() {
		return specialBundleName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSpecialBundleName(String newSpecialBundleName) {
		String oldSpecialBundleName = specialBundleName;
		specialBundleName = newSpecialBundleName;
		boolean oldSpecialBundleNameESet = specialBundleNameESet;
		specialBundleNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME,
					oldSpecialBundleName, specialBundleName,
					!oldSpecialBundleNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSpecialBundleName() {
		String oldSpecialBundleName = specialBundleName;
		boolean oldSpecialBundleNameESet = specialBundleNameESet;
		specialBundleName = SPECIAL_BUNDLE_NAME_EDEFAULT;
		specialBundleNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME,
					oldSpecialBundleName, SPECIAL_BUNDLE_NAME_EDEFAULT,
					oldSpecialBundleNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSpecialBundleName() {
		return specialBundleNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
			return ((InternalEList<?>) getOwnedPackage()).basicRemove(otherEnd,
					msgs);
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
			return ((InternalEList<?>) getResourceFolder())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MCOMPONENT__SEMANTICS:
			return basicUnsetSemantics(msgs);
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
			return ((InternalEList<?>) getGeneratedEPackage())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			return ((InternalEList<?>) getNamedEditor()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
			return getOwnedPackage();
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
			return getResourceFolder();
		case McorePackage.MCOMPONENT__SEMANTICS:
			if (resolve)
				return getSemantics();
			return basicGetSemantics();
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
			return getGeneratedEPackage();
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			return getNamedEditor();
		case McorePackage.MCOMPONENT__NAME_PREFIX:
			return getNamePrefix();
		case McorePackage.MCOMPONENT__DERIVED_NAME_PREFIX:
			return getDerivedNamePrefix();
		case McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES:
			return getNamePrefixForFeatures();
		case McorePackage.MCOMPONENT__CONTAINING_REPOSITORY:
			if (resolve)
				return getContainingRepository();
			return basicGetContainingRepository();
		case McorePackage.MCOMPONENT__REPRESENTS_CORE_COMPONENT:
			return getRepresentsCoreComponent();
		case McorePackage.MCOMPONENT__ROOT_PACKAGE:
			if (resolve)
				return getRootPackage();
			return basicGetRootPackage();
		case McorePackage.MCOMPONENT__DOMAIN_TYPE:
			return getDomainType();
		case McorePackage.MCOMPONENT__DOMAIN_NAME:
			return getDomainName();
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_TYPE:
			return getDerivedDomainType();
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_NAME:
			return getDerivedDomainName();
		case McorePackage.MCOMPONENT__DERIVED_BUNDLE_NAME:
			return getDerivedBundleName();
		case McorePackage.MCOMPONENT__DERIVED_URI:
			return getDerivedURI();
		case McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME:
			return getSpecialBundleName();
		case McorePackage.MCOMPONENT__PRELOADED_COMPONENT:
			return getPreloadedComponent();
		case McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR:
			if (resolve)
				return getMainNamedEditor();
			return basicGetMainNamedEditor();
		case McorePackage.MCOMPONENT__MAIN_EDITOR:
			if (resolve)
				return getMainEditor();
			return basicGetMainEditor();
		case McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES:
			return getHideAdvancedProperties();
		case McorePackage.MCOMPONENT__ALL_GENERAL_ANNOTATIONS:
			return getAllGeneralAnnotations();
		case McorePackage.MCOMPONENT__ABSTRACT_COMPONENT:
			return getAbstractComponent();
		case McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS:
			return getCountImplementations();
		case McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT:
			return getDisableDefaultContainment();
		case McorePackage.MCOMPONENT__DO_ACTION:
			return getDoAction();
		case McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION:
			return getAvoidRegenerationOfEditorConfiguration();
		case McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG:
			return getUseLegacyEditorconfig();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
			getOwnedPackage().clear();
			getOwnedPackage().addAll((Collection<? extends MPackage>) newValue);
			return;
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
			getResourceFolder().clear();
			getResourceFolder()
					.addAll((Collection<? extends MResourceFolder>) newValue);
			return;
		case McorePackage.MCOMPONENT__SEMANTICS:
			setSemantics((XSemantics) newValue);
			return;
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
			getGeneratedEPackage().clear();
			getGeneratedEPackage()
					.addAll((Collection<? extends EPackage>) newValue);
			return;
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			getNamedEditor().clear();
			getNamedEditor()
					.addAll((Collection<? extends MNamedEditor>) newValue);
			return;
		case McorePackage.MCOMPONENT__NAME_PREFIX:
			setNamePrefix((String) newValue);
			return;
		case McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES:
			setNamePrefixForFeatures((Boolean) newValue);
			return;
		case McorePackage.MCOMPONENT__DOMAIN_TYPE:
			setDomainType((String) newValue);
			return;
		case McorePackage.MCOMPONENT__DOMAIN_NAME:
			setDomainName((String) newValue);
			return;
		case McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME:
			setSpecialBundleName((String) newValue);
			return;
		case McorePackage.MCOMPONENT__PRELOADED_COMPONENT:
			getPreloadedComponent().clear();
			getPreloadedComponent()
					.addAll((Collection<? extends MComponent>) newValue);
			return;
		case McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR:
			setMainNamedEditor((MNamedEditor) newValue);
			return;
		case McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES:
			setHideAdvancedProperties((Boolean) newValue);
			return;
		case McorePackage.MCOMPONENT__ABSTRACT_COMPONENT:
			setAbstractComponent((Boolean) newValue);
			return;
		case McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS:
			setCountImplementations((Integer) newValue);
			return;
		case McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT:
			setDisableDefaultContainment((Boolean) newValue);
			return;
		case McorePackage.MCOMPONENT__DO_ACTION:
			setDoAction((MComponentAction) newValue);
			return;
		case McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION:
			setAvoidRegenerationOfEditorConfiguration((Boolean) newValue);
			return;
		case McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG:
			setUseLegacyEditorconfig((Boolean) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
			unsetOwnedPackage();
			return;
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
			unsetResourceFolder();
			return;
		case McorePackage.MCOMPONENT__SEMANTICS:
			unsetSemantics();
			return;
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
			unsetGeneratedEPackage();
			return;
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			unsetNamedEditor();
			return;
		case McorePackage.MCOMPONENT__NAME_PREFIX:
			unsetNamePrefix();
			return;
		case McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES:
			unsetNamePrefixForFeatures();
			return;
		case McorePackage.MCOMPONENT__DOMAIN_TYPE:
			unsetDomainType();
			return;
		case McorePackage.MCOMPONENT__DOMAIN_NAME:
			unsetDomainName();
			return;
		case McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME:
			unsetSpecialBundleName();
			return;
		case McorePackage.MCOMPONENT__PRELOADED_COMPONENT:
			unsetPreloadedComponent();
			return;
		case McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR:
			unsetMainNamedEditor();
			return;
		case McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES:
			unsetHideAdvancedProperties();
			return;
		case McorePackage.MCOMPONENT__ABSTRACT_COMPONENT:
			unsetAbstractComponent();
			return;
		case McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS:
			unsetCountImplementations();
			return;
		case McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT:
			unsetDisableDefaultContainment();
			return;
		case McorePackage.MCOMPONENT__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		case McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION:
			unsetAvoidRegenerationOfEditorConfiguration();
			return;
		case McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG:
			unsetUseLegacyEditorconfig();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
			return isSetOwnedPackage();
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
			return isSetResourceFolder();
		case McorePackage.MCOMPONENT__SEMANTICS:
			return isSetSemantics();
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
			return isSetGeneratedEPackage();
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			return isSetNamedEditor();
		case McorePackage.MCOMPONENT__NAME_PREFIX:
			return isSetNamePrefix();
		case McorePackage.MCOMPONENT__DERIVED_NAME_PREFIX:
			return DERIVED_NAME_PREFIX_EDEFAULT == null
					? getDerivedNamePrefix() != null
					: !DERIVED_NAME_PREFIX_EDEFAULT
							.equals(getDerivedNamePrefix());
		case McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES:
			return isSetNamePrefixForFeatures();
		case McorePackage.MCOMPONENT__CONTAINING_REPOSITORY:
			return basicGetContainingRepository() != null;
		case McorePackage.MCOMPONENT__REPRESENTS_CORE_COMPONENT:
			return REPRESENTS_CORE_COMPONENT_EDEFAULT == null
					? getRepresentsCoreComponent() != null
					: !REPRESENTS_CORE_COMPONENT_EDEFAULT
							.equals(getRepresentsCoreComponent());
		case McorePackage.MCOMPONENT__ROOT_PACKAGE:
			return basicGetRootPackage() != null;
		case McorePackage.MCOMPONENT__DOMAIN_TYPE:
			return isSetDomainType();
		case McorePackage.MCOMPONENT__DOMAIN_NAME:
			return isSetDomainName();
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_TYPE:
			return DERIVED_DOMAIN_TYPE_EDEFAULT == null
					? getDerivedDomainType() != null
					: !DERIVED_DOMAIN_TYPE_EDEFAULT
							.equals(getDerivedDomainType());
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_NAME:
			return DERIVED_DOMAIN_NAME_EDEFAULT == null
					? getDerivedDomainName() != null
					: !DERIVED_DOMAIN_NAME_EDEFAULT
							.equals(getDerivedDomainName());
		case McorePackage.MCOMPONENT__DERIVED_BUNDLE_NAME:
			return DERIVED_BUNDLE_NAME_EDEFAULT == null
					? getDerivedBundleName() != null
					: !DERIVED_BUNDLE_NAME_EDEFAULT
							.equals(getDerivedBundleName());
		case McorePackage.MCOMPONENT__DERIVED_URI:
			return DERIVED_URI_EDEFAULT == null ? getDerivedURI() != null
					: !DERIVED_URI_EDEFAULT.equals(getDerivedURI());
		case McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME:
			return isSetSpecialBundleName();
		case McorePackage.MCOMPONENT__PRELOADED_COMPONENT:
			return isSetPreloadedComponent();
		case McorePackage.MCOMPONENT__MAIN_NAMED_EDITOR:
			return isSetMainNamedEditor();
		case McorePackage.MCOMPONENT__MAIN_EDITOR:
			return basicGetMainEditor() != null;
		case McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES:
			return isSetHideAdvancedProperties();
		case McorePackage.MCOMPONENT__ALL_GENERAL_ANNOTATIONS:
			return !getAllGeneralAnnotations().isEmpty();
		case McorePackage.MCOMPONENT__ABSTRACT_COMPONENT:
			return isSetAbstractComponent();
		case McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS:
			return isSetCountImplementations();
		case McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT:
			return isSetDisableDefaultContainment();
		case McorePackage.MCOMPONENT__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION:
			return isSetAvoidRegenerationOfEditorConfiguration();
		case McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG:
			return isSetUseLegacyEditorconfig();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MCOMPONENT___DO_ACTION$_UPDATE__MCOMPONENTACTION:
			return doAction$Update((MComponentAction) arguments.get(0));
		case McorePackage.MCOMPONENT___DO_ACTION_UPDATE__MCOMPONENTACTION:
			return doActionUpdate((MComponentAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (namePrefix: ");
		if (namePrefixESet)
			result.append(namePrefix);
		else
			result.append("<unset>");
		result.append(", namePrefixForFeatures: ");
		if (namePrefixForFeaturesESet)
			result.append(namePrefixForFeatures);
		else
			result.append("<unset>");
		result.append(", domainType: ");
		if (domainTypeESet)
			result.append(domainType);
		else
			result.append("<unset>");
		result.append(", domainName: ");
		if (domainNameESet)
			result.append(domainName);
		else
			result.append("<unset>");
		result.append(", specialBundleName: ");
		if (specialBundleNameESet)
			result.append(specialBundleName);
		else
			result.append("<unset>");
		result.append(", hideAdvancedProperties: ");
		if (hideAdvancedPropertiesESet)
			result.append(hideAdvancedProperties);
		else
			result.append("<unset>");
		result.append(", abstractComponent: ");
		if (abstractComponentESet)
			result.append(abstractComponent);
		else
			result.append("<unset>");
		result.append(", countImplementations: ");
		if (countImplementationsESet)
			result.append(countImplementations);
		else
			result.append("<unset>");
		result.append(", disableDefaultContainment: ");
		if (disableDefaultContainmentESet)
			result.append(disableDefaultContainment);
		else
			result.append("<unset>");
		result.append(", avoidRegenerationOfEditorConfiguration: ");
		if (avoidRegenerationOfEditorConfigurationESet)
			result.append(avoidRegenerationOfEditorConfiguration);
		else
			result.append("<unset>");
		result.append(", useLegacyEditorconfig: ");
		if (useLegacyEditorconfigESet)
			result.append(useLegacyEditorconfig);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.derivedBundleName
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MCOMPONENT;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL fullLabel '[component] '.concat(self.calculatedName)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MCOMPONENT);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Component'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MCOMPONENT);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS09
	 * @generated
	 */

	@Override
	public NotificationChain eBasicSetContainer(InternalEObject newContainer,
			int newContainerFeatureID, NotificationChain msgs) {
		NotificationChain result = super.eBasicSetContainer(newContainer,
				newContainerFeatureID, msgs);
		for (EStructuralFeature eStructuralFeature : eClass()
				.getEAllStructuralFeatures()) {
			if (eStructuralFeature instanceof EReference) {
				EReference eReference = (EReference) eStructuralFeature;
				if (eReference.isContainer()) {
					if (eContainmentFeature() == eReference.getEOpposite()) {
						continue;
					}
				}
			}
			if (!eStructuralFeature.isDerived() && eIsSet(eStructuralFeature)) {
				if ((myInitValueMap == null) || (myInitValueMap
						.get(eStructuralFeature) != eGet(eStructuralFeature))) {
					myInitValueMap = null;
					return result;
				}
			}
		}
		myInitValueMap = null;
		Internal eInternalResource = eInternalResource();
		ensureClassInitialized(
				(eInternalResource != null) && eInternalResource.isLoading());
		return result;
	}

	/**
	 * @templateTag INS15
	 * @generated
	 */
	public void allowInitialization() {
		if (myInitValueMap == null) {
			myInitValueMap = new HashMap<EStructuralFeature, Object>();
		}
		if (eClass() != null) {
			for (EStructuralFeature eStructuralFeature : eClass()
					.getEAllStructuralFeatures()) {
				if (eStructuralFeature.isDerived()) {
					continue;
				}
				myInitValueMap.put(eStructuralFeature,
						eGet(eStructuralFeature));
			}
		}
	}

	/**
	 * Returns an array of structural features which are initialized with the init-family annotations 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS10
	 * @generated
	 */
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				McorePackage.Literals.MCOMPONENT__OWNED_PACKAGE,
				McorePackage.Literals.MCOMPONENT__RESOURCE_FOLDER,
				McorePackage.Literals.MCOMPONENT__DOMAIN_TYPE,
				McorePackage.Literals.MCOMPONENT__DOMAIN_NAME,
				McorePackage.Literals.MCOMPONENT__ABSTRACT_COMPONENT,
				McorePackage.Literals.MCOMPONENT__COUNT_IMPLEMENTATIONS,
				McorePackage.Literals.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT,
				McorePackage.Literals.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION,
				McorePackage.Literals.MCOMPONENT__USE_LEGACY_EDITORCONFIG,
				McorePackage.Literals.MNAMED__NAME };
		return initializedFeatures;
	}

	/**
	 * This method checks whether the class is initialized.
	 * If it is not yet initialized then the initialization is performed.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS11
	 * @generated
	 */
	public void ensureClassInitialized(boolean isLoadInProgress) {
		if (_isInitialized) {
			return;
		}
		_isInitialized = true;
		EStructuralFeature[] initializedFeatures = getInitializedStructuralFeatures();

		if (isLoadInProgress) {
			// only transient features are initialized then
			List<EStructuralFeature> filteredInitializedFeatures = new ArrayList<EStructuralFeature>();
			for (EStructuralFeature initializedFeature : initializedFeatures) {
				if (initializedFeature.isTransient()) {
					filteredInitializedFeatures.add(initializedFeature);
				}
			}
			initializedFeatures = filteredInitializedFeatures.toArray(
					new EStructuralFeature[filteredInitializedFeatures.size()]);
		}

		final Map<EStructuralFeature, Object> initOrderMap = new HashMap<EStructuralFeature, Object>();
		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOrderOclExpressionMap(), "initOrder", "InitOrder",
					true);
			if (value != NO_OBJECT) {
				initOrderMap.put(structuralFeature, value);
			}
		}

		if (!initOrderMap.isEmpty()) {
			Arrays.sort(initializedFeatures,
					new Comparator<EStructuralFeature>() {
						public int compare(
								EStructuralFeature structuralFeature1,
								EStructuralFeature structuralFeature2) {
							Object comparedObject1 = initOrderMap
									.get(structuralFeature1);
							Object comparedObject2 = initOrderMap
									.get(structuralFeature2);
							if (comparedObject1 == null) {
								if (comparedObject2 == null) {
									int index1 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject1);
									int index2 = eClass()
											.getEAllStructuralFeatures()
											.indexOf(comparedObject2);
									return index1 - index2;
								} else {
									return 1;
								}
							} else if (comparedObject2 == null) {
								return -1;
							}
							return XoclMutlitypeComparisonUtil
									.compare(comparedObject1, comparedObject2);
						}
					});
		}

		for (EStructuralFeature structuralFeature : initializedFeatures) {
			Object value = evaluateInitOclAnnotation(structuralFeature,
					getInitOclExpressionMap(), "initValue", "InitValue", false);
			if (value != NO_OBJECT) {
				eSet(structuralFeature, value);
			}
		}
	}

	/**
	 * Evaluates the value of an init-family annotation for the property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS12
	 * @generated
	 */
	protected Object evaluateInitOclAnnotation(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey,
			boolean isSimpleEvaluate) {
		OCLExpression oclExpression = getInitOclAnnotationExpression(
				structuralFeature, expressionMap, annotationKey,
				annotationOverrideKey);

		if (oclExpression == null) {
			return NO_OBJECT;
		}

		Query query = OCL_ENV.createQuery(oclExpression);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MCOMPONENT,
					"initOclAnnotation(" + structuralFeature.getName() + ")");

			query.getEvaluationEnvironment().clear();
			Object trg = eGet(structuralFeature);
			query.getEvaluationEnvironment().add("trg", trg);

			if (isSimpleEvaluate) {
				return query.evaluate(this);
			}
			XoclEvaluator xoclEval = new XoclEvaluator(this,
					new HashMap<ETypedElement, Object>());
			xoclEval.setContainerOnCreation(this);

			return xoclEval.evaluateElement(structuralFeature, query);
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return NO_OBJECT;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Compiles an init-family annotation for the property. Uses the corresponding init-family annotation cache.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS13
	 * @generated
	 */
	protected OCLExpression getInitOclAnnotationExpression(
			EStructuralFeature structuralFeature,
			Map<EStructuralFeature, OCLExpression> expressionMap,
			String annotationKey, String annotationOverrideKey) {
		OCLExpression oclExpression = expressionMap.get(structuralFeature);
		if (oclExpression != null) {
			return oclExpression;
		}

		String oclText = XoclEmfUtil.findAnnotationText(structuralFeature,
				eClass(), annotationKey, annotationOverrideKey);

		if (oclText != null) {
			// Hurray, the expression text is found! Let's compile it
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass(), structuralFeature);

			EClassifier propertyType = TypeUtil.getPropertyType(
					OCL_ENV.getEnvironment(), eClass(), structuralFeature);
			addEnvironmentVariable("trg", propertyType);

			try {
				oclExpression = helper.createQuery(oclText);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						oclText, helper.getProblems(), eClass(),
						structuralFeature);
			}

			expressionMap.put(structuralFeature, oclExpression);
		}

		return oclExpression;
	}
} //MComponentImpl
