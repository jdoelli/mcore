/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MOperationSignatureAction;
import com.montages.mcore.MParameter;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.MGeneralAnnotation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MOperation Signature</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getGeneralAnnotation <em>General Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getParameter <em>Parameter</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getOperation <em>Operation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getInternalEOperation <em>Internal EOperation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MOperationSignatureImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MOperationSignatureImpl extends MTypedImpl
		implements MOperationSignature {
	/**
	 * The cached value of the '{@link #getGeneralAnnotation() <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralAnnotation> generalAnnotation;

	/**
	 * The cached value of the '{@link #getParameter() <em>Parameter</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameter()
	 * @generated
	 * @ordered
	 */
	protected EList<MParameter> parameter;

	/**
	 * The default value of the '{@link #getELabel() <em>ELabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getELabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ELABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInternalEOperation() <em>Internal EOperation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalEOperation()
	 * @generated
	 * @ordered
	 */
	protected EOperation internalEOperation;

	/**
	 * This is true if the Internal EOperation reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean internalEOperationESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MOperationSignatureAction DO_ACTION_EDEFAULT = MOperationSignatureAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMOperationSignatureActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #sameSignature <em>Same Signature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #sameSignature
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression sameSignaturemcoreMOperationSignatureBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #signatureAsString <em>Signature As String</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #signatureAsString
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression signatureAsStringBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOperation <em>Operation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression operationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getELabel <em>ELabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getELabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MOperationSignatureImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MOPERATION_SIGNATURE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getGeneralAnnotation() {
		if (generalAnnotation == null) {
			generalAnnotation = new EObjectContainmentEList.Unsettable.Resolving<MGeneralAnnotation>(
					MGeneralAnnotation.class, this,
					McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION);
		}
		return generalAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralAnnotation() {
		if (generalAnnotation != null)
			((InternalEList.Unsettable<?>) generalAnnotation).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralAnnotation() {
		return generalAnnotation != null
				&& ((InternalEList.Unsettable<?>) generalAnnotation).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignatureAction getDoAction() {
		/**
		 * @OCL mcore::MOperationSignatureAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MOPERATION_SIGNATURE;
		EStructuralFeature eFeature = McorePackage.Literals.MOPERATION_SIGNATURE__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MOperationSignatureAction result = (MOperationSignatureAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MOperationSignatureAction newDoAction) {
		if (newDoAction != MOperationSignatureAction.DO) {
			MParameter p = McoreFactory.eINSTANCE.createMParameter();
			getParameter().add(p);
			switch (newDoAction.getValue()) {
			case MOperationSignatureAction.DATA_PARAMETER_VALUE:
				p.setSimpleType(SimpleType.STRING);
				p.setSingular(true);
				break;
			case MOperationSignatureAction.UNARY_OBJECT_PARAMETER_VALUE:
				p.setType(this.getContainingClassifier());
				p.setSimpleType(SimpleType.NONE);
				p.setSingular(true);
				break;
			case MOperationSignatureAction.NARY_OBJECT_PARAMETER_VALUE:
				p.setType(this.getContainingClassifier());
				p.setSimpleType(SimpleType.NONE);
				p.setSingular(false);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MOperationSignatureAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MOperationSignatureAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMOperationSignature_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MParameter> getParameter() {
		if (parameter == null) {
			parameter = new EObjectContainmentEList.Unsettable.Resolving<MParameter>(
					MParameter.class, this,
					McorePackage.MOPERATION_SIGNATURE__PARAMETER);
		}
		return parameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetParameter() {
		if (parameter != null)
			((InternalEList.Unsettable<?>) parameter).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetParameter() {
		return parameter != null
				&& ((InternalEList.Unsettable<?>) parameter).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getOperation() {
		MProperty operation = basicGetOperation();
		return operation != null && operation.eIsProxy()
				? (MProperty) eResolveProxy((InternalEObject) operation)
				: operation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetOperation() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(MProperty) then
		self.eContainer().oclAsType(MProperty) else null
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MOPERATION_SIGNATURE;
		EStructuralFeature eFeature = McorePackage.Literals.MOPERATION_SIGNATURE__OPERATION;

		if (operationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				operationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(operationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getContainingClassifier() {
		MClassifier containingClassifier = basicGetContainingClassifier();
		return containingClassifier != null && containingClassifier.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) containingClassifier)
				: containingClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetContainingClassifier() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() then null else
		if self.eContainer().oclIsKindOf(MProperty) then 
		self.eContainer().oclAsType(MProperty).containingClassifier else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MOPERATION_SIGNATURE;
		EStructuralFeature eFeature = McorePackage.Literals.MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getELabel() {
		/**
		 * @OCL (if self.operation.oclIsUndefined() then 
		'OPERATION MISSING' else 
		self.operation.eName endif)
		.concat(self.signatureAsString())
		.concat(':')
		.concat(self.operation.eTypeLabel)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MOPERATION_SIGNATURE;
		EStructuralFeature eFeature = McorePackage.Literals.MOPERATION_SIGNATURE__ELABEL;

		if (eLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getInternalEOperation() {
		if (internalEOperation != null && internalEOperation.eIsProxy()) {
			InternalEObject oldInternalEOperation = (InternalEObject) internalEOperation;
			internalEOperation = (EOperation) eResolveProxy(
					oldInternalEOperation);
			if (internalEOperation != oldInternalEOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION,
							oldInternalEOperation, internalEOperation));
			}
		}
		return internalEOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation basicGetInternalEOperation() {
		return internalEOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalEOperation(EOperation newInternalEOperation) {
		EOperation oldInternalEOperation = internalEOperation;
		internalEOperation = newInternalEOperation;
		boolean oldInternalEOperationESet = internalEOperationESet;
		internalEOperationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION,
					oldInternalEOperation, internalEOperation,
					!oldInternalEOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalEOperation() {
		EOperation oldInternalEOperation = internalEOperation;
		boolean oldInternalEOperationESet = internalEOperationESet;
		internalEOperation = null;
		internalEOperationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION,
					oldInternalEOperation, null, oldInternalEOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalEOperation() {
		return internalEOperationESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean sameSignature(MOperationSignature s) {

		/**
		 * @OCL if self.operation <> s.operation then false else
		self.parameter->size() = s.parameter->size() and
		self.parameter->forAll(p:MParameter|p.type=s.parameter->at(self.parameter->indexOf(p)).type) 
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EOperation eOperation = McorePackage.Literals.MOPERATION_SIGNATURE
				.getEOperations().get(1);
		if (sameSignaturemcoreMOperationSignatureBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				sameSignaturemcoreMOperationSignatureBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(sameSignaturemcoreMOperationSignatureBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String signatureAsString() {

		/**
		 * @OCL '('.concat(
		let ps:OrderedSet(MParameter) = self.parameter in
		if ps->isEmpty()then'' else
		ps->iterate(p:MParameter;res:String='' |
		(if res='' then '' else res.concat(', ') endif)
		.concat(p.eTypeName).concat(if p.singular then '' else '*' endif))
		endif).concat(')')
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EOperation eOperation = McorePackage.Literals.MOPERATION_SIGNATURE
				.getEOperations().get(2);
		if (signatureAsStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				signatureAsStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MOPERATION_SIGNATURE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(signatureAsStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MOPERATION_SIGNATURE, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
			return ((InternalEList<?>) getGeneralAnnotation())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			return ((InternalEList<?>) getParameter()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
			return getGeneralAnnotation();
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			return getParameter();
		case McorePackage.MOPERATION_SIGNATURE__OPERATION:
			if (resolve)
				return getOperation();
			return basicGetOperation();
		case McorePackage.MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER:
			if (resolve)
				return getContainingClassifier();
			return basicGetContainingClassifier();
		case McorePackage.MOPERATION_SIGNATURE__ELABEL:
			return getELabel();
		case McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION:
			if (resolve)
				return getInternalEOperation();
			return basicGetInternalEOperation();
		case McorePackage.MOPERATION_SIGNATURE__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
			getGeneralAnnotation().clear();
			getGeneralAnnotation().addAll(
					(Collection<? extends MGeneralAnnotation>) newValue);
			return;
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			getParameter().clear();
			getParameter().addAll((Collection<? extends MParameter>) newValue);
			return;
		case McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION:
			setInternalEOperation((EOperation) newValue);
			return;
		case McorePackage.MOPERATION_SIGNATURE__DO_ACTION:
			setDoAction((MOperationSignatureAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
			unsetGeneralAnnotation();
			return;
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			unsetParameter();
			return;
		case McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION:
			unsetInternalEOperation();
			return;
		case McorePackage.MOPERATION_SIGNATURE__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
			return isSetGeneralAnnotation();
		case McorePackage.MOPERATION_SIGNATURE__PARAMETER:
			return isSetParameter();
		case McorePackage.MOPERATION_SIGNATURE__OPERATION:
			return basicGetOperation() != null;
		case McorePackage.MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER:
			return basicGetContainingClassifier() != null;
		case McorePackage.MOPERATION_SIGNATURE__ELABEL:
			return ELABEL_EDEFAULT == null ? getELabel() != null
					: !ELABEL_EDEFAULT.equals(getELabel());
		case McorePackage.MOPERATION_SIGNATURE__INTERNAL_EOPERATION:
			return isSetInternalEOperation();
		case McorePackage.MOPERATION_SIGNATURE__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION:
				return McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (baseFeatureID) {
			case McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION:
				return McorePackage.MOPERATION_SIGNATURE__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MOPERATION_SIGNATURE___DO_ACTION$_UPDATE__MOPERATIONSIGNATUREACTION:
			return doAction$Update(
					(MOperationSignatureAction) arguments.get(0));
		case McorePackage.MOPERATION_SIGNATURE___SAME_SIGNATURE__MOPERATIONSIGNATURE:
			return sameSignature((MOperationSignature) arguments.get(0));
		case McorePackage.MOPERATION_SIGNATURE___SIGNATURE_AS_STRING:
			return signatureAsString();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL eLabel
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MOPERATION_SIGNATURE;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Signature'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedMandatory operation.calculatedMandatory
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedMandatory() {
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular operation.calculatedSingular
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedType operation.calculatedType
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (McorePackage.Literals.MOPERATION_SIGNATURE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MOperationSignatureImpl
