/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.SimpleType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MVariable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getMultiplicityAsString <em>Multiplicity As String</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getCalculatedType <em>Calculated Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getCalculatedMandatory <em>Calculated Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getCalculatedSingular <em>Calculated Singular</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getCalculatedTypePackage <em>Calculated Type Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getVoidTypeAllowed <em>Void Type Allowed</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getCalculatedSimpleType <em>Calculated Simple Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MVariableImpl#getMultiplicityCase <em>Multiplicity Case</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MVariableImpl extends MNamedImpl implements MVariable {
	/**
	 * The default value of the '{@link #getMultiplicityAsString() <em>Multiplicity As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString()
	 * @generated
	 * @ordered
	 */
	protected static final String MULTIPLICITY_AS_STRING_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedMandatory() <em>Calculated Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_MANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSingular() <em>Calculated Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CALCULATED_SINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getVoidTypeAllowed() <em>Void Type Allowed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean VOID_TYPE_ALLOWED_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getCalculatedSimpleType() <em>Calculated Simple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType CALCULATED_SIMPLE_TYPE_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getMultiplicityCase() <em>Multiplicity Case</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase()
	 * @generated
	 * @ordered
	 */
	protected static final MultiplicityCase MULTIPLICITY_CASE_EDEFAULT = MultiplicityCase.ZERO_ONE;

	/**
	 * The parsed OCL expression for the body of the '{@link #multiplicityCase$Update <em>Multiplicity Case$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #multiplicityCase$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression multiplicityCase$UpdatemcoreMultiplicityCaseBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #typeAsOcl <em>Type As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #typeAsOcl
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityAsString <em>Multiplicity As String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityAsString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityAsStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedTypePackage <em>Calculated Type Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedTypePackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedTypePackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getVoidTypeAllowed <em>Void Type Allowed</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVoidTypeAllowed
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression voidTypeAllowedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMultiplicityCase <em>Multiplicity Case</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMultiplicityCase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression multiplicityCaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MVARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getMultiplicityAsString() {
		/**
		 * @OCL (if self.calculatedMandatory then '[1..' else '[0..' endif)
		.concat(if self.calculatedSingular then '1]' else '*]' endif)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_AS_STRING;

		if (multiplicityAsStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityAsStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityAsStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getCalculatedType() {
		MClassifier calculatedType = basicGetCalculatedType();
		return calculatedType != null && calculatedType.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) calculatedType)
				: calculatedType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetCalculatedType() {
		/**
		 * @OCL let res:MClassifier=null in res
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedMandatory() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getCalculatedSingular() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage getCalculatedTypePackage() {
		MPackage calculatedTypePackage = basicGetCalculatedTypePackage();
		return calculatedTypePackage != null && calculatedTypePackage.eIsProxy()
				? (MPackage) eResolveProxy(
						(InternalEObject) calculatedTypePackage)
				: calculatedTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackage basicGetCalculatedTypePackage() {
		/**
		 * @OCL if self.calculatedType.oclIsUndefined() then null else self.calculatedType.containingPackage endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE_PACKAGE;

		if (calculatedTypePackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedTypePackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypePackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPackage result = (MPackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getVoidTypeAllowed() {
		/**
		 * @OCL false
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED;

		if (voidTypeAllowedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				voidTypeAllowedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(voidTypeAllowedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getCalculatedSimpleType() {
		/**
		 * @OCL mcore::SimpleType::None
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MultiplicityCase getMultiplicityCase() {
		/**
		 * @OCL if self.calculatedMandatory and self.calculatedSingular
		then MultiplicityCase::OneOne
		else if (not self.calculatedMandatory) and self.calculatedSingular
		then MultiplicityCase::ZeroOne
		else if self.calculatedMandatory and (not self.calculatedSingular)
		then MultiplicityCase::OneMany
		else MultiplicityCase::ZeroMany endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MVARIABLE;
		EStructuralFeature eFeature = McorePackage.Literals.MTYPED__MULTIPLICITY_CASE;

		if (multiplicityCaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				multiplicityCaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(multiplicityCaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MultiplicityCase result = (MultiplicityCase) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMultiplicityCase(MultiplicityCase newMultiplicityCase) {
		if (newMultiplicityCase == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate multiplicityCase$Update(MultiplicityCase trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MultiplicityCase triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMTyped_MultiplicityCase(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String typeAsOcl(MPackage oclPackage, MClassifier mClassifier,
			SimpleType simpleType, Boolean singular) {

		/**
		 * @OCL if  mClassifier.oclIsUndefined() and (simpleType = SimpleType::None)  
		then 'MISSING TYPE' 
		else let t: String = 
		if  simpleType<>SimpleType::None
		then 
		if simpleType=SimpleType::Double  then 'Real' 
		else if simpleType=SimpleType::Object then 'ecore::EObject'
		else if simpleType=SimpleType::Annotation then 'ecore::EAnnotation'
		else if simpleType=SimpleType::Attribute then 'ecore::EAttribute'
		else if simpleType=SimpleType::Class then 'ecore::EClass'
		else if simpleType=SimpleType::Classifier then 'ecore::EClassifier'
		else if simpleType=SimpleType::DataType then 'ecore::EDataType'
		else if simpleType=SimpleType::Enumeration then 'ecore::Enumeration'
		else if simpleType=SimpleType::Feature then 'ecore::EStructuralFeature'
		else if simpleType=SimpleType::Literal then 'ecore::EEnumLiteral'
		else if simpleType=SimpleType::KeyValue then 'ecore::EStringToStringMapEntry'
		else if simpleType=SimpleType::NamedElement then 'ecore::NamedElement'
		else if simpleType=SimpleType::Operation then 'ecore::EOperation'
		else if simpleType=SimpleType::Package then 'ecore::EPackage'
		else if simpleType=SimpleType::Parameter then 'ecore::EParameter'
		else if simpleType=SimpleType::Reference then 'ecore::EReference'
		else if simpleType=SimpleType::TypedElement then 'ecore::ETypedElement'
		else if simpleType = SimpleType::Date then 'ecore::EDate' 
		else simpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif
		else 
		/* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified 
		let pkg: String = 
		if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat('::')
		  else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then '' else mClassifier.containingPackage.eName.allLowerCase().concat('::') endif 
		endif
		in pkg.concat(mClassifier.eName) 
		NEW Create string with all package qualifiers *\/
		let pkg:String=
		if mClassifier.containingClassifier   =null then ''
		else mClassifier.containingPackage.qualifiedName.concat('::' ) endif                            in
		pkg.concat(mClassifier.eName)
		endif
		in 
		
		if  singular.oclIsUndefined() then 'MISSING ARITY INFO' else 
		if singular=true then t else 'OrderedSet('.concat(t).concat(') ') endif
		endif
		endif
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MTYPED);
		EOperation eOperation = McorePackage.Literals.MTYPED.getEOperations()
				.get(1);
		if (typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MVARIABLE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				typeAsOclmcoreMPackagemcoreMClassifiermcoreSimpleTypeecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MVARIABLE, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("oclPackage", oclPackage);

			evalEnv.add("mClassifier", mClassifier);

			evalEnv.add("simpleType", simpleType);

			evalEnv.add("singular", singular);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MVARIABLE__MULTIPLICITY_AS_STRING:
			return getMultiplicityAsString();
		case McorePackage.MVARIABLE__CALCULATED_TYPE:
			if (resolve)
				return getCalculatedType();
			return basicGetCalculatedType();
		case McorePackage.MVARIABLE__CALCULATED_MANDATORY:
			return getCalculatedMandatory();
		case McorePackage.MVARIABLE__CALCULATED_SINGULAR:
			return getCalculatedSingular();
		case McorePackage.MVARIABLE__CALCULATED_TYPE_PACKAGE:
			if (resolve)
				return getCalculatedTypePackage();
			return basicGetCalculatedTypePackage();
		case McorePackage.MVARIABLE__VOID_TYPE_ALLOWED:
			return getVoidTypeAllowed();
		case McorePackage.MVARIABLE__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType();
		case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
			return getMultiplicityCase();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
			setMultiplicityCase((MultiplicityCase) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
			setMultiplicityCase(MULTIPLICITY_CASE_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MVARIABLE__MULTIPLICITY_AS_STRING:
			return MULTIPLICITY_AS_STRING_EDEFAULT == null
					? getMultiplicityAsString() != null
					: !MULTIPLICITY_AS_STRING_EDEFAULT
							.equals(getMultiplicityAsString());
		case McorePackage.MVARIABLE__CALCULATED_TYPE:
			return basicGetCalculatedType() != null;
		case McorePackage.MVARIABLE__CALCULATED_MANDATORY:
			return CALCULATED_MANDATORY_EDEFAULT == null
					? getCalculatedMandatory() != null
					: !CALCULATED_MANDATORY_EDEFAULT
							.equals(getCalculatedMandatory());
		case McorePackage.MVARIABLE__CALCULATED_SINGULAR:
			return CALCULATED_SINGULAR_EDEFAULT == null
					? getCalculatedSingular() != null
					: !CALCULATED_SINGULAR_EDEFAULT
							.equals(getCalculatedSingular());
		case McorePackage.MVARIABLE__CALCULATED_TYPE_PACKAGE:
			return basicGetCalculatedTypePackage() != null;
		case McorePackage.MVARIABLE__VOID_TYPE_ALLOWED:
			return VOID_TYPE_ALLOWED_EDEFAULT == null
					? getVoidTypeAllowed() != null
					: !VOID_TYPE_ALLOWED_EDEFAULT.equals(getVoidTypeAllowed());
		case McorePackage.MVARIABLE__CALCULATED_SIMPLE_TYPE:
			return getCalculatedSimpleType() != CALCULATED_SIMPLE_TYPE_EDEFAULT;
		case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
			return getMultiplicityCase() != MULTIPLICITY_CASE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (derivedFeatureID) {
			case McorePackage.MVARIABLE__MULTIPLICITY_AS_STRING:
				return McorePackage.MTYPED__MULTIPLICITY_AS_STRING;
			case McorePackage.MVARIABLE__CALCULATED_TYPE:
				return McorePackage.MTYPED__CALCULATED_TYPE;
			case McorePackage.MVARIABLE__CALCULATED_MANDATORY:
				return McorePackage.MTYPED__CALCULATED_MANDATORY;
			case McorePackage.MVARIABLE__CALCULATED_SINGULAR:
				return McorePackage.MTYPED__CALCULATED_SINGULAR;
			case McorePackage.MVARIABLE__CALCULATED_TYPE_PACKAGE:
				return McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE;
			case McorePackage.MVARIABLE__VOID_TYPE_ALLOWED:
				return McorePackage.MTYPED__VOID_TYPE_ALLOWED;
			case McorePackage.MVARIABLE__CALCULATED_SIMPLE_TYPE:
				return McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE;
			case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
				return McorePackage.MTYPED__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseFeatureID) {
			case McorePackage.MTYPED__MULTIPLICITY_AS_STRING:
				return McorePackage.MVARIABLE__MULTIPLICITY_AS_STRING;
			case McorePackage.MTYPED__CALCULATED_TYPE:
				return McorePackage.MVARIABLE__CALCULATED_TYPE;
			case McorePackage.MTYPED__CALCULATED_MANDATORY:
				return McorePackage.MVARIABLE__CALCULATED_MANDATORY;
			case McorePackage.MTYPED__CALCULATED_SINGULAR:
				return McorePackage.MVARIABLE__CALCULATED_SINGULAR;
			case McorePackage.MTYPED__CALCULATED_TYPE_PACKAGE:
				return McorePackage.MVARIABLE__CALCULATED_TYPE_PACKAGE;
			case McorePackage.MTYPED__VOID_TYPE_ALLOWED:
				return McorePackage.MVARIABLE__VOID_TYPE_ALLOWED;
			case McorePackage.MTYPED__CALCULATED_SIMPLE_TYPE:
				return McorePackage.MVARIABLE__CALCULATED_SIMPLE_TYPE;
			case McorePackage.MTYPED__MULTIPLICITY_CASE:
				return McorePackage.MVARIABLE__MULTIPLICITY_CASE;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MTyped.class) {
			switch (baseOperationID) {
			case McorePackage.MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
				return McorePackage.MVARIABLE___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE;
			case McorePackage.MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
				return McorePackage.MVARIABLE___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MVARIABLE___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE:
			return multiplicityCase$Update((MultiplicityCase) arguments.get(0));
		case McorePackage.MVARIABLE___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN:
			return typeAsOcl((MPackage) arguments.get(0),
					(MClassifier) arguments.get(1),
					(SimpleType) arguments.get(2), (Boolean) arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL fullLabel 'TODO'
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MVARIABLE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())
	then self.calculatedShortName.camelCaseLower()
	else self.specialEName.camelCaseLower()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MVARIABLE);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MVariableImpl
