/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.langlets.acore.AComponent;
import org.langlets.acore.APackage;
import org.langlets.acore.abstractions.AElement;
import org.langlets.acore.abstractions.ANamed;
import org.langlets.acore.abstractions.AbstractionsPackage;
import org.langlets.acore.classifiers.AClass;
import org.langlets.acore.classifiers.AClassifier;
import org.langlets.acore.classifiers.ADataType;
import org.langlets.acore.classifiers.AFeature;
import org.langlets.acore.classifiers.ClassifiersPackage;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.XUpdate;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MParameter;
import com.montages.mcore.MParameterAction;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.MGeneralAnnotation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MParameter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getGeneralAnnotation <em>General Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTPackageUri <em>TPackage Uri</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTClassifierName <em>TClassifier Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTFeatureName <em>TFeature Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTPackage <em>TPackage</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTClassifier <em>TClassifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTFeature <em>TFeature</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getTACoreAStringClass <em>TA Core AString Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAName <em>AName</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getABusinessName <em>ABusiness Name</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getASpecializedClassifier <em>ASpecialized Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAActiveDataType <em>AActive Data Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAActiveEnumeration <em>AActive Enumeration</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAActiveClass <em>AActive Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAContainingPackage <em>AContaining Package</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAsDataType <em>As Data Type</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAsClass <em>As Class</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getAIsStringClassifier <em>AIs String Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getContainingSignature <em>Containing Signature</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getSignatureAsString <em>Signature As String</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getInternalEParameter <em>Internal EParameter</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MParameterImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MParameterImpl extends MExplicitlyTypedAndNamedImpl
		implements MParameter {
	/**
	 * The cached value of the '{@link #getGeneralAnnotation() <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralAnnotation> generalAnnotation;

	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getTPackageUri() <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackageUri()
	 * @generated
	 * @ordered
	 */
	protected static final String TPACKAGE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTPackageUri() <em>TPackage Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackageUri()
	 * @generated
	 * @ordered
	 */
	protected String tPackageUri = TPACKAGE_URI_EDEFAULT;

	/**
	 * This is true if the TPackage Uri attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tPackageUriESet;

	/**
	 * The default value of the '{@link #getTClassifierName() <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifierName()
	 * @generated
	 * @ordered
	 */
	protected static final String TCLASSIFIER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTClassifierName() <em>TClassifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifierName()
	 * @generated
	 * @ordered
	 */
	protected String tClassifierName = TCLASSIFIER_NAME_EDEFAULT;

	/**
	 * This is true if the TClassifier Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tClassifierNameESet;

	/**
	 * The default value of the '{@link #getTFeatureName() <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeatureName()
	 * @generated
	 * @ordered
	 */
	protected static final String TFEATURE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getTFeatureName() <em>TFeature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeatureName()
	 * @generated
	 * @ordered
	 */
	protected String tFeatureName = TFEATURE_NAME_EDEFAULT;

	/**
	 * This is true if the TFeature Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean tFeatureNameESet;

	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessName() <em>ABusiness Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveDataType() <em>AActive Data Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveDataType()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_DATA_TYPE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveEnumeration() <em>AActive Enumeration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveEnumeration()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_ENUMERATION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAActiveClass() <em>AActive Class</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveClass()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_CLASS_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAIsStringClassifier() <em>AIs String Classifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsStringClassifier()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AIS_STRING_CLASSIFIER_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getELabel() <em>ELabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getELabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ELABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getSignatureAsString() <em>Signature As String</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureAsString()
	 * @generated
	 * @ordered
	 */
	protected static final String SIGNATURE_AS_STRING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInternalEParameter() <em>Internal EParameter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInternalEParameter()
	 * @generated
	 * @ordered
	 */
	protected EParameter internalEParameter;

	/**
	 * This is true if the Internal EParameter reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean internalEParameterESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MParameterAction DO_ACTION_EDEFAULT = MParameterAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMParameterActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ambiguousParameterName <em>Ambiguous Parameter Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ambiguousParameterName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ambiguousParameterNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ambiguousParameterShortName <em>Ambiguous Parameter Short Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ambiguousParameterShortName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ambiguousParameterShortNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aAssignableTo <em>AAssignable To</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aAssignableTo
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aAssignableToclassifiersAClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentLevel <em>Indent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #indentationSpaces <em>Indentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #indentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression indentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringOrMissing <em>String Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #stringIsEmpty <em>String Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #stringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression stringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #listOfStringToStringWithSeparator <em>List Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #listOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression listOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aPackageFromUri <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aPackageFromUri
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aPackageFromUriecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aClassifierFromUriAndName <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aClassifierFromUriAndName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aFeatureFromUriAndNames <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aFeatureFromUriAndNames
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAStringClass <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAStringClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAStringClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreARealClass <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreARealClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreARealClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAIntegerClass <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAIntegerClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAIntegerClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAObjectClass <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAObjectClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAObjectClassBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase <em>AKind Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTPackage <em>TPackage</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTClassifier <em>TClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTFeature <em>TFeature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTACoreAStringClass <em>TA Core AString Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTACoreAStringClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression tACoreAStringClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessName <em>ABusiness Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASpecializedClassifier <em>ASpecialized Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASpecializedClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSpecializedClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveDataType <em>AActive Data Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveEnumeration <em>AActive Enumeration</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveEnumeration
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveEnumerationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveClass <em>AActive Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingPackage <em>AContaining Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsDataType <em>As Data Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsDataType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asDataTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAsClass <em>As Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAsClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression asClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAIsStringClassifier <em>AIs String Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAIsStringClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aIsStringClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingSignature <em>Containing Signature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingSignature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingSignatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getELabel <em>ELabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getELabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSignatureAsString <em>Signature As String</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSignatureAsString
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression signatureAsStringDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectName <em>Correct Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectShortName <em>Correct Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCorrectShortName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedType <em>Calculated Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSimpleTypeIsCorrect
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression simpleTypeIsCorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedName <em>Calculated Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedShortName <em>Calculated Short Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCalculatedShortName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel <em>Full Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MParameterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MPARAMETER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getGeneralAnnotation() {
		if (generalAnnotation == null) {
			generalAnnotation = new EObjectContainmentEList.Unsettable.Resolving<MGeneralAnnotation>(
					MGeneralAnnotation.class, this,
					McorePackage.MPARAMETER__GENERAL_ANNOTATION);
		}
		return generalAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralAnnotation() {
		if (generalAnnotation != null)
			((InternalEList.Unsettable<?>) generalAnnotation).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralAnnotation() {
		return generalAnnotation != null
				&& ((InternalEList.Unsettable<?>) generalAnnotation).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let e1: String = indentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent getAContainingComponent() {
		AComponent aContainingComponent = basicGetAContainingComponent();
		return aContainingComponent != null && aContainingComponent.eIsProxy()
				? (AComponent) eResolveProxy(
						(InternalEObject) aContainingComponent)
				: aContainingComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent basicGetAContainingComponent() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(acore::AComponent)
		then self.eContainer().oclAsType(acore::AComponent)
		else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)
		then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent
		else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AComponent result = (AComponent) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTPackageUri() {
		return tPackageUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTPackageUri(String newTPackageUri) {
		String oldTPackageUri = tPackageUri;
		tPackageUri = newTPackageUri;
		boolean oldTPackageUriESet = tPackageUriESet;
		tPackageUriESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPARAMETER__TPACKAGE_URI, oldTPackageUri,
					tPackageUri, !oldTPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTPackageUri() {
		String oldTPackageUri = tPackageUri;
		boolean oldTPackageUriESet = tPackageUriESet;
		tPackageUri = TPACKAGE_URI_EDEFAULT;
		tPackageUriESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPARAMETER__TPACKAGE_URI, oldTPackageUri,
					TPACKAGE_URI_EDEFAULT, oldTPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTPackageUri() {
		return tPackageUriESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTClassifierName() {
		return tClassifierName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTClassifierName(String newTClassifierName) {
		String oldTClassifierName = tClassifierName;
		tClassifierName = newTClassifierName;
		boolean oldTClassifierNameESet = tClassifierNameESet;
		tClassifierNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPARAMETER__TCLASSIFIER_NAME,
					oldTClassifierName, tClassifierName,
					!oldTClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTClassifierName() {
		String oldTClassifierName = tClassifierName;
		boolean oldTClassifierNameESet = tClassifierNameESet;
		tClassifierName = TCLASSIFIER_NAME_EDEFAULT;
		tClassifierNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPARAMETER__TCLASSIFIER_NAME,
					oldTClassifierName, TCLASSIFIER_NAME_EDEFAULT,
					oldTClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTClassifierName() {
		return tClassifierNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getTFeatureName() {
		return tFeatureName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTFeatureName(String newTFeatureName) {
		String oldTFeatureName = tFeatureName;
		tFeatureName = newTFeatureName;
		boolean oldTFeatureNameESet = tFeatureNameESet;
		tFeatureNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPARAMETER__TFEATURE_NAME, oldTFeatureName,
					tFeatureName, !oldTFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetTFeatureName() {
		String oldTFeatureName = tFeatureName;
		boolean oldTFeatureNameESet = tFeatureNameESet;
		tFeatureName = TFEATURE_NAME_EDEFAULT;
		tFeatureNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPARAMETER__TFEATURE_NAME, oldTFeatureName,
					TFEATURE_NAME_EDEFAULT, oldTFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTFeatureName() {
		return tFeatureNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getTPackage() {
		APackage tPackage = basicGetTPackage();
		return tPackage != null && tPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) tPackage)
				: tPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetTPackage() {
		/**
		 * @OCL let p: String = tPackageUri in
		aPackageFromUri(p)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TPACKAGE;

		if (tPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				tPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getTClassifier() {
		AClassifier tClassifier = basicGetTClassifier();
		return tClassifier != null && tClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) tClassifier)
				: tClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetTClassifier() {
		/**
		 * @OCL let p: String = tPackageUri in
		let c: String = tClassifierName in
		aClassifierFromUriAndName(p, c)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TCLASSIFIER;

		if (tClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				tClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getTFeature() {
		AFeature tFeature = basicGetTFeature();
		return tFeature != null && tFeature.eIsProxy()
				? (AFeature) eResolveProxy((InternalEObject) tFeature)
				: tFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetTFeature() {
		/**
		 * @OCL let p: String = tPackageUri in
		let c: String = tClassifierName in
		let f: String = tFeatureName in
		aFeatureFromUriAndNames(p, c, f)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TFEATURE;

		if (tFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				tFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getTACoreAStringClass() {
		AClassifier tACoreAStringClass = basicGetTACoreAStringClass();
		return tACoreAStringClass != null && tACoreAStringClass.eIsProxy()
				? (AClassifier) eResolveProxy(
						(InternalEObject) tACoreAStringClass)
				: tACoreAStringClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetTACoreAStringClass() {
		/**
		 * @OCL aCoreAStringClass()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__TA_CORE_ASTRING_CLASS;

		if (tACoreAStringClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				tACoreAStringClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(tACoreAStringClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL '<A Name Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME;

		if (aBusinessNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aBusinessNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AClassifier> getASpecializedClassifier() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__ASPECIALIZED_CLASSIFIER;

		if (aSpecializedClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aSpecializedClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSpecializedClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AClassifier> result = (EList<AClassifier>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveDataType() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_DATA_TYPE;

		if (aActiveDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aActiveDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveEnumeration() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_ENUMERATION;

		if (aActiveEnumerationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aActiveEnumerationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveEnumerationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveClass() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_CLASS;

		if (aActiveClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aActiveClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getAContainingPackage() {
		APackage aContainingPackage = basicGetAContainingPackage();
		return aContainingPackage != null && aContainingPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aContainingPackage)
				: aContainingPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetAContainingPackage() {
		/**
		 * @OCL let nl: acore::APackage = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__ACONTAINING_PACKAGE;

		if (aContainingPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aContainingPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType getAsDataType() {
		ADataType asDataType = basicGetAsDataType();
		return asDataType != null && asDataType.eIsProxy()
				? (ADataType) eResolveProxy((InternalEObject) asDataType)
				: asDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ADataType basicGetAsDataType() {
		/**
		 * @OCL if (let e0: Boolean = if (aActiveDataType)= true 
		then true 
		else if (aActiveEnumeration)= true 
		then true 
		else if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true 
		then null 
		else false endif endif endif in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then let chain: acore::classifiers::AClassifier = self in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::ADataType)
		then chain.oclAsType(acore::classifiers::ADataType)
		else null
		endif
		endif
		else null
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AS_DATA_TYPE;

		if (asDataTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asDataTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asDataTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			ADataType result = (ADataType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass getAsClass() {
		AClass asClass = basicGetAsClass();
		return asClass != null && asClass.eIsProxy()
				? (AClass) eResolveProxy((InternalEObject) asClass) : asClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClass basicGetAsClass() {
		/**
		 * @OCL if (aActiveClass) 
		=true 
		then let chain: acore::classifiers::AClassifier = self in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClass)
		then chain.oclAsType(acore::classifiers::AClass)
		else null
		endif
		endif
		else null
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AS_CLASS;

		if (asClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				asClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(asClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClass result = (AClass) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAIsStringClassifier() {
		/**
		 * @OCL let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aUri
		endif = 'http://www.langlets.org/ACoreC/ACore/Classifiers' in 
		if e2.oclIsInvalid() then null else e2 endif))= false 
		then false 
		else if (let e3: Boolean = aName = 'AString' in 
		if e3.oclIsInvalid() then null else e3 endif)= false 
		then false 
		else if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aUri
		endif = 'http://www.langlets.org/ACoreC/ACore/Classifiers' in 
		if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = 'AString' in 
		if e3.oclIsInvalid() then null else e3 endif)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = ClassifiersPackage.Literals.ACLASSIFIER__AIS_STRING_CLASSIFIER;

		if (aIsStringClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				aIsStringClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aIsStringClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature getContainingSignature() {
		MOperationSignature containingSignature = basicGetContainingSignature();
		return containingSignature != null && containingSignature.eIsProxy()
				? (MOperationSignature) eResolveProxy(
						(InternalEObject) containingSignature)
				: containingSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature basicGetContainingSignature() {
		/**
		 * @OCL self.eContainer().oclAsType(MOperationSignature)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = McorePackage.Literals.MPARAMETER__CONTAINING_SIGNATURE;

		if (containingSignatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingSignatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingSignatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MOperationSignature result = (MOperationSignature) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getELabel() {
		/**
		 * @OCL if stringEmpty(name)
		then self.eTypeLabel 
		else eName
		         .concat(if self.eTypeLabel=''
		                 then '' else ':'.concat(self.eTypeLabel) endif) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = McorePackage.Literals.MPARAMETER__ELABEL;

		if (eLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSignatureAsString() {
		/**
		 * @OCL if self.type.oclIsUndefined() then 'TYPE_MISSING' else self.type.eName.concat(self.multiplicityAsString) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = McorePackage.Literals.MPARAMETER__SIGNATURE_AS_STRING;

		if (signatureAsStringDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				signatureAsStringDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(signatureAsStringDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter getInternalEParameter() {
		if (internalEParameter != null && internalEParameter.eIsProxy()) {
			InternalEObject oldInternalEParameter = (InternalEObject) internalEParameter;
			internalEParameter = (EParameter) eResolveProxy(
					oldInternalEParameter);
			if (internalEParameter != oldInternalEParameter) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPARAMETER__INTERNAL_EPARAMETER,
							oldInternalEParameter, internalEParameter));
			}
		}
		return internalEParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EParameter basicGetInternalEParameter() {
		return internalEParameter;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalEParameter(EParameter newInternalEParameter) {
		EParameter oldInternalEParameter = internalEParameter;
		internalEParameter = newInternalEParameter;
		boolean oldInternalEParameterESet = internalEParameterESet;
		internalEParameterESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPARAMETER__INTERNAL_EPARAMETER,
					oldInternalEParameter, internalEParameter,
					!oldInternalEParameterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalEParameter() {
		EParameter oldInternalEParameter = internalEParameter;
		boolean oldInternalEParameterESet = internalEParameterESet;
		internalEParameter = null;
		internalEParameterESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPARAMETER__INTERNAL_EPARAMETER,
					oldInternalEParameter, null, oldInternalEParameterESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalEParameter() {
		return internalEParameterESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MParameterAction getDoAction() {
		/**
		 * @OCL let do: mcore::MParameterAction = mcore::MParameterAction::Do in
		do
		
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPARAMETER;
		EStructuralFeature eFeature = McorePackage.Literals.MPARAMETER__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MParameterAction result = (MParameterAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MParameterAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MParameterAction.INTO_UNARY_VALUE:
			this.setMultiplicityCase(MultiplicityCase.ZERO_ONE);
			break;
		case MParameterAction.INTO_NARY_VALUE:
			this.setMultiplicityCase(MultiplicityCase.ZERO_MANY);
			break;
		case MParameterAction.INTO_STRING_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.STRING);
			break;
		case MParameterAction.INTO_BOOLEAN_FLAG_VALUE:
			this.setSimpleType(SimpleType.BOOLEAN);
			break;
		case MParameterAction.INTO_INTEGER_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.INTEGER);
			break;
		case MParameterAction.INTO_REAL_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.DOUBLE);
			break;
		case MParameterAction.INTO_DATE_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.DATE);
			break;
		default:
			break;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MParameterAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MParameterAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMParameter_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ambiguousParameterName() {

		/**
		 * @OCL if true then false else
		if self.containingSignature.oclIsUndefined() then false else
		if self.containingSignature.parameter->isEmpty() then false else
		self.containingSignature.parameter
		->exists(p:MParameter|not (p=self) 
		and p.sameName(self)
		and (if stringEmpty(p.name) then 
		        p.type = self.type else true endif))
		endif endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EOperation eOperation = McorePackage.Literals.MPARAMETER
				.getEOperations().get(1);
		if (ambiguousParameterNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ambiguousParameterNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ambiguousParameterNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ambiguousParameterShortName() {

		/**
		 * @OCL if true then false else 
		if not self.ambiguousParameterName() then false else
		if self.containingSignature.oclIsUndefined() then false else
		if self.containingSignature.parameter->isEmpty() then false else
		self.containingSignature.parameter
		->exists(p:MParameter|not (p=self) 
		and p.sameShortName(self))
		endif endif endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EOperation eOperation = McorePackage.Literals.MPARAMETER
				.getEOperations().get(2);
		if (ambiguousParameterShortNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ambiguousParameterShortNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ambiguousParameterShortNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean aAssignableTo(AClassifier aClassifier) {

		/**
		 * @OCL if (let e0: Boolean = self = aClassifier in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then true
		else let e0: Boolean = aSpecializedClassifier.aAssignableTo(aClassifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in 
		if e0.oclIsInvalid() then null else e0 endif
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ClassifiersPackage.Literals.ACLASSIFIER);
		EOperation eOperation = ClassifiersPackage.Literals.ACLASSIFIER
				.getEOperations().get(0);
		if (aAssignableToclassifiersAClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aAssignableToclassifiersAClassifierBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(aAssignableToclassifiersAClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("aClassifier", aClassifier);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer indentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).indentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(0);
		if (indentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				indentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String indentationSpaces() {

		/**
		 * @OCL indentationSpaces(indentLevel * 4)
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MREPOSITORY_ELEMENT);
		EOperation eOperation = McorePackage.Literals.MREPOSITORY_ELEMENT
				.getEOperations().get(0);
		if (indentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				indentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MREPOSITORY_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(indentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MREPOSITORY_ELEMENT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public String indentationSpaces(Integer size) {

		/**
		 * @OCL if size < 1 then ''
		else self.indentationSpaces(size-1).concat(' ') endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MREPOSITORY_ELEMENT);
		EOperation eOperation = McorePackage.Literals.MREPOSITORY_ELEMENT
				.getEOperations().get(1);
		if (indentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				indentationSpacesecoreEIntegerObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MREPOSITORY_ELEMENT, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(indentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MREPOSITORY_ELEMENT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String stringOrMissing(String p) {

		/**
		 * @OCL if (stringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(3);
		if (stringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				stringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean stringIsEmpty(String s) {

		/**
		 * @OCL if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(4);
		if (stringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				stringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(stringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements,
			String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(5);
		if (listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				listOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String listOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		listOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(6);
		if (listOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				listOfStringToStringWithSeparatorecoreEStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				listOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage aPackageFromUri(String packageUri) {

		/**
		 * @OCL if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aPackageFromUri(packageUri)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(7);
		if (aPackageFromUriecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aPackageFromUriecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageFromUriecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("packageUri", packageUri);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aClassifierFromUriAndName(String uri, String name) {

		/**
		 * @OCL let p: acore::APackage = aPackageFromUri(uri) in
		if p = null
		then null
		else p.aClassifierFromName(name) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(8);
		if (aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("name", name);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature aFeatureFromUriAndNames(String uri, String className,
			String featureName) {

		/**
		 * @OCL let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in
		let cAsClass: acore::classifiers::AClass = let chain: acore::classifiers::AClassifier = c in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClass)
		then chain.oclAsType(acore::classifiers::AClass)
		else null
		endif
		endif in
		if cAsClass = null
		then null
		else cAsClass.aFeatureFromName(featureName) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(9);
		if (aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("className", className);

			evalEnv.add("featureName", featureName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAStringClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAStringName: String = 'AString' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(10);
		if (aCoreAStringClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aCoreAStringClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAStringClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreARealClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreARealName: String = 'AReal' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(11);
		if (aCoreARealClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aCoreARealClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreARealClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAIntegerClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAIntegerName: String = 'AInteger' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(12);
		if (aCoreAIntegerClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aCoreAIntegerClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAIntegerClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAObjectClass() {

		/**
		 * @OCL let aCoreValuesPackageUri: String = 'http://www.langlets.org/ACore/ACore/Values' in
		let aCoreAObjectName: String = 'AObject' in
		aClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT
				.getEOperations().get(13);
		if (aCoreAObjectClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				aCoreAObjectClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPARAMETER, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAObjectClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPARAMETER, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			return ((InternalEList<?>) getGeneralAnnotation())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			return getGeneralAnnotation();
		case McorePackage.MPARAMETER__ALABEL:
			return getALabel();
		case McorePackage.MPARAMETER__AKIND_BASE:
			return getAKindBase();
		case McorePackage.MPARAMETER__ARENDERED_KIND:
			return getARenderedKind();
		case McorePackage.MPARAMETER__ACONTAINING_COMPONENT:
			if (resolve)
				return getAContainingComponent();
			return basicGetAContainingComponent();
		case McorePackage.MPARAMETER__TPACKAGE_URI:
			return getTPackageUri();
		case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
			return getTClassifierName();
		case McorePackage.MPARAMETER__TFEATURE_NAME:
			return getTFeatureName();
		case McorePackage.MPARAMETER__TPACKAGE:
			if (resolve)
				return getTPackage();
			return basicGetTPackage();
		case McorePackage.MPARAMETER__TCLASSIFIER:
			if (resolve)
				return getTClassifier();
			return basicGetTClassifier();
		case McorePackage.MPARAMETER__TFEATURE:
			if (resolve)
				return getTFeature();
			return basicGetTFeature();
		case McorePackage.MPARAMETER__TA_CORE_ASTRING_CLASS:
			if (resolve)
				return getTACoreAStringClass();
			return basicGetTACoreAStringClass();
		case McorePackage.MPARAMETER__ANAME:
			return getAName();
		case McorePackage.MPARAMETER__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case McorePackage.MPARAMETER__ABUSINESS_NAME:
			return getABusinessName();
		case McorePackage.MPARAMETER__ASPECIALIZED_CLASSIFIER:
			return getASpecializedClassifier();
		case McorePackage.MPARAMETER__AACTIVE_DATA_TYPE:
			return getAActiveDataType();
		case McorePackage.MPARAMETER__AACTIVE_ENUMERATION:
			return getAActiveEnumeration();
		case McorePackage.MPARAMETER__AACTIVE_CLASS:
			return getAActiveClass();
		case McorePackage.MPARAMETER__ACONTAINING_PACKAGE:
			if (resolve)
				return getAContainingPackage();
			return basicGetAContainingPackage();
		case McorePackage.MPARAMETER__AS_DATA_TYPE:
			if (resolve)
				return getAsDataType();
			return basicGetAsDataType();
		case McorePackage.MPARAMETER__AS_CLASS:
			if (resolve)
				return getAsClass();
			return basicGetAsClass();
		case McorePackage.MPARAMETER__AIS_STRING_CLASSIFIER:
			return getAIsStringClassifier();
		case McorePackage.MPARAMETER__CONTAINING_SIGNATURE:
			if (resolve)
				return getContainingSignature();
			return basicGetContainingSignature();
		case McorePackage.MPARAMETER__ELABEL:
			return getELabel();
		case McorePackage.MPARAMETER__SIGNATURE_AS_STRING:
			return getSignatureAsString();
		case McorePackage.MPARAMETER__INTERNAL_EPARAMETER:
			if (resolve)
				return getInternalEParameter();
			return basicGetInternalEParameter();
		case McorePackage.MPARAMETER__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			getGeneralAnnotation().clear();
			getGeneralAnnotation().addAll(
					(Collection<? extends MGeneralAnnotation>) newValue);
			return;
		case McorePackage.MPARAMETER__TPACKAGE_URI:
			setTPackageUri((String) newValue);
			return;
		case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
			setTClassifierName((String) newValue);
			return;
		case McorePackage.MPARAMETER__TFEATURE_NAME:
			setTFeatureName((String) newValue);
			return;
		case McorePackage.MPARAMETER__INTERNAL_EPARAMETER:
			setInternalEParameter((EParameter) newValue);
			return;
		case McorePackage.MPARAMETER__DO_ACTION:
			setDoAction((MParameterAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			unsetGeneralAnnotation();
			return;
		case McorePackage.MPARAMETER__TPACKAGE_URI:
			unsetTPackageUri();
			return;
		case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
			unsetTClassifierName();
			return;
		case McorePackage.MPARAMETER__TFEATURE_NAME:
			unsetTFeatureName();
			return;
		case McorePackage.MPARAMETER__INTERNAL_EPARAMETER:
			unsetInternalEParameter();
			return;
		case McorePackage.MPARAMETER__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
			return isSetGeneralAnnotation();
		case McorePackage.MPARAMETER__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null
					: !ALABEL_EDEFAULT.equals(getALabel());
		case McorePackage.MPARAMETER__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null
					: !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case McorePackage.MPARAMETER__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		case McorePackage.MPARAMETER__ACONTAINING_COMPONENT:
			return basicGetAContainingComponent() != null;
		case McorePackage.MPARAMETER__TPACKAGE_URI:
			return isSetTPackageUri();
		case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
			return isSetTClassifierName();
		case McorePackage.MPARAMETER__TFEATURE_NAME:
			return isSetTFeatureName();
		case McorePackage.MPARAMETER__TPACKAGE:
			return basicGetTPackage() != null;
		case McorePackage.MPARAMETER__TCLASSIFIER:
			return basicGetTClassifier() != null;
		case McorePackage.MPARAMETER__TFEATURE:
			return basicGetTFeature() != null;
		case McorePackage.MPARAMETER__TA_CORE_ASTRING_CLASS:
			return basicGetTACoreAStringClass() != null;
		case McorePackage.MPARAMETER__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null
					: !ANAME_EDEFAULT.equals(getAName());
		case McorePackage.MPARAMETER__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null
					? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT
							.equals(getAUndefinedNameConstant());
		case McorePackage.MPARAMETER__ABUSINESS_NAME:
			return ABUSINESS_NAME_EDEFAULT == null ? getABusinessName() != null
					: !ABUSINESS_NAME_EDEFAULT.equals(getABusinessName());
		case McorePackage.MPARAMETER__ASPECIALIZED_CLASSIFIER:
			return !getASpecializedClassifier().isEmpty();
		case McorePackage.MPARAMETER__AACTIVE_DATA_TYPE:
			return AACTIVE_DATA_TYPE_EDEFAULT == null
					? getAActiveDataType() != null
					: !AACTIVE_DATA_TYPE_EDEFAULT.equals(getAActiveDataType());
		case McorePackage.MPARAMETER__AACTIVE_ENUMERATION:
			return AACTIVE_ENUMERATION_EDEFAULT == null
					? getAActiveEnumeration() != null
					: !AACTIVE_ENUMERATION_EDEFAULT
							.equals(getAActiveEnumeration());
		case McorePackage.MPARAMETER__AACTIVE_CLASS:
			return AACTIVE_CLASS_EDEFAULT == null ? getAActiveClass() != null
					: !AACTIVE_CLASS_EDEFAULT.equals(getAActiveClass());
		case McorePackage.MPARAMETER__ACONTAINING_PACKAGE:
			return basicGetAContainingPackage() != null;
		case McorePackage.MPARAMETER__AS_DATA_TYPE:
			return basicGetAsDataType() != null;
		case McorePackage.MPARAMETER__AS_CLASS:
			return basicGetAsClass() != null;
		case McorePackage.MPARAMETER__AIS_STRING_CLASSIFIER:
			return AIS_STRING_CLASSIFIER_EDEFAULT == null
					? getAIsStringClassifier() != null
					: !AIS_STRING_CLASSIFIER_EDEFAULT
							.equals(getAIsStringClassifier());
		case McorePackage.MPARAMETER__CONTAINING_SIGNATURE:
			return basicGetContainingSignature() != null;
		case McorePackage.MPARAMETER__ELABEL:
			return ELABEL_EDEFAULT == null ? getELabel() != null
					: !ELABEL_EDEFAULT.equals(getELabel());
		case McorePackage.MPARAMETER__SIGNATURE_AS_STRING:
			return SIGNATURE_AS_STRING_EDEFAULT == null
					? getSignatureAsString() != null
					: !SIGNATURE_AS_STRING_EDEFAULT
							.equals(getSignatureAsString());
		case McorePackage.MPARAMETER__INTERNAL_EPARAMETER:
			return isSetInternalEParameter();
		case McorePackage.MPARAMETER__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MVariable.class) {
			switch (derivedFeatureID) {
			default:
				return -1;
			}
		}
		if (baseClass == MModelElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPARAMETER__GENERAL_ANNOTATION:
				return McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		if (baseClass == AElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPARAMETER__ALABEL:
				return AbstractionsPackage.AELEMENT__ALABEL;
			case McorePackage.MPARAMETER__AKIND_BASE:
				return AbstractionsPackage.AELEMENT__AKIND_BASE;
			case McorePackage.MPARAMETER__ARENDERED_KIND:
				return AbstractionsPackage.AELEMENT__ARENDERED_KIND;
			case McorePackage.MPARAMETER__ACONTAINING_COMPONENT:
				return AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;
			case McorePackage.MPARAMETER__TPACKAGE_URI:
				return AbstractionsPackage.AELEMENT__TPACKAGE_URI;
			case McorePackage.MPARAMETER__TCLASSIFIER_NAME:
				return AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME;
			case McorePackage.MPARAMETER__TFEATURE_NAME:
				return AbstractionsPackage.AELEMENT__TFEATURE_NAME;
			case McorePackage.MPARAMETER__TPACKAGE:
				return AbstractionsPackage.AELEMENT__TPACKAGE;
			case McorePackage.MPARAMETER__TCLASSIFIER:
				return AbstractionsPackage.AELEMENT__TCLASSIFIER;
			case McorePackage.MPARAMETER__TFEATURE:
				return AbstractionsPackage.AELEMENT__TFEATURE;
			case McorePackage.MPARAMETER__TA_CORE_ASTRING_CLASS:
				return AbstractionsPackage.AELEMENT__TA_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPARAMETER__ANAME:
				return AbstractionsPackage.ANAMED__ANAME;
			case McorePackage.MPARAMETER__AUNDEFINED_NAME_CONSTANT:
				return AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;
			case McorePackage.MPARAMETER__ABUSINESS_NAME:
				return AbstractionsPackage.ANAMED__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AClassifier.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPARAMETER__ASPECIALIZED_CLASSIFIER:
				return ClassifiersPackage.ACLASSIFIER__ASPECIALIZED_CLASSIFIER;
			case McorePackage.MPARAMETER__AACTIVE_DATA_TYPE:
				return ClassifiersPackage.ACLASSIFIER__AACTIVE_DATA_TYPE;
			case McorePackage.MPARAMETER__AACTIVE_ENUMERATION:
				return ClassifiersPackage.ACLASSIFIER__AACTIVE_ENUMERATION;
			case McorePackage.MPARAMETER__AACTIVE_CLASS:
				return ClassifiersPackage.ACLASSIFIER__AACTIVE_CLASS;
			case McorePackage.MPARAMETER__ACONTAINING_PACKAGE:
				return ClassifiersPackage.ACLASSIFIER__ACONTAINING_PACKAGE;
			case McorePackage.MPARAMETER__AS_DATA_TYPE:
				return ClassifiersPackage.ACLASSIFIER__AS_DATA_TYPE;
			case McorePackage.MPARAMETER__AS_CLASS:
				return ClassifiersPackage.ACLASSIFIER__AS_CLASS;
			case McorePackage.MPARAMETER__AIS_STRING_CLASSIFIER:
				return ClassifiersPackage.ACLASSIFIER__AIS_STRING_CLASSIFIER;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MVariable.class) {
			switch (baseFeatureID) {
			default:
				return -1;
			}
		}
		if (baseClass == MModelElement.class) {
			switch (baseFeatureID) {
			case McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION:
				return McorePackage.MPARAMETER__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		if (baseClass == AElement.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AELEMENT__ALABEL:
				return McorePackage.MPARAMETER__ALABEL;
			case AbstractionsPackage.AELEMENT__AKIND_BASE:
				return McorePackage.MPARAMETER__AKIND_BASE;
			case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
				return McorePackage.MPARAMETER__ARENDERED_KIND;
			case AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT:
				return McorePackage.MPARAMETER__ACONTAINING_COMPONENT;
			case AbstractionsPackage.AELEMENT__TPACKAGE_URI:
				return McorePackage.MPARAMETER__TPACKAGE_URI;
			case AbstractionsPackage.AELEMENT__TCLASSIFIER_NAME:
				return McorePackage.MPARAMETER__TCLASSIFIER_NAME;
			case AbstractionsPackage.AELEMENT__TFEATURE_NAME:
				return McorePackage.MPARAMETER__TFEATURE_NAME;
			case AbstractionsPackage.AELEMENT__TPACKAGE:
				return McorePackage.MPARAMETER__TPACKAGE;
			case AbstractionsPackage.AELEMENT__TCLASSIFIER:
				return McorePackage.MPARAMETER__TCLASSIFIER;
			case AbstractionsPackage.AELEMENT__TFEATURE:
				return McorePackage.MPARAMETER__TFEATURE;
			case AbstractionsPackage.AELEMENT__TA_CORE_ASTRING_CLASS:
				return McorePackage.MPARAMETER__TA_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ANAMED__ANAME:
				return McorePackage.MPARAMETER__ANAME;
			case AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
				return McorePackage.MPARAMETER__AUNDEFINED_NAME_CONSTANT;
			case AbstractionsPackage.ANAMED__ABUSINESS_NAME:
				return McorePackage.MPARAMETER__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		if (baseClass == AClassifier.class) {
			switch (baseFeatureID) {
			case ClassifiersPackage.ACLASSIFIER__ASPECIALIZED_CLASSIFIER:
				return McorePackage.MPARAMETER__ASPECIALIZED_CLASSIFIER;
			case ClassifiersPackage.ACLASSIFIER__AACTIVE_DATA_TYPE:
				return McorePackage.MPARAMETER__AACTIVE_DATA_TYPE;
			case ClassifiersPackage.ACLASSIFIER__AACTIVE_ENUMERATION:
				return McorePackage.MPARAMETER__AACTIVE_ENUMERATION;
			case ClassifiersPackage.ACLASSIFIER__AACTIVE_CLASS:
				return McorePackage.MPARAMETER__AACTIVE_CLASS;
			case ClassifiersPackage.ACLASSIFIER__ACONTAINING_PACKAGE:
				return McorePackage.MPARAMETER__ACONTAINING_PACKAGE;
			case ClassifiersPackage.ACLASSIFIER__AS_DATA_TYPE:
				return McorePackage.MPARAMETER__AS_DATA_TYPE;
			case ClassifiersPackage.ACLASSIFIER__AS_CLASS:
				return McorePackage.MPARAMETER__AS_CLASS;
			case ClassifiersPackage.ACLASSIFIER__AIS_STRING_CLASSIFIER:
				return McorePackage.MPARAMETER__AIS_STRING_CLASSIFIER;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == MVariable.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		if (baseClass == MModelElement.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		if (baseClass == AElement.class) {
			switch (baseOperationID) {
			case AbstractionsPackage.AELEMENT___INDENT_LEVEL:
				return McorePackage.MPARAMETER___INDENT_LEVEL;
			case AbstractionsPackage.AELEMENT___INDENTATION_SPACES:
				return McorePackage.MPARAMETER___INDENTATION_SPACES;
			case AbstractionsPackage.AELEMENT___INDENTATION_SPACES__INTEGER:
				return McorePackage.MPARAMETER___INDENTATION_SPACES__INTEGER;
			case AbstractionsPackage.AELEMENT___STRING_OR_MISSING__STRING:
				return McorePackage.MPARAMETER___STRING_OR_MISSING__STRING;
			case AbstractionsPackage.AELEMENT___STRING_IS_EMPTY__STRING:
				return McorePackage.MPARAMETER___STRING_IS_EMPTY__STRING;
			case AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
				return McorePackage.MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;
			case AbstractionsPackage.AELEMENT___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
				return McorePackage.MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;
			case AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING:
				return McorePackage.MPARAMETER___APACKAGE_FROM_URI__STRING;
			case AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
				return McorePackage.MPARAMETER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;
			case AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
				return McorePackage.MPARAMETER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;
			case AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS:
				return McorePackage.MPARAMETER___ACORE_ASTRING_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS:
				return McorePackage.MPARAMETER___ACORE_AREAL_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS:
				return McorePackage.MPARAMETER___ACORE_AINTEGER_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS:
				return McorePackage.MPARAMETER___ACORE_AOBJECT_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		if (baseClass == AClassifier.class) {
			switch (baseOperationID) {
			case ClassifiersPackage.ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER:
				return McorePackage.MPARAMETER___AASSIGNABLE_TO__ACLASSIFIER;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MPARAMETER___DO_ACTION$_UPDATE__MPARAMETERACTION:
			return doAction$Update((MParameterAction) arguments.get(0));
		case McorePackage.MPARAMETER___AMBIGUOUS_PARAMETER_NAME:
			return ambiguousParameterName();
		case McorePackage.MPARAMETER___AMBIGUOUS_PARAMETER_SHORT_NAME:
			return ambiguousParameterShortName();
		case McorePackage.MPARAMETER___AASSIGNABLE_TO__ACLASSIFIER:
			return aAssignableTo((AClassifier) arguments.get(0));
		case McorePackage.MPARAMETER___INDENT_LEVEL:
			return indentLevel();
		case McorePackage.MPARAMETER___STRING_OR_MISSING__STRING:
			return stringOrMissing((String) arguments.get(0));
		case McorePackage.MPARAMETER___STRING_IS_EMPTY__STRING:
			return stringIsEmpty((String) arguments.get(0));
		case McorePackage.MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return listOfStringToStringWithSeparator(
					(EList<String>) arguments.get(0),
					(String) arguments.get(1));
		case McorePackage.MPARAMETER___LIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return listOfStringToStringWithSeparator(
					(EList<String>) arguments.get(0));
		case McorePackage.MPARAMETER___APACKAGE_FROM_URI__STRING:
			return aPackageFromUri((String) arguments.get(0));
		case McorePackage.MPARAMETER___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
			return aClassifierFromUriAndName((String) arguments.get(0),
					(String) arguments.get(1));
		case McorePackage.MPARAMETER___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
			return aFeatureFromUriAndNames((String) arguments.get(0),
					(String) arguments.get(1), (String) arguments.get(2));
		case McorePackage.MPARAMETER___ACORE_ASTRING_CLASS:
			return aCoreAStringClass();
		case McorePackage.MPARAMETER___ACORE_AREAL_CLASS:
			return aCoreARealClass();
		case McorePackage.MPARAMETER___ACORE_AINTEGER_CLASS:
			return aCoreAIntegerClass();
		case McorePackage.MPARAMETER___ACORE_AOBJECT_CLASS:
			return aCoreAObjectClass();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (tPackageUri: ");
		if (tPackageUriESet)
			result.append(tPackageUri);
		else
			result.append("<unset>");
		result.append(", tClassifierName: ");
		if (tClassifierNameESet)
			result.append(tClassifierName);
		else
			result.append("<unset>");
		result.append(", tFeatureName: ");
		if (tFeatureNameESet)
			result.append(tFeatureName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL eLabel
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MPARAMETER;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Parameter'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL correctName (not ambiguousParameterName()) and 
	if self.hasSimpleModelingType then not self.stringEmpty(self.name) else true endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectName() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CORRECT_NAME;

		if (correctNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL correctShortName not ambiguousParameterShortName()
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectShortName() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CORRECT_SHORT_NAME;

		if (correctShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedMandatory self.mandatory
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedMandatory() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedType self.type
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular self.singular
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL simpleTypeIsCorrect if  not type.oclIsUndefined() then simpleType=SimpleType::None else
	if voidTypeAllowed then true else simpleType<>SimpleType::None
	endif endif
	/* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... *\/
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getSimpleTypeIsCorrect() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;

		if (simpleTypeIsCorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				simpleTypeIsCorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeIsCorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedName (if not stringEmpty(name) then name else  
	if hasSimpleDataType then 'NAMEMISSING' else nameFromType() endif
	endif)
	.concat(if self.appendTypeNameToName.oclIsUndefined() then ''
	            else if self.appendTypeNameToName = false then ''
	            else if self.appendTypeNameToName = true and stringEmpty(name)=false then
	                               nameFromType().camelCaseUpper()
	            else '' endif endif endif
	            )
	.concat(if ambiguousParameterName() then '  (AMBIGUOUS)' else '' endif)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedName() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CALCULATED_NAME;

		if (calculatedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL calculatedShortName (if stringEmpty(name) then shortNameFromType()
	else
	if stringEmpty(shortName) then name
	else shortName
	endif
	endif)
	.concat(if self.appendTypeNameToName.oclIsUndefined() then ''
	            else if self.appendTypeNameToName = false then ''
	            else if self.appendTypeNameToName = true and stringEmpty(name)=false then
	                               shortNameFromType().camelCaseUpper()
	            else '' endif endif endif
	            )
	.concat(if self.ambiguousParameterShortName() then '( AMBIGUOUS)' else '' endif)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedShortName() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CALCULATED_SHORT_NAME;

		if (calculatedShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())
	then self.calculatedShortName.camelCaseLower()
	else self.specialEName.camelCaseLower()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL fullLabel 'TODO'
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MPARAMETER);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public void setMultiplicityCase(MultiplicityCase newMultiplicityCase) {
		// overridden from MTyped class. Added to both MExplicitlyTypedImple AND
		// MExplicitlyTypedAndNamedImpl, as the second does not directly inherit...
		switch (newMultiplicityCase) {
		case ONE_MANY:
			this.setMandatory(true);
			this.setSingular(false);
			break;
		case ONE_ONE:
			this.setMandatory(true);
			this.setSingular(true);
			break;
		case ZERO_MANY:
			this.setMandatory(false);
			this.setSingular(false);
			break;
		case ZERO_ONE:
			this.setMandatory(false);
			this.setSingular(true);
			break;
		default:
			break;
		}
	}

} //MParameterImpl
