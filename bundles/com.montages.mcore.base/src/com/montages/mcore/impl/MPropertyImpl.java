/**
 */
package com.montages.mcore.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MComponent;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.expressions.ExpressionBase;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MNamedConstant;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>MProperty</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getGeneralAnnotation <em>General Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getOperationSignature <em>Operation Signature</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getOppositeDefinition <em>Opposite Definition</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getOpposite <em>Opposite</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getContainment <em>Containment</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getHasStorage <em>Has Storage</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getChangeable <em>Changeable</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getPersisted <em>Persisted</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getPropertyBehavior <em>Property Behavior</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getIsOperation <em>Is Operation</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getIsAttribute <em>Is Attribute</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getIsReference <em>Is Reference</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getKind <em>Kind</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getTypeDefinition <em>Type Definition</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getUseCoreTypes <em>Use Core Types</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getELabel <em>ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getENameForELabel <em>EName For ELabel</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getOrderWithinGroup <em>Order Within Group</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getId <em>Id</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getInternalEStructuralFeature <em>Internal EStructural Feature</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getDirectSemantics <em>Direct Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getSemanticsSpecialization <em>Semantics Specialization</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getAllSignatures <em>All Signatures</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getDirectOperationSemantics <em>Direct Operation Semantics</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getContainingPropertiesContainer <em>Containing Properties Container</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getEKeyForPath <em>EKey For Path</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getNotUnsettable <em>Not Unsettable</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getAttributeForEId <em>Attribute For EId</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getEDefaultValueLiteral <em>EDefault Value Literal</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getDoAction <em>Do Action</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getBehaviorActions <em>Behavior Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getLayoutActions <em>Layout Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getArityActions <em>Arity Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getSimpleTypeActions <em>Simple Type Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getAnnotationActions <em>Annotation Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getMoveActions <em>Move Actions</em>}</li>
 *   <li>{@link com.montages.mcore.impl.MPropertyImpl#getDerivedJsonSchema <em>Derived Json Schema</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MPropertyImpl extends MExplicitlyTypedAndNamedImpl
		implements MProperty {
	/**
	 * The cached value of the '{@link #getGeneralAnnotation() <em>General Annotation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralAnnotation()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralAnnotation> generalAnnotation;

	/**
	 * The cached value of the '{@link #getOperationSignature() <em>Operation Signature</em>}' containment reference list.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getOperationSignature()
	 * @generated
	 * @ordered
	 */
	protected EList<MOperationSignature> operationSignature;

	/**
	 * The cached value of the '{@link #getOpposite() <em>Opposite</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOpposite()
	 * @generated
	 * @ordered
	 */
	protected MProperty opposite;

	/**
	 * This is true if the Opposite reference has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean oppositeESet;

	/**
	 * The default value of the '{@link #getContainment() <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainment()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CONTAINMENT_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getContainment() <em>Containment</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainment()
	 * @generated
	 * @ordered
	 */
	protected Boolean containment = CONTAINMENT_EDEFAULT;

	/**
	 * This is true if the Containment attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean containmentESet;

	/**
	 * The default value of the '{@link #getHasStorage() <em>Has Storage</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasStorage()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean HAS_STORAGE_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getHasStorage() <em>Has Storage</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getHasStorage()
	 * @generated
	 * @ordered
	 */
	protected Boolean hasStorage = HAS_STORAGE_EDEFAULT;

	/**
	 * This is true if the Has Storage attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean hasStorageESet;

	/**
	 * The default value of the '{@link #getChangeable() <em>Changeable</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getChangeable()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean CHANGEABLE_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getChangeable() <em>Changeable</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getChangeable()
	 * @generated
	 * @ordered
	 */
	protected Boolean changeable = CHANGEABLE_EDEFAULT;

	/**
	 * This is true if the Changeable attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean changeableESet;

	/**
	 * The default value of the '{@link #getPersisted() <em>Persisted</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPersisted()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean PERSISTED_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getPersisted() <em>Persisted</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPersisted()
	 * @generated
	 * @ordered
	 */
	protected Boolean persisted = PERSISTED_EDEFAULT;

	/**
	 * This is true if the Persisted attribute has been set.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean persistedESet;

	/**
	 * The default value of the '{@link #getPropertyBehavior() <em>Property Behavior</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getPropertyBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final PropertyBehavior PROPERTY_BEHAVIOR_EDEFAULT = PropertyBehavior.STORED;

	/**
	 * The default value of the '{@link #getIsOperation() <em>Is Operation</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIsOperation()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_OPERATION_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsAttribute() <em>Is Attribute</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIsAttribute()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_ATTRIBUTE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getIsReference() <em>Is Reference</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getIsReference()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean IS_REFERENCE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getKind() <em>Kind</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind()
	 * @generated
	 * @ordered
	 */
	protected static final PropertyKind KIND_EDEFAULT = PropertyKind.ATTRIBUTE;

	/**
	 * The default value of the '{@link #getUseCoreTypes() <em>Use Core Types</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUseCoreTypes()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean USE_CORE_TYPES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUseCoreTypes() <em>Use Core Types</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getUseCoreTypes()
	 * @generated
	 * @ordered
	 */
	protected Boolean useCoreTypes = USE_CORE_TYPES_EDEFAULT;

	/**
	 * This is true if the Use Core Types attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean useCoreTypesESet;

	/**
	 * The default value of the '{@link #getELabel() <em>ELabel</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getELabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ELABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getENameForELabel() <em>EName For ELabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENameForELabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ENAME_FOR_ELABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getOrderWithinGroup() <em>Order Within Group</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOrderWithinGroup()
	 * @generated
	 * @ordered
	 */
	protected static final Integer ORDER_WITHIN_GROUP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOrderWithinGroup() <em>Order Within Group</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOrderWithinGroup()
	 * @generated
	 * @ordered
	 */
	protected Integer orderWithinGroup = ORDER_WITHIN_GROUP_EDEFAULT;

	/**
	 * This is true if the Order Within Group attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean orderWithinGroupESet;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInternalEStructuralFeature() <em>Internal EStructural Feature</em>}' reference.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getInternalEStructuralFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature internalEStructuralFeature;

	/**
	 * This is true if the Internal EStructural Feature reference has been set.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean internalEStructuralFeatureESet;

	/**
	 * The cached value of the '{@link #getEKeyForPath() <em>EKey For Path</em>}' reference list.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEKeyForPath()
	 * @generated
	 * @ordered
	 */
	protected EList<MProperty> eKeyForPath;

	/**
	 * The default value of the '{@link #getNotUnsettable() <em>Not
	 * Unsettable</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getNotUnsettable()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean NOT_UNSETTABLE_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getNotUnsettable() <em>Not
	 * Unsettable</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getNotUnsettable()
	 * @generated
	 * @ordered
	 */
	protected Boolean notUnsettable = NOT_UNSETTABLE_EDEFAULT;

	/**
	 * This is true if the Not Unsettable attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean notUnsettableESet;

	/**
	 * The default value of the '{@link #getAttributeForEId() <em>Attribute For EId</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAttributeForEId()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ATTRIBUTE_FOR_EID_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getAttributeForEId() <em>Attribute For EId</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAttributeForEId()
	 * @generated
	 * @ordered
	 */
	protected Boolean attributeForEId = ATTRIBUTE_FOR_EID_EDEFAULT;

	/**
	 * This is true if the Attribute For EId attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean attributeForEIdESet;

	/**
	 * The default value of the '{@link #getEDefaultValueLiteral() <em>EDefault
	 * Value Literal</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getEDefaultValueLiteral()
	 * @generated
	 * @ordered
	 */
	protected static final String EDEFAULT_VALUE_LITERAL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEDefaultValueLiteral() <em>EDefault
	 * Value Literal</em>}' attribute. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getEDefaultValueLiteral()
	 * @generated
	 * @ordered
	 */
	protected String eDefaultValueLiteral = EDEFAULT_VALUE_LITERAL_EDEFAULT;

	/**
	 * This is true if the EDefault Value Literal attribute has been set. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 * @ordered
	 */
	protected boolean eDefaultValueLiteralESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MPropertyAction DO_ACTION_EDEFAULT = MPropertyAction.DO;

	/**
	 * The default value of the '{@link #getDerivedJsonSchema() <em>Derived Json Schema</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema()
	 * @generated
	 * @ordered
	 */
	protected static final String DERIVED_JSON_SCHEMA_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #oppositeDefinition$Update <em>Opposite Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #oppositeDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression oppositeDefinition$UpdatemcoreMPropertyBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #propertyBehavior$Update <em>Property Behavior$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #propertyBehavior$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression propertyBehavior$UpdatemcorePropertyBehaviorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #typeDefinition$Update <em>Type Definition$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #typeDefinition$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression typeDefinition$UpdatemcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdatemcoreMPropertyActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #selectableType <em>Selectable Type</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #selectableType
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression selectableTypemcoreMClassifierBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ambiguousPropertyName <em>Ambiguous Property Name</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #ambiguousPropertyName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ambiguousPropertyNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #ambiguousPropertyShortName <em>Ambiguous Property Short Name</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #ambiguousPropertyShortName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression ambiguousPropertyShortNameBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #doActionUpdate <em>Do Action Update</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #doActionUpdate
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doActionUpdatemcoreMPropertyActionBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetPropertyBehaviour <em>Invoke Set Property Behaviour</em>}' operation.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #invokeSetPropertyBehaviour
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetPropertyBehaviourmcorePropertyBehaviorBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #invokeSetDoAction <em>Invoke Set Do Action</em>}' operation.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #invokeSetDoAction
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression invokeSetDoActionmcoreMPropertyActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction
	 * <em>Do Action</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of
	 * '{@link #getDoAction <em>Do Action</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getBehaviorActions <em>Behavior Actions</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getBehaviorActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression behaviorActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLayoutActions <em>Layout Actions</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getLayoutActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression layoutActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getArityActions <em>Arity Actions</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getArityActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression arityActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeActions <em>Simple Type Actions</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleTypeActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression simpleTypeActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAnnotationActions <em>Annotation Actions</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getAnnotationActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression annotationActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getMoveActions <em>Move Actions</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getMoveActions
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression moveActionsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDerivedJsonSchema <em>Derived Json Schema</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDerivedJsonSchema
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression derivedJsonSchemaDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOppositeDefinition <em>Opposite Definition</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getOppositeDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression oppositeDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getOppositeDefinition <em>Opposite Definition</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @see #getOppositeDefinition
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression oppositeDefinitionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '
	 * {@link #getOpposite <em>Opposite</em>}' property. Is combined with the
	 * choice constraint definition. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getOpposite
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression oppositeChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsOperation <em>Is Operation</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIsOperation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isOperationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsAttribute <em>Is Attribute</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIsAttribute
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isAttributeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getIsReference <em>Is Reference</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getIsReference
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression isReferenceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getPropertyBehavior <em>Property Behavior</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getPropertyBehavior
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression propertyBehaviorDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKind <em>Kind</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression kindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getTypeDefinition <em>Type Definition</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getTypeDefinition
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression typeDefinitionDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getTypeDefinition <em>Type Definition</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypeDefinition
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression typeDefinitionChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getELabel <em>ELabel</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getELabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getENameForELabel <em>EName For ELabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getENameForELabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression eNameForELabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getId <em>Id</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getId
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression idDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getDirectSemantics <em>Direct Semantics</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDirectSemantics
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression directSemanticsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSemanticsSpecialization <em>Semantics Specialization</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSemanticsSpecialization
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression semanticsSpecializationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getAllSignatures <em>All Signatures</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getAllSignatures
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression allSignaturesDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDirectOperationSemantics <em>Direct Operation Semantics</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getDirectOperationSemantics
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression directOperationSemanticsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingClassifier <em>Containing Classifier</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainingClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingPropertiesContainer <em>Containing Properties Container</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getContainingPropertiesContainer
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingPropertiesContainerDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getEKeyForPath <em>EKey For Path</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getEKeyForPath
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression eKeyForPathChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getFullLabel
	 * <em>Full Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getFullLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression fullLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel
	 * <em>Kind Label</em>}' property. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCorrectName <em>Correct Name</em>}' property.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getCorrectName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getCorrectShortName <em>Correct Short Name</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCorrectShortName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression correctShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedMandatory <em>Calculated Mandatory</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCalculatedMandatory
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getCalculatedType <em>Calculated Type</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCalculatedType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSingular <em>Calculated Singular</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCalculatedSingular
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSimpleTypeIsCorrect <em>Simple Type Is Correct</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSimpleTypeIsCorrect
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression simpleTypeIsCorrectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '
	 * {@link #getVoidTypeAllowed <em>Void Type Allowed</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVoidTypeAllowed
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression voidTypeAllowedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedSimpleType <em>Calculated Simple Type</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCalculatedSimpleType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedSimpleTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getLocalStructuralName <em>Local Structural Name</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getLocalStructuralName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression localStructuralNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of
	 * '{@link #getCalculatedName <em>Calculated Name</em>}' property. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getCalculatedName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getCalculatedShortName <em>Calculated Short Name</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getCalculatedShortName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression calculatedShortNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getEName <em>EName</em>}' property.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getEName
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression eNameDeriveOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getType <em>Type</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getType
	 * @templateTag DFGFI07
	 * @generated
	 */
	private static OCLExpression typeChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected MPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return McorePackage.Literals.MPROPERTY;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralAnnotation> getGeneralAnnotation() {
		if (generalAnnotation == null) {
			generalAnnotation = new EObjectContainmentEList.Unsettable.Resolving<MGeneralAnnotation>(
					MGeneralAnnotation.class, this,
					McorePackage.MPROPERTY__GENERAL_ANNOTATION);
		}
		return generalAnnotation;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralAnnotation() {
		if (generalAnnotation != null)
			((InternalEList.Unsettable<?>) generalAnnotation).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralAnnotation() {
		return generalAnnotation != null
				&& ((InternalEList.Unsettable<?>) generalAnnotation).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAction getDoAction() {
		/**
		 * @OCL mcore::MPropertyAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyAction result = (MPropertyAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setDoAction(MPropertyAction newDoAction) {
		switch (newDoAction.getValue()) {
		case MPropertyAction.INTO_STORED_VALUE:
			this.setPropertyBehavior(PropertyBehavior.STORED);
			break;
		case MPropertyAction.INTO_CONTAINED_VALUE:
			this.setPropertyBehavior(PropertyBehavior.CONTAINED);
			break;
		case MPropertyAction.INTO_DERIVED_VALUE:
			this.setPropertyBehavior(PropertyBehavior.DERIVED);
			break;
		case MPropertyAction.INTO_OPERATION_VALUE:
			this.setPropertyBehavior(PropertyBehavior.IS_OPERATION);
			break;
		case MPropertyAction.INTO_CHANGEABLE_VALUE:
			this.setChangeable(true);
			break;
		case MPropertyAction.INTO_NOT_CHANGEABLE_VALUE:
			this.setChangeable(false);
			break;
		case MPropertyAction.INTO_UNARY_VALUE:
			this.setMultiplicityCase(MultiplicityCase.ZERO_ONE);
			break;
		case MPropertyAction.INTO_NARY_VALUE:
			this.setMultiplicityCase(MultiplicityCase.ZERO_MANY);
			break;
		case MPropertyAction.INTO_STRING_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.STRING);
			this.setContainment(false);
			break;
		case MPropertyAction.INTO_BOOLEAN_FLAG_VALUE:
			this.setSimpleType(SimpleType.BOOLEAN);
			this.setContainment(false);
			break;
		case MPropertyAction.INTO_INTEGER_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.INTEGER);
			this.setContainment(false);
			break;
		case MPropertyAction.INTO_REAL_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.DOUBLE);
			this.setContainment(false);
			break;
		case MPropertyAction.INTO_DATE_ATTRIBUTE_VALUE:
			this.setSimpleType(SimpleType.DATE);
			this.setContainment(false);
			break;
		case MPropertyAction.MOVE_TO_LAST_GROUP_VALUE:
			if (this.getContainingClassifier().getPropertyGroup().isEmpty()) {
				this.getContainingClassifier()
						.setDoAction(MClassifierAction.GROUP_OF_PROPERTIES);
			}
			int last = this.getContainingClassifier().getPropertyGroup().size()
					- 1;
			this.getContainingClassifier().getPropertyGroup().get(last)
					.getProperty().add(this);
			if (this.getDirectSemantics() != null) {
				MClassifierAnnotations semantics = this
						.getContainingClassifier().getPropertyGroup().get(last)
						.getAnnotations();
				if (semantics == null) {
					semantics = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					this.getContainingClassifier().getPropertyGroup().get(last)
							.setAnnotations(semantics);
				}
				semantics.getPropertyAnnotations()
						.add(this.getDirectSemantics());

				// Checks if the annotations are empty and deletes them if
				// necessary.
				if (!(!this.getContainingClassifier().getAnnotations()
						.getConstraint().isEmpty()
						|| !this.getContainingClassifier().getAnnotations()
								.getOperationAnnotations().isEmpty()
						|| !this.getContainingClassifier().getAnnotations()
								.getPropertyAnnotations().isEmpty())) {
					this.getContainingClassifier().unsetAnnotations();
				}

				for (MClassifier subClass : this.getContainingClassifier()
						.allDirectSubTypes()) {
					if (subClass.getAnnotations() != null) {
						for (int i = 0; i < subClass.getAnnotations()
								.getPropertyAnnotations().size(); i++) {
							if (subClass.getAnnotations()
									.getPropertyAnnotations().get(i)
									.getProperty().equals(this)) {
								subClass.setDoAction(
										MClassifierAction.GROUP_OF_PROPERTIES);
								subClass.getPropertyGroup()
										.get(subClass.getPropertyGroup().size()
												- 1)
										.setName(this.getContainingClassifier()
												.getPropertyGroup().get(last)
												.getName());
								last = subClass.getPropertyGroup().size() - 1;
								semantics = AnnotationsFactory.eINSTANCE
										.createMClassifierAnnotations();
								subClass.getPropertyGroup().get(last)
										.setAnnotations(semantics);
								semantics.getPropertyAnnotations()
										.add(subClass.getAnnotations()
												.getPropertyAnnotations()
												.get(i));

								// Checks if the annotations are empty and
								// deletes them if necessary.
								if (!(!subClass.getAnnotations().getConstraint()
										.isEmpty()
										|| !subClass.getAnnotations()
												.getOperationAnnotations()
												.isEmpty()
										|| !subClass.getAnnotations()
												.getPropertyAnnotations()
												.isEmpty())) {
									subClass.unsetAnnotations();
								}

								break;
							}
						}
					}
				}
			}
			break;

		case MPropertyAction.MOVE_TO_NEW_GROUP_VALUE:
			this.getContainingClassifier()
					.setDoAction(MClassifierAction.GROUP_OF_PROPERTIES);
			MPropertiesGroup newGroup = this.getContainingClassifier()
					.getPropertyGroup().get(this.getContainingClassifier()
							.getPropertyGroup().size() - 1);

			newGroup.getProperty().add(this);
			if (this.getDirectSemantics() != null) {
				MClassifierAnnotations semantics = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				newGroup.setAnnotations(semantics);
				semantics.getPropertyAnnotations()
						.add(this.getDirectSemantics());

				// Checks if the annotations are empty and deletes them if
				// necessary.
				if (!(!this.getContainingClassifier().getAnnotations()
						.getConstraint().isEmpty()
						|| !this.getContainingClassifier().getAnnotations()
								.getOperationAnnotations().isEmpty()
						|| !this.getContainingClassifier().getAnnotations()
								.getPropertyAnnotations().isEmpty())) {
					this.getContainingClassifier().unsetAnnotations();
				}

				for (MClassifier subClass : this.getContainingClassifier()
						.allDirectSubTypes()) {
					if (subClass.getAnnotations() != null) {
						for (int i = 0; i < subClass.getAnnotations()
								.getPropertyAnnotations().size(); i++) {
							if (subClass.getAnnotations()
									.getPropertyAnnotations().get(i)
									.getProperty().equals(this)) {
								subClass.setDoAction(
										MClassifierAction.GROUP_OF_PROPERTIES);
								subClass.getPropertyGroup()
										.get(subClass.getPropertyGroup().size()
												- 1)
										.setName(newGroup.getName());
								last = subClass.getPropertyGroup().size() - 1;
								semantics = AnnotationsFactory.eINSTANCE
										.createMClassifierAnnotations();
								subClass.getPropertyGroup().get(last)
										.setAnnotations(semantics);
								semantics.getPropertyAnnotations()
										.add(subClass.getAnnotations()
												.getPropertyAnnotations()
												.get(i));

								// Checks if the annotations are empty and
								// deletes them if necessary.
								if (!(!subClass.getAnnotations().getConstraint()
										.isEmpty()
										|| !subClass.getAnnotations()
												.getOperationAnnotations()
												.isEmpty()
										|| !subClass.getAnnotations()
												.getPropertyAnnotations()
												.isEmpty())) {
									subClass.unsetAnnotations();
								}
								break;
							}
						}
					}
				}
			}
			break;

		case MPropertyAction.ADD_UPDATE_ANNOTATION_VALUE: {
			if (getPropertyBehavior() != PropertyBehavior.DERIVED)
				this.setPropertyBehavior(PropertyBehavior.DERIVED);
			if (!getChangeable())
				this.setChangeable(true);

			ResourceSet resourceSet = this.eResource().getResourceSet();

			Iterator<Resource> itr = resourceSet.getResources().iterator();
			boolean referencesXSemanticsModel = false;
			String xSemanticsURI = "platform:/plugin/org.xocl.semantics/model.semantics.ecore";
			String xSemanticsMcoreURI = "mcore://www.xocl.org/semantics/semantics.mcore";

			while (itr.hasNext()) {
				Resource res = itr.next();
				if (res.getURI().toString().equals(xSemanticsURI)
						|| res.getURI().toString().equals(xSemanticsMcoreURI)) {
					referencesXSemanticsModel = true;
					break;
				}

			}

			if (!referencesXSemanticsModel) {
				Resource xSemanticsModel = resourceSet
						.getResource(URI.createURI(xSemanticsMcoreURI), true);

				if (xSemanticsModel != null) {
					MProperty xsemanticsProperty = McoreFactory.eINSTANCE
							.createMProperty();
					this.getContainingClassifier().getProperty()
							.add(xsemanticsProperty);
					xsemanticsProperty
							.setPropertyBehavior(PropertyBehavior.DERIVED);
					xsemanticsProperty
							.setMultiplicityCase(MultiplicityCase.ZERO_ONE);
					xsemanticsProperty.setName("xSemantics");
					xsemanticsProperty.setSimpleType(SimpleType.NONE);

					if (!xSemanticsModel.getContents().isEmpty())
						if (xSemanticsModel.getContents()
								.get(0) instanceof MComponent) {
							MComponent semanticsComponent = (MComponent) xSemanticsModel
									.getContents().get(0);

							if (!semanticsComponent.getOwnedPackage().isEmpty())
								if (semanticsComponent.getOwnedPackage().get(0)
										.getClassifier().size() >= 1)
									xsemanticsProperty
											.setType(semanticsComponent
													.getOwnedPackage().get(0)
													.getClassifier().get(1));

						}
				}
			}

			if (getDirectSemantics() != null) {
				MUpdate newUpdate = AnnotationsFactory.eINSTANCE
						.createMUpdate();
				getDirectSemantics().getUpdate().add(newUpdate);
				newUpdate.setValue(
						AnnotationsFactory.eINSTANCE.createMUpdateValue());
			}
			break;
		}

		case MPropertyAction.CLASS_WITH_STRING_PROPERTY_VALUE: {
			MClassifier newClass = McoreFactory.eINSTANCE.createMClassifier();
			newClass.setName(this.getName().concat(" Of ")
					.concat(this.getContainingClassifier().getName()));

			this.getContainingClassifier().getContainingPackage()
					.getClassifier().add(newClass);

			MProperty newContainment = McoreFactory.eINSTANCE.createMProperty();
			newContainment.setHasStorage(true);
			newContainment.setContainment(true);
			newContainment.setType(newClass);
			newContainment.setSimpleType(simpleType.NONE);
			newContainment.setMultiplicityCase(this.getMultiplicityCase());

			if (this.getDirectSemantics() != null) {
				MClassifierAnnotations newAnno = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				newClass.setAnnotations(newAnno);
				newAnno.getPropertyAnnotations().add(this.getDirectSemantics());
			}
			this.getContainingClassifier().getProperty().add(newContainment);

			for (MObject mObj : this.getContainingClassifier().getInstance()) {
				ArrayList<MPropertyInstance> allPropIn = new ArrayList<MPropertyInstance>(
						mObj.getPropertyInstance());
				for (int i = 0; i < allPropIn.size(); i++) {
					if (mObj.getPropertyInstance().get(i).getProperty()
							.equals(this)) {
						MPropertyInstance newPropIns = ObjectsFactory.eINSTANCE
								.createMPropertyInstance();
						MObject newContainedObj = ObjectsFactory.eINSTANCE
								.createMObject();
						newPropIns.setProperty(newContainment);
						newContainedObj.setType(newClass);

						mObj.getPropertyInstance().add(newPropIns);
						newPropIns.getInternalContainedObject()
								.add(newContainedObj);
						newContainedObj.getPropertyInstance()
								.add(mObj.getPropertyInstance().get(i));
						break;
					}
				}
			}

			newClass.getProperty().add(this);

			break;
		}

		case MPropertyAction.ABSTRACT_VALUE: {
			MClassifier superType = this.getContainingClassifier()
					.getSuperType().get(0);
			MClassifierAnnotations classAnno;
			if (superType.getAnnotations() == null) {
				classAnno = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				superType.setAnnotations(classAnno);
			} else {
				classAnno = superType.getAnnotations();
			}

			// Replace the already existing definition if necessary
			if (classAnno.getPropertyAnnotations().isEmpty()) {
				classAnno.getPropertyAnnotations()
						.add(this.getDirectSemantics());
			} else {
				for (int i = 0; i < classAnno.getPropertyAnnotations()
						.size(); i++) {
					if (classAnno.getPropertyAnnotations().get(i).getProperty()
							.equals(this)) {
						classAnno.getPropertyAnnotations().remove(i);
						classAnno.getPropertyAnnotations().add(i,
								this.getDirectSemantics());
						break;
					} else if (i == classAnno.getPropertyAnnotations().size()
							- 1) {
						classAnno.getPropertyAnnotations()
								.add(this.getDirectSemantics());
						break;
					}
				}
			}

			if (this.getContainingClassifier().getAnnotations()
					.getPropertyAnnotations().isEmpty()
					|| this.getContainingClassifier().getAnnotations()
							.getPropertyAnnotations() == null) {
				this.getContainingClassifier().unsetAnnotations();
			}
			break;
		}

		case MPropertyAction.SPECIALIZE_VALUE: {
			for (MClassifier subClass : this.getContainingClassifier()
					.allDirectSubTypes()) {
				// Create a copy so it can be pushed to all sub types and every
				// sub type can be specified different
				MPropertyAnnotations propCopy = EcoreUtil
						.copy(this.getDirectSemantics());

				MClassifierAnnotations classAnno;
				if (subClass.getAnnotations() == null) {
					classAnno = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					subClass.setAnnotations(classAnno);
					classAnno.getPropertyAnnotations().add(propCopy);
				} else {
					classAnno = subClass.getAnnotations();
					if (classAnno.getPropertyAnnotations().isEmpty()) {
						classAnno.getPropertyAnnotations().add(propCopy);
					} else {
						for (int i = 0; i < classAnno.getPropertyAnnotations()
								.size(); i++) {
							if (classAnno.getPropertyAnnotations().get(i)
									.getProperty().equals(this)) {
								break;
							} else {
								if (i == (classAnno.getPropertyAnnotations()
										.size() - 1)) {
									classAnno.getPropertyAnnotations()
											.add(propCopy);
								}
							}
						}
					}
				}
			}
			break;
		}

		default:
			MPropertiesContainer pc = this.getContainingPropertiesContainer();
			MClassifierAnnotations localClassifierAnnotations = pc
					.getAnnotations();
			// move direct property annotations to local, if they exist.
			MPropertyAnnotations directPropertyAnnotations = this
					.getDirectSemantics();
			if (directPropertyAnnotations != null && directPropertyAnnotations
					.getContainingPropertiesContainer() != pc) {/*
																 * Move if to
																 * local
																 */
				if (localClassifierAnnotations == null) {
					localClassifierAnnotations = AnnotationsFactory.eINSTANCE
							.createMClassifierAnnotations();
					pc.setAnnotations(localClassifierAnnotations);
				}
				localClassifierAnnotations.getPropertyAnnotations()
						.add(directPropertyAnnotations);
			}
			if (localClassifierAnnotations == null) {
				localClassifierAnnotations = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				pc.setAnnotations(localClassifierAnnotations);
			}
			if (directPropertyAnnotations == null) {
				directPropertyAnnotations = AnnotationsFactory.eINSTANCE
						.createMPropertyAnnotations();
				localClassifierAnnotations.getPropertyAnnotations()
						.add(directPropertyAnnotations);
				directPropertyAnnotations.setProperty(this);
			}
			ExpressionBase initValue;
			if (this.getSingular()) {
				initValue = ExpressionBase.NULL_VALUE;
			} else {
				initValue = ExpressionBase.EMPTY_COLLECTION;
			}
			switch (newDoAction.getValue()) {
			case MPropertyAction.CONSTRUCT_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstruction() == null) {
					directPropertyAnnotations
							.setChoiceConstruction(AnnotationsFactory.eINSTANCE
									.createMChoiceConstruction());
				}
				annotationInitialization(
						directPropertyAnnotations.getChoiceConstruction(),
						ExpressionBase.EMPTY_COLLECTION);
				break;
			case MPropertyAction.CONSTRAIN_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstraint() == null) {
					directPropertyAnnotations
							.setChoiceConstraint(AnnotationsFactory.eINSTANCE
									.createMChoiceConstraint());
				}
				annotationInitialization(
						directPropertyAnnotations.getChoiceConstraint(),
						(ExpressionBase.TRUE_VALUE));
				break;
			case MPropertyAction.INIT_VALUE_VALUE:
				if (directPropertyAnnotations.getInitValue() == null) {
					directPropertyAnnotations
							.setInitValue(AnnotationsFactory.eINSTANCE
									.createMInitializationValue());
				}
				annotationInitialization(
						directPropertyAnnotations.getInitValue(), initValue);
				break;
			case MPropertyAction.INIT_ORDER_VALUE:
				if (directPropertyAnnotations.getInitOrder() == null) {
					directPropertyAnnotations
							.setInitOrder(AnnotationsFactory.eINSTANCE
									.createMInitializationOrder());
				}
				annotationInitialization(
						directPropertyAnnotations.getInitOrder(),
						(ExpressionBase.ONE_VALUE));
				break;
			case MPropertyAction.RESULT_VALUE:
				if (directPropertyAnnotations.getResult() == null) {
					directPropertyAnnotations.setResult(
							AnnotationsFactory.eINSTANCE.createMResult());
				}
				annotationInitialization(directPropertyAnnotations.getResult(),
						initValue);
				break;
			case MPropertyAction.HIGHLIGHT_VALUE:
				if (directPropertyAnnotations.getHighlight() == null) {
					directPropertyAnnotations
							.setHighlight(AnnotationsFactory.eINSTANCE
									.createMHighlightAnnotation());
				}
				annotationInitialization(
						directPropertyAnnotations.getHighlight(),
						(ExpressionBase.TRUE_VALUE));
				break;
			case MPropertyAction.HIDE_IN_PROPERTIES_VIEW_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());

					if (this.getContainingPropertiesContainer() instanceof MPropertiesGroup)
						directPropertyAnnotations.getLayout()
								.setHideInTable(true);
					else
						directPropertyAnnotations.getLayout()
								.setHideInTable(false);
				}
				directPropertyAnnotations.getLayout()
						.setHideInPropertyView(true);
				break;
			case MPropertyAction.HIDE_IN_TABLE_EDITOR_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
					directPropertyAnnotations.getLayout()
							.setHideInPropertyView(false);
				}
				directPropertyAnnotations.getLayout().setHideInTable(true);
				break;
			case MPropertyAction.SHOW_IN_TABLE_EDITOR_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
					directPropertyAnnotations.getLayout()
							.setHideInPropertyView(false);
				}
				directPropertyAnnotations.getLayout().setHideInTable(false);
				break;
			case MPropertyAction.SHOW_IN_PROPERTIES_VIEW_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
					if (this.getContainingPropertiesContainer() instanceof MPropertiesGroup)
						directPropertyAnnotations.getLayout()
								.setHideInTable(true);
					else
						directPropertyAnnotations.getLayout()
								.setHideInTable(false);
				}
				directPropertyAnnotations.getLayout()
						.setHideInPropertyView(false);
				break;
			default:
				break;
			}

			break;
		}
	}

	/**
	 * ATTENTION: Duplicated in MPropertyImpl and MPropertyAnnotationsImpl
	 * 
	 * @param cc
	 * @param b
	 */
	private void annotationInitialization(MExprAnnotation cc,
			ExpressionBase b) {
		if (cc.getValue() == null || cc.getValue().trim().equals("")) {
			cc.setUseExplicitOcl(false);
		}
		if (!cc.getUseExplicitOcl()) {
			if (cc.getNamedExpression().isEmpty()
					&& cc.getNamedConstant().isEmpty()
					&& cc.getNamedTuple().isEmpty()) {
				if (cc.getBase() == null
						|| cc.getBase() == ExpressionBase.UNDEFINED) {
					cc.setBase(b);
				} else {
					if (cc.getBase() == ExpressionBase.SELF_OBJECT
							&& cc.getElement1() == null
							&& cc.getCastType() == null
							&& cc.getProcessor() == null) {
						cc.setBase(b);
					}
				}

			}
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MPropertyAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL     let arityActionsInput: OrderedSet(MPropertyAction) = behaviorActions->prepend(MPropertyAction::Do) in
	let simpleTypeActionsInput: OrderedSet(MPropertyAction) = 
	arityActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = arityActionsInput | ac->append(it)) in
	let annotationActionsInput: OrderedSet(MPropertyAction) = 
	simpleTypeActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = simpleTypeActionsInput | ac->append(it)) in
	let layoutActionsInput: OrderedSet(MPropertyAction) = 
	annotationActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = annotationActionsInput | ac->append(it))
	->append(MPropertyAction::Highlight) in
	
	let moveActionsInput: OrderedSet(MPropertyAction) = 
	layoutActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = layoutActionsInput | ac->append(it)) in 
	moveActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = moveActionsInput | ac->append(it))->append(MPropertyAction::AddUpdateAnnotation)
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MPropertyAction> evalDoActionChoiceConstruction(
			List<MPropertyAction> choice) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MPROPERTY__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MPropertyAction> result = new ArrayList<MPropertyAction>(
					(Collection<MPropertyAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getBehaviorActions() {
		/**
		 * @OCL let a:OrderedSet(MPropertyAction) =
		if (propertyBehavior = PropertyBehavior::Stored) 
		then  if self.kind= PropertyKind::Reference
		    then OrderedSet{MPropertyAction::IntoContained, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}
		    else OrderedSet{MPropertyAction::IntoDerived, MPropertyAction::IntoOperation} endif
		else if (propertyBehavior = PropertyBehavior::Contained)
		then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}
		else if propertyBehavior = PropertyBehavior::Derived
		then if self.kind= PropertyKind::Reference
		    then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoOperation}
		    else OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoOperation} endif
		else if propertyBehavior = PropertyBehavior::IsOperation
		then if self.hasSimpleModelingType 
		     then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}
		else if self.type.oclIsUndefined()
		     then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} 
		else if self.type.kind = ClassifierKind::ClassType
		     then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}
		     else  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} endif endif endif
		else OrderedSet{} endif endif endif endif
		in
		let b:OrderedSet(MPropertyAction) =
		let sts:OrderedSet(MClassifier) = self.containingClassifier.allSubTypes() in
		if sts->isEmpty()
		then a
		else if propertyBehavior = PropertyBehavior::Derived
		then a->prepend(MPropertyAction::SpecializeResult)
		else if propertyBehavior = PropertyBehavior::Stored or propertyBehavior = PropertyBehavior::Contained
		then a->prepend(MPropertyAction::SpecializeInitOrder)->prepend(MPropertyAction::SpecializeInitValue)
		else a
		endif endif endif in
		b
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__BEHAVIOR_ACTIONS;

		if (behaviorActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				behaviorActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(behaviorActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getLayoutActions() {
		/**
		 * @OCL if (kind = mcore::PropertyKind::Attribute) or 
		(kind = mcore::PropertyKind::Reference)
		then
		let a:mcore::annotations::MPropertyAnnotations= directSemantics in 
		if a.oclIsUndefined()
		then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)
		then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}
		else /* In property group *\/
		        OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif
		else if a.layout.oclIsUndefined()
		then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)
		then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}
		else /* In property group *\/
		        OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif
		else OrderedSet{
		                       if a.layout.hideInPropertyView
		                           then MPropertyAction::ShowInPropertiesView
		                           else MPropertyAction::HideInPropertiesView endif,
		                       if a.layout.hideInTable
		                           then MPropertyAction::ShowInTableEditor
		                           else MPropertyAction::HideInTableEditor endif
		                       } endif endif
		else OrderedSet{} endif      
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__LAYOUT_ACTIONS;

		if (layoutActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				layoutActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(layoutActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getArityActions() {
		/**
		 * @OCL if self.singular = true 
		then OrderedSet{MPropertyAction::IntoNary}
		else OrderedSet{MPropertyAction::IntoUnary} endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ARITY_ACTIONS;

		if (arityActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				arityActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(arityActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getSimpleTypeActions() {
		/**
		 * @OCL if self.calculatedSimpleType = SimpleType::String 
		then OrderedSet{MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}
		else if self.calculatedSimpleType = SimpleType::Boolean
		then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}
		else if self.calculatedSimpleType = SimpleType::Integer
		then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}
		else if self.calculatedSimpleType = SimpleType::Double
		then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoDateAttribute}
		else if self.calculatedSimpleType = SimpleType::Date
		then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute}  
		else OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute} endif endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__SIMPLE_TYPE_ACTIONS;

		if (simpleTypeActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				simpleTypeActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getAnnotationActions() {
		/**
		 * @OCL let a:OrderedSet(MPropertyAction) =
		if propertyBehavior = PropertyBehavior::Derived 
		or propertyBehavior = PropertyBehavior::IsOperation
		then OrderedSet{MPropertyAction::Result}
		else OrderedSet{MPropertyAction::InitValue, MPropertyAction::InitOrder} endif 
		in
		if changeable
		then a->append(MPropertyAction::ConstructChoice)
		->append(MPropertyAction::ConstrainChoice)
		else a endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ANNOTATION_ACTIONS;

		if (annotationActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				annotationActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(annotationActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAction> getMoveActions() {
		/**
		 * @OCL let e0: OrderedSet(MPropertyAction) = if self.containingClassifier.indentLevel = (self.indentLevel-1)
		then if self.hasStorage = true and self.simpleType = SimpleType::String
		then if self.containingClassifier.propertyGroup->isEmpty()
					then OrderedSet{MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}
					else OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}
					endif   			
		else if self.containingClassifier.propertyGroup->isEmpty()
					then OrderedSet{MPropertyAction::MoveToNewGroup}
					else OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup}
					endif  endif
		else if self.hasStorage = true and self.simpleType = SimpleType::String
		then OrderedSet{MPropertyAction::ClassWithStringProperty}
		else OrderedSet{} endif
		endif
		in if self.propertyBehavior = PropertyBehavior::Derived and not self.directSemantics.oclIsUndefined()
		then if self.containingClassifier.allDirectSubTypes()->isEmpty()
			then if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1
						then e0->prepend(MPropertyAction::Abstract)
						else e0
						endif
			else 
			let subTypes: OrderedSet(mcore::MClassifier)  =
				if containingClassifier.oclIsUndefined()
					then OrderedSet{}
					else containingClassifier.allDirectSubTypes()
				endif in let subTypesAnno: OrderedSet(mcore::annotations::MClassifierAnnotations)  = subTypes.annotations->asOrderedSet() in
				let alreadySpecialized: OrderedSet(mcore::annotations::MPropertyAnnotations)  = subTypesAnno.propertyAnnotations->asOrderedSet()->select(it: mcore::annotations::MPropertyAnnotations | let e1: Boolean = it.property = self in 
				if e1.oclIsInvalid()
					then null
					else e1
				endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
				let leftToSpecialize: Boolean = 
				if (let e1: Boolean = let chain01: OrderedSet(mcore::MClassifier)  = subTypes in
						if chain01->size().oclIsUndefined() 
							then null 
							else chain01->size()
						endif = let chain02: OrderedSet(mcore::annotations::MPropertyAnnotations)  = alreadySpecialized in
						if chain02->size().oclIsUndefined() 
							then null 
							else chain02->size()
						endif in 
						if e1.oclIsInvalid()
							then null
							else e1
						endif) 
				=true 
					then false
					else true
				endif in
				if leftToSpecialize
					then if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1
							then e0->prepend(MPropertyAction::Abstract)->prepend(MPropertyAction::Specialize)
							else e0->prepend(MPropertyAction::Specialize)
							endif
					else if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1										
								then e0->prepend(MPropertyAction::Abstract)
								else e0
								endif
					endif
		endif
		else e0
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__MOVE_ACTIONS;

		if (moveActionsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				moveActionsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(moveActionsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAction> result = (EList<MPropertyAction>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDerivedJsonSchema() {
		/**
		 * @OCL let space : String = ' ' in
		let branch : String = '{' in
		let lineBreak : String = '\n' in
		let closedBranch : String = '}' in
		let comma : String = ',' in
		let definitions : String = '#/definitions/' in
		if (self.hasStorage = false) then ''
		else
		'"'.concat(self.eName).concat('":').concat(space).concat(branch)
		.concat(lineBreak)
		.concat(
		if(self.calculatedSingular = false) then
		'"type":'.concat(space).concat('"array"').concat(comma).concat(lineBreak).concat(
		if(self.mandatory) then lineBreak.concat('"minItems":').concat(space).concat('"1"').concat(comma).concat(lineBreak) else '' endif)
		.concat('"items":').concat(branch).concat(lineBreak)  else '' endif)
		.concat('"description": ').concat('"').concat(if self.description.oclIsUndefined() then 'no description' else self.description endif).concat('"').concat(comma)
		.concat(lineBreak)
		.concat(
		if(self.hasSimpleDataType) then 
		'"type":'.concat(space).concat('"string"')
		else
		'"$ref":'.concat(space).concat('"').concat(definitions).concat(self.type.eName).concat('"')
		endif
		)
		endif
		.concat(
		if(self.calculatedSingular = false) then
		lineBreak.concat(closedBranch)
		else ''
		endif
		.concat(lineBreak).concat(closedBranch)
		)
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__DERIVED_JSON_SCHEMA;

		if (derivedJsonSchemaDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				derivedJsonSchemaDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(derivedJsonSchemaDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate oppositeDefinition$Update(MProperty trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MProperty triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				McorePackage.eINSTANCE.getMProperty_OppositeDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate propertyBehavior$Update(PropertyBehavior trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.PropertyBehavior triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_PropertyBehavior(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate typeDefinition$Update(MClassifier trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MClassifier triggerValue = trg;

		XUpdate currentTrigger = transition.addReferenceUpdate(this,
				McorePackage.eINSTANCE.getMProperty_TypeDefinition(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public XUpdate doAction$Update(MPropertyAction trg) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.MPropertyAction triggerValue = trg;

		XUpdate currentTrigger = transition.addAttributeUpdate(this,
				McorePackage.eINSTANCE.getMProperty_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		return null;

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationSignature> getOperationSignature() {
		if (operationSignature == null) {
			operationSignature = new EObjectContainmentEList.Unsettable.Resolving<MOperationSignature>(
					MOperationSignature.class, this,
					McorePackage.MPROPERTY__OPERATION_SIGNATURE);
		}
		return operationSignature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOperationSignature() {
		if (operationSignature != null)
			((InternalEList.Unsettable<?>) operationSignature).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperationSignature() {
		return operationSignature != null
				&& ((InternalEList.Unsettable<?>) operationSignature).isSet();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getOppositeDefinition() {
		MProperty oppositeDefinition = basicGetOppositeDefinition();
		return oppositeDefinition != null && oppositeDefinition.eIsProxy()
				? (MProperty) eResolveProxy(
						(InternalEObject) oppositeDefinition)
				: oppositeDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetOppositeDefinition() {
		/**
		 * @OCL opposite
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__OPPOSITE_DEFINITION;

		if (oppositeDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				oppositeDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(oppositeDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MProperty result = (MProperty) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public void setOppositeDefinition(MProperty newOppositeDefinition) {
		MProperty oldOpposite = this.getOpposite();
		if (oldOpposite != newOppositeDefinition) {
			if (oldOpposite != null) {
				oldOpposite.unsetOpposite();
			}
			if (newOppositeDefinition != null) {
				MProperty oldOppositeOfNewOpposite = newOppositeDefinition
						.getOpposite();
				if (oldOppositeOfNewOpposite != null) {
					oldOppositeOfNewOpposite.unsetOpposite();
				}
			}
			setOpposite(newOppositeDefinition);
			if (this.opposite != null && this.isSetHasStorage()
					&& !this.containment)
				this.setSingular(true);
			if (newOppositeDefinition == null)
				return;
			else
				newOppositeDefinition.setOpposite(this);
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Opposite Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL --Manually changed in MPropertyItemProvider
	if self.kind <> PropertyKind::Reference 
	then OrderedSet{}
	else if self.type.oclIsUndefined() 
	then OrderedSet{}
	else self.type.allProperties()
	 ->select(p:MProperty|p.type=self.containingClassifier and p<>self and 
	                  (if self.containment then not p.containment else true endif) 
	                  )
	endif 
	endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalOppositeDefinitionChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (oppositeDefinitionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MPROPERTY__OPPOSITE_DEFINITION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				oppositeDefinitionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"OppositeDefinitionChoiceConstruction");
			}
		}
		Query query = OCL_ENV
				.createQuery(oppositeDefinitionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"OppositeDefinitionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getOpposite() {
		if (opposite != null && opposite.eIsProxy()) {
			InternalEObject oldOpposite = (InternalEObject) opposite;
			opposite = (MProperty) eResolveProxy(oldOpposite);
			if (opposite != oldOpposite) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPROPERTY__OPPOSITE, oldOpposite,
							opposite));
			}
		}
		return opposite;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetOpposite() {
		return opposite;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOpposite(MProperty newOpposite) {
		MProperty oldOpposite = opposite;
		opposite = newOpposite;
		boolean oldOppositeESet = oppositeESet;
		oppositeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__OPPOSITE, oldOpposite, opposite,
					!oldOppositeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOpposite() {
		MProperty oldOpposite = opposite;
		boolean oldOppositeESet = oppositeESet;
		opposite = null;
		oppositeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__OPPOSITE, oldOpposite, null,
					oldOppositeESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOpposite() {
		return oppositeESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Opposite</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL if self.kind <> PropertyKind::Reference 
	then OrderedSet{}
	else if self.type.oclIsUndefined() 
	then OrderedSet{}
	else self.type.allProperties()
	 ->select(p:MProperty|p.type=self.containingClassifier and p<>self and 
	                  (if self.containment then not p.containment else true endif) 
	                  )
	endif 
	endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalOppositeChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (oppositeChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MPROPERTY__OPPOSITE;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				oppositeChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"OppositeChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(oppositeChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"OppositeChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsOperation() {
		/**
		 * @OCL self.kind=PropertyKind::Operation
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__IS_OPERATION;

		if (isOperationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isOperationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isOperationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsAttribute() {
		/**
		 * @OCL self.kind=PropertyKind::Attribute
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__IS_ATTRIBUTE;

		if (isAttributeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isAttributeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isAttributeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getIsReference() {
		/**
		 * @OCL self.kind=PropertyKind::Reference
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__IS_REFERENCE;

		if (isReferenceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				isReferenceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(isReferenceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyBehavior getPropertyBehavior() {
		/**
		 * @OCL if self.kind=PropertyKind::Operation
		then PropertyBehavior::IsOperation
		else if self.hasStorage 
		then if self.containment 
		then PropertyBehavior::Contained
		else  PropertyBehavior::Stored endif
		else PropertyBehavior::Derived endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__PROPERTY_BEHAVIOR;

		if (propertyBehaviorDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				propertyBehaviorDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(propertyBehaviorDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			PropertyBehavior result = (PropertyBehavior) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setPropertyBehavior(PropertyBehavior newPropertyBehavior) {

		MPropertiesContainer pc = this.getContainingPropertiesContainer();
		MClassifierAnnotations localClassifierAnnotations = pc.getAnnotations();
		// move direct property annotations to local, if they exist.
		MPropertyAnnotations directPropertyAnnotations = this
				.getDirectSemantics();

		if (directPropertyAnnotations != null && directPropertyAnnotations
				.getContainingPropertiesContainer() != pc) {/*
															 * Move if to local
															 */
			if (localClassifierAnnotations == null) {
				localClassifierAnnotations = AnnotationsFactory.eINSTANCE
						.createMClassifierAnnotations();
				pc.setAnnotations(localClassifierAnnotations);
			}
			localClassifierAnnotations.getPropertyAnnotations()
					.add(directPropertyAnnotations);
		}

		// Exclude all annotation creation, if there is no
		// directPropertyAnnotations and behavior != DERIVE
		Boolean annotationWorkSuppression = directPropertyAnnotations == null
				&& (newPropertyBehavior == PropertyBehavior.STORED
						|| newPropertyBehavior == PropertyBehavior.CONTAINED);
		if (!annotationWorkSuppression && localClassifierAnnotations == null) {
			localClassifierAnnotations = AnnotationsFactory.eINSTANCE
					.createMClassifierAnnotations();
			pc.setAnnotations(localClassifierAnnotations);
		}
		if (!annotationWorkSuppression && directPropertyAnnotations == null) {

			if (newPropertyBehavior != PropertyBehavior.IS_OPERATION)

			{
				directPropertyAnnotations = AnnotationsFactory.eINSTANCE
						.createMPropertyAnnotations();
				localClassifierAnnotations.getPropertyAnnotations()
						.add(directPropertyAnnotations);
				directPropertyAnnotations.setProperty(this);
			}
		}

		String oclValue = null;
		EList<MNamedConstant> oclConstants = null;
		EList<MNamedExpression> oclExpressions = null;
		MObject domainObject = null;
		MOperationSignature domainOperation = null;
		// if (this.getSingular()) {
		// //TO REFINE FOR number, boolean, e.t.d
		// oclValue = "null";
		// } else {
		// oclValue = "OrderedSet{}";
		// }
		if (newPropertyBehavior == PropertyBehavior.STORED
				|| newPropertyBehavior == PropertyBehavior.CONTAINED) {

			if (getPropertyBehavior() == PropertyBehavior.IS_OPERATION
					&& newPropertyBehavior == PropertyBehavior.STORED) {
				setDoAction(MPropertyAction.INTO_DERIVED);
				setDoAction(MPropertyAction.INTO_STORED);
				return;
			}

			this.setHasStorage(true);
			this.setPersisted(true);
			this.setChangeable(true);

			MClassifier mClass = null;
			if ((this.getContainingClassifier() != null)
					&& (this.getContainingClassifier().getContainingPackage()
							.getContainingComponent() != null)) {
				EList<MPackage> packageList = this.getContainingClassifier()
						.getContainingPackage().getContainingComponent()
						.getOwnedPackage();
				for (MPackage pack : packageList)
					if (pack.getResourceRootClass() != null)
						mClass = pack.getResourceRootClass();
			}

			if (getPropertyBehavior() == PropertyBehavior.CONTAINED
					&& newPropertyBehavior == PropertyBehavior.STORED) {
				boolean gotSuperClass = false;
				MProperty correspondingProp = null;

				if (getContainingClassifier() == null
						|| getContainingClassifier()
								.getContainingPackage() == null
						|| getContainingClassifier().getContainingPackage()
								.getContainingComponent() == null)
					return;

				if (!this.getContainingClassifier().getContainingPackage()
						.getContainingComponent()
						.isSetDisableDefaultContainment()
						|| (this.getContainingClassifier()
								.getContainingPackage().getContainingComponent()
								.getDisableDefaultContainment() == null
								|| this.getContainingClassifier()
										.getContainingPackage()
										.getContainingComponent()
										.getDisableDefaultContainment() != true)) {

					if (mClass != null) {
						for (MProperty containmentProp : mClass
								.allContainmentReferences()) {

							if (containmentProp.getType() != null
									&& (containmentProp.getType().allSubTypes()
											.contains(this.getType())
											|| this.getType() == containmentProp
													.getType())) {

								gotSuperClass = true;
								correspondingProp = containmentProp;
							}
						}
					}
					if (mClass != null && !gotSuperClass) {
						mClass.getProperty()
								.add(McoreFactory.eINSTANCE.createMProperty());

						correspondingProp = mClass.getProperty()
								.get(mClass.getProperty().size() - 1);

						mClass.getProperty()
								.get(mClass.getProperty().size() - 1)
								.setSimpleType(SimpleType.NONE);
						mClass.getProperty()
								.get(mClass.getProperty().size() - 1)
								.setType(type);
						mClass.getProperty()
								.get(mClass.getProperty().size() - 1)
								.setPropertyBehavior(
										PropertyBehavior.CONTAINED);
						mClass.getProperty()
								.get(mClass.getProperty().size() - 1)
								.setMultiplicityCase(
										MultiplicityCase.ZERO_MANY);

					}
					EList<MResourceFolder> resFolder = (EList<MResourceFolder>) this
							.getContainingClassifier().getContainingPackage()
							.getContainingComponent().getResourceFolder();

					this.setContainment(false);

					if (newPropertyBehavior == PropertyBehavior.STORED)
						for (MResourceFolder folder : resFolder) {
							if (folder != null) {

								folder.updateResource(this, correspondingProp);
							}

						}

				} else
					this.setContainment(false);

			}

			if (newPropertyBehavior == PropertyBehavior.CONTAINED) {
				this.setContainment(true);

				if (getContainingClassifier() == null
						|| getContainingClassifier()
								.getContainingPackage() == null
						|| getContainingClassifier().getContainingPackage()
								.getContainingComponent() == null)
					return;
				if (!this.getContainingClassifier().getContainingPackage()
						.getContainingComponent()
						.isSetDisableDefaultContainment()
						|| (this.getContainingClassifier()
								.getContainingPackage().getContainingComponent()
								.getDisableDefaultContainment() == null
								|| this.getContainingClassifier()
										.getContainingPackage()
										.getContainingComponent()
										.getDisableDefaultContainment() != true)) {

					// Setting feature contained removes the containment feature
					// in
					// RootClass
					MProperty corresponding = null;
					MClassifier classEContainer;
					if (eContainer instanceof MClassifier) {
						classEContainer = ((MClassifier) eContainer);
					} else
						classEContainer = ((MPropertiesGroup) eContainer)
								.getContainingClassifier();

					if (this.getContainingClassifier() != mClass
							&& mClass.getProperty() != null
							&& type != classEContainer) {

						Iterator<MProperty> itr = mClass.getProperty()
								.iterator();
						while (itr.hasNext()) {

							MProperty property = (MProperty) itr.next();

							if ((property != null) && property.getSimpleType()
									.equals(com.montages.mcore.SimpleType.NONE)) {

								if ((property.getType() == this.type)
										&& property.getContainment()) {
									corresponding = property; // Does not
																// delete??
									itr.remove();
									break;

								}

								else if (((!property.getType().allSubTypes()
										.isEmpty())
										&& property.getType().allSubTypes()
												.contains(this.type))
										&& property.getContainment()) {
									System.out.println("caugh it");
									corresponding = property;
								}

								else {
									// System.out.println(property.getType().toString());
									// System.out.println(this.type.toString());

								}

							}

						}
						EList<MResourceFolder> resFolder = (EList<MResourceFolder>) this
								.getContainingClassifier()
								.getContainingPackage().getContainingComponent()
								.getResourceFolder();

						System.out.println("update needed");

						for (MResourceFolder folder : resFolder)
							folder.updateResource(this, corresponding);

					}

				}
			} else {
				this.setContainment(false);
			}

			if (!annotationWorkSuppression) {
				MExprAnnotation a = directPropertyAnnotations.getResult();
				MExprAnnotation aNew = AnnotationsFactory.eINSTANCE
						.createMInitializationValue();
				directPropertyAnnotations
						.setInitValue((MInitializationValue) aNew);
				directPropertyAnnotations.setResult(null);
				ExpressionBase defaultBase;
				if (this.getSingular()) {
					defaultBase = ExpressionBase.NULL_VALUE;
				} else {
					defaultBase = ExpressionBase.EMPTY_COLLECTION;
				}
				reuseAnnotationContent(a, aNew, defaultBase);
			}
		} else if (newPropertyBehavior == PropertyBehavior.DERIVED) {

			boolean transformNeeded = true;
			if (getPropertyBehavior() == PropertyBehavior.IS_OPERATION) {

				transformNeeded = false;
				// getContainingClassifier().allOperationAnnotations(),
				if (this.getDirectOperationSemantics() != null && this
						.getDirectOperationSemantics().getResult() != null)
					this.getDirectSemantics().setResult(
							this.getDirectOperationSemantics().getResult());

				localClassifierAnnotations.getOperationAnnotations()
						.remove(this.getDirectOperationSemantics());

				this.getOperationSignature().clear();
			}
			setHasStorage(false);
			setPersisted(false);
			setChangeable(false);// only if no update annotation exists.
			setContainment(false);
			if (!annotationWorkSuppression && transformNeeded
					&& directPropertyAnnotations.getResult() != null) {

				MExprAnnotation a = directPropertyAnnotations.getInitValue();
				MExprAnnotation aNew = AnnotationsFactory.eINSTANCE
						.createMResult();
				directPropertyAnnotations.setResult((MResult) aNew);
				directPropertyAnnotations.setInitValue(null);
				ExpressionBase defaultBase;
				if (this.getSingular()) {
					defaultBase = ExpressionBase.NULL_VALUE;
				} else {
					defaultBase = ExpressionBase.EMPTY_COLLECTION;
				}

				reuseAnnotationContent(a, aNew, defaultBase);
				// TODO LATER if choice constraint and choice construction
				// exist, move in operation...
			}
		} else {

			if (newPropertyBehavior == PropertyBehavior.IS_OPERATION) {
				this.getOperationSignature().add(
						McoreFactory.eINSTANCE.createMOperationSignature());

				if (getDirectOperationSemantics() == null) {
					MOperationAnnotations directOperationAnnotations = AnnotationsFactory.eINSTANCE
							.createMOperationAnnotations();
					directOperationAnnotations.setResult(
							AnnotationsFactory.eINSTANCE.createMResult());

					directOperationAnnotations.getResult()
							.setBase(this.getSingular()
									? ExpressionBase.NULL_VALUE
									: ExpressionBase.EMPTY_COLLECTION);

					localClassifierAnnotations.getOperationAnnotations()
							.add(directOperationAnnotations);

					directOperationAnnotations
							.setOperationSignature(this.getOperationSignature()
									.get(getOperationSignature().size() - 1));

				}
				if (this.getDirectSemantics() != null) {

					ExpressionBase defaultBase = this.getSingular()
							? ExpressionBase.NULL_VALUE
							: ExpressionBase.EMPTY_COLLECTION;
					if (this.getDirectSemantics().getResult() == null && this
							.getDirectSemantics().getInitValue() != null) {
						reuseAnnotationContent(
								this.getDirectSemantics().getInitValue(),
								this.getDirectOperationSemantics().getResult(),
								defaultBase);
					} else if (this.getDirectSemantics().getResult() == null) {
						this.getDirectOperationSemantics().setResult(
								AnnotationsFactory.eINSTANCE.createMResult());
						this.getDirectOperationSemantics().getResult()
								.setBase(defaultBase);
					}

					else {
						this.getDirectOperationSemantics().setResult(
								this.getDirectSemantics().getResult());
					}
					localClassifierAnnotations.getPropertyAnnotations()
							.remove(this.getDirectSemantics());

				}
			}

			// TODO if is OPERATION!!!

			// this.getOperationSignature().add(McoreFactory.eINSTANCE.createMOperationSignature());
			// if (!annotationWorkSuppression && directPropertyAnnotations ==
			// null) {
			// MOperationAnnotations directOperationAnnotations =
			// AnnotationsFactory.eINSTANCE
			// .createMOperationAnnotations();
			// localClassifierAnnotations.getOperationAnnotations().add(
			// directOperationAnnotations);
			// directOperationAnnotations.setOperationSignature(this.getOperationSignature());

		}
		if (!this.getContainingClassifier().getInstance().isEmpty())
			for (MObject obj : this.getContainingClassifier().getInstance())
				obj.setDoAction(MObjectAction.COMPLETE_SLOTS);

		// TODO: add feature to MProperty, which returns all its
		// MPropertyAnnotations Objects.
		// add feature to MProperty, which returns all its MResult, e.t.c.
		// annotations.
		// do loop over all of them, replacing them with each other.
	}

	private void reuseAnnotationContent(MExprAnnotation removedAnnotation,
			MExprAnnotation newAnnotation, ExpressionBase defaultBase) {
		// TODO: remove initFromOtherExprAnnotation of ExprAnnotation as there
		// no reuse, but initilization is done.
		// and complement by respecting new parts (base, expr1-3, e.t.c.)
		// AND do not always set useExplicitOcl to false...
		if (removedAnnotation != null) {
			newAnnotation.setValue(removedAnnotation.getValue());
			newAnnotation.setUseExplicitOcl(false);
			if (!(removedAnnotation.getNamedConstant() == null)
					&& !removedAnnotation.getNamedConstant().isEmpty()) {
				newAnnotation.getNamedConstant()
						.addAll(removedAnnotation.getNamedConstant());
			}
			if (!(removedAnnotation.getNamedExpression() == null)
					&& !removedAnnotation.getNamedExpression().isEmpty()) {
				newAnnotation.getNamedExpression()
						.addAll(removedAnnotation.getNamedExpression());
			}
			if (!(removedAnnotation.getNamedTuple() == null)
					&& !removedAnnotation.getNamedTuple().isEmpty()) {
				newAnnotation.getNamedTuple()
						.addAll(removedAnnotation.getNamedTuple());
			}
			// Add named tuples
			newAnnotation.setDomainObject(removedAnnotation.getDomainObject());
			newAnnotation
					.setDomainOperation(removedAnnotation.getDomainOperation());
			// Add all operations of expr annotation, base, element 1-3, cast,
			// processor
			newAnnotation.reuseFromOtherNoMoreUsedChain(removedAnnotation);

		} else {
			newAnnotation.setBase(defaultBase);
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public PropertyKind getKind() {
		/**
		 * @OCL if  not self.operationSignature->isEmpty() then PropertyKind::Operation else
		if type.oclIsUndefined() then 
		if self.simpleType=SimpleType::None then PropertyKind::TypeMissing else 
		if self.hasSimpleDataType then PropertyKind::Attribute 
		else if self.hasSimpleModelingType then PropertyKind::Reference else PropertyKind::TypeMissing endif endif endif
		else
		if type.kind = ClassifierKind::ClassType then PropertyKind::Reference else
		if type.kind =ClassifierKind::DataType or type.kind = ClassifierKind::Enumeration then PropertyKind::Attribute else
		PropertyKind::Undefined
		endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__KIND;

		if (kindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				kindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			PropertyKind result = (PropertyKind) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getContainment() {
		return containment;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainment(Boolean newContainment) {
		Boolean oldContainment = containment;
		containment = newContainment;
		boolean oldContainmentESet = containmentESet;
		containmentESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__CONTAINMENT, oldContainment,
					containment, !oldContainmentESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetContainment() {
		Boolean oldContainment = containment;
		boolean oldContainmentESet = containmentESet;
		containment = CONTAINMENT_EDEFAULT;
		containmentESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__CONTAINMENT, oldContainment,
					CONTAINMENT_EDEFAULT, oldContainmentESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetContainment() {
		return containmentESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getHasStorage() {
		return hasStorage;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setHasStorage(Boolean newHasStorage) {
		Boolean oldHasStorage = hasStorage;
		hasStorage = newHasStorage;
		boolean oldHasStorageESet = hasStorageESet;
		hasStorageESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__HAS_STORAGE, oldHasStorage,
					hasStorage, !oldHasStorageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHasStorage() {
		Boolean oldHasStorage = hasStorage;
		boolean oldHasStorageESet = hasStorageESet;
		hasStorage = HAS_STORAGE_EDEFAULT;
		hasStorageESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__HAS_STORAGE, oldHasStorage,
					HAS_STORAGE_EDEFAULT, oldHasStorageESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHasStorage() {
		return hasStorageESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getChangeable() {
		return changeable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setChangeable(Boolean newChangeable) {
		Boolean oldChangeable = changeable;
		changeable = newChangeable;
		boolean oldChangeableESet = changeableESet;
		changeableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__CHANGEABLE, oldChangeable,
					changeable, !oldChangeableESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetChangeable() {
		Boolean oldChangeable = changeable;
		boolean oldChangeableESet = changeableESet;
		changeable = CHANGEABLE_EDEFAULT;
		changeableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__CHANGEABLE, oldChangeable,
					CHANGEABLE_EDEFAULT, oldChangeableESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetChangeable() {
		return changeableESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getPersisted() {
		return persisted;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setPersisted(Boolean newPersisted) {
		Boolean oldPersisted = persisted;
		persisted = newPersisted;
		boolean oldPersistedESet = persistedESet;
		persistedESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__PERSISTED, oldPersisted, persisted,
					!oldPersistedESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetPersisted() {
		Boolean oldPersisted = persisted;
		boolean oldPersistedESet = persistedESet;
		persisted = PERSISTED_EDEFAULT;
		persistedESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__PERSISTED, oldPersisted,
					PERSISTED_EDEFAULT, oldPersistedESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetPersisted() {
		return persistedESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getTypeDefinition() {
		MClassifier typeDefinition = basicGetTypeDefinition();
		return typeDefinition != null && typeDefinition.eIsProxy()
				? (MClassifier) eResolveProxy((InternalEObject) typeDefinition)
				: typeDefinition;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetTypeDefinition() {
		/**
		 * @OCL self.type
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__TYPE_DEFINITION;

		if (typeDefinitionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				typeDefinitionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(typeDefinitionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setTypeDefinition(MClassifier newTypeDefinition) {

		System.out.println("here");

		if (newTypeDefinition != null) {

			System.out.println("a");
			// PWK: renamed needNewType into needNewProperty
			boolean needNewProperty = false;
			EList<MObject> instances = this.getContainingClassifier()
					.getInstance();
			// Need for new property seems to be checkt only for data values...
			if (!instances.isEmpty())
				for (MObject obj : instances) {
					MPropertyInstance slot = obj
							.propertyInstanceFromProperty(this);
					needNewProperty = needNewProperty || slot != null
							&& (!slot.internalDataValuesAsString().isEmpty()
									&& !obj.propertyInstanceFromProperty(this)
											.internalDataValuesAsString()
											.equals("VALUE MISSING"));
				}

			if (needNewProperty) {
				System.out.println("b");
				// TODO: put new property at old place, and push old one down
				EList<MProperty> propList = getContainingClassifier()
						.getProperty();
				MProperty newProperty = McoreFactory.eINSTANCE
						.createMProperty();
				propList.add(propList.size() - 1, newProperty);
				newProperty.setTypeDefinition(newTypeDefinition);
				newProperty.setSingular(false);
				newProperty.setContainment(false);
				newProperty.setSimpleType(SimpleType.NONE);
				return;

			} else
			// Cleaning all instances??? Seems to be done just for Data Value...
			{
				System.out.println("c");
				if (!instances.isEmpty())
					for (MObject obj : instances) {
						MPropertyInstance slot = obj
								.propertyInstanceFromProperty(this);
						if (slot != null
								&& !slot.getInternalDataValue().isEmpty())
							slot.getInternalDataValue().clear();
					}
			}

			if (this.getType() == null) {
				if (newTypeDefinition.getKind() == ClassifierKind.CLASS_TYPE) {
					this.setSingular(false);
					this.setContainment(false);
					/*
					 * If this was a default property, with default name, then
					 * name can be unset since then Type will be used as name
					 */
					if (this.getName() != null) {
						if (this.getName().equals(this.getContainingClassifier()
								.defaultPropertyDescription())) {
							setName(null);
						}
					}
				}
				this.setType(newTypeDefinition);

			} else {
				if (this.getContainment() == true) {

					this.setPropertyBehavior(PropertyBehavior.STORED);
					this.setType(newTypeDefinition);

					this.setPropertyBehavior(PropertyBehavior.CONTAINED);
				} else {
					this.setType(newTypeDefinition);
				}
			}
			this.setSimpleType(SimpleType.NONE);
		} else {
			// newTypeDefinition == null
			if (this.getType() != null) {
				this.setSimpleType(SimpleType.STRING);
			}
			this.setType(null);
		}
		System.out.println("e");
		if (!this.getContainingClassifier().getInstance().isEmpty())
			for (MObject obj : this.getContainingClassifier().getInstance())
				obj.setDoAction(MObjectAction.COMPLETE_SLOTS);

		System.out.println("f");
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Type Definition</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL self.selectableType(trg)
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalTypeDefinitionChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (typeDefinitionChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = McorePackage.Literals.MPROPERTY__TYPE_DEFINITION;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				typeDefinitionChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						"TypeDefinitionChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(typeDefinitionChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"TypeDefinitionChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getUseCoreTypes() {
		return useCoreTypes;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public void setUseCoreTypes(Boolean newUseCoreTypes) {

		/*
		if(true)
		{
			useCoreTypes= true;
			return;
		}
			
		System.out.println(this.getUseCoreTypes());
		if(this.eResource() == null)
			return;
		
		if (newUseCoreTypes) {
			boolean p = false;
		
			final String URISTRING = "mcore://www.langlets.org/coreTypes/coretypes.mcore";
			final URI uri = URI.createURI(URISTRING);
			
		
		
			for (int i = 0; i < this.eResource().getResourceSet().getResources()
					.size(); i++)
				if (this.eResource().getResourceSet().getResources().get(i)
						.getURI().toString().equals(URISTRING))
					p = true;
		
			if (!p) {
				// this.eResource().getResourceSet().createResource(uri);
				System.out.println("todo Resolve types");
			}
		
			else {
		
				for (Resource r : this.eResource().getResourceSet()
						.getResources()) {
					if (r != null)
						if (r.getURI() == uri) {
							MComponentImpl comp = (MComponentImpl) r
									.getContents().get(0);
							for (MPackage m : comp.getOwnedPackage().get(0)
									.getAllSubpackages())
								if (m.getName().equals("XML"))
									this.typePackage = m;
						}
				}
		
			}
		
		} else
			this.typePackage = null;*/

		Boolean oldUseCoreTypes = useCoreTypes;
		useCoreTypes = newUseCoreTypes;
		boolean oldUseCoreTypesESet = useCoreTypesESet;
		useCoreTypesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__USE_CORE_TYPES, oldUseCoreTypes,
					useCoreTypes, !oldUseCoreTypesESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUseCoreTypes() {
		Boolean oldUseCoreTypes = useCoreTypes;
		boolean oldUseCoreTypesESet = useCoreTypesESet;
		useCoreTypes = USE_CORE_TYPES_EDEFAULT;
		useCoreTypesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__USE_CORE_TYPES, oldUseCoreTypes,
					USE_CORE_TYPES_EDEFAULT, oldUseCoreTypesESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUseCoreTypes() {
		return useCoreTypesESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getELabel() {
		/**
		 * @OCL if stringEmpty(name)
		then eTypeLabel
		else eNameForELabel
		.concat(if self.kind=PropertyKind::Operation 
		                 then '(...)' else '' endif)
		.concat(if self.eTypeLabel=''
		                 then '' else ':'.concat(self.eTypeLabel) endif) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ELABEL;

		if (eLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getENameForELabel() {
		/**
		 * @OCL let calculatedShortNameWithoutTypeName:String = 
		if stringEmpty(shortName) then name else shortName endif in 
		if self.isReference = true
		then if self.type.oclIsUndefined()
		then if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not 				(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())
		then (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()
		else calculatedShortNameWithoutTypeName.camelCaseLower()
		endif
		else if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or 							self.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())
					then (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()
				 	else calculatedShortNameWithoutTypeName.camelCaseLower()
		     endif
		endif
		else if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not 			(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())
		then (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()
		else calculatedShortNameWithoutTypeName.camelCaseLower()
		endif
		endif 
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ENAME_FOR_ELABEL;

		if (eNameForELabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				eNameForELabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameForELabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Integer getOrderWithinGroup() {
		return orderWithinGroup;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrderWithinGroup(Integer newOrderWithinGroup) {
		Integer oldOrderWithinGroup = orderWithinGroup;
		orderWithinGroup = newOrderWithinGroup;
		boolean oldOrderWithinGroupESet = orderWithinGroupESet;
		orderWithinGroupESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__ORDER_WITHIN_GROUP,
					oldOrderWithinGroup, orderWithinGroup,
					!oldOrderWithinGroupESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOrderWithinGroup() {
		Integer oldOrderWithinGroup = orderWithinGroup;
		boolean oldOrderWithinGroupESet = orderWithinGroupESet;
		orderWithinGroup = ORDER_WITHIN_GROUP_EDEFAULT;
		orderWithinGroupESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__ORDER_WITHIN_GROUP,
					oldOrderWithinGroup, ORDER_WITHIN_GROUP_EDEFAULT,
					oldOrderWithinGroupESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOrderWithinGroup() {
		return orderWithinGroupESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		/**
		 * @OCL if self.containingClassifier.oclIsUndefined() then 'CONTAINER MISSING' else 
		let pos:Integer=self.containingClassifier.ownedProperty->indexOf(self) in
		if pos<10 then '0'.concat(pos.toString()) else pos.toString() endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ID;

		if (idDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				idDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(idDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getInternalEStructuralFeature() {
		if (internalEStructuralFeature != null
				&& internalEStructuralFeature.eIsProxy()) {
			InternalEObject oldInternalEStructuralFeature = (InternalEObject) internalEStructuralFeature;
			internalEStructuralFeature = (EStructuralFeature) eResolveProxy(
					oldInternalEStructuralFeature);
			if (internalEStructuralFeature != oldInternalEStructuralFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE,
							oldInternalEStructuralFeature,
							internalEStructuralFeature));
			}
		}
		return internalEStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetInternalEStructuralFeature() {
		return internalEStructuralFeature;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setInternalEStructuralFeature(
			EStructuralFeature newInternalEStructuralFeature) {
		EStructuralFeature oldInternalEStructuralFeature = internalEStructuralFeature;
		internalEStructuralFeature = newInternalEStructuralFeature;
		boolean oldInternalEStructuralFeatureESet = internalEStructuralFeatureESet;
		internalEStructuralFeatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE,
					oldInternalEStructuralFeature, internalEStructuralFeature,
					!oldInternalEStructuralFeatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInternalEStructuralFeature() {
		EStructuralFeature oldInternalEStructuralFeature = internalEStructuralFeature;
		boolean oldInternalEStructuralFeatureESet = internalEStructuralFeatureESet;
		internalEStructuralFeature = null;
		internalEStructuralFeatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE,
					oldInternalEStructuralFeature, null,
					oldInternalEStructuralFeatureESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInternalEStructuralFeature() {
		return internalEStructuralFeatureESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations getDirectSemantics() {
		MPropertyAnnotations directSemantics = basicGetDirectSemantics();
		return directSemantics != null && directSemantics.eIsProxy()
				? (MPropertyAnnotations) eResolveProxy(
						(InternalEObject) directSemantics)
				: directSemantics;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations basicGetDirectSemantics() {
		/**
		 * @OCL if self.containingClassifier.oclIsUndefined() 
		then null
		else let pAs:OrderedSet(annotations::MPropertyAnnotations)
		=  containingClassifier
		       .allPropertyAnnotations()
		            ->select(p:annotations::MPropertyAnnotations|
		                       p.annotatedProperty=self) in 
		      if pAs->isEmpty() 
		        then null 
		        else pAs->first() endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__DIRECT_SEMANTICS;

		if (directSemanticsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				directSemanticsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(directSemanticsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyAnnotations result = (MPropertyAnnotations) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAnnotations> getSemanticsSpecialization() {
		/**
		 * @OCL if self.containingClassifier.oclIsUndefined()
		then OrderedSet{}
		else containingClassifier
		.allSubTypes()
		  .allPropertyAnnotations()
		    ->select(p:annotations::MPropertyAnnotations
		                    |p.annotatedProperty=self) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__SEMANTICS_SPECIALIZATION;

		if (semanticsSpecializationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				semanticsSpecializationDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(semanticsSpecializationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MPropertyAnnotations> result = (EList<MPropertyAnnotations>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationSignature> getAllSignatures() {
		/**
		 * @OCL if self.containingClassifier.oclIsUndefined()
		then Sequence{}
		else self.containingClassifier.allProperties()
		->select(p:MProperty|p.kind=PropertyKind::Operation and p.calculatedShortName = self.calculatedShortName).operationSignature endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__ALL_SIGNATURES;

		if (allSignaturesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				allSignaturesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(allSignaturesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<MOperationSignature> result = (EList<MOperationSignature>) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotations getDirectOperationSemantics() {
		MOperationAnnotations directOperationSemantics = basicGetDirectOperationSemantics();
		return directOperationSemantics != null
				&& directOperationSemantics.eIsProxy()
						? (MOperationAnnotations) eResolveProxy(
								(InternalEObject) directOperationSemantics)
						: directOperationSemantics;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotations basicGetDirectOperationSemantics() {
		/**
		 * @OCL if self.containingClassifier.oclIsUndefined() 
		then null
		else let pAs:OrderedSet(annotations::MOperationAnnotations)
		=  containingClassifier
		       .allOperationAnnotations()
		            ->select(p:annotations::MOperationAnnotations|
		                       p.annotatedProperty=self) in 
		      if pAs->isEmpty() 
		        then null 
		        else pAs->first() endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__DIRECT_OPERATION_SEMANTICS;

		if (directOperationSemanticsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				directOperationSemanticsDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(directOperationSemanticsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MOperationAnnotations result = (MOperationAnnotations) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getContainingClassifier() {
		MClassifier containingClassifier = basicGetContainingClassifier();
		return containingClassifier != null && containingClassifier.eIsProxy()
				? (MClassifier) eResolveProxy(
						(InternalEObject) containingClassifier)
				: containingClassifier;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetContainingClassifier() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() then null else
		self.eContainer().oclAsType(MPropertiesContainer).containingClassifier
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__CONTAINING_CLASSIFIER;

		if (containingClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(containingClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesContainer getContainingPropertiesContainer() {
		MPropertiesContainer containingPropertiesContainer = basicGetContainingPropertiesContainer();
		return containingPropertiesContainer != null
				&& containingPropertiesContainer.eIsProxy()
						? (MPropertiesContainer) eResolveProxy(
								(InternalEObject) containingPropertiesContainer)
						: containingPropertiesContainer;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesContainer basicGetContainingPropertiesContainer() {
		/**
		 * @OCL if self.eContainer().oclIsUndefined() then null else
		self.eContainer().oclAsType(MPropertiesContainer)
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eFeature = McorePackage.Literals.MPROPERTY__CONTAINING_PROPERTIES_CONTAINER;

		if (containingPropertiesContainerDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingPropertiesContainerDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingPropertiesContainerDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertiesContainer result = (MPropertiesContainer) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MProperty> getEKeyForPath() {
		if (eKeyForPath == null) {
			eKeyForPath = new EObjectResolvingEList.Unsettable<MProperty>(
					MProperty.class, this,
					McorePackage.MPROPERTY__EKEY_FOR_PATH);
		}
		return eKeyForPath;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEKeyForPath() {
		if (eKeyForPath != null)
			((InternalEList.Unsettable<?>) eKeyForPath).unset();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEKeyForPath() {
		return eKeyForPath != null
				&& ((InternalEList.Unsettable<?>) eKeyForPath).isSet();
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>EKey For Path</b></em>' reference list.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @OCL if self.type.oclIsUndefined() 
	then OrderedSet{}
	else self.type.allFeaturesWithStorage()->select(p:MProperty|p.isAttribute) endif
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalEKeyForPathChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (eKeyForPathChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = McorePackage.Literals.MPROPERTY__EKEY_FOR_PATH;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				eKeyForPathChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstruction, helper.getProblems(), eClass,
						"EKeyForPathChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(eKeyForPathChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"EKeyForPathChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getNotUnsettable() {
		return notUnsettable;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setNotUnsettable(Boolean newNotUnsettable) {
		Boolean oldNotUnsettable = notUnsettable;
		notUnsettable = newNotUnsettable;
		boolean oldNotUnsettableESet = notUnsettableESet;
		notUnsettableESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__NOT_UNSETTABLE, oldNotUnsettable,
					notUnsettable, !oldNotUnsettableESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetNotUnsettable() {
		Boolean oldNotUnsettable = notUnsettable;
		boolean oldNotUnsettableESet = notUnsettableESet;
		notUnsettable = NOT_UNSETTABLE_EDEFAULT;
		notUnsettableESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__NOT_UNSETTABLE, oldNotUnsettable,
					NOT_UNSETTABLE_EDEFAULT, oldNotUnsettableESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetNotUnsettable() {
		return notUnsettableESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAttributeForEId() {
		return attributeForEId;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeForEId(Boolean newAttributeForEId) {
		Boolean oldAttributeForEId = attributeForEId;
		attributeForEId = newAttributeForEId;
		boolean oldAttributeForEIdESet = attributeForEIdESet;
		attributeForEIdESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID,
					oldAttributeForEId, attributeForEId,
					!oldAttributeForEIdESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAttributeForEId() {
		Boolean oldAttributeForEId = attributeForEId;
		boolean oldAttributeForEIdESet = attributeForEIdESet;
		attributeForEId = ATTRIBUTE_FOR_EID_EDEFAULT;
		attributeForEIdESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID,
					oldAttributeForEId, ATTRIBUTE_FOR_EID_EDEFAULT,
					oldAttributeForEIdESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAttributeForEId() {
		return attributeForEIdESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public String getEDefaultValueLiteral() {
		return eDefaultValueLiteral;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setEDefaultValueLiteral(String newEDefaultValueLiteral) {
		String oldEDefaultValueLiteral = eDefaultValueLiteral;
		eDefaultValueLiteral = newEDefaultValueLiteral;
		boolean oldEDefaultValueLiteralESet = eDefaultValueLiteralESet;
		eDefaultValueLiteralESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL,
					oldEDefaultValueLiteral, eDefaultValueLiteral,
					!oldEDefaultValueLiteralESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetEDefaultValueLiteral() {
		String oldEDefaultValueLiteral = eDefaultValueLiteral;
		boolean oldEDefaultValueLiteralESet = eDefaultValueLiteralESet;
		eDefaultValueLiteral = EDEFAULT_VALUE_LITERAL_EDEFAULT;
		eDefaultValueLiteralESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL,
					oldEDefaultValueLiteral, EDEFAULT_VALUE_LITERAL_EDEFAULT,
					oldEDefaultValueLiteralESet));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetEDefaultValueLiteral() {
		return eDefaultValueLiteralESet;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean selectableType(MClassifier mClassifier) {

		/**
		 * @OCL  mClassifier.selectableClassifier(
		if type=null then OrderedSet{} else OrderedSet{type} endif, 
		typePackage)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EOperation eOperation = McorePackage.Literals.MPROPERTY.getEOperations()
				.get(4);
		if (selectableTypemcoreMClassifierBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				selectableTypemcoreMClassifierBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eOperation);
			}
		}

		Query query = OCL_ENV
				.createQuery(selectableTypemcoreMClassifierBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query
					.getEvaluationEnvironment();

			evalEnv.add("mClassifier", mClassifier);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ambiguousPropertyName() {

		/**
		 * @OCL if true then false else
		if self.containingClassifier.oclIsUndefined() then false else
		if self.isOperation then false else
		if self.containingClassifier.allProperties()->isEmpty() then false else
		self.containingClassifier.allProperties()
		->exists(p:MProperty|not (p=self) 
		and (not p.isOperation)
		and p.sameName(self)
		and if p.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(p.containingClassifier.containingPackage.derivedNamePrefix)
			then if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix)
					then if self.containingClassifier.containingPackage.derivedNamePrefix = p.containingClassifier.containingPackage.derivedNamePrefix
							then true
							else false
							endif
					else false
					endif
		   	else true
		   	endif
		and if stringEmpty(p.name) then 
		        p.type = self.type else true endif)
		endif endif endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EOperation eOperation = McorePackage.Literals.MPROPERTY.getEOperations()
				.get(5);
		if (ambiguousPropertyNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ambiguousPropertyNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ambiguousPropertyNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean ambiguousPropertyShortName() {

		/**
		 * @OCL if true then false else
		if self.ambiguousPropertyName() then true else
		if self.stringEmpty(self.shortName) then false else
		if self.containingClassifier.oclIsUndefined() then false else
		if self.containingClassifier.allProperties()->isEmpty() then false else
		if self.isOperation
		then
		self.containingClassifier.allProperties()
		->exists(p:MProperty|
		(not (p=self)) 
		and p.isOperation 
		and (
		  (p.sameShortName(self)
		    and (not p.sameName(self))
		    and (not p.stringEmpty(p.name)) 
		    and (not  p.stringEmpty(self.name)))
		 or
		  ((not p.sameShortName(self))
		   and p.sameName(self)
		   and (not p.stringEmpty(p.name))
		   and (not  p.stringEmpty(self.name)))
		 )
		)
		else
		self.containingClassifier.allProperties()
		->exists(p:MProperty|
		(not (p=self)) 
		and (not p.isOperation) 
		and p.sameShortName(self)
		and not p.stringEmpty(p.name) 
		and not  p.stringEmpty(self.name))
		endif endif endif endif endif
		endif
		 * @templateTag IGOT01
		 */
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EOperation eOperation = McorePackage.Literals.MPROPERTY.getEOperations()
				.get(6);
		if (ambiguousPropertyShortNameBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation,
					eClass());

			try {
				ambiguousPropertyShortNameBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						body, helper.getProblems(),
						McorePackage.Literals.MPROPERTY, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(ambiguousPropertyShortNameBodyOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query,
					McorePackage.Literals.MPROPERTY, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public XUpdate doActionUpdate(MPropertyAction trg) {

		// TODO Change code to work with new semantics model

		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();
		switch (trg.getValue()) {
		case MPropertyAction.INTO_NARY_VALUE: {

			EEnumLiteral eEnumLiteralINTO_NARY = McorePackage.Literals.MPROPERTY_ACTION
					.getEEnumLiteral(MPropertyAction.INTO_NARY_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_DoAction(),
					XUpdateMode.REDEFINE, null,
					// :=
					eEnumLiteralINTO_NARY, null, null);

			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMExplicitlyTyped_Mandatory(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);

			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMExplicitlyTyped_Singular(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);

			return currentTrigger;
		}

		case MPropertyAction.INTO_DERIVED_VALUE: {
			// 1. Create the XUpdate, which triggered it all
			// this.doAction := INTO_DERIVED_VALUE
			EEnumLiteral eEnumLiteralINTO_DERIVED_VALUE = McorePackage.Literals.MPROPERTY_ACTION
					.getEEnumLiteral(MPropertyAction.INTO_DERIVED_VALUE);
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_DoAction(),
					XUpdateMode.REDEFINE, null,
					// :=
					eEnumLiteralINTO_DERIVED_VALUE, null, null);

			// 2. this.changeable := FALSE
			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_Changeable(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);
			// 3. this.containment := FALSE
			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_Containment(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);
			// 4. this.persisted := FALSE
			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_Persisted(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);
			// 5. this.hasStorage := FALSE
			currentTrigger.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_HasStorage(),
					XUpdateMode.REDEFINE, null,
					// :=
					false, null, null);

			// 6. create/use direct semantics, in this class
			EObject propertyAnnotations = null;
			if (this.getDirectSemantics() != null) {
				propertyAnnotations = this.getDirectSemantics();
				// TODO: move to this propertyGroup, if it is in another one.
			} else {
				// 7. create/use semantics/annotations in PropertyContainer
				// (Class or PropertyGroup)
				EObject semantics = null;
				if (this.getContainingPropertiesContainer()
						.getAnnotations() != null) {
					semantics = this.getContainingPropertiesContainer()
							.getAnnotations();
				} else {
					semantics = currentTrigger.createNextStateNewObject(
							AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS);
					MPropertiesContainer containingPropertiesContainer = this
							.getContainingPropertiesContainer();
					currentTrigger.addReferenceUpdate(
							containingPropertiesContainer,
							McorePackage.eINSTANCE
									.getMPropertiesContainer_Annotations(),
							XUpdateMode.REDEFINE, null,
							// :=
							semantics, null, null);
				}
				// continue 6.
				propertyAnnotations = currentTrigger.createNextStateNewObject(
						AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS);
				// 8. Add new propertyAnnotations to semantics
				currentTrigger.addReferenceUpdate(semantics,
						AnnotationsPackage.eINSTANCE
								.getMClassifierAnnotations_PropertyAnnotations(),
						XUpdateMode.ADD, XAddUpdateMode.LAST,
						propertyAnnotations, null, null);
				// 9.Set property of new propertyAnnotations
				currentTrigger.addReferenceUpdate(propertyAnnotations,
						AnnotationsPackage.eINSTANCE
								.getMPropertyAnnotations_Property(),
						XUpdateMode.REDEFINE, null,
						// :=
						this, null, null);
			}
			// 10. Create a new result annotation
			EObject resultAnnotation = currentTrigger.createNextStateNewObject(
					AnnotationsPackage.Literals.MRESULT);
			// 11. Add new result annotation to propertyAnnotatios
			currentTrigger.addReferenceUpdate(propertyAnnotations,
					AnnotationsPackage.eINSTANCE
							.getMPropertyAnnotations_Result(),
					XUpdateMode.REDEFINE, null,
					// :=
					resultAnnotation, null, null);
			// 12. Set base of resultAnntation to null
			EEnumLiteral eEnumLiteralNULL_VALUE = ExpressionsPackage.Literals.EXPRESSION_BASE
					.getEEnumLiteral(ExpressionBase.NULL_VALUE_VALUE);
			currentTrigger.addAttributeUpdate(resultAnnotation,
					ExpressionsPackage.eINSTANCE
							.getMAbstractExpressionWithBase_Base(),
					XUpdateMode.REDEFINE, null,
					// :=
					eEnumLiteralNULL_VALUE, null, null);
			// 13. Focus on resultAnnotation
			transition.getFocusObjects().add(currentTrigger
					.nextStateObjectDefinitionFromObject(resultAnnotation));
			// NEW add to semantics of root.
			// create XSemantics in MCom, if it does not exist.

			MComponent rootObject = (MComponent) this.eResource().getContents()
					.get(0);
			EObject semantics = null;
			if (rootObject.getSemantics() == null) {
				// root.setXSemantics(SemanticsFactory.eINSTANCE.createXSemantics());
				semantics = currentTrigger.createNextStateNewObject(
						SemanticsPackage.Literals.XSEMANTICS);
				transition.addReferenceUpdate(rootObject,
						McorePackage.eINSTANCE.getMComponent_Semantics(),
						XUpdateMode.REDEFINE, null, semantics, null, null);
			} else {
				semantics = rootObject.getSemantics();
			}

			// 14. Return the trigger of all this.

			return currentTrigger;
		}

		default:

			EEnumLiteral eEnumLiteralTrigger = McorePackage.Literals.MPROPERTY_ACTION
					.getEEnumLiteral(trg.getValue());
			XUpdate currentTrigger = transition.addAttributeUpdate(this,
					McorePackage.eINSTANCE.getMProperty_DoAction(),
					XUpdateMode.REDEFINE, null,
					// :=
					eEnumLiteralTrigger, null, null);

			// Operations return the Object that shall be focussed
			// -> Put focus command on that
			transition.getFocusObjects()
					.add(currentTrigger.nextStateObjectDefinitionFromObject(
							currentTrigger.invokeOperationCall(this,
									McorePackage.Literals.MPROPERTY___INVOKE_SET_DO_ACTION__MPROPERTYACTION,
									trg, null, null)));

			return currentTrigger;

		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public String invokeSetPropertyBehaviour(
			PropertyBehavior propertyBehavior) {

		this.setPropertyBehavior(propertyBehavior);
		return "done";
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	public Object invokeSetDoAction(MPropertyAction mPropertyAction) {

		this.setDoAction(mPropertyAction);

		if ((getContainingClassifier().getAnnotations() != null
				&& !getContainingClassifier().getAnnotations()
						.getPropertyAnnotations().isEmpty())) {
			EList<MPropertyAnnotations> propertyAnnotations = this
					.getContainingClassifier().getAnnotations()
					.getPropertyAnnotations();
			for (MPropertyAnnotations annotation : propertyAnnotations)
				if (annotation.getProperty() == this) {
					switch (mPropertyAction) {
					case INIT_ORDER:
						return annotation.getInitOrder();
					case CONSTRAIN_CHOICE:
						return annotation.getChoiceConstraint();
					case CONSTRUCT_CHOICE:
						return annotation.getChoiceConstruction();
					case HIDE_IN_PROPERTIES_VIEW:
						return annotation.getLayout();
					case HIDE_IN_TABLE_EDITOR:
						return annotation.getLayout();
					case HIGHLIGHT:
						return annotation.getHighlight();
					case INIT_VALUE:
						return annotation.getInitValue();
					case SHOW_IN_PROPERTIES_VIEW:
						return annotation.getLayout();
					case SHOW_IN_TABLE_EDITOR:
						return annotation.getLayout();
					case ADD_UPDATE_ANNOTATION:
						return annotation.getUpdate().isEmpty() ? this
								: annotation.getUpdate()
										.get(annotation.getUpdate().size() - 1)
										.getValue();
					case RESULT:
						return this.getDirectSemantics().getResult();
					default:
						break;
					}
				}
		}
		return this;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
			return ((InternalEList<?>) getGeneralAnnotation())
					.basicRemove(otherEnd, msgs);
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			return ((InternalEList<?>) getOperationSignature())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
			return getGeneralAnnotation();
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			return getOperationSignature();
		case McorePackage.MPROPERTY__OPPOSITE_DEFINITION:
			if (resolve)
				return getOppositeDefinition();
			return basicGetOppositeDefinition();
		case McorePackage.MPROPERTY__OPPOSITE:
			if (resolve)
				return getOpposite();
			return basicGetOpposite();
		case McorePackage.MPROPERTY__CONTAINMENT:
			return getContainment();
		case McorePackage.MPROPERTY__HAS_STORAGE:
			return getHasStorage();
		case McorePackage.MPROPERTY__CHANGEABLE:
			return getChangeable();
		case McorePackage.MPROPERTY__PERSISTED:
			return getPersisted();
		case McorePackage.MPROPERTY__PROPERTY_BEHAVIOR:
			return getPropertyBehavior();
		case McorePackage.MPROPERTY__IS_OPERATION:
			return getIsOperation();
		case McorePackage.MPROPERTY__IS_ATTRIBUTE:
			return getIsAttribute();
		case McorePackage.MPROPERTY__IS_REFERENCE:
			return getIsReference();
		case McorePackage.MPROPERTY__KIND:
			return getKind();
		case McorePackage.MPROPERTY__TYPE_DEFINITION:
			if (resolve)
				return getTypeDefinition();
			return basicGetTypeDefinition();
		case McorePackage.MPROPERTY__USE_CORE_TYPES:
			return getUseCoreTypes();
		case McorePackage.MPROPERTY__ELABEL:
			return getELabel();
		case McorePackage.MPROPERTY__ENAME_FOR_ELABEL:
			return getENameForELabel();
		case McorePackage.MPROPERTY__ORDER_WITHIN_GROUP:
			return getOrderWithinGroup();
		case McorePackage.MPROPERTY__ID:
			return getId();
		case McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE:
			if (resolve)
				return getInternalEStructuralFeature();
			return basicGetInternalEStructuralFeature();
		case McorePackage.MPROPERTY__DIRECT_SEMANTICS:
			if (resolve)
				return getDirectSemantics();
			return basicGetDirectSemantics();
		case McorePackage.MPROPERTY__SEMANTICS_SPECIALIZATION:
			return getSemanticsSpecialization();
		case McorePackage.MPROPERTY__ALL_SIGNATURES:
			return getAllSignatures();
		case McorePackage.MPROPERTY__DIRECT_OPERATION_SEMANTICS:
			if (resolve)
				return getDirectOperationSemantics();
			return basicGetDirectOperationSemantics();
		case McorePackage.MPROPERTY__CONTAINING_CLASSIFIER:
			if (resolve)
				return getContainingClassifier();
			return basicGetContainingClassifier();
		case McorePackage.MPROPERTY__CONTAINING_PROPERTIES_CONTAINER:
			if (resolve)
				return getContainingPropertiesContainer();
			return basicGetContainingPropertiesContainer();
		case McorePackage.MPROPERTY__EKEY_FOR_PATH:
			return getEKeyForPath();
		case McorePackage.MPROPERTY__NOT_UNSETTABLE:
			return getNotUnsettable();
		case McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID:
			return getAttributeForEId();
		case McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL:
			return getEDefaultValueLiteral();
		case McorePackage.MPROPERTY__DO_ACTION:
			return getDoAction();
		case McorePackage.MPROPERTY__BEHAVIOR_ACTIONS:
			return getBehaviorActions();
		case McorePackage.MPROPERTY__LAYOUT_ACTIONS:
			return getLayoutActions();
		case McorePackage.MPROPERTY__ARITY_ACTIONS:
			return getArityActions();
		case McorePackage.MPROPERTY__SIMPLE_TYPE_ACTIONS:
			return getSimpleTypeActions();
		case McorePackage.MPROPERTY__ANNOTATION_ACTIONS:
			return getAnnotationActions();
		case McorePackage.MPROPERTY__MOVE_ACTIONS:
			return getMoveActions();
		case McorePackage.MPROPERTY__DERIVED_JSON_SCHEMA:
			return getDerivedJsonSchema();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
			getGeneralAnnotation().clear();
			getGeneralAnnotation().addAll(
					(Collection<? extends MGeneralAnnotation>) newValue);
			return;
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			getOperationSignature().clear();
			getOperationSignature().addAll(
					(Collection<? extends MOperationSignature>) newValue);
			return;
		case McorePackage.MPROPERTY__OPPOSITE_DEFINITION:
			setOppositeDefinition((MProperty) newValue);
			return;
		case McorePackage.MPROPERTY__OPPOSITE:
			setOpposite((MProperty) newValue);
			return;
		case McorePackage.MPROPERTY__CONTAINMENT:
			setContainment((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__HAS_STORAGE:
			setHasStorage((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__CHANGEABLE:
			setChangeable((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__PERSISTED:
			setPersisted((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__PROPERTY_BEHAVIOR:
			setPropertyBehavior((PropertyBehavior) newValue);
			return;
		case McorePackage.MPROPERTY__TYPE_DEFINITION:
			setTypeDefinition((MClassifier) newValue);
			return;
		case McorePackage.MPROPERTY__USE_CORE_TYPES:
			setUseCoreTypes((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__ORDER_WITHIN_GROUP:
			setOrderWithinGroup((Integer) newValue);
			return;
		case McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE:
			setInternalEStructuralFeature((EStructuralFeature) newValue);
			return;
		case McorePackage.MPROPERTY__EKEY_FOR_PATH:
			getEKeyForPath().clear();
			getEKeyForPath().addAll((Collection<? extends MProperty>) newValue);
			return;
		case McorePackage.MPROPERTY__NOT_UNSETTABLE:
			setNotUnsettable((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID:
			setAttributeForEId((Boolean) newValue);
			return;
		case McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL:
			setEDefaultValueLiteral((String) newValue);
			return;
		case McorePackage.MPROPERTY__DO_ACTION:
			setDoAction((MPropertyAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
			unsetGeneralAnnotation();
			return;
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			unsetOperationSignature();
			return;
		case McorePackage.MPROPERTY__OPPOSITE_DEFINITION:
			setOppositeDefinition((MProperty) null);
			return;
		case McorePackage.MPROPERTY__OPPOSITE:
			unsetOpposite();
			return;
		case McorePackage.MPROPERTY__CONTAINMENT:
			unsetContainment();
			return;
		case McorePackage.MPROPERTY__HAS_STORAGE:
			unsetHasStorage();
			return;
		case McorePackage.MPROPERTY__CHANGEABLE:
			unsetChangeable();
			return;
		case McorePackage.MPROPERTY__PERSISTED:
			unsetPersisted();
			return;
		case McorePackage.MPROPERTY__PROPERTY_BEHAVIOR:
			setPropertyBehavior(PROPERTY_BEHAVIOR_EDEFAULT);
			return;
		case McorePackage.MPROPERTY__TYPE_DEFINITION:
			setTypeDefinition((MClassifier) null);
			return;
		case McorePackage.MPROPERTY__USE_CORE_TYPES:
			unsetUseCoreTypes();
			return;
		case McorePackage.MPROPERTY__ORDER_WITHIN_GROUP:
			unsetOrderWithinGroup();
			return;
		case McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE:
			unsetInternalEStructuralFeature();
			return;
		case McorePackage.MPROPERTY__EKEY_FOR_PATH:
			unsetEKeyForPath();
			return;
		case McorePackage.MPROPERTY__NOT_UNSETTABLE:
			unsetNotUnsettable();
			return;
		case McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID:
			unsetAttributeForEId();
			return;
		case McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL:
			unsetEDefaultValueLiteral();
			return;
		case McorePackage.MPROPERTY__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
			return isSetGeneralAnnotation();
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			return isSetOperationSignature();
		case McorePackage.MPROPERTY__OPPOSITE_DEFINITION:
			return basicGetOppositeDefinition() != null;
		case McorePackage.MPROPERTY__OPPOSITE:
			return isSetOpposite();
		case McorePackage.MPROPERTY__CONTAINMENT:
			return isSetContainment();
		case McorePackage.MPROPERTY__HAS_STORAGE:
			return isSetHasStorage();
		case McorePackage.MPROPERTY__CHANGEABLE:
			return isSetChangeable();
		case McorePackage.MPROPERTY__PERSISTED:
			return isSetPersisted();
		case McorePackage.MPROPERTY__PROPERTY_BEHAVIOR:
			return getPropertyBehavior() != PROPERTY_BEHAVIOR_EDEFAULT;
		case McorePackage.MPROPERTY__IS_OPERATION:
			return IS_OPERATION_EDEFAULT == null ? getIsOperation() != null
					: !IS_OPERATION_EDEFAULT.equals(getIsOperation());
		case McorePackage.MPROPERTY__IS_ATTRIBUTE:
			return IS_ATTRIBUTE_EDEFAULT == null ? getIsAttribute() != null
					: !IS_ATTRIBUTE_EDEFAULT.equals(getIsAttribute());
		case McorePackage.MPROPERTY__IS_REFERENCE:
			return IS_REFERENCE_EDEFAULT == null ? getIsReference() != null
					: !IS_REFERENCE_EDEFAULT.equals(getIsReference());
		case McorePackage.MPROPERTY__KIND:
			return getKind() != KIND_EDEFAULT;
		case McorePackage.MPROPERTY__TYPE_DEFINITION:
			return basicGetTypeDefinition() != null;
		case McorePackage.MPROPERTY__USE_CORE_TYPES:
			return isSetUseCoreTypes();
		case McorePackage.MPROPERTY__ELABEL:
			return ELABEL_EDEFAULT == null ? getELabel() != null
					: !ELABEL_EDEFAULT.equals(getELabel());
		case McorePackage.MPROPERTY__ENAME_FOR_ELABEL:
			return ENAME_FOR_ELABEL_EDEFAULT == null
					? getENameForELabel() != null
					: !ENAME_FOR_ELABEL_EDEFAULT.equals(getENameForELabel());
		case McorePackage.MPROPERTY__ORDER_WITHIN_GROUP:
			return isSetOrderWithinGroup();
		case McorePackage.MPROPERTY__ID:
			return ID_EDEFAULT == null ? getId() != null
					: !ID_EDEFAULT.equals(getId());
		case McorePackage.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE:
			return isSetInternalEStructuralFeature();
		case McorePackage.MPROPERTY__DIRECT_SEMANTICS:
			return basicGetDirectSemantics() != null;
		case McorePackage.MPROPERTY__SEMANTICS_SPECIALIZATION:
			return !getSemanticsSpecialization().isEmpty();
		case McorePackage.MPROPERTY__ALL_SIGNATURES:
			return !getAllSignatures().isEmpty();
		case McorePackage.MPROPERTY__DIRECT_OPERATION_SEMANTICS:
			return basicGetDirectOperationSemantics() != null;
		case McorePackage.MPROPERTY__CONTAINING_CLASSIFIER:
			return basicGetContainingClassifier() != null;
		case McorePackage.MPROPERTY__CONTAINING_PROPERTIES_CONTAINER:
			return basicGetContainingPropertiesContainer() != null;
		case McorePackage.MPROPERTY__EKEY_FOR_PATH:
			return isSetEKeyForPath();
		case McorePackage.MPROPERTY__NOT_UNSETTABLE:
			return isSetNotUnsettable();
		case McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID:
			return isSetAttributeForEId();
		case McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL:
			return isSetEDefaultValueLiteral();
		case McorePackage.MPROPERTY__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		case McorePackage.MPROPERTY__BEHAVIOR_ACTIONS:
			return !getBehaviorActions().isEmpty();
		case McorePackage.MPROPERTY__LAYOUT_ACTIONS:
			return !getLayoutActions().isEmpty();
		case McorePackage.MPROPERTY__ARITY_ACTIONS:
			return !getArityActions().isEmpty();
		case McorePackage.MPROPERTY__SIMPLE_TYPE_ACTIONS:
			return !getSimpleTypeActions().isEmpty();
		case McorePackage.MPROPERTY__ANNOTATION_ACTIONS:
			return !getAnnotationActions().isEmpty();
		case McorePackage.MPROPERTY__MOVE_ACTIONS:
			return !getMoveActions().isEmpty();
		case McorePackage.MPROPERTY__DERIVED_JSON_SCHEMA:
			return DERIVED_JSON_SCHEMA_EDEFAULT == null
					? getDerivedJsonSchema() != null
					: !DERIVED_JSON_SCHEMA_EDEFAULT
							.equals(getDerivedJsonSchema());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (derivedFeatureID) {
			case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
				return McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID,
			Class<?> baseClass) {
		if (baseClass == MModelElement.class) {
			switch (baseFeatureID) {
			case McorePackage.MMODEL_ELEMENT__GENERAL_ANNOTATION:
				return McorePackage.MPROPERTY__GENERAL_ANNOTATION;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case McorePackage.MPROPERTY___OPPOSITE_DEFINITION$_UPDATE__MPROPERTY:
			return oppositeDefinition$Update((MProperty) arguments.get(0));
		case McorePackage.MPROPERTY___PROPERTY_BEHAVIOR$_UPDATE__PROPERTYBEHAVIOR:
			return propertyBehavior$Update((PropertyBehavior) arguments.get(0));
		case McorePackage.MPROPERTY___TYPE_DEFINITION$_UPDATE__MCLASSIFIER:
			return typeDefinition$Update((MClassifier) arguments.get(0));
		case McorePackage.MPROPERTY___DO_ACTION$_UPDATE__MPROPERTYACTION:
			return doAction$Update((MPropertyAction) arguments.get(0));
		case McorePackage.MPROPERTY___SELECTABLE_TYPE__MCLASSIFIER:
			return selectableType((MClassifier) arguments.get(0));
		case McorePackage.MPROPERTY___AMBIGUOUS_PROPERTY_NAME:
			return ambiguousPropertyName();
		case McorePackage.MPROPERTY___AMBIGUOUS_PROPERTY_SHORT_NAME:
			return ambiguousPropertyShortName();
		case McorePackage.MPROPERTY___DO_ACTION_UPDATE__MPROPERTYACTION:
			return doActionUpdate((MPropertyAction) arguments.get(0));
		case McorePackage.MPROPERTY___INVOKE_SET_PROPERTY_BEHAVIOUR__PROPERTYBEHAVIOR:
			return invokeSetPropertyBehaviour(
					(PropertyBehavior) arguments.get(0));
		case McorePackage.MPROPERTY___INVOKE_SET_DO_ACTION__MPROPERTYACTION:
			return invokeSetDoAction((MPropertyAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (containment: ");
		if (containmentESet)
			result.append(containment);
		else
			result.append("<unset>");
		result.append(", hasStorage: ");
		if (hasStorageESet)
			result.append(hasStorage);
		else
			result.append("<unset>");
		result.append(", changeable: ");
		if (changeableESet)
			result.append(changeable);
		else
			result.append("<unset>");
		result.append(", persisted: ");
		if (persistedESet)
			result.append(persisted);
		else
			result.append("<unset>");
		result.append(", useCoreTypes: ");
		if (useCoreTypesESet)
			result.append(useCoreTypes);
		else
			result.append("<unset>");
		result.append(", orderWithinGroup: ");
		if (orderWithinGroupESet)
			result.append(orderWithinGroup);
		else
			result.append("<unset>");
		result.append(", notUnsettable: ");
		if (notUnsettableESet)
			result.append(notUnsettable);
		else
			result.append("<unset>");
		result.append(", attributeForEId: ");
		if (attributeForEIdESet)
			result.append(attributeForEId);
		else
			result.append("<unset>");
		result.append(", eDefaultValueLiteral: ");
		if (eDefaultValueLiteralESet)
			result.append(eDefaultValueLiteral);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!-- <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @OCL eLabel
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						label, helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					"label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL fullLabel (if kind=PropertyKind::Attribute then '[attribute] ' else
	if kind=PropertyKind::Reference then '[reference] ' else
	if kind=PropertyKind::Operation then '[operation] ' else
	if kind=PropertyKind::Ambiguous then '[AMBIGUOUS]' else
	if kind=PropertyKind::TypeMissing then '[TYPE MISSING] ' else
	if kind=PropertyKind::Undefined then '[UNDEFINED] ' else
	'[IMPOSSIBLE] ' endif endif endif endif endif endif)
	.concat(self.localStructuralName)
	.concat(
	if kind = PropertyKind::Operation then '(...)' else '' endif) 
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getFullLabel() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__FULL_LABEL;

		if (fullLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				fullLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(fullLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL kindLabel if self.propertyBehavior = mcore::PropertyBehavior::Contained then
	if (not(self.opposite.oclIsUndefined()) and (self.opposite.multiplicityCase = MultiplicityCase::OneOne))
	then 'Composition'
	else
	'Containment'
	endif
	else
	if self.propertyBehavior = mcore::PropertyBehavior::Derived then
	let changeableString: String = if (self.changeable) then ' Change. ' else ' '  endif in
	if self.kind = mcore::PropertyKind::Attribute then 'Derived'.concat(changeableString).concat('Attr.') else
	if self.kind = mcore::PropertyKind::Reference then 'Derived'.concat(changeableString).concat('Ref.') else 'Derived WRONG' endif endif 
	else
	if self.kind = mcore::PropertyKind::Attribute then 'Attribute' else
	if self.kind = mcore::PropertyKind::Reference then
	'Reference'
	else
	if self.kind = mcore::PropertyKind::Operation then 'Operation' else
	'ERROR' endif endif endif endif endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL correctName (not ambiguousPropertyName()) and
	if self.hasSimpleDataType then not self.stringEmpty(name) else true endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CORRECT_NAME;

		if (correctNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL correctShortName not ambiguousPropertyShortName()
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCorrectShortName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CORRECT_SHORT_NAME;

		if (correctShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				correctShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(correctShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedMandatory self.mandatory
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedMandatory() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_MANDATORY;

		if (calculatedMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedType self.type
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetCalculatedType() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_TYPE;

		if (calculatedTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedSingular self.singular
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getCalculatedSingular() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SINGULAR;

		if (calculatedSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL simpleTypeIsCorrect if  not type.oclIsUndefined() then simpleType=SimpleType::None else
	if voidTypeAllowed then true else simpleType<>SimpleType::None
	endif endif
	/* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... *\/
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getSimpleTypeIsCorrect() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT;

		if (simpleTypeIsCorrectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				simpleTypeIsCorrectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(simpleTypeIsCorrectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL voidTypeAllowed self.kind=PropertyKind::Operation
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public Boolean getVoidTypeAllowed() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED;

		if (voidTypeAllowedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				voidTypeAllowedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(voidTypeAllowedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedSimpleType simpleType
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getCalculatedSimpleType() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE;

		if (calculatedSimpleTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedSimpleTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedSimpleTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL localStructuralName if self.containingClassifier.oclIsUndefined() then '' else
	self.containingClassifier.localStructuralName
	.concat('/')
	.concat(self.calculatedShortName.camelCaseUpper())
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getLocalStructuralName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__LOCAL_STRUCTURAL_NAME;

		if (localStructuralNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				localStructuralNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(localStructuralNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedName (if not stringEmpty(name) then name else  
	if hasSimpleDataType then 'NAMEMISSING' else nameFromType() endif
	endif)
	.concat(if self.appendTypeNameToName.oclIsUndefined() then ''
	            else if self.appendTypeNameToName = false then ''
	            else if self.appendTypeNameToName = true and stringEmpty(name)=false then
	                               ' '.concat(nameFromType())
	            else '' endif endif endif
	            )
	.concat(if ambiguousPropertyName() then '  (AMBIGUOUS)' else '' endif)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CALCULATED_NAME;

		if (calculatedNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL calculatedShortName (if stringEmpty(name) then shortNameFromType()
	else
	if stringEmpty(shortName) then name
	else shortName
	endif
	endif)
	.concat(if self.appendTypeNameToName.oclIsUndefined() then ''
	            else if self.appendTypeNameToName = false then ''
	            else if self.appendTypeNameToName = true and stringEmpty(name)=false then
	                   if self.hasSimpleDataType or self.hasSimpleModelingType then ''
	                            else   ' '.concat(shortNameFromType())
	                   endif
	            else '' endif endif endif
	            )
	.concat(if self.ambiguousPropertyShortName() then '( AMBIGUOUS)' else '' endif)
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getCalculatedShortName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__CALCULATED_SHORT_NAME;

		if (calculatedShortNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				calculatedShortNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(calculatedShortNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @overrideOCL eName if stringEmpty(self.specialEName) = true 
	then if self.isReference = true
	then if self.type.oclIsUndefined()
		then if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not 				(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())
			then (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()
		 	else self.calculatedShortName.camelCaseLower()
		 	endif
		else if stringEmpty(self.calculatedShortName) and ( not (stringEmpty(self.type.specialEName) or stringEmpty(self.type.specialEName.trim()))) and true
				then self.type.specialEName.camelCaseLower()
				else if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or 							self.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())
						then (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()
					 	else self.calculatedShortName.camelCaseLower()
						endif
				endif
		endif
	else if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not 			(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())
			then (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()
			else self.calculatedShortName.camelCaseLower()
			endif
	endif 
	else self.specialEName.camelCaseLower()
	endif
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getEName() {
		EClass eClass = (McorePackage.Literals.MPROPERTY);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MNAMED__ENAME;

		if (eNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				eNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						derive, helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(eNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Type</b></em>
	 * ' reference. The constraint is applied in the context of the source of
	 * the reference, and the target of the reference being of type MClassifier
	 * Inside the constraint, the target can be accessed as 'trg'. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @overrideOCL type true
	 * @templateTag INS05
	 * @generated
	 */
	@Override
	public boolean evalTypeChoiceConstraint(MClassifier trg) {
		EClass eClass = McorePackage.Literals.MPROPERTY;
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MEXPLICITLY_TYPED__TYPE;
		if (typeChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);

			addEnvironmentVariable("trg", eOverrideFeature.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eOverrideFeature,
							eClass());

			try {
				typeChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(McorePackage.PLUGIN_ID,
						choiceConstraint, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}
		Query query = OCL_ENV.createQuery(typeChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(McorePackage.PLUGIN_ID, query, eClass,
					eOverrideFeature);
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}

	/**
	 * @templateTag INS14
	 * @generated
	 */

	@Override
	protected EStructuralFeature[] getInitializedStructuralFeatures() {
		EStructuralFeature[] baseInititalizedFeatured = super.getInitializedStructuralFeatures();
		EStructuralFeature[] initializedFeatures = new EStructuralFeature[] {
				McorePackage.Literals.MPROPERTY__HAS_STORAGE,
				McorePackage.Literals.MPROPERTY__CHANGEABLE,
				McorePackage.Literals.MPROPERTY__PERSISTED };
		EStructuralFeature[] allInitializedFeatures = new EStructuralFeature[baseInititalizedFeatured.length
				+ initializedFeatures.length];
		System.arraycopy(baseInititalizedFeatured, 0, allInitializedFeatures, 0,
				baseInititalizedFeatured.length);
		System.arraycopy(initializedFeatures, 0, allInitializedFeatures,
				baseInititalizedFeatured.length, initializedFeatures.length);
		return allInitializedFeatures;
	}

} // MPropertyImpl
