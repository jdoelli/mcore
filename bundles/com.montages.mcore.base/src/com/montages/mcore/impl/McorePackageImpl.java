/**
 */
package com.montages.mcore.impl;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MComponent;
import com.montages.mcore.MComponentAction;
import com.montages.mcore.MEditor;
import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.MExplicitlyTypedAndNamed;
import com.montages.mcore.MHasSimpleType;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MLiteralAction;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MNamed;
import com.montages.mcore.MNamedEditor;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MOperationSignatureAction;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.MParameter;
import com.montages.mcore.MParameterAction;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.MPropertiesGroupAction;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.MRepository;
import com.montages.mcore.MRepositoryElement;
import com.montages.mcore.MTyped;
import com.montages.mcore.MVariable;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.MultiplicityCase;
import com.montages.mcore.OrderingStrategy;
import com.montages.mcore.PropertyBehavior;
import com.montages.mcore.PropertyKind;
import com.montages.mcore.SimpleType;
import com.montages.mcore.TopLevelDomain;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.impl.AnnotationsPackageImpl;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.ExpressionsPackageImpl;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.ObjectsPackageImpl;
import com.montages.mcore.tables.TablesPackage;
import com.montages.mcore.tables.impl.TablesPackageImpl;
import com.montages.mcore.updates.UpdatesPackage;
import com.montages.mcore.updates.impl.UpdatesPackageImpl;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.langlets.acore.AcorePackage;
import org.langlets.acore.classifiers.ClassifiersPackage;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.SemanticsPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class McorePackageImpl extends EPackageImpl implements McorePackage {
	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails()
					.get("rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID,
						choiceConstraint, helper.getProblems(),
						McorePackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRepositoryElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mRepositoryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mModelElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPackageEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mHasSimpleTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mClassifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPropertiesContainerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPropertiesGroupEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mOperationSignatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mExplicitlyTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mExplicitlyTypedAndNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mEditorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mNamedEditorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mComponentActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum topLevelDomainEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mPackageActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum simpleTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mClassifierActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum orderingStrategyEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mLiteralActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mPropertiesGroupActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mPropertyActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum propertyBehaviorEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mOperationSignatureActionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum multiplicityCaseEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum classifierKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum propertyKindEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mParameterActionEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.mcore.McorePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private McorePackageImpl() {
		super(eNS_URI, McoreFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link McorePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static McorePackage init() {
		if (isInited)
			return (McorePackage) EPackage.Registry.INSTANCE
					.getEPackage(McorePackage.eNS_URI);

		// Obtain or create and register package
		McorePackageImpl theMcorePackage = (McorePackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof McorePackageImpl
						? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new McorePackageImpl());

		isInited = true;

		// Initialize simple dependencies
		AcorePackage.eINSTANCE.eClass();
		SemanticsPackage.eINSTANCE.eClass();

		// Obtain or create and register interdependencies
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(AnnotationsPackage.eNS_URI)
								: AnnotationsPackage.eINSTANCE);
		ExpressionsPackageImpl theExpressionsPackage = (ExpressionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ExpressionsPackage.eNS_URI) instanceof ExpressionsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ExpressionsPackage.eNS_URI)
								: ExpressionsPackage.eINSTANCE);
		ObjectsPackageImpl theObjectsPackage = (ObjectsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						ObjectsPackage.eNS_URI) instanceof ObjectsPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(ObjectsPackage.eNS_URI)
								: ObjectsPackage.eINSTANCE);
		TablesPackageImpl theTablesPackage = (TablesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI) instanceof TablesPackageImpl
						? EPackage.Registry.INSTANCE
								.getEPackage(TablesPackage.eNS_URI)
						: TablesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(
						UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
								? EPackage.Registry.INSTANCE
										.getEPackage(UpdatesPackage.eNS_URI)
								: UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theMcorePackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theExpressionsPackage.createPackageContents();
		theObjectsPackage.createPackageContents();
		theTablesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theMcorePackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theExpressionsPackage.initializePackageContents();
		theObjectsPackage.initializePackageContents();
		theTablesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theMcorePackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(McorePackage.eNS_URI, theMcorePackage);
		return theMcorePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRepositoryElement() {
		return mRepositoryElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepositoryElement_KindLabel() {
		return (EAttribute) mRepositoryElementEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepositoryElement_RenderedKindLabel() {
		return (EAttribute) mRepositoryElementEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepositoryElement_IndentLevel() {
		return (EAttribute) mRepositoryElementEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepositoryElement_Description() {
		return (EAttribute) mRepositoryElementEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepositoryElement_AsText() {
		return (EAttribute) mRepositoryElementEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRepositoryElement__IndentationSpaces() {
		return mRepositoryElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRepositoryElement__IndentationSpaces__Integer() {
		return mRepositoryElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMRepositoryElement__NewLineString() {
		return mRepositoryElementEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMRepository() {
		return mRepositoryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRepository_Component() {
		return (EReference) mRepositoryEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRepository_Core() {
		return (EReference) mRepositoryEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMRepository_ReferencedEPackage() {
		return (EReference) mRepositoryEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMRepository_AvoidRegenerationOfEditorConfiguration() {
		return (EAttribute) mRepositoryEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNamed() {
		return mNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_CalculatedShortName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_Name() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_ShortName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_CorrectName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_CorrectShortName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_EName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_SpecialEName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_FullLabel() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_LocalStructuralName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamed_CalculatedName() {
		return (EAttribute) mNamedEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamed__SameName__MNamed() {
		return mNamedEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamed__SameShortName__MNamed() {
		return mNamedEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamed__SameString__String_String() {
		return mNamedEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMNamed__StringEmpty__String() {
		return mNamedEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMComponent() {
		return mComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DoAction() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMComponent__DoAction$Update__MComponentAction() {
		return mComponentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMComponent__DoActionUpdate__MComponentAction() {
		return mComponentEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_OwnedPackage() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_ResourceFolder() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_Semantics() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_PreloadedComponent() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_GeneratedEPackage() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_HideAdvancedProperties() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_AbstractComponent() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_CountImplementations() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DisableDefaultContainment() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_AllGeneralAnnotations() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_NamedEditor() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_MainEditor() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_UseLegacyEditorconfig() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_MainNamedEditor() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_NamePrefix() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DerivedNamePrefix() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_NamePrefixForFeatures() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DerivedBundleName() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DerivedURI() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_RepresentsCoreComponent() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_RootPackage() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_AvoidRegenerationOfEditorConfiguration() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMComponent_ContainingRepository() {
		return (EReference) mComponentEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DomainType() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DomainName() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DerivedDomainType() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_DerivedDomainName() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMComponent_SpecialBundleName() {
		return (EAttribute) mComponentEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMModelElement() {
		return mModelElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMModelElement_GeneralAnnotation() {
		return (EReference) mModelElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPackage() {
		return mPackageEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DoAction() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPackage__DoAction$Update__MPackageAction() {
		return mPackageEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPackage__GenerateJsonSchema$Update__Boolean() {
		return mPackageEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPackage__DoActionUpdate__MPackageAction() {
		return mPackageEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_Classifier() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_SubPackage() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_PackageAnnotations() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_NamePrefix() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedNamePrefix() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_NamePrefixScope() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedJsonSchema() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_GenerateJsonSchema() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_GeneratedJsonSchema() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPackage__GenerateJsonSchema$update01Object__Boolean() {
		return mPackageEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPackage__GenerateJsonSchema$update01Value__Boolean_MPackage() {
		return mPackageEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_UsePackageContentOnlyWithExplicitFilter() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_SpecialNsURI() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_SpecialNsPrefix() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_SpecialFileName() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_UseUUID() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_UuidAttributeName() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_Parent() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_ContainingPackage() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_ContainingComponent() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_RepresentsCorePackage() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_AllSubpackages() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_QualifiedName() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_IsRootPackage() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_InternalEPackage() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedNsURI() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedStandardNsURI() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedNsPrefix() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPackage_DerivedJavaPackageName() {
		return (EAttribute) mPackageEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPackage_ResourceRootClass() {
		return (EReference) mPackageEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMHasSimpleType() {
		return mHasSimpleTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMHasSimpleType_SimpleTypeString() {
		return (EAttribute) mHasSimpleTypeEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMHasSimpleType_SimpleType() {
		return (EAttribute) mHasSimpleTypeEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMHasSimpleType_HasSimpleDataType() {
		return (EAttribute) mHasSimpleTypeEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMHasSimpleType_HasSimpleModelingType() {
		return (EAttribute) mHasSimpleTypeEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMHasSimpleType_SimpleTypeIsCorrect() {
		return (EAttribute) mHasSimpleTypeEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMHasSimpleType__SimpleTypeAsString__SimpleType() {
		return mHasSimpleTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMClassifier() {
		return mClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_DoAction() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_ResolveContainerAction() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_AbstractDoAction() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ObjectReferences() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_NearestInstance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_StrictNearestInstance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_StrictInstance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IntelligentNearestInstance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ObjectUnreferenced() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_OutstandingToCopyObjects() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_PropertyInstances() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ObjectReferencePropertyInstances() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IntelligentInstance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ContainmentHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_StrictAllClassesContainedIn() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_StrictContainmentHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IntelligentAllClassesContainedIn() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IntelligentContainmentHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_InstantiationHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(36);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_InstantiationPropertyHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(37);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IntelligentInstantiationPropertyHierarchy() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(38);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_Classescontainedin() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(39);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_CanApplyDefaultContainment() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(40);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_PropertiesAsText() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(41);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_SemanticsAsText() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(42);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_DerivedJsonSchema() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(43);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__ToggleSuperType$Update__MClassifier() {
		return mClassifierEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__OrderingStrategy$Update__OrderingStrategy() {
		return mClassifierEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__DoAction$Update__MClassifierAction() {
		return mClassifierEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_SuperTypePackage() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_SuperType() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_Instance() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_Kind() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_EnforcedClass() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_AbstractClass() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_InternalEClassifier() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ContainingPackage() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_RepresentsCoreType() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_AllPropertyGroups() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_Literal() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_EInterface() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_OperationAsIdPropertyError() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_IsClassByStructure() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_ToggleSuperType() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMClassifier_OrderingStrategy() {
		return (EAttribute) mClassifierEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_TestAllProperties() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMClassifier_IdProperty() {
		return (EReference) mClassifierEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__DefaultPropertyValue() {
		return mClassifierEClass.getEOperations().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__DefaultPropertyDescription() {
		return mClassifierEClass.getEOperations().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllLiterals() {
		return mClassifierEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__SelectableClassifier__EList_MPackage() {
		return mClassifierEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllDirectSubTypes() {
		return mClassifierEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllSubTypes() {
		return mClassifierEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__SuperTypesAsString() {
		return mClassifierEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllSuperTypes() {
		return mClassifierEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllProperties() {
		return mClassifierEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllSuperProperties() {
		return mClassifierEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllFeatures() {
		return mClassifierEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllNonContainmentFeatures() {
		return mClassifierEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllStoredNonContainmentFeatures() {
		return mClassifierEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllFeaturesWithStorage() {
		return mClassifierEClass.getEOperations().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllContainmentReferences() {
		return mClassifierEClass.getEOperations().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllOperationSignatures() {
		return mClassifierEClass.getEOperations().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllClassesContainedIn() {
		return mClassifierEClass.getEOperations().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllIdProperties() {
		return mClassifierEClass.getEOperations().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AllSuperIdProperties() {
		return mClassifierEClass.getEOperations().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__DoActionUpdate__MClassifierAction() {
		return mClassifierEClass.getEOperations().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__InvokeSetDoAction__MClassifierAction() {
		return mClassifierEClass.getEOperations().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__InvokeToggleSuperType__MClassifier() {
		return mClassifierEClass.getEOperations().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__DoSuperTypeUpdate__MClassifier() {
		return mClassifierEClass.getEOperations().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMClassifier__AmbiguousClassifierName() {
		return mClassifierEClass.getEOperations().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMLiteral() {
		return mLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMLiteral_ConstantLabel() {
		return (EAttribute) mLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMLiteral_AsString() {
		return (EAttribute) mLiteralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMLiteral_QualifiedName() {
		return (EAttribute) mLiteralEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteral_ContainingEnumeration() {
		return (EReference) mLiteralEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMLiteral_InternalEEnumLiteral() {
		return (EReference) mLiteralEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPropertiesContainer() {
		return mPropertiesContainerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_Property() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_Annotations() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_PropertyGroup() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_AllConstraintAnnotations() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_ContainingClassifier() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMPropertiesContainer_OwnedProperty() {
		return (EReference) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertiesContainer_NrOfPropertiesAndSignatures() {
		return (EAttribute) mPropertiesContainerEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertiesContainer__AllPropertyAnnotations() {
		return mPropertiesContainerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertiesContainer__AllOperationAnnotations() {
		return mPropertiesContainerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMPropertiesGroup() {
		return mPropertiesGroupEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertiesGroup_DoAction() {
		return (EAttribute) mPropertiesGroupEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertiesGroup__DoAction$Update__MPropertiesGroupAction() {
		return mPropertiesGroupEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction() {
		return mPropertiesGroupEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction() {
		return mPropertiesGroupEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMPropertiesGroup_Order() {
		return (EAttribute) mPropertiesGroupEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMProperty() {
		return mPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_DoAction() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(29);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_BehaviorActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(30);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_LayoutActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(31);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_ArityActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(32);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_SimpleTypeActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(33);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_AnnotationActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(34);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_MoveActions() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(35);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_DerivedJsonSchema() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(36);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__OppositeDefinition$Update__MProperty() {
		return mPropertyEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__PropertyBehavior$Update__PropertyBehavior() {
		return mPropertyEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__TypeDefinition$Update__MClassifier() {
		return mPropertyEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__DoAction$Update__MPropertyAction() {
		return mPropertyEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_OperationSignature() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_OppositeDefinition() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_Opposite() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_IsOperation() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_IsAttribute() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_IsReference() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_PropertyBehavior() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_Kind() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_Containment() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_HasStorage() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_Changeable() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_Persisted() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_TypeDefinition() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_UseCoreTypes() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_ELabel() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(14);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_ENameForELabel() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(15);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_OrderWithinGroup() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(16);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_Id() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(17);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_InternalEStructuralFeature() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(18);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_DirectSemantics() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(19);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_SemanticsSpecialization() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(20);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_AllSignatures() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(21);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_DirectOperationSemantics() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(22);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_ContainingClassifier() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(23);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_ContainingPropertiesContainer() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(24);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMProperty_EKeyForPath() {
		return (EReference) mPropertyEClass.getEStructuralFeatures().get(25);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_NotUnsettable() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(26);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_AttributeForEId() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(27);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMProperty_EDefaultValueLiteral() {
		return (EAttribute) mPropertyEClass.getEStructuralFeatures().get(28);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__SelectableType__MClassifier() {
		return mPropertyEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__AmbiguousPropertyName() {
		return mPropertyEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__AmbiguousPropertyShortName() {
		return mPropertyEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__DoActionUpdate__MPropertyAction() {
		return mPropertyEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior() {
		return mPropertyEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMProperty__InvokeSetDoAction__MPropertyAction() {
		return mPropertyEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMOperationSignature() {
		return mOperationSignatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMOperationSignature_DoAction() {
		return (EAttribute) mOperationSignatureEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMOperationSignature__DoAction$Update__MOperationSignatureAction() {
		return mOperationSignatureEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationSignature_Parameter() {
		return (EReference) mOperationSignatureEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationSignature_Operation() {
		return (EReference) mOperationSignatureEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationSignature_ContainingClassifier() {
		return (EReference) mOperationSignatureEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMOperationSignature_ELabel() {
		return (EAttribute) mOperationSignatureEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMOperationSignature_InternalEOperation() {
		return (EReference) mOperationSignatureEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMOperationSignature__SameSignature__MOperationSignature() {
		return mOperationSignatureEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMOperationSignature__SignatureAsString() {
		return mOperationSignatureEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMTyped() {
		return mTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_MultiplicityAsString() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTyped_CalculatedType() {
		return (EReference) mTypedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_CalculatedMandatory() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_CalculatedSingular() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMTyped_CalculatedTypePackage() {
		return (EReference) mTypedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_VoidTypeAllowed() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_CalculatedSimpleType() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMTyped_MultiplicityCase() {
		return (EAttribute) mTypedEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTyped__MultiplicityCase$Update__MultiplicityCase() {
		return mTypedEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean() {
		return mTypedEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMExplicitlyTyped() {
		return mExplicitlyTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExplicitlyTyped_TypePackage() {
		return (EReference) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMExplicitlyTyped_Type() {
		return (EReference) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTyped_Mandatory() {
		return (EAttribute) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTyped_Singular() {
		return (EAttribute) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTyped_ETypeName() {
		return (EAttribute) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTyped_ETypeLabel() {
		return (EAttribute) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTyped_CorrectlyTyped() {
		return (EAttribute) mExplicitlyTypedEClass.getEStructuralFeatures()
				.get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMVariable() {
		return mVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMExplicitlyTypedAndNamed() {
		return mExplicitlyTypedAndNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMExplicitlyTypedAndNamed_AppendTypeNameToName() {
		return (EAttribute) mExplicitlyTypedAndNamedEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExplicitlyTypedAndNamed__NameFromType() {
		return mExplicitlyTypedAndNamedEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMExplicitlyTypedAndNamed__ShortNameFromType() {
		return mExplicitlyTypedAndNamedEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMParameter() {
		return mParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMParameter_ContainingSignature() {
		return (EReference) mParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMParameter_ELabel() {
		return (EAttribute) mParameterEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMParameter_SignatureAsString() {
		return (EAttribute) mParameterEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMParameter_InternalEParameter() {
		return (EReference) mParameterEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMParameter_DoAction() {
		return (EAttribute) mParameterEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMParameter__DoAction$Update__MParameterAction() {
		return mParameterEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMParameter__AmbiguousParameterName() {
		return mParameterEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMParameter__AmbiguousParameterShortName() {
		return mParameterEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMEditor() {
		return mEditorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMEditor__ResetEditor() {
		return mEditorEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMNamedEditor() {
		return mNamedEditorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamedEditor_Name() {
		return (EAttribute) mNamedEditorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamedEditor_Label() {
		return (EAttribute) mNamedEditorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMNamedEditor_UserVisible() {
		return (EAttribute) mNamedEditorEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMNamedEditor_Editor() {
		return (EReference) mNamedEditorEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMComponentAction() {
		return mComponentActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTopLevelDomain() {
		return topLevelDomainEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMPackageAction() {
		return mPackageActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getSimpleType() {
		return simpleTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMClassifierAction() {
		return mClassifierActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getOrderingStrategy() {
		return orderingStrategyEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMLiteralAction() {
		return mLiteralActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMPropertiesGroupAction() {
		return mPropertiesGroupActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMPropertyAction() {
		return mPropertyActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPropertyBehavior() {
		return propertyBehaviorEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMOperationSignatureAction() {
		return mOperationSignatureActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMultiplicityCase() {
		return multiplicityCaseEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getClassifierKind() {
		return classifierKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPropertyKind() {
		return propertyKindEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMParameterAction() {
		return mParameterActionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McoreFactory getMcoreFactory() {
		return (McoreFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		mRepositoryElementEClass = createEClass(MREPOSITORY_ELEMENT);
		createEAttribute(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT__KIND_LABEL);
		createEAttribute(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT__RENDERED_KIND_LABEL);
		createEAttribute(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT__INDENT_LEVEL);
		createEAttribute(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT__DESCRIPTION);
		createEAttribute(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT__AS_TEXT);
		createEOperation(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT___INDENTATION_SPACES);
		createEOperation(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT___INDENTATION_SPACES__INTEGER);
		createEOperation(mRepositoryElementEClass,
				MREPOSITORY_ELEMENT___NEW_LINE_STRING);

		mRepositoryEClass = createEClass(MREPOSITORY);
		createEReference(mRepositoryEClass, MREPOSITORY__COMPONENT);
		createEReference(mRepositoryEClass, MREPOSITORY__CORE);
		createEReference(mRepositoryEClass, MREPOSITORY__REFERENCED_EPACKAGE);
		createEAttribute(mRepositoryEClass,
				MREPOSITORY__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION);

		mNamedEClass = createEClass(MNAMED);
		createEAttribute(mNamedEClass, MNAMED__SPECIAL_ENAME);
		createEAttribute(mNamedEClass, MNAMED__NAME);
		createEAttribute(mNamedEClass, MNAMED__SHORT_NAME);
		createEAttribute(mNamedEClass, MNAMED__ENAME);
		createEAttribute(mNamedEClass, MNAMED__FULL_LABEL);
		createEAttribute(mNamedEClass, MNAMED__LOCAL_STRUCTURAL_NAME);
		createEAttribute(mNamedEClass, MNAMED__CALCULATED_NAME);
		createEAttribute(mNamedEClass, MNAMED__CALCULATED_SHORT_NAME);
		createEAttribute(mNamedEClass, MNAMED__CORRECT_NAME);
		createEAttribute(mNamedEClass, MNAMED__CORRECT_SHORT_NAME);
		createEOperation(mNamedEClass, MNAMED___SAME_NAME__MNAMED);
		createEOperation(mNamedEClass, MNAMED___SAME_SHORT_NAME__MNAMED);
		createEOperation(mNamedEClass, MNAMED___SAME_STRING__STRING_STRING);
		createEOperation(mNamedEClass, MNAMED___STRING_EMPTY__STRING);

		mComponentEClass = createEClass(MCOMPONENT);
		createEReference(mComponentEClass, MCOMPONENT__OWNED_PACKAGE);
		createEReference(mComponentEClass, MCOMPONENT__RESOURCE_FOLDER);
		createEReference(mComponentEClass, MCOMPONENT__SEMANTICS);
		createEReference(mComponentEClass, MCOMPONENT__GENERATED_EPACKAGE);
		createEReference(mComponentEClass, MCOMPONENT__NAMED_EDITOR);
		createEAttribute(mComponentEClass, MCOMPONENT__NAME_PREFIX);
		createEAttribute(mComponentEClass, MCOMPONENT__DERIVED_NAME_PREFIX);
		createEAttribute(mComponentEClass,
				MCOMPONENT__NAME_PREFIX_FOR_FEATURES);
		createEReference(mComponentEClass, MCOMPONENT__CONTAINING_REPOSITORY);
		createEAttribute(mComponentEClass,
				MCOMPONENT__REPRESENTS_CORE_COMPONENT);
		createEReference(mComponentEClass, MCOMPONENT__ROOT_PACKAGE);
		createEAttribute(mComponentEClass, MCOMPONENT__DOMAIN_TYPE);
		createEAttribute(mComponentEClass, MCOMPONENT__DOMAIN_NAME);
		createEAttribute(mComponentEClass, MCOMPONENT__DERIVED_DOMAIN_TYPE);
		createEAttribute(mComponentEClass, MCOMPONENT__DERIVED_DOMAIN_NAME);
		createEAttribute(mComponentEClass, MCOMPONENT__DERIVED_BUNDLE_NAME);
		createEAttribute(mComponentEClass, MCOMPONENT__DERIVED_URI);
		createEAttribute(mComponentEClass, MCOMPONENT__SPECIAL_BUNDLE_NAME);
		createEReference(mComponentEClass, MCOMPONENT__PRELOADED_COMPONENT);
		createEReference(mComponentEClass, MCOMPONENT__MAIN_NAMED_EDITOR);
		createEReference(mComponentEClass, MCOMPONENT__MAIN_EDITOR);
		createEAttribute(mComponentEClass,
				MCOMPONENT__HIDE_ADVANCED_PROPERTIES);
		createEReference(mComponentEClass, MCOMPONENT__ALL_GENERAL_ANNOTATIONS);
		createEAttribute(mComponentEClass, MCOMPONENT__ABSTRACT_COMPONENT);
		createEAttribute(mComponentEClass, MCOMPONENT__COUNT_IMPLEMENTATIONS);
		createEAttribute(mComponentEClass,
				MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT);
		createEAttribute(mComponentEClass, MCOMPONENT__DO_ACTION);
		createEAttribute(mComponentEClass,
				MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION);
		createEAttribute(mComponentEClass, MCOMPONENT__USE_LEGACY_EDITORCONFIG);
		createEOperation(mComponentEClass,
				MCOMPONENT___DO_ACTION$_UPDATE__MCOMPONENTACTION);
		createEOperation(mComponentEClass,
				MCOMPONENT___DO_ACTION_UPDATE__MCOMPONENTACTION);

		mModelElementEClass = createEClass(MMODEL_ELEMENT);
		createEReference(mModelElementEClass,
				MMODEL_ELEMENT__GENERAL_ANNOTATION);

		mPackageEClass = createEClass(MPACKAGE);
		createEReference(mPackageEClass, MPACKAGE__CLASSIFIER);
		createEReference(mPackageEClass, MPACKAGE__SUB_PACKAGE);
		createEReference(mPackageEClass, MPACKAGE__PACKAGE_ANNOTATIONS);
		createEAttribute(mPackageEClass,
				MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER);
		createEReference(mPackageEClass, MPACKAGE__RESOURCE_ROOT_CLASS);
		createEAttribute(mPackageEClass, MPACKAGE__SPECIAL_NS_URI);
		createEAttribute(mPackageEClass, MPACKAGE__SPECIAL_NS_PREFIX);
		createEAttribute(mPackageEClass, MPACKAGE__SPECIAL_FILE_NAME);
		createEAttribute(mPackageEClass, MPACKAGE__USE_UUID);
		createEAttribute(mPackageEClass, MPACKAGE__UUID_ATTRIBUTE_NAME);
		createEReference(mPackageEClass, MPACKAGE__PARENT);
		createEReference(mPackageEClass, MPACKAGE__CONTAINING_PACKAGE);
		createEReference(mPackageEClass, MPACKAGE__CONTAINING_COMPONENT);
		createEAttribute(mPackageEClass, MPACKAGE__REPRESENTS_CORE_PACKAGE);
		createEReference(mPackageEClass, MPACKAGE__ALL_SUBPACKAGES);
		createEAttribute(mPackageEClass, MPACKAGE__QUALIFIED_NAME);
		createEAttribute(mPackageEClass, MPACKAGE__IS_ROOT_PACKAGE);
		createEReference(mPackageEClass, MPACKAGE__INTERNAL_EPACKAGE);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_NS_URI);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_STANDARD_NS_URI);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_NS_PREFIX);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_JAVA_PACKAGE_NAME);
		createEAttribute(mPackageEClass, MPACKAGE__DO_ACTION);
		createEAttribute(mPackageEClass, MPACKAGE__NAME_PREFIX);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_NAME_PREFIX);
		createEReference(mPackageEClass, MPACKAGE__NAME_PREFIX_SCOPE);
		createEAttribute(mPackageEClass, MPACKAGE__DERIVED_JSON_SCHEMA);
		createEAttribute(mPackageEClass, MPACKAGE__GENERATE_JSON_SCHEMA);
		createEAttribute(mPackageEClass, MPACKAGE__GENERATED_JSON_SCHEMA);
		createEOperation(mPackageEClass,
				MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_OBJECT__BOOLEAN);
		createEOperation(mPackageEClass,
				MPACKAGE___GENERATE_JSON_SCHEMA$UPDATE01_VALUE__BOOLEAN_MPACKAGE);
		createEOperation(mPackageEClass,
				MPACKAGE___DO_ACTION$_UPDATE__MPACKAGEACTION);
		createEOperation(mPackageEClass,
				MPACKAGE___GENERATE_JSON_SCHEMA$_UPDATE__BOOLEAN);
		createEOperation(mPackageEClass,
				MPACKAGE___DO_ACTION_UPDATE__MPACKAGEACTION);

		mHasSimpleTypeEClass = createEClass(MHAS_SIMPLE_TYPE);
		createEAttribute(mHasSimpleTypeEClass,
				MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING);
		createEAttribute(mHasSimpleTypeEClass,
				MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE);
		createEAttribute(mHasSimpleTypeEClass,
				MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE);
		createEAttribute(mHasSimpleTypeEClass,
				MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT);
		createEAttribute(mHasSimpleTypeEClass, MHAS_SIMPLE_TYPE__SIMPLE_TYPE);
		createEOperation(mHasSimpleTypeEClass,
				MHAS_SIMPLE_TYPE___SIMPLE_TYPE_AS_STRING__SIMPLETYPE);

		mClassifierEClass = createEClass(MCLASSIFIER);
		createEAttribute(mClassifierEClass, MCLASSIFIER__ABSTRACT_CLASS);
		createEAttribute(mClassifierEClass, MCLASSIFIER__ENFORCED_CLASS);
		createEReference(mClassifierEClass, MCLASSIFIER__SUPER_TYPE_PACKAGE);
		createEReference(mClassifierEClass, MCLASSIFIER__SUPER_TYPE);
		createEReference(mClassifierEClass, MCLASSIFIER__INSTANCE);
		createEAttribute(mClassifierEClass, MCLASSIFIER__KIND);
		createEReference(mClassifierEClass, MCLASSIFIER__INTERNAL_ECLASSIFIER);
		createEReference(mClassifierEClass, MCLASSIFIER__CONTAINING_PACKAGE);
		createEAttribute(mClassifierEClass, MCLASSIFIER__REPRESENTS_CORE_TYPE);
		createEReference(mClassifierEClass, MCLASSIFIER__ALL_PROPERTY_GROUPS);
		createEReference(mClassifierEClass, MCLASSIFIER__LITERAL);
		createEAttribute(mClassifierEClass, MCLASSIFIER__EINTERFACE);
		createEAttribute(mClassifierEClass,
				MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR);
		createEAttribute(mClassifierEClass, MCLASSIFIER__IS_CLASS_BY_STRUCTURE);
		createEReference(mClassifierEClass, MCLASSIFIER__TOGGLE_SUPER_TYPE);
		createEAttribute(mClassifierEClass, MCLASSIFIER__ORDERING_STRATEGY);
		createEReference(mClassifierEClass, MCLASSIFIER__TEST_ALL_PROPERTIES);
		createEReference(mClassifierEClass, MCLASSIFIER__ID_PROPERTY);
		createEAttribute(mClassifierEClass, MCLASSIFIER__DO_ACTION);
		createEAttribute(mClassifierEClass,
				MCLASSIFIER__RESOLVE_CONTAINER_ACTION);
		createEAttribute(mClassifierEClass, MCLASSIFIER__ABSTRACT_DO_ACTION);
		createEReference(mClassifierEClass, MCLASSIFIER__OBJECT_REFERENCES);
		createEReference(mClassifierEClass, MCLASSIFIER__NEAREST_INSTANCE);
		createEReference(mClassifierEClass,
				MCLASSIFIER__STRICT_NEAREST_INSTANCE);
		createEReference(mClassifierEClass, MCLASSIFIER__STRICT_INSTANCE);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE);
		createEReference(mClassifierEClass, MCLASSIFIER__OBJECT_UNREFERENCED);
		createEReference(mClassifierEClass,
				MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS);
		createEReference(mClassifierEClass, MCLASSIFIER__PROPERTY_INSTANCES);
		createEReference(mClassifierEClass,
				MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES);
		createEReference(mClassifierEClass, MCLASSIFIER__INTELLIGENT_INSTANCE);
		createEReference(mClassifierEClass, MCLASSIFIER__CONTAINMENT_HIERARCHY);
		createEReference(mClassifierEClass,
				MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN);
		createEReference(mClassifierEClass,
				MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INSTANTIATION_HIERARCHY);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY);
		createEReference(mClassifierEClass,
				MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY);
		createEReference(mClassifierEClass, MCLASSIFIER__CLASSESCONTAINEDIN);
		createEAttribute(mClassifierEClass,
				MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT);
		createEAttribute(mClassifierEClass, MCLASSIFIER__PROPERTIES_AS_TEXT);
		createEAttribute(mClassifierEClass, MCLASSIFIER__SEMANTICS_AS_TEXT);
		createEAttribute(mClassifierEClass, MCLASSIFIER__DERIVED_JSON_SCHEMA);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___TOGGLE_SUPER_TYPE$_UPDATE__MCLASSIFIER);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ORDERING_STRATEGY$_UPDATE__ORDERINGSTRATEGY);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___DO_ACTION$_UPDATE__MCLASSIFIERACTION);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_LITERALS);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___SELECTABLE_CLASSIFIER__ELIST_MPACKAGE);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_DIRECT_SUB_TYPES);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_SUB_TYPES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___SUPER_TYPES_AS_STRING);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_SUPER_TYPES);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_PROPERTIES);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_SUPER_PROPERTIES);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_FEATURES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_NON_CONTAINMENT_FEATURES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_STORED_NON_CONTAINMENT_FEATURES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_FEATURES_WITH_STORAGE);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_CONTAINMENT_REFERENCES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_OPERATION_SIGNATURES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_CLASSES_CONTAINED_IN);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___DEFAULT_PROPERTY_VALUE);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___DEFAULT_PROPERTY_DESCRIPTION);
		createEOperation(mClassifierEClass, MCLASSIFIER___ALL_ID_PROPERTIES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___ALL_SUPER_ID_PROPERTIES);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___DO_ACTION_UPDATE__MCLASSIFIERACTION);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___INVOKE_SET_DO_ACTION__MCLASSIFIERACTION);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___INVOKE_TOGGLE_SUPER_TYPE__MCLASSIFIER);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___DO_SUPER_TYPE_UPDATE__MCLASSIFIER);
		createEOperation(mClassifierEClass,
				MCLASSIFIER___AMBIGUOUS_CLASSIFIER_NAME);

		mLiteralEClass = createEClass(MLITERAL);
		createEAttribute(mLiteralEClass, MLITERAL__CONSTANT_LABEL);
		createEAttribute(mLiteralEClass, MLITERAL__AS_STRING);
		createEAttribute(mLiteralEClass, MLITERAL__QUALIFIED_NAME);
		createEReference(mLiteralEClass, MLITERAL__CONTAINING_ENUMERATION);
		createEReference(mLiteralEClass, MLITERAL__INTERNAL_EENUM_LITERAL);

		mPropertiesContainerEClass = createEClass(MPROPERTIES_CONTAINER);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__PROPERTY);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__ANNOTATIONS);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__PROPERTY_GROUP);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER);
		createEReference(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__OWNED_PROPERTY);
		createEAttribute(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES);
		createEOperation(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER___ALL_PROPERTY_ANNOTATIONS);
		createEOperation(mPropertiesContainerEClass,
				MPROPERTIES_CONTAINER___ALL_OPERATION_ANNOTATIONS);

		mPropertiesGroupEClass = createEClass(MPROPERTIES_GROUP);
		createEAttribute(mPropertiesGroupEClass, MPROPERTIES_GROUP__ORDER);
		createEAttribute(mPropertiesGroupEClass, MPROPERTIES_GROUP__DO_ACTION);
		createEOperation(mPropertiesGroupEClass,
				MPROPERTIES_GROUP___DO_ACTION$_UPDATE__MPROPERTIESGROUPACTION);
		createEOperation(mPropertiesGroupEClass,
				MPROPERTIES_GROUP___DO_ACTION_UPDATE__MPROPERTIESGROUPACTION);
		createEOperation(mPropertiesGroupEClass,
				MPROPERTIES_GROUP___INVOKE_SET_DO_ACTION__MPROPERTIESGROUPACTION);

		mPropertyEClass = createEClass(MPROPERTY);
		createEReference(mPropertyEClass, MPROPERTY__OPERATION_SIGNATURE);
		createEReference(mPropertyEClass, MPROPERTY__OPPOSITE_DEFINITION);
		createEReference(mPropertyEClass, MPROPERTY__OPPOSITE);
		createEAttribute(mPropertyEClass, MPROPERTY__CONTAINMENT);
		createEAttribute(mPropertyEClass, MPROPERTY__HAS_STORAGE);
		createEAttribute(mPropertyEClass, MPROPERTY__CHANGEABLE);
		createEAttribute(mPropertyEClass, MPROPERTY__PERSISTED);
		createEAttribute(mPropertyEClass, MPROPERTY__PROPERTY_BEHAVIOR);
		createEAttribute(mPropertyEClass, MPROPERTY__IS_OPERATION);
		createEAttribute(mPropertyEClass, MPROPERTY__IS_ATTRIBUTE);
		createEAttribute(mPropertyEClass, MPROPERTY__IS_REFERENCE);
		createEAttribute(mPropertyEClass, MPROPERTY__KIND);
		createEReference(mPropertyEClass, MPROPERTY__TYPE_DEFINITION);
		createEAttribute(mPropertyEClass, MPROPERTY__USE_CORE_TYPES);
		createEAttribute(mPropertyEClass, MPROPERTY__ELABEL);
		createEAttribute(mPropertyEClass, MPROPERTY__ENAME_FOR_ELABEL);
		createEAttribute(mPropertyEClass, MPROPERTY__ORDER_WITHIN_GROUP);
		createEAttribute(mPropertyEClass, MPROPERTY__ID);
		createEReference(mPropertyEClass,
				MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE);
		createEReference(mPropertyEClass, MPROPERTY__DIRECT_SEMANTICS);
		createEReference(mPropertyEClass, MPROPERTY__SEMANTICS_SPECIALIZATION);
		createEReference(mPropertyEClass, MPROPERTY__ALL_SIGNATURES);
		createEReference(mPropertyEClass,
				MPROPERTY__DIRECT_OPERATION_SEMANTICS);
		createEReference(mPropertyEClass, MPROPERTY__CONTAINING_CLASSIFIER);
		createEReference(mPropertyEClass,
				MPROPERTY__CONTAINING_PROPERTIES_CONTAINER);
		createEReference(mPropertyEClass, MPROPERTY__EKEY_FOR_PATH);
		createEAttribute(mPropertyEClass, MPROPERTY__NOT_UNSETTABLE);
		createEAttribute(mPropertyEClass, MPROPERTY__ATTRIBUTE_FOR_EID);
		createEAttribute(mPropertyEClass, MPROPERTY__EDEFAULT_VALUE_LITERAL);
		createEAttribute(mPropertyEClass, MPROPERTY__DO_ACTION);
		createEAttribute(mPropertyEClass, MPROPERTY__BEHAVIOR_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__LAYOUT_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__ARITY_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__SIMPLE_TYPE_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__ANNOTATION_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__MOVE_ACTIONS);
		createEAttribute(mPropertyEClass, MPROPERTY__DERIVED_JSON_SCHEMA);
		createEOperation(mPropertyEClass,
				MPROPERTY___OPPOSITE_DEFINITION$_UPDATE__MPROPERTY);
		createEOperation(mPropertyEClass,
				MPROPERTY___PROPERTY_BEHAVIOR$_UPDATE__PROPERTYBEHAVIOR);
		createEOperation(mPropertyEClass,
				MPROPERTY___TYPE_DEFINITION$_UPDATE__MCLASSIFIER);
		createEOperation(mPropertyEClass,
				MPROPERTY___DO_ACTION$_UPDATE__MPROPERTYACTION);
		createEOperation(mPropertyEClass,
				MPROPERTY___SELECTABLE_TYPE__MCLASSIFIER);
		createEOperation(mPropertyEClass, MPROPERTY___AMBIGUOUS_PROPERTY_NAME);
		createEOperation(mPropertyEClass,
				MPROPERTY___AMBIGUOUS_PROPERTY_SHORT_NAME);
		createEOperation(mPropertyEClass,
				MPROPERTY___DO_ACTION_UPDATE__MPROPERTYACTION);
		createEOperation(mPropertyEClass,
				MPROPERTY___INVOKE_SET_PROPERTY_BEHAVIOUR__PROPERTYBEHAVIOR);
		createEOperation(mPropertyEClass,
				MPROPERTY___INVOKE_SET_DO_ACTION__MPROPERTYACTION);

		mOperationSignatureEClass = createEClass(MOPERATION_SIGNATURE);
		createEReference(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__PARAMETER);
		createEReference(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__OPERATION);
		createEReference(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__CONTAINING_CLASSIFIER);
		createEAttribute(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__ELABEL);
		createEReference(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__INTERNAL_EOPERATION);
		createEAttribute(mOperationSignatureEClass,
				MOPERATION_SIGNATURE__DO_ACTION);
		createEOperation(mOperationSignatureEClass,
				MOPERATION_SIGNATURE___DO_ACTION$_UPDATE__MOPERATIONSIGNATUREACTION);
		createEOperation(mOperationSignatureEClass,
				MOPERATION_SIGNATURE___SAME_SIGNATURE__MOPERATIONSIGNATURE);
		createEOperation(mOperationSignatureEClass,
				MOPERATION_SIGNATURE___SIGNATURE_AS_STRING);

		mTypedEClass = createEClass(MTYPED);
		createEAttribute(mTypedEClass, MTYPED__MULTIPLICITY_AS_STRING);
		createEReference(mTypedEClass, MTYPED__CALCULATED_TYPE);
		createEAttribute(mTypedEClass, MTYPED__CALCULATED_MANDATORY);
		createEAttribute(mTypedEClass, MTYPED__CALCULATED_SINGULAR);
		createEReference(mTypedEClass, MTYPED__CALCULATED_TYPE_PACKAGE);
		createEAttribute(mTypedEClass, MTYPED__VOID_TYPE_ALLOWED);
		createEAttribute(mTypedEClass, MTYPED__CALCULATED_SIMPLE_TYPE);
		createEAttribute(mTypedEClass, MTYPED__MULTIPLICITY_CASE);
		createEOperation(mTypedEClass,
				MTYPED___MULTIPLICITY_CASE$_UPDATE__MULTIPLICITYCASE);
		createEOperation(mTypedEClass,
				MTYPED___TYPE_AS_OCL__MPACKAGE_MCLASSIFIER_SIMPLETYPE_BOOLEAN);

		mExplicitlyTypedEClass = createEClass(MEXPLICITLY_TYPED);
		createEReference(mExplicitlyTypedEClass, MEXPLICITLY_TYPED__TYPE);
		createEReference(mExplicitlyTypedEClass,
				MEXPLICITLY_TYPED__TYPE_PACKAGE);
		createEAttribute(mExplicitlyTypedEClass, MEXPLICITLY_TYPED__MANDATORY);
		createEAttribute(mExplicitlyTypedEClass, MEXPLICITLY_TYPED__SINGULAR);
		createEAttribute(mExplicitlyTypedEClass, MEXPLICITLY_TYPED__ETYPE_NAME);
		createEAttribute(mExplicitlyTypedEClass,
				MEXPLICITLY_TYPED__ETYPE_LABEL);
		createEAttribute(mExplicitlyTypedEClass,
				MEXPLICITLY_TYPED__CORRECTLY_TYPED);

		mVariableEClass = createEClass(MVARIABLE);

		mExplicitlyTypedAndNamedEClass = createEClass(
				MEXPLICITLY_TYPED_AND_NAMED);
		createEAttribute(mExplicitlyTypedAndNamedEClass,
				MEXPLICITLY_TYPED_AND_NAMED__APPEND_TYPE_NAME_TO_NAME);
		createEOperation(mExplicitlyTypedAndNamedEClass,
				MEXPLICITLY_TYPED_AND_NAMED___NAME_FROM_TYPE);
		createEOperation(mExplicitlyTypedAndNamedEClass,
				MEXPLICITLY_TYPED_AND_NAMED___SHORT_NAME_FROM_TYPE);

		mParameterEClass = createEClass(MPARAMETER);
		createEReference(mParameterEClass, MPARAMETER__CONTAINING_SIGNATURE);
		createEAttribute(mParameterEClass, MPARAMETER__ELABEL);
		createEAttribute(mParameterEClass, MPARAMETER__SIGNATURE_AS_STRING);
		createEReference(mParameterEClass, MPARAMETER__INTERNAL_EPARAMETER);
		createEAttribute(mParameterEClass, MPARAMETER__DO_ACTION);
		createEOperation(mParameterEClass,
				MPARAMETER___DO_ACTION$_UPDATE__MPARAMETERACTION);
		createEOperation(mParameterEClass,
				MPARAMETER___AMBIGUOUS_PARAMETER_NAME);
		createEOperation(mParameterEClass,
				MPARAMETER___AMBIGUOUS_PARAMETER_SHORT_NAME);

		mEditorEClass = createEClass(MEDITOR);
		createEOperation(mEditorEClass, MEDITOR___RESET_EDITOR);

		mNamedEditorEClass = createEClass(MNAMED_EDITOR);
		createEAttribute(mNamedEditorEClass, MNAMED_EDITOR__NAME);
		createEAttribute(mNamedEditorEClass, MNAMED_EDITOR__LABEL);
		createEAttribute(mNamedEditorEClass, MNAMED_EDITOR__USER_VISIBLE);
		createEReference(mNamedEditorEClass, MNAMED_EDITOR__EDITOR);

		// Create enums
		mComponentActionEEnum = createEEnum(MCOMPONENT_ACTION);
		topLevelDomainEEnum = createEEnum(TOP_LEVEL_DOMAIN);
		mPackageActionEEnum = createEEnum(MPACKAGE_ACTION);
		simpleTypeEEnum = createEEnum(SIMPLE_TYPE);
		mClassifierActionEEnum = createEEnum(MCLASSIFIER_ACTION);
		orderingStrategyEEnum = createEEnum(ORDERING_STRATEGY);
		mLiteralActionEEnum = createEEnum(MLITERAL_ACTION);
		mPropertiesGroupActionEEnum = createEEnum(MPROPERTIES_GROUP_ACTION);
		mPropertyActionEEnum = createEEnum(MPROPERTY_ACTION);
		propertyBehaviorEEnum = createEEnum(PROPERTY_BEHAVIOR);
		mOperationSignatureActionEEnum = createEEnum(
				MOPERATION_SIGNATURE_ACTION);
		multiplicityCaseEEnum = createEEnum(MULTIPLICITY_CASE);
		classifierKindEEnum = createEEnum(CLASSIFIER_KIND);
		propertyKindEEnum = createEEnum(PROPERTY_KIND);
		mParameterActionEEnum = createEEnum(MPARAMETER_ACTION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI);
		ExpressionsPackage theExpressionsPackage = (ExpressionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ExpressionsPackage.eNS_URI);
		ObjectsPackage theObjectsPackage = (ObjectsPackage) EPackage.Registry.INSTANCE
				.getEPackage(ObjectsPackage.eNS_URI);
		TablesPackage theTablesPackage = (TablesPackage) EPackage.Registry.INSTANCE
				.getEPackage(TablesPackage.eNS_URI);
		UpdatesPackage theUpdatesPackage = (UpdatesPackage) EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI);
		SemanticsPackage theSemanticsPackage = (SemanticsPackage) EPackage.Registry.INSTANCE
				.getEPackage(SemanticsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theAnnotationsPackage);
		getESubpackages().add(theExpressionsPackage);
		getESubpackages().add(theObjectsPackage);
		getESubpackages().add(theTablesPackage);
		getESubpackages().add(theUpdatesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		mRepositoryEClass.getESuperTypes().add(this.getMRepositoryElement());
		mNamedEClass.getESuperTypes().add(this.getMRepositoryElement());
		mComponentEClass.getESuperTypes().add(this.getMNamed());
		mPackageEClass.getESuperTypes().add(this.getMNamed());
		mPackageEClass.getESuperTypes().add(this.getMModelElement());
		mClassifierEClass.getESuperTypes().add(this.getMPropertiesContainer());
		mClassifierEClass.getESuperTypes().add(this.getMHasSimpleType());
		mLiteralEClass.getESuperTypes().add(this.getMNamed());
		mLiteralEClass.getESuperTypes().add(this.getMModelElement());
		mPropertiesContainerEClass.getESuperTypes().add(this.getMNamed());
		mPropertiesContainerEClass.getESuperTypes()
				.add(this.getMModelElement());
		mPropertiesGroupEClass.getESuperTypes()
				.add(this.getMPropertiesContainer());
		mPropertiesGroupEClass.getESuperTypes()
				.add(this.getMRepositoryElement());
		mPropertyEClass.getESuperTypes()
				.add(this.getMExplicitlyTypedAndNamed());
		mPropertyEClass.getESuperTypes().add(this.getMModelElement());
		mOperationSignatureEClass.getESuperTypes().add(this.getMTyped());
		mOperationSignatureEClass.getESuperTypes().add(this.getMModelElement());
		mTypedEClass.getESuperTypes().add(this.getMRepositoryElement());
		mExplicitlyTypedEClass.getESuperTypes().add(this.getMTyped());
		mExplicitlyTypedEClass.getESuperTypes().add(this.getMHasSimpleType());
		mVariableEClass.getESuperTypes().add(this.getMNamed());
		mVariableEClass.getESuperTypes().add(this.getMTyped());
		mExplicitlyTypedAndNamedEClass.getESuperTypes().add(this.getMNamed());
		mExplicitlyTypedAndNamedEClass.getESuperTypes()
				.add(this.getMExplicitlyTyped());
		mParameterEClass.getESuperTypes()
				.add(this.getMExplicitlyTypedAndNamed());
		mParameterEClass.getESuperTypes().add(this.getMVariable());
		mParameterEClass.getESuperTypes().add(this.getMModelElement());
		mParameterEClass.getESuperTypes()
				.add(theClassifiersPackage.getAClassifier());

		// Initialize classes, features, and operations; add parameters
		initEClass(mRepositoryElementEClass, MRepositoryElement.class,
				"MRepositoryElement", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMRepositoryElement_KindLabel(),
				ecorePackage.getEString(), "kindLabel", "", 0, 1,
				MRepositoryElement.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMRepositoryElement_RenderedKindLabel(),
				ecorePackage.getEString(), "renderedKindLabel", null, 1, 1,
				MRepositoryElement.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMRepositoryElement_IndentLevel(),
				ecorePackage.getEIntegerObject(), "indentLevel", null, 1, 1,
				MRepositoryElement.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMRepositoryElement_Description(),
				ecorePackage.getEString(), "description", null, 0, 1,
				MRepositoryElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMRepositoryElement_AsText(),
				ecorePackage.getEString(), "asText", null, 0, 1,
				MRepositoryElement.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEOperation(getMRepositoryElement__IndentationSpaces(),
				ecorePackage.getEString(), "indentationSpaces", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		EOperation op = initEOperation(
				getMRepositoryElement__IndentationSpaces__Integer(),
				ecorePackage.getEString(), "indentationSpaces", 1, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMRepositoryElement__NewLineString(),
				ecorePackage.getEString(), "newLineString", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mRepositoryEClass, MRepository.class, "MRepository",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMRepository_Component(), this.getMComponent(), null,
				"component", null, 0, -1, MRepository.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMRepository_Component().getEKeys().add(this.getMNamed_Name());
		initEReference(getMRepository_Core(), this.getMComponent(), null,
				"core", null, 0, 1, MRepository.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMRepository_ReferencedEPackage(),
				ecorePackage.getEPackage(), null, "referencedEPackage", null, 0,
				-1, MRepository.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMRepository_AvoidRegenerationOfEditorConfiguration(),
				ecorePackage.getEBooleanObject(),
				"avoidRegenerationOfEditorConfiguration", null, 0, 1,
				MRepository.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mNamedEClass, MNamed.class, "MNamed", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMNamed_SpecialEName(), ecorePackage.getEString(),
				"specialEName", null, 0, 1, MNamed.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_Name(), ecorePackage.getEString(), "name",
				null, 0, 1, MNamed.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMNamed_ShortName(), ecorePackage.getEString(),
				"shortName", null, 0, 1, MNamed.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_EName(), ecorePackage.getEString(), "eName",
				null, 0, 1, MNamed.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMNamed_FullLabel(), ecorePackage.getEString(),
				"fullLabel", null, 0, 1, MNamed.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_LocalStructuralName(),
				ecorePackage.getEString(), "localStructuralName", null, 0, 1,
				MNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_CalculatedName(), ecorePackage.getEString(),
				"calculatedName", null, 0, 1, MNamed.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_CalculatedShortName(),
				ecorePackage.getEString(), "calculatedShortName", null, 0, 1,
				MNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_CorrectName(),
				ecorePackage.getEBooleanObject(), "correctName", null, 0, 1,
				MNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamed_CorrectShortName(),
				ecorePackage.getEBooleanObject(), "correctShortName", null, 0,
				1, MNamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMNamed__SameName__MNamed(),
				ecorePackage.getEBooleanObject(), "sameName", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getMNamed(), "n", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMNamed__SameShortName__MNamed(),
				ecorePackage.getEBooleanObject(), "sameShortName", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMNamed(), "n", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMNamed__SameString__String_String(),
				ecorePackage.getEBooleanObject(), "sameString", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s1", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s2", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMNamed__StringEmpty__String(),
				ecorePackage.getEBooleanObject(), "stringEmpty", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mComponentEClass, MComponent.class, "MComponent",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMComponent_OwnedPackage(), this.getMPackage(), null,
				"ownedPackage", null, 0, -1, MComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMComponent_OwnedPackage().getEKeys().add(this.getMNamed_Name());
		initEReference(getMComponent_ResourceFolder(),
				theObjectsPackage.getMResourceFolder(), null, "resourceFolder",
				null, 0, -1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMComponent_ResourceFolder().getEKeys().add(this.getMNamed_Name());
		initEReference(getMComponent_Semantics(),
				theSemanticsPackage.getXSemantics(), null, "semantics", null, 0,
				1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getMComponent_GeneratedEPackage(),
				ecorePackage.getEPackage(), null, "generatedEPackage", null, 0,
				-1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMComponent_NamedEditor(), this.getMNamedEditor(),
				null, "namedEditor", null, 0, -1, MComponent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMComponent_NamePrefix(), ecorePackage.getEString(),
				"namePrefix", null, 0, 1, MComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DerivedNamePrefix(),
				ecorePackage.getEString(), "derivedNamePrefix", null, 0, 1,
				MComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_NamePrefixForFeatures(),
				ecorePackage.getEBooleanObject(), "namePrefixForFeatures", null,
				0, 1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMComponent_ContainingRepository(),
				this.getMRepository(), null, "containingRepository", null, 0, 1,
				MComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_RepresentsCoreComponent(),
				ecorePackage.getEBooleanObject(), "representsCoreComponent",
				null, 0, 1, MComponent.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMComponent_RootPackage(), this.getMPackage(), null,
				"rootPackage", null, 0, 1, MComponent.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DomainType(), ecorePackage.getEString(),
				"domainType", null, 1, 1, MComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DomainName(), ecorePackage.getEString(),
				"domainName", null, 1, 1, MComponent.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DerivedDomainType(),
				ecorePackage.getEString(), "derivedDomainType", null, 0, 1,
				MComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DerivedDomainName(),
				ecorePackage.getEString(), "derivedDomainName", null, 0, 1,
				MComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DerivedBundleName(),
				ecorePackage.getEString(), "derivedBundleName", null, 1, 1,
				MComponent.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_DerivedURI(), ecorePackage.getEString(),
				"derivedURI", null, 1, 1, MComponent.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_SpecialBundleName(),
				ecorePackage.getEString(), "specialBundleName", null, 0, 1,
				MComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMComponent_PreloadedComponent(), this.getMComponent(),
				null, "preloadedComponent", null, 0, -1, MComponent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMComponent_MainNamedEditor(), this.getMNamedEditor(),
				null, "mainNamedEditor", null, 0, 1, MComponent.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMComponent_MainEditor(), this.getMEditor(), null,
				"mainEditor", null, 0, 1, MComponent.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_HideAdvancedProperties(),
				ecorePackage.getEBooleanObject(), "hideAdvancedProperties",
				null, 0, 1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMComponent_AllGeneralAnnotations(),
				theAnnotationsPackage.getMGeneralAnnotation(), null,
				"allGeneralAnnotations", null, 0, -1, MComponent.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMComponent_AbstractComponent(),
				ecorePackage.getEBooleanObject(), "abstractComponent", null, 0,
				1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_CountImplementations(),
				ecorePackage.getEIntegerObject(), "countImplementations", null,
				0, 1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMComponent_DisableDefaultContainment(),
				ecorePackage.getEBooleanObject(), "disableDefaultContainment",
				null, 0, 1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMComponent_DoAction(), this.getMComponentAction(),
				"doAction", null, 0, 1, MComponent.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_AvoidRegenerationOfEditorConfiguration(),
				ecorePackage.getEBooleanObject(),
				"avoidRegenerationOfEditorConfiguration", null, 0, 1,
				MComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMComponent_UseLegacyEditorconfig(),
				ecorePackage.getEBooleanObject(), "useLegacyEditorconfig", null,
				0, 1, MComponent.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(getMComponent__DoAction$Update__MComponentAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMComponentAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMComponent__DoActionUpdate__MComponentAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMComponentAction(), "mComponentAction", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mModelElementEClass, MModelElement.class, "MModelElement",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMModelElement_GeneralAnnotation(),
				theAnnotationsPackage.getMGeneralAnnotation(), null,
				"generalAnnotation", null, 0, -1, MModelElement.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(mPackageEClass, MPackage.class, "MPackage", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMPackage_Classifier(), this.getMClassifier(), null,
				"classifier", null, 0, -1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMPackage_Classifier().getEKeys().add(this.getMNamed_Name());
		initEReference(getMPackage_SubPackage(), this.getMPackage(), null,
				"subPackage", null, 0, -1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMPackage_SubPackage().getEKeys().add(this.getMNamed_Name());
		initEReference(getMPackage_PackageAnnotations(),
				theAnnotationsPackage.getMPackageAnnotations(), null,
				"packageAnnotations", null, 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_UsePackageContentOnlyWithExplicitFilter(),
				ecorePackage.getEBooleanObject(),
				"usePackageContentOnlyWithExplicitFilter", "false", 0, 1,
				MPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_ResourceRootClass(), this.getMClassifier(),
				null, "resourceRootClass", null, 0, 1, MPackage.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPackage_SpecialNsURI(), ecorePackage.getEString(),
				"specialNsURI", null, 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_SpecialNsPrefix(), ecorePackage.getEString(),
				"specialNsPrefix", null, 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_SpecialFileName(), ecorePackage.getEString(),
				"specialFileName", null, 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_UseUUID(), ecorePackage.getEBooleanObject(),
				"useUUID", "true", 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_UuidAttributeName(),
				ecorePackage.getEString(), "uuidAttributeName", null, 0, 1,
				MPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_Parent(), this.getMPackage(), null, "parent",
				null, 0, 1, MPackage.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_ContainingPackage(), this.getMPackage(),
				null, "containingPackage", null, 0, 1, MPackage.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPackage_ContainingComponent(), this.getMComponent(),
				null, "containingComponent", null, 0, 1, MPackage.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPackage_RepresentsCorePackage(),
				ecorePackage.getEBooleanObject(), "representsCorePackage", null,
				0, 1, MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_AllSubpackages(), this.getMPackage(), null,
				"allSubpackages", null, 0, -1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_QualifiedName(), ecorePackage.getEString(),
				"qualifiedName", null, 0, 1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_IsRootPackage(),
				ecorePackage.getEBooleanObject(), "isRootPackage", null, 0, 1,
				MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_InternalEPackage(),
				ecorePackage.getEPackage(), null, "internalEPackage", null, 0,
				1, MPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedNsURI(), ecorePackage.getEString(),
				"derivedNsURI", null, 0, 1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedStandardNsURI(),
				ecorePackage.getEString(), "derivedStandardNsURI", null, 1, 1,
				MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedNsPrefix(), ecorePackage.getEString(),
				"derivedNsPrefix", null, 1, 1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedJavaPackageName(),
				ecorePackage.getEString(), "derivedJavaPackageName", null, 0, 1,
				MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DoAction(), this.getMPackageAction(),
				"doAction", null, 0, 1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_NamePrefix(), ecorePackage.getEString(),
				"namePrefix", null, 0, 1, MPackage.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedNamePrefix(),
				ecorePackage.getEString(), "derivedNamePrefix", null, 0, 1,
				MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPackage_NamePrefixScope(), this.getMPackage(), null,
				"namePrefixScope", null, 0, 1, MPackage.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_DerivedJsonSchema(),
				ecorePackage.getEString(), "derivedJsonSchema", null, 0, 1,
				MPackage.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_GenerateJsonSchema(),
				ecorePackage.getEBooleanObject(), "generateJsonSchema", null, 0,
				1, MPackage.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPackage_GeneratedJsonSchema(),
				ecorePackage.getEString(), "generatedJsonSchema", null, 0, 1,
				MPackage.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMPackage__GenerateJsonSchema$update01Object__Boolean(),
				this.getMPackage(), "generateJsonSchema$update01Object", 0, -1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMPackage__GenerateJsonSchema$update01Value__Boolean_MPackage(),
				ecorePackage.getEString(), "generateJsonSchema$update01Value",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPackage(), "obj", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMPackage__DoAction$Update__MPackageAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPackageAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMPackage__GenerateJsonSchema$Update__Boolean(),
				theSemanticsPackage.getXUpdate(), "generateJsonSchema$Update",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMPackage__DoActionUpdate__MPackageAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPackageAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mHasSimpleTypeEClass, MHasSimpleType.class, "MHasSimpleType",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMHasSimpleType_SimpleTypeString(),
				ecorePackage.getEString(), "simpleTypeString", null, 0, 1,
				MHasSimpleType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMHasSimpleType_HasSimpleDataType(),
				ecorePackage.getEBooleanObject(), "hasSimpleDataType", null, 0,
				1, MHasSimpleType.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMHasSimpleType_HasSimpleModelingType(),
				ecorePackage.getEBooleanObject(), "hasSimpleModelingType", null,
				0, 1, MHasSimpleType.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMHasSimpleType_SimpleTypeIsCorrect(),
				ecorePackage.getEBooleanObject(), "simpleTypeIsCorrect", null,
				0, 1, MHasSimpleType.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMHasSimpleType_SimpleType(), this.getSimpleType(),
				"simpleType", null, 0, 1, MHasSimpleType.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMHasSimpleType__SimpleTypeAsString__SimpleType(),
				ecorePackage.getEString(), "simpleTypeAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSimpleType(), "simpleType", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mClassifierEClass, MClassifier.class, "MClassifier",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMClassifier_AbstractClass(),
				ecorePackage.getEBooleanObject(), "abstractClass", "false", 0,
				1, MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_EnforcedClass(),
				ecorePackage.getEBooleanObject(), "enforcedClass", "false", 0,
				1, MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_SuperTypePackage(), this.getMPackage(),
				null, "superTypePackage", null, 0, 1, MClassifier.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_SuperType(), this.getMClassifier(), null,
				"superType", null, 0, -1, MClassifier.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_Instance(),
				theObjectsPackage.getMObject(), null, "instance", null, 0, -1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_Kind(), this.getClassifierKind(), "kind",
				null, 0, 1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_InternalEClassifier(),
				ecorePackage.getEClassifier(), null, "internalEClassifier",
				null, 0, 1, MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_ContainingPackage(), this.getMPackage(),
				null, "containingPackage", null, 0, 1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_RepresentsCoreType(),
				ecorePackage.getEBooleanObject(), "representsCoreType", null, 1,
				1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_AllPropertyGroups(),
				this.getMPropertiesGroup(), null, "allPropertyGroups", null, 0,
				-1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_Literal(), this.getMLiteral(), null,
				"literal", null, 0, -1, MClassifier.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMClassifier_Literal().getEKeys().add(this.getMNamed_Name());
		initEAttribute(getMClassifier_EInterface(),
				ecorePackage.getEBooleanObject(), "eInterface", "false", 0, 1,
				MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_OperationAsIdPropertyError(),
				ecorePackage.getEBooleanObject(), "operationAsIdPropertyError",
				null, 0, 1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_IsClassByStructure(),
				ecorePackage.getEBooleanObject(), "isClassByStructure", null, 0,
				1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_ToggleSuperType(), this.getMClassifier(),
				null, "toggleSuperType", null, 0, 1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_OrderingStrategy(),
				this.getOrderingStrategy(), "orderingStrategy", null, 1, 1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_TestAllProperties(), this.getMProperty(),
				null, "testAllProperties", null, 0, -1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_IdProperty(), this.getMProperty(), null,
				"idProperty", null, 0, -1, MClassifier.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_DoAction(), this.getMClassifierAction(),
				"doAction", null, 0, 1, MClassifier.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_ResolveContainerAction(),
				ecorePackage.getEBooleanObject(), "resolveContainerAction",
				null, 0, 1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_AbstractDoAction(),
				ecorePackage.getEBooleanObject(), "abstractDoAction", null, 0,
				1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_ObjectReferences(),
				theObjectsPackage.getMObject(), null, "objectReferences", null,
				0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_NearestInstance(),
				theObjectsPackage.getMObject(), null, "nearestInstance", null,
				0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_StrictNearestInstance(),
				theObjectsPackage.getMObject(), null, "strictNearestInstance",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_StrictInstance(),
				theObjectsPackage.getMObject(), null, "strictInstance", null, 0,
				-1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_IntelligentNearestInstance(),
				theObjectsPackage.getMObject(), null,
				"intelligentNearestInstance", null, 0, -1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_ObjectUnreferenced(),
				theObjectsPackage.getMObject(), null, "objectUnreferenced",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_OutstandingToCopyObjects(),
				theObjectsPackage.getMObject(), null,
				"outstandingToCopyObjects", null, 0, -1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_PropertyInstances(),
				theObjectsPackage.getMPropertyInstance(), null,
				"propertyInstances", null, 0, -1, MClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMClassifier_ObjectReferencePropertyInstances(),
				theObjectsPackage.getMPropertyInstance(), null,
				"objectReferencePropertyInstances", null, 0, -1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_IntelligentInstance(),
				theObjectsPackage.getMObject(), null, "intelligentInstance",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_ContainmentHierarchy(),
				this.getMClassifier(), null, "containmentHierarchy", null, 0,
				-1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_StrictAllClassesContainedIn(),
				this.getMClassifier(), null, "strictAllClassesContainedIn",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_StrictContainmentHierarchy(),
				this.getMClassifier(), null, "strictContainmentHierarchy", null,
				0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_IntelligentAllClassesContainedIn(),
				this.getMClassifier(), null, "intelligentAllClassesContainedIn",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_IntelligentContainmentHierarchy(),
				this.getMClassifier(), null, "intelligentContainmentHierarchy",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_InstantiationHierarchy(),
				this.getMClassifier(), null, "instantiationHierarchy", null, 0,
				-1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_InstantiationPropertyHierarchy(),
				this.getMProperty(), null, "instantiationPropertyHierarchy",
				null, 0, -1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(
				getMClassifier_IntelligentInstantiationPropertyHierarchy(),
				this.getMProperty(), null,
				"intelligentInstantiationPropertyHierarchy", null, 0, -1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMClassifier_Classescontainedin(),
				this.getMClassifier(), null, "classescontainedin", null, 0, -1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_CanApplyDefaultContainment(),
				ecorePackage.getEBooleanObject(), "canApplyDefaultContainment",
				null, 0, 1, MClassifier.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMClassifier_PropertiesAsText(),
				ecorePackage.getEString(), "propertiesAsText", null, 0, 1,
				MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_SemanticsAsText(),
				ecorePackage.getEString(), "semanticsAsText", null, 0, 1,
				MClassifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMClassifier_DerivedJsonSchema(),
				ecorePackage.getEString(), "derivedJsonSchema", null, 0, 1,
				MClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMClassifier__ToggleSuperType$Update__MClassifier(),
				theSemanticsPackage.getXUpdate(), "toggleSuperType$Update", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMClassifier__OrderingStrategy$Update__OrderingStrategy(),
				theSemanticsPackage.getXUpdate(), "orderingStrategy$Update", 1,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getOrderingStrategy(), "trg", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMClassifier__DoAction$Update__MClassifierAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifierAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AllLiterals(), this.getMLiteral(),
				"allLiterals", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMClassifier__SelectableClassifier__EList_MPackage(),
				ecorePackage.getEBooleanObject(), "selectableClassifier", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "currentClassifier", 0, -1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPackage(), "filterPackage", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AllDirectSubTypes(),
				this.getMClassifier(), "allDirectSubTypes", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AllSubTypes(), this.getMClassifier(),
				"allSubTypes", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__SuperTypesAsString(),
				ecorePackage.getEString(), "superTypesAsString", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllSuperTypes(), this.getMClassifier(),
				"allSuperTypes", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllProperties(), this.getMProperty(),
				"allProperties", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllSuperProperties(),
				this.getMProperty(), "allSuperProperties", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AllFeatures(), this.getMProperty(),
				"allFeatures", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllNonContainmentFeatures(),
				this.getMProperty(), "allNonContainmentFeatures", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllStoredNonContainmentFeatures(),
				this.getMProperty(), "allStoredNonContainmentFeatures", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllFeaturesWithStorage(),
				this.getMProperty(), "allFeaturesWithStorage", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AllContainmentReferences(),
				this.getMProperty(), "allContainmentReferences", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllOperationSignatures(),
				this.getMOperationSignature(), "allOperationSignatures", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllClassesContainedIn(),
				this.getMClassifier(), "allClassesContainedIn", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__DefaultPropertyValue(),
				this.getMProperty(), "defaultPropertyValue", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__DefaultPropertyDescription(),
				ecorePackage.getEString(), "defaultPropertyDescription", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllIdProperties(), this.getMProperty(),
				"allIdProperties", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMClassifier__AllSuperIdProperties(),
				this.getMProperty(), "allSuperIdProperties", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMClassifier__DoActionUpdate__MClassifierAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifierAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMClassifier__InvokeSetDoAction__MClassifierAction(),
				ecorePackage.getEJavaObject(), "invokeSetDoAction", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifierAction(), "mClassifierAction", 0,
				1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMClassifier__InvokeToggleSuperType__MClassifier(),
				ecorePackage.getEJavaObject(), "invokeToggleSuperType", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "mClassifier", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMClassifier__DoSuperTypeUpdate__MClassifier(),
				theSemanticsPackage.getXUpdate(), "doSuperTypeUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMClassifier__AmbiguousClassifierName(),
				ecorePackage.getEBooleanObject(), "ambiguousClassifierName", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEClass(mLiteralEClass, MLiteral.class, "MLiteral", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMLiteral_ConstantLabel(), ecorePackage.getEString(),
				"constantLabel", null, 0, 1, MLiteral.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMLiteral_AsString(), ecorePackage.getEString(),
				"asString", null, 0, 1, MLiteral.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMLiteral_QualifiedName(), ecorePackage.getEString(),
				"qualifiedName", null, 0, 1, MLiteral.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteral_ContainingEnumeration(),
				this.getMClassifier(), null, "containingEnumeration", null, 0,
				1, MLiteral.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMLiteral_InternalEEnumLiteral(),
				ecorePackage.getEEnumLiteral(), null, "internalEEnumLiteral",
				null, 0, 1, MLiteral.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mPropertiesContainerEClass, MPropertiesContainer.class,
				"MPropertiesContainer", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMPropertiesContainer_Property(), this.getMProperty(),
				null, "property", null, 0, -1, MPropertiesContainer.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertiesContainer_Annotations(),
				theAnnotationsPackage.getMClassifierAnnotations(), null,
				"annotations", null, 0, 1, MPropertiesContainer.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMPropertiesContainer_PropertyGroup(),
				this.getMPropertiesGroup(), null, "propertyGroup", null, 0, -1,
				MPropertiesContainer.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		getMPropertiesContainer_PropertyGroup().getEKeys()
				.add(this.getMNamed_Name());
		initEReference(getMPropertiesContainer_AllConstraintAnnotations(),
				theAnnotationsPackage.getMInvariantConstraint(), null,
				"allConstraintAnnotations", null, 0, -1,
				MPropertiesContainer.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertiesContainer_ContainingClassifier(),
				this.getMClassifier(), null, "containingClassifier", null, 0, 1,
				MPropertiesContainer.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMPropertiesContainer_OwnedProperty(),
				this.getMProperty(), null, "ownedProperty", null, 0, -1,
				MPropertiesContainer.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMPropertiesContainer_NrOfPropertiesAndSignatures(),
				ecorePackage.getEIntegerObject(), "nrOfPropertiesAndSignatures",
				null, 0, 1, MPropertiesContainer.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEOperation(getMPropertiesContainer__AllPropertyAnnotations(),
				theAnnotationsPackage.getMPropertyAnnotations(),
				"allPropertyAnnotations", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMPropertiesContainer__AllOperationAnnotations(),
				theAnnotationsPackage.getMOperationAnnotations(),
				"allOperationAnnotations", 0, -1, IS_UNIQUE, IS_ORDERED);

		initEClass(mPropertiesGroupEClass, MPropertiesGroup.class,
				"MPropertiesGroup", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMPropertiesGroup_Order(),
				ecorePackage.getEIntegerObject(), "order", "0", 0, 1,
				MPropertiesGroup.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMPropertiesGroup_DoAction(),
				this.getMPropertiesGroupAction(), "doAction", null, 0, 1,
				MPropertiesGroup.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(
				getMPropertiesGroup__DoAction$Update__MPropertiesGroupAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertiesGroupAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertiesGroupAction(),
				"mPropertiesGroupAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction(),
				ecorePackage.getEJavaObject(), "invokeSetDoAction", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertiesGroupAction(),
				"mPropertiesGroupAction", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mPropertyEClass, MProperty.class, "MProperty", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMProperty_OperationSignature(),
				this.getMOperationSignature(), null, "operationSignature", null,
				0, -1, MProperty.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMProperty_OppositeDefinition(), this.getMProperty(),
				null, "oppositeDefinition", null, 0, 1, MProperty.class,
				IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_Opposite(), this.getMProperty(), null,
				"opposite", null, 0, 1, MProperty.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_Containment(),
				ecorePackage.getEBooleanObject(), "containment", "false", 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_HasStorage(),
				ecorePackage.getEBooleanObject(), "hasStorage", "false", 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_Changeable(),
				ecorePackage.getEBooleanObject(), "changeable", "false", 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_Persisted(),
				ecorePackage.getEBooleanObject(), "persisted", "false", 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_PropertyBehavior(),
				this.getPropertyBehavior(), "propertyBehavior", null, 1, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_IsOperation(),
				ecorePackage.getEBooleanObject(), "isOperation", null, 0, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_IsAttribute(),
				ecorePackage.getEBooleanObject(), "isAttribute", null, 0, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_IsReference(),
				ecorePackage.getEBooleanObject(), "isReference", null, 0, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_Kind(), this.getPropertyKind(), "kind",
				null, 0, 1, MProperty.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_TypeDefinition(), this.getMClassifier(),
				null, "typeDefinition", null, 0, 1, MProperty.class,
				IS_TRANSIENT, IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMProperty_UseCoreTypes(),
				ecorePackage.getEBooleanObject(), "useCoreTypes", null, 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_ELabel(), ecorePackage.getEString(),
				"eLabel", null, 0, 1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_ENameForELabel(), ecorePackage.getEString(),
				"eNameForELabel", null, 0, 1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_OrderWithinGroup(),
				ecorePackage.getEIntegerObject(), "orderWithinGroup", null, 0,
				1, MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_Id(), ecorePackage.getEString(), "id", null,
				0, 1, MProperty.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_InternalEStructuralFeature(),
				ecorePackage.getEStructuralFeature(), null,
				"internalEStructuralFeature", null, 0, 1, MProperty.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_DirectSemantics(),
				theAnnotationsPackage.getMPropertyAnnotations(), null,
				"directSemantics", null, 0, 1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMProperty_SemanticsSpecialization(),
				theAnnotationsPackage.getMPropertyAnnotations(), null,
				"semanticsSpecialization", null, 0, -1, MProperty.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_AllSignatures(),
				this.getMOperationSignature(), null, "allSignatures", null, 0,
				-1, MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMProperty_DirectOperationSemantics(),
				theAnnotationsPackage.getMOperationAnnotations(), null,
				"directOperationSemantics", null, 0, 1, MProperty.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_ContainingClassifier(),
				this.getMClassifier(), null, "containingClassifier", null, 0, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getMProperty_ContainingPropertiesContainer(),
				this.getMPropertiesContainer(), null,
				"containingPropertiesContainer", null, 0, 1, MProperty.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMProperty_EKeyForPath(), this.getMProperty(), null,
				"eKeyForPath", null, 0, -1, MProperty.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_NotUnsettable(),
				ecorePackage.getEBooleanObject(), "notUnsettable", "false", 0,
				1, MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_AttributeForEId(),
				ecorePackage.getEBooleanObject(), "attributeForEId", "false", 0,
				1, MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_EDefaultValueLiteral(),
				ecorePackage.getEString(), "eDefaultValueLiteral", null, 0, 1,
				MProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_DoAction(), this.getMPropertyAction(),
				"doAction", null, 0, 1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_BehaviorActions(),
				this.getMPropertyAction(), "behaviorActions", null, 0, -1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_LayoutActions(), this.getMPropertyAction(),
				"layoutActions", null, 0, -1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_ArityActions(), this.getMPropertyAction(),
				"arityActions", null, 0, -1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_SimpleTypeActions(),
				this.getMPropertyAction(), "simpleTypeActions", null, 0, -1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_AnnotationActions(),
				this.getMPropertyAction(), "annotationActions", null, 0, -1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_MoveActions(), this.getMPropertyAction(),
				"moveActions", null, 0, -1, MProperty.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMProperty_DerivedJsonSchema(),
				ecorePackage.getEString(), "derivedJsonSchema", null, 0, 1,
				MProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMProperty__OppositeDefinition$Update__MProperty(),
				theSemanticsPackage.getXUpdate(), "oppositeDefinition$Update",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMProperty(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMProperty__PropertyBehavior$Update__PropertyBehavior(),
				theSemanticsPackage.getXUpdate(), "propertyBehavior$Update", 1,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPropertyBehavior(), "trg", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMProperty__TypeDefinition$Update__MClassifier(),
				theSemanticsPackage.getXUpdate(), "typeDefinition$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMProperty__DoAction$Update__MPropertyAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertyAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(getMProperty__SelectableType__MClassifier(),
				ecorePackage.getEBooleanObject(), "selectableType", 1, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "mClassifier", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMProperty__AmbiguousPropertyName(),
				ecorePackage.getEBooleanObject(), "ambiguousPropertyName", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getMProperty__AmbiguousPropertyShortName(),
				ecorePackage.getEBooleanObject(), "ambiguousPropertyShortName",
				0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMProperty__DoActionUpdate__MPropertyAction(),
				theSemanticsPackage.getXUpdate(), "doActionUpdate", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertyAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior(),
				ecorePackage.getEString(), "invokeSetPropertyBehaviour", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPropertyBehavior(), "propertyBehavior", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getMProperty__InvokeSetDoAction__MPropertyAction(),
				ecorePackage.getEJavaObject(), "invokeSetDoAction", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMPropertyAction(), "mPropertyAction", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mOperationSignatureEClass, MOperationSignature.class,
				"MOperationSignature", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMOperationSignature_Parameter(), this.getMParameter(),
				null, "parameter", null, 0, -1, MOperationSignature.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getMOperationSignature_Operation(), this.getMProperty(),
				null, "operation", null, 1, 1, MOperationSignature.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMOperationSignature_ContainingClassifier(),
				this.getMClassifier(), null, "containingClassifier", null, 0, 1,
				MOperationSignature.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMOperationSignature_ELabel(),
				ecorePackage.getEString(), "eLabel", null, 0, 1,
				MOperationSignature.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getMOperationSignature_InternalEOperation(),
				ecorePackage.getEOperation(), null, "internalEOperation", null,
				0, 1, MOperationSignature.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMOperationSignature_DoAction(),
				this.getMOperationSignatureAction(), "doAction", null, 0, 1,
				MOperationSignature.class, IS_TRANSIENT, IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(
				getMOperationSignature__DoAction$Update__MOperationSignatureAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMOperationSignatureAction(), "trg", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		op = initEOperation(
				getMOperationSignature__SameSignature__MOperationSignature(),
				ecorePackage.getEBooleanObject(), "sameSignature", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMOperationSignature(), "s", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMOperationSignature__SignatureAsString(),
				ecorePackage.getEString(), "signatureAsString", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mTypedEClass, MTyped.class, "MTyped", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMTyped_MultiplicityAsString(),
				ecorePackage.getEString(), "multiplicityAsString", null, 0, 1,
				MTyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMTyped_CalculatedType(), this.getMClassifier(), null,
				"calculatedType", null, 0, 1, MTyped.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTyped_CalculatedMandatory(),
				ecorePackage.getEBooleanObject(), "calculatedMandatory", null,
				0, 1, MTyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTyped_CalculatedSingular(),
				ecorePackage.getEBooleanObject(), "calculatedSingular", null, 0,
				1, MTyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMTyped_CalculatedTypePackage(), this.getMPackage(),
				null, "calculatedTypePackage", null, 0, 1, MTyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMTyped_VoidTypeAllowed(),
				ecorePackage.getEBooleanObject(), "voidTypeAllowed", null, 1, 1,
				MTyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTyped_CalculatedSimpleType(), this.getSimpleType(),
				"calculatedSimpleType", null, 0, 1, MTyped.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMTyped_MultiplicityCase(), this.getMultiplicityCase(),
				"multiplicityCase", null, 1, 1, MTyped.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(
				getMTyped__MultiplicityCase$Update__MultiplicityCase(),
				theSemanticsPackage.getXUpdate(), "multiplicityCase$Update", 1,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMultiplicityCase(), "trg", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = initEOperation(
				getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean(),
				ecorePackage.getEString(), "typeAsOcl", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getMPackage(), "oclPackage", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getMClassifier(), "mClassifier", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, this.getSimpleType(), "simpleType", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "singular", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mExplicitlyTypedEClass, MExplicitlyTyped.class,
				"MExplicitlyTyped", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMExplicitlyTyped_Type(), this.getMClassifier(), null,
				"type", null, 0, 1, MExplicitlyTyped.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMExplicitlyTyped_TypePackage(), this.getMPackage(),
				null, "typePackage", null, 0, 1, MExplicitlyTyped.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExplicitlyTyped_Mandatory(),
				ecorePackage.getEBooleanObject(), "mandatory", "false", 0, 1,
				MExplicitlyTyped.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExplicitlyTyped_Singular(),
				ecorePackage.getEBooleanObject(), "singular", "false", 0, 1,
				MExplicitlyTyped.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExplicitlyTyped_ETypeName(),
				ecorePackage.getEString(), "eTypeName", null, 0, 1,
				MExplicitlyTyped.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExplicitlyTyped_ETypeLabel(),
				ecorePackage.getEString(), "eTypeLabel", null, 0, 1,
				MExplicitlyTyped.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getMExplicitlyTyped_CorrectlyTyped(),
				ecorePackage.getEBooleanObject(), "correctlyTyped", null, 0, 1,
				MExplicitlyTyped.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);

		initEClass(mVariableEClass, MVariable.class, "MVariable", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(mExplicitlyTypedAndNamedEClass,
				MExplicitlyTypedAndNamed.class, "MExplicitlyTypedAndNamed",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMExplicitlyTypedAndNamed_AppendTypeNameToName(),
				ecorePackage.getEBooleanObject(), "appendTypeNameToName", null,
				0, 1, MExplicitlyTypedAndNamed.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEOperation(getMExplicitlyTypedAndNamed__NameFromType(),
				ecorePackage.getEString(), "nameFromType", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMExplicitlyTypedAndNamed__ShortNameFromType(),
				ecorePackage.getEString(), "shortNameFromType", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(mParameterEClass, MParameter.class, "MParameter",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMParameter_ContainingSignature(),
				this.getMOperationSignature(), null, "containingSignature",
				null, 0, 1, MParameter.class, IS_TRANSIENT, IS_VOLATILE,
				!IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getMParameter_ELabel(), ecorePackage.getEString(),
				"eLabel", null, 0, 1, MParameter.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getMParameter_SignatureAsString(),
				ecorePackage.getEString(), "signatureAsString", null, 0, 1,
				MParameter.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getMParameter_InternalEParameter(),
				ecorePackage.getEParameter(), null, "internalEParameter", null,
				0, 1, MParameter.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE,
				IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMParameter_DoAction(), this.getMParameterAction(),
				"doAction", null, 0, 1, MParameter.class, IS_TRANSIENT,
				IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMParameter__DoAction$Update__MParameterAction(),
				theSemanticsPackage.getXUpdate(), "doAction$Update", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMParameterAction(), "trg", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getMParameter__AmbiguousParameterName(),
				ecorePackage.getEBooleanObject(), "ambiguousParameterName", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getMParameter__AmbiguousParameterShortName(),
				ecorePackage.getEBooleanObject(), "ambiguousParameterShortName",
				0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mEditorEClass, MEditor.class, "MEditor", IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getMEditor__ResetEditor(),
				ecorePackage.getEBooleanObject(), "resetEditor", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(mNamedEditorEClass, MNamedEditor.class, "MNamedEditor",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMNamedEditor_Name(), ecorePackage.getEString(),
				"name", null, 0, 1, MNamedEditor.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamedEditor_Label(), ecorePackage.getEString(),
				"label", null, 0, 1, MNamedEditor.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getMNamedEditor_UserVisible(),
				ecorePackage.getEBooleanObject(), "userVisible", null, 0, 1,
				MNamedEditor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMNamedEditor_Editor(), this.getMEditor(), null,
				"editor", null, 1, 1, MNamedEditor.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(mComponentActionEEnum, MComponentAction.class,
				"MComponentAction");
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.DO);
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.OBJECT);
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.CLASS);
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.ABSTRACT);
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.IMPLEMENT);
		addEEnumLiteral(mComponentActionEEnum,
				MComponentAction.FIX_DOCUMENTATION);
		addEEnumLiteral(mComponentActionEEnum,
				MComponentAction.GENERATE_PROPERTY_GROUPS);
		addEEnumLiteral(mComponentActionEEnum,
				MComponentAction.REMOVE_PROPERTY_GROUPS);
		addEEnumLiteral(mComponentActionEEnum,
				MComponentAction.COMPLETE_SEMANTICS);
		addEEnumLiteral(mComponentActionEEnum, MComponentAction.OPEN_EDITOR);

		initEEnum(topLevelDomainEEnum, TopLevelDomain.class, "TopLevelDomain");
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.COM);
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.ORG);
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.NET);
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.CH);
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.FR);
		addEEnumLiteral(topLevelDomainEEnum, TopLevelDomain.RU);

		initEEnum(mPackageActionEEnum, MPackageAction.class, "MPackageAction");
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.DO);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.CLASS);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.ABSTRACT_CLASS);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.ENUMERATION);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.DATA_TYPE);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.SUB_PACKAGE);
		addEEnumLiteral(mPackageActionEEnum,
				MPackageAction.GENERATE_PROPERTY_GROUPS);
		addEEnumLiteral(mPackageActionEEnum,
				MPackageAction.REMOVE_PROPERTY_GROUPS);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.COMPLETE_SEMANTICS);
		addEEnumLiteral(mPackageActionEEnum, MPackageAction.OPEN_EDITOR);

		initEEnum(simpleTypeEEnum, SimpleType.class, "SimpleType");
		addEEnumLiteral(simpleTypeEEnum, SimpleType.NONE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.STRING);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.BOOLEAN);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.INTEGER);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.DOUBLE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.DATE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.OBJECT);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.PACKAGE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.CLASSIFIER);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.DATA_TYPE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.ENUMERATION);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.LITERAL);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.CLASS);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.FEATURE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.ATTRIBUTE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.REFERENCE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.OPERATION);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.PARAMETER);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.ANNOTATION);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.KEY_VALUE);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.NAMED_ELEMENT);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.TYPED_ELEMENT);
		addEEnumLiteral(simpleTypeEEnum, SimpleType.ANY);

		initEEnum(mClassifierActionEEnum, MClassifierAction.class,
				"MClassifierAction");
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.DO);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.CREATE_INSTANCE);
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.ABSTRACT);
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.SPECIALIZE);
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.LABEL);
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.CONSTRAINT);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.STRING_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.BOOLEAN_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.LITERAL_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.INTEGER_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.REAL_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.DATE_ATTRIBUTE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.UNARY_REFERENCE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.NARY_REFERENCE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.UNARY_CONTAINMENT);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.NARY_CONTAINMENT);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.OPERATION_ONE_PARAMETER);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.OPERATION_TWO_PARAMETERS);
		addEEnumLiteral(mClassifierActionEEnum, MClassifierAction.LITERAL);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.GROUP_OF_PROPERTIES);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.RESOLVE_INTO_CONTAINER);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.INTO_ENUMERATION);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.INTO_DATATYPE);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.INTO_ABSTRACT_CLASS);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.INTO_CONCRETE_CLASS);
		addEEnumLiteral(mClassifierActionEEnum,
				MClassifierAction.COMPLETE_SEMANTICS);

		initEEnum(orderingStrategyEEnum, OrderingStrategy.class,
				"OrderingStrategy");
		addEEnumLiteral(orderingStrategyEEnum, OrderingStrategy.NONE);
		addEEnumLiteral(orderingStrategyEEnum, OrderingStrategy.BY_ORDER);
		addEEnumLiteral(orderingStrategyEEnum, OrderingStrategy.ALPHABETICALLY);

		initEEnum(mLiteralActionEEnum, MLiteralAction.class, "MLiteralAction");
		addEEnumLiteral(mLiteralActionEEnum,
				MLiteralAction.INSERT_ABOVE_LITERAL);
		addEEnumLiteral(mLiteralActionEEnum,
				MLiteralAction.INSERT_BELOW_LITERAL);

		initEEnum(mPropertiesGroupActionEEnum, MPropertiesGroupAction.class,
				"MPropertiesGroupAction");
		addEEnumLiteral(mPropertiesGroupActionEEnum, MPropertiesGroupAction.DO);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.CONSTRAINT);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.STRING_ATTRIBUTE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.BOOLEAN_ATTRIBUTE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.INTEGER_ATTRIBUTE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.REAL_ATTRIBUTE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.DATE_ATTRIBUTE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.UNARY_REFERENCE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.NARY_REFERENCE);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.UNARY_CONTAINMENT);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.NARY_CONTAINMENT);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.OPERATION_ONE_PARAMETER);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.OPERATION_TWO_PARAMETERS);
		addEEnumLiteral(mPropertiesGroupActionEEnum,
				MPropertiesGroupAction.GROUP_OF_PROPERTIES);

		initEEnum(mPropertyActionEEnum, MPropertyAction.class,
				"MPropertyAction");
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.DO);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_STORED);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_CONTAINED);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_DERIVED);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_OPERATION);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_CHANGEABLE);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_NOT_CHANGEABLE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_UNARY);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INTO_NARY);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_STRING_ATTRIBUTE);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_BOOLEAN_FLAG);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_INTEGER_ATTRIBUTE);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_REAL_ATTRIBUTE);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.INTO_DATE_ATTRIBUTE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.CONSTRUCT_CHOICE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.CONSTRAIN_CHOICE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INIT_VALUE);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.SPECIALIZE_INIT_VALUE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.INIT_ORDER);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.SPECIALIZE_INIT_ORDER);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.RESULT);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.SPECIALIZE_RESULT);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.HIGHLIGHT);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.HIDE_IN_PROPERTIES_VIEW);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.HIDE_IN_TABLE_EDITOR);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.SHOW_IN_TABLE_EDITOR);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.SHOW_IN_PROPERTIES_VIEW);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.CLASS_WITH_STRING_PROPERTY);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.MOVE_TO_LAST_GROUP);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.MOVE_TO_NEW_GROUP);
		addEEnumLiteral(mPropertyActionEEnum,
				MPropertyAction.ADD_UPDATE_ANNOTATION);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.SPECIALIZE);
		addEEnumLiteral(mPropertyActionEEnum, MPropertyAction.ABSTRACT);

		initEEnum(propertyBehaviorEEnum, PropertyBehavior.class,
				"PropertyBehavior");
		addEEnumLiteral(propertyBehaviorEEnum, PropertyBehavior.STORED);
		addEEnumLiteral(propertyBehaviorEEnum, PropertyBehavior.CONTAINED);
		addEEnumLiteral(propertyBehaviorEEnum, PropertyBehavior.DERIVED);
		addEEnumLiteral(propertyBehaviorEEnum, PropertyBehavior.IS_OPERATION);

		initEEnum(mOperationSignatureActionEEnum,
				MOperationSignatureAction.class, "MOperationSignatureAction");
		addEEnumLiteral(mOperationSignatureActionEEnum,
				MOperationSignatureAction.DO);
		addEEnumLiteral(mOperationSignatureActionEEnum,
				MOperationSignatureAction.DATA_PARAMETER);
		addEEnumLiteral(mOperationSignatureActionEEnum,
				MOperationSignatureAction.UNARY_OBJECT_PARAMETER);
		addEEnumLiteral(mOperationSignatureActionEEnum,
				MOperationSignatureAction.NARY_OBJECT_PARAMETER);

		initEEnum(multiplicityCaseEEnum, MultiplicityCase.class,
				"MultiplicityCase");
		addEEnumLiteral(multiplicityCaseEEnum, MultiplicityCase.ZERO_ONE);
		addEEnumLiteral(multiplicityCaseEEnum, MultiplicityCase.ONE_ONE);
		addEEnumLiteral(multiplicityCaseEEnum, MultiplicityCase.ZERO_MANY);
		addEEnumLiteral(multiplicityCaseEEnum, MultiplicityCase.ONE_MANY);
		addEEnumLiteral(multiplicityCaseEEnum, MultiplicityCase.OTHER);

		initEEnum(classifierKindEEnum, ClassifierKind.class, "ClassifierKind");
		addEEnumLiteral(classifierKindEEnum, ClassifierKind.CLASS_TYPE);
		addEEnumLiteral(classifierKindEEnum, ClassifierKind.DATA_TYPE);
		addEEnumLiteral(classifierKindEEnum, ClassifierKind.ENUMERATION);
		addEEnumLiteral(classifierKindEEnum,
				ClassifierKind.WRONG_CLASS_WITH_LITERAL);

		initEEnum(propertyKindEEnum, PropertyKind.class, "PropertyKind");
		addEEnumLiteral(propertyKindEEnum, PropertyKind.ATTRIBUTE);
		addEEnumLiteral(propertyKindEEnum, PropertyKind.REFERENCE);
		addEEnumLiteral(propertyKindEEnum, PropertyKind.OPERATION);
		addEEnumLiteral(propertyKindEEnum, PropertyKind.TYPE_MISSING);
		addEEnumLiteral(propertyKindEEnum, PropertyKind.AMBIGUOUS);
		addEEnumLiteral(propertyKindEEnum, PropertyKind.UNDEFINED);

		initEEnum(mParameterActionEEnum, MParameterAction.class,
				"MParameterAction");
		addEEnumLiteral(mParameterActionEEnum, MParameterAction.DO);
		addEEnumLiteral(mParameterActionEEnum, MParameterAction.INTO_UNARY);
		addEEnumLiteral(mParameterActionEEnum, MParameterAction.INTO_NARY);
		addEEnumLiteral(mParameterActionEEnum,
				MParameterAction.INTO_STRING_ATTRIBUTE);
		addEEnumLiteral(mParameterActionEEnum,
				MParameterAction.INTO_BOOLEAN_FLAG);
		addEEnumLiteral(mParameterActionEEnum,
				MParameterAction.INTO_INTEGER_ATTRIBUTE);
		addEEnumLiteral(mParameterActionEEnum,
				MParameterAction.INTO_REAL_ATTRIBUTE);
		addEEnumLiteral(mParameterActionEEnum,
				MParameterAction.INTO_DATE_ATTRIBUTE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/UUID
		createUUIDAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(this, source, new String[] { "mName", "MCore" });
		addAnnotation(getMComponent__DoActionUpdate__MComponentAction(), source,
				new String[] { "mName", "do Action Update" });
		addAnnotation(getMComponent_DisableDefaultContainment(), source,
				new String[] { "mName", "disableDefaultContainment" });
		addAnnotation((getMPackage__DoActionUpdate__MPackageAction())
				.getEParameters().get(0), source,
				new String[] { "mName", "trg" });
		addAnnotation(getMPackage_UuidAttributeName(), source,
				new String[] { "mName", "uuid Attribute Name" });
		addAnnotation(getMClassifier__AllSuperTypes(), source,
				new String[] { "mName", "allSuperTypes" });
		addAnnotation(
				(getMClassifier__DoActionUpdate__MClassifierAction())
						.getEParameters().get(0),
				source, new String[] { "mName", "trg" });
		addAnnotation(getMClassifier__InvokeSetDoAction__MClassifierAction(),
				source, new String[] { "mName", "invoke Set Do Action" });
		addAnnotation(getMClassifier__InvokeToggleSuperType__MClassifier(),
				source, new String[] { "mName", "invoke Toggle Super Type" });
		addAnnotation(getMClassifier__DoSuperTypeUpdate__MClassifier(), source,
				new String[] { "mName", "Do SuperType Update" });
		addAnnotation(
				(getMClassifier__DoSuperTypeUpdate__MClassifier())
						.getEParameters().get(0),
				source, new String[] { "mName", "trg" });
		addAnnotation(getMClassifier__AmbiguousClassifierName(), source,
				new String[] { "mName", "ambiguous Classifier Name" });
		addAnnotation(getMClassifier_ObjectReferences(), source,
				new String[] { "mName", "object References" });
		addAnnotation(getMClassifier_NearestInstance(), source,
				new String[] { "mName", "nearest Instance" });
		addAnnotation(getMClassifier_StrictNearestInstance(), source,
				new String[] { "mName", "strict Nearest Instance" });
		addAnnotation(getMClassifier_StrictInstance(), source,
				new String[] { "mName", "strict Instance" });
		addAnnotation(getMClassifier_IntelligentNearestInstance(), source,
				new String[] { "mName", "intelligent Nearest Instance" });
		addAnnotation(getMClassifier_ObjectUnreferenced(), source,
				new String[] { "mName", "object Unreferenced" });
		addAnnotation(getMClassifier_OutstandingToCopyObjects(), source,
				new String[] { "mName", "outstanding To Copy Objects" });
		addAnnotation(getMClassifier_PropertyInstances(), source,
				new String[] { "mName", "property Instances" });
		addAnnotation(getMClassifier_ObjectReferencePropertyInstances(), source,
				new String[] { "mName",
						"object Reference Property Instances" });
		addAnnotation(getMClassifier_IntelligentInstance(), source,
				new String[] { "mName", "intelligent Instance" });
		addAnnotation(getMClassifier_ContainmentHierarchy(), source,
				new String[] { "mName", "containmentHierarchy" });
		addAnnotation(getMClassifier_StrictAllClassesContainedIn(), source,
				new String[] { "mName", "strict All Classes Contained In" });
		addAnnotation(getMClassifier_StrictContainmentHierarchy(), source,
				new String[] { "mName", "strict Containment Hierarchy" });
		addAnnotation(getMClassifier_IntelligentAllClassesContainedIn(), source,
				new String[] { "mName",
						"intelligent All Classes Contained In" });
		addAnnotation(getMClassifier_IntelligentContainmentHierarchy(), source,
				new String[] { "mName", "intelligent Containment Hierarchy" });
		addAnnotation(getMClassifier_InstantiationHierarchy(), source,
				new String[] { "mName", "instantiation Hierarchy" });
		addAnnotation(getMClassifier_InstantiationPropertyHierarchy(), source,
				new String[] { "mName", "instantiation Property Hierarchy" });
		addAnnotation(
				getMClassifier_IntelligentInstantiationPropertyHierarchy(),
				source, new String[] { "mName",
						"intelligent Instantiation Property Hierarchy" });
		addAnnotation(getMClassifier_Classescontainedin(), source,
				new String[] { "mName", "classes contained in" });
		addAnnotation(getMClassifier_CanApplyDefaultContainment(), source,
				new String[] { "mName", "can Apply Default Containment" });
		addAnnotation(
				getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction(),
				source, new String[] { "mName", "doActionUpdate" });
		addAnnotation(
				getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction(),
				source, new String[] { "mName", "invokeSetDoAction" });
		addAnnotation(getMProperty__DoActionUpdate__MPropertyAction(), source,
				new String[] { "mName", "doActionUpdate" });
		addAnnotation(
				(getMProperty__DoActionUpdate__MPropertyAction())
						.getEParameters().get(0),
				source, new String[] { "mName", "trg" });
		addAnnotation(
				getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior(),
				source,
				new String[] { "mName", "invoke Set Property Behaviour" });
		addAnnotation(getMProperty__InvokeSetDoAction__MPropertyAction(),
				source, new String[] { "mName", "invoke Set Do Action" });
		addAnnotation(getMProperty_DoAction(), source,
				new String[] { "mName", "Do Action " });
		addAnnotation(getMEditor__ResetEditor(), source,
				new String[] { "mName", "resetEditor" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source,
				new String[] { "rootConstraint", "trg.name = \'MComponent\'" });
		addAnnotation(getMRepositoryElement__IndentationSpaces(), source,
				new String[] { "body", "indentationSpaces(indentLevel * 4)" });
		addAnnotation(getMRepositoryElement__IndentationSpaces__Integer(),
				source, new String[] { "body",
						"if size < 1 then \'\'\r\nelse self.indentationSpaces(size-1).concat(\' \') endif" });
		addAnnotation(getMRepositoryElement__NewLineString(), source,
				new String[] { "body", "\'\\n\'" });
		addAnnotation(getMRepositoryElement_KindLabel(), source,
				new String[] { "derive", "\'TODO\'\n" });
		addAnnotation(getMRepositoryElement_RenderedKindLabel(), source,
				new String[] { "derive",
						"/*OVERRITEN in Code for performance*/\nself.indentationSpaces().concat(self.kindLabel)" });
		addAnnotation(getMRepositoryElement_IndentLevel(), source,
				new String[] { "derive",
						"if eContainer().oclIsUndefined() \r\n  then 0\r\nelse if eContainer().oclIsKindOf(MRepositoryElement)\r\n  then eContainer().oclAsType(MRepositoryElement).indentLevel + 1\r\n  else 0 endif endif" });
		addAnnotation(mRepositoryEClass, source,
				new String[] { "label", "\'\' " });
		addAnnotation(getMRepository_Component(), source, new String[] {
				"initValue", "OrderedSet{Tuple{name=\'Main Component\'}}" });
		addAnnotation(getMRepository_AvoidRegenerationOfEditorConfiguration(),
				source, new String[] { "initValue", "false\n" });
		addAnnotation(mNamedEClass, source, new String[] { "label", "eName" });
		addAnnotation(getMNamed__SameName__MNamed(), source,
				new String[] { "body", "sameString(name, n.name)\r\n" });
		addAnnotation(getMNamed__SameShortName__MNamed(), source, new String[] {
				"body",
				"if stringEmpty(shortName)  then\r\n  sameString(name, n.shortName)\r\nelse if  stringEmpty(n.shortName) then\r\n sameString(shortName, n.name)\r\nelse sameString(shortName, n.shortName)\r\nendif endif" });
		addAnnotation(getMNamed__SameString__String_String(), source,
				new String[] { "body",
						"s1=s2 \r\nor \r\n(s1.oclIsUndefined() and s2=\'\')\r\nor\r\n(s1=\'\' and s2.oclIsUndefined())" });
		addAnnotation(getMNamed__StringEmpty__String(), source,
				new String[] { "body", "s.oclIsUndefined() or s=\'\'" });
		addAnnotation(getMNamed_EName(), source, new String[] { "derive",
				"if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseLower()\r\nelse self.specialEName.camelCaseLower()\r\nendif" });
		addAnnotation(getMNamed_FullLabel(), source, new String[] { "derive",
				"\'OVERRIDE IN SUBCLASS \'.concat(self.calculatedName)" });
		addAnnotation(getMNamed_LocalStructuralName(), source,
				new String[] { "derive", "\'\'\n" });
		addAnnotation(getMNamed_CalculatedName(), source, new String[] {
				"derive",
				"if stringEmpty(name) then \' NAME MISSING\' else name endif" });
		addAnnotation(getMNamed_CalculatedShortName(), source, new String[] {
				"derive",
				"if stringEmpty(name) or stringEmpty(shortName) then calculatedName else shortName endif" });
		addAnnotation(getMNamed_CorrectName(), source,
				new String[] { "derive", "not stringEmpty(name)" });
		addAnnotation(getMNamed_CorrectShortName(), source, new String[] {
				"derive",
				" stringEmpty(shortName)\r\n or (not stringEmpty(name))" });
		addAnnotation(mComponentEClass, source,
				new String[] { "label", "self.derivedBundleName" });
		addAnnotation(getMComponent__DoAction$Update__MComponentAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMComponent__DoActionUpdate__MComponentAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMComponent_OwnedPackage(), source,
				new String[] { "initOrder", "1", "initValue",
						"OrderedSet{Tuple{name=\'My Package\'}}" });
		addAnnotation(getMComponent_ResourceFolder(), source, new String[] {
				"initOrder", "2", "initValue",
				"OrderedSet{\r\n  Tuple{\r\n        name=\'workspace\', \r\n        folder=\r\n                OrderedSet{\r\n                       Tuple{\r\n                             name=\'test\',\r\n                             resource=\r\n                                 OrderedSet{Tuple{name=\'My\'}}}}}}" });
		addAnnotation(getMComponent_DerivedNamePrefix(), source, new String[] {
				"derive",
				"let chain: String = namePrefix in\nif chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() \n then null \n else chain.camelCaseUpper().trim()\n  endif\n" });
		addAnnotation(getMComponent_ContainingRepository(), source,
				new String[] { "derive",
						"if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else if self.eContainer().oclIsKindOf(MRepository)\r\n    then self.eContainer().oclAsType(MRepository)\r\n    else null endif endif" });
		addAnnotation(getMComponent_RepresentsCoreComponent(), source,
				new String[] { "derive",
						"if self.eContainmentFeature().oclIsUndefined() \r\n  then false\r\n  else eContainmentFeature().name=\'core\' endif" });
		addAnnotation(getMComponent_RootPackage(), source, new String[] {
				"derive",
				"ownedPackage->asOrderedSet()->select(it: mcore::MPackage | let chain: Boolean = it.isRootPackage in\r\nif chain = true then true else false \r\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet()->at(1)" });
		addAnnotation(getMComponent_DomainType(), source,
				new String[] { "initValue", "\'org\'" });
		addAnnotation(getMComponent_DomainName(), source,
				new String[] { "initValue", "\'langlets\'" });
		addAnnotation(getMComponent_DerivedDomainType(), source, new String[] {
				"derive",
				"let derivedOne:String = self.domainType.allLowerCase().trim() in\r\nSequence{1..(derivedOne.tokenize(\'.\')->size())}->iterate (\r\ni:Integer; c: String =\'\'|\r\nif i=1\r\nthen c.concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())))\r\nelse c.concat(\'.\').concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())-i+1))\r\nendif\r\n)" });
		addAnnotation(getMComponent_DerivedDomainName(), source, new String[] {
				"derive",
				"let derivedOne:String = self.domainName.allLowerCase().trim() in\r\nSequence{1..(derivedOne.tokenize(\'.\')->size())}->iterate (\r\ni:Integer; c: String =\'\'|\r\nif i=1\r\nthen c.concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())))\r\nelse c.concat(\'.\').concat(derivedOne.tokenize(\'.\')->at((derivedOne.tokenize(\'.\')->size())-i+1))\r\nendif\r\n)" });
		addAnnotation(getMComponent_DerivedBundleName(), source, new String[] {
				"derive",
				"(\nif domainType.oclIsUndefined() \n   then \'MISSING domainType\' \n   else derivedDomainType endif).concat(\n\'.\').concat(\nif domainName.oclIsUndefined() \n   then  \'MISSING domainName\' \n   else derivedDomainName endif).concat(\n\'.\').concat(\ncalculatedShortName.camelCaseLower().allLowerCase())" });
		addAnnotation(getMComponent_DerivedURI(), source, new String[] {
				"derive",
				"\'http://www.\'.concat(\nif domainName.oclIsUndefined() \n   then  \'MISSING domainName\' \n   else derivedDomainName endif).concat(\n\'.\').concat(\nif domainType.oclIsUndefined() \n   then \'MISSING domainType\' \n   else derivedDomainType endif).concat(\n\'/\').concat(\neName)" });
		addAnnotation(getMComponent_MainEditor(), source, new String[] {
				"derive",
				"if mainNamedEditor.oclIsUndefined()\n  then null\n  else mainNamedEditor.editor\nendif\n" });
		addAnnotation(getMComponent_AllGeneralAnnotations(), source,
				new String[] { "derive",
						"mcore::annotations::MGeneralAnnotation.allInstances()" });
		addAnnotation(getMComponent_AbstractComponent(), source,
				new String[] { "initValue", "false\n" });
		addAnnotation(getMComponent_CountImplementations(), source,
				new String[] { "initValue", "1\n" });
		addAnnotation(getMComponent_DisableDefaultContainment(), source,
				new String[] { "initValue", "false\n" });
		addAnnotation(getMComponent_DoAction(), source, new String[] { "derive",
				"mcore::MComponentAction::Do\n", "choiceConstruction",
				"let e1:OrderedSet(MComponentAction)=OrderedSet{MComponentAction::Do, MComponentAction::Object, MComponentAction::Class,MComponentAction::FixDocumentation,MComponentAction::OpenEditor} in\r\nif self.abstractComponent = true\r\nthen e1->append(MComponentAction::Implement)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)\r\nelse e1->append(MComponentAction::Abstract)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)\r\nendif" });
		addAnnotation(getMComponent_AvoidRegenerationOfEditorConfiguration(),
				source, new String[] { "initValue", "false\n" });
		addAnnotation(getMComponent_UseLegacyEditorconfig(), source,
				new String[] { "initValue", "true\n" });
		addAnnotation(mPackageEClass, source,
				new String[] { "label", "eName" });
		addAnnotation(getMPackage__GenerateJsonSchema$update01Object__Boolean(),
				source, new String[] { "body", "self" });
		addAnnotation(
				getMPackage__GenerateJsonSchema$update01Value__Boolean_MPackage(),
				source, new String[] { "body", "derivedJsonSchema\n" });
		addAnnotation(getMPackage__DoAction$Update__MPackageAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMPackage__GenerateJsonSchema$Update__Boolean(), source,
				new String[] { "body", "null" });
		addAnnotation(getMPackage__DoActionUpdate__MPackageAction(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getMPackage_ResourceRootClass(), source, new String[] {
				"choiceConstraint",
				"self->union(allSubpackages)->includes(trg.containingPackage)\r\n and\r\n not (trg.abstractClass)\r\nand\r\n trg.kind = ClassifierKind::ClassType" });
		addAnnotation(getMPackage_UseUUID(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMPackage_Parent(), source, new String[] { "derive",
				"if self.isRootPackage then null else self.eContainer().oclAsType(MPackage) endif" });
		addAnnotation(getMPackage_ContainingPackage(), source, new String[] {
				"derive",
				"if isRootPackage then null else\r\n eContainer().oclAsType(MPackage) endif " });
		addAnnotation(getMPackage_ContainingComponent(), source, new String[] {
				"derive",
				"if eContainer().oclIsUndefined() then null\r\nelse \r\nif isRootPackage then eContainer().oclAsType(MComponent) \r\nelse containingPackage.containingComponent endif\r\nendif" });
		addAnnotation(getMPackage_RepresentsCorePackage(), source,
				new String[] { "derive",
						"if self.eContainer().oclIsUndefined() \r\n  then false\r\n  else if isRootPackage \r\n    then containingComponent.representsCoreComponent \r\n    else containingPackage.representsCorePackage endif endif" });
		addAnnotation(getMPackage_AllSubpackages(), source,
				new String[] { "derive",
						"subPackage->union(subPackage->closure(subPackage))" });
		addAnnotation(getMPackage_QualifiedName(), source, new String[] {
				"derive",
				"if (let e0: Boolean = if (representsCorePackage)= true \n then true \n else if (isRootPackage)= true \n then true \nelse if ((representsCorePackage)= null or (isRootPackage)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: String = eName in\nif chain.allLowerCase().oclIsUndefined() \n then null \n else chain.allLowerCase()\n  endif\n  else (let e0: String = if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.qualifiedName\nendif.concat(\'::\').concat(let chain02: String = eName in\nif chain02.allLowerCase().oclIsUndefined() \n then null \n else chain02.allLowerCase()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n" });
		addAnnotation(getMPackage_IsRootPackage(), source, new String[] {
				"derive",
				"if eContainer().oclIsUndefined() \r\n  then false\r\n  else eContainer().oclIsTypeOf(MComponent) endif" });
		addAnnotation(getMPackage_DerivedNsURI(), source, new String[] {
				"derive",
				"if not (self.specialNsURI.oclIsUndefined() or self.specialNsURI.trim().size() = 0) then\r\n\tself.specialNsURI\r\nelse self.derivedStandardNsURI endif\r\n" });
		addAnnotation(getMPackage_DerivedStandardNsURI(), source, new String[] {
				"derive",
				"if self.isRootPackage then\r\nself.containingComponent.derivedURI.concat(\'/\').concat(self.calculatedShortName.camelCaseUpper()) else\r\nself.containingPackage.derivedStandardNsURI.concat(\'/\').concat(self.calculatedShortName.camelCaseUpper().firstUpperCase())\r\nendif" });
		addAnnotation(getMPackage_DerivedNsPrefix(), source, new String[] {
				"derive",
				"(if isRootPackage then \'\' else\r\ncontainingPackage.derivedNsPrefix.concat(\'.\') endif)\r\n.concat(calculatedShortName.camelCaseLower().allLowerCase())" });
		addAnnotation(getMPackage_DerivedJavaPackageName(), source,
				new String[] { "derive",
						"if containingComponent.calculatedShortName = containingComponent.rootPackage.calculatedShortName\r\nthen (\r\n\t\tif containingComponent.domainType.oclIsUndefined()\r\n\t\tthen \'MISSING domain type\'\r\n\t\telse containingComponent.derivedDomainType\r\n\t\tendif\r\n\t).concat(\'.\').concat(\r\n\t\tif containingComponent.domainName.oclIsUndefined()\r\n\t\tthen \'MISSING domain name\'\r\n\t\telse containingComponent.derivedDomainName\r\n\t\tendif\r\n\t).concat(\'.\').concat(derivedNsPrefix)\r\nelse\r\n\tcontainingComponent.derivedBundleName.concat(\'.\').concat(derivedNsPrefix)\r\nendif" });
		addAnnotation(getMPackage_DoAction(), source,
				new String[] { "derive", "mcore::MPackageAction::Do\n" });
		addAnnotation(getMPackage_DerivedNamePrefix(), source, new String[] {
				"derive",
				"let getOtherNamePrefix: String = if (let chain: Boolean = isRootPackage in\nif chain = true then true else false \n  endif) \n  =true \nthen if containingComponent.oclIsUndefined()\n  then null\n  else containingComponent.derivedNamePrefix\nendif\n  else if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.derivedNamePrefix\nendif\nendif in\nlet resultofDerivedNamePrefix: String = if (let e0: Boolean = not((let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: String = namePrefix in\nif chain.oclIsUndefined() or chain.camelCaseUpper().oclIsUndefined() \n then null \n else chain.camelCaseUpper()\n  endif\n  else getOtherNamePrefix\nendif in\nresultofDerivedNamePrefix\n" });
		addAnnotation(getMPackage_NamePrefixScope(), source, new String[] {
				"derive",
				"let choosePackage: mcore::MPackage = if (isRootPackage) \n  =true \nthen self\n  else if containingPackage.oclIsUndefined()\n  then null\n  else containingPackage.namePrefixScope\nendif\nendif in\nlet resultofNamePrefixScope: mcore::MPackage = if (let e0: Boolean =  namePrefix.oclIsInvalid() or  namePrefix.oclIsUndefined() or (let e0: Boolean = namePrefix = \'\' in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen choosePackage\n  else self\nendif in\nresultofNamePrefixScope\n" });
		addAnnotation(getMPackage_DerivedJsonSchema(), source, new String[] {
				"derive",
				"let space : String = \' \' in\nlet lineBreak : String = \'\\n\' in\nlet branch : String = \'{\' in\nlet closedBranch : String = \'}\' in\nlet comma : String = \',\' in\nlet rootPropertyName : String = \'rootProperty\' in \nlet reference : String = \'#/definitions/\' in \nlet jsonHeader: String = \'{\'\n.concat(lineBreak)\n.concat(\' \"$schema\": \"http://json-schema.org/draft-07/schema#\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"title\": \')\n.concat(\'\"\').concat(self.containingComponent.name).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"type\": \')\n.concat(\'\"object\"\')\n.concat(comma)\n.concat(lineBreak)\n.concat(\'\"properties\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\'\"\').concat(rootPropertyName).concat(\'\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\'\"description\": \').concat(\'\"\').concat(rootPropertyName).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\'\"$ref\":\').concat(space).concat(\'\"\').concat(reference).concat(self.resourceRootClass.eName).concat(\'\"\')\n.concat(lineBreak)\n.concat(closedBranch)\n.concat(lineBreak)\n.concat(closedBranch).concat(comma)\n.concat(lineBreak)\n.concat(\'\"required\":\').concat(space).concat(\'[\').concat(space).concat(\'\"\').concat(rootPropertyName).concat(\'\"\').concat(space).concat(\']\')\nin\njsonHeader.concat(comma)\n.concat(lineBreak)\n.concat(\'\"definitions\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(self.classifier->iterate(test : MClassifier; s : String = \'\' | s.concat(test.derivedJsonSchema).concat(if self.classifier->indexOf(test) = self.classifier->size() then \'}\' else lineBreak.concat(\',\') endif)))\n.concat(lineBreak)\n.concat(closedBranch)" });
		addAnnotation(getMPackage_GenerateJsonSchema(), source,
				new String[] { "derive", "null", "update01Object",
						"generateJsonSchema$update01Object", "update01Feature",
						"generatedJsonSchema", "update01Value",
						"generateJsonSchema$update01Value", "update01Mode",
						"MofRedefine" });
		addAnnotation(getMHasSimpleType__SimpleTypeAsString__SimpleType(),
				source, new String[] { "body",
						"if  simpleType=SimpleType::None then \'-\' else\r\nif  simpleType=SimpleType::String then \'String\' else\r\n if simpleType=SimpleType::Boolean then \'Boolean\' else\r\n if  simpleType=SimpleType::Date then \'Date\' else\r\n if  simpleType=SimpleType::Double then \'Double\' else\r\n if  simpleType=SimpleType::Integer then \'Integer\' else\r\n if  simpleType=SimpleType::Annotation then \'Annotation\' else\r\n if  simpleType=SimpleType::Attribute then \'Attribute\' else\r\n if  simpleType=SimpleType::Class then \'Class\' else\r\n if  simpleType=SimpleType::Classifier then \'Classifier\' else\r\n if  simpleType=SimpleType::DataType then \'Data Type\' else\r\n  if simpleType=SimpleType::Enumeration then \'Enumeration\' else\r\n  if  simpleType=SimpleType::Feature then \'Feature\' else\r\n  if  simpleType=SimpleType::KeyValue then \'KeyValue\' else\r\n  if  simpleType=SimpleType::Literal then \'Literal\' else\r\n  if  simpleType=SimpleType::Object then \'EObject\' else\r\n  if simpleType= SimpleType::Any then \'Object\' else\r\n  if  simpleType=SimpleType::Operation then \'Operation\' else\r\n   if simpleType=SimpleType::Package then \'Package\' else\r\n   if  simpleType=SimpleType::Parameter then \'Parameter\' else\r\n   if  simpleType=SimpleType::Reference then \'Reference\' else\r\n   if simpleType=SimpleType::NamedElement then \'Named Element\' else\r\n   if simpleType=SimpleType::TypedElement then \'Typed Element\' else \r\n   \'ERROR\' endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif " });
		addAnnotation(getMHasSimpleType_SimpleTypeString(), source,
				new String[] { "derive",
						"self.simpleTypeAsString(self.simpleType)" });
		addAnnotation(getMHasSimpleType_HasSimpleDataType(), source,
				new String[] { "derive",
						" simpleType=SimpleType::String or\r\n  simpleType=SimpleType::Boolean or\r\n   simpleType=SimpleType::Date or\r\n    simpleType=SimpleType::Double or\r\n     simpleType=SimpleType::Integer or \r\n     simpleType= SimpleType::Any" });
		addAnnotation(getMHasSimpleType_HasSimpleModelingType(), source,
				new String[] { "derive",
						" not (simpleType=SimpleType::None or hasSimpleDataType)" });
		addAnnotation(getMHasSimpleType_SimpleTypeIsCorrect(), source,
				new String[] { "derive", "false" });
		addAnnotation(getMHasSimpleType_SimpleType(), source, new String[] {
				"initValue",
				"let stringType: SimpleType = SimpleType::String in  stringType" });
		addAnnotation(mClassifierEClass, source, new String[] { "label",
				"eName.concat(\r\n\tif self.superType->isEmpty() \r\n    then \'\'\r\n    else \'->\'.concat(\r\n    \tif superType->size() = 1 \r\n    \tthen superTypesAsString()\r\n    \telse \'(\'.concat(superTypesAsString()).concat(\')\')\r\n    \tendif\r\n    ) endif\r\n)" });
		addAnnotation(getMClassifier__ToggleSuperType$Update__MClassifier(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMClassifier__OrderingStrategy$Update__OrderingStrategy(),
				source, new String[] { "body", "null" });
		addAnnotation(getMClassifier__DoAction$Update__MClassifierAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMClassifier__AllLiterals(), source,
				new String[] { "body", "literal" });
		addAnnotation(getMClassifier__SelectableClassifier__EList_MPackage(),
				source, new String[] { "body",
						"let mClassifier:MClassifier = self in \r\n\r\nif currentClassifier->includes(mClassifier) \r\n  then true\r\nelse if mClassifier.containingPackage.oclIsUndefined()\r\n  then false\r\nelse if filterPackage.oclIsUndefined()\r\n  then  if mClassifier.containingPackage.usePackageContentOnlyWithExplicitFilter\r\n          then if currentClassifier->isEmpty()\r\n            then false\r\n            else currentClassifier.containingPackage->includes(mClassifier.containingPackage) endif          \r\n   else true endif\r\nelse\r\n  filterPackage=mClassifier.containingPackage\r\nendif endif \r\n endif" });
		addAnnotation(getMClassifier__AllDirectSubTypes(), source,
				new String[] { "body",
						"MClassifier.allInstances()\r\n->select(c:MClassifier|c.kind=ClassifierKind::ClassType and c.superType->includes(self))" });
		addAnnotation(getMClassifier__AllSubTypes(), source, new String[] {
				"body",
				"let dS:Set(MClassifier)=self.allDirectSubTypes() in\r\nlet iDS:Set(MClassifier)=dS->iterate(c:MClassifier;i:Set(MClassifier)=Set{}|i->union(c.allSubTypes())) in\r\ndS->union(iDS)->excluding(null)" });
		addAnnotation(getMClassifier__SuperTypesAsString(), source,
				new String[] { "body",
						"let sts:OrderedSet(MClassifier) = self.superType in\r\nif sts->isEmpty()then\'\' else\r\nsts->iterate(c:MClassifier;res:String=\'\' |\r\n  (if res=\'\' then \'\' else res.concat(\', \') endif)\r\n     .concat(c.eName))\r\nendif" });
		addAnnotation(getMClassifier__AllSuperTypes(), source,
				new String[] { "body", "null" });
		addAnnotation(getMClassifier__AllProperties(), source, new String[] {
				"body",
				"ownedProperty\r\n->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperProperties()| ps->append(p))\r\n\r\n/*self.superType.allProperties()->asSet()\r\n->union(self.ownedProperty)->asOrderedSet()*/" });
		addAnnotation(getMClassifier__AllSuperProperties(), source,
				new String[] { "body", "superType.allProperties()" });
		addAnnotation(getMClassifier__AllFeatures(), source, new String[] {
				"body",
				"if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or p.kind=mcore::PropertyKind::Reference\r\n)\r\nendif" });
		addAnnotation(getMClassifier__AllNonContainmentFeatures(), source,
				new String[] { "body",
						"if allProperties()->isEmpty() then OrderedSet{} else\r\nallProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Attribute or (p.kind=mcore::PropertyKind::Reference and not (p.containment))\r\n)\r\nendif" });
		addAnnotation(getMClassifier__AllStoredNonContainmentFeatures(), source,
				new String[] { "body",
						"if allProperties()->isEmpty() \r\n  then OrderedSet{} \r\n  else allProperties()\r\n    ->select(p:mcore::MProperty|\r\n      p.kind=mcore::PropertyKind::Attribute \r\n        or \r\n     (p.kind=mcore::PropertyKind::Reference \r\n        and p.hasStorage \r\n        and not (p.containment))) endif" });
		addAnnotation(getMClassifier__AllFeaturesWithStorage(), source,
				new String[] { "body",
						"let ncs:OrderedSet(mcore::MProperty)=self.allNonContainmentFeatures()->select(p:mcore::MProperty|p.hasStorage) in\r\nlet cs:OrderedSet(mcore::MProperty)=self.allContainmentReferences()->select(p:mcore::MProperty|p.hasStorage) in\r\ncs->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=ncs| ps->append(p))" });
		addAnnotation(getMClassifier__AllContainmentReferences(), source,
				new String[] { "body",
						"if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Reference and p.containment\r\n)->asOrderedSet()\r\nendif" });
		addAnnotation(getMClassifier__AllOperationSignatures(), source,
				new String[] { "body",
						"if self.allProperties()->isEmpty() then OrderedSet{} else\r\nself.allProperties()->select(p:mcore::MProperty|p.kind=mcore::PropertyKind::Operation).operationSignature->asOrderedSet()\r\nendif" });
		addAnnotation(getMClassifier__AllClassesContainedIn(), source,
				new String[] { "body",
						"\r\nMClassifier.allInstances().ownedProperty->select(containment)\r\n     ->select(p:MProperty| (p.type=self.containingClassifier  or self.superType.containingClassifier->includes(p.type )and p<>self)).containingClassifier\r\n   ->excluding(self)\r\n" });
		addAnnotation(getMClassifier__DefaultPropertyValue(), source,
				new String[] { "body",
						"Tuple {simpleType=SimpleType::String, name=defaultPropertyDescription()}" });
		addAnnotation(getMClassifier__DefaultPropertyDescription(), source,
				new String[] { "body", "\'Text 1\'" });
		addAnnotation(getMClassifier__AllIdProperties(), source, new String[] {
				"body",
				"if self.idProperty->isEmpty() then self.allSuperIdProperties() else\r\nself.idProperty->iterate(p:mcore::MProperty;ps:OrderedSet(mcore::MProperty)=self.allSuperIdProperties()| ps->append(p))\r\nendif\r\n/*self.superType.allProperties()->asSet()\r\n->union(self.ownedProperty)->asOrderedSet()*/" });
		addAnnotation(getMClassifier__AllSuperIdProperties(), source,
				new String[] { "body",
						"self.superType.allIdProperties()->asOrderedSet()" });
		addAnnotation(getMClassifier__DoActionUpdate__MClassifierAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMClassifier__InvokeSetDoAction__MClassifierAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMClassifier__InvokeToggleSuperType__MClassifier(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMClassifier__DoSuperTypeUpdate__MClassifier(), source,
				new String[] { "body", "null\n" });
		addAnnotation(getMClassifier__AmbiguousClassifierName(), source,
				new String[] { "body",
						"let allClasses: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = if containingPackage.oclIsUndefined()\n  then OrderedSet{}\n  else containingPackage.classifier\nendif in\nchain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() \n else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it <> self in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet sameName: OrderedSet(mcore::MClassifier)  = let chain: OrderedSet(mcore::MClassifier)  = allClasses in\nchain->iterate(i:mcore::MClassifier; r: OrderedSet(mcore::MClassifier)=OrderedSet{} | if i.oclIsKindOf(mcore::MClassifier) then r->including(i.oclAsType(mcore::MClassifier))->asOrderedSet() \n else r endif)->select(it: mcore::MClassifier | let e0: Boolean = it.name = name in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet var0: Boolean = let chain: OrderedSet(mcore::MClassifier)  = sameName in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif in\nvar0\n" });
		addAnnotation(getMClassifier_SuperType(), source, new String[] {
				"choiceConstraint",
				"  not (trg = self) \r\nand\r\n  trg.kind = ClassifierKind::ClassType\r\nand\r\n  trg.selectableClassifier(superType, superTypePackage)\r\n\r\n" });
		addAnnotation(getMClassifier_Instance(), source, new String[] {
				"derive",
				"let os:OrderedSet(objects::MObject) = \r\n\tobjects::MObject.allInstances()->asOrderedSet()\r\nin\r\nif os->isEmpty() then\r\n\tOrderedSet{} \r\nelse\r\n\tos->select(x:objects::MObject| x.isInstanceOf(self) or x.type.allSuperTypes()->includes(self))->asOrderedSet()\r\nendif" });
		addAnnotation(getMClassifier_Kind(), source, new String[] { "derive",
				"if self.isClassByStructure \r\n  then if self.literal->isEmpty() \r\n    then ClassifierKind::ClassType \r\n    else ClassifierKind::WrongClassWithLiteral endif\r\nelse  \r\nif self.literal->notEmpty() then \r\n  ClassifierKind::Enumeration else \r\n  ClassifierKind::DataType\r\nendif endif \r\n" });
		addAnnotation(getMClassifier_ContainingPackage(), source, new String[] {
				"derive",
				"if self.eContainer().oclIsUndefined() then null else self.eContainer().oclAsType(MPackage) endif" });
		addAnnotation(getMClassifier_RepresentsCoreType(), source,
				new String[] { "derive",
						"if containingPackage.oclIsUndefined() \r\n  then false\r\n  else containingPackage.representsCorePackage endif" });
		addAnnotation(getMClassifier_AllPropertyGroups(), source, new String[] {
				"derive",
				"propertyGroup->union(propertyGroup->closure(propertyGroup))->sortedBy(indentLevel)" });
		addAnnotation(getMClassifier_OperationAsIdPropertyError(), source,
				new String[] { "derive",
						"self.idProperty->exists(p:MProperty|p.kind=PropertyKind::Operation)" });
		addAnnotation(getMClassifier_IsClassByStructure(), source,
				new String[] { "derive",
						"self.enforcedClass or \r\n  self.superType->notEmpty() or \r\n  self.ownedProperty->notEmpty() or \r\n   self.abstractClass or\r\n   self.hasSimpleModelingType " });
		addAnnotation(getMClassifier_ToggleSuperType(), source, new String[] {
				"derive", "null", "choiceConstraint",
				"  not (trg = self) \r\nand\r\n  trg.kind = ClassifierKind::ClassType\r\nand\r\n  trg.selectableClassifier(superType, superTypePackage)\r\n\r\n" });
		addAnnotation(getMClassifier_OrderingStrategy(), source,
				new String[] { "derive", "OrderingStrategy::None" });
		addAnnotation(getMClassifier_TestAllProperties(), source,
				new String[] { "derive", "self.allProperties()" });
		addAnnotation(getMClassifier_IdProperty(), source, new String[] {
				"choiceConstraint",
				"self.allFeatures()->includes(trg)\r\nand ( trg.kind<>PropertyKind::Operation)" });
		addAnnotation(getMClassifier_DoAction(), source, new String[] {
				"derive", "mcore::MClassifierAction::Do\n",
				"choiceConstruction",
				"let classActions:OrderedSet(MClassifierAction) =\n  if self.literal->isEmpty() \n      then OrderedSet{MClassifierAction::CreateInstance, if abstractDoAction then MClassifierAction::Abstract else null endif, MClassifierAction::Specialize,\n      MClassifierAction::StringAttribute, MClassifierAction::BooleanAttribute, MClassifierAction::IntegerAttribute, if not self.containingPackage.classifier->select( not literal->isEmpty())->isEmpty() then MClassifierAction::LiteralAttribute else null endif, MClassifierAction::RealAttribute, MClassifierAction::DateAttribute, MClassifierAction::UnaryReference, MClassifierAction::NaryReference,MClassifierAction::UnaryContainment, MClassifierAction::NaryContainment,MClassifierAction::OperationOneParameter, MClassifierAction::OperationTwoParameters, MClassifierAction::CompleteSemantics} \n      else OrderedSet{} endif in\n let withLabelAndConstraintAction:OrderedSet(MClassifierAction) =\n    if self.literal->isEmpty() then\n       if self.annotations.oclIsUndefined() \n          then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)\n        else if self.annotations.label.oclIsUndefined()\n          then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)\n          else  classActions->prepend(MClassifierAction::Constraint) endif endif \n    else classActions endif in\n      let intoAbstractOrConcrete: OrderedSet(MClassifierAction) =\n if self.literal->isEmpty() and (self.ownedProperty->notEmpty() or self.enforcedClass=true)\n   then withLabelAndConstraintAction->prepend(MClassifierAction::GroupOfProperties)\n   else  withLabelAndConstraintAction->append(MClassifierAction::Literal) endif in\n\tlet resolveContainer:OrderedSet(MClassifierAction) =\n   if self.literal->isEmpty()\n    then if self.abstractClass = true\n \t then intoAbstractOrConcrete->prepend(MClassifierAction::IntoConcreteClass)->prepend(MClassifierAction::Do) \n\t else intoAbstractOrConcrete->prepend(MClassifierAction::IntoAbstractClass)->prepend(MClassifierAction::Do) \n endif\n else intoAbstractOrConcrete->prepend(MClassifierAction::Do)\n endif in\n     if self.resolveContainerAction = true\n\tthen if resolveContainer->includes(MClassifierAction::Label)\n\t\tthen resolveContainer->insertAt(8, MClassifierAction::ResolveIntoContainer)\n\t\telse resolveContainer->insertAt(7, MClassifierAction::ResolveIntoContainer)\n\t\tendif\n\telse resolveContainer\n\tendif\n\n" });
		addAnnotation(getMClassifier_ResolveContainerAction(), source,
				new String[] { "derive",
						"let var1: OrderedSet(mcore::MProperty)  = let chain: OrderedSet(mcore::MProperty)  = classescontainedin.allFeaturesWithStorage()->asOrderedSet() in\nchain->iterate(i:mcore::MProperty; r: OrderedSet(mcore::MProperty)=OrderedSet{} | if i.oclIsKindOf(mcore::MProperty) then r->including(i.oclAsType(mcore::MProperty))->asOrderedSet() \n else r endif)->select(it: mcore::MProperty | let e0: Boolean = (let e0: Boolean = it.containment = true in \n if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.calculatedSingular = true in \n if e0.oclIsInvalid() then null else e0 endif) and (let e0: Boolean = it.type = containingClassifier in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofnull: Boolean = if (let e0: Boolean = let e0: Boolean = allFeaturesWithStorage()->asOrderedSet()->includesAll(allProperties()->asOrderedSet())   in \n if e0.oclIsInvalid() then null else e0 endif and let chain01: OrderedSet(mcore::MClassifier)  = classescontainedin->asOrderedSet() in\nif chain01->notEmpty().oclIsUndefined() \n then null \n else chain01->notEmpty()\n  endif and let chain02: OrderedSet(mcore::MClassifier)  = allDirectSubTypes()->asOrderedSet() in\nif chain02->isEmpty().oclIsUndefined() \n then null \n else chain02->isEmpty()\n  endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: OrderedSet(mcore::MProperty)  = var1 in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif\n  else false\nendif in\nresultofnull\n" });
		addAnnotation(getMClassifier_AbstractDoAction(), source, new String[] {
				"derive",
				"let var1: OrderedSet(mcore::MClassifier)  = allSuperTypes()->asOrderedSet()->select(it: mcore::MClassifier | let e0: Boolean = let chain01: Boolean = it.abstractClass in\nif chain01 = true then true else false \n  endif and (let e0: Boolean = it.name = (let e0: String = \'Abstract \'.concat(name) in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif) in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet resultofAbstractDoAction: Boolean = if (abstractClass) \n  =true \nthen false\n  else let chain: OrderedSet(mcore::MClassifier)  = var1 in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif\nendif in\nresultofAbstractDoAction\n" });
		addAnnotation(getMClassifier_ObjectReferences(), source, new String[] {
				"derive",
				"let var1: OrderedSet(mcore::objects::MObject)  = intelligentInstance->asOrderedSet()->select(it: mcore::objects::MObject | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.referencedBy->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (canApplyDefaultContainment) \n  =true \nthen var1\n  else OrderedSet{}\nendif\n" });
		addAnnotation(getMClassifier_NearestInstance(), source, new String[] {
				"derive",
				"if canApplyDefaultContainment = false then OrderedSet{}\r\nelse\r\nif allClassesContainedIn()->isEmpty() then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).instance\r\nelse\r\n if allClassesContainedIn().instance->isEmpty() then\r\nallClassesContainedIn()->first().nearestInstance\r\nelse\r\nallClassesContainedIn().instance\r\nendif endif endif" });
		addAnnotation(getMClassifier_StrictNearestInstance(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\nif strictAllClassesContainedIn->isEmpty() then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).strictInstance\r\n-- subpackages dont have resourceRootClass set, change that, saves runtime\r\nelse\r\n if strictAllClassesContainedIn.instance->isEmpty() then\r\nstrictAllClassesContainedIn->first().strictNearestInstance\r\nelse\r\nstrictAllClassesContainedIn.strictInstance\r\nendif endif endif" });
		addAnnotation(getMClassifier_StrictInstance(), source, new String[] {
				"derive",
				"let var1: OrderedSet(mcore::objects::MObject)  = instance->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = it.type = self in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (canApplyDefaultContainment) \n  =true \nthen var1\n  else OrderedSet{}\nendif\n" });
		addAnnotation(getMClassifier_IntelligentNearestInstance(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment= false then OrderedSet{}\r\nelse\r\nif intelligentAllClassesContainedIn->isEmpty()  then\r\nMClassifier.allInstances()->select(e:MClassifier|eContainer().oclAsType(MPackage).resourceRootClass = e ).intelligentInstance \r\n--subpackages don\'t have resourceRootClass set ,  change that  => saves runtime here\r\nelse\r\nif strictNearestInstance->isEmpty() then\r\n  if nearestInstance->isEmpty() then\r\n  intelligentAllClassesContainedIn->first().intelligentNearestInstance\r\n  else \r\n  nearestInstance\r\n  endif \r\n  \r\n else strictNearestInstance\r\n endif endif endif" });
		addAnnotation(getMClassifier_ObjectUnreferenced(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\nif  intelligentInstance->isEmpty() then OrderedSet{}\r\nelse if objectReferences->isEmpty() then intelligentInstance\r\nelse intelligentInstance->symmetricDifference(objectReferences) endif endif\r\n--add symmetric diff to MRules\r\nendif\r\n" });
		addAnnotation(getMClassifier_OutstandingToCopyObjects(), source,
				new String[] { "derive",
						"objectUnreferenced->asOrderedSet()->select(it: mcore::objects::MObject | let e0: Boolean = intelligentNearestInstance->asOrderedSet()->excludes(it.containingObject)   in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n\n" });
		addAnnotation(getMClassifier_PropertyInstances(), source, new String[] {
				"derive",
				"if canApplyDefaultContainment = false then OrderedSet{} else\r\nlet os:OrderedSet(objects::MPropertyInstance) = \r\n\tobjects::MPropertyInstance.allInstances()->asOrderedSet()\r\n\t--try to avoid allInstances\r\nin\r\nif os->isEmpty() then\r\n\tOrderedSet{} \r\nelse\r\n\tos->select(x:objects::MPropertyInstance| not( x.property.oclIsUndefined()) and x.property.simpleType = SimpleType::None and x.property.type = self)->asOrderedSet()\r\nendif endif" });
		addAnnotation(getMClassifier_ObjectReferencePropertyInstances(), source,
				new String[] { "derive",
						"if propertyInstances->isEmpty() then OrderedSet{} else\npropertyInstances->asOrderedSet()->select(it: mcore::objects::MPropertyInstance | let chain: OrderedSet(mcore::objects::MObjectReference)  = it.internalReferencedObject->asOrderedSet() in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n  endif\n" });
		addAnnotation(getMClassifier_IntelligentInstance(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\nif self.strictInstance->isEmpty() and self.instance->isEmpty() then OrderedSet{}\r\nelse\r\nif self.strictInstance->isEmpty() then self.instance else\r\nlet s: OrderedSet(MClassifier) = self.strictInstance->first().type.intelligentContainmentHierarchy in \r\nif self.instance->forAll(type.intelligentContainmentHierarchy = s ) then  instance\r\nelse \r\nself.strictInstance\r\nendif endif endif endif" });
		addAnnotation(getMClassifier_ContainmentHierarchy(), source,
				new String[] { "derive",
						" if allClassesContainedIn()->isEmpty() then OrderedSet{}  --isRoot\r\nelse\r\nif allClassesContainedIn()->first().allClassesContainedIn()->excludes(self) then\r\nallClassesContainedIn()->first()->asOrderedSet()->union(allClassesContainedIn()->first().containmentHierarchy)\r\nelse\r\nOrderedSet{} -- Circle\r\nendif\r\nendif \r\n\r\n  " });
		addAnnotation(getMClassifier_StrictAllClassesContainedIn(), source,
				new String[] { "derive",
						"\r\nallClassesContainedIn().property\r\n     ->select(p:MProperty| p.type=self.containingClassifier).containingClassifier->asSet()\r\n\r\n     --Need custom OCL because of Set" });
		addAnnotation(getMClassifier_StrictContainmentHierarchy(), source,
				new String[] { "derive",
						"if self.canApplyDefaultContainment then\r\nif strictAllClassesContainedIn->isEmpty() then OrderedSet{null}  --isRoot or non existing\r\nelse\r\nstrictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().strictContainmentHierarchy->asSequence())\r\nendif\r\n--need custom OCL because of asSequence\r\nelse\r\nOrderedSet{}endif" });
		addAnnotation(getMClassifier_IntelligentAllClassesContainedIn(), source,
				new String[] { "derive",
						"if (let chain: OrderedSet(mcore::MClassifier)  = strictAllClassesContainedIn->asOrderedSet() in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen allClassesContainedIn()->asOrderedSet()\n  else strictAllClassesContainedIn->asOrderedSet()\nendif\n" });
		addAnnotation(getMClassifier_IntelligentContainmentHierarchy(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment then\r\nif strictAllClassesContainedIn->isEmpty() and allClassesContainedIn()->isEmpty()\r\nthen OrderedSet{null}  --isRoot or non existing\r\nelse\r\nif  strictAllClassesContainedIn->isEmpty() then\r\nallClassesContainedIn()->first()->asSequence()->union(allClassesContainedIn()->first().intelligentContainmentHierarchy->asSequence())\r\nelse \r\nstrictAllClassesContainedIn->first()->asSequence()->union(strictAllClassesContainedIn->first().intelligentContainmentHierarchy->asSequence())\r\nendif endif\r\nelse\r\nOrderedSet{} endif\r\n\r\n-- Need sequence here again => Custom OCL" });
		addAnnotation(getMClassifier_InstantiationHierarchy(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\n\r\nlet s:Set(MClassifier) =\r\nintelligentContainmentHierarchy->asSet()->symmetricDifference(\r\nself.intelligentNearestInstance.type.intelligentContainmentHierarchy->asSet())\r\nin\r\nif s->isEmpty() then null else s\r\nendif endif\r\n--Custom OCL bc  symmetricDiff and Set" });
		addAnnotation(getMClassifier_InstantiationPropertyHierarchy(), source,
				new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\nif intelligentAllClassesContainedIn->isEmpty() then\r\nSequence{}\r\nelse\r\nlet s: Sequence(MProperty) = \r\nintelligentAllClassesContainedIn->first().property->asSequence() in\r\nif s->select(type = self)->isEmpty() then s->select(self.superType->includes(type)) else s->select(type = self) endif\r\n->asSequence()->union(intelligentAllClassesContainedIn->first().instantiationPropertyHierarchy->asSequence())\r\nendif endif\r\n\r\n--custom ocl bc  sequence " });
		addAnnotation(
				getMClassifier_IntelligentInstantiationPropertyHierarchy(),
				source, new String[] { "derive",
						"if canApplyDefaultContainment = false then OrderedSet{} else\r\n\r\nintelligentNearestInstance.type.instantiationPropertyHierarchy->asOrderedSet()->iterate(it: mcore::MProperty;  acc: Sequence(MProperty) = self.instantiationPropertyHierarchy->asSequence()  |  if acc->includes(it)  then acc->excluding(it) else acc endif)->asSequence() endif\r\n\r\n--custom ocl bc sequence" });
		addAnnotation(getMClassifier_Classescontainedin(), source,
				new String[] { "derive",
						"allClassesContainedIn()->asOrderedSet()\n" });
		addAnnotation(getMClassifier_CanApplyDefaultContainment(), source,
				new String[] { "derive",
						"self.containmentHierarchy->notEmpty() and\r\nlet s: OrderedSet(MClassifier) = \r\n\r\nif strictAllClassesContainedIn->isEmpty() then allClassesContainedIn()\r\nelse strictAllClassesContainedIn\r\nendif  in \r\n\r\ns->size()=1\r\n" });
		addAnnotation(getMClassifier_DerivedJsonSchema(), source, new String[] {
				"derive",
				"let lineBreak : String = \'\\n\' in\nlet space : String = \' \' in\nlet branch : String = \'{\' in\nlet definition : String = \'#/definitions/\' in\nlet closedBranch : String = \'}\' in\nlet mandatoryProperties : OrderedSet(MProperty)= self.property->asOrderedSet()->reject(e : MProperty| not(e.mandatory))in\nlet comma : String = \',\' in\n-- Classname\n\'\"\'.concat(self.eName).concat(\'\"\').concat(\':\').concat(branch)\n.concat(lineBreak)\n--Description\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n--Supertypes\n.concat(if self.superType->isEmpty() then \'\' else \'\"allOf\": [\'.concat(self.superType->iterate(classifier: MClassifier; s : String = \'\' | s.concat(\'{ \"$ref\": \').concat(\'\"\').concat(definition).concat(classifier.eName).concat(\'\"\').concat(\'}\').concat(if superType->indexOf(classifier) = self.superType->size() then \'\' else lineBreak.concat(\',\') endif))).concat(\']\').concat(comma).concat(lineBreak)endif)\n--Properties\n.concat(\'\"properties\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(self.property->iterate(property : MProperty;s : String= \'\' | s.concat(property.derivedJsonSchema).concat(if self.property->indexOf(property) = self.property->size() then \'\'.concat(lineBreak) else \',\'.concat(lineBreak) endif)))\n.concat(lineBreak)\n.concat(closedBranch)\n.concat(comma)\n.concat(lineBreak)\n-- Required fields\n.concat(\'\"required\": [\').concat(mandatoryProperties->iterate(property: MProperty;s : String = \'\'| if (property.mandatory) then s.concat(\'\"\').concat(property.eName).concat(\'\"\').concat(if mandatoryProperties->indexOf(property) = mandatoryProperties->size() then \'\' else \',\' endif) else s endif)).concat(\']\')\n.concat(closedBranch)" });
		addAnnotation(mLiteralEClass, source,
				new String[] { "label", "self.asString" });
		addAnnotation(getMLiteral_AsString(), source, new String[] { "derive",
				"self.containingEnumeration.eName.concat(\'::\').concat(self.eName)" });
		addAnnotation(getMLiteral_QualifiedName(), source, new String[] {
				"derive",
				"self.containingEnumeration.containingPackage.qualifiedName.concat(\'::\').concat(self.containingEnumeration.eName).concat(\'::\').concat(self.eName)" });
		addAnnotation(getMLiteral_ContainingEnumeration(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(MClassifier)" });
		addAnnotation(getMPropertiesContainer__AllPropertyAnnotations(), source,
				new String[] { "body",
						"let localAnnotations:Sequence(annotations::MPropertyAnnotations) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.propertyAnnotations->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MPropertyAnnotations) = \r\n  self.propertyGroup.allPropertyAnnotations()\r\nin  localAnnotations->union(groupedAnnotations)" });
		addAnnotation(getMPropertiesContainer__AllOperationAnnotations(),
				source, new String[] { "body",
						"let localAnnotations:Sequence(annotations::MOperationAnnotations) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.operationAnnotations->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MOperationAnnotations) = \r\n  self.propertyGroup.allOperationAnnotations()\r\nin  localAnnotations->union(groupedAnnotations)" });
		addAnnotation(getMPropertiesContainer_AllConstraintAnnotations(),
				source, new String[] { "derive",
						"\r\nlet localAnnotations:Sequence(annotations::MInvariantConstraint) = \r\n  if self.annotations.oclIsUndefined()\r\n   then Sequence{}\r\n   else self.annotations.constraint->asSequence() endif\r\nin \r\nlet groupedAnnotations: Sequence(annotations::MInvariantConstraint) = \r\n  self.propertyGroup.allConstraintAnnotations\r\nin  localAnnotations->union(groupedAnnotations)\r\n\r\n" });
		addAnnotation(getMPropertiesContainer_ContainingClassifier(), source,
				new String[] { "derive", "null" });
		addAnnotation(getMPropertiesContainer_OwnedProperty(), source,
				new String[] { "derive",
						"self.property->asSequence()->union(self.propertyGroup.ownedProperty)" });
		addAnnotation(getMPropertiesContainer_NrOfPropertiesAndSignatures(),
				source, new String[] { "derive",
						"let ps:OrderedSet(MProperty)= property->select(p:MProperty|p.kind=PropertyKind::Attribute or p.kind = PropertyKind::Reference) \r\nin\r\nlet\r\n nrOfPs:Integer = if ps->isEmpty() then 0 else  ps->size() endif\r\nin\r\nlet os:OrderedSet(MProperty)=  property->select(p:MProperty|p.kind=PropertyKind::Operation) \r\nin\r\nlet sigs:OrderedSet(MOperationSignature)= os.operationSignature->asOrderedSet()\r\nin\r\nlet nrOfSigs:Integer =if sigs->isEmpty() then 0 else  sigs->size() endif\r\nin\r\nlet sigs2:OrderedSet(MOperationSignature)= OrderedSet{} \r\nin\r\nlet nrOfSigs2:Integer = if sigs2->isEmpty() then 0 else sigs2->size() endif\r\nin\r\nlet nestedNr:Integer= \r\nif self.propertyGroup->isEmpty() then 0 else\r\npropertyGroup->iterate(g:MPropertiesGroup;i:Integer=0| i+ g.nrOfPropertiesAndSignatures) endif\r\nin\r\n\r\nnrOfPs+nrOfSigs+nestedNr" });
		addAnnotation(mPropertiesGroupEClass, source, new String[] { "label",
				"self.eName.concat(\'(\').concat(self.nrOfPropertiesAndSignatures.toString()).concat(\'):\')" });
		addAnnotation(
				getMPropertiesGroup__DoAction$Update__MPropertiesGroupAction(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(
				getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMPropertiesGroup_DoAction(), source, new String[] {
				"derive", "mcore::MPropertiesGroupAction::Do\n" });
		addAnnotation(mPropertyEClass, source,
				new String[] { "label", "eLabel " });
		addAnnotation(getMProperty__OppositeDefinition$Update__MProperty(),
				source, new String[] { "body", "null" });
		addAnnotation(getMProperty__PropertyBehavior$Update__PropertyBehavior(),
				source, new String[] { "body", "null" });
		addAnnotation(getMProperty__TypeDefinition$Update__MClassifier(),
				source, new String[] { "body", "null" });
		addAnnotation(getMProperty__DoAction$Update__MPropertyAction(), source,
				new String[] { "body", "null" });
		addAnnotation(getMProperty__SelectableType__MClassifier(), source,
				new String[] { "body",
						" mClassifier.selectableClassifier(\r\n  if type=null then OrderedSet{} else OrderedSet{type} endif, \r\n  typePackage)\r\n" });
		addAnnotation(getMProperty__AmbiguousPropertyName(), source,
				new String[] { "body",
						"if true then false else\r\nif self.containingClassifier.oclIsUndefined() then false else\r\nif self.isOperation then false else\r\nif self.containingClassifier.allProperties()->isEmpty() then false else\r\nself.containingClassifier.allProperties()\r\n  ->exists(p:MProperty|not (p=self) \r\n       and (not p.isOperation)\r\n       and p.sameName(self)\r\n       and if p.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(p.containingClassifier.containingPackage.derivedNamePrefix)\r\n       \t\t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix)\r\n       \t\t\t\t\tthen if self.containingClassifier.containingPackage.derivedNamePrefix = p.containingClassifier.containingPackage.derivedNamePrefix\r\n       \t\t\t\t\t\t\tthen true\r\n       \t\t\t\t\t\t\telse false\r\n       \t\t\t\t\t\t\tendif\r\n       \t\t\t\t\telse false\r\n       \t\t\t\t\tendif\r\n\t\t       \telse true\r\n\t\t       \tendif\r\n       \tand if stringEmpty(p.name) then \r\n                    p.type = self.type else true endif)\r\nendif endif endif\r\nendif" });
		addAnnotation(getMProperty__AmbiguousPropertyShortName(), source,
				new String[] { "body",
						"if true then false else\r\nif self.ambiguousPropertyName() then true else\r\nif self.stringEmpty(self.shortName) then false else\r\nif self.containingClassifier.oclIsUndefined() then false else\r\nif self.containingClassifier.allProperties()->isEmpty() then false else\r\nif self.isOperation\r\n  then\r\n    self.containingClassifier.allProperties()\r\n      ->exists(p:MProperty|\r\n         (not (p=self)) \r\n         and p.isOperation \r\n         and (\r\n              (p.sameShortName(self)\r\n                and (not p.sameName(self))\r\n                and (not p.stringEmpty(p.name)) \r\n                and (not  p.stringEmpty(self.name)))\r\n             or\r\n              ((not p.sameShortName(self))\r\n               and p.sameName(self)\r\n               and (not p.stringEmpty(p.name))\r\n               and (not  p.stringEmpty(self.name)))\r\n             )\r\n        )\r\n  else\r\n    self.containingClassifier.allProperties()\r\n      ->exists(p:MProperty|\r\n         (not (p=self)) \r\n         and (not p.isOperation) \r\n         and p.sameShortName(self)\r\n         and not p.stringEmpty(p.name) \r\n         and not  p.stringEmpty(self.name))\r\nendif endif endif endif endif\r\nendif" });
		addAnnotation(getMProperty__DoActionUpdate__MPropertyAction(), source,
				new String[] { "body", "null\n" });
		addAnnotation(
				getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMProperty__InvokeSetDoAction__MPropertyAction(),
				source, new String[] { "body", "null\n" });
		addAnnotation(getMProperty_OppositeDefinition(), source, new String[] {
				"derive", "opposite\n", "choiceConstruction",
				"--Manually changed in MPropertyItemProvider\r\nif self.kind <> PropertyKind::Reference \r\n  then OrderedSet{}\r\n  else if self.type.oclIsUndefined() \r\n    then OrderedSet{}\r\n   else self.type.allProperties()\r\n     ->select(p:MProperty|p.type=self.containingClassifier and p<>self and \r\n                      (if self.containment then not p.containment else true endif) \r\n                      )\r\n   endif \r\n endif" });
		addAnnotation(getMProperty_Opposite(), source, new String[] {
				"choiceConstruction",
				"if self.kind <> PropertyKind::Reference \r\n  then OrderedSet{}\r\n  else if self.type.oclIsUndefined() \r\n    then OrderedSet{}\r\n   else self.type.allProperties()\r\n     ->select(p:MProperty|p.type=self.containingClassifier and p<>self and \r\n                      (if self.containment then not p.containment else true endif) \r\n                      )\r\n   endif \r\n endif" });
		addAnnotation(getMProperty_HasStorage(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMProperty_Changeable(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMProperty_Persisted(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMProperty_PropertyBehavior(), source, new String[] {
				"derive",
				"if self.kind=PropertyKind::Operation\r\n  then PropertyBehavior::IsOperation\r\nelse if self.hasStorage \r\n  then if self.containment \r\n  \t\tthen PropertyBehavior::Contained\r\n  \t\telse  PropertyBehavior::Stored endif\r\n  else PropertyBehavior::Derived endif endif" });
		addAnnotation(getMProperty_IsOperation(), source,
				new String[] { "derive", "self.kind=PropertyKind::Operation" });
		addAnnotation(getMProperty_IsAttribute(), source,
				new String[] { "derive", "self.kind=PropertyKind::Attribute" });
		addAnnotation(getMProperty_IsReference(), source,
				new String[] { "derive", "self.kind=PropertyKind::Reference" });
		addAnnotation(getMProperty_Kind(), source, new String[] { "derive",
				"if  not self.operationSignature->isEmpty() then PropertyKind::Operation else\r\nif type.oclIsUndefined() then \r\n  if self.simpleType=SimpleType::None then PropertyKind::TypeMissing else \r\n  if self.hasSimpleDataType then PropertyKind::Attribute \r\n  else if self.hasSimpleModelingType then PropertyKind::Reference else PropertyKind::TypeMissing endif endif endif\r\nelse\r\nif type.kind = ClassifierKind::ClassType then PropertyKind::Reference else\r\nif type.kind =ClassifierKind::DataType or type.kind = ClassifierKind::Enumeration then PropertyKind::Attribute else\r\nPropertyKind::Undefined\r\nendif endif endif endif" });
		addAnnotation(getMProperty_TypeDefinition(), source,
				new String[] { "derive", "self.type", "choiceConstraint",
						"self.selectableType(trg)" });
		addAnnotation(getMProperty_ELabel(), source, new String[] { "derive",
				"if stringEmpty(name)\r\n  then eTypeLabel\r\n  else eNameForELabel\r\n           .concat(if self.kind=PropertyKind::Operation \r\n                             then \'(...)\' else \'\' endif)\r\n            .concat(if self.eTypeLabel=\'\'\r\n                             then \'\' else \':\'.concat(self.eTypeLabel) endif) endif" });
		addAnnotation(getMProperty_ENameForELabel(), source, new String[] {
				"derive",
				"let calculatedShortNameWithoutTypeName:String = \r\nif stringEmpty(shortName) then name else shortName endif in \r\nif self.isReference = true\r\n then if self.type.oclIsUndefined()\r\n \t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()\r\n  \t\t \telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n  \t\t \tendif\r\n \t\telse if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or \t\t\t\t\t\t\tself.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\t\t\t\tthen (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n \t\t\t\t\t \telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n   \t\t\t     endif\r\n   \t\tendif\r\n   \telse if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(calculatedShortNameWithoutTypeName)).camelCaseLower()\r\n   \t\t\telse calculatedShortNameWithoutTypeName.camelCaseLower()\r\n   \t\t\tendif\r\n\tendif " });
		addAnnotation(getMProperty_Id(), source, new String[] { "derive",
				"if self.containingClassifier.oclIsUndefined() then \'CONTAINER MISSING\' else \r\nlet pos:Integer=self.containingClassifier.ownedProperty->indexOf(self) in\r\nif pos<10 then \'0\'.concat(pos.toString()) else pos.toString() endif\r\nendif" });
		addAnnotation(getMProperty_DirectSemantics(), source, new String[] {
				"derive",
				"if self.containingClassifier.oclIsUndefined() \r\n  then null\r\n  else let pAs:OrderedSet(annotations::MPropertyAnnotations)\r\n            =  containingClassifier\r\n                   .allPropertyAnnotations()\r\n                        ->select(p:annotations::MPropertyAnnotations|\r\n                                   p.annotatedProperty=self) in \r\n                  if pAs->isEmpty() \r\n                    then null \r\n                    else pAs->first() endif endif\r\n" });
		addAnnotation(getMProperty_SemanticsSpecialization(), source,
				new String[] { "derive",
						"if self.containingClassifier.oclIsUndefined()\r\n  then OrderedSet{}\r\n  else containingClassifier\r\n            .allSubTypes()\r\n              .allPropertyAnnotations()\r\n                ->select(p:annotations::MPropertyAnnotations\r\n                                |p.annotatedProperty=self) endif" });
		addAnnotation(getMProperty_AllSignatures(), source, new String[] {
				"derive",
				"if self.containingClassifier.oclIsUndefined()\r\n  then Sequence{}\r\n  else self.containingClassifier.allProperties()\r\n  ->select(p:MProperty|p.kind=PropertyKind::Operation and p.calculatedShortName = self.calculatedShortName).operationSignature endif" });
		addAnnotation(getMProperty_DirectOperationSemantics(), source,
				new String[] { "derive",
						"if self.containingClassifier.oclIsUndefined() \r\n  then null\r\n  else let pAs:OrderedSet(annotations::MOperationAnnotations)\r\n            =  containingClassifier\r\n                   .allOperationAnnotations()\r\n                        ->select(p:annotations::MOperationAnnotations|\r\n                                   p.annotatedProperty=self) in \r\n                  if pAs->isEmpty() \r\n                    then null \r\n                    else pAs->first() endif endif\r\n" });
		addAnnotation(getMProperty_ContainingClassifier(), source,
				new String[] { "derive",
						"if self.eContainer().oclIsUndefined() then null else\r\nself.eContainer().oclAsType(MPropertiesContainer).containingClassifier\r\nendif" });
		addAnnotation(getMProperty_ContainingPropertiesContainer(), source,
				new String[] { "derive",
						"if self.eContainer().oclIsUndefined() then null else\r\nself.eContainer().oclAsType(MPropertiesContainer)\r\nendif" });
		addAnnotation(getMProperty_EKeyForPath(), source, new String[] {
				"choiceConstruction",
				"if self.type.oclIsUndefined() \r\n  then OrderedSet{}\r\n  else self.type.allFeaturesWithStorage()->select(p:MProperty|p.isAttribute) endif" });
		addAnnotation(getMProperty_DoAction(), source, new String[] { "derive",
				"mcore::MPropertyAction::Do\n", "choiceConstruction",
				"    let arityActionsInput: OrderedSet(MPropertyAction) = behaviorActions->prepend(MPropertyAction::Do) in\n    let simpleTypeActionsInput: OrderedSet(MPropertyAction) = \narityActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = arityActionsInput | ac->append(it)) in\n    let annotationActionsInput: OrderedSet(MPropertyAction) = \nsimpleTypeActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = simpleTypeActionsInput | ac->append(it)) in\n    let layoutActionsInput: OrderedSet(MPropertyAction) = \nannotationActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = annotationActionsInput | ac->append(it))\n->append(MPropertyAction::Highlight) in\n\n\tlet moveActionsInput: OrderedSet(MPropertyAction) = \nlayoutActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = layoutActionsInput | ac->append(it)) in \nmoveActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = moveActionsInput | ac->append(it))->append(MPropertyAction::AddUpdateAnnotation)\n" });
		addAnnotation(getMProperty_BehaviorActions(), source, new String[] {
				"derive",
				"let a:OrderedSet(MPropertyAction) =\nif (propertyBehavior = PropertyBehavior::Stored) \n   then  if self.kind= PropertyKind::Reference\n                then OrderedSet{MPropertyAction::IntoContained, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}\n                else OrderedSet{MPropertyAction::IntoDerived, MPropertyAction::IntoOperation} endif\nelse if (propertyBehavior = PropertyBehavior::Contained)\n   then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived, MPropertyAction::IntoOperation}\nelse if propertyBehavior = PropertyBehavior::Derived\n   then if self.kind= PropertyKind::Reference\n                then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoOperation}\n                else OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoOperation} endif\nelse if propertyBehavior = PropertyBehavior::IsOperation\n   then if self.hasSimpleModelingType \n                 then OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}\n           else if self.type.oclIsUndefined()\n                 then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} \n           else if self.type.kind = ClassifierKind::ClassType\n                 then  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoContained, MPropertyAction::IntoDerived}\n                 else  OrderedSet{MPropertyAction::IntoStored, MPropertyAction::IntoDerived} endif endif endif\n    else OrderedSet{} endif endif endif endif\n in\n let b:OrderedSet(MPropertyAction) =\n   let sts:OrderedSet(MClassifier) = self.containingClassifier.allSubTypes() in\n   if sts->isEmpty()\n        then a\n   else if propertyBehavior = PropertyBehavior::Derived\n         then a->prepend(MPropertyAction::SpecializeResult)\n   else if propertyBehavior = PropertyBehavior::Stored or propertyBehavior = PropertyBehavior::Contained\n         then a->prepend(MPropertyAction::SpecializeInitOrder)->prepend(MPropertyAction::SpecializeInitValue)\n         else a\n   endif endif endif in\nb\n" });
		addAnnotation(getMProperty_LayoutActions(), source, new String[] {
				"derive",
				"if (kind = mcore::PropertyKind::Attribute) or \n   (kind = mcore::PropertyKind::Reference)\n   then\n      let a:mcore::annotations::MPropertyAnnotations= directSemantics in \n       if a.oclIsUndefined()\n         then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)\n            then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}\n            else /* In property group */\n                    OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif\n       else if a.layout.oclIsUndefined()\n          then if containingPropertiesContainer.oclIsTypeOf(mcore::MClassifier)\n            then OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::HideInTableEditor}\n            else /* In property group */\n                    OrderedSet{MPropertyAction::HideInPropertiesView, MPropertyAction::ShowInTableEditor} endif\n       else OrderedSet{\n                                   if a.layout.hideInPropertyView\n                                       then MPropertyAction::ShowInPropertiesView\n                                       else MPropertyAction::HideInPropertiesView endif,\n                                   if a.layout.hideInTable\n                                       then MPropertyAction::ShowInTableEditor\n                                       else MPropertyAction::HideInTableEditor endif\n                                   } endif endif\n    else OrderedSet{} endif      " });
		addAnnotation(getMProperty_ArityActions(), source, new String[] {
				"derive",
				"if self.singular = true \n  then OrderedSet{MPropertyAction::IntoNary}\n  else OrderedSet{MPropertyAction::IntoUnary} endif" });
		addAnnotation(getMProperty_SimpleTypeActions(), source, new String[] {
				"derive",
				"if self.calculatedSimpleType = SimpleType::String \n   then OrderedSet{MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Boolean\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Integer\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Double\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoDateAttribute}\nelse if self.calculatedSimpleType = SimpleType::Date\n  then OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute}  \n  else OrderedSet{MPropertyAction::IntoStringAttribute, MPropertyAction::IntoBooleanFlag, MPropertyAction::IntoIntegerAttribute, MPropertyAction::IntoRealAttribute, MPropertyAction::IntoDateAttribute} endif endif endif endif endif" });
		addAnnotation(getMProperty_AnnotationActions(), source, new String[] {
				"derive",
				"let a:OrderedSet(MPropertyAction) =\nif propertyBehavior = PropertyBehavior::Derived \n    or propertyBehavior = PropertyBehavior::IsOperation\n  then OrderedSet{MPropertyAction::Result}\n  else OrderedSet{MPropertyAction::InitValue, MPropertyAction::InitOrder} endif \nin\nif changeable\n  then a->append(MPropertyAction::ConstructChoice)\n            ->append(MPropertyAction::ConstrainChoice)\n  else a endif\n" });
		addAnnotation(getMProperty_MoveActions(), source, new String[] {
				"derive",
				"let e0: OrderedSet(MPropertyAction) = if self.containingClassifier.indentLevel = (self.indentLevel-1)\r\nthen if self.hasStorage = true and self.simpleType = SimpleType::String\r\n   \t\t\tthen if self.containingClassifier.propertyGroup->isEmpty()\r\n   \t\t\t\t\t\tthen OrderedSet{MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}\r\n   \t\t\t\t\t\telse OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup, MPropertyAction::ClassWithStringProperty}\r\n   \t\t\t\t\t\tendif   \t\t\t\r\n   \t\t\telse if self.containingClassifier.propertyGroup->isEmpty()\r\n   \t\t\t\t\t\tthen OrderedSet{MPropertyAction::MoveToNewGroup}\r\n   \t\t\t\t\t\telse OrderedSet{MPropertyAction::MoveToLastGroup, MPropertyAction::MoveToNewGroup}\r\n   \t\t\t\t\t\tendif  endif\r\nelse if self.hasStorage = true and self.simpleType = SimpleType::String\r\n   \t\t\tthen OrderedSet{MPropertyAction::ClassWithStringProperty}\r\n   \t\t\telse OrderedSet{} endif\r\nendif\r\nin if self.propertyBehavior = PropertyBehavior::Derived and not self.directSemantics.oclIsUndefined()\r\n\tthen if self.containingClassifier.allDirectSubTypes()->isEmpty()\r\n\t\t\t\tthen if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\r\n\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)\r\n\t\t\t\t\t\t\telse e0\r\n\t\t\t\t\t\t\tendif\r\n\t\t\t\telse \r\n\t\t\t\tlet subTypes: OrderedSet(mcore::MClassifier)  =\r\n\t\t\t\t\tif containingClassifier.oclIsUndefined()\r\n  \t\t\t\t\t\tthen OrderedSet{}\r\n  \t\t\t\t\t\telse containingClassifier.allDirectSubTypes()\r\n\t\t\t\t\tendif in let subTypesAnno: OrderedSet(mcore::annotations::MClassifierAnnotations)  = subTypes.annotations->asOrderedSet() in\r\n\t\t\t\t\tlet alreadySpecialized: OrderedSet(mcore::annotations::MPropertyAnnotations)  = subTypesAnno.propertyAnnotations->asOrderedSet()->select(it: mcore::annotations::MPropertyAnnotations | let e1: Boolean = it.property = self in \r\n\t\t\t\t\tif e1.oclIsInvalid()\r\n\t\t\t\t\t\tthen null\r\n\t\t\t\t\t\telse e1\r\n\t\t\t\t\tendif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\r\n\t\t\t\t\tlet leftToSpecialize: Boolean = \r\n\t\t\t\t\tif (let e1: Boolean = let chain01: OrderedSet(mcore::MClassifier)  = subTypes in\r\n\t\t\t\t\t\t\tif chain01->size().oclIsUndefined() \r\n \t\t\t\t\t\t\t\tthen null \r\n \t\t\t\t\t\t\t\telse chain01->size()\r\n  \t\t\t\t\t\t\tendif = let chain02: OrderedSet(mcore::annotations::MPropertyAnnotations)  = alreadySpecialized in\r\n\t\t\t\t\t\t\tif chain02->size().oclIsUndefined() \r\n \t\t\t\t\t\t\t\tthen null \r\n \t\t\t\t\t\t\t\telse chain02->size()\r\n  \t\t\t\t\t\t\tendif in \r\n \t\t\t\t\t\t\tif e1.oclIsInvalid()\r\n \t\t\t\t\t\t\t\tthen null\r\n \t\t\t\t\t\t\t\telse e1\r\n \t\t\t\t\t\t\tendif) \r\n  \t\t\t\t\t=true \r\n\t\t\t\t\t\tthen false\r\n  \t\t\t\t\t\telse true\r\n\t\t\t\t\tendif in\r\n\t\t\t\t\tif leftToSpecialize\r\n\t\t\t\t\t\tthen if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\r\n\t\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)->prepend(MPropertyAction::Specialize)\r\n\t\t\t\t\t\t\t\telse e0->prepend(MPropertyAction::Specialize)\r\n\t\t\t\t\t\t\t\tendif\r\n\t\t\t\t\t\telse if not self.containingClassifier.allSuperTypes()->isEmpty() and self.containingClassifier.allSuperTypes()->size() = 1\t\t\t\t\t\t\t\t\t\t\r\n\t\t\t\t\t\t\t\t\tthen e0->prepend(MPropertyAction::Abstract)\r\n\t\t\t\t\t\t\t\t\telse e0\r\n\t\t\t\t\t\t\t\t\tendif\r\n\t\t\t\t\t\tendif\r\n\t\t\tendif\r\nelse e0\r\nendif" });
		addAnnotation(getMProperty_DerivedJsonSchema(), source, new String[] {
				"derive",
				"let space : String = \' \' in\nlet branch : String = \'{\' in\nlet lineBreak : String = \'\\n\' in\nlet closedBranch : String = \'}\' in\nlet comma : String = \',\' in\nlet definitions : String = \'#/definitions/\' in\nif (self.hasStorage = false) then \'\'\nelse\n\'\"\'.concat(self.eName).concat(\'\":\').concat(space).concat(branch)\n.concat(lineBreak)\n.concat(\nif(self.calculatedSingular = false) then\n\'\"type\":\'.concat(space).concat(\'\"array\"\').concat(comma).concat(lineBreak).concat(\nif(self.mandatory) then lineBreak.concat(\'\"minItems\":\').concat(space).concat(\'\"1\"\').concat(comma).concat(lineBreak) else \'\' endif)\n.concat(\'\"items\":\').concat(branch).concat(lineBreak)  else \'\' endif)\n.concat(\'\"description\": \').concat(\'\"\').concat(if self.description.oclIsUndefined() then \'no description\' else self.description endif).concat(\'\"\').concat(comma)\n.concat(lineBreak)\n.concat(\nif(self.hasSimpleDataType) then \n\'\"type\":\'.concat(space).concat(\'\"string\"\')\nelse\n\'\"$ref\":\'.concat(space).concat(\'\"\').concat(definitions).concat(self.type.eName).concat(\'\"\')\nendif\n)\nendif\n.concat(\nif(self.calculatedSingular = false) then\nlineBreak.concat(closedBranch)\nelse \'\'\nendif\n.concat(lineBreak).concat(closedBranch)\n)" });
		addAnnotation(mOperationSignatureEClass, source,
				new String[] { "label", "eLabel" });
		addAnnotation(
				getMOperationSignature__DoAction$Update__MOperationSignatureAction(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMOperationSignature__SameSignature__MOperationSignature(),
				source, new String[] { "body",
						"if self.operation <> s.operation then false else\r\nself.parameter->size() = s.parameter->size() and\r\nself.parameter->forAll(p:MParameter|p.type=s.parameter->at(self.parameter->indexOf(p)).type) \r\nendif" });
		addAnnotation(getMOperationSignature__SignatureAsString(), source,
				new String[] { "body",
						"\'(\'.concat(\r\nlet ps:OrderedSet(MParameter) = self.parameter in\r\nif ps->isEmpty()then\'\' else\r\nps->iterate(p:MParameter;res:String=\'\' |\r\n  (if res=\'\' then \'\' else res.concat(\', \') endif)\r\n     .concat(p.eTypeName).concat(if p.singular then \'\' else \'*\' endif))\r\nendif).concat(\')\')" });
		addAnnotation(getMOperationSignature_Operation(), source, new String[] {
				"derive",
				"if self.eContainer().oclIsTypeOf(MProperty) then\r\nself.eContainer().oclAsType(MProperty) else null\r\n endif" });
		addAnnotation(getMOperationSignature_ContainingClassifier(), source,
				new String[] { "derive",
						"if self.eContainer().oclIsUndefined() then null else\r\nif self.eContainer().oclIsKindOf(MProperty) then \r\nself.eContainer().oclAsType(MProperty).containingClassifier else null endif endif" });
		addAnnotation(getMOperationSignature_ELabel(), source, new String[] {
				"derive",
				"(if self.operation.oclIsUndefined() then \r\n    \'OPERATION MISSING\' else \r\n     self.operation.eName endif)\r\n.concat(self.signatureAsString())\r\n.concat(\':\')\r\n.concat(self.operation.eTypeLabel)" });
		addAnnotation(getMOperationSignature_DoAction(), source, new String[] {
				"derive", "mcore::MOperationSignatureAction::Do\n" });
		addAnnotation(getMTyped__MultiplicityCase$Update__MultiplicityCase(),
				source, new String[] { "body", "null" });
		addAnnotation(
				getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean(),
				source, new String[] { "body",
						"if  mClassifier.oclIsUndefined() and (simpleType = SimpleType::None)  \r\n  then \'MISSING TYPE\' \r\n  else let t: String = \r\n      if  simpleType<>SimpleType::None\r\n        then \r\n          if simpleType=SimpleType::Double  then \'Real\' \r\n          else if simpleType=SimpleType::Object then \'ecore::EObject\'\r\n          else if simpleType=SimpleType::Annotation then \'ecore::EAnnotation\'\r\n          else if simpleType=SimpleType::Attribute then \'ecore::EAttribute\'\r\n          else if simpleType=SimpleType::Class then \'ecore::EClass\'\r\n          else if simpleType=SimpleType::Classifier then \'ecore::EClassifier\'\r\n          else if simpleType=SimpleType::DataType then \'ecore::EDataType\'\r\n          else if simpleType=SimpleType::Enumeration then \'ecore::Enumeration\'\r\n          else if simpleType=SimpleType::Feature then \'ecore::EStructuralFeature\'\r\n          else if simpleType=SimpleType::Literal then \'ecore::EEnumLiteral\'\r\n          else if simpleType=SimpleType::KeyValue then \'ecore::EStringToStringMapEntry\'\r\n          else if simpleType=SimpleType::NamedElement then \'ecore::NamedElement\'\r\n          else if simpleType=SimpleType::Operation then \'ecore::EOperation\'\r\n          else if simpleType=SimpleType::Package then \'ecore::EPackage\'\r\n          else if simpleType=SimpleType::Parameter then \'ecore::EParameter\'\r\n           else if simpleType=SimpleType::Reference then \'ecore::EReference\'\r\n           else if simpleType=SimpleType::TypedElement then \'ecore::ETypedElement\'\r\n           else if simpleType = SimpleType::Date then \'ecore::EDate\' \r\n            else simpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif\r\n        else \r\n          /* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified \r\n\t      let pkg: String = \r\n\t        if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat(\'::\')\r\n\t          else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then \'\' else mClassifier.containingPackage.eName.allLowerCase().concat(\'::\') endif \r\n\t        endif\r\n\t      in pkg.concat(mClassifier.eName) \r\n\t      NEW Create string with all package qualifiers */\r\n\t      let pkg:String=\r\n\t       if mClassifier.containingClassifier   =null then \'\'\r\n\t       else mClassifier.containingPackage.qualifiedName.concat(\'::\' ) endif                            in\r\n\t       pkg.concat(mClassifier.eName)\r\n      endif\r\n\tin \r\n\r\n    if  singular.oclIsUndefined() then \'MISSING ARITY INFO\' else \r\n      if singular=true then t else \'OrderedSet(\'.concat(t).concat(\') \') endif\r\n    endif\r\nendif\r\n\r\n" });
		addAnnotation(getMTyped_MultiplicityAsString(), source, new String[] {
				"derive",
				"(if self.calculatedMandatory then \'[1..\' else \'[0..\' endif)\r\n.concat(if self.calculatedSingular then \'1]\' else \'*]\' endif)" });
		addAnnotation(getMTyped_CalculatedType(), source,
				new String[] { "derive", "let res:MClassifier=null in res" });
		addAnnotation(getMTyped_CalculatedMandatory(), source,
				new String[] { "derive", "false" });
		addAnnotation(getMTyped_CalculatedSingular(), source,
				new String[] { "derive", "false" });
		addAnnotation(getMTyped_CalculatedTypePackage(), source, new String[] {
				"derive",
				"if self.calculatedType.oclIsUndefined() then null else self.calculatedType.containingPackage endif" });
		addAnnotation(getMTyped_VoidTypeAllowed(), source,
				new String[] { "derive", "false" });
		addAnnotation(getMTyped_CalculatedSimpleType(), source,
				new String[] { "derive", "mcore::SimpleType::None\n" });
		addAnnotation(getMTyped_MultiplicityCase(), source, new String[] {
				"derive",
				"if self.calculatedMandatory and self.calculatedSingular\r\n  then MultiplicityCase::OneOne\r\n  else if (not self.calculatedMandatory) and self.calculatedSingular\r\n    then MultiplicityCase::ZeroOne\r\n    else if self.calculatedMandatory and (not self.calculatedSingular)\r\n      then MultiplicityCase::OneMany\r\n      else MultiplicityCase::ZeroMany endif endif endif" });
		addAnnotation(getMExplicitlyTyped_Type(), source, new String[] {
				"choiceConstraint",
				"trg.selectableClassifier(\r\n  if self.type.oclIsUndefined() then OrderedSet{} else OrderedSet{self.type} endif,\r\n  self.typePackage)" });
		addAnnotation(getMExplicitlyTyped_Singular(), source,
				new String[] { "initValue", "true" });
		addAnnotation(getMExplicitlyTyped_ETypeName(), source, new String[] {
				"derive",
				"if type.oclIsUndefined() then\r\n  if  simpleType=SimpleType::None then \r\n    \'TYPE_MISSING\'\r\n  else  simpleTypeString endif\r\nelse type.eName endif " });
		addAnnotation(getMExplicitlyTyped_ETypeLabel(), source, new String[] {
				"derive",
				"if self.type.oclIsUndefined() and self.simpleType=SimpleType::None and self.voidTypeAllowed then \'\' else eTypeName.concat(if self.singular then \'\' else \'*\' endif) endif" });
		addAnnotation(getMExplicitlyTyped_CorrectlyTyped(), source,
				new String[] { "derive", "simpleTypeIsCorrect" });
		addAnnotation(getMExplicitlyTypedAndNamed__NameFromType(), source,
				new String[] { "body",
						"if not type.oclIsUndefined() \r\n  then type.calculatedName\r\n  else if  self.simpleType <> SimpleType::None\r\n      then self.simpleType.toString().concat(\' Property\')\r\n      else \' ERROR UNNAMED UNTYPED\' endif endif" });
		addAnnotation(getMExplicitlyTypedAndNamed__ShortNameFromType(), source,
				new String[] { "body",
						"if not type.oclIsUndefined() \r\n  then type.calculatedShortName\r\n  else if  self.simpleType <> SimpleType::None\r\n      then self.simpleType.toString().concat(\' Property\')\r\n      else \' ERROR UNNAMED UNTYPED\' endif endif" });
		addAnnotation(getMExplicitlyTypedAndNamed_AppendTypeNameToName(),
				source, new String[] { "initValue", "true\n" });
		addAnnotation(mParameterEClass, source,
				new String[] { "label", "eLabel" });
		addAnnotation(getMParameter__DoAction$Update__MParameterAction(),
				source, new String[] { "body", "null" });
		addAnnotation(getMParameter__AmbiguousParameterName(), source,
				new String[] { "body",
						"if true then false else\r\nif self.containingSignature.oclIsUndefined() then false else\r\nif self.containingSignature.parameter->isEmpty() then false else\r\nself.containingSignature.parameter\r\n  ->exists(p:MParameter|not (p=self) \r\n       and p.sameName(self)\r\n       and (if stringEmpty(p.name) then \r\n                    p.type = self.type else true endif))\r\nendif endif\r\nendif" });
		addAnnotation(getMParameter__AmbiguousParameterShortName(), source,
				new String[] { "body",
						"if true then false else \r\nif not self.ambiguousParameterName() then false else\r\nif self.containingSignature.oclIsUndefined() then false else\r\nif self.containingSignature.parameter->isEmpty() then false else\r\nself.containingSignature.parameter\r\n  ->exists(p:MParameter|not (p=self) \r\n       and p.sameShortName(self))\r\nendif endif endif\r\nendif" });
		addAnnotation(getMParameter_ContainingSignature(), source,
				new String[] { "derive",
						"self.eContainer().oclAsType(MOperationSignature)" });
		addAnnotation(getMParameter_ELabel(), source, new String[] { "derive",
				"if stringEmpty(name)\r\n  then self.eTypeLabel \r\n  else eName\r\n                     .concat(if self.eTypeLabel=\'\'\r\n                             then \'\' else \':\'.concat(self.eTypeLabel) endif) endif" });
		addAnnotation(getMParameter_SignatureAsString(), source, new String[] {
				"derive",
				"if self.type.oclIsUndefined() then \'TYPE_MISSING\' else self.type.eName.concat(self.multiplicityAsString) endif" });
		addAnnotation(getMParameter_DoAction(), source, new String[] { "derive",
				"let do: mcore::MParameterAction = mcore::MParameterAction::Do in\ndo\n\n" });
		addAnnotation(getMEditor__ResetEditor(), source,
				new String[] { "body", "null\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/UUID</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createUUIDAnnotations() {
		String source = "http://www.xocl.org/UUID";
		addAnnotation(this, source, new String[] { "useUUIDs", "true",
				"uuidAttributeName", "_uuid" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(mRepositoryEClass, source,
				new String[] { "kindLabelDerive", "\'Repository\'\n" });
		addAnnotation(mComponentEClass, source,
				new String[] { "fullLabelDerive",
						"\'[component] \'.concat(self.calculatedName)",
						"kindLabelDerive", "\'Component\'\n", "nameInitValue",
						"\'My Component\'" });
		addAnnotation(mPackageEClass, source, new String[] { "fullLabelDerive",
				"self.localStructuralName", "kindLabelDerive", "\'Package\'\n",
				"eNameDerive",
				"if self.representsCorePackage then \'ecore\' else \r\nif stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseLower() \r\nelse self.specialEName.camelCaseLower()\r\nendif endif",
				"localStructuralNameDerive",
				"(if self.isRootPackage then \'\' else self.parent.localStructuralName.concat(\'/\') endif).concat(self.eName)" });
		addAnnotation(mClassifierEClass, source,
				new String[] { "fullLabelDerive",
						"(if kind=ClassifierKind::ClassType then \'[class] \' else\r\nif kind=ClassifierKind::DataType then  \'[data] \' else \r\nif kind=ClassifierKind::Enumeration then \'[enum]\' else\r\n\'[IMPOSSIBLE]\'endif endif endif)\r\n.concat(\'localstructuralname\')",
						"kindLabelDerive",
						"if self.kind = mcore::ClassifierKind::ClassType then\r\n  if self.eInterface=true then \'Interface\' else\r\n  if self.abstractClass = true then \'Abstract Class\' else\r\n       \'Class\' endif endif else\r\nif self.kind = mcore::ClassifierKind::DataType then \'Data Type\' else\r\nif self.kind = mcore::ClassifierKind::Enumeration then \'Enumeration\' else\r\n\'ERROR\' endif endif endif",
						"correctNameDerive",
						"(not ambiguousClassifierName()) and\r\nif self.hasSimpleModelingType then self.name=self.simpleTypeAsString(simpleType) else  not self.stringEmpty(self.name) endif",
						"containingClassifierDerive", "self",
						"simpleTypeIsCorrectDerive",
						"if self.kind = ClassifierKind::DataType then \r\n  self.hasSimpleDataType\r\nelse if self.kind = ClassifierKind::Enumeration then self.simpleType=SimpleType::None \r\nelse self.simpleType=SimpleType::None or \r\nself.hasSimpleModelingType endif endif",
						"eNameDerive",
						"if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen if hasSimpleModelingType then simpleTypeString else\r\n\tif stringEmpty(containingPackage.derivedNamePrefix)\r\n\tthen calculatedShortName.camelCaseUpper() \r\n\telse containingPackage.derivedNamePrefix.concat(calculatedShortName).camelCaseUpper()\r\n\tendif endif\r\nelse specialEName.camelCaseUpper()\r\nendif",
						"localStructuralNameDerive",
						"containingPackage.localStructuralName\r\n.concat(\'/\')\r\n.concat(self.eName)",
						"calculatedNameDerive",
						"name.concat(if ambiguousClassifierName() then \'  (AMBIGUOUS)\' else \'\' endif)",
						"simpleTypeInitValue",
						"let stringType: SimpleType = SimpleType::None in  stringType",
						"propertyInitValue",
						"OrderedSet{ defaultPropertyValue() }" });
		addAnnotation(mLiteralEClass, source,
				new String[] { "eNameDerive",
						"if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen calculatedShortName.camelCaseUpper()\r\nelse specialEName.camelCaseUpper()\r\nendif",
						"kindLabelDerive", "\'Literal\'\n" });
		addAnnotation(mPropertiesGroupEClass, source, new String[] {
				"kindLabelDerive", "\'Group\'\n", "containingClassifierDerive",
				"if self.eContainer().oclIsUndefined() then null else\r\n  self.eContainer().oclAsType(MPropertiesContainer).containingClassifier\r\nendif",
				"eNameDerive",
				"if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseUpper()\r\nelse self.specialEName.camelCaseUpper()\r\nendif" });
		addAnnotation(mPropertyEClass, source, new String[] { "fullLabelDerive",
				"(if kind=PropertyKind::Attribute then \'[attribute] \' else\r\nif kind=PropertyKind::Reference then \'[reference] \' else\r\nif kind=PropertyKind::Operation then \'[operation] \' else\r\nif kind=PropertyKind::Ambiguous then \'[AMBIGUOUS]\' else\r\nif kind=PropertyKind::TypeMissing then \'[TYPE MISSING] \' else\r\nif kind=PropertyKind::Undefined then \'[UNDEFINED] \' else\r\n\'[IMPOSSIBLE] \' endif endif endif endif endif endif)\r\n.concat(self.localStructuralName)\r\n.concat(\r\nif kind = PropertyKind::Operation then \'(...)\' else \'\' endif) \r\n",
				"kindLabelDerive",
				"if self.propertyBehavior = mcore::PropertyBehavior::Contained then\r\nif (not(self.opposite.oclIsUndefined()) and (self.opposite.multiplicityCase = MultiplicityCase::OneOne))\r\nthen \'Composition\'\r\nelse\r\n\'Containment\'\r\nendif\r\n else\r\nif self.propertyBehavior = mcore::PropertyBehavior::Derived then\r\nlet changeableString: String = if (self.changeable) then \' Change. \' else \' \'  endif in\r\n   if self.kind = mcore::PropertyKind::Attribute then \'Derived\'.concat(changeableString).concat(\'Attr.\') else\r\n   if self.kind = mcore::PropertyKind::Reference then \'Derived\'.concat(changeableString).concat(\'Ref.\') else \'Derived WRONG\' endif endif \r\nelse\r\nif self.kind = mcore::PropertyKind::Attribute then \'Attribute\' else\r\nif self.kind = mcore::PropertyKind::Reference then\r\n\'Reference\'\r\nelse\r\nif self.kind = mcore::PropertyKind::Operation then \'Operation\' else\r\n\'ERROR\' endif endif endif endif endif",
				"correctNameDerive",
				"(not ambiguousPropertyName()) and\r\nif self.hasSimpleDataType then not self.stringEmpty(name) else true endif",
				"correctShortNameDerive", "not ambiguousPropertyShortName()",
				"calculatedMandatoryDerive", "self.mandatory",
				"calculatedTypeDerive", "self.type", "calculatedSingularDerive",
				"self.singular", "simpleTypeIsCorrectDerive",
				"if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... */",
				"voidTypeAllowedDerive", "self.kind=PropertyKind::Operation",
				"calculatedSimpleTypeDerive", "simpleType\n",
				"localStructuralNameDerive",
				"if self.containingClassifier.oclIsUndefined() then \'\' else\r\nself.containingClassifier.localStructuralName\r\n.concat(\'/\')\r\n.concat(self.calculatedShortName.camelCaseUpper())\r\nendif",
				"calculatedNameDerive",
				"(if not stringEmpty(name) then name else  \r\nif hasSimpleDataType then \'NAMEMISSING\' else nameFromType() endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   \' \'.concat(nameFromType())\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if ambiguousPropertyName() then \'  (AMBIGUOUS)\' else \'\' endif)",
				"calculatedShortNameDerive",
				"(if stringEmpty(name) then shortNameFromType()\r\nelse\r\n  if stringEmpty(shortName) then name\r\n  else shortName\r\n endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                       if self.hasSimpleDataType or self.hasSimpleModelingType then \'\'\r\n                                else   \' \'.concat(shortNameFromType())\r\n                       endif\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if self.ambiguousPropertyShortName() then \'( AMBIGUOUS)\' else \'\' endif)",
				"eNameDerive",
				"if stringEmpty(self.specialEName) = true \r\nthen if self.isReference = true\r\n then if self.type.oclIsUndefined()\r\n \t\tthen if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n  \t\t \telse self.calculatedShortName.camelCaseLower()\r\n  \t\t \tendif\r\n \t\telse if stringEmpty(self.calculatedShortName) and ( not (stringEmpty(self.type.specialEName) or stringEmpty(self.type.specialEName.trim()))) and true\r\n  \t\t\t\tthen self.type.specialEName.camelCaseLower()\r\n  \t\t\t\telse if self.calculatedTypePackage.containingComponent.namePrefixForFeatures = true and not (stringEmpty(self.calculatedTypePackage.derivedNamePrefix) or \t\t\t\t\t\t\tself.calculatedTypePackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\t\t\t\tthen (self.calculatedTypePackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n \t\t\t\t\t \telse self.calculatedShortName.camelCaseLower()\r\n   \t\t\t\t\t\tendif\r\n   \t\t\t\tendif\r\n   \t\tendif\r\n   \telse if self.containingClassifier.containingPackage.containingComponent.namePrefixForFeatures = true and not \t\t\t(stringEmpty(self.containingClassifier.containingPackage.derivedNamePrefix) or self.containingClassifier.containingPackage.derivedNamePrefix.oclIsUndefined())\r\n   \t\t\tthen (self.containingClassifier.containingPackage.derivedNamePrefix.concat(self.calculatedShortName)).camelCaseLower()\r\n   \t\t\telse self.calculatedShortName.camelCaseLower()\r\n   \t\t\tendif\r\n\tendif \r\nelse self.specialEName.camelCaseLower()\r\nendif",
				"typeChoiceConstraint", "true" });
		addAnnotation(mOperationSignatureEClass, source, new String[] {
				"kindLabelDerive", "\'Signature\'\n",
				"calculatedMandatoryDerive", "operation.calculatedMandatory",
				"calculatedSingularDerive", "operation.calculatedSingular",
				"calculatedTypeDerive", "operation.calculatedType" });
		addAnnotation(mExplicitlyTypedEClass, source,
				new String[] { "calculatedMandatoryDerive", "self.mandatory",
						"calculatedTypeDerive", "self.type",
						"calculatedSingularDerive", "self.singular",
						"simpleTypeIsCorrectDerive",
						"if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* does not work because of bug, is repeated in MProperty and MParameter... */",
						"calculatedSimpleTypeDerive", "simpleType\n" });
		addAnnotation(mVariableEClass, source, new String[] { "fullLabelDerive",
				"\'TODO\'", "eNameDerive",
				"if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())\r\nthen self.calculatedShortName.camelCaseLower()\r\nelse self.specialEName.camelCaseLower()\r\nendif" });
		addAnnotation(mParameterEClass, source, new String[] {
				"kindLabelDerive", "\'Parameter\'\n", "correctNameDerive",
				"(not ambiguousParameterName()) and \r\nif self.hasSimpleModelingType then not self.stringEmpty(self.name) else true endif",
				"correctShortNameDerive", "not ambiguousParameterShortName()",
				"calculatedMandatoryDerive", "self.mandatory",
				"calculatedTypeDerive", "self.type", "calculatedSingularDerive",
				"self.singular", "simpleTypeIsCorrectDerive",
				"if  not type.oclIsUndefined() then simpleType=SimpleType::None else\r\nif voidTypeAllowed then true else simpleType<>SimpleType::None\r\nendif endif\r\n /* repeated from explicitly typedk because of bug, is repeated in MProperty and MParameter... */",
				"calculatedNameDerive",
				"(if not stringEmpty(name) then name else  \r\nif hasSimpleDataType then \'NAMEMISSING\' else nameFromType() endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   nameFromType().camelCaseUpper()\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if ambiguousParameterName() then \'  (AMBIGUOUS)\' else \'\' endif)",
				"calculatedShortNameDerive",
				"(if stringEmpty(name) then shortNameFromType()\r\nelse\r\n  if stringEmpty(shortName) then name\r\n  else shortName\r\n endif\r\nendif)\r\n.concat(if self.appendTypeNameToName.oclIsUndefined() then \'\'\r\n                else if self.appendTypeNameToName = false then \'\'\r\n                else if self.appendTypeNameToName = true and stringEmpty(name)=false then\r\n                                   shortNameFromType().camelCaseUpper()\r\n                else \'\' endif endif endif\r\n                )\r\n.concat(if self.ambiguousParameterShortName() then \'( AMBIGUOUS)\' else \'\' endif)" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(mVariableEClass, source,
				new String[] { "fullLabelCreateColumn", "false",
						"eNameCreateColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(this, source,
				new String[] { "hideAdvancedProperties", "null" });
		addAnnotation(getMRepositoryElement__IndentationSpaces(), source,
				new String[] {});
		addAnnotation(getMRepositoryElement__IndentationSpaces__Integer(),
				source, new String[] { "propertyCategory", "Kind",
						"createColumn", "true" });
		addAnnotation(getMRepositoryElement__NewLineString(), source,
				new String[] { "propertyCategory", "Kind", "createColumn",
						"true" });
		addAnnotation(getMRepositoryElement_KindLabel(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMRepositoryElement_RenderedKindLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Kind" });
		addAnnotation(getMRepositoryElement_IndentLevel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Kind" });
		addAnnotation(getMRepositoryElement_Description(), source,
				new String[] { "mixedEditor", "true", "propertyCategory",
						"Documentation", "createColumn", "false" });
		addAnnotation(getMRepositoryElement_AsText(), source,
				new String[] { "propertyCategory", "Summary As Text",
						"createColumn", "false" });
		addAnnotation(getMNamed_SpecialEName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Special ECore Settings" });
		addAnnotation(getMNamed_Name(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMNamed_ShortName(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMNamed_EName(), source, new String[] { "createColumn",
				"false", "propertyCategory", "Abbreviation and Name" });
		addAnnotation(getMNamed_FullLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMNamed_LocalStructuralName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMNamed_CalculatedName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMNamed_CalculatedShortName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMNamed_CorrectName(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Validation" });
		addAnnotation(getMNamed_CorrectShortName(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Validation" });
		addAnnotation(getMComponent__DoActionUpdate__MComponentAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn",
						"true" });
		addAnnotation(getMComponent_NamedEditor(), source,
				new String[] { "createColumn", "false" });
		addAnnotation(getMComponent_NamePrefix(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMComponent_DerivedNamePrefix(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMComponent_NamePrefixForFeatures(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMComponent_ContainingRepository(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMComponent_RepresentsCoreComponent(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMComponent_RootPackage(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMComponent_DomainType(), source, new String[] {
				"propertyCategory", "Bundle Naming", "createColumn", "false" });
		addAnnotation(getMComponent_DomainName(), source, new String[] {
				"propertyCategory", "Bundle Naming", "createColumn", "false" });
		addAnnotation(getMComponent_DerivedDomainType(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Bundle Naming" });
		addAnnotation(getMComponent_DerivedDomainName(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Bundle Naming" });
		addAnnotation(getMComponent_DerivedBundleName(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Bundle Naming" });
		addAnnotation(getMComponent_DerivedURI(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Bundle Naming" });
		addAnnotation(getMComponent_SpecialBundleName(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMComponent_PreloadedComponent(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMComponent_MainNamedEditor(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMComponent_MainEditor(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMComponent_HideAdvancedProperties(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Additional" });
		addAnnotation(getMComponent_AllGeneralAnnotations(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Additional" });
		addAnnotation(getMComponent_AbstractComponent(), source,
				new String[] { "propertyCategory", "Special Component Settings",
						"createColumn", "false" });
		addAnnotation(getMComponent_CountImplementations(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Special Component Settings" });
		addAnnotation(getMComponent_DisableDefaultContainment(), source,
				new String[] { "propertyCategory", "Special Component Settings",
						"createColumn", "false" });
		addAnnotation(getMComponent_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMComponent_AvoidRegenerationOfEditorConfiguration(),
				source, new String[] { "propertyCategory",
						"Editor Configuration", "createColumn", "false" });
		addAnnotation(getMComponent_UseLegacyEditorconfig(), source,
				new String[] { "propertyCategory", "Editor Configuration",
						"createColumn", "false" });
		addAnnotation(getMPackage__DoActionUpdate__MPackageAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn",
						"true" });
		addAnnotation(getMPackage_UsePackageContentOnlyWithExplicitFilter(),
				source, new String[] { "propertyCategory", "Additional",
						"createColumn", "false" });
		addAnnotation(getMPackage_ResourceRootClass(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMPackage_SpecialNsURI(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMPackage_SpecialNsPrefix(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMPackage_SpecialFileName(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMPackage_UseUUID(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMPackage_UuidAttributeName(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMPackage_Parent(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_ContainingPackage(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_ContainingComponent(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_RepresentsCorePackage(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMPackage_AllSubpackages(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_QualifiedName(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_IsRootPackage(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMPackage_InternalEPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relation To ECore" });
		addAnnotation(getMPackage_DerivedNsURI(), source, new String[] {
				"createColumn", "false", "propertyCategory", "URI" });
		addAnnotation(getMPackage_DerivedStandardNsURI(), source, new String[] {
				"createColumn", "false", "propertyCategory", "URI" });
		addAnnotation(getMPackage_DerivedNsPrefix(), source, new String[] {
				"createColumn", "false", "propertyCategory", "URI" });
		addAnnotation(getMPackage_DerivedJavaPackageName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"URI" });
		addAnnotation(getMPackage_DoAction(), source, new String[] {
				"propertyCategory", "Actions", "createColumn", "false" });
		addAnnotation(getMPackage_NamePrefix(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMPackage_DerivedNamePrefix(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Abbreviation and Name" });
		addAnnotation(getMPackage_NamePrefixScope(), source,
				new String[] { "propertyCategory", "Abbreviation and Name",
						"createColumn", "false" });
		addAnnotation(getMPackage_DerivedJsonSchema(), source, new String[] {
				"propertyCategory", "JSON Schema", "createColumn", "false" });
		addAnnotation(getMPackage_GenerateJsonSchema(), source, new String[] {
				"propertyCategory", "JSON Schema", "createColumn", "false" });
		addAnnotation(getMPackage_GeneratedJsonSchema(), source, new String[] {
				"propertyCategory", "JSON Schema", "createColumn", "false" });
		addAnnotation(getMHasSimpleType_SimpleTypeString(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMHasSimpleType_HasSimpleDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Typing" });
		addAnnotation(getMHasSimpleType_HasSimpleModelingType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Typing" });
		addAnnotation(getMHasSimpleType_SimpleTypeIsCorrect(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Validation" });
		addAnnotation(getMHasSimpleType_SimpleType(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMClassifier__AllLiterals(), source,
				new String[] { "propertyCategory", "For Enumerations",
						"createColumn", "true" });
		addAnnotation(getMClassifier__SelectableClassifier__EList_MPackage(),
				source, new String[] { "propertyCategory", "For Classes",
						"createColumn", "true" });
		addAnnotation(getMClassifier__AllDirectSubTypes(), source,
				new String[] { "propertyCategory",
						"For Classes/Supertype Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllSubTypes(), source,
				new String[] { "propertyCategory",
						"For Classes/Supertype Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__SuperTypesAsString(), source,
				new String[] { "propertyCategory",
						"For Classes/Supertype Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllSuperTypes(), source,
				new String[] { "propertyCategory",
						"For Classes/Supertype Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllProperties(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllSuperProperties(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllFeatures(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllNonContainmentFeatures(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllStoredNonContainmentFeatures(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllFeaturesWithStorage(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllContainmentReferences(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllOperationSignatures(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllClassesContainedIn(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__DefaultPropertyValue(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related/Defaulting",
						"createColumn", "true" });
		addAnnotation(getMClassifier__DefaultPropertyDescription(), source,
				new String[] { "propertyCategory",
						"For Classes/Properties Related/Defaulting",
						"createColumn", "true" });
		addAnnotation(getMClassifier__AllIdProperties(), source,
				new String[] { "propertyCategory",
						"For Classes/Id Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AllSuperIdProperties(), source,
				new String[] { "propertyCategory",
						"For Classes/Id Properties Related", "createColumn",
						"true" });
		addAnnotation(getMClassifier__DoActionUpdate__MClassifierAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMClassifier__InvokeSetDoAction__MClassifierAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMClassifier__InvokeToggleSuperType__MClassifier(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMClassifier__DoSuperTypeUpdate__MClassifier(), source,
				new String[] { "propertyCategory", "Actions", "createColumn",
						"true" });
		addAnnotation(getMClassifier__AmbiguousClassifierName(), source,
				new String[] { "propertyCategory", "Naming and Label",
						"createColumn", "true" });
		addAnnotation(getMClassifier_AbstractClass(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMClassifier_EnforcedClass(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMClassifier_SuperTypePackage(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMClassifier_SuperType(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMClassifier_Instance(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMClassifier_Kind(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Kind" });
		addAnnotation(getMClassifier_InternalEClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relation to ECore" });
		addAnnotation(getMClassifier_ContainingPackage(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMClassifier_RepresentsCoreType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMClassifier_AllPropertyGroups(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMClassifier_Literal(), source,
				new String[] { "propertyCategory", "For Enumerations",
						"createColumn", "true" });
		addAnnotation(getMClassifier_EInterface(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMClassifier_OperationAsIdPropertyError(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Validation" });
		addAnnotation(getMClassifier_IsClassByStructure(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"For Classes" });
		addAnnotation(getMClassifier_ToggleSuperType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"For Classes/Supertype Related" });
		addAnnotation(getMClassifier_OrderingStrategy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"For Classes/Properties Related" });
		addAnnotation(getMClassifier_TestAllProperties(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"For Classes/Properties Related" });
		addAnnotation(getMClassifier_IdProperty(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"For Classes/Id Properties Related" });
		addAnnotation(getMClassifier_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMClassifier_ResolveContainerAction(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Actions" });
		addAnnotation(getMClassifier_AbstractDoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMClassifier_ObjectReferences(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_NearestInstance(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_StrictNearestInstance(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_StrictInstance(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_IntelligentNearestInstance(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_ObjectUnreferenced(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_OutstandingToCopyObjects(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_PropertyInstances(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_ObjectReferencePropertyInstances(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_IntelligentInstance(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Object Related" });
		addAnnotation(getMClassifier_ContainmentHierarchy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_StrictAllClassesContainedIn(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_StrictContainmentHierarchy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_IntelligentAllClassesContainedIn(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_IntelligentContainmentHierarchy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_InstantiationHierarchy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_InstantiationPropertyHierarchy(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(
				getMClassifier_IntelligentInstantiationPropertyHierarchy(),
				source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_Classescontainedin(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Classifier Related" });
		addAnnotation(getMClassifier_CanApplyDefaultContainment(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Default Containment/Validation" });
		addAnnotation(getMClassifier_PropertiesAsText(), source,
				new String[] { "propertyCategory", "Summary As Text",
						"createColumn", "false" });
		addAnnotation(getMClassifier_SemanticsAsText(), source,
				new String[] { "propertyCategory", "Summary As Text",
						"createColumn", "false" });
		addAnnotation(getMClassifier_DerivedJsonSchema(), source, new String[] {
				"propertyCategory", "JSON Schema", "createColumn", "false" });
		addAnnotation(getMLiteral_ConstantLabel(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMLiteral_AsString(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMLiteral_QualifiedName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMLiteral_ContainingEnumeration(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMLiteral_InternalEEnumLiteral(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relation to ECore" });
		addAnnotation(getMPropertiesContainer_AllConstraintAnnotations(),
				source, new String[] { "createColumn", "false" });
		addAnnotation(getMPropertiesContainer_ContainingClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMPropertiesContainer_OwnedProperty(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMPropertiesContainer_NrOfPropertiesAndSignatures(),
				source, new String[] { "createColumn", "false",
						"propertyCategory", "Structural" });
		addAnnotation(
				getMPropertiesGroup__DoActionUpdate__MPropertiesGroupAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(
				getMPropertiesGroup__InvokeSetDoAction__MPropertiesGroupAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMPropertiesGroup_Order(), source, new String[] {
				"propertyCategory", "Order", "createColumn", "false" });
		addAnnotation(getMPropertiesGroup_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty__SelectableType__MClassifier(), source,
				new String[] { "propertyCategory", "Typing", "createColumn",
						"true" });
		addAnnotation(getMProperty__AmbiguousPropertyName(), source,
				new String[] { "propertyCategory", "Naming and Labels",
						"createColumn", "true" });
		addAnnotation(getMProperty__AmbiguousPropertyShortName(), source,
				new String[] { "propertyCategory", "Naming and Labels",
						"createColumn", "true" });
		addAnnotation(getMProperty__DoActionUpdate__MPropertyAction(), source,
				new String[] { "propertyCategory", "Actions", "createColumn",
						"true" });
		addAnnotation(
				getMProperty__InvokeSetPropertyBehaviour__PropertyBehavior(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMProperty__InvokeSetDoAction__MPropertyAction(),
				source, new String[] { "propertyCategory", "Actions",
						"createColumn", "true" });
		addAnnotation(getMProperty_OppositeDefinition(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Behavior" });
		addAnnotation(getMProperty_Opposite(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Behavior" });
		addAnnotation(getMProperty_Containment(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Behavior" });
		addAnnotation(getMProperty_HasStorage(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Behavior" });
		addAnnotation(getMProperty_Changeable(), source, new String[] {
				"propertyCategory", "Behavior", "createColumn", "false" });
		addAnnotation(getMProperty_Persisted(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Behavior" });
		addAnnotation(getMProperty_PropertyBehavior(), source, new String[] {
				"propertyCategory", "Behavior", "createColumn", "false" });
		addAnnotation(getMProperty_IsOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Behavior/Derived Helpers" });
		addAnnotation(getMProperty_IsAttribute(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Behavior/Derived Helpers" });
		addAnnotation(getMProperty_IsReference(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Behavior/Derived Helpers" });
		addAnnotation(getMProperty_Kind(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Behavior/Derived Helpers" });
		addAnnotation(getMProperty_TypeDefinition(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Typing" });
		addAnnotation(getMProperty_UseCoreTypes(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Typing" });
		addAnnotation(getMProperty_ELabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMProperty_ENameForELabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMProperty_OrderWithinGroup(), source, new String[] {
				"propertyCategory", "Order", "createColumn", "false" });
		addAnnotation(getMProperty_Id(), source, new String[] { "createColumn",
				"false", "propertyCategory", "Numeric ID" });
		addAnnotation(getMProperty_InternalEStructuralFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relationship to ECore" });
		addAnnotation(getMProperty_DirectSemantics(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Semantics" });
		addAnnotation(getMProperty_SemanticsSpecialization(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Semantics" });
		addAnnotation(getMProperty_AllSignatures(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Semantics" });
		addAnnotation(getMProperty_DirectOperationSemantics(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Semantics" });
		addAnnotation(getMProperty_ContainingClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMProperty_ContainingPropertiesContainer(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMProperty_EKeyForPath(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMProperty_NotUnsettable(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMProperty_AttributeForEId(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMProperty_EDefaultValueLiteral(), source,
				new String[] { "propertyCategory", "Special ECore Settings",
						"createColumn", "false" });
		addAnnotation(getMProperty_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_BehaviorActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_LayoutActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_ArityActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_SimpleTypeActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_AnnotationActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_MoveActions(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(getMProperty_DerivedJsonSchema(), source, new String[] {
				"propertyCategory", "JSON Schema", "createColumn", "false" });
		addAnnotation(getMOperationSignature_Operation(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Structural" });
		addAnnotation(getMOperationSignature_ContainingClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMOperationSignature_ELabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMOperationSignature_InternalEOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relation to ECore" });
		addAnnotation(getMOperationSignature_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
		addAnnotation(
				getMTyped__TypeAsOcl__MPackage_MClassifier_SimpleType_Boolean(),
				source, new String[] { "propertyCategory", "Type Rendering",
						"createColumn", "true" });
		addAnnotation(getMTyped_MultiplicityAsString(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_CalculatedType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_CalculatedMandatory(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_CalculatedSingular(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_CalculatedTypePackage(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_VoidTypeAllowed(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_CalculatedSimpleType(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Type Information" });
		addAnnotation(getMTyped_MultiplicityCase(), source, new String[] {
				"propertyCategory", "Behavior", "createColumn", "false" });
		addAnnotation(getMExplicitlyTyped_Type(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Additional" });
		addAnnotation(getMExplicitlyTyped_TypePackage(), source, new String[] {
				"propertyCategory", "Additional", "createColumn", "false" });
		addAnnotation(getMExplicitlyTyped_Mandatory(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Typing" });
		addAnnotation(getMExplicitlyTyped_Singular(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Typing" });
		addAnnotation(getMExplicitlyTyped_ETypeName(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMExplicitlyTyped_ETypeLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMExplicitlyTyped_CorrectlyTyped(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Validation" });
		addAnnotation(getMExplicitlyTypedAndNamed_AppendTypeNameToName(),
				source, new String[] { "propertyCategory",
						"Abbreviation and Name", "createColumn", "false" });
		addAnnotation(getMParameter_ContainingSignature(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Structural" });
		addAnnotation(getMParameter_ELabel(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMParameter_SignatureAsString(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Naming and Labels" });
		addAnnotation(getMParameter_InternalEParameter(), source,
				new String[] { "createColumn", "false", "propertyCategory",
						"Relation to ECore" });
		addAnnotation(getMParameter_DoAction(), source, new String[] {
				"createColumn", "false", "propertyCategory", "Actions" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getMRepositoryElement_KindLabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRepositoryElement_RenderedKindLabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMRepositoryElement_IndentLevel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_EName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_FullLabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_LocalStructuralName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_CalculatedName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_CalculatedShortName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_CorrectName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMNamed_CorrectShortName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_NamedEditor(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DerivedNamePrefix(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_ContainingRepository(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_RepresentsCoreComponent(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_RootPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DerivedDomainType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DerivedDomainName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DerivedBundleName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DerivedURI(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_PreloadedComponent(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_MainNamedEditor(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_MainEditor(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_HideAdvancedProperties(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_AllGeneralAnnotations(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_CountImplementations(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMComponent_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMPackage_Parent(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_ContainingPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_ContainingComponent(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_RepresentsCorePackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_AllSubpackages(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_QualifiedName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_IsRootPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_InternalEPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedNsURI(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedStandardNsURI(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedNsPrefix(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedJavaPackageName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedNamePrefix(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPackage_DerivedJsonSchema(), source,
				new String[] { "propertyMultiLine", "true" });
		addAnnotation(getMPackage_GeneratedJsonSchema(), source,
				new String[] { "propertyMultiLine", "true" });
		addAnnotation(getMHasSimpleType_SimpleTypeString(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMHasSimpleType_HasSimpleDataType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMHasSimpleType_HasSimpleModelingType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMHasSimpleType_SimpleTypeIsCorrect(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_Instance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_Kind(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_InternalEClassifier(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ContainingPackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_RepresentsCoreType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_AllPropertyGroups(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_OperationAsIdPropertyError(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IsClassByStructure(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ToggleSuperType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_OrderingStrategy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_TestAllProperties(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IdProperty(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMClassifier_ResolveContainerAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_AbstractDoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ObjectReferences(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_NearestInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_StrictNearestInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_StrictInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IntelligentNearestInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ObjectUnreferenced(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_OutstandingToCopyObjects(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_PropertyInstances(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ObjectReferencePropertyInstances(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IntelligentInstance(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_ContainmentHierarchy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_StrictAllClassesContainedIn(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_StrictContainmentHierarchy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IntelligentAllClassesContainedIn(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_IntelligentContainmentHierarchy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_InstantiationHierarchy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_InstantiationPropertyHierarchy(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(
				getMClassifier_IntelligentInstantiationPropertyHierarchy(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_Classescontainedin(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMClassifier_CanApplyDefaultContainment(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteral_ConstantLabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteral_AsString(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteral_QualifiedName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteral_ContainingEnumeration(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMLiteral_InternalEEnumLiteral(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertiesContainer_AllConstraintAnnotations(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertiesContainer_ContainingClassifier(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertiesContainer_OwnedProperty(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertiesContainer_NrOfPropertiesAndSignatures(),
				source, new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMPropertiesGroup_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_OppositeDefinition(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMProperty_Opposite(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMProperty_Containment(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_HasStorage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_IsOperation(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_IsAttribute(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_IsReference(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_Kind(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_TypeDefinition(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_UseCoreTypes(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_ELabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_ENameForELabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_Id(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_InternalEStructuralFeature(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_DirectSemantics(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_SemanticsSpecialization(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_AllSignatures(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_DirectOperationSemantics(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_ContainingClassifier(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_ContainingPropertiesContainer(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_EKeyForPath(), source,
				new String[] { "propertySortChoices", "false" });
		addAnnotation(getMProperty_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert",
						"propertySortChoices", "false" });
		addAnnotation(getMProperty_BehaviorActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_LayoutActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_ArityActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_SimpleTypeActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_AnnotationActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMProperty_MoveActions(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationSignature_Operation(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationSignature_ContainingClassifier(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationSignature_ELabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationSignature_InternalEOperation(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMOperationSignature_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_MultiplicityAsString(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_CalculatedType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_CalculatedMandatory(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_CalculatedSingular(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_CalculatedTypePackage(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_VoidTypeAllowed(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMTyped_CalculatedSimpleType(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMExplicitlyTyped_Mandatory(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMExplicitlyTyped_Singular(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMExplicitlyTyped_ETypeName(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMExplicitlyTyped_ETypeLabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMExplicitlyTyped_CorrectlyTyped(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMParameter_ContainingSignature(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMParameter_ELabel(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMParameter_SignatureAsString(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMParameter_InternalEParameter(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
		addAnnotation(getMParameter_DoAction(), source,
				new String[] { "propertyFilterFlags",
						"org.eclipse.ui.views.properties.expert" });
	}

} //McorePackageImpl
