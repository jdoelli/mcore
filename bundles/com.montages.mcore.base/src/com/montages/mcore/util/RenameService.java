package com.montages.mcore.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;

import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;

public class RenameService {

	private static final EAttribute DERIVED_URI = McorePackage.Literals.MCOMPONENT__DERIVED_URI;

	private static final EAttribute MNAMED_NAME = McorePackage.Literals.MNAMED__NAME;

	private static final EAttribute DOMAIN_NAME = McorePackage.Literals.MCOMPONENT__DOMAIN_NAME;

	private static final EAttribute SHORT_NAME = McorePackage.Literals.MNAMED__SHORT_NAME;

	private static final EAttribute DOMAIN_TYPE = McorePackage.Literals.MCOMPONENT__DOMAIN_TYPE;

	private Map<String, String> renameMap = null;

	private Map<String, String> packageRenameMap = null;

	private Map<String, String> fileRenameMap = null;

	private URIService uriService = new URIService();

	public void renameMComponent(String name, Object[] data) {
		if (renameMap == null) {
			renameMap = new HashMap<String, String>();
		}
		if (packageRenameMap == null) {
			packageRenameMap = new HashMap<String, String>();
		}

		if (fileRenameMap == null) {
			fileRenameMap = new HashMap<String, String>();
		}

		MComponent component = (MComponent) data[0];

		//remove old mapping
		URI mcore = uriService.createComponentURI(component);
		Map<URI, URI> uriMap = component.eResource().getResourceSet().getURIConverter().getURIMap();
		uriMap.remove(mcore);

		String http = getDerivedHttpUri(component);
		if (packageRenameMap.containsKey(http)) {
			http = packageRenameMap.remove(http);
		}
		String oldReplacement = getReplacement(component);
		if (renameMap.containsKey(oldReplacement)) {
			oldReplacement = renameMap.remove(oldReplacement);

		}
		String fileRename = getFileRename(component);
		if (fileRenameMap.containsKey(fileRename)) {
			fileRename = fileRenameMap.remove(fileRename);
		}

		//rename component
		for (int i = 1; i < data.length; i++) {
			EAttribute attribute = (EAttribute) data[i];
			component.eSet(attribute, name);
		}

		//set new mapping
		URI platfomURI = uriService.createPlatformResource(component);
		component.eResource().setURI(platfomURI);

		uriMap.put(mcore, platfomURI);

		String newReplacement = getReplacement(component);
		if (!newReplacement.equals(oldReplacement)) {
			renameMap.put(newReplacement, oldReplacement);
		}

		String newHttp = getDerivedHttpUri(component);
		if (!newHttp.equals(http)) {
			packageRenameMap.put(newHttp, http);
		}
		String newFileRename = getFileRename(component);
		if (!newFileRename.equals(fileRename)) {
			fileRenameMap.put(newFileRename, fileRename);
		}
	}

	protected String getDerivedHttpUri(MComponent component) {
		return URI.createURI(component.getDerivedURI() + "/").toString().replaceFirst("http://", "");
	}

	private String getReplacement(MComponent component) {
		URI oldURI = uriService.createPlatformResource(component);
		return oldURI.trimSegments(1).toPlatformString(false);
	}

	protected String getFileRename(MComponent component) {
		return "/" + uriService.componentFileName(component) + "#_";
	}

	public String updateRef(String ref) {
		if (renameMap != null) {
			for (String key : renameMap.keySet()) {
				String value = renameMap.get(key);
				if (ref.startsWith(value)) {
					ref = ref.replaceFirst(value, key);
					break;
				}
			}
		}
		if (packageRenameMap != null) {
			for (String key : packageRenameMap.keySet()) {
				String value = packageRenameMap.get(key);
				if (ref.startsWith(value)) {
					ref = ref.replaceFirst(value, key);
					break;
				}
			}
		}
		if (fileRenameMap != null) {
			for (String key : fileRenameMap.keySet()) {
				String value = fileRenameMap.get(key);
				if (ref.contains(value)) {
					ref = ref.replace(value, key);
					break;
				}
			}
		}
		return ref;
	}

	public boolean canRename(String newName, Object[] data, List<MComponent> otherComponents) {
		if (data == null || data.length < 2 || otherComponents == null) {
			return false;
		}
		MComponent component = (MComponent) data[0];
		Object[] oldValues = new Object[data.length - 1];
		try {
			
			for (int i = 1; i < data.length; i++) {
				EAttribute attribute = (EAttribute) data[i];
				oldValues[i - 1] = component.eGet(attribute);
				component.eSet(attribute, newName);
			}

			for (MComponent otherComponent : otherComponents) {
				if (component == otherComponent) {
					continue;
				}

				if (isEquals(component, otherComponent)) {
					return false;
				}
			}
			return true;
		} finally {
			for (int i = 1; i < data.length; i++) {
				EAttribute attribute = (EAttribute) data[i];
				component.eSet(attribute, oldValues[i - 1]);
			}
		}
	}

	public static boolean isEquals(MComponent component, MComponent otherComponent) {
		if (compareValues((String) component.eGet(DOMAIN_TYPE), (String) otherComponent.eGet(DOMAIN_TYPE))) {
			if (compareValues((String) component.eGet(DOMAIN_NAME), (String) otherComponent.eGet(DOMAIN_NAME))) {
				if (compareValues((String) component.eGet(SHORT_NAME), (String) otherComponent.eGet(SHORT_NAME))) {
					if (compareValues((String) component.eGet(MNAMED_NAME), (String) otherComponent.eGet(MNAMED_NAME))) {
						if (compareValues((String) component.eGet(DERIVED_URI), (String) otherComponent.eGet(DERIVED_URI))) {

							return true;
						}
					}
				}
			}
		}
		return false;
	}

	public static boolean compareValues(String o1, String o2) {
		if (o1 == null && o1 == o2) {
			return true;
		}
		if (o2 == null || o1== null) {
			return false;
		}
		return o1.toLowerCase().equals(o2.toLowerCase());
	}
}
