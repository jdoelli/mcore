package com.montages.mcore.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MParameter;
import com.montages.mcore.MProperty;

public class Dependencies {

	private final Map<MComponent, Set<MComponent>> map;

	Dependencies(Map<MComponent, Set<MComponent>> map) {
		this.map = map;
	}

	public static Dependencies compute(MComponent component) {
		return compute(component, null);
	}

	public static Dependencies compute(Collection<MComponent> components) {
		Dependencies dep = new Dependencies(new HashMap<MComponent, Set<MComponent>>());
		for (MComponent c: components) {
			compute(c, dep);
		}
		return dep;
	}

	private static Dependencies compute(MComponent component, Dependencies dep) {
		if (dep == null) {
			dep = new Dependencies(new HashMap<MComponent, Set<MComponent>>());
		}

		if (!dep.map.containsKey(component)) {
			Set<MComponent> externals = findDependencies(component);
			dep.map.put(component, externals);

			for (MComponent extern: externals) {
				compute(extern, dep);
			}
		}

		return dep;
	}

	public boolean hasCycle() {
		Graph graph = Graph.createGraph(this);
		graph.sort();

		return !graph.edges.isEmpty();
	}

	public String printCycles() {
		Graph graph = Graph.createGraph(this);
		return graph.printCycles();
	}

	public Map<MComponent, Set<MComponent>> getMap() {
		return map;
	}

	private static class Graph {
		List<Node> nodes = new ArrayList<Node>();
		List<Edge> edges = new ArrayList<Edge>();

		public Node find(MComponent c) {
			for (Node n: nodes) {
				if (n.component.equals(c)) {
					return n;
				}
			}
			return null;
		}

		private static Graph createGraph(Dependencies dep) {
			Graph g = new Graph();

			for (MComponent c: dep.map.keySet()) {
				Node n = new Node();
				n.component = c;
				g.nodes.add(n);
			}

			for (MComponent c: dep.map.keySet()) {
				Node n = g.find(c);
				Set<MComponent> deps = dep.map.get(c);
				for (MComponent d: deps) {
					Node nd = g.find(d);
					Edge e = new Edge();
					e.source = n;
					e.target = nd;
					n.outs.add(e);
					nd.ins.add(e);
					g.edges.add(e);
				}
			}

			return g;
		}

		private static class Node {
			MComponent component;
			List<Edge> ins = new ArrayList<Edge>();
			List<Edge> outs = new ArrayList<Edge>();
		}

		private static class Edge {
			Node source;
			Node target;
		}

		public List<MComponent> sort() {
			List<MComponent> sorted = new ArrayList<MComponent>();
			Set<Graph.Node> s = new HashSet<Graph.Node>();

			for (Graph.Node n: this.nodes) {
				if (n.ins.isEmpty()) {
					s.add(n);
				}
			}

			while(!s.isEmpty()) {
				Graph.Node n = s.iterator().next();
				s.remove(n);
				sorted.add(0, n.component);

				for (Iterator<Graph.Edge> it = n.outs.iterator(); it.hasNext();) {
					Graph.Edge e = it.next();
					Graph.Node m = e.target;
					it.remove();
					m.ins.remove(e);
					edges.remove(e);
					if (m.ins.isEmpty()) {
						s.add(m);
					}
				}
			}

			return sorted;
		}

		public String printCycles() {
			sort();
			if (edges.isEmpty()) return "No cycles";
			else {
				String result = "Detected cycles:\n";
				for (Edge e: edges) {
					result += " " + e.source.component.getEName() + " -> " + e.target.component.getEName() + "\n";
				}

				return result;
			}
		}
	}

	public List<MComponent> sort() {
		Graph g = Graph.createGraph(this);
		return g.sort();
	}

	private static Set<MComponent> findDependencies(MComponent component) {
		Set<MComponent> dependencies = new HashSet<MComponent>();

		for (TreeIterator<EObject> it = component.eAllContents(); it.hasNext();) {
			EObject current = it.next();

			if (current instanceof MClassifier) {
				for (MClassifier c: ((MClassifier)current).getSuperType()) {
					addDependency(c, component, dependencies);
				}
			}
			else if (current instanceof MProperty) {
				addDependency(((MProperty) current).getCalculatedType(), component, dependencies);
			}
			else if (current instanceof MOperationSignature) {
				addDependency(((MOperationSignature) current).getCalculatedType(), component, dependencies);

				for (MParameter p: ((MOperationSignature) current).getParameter()) {
					addDependency(p.getCalculatedType(), component, dependencies);
				}
			}
		}

		return dependencies;
	}

	private static void addDependency(MClassifier classifier, MComponent source, Set<MComponent> dependencies) {
		if (classifier != null && classifier.getContainingPackage() != null) {
			MComponent target = classifier.getContainingPackage().getContainingComponent();
			if (isDependency(source, target)) {
				dependencies.add(target);
			}
		}
	}

	private static boolean isDependency(MComponent source, MComponent target) {
		String core = "http://www.langlets.org/coreTypes";
		return target != null && !target.equals(source) && !target.getDerivedURI().equals(core);
	}

}
