/**
 */
package com.montages.mcore.util;

import com.montages.mcore.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage
 * @generated
 */
public class McoreValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final McoreValidator INSTANCE = new McoreValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "com.montages.mcore";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public McoreValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return McorePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
		case McorePackage.MREPOSITORY_ELEMENT:
			return validateMRepositoryElement((MRepositoryElement) value,
					diagnostics, context);
		case McorePackage.MREPOSITORY:
			return validateMRepository((MRepository) value, diagnostics,
					context);
		case McorePackage.MNAMED:
			return validateMNamed((MNamed) value, diagnostics, context);
		case McorePackage.MCOMPONENT:
			return validateMComponent((MComponent) value, diagnostics, context);
		case McorePackage.MMODEL_ELEMENT:
			return validateMModelElement((MModelElement) value, diagnostics,
					context);
		case McorePackage.MPACKAGE:
			return validateMPackage((MPackage) value, diagnostics, context);
		case McorePackage.MHAS_SIMPLE_TYPE:
			return validateMHasSimpleType((MHasSimpleType) value, diagnostics,
					context);
		case McorePackage.MCLASSIFIER:
			return validateMClassifier((MClassifier) value, diagnostics,
					context);
		case McorePackage.MLITERAL:
			return validateMLiteral((MLiteral) value, diagnostics, context);
		case McorePackage.MPROPERTIES_CONTAINER:
			return validateMPropertiesContainer((MPropertiesContainer) value,
					diagnostics, context);
		case McorePackage.MPROPERTIES_GROUP:
			return validateMPropertiesGroup((MPropertiesGroup) value,
					diagnostics, context);
		case McorePackage.MPROPERTY:
			return validateMProperty((MProperty) value, diagnostics, context);
		case McorePackage.MOPERATION_SIGNATURE:
			return validateMOperationSignature((MOperationSignature) value,
					diagnostics, context);
		case McorePackage.MTYPED:
			return validateMTyped((MTyped) value, diagnostics, context);
		case McorePackage.MEXPLICITLY_TYPED:
			return validateMExplicitlyTyped((MExplicitlyTyped) value,
					diagnostics, context);
		case McorePackage.MVARIABLE:
			return validateMVariable((MVariable) value, diagnostics, context);
		case McorePackage.MEXPLICITLY_TYPED_AND_NAMED:
			return validateMExplicitlyTypedAndNamed(
					(MExplicitlyTypedAndNamed) value, diagnostics, context);
		case McorePackage.MPARAMETER:
			return validateMParameter((MParameter) value, diagnostics, context);
		case McorePackage.MEDITOR:
			return validateMEditor((MEditor) value, diagnostics, context);
		case McorePackage.MNAMED_EDITOR:
			return validateMNamedEditor((MNamedEditor) value, diagnostics,
					context);
		case McorePackage.MCOMPONENT_ACTION:
			return validateMComponentAction((MComponentAction) value,
					diagnostics, context);
		case McorePackage.TOP_LEVEL_DOMAIN:
			return validateTopLevelDomain((TopLevelDomain) value, diagnostics,
					context);
		case McorePackage.MPACKAGE_ACTION:
			return validateMPackageAction((MPackageAction) value, diagnostics,
					context);
		case McorePackage.SIMPLE_TYPE:
			return validateSimpleType((SimpleType) value, diagnostics, context);
		case McorePackage.MCLASSIFIER_ACTION:
			return validateMClassifierAction((MClassifierAction) value,
					diagnostics, context);
		case McorePackage.ORDERING_STRATEGY:
			return validateOrderingStrategy((OrderingStrategy) value,
					diagnostics, context);
		case McorePackage.MLITERAL_ACTION:
			return validateMLiteralAction((MLiteralAction) value, diagnostics,
					context);
		case McorePackage.MPROPERTIES_GROUP_ACTION:
			return validateMPropertiesGroupAction(
					(MPropertiesGroupAction) value, diagnostics, context);
		case McorePackage.MPROPERTY_ACTION:
			return validateMPropertyAction((MPropertyAction) value, diagnostics,
					context);
		case McorePackage.PROPERTY_BEHAVIOR:
			return validatePropertyBehavior((PropertyBehavior) value,
					diagnostics, context);
		case McorePackage.MOPERATION_SIGNATURE_ACTION:
			return validateMOperationSignatureAction(
					(MOperationSignatureAction) value, diagnostics, context);
		case McorePackage.MULTIPLICITY_CASE:
			return validateMultiplicityCase((MultiplicityCase) value,
					diagnostics, context);
		case McorePackage.CLASSIFIER_KIND:
			return validateClassifierKind((ClassifierKind) value, diagnostics,
					context);
		case McorePackage.PROPERTY_KIND:
			return validatePropertyKind((PropertyKind) value, diagnostics,
					context);
		case McorePackage.MPARAMETER_ACTION:
			return validateMParameterAction((MParameterAction) value,
					diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMRepositoryElement(
			MRepositoryElement mRepositoryElement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mRepositoryElement, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMRepository(MRepository mRepository,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mRepository, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMNamed(MNamed mNamed, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mNamed, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMComponent(MComponent mComponent,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mComponent, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMModelElement(MModelElement mModelElement,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mModelElement, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPackage(MPackage mPackage,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mPackage, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMHasSimpleType(MHasSimpleType mHasSimpleType,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mHasSimpleType, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMClassifier(MClassifier mClassifier,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mClassifier, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMLiteral(MLiteral mLiteral,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mLiteral, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPropertiesContainer(
			MPropertiesContainer mPropertiesContainer,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mPropertiesContainer,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPropertiesGroup(MPropertiesGroup mPropertiesGroup,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mPropertiesGroup, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMProperty(MProperty mProperty,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mProperty, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMOperationSignature(
			MOperationSignature mOperationSignature,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mOperationSignature, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMTyped(MTyped mTyped, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mTyped, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMExplicitlyTyped(MExplicitlyTyped mExplicitlyTyped,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mExplicitlyTyped, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMVariable(MVariable mVariable,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mVariable, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMExplicitlyTypedAndNamed(
			MExplicitlyTypedAndNamed mExplicitlyTypedAndNamed,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mExplicitlyTypedAndNamed,
				diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMParameter(MParameter mParameter,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(mParameter, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(mParameter,
				diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(mParameter, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(mParameter,
					diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(mParameter,
					diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(mParameter, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(mParameter, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(mParameter, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(mParameter, diagnostics,
					context);
		if (result || diagnostics != null)
			result &= validateMParameter_NAMEMISSING(mParameter, diagnostics,
					context);
		return result;
	}

	/**
	 * Validates the NAMEMISSING constraint of '<em>MParameter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMParameter_NAMEMISSING(MParameter mParameter,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint (no OCL constraint given!)
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add(
						createDiagnostic(Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0,
								"_UI_GenericConstraint_diagnostic",
								new Object[] { "NAMEMISSING",
										getObjectLabel(mParameter, context) },
								new Object[] { mParameter }, context));
			}
			return false;
		}
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMEditor(MEditor mEditor, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mEditor, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMNamedEditor(MNamedEditor mNamedEditor,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint(mNamedEditor, diagnostics,
				context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMComponentAction(MComponentAction mComponentAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTopLevelDomain(TopLevelDomain topLevelDomain,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPackageAction(MPackageAction mPackageAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSimpleType(SimpleType simpleType,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMClassifierAction(
			MClassifierAction mClassifierAction, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOrderingStrategy(OrderingStrategy orderingStrategy,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMLiteralAction(MLiteralAction mLiteralAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPropertiesGroupAction(
			MPropertiesGroupAction mPropertiesGroupAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMPropertyAction(MPropertyAction mPropertyAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyBehavior(PropertyBehavior propertyBehavior,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMOperationSignatureAction(
			MOperationSignatureAction mOperationSignatureAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMultiplicityCase(MultiplicityCase multiplicityCase,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClassifierKind(ClassifierKind classifierKind,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePropertyKind(PropertyKind propertyKind,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMParameterAction(MParameterAction mParameterAction,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //McoreValidator
