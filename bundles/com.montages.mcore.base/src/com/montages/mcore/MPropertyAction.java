/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProperty Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMPropertyAction()
 * @model
 * @generated
 */
public enum MPropertyAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Into Stored</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_STORED_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_STORED(1, "IntoStored", "-> Stored"),
	/**
	 * The '<em><b>Into Contained</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_CONTAINED_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_CONTAINED(2, "IntoContained", "-> Contained"),
	/**
	 * The '<em><b>Into Derived</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_DERIVED_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_DERIVED(3, "IntoDerived", "-> Derived"),
	/**
	 * The '<em><b>Into Operation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_OPERATION_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_OPERATION(4, "IntoOperation", "-> Operation"),
	/**
	 * The '<em><b>Into Changeable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_CHANGEABLE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_CHANGEABLE(5, "IntoChangeable", "-> Changeable"),
	/**
	 * The '<em><b>Into Not Changeable</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_NOT_CHANGEABLE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_NOT_CHANGEABLE(6, "IntoNotChangeable", "-> Not Changeable"),
	/**
	 * The '<em><b>Into Unary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_UNARY(7, "IntoUnary", "-> [0..1]"),
	/**
	 * The '<em><b>Into Nary</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_NARY(8, "IntoNary", "-> [0..*]"),
	/**
	 * The '<em><b>Into String Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_STRING_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_STRING_ATTRIBUTE(9, "IntoStringAttribute", "-> String Attribute"),
	/**
	 * The '<em><b>Into Boolean Flag</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_BOOLEAN_FLAG_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_BOOLEAN_FLAG(10, "IntoBooleanFlag", "-> Boolean Flag"),
	/**
	 * The '<em><b>Into Integer Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_INTEGER_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_INTEGER_ATTRIBUTE(11, "IntoIntegerAttribute", "-> Integer Attribute"),
	/**
	 * The '<em><b>Into Real Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_REAL_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_REAL_ATTRIBUTE(12, "IntoRealAttribute", "-> Real Attribute"),
	/**
	 * The '<em><b>Into Date Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INTO_DATE_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	INTO_DATE_ATTRIBUTE(13, "IntoDateAttribute", "-> Date Attribute"),
	/**
	 * The '<em><b>Construct Choice</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CONSTRUCT_CHOICE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRUCT_CHOICE(14, "ConstructChoice", "Construct Choice"),
	/**
	 * The '<em><b>Constrain Choice</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CONSTRAIN_CHOICE_VALUE
	 * @generated
	 * @ordered
	 */
	CONSTRAIN_CHOICE(15, "ConstrainChoice", "Constrain Choice"),
	/**
	 * The '<em><b>Init Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INIT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	INIT_VALUE(16, "InitValue", "Init Value"),
	/**
	 * The '<em><b>Specialize Init Value</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_INIT_VALUE_VALUE
	 * @generated
	 * @ordered
	 */
	SPECIALIZE_INIT_VALUE(17, "SpecializeInitValue", "Specialize Init Value"),
	/**
	 * The '<em><b>Init Order</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #INIT_ORDER_VALUE
	 * @generated
	 * @ordered
	 */
	INIT_ORDER(18, "InitOrder", "Init Order"),
	/**
	 * The '<em><b>Specialize Init Order</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_INIT_ORDER_VALUE
	 * @generated
	 * @ordered
	 */
	SPECIALIZE_INIT_ORDER(19, "SpecializeInitOrder", "Specialize Init Order"),
	/**
	 * The '<em><b>Result</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #RESULT_VALUE
	 * @generated
	 * @ordered
	 */
	RESULT(20, "Result", "Result"),
	/**
	 * The '<em><b>Specialize Result</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_RESULT_VALUE
	 * @generated
	 * @ordered
	 */
	SPECIALIZE_RESULT(21, "SpecializeResult", "Specialize Result"),
	/**
	 * The '<em><b>Highlight</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIGHLIGHT_VALUE
	 * @generated
	 * @ordered
	 */
	HIGHLIGHT(22, "Highlight", "Highlight"),
	/**
	 * The '<em><b>Hide In Properties View</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIDE_IN_PROPERTIES_VIEW_VALUE
	 * @generated
	 * @ordered
	 */
	HIDE_IN_PROPERTIES_VIEW(23, "HideInPropertiesView", "Hide in Properties View"),
	/**
	 * The '<em><b>Hide In Table Editor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #HIDE_IN_TABLE_EDITOR_VALUE
	 * @generated
	 * @ordered
	 */
	HIDE_IN_TABLE_EDITOR(24, "HideInTableEditor", "Hide in Table Editor"),
	/**
	 * The '<em><b>Show In Table Editor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHOW_IN_TABLE_EDITOR_VALUE
	 * @generated
	 * @ordered
	 */
	SHOW_IN_TABLE_EDITOR(25, "ShowInTableEditor", "Show in Table Editor"),
	/**
	 * The '<em><b>Show In Properties View</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SHOW_IN_PROPERTIES_VIEW_VALUE
	 * @generated
	 * @ordered
	 */
	SHOW_IN_PROPERTIES_VIEW(26, "ShowInPropertiesView", "Show in Properties View"),
	/**
	 * The '<em><b>Class With String Property</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #CLASS_WITH_STRING_PROPERTY_VALUE
	 * @generated
	 * @ordered
	 */
	CLASS_WITH_STRING_PROPERTY(27, "ClassWithStringProperty", "Move to New Contained Class"),
	/**
	 * The '<em><b>Move To Last Group</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOVE_TO_LAST_GROUP_VALUE
	 * @generated
	 * @ordered
	 */
	MOVE_TO_LAST_GROUP(28, "MoveToLastGroup", "Move to Last Group"),
	/**
	 * The '<em><b>Move To New Group</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #MOVE_TO_NEW_GROUP_VALUE
	 * @generated
	 * @ordered
	 */
	MOVE_TO_NEW_GROUP(29, "MoveToNewGroup", "Move to New Group"),
	/**
	 * The '<em><b>Add Update Annotation</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ADD_UPDATE_ANNOTATION_VALUE
	 * @generated
	 * @ordered
	 */
	ADD_UPDATE_ANNOTATION(30, "AddUpdateAnnotation", "Add Update Annotation"),
	/**
	 * The '<em><b>Specialize</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #SPECIALIZE_VALUE
	 * @generated
	 * @ordered
	 */
	SPECIALIZE(31, "Specialize", "Specialize"),
	/**
	 * The '<em><b>Abstract</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ABSTRACT_VALUE
	 * @generated
	 * @ordered
	 */
	ABSTRACT(32, "Abstract", "Abstract");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Into Stored</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Stored</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_STORED
	 * @model name="IntoStored" literal="-> Stored"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_STORED_VALUE = 1;

	/**
	 * The '<em><b>Into Contained</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Contained</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_CONTAINED
	 * @model name="IntoContained" literal="-> Contained"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_CONTAINED_VALUE = 2;

	/**
	 * The '<em><b>Into Derived</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Derived</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_DERIVED
	 * @model name="IntoDerived" literal="-> Derived"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_DERIVED_VALUE = 3;

	/**
	 * The '<em><b>Into Operation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Operation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_OPERATION
	 * @model name="IntoOperation" literal="-> Operation"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_OPERATION_VALUE = 4;

	/**
	 * The '<em><b>Into Changeable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Changeable</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_CHANGEABLE
	 * @model name="IntoChangeable" literal="-> Changeable"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_CHANGEABLE_VALUE = 5;

	/**
	 * The '<em><b>Into Not Changeable</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Not Changeable</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_NOT_CHANGEABLE
	 * @model name="IntoNotChangeable" literal="-> Not Changeable"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_NOT_CHANGEABLE_VALUE = 6;

	/**
	 * The '<em><b>Into Unary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Unary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_UNARY
	 * @model name="IntoUnary" literal="-> [0..1]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_UNARY_VALUE = 7;

	/**
	 * The '<em><b>Into Nary</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Nary</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_NARY
	 * @model name="IntoNary" literal="-> [0..*]"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_NARY_VALUE = 8;

	/**
	 * The '<em><b>Into String Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into String Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_STRING_ATTRIBUTE
	 * @model name="IntoStringAttribute" literal="-> String Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_STRING_ATTRIBUTE_VALUE = 9;

	/**
	 * The '<em><b>Into Boolean Flag</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Boolean Flag</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_BOOLEAN_FLAG
	 * @model name="IntoBooleanFlag" literal="-> Boolean Flag"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_BOOLEAN_FLAG_VALUE = 10;

	/**
	 * The '<em><b>Into Integer Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Integer Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_INTEGER_ATTRIBUTE
	 * @model name="IntoIntegerAttribute" literal="-> Integer Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_INTEGER_ATTRIBUTE_VALUE = 11;

	/**
	 * The '<em><b>Into Real Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Real Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_REAL_ATTRIBUTE
	 * @model name="IntoRealAttribute" literal="-> Real Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_REAL_ATTRIBUTE_VALUE = 12;

	/**
	 * The '<em><b>Into Date Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Into Date Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INTO_DATE_ATTRIBUTE
	 * @model name="IntoDateAttribute" literal="-> Date Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int INTO_DATE_ATTRIBUTE_VALUE = 13;

	/**
	 * The '<em><b>Construct Choice</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Construct Choice</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRUCT_CHOICE
	 * @model name="ConstructChoice" literal="Construct Choice"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRUCT_CHOICE_VALUE = 14;

	/**
	 * The '<em><b>Constrain Choice</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Constrain Choice</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CONSTRAIN_CHOICE
	 * @model name="ConstrainChoice" literal="Constrain Choice"
	 * @generated
	 * @ordered
	 */
	public static final int CONSTRAIN_CHOICE_VALUE = 15;

	/**
	 * The '<em><b>Init Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Init Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INIT_VALUE
	 * @model name="InitValue" literal="Init Value"
	 * @generated
	 * @ordered
	 */
	public static final int INIT_VALUE_VALUE = 16;

	/**
	 * The '<em><b>Specialize Init Value</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Specialize Init Value</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_INIT_VALUE
	 * @model name="SpecializeInitValue" literal="Specialize Init Value"
	 * @generated
	 * @ordered
	 */
	public static final int SPECIALIZE_INIT_VALUE_VALUE = 17;

	/**
	 * The '<em><b>Init Order</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Init Order</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #INIT_ORDER
	 * @model name="InitOrder" literal="Init Order"
	 * @generated
	 * @ordered
	 */
	public static final int INIT_ORDER_VALUE = 18;

	/**
	 * The '<em><b>Specialize Init Order</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Specialize Init Order</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_INIT_ORDER
	 * @model name="SpecializeInitOrder" literal="Specialize Init Order"
	 * @generated
	 * @ordered
	 */
	public static final int SPECIALIZE_INIT_ORDER_VALUE = 19;

	/**
	 * The '<em><b>Result</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Result</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #RESULT
	 * @model name="Result"
	 * @generated
	 * @ordered
	 */
	public static final int RESULT_VALUE = 20;

	/**
	 * The '<em><b>Specialize Result</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Specialize Result</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE_RESULT
	 * @model name="SpecializeResult" literal="Specialize Result"
	 * @generated
	 * @ordered
	 */
	public static final int SPECIALIZE_RESULT_VALUE = 21;

	/**
	 * The '<em><b>Highlight</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Highlight</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIGHLIGHT
	 * @model name="Highlight"
	 * @generated
	 * @ordered
	 */
	public static final int HIGHLIGHT_VALUE = 22;

	/**
	 * The '<em><b>Hide In Properties View</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hide In Properties View</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIDE_IN_PROPERTIES_VIEW
	 * @model name="HideInPropertiesView" literal="Hide in Properties View"
	 * @generated
	 * @ordered
	 */
	public static final int HIDE_IN_PROPERTIES_VIEW_VALUE = 23;

	/**
	 * The '<em><b>Hide In Table Editor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Hide In Table Editor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #HIDE_IN_TABLE_EDITOR
	 * @model name="HideInTableEditor" literal="Hide in Table Editor"
	 * @generated
	 * @ordered
	 */
	public static final int HIDE_IN_TABLE_EDITOR_VALUE = 24;

	/**
	 * The '<em><b>Show In Table Editor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Show In Table Editor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHOW_IN_TABLE_EDITOR
	 * @model name="ShowInTableEditor" literal="Show in Table Editor"
	 * @generated
	 * @ordered
	 */
	public static final int SHOW_IN_TABLE_EDITOR_VALUE = 25;

	/**
	 * The '<em><b>Show In Properties View</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Show In Properties View</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SHOW_IN_PROPERTIES_VIEW
	 * @model name="ShowInPropertiesView" literal="Show in Properties View"
	 * @generated
	 * @ordered
	 */
	public static final int SHOW_IN_PROPERTIES_VIEW_VALUE = 26;

	/**
	 * The '<em><b>Class With String Property</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Class With String Property</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLASS_WITH_STRING_PROPERTY
	 * @model name="ClassWithStringProperty" literal="Move to New Contained Class"
	 * @generated
	 * @ordered
	 */
	public static final int CLASS_WITH_STRING_PROPERTY_VALUE = 27;

	/**
	 * The '<em><b>Move To Last Group</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Move To Last Group</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOVE_TO_LAST_GROUP
	 * @model name="MoveToLastGroup" literal="Move to Last Group"
	 * @generated
	 * @ordered
	 */
	public static final int MOVE_TO_LAST_GROUP_VALUE = 28;

	/**
	 * The '<em><b>Move To New Group</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Move To New Group</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #MOVE_TO_NEW_GROUP
	 * @model name="MoveToNewGroup" literal="Move to New Group"
	 * @generated
	 * @ordered
	 */
	public static final int MOVE_TO_NEW_GROUP_VALUE = 29;

	/**
	 * The '<em><b>Add Update Annotation</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Add Update Annotation</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ADD_UPDATE_ANNOTATION
	 * @model name="AddUpdateAnnotation" literal="Add Update Annotation"
	 * @generated
	 * @ordered
	 */
	public static final int ADD_UPDATE_ANNOTATION_VALUE = 30;

	/**
	 * The '<em><b>Specialize</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Specialize</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SPECIALIZE
	 * @model name="Specialize"
	 * @generated
	 * @ordered
	 */
	public static final int SPECIALIZE_VALUE = 31;

	/**
	 * The '<em><b>Abstract</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Abstract</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ABSTRACT
	 * @model name="Abstract"
	 * @generated
	 * @ordered
	 */
	public static final int ABSTRACT_VALUE = 32;

	/**
	 * An array of all the '<em><b>MProperty Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPropertyAction[] VALUES_ARRAY = new MPropertyAction[] {
			DO, INTO_STORED, INTO_CONTAINED, INTO_DERIVED, INTO_OPERATION,
			INTO_CHANGEABLE, INTO_NOT_CHANGEABLE, INTO_UNARY, INTO_NARY,
			INTO_STRING_ATTRIBUTE, INTO_BOOLEAN_FLAG, INTO_INTEGER_ATTRIBUTE,
			INTO_REAL_ATTRIBUTE, INTO_DATE_ATTRIBUTE, CONSTRUCT_CHOICE,
			CONSTRAIN_CHOICE, INIT_VALUE, SPECIALIZE_INIT_VALUE, INIT_ORDER,
			SPECIALIZE_INIT_ORDER, RESULT, SPECIALIZE_RESULT, HIGHLIGHT,
			HIDE_IN_PROPERTIES_VIEW, HIDE_IN_TABLE_EDITOR, SHOW_IN_TABLE_EDITOR,
			SHOW_IN_PROPERTIES_VIEW, CLASS_WITH_STRING_PROPERTY,
			MOVE_TO_LAST_GROUP, MOVE_TO_NEW_GROUP, ADD_UPDATE_ANNOTATION,
			SPECIALIZE, ABSTRACT, };

	/**
	 * A public read-only list of all the '<em><b>MProperty Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPropertyAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProperty Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertyAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperty Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertyAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case INTO_STORED_VALUE:
			return INTO_STORED;
		case INTO_CONTAINED_VALUE:
			return INTO_CONTAINED;
		case INTO_DERIVED_VALUE:
			return INTO_DERIVED;
		case INTO_OPERATION_VALUE:
			return INTO_OPERATION;
		case INTO_CHANGEABLE_VALUE:
			return INTO_CHANGEABLE;
		case INTO_NOT_CHANGEABLE_VALUE:
			return INTO_NOT_CHANGEABLE;
		case INTO_UNARY_VALUE:
			return INTO_UNARY;
		case INTO_NARY_VALUE:
			return INTO_NARY;
		case INTO_STRING_ATTRIBUTE_VALUE:
			return INTO_STRING_ATTRIBUTE;
		case INTO_BOOLEAN_FLAG_VALUE:
			return INTO_BOOLEAN_FLAG;
		case INTO_INTEGER_ATTRIBUTE_VALUE:
			return INTO_INTEGER_ATTRIBUTE;
		case INTO_REAL_ATTRIBUTE_VALUE:
			return INTO_REAL_ATTRIBUTE;
		case INTO_DATE_ATTRIBUTE_VALUE:
			return INTO_DATE_ATTRIBUTE;
		case CONSTRUCT_CHOICE_VALUE:
			return CONSTRUCT_CHOICE;
		case CONSTRAIN_CHOICE_VALUE:
			return CONSTRAIN_CHOICE;
		case INIT_VALUE_VALUE:
			return INIT_VALUE;
		case SPECIALIZE_INIT_VALUE_VALUE:
			return SPECIALIZE_INIT_VALUE;
		case INIT_ORDER_VALUE:
			return INIT_ORDER;
		case SPECIALIZE_INIT_ORDER_VALUE:
			return SPECIALIZE_INIT_ORDER;
		case RESULT_VALUE:
			return RESULT;
		case SPECIALIZE_RESULT_VALUE:
			return SPECIALIZE_RESULT;
		case HIGHLIGHT_VALUE:
			return HIGHLIGHT;
		case HIDE_IN_PROPERTIES_VIEW_VALUE:
			return HIDE_IN_PROPERTIES_VIEW;
		case HIDE_IN_TABLE_EDITOR_VALUE:
			return HIDE_IN_TABLE_EDITOR;
		case SHOW_IN_TABLE_EDITOR_VALUE:
			return SHOW_IN_TABLE_EDITOR;
		case SHOW_IN_PROPERTIES_VIEW_VALUE:
			return SHOW_IN_PROPERTIES_VIEW;
		case CLASS_WITH_STRING_PROPERTY_VALUE:
			return CLASS_WITH_STRING_PROPERTY;
		case MOVE_TO_LAST_GROUP_VALUE:
			return MOVE_TO_LAST_GROUP;
		case MOVE_TO_NEW_GROUP_VALUE:
			return MOVE_TO_NEW_GROUP;
		case ADD_UPDATE_ANNOTATION_VALUE:
			return ADD_UPDATE_ANNOTATION;
		case SPECIALIZE_VALUE:
			return SPECIALIZE;
		case ABSTRACT_VALUE:
			return ABSTRACT;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPropertyAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPropertyAction
