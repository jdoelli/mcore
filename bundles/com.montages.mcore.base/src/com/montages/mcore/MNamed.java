/**
 */
package com.montages.mcore;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MNamed</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MNamed#getSpecialEName <em>Special EName</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getName <em>Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getShortName <em>Short Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getEName <em>EName</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getFullLabel <em>Full Label</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getLocalStructuralName <em>Local Structural Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getCalculatedName <em>Calculated Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getCalculatedShortName <em>Calculated Short Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getCorrectName <em>Correct Name</em>}</li>
 *   <li>{@link com.montages.mcore.MNamed#getCorrectShortName <em>Correct Short Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMNamed()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OCL label='eName'"
 * @generated
 */

public interface MNamed extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Calculated Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Short Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_CalculatedShortName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if stringEmpty(name) or stringEmpty(shortName) then calculatedName else shortName endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getCalculatedShortName();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #setName(String)
	 * @see com.montages.mcore.McorePackage#getMNamed_Name()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	String getName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamed#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #isSetName()
	 * @see #unsetName()
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamed#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	void unsetName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamed#getName <em>Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Name</em>' attribute is set.
	 * @see #unsetName()
	 * @see #getName()
	 * @see #setName(String)
	 * @generated
	 */
	boolean isSetName();

	/**
	 * Returns the value of the '<em><b>Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Short Name</em>' attribute.
	 * @see #isSetShortName()
	 * @see #unsetShortName()
	 * @see #setShortName(String)
	 * @see com.montages.mcore.McorePackage#getMNamed_ShortName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Abbreviation and Name' createColumn='false'"
	 * @generated
	 */
	String getShortName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamed#getShortName <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Short Name</em>' attribute.
	 * @see #isSetShortName()
	 * @see #unsetShortName()
	 * @see #getShortName()
	 * @generated
	 */
	void setShortName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamed#getShortName <em>Short Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetShortName()
	 * @see #getShortName()
	 * @see #setShortName(String)
	 * @generated
	 */
	void unsetShortName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamed#getShortName <em>Short Name</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Short Name</em>' attribute is set.
	 * @see #unsetShortName()
	 * @see #getShortName()
	 * @see #setShortName(String)
	 * @generated
	 */
	boolean isSetShortName();

	/**
	 * Returns the value of the '<em><b>Correct Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correct Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correct Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_CorrectName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='not stringEmpty(name)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCorrectName();

	/**
	 * Returns the value of the '<em><b>Correct Short Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Correct Short Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Correct Short Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_CorrectShortName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive=' stringEmpty(shortName)\r\n or (not stringEmpty(name))'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Validation'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getCorrectShortName();

	/**
	 * Returns the value of the '<em><b>EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>EName</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>EName</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_EName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen self.calculatedShortName.camelCaseLower()\r\nelse self.specialEName.camelCaseLower()\r\nendif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getEName();

	/**
	 * Returns the value of the '<em><b>Special EName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special EName</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special EName</em>' attribute.
	 * @see #isSetSpecialEName()
	 * @see #unsetSpecialEName()
	 * @see #setSpecialEName(String)
	 * @see com.montages.mcore.McorePackage#getMNamed_SpecialEName()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Special ECore Settings'"
	 * @generated
	 */
	String getSpecialEName();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MNamed#getSpecialEName <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Special EName</em>' attribute.
	 * @see #isSetSpecialEName()
	 * @see #unsetSpecialEName()
	 * @see #getSpecialEName()
	 * @generated
	 */
	void setSpecialEName(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MNamed#getSpecialEName <em>Special EName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSpecialEName()
	 * @see #getSpecialEName()
	 * @see #setSpecialEName(String)
	 * @generated
	 */
	void unsetSpecialEName();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MNamed#getSpecialEName <em>Special EName</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Special EName</em>' attribute is set.
	 * @see #unsetSpecialEName()
	 * @see #getSpecialEName()
	 * @see #setSpecialEName(String)
	 * @generated
	 */
	boolean isSetSpecialEName();

	/**
	 * Returns the value of the '<em><b>Full Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Full Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Full Label</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_FullLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'OVERRIDE IN SUBCLASS \'.concat(self.calculatedName)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getFullLabel();

	/**
	 * Returns the value of the '<em><b>Local Structural Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Local Structural Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Local Structural Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_LocalStructuralName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getLocalStructuralName();

	/**
	 * Returns the value of the '<em><b>Calculated Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Calculated Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Calculated Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMNamed_CalculatedName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if stringEmpty(name) then \' NAME MISSING\' else name endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Abbreviation and Name'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getCalculatedName();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='sameString(name, n.name)\r\n'"
	 * @generated
	 */
	Boolean sameName(MNamed n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='if stringEmpty(shortName)  then\r\n  sameString(name, n.shortName)\r\nelse if  stringEmpty(n.shortName) then\r\n sameString(shortName, n.name)\r\nelse sameString(shortName, n.shortName)\r\nendif endif'"
	 * @generated
	 */
	Boolean sameShortName(MNamed n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='s1=s2 \r\nor \r\n(s1.oclIsUndefined() and s2=\'\')\r\nor\r\n(s1=\'\' and s2.oclIsUndefined())'"
	 * @generated
	 */
	Boolean sameString(String s1, String s2);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='s.oclIsUndefined() or s=\'\''"
	 * @generated
	 */
	Boolean stringEmpty(String s);

} // MNamed
