/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MProperties Container Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMPropertiesContainerAction()
 * @model
 * @generated
 */
public enum MPropertiesContainerAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>New Attribute</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_ATTRIBUTE_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_ATTRIBUTE(1, "NewAttribute", "New Attribute"),

	/**
	 * The '<em><b>New Unary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_UNARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_UNARY_REFERENCE(2, "NewUnaryReference", "New [0..1] Reference"),

	/**
	 * The '<em><b>New Nary Reference</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_NARY_REFERENCE_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_NARY_REFERENCE(3, "NewNaryReference", "New [0..*] Reference"),

	/**
	 * The '<em><b>New Operation One Parameter</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_OPERATION_ONE_PARAMETER_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_OPERATION_ONE_PARAMETER(4, "NewOperationOneParameter",
			"New Operation, 1 Parameter"),

	/**
	 * The '<em><b>New Operation Two Parameters</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_OPERATION_TWO_PARAMETERS_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_OPERATION_TWO_PARAMETERS(5, "NewOperationTwoParameters",
			"New Operation, 2 Parameters"),

	/**
	 * The '<em><b>New Literal</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_LITERAL_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_LITERAL(6, "NewLiteral", "New Literal"),

	/**
	 * The '<em><b>New Properties Group</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #NEW_PROPERTIES_GROUP_VALUE
	 * @generated
	 * @ordered
	 */
	NEW_PROPERTIES_GROUP(7, "NewPropertiesGroup", "New Properties Group");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>New Attribute</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Attribute</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_ATTRIBUTE
	 * @model name="NewAttribute" literal="New Attribute"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_ATTRIBUTE_VALUE = 1;

	/**
	 * The '<em><b>New Unary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Unary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_UNARY_REFERENCE
	 * @model name="NewUnaryReference" literal="New [0..1] Reference"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_UNARY_REFERENCE_VALUE = 2;

	/**
	 * The '<em><b>New Nary Reference</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Nary Reference</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_NARY_REFERENCE
	 * @model name="NewNaryReference" literal="New [0..*] Reference"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_NARY_REFERENCE_VALUE = 3;

	/**
	 * The '<em><b>New Operation One Parameter</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Operation One Parameter</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_OPERATION_ONE_PARAMETER
	 * @model name="NewOperationOneParameter" literal="New Operation, 1 Parameter"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_OPERATION_ONE_PARAMETER_VALUE = 4;

	/**
	 * The '<em><b>New Operation Two Parameters</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Operation Two Parameters</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_OPERATION_TWO_PARAMETERS
	 * @model name="NewOperationTwoParameters" literal="New Operation, 2 Parameters"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_OPERATION_TWO_PARAMETERS_VALUE = 5;

	/**
	 * The '<em><b>New Literal</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Literal</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_LITERAL
	 * @model name="NewLiteral" literal="New Literal"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_LITERAL_VALUE = 6;

	/**
	 * The '<em><b>New Properties Group</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>New Properties Group</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #NEW_PROPERTIES_GROUP
	 * @model name="NewPropertiesGroup" literal="New Properties Group"
	 * @generated
	 * @ordered
	 */
	public static final int NEW_PROPERTIES_GROUP_VALUE = 7;

	/**
	 * An array of all the '<em><b>MProperties Container Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPropertiesContainerAction[] VALUES_ARRAY = new MPropertiesContainerAction[] {
			DO, NEW_ATTRIBUTE, NEW_UNARY_REFERENCE, NEW_NARY_REFERENCE,
			NEW_OPERATION_ONE_PARAMETER, NEW_OPERATION_TWO_PARAMETERS,
			NEW_LITERAL, NEW_PROPERTIES_GROUP, };

	/**
	 * A public read-only list of all the '<em><b>MProperties Container Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPropertiesContainerAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MProperties Container Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesContainerAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertiesContainerAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperties Container Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesContainerAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPropertiesContainerAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MProperties Container Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPropertiesContainerAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case NEW_ATTRIBUTE_VALUE:
			return NEW_ATTRIBUTE;
		case NEW_UNARY_REFERENCE_VALUE:
			return NEW_UNARY_REFERENCE;
		case NEW_NARY_REFERENCE_VALUE:
			return NEW_NARY_REFERENCE;
		case NEW_OPERATION_ONE_PARAMETER_VALUE:
			return NEW_OPERATION_ONE_PARAMETER;
		case NEW_OPERATION_TWO_PARAMETERS_VALUE:
			return NEW_OPERATION_TWO_PARAMETERS;
		case NEW_LITERAL_VALUE:
			return NEW_LITERAL;
		case NEW_PROPERTIES_GROUP_VALUE:
			return NEW_PROPERTIES_GROUP;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPropertiesContainerAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPropertiesContainerAction
