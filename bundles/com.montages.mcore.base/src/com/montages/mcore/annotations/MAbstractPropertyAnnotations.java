/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MProperty;
import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Property Annotations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MAbstractPropertyAnnotations#getContainingPropertiesContainer <em>Containing Properties Container</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAbstractPropertyAnnotations#getContainingClassifier <em>Containing Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAbstractPropertyAnnotations#getAnnotatedProperty <em>Annotated Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractPropertyAnnotations()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractPropertyAnnotations extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Annotated Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Annotated Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Annotated Property</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractPropertyAnnotations_AnnotatedProperty()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Annotated'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MProperty getAnnotatedProperty();

	/**
	 * Returns the value of the '<em><b>Containing Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Classifier</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractPropertyAnnotations_ContainingClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else let pc:MPropertiesContainer\r\n   = self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in\r\n  pc.containingClassifier  endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Annotated'"
	 * @generated
	 */
	MClassifier getContainingClassifier();

	/**
	 * Returns the value of the '<em><b>Containing Properties Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Properties Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Properties Container</em>' reference.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAbstractPropertyAnnotations_ContainingPropertiesContainer()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.eContainer().oclIsUndefined() \r\n  then null\r\n  else let pc:MPropertiesContainer\r\n   = self.eContainer().oclAsType(annotations::MClassifierAnnotations).eContainer().oclAsType(MPropertiesContainer) in\r\n  pc  endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertiesContainer getContainingPropertiesContainer();

} // MAbstractPropertyAnnotations
