
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Alternative Persistence Package</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateAlternativePersistencePackage()
 * @model annotation="http://www.xocl.org/OCL label='\'\'\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Alternative Package\'\n'"
 * @generated
 */

public interface MUpdateAlternativePersistencePackage
		extends MUpdatePersistence {

} // MUpdateAlternativePersistencePackage
