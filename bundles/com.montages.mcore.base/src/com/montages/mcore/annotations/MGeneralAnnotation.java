/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;
import org.xocl.semantics.XUpdate;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MGeneral Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MGeneralAnnotation#getSource <em>Source</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MGeneralAnnotation#getMGeneralDetail <em>MGeneral Detail</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MGeneralAnnotation#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralAnnotation()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Source\'\n'"
 * @generated
 */

public interface MGeneralAnnotation extends MAbstractAnnotion {
	/**
	 * Returns the value of the '<em><b>Source</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source</em>' attribute.
	 * @see #isSetSource()
	 * @see #unsetSource()
	 * @see #setSource(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralAnnotation_Source()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special General Annotation' createColumn='false'"
	 * @generated
	 */
	String getSource();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source</em>' attribute.
	 * @see #isSetSource()
	 * @see #unsetSource()
	 * @see #getSource()
	 * @generated
	 */
	void setSource(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getSource <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetSource()
	 * @see #getSource()
	 * @see #setSource(String)
	 * @generated
	 */
	void unsetSource();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getSource <em>Source</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Source</em>' attribute is set.
	 * @see #unsetSource()
	 * @see #getSource()
	 * @see #setSource(String)
	 * @generated
	 */
	boolean isSetSource();

	/**
	 * Returns the value of the '<em><b>MGeneral Detail</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MGeneralDetail}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>MGeneral Detail</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>MGeneral Detail</em>' containment reference list.
	 * @see #isSetMGeneralDetail()
	 * @see #unsetMGeneralDetail()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralAnnotation_MGeneralDetail()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Special General Annotation' createColumn='true'"
	 * @generated
	 */
	EList<MGeneralDetail> getMGeneralDetail();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getMGeneralDetail <em>MGeneral Detail</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetMGeneralDetail()
	 * @see #getMGeneralDetail()
	 * @generated
	 */
	void unsetMGeneralDetail();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getMGeneralDetail <em>MGeneral Detail</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>MGeneral Detail</em>' containment reference list is set.
	 * @see #unsetMGeneralDetail()
	 * @see #getMGeneralDetail()
	 * @generated
	 */
	boolean isSetMGeneralDetail();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.annotations.GeneralAnnotationAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.annotations.GeneralAnnotationAction
	 * @see #setDoAction(GeneralAnnotationAction)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralAnnotation_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='doAction'"
	 *        annotation="http://www.xocl.org/OCL derive='let const1: mcore::annotations::GeneralAnnotationAction = mcore::annotations::GeneralAnnotationAction::Do in\nconst1\n' choiceConstruction='let mySet : Set(GeneralAnnotationAction) = Set{GeneralAnnotationAction::Do}\r\nin\r\nmySet->including(if self.mGeneralDetail.key->count(\'documentation\') >=1 then GeneralAnnotationAction::IntoDescription else null endif)'"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Actions' createColumn='false'"
	 * @generated
	 */
	GeneralAnnotationAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MGeneralAnnotation#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.annotations.GeneralAnnotationAction
	 * @see #getDoAction()
	 * @generated
	 */

	void setDoAction(GeneralAnnotationAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(GeneralAnnotationAction trg);

} // MGeneralAnnotation
