/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MChoice Construction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMChoiceConstruction()
 * @model annotation="http://www.xocl.org/OCL label='let prefix: String = \'choice construction =\' in\nlet postfix: String = \'...\' in\nlet e1: String = prefix.concat(postfix) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Choice Values\'\n'"
 * @generated
 */

public interface MChoiceConstruction extends MExprAnnotation {
} // MChoiceConstruction
