/**
 */
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MBoolean Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMBooleanAnnotation()
 * @model
 * @generated
 */

public interface MBooleanAnnotation extends MExprAnnotation {
} // MBooleanAnnotation
