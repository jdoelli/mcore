/**
 */
package com.montages.mcore.annotations;

import org.eclipse.emf.common.util.EList;

import org.xocl.semantics.XUpdate;
import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MProperty Annotations</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverriddenIn <em>Overridden In</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getResult <em>Result</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitOrder <em>Init Order</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitValue <em>Init Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstruction <em>Choice Construction</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstraint <em>Choice Constraint</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getAdd <em>Add</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getUpdate <em>Update</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getStringAsOcl <em>String As Ocl</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getHighlight <em>Highlight</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getLayout <em>Layout</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MPropertyAnnotations#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations()
 * @model annotation="http://www.xocl.org/OCL label='(if self.property.oclIsUndefined() then \'PROPERTY MISSING\' else self.property.eLabel endif)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='if self.annotatedProperty.oclIsUndefined() \n     then \'PROPERTY of ?\' \n     else  annotatedProperty.kind.toString().toUpperCase().concat(\' of \')\n              .concat(annotatedProperty.containingClassifier.eName)\n--self.annotatedProperty.containingClassifier.eName.concat(\'-\').concat(\n--self.annotatedProperty.kindLabel.toUpperCase())\nendif \n  \n--if self.property.oclIsUndefined() then \'Select Property\' else self.property.eName endif .concat(\'-Semantics\')' annotatedPropertyDerive='self.property'"
 * @generated
 */

public interface MPropertyAnnotations extends MAbstractPropertyAnnotations {
	/**
	 * Returns the value of the '<em><b>Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' reference.
	 * @see #isSetProperty()
	 * @see #unsetProperty()
	 * @see #setProperty(MProperty)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Property()
	 * @model unsettable="true" required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='\r\nself.containingClassifier.allFeatures()' choiceConstraint='trg=self.property or self.containingClassifier.allPropertyAnnotations().property\r\n->excludes(trg)  '"
	 *        annotation="http://www.xocl.org/GENMODEL propertySortChoices='false'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotated' createColumn='false'"
	 * @generated
	 */
	MProperty getProperty();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property</em>' reference.
	 * @see #isSetProperty()
	 * @see #unsetProperty()
	 * @see #getProperty()
	 * @generated
	 */
	void setProperty(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getProperty <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetProperty()
	 * @see #getProperty()
	 * @see #setProperty(MProperty)
	 * @generated
	 */
	void unsetProperty();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getProperty <em>Property</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Property</em>' reference is set.
	 * @see #unsetProperty()
	 * @see #getProperty()
	 * @see #setProperty(MProperty)
	 * @generated
	 */
	boolean isSetProperty();

	/**
	 * Returns the value of the '<em><b>Overriding</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overriding</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overriding</em>' attribute.
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Overriding()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.property.oclIsUndefined() then false else not(self.property.containingClassifier = self.containingClassifier) endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Overriding' createColumn='false'"
	 * @generated
	 */
	Boolean getOverriding();

	/**
	 * Returns the value of the '<em><b>Overrides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overrides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overrides</em>' reference.
	 * @see #isSetOverrides()
	 * @see #unsetOverrides()
	 * @see #setOverrides(MPropertyAnnotations)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Overrides()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Overriding'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertyAnnotations getOverrides();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Overrides</em>' reference.
	 * @see #isSetOverrides()
	 * @see #unsetOverrides()
	 * @see #getOverrides()
	 * @generated
	 */
	void setOverrides(MPropertyAnnotations value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverrides <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOverrides()
	 * @see #getOverrides()
	 * @see #setOverrides(MPropertyAnnotations)
	 * @generated
	 */
	void unsetOverrides();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverrides <em>Overrides</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Overrides</em>' reference is set.
	 * @see #unsetOverrides()
	 * @see #getOverrides()
	 * @see #setOverrides(MPropertyAnnotations)
	 * @generated
	 */
	boolean isSetOverrides();

	/**
	 * Returns the value of the '<em><b>Overridden In</b></em>' reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MPropertyAnnotations}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Overridden In</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Overridden In</em>' reference list.
	 * @see #isSetOverriddenIn()
	 * @see #unsetOverriddenIn()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_OverriddenIn()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='Overriding'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<MPropertyAnnotations> getOverriddenIn();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverriddenIn <em>Overridden In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetOverriddenIn()
	 * @see #getOverriddenIn()
	 * @generated
	 */
	void unsetOverriddenIn();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getOverriddenIn <em>Overridden In</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Overridden In</em>' reference list is set.
	 * @see #unsetOverriddenIn()
	 * @see #getOverriddenIn()
	 * @generated
	 */
	boolean isSetOverriddenIn();

	/**
	 * Returns the value of the '<em><b>Result</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Result</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Result</em>' containment reference.
	 * @see #isSetResult()
	 * @see #unsetResult()
	 * @see #setResult(MResult)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Result()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MResult getResult();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getResult <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Result</em>' containment reference.
	 * @see #isSetResult()
	 * @see #unsetResult()
	 * @see #getResult()
	 * @generated
	 */
	void setResult(MResult value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getResult <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetResult()
	 * @see #getResult()
	 * @see #setResult(MResult)
	 * @generated
	 */
	void unsetResult();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getResult <em>Result</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Result</em>' containment reference is set.
	 * @see #unsetResult()
	 * @see #getResult()
	 * @see #setResult(MResult)
	 * @generated
	 */
	boolean isSetResult();

	/**
	 * Returns the value of the '<em><b>Init Order</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Order</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Order</em>' containment reference.
	 * @see #isSetInitOrder()
	 * @see #unsetInitOrder()
	 * @see #setInitOrder(MInitializationOrder)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_InitOrder()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MInitializationOrder getInitOrder();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitOrder <em>Init Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Order</em>' containment reference.
	 * @see #isSetInitOrder()
	 * @see #unsetInitOrder()
	 * @see #getInitOrder()
	 * @generated
	 */
	void setInitOrder(MInitializationOrder value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitOrder <em>Init Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitOrder()
	 * @see #getInitOrder()
	 * @see #setInitOrder(MInitializationOrder)
	 * @generated
	 */
	void unsetInitOrder();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitOrder <em>Init Order</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Init Order</em>' containment reference is set.
	 * @see #unsetInitOrder()
	 * @see #getInitOrder()
	 * @see #setInitOrder(MInitializationOrder)
	 * @generated
	 */
	boolean isSetInitOrder();

	/**
	 * Returns the value of the '<em><b>Init Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Init Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Init Value</em>' containment reference.
	 * @see #isSetInitValue()
	 * @see #unsetInitValue()
	 * @see #setInitValue(MInitializationValue)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_InitValue()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MInitializationValue getInitValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitValue <em>Init Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Init Value</em>' containment reference.
	 * @see #isSetInitValue()
	 * @see #unsetInitValue()
	 * @see #getInitValue()
	 * @generated
	 */
	void setInitValue(MInitializationValue value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitValue <em>Init Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInitValue()
	 * @see #getInitValue()
	 * @see #setInitValue(MInitializationValue)
	 * @generated
	 */
	void unsetInitValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getInitValue <em>Init Value</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Init Value</em>' containment reference is set.
	 * @see #unsetInitValue()
	 * @see #getInitValue()
	 * @see #setInitValue(MInitializationValue)
	 * @generated
	 */
	boolean isSetInitValue();

	/**
	 * Returns the value of the '<em><b>Choice Construction</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Construction</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Construction</em>' containment reference.
	 * @see #isSetChoiceConstruction()
	 * @see #unsetChoiceConstruction()
	 * @see #setChoiceConstruction(MChoiceConstruction)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_ChoiceConstruction()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MChoiceConstruction getChoiceConstruction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstruction <em>Choice Construction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice Construction</em>' containment reference.
	 * @see #isSetChoiceConstruction()
	 * @see #unsetChoiceConstruction()
	 * @see #getChoiceConstruction()
	 * @generated
	 */
	void setChoiceConstruction(MChoiceConstruction value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstruction <em>Choice Construction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetChoiceConstruction()
	 * @see #getChoiceConstruction()
	 * @see #setChoiceConstruction(MChoiceConstruction)
	 * @generated
	 */
	void unsetChoiceConstruction();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstruction <em>Choice Construction</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Choice Construction</em>' containment reference is set.
	 * @see #unsetChoiceConstruction()
	 * @see #getChoiceConstruction()
	 * @see #setChoiceConstruction(MChoiceConstruction)
	 * @generated
	 */
	boolean isSetChoiceConstruction();

	/**
	 * Returns the value of the '<em><b>Choice Constraint</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Choice Constraint</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Choice Constraint</em>' containment reference.
	 * @see #isSetChoiceConstraint()
	 * @see #unsetChoiceConstraint()
	 * @see #setChoiceConstraint(MChoiceConstraint)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_ChoiceConstraint()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MChoiceConstraint getChoiceConstraint();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstraint <em>Choice Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Choice Constraint</em>' containment reference.
	 * @see #isSetChoiceConstraint()
	 * @see #unsetChoiceConstraint()
	 * @see #getChoiceConstraint()
	 * @generated
	 */
	void setChoiceConstraint(MChoiceConstraint value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstraint <em>Choice Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetChoiceConstraint()
	 * @see #getChoiceConstraint()
	 * @see #setChoiceConstraint(MChoiceConstraint)
	 * @generated
	 */
	void unsetChoiceConstraint();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getChoiceConstraint <em>Choice Constraint</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Choice Constraint</em>' containment reference is set.
	 * @see #unsetChoiceConstraint()
	 * @see #getChoiceConstraint()
	 * @see #setChoiceConstraint(MChoiceConstraint)
	 * @generated
	 */
	boolean isSetChoiceConstraint();

	/**
	 * Returns the value of the '<em><b>Add</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Add</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Add</em>' containment reference.
	 * @see #isSetAdd()
	 * @see #unsetAdd()
	 * @see #setAdd(MAdd)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Add()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MAdd getAdd();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getAdd <em>Add</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Add</em>' containment reference.
	 * @see #isSetAdd()
	 * @see #unsetAdd()
	 * @see #getAdd()
	 * @generated
	 */
	void setAdd(MAdd value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getAdd <em>Add</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAdd()
	 * @see #getAdd()
	 * @see #setAdd(MAdd)
	 * @generated
	 */
	void unsetAdd();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getAdd <em>Add</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Add</em>' containment reference is set.
	 * @see #unsetAdd()
	 * @see #getAdd()
	 * @see #setAdd(MAdd)
	 * @generated
	 */
	boolean isSetAdd();

	/**
	 * Returns the value of the '<em><b>Update</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.annotations.MUpdate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update</em>' containment reference list.
	 * @see #isSetUpdate()
	 * @see #unsetUpdate()
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Update()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	EList<MUpdate> getUpdate();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getUpdate <em>Update</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetUpdate()
	 * @see #getUpdate()
	 * @generated
	 */
	void unsetUpdate();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getUpdate <em>Update</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Update</em>' containment reference list is set.
	 * @see #unsetUpdate()
	 * @see #getUpdate()
	 * @generated
	 */
	boolean isSetUpdate();

	/**
	 * Returns the value of the '<em><b>String As Ocl</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>String As Ocl</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>String As Ocl</em>' containment reference.
	 * @see #isSetStringAsOcl()
	 * @see #unsetStringAsOcl()
	 * @see #setStringAsOcl(MStringAsOclAnnotation)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_StringAsOcl()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MStringAsOclAnnotation getStringAsOcl();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getStringAsOcl <em>String As Ocl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>String As Ocl</em>' containment reference.
	 * @see #isSetStringAsOcl()
	 * @see #unsetStringAsOcl()
	 * @see #getStringAsOcl()
	 * @generated
	 */
	void setStringAsOcl(MStringAsOclAnnotation value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getStringAsOcl <em>String As Ocl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetStringAsOcl()
	 * @see #getStringAsOcl()
	 * @see #setStringAsOcl(MStringAsOclAnnotation)
	 * @generated
	 */
	void unsetStringAsOcl();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getStringAsOcl <em>String As Ocl</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>String As Ocl</em>' containment reference is set.
	 * @see #unsetStringAsOcl()
	 * @see #getStringAsOcl()
	 * @see #setStringAsOcl(MStringAsOclAnnotation)
	 * @generated
	 */
	boolean isSetStringAsOcl();

	/**
	 * Returns the value of the '<em><b>Highlight</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Highlight</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Highlight</em>' containment reference.
	 * @see #isSetHighlight()
	 * @see #unsetHighlight()
	 * @see #setHighlight(MHighlightAnnotation)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Highlight()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MHighlightAnnotation getHighlight();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getHighlight <em>Highlight</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Highlight</em>' containment reference.
	 * @see #isSetHighlight()
	 * @see #unsetHighlight()
	 * @see #getHighlight()
	 * @generated
	 */
	void setHighlight(MHighlightAnnotation value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getHighlight <em>Highlight</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetHighlight()
	 * @see #getHighlight()
	 * @see #setHighlight(MHighlightAnnotation)
	 * @generated
	 */
	void unsetHighlight();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getHighlight <em>Highlight</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Highlight</em>' containment reference is set.
	 * @see #unsetHighlight()
	 * @see #getHighlight()
	 * @see #setHighlight(MHighlightAnnotation)
	 * @generated
	 */
	boolean isSetHighlight();

	/**
	 * Returns the value of the '<em><b>Layout</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layout</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layout</em>' containment reference.
	 * @see #isSetLayout()
	 * @see #unsetLayout()
	 * @see #setLayout(MLayoutAnnotation)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_Layout()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Annotations' createColumn='true'"
	 * @generated
	 */
	MLayoutAnnotation getLayout();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getLayout <em>Layout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layout</em>' containment reference.
	 * @see #isSetLayout()
	 * @see #unsetLayout()
	 * @see #getLayout()
	 * @generated
	 */
	void setLayout(MLayoutAnnotation value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getLayout <em>Layout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLayout()
	 * @see #getLayout()
	 * @see #setLayout(MLayoutAnnotation)
	 * @generated
	 */
	void unsetLayout();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getLayout <em>Layout</em>}' containment reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Layout</em>' containment reference is set.
	 * @see #unsetLayout()
	 * @see #getLayout()
	 * @see #setLayout(MLayoutAnnotation)
	 * @generated
	 */
	boolean isSetLayout();

	/**
	 * Returns the value of the '<em><b>Do Action</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.mcore.annotations.MPropertyAnnotationsAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Do Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.annotations.MPropertyAnnotationsAction
	 * @see #setDoAction(MPropertyAnnotationsAction)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMPropertyAnnotations_DoAction()
	 * @model transient="true" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='mcore::annotations::MPropertyAnnotationsAction::Do\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Actions'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MPropertyAnnotationsAction getDoAction();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MPropertyAnnotations#getDoAction <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Do Action</em>' attribute.
	 * @see com.montages.mcore.annotations.MPropertyAnnotationsAction
	 * @see #getDoAction()
	 * @generated
	 */
	void setDoAction(MPropertyAnnotationsAction value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	XUpdate doAction$Update(MPropertyAnnotationsAction trg);

} // MPropertyAnnotations
