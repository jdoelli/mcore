/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MNamed;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAdd Command</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MAddCommand#getType <em>Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MAddCommand#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAddCommand()
 * @model
 * @generated
 */

public interface MAddCommand extends MExprAnnotation, MNamed {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #setType(MClassifier)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAddCommand_Type()
	 * @model unsettable="true"
	 * @generated
	 */
	MClassifier getType();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAddCommand#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #isSetType()
	 * @see #unsetType()
	 * @see #getType()
	 * @generated
	 */
	void setType(MClassifier value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAddCommand#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	void unsetType();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAddCommand#getType <em>Type</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Type</em>' reference is set.
	 * @see #unsetType()
	 * @see #getType()
	 * @see #setType(MClassifier)
	 * @generated
	 */
	boolean isSetType();

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #setLabel(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMAddCommand_Label()
	 * @model unsettable="true"
	 * @generated
	 */
	String getLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MAddCommand#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #isSetLabel()
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MAddCommand#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	void unsetLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MAddCommand#getLabel <em>Label</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Label</em>' attribute is set.
	 * @see #unsetLabel()
	 * @see #getLabel()
	 * @see #setLabel(String)
	 * @generated
	 */
	boolean isSetLabel();

} // MAddCommand
