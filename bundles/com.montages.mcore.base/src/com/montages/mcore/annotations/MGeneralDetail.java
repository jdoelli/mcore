/**
 */
package com.montages.mcore.annotations;

import com.montages.mcore.MRepositoryElement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MGeneral Detail</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.MGeneralDetail#getKey <em>Key</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.MGeneralDetail#getValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralDetail()
 * @model annotation="http://www.xocl.org/OCL label='let k: String = if key.oclIsUndefined() then \'<?>\' else key endif in\r\nlet v: String = if value.oclIsUndefined() then \'\' else value endif in\r\n\r\nk.concat(\' -> \').concat(v)'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Key\'\n'"
 * @generated
 */

public interface MGeneralDetail extends MRepositoryElement {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #isSetKey()
	 * @see #unsetKey()
	 * @see #setKey(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralDetail_Key()
	 * @model unsettable="true" required="true"
	 * @generated
	 */
	String getKey();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #isSetKey()
	 * @see #unsetKey()
	 * @see #getKey()
	 * @generated
	 */
	void setKey(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetKey()
	 * @see #getKey()
	 * @see #setKey(String)
	 * @generated
	 */
	void unsetKey();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getKey <em>Key</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Key</em>' attribute is set.
	 * @see #unsetKey()
	 * @see #getKey()
	 * @see #setKey(String)
	 * @generated
	 */
	boolean isSetKey();

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #setValue(String)
	 * @see com.montages.mcore.annotations.AnnotationsPackage#getMGeneralDetail_Value()
	 * @model unsettable="true"
	 * @generated
	 */
	String getValue();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #isSetValue()
	 * @see #unsetValue()
	 * @see #getValue()
	 * @generated
	 */
	void setValue(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	void unsetValue();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.annotations.MGeneralDetail#getValue <em>Value</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Value</em>' attribute is set.
	 * @see #unsetValue()
	 * @see #getValue()
	 * @see #setValue(String)
	 * @generated
	 */
	boolean isSetValue();

} // MGeneralDetail
