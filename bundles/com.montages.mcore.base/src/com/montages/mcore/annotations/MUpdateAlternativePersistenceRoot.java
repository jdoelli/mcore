
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Alternative Persistence Root</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdateAlternativePersistenceRoot()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL kindLabelDerive='\'Alternative Package Root\'\n'"
 * @generated
 */

public interface MUpdateAlternativePersistenceRoot extends MUpdatePersistence {

} // MUpdateAlternativePersistenceRoot
