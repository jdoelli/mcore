/**
 */
package com.montages.mcore.annotations.impl;

import com.montages.mcore.annotations.*;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.DefaultCommandMode;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MAddCommand;
import com.montages.mcore.annotations.MBooleanAnnotation;
import com.montages.mcore.annotations.MChoiceConstraint;
import com.montages.mcore.annotations.MChoiceConstruction;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import com.montages.mcore.annotations.MHighlightAnnotation;
import com.montages.mcore.annotations.MInitializationOrder;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MInvariantConstraint;
import com.montages.mcore.annotations.MLabel;
import com.montages.mcore.annotations.MLayoutAnnotation;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPackageAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MStringAsOclVariable;
import com.montages.mcore.annotations.MTrgAnnotation;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdateObject;
import com.montages.mcore.annotations.MUpdateValue;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsFactoryImpl extends EFactoryImpl
		implements AnnotationsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static AnnotationsFactory init() {
		try {
			AnnotationsFactory theAnnotationsFactory = (AnnotationsFactory) EPackage.Registry.INSTANCE
					.getEFactory(AnnotationsPackage.eNS_URI);
			if (theAnnotationsFactory != null) {
				return theAnnotationsFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new AnnotationsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case AnnotationsPackage.MPACKAGE_ANNOTATIONS:
			return createMPackageAnnotations();
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS:
			return createMClassifierAnnotations();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS:
			return createMPropertyAnnotations();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS:
			return createMOperationAnnotations();
		case AnnotationsPackage.MGENERAL_ANNOTATION:
			return createMGeneralAnnotation();
		case AnnotationsPackage.MGENERAL_DETAIL:
			return createMGeneralDetail();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION:
			return createMStringAsOclAnnotation();
		case AnnotationsPackage.MSTRING_AS_OCL_VARIABLE:
			return createMStringAsOclVariable();
		case AnnotationsPackage.MLAYOUT_ANNOTATION:
			return createMLayoutAnnotation();
		case AnnotationsPackage.MLABEL:
			return createMLabel();
		case AnnotationsPackage.MTRG_ANNOTATION:
			return createMTrgAnnotation();
		case AnnotationsPackage.MBOOLEAN_ANNOTATION:
			return createMBooleanAnnotation();
		case AnnotationsPackage.MINVARIANT_CONSTRAINT:
			return createMInvariantConstraint();
		case AnnotationsPackage.MRESULT:
			return createMResult();
		case AnnotationsPackage.MADD:
			return createMAdd();
		case AnnotationsPackage.MADD_COMMAND:
			return createMAddCommand();
		case AnnotationsPackage.MUPDATE:
			return createMUpdate();
		case AnnotationsPackage.MUPDATE_CONDITION:
			return createMUpdateCondition();
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION:
			return createMUpdatePersistenceLocation();
		case AnnotationsPackage.MUPDATE_OBJECT:
			return createMUpdateObject();
		case AnnotationsPackage.MUPDATE_VALUE:
			return createMUpdateValue();
		case AnnotationsPackage.MINITIALIZATION_ORDER:
			return createMInitializationOrder();
		case AnnotationsPackage.MINITIALIZATION_VALUE:
			return createMInitializationValue();
		case AnnotationsPackage.MHIGHLIGHT_ANNOTATION:
			return createMHighlightAnnotation();
		case AnnotationsPackage.MCHOICE_CONSTRUCTION:
			return createMChoiceConstruction();
		case AnnotationsPackage.MCHOICE_CONSTRAINT:
			return createMChoiceConstraint();
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_PACKAGE:
			return createMUpdateAlternativePersistencePackage();
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_ROOT:
			return createMUpdateAlternativePersistenceRoot();
		case AnnotationsPackage.MUPDATE_ALTERNATIVE_PERSISTENCE_REFERENCE:
			return createMUpdateAlternativePersistenceReference();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS_ACTION:
			return createMPropertyAnnotationsActionFromString(eDataType,
					initialValue);
		case AnnotationsPackage.MEXPR_ANNOTATION_ACTION:
			return createMExprAnnotationActionFromString(eDataType,
					initialValue);
		case AnnotationsPackage.DEFAULT_COMMAND_MODE:
			return createDefaultCommandModeFromString(eDataType, initialValue);
		case AnnotationsPackage.GENERAL_ANNOTATION_ACTION:
			return createGeneralAnnotationActionFromString(eDataType,
					initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS_ACTION:
			return convertMPropertyAnnotationsActionToString(eDataType,
					instanceValue);
		case AnnotationsPackage.MEXPR_ANNOTATION_ACTION:
			return convertMExprAnnotationActionToString(eDataType,
					instanceValue);
		case AnnotationsPackage.DEFAULT_COMMAND_MODE:
			return convertDefaultCommandModeToString(eDataType, instanceValue);
		case AnnotationsPackage.GENERAL_ANNOTATION_ACTION:
			return convertGeneralAnnotationActionToString(eDataType,
					instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPackageAnnotations createMPackageAnnotations() {
		MPackageAnnotationsImpl mPackageAnnotations = new MPackageAnnotationsImpl();
		return mPackageAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotations createMClassifierAnnotations() {
		MClassifierAnnotationsImpl mClassifierAnnotations = new MClassifierAnnotationsImpl();
		return mClassifierAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations createMPropertyAnnotations() {
		MPropertyAnnotationsImpl mPropertyAnnotations = new MPropertyAnnotationsImpl();
		return mPropertyAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotations createMOperationAnnotations() {
		MOperationAnnotationsImpl mOperationAnnotations = new MOperationAnnotationsImpl();
		return mOperationAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MGeneralAnnotation createMGeneralAnnotation() {
		MGeneralAnnotationImpl mGeneralAnnotation = new MGeneralAnnotationImpl();
		return mGeneralAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MGeneralDetail createMGeneralDetail() {
		MGeneralDetailImpl mGeneralDetail = new MGeneralDetailImpl();
		return mGeneralDetail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MStringAsOclAnnotation createMStringAsOclAnnotation() {
		MStringAsOclAnnotationImpl mStringAsOclAnnotation = new MStringAsOclAnnotationImpl();
		return mStringAsOclAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MStringAsOclVariable createMStringAsOclVariable() {
		MStringAsOclVariableImpl mStringAsOclVariable = new MStringAsOclVariableImpl();
		return mStringAsOclVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLayoutAnnotation createMLayoutAnnotation() {
		MLayoutAnnotationImpl mLayoutAnnotation = new MLayoutAnnotationImpl();
		return mLayoutAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLabel createMLabel() {
		MLabelImpl mLabel = new MLabelImpl();
		return mLabel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MTrgAnnotation createMTrgAnnotation() {
		MTrgAnnotationImpl mTrgAnnotation = new MTrgAnnotationImpl();
		return mTrgAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MBooleanAnnotation createMBooleanAnnotation() {
		MBooleanAnnotationImpl mBooleanAnnotation = new MBooleanAnnotationImpl();
		return mBooleanAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInvariantConstraint createMInvariantConstraint() {
		MInvariantConstraintImpl mInvariantConstraint = new MInvariantConstraintImpl();
		return mInvariantConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResult createMResult() {
		MResultImpl mResult = new MResultImpl();
		return mResult;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAdd createMAdd() {
		MAddImpl mAdd = new MAddImpl();
		return mAdd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAddCommand createMAddCommand() {
		MAddCommandImpl mAddCommand = new MAddCommandImpl();
		return mAddCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdate createMUpdate() {
		MUpdateImpl mUpdate = new MUpdateImpl();
		return mUpdate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateCondition createMUpdateCondition() {
		MUpdateConditionImpl mUpdateCondition = new MUpdateConditionImpl();
		return mUpdateCondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdatePersistenceLocation createMUpdatePersistenceLocation() {
		MUpdatePersistenceLocationImpl mUpdatePersistenceLocation = new MUpdatePersistenceLocationImpl();
		return mUpdatePersistenceLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateObject createMUpdateObject() {
		MUpdateObjectImpl mUpdateObject = new MUpdateObjectImpl();
		return mUpdateObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateValue createMUpdateValue() {
		MUpdateValueImpl mUpdateValue = new MUpdateValueImpl();
		return mUpdateValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationOrder createMInitializationOrder() {
		MInitializationOrderImpl mInitializationOrder = new MInitializationOrderImpl();
		return mInitializationOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationValue createMInitializationValue() {
		MInitializationValueImpl mInitializationValue = new MInitializationValueImpl();
		return mInitializationValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MHighlightAnnotation createMHighlightAnnotation() {
		MHighlightAnnotationImpl mHighlightAnnotation = new MHighlightAnnotationImpl();
		return mHighlightAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstruction createMChoiceConstruction() {
		MChoiceConstructionImpl mChoiceConstruction = new MChoiceConstructionImpl();
		return mChoiceConstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstraint createMChoiceConstraint() {
		MChoiceConstraintImpl mChoiceConstraint = new MChoiceConstraintImpl();
		return mChoiceConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistencePackage createMUpdateAlternativePersistencePackage() {
		MUpdateAlternativePersistencePackageImpl mUpdateAlternativePersistencePackage = new MUpdateAlternativePersistencePackageImpl();
		return mUpdateAlternativePersistencePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceRoot createMUpdateAlternativePersistenceRoot() {
		MUpdateAlternativePersistenceRootImpl mUpdateAlternativePersistenceRoot = new MUpdateAlternativePersistenceRootImpl();
		return mUpdateAlternativePersistenceRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdateAlternativePersistenceReference createMUpdateAlternativePersistenceReference() {
		MUpdateAlternativePersistenceReferenceImpl mUpdateAlternativePersistenceReference = new MUpdateAlternativePersistenceReferenceImpl();
		return mUpdateAlternativePersistenceReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotationsAction createMPropertyAnnotationsActionFromString(
			EDataType eDataType, String initialValue) {
		MPropertyAnnotationsAction result = MPropertyAnnotationsAction
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMPropertyAnnotationsActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MExprAnnotationAction createMExprAnnotationActionFromString(
			EDataType eDataType, String initialValue) {
		MExprAnnotationAction result = MExprAnnotationAction.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMExprAnnotationActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultCommandMode createDefaultCommandModeFromString(
			EDataType eDataType, String initialValue) {
		DefaultCommandMode result = DefaultCommandMode.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDefaultCommandModeToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralAnnotationAction createGeneralAnnotationActionFromString(
			EDataType eDataType, String initialValue) {
		GeneralAnnotationAction result = GeneralAnnotationAction
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGeneralAnnotationActionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsPackage getAnnotationsPackage() {
		return (AnnotationsPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static AnnotationsPackage getPackage() {
		return AnnotationsPackage.eINSTANCE;
	}

} //AnnotationsFactoryImpl
