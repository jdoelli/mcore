/**
 */
package com.montages.mcore.annotations.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MStringAsOclVariable;
import com.montages.mcore.impl.MRepositoryElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MString As Ocl Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MStringAsOclAnnotationImpl#getSelfObjectType <em>Self Object Type</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MStringAsOclAnnotationImpl#getSelfObjectMandatory <em>Self Object Mandatory</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MStringAsOclAnnotationImpl#getSelfObjectSingular <em>Self Object Singular</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MStringAsOclAnnotationImpl#getMStringAsOclVariable <em>MString As Ocl Variable</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MStringAsOclAnnotationImpl extends MRepositoryElementImpl
		implements MStringAsOclAnnotation {
	/**
	 * The cached value of the '{@link #getSelfObjectType() <em>Self Object Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectType()
	 * @generated
	 * @ordered
	 */
	protected MClassifier selfObjectType;

	/**
	 * This is true if the Self Object Type reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean selfObjectTypeESet;

	/**
	 * The default value of the '{@link #getSelfObjectMandatory() <em>Self Object Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SELF_OBJECT_MANDATORY_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getSelfObjectMandatory() <em>Self Object Mandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectMandatory()
	 * @generated
	 * @ordered
	 */
	protected Boolean selfObjectMandatory = SELF_OBJECT_MANDATORY_EDEFAULT;

	/**
	 * This is true if the Self Object Mandatory attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean selfObjectMandatoryESet;

	/**
	 * The default value of the '{@link #getSelfObjectSingular() <em>Self Object Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectSingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean SELF_OBJECT_SINGULAR_EDEFAULT = Boolean.FALSE;

	/**
	 * The cached value of the '{@link #getSelfObjectSingular() <em>Self Object Singular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectSingular()
	 * @generated
	 * @ordered
	 */
	protected Boolean selfObjectSingular = SELF_OBJECT_SINGULAR_EDEFAULT;

	/**
	 * This is true if the Self Object Singular attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean selfObjectSingularESet;

	/**
	 * The cached value of the '{@link #getMStringAsOclVariable() <em>MString As Ocl Variable</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMStringAsOclVariable()
	 * @generated
	 * @ordered
	 */
	protected EList<MStringAsOclVariable> mStringAsOclVariable;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MStringAsOclAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MSTRING_AS_OCL_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSelfObjectType() {
		if (selfObjectType != null && selfObjectType.eIsProxy()) {
			InternalEObject oldSelfObjectType = (InternalEObject) selfObjectType;
			selfObjectType = (MClassifier) eResolveProxy(oldSelfObjectType);
			if (selfObjectType != oldSelfObjectType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE,
							oldSelfObjectType, selfObjectType));
			}
		}
		return selfObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSelfObjectType() {
		return selfObjectType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelfObjectType(MClassifier newSelfObjectType) {
		MClassifier oldSelfObjectType = selfObjectType;
		selfObjectType = newSelfObjectType;
		boolean oldSelfObjectTypeESet = selfObjectTypeESet;
		selfObjectTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE,
					oldSelfObjectType, selfObjectType, !oldSelfObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSelfObjectType() {
		MClassifier oldSelfObjectType = selfObjectType;
		boolean oldSelfObjectTypeESet = selfObjectTypeESet;
		selfObjectType = null;
		selfObjectTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE,
					oldSelfObjectType, null, oldSelfObjectTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSelfObjectType() {
		return selfObjectTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSelfObjectMandatory() {
		return selfObjectMandatory;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelfObjectMandatory(Boolean newSelfObjectMandatory) {
		Boolean oldSelfObjectMandatory = selfObjectMandatory;
		selfObjectMandatory = newSelfObjectMandatory;
		boolean oldSelfObjectMandatoryESet = selfObjectMandatoryESet;
		selfObjectMandatoryESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY,
					oldSelfObjectMandatory, selfObjectMandatory,
					!oldSelfObjectMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSelfObjectMandatory() {
		Boolean oldSelfObjectMandatory = selfObjectMandatory;
		boolean oldSelfObjectMandatoryESet = selfObjectMandatoryESet;
		selfObjectMandatory = SELF_OBJECT_MANDATORY_EDEFAULT;
		selfObjectMandatoryESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY,
					oldSelfObjectMandatory, SELF_OBJECT_MANDATORY_EDEFAULT,
					oldSelfObjectMandatoryESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSelfObjectMandatory() {
		return selfObjectMandatoryESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getSelfObjectSingular() {
		return selfObjectSingular;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSelfObjectSingular(Boolean newSelfObjectSingular) {
		Boolean oldSelfObjectSingular = selfObjectSingular;
		selfObjectSingular = newSelfObjectSingular;
		boolean oldSelfObjectSingularESet = selfObjectSingularESet;
		selfObjectSingularESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR,
					oldSelfObjectSingular, selfObjectSingular,
					!oldSelfObjectSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSelfObjectSingular() {
		Boolean oldSelfObjectSingular = selfObjectSingular;
		boolean oldSelfObjectSingularESet = selfObjectSingularESet;
		selfObjectSingular = SELF_OBJECT_SINGULAR_EDEFAULT;
		selfObjectSingularESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR,
					oldSelfObjectSingular, SELF_OBJECT_SINGULAR_EDEFAULT,
					oldSelfObjectSingularESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSelfObjectSingular() {
		return selfObjectSingularESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MStringAsOclVariable> getMStringAsOclVariable() {
		if (mStringAsOclVariable == null) {
			mStringAsOclVariable = new EObjectContainmentEList.Unsettable.Resolving<MStringAsOclVariable>(
					MStringAsOclVariable.class, this,
					AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE);
		}
		return mStringAsOclVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMStringAsOclVariable() {
		if (mStringAsOclVariable != null)
			((InternalEList.Unsettable<?>) mStringAsOclVariable).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMStringAsOclVariable() {
		return mStringAsOclVariable != null
				&& ((InternalEList.Unsettable<?>) mStringAsOclVariable).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE:
			return ((InternalEList<?>) getMStringAsOclVariable())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE:
			if (resolve)
				return getSelfObjectType();
			return basicGetSelfObjectType();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY:
			return getSelfObjectMandatory();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR:
			return getSelfObjectSingular();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE:
			return getMStringAsOclVariable();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE:
			setSelfObjectType((MClassifier) newValue);
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY:
			setSelfObjectMandatory((Boolean) newValue);
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR:
			setSelfObjectSingular((Boolean) newValue);
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE:
			getMStringAsOclVariable().clear();
			getMStringAsOclVariable().addAll(
					(Collection<? extends MStringAsOclVariable>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE:
			unsetSelfObjectType();
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY:
			unsetSelfObjectMandatory();
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR:
			unsetSelfObjectSingular();
			return;
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE:
			unsetMStringAsOclVariable();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_TYPE:
			return isSetSelfObjectType();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_MANDATORY:
			return isSetSelfObjectMandatory();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__SELF_OBJECT_SINGULAR:
			return isSetSelfObjectSingular();
		case AnnotationsPackage.MSTRING_AS_OCL_ANNOTATION__MSTRING_AS_OCL_VARIABLE:
			return isSetMStringAsOclVariable();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (selfObjectMandatory: ");
		if (selfObjectMandatoryESet)
			result.append(selfObjectMandatory);
		else
			result.append("<unset>");
		result.append(", selfObjectSingular: ");
		if (selfObjectSingularESet)
			result.append(selfObjectSingular);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL '<stringAsOcl>'
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MSTRING_AS_OCL_ANNOTATION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MStringAsOclAnnotationImpl
