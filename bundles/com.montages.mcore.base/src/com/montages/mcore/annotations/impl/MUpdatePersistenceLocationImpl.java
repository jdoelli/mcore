
package com.montages.mcore.annotations.impl;

import com.montages.mcore.MClassifier;
import com.montages.mcore.McorePackage;
import com.montages.mcore.SimpleType;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.annotations.MUpdatePersistenceLocation;
import com.montages.mcore.expressions.ExpressionsPackage;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MUpdate Persistence Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MUpdatePersistenceLocationImpl#getContainingUpdateAnnotation <em>Containing Update Annotation</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MUpdatePersistenceLocationImpl extends MTrgAnnotationImpl
		implements MUpdatePersistenceLocation {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetObjectTypeOfAnnotation <em>Target Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression targetObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetSimpleTypeOfAnnotation <em>Target Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression targetSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAbstractPropertyAnnotations <em>Containing Abstract Property Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAbstractPropertyAnnotations
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression containingAbstractPropertyAnnotationsDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectTypeOfAnnotation <em>Object Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression objectObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getExpectedReturnSimpleTypeOfAnnotation <em>Expected Return Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExpectedReturnSimpleTypeOfAnnotation
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression expectedReturnSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectType <em>Object Object Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectType
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression objectObjectTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * Cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI16
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * Cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI17
	 * @generated
	 */
	private static Map<EStructuralFeature, OCLExpression> ourInitOrderOclExpressionMap = new HashMap<EStructuralFeature, OCLExpression>();

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MUpdatePersistenceLocationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdate getContainingUpdateAnnotation() {
		if (eContainerFeatureID() != AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION)
			return null;
		return (MUpdate) eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MUpdate basicGetContainingUpdateAnnotation() {
		if (eContainerFeatureID() != AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION)
			return null;
		return (MUpdate) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetContainingUpdateAnnotation(
			MUpdate newContainingUpdateAnnotation, NotificationChain msgs) {
		msgs = eBasicSetContainer(
				(InternalEObject) newContainingUpdateAnnotation,
				AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
				msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainingUpdateAnnotation(
			MUpdate newContainingUpdateAnnotation) {
		if (newContainingUpdateAnnotation != eInternalContainer()
				|| (eContainerFeatureID() != AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION
						&& newContainingUpdateAnnotation != null)) {
			if (EcoreUtil.isAncestor(this, newContainingUpdateAnnotation))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newContainingUpdateAnnotation != null)
				msgs = ((InternalEObject) newContainingUpdateAnnotation)
						.eInverseAdd(this, AnnotationsPackage.MUPDATE__LOCATION,
								MUpdate.class, msgs);
			msgs = basicSetContainingUpdateAnnotation(
					newContainingUpdateAnnotation, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION,
					newContainingUpdateAnnotation,
					newContainingUpdateAnnotation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetContainingUpdateAnnotation((MUpdate) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			return basicSetContainingUpdateAnnotation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			return eInternalContainer().eInverseRemove(this,
					AnnotationsPackage.MUPDATE__LOCATION, MUpdate.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			if (resolve)
				return getContainingUpdateAnnotation();
			return basicGetContainingUpdateAnnotation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			setContainingUpdateAnnotation((MUpdate) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			setContainingUpdateAnnotation((MUpdate) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MUPDATE_PERSISTENCE_LOCATION__CONTAINING_UPDATE_ANNOTATION:
			return basicGetContainingUpdateAnnotation() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL ''
	
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Persistence Location'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL targetObjectTypeOfAnnotation let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in
	if chain.oclIsUndefined()
	then null
	else if chain.oclIsKindOf(MPropertyAnnotations)
	then chain.oclAsType(MPropertyAnnotations)
	else null
	endif
	endif in
	if annotations = null
	then null
	else if annotations.annotatedProperty.oclIsUndefined()
	then null
	else annotations.annotatedProperty.calculatedType
	endif endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetTargetObjectTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION;

		if (targetObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				targetObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL targetSimpleTypeOfAnnotation let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in
	if chain.oclIsUndefined()
	then null
	else if chain.oclIsKindOf(MPropertyAnnotations)
	then chain.oclAsType(MPropertyAnnotations)
	else null
	endif
	endif in
	if annotations = null
	then null
	else if annotations.annotatedProperty.oclIsUndefined()
	then null
	else annotations.annotatedProperty.calculatedSimpleType
	endif endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getTargetSimpleTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION;

		if (targetSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				targetSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL containingAbstractPropertyAnnotations self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MAbstractPropertyAnnotations basicGetContainingAbstractPropertyAnnotations() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS;

		if (containingAbstractPropertyAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				containingAbstractPropertyAnnotationsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingAbstractPropertyAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MAbstractPropertyAnnotations) xoclEval
					.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL objectObjectTypeOfAnnotation if containingUpdateAnnotation.feature.oclIsUndefined()
	then null
	else containingUpdateAnnotation.feature.containingClassifier
	endif
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetObjectObjectTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION;

		if (objectObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				objectObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(objectObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL expectedReturnSimpleTypeOfAnnotation mcore::SimpleType::String
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public SimpleType getExpectedReturnSimpleTypeOfAnnotation() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MEXPR_ANNOTATION__EXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION;

		if (expectedReturnSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				expectedReturnSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(expectedReturnSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (SimpleType) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL objectObjectType objectObjectTypeOfAnnotation
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MClassifier basicGetObjectObjectType() {
		EClass eClass = (AnnotationsPackage.Literals.MUPDATE_PERSISTENCE_LOCATION);
		EStructuralFeature eOverrideFeature = ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE;

		if (objectObjectTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				objectObjectTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(objectObjectTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MClassifier) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Returns the cache for init annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS07
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOclExpressionMap() {
		return ourInitOclExpressionMap;
	}

	/**
	 * Returns the cache for init order annotation OCL expressions
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag INS08
	 * @generated
	 */
	public Map<EStructuralFeature, OCLExpression> getInitOrderOclExpressionMap() {
		return ourInitOrderOclExpressionMap;
	}
} //MUpdatePersistenceLocationImpl
