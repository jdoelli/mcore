/**
 */
package com.montages.mcore.annotations.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.SimpleType;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAnnotation;
import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAnnotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getDomainObject <em>Domain Object</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getDomainOperation <em>Domain Operation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getSelfObjectTypeOfAnnotation <em>Self Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getTargetObjectTypeOfAnnotation <em>Target Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getTargetSimpleTypeOfAnnotation <em>Target Simple Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getObjectObjectTypeOfAnnotation <em>Object Object Type Of Annotation</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getValue <em>Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL1 <em>Historized OCL1</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL1Timestamp <em>Historized OCL1 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL2 <em>Historized OCL2</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL2Timestamp <em>Historized OCL2 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL3 <em>Historized OCL3</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL3Timestamp <em>Historized OCL3 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL4 <em>Historized OCL4</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL4Timestamp <em>Historized OCL4 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL5 <em>Historized OCL5</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL5Timestamp <em>Historized OCL5 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL6 <em>Historized OCL6</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL6Timestamp <em>Historized OCL6 Timestamp</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL7 <em>Historized OCL7</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAnnotationImpl#getHistorizedOCL7Timestamp <em>Historized OCL7 Timestamp</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAnnotationImpl extends MAbstractAnnotionImpl
		implements MAnnotation {
	/**
	 * The cached value of the '{@link #getDomainObject() <em>Domain Object</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainObject()
	 * @generated
	 * @ordered
	 */
	protected MObject domainObject;

	/**
	 * This is true if the Domain Object reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainObjectESet;

	/**
	 * The cached value of the '{@link #getDomainOperation() <em>Domain Operation</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainOperation()
	 * @generated
	 * @ordered
	 */
	protected MOperationSignature domainOperation;

	/**
	 * This is true if the Domain Operation reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean domainOperationESet;

	/**
	 * The default value of the '{@link #getTargetSimpleTypeOfAnnotation() <em>Target Simple Type Of Annotation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleTypeOfAnnotation()
	 * @generated
	 * @ordered
	 */
	protected static final SimpleType TARGET_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT = SimpleType.NONE;

	/**
	 * The default value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected static final String VALUE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getValue() <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getValue()
	 * @generated
	 * @ordered
	 */
	protected String value = VALUE_EDEFAULT;

	/**
	 * This is true if the Value attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean valueESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL1() <em>Historized OCL1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL1()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL1_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL1() <em>Historized OCL1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL1()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL1 = HISTORIZED_OCL1_EDEFAULT;

	/**
	 * This is true if the Historized OCL1 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL1ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL1Timestamp() <em>Historized OCL1 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL1Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL1_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL1Timestamp() <em>Historized OCL1 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL1Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL1Timestamp = HISTORIZED_OCL1_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL1 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL1TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL2() <em>Historized OCL2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL2()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL2_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL2() <em>Historized OCL2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL2()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL2 = HISTORIZED_OCL2_EDEFAULT;

	/**
	 * This is true if the Historized OCL2 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL2ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL2Timestamp() <em>Historized OCL2 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL2Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL2_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL2Timestamp() <em>Historized OCL2 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL2Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL2Timestamp = HISTORIZED_OCL2_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL2 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL2TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL3() <em>Historized OCL3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL3()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL3_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL3() <em>Historized OCL3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL3()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL3 = HISTORIZED_OCL3_EDEFAULT;

	/**
	 * This is true if the Historized OCL3 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL3ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL3Timestamp() <em>Historized OCL3 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL3Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL3_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL3Timestamp() <em>Historized OCL3 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL3Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL3Timestamp = HISTORIZED_OCL3_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL3 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL3TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL4() <em>Historized OCL4</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL4()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL4_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL4() <em>Historized OCL4</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL4()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL4 = HISTORIZED_OCL4_EDEFAULT;

	/**
	 * This is true if the Historized OCL4 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL4ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL4Timestamp() <em>Historized OCL4 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL4Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL4_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL4Timestamp() <em>Historized OCL4 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL4Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL4Timestamp = HISTORIZED_OCL4_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL4 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL4TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL5() <em>Historized OCL5</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL5()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL5_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL5() <em>Historized OCL5</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL5()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL5 = HISTORIZED_OCL5_EDEFAULT;

	/**
	 * This is true if the Historized OCL5 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL5ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL5Timestamp() <em>Historized OCL5 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL5Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL5_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL5Timestamp() <em>Historized OCL5 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL5Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL5Timestamp = HISTORIZED_OCL5_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL5 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL5TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL6() <em>Historized OCL6</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL6()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL6_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL6() <em>Historized OCL6</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL6()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL6 = HISTORIZED_OCL6_EDEFAULT;

	/**
	 * This is true if the Historized OCL6 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL6ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL6Timestamp() <em>Historized OCL6 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL6Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL6_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL6Timestamp() <em>Historized OCL6 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL6Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL6Timestamp = HISTORIZED_OCL6_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL6 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL6TimestampESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL7() <em>Historized OCL7</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL7()
	 * @generated
	 * @ordered
	 */
	protected static final String HISTORIZED_OCL7_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL7() <em>Historized OCL7</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL7()
	 * @generated
	 * @ordered
	 */
	protected String historizedOCL7 = HISTORIZED_OCL7_EDEFAULT;

	/**
	 * This is true if the Historized OCL7 attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL7ESet;

	/**
	 * The default value of the '{@link #getHistorizedOCL7Timestamp() <em>Historized OCL7 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL7Timestamp()
	 * @generated
	 * @ordered
	 */
	protected static final Date HISTORIZED_OCL7_TIMESTAMP_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getHistorizedOCL7Timestamp() <em>Historized OCL7 Timestamp</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHistorizedOCL7Timestamp()
	 * @generated
	 * @ordered
	 */
	protected Date historizedOCL7Timestamp = HISTORIZED_OCL7_TIMESTAMP_EDEFAULT;

	/**
	 * This is true if the Historized OCL7 Timestamp attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean historizedOCL7TimestampESet;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getDomainObject <em>Domain Object</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainObject
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression domainObjectChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getDomainOperation <em>Domain Operation</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDomainOperation
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression domainOperationChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getSelfObjectTypeOfAnnotation <em>Self Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSelfObjectTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression selfObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetObjectTypeOfAnnotation <em>Target Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetObjectTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getTargetSimpleTypeOfAnnotation <em>Target Simple Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTargetSimpleTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression targetSimpleTypeOfAnnotationDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getObjectObjectTypeOfAnnotation <em>Object Object Type Of Annotation</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectObjectTypeOfAnnotation
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression objectObjectTypeOfAnnotationDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject getDomainObject() {
		if (domainObject != null && domainObject.eIsProxy()) {
			InternalEObject oldDomainObject = (InternalEObject) domainObject;
			domainObject = (MObject) eResolveProxy(oldDomainObject);
			if (domainObject != oldDomainObject) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT,
							oldDomainObject, domainObject));
			}
		}
		return domainObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObject basicGetDomainObject() {
		return domainObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainObject(MObject newDomainObject) {
		MObject oldDomainObject = domainObject;
		domainObject = newDomainObject;
		boolean oldDomainObjectESet = domainObjectESet;
		domainObjectESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT,
					oldDomainObject, domainObject, !oldDomainObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDomainObject() {
		MObject oldDomainObject = domainObject;
		boolean oldDomainObjectESet = domainObjectESet;
		domainObject = null;
		domainObjectESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT,
					oldDomainObject, null, oldDomainObjectESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainObject() {
		return domainObjectESet;
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Domain Object</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MObject>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.generalContent->isEmpty() 
	then self.generalReference
	else if self.generalReference->isEmpty()
	then self.generalContent
	else self.generalContent->asSequence()
	->union(self.generalReference->asSequence()) endif endif
	
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MObject> evalDomainObjectChoiceConstruction(
			List<MObject> choice) {
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		if (domainObjectChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = AnnotationsPackage.Literals.MANNOTATION__DOMAIN_OBJECT;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				domainObjectChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"DomainObjectChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(domainObjectChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "DomainObjectChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MObject> result = new ArrayList<MObject>(
					(Collection<MObject>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature getDomainOperation() {
		if (domainOperation != null && domainOperation.eIsProxy()) {
			InternalEObject oldDomainOperation = (InternalEObject) domainOperation;
			domainOperation = (MOperationSignature) eResolveProxy(
					oldDomainOperation);
			if (domainOperation != oldDomainOperation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION,
							oldDomainOperation, domainOperation));
			}
		}
		return domainOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature basicGetDomainOperation() {
		return domainOperation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomainOperation(MOperationSignature newDomainOperation) {
		MOperationSignature oldDomainOperation = domainOperation;
		domainOperation = newDomainOperation;
		boolean oldDomainOperationESet = domainOperationESet;
		domainOperationESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION,
					oldDomainOperation, domainOperation,
					!oldDomainOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDomainOperation() {
		MOperationSignature oldDomainOperation = domainOperation;
		boolean oldDomainOperationESet = domainOperationESet;
		domainOperation = null;
		domainOperationESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION,
					oldDomainOperation, null, oldDomainOperationESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDomainOperation() {
		return domainOperationESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Domain Operation</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MOperationSignature
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if domainObject.oclIsUndefined() 
	then false
	else if self.selfObjectTypeOfAnnotation.oclIsUndefined()
	then false
	else let c:MClassifier=domainObject.type in
	  if c.oclIsUndefined() 
	    then false
	    else let sts:OrderedSet(MClassifier) = 
	                self.selfObjectTypeOfAnnotation.allSubTypes()
	                   ->append(self.selfObjectTypeOfAnnotation) in
	    
	    c.allProperties().operationSignature
	     ->select(s:MOperationSignature|
	        if  s.parameter->size() = 1
	          then let p:MParameter = s.parameter->first() in
	            if p.type.oclIsUndefined() 
	               then false
	               else sts->includes(p.type) endif
	          else false endif)->includes(trg)
	
	    endif endif endif
	          
	         
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalDomainOperationChoiceConstraint(
			MOperationSignature trg) {
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		if (domainOperationChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = AnnotationsPackage.Literals.MANNOTATION__DOMAIN_OPERATION;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				domainOperationChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"DomainOperationChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(domainOperationChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "DomainOperationChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getSelfObjectTypeOfAnnotation() {
		MClassifier selfObjectTypeOfAnnotation = basicGetSelfObjectTypeOfAnnotation();
		return selfObjectTypeOfAnnotation != null
				&& selfObjectTypeOfAnnotation.eIsProxy()
						? (MClassifier) eResolveProxy(
								(InternalEObject) selfObjectTypeOfAnnotation)
						: selfObjectTypeOfAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetSelfObjectTypeOfAnnotation() {
		/**
		 * @OCL if eContainer().oclIsKindOf(annotations::MClassifierAnnotations) 
		then eContainer().oclAsType(annotations::MClassifierAnnotations).classifier 
		else if eContainer().oclIsTypeOf(annotations::MPropertyAnnotations) 
		then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier 
		else if eContainer().oclIsTypeOf(annotations::MOperationAnnotations) 
		then eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier
		else if eContainer().oclIsTypeOf(annotations::MUpdate)
		then eContainer().eContainer().eContainer().oclAsType(annotations::MClassifierAnnotations).classifier 
		else null 
		endif endif endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MANNOTATION__SELF_OBJECT_TYPE_OF_ANNOTATION;

		if (selfObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				selfObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(selfObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getTargetObjectTypeOfAnnotation() {
		MClassifier targetObjectTypeOfAnnotation = basicGetTargetObjectTypeOfAnnotation();
		return targetObjectTypeOfAnnotation != null
				&& targetObjectTypeOfAnnotation.eIsProxy()
						? (MClassifier) eResolveProxy(
								(InternalEObject) targetObjectTypeOfAnnotation)
						: targetObjectTypeOfAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetTargetObjectTypeOfAnnotation() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION;

		if (targetObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SimpleType getTargetSimpleTypeOfAnnotation() {
		/**
		 * @OCL mcore::SimpleType::None
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION;

		if (targetSimpleTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				targetSimpleTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(targetSimpleTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			SimpleType result = (SimpleType) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier getObjectObjectTypeOfAnnotation() {
		MClassifier objectObjectTypeOfAnnotation = basicGetObjectObjectTypeOfAnnotation();
		return objectObjectTypeOfAnnotation != null
				&& objectObjectTypeOfAnnotation.eIsProxy()
						? (MClassifier) eResolveProxy(
								(InternalEObject) objectObjectTypeOfAnnotation)
						: objectObjectTypeOfAnnotation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifier basicGetObjectObjectTypeOfAnnotation() {
		/**
		 * @OCL null
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION;

		if (objectObjectTypeOfAnnotationDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				objectObjectTypeOfAnnotationDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(objectObjectTypeOfAnnotationDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MClassifier result = (MClassifier) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(String newValue) {
		String oldValue = value;
		value = newValue;
		boolean oldValueESet = valueESet;
		valueESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__VALUE, oldValue, value,
					!oldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetValue() {
		String oldValue = value;
		boolean oldValueESet = valueESet;
		value = VALUE_EDEFAULT;
		valueESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__VALUE, oldValue,
					VALUE_EDEFAULT, oldValueESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetValue() {
		return valueESet;
	}

	/**
	 * Returns the OCL expression context for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag GFI03
	 * @generated NOT
	 */
	public OCLExpressionContext getValueOCLExpressionContext() {
		// We overwrite it in specific annotations types.
		return new OCLExpressionContext(OCLExpressionContext.getEObjectClass(),
				null, null);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL1() {
		return historizedOCL1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL1(String newHistorizedOCL1) {
		String oldHistorizedOCL1 = historizedOCL1;
		historizedOCL1 = newHistorizedOCL1;
		boolean oldHistorizedOCL1ESet = historizedOCL1ESet;
		historizedOCL1ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1,
					oldHistorizedOCL1, historizedOCL1, !oldHistorizedOCL1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL1() {
		String oldHistorizedOCL1 = historizedOCL1;
		boolean oldHistorizedOCL1ESet = historizedOCL1ESet;
		historizedOCL1 = HISTORIZED_OCL1_EDEFAULT;
		historizedOCL1ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1,
					oldHistorizedOCL1, HISTORIZED_OCL1_EDEFAULT,
					oldHistorizedOCL1ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL1() {
		return historizedOCL1ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL1Timestamp() {
		return historizedOCL1Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL1Timestamp(Date newHistorizedOCL1Timestamp) {
		Date oldHistorizedOCL1Timestamp = historizedOCL1Timestamp;
		historizedOCL1Timestamp = newHistorizedOCL1Timestamp;
		boolean oldHistorizedOCL1TimestampESet = historizedOCL1TimestampESet;
		historizedOCL1TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP,
					oldHistorizedOCL1Timestamp, historizedOCL1Timestamp,
					!oldHistorizedOCL1TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL1Timestamp() {
		Date oldHistorizedOCL1Timestamp = historizedOCL1Timestamp;
		boolean oldHistorizedOCL1TimestampESet = historizedOCL1TimestampESet;
		historizedOCL1Timestamp = HISTORIZED_OCL1_TIMESTAMP_EDEFAULT;
		historizedOCL1TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP,
					oldHistorizedOCL1Timestamp,
					HISTORIZED_OCL1_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL1TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL1Timestamp() {
		return historizedOCL1TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL2() {
		return historizedOCL2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL2(String newHistorizedOCL2) {
		String oldHistorizedOCL2 = historizedOCL2;
		historizedOCL2 = newHistorizedOCL2;
		boolean oldHistorizedOCL2ESet = historizedOCL2ESet;
		historizedOCL2ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2,
					oldHistorizedOCL2, historizedOCL2, !oldHistorizedOCL2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL2() {
		String oldHistorizedOCL2 = historizedOCL2;
		boolean oldHistorizedOCL2ESet = historizedOCL2ESet;
		historizedOCL2 = HISTORIZED_OCL2_EDEFAULT;
		historizedOCL2ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2,
					oldHistorizedOCL2, HISTORIZED_OCL2_EDEFAULT,
					oldHistorizedOCL2ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL2() {
		return historizedOCL2ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL2Timestamp() {
		return historizedOCL2Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL2Timestamp(Date newHistorizedOCL2Timestamp) {
		Date oldHistorizedOCL2Timestamp = historizedOCL2Timestamp;
		historizedOCL2Timestamp = newHistorizedOCL2Timestamp;
		boolean oldHistorizedOCL2TimestampESet = historizedOCL2TimestampESet;
		historizedOCL2TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP,
					oldHistorizedOCL2Timestamp, historizedOCL2Timestamp,
					!oldHistorizedOCL2TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL2Timestamp() {
		Date oldHistorizedOCL2Timestamp = historizedOCL2Timestamp;
		boolean oldHistorizedOCL2TimestampESet = historizedOCL2TimestampESet;
		historizedOCL2Timestamp = HISTORIZED_OCL2_TIMESTAMP_EDEFAULT;
		historizedOCL2TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP,
					oldHistorizedOCL2Timestamp,
					HISTORIZED_OCL2_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL2TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL2Timestamp() {
		return historizedOCL2TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL3() {
		return historizedOCL3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL3(String newHistorizedOCL3) {
		String oldHistorizedOCL3 = historizedOCL3;
		historizedOCL3 = newHistorizedOCL3;
		boolean oldHistorizedOCL3ESet = historizedOCL3ESet;
		historizedOCL3ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3,
					oldHistorizedOCL3, historizedOCL3, !oldHistorizedOCL3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL3() {
		String oldHistorizedOCL3 = historizedOCL3;
		boolean oldHistorizedOCL3ESet = historizedOCL3ESet;
		historizedOCL3 = HISTORIZED_OCL3_EDEFAULT;
		historizedOCL3ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3,
					oldHistorizedOCL3, HISTORIZED_OCL3_EDEFAULT,
					oldHistorizedOCL3ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL3() {
		return historizedOCL3ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL3Timestamp() {
		return historizedOCL3Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL3Timestamp(Date newHistorizedOCL3Timestamp) {
		Date oldHistorizedOCL3Timestamp = historizedOCL3Timestamp;
		historizedOCL3Timestamp = newHistorizedOCL3Timestamp;
		boolean oldHistorizedOCL3TimestampESet = historizedOCL3TimestampESet;
		historizedOCL3TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP,
					oldHistorizedOCL3Timestamp, historizedOCL3Timestamp,
					!oldHistorizedOCL3TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL3Timestamp() {
		Date oldHistorizedOCL3Timestamp = historizedOCL3Timestamp;
		boolean oldHistorizedOCL3TimestampESet = historizedOCL3TimestampESet;
		historizedOCL3Timestamp = HISTORIZED_OCL3_TIMESTAMP_EDEFAULT;
		historizedOCL3TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP,
					oldHistorizedOCL3Timestamp,
					HISTORIZED_OCL3_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL3TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL3Timestamp() {
		return historizedOCL3TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL4() {
		return historizedOCL4;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL4(String newHistorizedOCL4) {
		String oldHistorizedOCL4 = historizedOCL4;
		historizedOCL4 = newHistorizedOCL4;
		boolean oldHistorizedOCL4ESet = historizedOCL4ESet;
		historizedOCL4ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4,
					oldHistorizedOCL4, historizedOCL4, !oldHistorizedOCL4ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL4() {
		String oldHistorizedOCL4 = historizedOCL4;
		boolean oldHistorizedOCL4ESet = historizedOCL4ESet;
		historizedOCL4 = HISTORIZED_OCL4_EDEFAULT;
		historizedOCL4ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4,
					oldHistorizedOCL4, HISTORIZED_OCL4_EDEFAULT,
					oldHistorizedOCL4ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL4() {
		return historizedOCL4ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL4Timestamp() {
		return historizedOCL4Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL4Timestamp(Date newHistorizedOCL4Timestamp) {
		Date oldHistorizedOCL4Timestamp = historizedOCL4Timestamp;
		historizedOCL4Timestamp = newHistorizedOCL4Timestamp;
		boolean oldHistorizedOCL4TimestampESet = historizedOCL4TimestampESet;
		historizedOCL4TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP,
					oldHistorizedOCL4Timestamp, historizedOCL4Timestamp,
					!oldHistorizedOCL4TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL4Timestamp() {
		Date oldHistorizedOCL4Timestamp = historizedOCL4Timestamp;
		boolean oldHistorizedOCL4TimestampESet = historizedOCL4TimestampESet;
		historizedOCL4Timestamp = HISTORIZED_OCL4_TIMESTAMP_EDEFAULT;
		historizedOCL4TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP,
					oldHistorizedOCL4Timestamp,
					HISTORIZED_OCL4_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL4TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL4Timestamp() {
		return historizedOCL4TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL5() {
		return historizedOCL5;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL5(String newHistorizedOCL5) {
		String oldHistorizedOCL5 = historizedOCL5;
		historizedOCL5 = newHistorizedOCL5;
		boolean oldHistorizedOCL5ESet = historizedOCL5ESet;
		historizedOCL5ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5,
					oldHistorizedOCL5, historizedOCL5, !oldHistorizedOCL5ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL5() {
		String oldHistorizedOCL5 = historizedOCL5;
		boolean oldHistorizedOCL5ESet = historizedOCL5ESet;
		historizedOCL5 = HISTORIZED_OCL5_EDEFAULT;
		historizedOCL5ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5,
					oldHistorizedOCL5, HISTORIZED_OCL5_EDEFAULT,
					oldHistorizedOCL5ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL5() {
		return historizedOCL5ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL5Timestamp() {
		return historizedOCL5Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL5Timestamp(Date newHistorizedOCL5Timestamp) {
		Date oldHistorizedOCL5Timestamp = historizedOCL5Timestamp;
		historizedOCL5Timestamp = newHistorizedOCL5Timestamp;
		boolean oldHistorizedOCL5TimestampESet = historizedOCL5TimestampESet;
		historizedOCL5TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP,
					oldHistorizedOCL5Timestamp, historizedOCL5Timestamp,
					!oldHistorizedOCL5TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL5Timestamp() {
		Date oldHistorizedOCL5Timestamp = historizedOCL5Timestamp;
		boolean oldHistorizedOCL5TimestampESet = historizedOCL5TimestampESet;
		historizedOCL5Timestamp = HISTORIZED_OCL5_TIMESTAMP_EDEFAULT;
		historizedOCL5TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP,
					oldHistorizedOCL5Timestamp,
					HISTORIZED_OCL5_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL5TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL5Timestamp() {
		return historizedOCL5TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL6() {
		return historizedOCL6;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL6(String newHistorizedOCL6) {
		String oldHistorizedOCL6 = historizedOCL6;
		historizedOCL6 = newHistorizedOCL6;
		boolean oldHistorizedOCL6ESet = historizedOCL6ESet;
		historizedOCL6ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6,
					oldHistorizedOCL6, historizedOCL6, !oldHistorizedOCL6ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL6() {
		String oldHistorizedOCL6 = historizedOCL6;
		boolean oldHistorizedOCL6ESet = historizedOCL6ESet;
		historizedOCL6 = HISTORIZED_OCL6_EDEFAULT;
		historizedOCL6ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6,
					oldHistorizedOCL6, HISTORIZED_OCL6_EDEFAULT,
					oldHistorizedOCL6ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL6() {
		return historizedOCL6ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL6Timestamp() {
		return historizedOCL6Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL6Timestamp(Date newHistorizedOCL6Timestamp) {
		Date oldHistorizedOCL6Timestamp = historizedOCL6Timestamp;
		historizedOCL6Timestamp = newHistorizedOCL6Timestamp;
		boolean oldHistorizedOCL6TimestampESet = historizedOCL6TimestampESet;
		historizedOCL6TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP,
					oldHistorizedOCL6Timestamp, historizedOCL6Timestamp,
					!oldHistorizedOCL6TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL6Timestamp() {
		Date oldHistorizedOCL6Timestamp = historizedOCL6Timestamp;
		boolean oldHistorizedOCL6TimestampESet = historizedOCL6TimestampESet;
		historizedOCL6Timestamp = HISTORIZED_OCL6_TIMESTAMP_EDEFAULT;
		historizedOCL6TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP,
					oldHistorizedOCL6Timestamp,
					HISTORIZED_OCL6_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL6TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL6Timestamp() {
		return historizedOCL6TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHistorizedOCL7() {
		return historizedOCL7;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL7(String newHistorizedOCL7) {
		String oldHistorizedOCL7 = historizedOCL7;
		historizedOCL7 = newHistorizedOCL7;
		boolean oldHistorizedOCL7ESet = historizedOCL7ESet;
		historizedOCL7ESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7,
					oldHistorizedOCL7, historizedOCL7, !oldHistorizedOCL7ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL7() {
		String oldHistorizedOCL7 = historizedOCL7;
		boolean oldHistorizedOCL7ESet = historizedOCL7ESet;
		historizedOCL7 = HISTORIZED_OCL7_EDEFAULT;
		historizedOCL7ESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7,
					oldHistorizedOCL7, HISTORIZED_OCL7_EDEFAULT,
					oldHistorizedOCL7ESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL7() {
		return historizedOCL7ESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getHistorizedOCL7Timestamp() {
		return historizedOCL7Timestamp;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHistorizedOCL7Timestamp(Date newHistorizedOCL7Timestamp) {
		Date oldHistorizedOCL7Timestamp = historizedOCL7Timestamp;
		historizedOCL7Timestamp = newHistorizedOCL7Timestamp;
		boolean oldHistorizedOCL7TimestampESet = historizedOCL7TimestampESet;
		historizedOCL7TimestampESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP,
					oldHistorizedOCL7Timestamp, historizedOCL7Timestamp,
					!oldHistorizedOCL7TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHistorizedOCL7Timestamp() {
		Date oldHistorizedOCL7Timestamp = historizedOCL7Timestamp;
		boolean oldHistorizedOCL7TimestampESet = historizedOCL7TimestampESet;
		historizedOCL7Timestamp = HISTORIZED_OCL7_TIMESTAMP_EDEFAULT;
		historizedOCL7TimestampESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP,
					oldHistorizedOCL7Timestamp,
					HISTORIZED_OCL7_TIMESTAMP_EDEFAULT,
					oldHistorizedOCL7TimestampESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHistorizedOCL7Timestamp() {
		return historizedOCL7TimestampESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT:
			if (resolve)
				return getDomainObject();
			return basicGetDomainObject();
		case AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION:
			if (resolve)
				return getDomainOperation();
			return basicGetDomainOperation();
		case AnnotationsPackage.MANNOTATION__SELF_OBJECT_TYPE_OF_ANNOTATION:
			if (resolve)
				return getSelfObjectTypeOfAnnotation();
			return basicGetSelfObjectTypeOfAnnotation();
		case AnnotationsPackage.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION:
			if (resolve)
				return getTargetObjectTypeOfAnnotation();
			return basicGetTargetObjectTypeOfAnnotation();
		case AnnotationsPackage.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION:
			return getTargetSimpleTypeOfAnnotation();
		case AnnotationsPackage.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION:
			if (resolve)
				return getObjectObjectTypeOfAnnotation();
			return basicGetObjectObjectTypeOfAnnotation();
		case AnnotationsPackage.MANNOTATION__VALUE:
			return getValue();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1:
			return getHistorizedOCL1();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP:
			return getHistorizedOCL1Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2:
			return getHistorizedOCL2();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP:
			return getHistorizedOCL2Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3:
			return getHistorizedOCL3();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP:
			return getHistorizedOCL3Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4:
			return getHistorizedOCL4();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP:
			return getHistorizedOCL4Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5:
			return getHistorizedOCL5();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP:
			return getHistorizedOCL5Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6:
			return getHistorizedOCL6();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP:
			return getHistorizedOCL6Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7:
			return getHistorizedOCL7();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP:
			return getHistorizedOCL7Timestamp();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT:
			setDomainObject((MObject) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION:
			setDomainOperation((MOperationSignature) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__VALUE:
			setValue((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1:
			setHistorizedOCL1((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP:
			setHistorizedOCL1Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2:
			setHistorizedOCL2((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP:
			setHistorizedOCL2Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3:
			setHistorizedOCL3((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP:
			setHistorizedOCL3Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4:
			setHistorizedOCL4((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP:
			setHistorizedOCL4Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5:
			setHistorizedOCL5((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP:
			setHistorizedOCL5Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6:
			setHistorizedOCL6((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP:
			setHistorizedOCL6Timestamp((Date) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7:
			setHistorizedOCL7((String) newValue);
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP:
			setHistorizedOCL7Timestamp((Date) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT:
			unsetDomainObject();
			return;
		case AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION:
			unsetDomainOperation();
			return;
		case AnnotationsPackage.MANNOTATION__VALUE:
			unsetValue();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1:
			unsetHistorizedOCL1();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP:
			unsetHistorizedOCL1Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2:
			unsetHistorizedOCL2();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP:
			unsetHistorizedOCL2Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3:
			unsetHistorizedOCL3();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP:
			unsetHistorizedOCL3Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4:
			unsetHistorizedOCL4();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP:
			unsetHistorizedOCL4Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5:
			unsetHistorizedOCL5();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP:
			unsetHistorizedOCL5Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6:
			unsetHistorizedOCL6();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP:
			unsetHistorizedOCL6Timestamp();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7:
			unsetHistorizedOCL7();
			return;
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP:
			unsetHistorizedOCL7Timestamp();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MANNOTATION__DOMAIN_OBJECT:
			return isSetDomainObject();
		case AnnotationsPackage.MANNOTATION__DOMAIN_OPERATION:
			return isSetDomainOperation();
		case AnnotationsPackage.MANNOTATION__SELF_OBJECT_TYPE_OF_ANNOTATION:
			return basicGetSelfObjectTypeOfAnnotation() != null;
		case AnnotationsPackage.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION:
			return basicGetTargetObjectTypeOfAnnotation() != null;
		case AnnotationsPackage.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION:
			return getTargetSimpleTypeOfAnnotation() != TARGET_SIMPLE_TYPE_OF_ANNOTATION_EDEFAULT;
		case AnnotationsPackage.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION:
			return basicGetObjectObjectTypeOfAnnotation() != null;
		case AnnotationsPackage.MANNOTATION__VALUE:
			return isSetValue();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1:
			return isSetHistorizedOCL1();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP:
			return isSetHistorizedOCL1Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2:
			return isSetHistorizedOCL2();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP:
			return isSetHistorizedOCL2Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3:
			return isSetHistorizedOCL3();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP:
			return isSetHistorizedOCL3Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4:
			return isSetHistorizedOCL4();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP:
			return isSetHistorizedOCL4Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5:
			return isSetHistorizedOCL5();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP:
			return isSetHistorizedOCL5Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6:
			return isSetHistorizedOCL6();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP:
			return isSetHistorizedOCL6Timestamp();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7:
			return isSetHistorizedOCL7();
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP:
			return isSetHistorizedOCL7Timestamp();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (value: ");
		if (valueESet)
			result.append(value);
		else
			result.append("<unset>");
		result.append(", historizedOCL1: ");
		if (historizedOCL1ESet)
			result.append(historizedOCL1);
		else
			result.append("<unset>");
		result.append(", historizedOCL1Timestamp: ");
		if (historizedOCL1TimestampESet)
			result.append(historizedOCL1Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL2: ");
		if (historizedOCL2ESet)
			result.append(historizedOCL2);
		else
			result.append("<unset>");
		result.append(", historizedOCL2Timestamp: ");
		if (historizedOCL2TimestampESet)
			result.append(historizedOCL2Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL3: ");
		if (historizedOCL3ESet)
			result.append(historizedOCL3);
		else
			result.append("<unset>");
		result.append(", historizedOCL3Timestamp: ");
		if (historizedOCL3TimestampESet)
			result.append(historizedOCL3Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL4: ");
		if (historizedOCL4ESet)
			result.append(historizedOCL4);
		else
			result.append("<unset>");
		result.append(", historizedOCL4Timestamp: ");
		if (historizedOCL4TimestampESet)
			result.append(historizedOCL4Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL5: ");
		if (historizedOCL5ESet)
			result.append(historizedOCL5);
		else
			result.append("<unset>");
		result.append(", historizedOCL5Timestamp: ");
		if (historizedOCL5TimestampESet)
			result.append(historizedOCL5Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL6: ");
		if (historizedOCL6ESet)
			result.append(historizedOCL6);
		else
			result.append("<unset>");
		result.append(", historizedOCL6Timestamp: ");
		if (historizedOCL6TimestampESet)
			result.append(historizedOCL6Timestamp);
		else
			result.append("<unset>");
		result.append(", historizedOCL7: ");
		if (historizedOCL7ESet)
			result.append(historizedOCL7);
		else
			result.append("<unset>");
		result.append(", historizedOCL7Timestamp: ");
		if (historizedOCL7TimestampESet)
			result.append(historizedOCL7Timestamp);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MAnnotationImpl
