/**
 */
package com.montages.mcore.annotations.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MProperty;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MResult;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MOperation Annotations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MOperationAnnotationsImpl#getResult <em>Result</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MOperationAnnotationsImpl#getOperationSignature <em>Operation Signature</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MOperationAnnotationsImpl#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MOperationAnnotationsImpl#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MOperationAnnotationsImpl#getOverriddenIn <em>Overridden In</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MOperationAnnotationsImpl extends MAbstractPropertyAnnotationsImpl
		implements MOperationAnnotations {
	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected MResult result;

	/**
	 * This is true if the Result containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resultESet;

	/**
	 * The cached value of the '{@link #getOperationSignature() <em>Operation Signature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationSignature()
	 * @generated
	 * @ordered
	 */
	protected MOperationSignature operationSignature;

	/**
	 * This is true if the Operation Signature reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean operationSignatureESet;

	/**
	 * The default value of the '{@link #getOverriding() <em>Overriding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriding()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OVERRIDING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOverrides() <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverrides()
	 * @generated
	 * @ordered
	 */
	protected MOperationAnnotations overrides;

	/**
	 * This is true if the Overrides reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean overridesESet;

	/**
	 * The cached value of the '{@link #getOverriddenIn() <em>Overridden In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriddenIn()
	 * @generated
	 * @ordered
	 */
	protected EList<MOperationAnnotations> overriddenIn;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getOperationSignature <em>Operation Signature</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationSignature
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression operationSignatureChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getOperationSignature <em>Operation Signature</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOperationSignature
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression operationSignatureChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOverriding <em>Overriding</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriding
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression overridingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAnnotatedProperty <em>Annotated Property</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatedProperty
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression annotatedPropertyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MOperationAnnotationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature getOperationSignature() {
		if (operationSignature != null && operationSignature.eIsProxy()) {
			InternalEObject oldOperationSignature = (InternalEObject) operationSignature;
			operationSignature = (MOperationSignature) eResolveProxy(
					oldOperationSignature);
			if (operationSignature != oldOperationSignature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE,
							oldOperationSignature, operationSignature));
			}
		}
		return operationSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationSignature basicGetOperationSignature() {
		return operationSignature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOperationSignature(
			MOperationSignature newOperationSignature) {
		MOperationSignature oldOperationSignature = operationSignature;
		operationSignature = newOperationSignature;
		boolean oldOperationSignatureESet = operationSignatureESet;
		operationSignatureESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE,
					oldOperationSignature, operationSignature,
					!oldOperationSignatureESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unsetOperationSignature() {
		MOperationSignature oldOperationSignature = operationSignature;
		boolean oldOperationSignatureESet = operationSignatureESet;
		operationSignature = null;
		operationSignatureESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE,
					oldOperationSignature, null, oldOperationSignatureESet));
		//DOES NOT WORK, as container of oldOperationSignature is not known anymore... move logic to BehaviorDefinition of MProperty.
		//		MProperty p = (MProperty) oldOperationSignature.eContainer();
		//		if ((p.getAllSignatures().isEmpty())
		//				|| ((p.getAllSignatures().get(0) == oldOperationSignature) && (p
		//						.getAllSignatures().size() == 1))) {
		//			//make property "derived"
		//			p.setHasStorage(false);
		//			p.setPersisted(false);
		//			p.setChangeable(false);//only if no update annotation exists.
		//			p.setContainment(false);
		//			//Add Property Annotation
		//			MClassifierAnnotations semantics = (MClassifierAnnotations) eContainer();
		//			MPropertyAnnotations newPropertyAnnotations = AnnotationsFactory.eINSTANCE
		//					.createMPropertyAnnotations();
		//			semantics.getPropertyAnnotations().add(newPropertyAnnotations);
		//			newPropertyAnnotations.setProperty(p);
		//			//COPY OVER OCL STUFF and MRULES!!!
		//			newPropertyAnnotations.setResult(this.getResult());
		//			//FINALLY DESTROY THIS ELEMENT
		//			semantics.getOperationAnnotations().remove(this);
		//		} else {
		//
		//		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOperationSignature() {
		return operationSignatureESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Operation Signature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MOperationSignature
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg=self.operationSignature or 
	self.containingClassifier.allOperationAnnotations().operationSignature->excludes(trg)
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalOperationSignatureChoiceConstraint(
			MOperationSignature trg) {
		EClass eClass = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS;
		if (operationSignatureChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				operationSignatureChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"OperationSignatureChoiceConstraint");
			}
		}
		Query query = OCL_ENV
				.createQuery(operationSignatureChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "OperationSignatureChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Operation Signature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MOperationSignature>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL self.containingClassifier.allOperationSignatures()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MOperationSignature> evalOperationSignatureChoiceConstruction(
			List<MOperationSignature> choice) {
		EClass eClass = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS;
		if (operationSignatureChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				operationSignatureChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"OperationSignatureChoiceConstruction");
			}
		}
		Query query = OCL_ENV
				.createQuery(operationSignatureChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "OperationSignatureChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MOperationSignature> result = new ArrayList<MOperationSignature>(
					(Collection<MOperationSignature>) query.evaluate(this));
			result.remove(null);
			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResult getResult() {
		if (result != null && result.eIsProxy()) {
			InternalEObject oldResult = (InternalEObject) result;
			result = (MResult) eResolveProxy(oldResult);
			if (result != oldResult) {
				InternalEObject newResult = (InternalEObject) result;
				NotificationChain msgs = oldResult.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
						null, null);
				if (newResult.eInternalContainer() == null) {
					msgs = newResult.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
							oldResult, result));
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResult basicGetResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResult(MResult newResult,
			NotificationChain msgs) {
		MResult oldResult = result;
		result = newResult;
		boolean oldResultESet = resultESet;
		resultESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
					oldResult, newResult, !oldResultESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(MResult newResult) {
		if (newResult != result) {
			NotificationChain msgs = null;
			if (result != null)
				msgs = ((InternalEObject) result).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
						null, msgs);
			if (newResult != null)
				msgs = ((InternalEObject) newResult).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
						null, msgs);
			msgs = basicSetResult(newResult, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldResultESet = resultESet;
			resultESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
						newResult, newResult, !oldResultESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetResult(NotificationChain msgs) {
		MResult oldResult = result;
		result = null;
		boolean oldResultESet = resultESet;
		resultESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
					oldResult, null, oldResultESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResult() {
		if (result != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) result).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT,
					null, msgs);
			msgs = basicUnsetResult(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldResultESet = resultESet;
			resultESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT, null,
						null, oldResultESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResult() {
		return resultESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getOverriding() {
		/**
		 * @OCL if self.operationSignature.oclIsUndefined() then false else 
		not(self.operationSignature.containingClassifier = self.containingClassifier) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OVERRIDING;

		if (overridingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				overridingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(overridingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotations getOverrides() {
		if (overrides != null && overrides.eIsProxy()) {
			InternalEObject oldOverrides = (InternalEObject) overrides;
			overrides = (MOperationAnnotations) eResolveProxy(oldOverrides);
			if (overrides != oldOverrides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES,
							oldOverrides, overrides));
			}
		}
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotations basicGetOverrides() {
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverrides(MOperationAnnotations newOverrides) {
		MOperationAnnotations oldOverrides = overrides;
		overrides = newOverrides;
		boolean oldOverridesESet = overridesESet;
		overridesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES,
					oldOverrides, overrides, !oldOverridesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOverrides() {
		MOperationAnnotations oldOverrides = overrides;
		boolean oldOverridesESet = overridesESet;
		overrides = null;
		overridesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES,
					oldOverrides, null, oldOverridesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOverrides() {
		return overridesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MOperationAnnotations> getOverriddenIn() {
		if (overriddenIn == null) {
			overriddenIn = new EObjectResolvingEList.Unsettable<MOperationAnnotations>(
					MOperationAnnotations.class, this,
					AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN);
		}
		return overriddenIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOverriddenIn() {
		if (overriddenIn != null)
			((InternalEList.Unsettable<?>) overriddenIn).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOverriddenIn() {
		return overriddenIn != null
				&& ((InternalEList.Unsettable<?>) overriddenIn).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			return basicUnsetResult(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			if (resolve)
				return getResult();
			return basicGetResult();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE:
			if (resolve)
				return getOperationSignature();
			return basicGetOperationSignature();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDING:
			return getOverriding();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES:
			if (resolve)
				return getOverrides();
			return basicGetOverrides();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN:
			return getOverriddenIn();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			setResult((MResult) newValue);
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE:
			setOperationSignature((MOperationSignature) newValue);
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES:
			setOverrides((MOperationAnnotations) newValue);
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN:
			getOverriddenIn().clear();
			getOverriddenIn().addAll(
					(Collection<? extends MOperationAnnotations>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			unsetResult();
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE:
			unsetOperationSignature();
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES:
			unsetOverrides();
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN:
			unsetOverriddenIn();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			return isSetResult();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE:
			return isSetOperationSignature();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDING:
			return OVERRIDING_EDEFAULT == null ? getOverriding() != null
					: !OVERRIDING_EDEFAULT.equals(getOverriding());
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDES:
			return isSetOverrides();
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN:
			return isSetOverriddenIn();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if self.operationSignature.oclIsUndefined() then 'OP SIGNATURE MISSING' else self.operationSignature.eLabel endif
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL annotatedProperty if self.operationSignature.oclIsUndefined() then null
	else self.operationSignature.operation endif
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetAnnotatedProperty() {
		EClass eClass = (AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY;

		if (annotatedPropertyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				annotatedPropertyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(annotatedPropertyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel if self.operationSignature.oclIsUndefined() then 
	'OPERATION of ?'
	else 'OPERATION of '.concat(annotatedProperty.containingClassifier.eName) endif
	
	--self.annotatedProperty.containingClassifier.eName.concat('-OPERATION') endif
	--if self.operationSignature.oclIsUndefined() then 'Select Signature' else self.operationSignature.eLabel endif .concat('-Semantics')
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MOperationAnnotationsImpl
