/**
 */
package com.montages.mcore.annotations.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.DefaultCommandMode;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MAddCommand;
import com.montages.mcore.impl.MRepositoryElementImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAdd</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MAddImpl#getDefaultCommandMode <em>Default Command Mode</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAddImpl#getAddCommand <em>Add Command</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MAddImpl extends MRepositoryElementImpl implements MAdd {
	/**
	 * The default value of the '{@link #getDefaultCommandMode() <em>Default Command Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultCommandMode()
	 * @generated
	 * @ordered
	 */
	protected static final DefaultCommandMode DEFAULT_COMMAND_MODE_EDEFAULT = DefaultCommandMode.HIDE_DEFAULTFOR_EXPLICIT_ADD;

	/**
	 * The cached value of the '{@link #getDefaultCommandMode() <em>Default Command Mode</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDefaultCommandMode()
	 * @generated
	 * @ordered
	 */
	protected DefaultCommandMode defaultCommandMode = DEFAULT_COMMAND_MODE_EDEFAULT;

	/**
	 * This is true if the Default Command Mode attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean defaultCommandModeESet;

	/**
	 * The cached value of the '{@link #getAddCommand() <em>Add Command</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAddCommand()
	 * @generated
	 * @ordered
	 */
	protected EList<MAddCommand> addCommand;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAddImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MADD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DefaultCommandMode getDefaultCommandMode() {
		return defaultCommandMode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDefaultCommandMode(
			DefaultCommandMode newDefaultCommandMode) {
		DefaultCommandMode oldDefaultCommandMode = defaultCommandMode;
		defaultCommandMode = newDefaultCommandMode == null
				? DEFAULT_COMMAND_MODE_EDEFAULT : newDefaultCommandMode;
		boolean oldDefaultCommandModeESet = defaultCommandModeESet;
		defaultCommandModeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE,
					oldDefaultCommandMode, defaultCommandMode,
					!oldDefaultCommandModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetDefaultCommandMode() {
		DefaultCommandMode oldDefaultCommandMode = defaultCommandMode;
		boolean oldDefaultCommandModeESet = defaultCommandModeESet;
		defaultCommandMode = DEFAULT_COMMAND_MODE_EDEFAULT;
		defaultCommandModeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE,
					oldDefaultCommandMode, DEFAULT_COMMAND_MODE_EDEFAULT,
					oldDefaultCommandModeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetDefaultCommandMode() {
		return defaultCommandModeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MAddCommand> getAddCommand() {
		if (addCommand == null) {
			addCommand = new EObjectContainmentEList.Unsettable.Resolving<MAddCommand>(
					MAddCommand.class, this,
					AnnotationsPackage.MADD__ADD_COMMAND);
		}
		return addCommand;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAddCommand() {
		if (addCommand != null)
			((InternalEList.Unsettable<?>) addCommand).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAddCommand() {
		return addCommand != null
				&& ((InternalEList.Unsettable<?>) addCommand).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MADD__ADD_COMMAND:
			return ((InternalEList<?>) getAddCommand()).basicRemove(otherEnd,
					msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE:
			return getDefaultCommandMode();
		case AnnotationsPackage.MADD__ADD_COMMAND:
			return getAddCommand();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE:
			setDefaultCommandMode((DefaultCommandMode) newValue);
			return;
		case AnnotationsPackage.MADD__ADD_COMMAND:
			getAddCommand().clear();
			getAddCommand()
					.addAll((Collection<? extends MAddCommand>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE:
			unsetDefaultCommandMode();
			return;
		case AnnotationsPackage.MADD__ADD_COMMAND:
			unsetAddCommand();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MADD__DEFAULT_COMMAND_MODE:
			return isSetDefaultCommandMode();
		case AnnotationsPackage.MADD__ADD_COMMAND:
			return isSetAddCommand();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (defaultCommandMode: ");
		if (defaultCommandModeESet)
			result.append(defaultCommandMode);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //MAddImpl
