/**
 */
package com.montages.mcore.annotations.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

import com.montages.mcore.MModelElement;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAbstractAnnotion;
import com.montages.mcore.annotations.MAbstractPropertyAnnotations;
import com.montages.mcore.impl.MRepositoryElementImpl;
import com.montages.mcore.objects.MObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MAbstract Annotion</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractAnnotionImpl#getGeneralReference <em>General Reference</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractAnnotionImpl#getGeneralContent <em>General Content</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractAnnotionImpl#getAnnotatedElement <em>Annotated Element</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MAbstractAnnotionImpl#getContainingAbstractPropertyAnnotations <em>Containing Abstract Property Annotations</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class MAbstractAnnotionImpl extends MRepositoryElementImpl
		implements MAbstractAnnotion {
	/**
	 * The cached value of the '{@link #getGeneralReference() <em>General Reference</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralReference()
	 * @generated
	 * @ordered
	 */
	protected EList<MObject> generalReference;

	/**
	 * The cached value of the '{@link #getGeneralContent() <em>General Content</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGeneralContent()
	 * @generated
	 * @ordered
	 */
	protected EList<MObject> generalContent;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAnnotatedElement <em>Annotated Element</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatedElement
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression annotatedElementDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getContainingAbstractPropertyAnnotations <em>Containing Abstract Property Annotations</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getContainingAbstractPropertyAnnotations
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression containingAbstractPropertyAnnotationsDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MAbstractAnnotionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MABSTRACT_ANNOTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getGeneralReference() {
		if (generalReference == null) {
			generalReference = new EObjectResolvingEList.Unsettable<MObject>(
					MObject.class, this,
					AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_REFERENCE);
		}
		return generalReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralReference() {
		if (generalReference != null)
			((InternalEList.Unsettable<?>) generalReference).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralReference() {
		return generalReference != null
				&& ((InternalEList.Unsettable<?>) generalReference).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MObject> getGeneralContent() {
		if (generalContent == null) {
			generalContent = new EObjectContainmentEList.Unsettable.Resolving<MObject>(
					MObject.class, this,
					AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT);
		}
		return generalContent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetGeneralContent() {
		if (generalContent != null)
			((InternalEList.Unsettable<?>) generalContent).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetGeneralContent() {
		return generalContent != null
				&& ((InternalEList.Unsettable<?>) generalContent).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MModelElement getAnnotatedElement() {
		MModelElement annotatedElement = basicGetAnnotatedElement();
		return annotatedElement != null && annotatedElement.eIsProxy()
				? (MModelElement) eResolveProxy(
						(InternalEObject) annotatedElement)
				: annotatedElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MModelElement basicGetAnnotatedElement() {
		/**
		 * @OCL if not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MPropertyAnnotations) 
		then self.containingAbstractPropertyAnnotations.oclAsType(MPropertyAnnotations).property
		else 
		if not self.containingAbstractPropertyAnnotations.oclIsUndefined() and self.containingAbstractPropertyAnnotations.oclIsKindOf(MOperationAnnotations)
		then self.containingAbstractPropertyAnnotations.oclAsType(MOperationAnnotations).operationSignature
		else 
			if self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MClassifierAnnotations) 
				then  self.oclAsType(ecore::EObject).eContainer().oclAsType(MClassifierAnnotations).classifier
				else 
					if self.oclAsType(ecore::EObject).eContainer().oclIsKindOf(MModelElement) 
						then self.oclAsType(ecore::EObject).eContainer().oclAsType(MModelElement) 
						else null 
					endif
			endif
		endif
		endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MABSTRACT_ANNOTION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MABSTRACT_ANNOTION__ANNOTATED_ELEMENT;

		if (annotatedElementDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				annotatedElementDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MABSTRACT_ANNOTION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(annotatedElementDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MABSTRACT_ANNOTION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MModelElement result = (MModelElement) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractPropertyAnnotations getContainingAbstractPropertyAnnotations() {
		MAbstractPropertyAnnotations containingAbstractPropertyAnnotations = basicGetContainingAbstractPropertyAnnotations();
		return containingAbstractPropertyAnnotations != null
				&& containingAbstractPropertyAnnotations.eIsProxy()
						? (MAbstractPropertyAnnotations) eResolveProxy(
								(InternalEObject) containingAbstractPropertyAnnotations)
						: containingAbstractPropertyAnnotations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractPropertyAnnotations basicGetContainingAbstractPropertyAnnotations() {
		/**
		 * @OCL if self.eContainer().oclIsKindOf(MAbstractPropertyAnnotations) then
		self.eContainer().oclAsType(MAbstractPropertyAnnotations)
		else null endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MABSTRACT_ANNOTION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS;

		if (containingAbstractPropertyAnnotationsDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				containingAbstractPropertyAnnotationsDeriveOCL = helper
						.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MABSTRACT_ANNOTION,
						eFeature);
			}
		}

		Query query = OCL_ENV
				.createQuery(containingAbstractPropertyAnnotationsDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MABSTRACT_ANNOTION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MAbstractPropertyAnnotations result = (MAbstractPropertyAnnotations) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			return ((InternalEList<?>) getGeneralContent())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_REFERENCE:
			return getGeneralReference();
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			return getGeneralContent();
		case AnnotationsPackage.MABSTRACT_ANNOTION__ANNOTATED_ELEMENT:
			if (resolve)
				return getAnnotatedElement();
			return basicGetAnnotatedElement();
		case AnnotationsPackage.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS:
			if (resolve)
				return getContainingAbstractPropertyAnnotations();
			return basicGetContainingAbstractPropertyAnnotations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_REFERENCE:
			getGeneralReference().clear();
			getGeneralReference()
					.addAll((Collection<? extends MObject>) newValue);
			return;
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			getGeneralContent().clear();
			getGeneralContent()
					.addAll((Collection<? extends MObject>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_REFERENCE:
			unsetGeneralReference();
			return;
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			unsetGeneralContent();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_REFERENCE:
			return isSetGeneralReference();
		case AnnotationsPackage.MABSTRACT_ANNOTION__GENERAL_CONTENT:
			return isSetGeneralContent();
		case AnnotationsPackage.MABSTRACT_ANNOTION__ANNOTATED_ELEMENT:
			return basicGetAnnotatedElement() != null;
		case AnnotationsPackage.MABSTRACT_ANNOTION__CONTAINING_ABSTRACT_PROPERTY_ANNOTATIONS:
			return basicGetContainingAbstractPropertyAnnotations() != null;
		}
		return super.eIsSet(featureID);
	}

} //MAbstractAnnotionImpl
