/**
 */
package com.montages.mcore.annotations.impl;

import com.montages.mcore.MModelElement;
import com.montages.mcore.McorePackage;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.GeneralAnnotationAction;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.annotations.MGeneralDetail;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MGeneral Annotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MGeneralAnnotationImpl#getSource <em>Source</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MGeneralAnnotationImpl#getMGeneralDetail <em>MGeneral Detail</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MGeneralAnnotationImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MGeneralAnnotationImpl extends MAbstractAnnotionImpl
		implements MGeneralAnnotation {
	/**
	 * The default value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected static final String SOURCE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSource() <em>Source</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSource()
	 * @generated
	 * @ordered
	 */
	protected String source = SOURCE_EDEFAULT;

	/**
	 * This is true if the Source attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean sourceESet;

	/**
	 * The cached value of the '{@link #getMGeneralDetail() <em>MGeneral Detail</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMGeneralDetail()
	 * @generated
	 * @ordered
	 */
	protected EList<MGeneralDetail> mGeneralDetail;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final GeneralAnnotationAction DO_ACTION_EDEFAULT = GeneralAnnotationAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateannotationsGeneralAnnotationActionBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getDoAction <em>Do Action</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression doActionChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MGeneralAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MGENERAL_ANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSource() {
		return source;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSource(String newSource) {
		String oldSource = source;
		source = newSource;
		boolean oldSourceESet = sourceESet;
		sourceESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE, oldSource,
					source, !oldSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetSource() {
		String oldSource = source;
		boolean oldSourceESet = sourceESet;
		source = SOURCE_EDEFAULT;
		sourceESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE, oldSource,
					SOURCE_EDEFAULT, oldSourceESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetSource() {
		return sourceESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MGeneralDetail> getMGeneralDetail() {
		if (mGeneralDetail == null) {
			mGeneralDetail = new EObjectContainmentEList.Unsettable.Resolving<MGeneralDetail>(
					MGeneralDetail.class, this,
					AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL);
		}
		return mGeneralDetail;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetMGeneralDetail() {
		if (mGeneralDetail != null)
			((InternalEList.Unsettable<?>) mGeneralDetail).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetMGeneralDetail() {
		return mGeneralDetail != null
				&& ((InternalEList.Unsettable<?>) mGeneralDetail).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GeneralAnnotationAction getDoAction() {
		/**
		 * @OCL let const1: mcore::annotations::GeneralAnnotationAction = mcore::annotations::GeneralAnnotationAction::Do in
		const1
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MGENERAL_ANNOTATION;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MGENERAL_ANNOTATION__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MGENERAL_ANNOTATION,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MGENERAL_ANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			GeneralAnnotationAction result = (GeneralAnnotationAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDoAction(GeneralAnnotationAction newDoAction) {
		if (newDoAction == null) {
			return;
		}
		//Todo: Call XUpdate, getEditingDomain  execute XUpdate
		//org.eclipse.emf.edit.domain.EditingDomain domain = org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain.getEditingDomainFor(this);
		//domain.getCommandStack().execute(command);

		throw new UnsupportedOperationException();
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Do Action</b></em>' attribute.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<GeneralAnnotationAction>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let mySet : Set(GeneralAnnotationAction) = Set{GeneralAnnotationAction::Do}
	in
	mySet->including(if self.mGeneralDetail.key->count('documentation') >=1 then GeneralAnnotationAction::IntoDescription else null endif)
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<GeneralAnnotationAction> evalDoActionChoiceConstruction(
			List<GeneralAnnotationAction> choice) {
		EClass eClass = AnnotationsPackage.Literals.MGENERAL_ANNOTATION;
		if (doActionChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = AnnotationsPackage.Literals.MGENERAL_ANNOTATION__DO_ACTION;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				doActionChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"DoActionChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(doActionChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "DoActionChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<GeneralAnnotationAction> result = new ArrayList<GeneralAnnotationAction>(
					(Collection<GeneralAnnotationAction>) query.evaluate(this));

			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doAction$Update(GeneralAnnotationAction obj) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.annotations.GeneralAnnotationAction triggerValue = obj;
		XUpdate currentTrigger = transition.addAttributeUpdate

		(this, AnnotationsPackage.eINSTANCE.getMGeneralAnnotation_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		MModelElement annotatedElement = this.getAnnotatedElement();

		if (!this.getMGeneralDetail().isEmpty()) {
			for (MGeneralDetail details : getMGeneralDetail())
				if (details.getKey().equals("Documentation")
						|| details.getKey().equals("documentation")) {
					currentTrigger.addAttributeUpdate(annotatedElement,
							McorePackage.eINSTANCE
									.getMRepositoryElement_Description(),
							XUpdateMode.REDEFINE, XAddUpdateMode.FIRST,
							details.getValue(), null, null);
					if (getMGeneralDetail().size() > 1)
						currentTrigger.addReferenceUpdate(annotatedElement,
								McorePackage.eINSTANCE
										.getMModelElement_GeneralAnnotation(),
								XUpdateMode.REMOVE, null, details, null, null);
					else
						currentTrigger.addReferenceUpdate(annotatedElement,
								McorePackage.eINSTANCE
										.getMModelElement_GeneralAnnotation(),
								XUpdateMode.REMOVE, null, this, null, null);

				}
		}

		return currentTrigger;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL:
			return ((InternalEList<?>) getMGeneralDetail())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE:
			return getSource();
		case AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL:
			return getMGeneralDetail();
		case AnnotationsPackage.MGENERAL_ANNOTATION__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE:
			setSource((String) newValue);
			return;
		case AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL:
			getMGeneralDetail().clear();
			getMGeneralDetail()
					.addAll((Collection<? extends MGeneralDetail>) newValue);
			return;
		case AnnotationsPackage.MGENERAL_ANNOTATION__DO_ACTION:
			setDoAction((GeneralAnnotationAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE:
			unsetSource();
			return;
		case AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL:
			unsetMGeneralDetail();
			return;
		case AnnotationsPackage.MGENERAL_ANNOTATION__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION__SOURCE:
			return isSetSource();
		case AnnotationsPackage.MGENERAL_ANNOTATION__MGENERAL_DETAIL:
			return isSetMGeneralDetail();
		case AnnotationsPackage.MGENERAL_ANNOTATION__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case AnnotationsPackage.MGENERAL_ANNOTATION___DO_ACTION$_UPDATE__GENERALANNOTATIONACTION:
			return doAction$Update((GeneralAnnotationAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (source: ");
		if (sourceESet)
			result.append(source);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel 'Source'
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MGENERAL_ANNOTATION);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MGeneralAnnotationImpl
