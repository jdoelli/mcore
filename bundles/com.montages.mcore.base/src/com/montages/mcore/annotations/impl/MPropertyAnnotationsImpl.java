/**
 */
package com.montages.mcore.annotations.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAdd;
import com.montages.mcore.annotations.MChoiceConstraint;
import com.montages.mcore.annotations.MChoiceConstruction;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MHighlightAnnotation;
import com.montages.mcore.annotations.MInitializationOrder;
import com.montages.mcore.annotations.MInitializationValue;
import com.montages.mcore.annotations.MLayoutAnnotation;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotationsAction;
import com.montages.mcore.annotations.MResult;
import com.montages.mcore.annotations.MStringAsOclAnnotation;
import com.montages.mcore.annotations.MUpdate;
import com.montages.mcore.expressions.ExpressionBase;
import java.lang.reflect.InvocationTargetException;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>MProperty Annotations</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getOverriding <em>Overriding</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getOverrides <em>Overrides</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getOverriddenIn <em>Overridden In</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getResult <em>Result</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getInitOrder <em>Init Order</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getInitValue <em>Init Value</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getChoiceConstruction <em>Choice Construction</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getChoiceConstraint <em>Choice Constraint</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getAdd <em>Add</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getUpdate <em>Update</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getStringAsOcl <em>String As Ocl</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getHighlight <em>Highlight</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getLayout <em>Layout</em>}</li>
 *   <li>{@link com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl#getDoAction <em>Do Action</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public class MPropertyAnnotationsImpl extends MAbstractPropertyAnnotationsImpl
		implements MPropertyAnnotations {
	/**
	 * The cached value of the '{@link #getProperty() <em>Property</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty()
	 * @generated
	 * @ordered
	 */
	protected MProperty property;

	/**
	 * This is true if the Property reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean propertyESet;

	/**
	 * The default value of the '{@link #getOverriding() <em>Overriding</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriding()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean OVERRIDING_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getOverrides() <em>Overrides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverrides()
	 * @generated
	 * @ordered
	 */
	protected MPropertyAnnotations overrides;

	/**
	 * This is true if the Overrides reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean overridesESet;

	/**
	 * The cached value of the '{@link #getOverriddenIn() <em>Overridden In</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriddenIn()
	 * @generated
	 * @ordered
	 */
	protected EList<MPropertyAnnotations> overriddenIn;

	/**
	 * The cached value of the '{@link #getResult() <em>Result</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResult()
	 * @generated
	 * @ordered
	 */
	protected MResult result;

	/**
	 * This is true if the Result containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean resultESet;

	/**
	 * The cached value of the '{@link #getInitOrder() <em>Init Order</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitOrder()
	 * @generated
	 * @ordered
	 */
	protected MInitializationOrder initOrder;

	/**
	 * This is true if the Init Order containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initOrderESet;

	/**
	 * The cached value of the '{@link #getInitValue() <em>Init Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInitValue()
	 * @generated
	 * @ordered
	 */
	protected MInitializationValue initValue;

	/**
	 * This is true if the Init Value containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean initValueESet;

	/**
	 * The cached value of the '{@link #getChoiceConstruction() <em>Choice Construction</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoiceConstruction()
	 * @generated
	 * @ordered
	 */
	protected MChoiceConstruction choiceConstruction;

	/**
	 * This is true if the Choice Construction containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean choiceConstructionESet;

	/**
	 * The cached value of the '{@link #getChoiceConstraint() <em>Choice Constraint</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getChoiceConstraint()
	 * @generated
	 * @ordered
	 */
	protected MChoiceConstraint choiceConstraint;

	/**
	 * This is true if the Choice Constraint containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean choiceConstraintESet;

	/**
	 * The cached value of the '{@link #getAdd() <em>Add</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdd()
	 * @generated
	 * @ordered
	 */
	protected MAdd add;

	/**
	 * This is true if the Add containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean addESet;

	/**
	 * The cached value of the '{@link #getUpdate() <em>Update</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUpdate()
	 * @generated
	 * @ordered
	 */
	protected EList<MUpdate> update;

	/**
	 * The cached value of the '{@link #getStringAsOcl() <em>String As Ocl</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStringAsOcl()
	 * @generated
	 * @ordered
	 */
	protected MStringAsOclAnnotation stringAsOcl;

	/**
	 * This is true if the String As Ocl containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean stringAsOclESet;

	/**
	 * The cached value of the '{@link #getHighlight() <em>Highlight</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHighlight()
	 * @generated
	 * @ordered
	 */
	protected MHighlightAnnotation highlight;

	/**
	 * This is true if the Highlight containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean highlightESet;

	/**
	 * The cached value of the '{@link #getLayout() <em>Layout</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLayout()
	 * @generated
	 * @ordered
	 */
	protected MLayoutAnnotation layout;

	/**
	 * This is true if the Layout containment reference has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean layoutESet;

	/**
	 * The default value of the '{@link #getDoAction() <em>Do Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction()
	 * @generated
	 * @ordered
	 */
	protected static final MPropertyAnnotationsAction DO_ACTION_EDEFAULT = MPropertyAnnotationsAction.DO;

	/**
	 * The parsed OCL expression for the body of the '{@link #doAction$Update <em>Do Action$ Update</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #doAction$Update
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression doAction$UpdateannotationsMPropertyAnnotationsActionBodyOCL;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getProperty <em>Property</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression propertyChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getProperty <em>Property</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getProperty
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression propertyChoiceConstructionOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getOverriding <em>Overriding</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOverriding
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression overridingDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getDoAction <em>Do Action</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoAction
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression doActionDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAnnotatedProperty <em>Annotated Property</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAnnotatedProperty
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression annotatedPropertyDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getKindLabel <em>Kind Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getKindLabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression kindLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(),
				ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(),
				EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MPropertyAnnotationsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty getProperty() {
		if (property != null && property.eIsProxy()) {
			InternalEObject oldProperty = (InternalEObject) property;
			property = (MProperty) eResolveProxy(oldProperty);
			if (property != oldProperty) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY,
							oldProperty, property));
			}
		}
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MProperty basicGetProperty() {
		return property;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProperty(MProperty newProperty) {
		MProperty oldProperty = property;
		property = newProperty;
		boolean oldPropertyESet = propertyESet;
		propertyESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY,
					oldProperty, property, !oldPropertyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetProperty() {
		MProperty oldProperty = property;
		boolean oldPropertyESet = propertyESet;
		property = null;
		propertyESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY,
					oldProperty, null, oldPropertyESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetProperty() {
		return propertyESet;
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Property</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type MProperty
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL trg=self.property or self.containingClassifier.allPropertyAnnotations().property
	->excludes(trg)  
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalPropertyChoiceConstraint(MProperty trg) {
		EClass eClass = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
		if (propertyChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__PROPERTY;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				propertyChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstraint,
						helper.getProblems(), eClass,
						"PropertyChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(propertyChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "PropertyChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Property</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<MProperty>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL 
	self.containingClassifier.allFeatures()
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<MProperty> evalPropertyChoiceConstruction(
			List<MProperty> choice) {
		EClass eClass = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
		if (propertyChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__PROPERTY;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				propertyChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"PropertyChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(propertyChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "PropertyChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<MProperty> result = new ArrayList<MProperty>(
					(Collection<MProperty>) query.evaluate(this));
			result.remove(null);
			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getOverriding() {
		/**
		 * @OCL if self.property.oclIsUndefined() then false else not(self.property.containingClassifier = self.containingClassifier) endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__OVERRIDING;

		if (overridingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				overridingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(overridingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature,
					query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations getOverrides() {
		if (overrides != null && overrides.eIsProxy()) {
			InternalEObject oldOverrides = (InternalEObject) overrides;
			overrides = (MPropertyAnnotations) eResolveProxy(oldOverrides);
			if (overrides != oldOverrides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES,
							oldOverrides, overrides));
			}
		}
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotations basicGetOverrides() {
		return overrides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOverrides(MPropertyAnnotations newOverrides) {
		MPropertyAnnotations oldOverrides = overrides;
		overrides = newOverrides;
		boolean oldOverridesESet = overridesESet;
		overridesESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES,
					oldOverrides, overrides, !oldOverridesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOverrides() {
		MPropertyAnnotations oldOverrides = overrides;
		boolean oldOverridesESet = overridesESet;
		overrides = null;
		overridesESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES,
					oldOverrides, null, oldOverridesESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOverrides() {
		return overridesESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MPropertyAnnotations> getOverriddenIn() {
		if (overriddenIn == null) {
			overriddenIn = new EObjectResolvingEList.Unsettable<MPropertyAnnotations>(
					MPropertyAnnotations.class, this,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN);
		}
		return overriddenIn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetOverriddenIn() {
		if (overriddenIn != null)
			((InternalEList.Unsettable<?>) overriddenIn).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetOverriddenIn() {
		return overriddenIn != null
				&& ((InternalEList.Unsettable<?>) overriddenIn).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResult getResult() {
		if (result != null && result.eIsProxy()) {
			InternalEObject oldResult = (InternalEObject) result;
			result = (MResult) eResolveProxy(oldResult);
			if (result != oldResult) {
				InternalEObject newResult = (InternalEObject) result;
				NotificationChain msgs = oldResult.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
						null, null);
				if (newResult.eInternalContainer() == null) {
					msgs = newResult.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
							oldResult, result));
			}
		}
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MResult basicGetResult() {
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetResult(MResult newResult,
			NotificationChain msgs) {
		MResult oldResult = result;
		result = newResult;
		boolean oldResultESet = resultESet;
		resultESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT, oldResult,
					newResult, !oldResultESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResult(MResult newResult) {
		if (newResult != result) {
			NotificationChain msgs = null;
			if (result != null)
				msgs = ((InternalEObject) result).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
						null, msgs);
			if (newResult != null)
				msgs = ((InternalEObject) newResult).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
						null, msgs);
			msgs = basicSetResult(newResult, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldResultESet = resultESet;
			resultESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
						newResult, newResult, !oldResultESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetResult(NotificationChain msgs) {
		MResult oldResult = result;
		result = null;
		boolean oldResultESet = resultESet;
		resultESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT, oldResult,
					null, oldResultESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetResult() {
		if (result != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) result).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT,
					null, msgs);
			msgs = basicUnsetResult(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldResultESet = resultESet;
			resultESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT, null,
						null, oldResultESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetResult() {
		return resultESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationOrder getInitOrder() {
		if (initOrder != null && initOrder.eIsProxy()) {
			InternalEObject oldInitOrder = (InternalEObject) initOrder;
			initOrder = (MInitializationOrder) eResolveProxy(oldInitOrder);
			if (initOrder != oldInitOrder) {
				InternalEObject newInitOrder = (InternalEObject) initOrder;
				NotificationChain msgs = oldInitOrder.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
						null, null);
				if (newInitOrder.eInternalContainer() == null) {
					msgs = newInitOrder.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
							oldInitOrder, initOrder));
			}
		}
		return initOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationOrder basicGetInitOrder() {
		return initOrder;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitOrder(
			MInitializationOrder newInitOrder, NotificationChain msgs) {
		MInitializationOrder oldInitOrder = initOrder;
		initOrder = newInitOrder;
		boolean oldInitOrderESet = initOrderESet;
		initOrderESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
					oldInitOrder, newInitOrder, !oldInitOrderESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitOrder(MInitializationOrder newInitOrder) {
		if (newInitOrder != initOrder) {
			NotificationChain msgs = null;
			if (initOrder != null)
				msgs = ((InternalEObject) initOrder).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
						null, msgs);
			if (newInitOrder != null)
				msgs = ((InternalEObject) newInitOrder).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
						null, msgs);
			msgs = basicSetInitOrder(newInitOrder, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInitOrderESet = initOrderESet;
			initOrderESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
						newInitOrder, newInitOrder, !oldInitOrderESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInitOrder(NotificationChain msgs) {
		MInitializationOrder oldInitOrder = initOrder;
		initOrder = null;
		boolean oldInitOrderESet = initOrderESet;
		initOrderESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
					oldInitOrder, null, oldInitOrderESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitOrder() {
		if (initOrder != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) initOrder).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
					null, msgs);
			msgs = basicUnsetInitOrder(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInitOrderESet = initOrderESet;
			initOrderESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER,
						null, null, oldInitOrderESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitOrder() {
		return initOrderESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationValue getInitValue() {
		if (initValue != null && initValue.eIsProxy()) {
			InternalEObject oldInitValue = (InternalEObject) initValue;
			initValue = (MInitializationValue) eResolveProxy(oldInitValue);
			if (initValue != oldInitValue) {
				InternalEObject newInitValue = (InternalEObject) initValue;
				NotificationChain msgs = oldInitValue.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
						null, null);
				if (newInitValue.eInternalContainer() == null) {
					msgs = newInitValue.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
							oldInitValue, initValue));
			}
		}
		return initValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MInitializationValue basicGetInitValue() {
		return initValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetInitValue(
			MInitializationValue newInitValue, NotificationChain msgs) {
		MInitializationValue oldInitValue = initValue;
		initValue = newInitValue;
		boolean oldInitValueESet = initValueESet;
		initValueESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
					oldInitValue, newInitValue, !oldInitValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInitValue(MInitializationValue newInitValue) {
		if (newInitValue != initValue) {
			NotificationChain msgs = null;
			if (initValue != null)
				msgs = ((InternalEObject) initValue).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
						null, msgs);
			if (newInitValue != null)
				msgs = ((InternalEObject) newInitValue).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
						null, msgs);
			msgs = basicSetInitValue(newInitValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInitValueESet = initValueESet;
			initValueESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
						newInitValue, newInitValue, !oldInitValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetInitValue(NotificationChain msgs) {
		MInitializationValue oldInitValue = initValue;
		initValue = null;
		boolean oldInitValueESet = initValueESet;
		initValueESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
					oldInitValue, null, oldInitValueESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetInitValue() {
		if (initValue != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) initValue).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
					null, msgs);
			msgs = basicUnsetInitValue(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldInitValueESet = initValueESet;
			initValueESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE,
						null, null, oldInitValueESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetInitValue() {
		return initValueESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstruction getChoiceConstruction() {
		if (choiceConstruction != null && choiceConstruction.eIsProxy()) {
			InternalEObject oldChoiceConstruction = (InternalEObject) choiceConstruction;
			choiceConstruction = (MChoiceConstruction) eResolveProxy(
					oldChoiceConstruction);
			if (choiceConstruction != oldChoiceConstruction) {
				InternalEObject newChoiceConstruction = (InternalEObject) choiceConstruction;
				NotificationChain msgs = oldChoiceConstruction.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
						null, null);
				if (newChoiceConstruction.eInternalContainer() == null) {
					msgs = newChoiceConstruction.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
							oldChoiceConstruction, choiceConstruction));
			}
		}
		return choiceConstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstruction basicGetChoiceConstruction() {
		return choiceConstruction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoiceConstruction(
			MChoiceConstruction newChoiceConstruction, NotificationChain msgs) {
		MChoiceConstruction oldChoiceConstruction = choiceConstruction;
		choiceConstruction = newChoiceConstruction;
		boolean oldChoiceConstructionESet = choiceConstructionESet;
		choiceConstructionESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
					oldChoiceConstruction, newChoiceConstruction,
					!oldChoiceConstructionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoiceConstruction(
			MChoiceConstruction newChoiceConstruction) {
		if (newChoiceConstruction != choiceConstruction) {
			NotificationChain msgs = null;
			if (choiceConstruction != null)
				msgs = ((InternalEObject) choiceConstruction).eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
						null, msgs);
			if (newChoiceConstruction != null)
				msgs = ((InternalEObject) newChoiceConstruction).eInverseAdd(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
						null, msgs);
			msgs = basicSetChoiceConstruction(newChoiceConstruction, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldChoiceConstructionESet = choiceConstructionESet;
			choiceConstructionESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
						newChoiceConstruction, newChoiceConstruction,
						!oldChoiceConstructionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetChoiceConstruction(
			NotificationChain msgs) {
		MChoiceConstruction oldChoiceConstruction = choiceConstruction;
		choiceConstruction = null;
		boolean oldChoiceConstructionESet = choiceConstructionESet;
		choiceConstructionESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
					oldChoiceConstruction, null, oldChoiceConstructionESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetChoiceConstruction() {
		if (choiceConstruction != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) choiceConstruction).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
					null, msgs);
			msgs = basicUnsetChoiceConstruction(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldChoiceConstructionESet = choiceConstructionESet;
			choiceConstructionESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
						null, null, oldChoiceConstructionESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetChoiceConstruction() {
		return choiceConstructionESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstraint getChoiceConstraint() {
		if (choiceConstraint != null && choiceConstraint.eIsProxy()) {
			InternalEObject oldChoiceConstraint = (InternalEObject) choiceConstraint;
			choiceConstraint = (MChoiceConstraint) eResolveProxy(
					oldChoiceConstraint);
			if (choiceConstraint != oldChoiceConstraint) {
				InternalEObject newChoiceConstraint = (InternalEObject) choiceConstraint;
				NotificationChain msgs = oldChoiceConstraint.eInverseRemove(
						this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
						null, null);
				if (newChoiceConstraint.eInternalContainer() == null) {
					msgs = newChoiceConstraint.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
							oldChoiceConstraint, choiceConstraint));
			}
		}
		return choiceConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MChoiceConstraint basicGetChoiceConstraint() {
		return choiceConstraint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetChoiceConstraint(
			MChoiceConstraint newChoiceConstraint, NotificationChain msgs) {
		MChoiceConstraint oldChoiceConstraint = choiceConstraint;
		choiceConstraint = newChoiceConstraint;
		boolean oldChoiceConstraintESet = choiceConstraintESet;
		choiceConstraintESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
					oldChoiceConstraint, newChoiceConstraint,
					!oldChoiceConstraintESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setChoiceConstraint(MChoiceConstraint newChoiceConstraint) {
		if (newChoiceConstraint != choiceConstraint) {
			NotificationChain msgs = null;
			if (choiceConstraint != null)
				msgs = ((InternalEObject) choiceConstraint).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
						null, msgs);
			if (newChoiceConstraint != null)
				msgs = ((InternalEObject) newChoiceConstraint).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
						null, msgs);
			msgs = basicSetChoiceConstraint(newChoiceConstraint, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldChoiceConstraintESet = choiceConstraintESet;
			choiceConstraintESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
						newChoiceConstraint, newChoiceConstraint,
						!oldChoiceConstraintESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetChoiceConstraint(
			NotificationChain msgs) {
		MChoiceConstraint oldChoiceConstraint = choiceConstraint;
		choiceConstraint = null;
		boolean oldChoiceConstraintESet = choiceConstraintESet;
		choiceConstraintESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
					oldChoiceConstraint, null, oldChoiceConstraintESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetChoiceConstraint() {
		if (choiceConstraint != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) choiceConstraint).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
					null, msgs);
			msgs = basicUnsetChoiceConstraint(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldChoiceConstraintESet = choiceConstraintESet;
			choiceConstraintESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
						null, null, oldChoiceConstraintESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetChoiceConstraint() {
		return choiceConstraintESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAdd getAdd() {
		if (add != null && add.eIsProxy()) {
			InternalEObject oldAdd = (InternalEObject) add;
			add = (MAdd) eResolveProxy(oldAdd);
			if (add != oldAdd) {
				InternalEObject newAdd = (InternalEObject) add;
				NotificationChain msgs = oldAdd.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
						null, null);
				if (newAdd.eInternalContainer() == null) {
					msgs = newAdd.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
							oldAdd, add));
			}
		}
		return add;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAdd basicGetAdd() {
		return add;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAdd(MAdd newAdd, NotificationChain msgs) {
		MAdd oldAdd = add;
		add = newAdd;
		boolean oldAddESet = addESet;
		addESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD, oldAdd,
					newAdd, !oldAddESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAdd(MAdd newAdd) {
		if (newAdd != add) {
			NotificationChain msgs = null;
			if (add != null)
				msgs = ((InternalEObject) add).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
						null, msgs);
			if (newAdd != null)
				msgs = ((InternalEObject) newAdd).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
						null, msgs);
			msgs = basicSetAdd(newAdd, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAddESet = addESet;
			addESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD, newAdd,
						newAdd, !oldAddESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetAdd(NotificationChain msgs) {
		MAdd oldAdd = add;
		add = null;
		boolean oldAddESet = addESet;
		addESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD, oldAdd, null,
					oldAddESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetAdd() {
		if (add != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) add).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD,
					null, msgs);
			msgs = basicUnsetAdd(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldAddESet = addESet;
			addESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD, null,
						null, oldAddESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetAdd() {
		return addESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<MUpdate> getUpdate() {
		if (update == null) {
			update = new EObjectContainmentEList.Unsettable.Resolving<MUpdate>(
					MUpdate.class, this,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE);
		}
		return update;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetUpdate() {
		if (update != null)
			((InternalEList.Unsettable<?>) update).unset();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetUpdate() {
		return update != null && ((InternalEList.Unsettable<?>) update).isSet();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MStringAsOclAnnotation getStringAsOcl() {
		if (stringAsOcl != null && stringAsOcl.eIsProxy()) {
			InternalEObject oldStringAsOcl = (InternalEObject) stringAsOcl;
			stringAsOcl = (MStringAsOclAnnotation) eResolveProxy(
					oldStringAsOcl);
			if (stringAsOcl != oldStringAsOcl) {
				InternalEObject newStringAsOcl = (InternalEObject) stringAsOcl;
				NotificationChain msgs = oldStringAsOcl.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
						null, null);
				if (newStringAsOcl.eInternalContainer() == null) {
					msgs = newStringAsOcl.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
							oldStringAsOcl, stringAsOcl));
			}
		}
		return stringAsOcl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MStringAsOclAnnotation basicGetStringAsOcl() {
		return stringAsOcl;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetStringAsOcl(
			MStringAsOclAnnotation newStringAsOcl, NotificationChain msgs) {
		MStringAsOclAnnotation oldStringAsOcl = stringAsOcl;
		stringAsOcl = newStringAsOcl;
		boolean oldStringAsOclESet = stringAsOclESet;
		stringAsOclESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
					oldStringAsOcl, newStringAsOcl, !oldStringAsOclESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStringAsOcl(MStringAsOclAnnotation newStringAsOcl) {
		if (newStringAsOcl != stringAsOcl) {
			NotificationChain msgs = null;
			if (stringAsOcl != null)
				msgs = ((InternalEObject) stringAsOcl).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
						null, msgs);
			if (newStringAsOcl != null)
				msgs = ((InternalEObject) newStringAsOcl).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
						null, msgs);
			msgs = basicSetStringAsOcl(newStringAsOcl, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldStringAsOclESet = stringAsOclESet;
			stringAsOclESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
						newStringAsOcl, newStringAsOcl, !oldStringAsOclESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetStringAsOcl(NotificationChain msgs) {
		MStringAsOclAnnotation oldStringAsOcl = stringAsOcl;
		stringAsOcl = null;
		boolean oldStringAsOclESet = stringAsOclESet;
		stringAsOclESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
					oldStringAsOcl, null, oldStringAsOclESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetStringAsOcl() {
		if (stringAsOcl != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) stringAsOcl).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
					null, msgs);
			msgs = basicUnsetStringAsOcl(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldStringAsOclESet = stringAsOclESet;
			stringAsOclESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
						null, null, oldStringAsOclESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetStringAsOcl() {
		return stringAsOclESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MHighlightAnnotation getHighlight() {
		if (highlight != null && highlight.eIsProxy()) {
			InternalEObject oldHighlight = (InternalEObject) highlight;
			highlight = (MHighlightAnnotation) eResolveProxy(oldHighlight);
			if (highlight != oldHighlight) {
				InternalEObject newHighlight = (InternalEObject) highlight;
				NotificationChain msgs = oldHighlight.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
						null, null);
				if (newHighlight.eInternalContainer() == null) {
					msgs = newHighlight.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
							oldHighlight, highlight));
			}
		}
		return highlight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MHighlightAnnotation basicGetHighlight() {
		return highlight;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHighlight(
			MHighlightAnnotation newHighlight, NotificationChain msgs) {
		MHighlightAnnotation oldHighlight = highlight;
		highlight = newHighlight;
		boolean oldHighlightESet = highlightESet;
		highlightESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
					oldHighlight, newHighlight, !oldHighlightESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHighlight(MHighlightAnnotation newHighlight) {
		if (newHighlight != highlight) {
			NotificationChain msgs = null;
			if (highlight != null)
				msgs = ((InternalEObject) highlight).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
						null, msgs);
			if (newHighlight != null)
				msgs = ((InternalEObject) newHighlight).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
						null, msgs);
			msgs = basicSetHighlight(newHighlight, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldHighlightESet = highlightESet;
			highlightESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
						newHighlight, newHighlight, !oldHighlightESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetHighlight(NotificationChain msgs) {
		MHighlightAnnotation oldHighlight = highlight;
		highlight = null;
		boolean oldHighlightESet = highlightESet;
		highlightESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
					oldHighlight, null, oldHighlightESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetHighlight() {
		if (highlight != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) highlight).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
					null, msgs);
			msgs = basicUnsetHighlight(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldHighlightESet = highlightESet;
			highlightESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
						null, null, oldHighlightESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetHighlight() {
		return highlightESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLayoutAnnotation getLayout() {
		if (layout != null && layout.eIsProxy()) {
			InternalEObject oldLayout = (InternalEObject) layout;
			layout = (MLayoutAnnotation) eResolveProxy(oldLayout);
			if (layout != oldLayout) {
				InternalEObject newLayout = (InternalEObject) layout;
				NotificationChain msgs = oldLayout.eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
						null, null);
				if (newLayout.eInternalContainer() == null) {
					msgs = newLayout.eInverseAdd(this,
							EOPPOSITE_FEATURE_BASE
									- AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
							null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
							oldLayout, layout));
			}
		}
		return layout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLayoutAnnotation basicGetLayout() {
		return layout;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLayout(MLayoutAnnotation newLayout,
			NotificationChain msgs) {
		MLayoutAnnotation oldLayout = layout;
		layout = newLayout;
		boolean oldLayoutESet = layoutESet;
		layoutESet = true;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT, oldLayout,
					newLayout, !oldLayoutESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayout(MLayoutAnnotation newLayout) {
		if (newLayout != layout) {
			NotificationChain msgs = null;
			if (layout != null)
				msgs = ((InternalEObject) layout).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
						null, msgs);
			if (newLayout != null)
				msgs = ((InternalEObject) newLayout).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE
								- AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
						null, msgs);
			msgs = basicSetLayout(newLayout, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLayoutESet = layoutESet;
			layoutESet = true;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.SET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
						newLayout, newLayout, !oldLayoutESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicUnsetLayout(NotificationChain msgs) {
		MLayoutAnnotation oldLayout = layout;
		layout = null;
		boolean oldLayoutESet = layoutESet;
		layoutESet = false;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.UNSET,
					AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT, oldLayout,
					null, oldLayoutESet);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetLayout() {
		if (layout != null) {
			NotificationChain msgs = null;
			msgs = ((InternalEObject) layout).eInverseRemove(this,
					EOPPOSITE_FEATURE_BASE
							- AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT,
					null, msgs);
			msgs = basicUnsetLayout(msgs);
			if (msgs != null)
				msgs.dispatch();
		} else {
			boolean oldLayoutESet = layoutESet;
			layoutESet = false;
			if (eNotificationRequired())
				eNotify(new ENotificationImpl(this, Notification.UNSET,
						AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT, null,
						null, oldLayoutESet));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetLayout() {
		return layoutESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotationsAction getDoAction() {
		/**
		 * @OCL mcore::annotations::MPropertyAnnotationsAction::Do
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
		EStructuralFeature eFeature = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__DO_ACTION;

		if (doActionDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature,
					eClass());

			try {
				doActionDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(),
						AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS,
						eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(doActionDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			MPropertyAnnotationsAction result = (MPropertyAnnotationsAction) xoclEval
					.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setDoAction(MPropertyAnnotationsAction newDoAction) {
		MPropertiesContainer pc = this.getContainingPropertiesContainer();
		//MClassifierAnnotations localClassifierAnnotations = pc
		//		.getAnnotations();
		//move direct property annotations to local, if they exist.
		MPropertyAnnotations directPropertyAnnotations = this;
		//		if (directPropertyAnnotations != null
		//				&& directPropertyAnnotations
		//						.getContainingPropertiesContainer() != pc) {/*Move if to local*/
		//			if (localClassifierAnnotations == null) {
		//				localClassifierAnnotations = AnnotationsFactory.eINSTANCE
		//						.createMClassifierAnnotations();
		//				pc.setAnnotations(localClassifierAnnotations);
		//			}
		//			localClassifierAnnotations.getPropertyAnnotations().add(
		//					directPropertyAnnotations);
		//		}
		//		if (localClassifierAnnotations == null) {
		//			localClassifierAnnotations = AnnotationsFactory.eINSTANCE
		//					.createMClassifierAnnotations();
		//			pc.setAnnotations(localClassifierAnnotations);
		//		}
		//		if (directPropertyAnnotations == null) {
		//			directPropertyAnnotations = AnnotationsFactory.eINSTANCE
		//					.createMPropertyAnnotations();
		//			localClassifierAnnotations.getPropertyAnnotations().add(
		//					directPropertyAnnotations);
		//			directPropertyAnnotations.setProperty(this);
		//		}
		ExpressionBase initValue;
		if (getProperty() != null) {
			if (getProperty().getSingular()) {
				initValue = ExpressionBase.NULL_VALUE;
			} else {
				initValue = ExpressionBase.EMPTY_COLLECTION;
			}
			switch (newDoAction.getValue()) {
			case MPropertyAction.CONSTRUCT_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstruction() == null) {
					directPropertyAnnotations
							.setChoiceConstruction(AnnotationsFactory.eINSTANCE
									.createMChoiceConstruction());
				}
				annotationInitialization(
						directPropertyAnnotations.getChoiceConstruction(),
						ExpressionBase.EMPTY_COLLECTION);
				break;
			case MPropertyAction.CONSTRAIN_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstraint() == null) {
					directPropertyAnnotations
							.setChoiceConstraint(AnnotationsFactory.eINSTANCE
									.createMChoiceConstraint());
				}
				annotationInitialization(
						directPropertyAnnotations.getChoiceConstraint(),
						(ExpressionBase.TRUE_VALUE));
				break;
			case MPropertyAction.INIT_VALUE_VALUE:
				if (directPropertyAnnotations.getInitValue() == null) {
					directPropertyAnnotations
							.setInitValue(AnnotationsFactory.eINSTANCE
									.createMInitializationValue());
				}
				annotationInitialization(
						directPropertyAnnotations.getInitValue(), initValue);
				break;
			case MPropertyAction.INIT_ORDER_VALUE:
				if (directPropertyAnnotations.getInitOrder() == null) {
					directPropertyAnnotations
							.setInitOrder(AnnotationsFactory.eINSTANCE
									.createMInitializationOrder());
				}
				annotationInitialization(
						directPropertyAnnotations.getInitOrder(),
						(ExpressionBase.ONE_VALUE));
				break;
			case MPropertyAction.RESULT_VALUE:
				if (directPropertyAnnotations.getResult() == null) {
					directPropertyAnnotations.setResult(
							AnnotationsFactory.eINSTANCE.createMResult());
				}
				annotationInitialization(directPropertyAnnotations.getResult(),
						initValue);
				break;
			case MPropertyAction.HIGHLIGHT_VALUE:
				if (directPropertyAnnotations.getHighlight() == null) {
					directPropertyAnnotations
							.setHighlight(AnnotationsFactory.eINSTANCE
									.createMHighlightAnnotation());
				}
				annotationInitialization(
						directPropertyAnnotations.getHighlight(),
						(ExpressionBase.TRUE_VALUE));
				break;
			case MPropertyAction.HIDE_IN_PROPERTIES_VIEW_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
				}
				directPropertyAnnotations.getLayout()
						.setHideInPropertyView(true);
				break;
			case MPropertyAction.HIDE_IN_TABLE_EDITOR_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
				}
				directPropertyAnnotations.getLayout().setHideInTable(true);
				break;
			case MPropertyAction.SHOW_IN_TABLE_EDITOR_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
				}
				directPropertyAnnotations.getLayout().setHideInTable(false);
				break;
			case MPropertyAction.SHOW_IN_PROPERTIES_VIEW_VALUE:
				if (directPropertyAnnotations.getLayout() == null) {
					directPropertyAnnotations
							.setLayout(AnnotationsFactory.eINSTANCE
									.createMLayoutAnnotation());
				}
				directPropertyAnnotations.getLayout()
						.setHideInPropertyView(false);
				break;
			default:
				break;
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public XUpdate doAction$Update(MPropertyAnnotationsAction obj) {

		// Auto Generated XSemantics;

		org.xocl.semantics.XTransition transition = org.xocl.semantics.SemanticsFactory.eINSTANCE
				.createXTransition();
		com.montages.mcore.annotations.MPropertyAnnotationsAction triggerValue = obj;
		XUpdate currentTrigger = transition.addAttributeUpdate

		(this, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_DoAction(),
				org.xocl.semantics.XUpdateMode.REDEFINE, null, triggerValue,
				null, null);

		MPropertiesContainer pc = this.getContainingPropertiesContainer();

		MPropertyAnnotations directPropertyAnnotations = this;

		ExpressionBase initValue;
		if (getProperty() != null) {
			if (getProperty().getSingular()) {
				initValue = ExpressionBase.NULL_VALUE;
			} else {
				initValue = ExpressionBase.EMPTY_COLLECTION;
			}
			switch (obj.getValue()) {
			case MPropertyAnnotationsAction.CONSTRUCT_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstruction() == null) {
					MChoiceConstruction construct = AnnotationsFactory.eINSTANCE
							.createMChoiceConstruction();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
							XUpdateMode.REDEFINE, null, construct, null, null);
					annotationInitialization(construct,
							ExpressionBase.EMPTY_COLLECTION);
				}
				break;
			case MPropertyAnnotationsAction.CONSTRAIN_CHOICE_VALUE:
				if (directPropertyAnnotations.getChoiceConstraint() == null) {
					MChoiceConstraint constrain = AnnotationsFactory.eINSTANCE
							.createMChoiceConstraint();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
							XUpdateMode.REDEFINE, null, constrain, null, null);
					annotationInitialization(constrain,
							ExpressionBase.TRUE_VALUE);
				}
				break;
			case MPropertyAnnotationsAction.INIT_VALUE_VALUE:
				if (directPropertyAnnotations.getInitValue() == null) {
					MInitializationValue initVal = AnnotationsFactory.eINSTANCE
							.createMInitializationValue();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_VALUE,
							XUpdateMode.REDEFINE, null, initVal, null, null);
					annotationInitialization(initVal, initValue);
				}
				break;
			case MPropertyAnnotationsAction.INIT_ORDER_VALUE:
				if (directPropertyAnnotations.getInitOrder() == null) {
					MInitializationOrder iniOrder = AnnotationsFactory.eINSTANCE
							.createMInitializationOrder();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_ORDER,
							XUpdateMode.REDEFINE, null, iniOrder, null, null);
					annotationInitialization(iniOrder,
							ExpressionBase.ONE_VALUE);
				}
				break;
			case MPropertyAnnotationsAction.RESULT_VALUE:
				if (directPropertyAnnotations.getResult() == null) {
					MResult result = AnnotationsFactory.eINSTANCE
							.createMResult();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__RESULT,
							XUpdateMode.REDEFINE, null, result, null, null);
					annotationInitialization(result, initValue);
				}
				break;
			case MPropertyAnnotationsAction.HIGHLIGHT_VALUE:
				if (directPropertyAnnotations.getHighlight() == null) {
					MHighlightAnnotation highlight = AnnotationsFactory.eINSTANCE
							.createMHighlightAnnotation();
					currentTrigger.addReferenceUpdate(this,
							AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
							XUpdateMode.REDEFINE, null, highlight, null, null);
					annotationInitialization(highlight,
							ExpressionBase.TRUE_VALUE);
				}
			default:
				break;
			}
		}

		return currentTrigger;
	}

	/**
	 * ATTENTION: Duplicated in MPropertyImpl and MPropertyAnnotationsImpl
	 * @param cc
	 * @param b
	 */
	private void annotationInitialization(MExprAnnotation cc,
			ExpressionBase b) {
		if (cc.getValue() == null || cc.getValue().trim().equals("")) {
			cc.setUseExplicitOcl(false);
		}
		if (!cc.getUseExplicitOcl()) {
			if (cc.getNamedExpression().isEmpty()
					&& cc.getNamedConstant().isEmpty()
					&& cc.getNamedTuple().isEmpty()) {
				if (cc.getBase() == null
						|| cc.getBase() == ExpressionBase.UNDEFINED) {
					cc.setBase(b);
				} else {
					if (cc.getBase() == ExpressionBase.SELF_OBJECT
							&& cc.getElement1() == null
							&& cc.getCastType() == null
							&& cc.getProcessor() == null) {
						cc.setBase(b);
					}
				}

			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
			return basicUnsetResult(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
			return basicUnsetInitOrder(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
			return basicUnsetInitValue(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
			return basicUnsetChoiceConstruction(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
			return basicUnsetChoiceConstraint(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
			return basicUnsetAdd(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
			return ((InternalEList<?>) getUpdate()).basicRemove(otherEnd, msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
			return basicUnsetStringAsOcl(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
			return basicUnsetHighlight(msgs);
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			return basicUnsetLayout(msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY:
			if (resolve)
				return getProperty();
			return basicGetProperty();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDING:
			return getOverriding();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES:
			if (resolve)
				return getOverrides();
			return basicGetOverrides();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN:
			return getOverriddenIn();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
			if (resolve)
				return getResult();
			return basicGetResult();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
			if (resolve)
				return getInitOrder();
			return basicGetInitOrder();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
			if (resolve)
				return getInitValue();
			return basicGetInitValue();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
			if (resolve)
				return getChoiceConstruction();
			return basicGetChoiceConstruction();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
			if (resolve)
				return getChoiceConstraint();
			return basicGetChoiceConstraint();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
			if (resolve)
				return getAdd();
			return basicGetAdd();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
			return getUpdate();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
			if (resolve)
				return getStringAsOcl();
			return basicGetStringAsOcl();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
			if (resolve)
				return getHighlight();
			return basicGetHighlight();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			if (resolve)
				return getLayout();
			return basicGetLayout();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__DO_ACTION:
			return getDoAction();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY:
			setProperty((MProperty) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES:
			setOverrides((MPropertyAnnotations) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN:
			getOverriddenIn().clear();
			getOverriddenIn().addAll(
					(Collection<? extends MPropertyAnnotations>) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
			setResult((MResult) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
			setInitOrder((MInitializationOrder) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
			setInitValue((MInitializationValue) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
			setChoiceConstruction((MChoiceConstruction) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
			setChoiceConstraint((MChoiceConstraint) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
			setAdd((MAdd) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
			getUpdate().clear();
			getUpdate().addAll((Collection<? extends MUpdate>) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
			setStringAsOcl((MStringAsOclAnnotation) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
			setHighlight((MHighlightAnnotation) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			setLayout((MLayoutAnnotation) newValue);
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__DO_ACTION:
			setDoAction((MPropertyAnnotationsAction) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY:
			unsetProperty();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES:
			unsetOverrides();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN:
			unsetOverriddenIn();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
			unsetResult();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
			unsetInitOrder();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
			unsetInitValue();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
			unsetChoiceConstruction();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
			unsetChoiceConstraint();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
			unsetAdd();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
			unsetUpdate();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
			unsetStringAsOcl();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
			unsetHighlight();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			unsetLayout();
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__DO_ACTION:
			setDoAction(DO_ACTION_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__PROPERTY:
			return isSetProperty();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDING:
			return OVERRIDING_EDEFAULT == null ? getOverriding() != null
					: !OVERRIDING_EDEFAULT.equals(getOverriding());
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDES:
			return isSetOverrides();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN:
			return isSetOverriddenIn();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
			return isSetResult();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
			return isSetInitOrder();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
			return isSetInitValue();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
			return isSetChoiceConstruction();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
			return isSetChoiceConstraint();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
			return isSetAdd();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
			return isSetUpdate();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
			return isSetStringAsOcl();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
			return isSetHighlight();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			return isSetLayout();
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__DO_ACTION:
			return getDoAction() != DO_ACTION_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments)
			throws InvocationTargetException {
		switch (operationID) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS___DO_ACTION$_UPDATE__MPROPERTYANNOTATIONSACTION:
			return doAction$Update(
					(MPropertyAnnotationsAction) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL (if self.property.oclIsUndefined() then 'PROPERTY MISSING' else self.property.eLabel endif)
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, label,
						helper.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL annotatedProperty self.property
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public MProperty basicGetAnnotatedProperty() {
		EClass eClass = (AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS);
		EStructuralFeature eOverrideFeature = AnnotationsPackage.Literals.MABSTRACT_PROPERTY_ANNOTATIONS__ANNOTATED_PROPERTY;

		if (annotatedPropertyDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				annotatedPropertyDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(annotatedPropertyDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (MProperty) xoclEval.evaluateElement(eOverrideFeature,
					query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL kindLabel if self.annotatedProperty.oclIsUndefined() 
	 then 'PROPERTY of ?' 
	 else  annotatedProperty.kind.toString().toUpperCase().concat(' of ')
	          .concat(annotatedProperty.containingClassifier.eName)
	--self.annotatedProperty.containingClassifier.eName.concat('-').concat(
	--self.annotatedProperty.kindLabel.toUpperCase())
	endif 
	
	--if self.property.oclIsUndefined() then 'Select Property' else self.property.eName endif .concat('-Semantics')
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getKindLabel() {
		EClass eClass = (AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS);
		EStructuralFeature eOverrideFeature = McorePackage.Literals.MREPOSITORY_ELEMENT__KIND_LABEL;

		if (kindLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil
					.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				kindLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						AnnotationsPackage.PLUGIN_ID, derive,
						helper.getProblems(), eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(kindLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //MPropertyAnnotationsImpl
