
package com.montages.mcore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MUpdate Persistence</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.annotations.AnnotationsPackage#getMUpdatePersistence()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='MUpdatePersistence'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL targetObjectTypeOfAnnotationDerive='let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedType\nendif endif\n' targetSimpleTypeOfAnnotationDerive='let annotations: MPropertyAnnotations = let chain: MAbstractPropertyAnnotations = containingAbstractPropertyAnnotations in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(MPropertyAnnotations)\n    then chain.oclAsType(MPropertyAnnotations)\n    else null\n  endif\n  endif in\nif annotations = null\n  then null\n  else if annotations.annotatedProperty.oclIsUndefined()\n  then null\n  else annotations.annotatedProperty.calculatedSimpleType\nendif endif\n' containingAbstractPropertyAnnotationsDerive='self.eContainer().eContainer().oclAsType(MAbstractPropertyAnnotations)' expectedReturnSimpleTypeOfAnnotationDerive='mcore::SimpleType::String\n' objectObjectTypeOfAnnotationDerive='if eContainer().oclIsKindOf(MUpdate) then\r\nlet update :MUpdate  = eContainer().oclAsType(MUpdate) in let class:MClassifier = update.feature.containingClassifier in if class.oclIsUndefined() then null else class endif\r\nelse \r\nnull\r\nendif'"
 * @generated
 */

public interface MUpdatePersistence extends MTrgAnnotation {
} // MUpdatePersistence
