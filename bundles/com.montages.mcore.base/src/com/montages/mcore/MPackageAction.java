/**
 */
package com.montages.mcore;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>MPackage Action</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.McorePackage#getMPackageAction()
 * @model
 * @generated
 */
public enum MPackageAction implements Enumerator {
	/**
	 * The '<em><b>Do</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DO_VALUE
	 * @generated
	 * @ordered
	 */
	DO(0, "Do", "do..."),

	/**
	 * The '<em><b>Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	CLASS(1, "Class", "+ Class"),
	/**
	 * The '<em><b>Abstract Class</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #ABSTRACT_CLASS_VALUE
	 * @generated
	 * @ordered
	 */
	ABSTRACT_CLASS(2, "AbstractClass", "+ Abstract Class"),
	/**
	 * The '<em><b>Enumeration</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #ENUMERATION_VALUE
	 * @generated
	 * @ordered
	 */
	ENUMERATION(3, "Enumeration", "+ Enumeration"),
	/**
	 * The '<em><b>Data Type</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #DATA_TYPE_VALUE
	 * @generated
	 * @ordered
	 */
	DATA_TYPE(4, "DataType", "+ Datatype"),
	/**
	 * The '<em><b>Sub Package</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SUB_PACKAGE_VALUE
	 * @generated
	 * @ordered
	 */
	SUB_PACKAGE(5, "SubPackage", "+ Sub Package"),
	/**
	 * The '<em><b>Generate Property Groups</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #GENERATE_PROPERTY_GROUPS_VALUE
	 * @generated
	 * @ordered
	 */
	GENERATE_PROPERTY_GROUPS(6, "GeneratePropertyGroups", "Generate Property Groups"),
	/**
	 * The '<em><b>Remove Property Groups</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #REMOVE_PROPERTY_GROUPS_VALUE
	 * @generated
	 * @ordered
	 */
	REMOVE_PROPERTY_GROUPS(7, "RemovePropertyGroups", "Remove Property Groups"),
	/**
	 * The '<em><b>Complete Semantics</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #COMPLETE_SEMANTICS_VALUE
	 * @generated
	 * @ordered
	 */
	COMPLETE_SEMANTICS(8, "CompleteSemantics", "Complete Semantics"),
	/**
	 * The '<em><b>Open Editor</b></em>' literal object.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @see #OPEN_EDITOR_VALUE
	 * @generated
	 * @ordered
	 */
	OPEN_EDITOR(9, "OpenEditor", "Open Dynamic Editor");

	/**
	 * The '<em><b>Do</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Do</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DO
	 * @model name="Do" literal="do..."
	 * @generated
	 * @ordered
	 */
	public static final int DO_VALUE = 0;

	/**
	 * The '<em><b>Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #CLASS
	 * @model name="Class" literal="+ Class"
	 * @generated
	 * @ordered
	 */
	public static final int CLASS_VALUE = 1;

	/**
	 * The '<em><b>Abstract Class</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Abstract Class</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ABSTRACT_CLASS
	 * @model name="AbstractClass" literal="+ Abstract Class"
	 * @generated
	 * @ordered
	 */
	public static final int ABSTRACT_CLASS_VALUE = 2;

	/**
	 * The '<em><b>Enumeration</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Enumeration</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #ENUMERATION
	 * @model name="Enumeration" literal="+ Enumeration"
	 * @generated
	 * @ordered
	 */
	public static final int ENUMERATION_VALUE = 3;

	/**
	 * The '<em><b>Data Type</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Data Type</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #DATA_TYPE
	 * @model name="DataType" literal="+ Datatype"
	 * @generated
	 * @ordered
	 */
	public static final int DATA_TYPE_VALUE = 4;

	/**
	 * The '<em><b>Sub Package</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Sub Package</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SUB_PACKAGE
	 * @model name="SubPackage" literal="+ Sub Package"
	 * @generated
	 * @ordered
	 */
	public static final int SUB_PACKAGE_VALUE = 5;

	/**
	 * The '<em><b>Generate Property Groups</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Generate Property Groups</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GENERATE_PROPERTY_GROUPS
	 * @model name="GeneratePropertyGroups" literal="Generate Property Groups"
	 * @generated
	 * @ordered
	 */
	public static final int GENERATE_PROPERTY_GROUPS_VALUE = 6;

	/**
	 * The '<em><b>Remove Property Groups</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Remove Property Groups</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #REMOVE_PROPERTY_GROUPS
	 * @model name="RemovePropertyGroups" literal="Remove Property Groups"
	 * @generated
	 * @ordered
	 */
	public static final int REMOVE_PROPERTY_GROUPS_VALUE = 7;

	/**
	 * The '<em><b>Complete Semantics</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Complete Semantics</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #COMPLETE_SEMANTICS
	 * @model name="CompleteSemantics" literal="Complete Semantics"
	 * @generated
	 * @ordered
	 */
	public static final int COMPLETE_SEMANTICS_VALUE = 8;

	/**
	 * The '<em><b>Open Editor</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Open Editor</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #OPEN_EDITOR
	 * @model name="OpenEditor" literal="Open Dynamic Editor"
	 * @generated
	 * @ordered
	 */
	public static final int OPEN_EDITOR_VALUE = 9;

	/**
	 * An array of all the '<em><b>MPackage Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final MPackageAction[] VALUES_ARRAY = new MPackageAction[] {
			DO, CLASS, ABSTRACT_CLASS, ENUMERATION, DATA_TYPE, SUB_PACKAGE,
			GENERATE_PROPERTY_GROUPS, REMOVE_PROPERTY_GROUPS,
			COMPLETE_SEMANTICS, OPEN_EDITOR, };

	/**
	 * A public read-only list of all the '<em><b>MPackage Action</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<MPackageAction> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>MPackage Action</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPackageAction get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPackageAction result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MPackage Action</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPackageAction getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			MPackageAction result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>MPackage Action</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MPackageAction get(int value) {
		switch (value) {
		case DO_VALUE:
			return DO;
		case CLASS_VALUE:
			return CLASS;
		case ABSTRACT_CLASS_VALUE:
			return ABSTRACT_CLASS;
		case ENUMERATION_VALUE:
			return ENUMERATION;
		case DATA_TYPE_VALUE:
			return DATA_TYPE;
		case SUB_PACKAGE_VALUE:
			return SUB_PACKAGE;
		case GENERATE_PROPERTY_GROUPS_VALUE:
			return GENERATE_PROPERTY_GROUPS;
		case REMOVE_PROPERTY_GROUPS_VALUE:
			return REMOVE_PROPERTY_GROUPS;
		case COMPLETE_SEMANTICS_VALUE:
			return COMPLETE_SEMANTICS;
		case OPEN_EDITOR_VALUE:
			return OPEN_EDITOR;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private MPackageAction(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //MPackageAction
