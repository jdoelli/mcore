/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MRepository Element</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MRepositoryElement#getKindLabel <em>Kind Label</em>}</li>
 *   <li>{@link com.montages.mcore.MRepositoryElement#getRenderedKindLabel <em>Rendered Kind Label</em>}</li>
 *   <li>{@link com.montages.mcore.MRepositoryElement#getIndentLevel <em>Indent Level</em>}</li>
 *   <li>{@link com.montages.mcore.MRepositoryElement#getDescription <em>Description</em>}</li>
 *   <li>{@link com.montages.mcore.MRepositoryElement#getAsText <em>As Text</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMRepositoryElement()
 * @model abstract="true"
 * @generated
 */

public interface MRepositoryElement extends EObject {
	/**
	 * Returns the value of the '<em><b>Kind Label</b></em>' attribute.
	 * The default value is <code>""</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Kind Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Kind Label</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMRepositoryElement_KindLabel()
	 * @model default="" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='\'TODO\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getKindLabel();

	/**
	 * Returns the value of the '<em><b>Rendered Kind Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rendered Kind Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rendered Kind Label</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMRepositoryElement_RenderedKindLabel()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='/*OVERRITEN in Code for performance\052/\nself.indentationSpaces().concat(self.kindLabel)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getRenderedKindLabel();

	/**
	 * Returns the value of the '<em><b>Indent Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Indent Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Indent Level</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMRepositoryElement_IndentLevel()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if eContainer().oclIsUndefined() \r\n  then 0\r\nelse if eContainer().oclIsKindOf(MRepositoryElement)\r\n  then eContainer().oclAsType(MRepositoryElement).indentLevel + 1\r\n  else 0 endif endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Kind'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Integer getIndentLevel();

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #isSetDescription()
	 * @see #unsetDescription()
	 * @see #setDescription(String)
	 * @see com.montages.mcore.McorePackage#getMRepositoryElement_Description()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG mixedEditor='true' propertyCategory='Documentation' createColumn='false'"
	 * @generated
	 */
	String getDescription();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MRepositoryElement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #isSetDescription()
	 * @see #unsetDescription()
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepositoryElement#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetDescription()
	 * @see #getDescription()
	 * @see #setDescription(String)
	 * @generated
	 */
	void unsetDescription();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepositoryElement#getDescription <em>Description</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Description</em>' attribute is set.
	 * @see #unsetDescription()
	 * @see #getDescription()
	 * @see #setDescription(String)
	 * @generated
	 */
	boolean isSetDescription();

	/**
	 * Returns the value of the '<em><b>As Text</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As Text</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As Text</em>' attribute.
	 * @see #isSetAsText()
	 * @see #unsetAsText()
	 * @see #setAsText(String)
	 * @see com.montages.mcore.McorePackage#getMRepositoryElement_AsText()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Summary As Text' createColumn='false'"
	 * @generated
	 */
	String getAsText();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MRepositoryElement#getAsText <em>As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>As Text</em>' attribute.
	 * @see #isSetAsText()
	 * @see #unsetAsText()
	 * @see #getAsText()
	 * @generated
	 */
	void setAsText(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MRepositoryElement#getAsText <em>As Text</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetAsText()
	 * @see #getAsText()
	 * @see #setAsText(String)
	 * @generated
	 */
	void unsetAsText();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MRepositoryElement#getAsText <em>As Text</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>As Text</em>' attribute is set.
	 * @see #unsetAsText()
	 * @see #getAsText()
	 * @see #setAsText(String)
	 * @generated
	 */
	boolean isSetAsText();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL body='indentationSpaces(indentLevel * 4)'"
	 * @generated
	 */
	String indentationSpaces();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" sizeRequired="true"
	 *        annotation="http://www.xocl.org/OCL body='if size < 1 then \'\'\r\nelse self.indentationSpaces(size-1).concat(\' \') endif'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Kind' createColumn='true'"
	 * @generated
	 */
	String indentationSpaces(Integer size);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.xocl.org/OCL body='\'\\n\''"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='Kind' createColumn='true'"
	 * @generated
	 */
	String newLineString();

} // MRepositoryElement
