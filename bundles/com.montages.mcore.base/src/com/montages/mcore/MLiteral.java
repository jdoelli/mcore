/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EEnumLiteral;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MLiteral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.MLiteral#getConstantLabel <em>Constant Label</em>}</li>
 *   <li>{@link com.montages.mcore.MLiteral#getAsString <em>As String</em>}</li>
 *   <li>{@link com.montages.mcore.MLiteral#getQualifiedName <em>Qualified Name</em>}</li>
 *   <li>{@link com.montages.mcore.MLiteral#getContainingEnumeration <em>Containing Enumeration</em>}</li>
 *   <li>{@link com.montages.mcore.MLiteral#getInternalEEnumLiteral <em>Internal EEnum Literal</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.McorePackage#getMLiteral()
 * @model annotation="http://www.xocl.org/OCL label='self.asString'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL eNameDerive='if stringEmpty(self.specialEName) = true or stringEmpty(self.specialEName.trim()) = true\r\nthen calculatedShortName.camelCaseUpper()\r\nelse specialEName.camelCaseUpper()\r\nendif' kindLabelDerive='\'Literal\'\n'"
 * @generated
 */

public interface MLiteral extends MNamed, MModelElement {
	/**
	 * Returns the value of the '<em><b>Constant Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constant Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constant Label</em>' attribute.
	 * @see #isSetConstantLabel()
	 * @see #unsetConstantLabel()
	 * @see #setConstantLabel(String)
	 * @see com.montages.mcore.McorePackage#getMLiteral_ConstantLabel()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Additional'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getConstantLabel();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MLiteral#getConstantLabel <em>Constant Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constant Label</em>' attribute.
	 * @see #isSetConstantLabel()
	 * @see #unsetConstantLabel()
	 * @see #getConstantLabel()
	 * @generated
	 */
	void setConstantLabel(String value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MLiteral#getConstantLabel <em>Constant Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetConstantLabel()
	 * @see #getConstantLabel()
	 * @see #setConstantLabel(String)
	 * @generated
	 */
	void unsetConstantLabel();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MLiteral#getConstantLabel <em>Constant Label</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Constant Label</em>' attribute is set.
	 * @see #unsetConstantLabel()
	 * @see #getConstantLabel()
	 * @see #setConstantLabel(String)
	 * @generated
	 */
	boolean isSetConstantLabel();

	/**
	 * Returns the value of the '<em><b>As String</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>As String</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>As String</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMLiteral_AsString()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.containingEnumeration.eName.concat(\'::\').concat(self.eName)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAsString();

	/**
	 * Returns the value of the '<em><b>Qualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * <!-- begin-model-doc -->
	 * Returns a string wich identifies the literal and that can be used in OCL code to indicate the literal. An optional package, where the literal will be used, can be provided. Package will be prepended to literal name, if necessary.
	 * <!-- end-model-doc -->
	 * @return the value of the '<em>Qualified Name</em>' attribute.
	 * @see com.montages.mcore.McorePackage#getMLiteral_QualifiedName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.containingEnumeration.containingPackage.qualifiedName.concat(\'::\').concat(self.containingEnumeration.eName).concat(\'::\').concat(self.eName)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Naming and Labels'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getQualifiedName();

	/**
	 * Returns the value of the '<em><b>Containing Enumeration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Enumeration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Enumeration</em>' reference.
	 * @see com.montages.mcore.McorePackage#getMLiteral_ContainingEnumeration()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(MClassifier)'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Structural'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	MClassifier getContainingEnumeration();

	/**
	 * Returns the value of the '<em><b>Internal EEnum Literal</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Internal EEnum Literal</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Internal EEnum Literal</em>' reference.
	 * @see #isSetInternalEEnumLiteral()
	 * @see #unsetInternalEEnumLiteral()
	 * @see #setInternalEEnumLiteral(EEnumLiteral)
	 * @see com.montages.mcore.McorePackage#getMLiteral_InternalEEnumLiteral()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='Relation to ECore'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EEnumLiteral getInternalEEnumLiteral();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.MLiteral#getInternalEEnumLiteral <em>Internal EEnum Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Internal EEnum Literal</em>' reference.
	 * @see #isSetInternalEEnumLiteral()
	 * @see #unsetInternalEEnumLiteral()
	 * @see #getInternalEEnumLiteral()
	 * @generated
	 */
	void setInternalEEnumLiteral(EEnumLiteral value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.MLiteral#getInternalEEnumLiteral <em>Internal EEnum Literal</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetInternalEEnumLiteral()
	 * @see #getInternalEEnumLiteral()
	 * @see #setInternalEEnumLiteral(EEnumLiteral)
	 * @generated
	 */
	void unsetInternalEEnumLiteral();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.MLiteral#getInternalEEnumLiteral <em>Internal EEnum Literal</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Internal EEnum Literal</em>' reference is set.
	 * @see #unsetInternalEEnumLiteral()
	 * @see #getInternalEEnumLiteral()
	 * @see #setInternalEEnumLiteral(EEnumLiteral)
	 * @generated
	 */
	boolean isSetInternalEEnumLiteral();

} // MLiteral
