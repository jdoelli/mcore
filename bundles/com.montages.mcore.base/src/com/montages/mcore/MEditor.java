/**
 */
package com.montages.mcore;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MEditor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.McorePackage#getMEditor()
 * @model abstract="true"
 * @generated
 */

public interface MEditor extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='resetEditor'"
	 *        annotation="http://www.xocl.org/OCL body='null\n'"
	 * @generated
	 */
	Boolean resetEditor();

} // MEditor
