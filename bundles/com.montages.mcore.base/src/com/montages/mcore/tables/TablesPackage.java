/**
 */
package com.montages.mcore.tables;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.mcore.tables.TablesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.mcore'"
 * @generated
 */
public interface TablesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.mcore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "tables";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/mCore/MCore/Tables";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mcore.tables";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	TablesPackage eINSTANCE = com.montages.mcore.tables.impl.TablesPackageImpl
			.init();

	/**
	 * The meta object id for the '{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl <em>MAbstract Header Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.tables.impl.MAbstractHeaderRowImpl
	 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMAbstractHeaderRow()
	 * @generated
	 */
	int MABSTRACT_HEADER_ROW = 0;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__CLASSIFIER = 0;

	/**
	 * The feature id for the '<em><b>Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__COLUMN1 = 1;

	/**
	 * The feature id for the '<em><b>Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__COLUMN2 = 2;

	/**
	 * The feature id for the '<em><b>Object Row</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__OBJECT_ROW = 3;

	/**
	 * The feature id for the '<em><b>Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__COLUMN3 = 4;

	/**
	 * The feature id for the '<em><b>Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__COLUMN4 = 5;

	/**
	 * The feature id for the '<em><b>Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__COLUMN5 = 6;

	/**
	 * The feature id for the '<em><b>Displayed Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1 = 7;

	/**
	 * The feature id for the '<em><b>Displayed Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2 = 8;

	/**
	 * The feature id for the '<em><b>Displayed Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3 = 9;

	/**
	 * The feature id for the '<em><b>Displayed Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4 = 10;

	/**
	 * The feature id for the '<em><b>Displayed Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5 = 11;

	/**
	 * The number of structural features of the '<em>MAbstract Header Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW_FEATURE_COUNT = 12;

	/**
	 * The number of operations of the '<em>MAbstract Header Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MABSTRACT_HEADER_ROW_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.tables.impl.MTableImpl <em>MTable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.tables.impl.MTableImpl
	 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMTable()
	 * @generated
	 */
	int MTABLE = 1;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__CLASSIFIER = MABSTRACT_HEADER_ROW__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__COLUMN1 = MABSTRACT_HEADER_ROW__COLUMN1;

	/**
	 * The feature id for the '<em><b>Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__COLUMN2 = MABSTRACT_HEADER_ROW__COLUMN2;

	/**
	 * The feature id for the '<em><b>Object Row</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__OBJECT_ROW = MABSTRACT_HEADER_ROW__OBJECT_ROW;

	/**
	 * The feature id for the '<em><b>Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__COLUMN3 = MABSTRACT_HEADER_ROW__COLUMN3;

	/**
	 * The feature id for the '<em><b>Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__COLUMN4 = MABSTRACT_HEADER_ROW__COLUMN4;

	/**
	 * The feature id for the '<em><b>Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__COLUMN5 = MABSTRACT_HEADER_ROW__COLUMN5;

	/**
	 * The feature id for the '<em><b>Displayed Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__DISPLAYED_COLUMN1 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1;

	/**
	 * The feature id for the '<em><b>Displayed Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__DISPLAYED_COLUMN2 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2;

	/**
	 * The feature id for the '<em><b>Displayed Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__DISPLAYED_COLUMN3 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3;

	/**
	 * The feature id for the '<em><b>Displayed Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__DISPLAYED_COLUMN4 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4;

	/**
	 * The feature id for the '<em><b>Displayed Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__DISPLAYED_COLUMN5 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE__OBJECT = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>MTable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_FEATURE_COUNT = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>MTable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MTABLE_OPERATION_COUNT = MABSTRACT_HEADER_ROW_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.mcore.tables.impl.MObjectRowImpl <em>MObject Row</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.tables.impl.MObjectRowImpl
	 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMObjectRow()
	 * @generated
	 */
	int MOBJECT_ROW = 2;

	/**
	 * The feature id for the '<em><b>Containing Header Row</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__CONTAINING_HEADER_ROW = 0;

	/**
	 * The feature id for the '<em><b>Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__OBJECT = 1;

	/**
	 * The feature id for the '<em><b>Containment Property Header</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER = 2;

	/**
	 * The feature id for the '<em><b>Column1 Values</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN1_VALUES = 3;

	/**
	 * The feature id for the '<em><b>Column1 Referenced Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS = 4;

	/**
	 * The feature id for the '<em><b>Column2 Referenced Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS = 5;

	/**
	 * The feature id for the '<em><b>Column3 Referenced Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS = 6;

	/**
	 * The feature id for the '<em><b>Column4 Referenced Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS = 7;

	/**
	 * The feature id for the '<em><b>Column5 Referenced Objects</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS = 8;

	/**
	 * The feature id for the '<em><b>Column1 Data Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN1_DATA_VALUES = 9;

	/**
	 * The feature id for the '<em><b>Column1 Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN1_LITERAL_VALUE = 10;

	/**
	 * The feature id for the '<em><b>Column2 Data Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN2_DATA_VALUES = 11;

	/**
	 * The feature id for the '<em><b>Column2 Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN2_LITERAL_VALUE = 12;

	/**
	 * The feature id for the '<em><b>Column3 Data Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN3_DATA_VALUES = 13;

	/**
	 * The feature id for the '<em><b>Column3 Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN3_LITERAL_VALUE = 14;

	/**
	 * The feature id for the '<em><b>Column4 Data Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN4_DATA_VALUES = 15;

	/**
	 * The feature id for the '<em><b>Column4 Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN4_LITERAL_VALUE = 16;

	/**
	 * The feature id for the '<em><b>Column5 Data Values</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN5_DATA_VALUES = 17;

	/**
	 * The feature id for the '<em><b>Column5 Literal Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN5_LITERAL_VALUE = 18;

	/**
	 * The feature id for the '<em><b>Column2 Values</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN2_VALUES = 19;

	/**
	 * The feature id for the '<em><b>Column3 Values</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN3_VALUES = 20;

	/**
	 * The feature id for the '<em><b>Column4 Values</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN4_VALUES = 21;

	/**
	 * The feature id for the '<em><b>Column5 Values</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW__COLUMN5_VALUES = 22;

	/**
	 * The number of structural features of the '<em>MObject Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW_FEATURE_COUNT = 23;

	/**
	 * The operation id for the '<em>Containment Property Headers From Containment Properties</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW___CONTAINMENT_PROPERTY_HEADERS_FROM_CONTAINMENT_PROPERTIES__ELIST = 0;

	/**
	 * The number of operations of the '<em>MObject Row</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MOBJECT_ROW_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link com.montages.mcore.tables.impl.MHeaderImpl <em>MHeader</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.mcore.tables.impl.MHeaderImpl
	 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMHeader()
	 * @generated
	 */
	int MHEADER = 3;

	/**
	 * The feature id for the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__CLASSIFIER = MABSTRACT_HEADER_ROW__CLASSIFIER;

	/**
	 * The feature id for the '<em><b>Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__COLUMN1 = MABSTRACT_HEADER_ROW__COLUMN1;

	/**
	 * The feature id for the '<em><b>Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__COLUMN2 = MABSTRACT_HEADER_ROW__COLUMN2;

	/**
	 * The feature id for the '<em><b>Object Row</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__OBJECT_ROW = MABSTRACT_HEADER_ROW__OBJECT_ROW;

	/**
	 * The feature id for the '<em><b>Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__COLUMN3 = MABSTRACT_HEADER_ROW__COLUMN3;

	/**
	 * The feature id for the '<em><b>Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__COLUMN4 = MABSTRACT_HEADER_ROW__COLUMN4;

	/**
	 * The feature id for the '<em><b>Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__COLUMN5 = MABSTRACT_HEADER_ROW__COLUMN5;

	/**
	 * The feature id for the '<em><b>Displayed Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__DISPLAYED_COLUMN1 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1;

	/**
	 * The feature id for the '<em><b>Displayed Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__DISPLAYED_COLUMN2 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2;

	/**
	 * The feature id for the '<em><b>Displayed Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__DISPLAYED_COLUMN3 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3;

	/**
	 * The feature id for the '<em><b>Displayed Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__DISPLAYED_COLUMN4 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4;

	/**
	 * The feature id for the '<em><b>Displayed Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__DISPLAYED_COLUMN5 = MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5;

	/**
	 * The feature id for the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__REFERENCE = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Containing Object Row</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__CONTAINING_OBJECT_ROW = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Previous Header</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER__PREVIOUS_HEADER = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>MHeader</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER_FEATURE_COUNT = MABSTRACT_HEADER_ROW_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>MHeader</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MHEADER_OPERATION_COUNT = MABSTRACT_HEADER_ROW_OPERATION_COUNT + 0;

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.tables.MAbstractHeaderRow <em>MAbstract Header Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MAbstract Header Row</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow
	 * @generated
	 */
	EClass getMAbstractHeaderRow();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getClassifier <em>Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Classifier</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getClassifier()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Classifier();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn1 <em>Column1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column1</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getColumn1()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Column1();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn2 <em>Column2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column2</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getColumn2()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Column2();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.tables.MAbstractHeaderRow#getObjectRow <em>Object Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Row</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getObjectRow()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_ObjectRow();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn3 <em>Column3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column3</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getColumn3()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Column3();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn4 <em>Column4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column4</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getColumn4()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Column4();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn5 <em>Column5</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Column5</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getColumn5()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_Column5();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn1 <em>Displayed Column1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Column1</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn1()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_DisplayedColumn1();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn2 <em>Displayed Column2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Column2</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn2()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_DisplayedColumn2();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn3 <em>Displayed Column3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Column3</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn3()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_DisplayedColumn3();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn4 <em>Displayed Column4</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Column4</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn4()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_DisplayedColumn4();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn5 <em>Displayed Column5</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Displayed Column5</em>'.
	 * @see com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn5()
	 * @see #getMAbstractHeaderRow()
	 * @generated
	 */
	EReference getMAbstractHeaderRow_DisplayedColumn5();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.tables.MTable <em>MTable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MTable</em>'.
	 * @see com.montages.mcore.tables.MTable
	 * @generated
	 */
	EClass getMTable();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MTable#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see com.montages.mcore.tables.MTable#getObject()
	 * @see #getMTable()
	 * @generated
	 */
	EReference getMTable_Object();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.tables.MObjectRow <em>MObject Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MObject Row</em>'.
	 * @see com.montages.mcore.tables.MObjectRow
	 * @generated
	 */
	EClass getMObjectRow();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MObjectRow#getContainingHeaderRow <em>Containing Header Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Header Row</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getContainingHeaderRow()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_ContainingHeaderRow();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MObjectRow#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getObject()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Object();

	/**
	 * Returns the meta object for the containment reference list '{@link com.montages.mcore.tables.MObjectRow#getContainmentPropertyHeader <em>Containment Property Header</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Containment Property Header</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getContainmentPropertyHeader()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_ContainmentPropertyHeader();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn1Values <em>Column1 Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column1 Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn1Values()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column1Values();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn1ReferencedObjects <em>Column1 Referenced Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column1 Referenced Objects</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn1ReferencedObjects()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column1ReferencedObjects();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn2ReferencedObjects <em>Column2 Referenced Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column2 Referenced Objects</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn2ReferencedObjects()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column2ReferencedObjects();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn3ReferencedObjects <em>Column3 Referenced Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column3 Referenced Objects</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn3ReferencedObjects()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column3ReferencedObjects();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn4ReferencedObjects <em>Column4 Referenced Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column4 Referenced Objects</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn4ReferencedObjects()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column4ReferencedObjects();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn5ReferencedObjects <em>Column5 Referenced Objects</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column5 Referenced Objects</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn5ReferencedObjects()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column5ReferencedObjects();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.tables.MObjectRow#getColumn1DataValues <em>Column1 Data Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Column1 Data Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn1DataValues()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EAttribute getMObjectRow_Column1DataValues();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn1LiteralValue <em>Column1 Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column1 Literal Value</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn1LiteralValue()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column1LiteralValue();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.tables.MObjectRow#getColumn2DataValues <em>Column2 Data Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Column2 Data Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn2DataValues()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EAttribute getMObjectRow_Column2DataValues();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn2LiteralValue <em>Column2 Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column2 Literal Value</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn2LiteralValue()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column2LiteralValue();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.tables.MObjectRow#getColumn3DataValues <em>Column3 Data Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Column3 Data Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn3DataValues()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EAttribute getMObjectRow_Column3DataValues();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn3LiteralValue <em>Column3 Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column3 Literal Value</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn3LiteralValue()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column3LiteralValue();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.tables.MObjectRow#getColumn4DataValues <em>Column4 Data Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Column4 Data Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn4DataValues()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EAttribute getMObjectRow_Column4DataValues();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn4LiteralValue <em>Column4 Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column4 Literal Value</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn4LiteralValue()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column4LiteralValue();

	/**
	 * Returns the meta object for the attribute list '{@link com.montages.mcore.tables.MObjectRow#getColumn5DataValues <em>Column5 Data Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Column5 Data Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn5DataValues()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EAttribute getMObjectRow_Column5DataValues();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn5LiteralValue <em>Column5 Literal Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column5 Literal Value</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn5LiteralValue()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column5LiteralValue();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn2Values <em>Column2 Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column2 Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn2Values()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column2Values();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn3Values <em>Column3 Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column3 Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn3Values()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column3Values();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn4Values <em>Column4 Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column4 Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn4Values()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column4Values();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.mcore.tables.MObjectRow#getColumn5Values <em>Column5 Values</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Column5 Values</em>'.
	 * @see com.montages.mcore.tables.MObjectRow#getColumn5Values()
	 * @see #getMObjectRow()
	 * @generated
	 */
	EReference getMObjectRow_Column5Values();

	/**
	 * Returns the meta object for the '{@link com.montages.mcore.tables.MObjectRow#containmentPropertyHeadersFromContainmentProperties(org.eclipse.emf.common.util.EList) <em>Containment Property Headers From Containment Properties</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Containment Property Headers From Containment Properties</em>' operation.
	 * @see com.montages.mcore.tables.MObjectRow#containmentPropertyHeadersFromContainmentProperties(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getMObjectRow__ContainmentPropertyHeadersFromContainmentProperties__EList();

	/**
	 * Returns the meta object for class '{@link com.montages.mcore.tables.MHeader <em>MHeader</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>MHeader</em>'.
	 * @see com.montages.mcore.tables.MHeader
	 * @generated
	 */
	EClass getMHeader();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MHeader#getReference <em>Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Reference</em>'.
	 * @see com.montages.mcore.tables.MHeader#getReference()
	 * @see #getMHeader()
	 * @generated
	 */
	EReference getMHeader_Reference();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MHeader#getContainingObjectRow <em>Containing Object Row</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Containing Object Row</em>'.
	 * @see com.montages.mcore.tables.MHeader#getContainingObjectRow()
	 * @see #getMHeader()
	 * @generated
	 */
	EReference getMHeader_ContainingObjectRow();

	/**
	 * Returns the meta object for the reference '{@link com.montages.mcore.tables.MHeader#getPreviousHeader <em>Previous Header</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Previous Header</em>'.
	 * @see com.montages.mcore.tables.MHeader#getPreviousHeader()
	 * @see #getMHeader()
	 * @generated
	 */
	EReference getMHeader_PreviousHeader();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	TablesFactory getTablesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.mcore.tables.impl.MAbstractHeaderRowImpl <em>MAbstract Header Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.tables.impl.MAbstractHeaderRowImpl
		 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMAbstractHeaderRow()
		 * @generated
		 */
		EClass MABSTRACT_HEADER_ROW = eINSTANCE.getMAbstractHeaderRow();

		/**
		 * The meta object literal for the '<em><b>Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__CLASSIFIER = eINSTANCE
				.getMAbstractHeaderRow_Classifier();

		/**
		 * The meta object literal for the '<em><b>Column1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__COLUMN1 = eINSTANCE
				.getMAbstractHeaderRow_Column1();

		/**
		 * The meta object literal for the '<em><b>Column2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__COLUMN2 = eINSTANCE
				.getMAbstractHeaderRow_Column2();

		/**
		 * The meta object literal for the '<em><b>Object Row</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__OBJECT_ROW = eINSTANCE
				.getMAbstractHeaderRow_ObjectRow();

		/**
		 * The meta object literal for the '<em><b>Column3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__COLUMN3 = eINSTANCE
				.getMAbstractHeaderRow_Column3();

		/**
		 * The meta object literal for the '<em><b>Column4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__COLUMN4 = eINSTANCE
				.getMAbstractHeaderRow_Column4();

		/**
		 * The meta object literal for the '<em><b>Column5</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__COLUMN5 = eINSTANCE
				.getMAbstractHeaderRow_Column5();

		/**
		 * The meta object literal for the '<em><b>Displayed Column1</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1 = eINSTANCE
				.getMAbstractHeaderRow_DisplayedColumn1();

		/**
		 * The meta object literal for the '<em><b>Displayed Column2</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2 = eINSTANCE
				.getMAbstractHeaderRow_DisplayedColumn2();

		/**
		 * The meta object literal for the '<em><b>Displayed Column3</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3 = eINSTANCE
				.getMAbstractHeaderRow_DisplayedColumn3();

		/**
		 * The meta object literal for the '<em><b>Displayed Column4</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4 = eINSTANCE
				.getMAbstractHeaderRow_DisplayedColumn4();

		/**
		 * The meta object literal for the '<em><b>Displayed Column5</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5 = eINSTANCE
				.getMAbstractHeaderRow_DisplayedColumn5();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.tables.impl.MTableImpl <em>MTable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.tables.impl.MTableImpl
		 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMTable()
		 * @generated
		 */
		EClass MTABLE = eINSTANCE.getMTable();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MTABLE__OBJECT = eINSTANCE.getMTable_Object();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.tables.impl.MObjectRowImpl <em>MObject Row</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.tables.impl.MObjectRowImpl
		 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMObjectRow()
		 * @generated
		 */
		EClass MOBJECT_ROW = eINSTANCE.getMObjectRow();

		/**
		 * The meta object literal for the '<em><b>Containing Header Row</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__CONTAINING_HEADER_ROW = eINSTANCE
				.getMObjectRow_ContainingHeaderRow();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__OBJECT = eINSTANCE.getMObjectRow_Object();

		/**
		 * The meta object literal for the '<em><b>Containment Property Header</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__CONTAINMENT_PROPERTY_HEADER = eINSTANCE
				.getMObjectRow_ContainmentPropertyHeader();

		/**
		 * The meta object literal for the '<em><b>Column1 Values</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN1_VALUES = eINSTANCE
				.getMObjectRow_Column1Values();

		/**
		 * The meta object literal for the '<em><b>Column1 Referenced Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN1_REFERENCED_OBJECTS = eINSTANCE
				.getMObjectRow_Column1ReferencedObjects();

		/**
		 * The meta object literal for the '<em><b>Column2 Referenced Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN2_REFERENCED_OBJECTS = eINSTANCE
				.getMObjectRow_Column2ReferencedObjects();

		/**
		 * The meta object literal for the '<em><b>Column3 Referenced Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN3_REFERENCED_OBJECTS = eINSTANCE
				.getMObjectRow_Column3ReferencedObjects();

		/**
		 * The meta object literal for the '<em><b>Column4 Referenced Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN4_REFERENCED_OBJECTS = eINSTANCE
				.getMObjectRow_Column4ReferencedObjects();

		/**
		 * The meta object literal for the '<em><b>Column5 Referenced Objects</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN5_REFERENCED_OBJECTS = eINSTANCE
				.getMObjectRow_Column5ReferencedObjects();

		/**
		 * The meta object literal for the '<em><b>Column1 Data Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_ROW__COLUMN1_DATA_VALUES = eINSTANCE
				.getMObjectRow_Column1DataValues();

		/**
		 * The meta object literal for the '<em><b>Column1 Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN1_LITERAL_VALUE = eINSTANCE
				.getMObjectRow_Column1LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Column2 Data Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_ROW__COLUMN2_DATA_VALUES = eINSTANCE
				.getMObjectRow_Column2DataValues();

		/**
		 * The meta object literal for the '<em><b>Column2 Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN2_LITERAL_VALUE = eINSTANCE
				.getMObjectRow_Column2LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Column3 Data Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_ROW__COLUMN3_DATA_VALUES = eINSTANCE
				.getMObjectRow_Column3DataValues();

		/**
		 * The meta object literal for the '<em><b>Column3 Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN3_LITERAL_VALUE = eINSTANCE
				.getMObjectRow_Column3LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Column4 Data Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_ROW__COLUMN4_DATA_VALUES = eINSTANCE
				.getMObjectRow_Column4DataValues();

		/**
		 * The meta object literal for the '<em><b>Column4 Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN4_LITERAL_VALUE = eINSTANCE
				.getMObjectRow_Column4LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Column5 Data Values</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MOBJECT_ROW__COLUMN5_DATA_VALUES = eINSTANCE
				.getMObjectRow_Column5DataValues();

		/**
		 * The meta object literal for the '<em><b>Column5 Literal Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN5_LITERAL_VALUE = eINSTANCE
				.getMObjectRow_Column5LiteralValue();

		/**
		 * The meta object literal for the '<em><b>Column2 Values</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN2_VALUES = eINSTANCE
				.getMObjectRow_Column2Values();

		/**
		 * The meta object literal for the '<em><b>Column3 Values</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN3_VALUES = eINSTANCE
				.getMObjectRow_Column3Values();

		/**
		 * The meta object literal for the '<em><b>Column4 Values</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN4_VALUES = eINSTANCE
				.getMObjectRow_Column4Values();

		/**
		 * The meta object literal for the '<em><b>Column5 Values</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MOBJECT_ROW__COLUMN5_VALUES = eINSTANCE
				.getMObjectRow_Column5Values();

		/**
		 * The meta object literal for the '<em><b>Containment Property Headers From Containment Properties</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MOBJECT_ROW___CONTAINMENT_PROPERTY_HEADERS_FROM_CONTAINMENT_PROPERTIES__ELIST = eINSTANCE
				.getMObjectRow__ContainmentPropertyHeadersFromContainmentProperties__EList();

		/**
		 * The meta object literal for the '{@link com.montages.mcore.tables.impl.MHeaderImpl <em>MHeader</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.mcore.tables.impl.MHeaderImpl
		 * @see com.montages.mcore.tables.impl.TablesPackageImpl#getMHeader()
		 * @generated
		 */
		EClass MHEADER = eINSTANCE.getMHeader();

		/**
		 * The meta object literal for the '<em><b>Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MHEADER__REFERENCE = eINSTANCE.getMHeader_Reference();

		/**
		 * The meta object literal for the '<em><b>Containing Object Row</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MHEADER__CONTAINING_OBJECT_ROW = eINSTANCE
				.getMHeader_ContainingObjectRow();

		/**
		 * The meta object literal for the '<em><b>Previous Header</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MHEADER__PREVIOUS_HEADER = eINSTANCE
				.getMHeader_PreviousHeader();

	}

} //TablesPackage
