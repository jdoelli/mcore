/**
 */
package com.montages.mcore.tables.util;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.Switch;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.MHeader;
import com.montages.mcore.tables.MObjectRow;
import com.montages.mcore.tables.MTable;
import com.montages.mcore.tables.TablesPackage;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.mcore.tables.TablesPackage
 * @generated
 */
public class TablesSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static TablesPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TablesSwitch() {
		if (modelPackage == null) {
			modelPackage = TablesPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case TablesPackage.MABSTRACT_HEADER_ROW: {
			MAbstractHeaderRow mAbstractHeaderRow = (MAbstractHeaderRow) theEObject;
			T result = caseMAbstractHeaderRow(mAbstractHeaderRow);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TablesPackage.MTABLE: {
			MTable mTable = (MTable) theEObject;
			T result = caseMTable(mTable);
			if (result == null)
				result = caseMAbstractHeaderRow(mTable);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TablesPackage.MOBJECT_ROW: {
			MObjectRow mObjectRow = (MObjectRow) theEObject;
			T result = caseMObjectRow(mObjectRow);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case TablesPackage.MHEADER: {
			MHeader mHeader = (MHeader) theEObject;
			T result = caseMHeader(mHeader);
			if (result == null)
				result = caseMAbstractHeaderRow(mHeader);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MAbstract Header Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MAbstract Header Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMAbstractHeaderRow(MAbstractHeaderRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MTable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MTable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMTable(MTable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MObject Row</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MObject Row</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMObjectRow(MObjectRow object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>MHeader</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>MHeader</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMHeader(MHeader object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //TablesSwitch
