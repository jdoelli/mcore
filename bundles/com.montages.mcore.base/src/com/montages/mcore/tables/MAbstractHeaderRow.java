/**
 */
package com.montages.mcore.tables;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MAbstract Header Row</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getClassifier <em>Classifier</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn1 <em>Column1</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn2 <em>Column2</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getObjectRow <em>Object Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn3 <em>Column3</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn4 <em>Column4</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getColumn5 <em>Column5</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn1 <em>Displayed Column1</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn2 <em>Displayed Column2</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn3 <em>Displayed Column3</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn4 <em>Displayed Column4</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MAbstractHeaderRow#getDisplayedColumn5 <em>Displayed Column5</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow()
 * @model abstract="true"
 * @generated
 */

public interface MAbstractHeaderRow extends EObject {
	/**
	 * Returns the value of the '<em><b>Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Classifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Classifier</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Classifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: MClassifier = null in nl\n'"
	 * @generated
	 */
	MClassifier getClassifier();

	/**
	 * Returns the value of the '<em><b>Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column1</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Column1()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr: Integer = 1 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n'"
	 * @generated
	 */
	MProperty getColumn1();

	/**
	 * Returns the value of the '<em><b>Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column2</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Column2()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr: Integer = 2 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n'"
	 * @generated
	 */
	MProperty getColumn2();

	/**
	 * Returns the value of the '<em><b>Object Row</b></em>' containment reference list.
	 * The list contents are of type {@link com.montages.mcore.tables.MObjectRow}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Row</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Row</em>' containment reference list.
	 * @see #isSetObjectRow()
	 * @see #unsetObjectRow()
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_ObjectRow()
	 * @model containment="true" resolveProxies="true" unsettable="true"
	 * @generated
	 */
	EList<MObjectRow> getObjectRow();

	/**
	 * Unsets the value of the '{@link com.montages.mcore.tables.MAbstractHeaderRow#getObjectRow <em>Object Row</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetObjectRow()
	 * @see #getObjectRow()
	 * @generated
	 */
	void unsetObjectRow();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.tables.MAbstractHeaderRow#getObjectRow <em>Object Row</em>}' containment reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Object Row</em>' containment reference list is set.
	 * @see #unsetObjectRow()
	 * @see #getObjectRow()
	 * @generated
	 */
	boolean isSetObjectRow();

	/**
	 * Returns the value of the '<em><b>Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column3</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Column3()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr: Integer = 3 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n'"
	 * @generated
	 */
	MProperty getColumn3();

	/**
	 * Returns the value of the '<em><b>Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column4</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Column4()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr: Integer = 4 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n'"
	 * @generated
	 */
	MProperty getColumn4();

	/**
	 * Returns the value of the '<em><b>Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column5</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column5</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_Column5()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let colNr: Integer = 5 in\nif (let e: Boolean = classifier.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null else if (let e: Boolean = let e: Integer = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->size() in \n    if e.oclIsInvalid() then null else e endif < colNr in \n    if e.oclIsInvalid() then null else e endif) then null\n  else let e: MProperty = if classifier.oclIsUndefined()\n  then OrderedSet{}\n  else classifier.allStoredNonContainmentFeatures()\nendif->at(colNr) in \n    if e.oclIsInvalid() then null else e endif\nendif endif\n'"
	 * @generated
	 */
	MProperty getColumn5();

	/**
	 * Returns the value of the '<em><b>Displayed Column1</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Column1</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Column1</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_DisplayedColumn1()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='column1\n'"
	 * @generated
	 */
	MProperty getDisplayedColumn1();

	/**
	 * Returns the value of the '<em><b>Displayed Column2</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Column2</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Column2</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_DisplayedColumn2()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='column2\n'"
	 * @generated
	 */
	MProperty getDisplayedColumn2();

	/**
	 * Returns the value of the '<em><b>Displayed Column3</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Column3</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Column3</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_DisplayedColumn3()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='column3\n'"
	 * @generated
	 */
	MProperty getDisplayedColumn3();

	/**
	 * Returns the value of the '<em><b>Displayed Column4</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Column4</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Column4</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_DisplayedColumn4()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='column4\n'"
	 * @generated
	 */
	MProperty getDisplayedColumn4();

	/**
	 * Returns the value of the '<em><b>Displayed Column5</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Displayed Column5</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Displayed Column5</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMAbstractHeaderRow_DisplayedColumn5()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='column5\n'"
	 * @generated
	 */
	MProperty getDisplayedColumn5();

} // MAbstractHeaderRow
