/**
 */
package com.montages.mcore.tables;

import com.montages.mcore.MProperty;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MHeader</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.mcore.tables.MHeader#getReference <em>Reference</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MHeader#getContainingObjectRow <em>Containing Object Row</em>}</li>
 *   <li>{@link com.montages.mcore.tables.MHeader#getPreviousHeader <em>Previous Header</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.mcore.tables.TablesPackage#getMHeader()
 * @model annotation="http://www.xocl.org/OVERRIDE_OCL classifierDerive='if (let e: Boolean = reference.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen null\n  else if reference.oclIsUndefined()\n  then null\n  else reference.type\nendif\nendif\n' displayedColumn2Derive='let p: MProperty = column2 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column2\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n' displayedColumn1Derive='let p: MProperty = column1 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column1\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n' displayedColumn4Derive='let p: MProperty = column4 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column4\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n' displayedColumn3Derive='let p: MProperty = column3 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column3\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n' displayedColumn5Derive='let p: MProperty = column5 in\nif (let e: Boolean = previousHeader.oclIsUndefined() in \n    if e.oclIsInvalid() then null else e endif) \n  =true \nthen p else if (let e: Boolean = if previousHeader.oclIsUndefined()\n  then null\n  else previousHeader.column5\nendif = p in \n    if e.oclIsInvalid() then null else e endif) then null\n  else p\nendif endif\n' objectRowInitValue='/*let r:OrderedSet(tables::MObjectRow)=OrderedSet{} in r\052/\r\nlet o:objects::MObject=self.containingObjectRow.object in\r\nif o.oclIsUndefined() then OrderedSet{} else\r\nlet r:mcore::MProperty=self.reference in\r\nif r.oclIsUndefined() then OrderedSet{} else\r\nlet prIns:OrderedSet(objects::MPropertyInstance) = o.propertyInstance->select(pi2:objects::MPropertyInstance|pi2.property=r) in\r\nif prIns->isEmpty() then OrderedSet{} else\r\nprIns->first().internalContainedObject->collect(o2:objects::MObject|Tuple{object=o2, containingHeaderRow=self})\r\n->asOrderedSet()\r\nendif endif endif'"
 * @generated
 */

public interface MHeader extends MAbstractHeaderRow {
	/**
	 * Returns the value of the '<em><b>Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reference</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference</em>' reference.
	 * @see #isSetReference()
	 * @see #unsetReference()
	 * @see #setReference(MProperty)
	 * @see com.montages.mcore.tables.TablesPackage#getMHeader_Reference()
	 * @model unsettable="true"
	 * @generated
	 */
	MProperty getReference();

	/** 
	 * Sets the value of the '{@link com.montages.mcore.tables.MHeader#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reference</em>' reference.
	 * @see #isSetReference()
	 * @see #unsetReference()
	 * @see #getReference()
	 * @generated
	 */
	void setReference(MProperty value);

	/**
	 * Unsets the value of the '{@link com.montages.mcore.tables.MHeader#getReference <em>Reference</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetReference()
	 * @see #getReference()
	 * @see #setReference(MProperty)
	 * @generated
	 */
	void unsetReference();

	/**
	 * Returns whether the value of the '{@link com.montages.mcore.tables.MHeader#getReference <em>Reference</em>}' reference is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Reference</em>' reference is set.
	 * @see #unsetReference()
	 * @see #getReference()
	 * @see #setReference(MProperty)
	 * @generated
	 */
	boolean isSetReference();

	/**
	 * Returns the value of the '<em><b>Containing Object Row</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containing Object Row</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containing Object Row</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMHeader_ContainingObjectRow()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='self.eContainer().oclAsType(tables::MObjectRow)'"
	 * @generated
	 */
	MObjectRow getContainingObjectRow();

	/**
	 * Returns the value of the '<em><b>Previous Header</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Previous Header</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Previous Header</em>' reference.
	 * @see com.montages.mcore.tables.TablesPackage#getMHeader_PreviousHeader()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='if self.containingObjectRow.oclIsUndefined() then null else\r\nif self.containingObjectRow.containmentPropertyHeader->isEmpty() then null else\r\nlet pos:Integer=self.containingObjectRow.containmentPropertyHeader->indexOf(self) in \r\nif pos=1 then self.containingObjectRow.containingHeaderRow else self.containingObjectRow.containmentPropertyHeader->at(pos-1) endif\r\nendif endif'"
	 * @generated
	 */
	MAbstractHeaderRow getPreviousHeader();

} // MHeader
