/**
 */
package com.montages.mcore;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>MVariable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.mcore.McorePackage#getMVariable()
 * @model abstract="true"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL fullLabelDerive='\'TODO\'' eNameDerive='if stringEmpty(self.specialEName) or stringEmpty(self.specialEName.trim())\r\nthen self.calculatedShortName.camelCaseLower()\r\nelse self.specialEName.camelCaseLower()\r\nendif'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG fullLabelCreateColumn='false' eNameCreateColumn='false'"
 * @generated
 */

public interface MVariable extends MNamed, MTyped {
} // MVariable
