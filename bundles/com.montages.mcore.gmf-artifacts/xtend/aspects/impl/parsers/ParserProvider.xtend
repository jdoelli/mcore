package aspects.impl.parsers

import com.google.inject.Inject
import metamodel.MetaModel
import org.eclipse.emf.codegen.ecore.genmodel.GenClass
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature
import org.eclipse.gmf.codegen.gmfgen.FeatureLabelModelFacet
import org.eclipse.gmf.codegen.gmfgen.OclChoiceParser
import xpt.expressions.getExpression

class ParserProvider extends impl.parsers.ParserProvider{
	
	@Inject MetaModel xptMetaModel;
	@Inject getExpression xptGetExpression
	
	override createOclChoiceParser(OclChoiceParser it, FeatureLabelModelFacet modelFacet, String parserVar, GenFeature feature, GenClass context) '''
		org.eclipse.emf.ecore.EStructuralFeature editableFeature = «xptMetaModel.MetaFeature(feature)»;
		org.eclipse.gmf.tooling.runtime.parsers.ChoiceParserBase «parserVar» = «»
		«IF it.showExpression != null»
			new org.eclipse.gmf.tooling.runtime.parsers.OclTrackerChoiceParser( //
				editableFeature
				, «safeItemExpression(it, feature)»
				, «xptGetExpression.getExpressionBody(showExpression)»
				, «itemProviderAdapterFactory(it)»
				«addLastParameter(optionalOclTrackerFactoryTypeHint(showExpression))»
				);
		«ELSE»
			new org.eclipse.gmf.tooling.runtime.parsers.OclChoiceParser( //
				editableFeature, Â«safeItemExpression(it, feature)», null, «itemProviderAdapterFactory(it)»);
		«ENDIF»
	'''
	
	def String addLastParameter(CharSequence it) {
		if (it.length == 0) {
			return '';
		}
		return ',' + it;
	}
}