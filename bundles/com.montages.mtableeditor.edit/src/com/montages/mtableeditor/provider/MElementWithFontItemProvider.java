/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mtableeditor.MElementWithFont;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a
 * {@link com.montages.mtableeditor.MElementWithFont} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class MElementWithFontItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MElementWithFontItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBoldFontPropertyDescriptor(object);
			addItalicFontPropertyDescriptor(object);
			addFontSizePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDerivedFontOptionsEncodingPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Font Size feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFontSizePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Font Size feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MElementWithFont_fontSize_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_fontSize_feature",
						"_UI_MElementWithFont_type"),
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__FONT_SIZE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Bold Font feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addBoldFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Bold Font feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MElementWithFont_boldFont_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_boldFont_feature",
						"_UI_MElementWithFont_type"),
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__BOLD_FONT, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Italic Font feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addItalicFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Italic Font feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MElementWithFont_italicFont_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_italicFont_feature",
								"_UI_MElementWithFont_type"),
						MtableeditorPackage.Literals.MELEMENT_WITH_FONT__ITALIC_FONT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Font Options Encoding feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedFontOptionsEncodingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Font Options Encoding feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MElementWithFont_derivedFontOptionsEncoding_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MElementWithFont_derivedFontOptionsEncoding_feature", "_UI_MElementWithFont_type"),
						MtableeditorPackage.Literals.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING, false, false,
						false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		Boolean labelValue = ((MElementWithFont) object).getBoldFont();
		String label = labelValue == null ? null : labelValue.toString();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MElementWithFont.class)) {
		case MtableeditorPackage.MELEMENT_WITH_FONT__BOLD_FONT:
		case MtableeditorPackage.MELEMENT_WITH_FONT__ITALIC_FONT:
		case MtableeditorPackage.MELEMENT_WITH_FONT__FONT_SIZE:
		case MtableeditorPackage.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MtableeditorEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
