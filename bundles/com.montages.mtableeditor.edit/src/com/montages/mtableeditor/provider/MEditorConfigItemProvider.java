/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mcore.provider.MEditorItemProvider;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MEditorConfigAction;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import com.montages.mtableeditor.impl.MEditorConfigImpl;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.langlets.autil.AutilPackage;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mtableeditor.MEditorConfig} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class MEditorConfigItemProvider extends MEditorItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MEditorConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		if (owner instanceof MEditorConfig) {
			if (feature == MtableeditorPackage.Literals.MEDITOR_CONFIG__DO_ACTION) {
				return ((MEditorConfig) owner).doActionUpdate((MEditorConfigAction) value).getContainingTransition()
						.interpreteTransitionAsCommand();
			}
		}

		return super.createSetCommand(domain, owner, feature, value, index);
	};

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			addKindPropertyDescriptor(object);
			addHeaderTableConfigPropertyDescriptor(object);
			addSuperConfigPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aLabel_feature",
								"_UI_AElement_type"),
						AutilPackage.Literals.AELEMENT__ALABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80AUtilLabelAndKindPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aKindBase_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aKindBase_feature",
								"_UI_AElement_type"),
						AutilPackage.Literals.AELEMENT__AKIND_BASE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI__80AUtilLabelAndKindPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aRenderedKind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aRenderedKind_feature",
						"_UI_AElement_type"),
				AutilPackage.Literals.AELEMENT__ARENDERED_KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI__80AUtilLabelAndKindPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Kind feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableEditorElement_kind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableEditorElement_kind_feature",
						"_UI_MTableEditorElement_type"),
				MtableeditorPackage.Literals.MTABLE_EDITOR_ELEMENT__KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Header Table Config feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addHeaderTableConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Header Table Config feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MEditorConfig_headerTableConfig_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MEditorConfig_headerTableConfig_feature",
						"_UI_MEditorConfig_type"),
				MtableeditorPackage.Literals.MEDITOR_CONFIG__HEADER_TABLE_CONFIG, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Super Config feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSuperConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Super Config feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MEditorConfig_superConfig_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MEditorConfig_superConfig_feature",
								"_UI_MEditorConfig_type"),
						MtableeditorPackage.Literals.MEDITOR_CONFIG__SUPER_CONFIG, true, false, true, null,
						getString("_UI_AdditionalConceptsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MEditorConfig_doAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MEditorConfig_doAction_feature",
						"_UI_MEditorConfig_type"),
				MtableeditorPackage.Literals.MEDITOR_CONFIG__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG);
			childrenFeatures.add(MtableeditorPackage.Literals.MEDITOR_CONFIG__MCOLUMN_CONFIG);
			childrenFeatures.add(MtableeditorPackage.Literals.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MEditorConfig.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MEditorConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MEditorConfigImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MEditorConfig.class)) {
		case MtableeditorPackage.MEDITOR_CONFIG__ALABEL:
		case MtableeditorPackage.MEDITOR_CONFIG__AKIND_BASE:
		case MtableeditorPackage.MEDITOR_CONFIG__ARENDERED_KIND:
		case MtableeditorPackage.MEDITOR_CONFIG__KIND:
		case MtableeditorPackage.MEDITOR_CONFIG__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MtableeditorPackage.MEDITOR_CONFIG__MTABLE_CONFIG:
		case MtableeditorPackage.MEDITOR_CONFIG__MCOLUMN_CONFIG:
		case MtableeditorPackage.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MEDITOR_CONFIG__MTABLE_CONFIG,
				MtableeditorFactory.eINSTANCE.createMTableConfig()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MEDITOR_CONFIG__MCOLUMN_CONFIG,
				MtableeditorFactory.eINSTANCE.createMColumnConfig()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MEDITOR_CONFIG__CLASS_TO_TABLE_CONFIG,
				MtableeditorFactory.eINSTANCE.createMClassToTableConfig()));
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MtableeditorEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
