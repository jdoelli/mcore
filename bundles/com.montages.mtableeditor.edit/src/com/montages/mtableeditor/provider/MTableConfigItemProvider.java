/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mcore.MClassifier;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MTableConfigAction;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import com.montages.mtableeditor.impl.MTableConfigImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.command.IdentityCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.ChangeCommand;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XTransition;

/**
 * This is the item provider adapter for a {@link com.montages.mtableeditor.MTableConfig} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class MTableConfigItemProvider extends MTableEditorElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MTableConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		/*
		 * Copied out of MPropertyItemProvider
		 */
		if (feature == MtableeditorPackage.eINSTANCE.getMTableConfig_DoAction())
			if (owner instanceof MTableConfig) {

				// Gather Property from SetCommand, Gather Commands from command
				// interpreter
				MTableConfig mTabConf = (MTableConfig) owner;
				Command cmdInterpretation = mTabConf.doActionUpdate((MTableConfigAction) value)
						.getContainingTransition().interpreteTransitionAsCommand();
				CompoundCommand cmdInterpretationAsCompound = (CompoundCommand) cmdInterpretation;

				// As soon as we got a ChangeCommand, we know that we use
				// customJavaCode Annotations
				boolean useJavaCodeAnnotation = false;
				int changeCommandPosition = -1;
				Object[] commandListArray = cmdInterpretationAsCompound.getCommandList().toArray();

				for (int i = 0; i < commandListArray.length; i++)
					if (commandListArray[i] instanceof ChangeCommand) {
						useJavaCodeAnnotation = true;
						changeCommandPosition = i;
					}

				// If custom java code is used, manipulate here to change focus
				// upon commandstack execution
				if (useJavaCodeAnnotation) {
					final ChangeCommand changeCommand = (ChangeCommand) commandListArray[changeCommandPosition];

					CompoundCommand compoundWithFocusElement = new CompoundCommand(CompoundCommand.MERGE_COMMAND_ALL,
							XTransition.FOCUSCOMMAND);

					for (Command com : cmdInterpretationAsCompound.getCommandList())
						compoundWithFocusElement.append(com);

					compoundWithFocusElement.append(new IdentityCommand(XTransition.FOCUSCOMMAND)

					{

						@Override
						public Collection<?> getAffectedObjects() {
							// TODO Auto-generated method stub
							return changeCommand.getAffectedObjects();
						}
					});

					return compoundWithFocusElement;
				}
				// Else return interpretation of XSemantics annotations (non
				// Java)
				return cmdInterpretation;
			}
		return super.createSetCommand(domain, owner, feature, value, index);
	};

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addIntendedPackagePropertyDescriptor(object);
			addIntendedClassPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addECNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addECLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addECColumnConfigPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addComplementActionTablePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLabelPropertyDescriptor(object);
			}
			addDisplayDefaultColumnPropertyDescriptor(object);
			addDisplayHeaderPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntendedActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addComplementActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSplitChildrenActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addMergeChildrenActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_name_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Intended Package feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIntendedPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_intendedPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_intendedPackage_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_PACKAGE, true, false, true, null,
						getString("_UI_EntryFilteringPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Intended Class feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIntendedClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Class feature.
		 * The list of possible choices is constraint by OCL let rightPackage: Boolean = if (let chain: mcore::MPackage = intendedPackage in
		if chain <> null then true else false 
		endif) 
		=true 
		then (let e0: Boolean = trg.containingPackage = intendedPackage in 
		if e0.oclIsInvalid() then null else e0 endif)
		else true
		endif in
		let isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
		if e1.oclIsInvalid() then null else e1 endif in
		let intended: Boolean = let e1: Boolean = if (isClass)= false 
		then false 
		else if (rightPackage)= false 
		then false 
		else if ((isClass)= null or (rightPackage)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif in
		intended
		
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_intendedClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_intendedClass_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_CLASS, true, false, true, null,
						getString("_UI_EntryFilteringPropertyCategory"), null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<MClassifier> result = new ArrayList<MClassifier>();
						Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						for (Iterator<MClassifier> iterator = result.iterator(); iterator.hasNext();) {
							MClassifier trg = iterator.next();
							if (trg == null) {
								continue;
							}
							if (!((MTableConfigImpl) object).evalIntendedClassChoiceConstraint(trg)) {
								iterator.remove();
							}
						}
						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the EC Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_eCName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_eCName_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__EC_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EC Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_eCLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_eCLabel_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__EC_LABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EC Column Config feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECColumnConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Column Config feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_eCColumnConfig_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_eCColumnConfig_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__EC_COLUMN_CONFIG, false, false, false, null,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_label_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_label_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__LABEL, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Display Default Column feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayDefaultColumnPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Display Default Column feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_displayDefaultColumn_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_displayDefaultColumn_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Display Header feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDisplayHeaderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Display Header feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MTableConfig_displayHeader_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_displayHeader_feature",
								"_UI_MTableConfig_type"),
						MtableeditorPackage.Literals.MTABLE_CONFIG__DISPLAY_HEADER, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL let var0: OrderedSet(MTableConfigAction) = intendedAction in var0
		->append(MTableConfigAction::AddEmptyColumn)
		->append(MTableConfigAction::AddColumnWithRow)
		->append(MTableConfigAction::AddColumnWithOcl)
		->append(MTableConfigAction::AddTableRef)
		->prepend(MTableConfigAction::Do)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_doAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_doAction_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MTableConfigAction> result = new ArrayList<MTableConfigAction>();
				Collection<? extends MTableConfigAction> superResult = (Collection<? extends MTableConfigAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MTableConfigImpl) object).evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Intended Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIntendedActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_intendedAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_intendedAction_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__INTENDED_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Complement Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addComplementActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Complement Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_complementAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_complementAction_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__COMPLEMENT_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Split Children Action feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSplitChildrenActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Split Children Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_splitChildrenAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_splitChildrenAction_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__SPLIT_CHILDREN_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Merge Children Action feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMergeChildrenActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Merge Children Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_mergeChildrenAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_mergeChildrenAction_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__MERGE_CHILDREN_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Complement Action Table feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addComplementActionTablePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Complement Action Table feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MTableConfig_complementActionTable_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MTableConfig_complementActionTable_feature",
						"_UI_MTableConfig_type"),
				MtableeditorPackage.Literals.MTABLE_CONFIG__COMPLEMENT_ACTION_TABLE, true, false, true, null,
				getString("_UI_StructurePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG);
			childrenFeatures.add(MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MTableConfig.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MTableConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MTableConfigImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MTableConfig.class)) {
		case MtableeditorPackage.MTABLE_CONFIG__NAME:
		case MtableeditorPackage.MTABLE_CONFIG__EC_NAME:
		case MtableeditorPackage.MTABLE_CONFIG__EC_LABEL:
		case MtableeditorPackage.MTABLE_CONFIG__LABEL:
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
		case MtableeditorPackage.MTABLE_CONFIG__DISPLAY_HEADER:
		case MtableeditorPackage.MTABLE_CONFIG__DO_ACTION:
		case MtableeditorPackage.MTABLE_CONFIG__INTENDED_ACTION:
		case MtableeditorPackage.MTABLE_CONFIG__COMPLEMENT_ACTION:
		case MtableeditorPackage.MTABLE_CONFIG__SPLIT_CHILDREN_ACTION:
		case MtableeditorPackage.MTABLE_CONFIG__MERGE_CHILDREN_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MtableeditorPackage.MTABLE_CONFIG__COLUMN_CONFIG:
		case MtableeditorPackage.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MTABLE_CONFIG__COLUMN_CONFIG,
				MtableeditorFactory.eINSTANCE.createMColumnConfig()));

		newChildDescriptors
				.add(createChildParameter(MtableeditorPackage.Literals.MTABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG,
						MtableeditorFactory.eINSTANCE.createMReferenceToTableConfig()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
