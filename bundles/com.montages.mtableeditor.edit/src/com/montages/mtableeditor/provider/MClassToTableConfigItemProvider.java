/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mcore.MClassifier;

import com.montages.mtableeditor.MtableeditorPackage;

import com.montages.mtableeditor.impl.MClassToTableConfigImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a
 * {@link com.montages.mtableeditor.MClassToTableConfig} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class MClassToTableConfigItemProvider extends ItemProviderAdapter implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MClassToTableConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassPropertyDescriptor(object);
			addTableConfigPropertyDescriptor(object);
			addIntendedPackagePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Class feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Class feature.
		 * The list of possible choices is constraint by OCL let var1: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
		if e1.oclIsInvalid() then null else e1 endif in
		var1
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MClassToTableConfig_class_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MClassToTableConfig_class_feature",
						"_UI_MClassToTableConfig_type"),
				MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG__CLASS, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MClassToTableConfigImpl) object).evalClassChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Table Config feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addTableConfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Table Config feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MClassToTableConfig_tableConfig_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MClassToTableConfig_tableConfig_feature",
								"_UI_MClassToTableConfig_type"),
						MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG__TABLE_CONFIG, true, false, true, null,
						null, null));
	}

	/**
	 * This adds a property descriptor for the Intended Package feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIntendedPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intended Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MClassToTableConfig_intendedPackage_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MClassToTableConfig_intendedPackage_feature", "_UI_MClassToTableConfig_type"),
						MtableeditorPackage.Literals.MCLASS_TO_TABLE_CONFIG__INTENDED_PACKAGE, true, false, true, null,
						getString("_UI_EntryFilteringPropertyCategory"), null));
	}

	/**
	 * This returns MClassToTableConfig.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MClassToTableConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return "<" + containingFeatureName + ">";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MtableeditorEditPlugin.INSTANCE;
	}

}
