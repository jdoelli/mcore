/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MColumnConfigAction;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import java.util.Collection;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mtableeditor.MColumnConfig} object.
 * <!-- begin-user-doc
 * --> <!-- end-user-doc -->
 * @generated
 */
public class MColumnConfigItemProvider extends MTableEditorElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MColumnConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {
		if (owner instanceof MColumnConfig) {
			if (feature == MtableeditorPackage.Literals.MCOLUMN_CONFIG__DO_ACTION) {
				return ((MColumnConfig) owner).doActionUpdate((MColumnConfigAction) value).getContainingTransition()
						.interpreteTransitionAsCommand();
			}
		}

		return super.createSetCommand(domain, owner, feature, value, index);
	};

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addBoldFontPropertyDescriptor(object);
			addItalicFontPropertyDescriptor(object);
			addFontSizePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDerivedFontOptionsEncodingPropertyDescriptor(object);
			}
			addNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addECNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addECLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addECWidthPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLabelPropertyDescriptor(object);
			}
			addWidthPropertyDescriptor(object);
			addMinimumWidthPropertyDescriptor(object);
			addMaximumWidthPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_name_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Bold Font feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addBoldFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Bold Font feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MElementWithFont_boldFont_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_boldFont_feature",
						"_UI_MElementWithFont_type"),
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__BOLD_FONT, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Italic Font feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addItalicFontPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Italic Font feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MElementWithFont_italicFont_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_italicFont_feature",
								"_UI_MElementWithFont_type"),
						MtableeditorPackage.Literals.MELEMENT_WITH_FONT__ITALIC_FONT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Font Size feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addFontSizePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Font Size feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MElementWithFont_fontSize_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MElementWithFont_fontSize_feature",
						"_UI_MElementWithFont_type"),
				MtableeditorPackage.Literals.MELEMENT_WITH_FONT__FONT_SIZE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Font Options Encoding feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedFontOptionsEncodingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Font Options Encoding feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MElementWithFont_derivedFontOptionsEncoding_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MElementWithFont_derivedFontOptionsEncoding_feature", "_UI_MElementWithFont_type"),
						MtableeditorPackage.Literals.MELEMENT_WITH_FONT__DERIVED_FONT_OPTIONS_ENCODING, false, false,
						false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_FontPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EC Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_eCName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_eCName_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EC Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_eCLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_eCLabel_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_LABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EC Width feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addECWidthPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EC Width feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_eCWidth_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_eCWidth_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__EC_WIDTH, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialEditorConfigSettingsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Label feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MColumnConfig_label_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_label_feature",
						"_UI_MColumnConfig_type"),
				MtableeditorPackage.Literals.MCOLUMN_CONFIG__LABEL, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Width feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addWidthPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Width feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MColumnConfig_width_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_width_feature",
						"_UI_MColumnConfig_type"),
				MtableeditorPackage.Literals.MCOLUMN_CONFIG__WIDTH, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Minimum Width feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMinimumWidthPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Minimum Width feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_minimumWidth_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_minimumWidth_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__MINIMUM_WIDTH, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Maximum Width feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMaximumWidthPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Maximum Width feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MColumnConfig_maximumWidth_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_maximumWidth_feature",
								"_UI_MColumnConfig_type"),
						MtableeditorPackage.Literals.MCOLUMN_CONFIG__MAXIMUM_WIDTH, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_RenderingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MColumnConfig_doAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MColumnConfig_doAction_feature",
						"_UI_MColumnConfig_type"),
				MtableeditorPackage.Literals.MCOLUMN_CONFIG__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MColumnConfig.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MColumnConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MColumnConfig) object).getName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MColumnConfig.class)) {
		case MtableeditorPackage.MCOLUMN_CONFIG__BOLD_FONT:
		case MtableeditorPackage.MCOLUMN_CONFIG__ITALIC_FONT:
		case MtableeditorPackage.MCOLUMN_CONFIG__FONT_SIZE:
		case MtableeditorPackage.MCOLUMN_CONFIG__DERIVED_FONT_OPTIONS_ENCODING:
		case MtableeditorPackage.MCOLUMN_CONFIG__NAME:
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_NAME:
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_LABEL:
		case MtableeditorPackage.MCOLUMN_CONFIG__EC_WIDTH:
		case MtableeditorPackage.MCOLUMN_CONFIG__LABEL:
		case MtableeditorPackage.MCOLUMN_CONFIG__WIDTH:
		case MtableeditorPackage.MCOLUMN_CONFIG__MINIMUM_WIDTH:
		case MtableeditorPackage.MCOLUMN_CONFIG__MAXIMUM_WIDTH:
		case MtableeditorPackage.MCOLUMN_CONFIG__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MtableeditorPackage.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMClassToCellConfig()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMRowFeatureCell()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMOclCell()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCOLUMN_CONFIG__CLASS_TO_CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMEditProviderCell()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
