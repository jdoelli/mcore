/**
 */
package com.montages.mtableeditor.provider;

import com.montages.mcore.MClassifier;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MtableeditorFactory;
import com.montages.mtableeditor.MtableeditorPackage;
import com.montages.mtableeditor.impl.MClassToCellConfigImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a
 * {@link com.montages.mtableeditor.MClassToCellConfig} object. <!--
 * begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class MClassToCellConfigItemProvider extends MTableEditorElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {

	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MClassToCellConfigItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addCellKindPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Class feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Class feature.
		 * The list of possible choices is constraint by OCL let rightPackage: Boolean = if (let chain: mcore::MPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()
		then null
		else containingColumnConfig.containingTableConfig.intendedPackage
		endif in
		if chain <> null then true else false 
		endif) 
		=true 
		then (let e0: Boolean = trg.containingPackage = if containingColumnConfig.containingTableConfig.oclIsUndefined()
		then null
		else containingColumnConfig.containingTableConfig.intendedPackage
		endif in 
		if e0.oclIsInvalid() then null else e0 endif)
		else true
		endif in
		let isClass: Boolean = let e1: Boolean = trg.kind = mcore::ClassifierKind::ClassType in 
		if e1.oclIsInvalid() then null else e1 endif in
		let intended: Boolean = let e1: Boolean = if (isClass)= false 
		then false 
		else if (rightPackage)= false 
		then false 
		else if ((isClass)= null or (rightPackage)= null) = true 
		then null 
		else true endif endif endif in 
		if e1.oclIsInvalid() then null else e1 endif in
		intended
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MClassToCellConfig_class_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MClassToCellConfig_class_feature",
						"_UI_MClassToCellConfig_type"),
				MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CLASS, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MClassToCellConfigImpl) object).evalClassChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Cell Kind feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addCellKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cell Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MClassToCellConfig_cellKind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MClassToCellConfig_cellKind_feature",
						"_UI_MClassToCellConfig_type"),
				MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_KindPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_CONFIG);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MClassToCellConfig.gif.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MClassToCellConfig"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MClassToCellConfig) object).getALabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MClassToCellConfig.class)) {
		case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_KIND:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MtableeditorPackage.MCLASS_TO_CELL_CONFIG__CELL_CONFIG:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMRowFeatureCell()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMOclCell()));

		newChildDescriptors.add(createChildParameter(MtableeditorPackage.Literals.MCLASS_TO_CELL_CONFIG__CELL_CONFIG,
				MtableeditorFactory.eINSTANCE.createMEditProviderCell()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MtableeditorItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
