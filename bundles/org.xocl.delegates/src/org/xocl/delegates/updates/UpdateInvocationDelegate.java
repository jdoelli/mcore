package org.xocl.delegates.updates;

import static org.eclipse.emf.common.util.ECollections.singletonEList;
import static org.xocl.delegates.updates.UpdateHelpers.findOperation;
import static org.xocl.delegates.updates.UpdateParameters.computeParameters;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EOperation.Internal.InvocationDelegate;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.xocl.delegates.eval.XOCLOperationEvaluator;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XAddUpdateMode;
import org.xocl.semantics.XTransition;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.XUpdateMode;

public class UpdateInvocationDelegate implements InvocationDelegate {

	private final EStructuralFeature feature;
	private final List<UpdateParameters> parameters;

	public UpdateInvocationDelegate(EStructuralFeature feature) {
		this.feature = feature;
		this.parameters = computeParameters(feature);
	}

	@Override
	public Object dynamicInvoke(InternalEObject target, EList<?> arguments) throws InvocationTargetException {
		XTransition transition = SemanticsFactory.eINSTANCE.createXTransition();
		EStructuralFeature innerValue = getInnerValueFeature(target, feature);

		Object value = arguments.size() > 0 ? arguments.get(0) : null;

		XUpdate currentTrigger;
		if (feature instanceof EReference) {
			currentTrigger = transition.addReferenceUpdate(target, (EReference) feature, XUpdateMode.REDEFINE, null,
					value, null, null);
		} else {
			currentTrigger = transition.addAttributeUpdate(target, (EAttribute) feature, XUpdateMode.REDEFINE, null,
					value, null, null);
		}

		if (innerValue != null) {
			if (innerValue instanceof EReference && !(innerValue.getEType() instanceof EEnum)) {
				currentTrigger.addReferenceUpdate(target, (EReference) innerValue, XUpdateMode.REDEFINE, null, value,
						null, null);
			} else if (innerValue instanceof EAttribute) {
				currentTrigger.addAttributeUpdate(target, (EAttribute) innerValue, XUpdateMode.REDEFINE, null, value,
						null, null);
			}
		}

		return addUpdates(currentTrigger, target, feature, value);
	}

	private EStructuralFeature getInnerValueFeature(EObject owner, EStructuralFeature feature) {
		return owner.eClass().getEAllStructuralFeatures().stream() //
				.filter(e -> e.getName().equals(feature.getName() + "$InnerValue")) //
				.filter(e -> e.isChangeable()) //
				.findFirst() //
				.orElse(null);
	}

	private XUpdate addUpdates(XUpdate currentTrigger, EObject owner, EStructuralFeature feature, Object value) {
		for (UpdateParameters params : parameters) {
			XOCLOperationEvaluator evaluator = new XOCLOperationEvaluator(params.updateOperation);
			XOCLOperationEvaluator valueEval = new XOCLOperationEvaluator(params.updateValue);

			EStructuralFeature updateFeature = params.updateFeature;

			Object result = evaluator.eval(owner, value);

			if (result != null && Collection.class.isAssignableFrom(result.getClass())) {
				Collection<?> values = (Collection<?>) result;

				for (Object obj : values) {
					if (obj instanceof EObject) {
						EObject eObject = (EObject) obj;
						Object updateValue = valueEval.eval(owner, value, eObject);

						if (updateFeature.isDerived() && updateFeature.isChangeable()) {

							if (params.condition == null
									|| invokeCondition(eObject, params.condition, value, updateValue)) {
								XUpdate xUpdate = invokeUpdate(eObject, updateFeature, updateValue);
								if (xUpdate != null) {
									currentTrigger = currentTrigger.addAndMergeUpdate(xUpdate);
								}
							}

						} else {
							addUpdate(currentTrigger, eObject, params, updateValue);
						}
					}
				}
			}
		}

		return currentTrigger;
	}

	private XUpdate invokeUpdate(EObject target, EStructuralFeature feature, Object value) {
		String opName = feature.getName().concat("$Update");
		EOperation operation = findOperation(target.eClass(), opName);

		if (operation != null) {
			try {
				Object result = target.eInvoke(operation, singletonEList(value));
				if (result instanceof XUpdate) {
					return (XUpdate) result;
				}
			} catch (InvocationTargetException e) {
				throw new RuntimeException(e);
			}
		}
		return null;
	}

	private boolean invokeCondition(EObject target, EOperation condition, Object... params) {
		Object res;
		try {
			res = target.eInvoke(condition, ECollections.asEList(params));

			if (res instanceof Boolean) {
				return (boolean) res;
			}
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		}
		return false;
	}

	private void addUpdate(XUpdate update, EObject owner, UpdateParameters params, Object value) {
		EStructuralFeature feature = params.updateFeature;
		XAddUpdateMode addMode = params.mode == XUpdateMode.ADD ? XAddUpdateMode.FIRST : null;

		if (feature instanceof EReference) {
			update.addReferenceUpdate(owner, (EReference) feature, params.mode, addMode, value, null, null,
					params.persistenceLocation, //
					params.alternativePersistencePackage, //
					params.alternativePersistenceRoot, //
					params.alternativePersistenceReference);
		} else {
			update.addAttributeUpdate(owner, (EAttribute) feature, params.mode, addMode, value, null, null,
					params.persistenceLocation, //
					params.alternativePersistencePackage, //
					params.alternativePersistenceRoot, //
					params.alternativePersistenceReference);
		}
	}

}
