package org.xocl.delegates.updates;

import static org.xocl.delegates.updates.UpdateHelpers.findFeature;
import static org.xocl.delegates.updates.UpdateHelpers.findOperation;
import static org.xocl.delegates.updates.UpdateHelpers.getMode;

import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.xocl.core.util.XoclAnnotationHelper;
import org.xocl.semantics.XUpdateMode;

public class UpdateParameters {

	public final XUpdateMode mode;
	public final EOperation updateOperation;
	public final EStructuralFeature updateFeature;
	public final EOperation updateValue;
	public final String persistenceLocation;
	public final String alternativePersistencePackage;
	public final String alternativePersistenceRoot;
	public final String alternativePersistenceReference;
	public final EOperation condition;

	private UpdateParameters(XUpdateMode mode, EOperation updateOperation, EStructuralFeature updateFeature,
			EOperation updateValue, String persistenceLocation, String alternativePersistencePackage,
			String alternativePersistenceRoot, String alternativePersistenceReference, EOperation condition) {
		this.mode = mode;
		this.updateOperation = updateOperation;
		this.updateFeature = updateFeature;
		this.updateValue = updateValue;
		this.persistenceLocation = persistenceLocation;
		this.alternativePersistencePackage = alternativePersistencePackage;
		this.alternativePersistenceRoot = alternativePersistenceRoot;
		this.alternativePersistenceReference = alternativePersistenceReference;
		this.condition = condition;
	}

	public static List<UpdateParameters> computeParameters(EStructuralFeature feature) {
		EAnnotation annotation = feature.getEAnnotation(XoclAnnotationHelper.oclNsURI);
		EClass eClass = feature.getEContainingClass();

		return annotation.getDetails().keySet().stream() //
				.filter(e -> e.endsWith("Object")) //
				.map(e -> e.replace("Object", "")) //
				.map(name -> new UpdateParameters( //
						getMode(annotation, name), //
						findOperation(eClass, getValue(annotation, name + "Object")), //
						findFeature(eClass, name, annotation), //
						findOperation(eClass, getValue(annotation, name + "Value")), //
						getValue(annotation, name + "ValuePersistenceLocation"), //
						getValue(annotation, name + "ValueAlternativePersistencePackage"), //
						getValue(annotation, name + "ValueAlternativePersistenceRoot"), //
						getValue(annotation, name + "ValueAlternativePersistenceReference"), //
						findOperation(eClass, getValue(annotation, name + "Condition")))) //
				.collect(Collectors.toList());

	}

	private static String getValue(EAnnotation annotation, String key) {
		return annotation.getDetails().get(key);
	}
}
