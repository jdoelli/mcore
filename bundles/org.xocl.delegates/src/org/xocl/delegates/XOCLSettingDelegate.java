package org.xocl.delegates;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EStructuralFeature.Internal.DynamicValueHolder;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.BasicSettingDelegate;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.delegates.eval.XOCLSettingEvaluator;

public class XOCLSettingDelegate extends BasicSettingDelegate {

	private final XOCLSettingEvaluator evaluator;

	public class SettingAnnotation {
		public final String expression;
		public final SettingType type;

		public SettingAnnotation(SettingType type, String expression) {
			this.expression = expression;
			this.type = type;
		}
	}

	public enum SettingType {
		DERIVE,
		UNDEFINED;
	}

	public XOCLSettingDelegate(EStructuralFeature eStructuralFeature) {
		super(eStructuralFeature);
		this.evaluator = new XOCLSettingEvaluator(eStructuralFeature);
	}

	protected SettingAnnotation getSettingAnnotation(EClass eClass) {
		String expression = XoclEmfUtil.findDeriveAnnotationText(eStructuralFeature, eClass);
		if (expression != null) {
			return new SettingAnnotation(SettingType.DERIVE, expression);
		}
		return new SettingAnnotation(SettingType.UNDEFINED, null);
	}

	@Override
	public Object dynamicGet(InternalEObject owner, DynamicValueHolder settings, int dynamicFeatureID, boolean resolve, boolean coreType) {
		Object result = null;
		SettingAnnotation annotation = getSettingAnnotation(owner.eClass());

		if (annotation.type == SettingType.DERIVE) {
			result = evaluator.eval(owner, annotation.expression);
		}
		if (result == null) {
			result = eStructuralFeature.isMany() ? new BasicEList<Object>() : null;
		}

		return result;
	}

	@Override
	public boolean dynamicIsSet(InternalEObject owner, DynamicValueHolder settings, int dynamicFeatureID) {
		return false;
	}

}
