package org.xocl.delegates;

import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;

/**
 * Adapt xocl annotations to be used in context of dynamic 
 * delegates.
 * 
 * We keep
 *  - derive
 *  
 * We transform:
 *  - init
 *  - label
 *  - choice 
 */
public class XOCLAnnotationAdapter {

	public static final String SOURCE = "http://www.xocl.org/OCL";
	public static final String MODIFIED = "http://www.xocl.org/OCL_EDIT";
	public static final String ECORE = "http://www.eclipse.org/emf/2002/Ecore";

	public EPackage adapt(EPackage ePackage) {
		final EAnnotation delegates = getOrCreate(ePackage, ECORE);
		delegates.getDetails().put("settingDelegates", SOURCE);
		delegates.getDetails().put("invocationDelegates", SOURCE);

		for (EClassifier classifier: ePackage.getEClassifiers()) {
			adapt(classifier);

			if (classifier instanceof EClass) {
				List<EStructuralFeature> features = ((EClass) classifier).getEStructuralFeatures();
				for (EStructuralFeature feature: features) {
					adapt(feature);
				}

				List<EOperation> operations = ((EClass) classifier).getEOperations();
				for (EOperation operation: operations) {
					adapt(operation);
				}
			}
		}

		return ePackage;
	}

	public EModelElement adapt(EModelElement element) {
		final EAnnotation annotation = element.getEAnnotation(SOURCE);
		if (annotation == null) {
			return element;
		}

		final String derive = annotation.getDetails().get("derive");
		final String body = annotation.getDetails().get("body");
		final String initValue = annotation.getDetails().get("initValue");
		final String label = annotation.getDetails().get("label");
		final String choiceConstraint = annotation.getDetails().get("choiceConstraint");
		final String choiceConstruction = annotation.getDetails().get("choiceConstruction");

		if (derive == null && body == null) {
			element.getEAnnotations().remove(annotation);
		}
		if (initValue != null) {
			getOrCreate(element, MODIFIED).getDetails().put("initValue", initValue);
		}
		if (label != null) {
			getOrCreate(element, MODIFIED).getDetails().put("label", label);
		}
		if (choiceConstraint != null) {
			getOrCreate(element, MODIFIED).getDetails().put("choiceConstraint", choiceConstraint);
		}
		if (choiceConstruction != null) {
			getOrCreate(element, MODIFIED).getDetails().put("choiceConstruction", choiceConstruction);
		}

		return element;
	}

	private EAnnotation getOrCreate(EModelElement element, String source) {
		EAnnotation annotation = element.getEAnnotation(source);
		if (annotation == null) {
			annotation = EcoreFactory.eINSTANCE.createEAnnotation();
			annotation.setSource(source);
			element.getEAnnotations().add(annotation);
		}
		return annotation;
	}

}
