package com.montages.mcore.qvto;

import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.m2m.internal.qvt.oml.compiler.URIUnitResolver;
import org.eclipse.m2m.internal.qvt.oml.compiler.UnitResolver;
import org.eclipse.m2m.internal.qvt.oml.compiler.UnitResolverFactory;

@SuppressWarnings("restriction")
public class McoreUnitResolverFactory extends UnitResolverFactory {

	public UnitResolver getResolver(URI uri) {
		return new URIUnitResolver(Collections.singletonList(uri.trimSegments(1)));
	}

	public boolean isAccepted(Object source) {
		return source instanceof URI;
	}

	@Override
	protected boolean accepts(URI uri) {
		return isAccepted(uri);
	}

	@Override
	public String getQualifiedName(URI uri) {
		return uri.trimFileExtension().lastSegment();
	}

}
