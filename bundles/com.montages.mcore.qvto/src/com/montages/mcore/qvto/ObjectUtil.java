package com.montages.mcore.qvto;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation.Kind;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;

public class ObjectUtil {

	@Operation (kind=Kind.HELPER, withExecutionContext=true)
	public void logToConsole(org.eclipse.m2m.qvt.oml.util.IContext context, String param) {
		context.getLog().log(param);
	}
	
	private static Object getValue(EAttribute attr, MDataValue dataValue) {
		Object value = null;

		String literal = dataValue.getDataValue();
		if (literal != null) {
			try {
				value = EcoreUtil.createFromString(attr.getEAttributeType(), literal);
			} catch (IllegalArgumentException e) {
				// TODO: handle exception
			}
		}

		return value;
	}

	@Operation(contextual = true)
	public static void setEReference(Object self, Object feature, Object value) {
		if (self instanceof EObject && feature instanceof EReference && value instanceof EObject) {
			EObject src = (EObject) self;
			EReference ref = (EReference) feature;
			EObject val = (EObject) value;

			if (ref.isMany()) {
				@SuppressWarnings("unchecked")
				Collection<Object> values = (Collection<Object>) src.eGet(ref);
				values.add(val);
			} else {
				src.eSet(ref, val);
			}
		}
	}

	@Operation(contextual = true)
	public static void setAttributes(Object self, Object eClass, Object object) {
		if (self instanceof EObject && eClass instanceof EClass && object instanceof MObject) {
			EObject result = (EObject) self;
			EClass aClass = (EClass) eClass;
			MObject mObject = (MObject) object;

			for (MPropertyInstance pInstance :  mObject.getPropertyInstance()) {
				EAttribute attr = find(aClass, pInstance.getProperty().getEName());
				if (attr != null) {
					if (attr.isMany()) {
						@SuppressWarnings("unchecked")
						Collection<Object> values = (Collection<Object>) result.eGet(attr);

						for (MDataValue dataValue: pInstance.getInternalDataValue()) {
							Object value = getValue(attr, dataValue);
							if (value != null)
								values.add(value);
						}
					} else {
						if (!pInstance.getInternalDataValue().isEmpty()) {
							Object value = getValue(attr, pInstance.getInternalDataValue().get(0));
							if (value != null)
								result.eSet(attr, value);
						}
					}
				}
			}
		}
	}

	@Operation(contextual = true)
	public static void setProperties(Object self, Object mClass, Object object) {
		if (self instanceof EObject && mClass instanceof MClassifier
				&& object instanceof MObject) {

			EObject eObject = (EObject) self;
			MObject result = (MObject) object;
			MClassifier mClassifier = (MClassifier) mClass;

			for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
				if (eObject.eIsSet(eAttribute)) {

					MProperty mProperty = findMProperty(mClassifier, eAttribute);
					if (mProperty != null) {
						if (eAttribute.isMany()) {

						} else {
							Object value = eObject.eGet(eAttribute);
							String stringValue = EcoreUtil.convertToString(
									eAttribute.getEAttributeType(), value);

							MPropertyInstance mPropertyInstance = ObjectsFactory.eINSTANCE
									.createMPropertyInstance();
							MDataValue mDataValue = ObjectsFactory.eINSTANCE
									.createMDataValue();
							mDataValue.setDataValue(stringValue);
							mPropertyInstance.getInternalDataValue().add(
									mDataValue);

							mPropertyInstance.setProperty(mProperty);

							result.getPropertyInstance().add(mPropertyInstance);
						}
					}
				}
			}
		}
	}

	private static MProperty findMProperty(MClassifier mClassifier,
			EAttribute eAttribute) {
		for (MProperty mProperty : mClassifier.allFeatures()) {
			if (mProperty.getEName().equals(eAttribute.getName())) {
				return mProperty;
			}
		}
		return null;
	}

	private static EAttribute find(EClass eClass, String pInstance) {
		for (EAttribute attr: eClass.getEAllAttributes()) {
			if (attr.getName().equals(pInstance)) {
				return attr;
			}
		}
		return null;
	}

	@Operation(contextual = true)
	public static void addValue(Object self, Object value) {
		if (self instanceof List<?>) {
			@SuppressWarnings("unchecked")
			List<Object> col = (List<Object>) self;
			try {
				col.add(value);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Operation(contextual = true)
	public static Object getListValue(Object self, Object ref) {
		if (self instanceof EObject && ref instanceof EReference) {
			return (EList<EObject>) ((EObject) self).eGet((EReference) ref);
		}
		return null;
	}

	@Operation(contextual = true)
	public static Object getValue(Object self, Object ref) {
		if (self instanceof EObject && ref instanceof EReference) {
			return ((EObject) self).eGet((EReference) ref);
		}
		return null;
	}

	@Operation(contextual = true)
	public static Object findEClass(Object self) {
		if (self instanceof MObject) {
			MObject mObject = (MObject) self;
			MClassifier mClassifier = mObject.getType();
			MComponent mComponent = mClassifier.getContainingPackage()
					.getContainingComponent();
			EPackage ePackage = findPackage(
					mClassifier.getContainingPackage(),
					mComponent.getGeneratedEPackage());

			if (ePackage != null) {
				EClassifier eClassifier = ePackage.getEClassifier(mClassifier
						.getEName());

				if (eClassifier instanceof EClass) {
					return (EClass) eClassifier;
				}
			}

		}
		return null;
	}

	@Operation(contextual = true)
	public static boolean canResolveLate(Object self, Object ref) {
		if (false == self instanceof EObject || false == ref instanceof EObject) {
			return false;
		}
		EObject selfEObject = (EObject)self;
		EObject refEObject = (EObject)ref;
		return refEObject.eResource() == selfEObject.eResource();
	}

	private static EPackage findPackage(MPackage mPackage,
			EList<EPackage> generatedEPackage) {
		for (EPackage ePackage: generatedEPackage) {
			if (ePackage.getName().equals(mPackage.getEName())) {
				return ePackage;
			}
			EPackage found = findPackage(mPackage, ePackage.getESubpackages());
			if (found != null) {
				return found;
			}
		}
		return null;
	}

}
