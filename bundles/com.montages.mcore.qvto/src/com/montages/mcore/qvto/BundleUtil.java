package com.montages.mcore.qvto;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;

public class BundleUtil {

	/**
	 * Returns the EPackage name from its nsURI last segment.
	 * @param EPackage instance
	 * @return {@link EPackage} name.
	 */
	@Operation(contextual = true)
	public static String getPackageName(Object self) {
		String packageName = null;
		
		if (self instanceof EPackage) {
			EPackage ePackage = (EPackage) self;
			URI uri = URI.createURI(ePackage.getNsURI());
			String[] segments = uri.segments();

			if (ePackage.getESuperPackage() == null) {
				if (segments.length == 2) {
					// Segment has component name and package name
					// Take last segment as package name.
					packageName = segments[1];
				}
			} else {
				String superPackageName = getPackageName(ePackage.getESuperPackage());
				int i = 1; // removes segment for component.
				while (packageName == null || i < segments.length) {
					if ( (superPackageName == null || packageName==null) && i== segments.length)
					{
						packageName = segments[i-1];
						break;
					}
						
					if (segments[i].equalsIgnoreCase(superPackageName) && segments.length < i + 1) {
						packageName = segments[i + 1];
					}
					
					i++;
				}
			}
		}
		
		return packageName;
	}

	@Operation(contextual = true)
	public static String getFileName(Object self) {
		if (self instanceof EPackage) {
			EPackage ePackage = (EPackage) self;
			return ePackage.eResource().getURI().lastSegment();
		}
		return null;
	}

	@Operation(contextual = true)
	public static String getBundleName(Object self) {
		if (self instanceof EPackage) {
			EPackage ePackage = (EPackage) self;
			URI uri = URI.createURI(ePackage.getNsURI());
			if (uri.hasAuthority()){
				String[] authority = null;
				if(uri.authority().contains("\\."))
				     authority = uri.authority().split("\\.");
				else if(uri.authority().contains(":"))
					authority = uri.authority().split(":");

				if (authority.length > 0) {
					String bundle;
					if (authority[0].trim().isEmpty()) {
						bundle = split(authority[1]);
					} else {
						bundle = split(authority[0]);
					}

					if (uri.segmentCount() == 1){
						bundle += "."+uri.segment(0).toLowerCase();
					} else {
						for (int i=0; i<uri.segmentCount() - 1; i++){
							bundle += "."+uri.segment(i).toLowerCase();
						}
					}
					return bundle;
				}
			}
		}
		return null;
	}

	@Operation(contextual = true)
	public static String getDomainType(Object self) {
		if (self instanceof String) {
			URI uri = URI.createURI((String) self);
			String[] authority = null;
			if(uri.hasAuthority() && uri.authority().contains("\\."))
			     authority = uri.authority().split("\\.");
			else if(uri.hasAuthority() && uri.authority().contains(":"))
				authority = uri.authority().split(":");
			if (authority != null && authority.length == 3) {
				return authority[2];
			}
		}
		return "org";
	}

	@Operation(contextual = true)
	public static String getDomainName(Object self) {
		if (self instanceof String) {
			URI uri = URI.createURI((String) self);
			String[] authority = null;
			if(uri.hasAuthority() && uri.authority().contains("\\."))
			     authority = uri.authority().split("\\.");
			else if(uri.hasAuthority() && uri.authority().contains(":"))
				authority = uri.authority().split(":");
			if (authority != null && authority.length == 3) {
				return authority[1];
			}
		}
		return "example";
	}

	@Operation(contextual = true)
	public static String getSimpleName(Object self) {
		if (self instanceof String) {
			URI uri = URI.createURI((String) self);
			String[] segments = uri.segments();

			return segments != null && segments.length > 0 ? segments[0] : "MComponent";
		}
		return "MComponent";
	}

	private static String split(String path) {
		String res = "";
		String[] values = path.split("\\.");

		if (values.length >= 3) {
			res = values[2]+"."+values[1];
		}

		return res;
	}
}
