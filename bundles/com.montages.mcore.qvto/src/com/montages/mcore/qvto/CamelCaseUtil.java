package com.montages.mcore.qvto;

import org.eclipse.m2m.qvt.oml.blackbox.java.Operation;
import org.xocl.core.util.XoclHelper;

public class CamelCaseUtil {
	
	@Operation(contextual = true)
	public static String camelCaseToBusiness(Object self) {
	  return XoclHelper.camelCaseToBusiness((String) self);
	}
	
	@Operation(contextual = true)
	public static String camelCaseLower(Object self) {
	  return XoclHelper.camelCaseLower((String) self);
	}
	
	@Operation(contextual = true)
	public static String camelCaseUpper(Object self) {
	  return XoclHelper.camelCaseUpper((String) self);
	}
	
	@Operation(contextual = true)
	public static String allLowerCase(Object self) {
	  return XoclHelper.allLowerCase((String) self);
	}
	
}

