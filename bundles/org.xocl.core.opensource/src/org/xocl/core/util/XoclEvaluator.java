package org.xocl.core.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.impl.BasicEObjectImpl;
import org.eclipse.emf.ecore.impl.DynamicEObjectImpl;
import org.eclipse.emf.ecore.util.BasicInternalEList;
import org.eclipse.emf.ecore.util.EcoreEList;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.types.CollectionType;
import org.eclipse.ocl.types.TupleType;
import org.eclipse.ocl.util.Tuple;
import org.xocl.core.XOCLPlugin;

public class XoclEvaluator {

	private BasicEObjectImpl context = null;
	private Map<ETypedElement, Object> cachedValues = null;
	private EObject myContainerEObject;
	private boolean isContainerSetOnCreation = false;

	public XoclEvaluator(BasicEObjectImpl context, Map<ETypedElement, Object> cachedValues) {
		this.context = context;
		this.cachedValues = cachedValues;
	}

	public Object evaluateElement(ETypedElement eElement, Query query) throws IOException {
		boolean caching = XOCLPlugin.getDefault().getPrefs().getBoolean(XoclConstants.CACHING, false);
		Object result = cachedValues.get(eElement);

		if (!caching || result == null) {
			result = evaluateElementValue(eElement, query);
			result = convert(eElement, result);
			cachedValues.put(eElement, result);
		}

		return result;
	}

	public boolean isContainerSetOnCreation() {
		return isContainerSetOnCreation;
	}

	public void setContainerOnCreation(EObject containerEObject) {
		myContainerEObject = containerEObject;
	}

	@SuppressWarnings("unchecked")
	private Object convert(ETypedElement eElement, Object result) {
		Object converted = result;

		if (eElement.isMany()) {

			// We allow XOCL annotations to return single values also for elements
			// with a multiplicity greater than 1. In this case, we implicitly convert
			// the value into a list with a single item.
			if (!(converted instanceof Collection)) {
				ArrayList<Object> list = new ArrayList<Object>();
				// we check for 'null' value, because
				// 'null' translates to an empty list,
				// not a list containing 'null'
				if (converted != null) {
					list.add(converted);
				}
				converted = list;
			}

			if (eElement.getEType().getInstanceClass() == java.util.Map.Entry.class) {
				BasicEMap<Object, Object> map = new BasicEMap<Object, Object>();
				map.addAll((Collection) converted);
				converted = map;
			} else if (eElement instanceof EStructuralFeature) {
				if ((eElement instanceof EReference) && ((EReference) eElement).isContainment()) {
					EReference eReference = (EReference) eElement;
					Class<?> instanceClass = eReference.getEType().getInstanceClass();

					converted = new BasicInternalEList<EObject>(
							instanceClass == null ? DynamicEObjectImpl.class : instanceClass,
							(Collection<EObject>) converted);
				} else {
					converted = new EcoreEList.UnmodifiableEList<EObject>(context, (EStructuralFeature) eElement,
							((Collection<?>) converted).size(), ((Collection<?>) converted).toArray());
				}
			} else {
				converted = new BasicEList<Object>((Collection<?>) converted);
			}
		}

		// If OCL returns a collection but the ETypedElement requires a single value,
		// we try to convert it
		if (result instanceof Collection && !eElement.isMany()) {
			EClassifier eType = eElement.getEType();
			String eTypeName = eType.getName();
			converted = convertManyToOne(eTypeName, result);
		}

		return converted;
	}

	private Object convertManyToOne(String eTypeName, Object result) {
		Object converted = result;

		if (eTypeName.equalsIgnoreCase("EString")) {
			@SuppressWarnings("unchecked")
			Collection<String> typedResult = (Collection<String>) result;
			StringBuffer sb = new StringBuffer();
			for (String string : typedResult) {
				sb.append(string);
				sb.append(", ");
			}
			// remove superfluous ", " at the end
			converted = sb.substring(0, sb.lastIndexOf(","));
		} else {
			// do nothing...
			// we may add more cases here in the future
		}

		return converted;
	}

	private Object evaluateElementValue(ETypedElement eElement, Query query) throws IOException {
		Object oclResult = null;
		try {
			oclResult = query.evaluate(context);
		} catch (Exception e) {
			return oclResult;
		}

		Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment = query
				.getOCL().getEnvironment();

		if (isTupleType(query)) {
			oclResult = handleTuple(eElement, oclResult, environment);
		}

		return oclResult;
	}

	private boolean isTupleType(Query query) {
		EClassifier resultType = query.resultType();

		if (resultType instanceof TupleType<?, ?>) {
			return true;
		} else if (resultType instanceof CollectionType<?, ?>) {
			Object elementType = ((CollectionType<?, ?>) resultType).getElementType();
			if (elementType instanceof TupleType<?, ?>) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	private Object handleTuple(ETypedElement eElement, Object oclResultObject,
			Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment)
			throws IOException {
		Object res = null;

		// We allow XOCL annotations to return single values also for elements
		// with a multiplicity greater than 1. In this case, we implicitly convert
		// the value into a list with a single item. Therefore, we can receive a single
		// tuple
		// here even if eElement.isMany() is true, and we need to populate it correctly.
		// Implicit conversion into a list is then handled in convert(...)
		if (!eElement.isMany() || !(oclResultObject instanceof Collection)) {
			Tuple<EOperation, EStructuralFeature> oclResult = (Tuple<EOperation, EStructuralFeature>) oclResultObject;
			EObject resEObject = createTuple(eElement, oclResult, environment);

			res = populateTuple(resEObject, eElement, oclResult, environment);

			if ((myContainerEObject != null) && (eElement instanceof EStructuralFeature)) {
				myContainerEObject.eSet((EStructuralFeature) eElement, convert(eElement, resEObject));
				isContainerSetOnCreation = true;
			}
		} else {
			Collection<Tuple<EOperation, EStructuralFeature>> oclResultCollection = (Collection<Tuple<EOperation, EStructuralFeature>>) oclResultObject;
			Collection<EObject> result = new LinkedHashSet<EObject>();
			for (Tuple<EOperation, EStructuralFeature> oclResult : oclResultCollection) {
				EObject resEObject = createTuple(eElement, oclResult, environment);
				result.add(resEObject);
			}

			Iterator<Tuple<EOperation, EStructuralFeature>> iterator = oclResultCollection.iterator();
			for (EObject resultEntry : result) {
				Tuple<EOperation, EStructuralFeature> oclResult = iterator.next();
				res = populateTuple(resultEntry, eElement, oclResult, environment);
			}

			if ((myContainerEObject != null) && (eElement instanceof EStructuralFeature)) {
				myContainerEObject.eSet((EStructuralFeature) eElement, convert(eElement, result));
				isContainerSetOnCreation = true;
			}

			res = result;
		}

		return res;
	}

	private EObject createTuple(ETypedElement eElement, Tuple<EOperation, EStructuralFeature> oclResult,
			Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment)
			throws IOException {
		if (oclResult == null) {
			return null;
		}

		// It is only possible to create instances of concrete eclasses.
		EClassifier eType = eElement.getEType();
		if (!(eType instanceof EClass) || ((EClass) eType).isAbstract()) {
			throw new IllegalStateException("Cannot create instance for type: " + eType);
		}

		EFactory factoryInstance = eType.getEPackage().getEFactoryInstance();
		return factoryInstance.create((EClass) eType);
	}

	@SuppressWarnings("unchecked")
	private Object populateTuple(EObject resultEntry, ETypedElement eElement,
			Tuple<EOperation, EStructuralFeature> oclResult,
			Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment)
			throws IOException {
		if (oclResult == null) {
			return null;
		}
		// Iterate over attributes/references of the tuple and the target result type
		// If two elements match (i.e. same name, same type) copy value
		TupleType<EOperation, EStructuralFeature> oclTupleType = oclResult.getTupleType();
		EList<EStructuralFeature> oclStructuralFeatures = oclTupleType.oclProperties();
		EList<EStructuralFeature> resultStructuralFeatures = resultEntry.eClass().getEAllStructuralFeatures();

		for (EStructuralFeature oclStructuralFeature : oclStructuralFeatures) {
			for (EStructuralFeature resultStructuralFeature : resultStructuralFeatures) {
				if (oclStructuralFeature.getName().equalsIgnoreCase(resultStructuralFeature.getName()) && (XoclHelper
						.isSameType(oclStructuralFeature.getEType(), resultStructuralFeature.getEType(), environment)
						|| XoclHelper.isFirstSubTypeOfSecond(oclStructuralFeature.getEType(),
								resultStructuralFeature.getEType())
						|| XoclHelper.isCollectionEquivalent(oclStructuralFeature, resultStructuralFeature,
								environment))) {

					if (resultStructuralFeature.isDerived()) {
						throw new IllegalStateException("Cannot set feature: " + resultStructuralFeature.getName() + ":"
								+ resultStructuralFeature.getEType().getName());
					}

					Object value = oclResult.getValue(oclStructuralFeature.getName());
					if (resultStructuralFeature.isMany()
							&& resultStructuralFeature.getEType().getInstanceClass() == java.util.Map.Entry.class) {

						EMap<?, ?> map = new BasicEMap<>();
						map.addAll((Collection) value);
						value = map;
					}

					if (value != null && Collection.class.isAssignableFrom(value.getClass())) {
						value = ECollections.asEList(new ArrayList<>((Collection) value));
					}

					resultEntry.eSet(resultStructuralFeature, value);
					break;
				}
			}
		}

		if (resultEntry instanceof IXoclInitializable) {
			((IXoclInitializable) resultEntry).allowInitialization();
		}

		return resultEntry;
	}
}