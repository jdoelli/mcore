/**
 * 
 */
package org.xocl.core.util;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceFactoryImpl;

public final class XoclResourceHelper {
	
	public static final XoclResourceHelper INSTANCE = new XoclResourceHelper();
	
	private XoclResourceHelper() {
	}
	
	public Object loadObjectFromXML(IFile file, Registry packageRegistry) {
		return loadObject(file, packageRegistry, new XMLResourceFactoryImpl());
	}
	
	public Object loadObjectFromXML(URI uri, Registry packageRegistry) {
		return loadObject(uri, packageRegistry, new XMLResourceFactoryImpl());
	}
	
	public Object loadObject(IFile file, EPackage.Registry packageRegistry, Resource.Factory resourceFactory) {
		URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		Resource resource = loadResource(uri, packageRegistry, resourceFactory);
		return resource.getContents().get(0);
	}

	public Object loadObject(IFile file, ResourceSet rs, Resource.Factory resourceFactory) {
		URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		Resource resource = loadResource(uri, resourceFactory, rs);
		return resource.getContents().get(0);
	}

	public Object loadObject(URI uri, EPackage.Registry packageRegistry, Resource.Factory resourceFactory) {
		Resource resource = loadResource(uri, packageRegistry, resourceFactory);
		return resource.getContents().get(0);
	}

	public Object loadObject(URI uri, ResourceSet rs, Resource.Factory resourceFactory) {
		Resource resource = loadResource(uri, resourceFactory, rs);
		return resource.getContents().get(0);
	}

	public Resource loadXMLResource(URI uri, EPackage.Registry packageRegistry) {
		return loadResource(uri, packageRegistry, new XMLResourceFactoryImpl());
	}
	
	public Resource loadResource(URI uri, Resource.Factory resourceFactory, ResourceSet rs) {
		Map<String, Object> map = rs.getResourceFactoryRegistry().getExtensionToFactoryMap();
		map.put(uri.fileExtension(), resourceFactory);
		
		Resource resource = rs.getResource(uri, true);
		return resource;
	}
	
	public Resource loadResource(URI uri, EPackage.Registry packageRegistry, Resource.Factory resourceFactory) {
		ResourceSet rs = new ResourceSetImpl();
		if (packageRegistry != null) {
			rs.setPackageRegistry(packageRegistry);
		}
		return loadResource(uri, resourceFactory, rs);
	}
	
	public Resource createXMLResource(URI uri) {
		return (new XMLResourceFactoryImpl()).createResource(uri);
	}
	
	public Resource createResource(Resource resource, ResourceFactoryImpl factory, String fileName, String fileExtension) {
		URI uri = resource.getURI();
		uri = uri.trimFileExtension();
		uri = uri.trimSegments(1).appendSegment(fileName);
		uri = uri.appendFileExtension(fileExtension);
		return factory.createResource(uri);
	}
	
	public IFile getFile(URI uri) {
	    String scheme = uri.scheme();
	    if ("platform".equals(scheme) && uri.segmentCount() > 1 && "resource".equals(uri.segment(0))) {
	      StringBuffer platformResourcePath = new StringBuffer();
	      for (int j = 1, size = uri.segmentCount(); j < size; ++j) {
	        platformResourcePath.append('/');
	        platformResourcePath.append(uri.segment(j));
	      }
	      return ResourcesPlugin.getWorkspace().getRoot().getFile(new Path(platformResourcePath.toString()));
	    }
	    return null;
	}
	
	public void saveAsUTF8(Resource r) throws IOException {
    	Map<String, Object> options = new HashMap<String, Object>();
    	options.put(XMLResource.OPTION_ENCODING, "UTF8" ); //$NON-NLS-1$
    	r.save(options);
	}
}
