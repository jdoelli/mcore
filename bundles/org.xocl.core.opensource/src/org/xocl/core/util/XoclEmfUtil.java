package org.xocl.core.util;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;

public class XoclEmfUtil {
	public static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL"; //$NON-NLS-1$
	public static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL"; //$NON-NLS-1$
	
	public static final String EDITOR_CONFIG_ANNOTATION_SOURCE = "http://www.xocl.org/EDITORCONFIG"; //$NON-NLS-1$
	public static final String OVERRIDE_EDITOR_CONFIG_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_EDITORCONFIG"; //$NON-NLS-1$

	public static final String findDeriveAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass) {
		return findAnnotationText(eStructuralFeature, eClass, "derive", "Derive"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public static final String findBodyAnnotationText(EOperation eOperation, EClass eClass) {
		return findAnnotationText(eOperation, eClass, "body", "Body"); //$NON-NLS-1$ //$NON-NLS-2$
	}
	
	public static final String findChoiceConstraintAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass) {
		return findAnnotationText(eStructuralFeature, eClass, "choiceConstraint", "ChoiceConstraint"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static final String findChoiceConstructionAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass) {
		return findAnnotationText(eStructuralFeature, eClass, "choiceConstruction", "ChoiceConstruction"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	public static final String findPropertyCategoryAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass) {
		return findAnnotationText(eStructuralFeature, eClass, "propertyCategory", null, //$NON-NLS-1$
				 EDITOR_CONFIG_ANNOTATION_SOURCE, null);
	}

	public static final String findPropertyMultilineAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass) {
		return findAnnotationText(eStructuralFeature, eClass, "propertyMultiline", null, //$NON-NLS-1$
				 EDITOR_CONFIG_ANNOTATION_SOURCE, null);
	}

	public static final String findAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass,
			String keyPattern, String overrideKeyPattern) {
		return findAnnotationText(eStructuralFeature, eClass, keyPattern, overrideKeyPattern, OCL_ANNOTATION_SOURCE, OVERRIDE_OCL_ANNOTATION_SOURCE);
	}

	public static final String findAnnotationText(EOperation eOperation, EClass eClass,
			String keyPattern, String overrideKeyPattern) {
		return findAnnotationText(eOperation, eClass, keyPattern, overrideKeyPattern, OCL_ANNOTATION_SOURCE, OVERRIDE_OCL_ANNOTATION_SOURCE);
	}

	
	public static final String findAnnotationText(EStructuralFeature eStructuralFeature, EClass eClass,
			String keyPattern, String overrideKeyPattern, String annotationSource, String overrideAnnotationSource) {
		return findAnnotationText(new EStructuralFeatureWrapper(eStructuralFeature), eClass, keyPattern, overrideKeyPattern,
				annotationSource, overrideAnnotationSource);
	}

	public static final String findAnnotationText(EOperation eOperation, EClass eClass,
			String keyPattern, String overrideKeyPattern, String annotationSource, String overrideAnnotationSource) {
		return findAnnotationText(new EOperationWrapper(eOperation), eClass, keyPattern, overrideKeyPattern,
				annotationSource, overrideAnnotationSource);
	}
	
	public static final String findAnnotationText(ENamedClassMember eNamedClassMember, EClass eClass,
			String keyPattern, String overrideKeyPattern, String annotationSource, String overrideAnnotationSource) {
		// EMF algorithm to find the best matching annotation to the eClass
		EList<EClass> eAllTypes = new UniqueEList<EClass>(eClass.getEAllSuperTypes());
		eAllTypes.add(eClass);
	    ECollections.reverse(eAllTypes);
	    for (EClass currentClass : eAllTypes) {
			String annotationText = findDirectAnnotationText(eNamedClassMember, currentClass, keyPattern, overrideKeyPattern,
					annotationSource, overrideAnnotationSource);
			if (annotationText != null) {
				return annotationText;
			}
	    }
		return null;
	}
	
	
	public static final String findDirectAnnotationText(ENamedClassMember eNamedClassMember, EClass eClass,
			String keyPattern, String overrideKeyPattern, String annotationSource, String overrideAnnotationSource) {
		String annotationText = null;

		// Try to find the annotation in the keyPattern annotations
		if (eClass == eNamedClassMember.getEContainingClass()) {
			EAnnotation eAnnotation = eNamedClassMember.getENamedElement().getEAnnotation(annotationSource);
			if (eAnnotation != null) {
				annotationText = eAnnotation.getDetails().get(keyPattern);
			}
		}

		if ((annotationText == null) && (overrideKeyPattern != null)) {
			// Try to find the annotation in the overriding annotations
			EAnnotation eAnnotation = eClass.getEAnnotation(overrideAnnotationSource);
			if (eAnnotation != null) {
				annotationText = eAnnotation.getDetails().get(eNamedClassMember.getENamedElement().getName() + overrideKeyPattern);
			}
		}
		
		return annotationText;
	}

	public static EAnnotation findAnnotationForSuffix(EModelElement modelElement, String suffix) {
		for (EAnnotation next : modelElement.getEAnnotations()) {
			String nextSource = next.getSource();
			if (nextSource != null && nextSource.endsWith(suffix)) {
				return next;
			}
		}

		EAnnotation annotation = org.eclipse.emf.ecore.EcoreFactory.eINSTANCE.createEAnnotation();
		annotation.setSource("http://www.xocl.org/" + suffix);
		modelElement.getEAnnotations().add(annotation);

		return annotation;
	}
	
	public interface ENamedClassMember {
		public EClass getEContainingClass();
		public ENamedElement getENamedElement();
	}
	
	public static class EStructuralFeatureWrapper implements ENamedClassMember {
		private final EStructuralFeature myEStructuralFeature;

		public EStructuralFeatureWrapper(EStructuralFeature eStructuralFeature) {
			myEStructuralFeature = eStructuralFeature;
		}

		public EClass getEContainingClass() {
			return myEStructuralFeature.getEContainingClass();
		}

		public ENamedElement getENamedElement() {
			return myEStructuralFeature;
		}
	}

	public static class EOperationWrapper implements ENamedClassMember {
		private final EOperation myEOperation;

		public EOperationWrapper(EOperation eOperation) {
			myEOperation = eOperation;
		}

		public EClass getEContainingClass() {
			return myEOperation.getEContainingClass();
		}

		public ENamedElement getENamedElement() {
			return myEOperation;
		}
	}
}
