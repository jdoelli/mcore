/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.core.util;

import java.text.MessageFormat;
import java.util.Stack;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.osgi.util.NLS;
import org.xocl.core.XOCLPlugin;


/**
 * @author Max Stepanov
 *
 */
public final class XoclErrorHandler {

	private static class Context {
		String pluginId;
		String expression;
		EClass eClass;
		Object object;
		
		public Context(String pluginId, String expression, EClass eClass, Object object) {
			this.pluginId = pluginId;
			this.expression = expression;
			this.eClass = eClass;
			this.object = object;
		}

		
		@Override
		public String toString() {
			return " for: " + className(eClass) + "#" + operationOrFeature(object) + " : ocl: " + firstN(expression, 60).replace('\n', ' ');
		}
		
		private static String firstN(String text, int n) {
			if (text == null) { 
				return "<null>";
			}
			return text.length() <= n ? text : text.substring(0, n) + "...";
		}
		
		private static String className(EClass eClass) {
			return eClass == null ? "<null>" : eClass.getName();
		}
		
		private static String packageName(EClass eClass) {
			EPackage pack = eClass == null ? null : eClass.getEPackage();
			return pack == null ? "" : pack.getName(); 
		
		}
		
		private static String operationOrFeature(Object o) {
			return o instanceof ENamedElement ? ((ENamedElement)o).getName() : String.valueOf(o);
		}
		
		public BasicDiagnostic createDiagnostic() {
			String message;
			if (eClass != null) {
				message = nlsBind(//
						"Error while evaluating expression in {0}::{1}->{2} : {3}", //
						packageName(eClass), // 
						className(eClass), //
						operationOrFeature(object), //
						expression);
			} else if (object != null) {
				message = nlsBind(//
						"Error while evaluating expression in {0} : {1}",
						String.valueOf(object), expression );
			} else {
				message = nlsBind(//
						"Error while evaluating expression : {1}", //
						expression );
			}
			return new BasicDiagnostic(Diagnostic.OK, pluginId, 0, message, null);
		}
		
		private static String nlsBind(String message, Object... args) {
			return NLS.bind(message, args);
		}
	}
	
	
	private static Stack<Context> contexts = new Stack<Context>();
	
	
	private XoclErrorHandler() {
	}

	public static void enterContext(String pluginId, Query query, EClass eClass, EStructuralFeature eFeature) {
		enterContextInternal(pluginId, query, eClass, eFeature);
	}

	public static void enterContext(String pluginId, Query query, EClass eClass, EOperation eOperation) {
		enterContextInternal(pluginId, query, eClass, eOperation);
	}

	public static void enterContext(String pluginId, Query query, EClass eClass, String subject) {
		enterContextInternal(pluginId, query, eClass, subject);
	}

	public static void enterContext(String pluginId, Query query, String subject) {
		enterContextInternal(pluginId, query, null, subject);
	}

	public static void enterContext(String pluginId, Query query) {
		enterContextInternal(pluginId, query, null, null);
	}

	private static void enterContextInternal(String pluginId, Query query, EClass eClass, Object object) {
		contexts.push(new Context(pluginId, query.queryText(), eClass, object));
	}
		
	public static void leaveContext() {
		if (contexts.isEmpty()) {
			XOCLPlugin.log("No matching context found");
			return;
		}
		contexts.pop();
	}

	public static void handleException(Throwable exception) {
		// we will always log the digest first, see #741
		safeLog(buildContextsDigest(exception));

		// now the old way
		BasicDiagnostic last = new BasicDiagnostic(Diagnostic.ERROR,
				contexts.peek().pluginId, 0, exception.getLocalizedMessage(),
				new Object[] { exception });
		Diagnostic traceRoot = appendToContextsChain(last);
		safeLog(BasicDiagnostic.toIStatus(traceRoot));
	}
	
	private static Diagnostic appendToContextsChain(Diagnostic last) {
		Diagnostic traceRoot = null;
		BasicDiagnostic prevFrame = null;
		for (int i = 0; i < contexts.size(); i++) {
			Context next = contexts.get(i);
			BasicDiagnostic nextFrame = next.createDiagnostic();
			if (traceRoot == null) {
				traceRoot = nextFrame;
			} 
			if (prevFrame != null) {
				prevFrame.add(nextFrame);
			} 
			prevFrame = nextFrame;
		}
		
		if (prevFrame == null) {
			assert traceRoot == null;
			traceRoot = last;
		} else {
			prevFrame.add(last);
		}
		return traceRoot;
	}
	
	private static void safeLog(IStatus status) {
		if (status == null) {
			return;
		}
		try {
			XOCLPlugin.log(status);
		} catch (RuntimeException e) {
			//what can we do now
			XOCLPlugin.log(e);
			throw e;
		}
	}
	
	public static IStatus buildContextsDigest(Throwable e) {
		String CRLF = System.getProperty("line.separator");
		StringBuffer result = new StringBuffer("OCL exception digest: ");
		result.append(CRLF);
		for (int idx = 0; idx < contexts.size(); idx++) {
			if (idx > 0) {
				result.append(CRLF);
			}
			result.append("#" + idx + ": " + contexts.get(idx));
		}
		String pluginId = contexts.isEmpty() ? "null" : contexts.peek().pluginId;
		return new Status(IStatus.ERROR, pluginId, result.toString(), /*we will log it later anyway*/ null);
	}
	
	public static void handleQueryProblems(String pluginId, String expression, Diagnostic diagnostic, EClass eClass, EStructuralFeature eFeature) {
		if (diagnostic != null && diagnostic.getSeverity() != Diagnostic.OK) {
			handleQueryProblemsInternal(pluginId, expression, diagnostic, eClass.getEPackage(), eClass, eFeature);
		}
	}

	public static void handleQueryProblems(String pluginId, String expression, Diagnostic diagnostic, EClass eClass, EOperation eOperation) {
		if (diagnostic != null && diagnostic.getSeverity() != Diagnostic.OK) {
			handleQueryProblemsInternal(pluginId, expression, diagnostic, eClass.getEPackage(), eClass, eOperation);
		}
	}

	public static void handleQueryProblems(String pluginId, String expression, Diagnostic diagnostic, EClass eClass, String subject) {
		if (diagnostic != null && diagnostic.getSeverity() != Diagnostic.OK) {
			handleQueryProblemsInternal(pluginId, expression, diagnostic, eClass.getEPackage(), eClass, subject);
		}
	}

	public static void handleQueryProblems(String pluginId, String expression, Diagnostic diagnostic, EPackage ePackage, String subject) {
		if (diagnostic != null && diagnostic.getSeverity() != Diagnostic.OK) {
			handleQueryProblemsInternal(pluginId, expression, diagnostic, ePackage, null, subject);
		}
	}

	private static void handleQueryProblemsInternal(String pluginId, String expression, Diagnostic diagnostic, EPackage ePackage, EClass eClass, Object object) {
		String message;
		if (object instanceof ETypedElement) {
			message = MessageFormat.format("OCL issue in {0}::{1}->{2} : {3}", new Object[] {ePackage != null ? ePackage.getName() : "", eClass.getName(), ((ETypedElement)object).getName(), expression});
		} else {
			message = MessageFormat.format("OCL issue in {0}::{1}->{2} : {3}", new Object[] {ePackage != null ? ePackage.getName() : "", eClass != null ? eClass.getName() : "", object.toString(), expression});						
		}
		
		BasicDiagnostic chain = new BasicDiagnostic(Diagnostic.OK, pluginId, 0,
				message, new Object[] {ePackage, eClass, object});
		if (diagnostic != null) {
			chain.add(diagnostic);	
		}
		
		safeLog(BasicDiagnostic.toIStatus(appendToContextsChain(chain)));
	}

}
 