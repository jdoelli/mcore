package org.xocl.core.util;

public interface IXoclInitializable {
	/**
	 * This method is called (e.g. after creation and initialization of features by Tuple)
	 * indicating that the object can still be initialized by its own init annotations.
	 * If the method is called current object features are considered non-initialized. 
	 */
	public void allowInitialization();

	/**
	 * This method triggers the class initialization with ocl init-family annotations.
	 * Classes having init annotations must implement this interface to let the external code
	 * trigger the initialization event.
	 */
	public void ensureClassInitialized(boolean isLoadInProgress);
}