package org.xocl.core.util;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.impl.AdapterImpl;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.ocl.AbstractTypeChecker;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.EnvironmentFactory;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.TypeChecker;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.EcoreEnvironment;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.EcoreEvaluationEnvironment;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.ecore.internal.OCLStandardLibraryImpl;
import org.eclipse.ocl.expressions.CollectionKind;
import org.eclipse.ocl.options.ParsingOptions;
import org.eclipse.ocl.types.CollectionType;
import org.eclipse.ocl.types.TupleType;
import org.eclipse.ocl.util.TypeUtil;
import org.eclipse.ocl.utilities.PredefinedType;
import org.eclipse.ocl.utilities.TypedElement;
import org.eclipse.ocl.utilities.UMLReflection;

@SuppressWarnings("restriction")
public class XoclLibrary {
	private static final String SYSTEM_NAME = "SYSTEM"; //$NON-NLS-1$
	private static final EClass SYSTEM_CLASS = createSystemClass();

	private static EClass createSystemClass() {
		EFactory eFactory = EcoreFactory.eINSTANCE.createEFactory();

		EPackage ePackage = EcoreFactory.eINSTANCE.createEPackage();
		ePackage.setName(SYSTEM_NAME);
		ePackage.setNsPrefix(SYSTEM_NAME);
		ePackage.setNsURI(SYSTEM_NAME);
		ePackage.setEFactoryInstance(eFactory);

		EClass eClass = EcoreFactory.eINSTANCE.createEClass();
		eClass.setName(SYSTEM_NAME);
		ePackage.getEClassifiers().add(eClass);

		return eClass;
	}

	private static class XoclEnvironment extends EcoreEnvironment {
		private static final EClass EOBJECT_CLASS = EcorePackage.eINSTANCE.getEObject();

		private final EClassifier STRING_CLASS = getOCLStandardLibrary().getString();
		private final EClassifier INTEGER_CLASS = getOCLStandardLibrary().getInteger();
		private final EClassifier BOOLEAN_CLASS = getOCLStandardLibrary().getBoolean();

		private final EClassifier STRING_SEQUENCE_CLASS = TypeUtil.resolveSequenceType(this, STRING_CLASS);


		private static final long MILLIS_IN_SECOND = 1000;
		private static final long MILLIS_IN_MINUTE = 60 * MILLIS_IN_SECOND;
		private static final long MILLIS_IN_HOUR = 60 * MILLIS_IN_MINUTE;
		private static final long MILLIS_IN_DAY = 24 * MILLIS_IN_HOUR;
		private static final long MILLIS_IN_MONTH = 30 * MILLIS_IN_DAY;
		private static final long MILLIS_IN_YEAR = 365 * MILLIS_IN_DAY;

		public XoclEnvironment(EPackage.Registry registry) {
			super(registry);
			ParsingOptions.setOption(this, ParsingOptions.implicitRootClass(this), EOBJECT_CLASS);			
			defineCustomOperations();
		}

		public XoclEnvironment(XoclEnvironment parent) {
			super(parent);
			ParsingOptions.setOption(this, ParsingOptions.implicitRootClass(this), EOBJECT_CLASS);			
		}

		@Override
		protected TypeChecker<EClassifier, EOperation, EStructuralFeature> createTypeChecker() {
			return new AbstractTypeChecker<EClassifier, EOperation, EStructuralFeature, EParameter>(this) {

				@Override
				protected EClassifier resolve(EClassifier type) {
					return getTypeResolver().resolve(type);
				}

				@Override
				protected CollectionType<EClassifier, EOperation> resolveCollectionType(
						CollectionKind kind, EClassifier elementType) {

					return getTypeResolver().resolveCollectionType(kind, elementType);
				}

				@Override
				protected TupleType<EOperation, EStructuralFeature> resolveTupleType(
						EList<? extends TypedElement<EClassifier>> parts) {
					return getTypeResolver().resolveTupleType(parts);
				}

				@Override
				public List<EOperation> getOperations(EClassifier owner) {
					if (!(owner instanceof PredefinedType<?>)
							&& !(getUMLReflection().asOCLType(owner) instanceof PredefinedType<?>)) {
						defineCustomOperation(owner, "deepCopy", //$NON-NLS-1$
								owner, null,
								new ExecutionAdapter() {
							@Override
							public Object run(Object source, Object[] args) {
								return EcoreUtil.copy((EObject) source);
							}
						});
						defineCustomOperation(owner, "deepEquals", //$NON-NLS-1$
								BOOLEAN_CLASS, new EParameter[] {newParameter("anObject", EOBJECT_CLASS)}, //$NON-NLS-1$
								new ExecutionAdapter() {
							@Override
							public Object run(Object source, Object[] args) {
								return EcoreUtil.equals((EObject) source, (EObject) args[0]);
							}
						});
						if (owner instanceof EClass) {
							EClass eClass = (EClass) owner;
							EPackage ePackage = eClass.getEPackage();
							if (ePackage != null) {
								if (EcorePackage.eNS_URI.equals(ePackage.getNsURI())) {
									String eStructuralFeatureClassName = EcorePackage.eINSTANCE.getEStructuralFeature().getName();
									EClassifier eStructuralFeatureClass = ePackage.getEClassifier(eStructuralFeatureClassName);
									if ((TypeUtil.getRelationship(getEnvironment(), eClass, eStructuralFeatureClass) & UMLReflection.SUBTYPE) != 0) {
										String eClassClassName = EcorePackage.eINSTANCE.getEClass().getName();
										EClassifier eClassClass = ePackage.getEClassifier(eClassClassName);
										defineCustomOperation(owner, "findAnnotationText", //$NON-NLS-1$
												STRING_CLASS, new EParameter[] {
												newParameter("eClass", eClassClass), //$NON-NLS-1$
												newParameter("keyPattern", STRING_CLASS), //$NON-NLS-1$
												newParameter("overrideKeyPattern", STRING_CLASS), //$NON-NLS-1$
												newParameter("annotationSource", STRING_CLASS), //$NON-NLS-1$
												newParameter("overrideAnnotationSource", STRING_CLASS), //$NON-NLS-1$
										},
										new ExecutionAdapter() {
											@Override
											public Object run(Object source, Object[] args) {
												return XoclEmfUtil.findAnnotationText((EStructuralFeature) source, (EClass) args[0], (String) args[1], (String) args[2], (String) args[3], (String) args[4]);
											}
										});
									}
								}
							}
						}
					}

					return super.getOperations(owner);
				}
			};
		}

		@Override
		protected void setFactory(EnvironmentFactory<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction, Constraint, EClass, EObject> factory) {
			super.setFactory(factory);
		}

		private void defineCustomOperations() {
			defineCamelCaseOperations();

			defineToStringOperations();

			addEnvironmentVariable(SYSTEM_NAME, SYSTEM_CLASS);

			defineDateOperations();

			defineCustomOperation(STRING_CLASS, "toSequenceOfLines", //$NON-NLS-1$
					STRING_SEQUENCE_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.toLineList((String) source);
				}
			});
			defineCustomOperation(getOCLStandardLibrary().getOclAny(), "getUUID", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					if (source instanceof EObject) {
						EObject eo = (EObject)source;
						Resource r = eo.eResource();
						return r == null ? "" : r.getURIFragment(eo);
					}
					return "";
				}
			});
		}

		private void defineCamelCaseOperations() {
			defineCustomOperation(STRING_CLASS, "camelCaseUpper", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.camelCaseUpper((String) source);
				}
			});

			defineCustomOperation(STRING_CLASS, "camelCaseLower", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					if (source instanceof String) {
						return XoclHelper.camelCaseLower((String) source);
					} else {
						return null;
					}
				}
			});

			defineCustomOperation(STRING_CLASS, "allLowerCase", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.allLowerCase((String) source);
				}
			});

			defineCustomOperation(STRING_CLASS, "firstUpperCase", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.firstUpperCase((String) source);
				}
			});

			defineCustomOperation(STRING_CLASS, "camelCaseToBusiness", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.camelCaseToBusiness((String) source);
				}
			});

			defineCustomOperation(STRING_CLASS, "matches", //$NON-NLS-1$
					BOOLEAN_CLASS,  new EParameter[] {newParameter("regex", STRING_CLASS)}, //$NON-NLS-1$
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return ((String) source).matches((String) args[0]);
				}
			});
		}

		private void defineToStringOperations() {
			defineCustomOperation(getOCLStandardLibrary().getOclAny(), "toString", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return source.toString();
				}
			});

			defineCustomOperation(INTEGER_CLASS, "toString", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return source.toString();
				}
			});

			defineCustomOperation(BOOLEAN_CLASS, "toString", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return source.toString();
				}
			});
		}

		private void defineDateOperations() {
			defineCustomOperation(SYSTEM_CLASS, "timestamp", //$NON-NLS-1$
					STRING_CLASS, null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return XoclHelper.timestamp();
				}
			});
			defineCustomOperation(SYSTEM_CLASS, "currentTime", //$NON-NLS-1$ 
					EcorePackage.Literals.EDATE, 
					null, 
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return new Date();
				}
			});
			defineCustomOperation(SYSTEM_CLASS, "currentTime", EcorePackage.Literals.EDATE, null, new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					return Calendar.getInstance().getTime();
				}
			});

			defineDateDiffOperation("diffInSeconds", MILLIS_IN_SECOND); //$NON-NLS-1$
			defineDateDiffOperation("diffInMinutes", MILLIS_IN_MINUTE); //$NON-NLS-1$
			defineDateDiffOperation("diffInHours", MILLIS_IN_HOUR); //$NON-NLS-1$
			defineDateDiffOperation("diffInDays", MILLIS_IN_DAY); //$NON-NLS-1$
			defineDateDiffOperation("diffInMonths", MILLIS_IN_MONTH); //$NON-NLS-1$
			defineDateDiffOperation("diffInYears", MILLIS_IN_YEAR); //$NON-NLS-1$

			defineDateGetFieldOperation("year", Calendar.YEAR); //$NON-NLS-1$
			defineDateGetFieldOperation("month", Calendar.MONTH); //$NON-NLS-1$
			defineDateGetFieldOperation("day", Calendar.DAY_OF_MONTH); //$NON-NLS-1$
			defineDateGetFieldOperation("hour", Calendar.HOUR_OF_DAY); //$NON-NLS-1$
			defineDateGetFieldOperation("minute", Calendar.MINUTE); //$NON-NLS-1$
			defineDateGetFieldOperation("second", Calendar.SECOND); //$NON-NLS-1$

			defineDateToFixedStringFormatOperation("toDdMmYyyy", "dd.MM.yyyy"); //$NON-NLS-1$ //$NON-NLS-2$
			defineDateToFixedStringFormatOperation("toYyyyMmDd", "yyyy.MM.dd"); //$NON-NLS-1$ //$NON-NLS-2$
			defineDateToFixedStringFormatOperation("toHhMm", "HH:mm"); //$NON-NLS-1$ //$NON-NLS-2$

			defineCustomOperation(EcorePackage.Literals.EDATE, "toString", //$NON-NLS-1$
					STRING_CLASS, 
					new EParameter[] {
					newParameter("pattern", STRING_CLASS), //$NON-NLS-1$
			},
			new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					SimpleDateFormat dateFormat = new SimpleDateFormat((String) args[0]);
					return dateFormat.format((Date) source);
				}
			});
		}

		private void defineDateDiffOperation(String name, final long millisInUnit) {
			defineCustomOperation(EcorePackage.Literals.EDATE, name,
					INTEGER_CLASS, 
					new EParameter[] {
					newParameter("eClass", EcorePackage.Literals.EDATE), //$NON-NLS-1$
			},
			new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					Date srcDate = (Date) source;
					Date argDate = (Date) args[0];
					long delta = srcDate.getTime() - argDate.getTime();
					long diffInUnits = delta / millisInUnit;
					return new Integer((int) diffInUnits);
				}
			});
		}

		private void defineDateGetFieldOperation(String name, final int fieldID) {
			defineCustomOperation(EcorePackage.Literals.EDATE, name,
					INTEGER_CLASS, 
					null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					Date date = (Date) source;
					GregorianCalendar gregorianCalendar = new GregorianCalendar();
					gregorianCalendar.setTime(date);
					return gregorianCalendar.get(fieldID);
				}
			});
		}

		private void defineDateToFixedStringFormatOperation(String name, final String pattern) {
			defineCustomOperation(EcorePackage.Literals.EDATE, name,
					STRING_CLASS, 
					null,
					new ExecutionAdapter() {
				@Override
				public Object run(Object source, Object[] args) {
					SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
					return dateFormat.format((Date) source);
				}
			});
		}

		private void addEnvironmentVariable(String variableName, EClassifier variableType) {
			Variable variable = org.eclipse.ocl.ecore.EcoreFactory.eINSTANCE.createVariable();
			variable.setName(variableName);
			variable.setType(variableType);
			addElement(variableName, variable, true);
		}		

		private EOperation defineCustomOperation(EClassifier context, String name, EClassifier type,	
				EParameter[] parameters, ExecutionAdapter executionAdapter) {
			EOperation eOperation = EcoreFactory.eINSTANCE.createEOperation();
			eOperation.setName(name);
			eOperation.setEType(type);

			if (parameters != null) {
				for (EParameter parameter : parameters) {
					eOperation.getEParameters().add(parameter);
				}
			}

			eOperation.eAdapters().add(executionAdapter);

			addHelperOperation(context, eOperation);
			return eOperation;
		}

		private EParameter newParameter(String name, EClassifier type) {
			EParameter parameter = EcoreFactory.eINSTANCE.createEParameter();
			parameter.setName(name);
			parameter.setEType(type);
			return parameter;
		}
	}

	private abstract static class ExecutionAdapter extends AdapterImpl {
		public abstract Object run(Object source, Object[] args);
	}

	private static class XoclEvaluationEnvironment extends EcoreEvaluationEnvironment {
		public XoclEvaluationEnvironment() {
			super();
			init();
		}

		public XoclEvaluationEnvironment(EvaluationEnvironment<EClassifier, EOperation, EStructuralFeature, EClass, EObject> parent) {
			super(parent);
			init();
		}

		private void init() {
			add(SYSTEM_NAME, SYSTEM_CLASS.getEPackage().getEFactoryInstance().create(SYSTEM_CLASS));
		}

		public Object callOperation(EOperation operation, int opcode, Object source, Object[] args) {
			EOperation actualOperation = resolveActualOperation(source, operation);

			for (Adapter adapter : actualOperation.eAdapters()) {
				if (adapter instanceof ExecutionAdapter) {
					ExecutionAdapter executionAdapter = (ExecutionAdapter) adapter;
					if ((source == null) || 
							(source == OCLStandardLibraryImpl.INSTANCE.getOclInvalid())) {
						return OCLStandardLibraryImpl.INSTANCE.getOclInvalid();
					}
					return executionAdapter.run(source, args);
				}
			}

			EAnnotation a = actualOperation.getEContainingClass().getEPackage().getEAnnotation("http://www.eclipse.org/emf/2002/Ecore");
			if (a != null && a.getDetails().containsKey("invocationDelegates")) {
				EList<Object> arguments = (args.length == 0) ? ECollections.emptyEList() : new BasicEList.UnmodifiableEList<Object>(args.length, args);
				try {
					Object result = ((EOperation.Internal) operation).getInvocationDelegate().dynamicInvoke((InternalEObject) source, arguments);
					return coerceValue(operation, result, true);
				} catch (InvocationTargetException e) {
					e.printStackTrace();
					return null;
				}
			} else {
				return super.callOperation(actualOperation, opcode, source, args);
			}
		}

		private EOperation resolveActualOperation(Object source, EOperation operation) {
			if (source instanceof EObject) {
				EObject eObject = (EObject) source;
				EClass eClass = eObject.eClass();
				EOperation overridingOperation = findOverridingOperation(eClass, operation.getName(), operation.getEParameters());
				if (overridingOperation != null) {
					return overridingOperation;
				}
			}
			return operation;
		}

		private EOperation findOverridingOperation(EClass eClass, String name, EList<EParameter> eParameters) {
			// EMF algorithm to find the best matching operation to the eClass
			EList<EClass> eAllTypes = new UniqueEList<EClass>(eClass.getEAllSuperTypes());
			eAllTypes.add(eClass);
			ECollections.reverse(eAllTypes);
			for (EClass currentClass : eAllTypes) {
				EOperation overridingOperation = findImmediateOverridingOperation(currentClass, name, eParameters);
				if (overridingOperation != null) {
					return overridingOperation;
				}
			}
			return null;					
		}

		private EOperation findImmediateOverridingOperation(EClass eClass, String name, EList<EParameter> eParameters) {
			EList<EOperation> eOperations = eClass.getEOperations();
			operationLoop : for (EOperation eOperation : eOperations) {
				if ((name != null) && name.equals(eOperation.getName()) 
						&& (eParameters.size() == eOperation.getEParameters().size())) {
					for (int i = 0, n = eParameters.size(); i < n; i++) {
						EParameter desiredParameter = eParameters.get(i);
						EParameter actualParameter = eOperation.getEParameters().get(i);
						if ((desiredParameter == null) || (desiredParameter.getEType() == null) || (actualParameter == null)
								|| !desiredParameter.getEType().equals(actualParameter.getEType())) {
							continue operationLoop;
						}
					}
					return eOperation;
				}
			}
			return null;
		}
	}

	public static class XoclEnvironmentFactory extends EcoreEnvironmentFactory {
		public XoclEnvironmentFactory() {
			super();
		}

		public XoclEnvironmentFactory(Registry reg) {
			super(reg);
		}

		public Environment<EPackage, EClassifier, EOperation, EStructuralFeature,
		EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction,
		Constraint, EClass, EObject> createEnvironment() {

			XoclEnvironment result = new XoclEnvironment(getEPackageRegistry());
			result.setFactory(this);
			return result;
		}

		public Environment<EPackage, EClassifier, EOperation, EStructuralFeature,
		EEnumLiteral, EParameter, EObject, CallOperationAction, SendSignalAction,
		Constraint, EClass, EObject> createEnvironment(Environment<EPackage, EClassifier,
				EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject,
				CallOperationAction, SendSignalAction, Constraint, EClass, EObject> parent) {

			XoclEnvironment result = new XoclEnvironment((XoclEnvironment) parent);
			result.setFactory(this);
			return result;
		}

		public EvaluationEnvironment<EClassifier, EOperation, EStructuralFeature, EClass, EObject>
		createEvaluationEnvironment() {

			return new XoclEvaluationEnvironment();
		}

		public EvaluationEnvironment<EClassifier, EOperation, EStructuralFeature, EClass, EObject>
		createEvaluationEnvironment(EvaluationEnvironment<EClassifier, EOperation,
				EStructuralFeature, EClass, EObject> parent) {
			return new XoclEvaluationEnvironment(parent);
		}
	}
}