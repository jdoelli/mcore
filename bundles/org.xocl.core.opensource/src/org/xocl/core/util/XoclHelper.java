/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.core.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnumLiteral;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.ocl.Environment;
import org.eclipse.ocl.ecore.BagType;
import org.eclipse.ocl.ecore.CallOperationAction;
import org.eclipse.ocl.ecore.CollectionType;
import org.eclipse.ocl.ecore.Constraint;
import org.eclipse.ocl.ecore.OrderedSetType;
import org.eclipse.ocl.ecore.SendSignalAction;
import org.eclipse.ocl.ecore.SequenceType;
import org.eclipse.ocl.ecore.SetType;
import org.eclipse.ocl.util.TypeUtil;
import org.eclipse.ocl.utilities.UMLReflection;

/**
 * @author Max Stepanov
 *
 */
/**
 * @author Marc Moser
 *
 */
public final class XoclHelper {

	private XoclHelper() {
	}
	static final SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
	
	/**
	 * Format OCL result object
	 * @param object
	 * @return
	 */
	public static String format(Object object) {
		if (object instanceof String) {
			return (String) object;
		} else if (object instanceof Collection<?>) {
			StringBuffer sb = new StringBuffer();
			for(Iterator<?> i = ((Collection<?>)object).iterator(); i.hasNext(); ) {
				sb.append(format(i.next()));
			}
			return sb.toString();
		}
		if (object != null) {
			return object.toString();
		}
		return "";
	}
	
	/**
	 * Format OCL result object
	 * @param object
	 * @return
	 */
	public static String dateToString(Date date) {
		if(date == null){return "DATE_MISSING";};
		return df.format(date);
	}
	
	/**
	 * Check if two EClassifiers represent the same Java type
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean isSameType(EClassifier left, EClassifier right, 
			Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject,
			CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment) {
		if (left == right) {
			return true;
		} else if ((left instanceof EDataType && right instanceof EDataType) 
				&& (left.getInstanceClass() == right.getInstanceClass())) {
			return true;
		}	

		return (TypeUtil.getRelationship(environment, left, right) & UMLReflection.SAME_TYPE) != 0; 
	}
	
	/**
	 * Check if one EClassifier is a subtype of another
	 * 
	 * @param left
	 * @param right
	 * @return
	 */
	public static boolean isFirstSubTypeOfSecond(EClassifier first, EClassifier second) {

		if (first instanceof EClass && second instanceof EClass) {
			return ((EClass)second).isSuperTypeOf((EClass)first);
		}

		return false;
	}

	/**
	 * Check if an EStructuralFeature whose type is an OCL collection is equivalent to a regular EStructuralFeature. 
	 * @param oclStructuralFeature
	 * @param resultStructuralFeature
	 * @return
	 */
	public static boolean isCollectionEquivalent(
			EStructuralFeature oclStructuralFeature,
			EStructuralFeature resultStructuralFeature, 
			Environment<EPackage, EClassifier, EOperation, EStructuralFeature, EEnumLiteral, EParameter, EObject,
			CallOperationAction, SendSignalAction, Constraint, EClass, EObject> environment) {
		
		if (oclStructuralFeature == null || resultStructuralFeature == null)
			return true;
		
		EClassifier oclType = oclStructuralFeature.getEType();
		
		//check multiplicity
		if (!resultStructuralFeature.isMany())
			return false;
		
		//check collection element type
		if (oclType instanceof CollectionType) {
			EClassifier oclCollectionElementType = ((CollectionType) oclType).getElementType();
			if (oclCollectionElementType instanceof EClass && resultStructuralFeature.getEType() instanceof EClass) {
				if (!((EClass)resultStructuralFeature.getEType()).isSuperTypeOf(((EClass)oclCollectionElementType)))
					return false;
			} else {
				if (!isSameType(oclCollectionElementType, resultStructuralFeature.getEType(), environment))
					return false;
			}
		}	
		
		//check collection type
		if (oclType instanceof BagType)
			return !resultStructuralFeature.isOrdered() && !resultStructuralFeature.isUnique();
		if (oclType instanceof SetType)
			return !resultStructuralFeature.isOrdered() && resultStructuralFeature.isUnique();
		if (oclType instanceof SequenceType)
			return resultStructuralFeature.isOrdered() && !resultStructuralFeature.isUnique();
		if (oclType instanceof OrderedSetType)
			return resultStructuralFeature.isOrdered() && resultStructuralFeature.isUnique();
		
		return false;
	}
	
	public static final String camelCaseToBusiness(String input) {
		if (input == null)
			return input;
		
		// First to Upper
		if (input.length() == 1)
			input = input.toUpperCase();
		else
			input = input.substring(0, 1).toUpperCase().concat(input.substring(1));
		
		// Put a space if match one of the case
		String result = input.replaceAll("\\s", "").replaceAll(
				String.format("%s|%s|%s",
						"(?<=[A-Z])(?=[A-Z][a-z])", // case 1: Upper letter before, Upper follow by lower after
						"(?<=[^A-Z])(?=[A-Z])", // case 2: Not Upper before follow by letter
						"(?<=[A-Za-z])(?=[^A-Za-z])"
						), // case 3: Upper or lower before, follow by not letter 
						" "
				);
		
		return result;
	}

	public static final String camelCaseUpper(String inputString) {
		return toCamelCaseName(inputString, true, false, false);
	}
	
	public static final String camelCaseLower(String inputString) {
		return toCamelCaseName(inputString, false, false, false);
	}
	
	public static final String allLowerCase(String inputString) {
		return toCamelCaseName(inputString, false, true, false);
	}

	public static final String firstUpperCase(String inputString) {
		return toCamelCaseName(inputString, true, true, false);
	}

	private static String toCamelCaseName(String input, boolean isFirstUpper, boolean isAll, boolean isAllUpper) {
		if (input.length() == 0) {
			return input;
		}
		StringBuilder output = new StringBuilder();
		char startingChar = input.charAt(0);
		output.append(isFirstUpper ? Character.toUpperCase(startingChar) : Character.toLowerCase(startingChar));
		if (input.length() == 1) {
			return output.toString();
		}
		for (int i = 1; i < input.length(); i++) {
			final char iChar = input.charAt(i);
			if (Character.isSpaceChar(iChar)) {
				;
			} else {
				char ch = iChar;
				if (isAll) {
					ch = isAllUpper ? Character.toUpperCase(iChar) : Character.toLowerCase(iChar);
				}
				output.append(ch);
			}
		}
		return output.toString();
	}
	
	public static final String timestamp() {
		Calendar calendar = Calendar.getInstance();
		
		StringBuilder timeStringBuilder = new StringBuilder();
		timeStringBuilder.append(calendar.get(Calendar.YEAR));
		padValue(timeStringBuilder, calendar.get(Calendar.MONTH)+1, 1);
		padValue(timeStringBuilder, calendar.get(Calendar.DAY_OF_MONTH), 1);
		padValue(timeStringBuilder, calendar.get(Calendar.HOUR_OF_DAY), 1);
		padValue(timeStringBuilder, calendar.get(Calendar.MINUTE), 1);
		padValue(timeStringBuilder, calendar.get(Calendar.SECOND), 1);
		padValue(timeStringBuilder, calendar.get(Calendar.MILLISECOND), 3);
		
		return timeStringBuilder.toString();
	}
	
	public static final List<String> toLineList(String text) {
		String[] lines = text.split("\r\n|\r|\n"); //$NON-NLS-1$
		List<String> result = new ArrayList<String>(lines.length);
		for (String line : lines) {
			result.add(line);
		}
		return result;
	}

	private static final void padValue(StringBuilder builder, int value, int power) {
		for (int i = 0; i< power+1; i++) {
			if (value < java.lang.Math.pow(10, i)) {
				builder.append(0);
			}
		}
		builder.append(value);
	}
}