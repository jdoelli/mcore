package org.xocl.core.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.codegen.ecore.genmodel.GenClass;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenOperation;
import org.eclipse.emf.codegen.ecore.genmodel.GenParameter;
import org.eclipse.emf.codegen.ecore.genmodel.impl.GenClassImpl;
import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.UniqueEList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EParameter;
import org.eclipse.emf.ecore.EStructuralFeature;

public class XoclAnnotationHelper {
	public static final String oclNsURI = XoclEmfUtil.OCL_ANNOTATION_SOURCE;
	public static final String overrideOclNsURI = XoclEmfUtil.OVERRIDE_OCL_ANNOTATION_SOURCE;
	public static final String namedOclNsURI = "http://www.xocl.org/NAMED_OCL";
	public static final String expressionOclNsURI = "http://www.xocl.org/EXPRESSION_OCL";

	private GenClass myGenClass;
	private Set<String> myDirectInitializedFeatures;
	private Set<String> myAllInitializedFeatures;
	private boolean isDirectInitializable = false;

	public XoclAnnotationHelper(GenClass genClass) {
		myGenClass = genClass;
		myAllInitializedFeatures = computeAllInitializedFeatures();
		isDirectInitializable = !myAllInitializedFeatures.isEmpty();
		myDirectInitializedFeatures = new LinkedHashSet<String>(
				myAllInitializedFeatures);
		GenClass baseGenClass = genClass.getClassExtendsGenClass();
		if (baseGenClass != null) {
			XoclAnnotationHelper baseOclAnnotationsHelper = new XoclAnnotationHelper(
					baseGenClass);
			Set<String> baseClassAllInitializedFeatures = baseOclAnnotationsHelper
					.getAllInitializedFeatures();
			myDirectInitializedFeatures
					.removeAll(baseClassAllInitializedFeatures);
			isDirectInitializable &= baseClassAllInitializedFeatures.isEmpty();
		}
	}

	public boolean isDirectInitializable() {
		return isDirectInitializable;
	}

	public boolean isInitializable() {
		return !myAllInitializedFeatures.isEmpty();
	}

	public Set<String> getDirectInitializedFeatures() {
		return myDirectInitializedFeatures;
	}

	public Set<String> getAllInitializedFeatures() {
		return myAllInitializedFeatures;
	}

	public Set<String> computeAllInitializedFeatures() {
		Set<String> inititalizedFeatureSet = new LinkedHashSet<String>();
		for (GenFeature genFeature : myGenClass.getAllGenFeatures()) {
			EAnnotation annotation = genFeature.getEcoreFeature()
					.getEAnnotation(oclNsURI);
			if ((annotation != null)
					&& (annotation.getDetails().get("initValue") != null)) {
				inititalizedFeatureSet.add(genFeature
						.getQualifiedFeatureAccessor());
			}
		}
		List<EClass> eClasses = new ArrayList<EClass>();
		eClasses.addAll(myGenClass.getEcoreClass().getEAllSuperTypes());
		eClasses.add(myGenClass.getEcoreClass());
		for (EClass eClass : eClasses) {
			EAnnotation annotation = eClass.getEAnnotation(overrideOclNsURI);
			if (annotation != null) {
				EMap<String, String> details = annotation.getDetails();
				for (Map.Entry<String, String> entry : details) {
					String detailKey = entry.getKey();
					if (detailKey.endsWith("InitValue")) {
						String featureName = detailKey.substring(0,
								detailKey.length() - "InitValue".length());
						for (GenFeature genFeature : myGenClass
								.getAllGenFeatures()) {
							if (genFeature.getName().equals(featureName)) {
								inititalizedFeatureSet.add(genFeature
										.getQualifiedFeatureAccessor());
								break;
							}
						}
					}
				}
			}
		}
		return inititalizedFeatureSet;
	}

	public String getOperationBodyVariable(GenOperation genOperation) {
		String varName = genOperation.getName();
		for (GenParameter genParameter : genOperation.getGenParameters()) {
			EParameter eParameter = genParameter.getEcoreParameter();
			EClassifier eClassifier = eParameter.getEType();
			EPackage ePackage = eClassifier.getEPackage();
			String argumentContribution = ePackage.getName()
					+ eClassifier.getName();
			varName += argumentContribution;
		}
		return varName + "BodyOCL";
	}

	public boolean hasDeriveAnnotation(GenFeature genFeature) {
		EAnnotation annotation = genFeature.getEcoreFeature().getEAnnotation(
				oclNsURI);
		return (annotation != null)
				&& (annotation.getDetails().get("derive") != null);
	}

	public List<GenOperation> getImplementedGenOperations(GenClass genClass) {
		EList<GenClass> implementedGenClasses = new UniqueEList<GenClass>(
				genClass.getImplementedGenClasses());
		ECollections.reverse(implementedGenClasses);
		if (((GenClassImpl) genClass).needsRootImplementsInterfaceOperations()) {
			GenClass rootImplementsInterface = genClass.getGenModel()
					.getRootImplementsInterfaceGenClass();
			if (rootImplementsInterface != null) {
				List<GenClass> allBaseClasses = new UniqueEList<GenClass>(
						rootImplementsInterface.getAllBaseGenClasses());
				for (Iterator<GenClass> i = allBaseClasses.iterator(); i
						.hasNext();) {
					GenClass baseClass = i.next();
					if (baseClass.isEObject()) {
						i.remove();
					}
				}
				allBaseClasses.add(rootImplementsInterface);
				implementedGenClasses.addAll(allBaseClasses);
			}
		}
		return collectGenOperations(genClass, implementedGenClasses, null);
	}

	protected boolean isAcceptedByCollidingGenOperationFilter(
			GenClass genClass, GenOperation genOperation) {
		EOperation eOperation = genOperation.getEcoreOperation();
		EAnnotation eAnnotation = eOperation.getEAnnotation(oclNsURI);
		if ((eAnnotation != null)
				&& (eAnnotation.getDetails().get("body") != null)) {
			return true;
		}
		return ((GenClassImpl) genClass).new CollidingGenOperationFilter()
				.accept(genOperation);
	}

	protected List<GenOperation> collectGenOperations(GenClass context,
			List<GenClass> genClasses, List<GenOperation> genOperations) {
		List<GenOperation> result = new ArrayList<GenOperation>();

		if (genClasses != null) {
			for (GenClass genClass : genClasses) {

				LOOP: for (GenOperation genOperation : genClass
						.getGenOperations()) {
					if (isAcceptedByCollidingGenOperationFilter(context,
							genOperation)) {
						for (GenOperation otherGenOperation : result) {
							if (otherGenOperation.isOverrideOf(context,
									genOperation)) {
								continue LOOP;
							}
						}
						result.add(genOperation);
					}
				}
			}
		}

		if (genOperations != null) {

			LOOP: for (GenOperation genOperation : genOperations) {
				if (isAcceptedByCollidingGenOperationFilter(context,
						genOperation)) {
					for (GenOperation otherGenOperation : result) {
						if (otherGenOperation.isOverrideOf(context,
								genOperation)) {
							continue LOOP;
						}
					}
					result.add(genOperation);
				}
			}
		}

		return result;
	}

	public boolean isDiamondJunction(GenClass genClass) {
		Set<EClass> superTypes = new HashSet<EClass>();
		EList<EClass> eSuperTypes = genClass.getEcoreClass().getESuperTypes();
		if (eSuperTypes.size() <= 1) {
			return false;
		}
		for (EClass eSuperType : eSuperTypes) {
			int oldSuperTypesSize = superTypes.size();
			EList<EClass> addedSuperTypes = eSuperType.getEAllSuperTypes();
			superTypes.addAll(addedSuperTypes);
			superTypes.add(eSuperType);
			if (superTypes.size() != oldSuperTypesSize + addedSuperTypes.size()
					+ 1) {
				// Duplication in super types
				return true;
			}
		}
		return false;
	}

	public EList<GenFeature> getAdditionalXoclDerivedGenFeatures(
			GenClass genClass) {
		return getAdditionalXoclAnnotatedGenFeatures(genClass, "derive",
				"Derive");
	}

	public EList<GenFeature> getAdditionalXoclChoiceConstraintGenFeatures(
			GenClass genClass) {
		return getAdditionalXoclAnnotatedGenFeatures(genClass,
				"choiceConstraint", "ChoiceConstraint");
	}

	public EList<GenFeature> getAdditionalXoclChoiceConstructionGenFeatures(
			GenClass genClass) {
		return getAdditionalXoclAnnotatedGenFeatures(genClass,
				"choiceConstruction", "ChoiceConstruction");
	}

	public EList<GenFeature> getAdditionalXoclAnnotatedGenFeatures(
			GenClass genClass, String keyPattern, String overrideKeyPattern) {
		EList<GenFeature> result = new UniqueEList<GenFeature>();
		EAnnotation eAnnotation = genClass.getEcoreClass().getEAnnotation(
				overrideOclNsURI);
		if (eAnnotation != null) {
			EMap<String, String> details = eAnnotation.getDetails();
			for (Map.Entry<String, String> entry : details) {
				String detailKey = entry.getKey();
				if (detailKey.endsWith(overrideKeyPattern)) {
					String featureName = detailKey.substring(0,
							detailKey.length() - overrideKeyPattern.length());
					for (GenFeature genFeature : genClass.getAllGenFeatures()) {
						if (genFeature.getName().equals(featureName)) {
							result.add(genFeature);
							break;
						}
					}
				}
			}
		}

		if (isDiamondJunction(genClass)) {
			for (GenFeature genFeature : genClass.getAllGenFeatures()) {
				if (genClass.getImplementedGenFeatures().contains(genFeature)) {
					continue;
				}
				String annotation = XoclEmfUtil.findAnnotationText(
						genFeature.getEcoreFeature(), genClass.getEcoreClass(),
						keyPattern, overrideKeyPattern);
				if (annotation != null) {
					String baseAnnotation = XoclEmfUtil.findAnnotationText(
							genFeature.getEcoreFeature(), genClass
									.getBaseGenClass().getEcoreClass(),
							keyPattern, overrideKeyPattern);
					if (!annotation.equals(baseAnnotation)) {
						result.add(genFeature);
					}
				}
			}
		}
		return result;
	}

	public boolean isGenerateUpdateAnnotationCommon(GenClass genClass) {
		GenClass extendedGenClass = genClass.getClassExtendsGenClass();
		if ((extendedGenClass != null)
				&& hasUpdateAnnotations(extendedGenClass.getEcoreClass(), false)) {
			return false;
		}
		return hasImplementedUpdateAnnotations(genClass);
	}

	public boolean hasImplementedUpdateAnnotations(GenClass genClass) {
		GenClass extendedGenClass = genClass.getClassExtendsGenClass();
		if (extendedGenClass == null) {
			return hasUpdateAnnotations(genClass.getEcoreClass(), false);
		}
		if (hasUpdateAnnotations(genClass.getEcoreClass(), true)) {
			return true;
		}
		for (EClass parentEClass : genClass.getEcoreClass().getESuperTypes()) {
			if (!extendedGenClass.getEcoreClass().equals(parentEClass)
					&& hasUpdateAnnotations(parentEClass, false)) {
				return true;
			}
		}
		return false;
	}

	public boolean hasUpdateAnnotations(EClass eClass, boolean isDirect) {
		for (EStructuralFeature eStructuralFeature : eClass
				.getEAllStructuralFeatures()) {
			LinkedHashMap<String, String> featureAnnotations = findUpdateAnnotationParams(
					eStructuralFeature, eClass, isDirect);
			if (!featureAnnotations.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	public LinkedHashMap<String, String> findUpdateAnnotationParams(
			EStructuralFeature eStructuralFeature, EClass eClass,
			boolean isDirect) {
		LinkedHashMap<String, String> featureAnnotations = new LinkedHashMap<String, String>();

		// EMF algorithm to find the best matching annotation to the eClass
		EList<EClass> eAllTypes = isDirect ? new UniqueEList<EClass>()
				: new UniqueEList<EClass>(eClass.getEAllSuperTypes());
		eAllTypes.add(eClass);
		ECollections.reverse(eAllTypes);
		for (EClass currentClass : eAllTypes) {
			findDirectUpdateAnnotationParams(eStructuralFeature, currentClass,
					featureAnnotations);
		}
		return featureAnnotations;
	}

	public void findDirectUpdateAnnotationParams(
			EStructuralFeature eStructuralFeature, EClass eClass,
			LinkedHashMap<String, String> featureAnnotations) {
		String updatedFeatureName = null;
		String annotationText = null;

		// Try to find the annotation in the keyPattern annotations
		if (eClass == eStructuralFeature.getEContainingClass()) {
			EAnnotation eAnnotation = eStructuralFeature
					.getEAnnotation(XoclEmfUtil.OCL_ANNOTATION_SOURCE);
			if (eAnnotation != null) {
				for (Map.Entry<String, String> entry : eAnnotation.getDetails()) {
					String key = entry.getKey();
					if ((key != null) && (key.endsWith("Update"))) {
						updatedFeatureName = key.substring(0, key.length()
								- "Update".length());
						annotationText = entry.getValue();
						if (!featureAnnotations.containsKey(updatedFeatureName)) {
							featureAnnotations.put(updatedFeatureName,
									annotationText);
						}
					}
				}
			}
		}

		if (annotationText == null) {
			// Try to find the annotation in the overriding annotations
			EAnnotation eAnnotation = eClass
					.getEAnnotation(XoclEmfUtil.OVERRIDE_OCL_ANNOTATION_SOURCE);
			if (eAnnotation != null) {
				for (Map.Entry<String, String> entry : eAnnotation.getDetails()) {
					String key = entry.getKey();
					if ((key != null) && (key.endsWith("Update"))) {
						String featureNamesString = key.substring(0,
								key.length() - "Update".length());
						String[] featureNameArray = featureNamesString
								.split("-");
						if ((featureNameArray != null)
								&& (featureNameArray.length == 2)
								&& (featureNameArray[0] != null)
								&& (featureNameArray[0]
										.equals(eStructuralFeature.getName()))) {
							updatedFeatureName = featureNameArray[1];
							annotationText = entry.getValue();
							if (!featureAnnotations
									.containsKey(updatedFeatureName)) {
								featureAnnotations.put(updatedFeatureName,
										annotationText);
							}
						}
					}
				}
			}
		}
	}
}
