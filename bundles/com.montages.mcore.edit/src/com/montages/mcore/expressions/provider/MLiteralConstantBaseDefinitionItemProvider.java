/**
 */
package com.montages.mcore.expressions.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.MLiteralConstantBaseDefinitionImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MLiteralConstantBaseDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MLiteralConstantBaseDefinitionItemProvider
		extends MAbstractBaseDefinitionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralConstantBaseDefinitionItemProvider(
			AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamedConstantPropertyDescriptor(object);
			addConstantLetPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Named Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamedConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Named Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MLiteralConstantBaseDefinition_namedConstant_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralConstantBaseDefinition_namedConstant_feature",
						"_UI_MLiteralConstantBaseDefinition_type"),
				ExpressionsPackage.Literals.MLITERAL_CONSTANT_BASE_DEFINITION__NAMED_CONSTANT,
				true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Constant Let feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstantLetPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant Let feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MLiteralConstantBaseDefinition_constantLet_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralConstantBaseDefinition_constantLet_feature",
						"_UI_MLiteralConstantBaseDefinition_type"),
				ExpressionsPackage.Literals.MLITERAL_CONSTANT_BASE_DEFINITION__CONSTANT_LET,
				false, false, false, null, null, null));
	}

	/**
	 * This returns MLiteralConstantBaseDefinition.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MLiteralConstantBaseDefinition"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MLiteralConstantBaseDefinitionImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
