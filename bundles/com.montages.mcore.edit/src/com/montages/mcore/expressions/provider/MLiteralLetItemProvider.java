/**
 */
package com.montages.mcore.expressions.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MLiteral;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.impl.MLiteralLetImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MLiteralLet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MLiteralLetItemProvider extends MConstantLetItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MLiteralLetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addEnumerationTypePropertyDescriptor(object);
			addConstant1PropertyDescriptor(object);
			addConstant2PropertyDescriptor(object);
			addConstant3PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Enumeration Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnumerationTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Enumeration Type feature.
		 * The list of possible choices is constraint by OCL trg.allLiterals()->size() > 0
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MLiteralLet_enumerationType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralLet_enumerationType_feature",
						"_UI_MLiteralLet_type"),
				ExpressionsPackage.Literals.MLITERAL_LET__ENUMERATION_TYPE,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MLiteralLetImpl) object)
							.evalEnumerationTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Constant1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant1 feature.
		 * The list of possible choices is constructed by OCL getAllowedLiterals()
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MLiteralLet_constant1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralLet_constant1_feature",
						"_UI_MLiteralLet_type"),
				ExpressionsPackage.Literals.MLITERAL_LET__CONSTANT1, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MLiteral> result = new ArrayList<MLiteral>();
				Collection<? extends MLiteral> superResult = (Collection<? extends MLiteral>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MLiteralLetImpl) object)
						.evalConstant1ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Constant2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant2 feature.
		 * The list of possible choices is constructed by OCL getAllowedLiterals()
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MLiteralLet_constant2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralLet_constant2_feature",
						"_UI_MLiteralLet_type"),
				ExpressionsPackage.Literals.MLITERAL_LET__CONSTANT2, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MLiteral> result = new ArrayList<MLiteral>();
				Collection<? extends MLiteral> superResult = (Collection<? extends MLiteral>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MLiteralLetImpl) object)
						.evalConstant2ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Constant3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant3 feature.
		 * The list of possible choices is constructed by OCL getAllowedLiterals()
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MLiteralLet_constant3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MLiteralLet_constant3_feature",
						"_UI_MLiteralLet_type"),
				ExpressionsPackage.Literals.MLITERAL_LET__CONSTANT3, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MLiteral> result = new ArrayList<MLiteral>();
				Collection<? extends MLiteral> superResult = (Collection<? extends MLiteral>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MLiteralLetImpl) object)
						.evalConstant3ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This returns MLiteralLet.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MLiteralLet"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MLiteralLetImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
