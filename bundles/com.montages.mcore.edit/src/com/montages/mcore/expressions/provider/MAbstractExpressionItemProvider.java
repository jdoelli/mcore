/**
 */
package com.montages.mcore.expressions.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractExpression;
import com.montages.mcore.provider.MTypedItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MAbstractExpression} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractExpressionItemProvider extends MTypedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpressionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAsCodePropertyDescriptor(object);
			addAsBasicCodePropertyDescriptor(object);
			addCollectorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeContainerBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeNumberBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addScopeSourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalEntireScopePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSelfPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeTrgPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeSimpleTypeConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeLiteralConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeObjectReferenceConstantsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeVariablesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeIteratorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeAccumulatorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeParametersPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeNumberBaseDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalScopeContainerPropertyDescriptor(object);
			}
			addContainingAnnotationPropertyDescriptor(object);
			addContainingExpressionPropertyDescriptor(object);
			addIsComplexExpressionPropertyDescriptor(object);
			addIsSubExpressionPropertyDescriptor(object);
			addCalculatedOwnTypePropertyDescriptor(object);
			addCalculatedOwnMandatoryPropertyDescriptor(object);
			addCalculatedOwnSingularPropertyDescriptor(object);
			addCalculatedOwnSimpleTypePropertyDescriptor(object);
			addSelfObjectTypePropertyDescriptor(object);
			addSelfObjectPackagePropertyDescriptor(object);
			addTargetObjectTypePropertyDescriptor(object);
			addTargetSimpleTypePropertyDescriptor(object);
			addObjectObjectTypePropertyDescriptor(object);
			addExpectedReturnTypePropertyDescriptor(object);
			addExpectedReturnSimpleTypePropertyDescriptor(object);
			addIsReturnValueMandatoryPropertyDescriptor(object);
			addIsReturnValueSingularPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addSrcObjectTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSrcObjectSimpleTypePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_asCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_asCode_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the As Basic Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAsBasicCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the As Basic Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_asBasicCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_asBasicCode_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__AS_BASIC_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Collector feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCollectorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Collector feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_collector_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_collector_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__COLLECTOR,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Entire Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_entireScope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_entireScope_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__ENTIRE_SCOPE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Self feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSelf_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSelf_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SELF,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Trg feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeTrg_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeTrg_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_TRG,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Obj feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeObj_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeObj_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJ,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSimpleTypeConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSimpleTypeConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SIMPLE_TYPE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeLiteralConstantsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeLiteralConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_LITERAL_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeObjectReferenceConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_scopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_OBJECT_REFERENCE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Variables feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeVariables_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeVariables_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_VARIABLES,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Iterator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeIterator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeIterator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ITERATOR,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_ACCUMULATOR,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Parameters feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeParameters_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeParameters_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_PARAMETERS,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Container Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeContainerBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Container Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeContainerBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeContainerBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_CONTAINER_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Number Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeNumberBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Number Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeNumberBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeNumberBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_NUMBER_BASE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Scope Source feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_scopeSource_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_scopeSource_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SCOPE_SOURCE,
				false, false, false, null,
				getString("_UI_BaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Entire Scope feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalEntireScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Entire Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localEntireScope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localEntireScope_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_ENTIRE_SCOPE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeBase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeBase_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_BASE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Self feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSelfPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Self feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeSelf_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSelf_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SELF,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Trg feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeTrgPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Trg feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeTrg_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeTrg_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_TRG,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Obj feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Obj feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeObj_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeObj_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJ,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Source feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Source feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeSource_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSource_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SOURCE,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeSimpleTypeConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Simple Type Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeSimpleTypeConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_SIMPLE_TYPE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Literal Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeLiteralConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Literal Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeLiteralConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeLiteralConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_LITERAL_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeObjectReferenceConstantsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Object Reference Constants feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeObjectReferenceConstants_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_OBJECT_REFERENCE_CONSTANTS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Variables feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeVariablesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Variables feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeVariables_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeVariables_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_VARIABLES,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Iterator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeIteratorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Iterator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_localScopeIterator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeIterator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ITERATOR,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Accumulator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeAccumulatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Accumulator feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeAccumulator_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeAccumulator_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_ACCUMULATOR,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Parameters feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeParametersPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Parameters feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeParameters_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeParameters_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_PARAMETERS,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Number Base Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeNumberBaseDefinitionPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Number Base Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeNumberBaseDefinition_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_NUMBER_BASE_DEFINITION,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Scope Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalScopeContainerPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Scope Container feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_localScopeContainer_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_localScopeContainer_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__LOCAL_SCOPE_CONTAINER,
				false, false, false, null,
				getString("_UI_BaseScopeLocalBaseScopePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_containingAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_containingAnnotation_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_ANNOTATION,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_containingExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_containingExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CONTAINING_EXPRESSION,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Complex Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsComplexExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Complex Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isComplexExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isComplexExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Sub Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsSubExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Sub Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_isSubExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isSubExpression_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Self Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelfObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Self Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_selfObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_selfObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Self Object Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelfObjectPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Self Object Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_selfObjectPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_selfObjectPackage_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SELF_OBJECT_PACKAGE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Target Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_targetObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_targetObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Target Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_targetSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_targetSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Object Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_objectObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_objectObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__OBJECT_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_expectedReturnType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_expectedReturnType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Expected Return Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addExpectedReturnSimpleTypePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Expected Return Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_expectedReturnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_expectedReturnSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isReturnValueMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isReturnValueMandatory_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_isReturnValueSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_isReturnValueSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Src Object Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSrcObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Src Object Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_srcObjectType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_srcObjectType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Src Object Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSrcObjectSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Src Object Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_srcObjectSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_srcObjectSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractExpression_calculatedOwnType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_TYPE,
				false, false, false, null,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnMandatory_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnSingular_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Own Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedOwnSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Own Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractExpression_calculatedOwnSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractExpression_calculatedOwnSimpleType_feature",
						"_UI_MAbstractExpression_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MAbstractExpression) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractExpression.class)) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_CODE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__AS_BASIC_CODE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_COMPLEX_EXPRESSION:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_SUB_EXPRESSION:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_MANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SINGULAR:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__CALCULATED_OWN_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__TARGET_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__EXPECTED_RETURN_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_MANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__IS_RETURN_VALUE_SINGULAR:
		case ExpressionsPackage.MABSTRACT_EXPRESSION__SRC_OBJECT_SIMPLE_TYPE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
