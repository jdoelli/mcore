package com.montages.mcore.expressions.provider.actions;

import static com.montages.mcore.expressions.provider.commands.ReplaceMoveChildCommand.create;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.UnexecutableCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.ui.action.CreateChildAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorPart;

public class ReplaceMoveChildAction extends CreateChildAction {

	public ReplaceMoveChildAction(EditingDomain editingDomain, ISelection selection, Object descriptor) {
		super(editingDomain, selection, descriptor);
	}

	public ReplaceMoveChildAction(IEditorPart activeEditorPart, ISelection selection, Object descriptor) {
		super(activeEditorPart, selection, descriptor);
	}

	@Override
	protected Command createActionCommand(EditingDomain editingDomain, Collection<?> collection) {
		if (collection.size() == 1) {
			Object owner = collection.iterator().next();
			Command command = create(editingDomain, owner, descriptor, collection);
			
			return command;
			
		}		
		return UnexecutableCommand.INSTANCE;
	}	
	
}
