/**
 */
package com.montages.mcore.expressions.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MProcessorDefinition;
import com.montages.mcore.expressions.MSubChain;
import com.montages.mcore.expressions.impl.MSubChainImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.expressions.MSubChain} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MSubChainItemProvider extends MAbstractExpressionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSubChainItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MSubChain) {
			MSubChain typedOwner = ((MSubChain) owner);

			if (feature == ExpressionsPackage.eINSTANCE.getMSubChain_DoAction())
				myUpdate = typedOwner.doAction$Update(
						(com.montages.mcore.expressions.MSubChainAction) value);

		}

		return myUpdate == null
				? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addChainEntryTypePropertyDescriptor(object);
			addChainAsCodePropertyDescriptor(object);
			addElement1PropertyDescriptor(object);
			addElement1CorrectPropertyDescriptor(object);
			addElement2EntryTypePropertyDescriptor(object);
			addElement2PropertyDescriptor(object);
			addElement2CorrectPropertyDescriptor(object);
			addElement3EntryTypePropertyDescriptor(object);
			addElement3PropertyDescriptor(object);
			addElement3CorrectPropertyDescriptor(object);
			addCastTypePropertyDescriptor(object);
			addLastElementPropertyDescriptor(object);
			addChainCalculatedTypePropertyDescriptor(object);
			addChainCalculatedSimpleTypePropertyDescriptor(object);
			addChainCalculatedSingularPropertyDescriptor(object);
			addProcessorPropertyDescriptor(object);
			addProcessorDefinitionPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addPreviousExpressionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addNextExpressionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Chain Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainEntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainEntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainEntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainAsCode_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainAsCode_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_AS_CODE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element1 feature.
		 * The list of possible choices is constructed by OCL 
		let annotatedProp: mcore::MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		if self.chainEntryType.oclIsUndefined() then 
		OrderedSet{} else
		self.chainEntryType.allProperties()
		endif
		
		--
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element1_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MSubChainImpl) object)
						.evalElement1ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element1 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement1CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element1 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element1Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element1Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT1_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element2 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2EntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Element2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		
		if element1.oclIsUndefined() 
		then OrderedSet{}
		else if element1.isOperation
		then OrderedSet{} 
		else if element2EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else element2EntryType.allProperties() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MSubChainImpl) object)
						.evalElement2ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element2 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement2CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element2 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element2Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element2Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT2_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Element3 Entry Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3EntryTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 Entry Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3EntryType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3EntryType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_ENTRY_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Element3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 feature.
		 * The list of possible choices is constructed by OCL let annotatedProp: mcore::MProperty = 
		self.oclAsType(mcore::expressions::MAbstractExpression).containingAnnotation.annotatedElement.oclAsType(mcore::MProperty)
		in
		
		if element2.oclIsUndefined() 
		then OrderedSet{}
		else if element2.isOperation
		then OrderedSet{} 
		else if element3EntryType.oclIsUndefined() 
		  then OrderedSet{}
		  else element3EntryType.allProperties() endif
		 endif
		endif
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3, true,
				false, false, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MSubChainImpl) object)
						.evalElement3ChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Element3 Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addElement3CorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Element3 Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_element3Correct_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_element3Correct_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__ELEMENT3_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Cast Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCastTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Cast Type feature.
		 * The list of possible choices is constraint by OCL trg.kind = mcore::ClassifierKind::ClassType 
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_castType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_castType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CAST_TYPE, true,
				false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MSubChainImpl) object)
							.evalCastTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Last Element feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLastElementPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Last Element feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_lastElement_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_lastElement_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__LAST_ELEMENT,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Processor feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_processor_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_processor_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ProcessorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Processor Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addProcessorDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Processor Definition feature.
		 * The list of possible choices is constructed by OCL 
		let s:SimpleType = self.chainCalculatedSimpleType in
		let t:MClassifier = self.chainCalculatedType in
		let res:OrderedSet(mcore::expressions::MProcessorDefinition) =
		if s = mcore::SimpleType::Boolean
		then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForBoolean()
		         else self.procDefChoicesForBooleans() endif
		else if s = mcore::SimpleType::Integer 
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForInteger()
		         else self.procDefChoicesForIntegers() endif
		else if   s = mcore::SimpleType::Double
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForReal()
		         else self.procDefChoicesForReals() endif
		else if   s = mcore::SimpleType::String
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForString()
		         else self.procDefChoicesForStrings() endif
		else if   s = mcore::SimpleType::Date
		 then if self.chainCalculatedSingular = true
		         then self.procDefChoicesForDate()
		         else self.procDefChoicesForDates() endif
		else if s = mcore::SimpleType::None or 
		  s = mcore::SimpleType::Annotation or 
		  s = mcore::SimpleType::Attribute or 
		  s = mcore::SimpleType::Class or 
		  s = mcore::SimpleType::Classifier or 
		  s = mcore::SimpleType::DataType or 
		  s = mcore::SimpleType::Enumeration or 
		  s = mcore::SimpleType::Feature or 
		  s = mcore::SimpleType::KeyValue or 
		  s = mcore::SimpleType::Literal or 
		  s = mcore::SimpleType::NamedElement or 
		  s = mcore::SimpleType::Object or 
		  s = mcore::SimpleType::Operation or 
		  s = mcore::SimpleType::Package or 
		  s = mcore::SimpleType::Parameter or 
		  s = mcore::SimpleType::Reference or 
		  s = mcore::SimpleType::TypedElement 
		 then if self.chainCalculatedSingular 
		         then self.procDefChoicesForObject()
		         else self.procDefChoicesForObjects()
		                --OrderedSet{Tuple{processor=MProcessor::Head},
		                --                   Tuple{processor=MProcessor::Tail}} 
		                endif
		 else OrderedSet{} endif endif endif endif endif endif
		in res->prepend(null)
		                                   
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_processorDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_processorDefinition_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__PROCESSOR_DEFINITION,
				true, false, false, null,
				getString("_UI_ProcessorDefinitionPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProcessorDefinition> result = new ArrayList<MProcessorDefinition>();
				Collection<? extends MProcessorDefinition> superResult = (Collection<? extends MProcessorDefinition>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MSubChainImpl) object)
						.evalProcessorDefinitionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainCalculatedType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_TYPE,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedSimpleTypePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAbstractChain_chainCalculatedSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedSimpleType_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Chain Calculated Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChainCalculatedSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Chain Calculated Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractChain_chainCalculatedSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractChain_chainCalculatedSingular_feature",
						"_UI_MAbstractChain_type"),
				ExpressionsPackage.Literals.MABSTRACT_CHAIN__CHAIN_CALCULATED_SINGULAR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Previous Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPreviousExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Previous Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MSubChain_previousExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MSubChain_previousExpression_feature",
						"_UI_MSubChain_type"),
				ExpressionsPackage.Literals.MSUB_CHAIN__PREVIOUS_EXPRESSION,
				false, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Next Expression feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNextExpressionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Next Expression feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MSubChain_nextExpression_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MSubChain_nextExpression_feature",
						"_UI_MSubChain_type"),
				ExpressionsPackage.Literals.MSUB_CHAIN__NEXT_EXPRESSION, false,
				false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MSubChain_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MSubChain_doAction_feature", "_UI_MSubChain_type"),
				ExpressionsPackage.Literals.MSUB_CHAIN__DO_ACTION, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns MSubChain.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MSubChain"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MSubChainImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MSubChain.class)) {
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_AS_CODE:
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT1_CORRECT:
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT2_CORRECT:
		case ExpressionsPackage.MSUB_CHAIN__ELEMENT3_CORRECT:
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SIMPLE_TYPE:
		case ExpressionsPackage.MSUB_CHAIN__CHAIN_CALCULATED_SINGULAR:
		case ExpressionsPackage.MSUB_CHAIN__PROCESSOR:
		case ExpressionsPackage.MSUB_CHAIN__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
