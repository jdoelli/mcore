/**
 */
package com.montages.mcore.tables.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ItemProviderAdapter;
import org.eclipse.emf.edit.provider.ViewerNotification;

import com.montages.mcore.provider.McoreEditPlugin;
import com.montages.mcore.tables.MAbstractHeaderRow;
import com.montages.mcore.tables.TablesFactory;
import com.montages.mcore.tables.TablesPackage;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.tables.MAbstractHeaderRow} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractHeaderRowItemProvider extends ItemProviderAdapter
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractHeaderRowItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addClassifierPropertyDescriptor(object);
			addColumn1PropertyDescriptor(object);
			addColumn2PropertyDescriptor(object);
			addColumn3PropertyDescriptor(object);
			addColumn4PropertyDescriptor(object);
			addColumn5PropertyDescriptor(object);
			addDisplayedColumn1PropertyDescriptor(object);
			addDisplayedColumn2PropertyDescriptor(object);
			addDisplayedColumn3PropertyDescriptor(object);
			addDisplayedColumn4PropertyDescriptor(object);
			addDisplayedColumn5PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_classifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_classifier_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__CLASSIFIER, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column1 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_column1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_column1_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN1, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column2 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_column2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_column2_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN2, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column3 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_column3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_column3_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN3, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column4 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn4PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column4 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_column4_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_column4_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN4, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Column5 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addColumn5PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Column5 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_column5_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_column5_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__COLUMN5, false,
				false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Displayed Column1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedColumn1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Displayed Column1 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_displayedColumn1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_displayedColumn1_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN1,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Displayed Column2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedColumn2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Displayed Column2 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_displayedColumn2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_displayedColumn2_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN2,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Displayed Column3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedColumn3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Displayed Column3 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_displayedColumn3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_displayedColumn3_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN3,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Displayed Column4 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedColumn4PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Displayed Column4 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_displayedColumn4_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_displayedColumn4_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN4,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Displayed Column5 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisplayedColumn5PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Displayed Column5 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAbstractHeaderRow_displayedColumn5_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAbstractHeaderRow_displayedColumn5_feature",
						"_UI_MAbstractHeaderRow_type"),
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__DISPLAYED_COLUMN5,
				false, false, false, null, null, null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					TablesPackage.Literals.MABSTRACT_HEADER_ROW__OBJECT_ROW);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return "<" + containingFeatureName + ">";
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractHeaderRow.class)) {
		case TablesPackage.MABSTRACT_HEADER_ROW__OBJECT_ROW:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				TablesPackage.Literals.MABSTRACT_HEADER_ROW__OBJECT_ROW,
				TablesFactory.eINSTANCE.createMObjectRow()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

}
