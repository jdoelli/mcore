/**
 */
package com.montages.mcore.objects.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MObjectReferenceImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.objects.MObjectReference} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MObjectReferenceItemProvider extends MValueItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectReferenceItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addReferencedObjectPropertyDescriptor(object);
			addTypeOfReferencedObjectPropertyDescriptor(object);
			addIdOfReferencedObjectPropertyDescriptor(object);
			addChoosableReferencePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Referenced Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferencedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Referenced Object feature.
		 * The list of possible choices is constraint by OCL let p:MProperty=self.containingPropertyInstance.property in
		if p.oclIsUndefined() 
		then true 
		else if p.type.oclIsUndefined() 
		 then if p.simpleType = SimpleType::Object 
		then true 
		else false endif
		 else if p.type.simpleType = SimpleType::Object
		   then true
		   else (trg.type = p.type) 
		   or p.type.allSubTypes()->includes(trg.type)
		endif endif endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReference_referencedObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReference_referencedObject_feature",
						"_UI_MObjectReference_type"),
				ObjectsPackage.Literals.MOBJECT_REFERENCE__REFERENCED_OBJECT,
				true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObject> result = new ArrayList<MObject>();
				Collection<? extends MObject> superResult = (Collection<? extends MObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MObject> iterator = result.iterator(); iterator
						.hasNext();) {
					MObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceImpl) object)
							.evalReferencedObjectChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Type Of Referenced Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeOfReferencedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Of Referenced Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MObjectReference_typeOfReferencedObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReference_typeOfReferencedObject_feature",
						"_UI_MObjectReference_type"),
				ObjectsPackage.Literals.MOBJECT_REFERENCE__TYPE_OF_REFERENCED_OBJECT,
				false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Id Of Referenced Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdOfReferencedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Id Of Referenced Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReference_idOfReferencedObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReference_idOfReferencedObject_feature",
						"_UI_MObjectReference_type"),
				ObjectsPackage.Literals.MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				null, null));
	}

	/**
	 * This adds a property descriptor for the Choosable Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChoosableReferencePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Choosable Reference feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObjectReference_choosableReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObjectReference_choosableReference_feature",
						"_UI_MObjectReference_type"),
				ObjectsPackage.Literals.MOBJECT_REFERENCE__CHOOSABLE_REFERENCE,
				false, false, false, null, null, null));
	}

	/**
	 * This returns MObjectReference.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MObjectReference"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MObjectReferenceImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MObjectReference.class)) {
		case ObjectsPackage.MOBJECT_REFERENCE__ID_OF_REFERENCED_OBJECT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
