/**
 */
package com.montages.mcore.objects.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;
import org.xocl.semantics.commands.CompoundCommandFocus;

import com.montages.mcore.MClassifier;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.objects.impl.MObjectImpl;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;
import com.montages.mcore.tables.TablesFactory;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.objects.MObject} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MObjectItemProvider extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {
		if (owner instanceof MObject) {

			if (feature == ObjectsPackage.Literals.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION)
				if (value instanceof String) {
					XUpdate myUpdate = ((MObject) owner)
							.classNameOrNatureUpdate((String) value);

					return myUpdate == null ? null
							: myUpdate.getContainingTransition()
									.interpreteTransitionAsCommand();

				}

			if (feature == ObjectsPackage.Literals.MOBJECT__DO_ACTION)
				if (value instanceof MObjectAction) {
					MObjectAction myValue = (MObjectAction) value;
					XUpdate myUpdate = ((MObject) owner)
							.doActionUpdate(myValue);

					Command interpretedCommand = null;
					List<Command> cmdList = null;
					if (myUpdate != null) {
						interpretedCommand = myUpdate.getContainingTransition()
								.interpreteTransitionAsCommand();
						if (interpretedCommand instanceof CompoundCommand) {
							cmdList = ((CompoundCommand) interpretedCommand)
									.getCommandList();
							if (cmdList.get(cmdList.size()
									- 1) instanceof ChangeCommandWithOperationCall) {
								final ChangeCommandWithOperationCall changeCommand = (ChangeCommandWithOperationCall) cmdList
										.get(cmdList.size() - 1);
								return new CompoundCommandFocus(cmdList,
										changeCommand);
							}

						}
						return interpretedCommand;

					}
				}
		}

		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addCreateTypeInPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasPropertyInstanceNotOfTypePropertyDescriptor(object);
			}
			addNaturePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addClassNameOrNatureDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addNatureNeededPropertyDescriptor(object);
			}
			addNatureMissingPropertyDescriptor(object);
			addTypePropertyDescriptor(object);
			addTypePackagePropertyDescriptor(object);
			addNatureENamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addCalculatedNaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInitializationTypePropertyDescriptor(object);
			}
			addIdPropertyDescriptor(object);
			addNumericIdOfObjectPropertyDescriptor(object);
			addContainingObjectPropertyDescriptor(object);
			addContainingPropertyInstancePropertyDescriptor(object);
			addResourcePropertyDescriptor(object);
			addReferencedByPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addWrongPropertyInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInitializedContainmentLevelsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			addStatusAsTextPropertyDescriptor(object);
			addDerivationsAsTextPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Nature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nature feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MObject_nature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_nature_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__NATURE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Class Name Or Nature Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassNameOrNatureDefinitionPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Class Name Or Nature Definition feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_classNameOrNatureDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_classNameOrNatureDefinition_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Nature Needed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNatureNeededPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nature Needed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_natureNeeded_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_natureNeeded_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__NATURE_NEEDED, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Nature Missing feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNatureMissingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nature Missing feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_natureMissing_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_natureMissing_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__NATURE_MISSING, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Nature EName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNatureENamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nature EName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_natureEName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_natureEName_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__NATURE_ENAME, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Calculated Nature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedNaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Nature feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_calculatedNature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_calculatedNature_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__CALCULATED_NATURE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NatureandTypePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Type Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_typePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_typePackage_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__TYPE_PACKAGE, true, false,
				true, null, getString("_UI_NatureandTypePropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type feature.
		 * The list of possible choices is constraint by OCL trg.kind = ClassifierKind::ClassType
		and
		trg.abstractClass = false
		and 
		if self.isRootObject() 
		then let tP:MPackage=
		 if typePackage.oclIsUndefined()
		  then if resource.oclIsUndefined() 
		then null
		else resource.rootObjectPackage endif
		  else typePackage endif in
		trg.selectableClassifier(
		  if type.oclIsUndefined() 
		then OrderedSet{} else OrderedSet{type} endif,
		  tP)
		else 
		let p:MProperty=self.eContainer().oclAsType(MPropertyInstance).property in
		if p.oclIsUndefined() then true 
		else p.type = trg or 
		    (if p.type.oclIsUndefined() then false 
		          else p.type.allSubTypes()->includes(trg) endif) endif 
		endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MObject_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_type_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__TYPE, true, false, true, null,
				getString("_UI_NatureandTypePropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectImpl) object).evalTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Initialization Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitializationTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Initialization Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_initializationType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_initializationType_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__INITIALIZATION_TYPE, false,
				false, false, null,
				getString("_UI_NatureandTypePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Id feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MObject_id_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_id_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__ID, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ObjectIdPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_containingObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_containingObject_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__CONTAINING_OBJECT, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Containing Property Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPropertyInstancePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Property Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_containingPropertyInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_containingPropertyInstance_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__CONTAINING_PROPERTY_INSTANCE,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Resource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Resource feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MObject_resource_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_resource_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__RESOURCE, false, false, false,
				null, getString("_UI_StructuralPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Referenced By feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addReferencedByPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Referenced By feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_referencedBy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_referencedBy_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__REFERENCED_BY, false, false,
				false, null, getString("_UI_StructuralPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Wrong Property Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addWrongPropertyInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Wrong Property Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_wrongPropertyInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_wrongPropertyInstance_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__WRONG_PROPERTY_INSTANCE, false,
				false, false, null, getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Numeric Id Of Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNumericIdOfObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Numeric Id Of Object feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_numericIdOfObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_numericIdOfObject_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__NUMERIC_ID_OF_OBJECT, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ObjectIdPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Initialized Containment Levels feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInitializedContainmentLevelsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Initialized Containment Levels feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_initializedContainmentLevels_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_initializedContainmentLevels_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_InitializationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL let instantiatedProperties:OrderedSet(MProperty) = self.propertyInstance.property->excluding(null)->asOrderedSet() in 
		let nonInstantiatedProperties:OrderedSet(MProperty) =
		if type.oclIsUndefined() then OrderedSet{} else
		 type.allFeaturesWithStorage()->select(p:MProperty | instantiatedProperties->excludes(p)) endif in
		let start:OrderedSet(MObjectAction) = OrderedSet{MObjectAction::Do} in start->append(
		if type.oclIsUndefined() or hasPropertyInstanceNotOfType then 
		MObjectAction::FixType else null endif)->append(
		if not nonInstantiatedProperties->isEmpty() then 
		MObjectAction::CompleteSlots else null endif)->append(
		if type.oclIsUndefined() then null else
		if  not self.hasPropertyInstanceNotOfType
		then null else
		MObjectAction::CleanSlots endif endif)->append(
		if true then
		MObjectAction::StringSlot else null endif)->append(
		if true then 
		MObjectAction::BooleanSlot else null endif)->append(
		if true then 
		MObjectAction::IntegerSlot else null endif)->append(
		if true then 
		MObjectAction::RealSlot else null endif)->append(
		if true then 
		MObjectAction::DateSlot else null endif)->append(
		if true then 
		MObjectAction::LiteralSlot else null endif)->append(
		if true then 
		MObjectAction::ReferenceSlot else null endif)->append(
		if true then 
		MObjectAction::ContainmentSlot else null endif)->append(MObjectAction::AddSibling)->excluding(null)
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MObject_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_doAction_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObjectAction> result = new ArrayList<MObjectAction>();
				Collection<? extends MObjectAction> superResult = (Collection<? extends MObjectAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MObjectImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Status As Text feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStatusAsTextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Status As Text feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_statusAsText_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_statusAsText_feature", "_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__STATUS_AS_TEXT, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SummaryAsTextPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derivations As Text feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivationsAsTextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derivations As Text feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_derivationsAsText_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_derivationsAsText_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__DERIVATIONS_AS_TEXT, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SummaryAsTextPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Create Type In Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCreateTypeInPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Create Type In Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_createTypeInPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_createTypeInPackage_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__CREATE_TYPE_IN_PACKAGE, true,
				false, true, null,
				getString("_UI_TypeDerivationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Has Property Instance Not Of Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasPropertyInstanceNotOfTypePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Has Property Instance Not Of Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MObject_hasPropertyInstanceNotOfType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MObject_hasPropertyInstanceNotOfType_feature",
						"_UI_MObject_type"),
				ObjectsPackage.Literals.MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeDerivationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(ObjectsPackage.Literals.MOBJECT__PROPERTY_INSTANCE);
			childrenFeatures.add(ObjectsPackage.Literals.MOBJECT__TABLE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MObject.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/element_obj"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MObjectImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MObject.class)) {
		case ObjectsPackage.MOBJECT__HAS_PROPERTY_INSTANCE_NOT_OF_TYPE:
		case ObjectsPackage.MOBJECT__NATURE:
		case ObjectsPackage.MOBJECT__CLASS_NAME_OR_NATURE_DEFINITION:
		case ObjectsPackage.MOBJECT__NATURE_NEEDED:
		case ObjectsPackage.MOBJECT__NATURE_MISSING:
		case ObjectsPackage.MOBJECT__NATURE_ENAME:
		case ObjectsPackage.MOBJECT__CALCULATED_NATURE:
		case ObjectsPackage.MOBJECT__ID:
		case ObjectsPackage.MOBJECT__NUMERIC_ID_OF_OBJECT:
		case ObjectsPackage.MOBJECT__INITIALIZED_CONTAINMENT_LEVELS:
		case ObjectsPackage.MOBJECT__DO_ACTION:
		case ObjectsPackage.MOBJECT__STATUS_AS_TEXT:
		case ObjectsPackage.MOBJECT__DERIVATIONS_AS_TEXT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case ObjectsPackage.MOBJECT__PROPERTY_INSTANCE:
		case ObjectsPackage.MOBJECT__TABLE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				ObjectsPackage.Literals.MOBJECT__PROPERTY_INSTANCE,
				ObjectsFactory.eINSTANCE.createMPropertyInstance()));

		newChildDescriptors.add(
				createChildParameter(ObjectsPackage.Literals.MOBJECT__TABLE,
						TablesFactory.eINSTANCE.createMTable()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ObjectsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
