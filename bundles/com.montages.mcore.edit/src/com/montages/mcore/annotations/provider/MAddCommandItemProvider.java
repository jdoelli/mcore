/**
 */
package com.montages.mcore.annotations.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAddCommand;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MAddCommand} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAddCommandItemProvider extends MExprAnnotationItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAddCommandItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSpecialENamePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addShortNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addENamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addFullLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalStructuralNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedShortNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCorrectNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCorrectShortNamePropertyDescriptor(object);
			}
			addTypePropertyDescriptor(object);
			addLabelPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Calculated Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Short Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_calculatedShortName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_calculatedShortName_feature",
						"_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__CALCULATED_SHORT_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MNamed_name_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_name_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__NAME, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Short Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MNamed_shortName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_shortName_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__SHORT_NAME, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Correct Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_correctName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_correctName_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__CORRECT_NAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Correct Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct Short Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_correctShortName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_correctShortName_feature",
						"_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__CORRECT_SHORT_NAME, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addENamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MNamed_eName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_eName_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__ENAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Special EName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpecialENamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special EName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_specialEName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_specialEName_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__SPECIAL_ENAME, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Full Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFullLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Full Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MNamed_fullLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_fullLabel_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__FULL_LABEL, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Structural Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalStructuralNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Structural Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_localStructuralName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_localStructuralName_feature",
						"_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__LOCAL_STRUCTURAL_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MNamed_calculatedName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MNamed_calculatedName_feature", "_UI_MNamed_type"),
				McorePackage.Literals.MNAMED__CALCULATED_NAME, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MAddCommand_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAddCommand_type_feature", "_UI_MAddCommand_type"),
				AnnotationsPackage.Literals.MADD_COMMAND__TYPE, true, false,
				true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAddCommand_label_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAddCommand_label_feature",
						"_UI_MAddCommand_type"),
				AnnotationsPackage.Literals.MADD_COMMAND__LABEL, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns MAddCommand.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/MAddCommand"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MAddCommand) object).getName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAddCommand.class)) {
		case AnnotationsPackage.MADD_COMMAND__SPECIAL_ENAME:
		case AnnotationsPackage.MADD_COMMAND__NAME:
		case AnnotationsPackage.MADD_COMMAND__SHORT_NAME:
		case AnnotationsPackage.MADD_COMMAND__ENAME:
		case AnnotationsPackage.MADD_COMMAND__FULL_LABEL:
		case AnnotationsPackage.MADD_COMMAND__LOCAL_STRUCTURAL_NAME:
		case AnnotationsPackage.MADD_COMMAND__CALCULATED_NAME:
		case AnnotationsPackage.MADD_COMMAND__CALCULATED_SHORT_NAME:
		case AnnotationsPackage.MADD_COMMAND__CORRECT_NAME:
		case AnnotationsPackage.MADD_COMMAND__CORRECT_SHORT_NAME:
		case AnnotationsPackage.MADD_COMMAND__LABEL:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
