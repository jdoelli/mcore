/**
 */
package com.montages.mcore.annotations.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.xocl.common.ui.celleditors.DateTimeCellEditor;
import org.xocl.common.ui.dialogs.DateTimeDialog;
import org.xocl.core.edit.provider.DataTimeCellEditorProvider;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.core.expr.OCLExpressionContext;

import com.montages.common.McoreCommandUtil;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MModelElement;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MPackage;
import com.montages.mcore.MParameter;
import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MAnnotation;
import com.montages.mcore.annotations.impl.MAnnotationImpl;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.objects.MObject;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MAnnotation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAnnotationItemProvider extends MAbstractAnnotionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAnnotationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addDomainObjectPropertyDescriptor(object);
			addDomainOperationPropertyDescriptor(object);
			addSelfObjectTypeOfAnnotationPropertyDescriptor(object);
			addTargetObjectTypeOfAnnotationPropertyDescriptor(object);
			addTargetSimpleTypeOfAnnotationPropertyDescriptor(object);
			addObjectObjectTypeOfAnnotationPropertyDescriptor(object);
			addValuePropertyDescriptor(object);
			addHistorizedOCL1PropertyDescriptor(object);
			addHistorizedOCL1TimestampPropertyDescriptor(object);
			addHistorizedOCL2PropertyDescriptor(object);
			addHistorizedOCL2TimestampPropertyDescriptor(object);
			addHistorizedOCL3PropertyDescriptor(object);
			addHistorizedOCL3TimestampPropertyDescriptor(object);
			addHistorizedOCL4PropertyDescriptor(object);
			addHistorizedOCL4TimestampPropertyDescriptor(object);
			addHistorizedOCL5PropertyDescriptor(object);
			addHistorizedOCL5TimestampPropertyDescriptor(object);
			addHistorizedOCL6PropertyDescriptor(object);
			addHistorizedOCL6TimestampPropertyDescriptor(object);
			addHistorizedOCL7PropertyDescriptor(object);
			addHistorizedOCL7TimestampPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Domain Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDomainObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Domain Object feature.
		 * The list of possible choices is constructed by OCL if self.generalContent->isEmpty() 
		then self.generalReference
		else if self.generalReference->isEmpty()
		then self.generalContent
		else self.generalContent->asSequence()
		->union(self.generalReference->asSequence()) endif endif
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_domainObject_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_domainObject_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__DOMAIN_OBJECT, true,
				false, false, null,
				getString("_UI_SpecialDomainSpecificSemanticsPropertyCategory"),
				null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MObject> result = new ArrayList<MObject>();
				Collection<? extends MObject> superResult = (Collection<? extends MObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MAnnotationImpl) object)
						.evalDomainObjectChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Domain Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDomainOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Domain Operation feature.
		 * The list of possible choices is constraint by OCL if domainObject.oclIsUndefined() 
		then false
		else if self.selfObjectTypeOfAnnotation.oclIsUndefined()
		then false
		else let c:MClassifier=domainObject.type in
		  if c.oclIsUndefined() 
		then false
		else let sts:OrderedSet(MClassifier) = 
		            self.selfObjectTypeOfAnnotation.allSubTypes()
		               ->append(self.selfObjectTypeOfAnnotation) in
		
		c.allProperties().operationSignature
		 ->select(s:MOperationSignature|
		    if  s.parameter->size() = 1
		      then let p:MParameter = s.parameter->first() in
		        if p.type.oclIsUndefined() 
		           then false
		           else sts->includes(p.type) endif
		      else false endif)->includes(trg)
		
		endif endif endif
		      
		     
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_domainOperation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_domainOperation_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__DOMAIN_OPERATION, true,
				false, true, null,
				getString("_UI_SpecialDomainSpecificSemanticsPropertyCategory"),
				null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MOperationSignature> result = new ArrayList<MOperationSignature>();
				Collection<? extends MOperationSignature> superResult = (Collection<? extends MOperationSignature>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MOperationSignature> iterator = result
						.iterator(); iterator.hasNext();) {
					MOperationSignature trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MAnnotationImpl) object)
							.evalDomainOperationChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Self Object Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSelfObjectTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Self Object Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_selfObjectTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_selfObjectTypeOfAnnotation_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__SELF_OBJECT_TYPE_OF_ANNOTATION,
				false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Object Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetObjectTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Target Object Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAnnotation_targetObjectTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_targetObjectTypeOfAnnotation_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__TARGET_OBJECT_TYPE_OF_ANNOTATION,
				false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Target Simple Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTargetSimpleTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Target Simple Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAnnotation_targetSimpleTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_targetSimpleTypeOfAnnotation_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Object Object Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectObjectTypeOfAnnotationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Object Object Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MAnnotation_objectObjectTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_objectObjectTypeOfAnnotation_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__OBJECT_OBJECT_TYPE_OF_ANNOTATION,
				false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addValuePropertyDescriptorGen(Object object) {
		/*
		 * This adds a property descriptor for the Value feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_value_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_value_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__VALUE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ExplicitOCLPropertyCategory"), null) {
			@Override
			public OCLExpressionContext getOCLExpressionContext(Object object) {
				return ((MAnnotationImpl) object)
						.getValueOCLExpressionContext();
			}
		});
	}

	/**
	 * @generated NOT
	 * @param object
	 */
	protected void addValuePropertyDescriptor(Object object) {
		addValuePropertyDescriptorGen(object);
		int lastIdx = itemPropertyDescriptors.size() - 1;
		ItemPropertyDescriptor defaultGenerated = (ItemPropertyDescriptor) itemPropertyDescriptors
				.remove(lastIdx);

		ItemPropertyDescriptor withCorrectContext = new ItemPropertyDescriptor(
				defaultGenerated) {
			@Override
			public OCLExpressionContext getOCLExpressionContext(Object object) {
				return getValuePropertyDescriptorExpressionContext(object);
			}
		};
		itemPropertyDescriptors.add(withCorrectContext);
	}

	/**
	 * This adds a property descriptor for the Historized OCL1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL1 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL1_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL1_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL1, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL1 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL1TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL1 Timestamp feature.
		 */
		ItemPropertyDescriptor descriptor = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL1Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL1Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descriptor);
	}

	/**
	 * This adds a property descriptor for the Historized OCL2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL2 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL2_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL2_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL2, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null) {
		};
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL2 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL2TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL2 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL2Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL2Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL3 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL3_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL3_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL3, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL3 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL3TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL3 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL3Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL3Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL4 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL4PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL4 feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL4_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL4_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL4, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL4 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL4TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL4 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL4Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL4Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL5 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL5PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL5 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL5_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL5_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL5, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL5 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL5TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL5 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL5Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL5Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL6 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL6PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL6 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL6_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL6_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL6, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL6 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL6TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL6 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL6Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL6Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Historized OCL7 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addHistorizedOCL7PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL7 feature.
		 */
		ItemPropertyDescriptor descr = new DisableEditrItemDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL7_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL7_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL7, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null);
		itemPropertyDescriptors.add(descr);
	}

	/**
	 * This adds a property descriptor for the Historized OCL7 Timestamp feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHistorizedOCL7TimestampPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Historized OCL7 Timestamp feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MAnnotation_historizedOCL7Timestamp_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MAnnotation_historizedOCL7Timestamp_feature",
						"_UI_MAnnotation_type"),
				AnnotationsPackage.Literals.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_HistorizedOCLPropertyCategory"), null));
	}

	private OCLExpressionContext getValuePropertyDescriptorExpressionContext(
			Object object) {
		final MModelElement modelElement = ((MAnnotationImpl) object)
				.getAnnotatedElement();

		// Generate Ecore if access to OCL is required
		// but Ecore models are not generated.

		if (isContextMissing(modelElement)) {
			generateEcore(modelElement);
		}

		return ((MAnnotationImpl) object).getValueOCLExpressionContext();
	}

	private static class GenerateECore extends AbstractCommand {

		final MModelElement myModelElement;

		public GenerateECore(final MModelElement modelElement) {
			myModelElement = modelElement;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public void execute() {
			try {
				new Mcore2Ecore().transform(//
						myModelElement.eResource().getResourceSet(), //
						(MComponent) EcoreUtil
								.getRootContainer(myModelElement));
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		@Override
		public void undo() {
		}

		@Override
		public void redo() {
		}
	}

	private static void generateEcore(MModelElement modelElement) {
		if (isContextMissing(modelElement)) {
			McoreCommandUtil.executeUnsafeCommand(
					new GenerateECore(modelElement),
					TransactionUtil.getEditingDomain(modelElement));
		}
	}

	private static boolean isContextMissing(MModelElement element) {
		if (element instanceof MPackage) {
			return ((MPackage) element).getInternalEPackage() == null;
		} else if (element instanceof MClassifier) {
			return ((MClassifier) element).getInternalEClassifier() == null;
		} else if (element instanceof MProperty) {
			return ((MProperty) element)
					.getInternalEStructuralFeature() == null;
		} else if (element instanceof MOperationSignature) {
			return ((MOperationSignature) element)
					.getInternalEOperation() == null;
		} else if (element instanceof MParameter) {
			return ((MParameter) element).getInternalEParameter() == null;
		} else {
			return false;
		}
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MAnnotation) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAnnotation.class)) {
		case AnnotationsPackage.MANNOTATION__TARGET_SIMPLE_TYPE_OF_ANNOTATION:
		case AnnotationsPackage.MANNOTATION__VALUE:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL1_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL2_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL3_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL4_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL5_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL6_TIMESTAMP:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7:
		case AnnotationsPackage.MANNOTATION__HISTORIZED_OCL7_TIMESTAMP:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * @generated NOT
	 */
	private static class DisableEditrItemDescriptor extends
			ItemPropertyDescriptor implements DataTimeCellEditorProvider {

		public DisableEditrItemDescriptor(AdapterFactory adapterFactory,
				ResourceLocator resourceLocator, String displayName,
				String description, EStructuralFeature feature,
				boolean isSettable, boolean multiLine, boolean sortChoices,
				Object staticImage, String category, String[] filterFlags) {
			super(adapterFactory, resourceLocator, displayName, description,
					feature, isSettable, multiLine, sortChoices, staticImage,
					category, filterFlags);
		}

		@Override
		public OCLExpressionContext getOCLExpressionContext(Object object) {
			final MModelElement modelElement = ((MAnnotationImpl) object)
					.getAnnotatedElement();

			if (isContextMissing(modelElement)) {
				generateEcore(modelElement);
			}

			return new OCLExpressionContext(
					OCLExpressionContext.getEObjectClass(), null, null, false);
		}

		@Override
		public CellEditor getDateTimeCellEditor(Composite composite,
				Date date) {
			return new DateTimeCellEditor(composite, date, null) {

				@Override
				protected Object openDialogBox(Control cellEditorWindow) {
					new DateTimeDialog(cellEditorWindow.getShell(), getDate()) {

						@Override
						protected void createButtonsForButtonBar(
								Composite parent) {
							// create OK and Cancel buttons by default
							createButton(parent, IDialogConstants.OK_ID,
									IDialogConstants.OK_LABEL, true);
						}

					}.open();
					return null;
				}
			};
		}

	}
}
