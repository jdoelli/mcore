/**
 */
package com.montages.mcore.annotations.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.ResourceLocator;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.impl.MClassifierAnnotationsImpl;
import com.montages.mcore.provider.MRepositoryElementItemProvider;
import com.montages.mcore.provider.McoreEditPlugin;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MClassifierAnnotations} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MClassifierAnnotationsItemProvider
		extends MRepositoryElementItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierAnnotationsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addClassifierPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifierAnnotations_classifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifierAnnotations_classifier_feature",
						"_UI_MClassifierAnnotations_type"),
				AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__CLASSIFIER,
				false, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__LABEL);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__CONSTRAINT);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MClassifierAnnotations.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MClassifierAnnotations"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MClassifierAnnotationsImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MClassifierAnnotations.class)) {
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__LABEL:
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__CONSTRAINT:
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS:
		case AnnotationsPackage.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__LABEL,
				AnnotationsFactory.eINSTANCE.createMLabel()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__CONSTRAINT,
				AnnotationsFactory.eINSTANCE.createMInvariantConstraint()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__PROPERTY_ANNOTATIONS,
				AnnotationsFactory.eINSTANCE.createMPropertyAnnotations()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MCLASSIFIER_ANNOTATIONS__OPERATION_ANNOTATIONS,
				AnnotationsFactory.eINSTANCE.createMOperationAnnotations()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return McoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
