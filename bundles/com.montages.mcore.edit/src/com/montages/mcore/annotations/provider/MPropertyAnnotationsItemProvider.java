/**
 */
package com.montages.mcore.annotations.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.annotations.impl.MPropertyAnnotationsImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MPropertyAnnotations} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MPropertyAnnotationsItemProvider
		extends MAbstractPropertyAnnotationsItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyAnnotationsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MPropertyAnnotations) {
			MPropertyAnnotations typedOwner = ((MPropertyAnnotations) owner);

			if (feature == AnnotationsPackage.eINSTANCE
					.getMPropertyAnnotations_DoAction())
				myUpdate = typedOwner.doAction$Update(
						(com.montages.mcore.annotations.MPropertyAnnotationsAction) value);

		}

		return myUpdate == null
				? super.createSetCommand(domain, owner, feature, value, index)
				: myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addPropertyPropertyDescriptor(object);
			addOverridingPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addOverridesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOverriddenInPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Property feature.
		 * The list of possible choices is constructed by OCL 
		self.containingClassifier.allFeatures()
		 * The list of possible choices is constraint by OCL trg=self.property or self.containingClassifier.allPropertyAnnotations().property
		->excludes(trg)  
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyAnnotations_property_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyAnnotations_property_feature",
						"_UI_MPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__PROPERTY,
				true, false, false, null,
				getString("_UI_AnnotatedPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyAnnotationsImpl) object)
						.evalPropertyChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				for (Iterator<MProperty> iterator = result.iterator(); iterator
						.hasNext();) {
					MProperty trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MPropertyAnnotationsImpl) object)
							.evalPropertyChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Overriding feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverridingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overriding feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyAnnotations_overriding_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyAnnotations_overriding_feature",
						"_UI_MPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__OVERRIDING,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_OverridingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Overrides feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverridesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overrides feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyAnnotations_overrides_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyAnnotations_overrides_feature",
						"_UI_MPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__OVERRIDES,
				true, false, true, null,
				getString("_UI_OverridingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Overridden In feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverriddenInPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overridden In feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyAnnotations_overriddenIn_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyAnnotations_overriddenIn_feature",
						"_UI_MPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__OVERRIDDEN_IN,
				true, false, true, null,
				getString("_UI_OverridingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertyAnnotations_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertyAnnotations_doAction_feature",
						"_UI_MPropertyAnnotations_type"),
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__DO_ACTION,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__RESULT);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_ORDER);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_VALUE);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__ADD);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__UPDATE);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__STRING_AS_OCL);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__HIGHLIGHT);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__LAYOUT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MPropertyAnnotations.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MPropertyAnnotations"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MPropertyAnnotationsImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MPropertyAnnotations.class)) {
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__OVERRIDING:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__RESULT:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_ORDER:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__INIT_VALUE:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__ADD:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__UPDATE:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__STRING_AS_OCL:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__HIGHLIGHT:
		case AnnotationsPackage.MPROPERTY_ANNOTATIONS__LAYOUT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__RESULT,
				AnnotationsFactory.eINSTANCE.createMResult()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_ORDER,
				AnnotationsFactory.eINSTANCE.createMInitializationOrder()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__INIT_VALUE,
				AnnotationsFactory.eINSTANCE.createMInitializationValue()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRUCTION,
				AnnotationsFactory.eINSTANCE.createMChoiceConstruction()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__CHOICE_CONSTRAINT,
				AnnotationsFactory.eINSTANCE.createMChoiceConstraint()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__ADD,
				AnnotationsFactory.eINSTANCE.createMAdd()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__UPDATE,
				AnnotationsFactory.eINSTANCE.createMUpdate()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__STRING_AS_OCL,
				AnnotationsFactory.eINSTANCE.createMStringAsOclAnnotation()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__HIGHLIGHT,
				AnnotationsFactory.eINSTANCE.createMHighlightAnnotation()));

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MPROPERTY_ANNOTATIONS__LAYOUT,
				AnnotationsFactory.eINSTANCE.createMLayoutAnnotation()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
