/**
 */
package com.montages.mcore.annotations.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MOperationSignature;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.impl.MOperationAnnotationsImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.annotations.MOperationAnnotations} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MOperationAnnotationsItemProvider
		extends MAbstractPropertyAnnotationsItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MOperationAnnotationsItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addOperationSignaturePropertyDescriptor(object);
			addOverridingPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addOverridesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOverriddenInPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Operation Signature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationSignaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Operation Signature feature.
		 * The list of possible choices is constructed by OCL self.containingClassifier.allOperationSignatures()
		 * The list of possible choices is constraint by OCL trg=self.operationSignature or 
		self.containingClassifier.allOperationAnnotations().operationSignature->excludes(trg)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MOperationAnnotations_operationSignature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationAnnotations_operationSignature_feature",
						"_UI_MOperationAnnotations_type"),
				AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OPERATION_SIGNATURE,
				true, false, false, null,
				getString("_UI_AnnotatedPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MOperationSignature> result = new ArrayList<MOperationSignature>();
				Collection<? extends MOperationSignature> superResult = (Collection<? extends MOperationSignature>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MOperationAnnotationsImpl) object)
						.evalOperationSignatureChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				for (Iterator<MOperationSignature> iterator = result
						.iterator(); iterator.hasNext();) {
					MOperationSignature trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MOperationAnnotationsImpl) object)
							.evalOperationSignatureChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Overriding feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverridingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overriding feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationAnnotations_overriding_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationAnnotations_overriding_feature",
						"_UI_MOperationAnnotations_type"),
				AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OVERRIDING,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_OverridingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Overrides feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverridesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overrides feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationAnnotations_overrides_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationAnnotations_overrides_feature",
						"_UI_MOperationAnnotations_type"),
				AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OVERRIDES,
				true, false, true, null,
				getString("_UI_OverridingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Overridden In feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOverriddenInPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Overridden In feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MOperationAnnotations_overriddenIn_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MOperationAnnotations_overriddenIn_feature",
						"_UI_MOperationAnnotations_type"),
				AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__OVERRIDDEN_IN,
				true, false, true, null,
				getString("_UI_OverridingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__RESULT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MOperationAnnotations.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator()
				.getImage("full/obj16/MOperationAnnotations"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MOperationAnnotationsImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MOperationAnnotations.class)) {
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__OVERRIDING:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case AnnotationsPackage.MOPERATION_ANNOTATIONS__RESULT:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				AnnotationsPackage.Literals.MOPERATION_ANNOTATIONS__RESULT,
				AnnotationsFactory.eINSTANCE.createMResult()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AnnotationsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
