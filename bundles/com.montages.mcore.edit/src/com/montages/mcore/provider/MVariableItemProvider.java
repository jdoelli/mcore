/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MVariable} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MVariableItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MVariableItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addMultiplicityAsStringPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedSingularPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedTypePackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addVoidTypeAllowedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedSimpleTypePropertyDescriptor(object);
			}
			addMultiplicityCasePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Multiplicity As String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiplicityAsStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Multiplicity As String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_multiplicityAsString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_multiplicityAsString_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__MULTIPLICITY_AS_STRING, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedType_feature", "_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_TYPE, false, false,
				false, null, getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedMandatory_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_MANDATORY, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedSingular_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_SINGULAR, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Type Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedTypePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Type Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedTypePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedTypePackage_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_TYPE_PACKAGE, false,
				false, false, null,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Void Type Allowed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addVoidTypeAllowedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Void Type Allowed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_voidTypeAllowed_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_voidTypeAllowed_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__VOID_TYPE_ALLOWED, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_calculatedSimpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_calculatedSimpleType_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__CALCULATED_SIMPLE_TYPE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypeInformationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Multiplicity Case feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMultiplicityCasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Multiplicity Case feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MTyped_multiplicityCase_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MTyped_multiplicityCase_feature",
						"_UI_MTyped_type"),
				McorePackage.Literals.MTYPED__MULTIPLICITY_CASE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MVariable) object).getName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MVariable.class)) {
		case McorePackage.MVARIABLE__MULTIPLICITY_AS_STRING:
		case McorePackage.MVARIABLE__CALCULATED_MANDATORY:
		case McorePackage.MVARIABLE__CALCULATED_SINGULAR:
		case McorePackage.MVARIABLE__VOID_TYPE_ALLOWED:
		case McorePackage.MVARIABLE__CALCULATED_SIMPLE_TYPE:
		case McorePackage.MVARIABLE__MULTIPLICITY_CASE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
