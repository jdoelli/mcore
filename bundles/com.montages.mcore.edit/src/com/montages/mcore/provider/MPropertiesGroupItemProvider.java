/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;
import org.xocl.semantics.commands.CompoundCommandFocus;

import com.montages.mcore.MPropertiesGroup;
import com.montages.mcore.McorePackage;
import com.montages.mcore.impl.MPropertiesGroupImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MPropertiesGroup} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MPropertiesGroupItemProvider
		extends MPropertiesContainerItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesGroupItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MPropertiesGroup) {
			MPropertiesGroup typedOwner = ((MPropertiesGroup) owner);

			if (feature == McorePackage.eINSTANCE
					.getMPropertiesGroup_DoAction())
				myUpdate = typedOwner.doActionUpdate(
						(com.montages.mcore.MPropertiesGroupAction) value);

			Command interpretedCommand = null;
			List<Command> cmdList = null;
			if (myUpdate != null) {
				interpretedCommand = myUpdate.getContainingTransition()
						.interpreteTransitionAsCommand();
				if (interpretedCommand instanceof CompoundCommand) {
					cmdList = ((CompoundCommand) interpretedCommand)
							.getCommandList();
					if (cmdList.get(cmdList.size()
							- 1) instanceof ChangeCommandWithOperationCall) {
						final ChangeCommandWithOperationCall changeCommand = (ChangeCommandWithOperationCall) cmdList
								.get(cmdList.size() - 1);
						return new CompoundCommandFocus(cmdList, changeCommand);
					}

				}
				return interpretedCommand;

			}

		}

		return super.createSetCommand(domain, owner, feature, value, index);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addOrderPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertiesGroup_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesGroup_doAction_feature",
						"_UI_MPropertiesGroup_type"),
				McorePackage.Literals.MPROPERTIES_GROUP__DO_ACTION, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Order feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOrderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Order feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertiesGroup_order_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesGroup_order_feature",
						"_UI_MPropertiesGroup_type"),
				McorePackage.Literals.MPROPERTIES_GROUP__ORDER, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_OrderPropertyCategory"), null));
	}

	/**
	 * This returns MPropertiesGroup.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Association_none"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MPropertiesGroupImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MPropertiesGroup.class)) {
		case McorePackage.MPROPERTIES_GROUP__ORDER:
		case McorePackage.MPROPERTIES_GROUP__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
