/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsFactory;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.MComponent;
import com.montages.mcore.MComponentAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.impl.MComponentImpl;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.objects.ObjectsPackage;
import com.montages.mcore.provider.commands.MCoreCommandClasses;
import java.util.ArrayList;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MComponent} object.
 * <!-- begin-user-doc --> <!-- end-user-doc -->
 * @generated
 */
public class MComponentItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MComponentItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	// Whenever any feature of MComponent is set, this method is executed
	/**
	 * <!-- begin-user-doc --> Override SetCommand for XUpdate execution <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		// The owner tells us, which MComponent object calls the set Command
		// Feature tells us, which feature is ought to be set ( here we need to
		// fish for our doActions Property)
		// Object value : Tells to which value our feature is requested to be
		// set (here we need to fish for our doActions Literals)

		// Check whether the owner is of instance MComponent ( trivial)
		if (owner instanceof MComponent)
			// Check whether the feature that just got set is DO ACTION
			if (feature == McorePackage.Literals.MCOMPONENT__DO_ACTION)
				// Check whether the value is a literal from DO ACTION
				if (value instanceof MComponentAction) {
					MComponentAction doAction = (MComponentAction) value;
					MComponent mComp = (MComponent) owner;
					XUpdate actionUpdate = mComp.doActionUpdate(doAction);

					switch (((MComponentAction) value).getValue()) {
					case MComponentAction.ABSTRACT_VALUE:
						return new MCoreCommandClasses.MComponent_Abstract(
								domain, owner, feature, value);

					case MComponentAction.IMPLEMENT_VALUE:
						return new MCoreCommandClasses.MComponent_Implement(
								domain, owner, feature, value);

					case MComponentAction.OPEN_EDITOR_VALUE:

						try {
							if (!((MComponent) owner).getResourceFolder()
									.isEmpty()) {
								for (Adapter adapt : ((MComponent) owner)
										.getResourceFolder().get(0)
										.eAdapters()) {
									if (adapt instanceof IEditingDomainItemProvider) {
										CommandParameter p = new CommandParameter(
												((MComponent) owner)
														.getResourceFolder()
														.get(0),
												ObjectsPackage.eINSTANCE
														.getMResourceFolder_DoAction(),
												MResourceFolderAction.OPEN_EDITOR,
												index);
										return ((IEditingDomainItemProvider) adapt)
												.createCommand(
														((MComponent) owner)
																.getResourceFolder()
																.get(0),
														domain,
														SetCommand.class, p);
									}
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					// If we did not cover a doAction , we will perform as
					// nothing happened
					// Else we call interpreteTransitionAsCommand and return
					// these Commands to be executed instead of the setCommand
					return actionUpdate == null
							? super.createSetCommand(domain, owner, feature,
									value, index)
							: actionUpdate.getContainingTransition()
									.interpreteTransitionAsCommand();
				}

		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePrefixPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDerivedNamePrefixPropertyDescriptor(object);
			}
			addNamePrefixForFeaturesPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addContainingRepositoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addRepresentsCoreComponentPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addRootPackagePropertyDescriptor(object);
			}
			addDomainTypePropertyDescriptor(object);
			addDomainNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDerivedDomainTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedDomainNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedBundleNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedURIPropertyDescriptor(object);
			}
			addSpecialBundleNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addPreloadedComponentPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addMainNamedEditorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addMainEditorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHideAdvancedPropertiesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllGeneralAnnotationsPropertyDescriptor(object);
			}
			addAbstractComponentPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addCountImplementationsPropertyDescriptor(object);
			}
			addDisableDefaultContainmentPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			addAvoidRegenerationOfEditorConfigurationPropertyDescriptor(object);
			addUseLegacyEditorconfigPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Hide Advanced Properties feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHideAdvancedPropertiesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Hide Advanced Properties feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_hideAdvancedProperties_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_hideAdvancedProperties_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__HIDE_ADVANCED_PROPERTIES,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Abstract Component feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAbstractComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Abstract Component feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_abstractComponent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_abstractComponent_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__ABSTRACT_COMPONENT, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialComponentSettingsPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Count Implementations feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCountImplementationsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Count Implementations feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_countImplementations_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_countImplementations_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__COUNT_IMPLEMENTATIONS, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialComponentSettingsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Disable Default Containment feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDisableDefaultContainmentPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Disable Default Containment feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_disableDefaultContainment_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_disableDefaultContainment_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialComponentSettingsPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the All General Annotations feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllGeneralAnnotationsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All General Annotations feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_allGeneralAnnotations_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_allGeneralAnnotations_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__ALL_GENERAL_ANNOTATIONS,
				false, false, false, null,
				getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Name Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNamePrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_namePrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_namePrefix_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__NAME_PREFIX, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Name Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedNamePrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Name Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_derivedNamePrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_derivedNamePrefix_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DERIVED_NAME_PREFIX, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Name Prefix For Features feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePrefixForFeaturesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name Prefix For Features feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_namePrefixForFeatures_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_namePrefixForFeatures_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__NAME_PREFIX_FOR_FEATURES,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Main Editor feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMainEditorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Main Editor feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_mainEditor_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_mainEditor_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__MAIN_EDITOR, false, false,
				false, null, getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Use Legacy Editorconfig feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseLegacyEditorconfigPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Use Legacy Editorconfig feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_useLegacyEditorconfig_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_useLegacyEditorconfig_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__USE_LEGACY_EDITORCONFIG, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_EditorConfigurationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Main Named Editor feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addMainNamedEditorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Main Named Editor feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_mainNamedEditor_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_mainNamedEditor_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__MAIN_NAMED_EDITOR, true,
				false, true, null, getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL let e1:OrderedSet(MComponentAction)=OrderedSet{MComponentAction::Do, MComponentAction::Object, MComponentAction::Class,MComponentAction::FixDocumentation,MComponentAction::OpenEditor} in
		if self.abstractComponent = true
		then e1->append(MComponentAction::Implement)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)
		else e1->append(MComponentAction::Abstract)->append(MComponentAction::GeneratePropertyGroups)->append(MComponentAction::RemovePropertyGroups)->append(MComponentAction::CompleteSemantics)
		endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_doAction_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MComponentAction> result = new ArrayList<MComponentAction>();
				Collection<? extends MComponentAction> superResult = (Collection<? extends MComponentAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MComponentImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Derived Bundle Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedBundleNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Bundle Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_derivedBundleName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_derivedBundleName_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DERIVED_BUNDLE_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived URI feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedURIPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived URI feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_derivedURI_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_derivedURI_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DERIVED_URI, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Preloaded Component feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addPreloadedComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Preloaded Component feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_preloadedComponent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_preloadedComponent_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__PRELOADED_COMPONENT, true,
				false, true, null, getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Represents Core Component feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRepresentsCoreComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Represents Core Component feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_representsCoreComponent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_representsCoreComponent_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__REPRESENTS_CORE_COMPONENT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Root Package feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRootPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Root Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_rootPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_rootPackage_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__ROOT_PACKAGE, false, false,
				false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Avoid Regeneration Of Editor Configuration feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAvoidRegenerationOfEditorConfigurationPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Avoid Regeneration Of Editor Configuration feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MComponent_avoidRegenerationOfEditorConfiguration_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_avoidRegenerationOfEditorConfiguration_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_EditorConfigurationPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Containing Repository feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingRepositoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Repository feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_containingRepository_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_containingRepository_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__CONTAINING_REPOSITORY, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Domain Type feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDomainTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Domain Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_domainType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_domainType_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DOMAIN_TYPE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Domain Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDomainNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Domain Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_domainName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_domainName_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DOMAIN_NAME, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Domain Type feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedDomainTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Domain Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_derivedDomainType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_derivedDomainType_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DERIVED_DOMAIN_TYPE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Domain Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedDomainNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Domain Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_derivedDomainName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_derivedDomainName_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__DERIVED_DOMAIN_NAME, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BundleNamingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Special Bundle Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSpecialBundleNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special Bundle Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MComponent_specialBundleName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MComponent_specialBundleName_feature",
						"_UI_MComponent_type"),
				McorePackage.Literals.MCOMPONENT__SPECIAL_BUNDLE_NAME, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures
					.add(McorePackage.Literals.MCOMPONENT__OWNED_PACKAGE);
			childrenFeatures
					.add(McorePackage.Literals.MCOMPONENT__RESOURCE_FOLDER);
			childrenFeatures.add(McorePackage.Literals.MCOMPONENT__SEMANTICS);
			childrenFeatures
					.add(McorePackage.Literals.MCOMPONENT__GENERATED_EPACKAGE);
			childrenFeatures
					.add(McorePackage.Literals.MCOMPONENT__NAMED_EDITOR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MComponent.gif. <!-- begin-user-doc --> <!-- end-user-doc
	 * -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/Component"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MComponentImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MComponent.class)) {
		case McorePackage.MCOMPONENT__NAME_PREFIX:
		case McorePackage.MCOMPONENT__DERIVED_NAME_PREFIX:
		case McorePackage.MCOMPONENT__NAME_PREFIX_FOR_FEATURES:
		case McorePackage.MCOMPONENT__REPRESENTS_CORE_COMPONENT:
		case McorePackage.MCOMPONENT__DOMAIN_TYPE:
		case McorePackage.MCOMPONENT__DOMAIN_NAME:
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_TYPE:
		case McorePackage.MCOMPONENT__DERIVED_DOMAIN_NAME:
		case McorePackage.MCOMPONENT__DERIVED_BUNDLE_NAME:
		case McorePackage.MCOMPONENT__DERIVED_URI:
		case McorePackage.MCOMPONENT__SPECIAL_BUNDLE_NAME:
		case McorePackage.MCOMPONENT__HIDE_ADVANCED_PROPERTIES:
		case McorePackage.MCOMPONENT__ABSTRACT_COMPONENT:
		case McorePackage.MCOMPONENT__COUNT_IMPLEMENTATIONS:
		case McorePackage.MCOMPONENT__DISABLE_DEFAULT_CONTAINMENT:
		case McorePackage.MCOMPONENT__DO_ACTION:
		case McorePackage.MCOMPONENT__AVOID_REGENERATION_OF_EDITOR_CONFIGURATION:
		case McorePackage.MCOMPONENT__USE_LEGACY_EDITORCONFIG:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MCOMPONENT__OWNED_PACKAGE:
		case McorePackage.MCOMPONENT__RESOURCE_FOLDER:
		case McorePackage.MCOMPONENT__SEMANTICS:
		case McorePackage.MCOMPONENT__GENERATED_EPACKAGE:
		case McorePackage.MCOMPONENT__NAMED_EDITOR:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MCOMPONENT__OWNED_PACKAGE,
				McoreFactory.eINSTANCE.createMPackage()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MCOMPONENT__RESOURCE_FOLDER,
				ObjectsFactory.eINSTANCE.createMResourceFolder()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MCOMPONENT__SEMANTICS,
				SemanticsFactory.eINSTANCE.createXSemantics()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MCOMPONENT__NAMED_EDITOR,
				McoreFactory.eINSTANCE.createMNamedEditor()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
