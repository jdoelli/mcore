/**
 */
package com.montages.mcore.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.RemoveCommand;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;
import org.xocl.semantics.commands.CompoundCommandFocus;

import com.montages.mcore.ClassifierKind;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MClassifierAction;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.impl.MClassifierImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MClassifier} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MClassifierItemProvider extends MPropertiesContainerItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MClassifierItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		if (feature == McorePackage.eINSTANCE.getMClassifier_DoAction()
				|| feature == McorePackage.eINSTANCE
						.getMClassifier_ToggleSuperType())
			if (owner instanceof MClassifier) {
				XUpdate myUpdate = null;
				MClassifier cla = (MClassifier) owner;
				if (value instanceof MClassifierAction)
					myUpdate = cla.doActionUpdate((MClassifierAction) value);
				else if (value instanceof MClassifier)
					myUpdate = cla.doSuperTypeUpdate((MClassifier) value);

				Command interpretedCommand = null;
				List<Command> cmdList = null;
				if (myUpdate != null) {
					interpretedCommand = myUpdate.getContainingTransition()
							.interpreteTransitionAsCommand();
					if (interpretedCommand instanceof CompoundCommand) {
						cmdList = ((CompoundCommand) interpretedCommand)
								.getCommandList();
						if (cmdList.get(cmdList.size()
								- 1) instanceof ChangeCommandWithOperationCall) {
							final ChangeCommandWithOperationCall changeCommand = (ChangeCommandWithOperationCall) cmdList
									.get(cmdList.size() - 1);
							return new CompoundCommandFocus(cmdList,
									changeCommand);
						}

					}
					return interpretedCommand;

				}

			}

		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeStringPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasSimpleDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasSimpleModelingTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeIsCorrectPropertyDescriptor(object);
			}
			addSimpleTypePropertyDescriptor(object);
			addAbstractClassPropertyDescriptor(object);
			addEnforcedClassPropertyDescriptor(object);
			addSuperTypePackagePropertyDescriptor(object);
			addSuperTypePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInternalEClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addRepresentsCoreTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllPropertyGroupsPropertyDescriptor(object);
			}
			addEInterfacePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addOperationAsIdPropertyErrorPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIsClassByStructurePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addToggleSuperTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOrderingStrategyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTestAllPropertiesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIdPropertyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addResolveContainerActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAbstractDoActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addObjectReferencesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addNearestInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addStrictNearestInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addStrictInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntelligentNearestInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addObjectUnreferencedPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOutstandingToCopyObjectsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addPropertyInstancesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addObjectReferencePropertyInstancesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntelligentInstancePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainmentHierarchyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addStrictAllClassesContainedInPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addStrictContainmentHierarchyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntelligentAllClassesContainedInPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntelligentContainmentHierarchyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInstantiationHierarchyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInstantiationPropertyHierarchyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIntelligentInstantiationPropertyHierarchyPropertyDescriptor(
						object);
			}
			if (shouldShowAdvancedProperties()) {
				addClassescontainedinPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCanApplyDefaultContainmentPropertyDescriptor(object);
			}
			addPropertiesAsTextPropertyDescriptor(object);
			addSemanticsAsTextPropertyDescriptor(object);
			addDerivedJsonSchemaPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Simple Type String feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeStringPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type String feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleTypeString_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleTypeString_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_STRING,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Has Simple Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasSimpleDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Has Simple Data Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_hasSimpleDataType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_hasSimpleDataType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_DATA_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Has Simple Modeling Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasSimpleModelingTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Has Simple Modeling Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_hasSimpleModelingType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_hasSimpleModelingType_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__HAS_SIMPLE_MODELING_TYPE,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type Is Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeIsCorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type Is Correct feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MHasSimpleType_simpleTypeIsCorrect_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MHasSimpleType_simpleTypeIsCorrect_feature",
						"_UI_MHasSimpleType_type"),
				McorePackage.Literals.MHAS_SIMPLE_TYPE__SIMPLE_TYPE_IS_CORRECT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL let classActions:OrderedSet(MClassifierAction) =
		if self.literal->isEmpty() 
		  then OrderedSet{MClassifierAction::CreateInstance, if abstractDoAction then MClassifierAction::Abstract else null endif, MClassifierAction::Specialize,
		  MClassifierAction::StringAttribute, MClassifierAction::BooleanAttribute, MClassifierAction::IntegerAttribute, if not self.containingPackage.classifier->select( not literal->isEmpty())->isEmpty() then MClassifierAction::LiteralAttribute else null endif, MClassifierAction::RealAttribute, MClassifierAction::DateAttribute, MClassifierAction::UnaryReference, MClassifierAction::NaryReference,MClassifierAction::UnaryContainment, MClassifierAction::NaryContainment,MClassifierAction::OperationOneParameter, MClassifierAction::OperationTwoParameters, MClassifierAction::CompleteSemantics} 
		  else OrderedSet{} endif in
		let withLabelAndConstraintAction:OrderedSet(MClassifierAction) =
		if self.literal->isEmpty() then
		   if self.annotations.oclIsUndefined() 
		  then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)
		else if self.annotations.label.oclIsUndefined()
		  then classActions->prepend(MClassifierAction::Label)->prepend(MClassifierAction::Constraint)
		  else  classActions->prepend(MClassifierAction::Constraint) endif endif 
		else classActions endif in
		  let intoAbstractOrConcrete: OrderedSet(MClassifierAction) =
		if self.literal->isEmpty() and (self.ownedProperty->notEmpty() or self.enforcedClass=true)
		then withLabelAndConstraintAction->prepend(MClassifierAction::GroupOfProperties)
		else  withLabelAndConstraintAction->append(MClassifierAction::Literal) endif in
		let resolveContainer:OrderedSet(MClassifierAction) =
		if self.literal->isEmpty()
		then if self.abstractClass = true
		 then intoAbstractOrConcrete->prepend(MClassifierAction::IntoConcreteClass)->prepend(MClassifierAction::Do) 
		 else intoAbstractOrConcrete->prepend(MClassifierAction::IntoAbstractClass)->prepend(MClassifierAction::Do) 
		endif
		else intoAbstractOrConcrete->prepend(MClassifierAction::Do)
		endif in
		 if self.resolveContainerAction = true
		then if resolveContainer->includes(MClassifierAction::Label)
			then resolveContainer->insertAt(8, MClassifierAction::ResolveIntoContainer)
			else resolveContainer->insertAt(7, MClassifierAction::ResolveIntoContainer)
			endif
		else resolveContainer
		endif
		
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_doAction_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__DO_ACTION, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifierAction> result = new ArrayList<MClassifierAction>();
				Collection<? extends MClassifierAction> superResult = (Collection<? extends MClassifierAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MClassifierImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Resolve Container Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addResolveContainerActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Resolve Container Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_resolveContainerAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_resolveContainerAction_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__RESOLVE_CONTAINER_ACTION,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Abstract Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Abstract Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_abstractDoAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_abstractDoAction_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ABSTRACT_DO_ACTION, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Object References feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectReferencesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object References feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_objectReferences_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_objectReferences_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__OBJECT_REFERENCES, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Nearest Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNearestInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Nearest Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_nearestInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_nearestInstance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__NEAREST_INSTANCE, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Strict Nearest Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrictNearestInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Strict Nearest Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_strictNearestInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_strictNearestInstance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__STRICT_NEAREST_INSTANCE,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Strict Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrictInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Strict Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_strictInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_strictInstance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__STRICT_INSTANCE, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Intelligent Nearest Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntelligentNearestInstancePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Intelligent Nearest Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_intelligentNearestInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_intelligentNearestInstance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTELLIGENT_NEAREST_INSTANCE,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Object Unreferenced feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectUnreferencedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Object Unreferenced feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_objectUnreferenced_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_objectUnreferenced_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__OBJECT_UNREFERENCED, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Outstanding To Copy Objects feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOutstandingToCopyObjectsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Outstanding To Copy Objects feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_outstandingToCopyObjects_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_outstandingToCopyObjects_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__OUTSTANDING_TO_COPY_OBJECTS,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Property Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyInstancesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Property Instances feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_propertyInstances_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_propertyInstances_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__PROPERTY_INSTANCES, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Object Reference Property Instances feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addObjectReferencePropertyInstancesPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Object Reference Property Instances feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_objectReferencePropertyInstances_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_objectReferencePropertyInstances_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__OBJECT_REFERENCE_PROPERTY_INSTANCES,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Intelligent Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntelligentInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Intelligent Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_intelligentInstance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_intelligentInstance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTELLIGENT_INSTANCE, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentObjectRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containment Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainmentHierarchyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containment Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_containmentHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_containmentHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__CONTAINMENT_HIERARCHY, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Strict All Classes Contained In feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrictAllClassesContainedInPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Strict All Classes Contained In feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_strictAllClassesContainedIn_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_strictAllClassesContainedIn_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__STRICT_ALL_CLASSES_CONTAINED_IN,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Strict Containment Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStrictContainmentHierarchyPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Strict Containment Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_strictContainmentHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_strictContainmentHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__STRICT_CONTAINMENT_HIERARCHY,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Intelligent All Classes Contained In feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntelligentAllClassesContainedInPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Intelligent All Classes Contained In feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_intelligentAllClassesContainedIn_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_intelligentAllClassesContainedIn_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTELLIGENT_ALL_CLASSES_CONTAINED_IN,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Intelligent Containment Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntelligentContainmentHierarchyPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Intelligent Containment Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_intelligentContainmentHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_intelligentContainmentHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTELLIGENT_CONTAINMENT_HIERARCHY,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Instantiation Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInstantiationHierarchyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Instantiation Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_instantiationHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_instantiationHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INSTANTIATION_HIERARCHY,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Instantiation Property Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInstantiationPropertyHierarchyPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Instantiation Property Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_instantiationPropertyHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_instantiationPropertyHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INSTANTIATION_PROPERTY_HIERARCHY,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Intelligent Instantiation Property Hierarchy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntelligentInstantiationPropertyHierarchyPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Intelligent Instantiation Property Hierarchy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MClassifier_intelligentInstantiationPropertyHierarchy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_intelligentInstantiationPropertyHierarchy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTELLIGENT_INSTANTIATION_PROPERTY_HIERARCHY,
				false, false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Classescontainedin feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addClassescontainedinPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Classescontainedin feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_classescontainedin_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_classescontainedin_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__CLASSESCONTAINEDIN, false,
				false, false, null,
				getString(
						"_UI_DefaultContainmentClassifierRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Can Apply Default Containment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCanApplyDefaultContainmentPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Can Apply Default Containment feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_canApplyDefaultContainment_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_canApplyDefaultContainment_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_DefaultContainmentValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Properties As Text feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertiesAsTextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Properties As Text feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_propertiesAsText_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_propertiesAsText_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__PROPERTIES_AS_TEXT, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SummaryAsTextPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Semantics As Text feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSemanticsAsTextPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Semantics As Text feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_semanticsAsText_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_semanticsAsText_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__SEMANTICS_AS_TEXT, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SummaryAsTextPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Json Schema feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedJsonSchemaPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Json Schema feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_derivedJsonSchema_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_derivedJsonSchema_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__DERIVED_JSON_SCHEMA, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_JSONSchemaPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Super Type Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperTypePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Super Type Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_superTypePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_superTypePackage_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__SUPER_TYPE_PACKAGE, true,
				false, true, null, getString("_UI_AdditionalPropertyCategory"),
				null));
	}

	/**
	 * This adds a property descriptor for the Super Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSuperTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Super Type feature.
		 * The list of possible choices is constraint by OCL   not (trg = self) 
		and
		trg.kind = ClassifierKind::ClassType
		and
		trg.selectableClassifier(superType, superTypePackage)
		
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_superType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_superType_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__SUPER_TYPE, true, false,
				true, null, getString("_UI_AdditionalPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MClassifierImpl) object)
							.evalSuperTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Instance feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addInstancePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Instance feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_instance_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_instance_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INSTANCE, false, false,
				false, null, getString("_UI_AdditionalPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MClassifier_kind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_kind_feature", "_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_KindPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Enforced Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEnforcedClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Enforced Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_enforcedClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_enforcedClass_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ENFORCED_CLASS, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Abstract Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAbstractClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Abstract Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_abstractClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_abstractClass_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ABSTRACT_CLASS, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Internal EClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInternalEClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Internal EClassifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_internalEClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_internalEClassifier_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__INTERNAL_ECLASSIFIER, true,
				false, true, null,
				getString("_UI_RelationtoECorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_containingPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_containingPackage_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__CONTAINING_PACKAGE, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Represents Core Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addRepresentsCoreTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Represents Core Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_representsCoreType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_representsCoreType_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__REPRESENTS_CORE_TYPE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the All Property Groups feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllPropertyGroupsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All Property Groups feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_allPropertyGroups_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_allPropertyGroups_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ALL_PROPERTY_GROUPS, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EInterface feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEInterfacePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EInterface feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_eInterface_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_eInterface_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__EINTERFACE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Operation As Id Property Error feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperationAsIdPropertyErrorPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Operation As Id Property Error feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_operationAsIdPropertyError_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_operationAsIdPropertyError_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Is Class By Structure feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsClassByStructurePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Class By Structure feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_isClassByStructure_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_isClassByStructure_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__IS_CLASS_BY_STRUCTURE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ForClassesPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}
	
	@Override
	public Command createCommand(Object object, EditingDomain domain, Class<? extends Command> commandClass, CommandParameter commandParameter) {
		CompoundCommand result = new CompoundCommand();
		result.append(super.createCommand(object, domain, commandClass, commandParameter));
		if (commandClass == RemoveCommand.class /*&& commandParameter.getEStructuralFeature() instanceof EReference*/) {
			Optional.ofNullable(createEnforcedClassEnablingCommand(domain, (MClassifier) object)).ifPresent(result::append);
	    }
		return result;
	}
	
	/*
	 * @see MCore-38 for additional details
	 */
	private Command createEnforcedClassEnablingCommand(EditingDomain domain, MClassifier owner) {
		MClassifier classifier = (MClassifier) owner;
		boolean enforcedClass = ClassifierKind.CLASS_TYPE == classifier.getKind() && !classifier.isSetEnforcedClass();
		return enforcedClass ? createSetCommand(domain, owner, McorePackage.eINSTANCE.getMClassifier_EnforcedClass(), true) : null;
	}

	/**
	 * This adds a property descriptor for the Toggle Super Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addToggleSuperTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Toggle Super Type feature.
		 * The list of possible choices is constraint by OCL   not (trg = self) 
		and
		trg.kind = ClassifierKind::ClassType
		and
		trg.selectableClassifier(superType, superTypePackage)
		
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_toggleSuperType_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_toggleSuperType_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__TOGGLE_SUPER_TYPE, true,
				false, true, null,
				getString("_UI_ForClassesSupertypeRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {

						iterator.remove();
						continue;
					}
					if (!((MClassifierImpl) object)
							.evalToggleSuperTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Ordering Strategy feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOrderingStrategyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Ordering Strategy feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_orderingStrategy_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_orderingStrategy_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ORDERING_STRATEGY, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ForClassesPropertiesRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Test All Properties feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTestAllPropertiesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Test All Properties feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_testAllProperties_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_testAllProperties_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__TEST_ALL_PROPERTIES, false,
				false, false, null,
				getString("_UI_ForClassesPropertiesRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Id Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Id Property feature.
		 * The list of possible choices is constraint by OCL self.allFeatures()->includes(trg)
		and ( trg.kind<>PropertyKind::Operation)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MClassifier_idProperty_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MClassifier_idProperty_feature",
						"_UI_MClassifier_type"),
				McorePackage.Literals.MCLASSIFIER__ID_PROPERTY, true, false,
				true, null,
				getString("_UI_ForClassesIdPropertiesRelatedPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MProperty> iterator = result.iterator(); iterator
						.hasNext();) {
					MProperty trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MClassifierImpl) object)
							.evalIdPropertyChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(McorePackage.Literals.MCLASSIFIER__LITERAL);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MClassifier.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		MClassifier c = (MClassifier) object;
		if (c == null) {
			return overlayImage(object,
					getResourceLocator().getImage("full/obj16/MClassifier"));
		} else {
			switch (c.getKind()) {
			case CLASS_TYPE:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EClass"));
			case ENUMERATION:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EEnum"));
			default:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EDataType"));
			}
		}
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MClassifierImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MClassifier.class)) {
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_STRING:
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_DATA_TYPE:
		case McorePackage.MCLASSIFIER__HAS_SIMPLE_MODELING_TYPE:
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE_IS_CORRECT:
		case McorePackage.MCLASSIFIER__SIMPLE_TYPE:
		case McorePackage.MCLASSIFIER__ABSTRACT_CLASS:
		case McorePackage.MCLASSIFIER__ENFORCED_CLASS:
		case McorePackage.MCLASSIFIER__KIND:
		case McorePackage.MCLASSIFIER__REPRESENTS_CORE_TYPE:
		case McorePackage.MCLASSIFIER__EINTERFACE:
		case McorePackage.MCLASSIFIER__OPERATION_AS_ID_PROPERTY_ERROR:
		case McorePackage.MCLASSIFIER__IS_CLASS_BY_STRUCTURE:
		case McorePackage.MCLASSIFIER__ORDERING_STRATEGY:
		case McorePackage.MCLASSIFIER__DO_ACTION:
		case McorePackage.MCLASSIFIER__RESOLVE_CONTAINER_ACTION:
		case McorePackage.MCLASSIFIER__ABSTRACT_DO_ACTION:
		case McorePackage.MCLASSIFIER__CAN_APPLY_DEFAULT_CONTAINMENT:
		case McorePackage.MCLASSIFIER__PROPERTIES_AS_TEXT:
		case McorePackage.MCLASSIFIER__SEMANTICS_AS_TEXT:
		case McorePackage.MCLASSIFIER__DERIVED_JSON_SCHEMA:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MCLASSIFIER__LITERAL:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(
				createChildParameter(McorePackage.Literals.MCLASSIFIER__LITERAL,
						McoreFactory.eINSTANCE.createMLiteral()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
