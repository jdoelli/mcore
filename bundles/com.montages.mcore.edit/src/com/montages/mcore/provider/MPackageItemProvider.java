/**
 */
package com.montages.mcore.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.command.CommandParameter;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MPackageAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.impl.MPackageImpl;
import com.montages.mcore.objects.MResourceFolderAction;
import com.montages.mcore.objects.ObjectsPackage;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MPackage}
 * object. <!-- begin-user-doc --> <!-- end-user-doc -->
 * 
 * @generated
 */
public class MPackageItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public MPackageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc --> Override SetCommand for XUpdate execution <!--
	 * end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		org.xocl.semantics.XUpdate myUpdate = null;
		if (owner instanceof MPackage) {
			MPackage ownerMPackage = (MPackage) owner;
			if (feature == McorePackage.Literals.MPACKAGE__DO_ACTION) {
			MPackageAction action = (MPackageAction) value;


			if (action == MPackageAction.OPEN_EDITOR) {
			if (!((MPackage) owner).getContainingComponent().getResourceFolder()
					.isEmpty())
				for (Adapter adapt : ((MPackage) owner).getContainingComponent()
						.getResourceFolder().get(0).eAdapters())
					if (adapt instanceof IEditingDomainItemProvider) {
						CommandParameter p = new CommandParameter(
								((MPackage) owner).getContainingComponent()
										.getResourceFolder().get(0),
								ObjectsPackage.eINSTANCE
										.getMResourceFolder_DoAction(),
								MResourceFolderAction.OPEN_EDITOR, index);
						return ((IEditingDomainItemProvider) adapt)
								.createCommand(
										((MPackage) owner)
												.getContainingComponent()
												.getResourceFolder().get(0),
										domain, SetCommand.class, p);
					}

		}
			}
			else if (feature == McorePackage.Literals.MPACKAGE__GENERATE_JSON_SCHEMA){

				if (feature == McorePackage.eINSTANCE.getMPackage_DoAction())
					myUpdate = ownerMPackage.doAction$Update(
							(com.montages.mcore.MPackageAction) value);

				if (feature == McorePackage.eINSTANCE
						.getMPackage_GenerateJsonSchema())
					myUpdate = ownerMPackage
							.generateJsonSchema$Update((java.lang.Boolean) value);
				
			}
			return myUpdate == null
					? super.createSetCommand(domain, owner, feature, value, index)
					: myUpdate.getContainingTransition()
							.interpreteTransitionAsCommand();
		}
				return super.createSetCommand(domain, owner, feature, value, index);
	};

	/**
	 * This returns the property descriptors for the adapted class. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addUsePackageContentOnlyWithExplicitFilterPropertyDescriptor(
					object);
			addResourceRootClassPropertyDescriptor(object);
			addSpecialNsURIPropertyDescriptor(object);
			addSpecialNsPrefixPropertyDescriptor(object);
			addSpecialFileNamePropertyDescriptor(object);
			addUseUUIDPropertyDescriptor(object);
			addUuidAttributeNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addParentPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingComponentPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addRepresentsCorePackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllSubpackagesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addQualifiedNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIsRootPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInternalEPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedNsURIPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedStandardNsURIPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedNsPrefixPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDerivedJavaPackageNamePropertyDescriptor(object);
			}
			addDoActionPropertyDescriptor(object);
			addNamePrefixPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDerivedNamePrefixPropertyDescriptor(object);
			}
			addNamePrefixScopePropertyDescriptor(object);
			addDerivedJsonSchemaPropertyDescriptor(object);
			addGenerateJsonSchemaPropertyDescriptor(object);
			addGeneratedJsonSchemaPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNamePrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_namePrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_namePrefix_feature", "_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__NAME_PREFIX, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Name Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedNamePrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Name Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedNamePrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedNamePrefix_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_NAME_PREFIX, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Name Prefix Scope feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addNamePrefixScopePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name Prefix Scope feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_namePrefixScope_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_namePrefixScope_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__NAME_PREFIX_SCOPE, false, false,
				false, null,
				getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Derived Json Schema feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedJsonSchemaPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Json Schema feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedJsonSchema_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedJsonSchema_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_JSON_SCHEMA, false,
				true, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_JSONSchemaPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Generate Json Schema feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addGenerateJsonSchemaPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Generate Json Schema feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_generateJsonSchema_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_generateJsonSchema_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__GENERATE_JSON_SCHEMA, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_JSONSchemaPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Generated Json Schema feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addGeneratedJsonSchemaPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Generated Json Schema
		 * feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_generatedJsonSchema_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_generatedJsonSchema_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__GENERATED_JSON_SCHEMA, true,
				true, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_JSONSchemaPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_doAction_feature", "_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Use Package Content Only With
	 * Explicit Filter feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addUsePackageContentOnlyWithExplicitFilterPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Use Package Content Only With
		 * Explicit Filter feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPackage_usePackageContentOnlyWithExplicitFilter_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_usePackageContentOnlyWithExplicitFilter_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER,
				true, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_AdditionalPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Special Ns URI feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSpecialNsURIPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special Ns URI feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_specialNsURI_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_specialNsURI_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__SPECIAL_NS_URI, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Special Ns Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSpecialNsPrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special Ns Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_specialNsPrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_specialNsPrefix_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__SPECIAL_NS_PREFIX, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Special File Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addSpecialFileNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special File Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_specialFileName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_specialFileName_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__SPECIAL_FILE_NAME, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Use UUID feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addUseUUIDPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Use UUID feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MPackage_useUUID_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_useUUID_feature", "_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__USE_UUID, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Uuid Attribute Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addUuidAttributeNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Uuid Attribute Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_uuidAttributeName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_uuidAttributeName_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__UUID_ATTRIBUTE_NAME, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Parent feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addParentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Parent feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MPackage_parent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_parent_feature", "_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__PARENT, false, false, false,
				null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Package feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addContainingPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_containingPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_containingPackage_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__CONTAINING_PACKAGE, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Component feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Component feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_containingComponent_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_containingComponent_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__CONTAINING_COMPONENT, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Represents Core Package feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addRepresentsCorePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Represents Core Package
		 * feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_representsCorePackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_representsCorePackage_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__REPRESENTS_CORE_PACKAGE, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the All Subpackages feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addAllSubpackagesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All Subpackages feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_allSubpackages_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_allSubpackages_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__ALL_SUBPACKAGES, false, false,
				false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Qualified Name feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addQualifiedNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Qualified Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_qualifiedName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_qualifiedName_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__QUALIFIED_NAME, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Is Root Package feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addIsRootPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Root Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_isRootPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_isRootPackage_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__IS_ROOT_PACKAGE, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Internal EPackage feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addInternalEPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Internal EPackage feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_internalEPackage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_internalEPackage_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__INTERNAL_EPACKAGE, true, false,
				true, null, getString("_UI_RelationToECorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Ns URI feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedNsURIPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Ns URI feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedNsURI_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedNsURI_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_NS_URI, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_URIPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Standard Ns URI feature.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedStandardNsURIPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Standard Ns URI
		 * feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedStandardNsURI_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedStandardNsURI_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_STANDARD_NS_URI, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_URIPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Ns Prefix feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedNsPrefixPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Ns Prefix feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedNsPrefix_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedNsPrefix_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_NS_PREFIX, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_URIPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Java Package Name
	 * feature. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addDerivedJavaPackageNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Java Package Name
		 * feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_derivedJavaPackageName_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_derivedJavaPackageName_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__DERIVED_JAVA_PACKAGE_NAME,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_URIPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Resource Root Class feature. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected void addResourceRootClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Resource Root Class feature.
		 * The list of possible choices is constraint by OCL
		 * self->union(allSubpackages)->includes(trg.containingPackage) and not
		 * (trg.abstractClass) and trg.kind = ClassifierKind::ClassType
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPackage_resourceRootClass_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPackage_resourceRootClass_feature",
						"_UI_MPackage_type"),
				McorePackage.Literals.MPACKAGE__RESOURCE_ROOT_CLASS, true,
				false, true, null, getString("_UI_AdditionalPropertyCategory"),
				null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MPackageImpl) object)
							.evalResourceRootClassChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to
	 * deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand},
	 * {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in
	 * {@link #createCommand}. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION);
			childrenFeatures.add(McorePackage.Literals.MPACKAGE__CLASSIFIER);
			childrenFeatures.add(McorePackage.Literals.MPACKAGE__SUB_PACKAGE);
			childrenFeatures
					.add(McorePackage.Literals.MPACKAGE__PACKAGE_ANNOTATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper
		// feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MPackage.gif. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object,
				getResourceLocator().getImage("full/obj16/EPackage"));
	}

	/**
	 * This returns the label text for the adapted class. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MPackageImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to
	 * update any cached children and by creating a viewer notification, which
	 * it passes to {@link #fireNotifyChanged}. <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MPackage.class)) {
		case McorePackage.MPACKAGE__USE_PACKAGE_CONTENT_ONLY_WITH_EXPLICIT_FILTER:
		case McorePackage.MPACKAGE__SPECIAL_NS_URI:
		case McorePackage.MPACKAGE__SPECIAL_NS_PREFIX:
		case McorePackage.MPACKAGE__SPECIAL_FILE_NAME:
		case McorePackage.MPACKAGE__USE_UUID:
		case McorePackage.MPACKAGE__UUID_ATTRIBUTE_NAME:
		case McorePackage.MPACKAGE__REPRESENTS_CORE_PACKAGE:
		case McorePackage.MPACKAGE__QUALIFIED_NAME:
		case McorePackage.MPACKAGE__IS_ROOT_PACKAGE:
		case McorePackage.MPACKAGE__DERIVED_NS_URI:
		case McorePackage.MPACKAGE__DERIVED_STANDARD_NS_URI:
		case McorePackage.MPACKAGE__DERIVED_NS_PREFIX:
		case McorePackage.MPACKAGE__DERIVED_JAVA_PACKAGE_NAME:
		case McorePackage.MPACKAGE__DO_ACTION:
		case McorePackage.MPACKAGE__NAME_PREFIX:
		case McorePackage.MPACKAGE__DERIVED_NAME_PREFIX:
		case McorePackage.MPACKAGE__DERIVED_JSON_SCHEMA:
		case McorePackage.MPACKAGE__GENERATE_JSON_SCHEMA:
		case McorePackage.MPACKAGE__GENERATED_JSON_SCHEMA:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MPACKAGE__GENERAL_ANNOTATION:
		case McorePackage.MPACKAGE__CLASSIFIER:
		case McorePackage.MPACKAGE__SUB_PACKAGE:
		case McorePackage.MPACKAGE__PACKAGE_ANNOTATIONS:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s
	 * describing the children that can be created under this object. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION,
				AnnotationsFactory.eINSTANCE.createMGeneralAnnotation()));

		newChildDescriptors.add(
				createChildParameter(McorePackage.Literals.MPACKAGE__CLASSIFIER,
						McoreFactory.eINSTANCE.createMClassifier()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPACKAGE__SUB_PACKAGE,
				McoreFactory.eINSTANCE.createMPackage()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPACKAGE__PACKAGE_ANNOTATIONS,
				AnnotationsFactory.eINSTANCE.createMPackageAnnotations()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
