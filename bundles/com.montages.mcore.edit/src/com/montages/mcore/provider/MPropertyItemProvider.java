/**
 */
package com.montages.mcore.provider;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CompoundCommand;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage.Registry;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.XUpdate;
import org.xocl.semantics.commands.ChangeCommandWithOperationCall;
import org.xocl.semantics.commands.CompoundCommandFocus;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MProperty;
import com.montages.mcore.MPropertyAction;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;
import com.montages.mcore.impl.MPropertyImpl;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MProperty} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MPropertyItemProvider extends MExplicitlyTypedAndNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(
			org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner,
			org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		if (feature == McorePackage.eINSTANCE.getMProperty_DoAction())
			if (owner instanceof MProperty) {

				MPropertyAction myValue = (MPropertyAction) value;
				XUpdate myUpdate = ((MProperty) owner).doActionUpdate(myValue);

				Command interpretedCommand = null;
				List<Command> cmdList = null;
				if (myUpdate != null) {
					interpretedCommand = myUpdate.getContainingTransition()
							.interpreteTransitionAsCommand();
					if (interpretedCommand instanceof CompoundCommand) {
						cmdList = ((CompoundCommand) interpretedCommand)
								.getCommandList();
						if (cmdList.get(cmdList.size()
								- 1) instanceof ChangeCommandWithOperationCall) {
							final ChangeCommandWithOperationCall changeCommand = (ChangeCommandWithOperationCall) cmdList
									.get(cmdList.size() - 1);
							return new CompoundCommandFocus(cmdList,
									changeCommand);
						}

					}
					return interpretedCommand;

				}
			}
		return super.createSetCommand(domain, owner, feature, value, index);

	};

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addOppositeDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOppositePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainmentPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addHasStoragePropertyDescriptor(object);
			}
			addChangeablePropertyDescriptor(object);
			addPersistedPropertyDescriptor(object);
			addPropertyBehaviorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addIsOperationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIsAttributePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addIsReferencePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addTypeDefinitionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addUseCoreTypesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addELabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addENameForELabelPropertyDescriptor(object);
			}
			addOrderWithinGroupPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addIdPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addInternalEStructuralFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDirectSemanticsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSemanticsSpecializationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAllSignaturesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addDirectOperationSemanticsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingPropertiesContainerPropertyDescriptor(object);
			}
			addEKeyForPathPropertyDescriptor(object);
			addNotUnsettablePropertyDescriptor(object);
			addAttributeForEIdPropertyDescriptor(object);
			addEDefaultValueLiteralPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBehaviorActionsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLayoutActionsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addArityActionsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addSimpleTypeActionsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAnnotationActionsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addMoveActionsPropertyDescriptor(object);
			}
			addDerivedJsonSchemaPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 * The list of possible choices is constructed by OCL     let arityActionsInput: OrderedSet(MPropertyAction) = behaviorActions->prepend(MPropertyAction::Do) in
		let simpleTypeActionsInput: OrderedSet(MPropertyAction) = 
		arityActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = arityActionsInput | ac->append(it)) in
		let annotationActionsInput: OrderedSet(MPropertyAction) = 
		simpleTypeActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = simpleTypeActionsInput | ac->append(it)) in
		let layoutActionsInput: OrderedSet(MPropertyAction) = 
		annotationActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = annotationActionsInput | ac->append(it))
		->append(MPropertyAction::Highlight) in
		
		let moveActionsInput: OrderedSet(MPropertyAction) = 
		layoutActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = layoutActionsInput | ac->append(it)) in 
		moveActions->iterate(it: MPropertyAction; ac: OrderedSet(MPropertyAction) = moveActionsInput | ac->append(it))->append(MPropertyAction::AddUpdateAnnotation)
		
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_doAction_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_doAction_feature", "_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__DO_ACTION, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MPropertyAction> result = new ArrayList<MPropertyAction>();
				Collection<? extends MPropertyAction> superResult = (Collection<? extends MPropertyAction>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyImpl) object)
						.evalDoActionChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Behavior Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBehaviorActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Behavior Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_behaviorActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_behaviorActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__BEHAVIOR_ACTIONS, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Layout Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLayoutActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Layout Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_layoutActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_layoutActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__LAYOUT_ACTIONS, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Arity Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addArityActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Arity Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_arityActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_arityActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ARITY_ACTIONS, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Simple Type Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypeActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_simpleTypeActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_simpleTypeActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__SIMPLE_TYPE_ACTIONS, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Annotation Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAnnotationActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Annotation Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_annotationActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_annotationActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ANNOTATION_ACTIONS, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Move Actions feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addMoveActionsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Move Actions feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_moveActions_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_moveActions_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__MOVE_ACTIONS, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Derived Json Schema feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDerivedJsonSchemaPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Derived Json Schema feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_derivedJsonSchema_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_derivedJsonSchema_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__DERIVED_JSON_SCHEMA, false,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_JSONSchemaPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Opposite Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void addOppositeDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Opposite Definition feature.
		 * The list of possible choices is constructed by OCL if self.kind <> PropertyKind::Reference 
		then OrderedSet{}
		else if self.type.oclIsUndefined() 
		then OrderedSet{}
		else self.type.allProperties()
		 ->select(p:MProperty|p.type=self.containingClassifier and p<>self and 
		                  (if self.containment then not p.containment else true endif) 
		                  )
		endif 
		endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_oppositeDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_oppositeDefinition_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__OPPOSITE_DEFINITION, true,
				false, false, null, getString("_UI_BehaviorPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyImpl) object)
						.evalOppositeDefinitionChoiceConstruction(result);

				//	OLD: result.remove(null);
				//New start	
				if (!result.contains(null)) {
					result.add(null);
				}

				//New end

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Opposite feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOppositePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Opposite feature.
		 * The list of possible choices is constructed by OCL if self.kind <> PropertyKind::Reference 
		then OrderedSet{}
		else if self.type.oclIsUndefined() 
		then OrderedSet{}
		else self.type.allProperties()
		 ->select(p:MProperty|p.type=self.containingClassifier and p<>self and 
		              (if self.containment then not p.containment else true endif) 
		              )
		endif 
		endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_opposite_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_opposite_feature", "_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__OPPOSITE, true, false, false,
				null, getString("_UI_BehaviorPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyImpl) object)
						.evalOppositeChoiceConstruction(result);

				if (!result.contains(null)) {
					result.add(null);
				}

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Is Operation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Operation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_isOperation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_isOperation_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__IS_OPERATION, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorDerivedHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Is Attribute feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsAttributePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Attribute feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_isAttribute_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_isAttribute_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__IS_ATTRIBUTE, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorDerivedHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Is Reference feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReferencePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Reference feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_isReference_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_isReference_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__IS_REFERENCE, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorDerivedHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Property Behavior feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPropertyBehaviorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Property Behavior feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_propertyBehavior_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_propertyBehavior_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__PROPERTY_BEHAVIOR, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MProperty_kind_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_kind_feature", "_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorDerivedHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainmentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containment feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_containment_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_containment_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__CONTAINMENT, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Has Storage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addHasStoragePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Has Storage feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_hasStorage_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_hasStorage_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__HAS_STORAGE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Changeable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addChangeablePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Changeable feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_changeable_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_changeable_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__CHANGEABLE, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Persisted feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addPersistedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Persisted feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_persisted_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_persisted_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__PERSISTED, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_BehaviorPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Type Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addTypeDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Type Definition feature.
		 * The list of possible choices is constraint by OCL self.selectableType(trg)
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_typeDefinition_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_typeDefinition_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__TYPE_DEFINITION, true, false,
				true, null, getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> result = new ArrayList<MClassifier>();
				Collection<? extends MClassifier> superResult = (Collection<? extends MClassifier>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<MClassifier> iterator = result
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (trg == null) {

						iterator.remove();
						continue;
					}
					if (!((MPropertyImpl) object)
							.evalTypeDefinitionChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Use Core Types feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseCoreTypesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Use Core Types feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_useCoreTypes_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_useCoreTypes_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__USE_CORE_TYPES, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_TypingPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ELabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addELabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ELabel feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MProperty_eLabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_eLabel_feature", "_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ELABEL, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EName For ELabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addENameForELabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EName For ELabel feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_eNameForELabel_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_eNameForELabel_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ENAME_FOR_ELABEL, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NamingandLabelsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Order Within Group feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOrderWithinGroupPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Order Within Group feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_orderWithinGroup_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_orderWithinGroup_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ORDER_WITHIN_GROUP, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_OrderPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Id feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIdPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Id feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(), getString("_UI_MProperty_id_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_id_feature", "_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ID, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_NumericIDPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Internal EStructural Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addInternalEStructuralFeaturePropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Internal EStructural Feature feature.
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_internalEStructuralFeature_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_internalEStructuralFeature_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE,
				true, false, true, null,
				getString("_UI_RelationshiptoECorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<EStructuralFeature> result = new ArrayList<EStructuralFeature>();
				Collection<? extends EStructuralFeature> superResult = (Collection<? extends EStructuralFeature>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				List<EObject> eObjects = (List<EObject>) (List<?>) new LinkedList<Object>(
						result);
				Resource resource = ((EObject) object).eResource();
				if (resource != null) {
					ResourceSet resourceSet = resource.getResourceSet();
					if (resourceSet != null) {
						Collection<EObject> visited = new HashSet<EObject>(
								eObjects);
						Registry packageRegistry = resourceSet
								.getPackageRegistry();
						for (Iterator<String> i = packageRegistry.keySet()
								.iterator(); i.hasNext();) {
							collectReachableObjectsOfType(visited, eObjects,
									packageRegistry.getEPackage(i.next()),
									McorePackage.Literals.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE
											.getEType());
						}
						result = (List<EStructuralFeature>) (List<?>) eObjects;
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Direct Semantics feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectSemanticsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Direct Semantics feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_directSemantics_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_directSemantics_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__DIRECT_SEMANTICS, false, false,
				false, null, getString("_UI_SemanticsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Semantics Specialization feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSemanticsSpecializationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Semantics Specialization feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_semanticsSpecialization_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_semanticsSpecialization_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__SEMANTICS_SPECIALIZATION,
				false, false, false, null,
				getString("_UI_SemanticsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the All Signatures feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllSignaturesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the All Signatures feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_allSignatures_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_allSignatures_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ALL_SIGNATURES, false, false,
				false, null, getString("_UI_SemanticsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Direct Operation Semantics feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDirectOperationSemanticsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Direct Operation Semantics feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_directOperationSemantics_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_directOperationSemantics_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__DIRECT_OPERATION_SEMANTICS,
				false, false, false, null,
				getString("_UI_SemanticsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_containingClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_containingClassifier_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__CONTAINING_CLASSIFIER, false,
				false, false, null, getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Properties Container feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingPropertiesContainerPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Containing Properties Container feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MProperty_containingPropertiesContainer_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_containingPropertiesContainer_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__CONTAINING_PROPERTIES_CONTAINER,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EKey For Path feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEKeyForPathPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EKey For Path feature.
		 * The list of possible choices is constructed by OCL if self.type.oclIsUndefined() 
		then OrderedSet{}
		else self.type.allFeaturesWithStorage()->select(p:MProperty|p.isAttribute) endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_eKeyForPath_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_eKeyForPath_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__EKEY_FOR_PATH, true, false,
				false, null,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MProperty> result = new ArrayList<MProperty>();
				Collection<? extends MProperty> superResult = (Collection<? extends MProperty>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				result = ((MPropertyImpl) object)
						.evalEKeyForPathChoiceConstruction(result);

				result.remove(null);

				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the Not Unsettable feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNotUnsettablePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Not Unsettable feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_notUnsettable_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_notUnsettable_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__NOT_UNSETTABLE, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Attribute For EId feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAttributeForEIdPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Attribute For EId feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_attributeForEId_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_attributeForEId_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__ATTRIBUTE_FOR_EID, true, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the EDefault Value Literal feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEDefaultValueLiteralPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EDefault Value Literal feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MProperty_eDefaultValueLiteral_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MProperty_eDefaultValueLiteral_feature",
						"_UI_MProperty_type"),
				McorePackage.Literals.MPROPERTY__EDEFAULT_VALUE_LITERAL, true,
				false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION);
			childrenFeatures
					.add(McorePackage.Literals.MPROPERTY__OPERATION_SIGNATURE);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MProperty.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object getImage(Object object) {
		MProperty c = (MProperty) object;
		if (c == null) {
			return overlayImage(object,
					getResourceLocator().getImage("full/obj16/MProperty"));
		} else {
			switch (c.getKind()) {
			case OPERATION:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EOperation"));
			case REFERENCE:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EReference"));
			case ATTRIBUTE:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/EAttribute"));
			default:
				return overlayImage(object,
						getResourceLocator().getImage("full/obj16/MProperty"));
			}
		}
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MPropertyImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MProperty.class)) {
		case McorePackage.MPROPERTY__CONTAINMENT:
		case McorePackage.MPROPERTY__HAS_STORAGE:
		case McorePackage.MPROPERTY__CHANGEABLE:
		case McorePackage.MPROPERTY__PERSISTED:
		case McorePackage.MPROPERTY__PROPERTY_BEHAVIOR:
		case McorePackage.MPROPERTY__IS_OPERATION:
		case McorePackage.MPROPERTY__IS_ATTRIBUTE:
		case McorePackage.MPROPERTY__IS_REFERENCE:
		case McorePackage.MPROPERTY__KIND:
		case McorePackage.MPROPERTY__USE_CORE_TYPES:
		case McorePackage.MPROPERTY__ELABEL:
		case McorePackage.MPROPERTY__ENAME_FOR_ELABEL:
		case McorePackage.MPROPERTY__ORDER_WITHIN_GROUP:
		case McorePackage.MPROPERTY__ID:
		case McorePackage.MPROPERTY__NOT_UNSETTABLE:
		case McorePackage.MPROPERTY__ATTRIBUTE_FOR_EID:
		case McorePackage.MPROPERTY__EDEFAULT_VALUE_LITERAL:
		case McorePackage.MPROPERTY__DO_ACTION:
		case McorePackage.MPROPERTY__BEHAVIOR_ACTIONS:
		case McorePackage.MPROPERTY__LAYOUT_ACTIONS:
		case McorePackage.MPROPERTY__ARITY_ACTIONS:
		case McorePackage.MPROPERTY__SIMPLE_TYPE_ACTIONS:
		case McorePackage.MPROPERTY__ANNOTATION_ACTIONS:
		case McorePackage.MPROPERTY__MOVE_ACTIONS:
		case McorePackage.MPROPERTY__DERIVED_JSON_SCHEMA:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MPROPERTY__GENERAL_ANNOTATION:
		case McorePackage.MPROPERTY__OPERATION_SIGNATURE:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION,
				AnnotationsFactory.eINSTANCE.createMGeneralAnnotation()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPROPERTY__OPERATION_SIGNATURE,
				McoreFactory.eINSTANCE.createMOperationSignature()));
	}

	/**
	 * This adds a property descriptor for the Type feature.
	 * The list of possible choices is constraint by OCL true
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag IPINS01
	 */
	@Override
	protected void addTypePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MExplicitlyTyped_type_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MExplicitlyTyped_type_feature",
						"_UI_MExplicitlyTyped_type"),
				McorePackage.Literals.MEXPLICITLY_TYPED__TYPE, true, false,
				true, null, getString("_UI_AdditionalPropertyCategory"), null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<MClassifier> choice = new ArrayList<MClassifier>(
						(Collection<? extends MClassifier>) super.getChoiceOfValues(
								object));
				for (Iterator<MClassifier> iterator = choice
						.iterator(); iterator.hasNext();) {
					MClassifier trg = iterator.next();
					if (!((MPropertyImpl) object)
							.evalTypeChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return choice;
			}
		});
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
