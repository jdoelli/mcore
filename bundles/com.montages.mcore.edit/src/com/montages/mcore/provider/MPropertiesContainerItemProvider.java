/**
 */
package com.montages.mcore.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;

import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsFactory;

/**
 * This is the item provider adapter for a {@link com.montages.mcore.MPropertiesContainer} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MPropertiesContainerItemProvider extends MNamedItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider,
		ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MPropertiesContainerItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addAllConstraintAnnotationsPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addContainingClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addOwnedPropertyPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addNrOfPropertiesAndSignaturesPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the All Constraint Annotations feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAllConstraintAnnotationsPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the All Constraint Annotations feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertiesContainer_allConstraintAnnotations_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesContainer_allConstraintAnnotations_feature",
						"_UI_MPropertiesContainer_type"),
				McorePackage.Literals.MPROPERTIES_CONTAINER__ALL_CONSTRAINT_ANNOTATIONS,
				false, false, false, null, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Containing Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addContainingClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Containing Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertiesContainer_containingClassifier_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesContainer_containingClassifier_feature",
						"_UI_MPropertiesContainer_type"),
				McorePackage.Literals.MPROPERTIES_CONTAINER__CONTAINING_CLASSIFIER,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Owned Property feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOwnedPropertyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Owned Property feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString("_UI_MPropertiesContainer_ownedProperty_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesContainer_ownedProperty_feature",
						"_UI_MPropertiesContainer_type"),
				McorePackage.Literals.MPROPERTIES_CONTAINER__OWNED_PROPERTY,
				false, false, false, null,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Nr Of Properties And Signatures feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNrOfPropertiesAndSignaturesPropertyDescriptor(
			Object object) {
		/*
		 * This adds a property descriptor for the Nr Of Properties And Signatures feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory)
						.getRootAdapterFactory(),
				getResourceLocator(),
				getString(
						"_UI_MPropertiesContainer_nrOfPropertiesAndSignatures_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MPropertiesContainer_nrOfPropertiesAndSignatures_feature",
						"_UI_MPropertiesContainer_type"),
				McorePackage.Literals.MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES,
				false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_StructuralPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(
			Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(
					McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION);
			childrenFeatures
					.add(McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY);
			childrenFeatures.add(
					McorePackage.Literals.MPROPERTIES_CONTAINER__ANNOTATIONS);
			childrenFeatures.add(
					McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY_GROUP);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object)
				.eContainingFeature();
		String containingFeatureName = (containingFeature == null ? ""
				: containingFeature.getName());

		String label = ((MPropertiesContainer) object).getName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0
				? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MPropertiesContainer.class)) {
		case McorePackage.MPROPERTIES_CONTAINER__NR_OF_PROPERTIES_AND_SIGNATURES:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), false, true));
			return;
		case McorePackage.MPROPERTIES_CONTAINER__GENERAL_ANNOTATION:
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY:
		case McorePackage.MPROPERTIES_CONTAINER__ANNOTATIONS:
		case McorePackage.MPROPERTIES_CONTAINER__PROPERTY_GROUP:
			fireNotifyChanged(new ViewerNotification(notification,
					notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(
			Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MMODEL_ELEMENT__GENERAL_ANNOTATION,
				AnnotationsFactory.eINSTANCE.createMGeneralAnnotation()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY,
				McoreFactory.eINSTANCE.createMProperty()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPROPERTIES_CONTAINER__ANNOTATIONS,
				AnnotationsFactory.eINSTANCE.createMClassifierAnnotations()));

		newChildDescriptors.add(createChildParameter(
				McorePackage.Literals.MPROPERTIES_CONTAINER__PROPERTY_GROUP,
				McoreFactory.eINSTANCE.createMPropertiesGroup()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !McoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
