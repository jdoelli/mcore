package com.montages.mcore.ui.dialogs;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.dialogs.IMessageProvider;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.Dependencies;

/**
 * Dialog showing the list of components to be converted to Ecore.
 * 
 * The list of components is calculated by {@link Dependencies} from a selected component.
 * The dialog offers the option to force the regeneration of the ecore files via a checkbox.  
 * 
 */
public class GenerateEcoreDialog extends TitleAreaDialog {

	private final MComponent component;
	private List<MComponent> targets = new ArrayList<MComponent>();
	private boolean forceGeneration = false;
	private TableViewer viewer;

	public GenerateEcoreDialog(Shell parentShell, MComponent component) {
		super(parentShell);
		this.component = component;
	}

	public boolean isForceGeneration() {
		return forceGeneration;
	}

	public List<MComponent> getTargets() {
		return targets;
	}

	@Override
	public void create() {
		super.create();
		setTitle("Generate Ecore");
		setMessage("List of components to be converted to Ecore.", IMessageProvider.INFORMATION);
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		Composite container = new Composite(area, SWT.NONE);
	    container.setLayoutData(new GridData(GridData.FILL_BOTH));
	    GridLayout layout = new GridLayout(1, false);
	    container.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
	    container.setLayout(layout);

	    final Table table = new Table(container, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			table.setLayoutData(data);
		}
		final Button forceGenerationBtn = new Button(container, SWT.CHECK);
	    forceGenerationBtn.setText("Force Regeneration");
	    forceGenerationBtn.addSelectionListener(new SelectionAdapter() {
	    	@Override
	    	public void widgetSelected(SelectionEvent e) {
	    		forceGeneration = forceGenerationBtn.isEnabled();
	    	}
		});

		viewer = new TableViewer(table);
		viewer.setLabelProvider(new LabelProvider() {
			@Override
			public Image getImage(Object element) {
				if (element instanceof MComponent) {
					return McoreUIPlugin.MCOMPONENT_IMAGE;
				}
				return super.getImage(element);
			}
			@Override
			public String getText(Object element) {
				if (element instanceof MComponent) {
					return ((MComponent) element).getDerivedURI();
				}
				return super.getText(element);
			}
		});
		viewer.setContentProvider(ArrayContentProvider.getInstance());

		Dependencies dependencies = Dependencies.compute(component);
		List<MComponent> sorted = dependencies.sort(); 
		viewer.setInput(sorted.toArray());
		getTargets().addAll(sorted);

		return area;
	}
	
}
