package com.montages.mcore.ui.workspace;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.ResourceUtil;
import org.osgi.service.prefs.BackingStoreException;

import com.montages.mcore.ui.McoreUIPlugin;

public class FileFunctions {

	private static final List<String> filters = Arrays.asList(new String[] { "META-INF", "model" });

	public static String[] collectNames(List<IResource> resources) {
		final String[] names = new String[resources.size()];

		for (int i = 0; i < resources.size(); i++) {
			IResource resource = resources.get(i);
			IProject project = resource.getProject();
			names[i] = project.getName() + "/" + resource.getName();
		}

		return names;
	}

	public static IFile getFile(URI uri) {
		IResource file = null;

		if (uri.isPlatformResource()) {
			file = ResourcesPlugin.getWorkspace().getRoot().findMember(uri.toPlatformString(true));
		}

		return file instanceof IFile ? (IFile) file : null;
	}

	public static IFile getFileFromResource(Resource resource) {
		if (resource == null)
			return null;

		URI uri = resource.getURI();
		URI normalized = resource.getResourceSet().getURIConverter().normalize(uri);

		return getFile(normalized);
	}

	public static List<Resource> getResourcesFromFiles(List<IResource> files, boolean load, ResourceSet resourceSet) {
		List<Resource> resources = new ArrayList<Resource>();
		for (IResource file : files) {
			resources.add(getResourceFromFile(file, load, resourceSet));
		}
		return resources;
	}

	public static Resource getResourceFromFile(IResource file, boolean load, ResourceSet resourceSet) {
		final ResourceSet set = resourceSet != null ? resourceSet : new ResourceSetImpl();
		final String path = file.getFullPath().toString();
		final URI uri = URI.createPlatformResourceURI(path, false);
		final Resource resource = set.createResource(uri);
		if (load) {
			try {
				resource.load(set.getLoadOptions());
			} catch (IOException e) {
				McoreUIPlugin.log(e);
			}
		}
		return resource;
	}

	public static List<IFolder> collectFolders(IContainer container) {
		return collectFolders(container, filters);
	}

	public static List<IFolder> collectFolders(IContainer container, List<String> filters) {
		List<IFolder> folders = new ArrayList<IFolder>();
		IResource[] members = null;

		try {
			members = container.members();
		} catch (CoreException e) {
			return folders;
		}

		for (IResource resource : members) {
			if (resource.getType() == IResource.FOLDER && resource.isAccessible()) {
				String resName = resource.getName();
				if (!filters.contains(resName) && !resName.startsWith(".")) {
					folders.add((IFolder) resource);
				}
			}
		}

		return folders;
	}

	public static List<IFile> collectFiles(IContainer container, Collection<String> extensions) {
		final List<IFile> modelFiles = new ArrayList<IFile>();
		for (IFile resource : collectFiles(container)) {
			if (extensions.contains(resource.getFileExtension())) {
				modelFiles.add((IFile) resource);
			}
		}
		return modelFiles;
	}

	public static List<IFile> collectFilesExcludeFilter(IContainer container, Collection<String> filter) {
		final List<IFile> modelFiles = new ArrayList<IFile>();
		for (IFile resource : collectFiles(container)) {
			if (filter.contains(resource.getName())) {
				continue;
			}
			modelFiles.add((IFile) resource);
		}

		return modelFiles;
	}

	public static List<IFile> collectFiles(IContainer container) {
		final List<IFile> modelFiles = new ArrayList<IFile>();
		IResource[] members = null;
		try {
			members = container.members(false);
		} catch (CoreException e) {
			return modelFiles;
		}
		for (IResource resource : members) {
			if (resource.getType() == IResource.FILE) {
				modelFiles.add((IFile) resource);
			}
		}
		return modelFiles;
	}

	public static void insureIsSave(final IFile file) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				if (file != null && file.exists()) {
					final IWorkbenchWindow iw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
					if (iw.getActivePage() != null) {
						final IEditorPart editor = iw == null ? null : ResourceUtil.findEditor(iw.getActivePage(), file);

						if (editor != null && editor.isDirty()) {
							editor.doSave(new NullProgressMonitor());
						}
					}
				}
			}
		});
	}

	public static void setModificationStamp(IFile selectedFile) {
		if (selectedFile == null)
			return;

		try {
			ResourcesPlugin.getWorkspace().getRoot().refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
		} catch (CoreException e) {
			McoreUIPlugin.log(e);
		}

		IProject project = selectedFile.getProject();
		IScopeContext projectScope = new ProjectScope(project);
		IEclipsePreferences preferences = projectScope.getNode(McoreUIPlugin.PLUGIN_ID);

		long current = selectedFile.getModificationStamp();
		preferences.putLong(selectedFile.getName(), current);

		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			McoreUIPlugin.log(e);
		}
	}

	public static boolean hasChanged(IFile file) {
		if (file == null)
			return true;

		long current = file.getModificationStamp();

		IProject project = file.getProject();
		IScopeContext projectScope = new ProjectScope(project);
		IEclipsePreferences preferences = projectScope.getNode(McoreUIPlugin.PLUGIN_ID);

		long stored = preferences.getLong(file.getName(), -1);

		return current != stored;
	}

	public static void deleteProjectPreferences(IFile selectedFile) {
		IProject project = selectedFile.getProject();
		IScopeContext projectScope = new ProjectScope(project);
		IEclipsePreferences preferences = projectScope.getNode(McoreUIPlugin.PLUGIN_ID);

		try {
			preferences.clear();
		} catch (BackingStoreException e) {
			McoreUIPlugin.log(e);
		}

		try {
			preferences.flush();
		} catch (BackingStoreException e) {
			McoreUIPlugin.log(e);
		}
	}

}
