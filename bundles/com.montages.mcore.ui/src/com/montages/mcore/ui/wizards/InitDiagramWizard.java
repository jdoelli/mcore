package com.montages.mcore.ui.wizards;

import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.PartInitException;

import com.montages.mcore.diagram.part.McoreDiagramEditorUtil;
import com.montages.mcore.ui.wizards.pages.NewDiagramNamePage;

public class InitDiagramWizard extends Wizard implements INewWizard {

	public static final String ID = "com.montages.mcore.ui.wizards.InitDiagramWizard";

	private URI mcoreURI;

	private NewDiagramNamePage inputDiagramNamePage = new NewDiagramNamePage("");

	@Override
	public void addPages() {
		this.addPage(inputDiagramNamePage);
	}

	@Override
	public boolean performFinish() {
		URI diagramURI = mcoreURI.trimSegments(1).appendSegment(inputDiagramNamePage.getDiagramName()).appendFileExtension("mcore_diagram");
		McoreDiagramEditorUtil.createDiagram(diagramURI, mcoreURI, new NullProgressMonitor());
		try {
			McoreDiagramEditorUtil.openDiagram(new ResourceSetImpl().createResource(diagramURI));
		} catch (PartInitException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		mcoreURI = (URI)selection.getFirstElement();
		inputDiagramNamePage.init(mcoreURI);
	}
}
