package com.montages.mcore.ui.wizards.pages;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String MESSAGES = "com.montages.mcore.ui.wizards.pages.messages"; //$NON-NLS-1$

	public static String ComponentPreviewPage_Title;

	public static String ComponentPreviewPage_Description;

	public static String ComponentPreviewPage_Error_message;

	public static String ComponentPreviewPage_Validate_ComponentOverwrite;

	public static String ComponentPreviewPage_Validate_ComponentCollision;

	public static String DomainType_Label;

	public static String DomainName_Label;

	public static String ComponentName_Label;

	public static String ComponentPreviewPage_ReplaceExistingComponent_Label;

	public static String ComponentPreviewPage_ReuseExistingComponent_Label;

	public static String ComponentPreviewPage_Validate_URI_Collision;

	public static String RepositoryExportPage_Title;

	public static String RepositoryExportPage_Description;

	public static String RepositoryExportPage_EmailCheckBox_Label;

	public static String RepositoryExportPage_PreferenceLink_Label;

	public static String RepositoryExportPage_ExportDependenciesCheckBox_Label;

	public static String RepositoryExportPage_PathSection_Label;

	public static String RepositoryExportPage_PathSection_Browse;

	public static String RepositoryExportPage_FileSection_FileName;

	public static String McoreFilter_McoreFiles;

	public static String McoreFilter_AllFiles;

	private Messages() {
	}

	static {
		// initialize resource bundle
		NLS.initializeMessages(MESSAGES, Messages.class);
	}
}
