package com.montages.mcore.ui.wizards;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.xocl.core.util.XoclHelper;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MNamedEditor;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.SimpleType;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.util.ResourceService;
import com.montages.mcore.util.URIService;

public final class WizardData {

	private final URIService uriService = new URIService();

	private String domainType;
	private String domainName;
	private String componentName;
	private String packageName;
	private String classifierName;
	private String propertyName;

	WizardData() {}

	public static class Builder {
		private final WizardData data;
		public Builder() {
			data = new WizardData();
		}
		public Builder setComponent(String type, String domain, String name) {
			data.domainType = type;
			data.domainName = domain;
			data.componentName = XoclHelper.camelCaseToBusiness(name);
			return this;
		}
		public Builder setPackage(String name, String classifierName, String propertyName) {
			data.packageName = XoclHelper.camelCaseToBusiness(
					name.replaceAll("\\s", "").substring(0, 1).toLowerCase().concat(name.substring(1)));
			data.classifierName = XoclHelper.camelCaseToBusiness(classifierName);
			data.propertyName = XoclHelper.camelCaseToBusiness(propertyName);
			return this;
		}
		public WizardData build() {
			return data;
		}
	}

	public MComponent createComponent() {
		MComponent component = McoreFactory.eINSTANCE.createMComponent();
		component.setDomainType(domainType);
		component.setDomainName(domainName);
		component.setName(componentName);
		component.setUseLegacyEditorconfig(true);
		component.setAvoidRegenerationOfEditorConfiguration(false);
		component.setAbstractComponent(false);

		MPackage mPackage = McoreFactory.eINSTANCE.createMPackage();
		mPackage.setName(packageName);

		MClassifier classifier = McoreFactory.eINSTANCE.createMClassifier();
		classifier.setName(classifierName);

		MProperty property = McoreFactory.eINSTANCE.createMProperty();
		property.setName(propertyName);
		property.setSingular(true);
		property.setMandatory(true);
		property.setSimpleType(SimpleType.STRING);
		property.setChangeable(true);
		property.setHasStorage(true);
		property.setPersisted(true);
		
		MNamedEditor mNamedEditor = McoreFactory.eINSTANCE.createMNamedEditor();
		mNamedEditor.setLabel("Main");
		mNamedEditor.setName("Main");
		mNamedEditor.setUserVisible(true);
		component.getNamedEditor().add(mNamedEditor);

		classifier.getProperty().add(property);
		mPackage.getClassifier().add(classifier);
		component.getOwnedPackage().add(mPackage);
		mPackage.setResourceRootClass(classifier);

		component.getResourceFolder().add(createInstances(createObject(classifier, property)));

		ResourceSet rs = ResourceService.createResourceSet();

		URI uri = uriService.addUriMapping(
				uriService.createComponentURI(component), 
				uriService.createPlatformResource(component), 
				rs.getURIConverter().getURIMap());

		Resource resource = rs.createResource(uri);
		resource.getContents().add(component);

		return component;
	}

	private MResourceFolder createInstances(MObject object) {
		MResourceFolder wfolder = ObjectsFactory.eINSTANCE.createMResourceFolder();
		wfolder.setName("workspace");
		MResourceFolder testfolder = ObjectsFactory.eINSTANCE.createMResourceFolder();
		testfolder.setName("test");
		MResource r = ObjectsFactory.eINSTANCE.createMResource();
		r.setName("My");
		r.setId("R01");
		r.getObject().add(object);
		testfolder.getResource().add(r);
		wfolder.getFolder().add(testfolder);

		return wfolder;
	}

	private MObject createObject(MClassifier classifier, MProperty property) {
		MObject o = ObjectsFactory.eINSTANCE.createMObject();
		o.setTypePackage(classifier.getContainingPackage());
		o.setType(classifier);
		MPropertyInstance i = ObjectsFactory.eINSTANCE.createMPropertyInstance();
		i.setProperty(property);
		MDataValue v = ObjectsFactory.eINSTANCE.createMDataValue();
		v.setDataValue("1");
		i.getInternalDataValue().add(v);
		o.getPropertyInstance().add(i);

		return o;
	}

}