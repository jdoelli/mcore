package com.montages.mcore.ui.wizards.pages;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.montages.mcore.ui.wizards.Messages;

public class MCorePackagePage extends WizardPage {

	private Text rootClassNameField;
	private Text packageNameField;
	private String packageName = "My Package"; //$NON-NLS-1$
	private String className = "Class 0"; //$NON-NLS-1$
	public static String propertyName = "Text 1"; //$NON-NLS-1$

	public MCorePackagePage(String pageName) {
		super(pageName);
		setTitle(Messages.MCoreProjectFirstPage_MCore_Project);
		setDescription("Specify root package.");
	}

	protected ModifyListener validator = new ModifyListener() {
		public void modifyText(ModifyEvent e) {
			setPageComplete(validatePage());
		}
	};

	void setPackageName(String name) {
		if (name == null) {
			packageNameField.setText(packageName);
		} else {
			packageNameField.setText(name);
		}
	}

	void setRootClassName(String name) {
		if (name == null) {
			rootClassNameField.setText(className);
		} else {
			rootClassNameField.setText(name);
		}
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		{
			GridLayout layout = new GridLayout();
			layout.verticalSpacing = 5;
			composite.setLayout(layout);
			composite.setLayoutData(new GridData(GridData.FILL_BOTH));
		}
		Label packageNameLabel = new Label(composite, SWT.LEFT);
		{
			packageNameLabel.setText(Messages.MCoreModelCreationPage_MCore_Package_Name);

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			packageNameLabel.setLayoutData(data);
		}
		packageNameField = new Text(composite, SWT.SINGLE | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			packageNameField.setLayoutData(data);
			packageNameField.setText(packageName);
		}
		Label rootClassNameLabel = new Label(composite, SWT.LEFT);
		{
			rootClassNameLabel.setText("Root Classifier"); //$NON-NLS-1$

			GridData data = new GridData();
			data.horizontalAlignment = GridData.FILL;
			rootClassNameLabel.setLayoutData(data);
		}
		rootClassNameField = new Text(composite, SWT.SINGLE | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_HORIZONTAL);

			rootClassNameField.setLayoutData(data);
			rootClassNameField.setText(className);
		}
		rootClassNameField.addModifyListener(validator);

		packageNameField.addModifyListener(validator);

		setPageComplete(validatePage());
		setControl(composite);
	}

	protected boolean isEmptyTextField(Text text) {
		String textString = text.getText();
		return (textString == null) || (textString.trim().length() == 0);
	}

	protected boolean validatePage() {
		if (isEmptyTextField(packageNameField)) {
			setErrorMessage(Messages.XoclEcoreModelCreationPageBase_PackageNameEmptyError);
			return false;
		}
		if (isEmptyTextField(rootClassNameField)) {
			setErrorMessage(Messages.XoclEcoreModelCreationPageBase_RootClassNameEmptyError);
			return false;
		}
		setErrorMessage(null);
		return true;
	}

	public String getPackageName() {
		return packageNameField.getText();
	}

	public String getRootClassName() {
		return rootClassNameField.getText();
	}
}
