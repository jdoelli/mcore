package com.montages.mcore.ui.wizards;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;

public class ExportRepositoryWizardData {

	private IStructuredSelection myViewerSelection;

	private String myFolder;

	public String getFolder() {
		return myFolder;
	}

	public void setFolder(String folder) {
		myFolder = folder;
	}

	public IStructuredSelection getViewerSelection() {
		return myViewerSelection;
	}

	public void setViewerSelection(IStructuredSelection viewerSelection) {
		myViewerSelection = viewerSelection;
	}

	public URI getSelectedComponent() {
		if (myViewerSelection == null || myViewerSelection.isEmpty()) {
			return null;
		}
		Object selection = myViewerSelection.getFirstElement();
		if (false == selection instanceof URI) {
			return null;
		}
		return (URI) selection;
	}
}
