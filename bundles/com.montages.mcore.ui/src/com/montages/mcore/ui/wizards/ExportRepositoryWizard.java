package com.montages.mcore.ui.wizards;

import java.io.File;
import java.io.IOException;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.IExportWizard;
import org.eclipse.ui.IWorkbench;

import com.montages.mcore.codegen.ui.conversions.Exporter;
import com.montages.mcore.ui.wizards.pages.ComponentSelectionPage;
import com.montages.mcore.ui.wizards.pages.RepositoryExportPage;
import com.montages.mcore.util.mailer.EclipsePreferenceStoreMailConfig;
import com.montages.mcore.util.mailer.Emailer;
import com.montages.mcore.util.mailer.IMailer.MailMessage;
import com.montages.mcore.util.mailer.IMailer.StandartMailMessageImpl;
import com.montages.mcore.util.mailer.MailConfig;

public class ExportRepositoryWizard extends Wizard implements IExportWizard {

	public static final String ID = "com.montages.mcore.ui.wizards.ExportRepositoryWizard";

	private RepositoryExportPage repositoryExportPage;

	private ExportRepositoryWizardData exportRepositoryWizardData;

	private Exporter myExporter;

	public ExportRepositoryWizard() {
		super();
		setWindowTitle("Export");
	}

	@Override
	public void addPages() {
		if (exportRepositoryWizardData == null || exportRepositoryWizardData.getSelectedComponent() == null) {
			addPage(new ComponentSelectionPage("Select"));
		}
		addPage(repositoryExportPage = new RepositoryExportPage("Export"));
	}

	@Override
	public void init(IWorkbench workbench, IStructuredSelection selection) {
		if (selection == null || selection.isEmpty()) {
			return;
		}
		Object selectionData = selection.getFirstElement();
		if (false == selectionData instanceof ExportRepositoryWizardData) {
			return;
		}
		exportRepositoryWizardData = (ExportRepositoryWizardData) selectionData;
	}

	@Override
	public boolean canFinish() {
		RepositoryExportPage page = (RepositoryExportPage) getPage("Export");
		return page != null && page.validatePage() && getExporter() != null;
	}

	@Override
	public IWizardPage getStartingPage() {
		IWizardPage startingPage = super.getStartingPage();
		if (startingPage instanceof RepositoryExportPage) {
			URI input = getSelectedURI();
			if (input != null) {
				((RepositoryExportPage) startingPage).setInput(getExporter(), extractFolder());
			}
		}
		return startingPage;
	}

	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		IWizardPage nextPage = super.getNextPage(page);
		if (nextPage instanceof RepositoryExportPage) {
			URI input = getSelectedURI();
			if (input != null) {
				((RepositoryExportPage) nextPage).setInput(getExporter(), extractFolder());
			}
		}
		return nextPage;
	}

	@Override
	public boolean performFinish() {
		RepositoryExportPage page = (RepositoryExportPage) getPage("Export");
		if (page == null) {
			return false;
		}
		if (repositoryExportPage.isEmailSend() && !isValidEmailAddress(repositoryExportPage.getEmail())) {
			MessageDialog.openError(getShell(), "Error sending mail!", "Email address is wrong."); //$NON-NLS-1$ //$NON-NLS-1$
			return false;
		}
		String folder = page.getFolderPath();
		String file = page.getFileName();
		File outputFiles = null;
		if (!folder.isEmpty() && !file.isEmpty()) {
			try {
				outputFiles = getExporter().exportMCore(folder, file);
			} catch (IOException e) {
				MessageDialog.openError(getShell(), "Error Exporting Resource", e.getMessage());
				return false;
			}
		}
		if (repositoryExportPage.isEmailSend() && outputFiles != null) {
			sendEmail(repositoryExportPage.getEmail(), outputFiles);
		}
		MessageDialog.openInformation(getShell(), "Export result", "Export completed successfully");
		exportRepositoryWizardData.setFolder(repositoryExportPage.getFolder());

		return true;
	}

	private void sendEmail(String email, File attachment) {
		try {
			MailConfig config = new EclipsePreferenceStoreMailConfig();
			Emailer emailer = new Emailer(config);
			MailMessage message = new StandartMailMessageImpl(config.getSubject(), config.getBody());
			message.getAttachments().add(attachment);
			emailer.send(email, message);
		} catch (Exception e) {
			MessageDialog.openError(getShell(), "Error sending mail!", e.getMessage());
		}
	}

	private boolean isValidEmailAddress(String email) {
		try {
			new InternetAddress(email).validate();
			return true;
		} catch (AddressException ex) {
			return false;
		}
	}

	public URI getSelectedURI() {
		URI result = null;
		ComponentSelectionPage page = (ComponentSelectionPage) getPage("Select");
		if (page != null) {
			StructuredSelection selection = (StructuredSelection) page.getSelection();
			result = selection.isEmpty() ? null : (URI) selection.getFirstElement();
		}
		if (result == null && exportRepositoryWizardData != null) {
			result = exportRepositoryWizardData.getSelectedComponent();
		}
		return result;
	}

	public String extractFolder() {
		if (exportRepositoryWizardData == null) {
			return null;
		}
		String folder = exportRepositoryWizardData.getFolder();
		return folder == null || folder.isEmpty() ? "" : folder;
	}

	public Exporter getExporter() {
		URI input = getSelectedURI();
		if (input == null) {
			return myExporter = null;
		}
		if (myExporter == null || !myExporter.getUri().equals(input)) {
			myExporter = new Exporter(input);
		}
		return myExporter;
	}
}
