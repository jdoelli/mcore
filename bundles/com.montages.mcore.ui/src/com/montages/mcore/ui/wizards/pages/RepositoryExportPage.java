package com.montages.mcore.ui.wizards.pages;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.provider.ReflectiveItemProviderAdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.preference.PreferenceNode;
import org.eclipse.jface.viewers.CheckStateChangedEvent;
import org.eclipse.jface.viewers.CheckboxTreeViewer;
import org.eclipse.jface.viewers.ICheckStateListener;
import org.eclipse.jface.viewers.ICheckStateProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.widgets.Hyperlink;

import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;
import com.montages.mcore.codegen.ui.conversions.Exporter;
import com.montages.mcore.impl.MRepositoryImpl;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.preferencePages.McoreEmailIntegrationPreferencePage;
import com.montages.mcore.util.DiagramLabelProvider;
import com.montages.mcore.util.McoreResourceImpl;
import com.montages.transactions.McoreEditingDomainFactory;

public class RepositoryExportPage extends WizardPage {

	public static final String LAST_EXPORT_PATH = "lastExportPath";

	public static final String LAST_EXPORT_MODEL = "LastExportModel";

	private Text fileNameText;

	private Text folderPathText;

	private TreeViewer viewer;

	private Composite componentExportValuesPanel;

	private Text componentDomainTypeText;

	private Text componentDomainNameText;

	private Text componentNameText;

	private Button emailCheckbox;

	private Text emailText;

	private Composite composite;

	private Button exportPluginsCheckbox;

	private Exporter exporter;

	private CheckboxTreeViewer diagramViewer;

	public RepositoryExportPage(String pageName) {
		super(pageName);
		setTitle(Messages.RepositoryExportPage_Title);
		setDescription(Messages.RepositoryExportPage_Description);
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, false);
		composite.setLayout(layout);

		initializeDialogUnits(parent);

		createExportSection(composite);

		setErrorMessage(null);
		setMessage(null);
		setControl(composite);

		Dialog.applyDialogFont(parent);
		setPageComplete(validatePage());
	}

	public boolean validatePage() {
		return !getFolderPath().isEmpty() && !getFileName().isEmpty();
	}

	public String getFolderPath() {
		return folderPathText == null ? "" : folderPathText.getText();
	}

	public String getFileName() {
		return fileNameText == null ? "" : fileNameText.getText();
	}

	private void createExportSection(Composite parent) {
		final Tree tree = new Tree(parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			tree.setLayoutData(data);
		}
		viewer = new TreeViewer(tree);

		ComposedAdapterFactory adapterFactory = McoreEditingDomainFactory.getInstance().createComposedAdapterFactory();
		adapterFactory.addAdapterFactory(new ReflectiveItemProviderAdapterFactory());
		viewer.setContentProvider(new AdapterFactoryContentProvider(adapterFactory));
		viewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {

			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				TreeItem[] items = tree.getSelection();
				if (items.length == 0) {
					componentExportValuesPanel.setVisible(false);
					return;
				}
				TreeItem selected = items[0];
				if (false == selected.getData() instanceof MComponent) {
					componentExportValuesPanel.setVisible(false);
					return;
				}
				MComponent component = (MComponent) selected.getData();
				updateExportComponentValues(component);
			}
		});
		createDiagramCheckboxViewer(parent);
		createExportComponentValuesArea(parent);
		createEmailSection(parent);
		createPathSection(parent);
		createFileNameSection(parent);
		createExportPluginDependenciesSection(parent);
	}

	private void createDiagramCheckboxViewer(Composite parent) {
		final Tree tree = new Tree(parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.BORDER | SWT.CHECK);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
//			data.grabExcessHorizontalSpace = true;
//			data.grabExcessVerticalSpace = true;
			tree.setLayoutData(data);
		}
		diagramViewer = new CheckboxTreeViewer(tree);

		diagramViewer.setCheckStateProvider(new ICheckStateProvider() {
			@Override
			public boolean isGrayed(Object element) {
				return false;
			}

			@Override
			public boolean isChecked(Object element) {
				return true;
			}
		});

		diagramViewer.setContentProvider(new ITreeContentProvider() {

			@Override
			public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
			}

			@Override
			public void dispose() {
			}

			@Override
			public boolean hasChildren(Object element) {
				return false;
			}

			@Override
			public Object getParent(Object element) {
				return null;
			}

			@Override
			public Object[] getElements(Object inputElement) {
				if (inputElement == null) {
					return new Object[0];
				}
				if (inputElement instanceof List) {
					return ((List<?>) inputElement).toArray();
				}
				return new Object[] { inputElement };
			}

			@Override
			public Object[] getChildren(Object parentElement) {
				return null;
			}
		});

		diagramViewer.setLabelProvider(new DiagramLabelProvider());
		diagramViewer.addCheckStateListener(new ICheckStateListener() {

			@Override
			public void checkStateChanged(CheckStateChangedEvent event) {
				if (exporter == null) {
					return;
				}
				List<URI> checkedElement = new ArrayList<URI>();
				for (Object uri : diagramViewer.getCheckedElements()) {
					checkedElement.add((URI) uri);
				}
				exporter.setDiagramToExport(checkedElement);
			}
		});
	}

	private void createEmailSection(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(3, false));
		emailCheckbox = new Button(composite, SWT.CHECK);
		emailCheckbox.setText(Messages.RepositoryExportPage_EmailCheckBox_Label);
		emailText = new Text(composite, SWT.BORDER);
		emailText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		emailCheckbox.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				emailText.setEnabled(emailCheckbox.getSelection());
			}
		});
		refreshEmailEnablingVisibles();
		Hyperlink preferenceLink = new Hyperlink(composite, SWT.WRAP);
		preferenceLink.setText(Messages.RepositoryExportPage_PreferenceLink_Label);
		preferenceLink.setForeground(preferenceLink.getDisplay().getSystemColor(SWT.COLOR_BLUE));
		GridData gridData = new GridData(SWT.LEFT, SWT.TOP, false, false);
		gridData.horizontalSpan = 1;
		preferenceLink.setLayoutData(gridData);
		preferenceLink.setUnderlined(true);
		preferenceLink.addHyperlinkListener(new HyperlinkAdapter() {

			public void linkActivated(HyperlinkEvent e) {
				IWorkbenchPreferencePage page = new McoreEmailIntegrationPreferencePage();
				page.init(null);
				PreferenceManager mgr = new PreferenceManager();
				IPreferenceNode node = new PreferenceNode(
						"com.montages.mcore.ui.preferencePages.McoreEmailIntegrationPreferencePage", page); //$NON-NLS-1$
				mgr.addToRoot(node);
				PreferenceDialog dialog = new PreferenceDialog(getShell(), mgr);
				dialog.create();
				dialog.setMessage(page.getTitle());
				dialog.open();
				refreshEmailEnablingVisibles();
			}
		});
		emailText.setEnabled(emailCheckbox.getSelection());
	}

	private void createExportPluginDependenciesSection(Composite parent) {
		composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(3, false));

		exportPluginsCheckbox = new Button(composite, SWT.CHECK);
		exportPluginsCheckbox.setText(Messages.RepositoryExportPage_ExportDependenciesCheckBox_Label);
		exportPluginsCheckbox.setSelection(exporter != null ? exporter.shouldExportPluginComponents() : false);
		exportPluginsCheckbox.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent e) {
				if (exporter != null) {
					exporter.setShouldExportPluginComponents(exportPluginsCheckbox.getSelection());
				}
				if (viewer != null) {
					viewer.setInput(exporter.getMRepository());
				}
			}
		});
	}

	private void refreshEmailEnablingVisibles() {
		boolean isEmailEnabled = McoreEmailIntegrationPreferencePage.isEmailEnabled();
		emailCheckbox.setEnabled(isEmailEnabled);
		emailCheckbox.setSelection(isEmailEnabled);
		emailText.setEnabled(emailCheckbox.getSelection());
	}

	public boolean isEmailSend() {
		return emailCheckbox.getSelection();
	}

	public String getEmail() {
		return emailText.getText();
	}

	private void createFileNameSection(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(2, false));
		Label label = new Label(composite, SWT.NONE);
		label.setText(Messages.RepositoryExportPage_FileSection_FileName);
		fileNameText = new Text(composite, SWT.BORDER);
		fileNameText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		fileNameText.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(validatePage());
			}
		});
		refreshFileName();
	}

	private void createPathSection(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(3, false));
		Label label = new Label(composite, SWT.NONE);
		label.setText(Messages.RepositoryExportPage_PathSection_Label);
		folderPathText = new Text(composite, SWT.BORDER);
		folderPathText.setEnabled(false);
		folderPathText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		Button button = new Button(composite, SWT.PUSH);
		button.setText(Messages.RepositoryExportPage_PathSection_Browse);
		button.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				FileDialog fileDialog = selectTargetFile(fileNameText.getText(), folderPathText.getText());
				String fileName = fileDialog.getFileName();
				if (fileName == null || fileName.isEmpty()) {
					return;
				}
				String folderPath = fileDialog.getFilterPath();
				if (folderPath == null || folderPath.isEmpty()) {
					return;
				}
				refreshFolder(folderPath);
				refreshFileName(fileName);
				setPageComplete(validatePage());
			}
		});
	}

	private String getPreselectedFileName(MComponent selectedComponent) {
		if (selectedComponent == null) {
			return ""; //$NON-NLS-1$
		}
		return exporter.createMCoreFileNameWithTimestamp(selectedComponent.getEName());
	}

	private MComponent getSelectedComponent() {
		ISelection selection = viewer.getSelection();
		if (selection == null || false == selection instanceof TreeSelection || ((TreeSelection) selection).isEmpty()) {
			return extractComponentFromViewerInput();
		}
		TreeSelection treeSelection = (TreeSelection) selection;
		Object first = treeSelection.getFirstElement();
		return false == first instanceof MComponent ? null : (MComponent) first;
	}

	private MComponent extractComponentFromViewerInput() {
		Object input = viewer.getInput();
		if (input == null || false == input instanceof McoreResourceImpl) {
			return null;
		}
		McoreResourceImpl mcoreResource = (McoreResourceImpl) input;
		if (mcoreResource.getContents().isEmpty()) {
			return null;
		}
		EObject firstContent = mcoreResource.getContents().get(0);
		if (firstContent == null) {
			return null;
		}
		if (firstContent instanceof MRepositoryImpl) {
			MRepositoryImpl repository = (MRepositoryImpl) firstContent;
			return repository.getComponent().isEmpty() ? null : repository.getComponent().get(0);
		}
		if (firstContent instanceof MComponent) {
			return (MComponent) firstContent;
		}
		return null;
	}

	protected void updateExportComponentValues(MComponent component) {
		componentExportValuesPanel.setVisible(true);
		componentDomainTypeText.setText(component.getDomainType());
		componentDomainTypeText.setData(new Object[] { component, McorePackage.Literals.MCOMPONENT__DOMAIN_TYPE });
		componentDomainNameText.setText(component.getDomainName());
		componentDomainNameText.setData(new Object[] { component, McorePackage.Literals.MCOMPONENT__DOMAIN_NAME });
		if (component.eIsSet(McorePackage.Literals.MNAMED__SHORT_NAME)) {
			componentNameText.setText(component.getShortName());
			componentNameText.setData(new Object[] { component, McorePackage.Literals.MNAMED__NAME,
					McorePackage.Literals.MNAMED__SHORT_NAME });
		} else {
			componentNameText.setText(component.getName());
			componentNameText.setData(new Object[] { component, McorePackage.Literals.MNAMED__NAME });
		}
		refreshFileName();
	}

	private void createExportComponentValuesArea(Composite exportModelComposite) {
		componentExportValuesPanel = new Composite(exportModelComposite, SWT.NONE);
		GridLayout groupLayout = new GridLayout(2, false);
		groupLayout.verticalSpacing = 8;
		groupLayout.marginTop = 8;
		componentExportValuesPanel.setLayout(groupLayout);
		GridData data = new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL);
		componentExportValuesPanel.setLayoutData(data);
		componentDomainTypeText = createValueSection(componentExportValuesPanel, Messages.DomainType_Label + ":"); //$NON-NLS-1$
		componentDomainNameText = createValueSection(componentExportValuesPanel, Messages.DomainName_Label + ":"); //$NON-NLS-1$
		componentNameText = createValueSection(componentExportValuesPanel, Messages.ComponentName_Label + ":"); //$NON-NLS-1$
		componentExportValuesPanel.setVisible(false);
	}

	private Text createValueSection(Composite parent, String labelText) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(labelText);
		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		text.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		text.addModifyListener(new ModifyListener() {

			@Override
			public void modifyText(ModifyEvent e) {
				Text text = (Text) e.getSource();
				Object[] data = (Object[]) text.getData();
				ISelection selection = viewer.getSelection();

				if (data != null && selection instanceof StructuredSelection) {
					MComponent component = (MComponent) data[0];

					if (component.equals(((StructuredSelection) selection).getFirstElement())) {
						for (int i = 1; i < data.length; i++) {
							EAttribute attribute = (EAttribute) data[i];
							component.eSet(attribute, text.getText());
						}

						viewer.update(component, null);
						refreshFileName();

						setPageComplete(validatePage());
					}
				}
			}
		});
		return text;
	}

	public void setInput(Exporter exporter, String folderPath) {
		if (folderPath == null || folderPath.isEmpty()) {
			folderPath = McoreUIPlugin.getInstance().getPreference(LAST_EXPORT_PATH);
		}
		this.exporter = exporter;
		if (exporter == null) {
			viewer.setInput(null);
			return;
		}
		viewer.setInput(exporter.getMRepository());
		exportPluginsCheckbox.setVisible(exporter.canExportPluginComponents());
		if (exporter.getAllDiagrams().isEmpty()) {
			diagramViewer.getTree().setVisible(false);
			diagramViewer.setInput(new Object());
		} else {
			diagramViewer.getTree().setVisible(true);
			diagramViewer.setInput(exporter.getAllDiagrams());

			if (!exporter.getSelectedDiagram().isEmpty()) {
				diagramViewer.setCheckedElements(exporter.getSelectedDiagram().toArray());
			}
		}
		refreshFileName();
		refreshFolder(folderPath);
		refreshExportPluginsCheckbox(exporter.shouldExportPluginComponents());

		exporter.setDiagramToExport(Stream.of(diagramViewer.getCheckedElements()) //
				.filter(e -> e instanceof URI) //
				.map(e -> URI.class.cast(e)) //
				.collect(Collectors.toList()));
	}

	private void refreshFileName() {
		refreshFileName(null);
	}

	private void refreshFileName(String fileName) {
		fileName = fileName == null || fileName.isEmpty() ? getPreselectedFileName(getSelectedComponent()) : fileName;
		if (fileName != null) {
			McoreUIPlugin.getInstance().storePreference(LAST_EXPORT_MODEL, fileName);
		}
		fileNameText.setText(fileName);
	}

	private void refreshFolder(String path) {
		path = path == null ? "" : path;
		McoreUIPlugin.getInstance().storePreference(LAST_EXPORT_PATH, path);
		folderPathText.setText(path); // $NON-NLS-1$
	}

	private void refreshExportPluginsCheckbox(boolean exportPlugins) {
		exportPluginsCheckbox.setSelection(exportPlugins);
	}

	private FileDialog selectTargetFile(String fileName, String filePath) {
		FileDialog dialog = new FileDialog(getShell(), SWT.SAVE);
		String[] filterNames = new String[] { Messages.McoreFilter_McoreFiles, Messages.McoreFilter_AllFiles + "(*)" }; //$NON-NLS-1$
		String[] filterExtensions = new String[] { "*.mcore", "*" }; //$NON-NLS-1$ //$NON-NLS-2$

		String platform = SWT.getPlatform();
		if (platform.equals("win32") || platform.equals("wpf")) { //$NON-NLS-1$ //$NON-NLS-2$
			filterNames = new String[] { Messages.McoreFilter_McoreFiles, Messages.McoreFilter_AllFiles + "(*.*)" }; //$NON-NLS-1$
			filterExtensions = new String[] { "*.mcore", "*.*" }; //$NON-NLS-1$ //$NON-NLS-2$
		}

		dialog.setFilterNames(filterNames);
		dialog.setFilterExtensions(filterExtensions);
		dialog.setFileName(fileName);
		if (filePath != null && !filePath.isEmpty()) {
			dialog.setFilterPath(filePath);
		}
		dialog.open();
		return dialog;
	}

	public String getFolder() {
		return folderPathText.getText();
	}

}
