package com.montages.mcore.ui.wizards;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {

	private static final String BUNDLE_NAME = "com.montages.mcore.ui.wizards.messages"; //$NON-NLS-1$

	public static String InitNewDiagramPage_Name_Collision;

	public static String MCoreImportPage_Description;

	public static String MCoreProjectFirstPage_Browse_File_System;

	public static String MCoreProjectFirstPage_Browse_Workspace;

	public static String MCoreProjectFirstPage_Choose_MCore_FIle;

	public static String MCoreProjectFirstPage_Component_Name_Value;

	public static String MCoreProjectFirstPage_Domain_Name_Value;

	public static String MCoreProjectFirstPage_Domain_Type_Value;

	public static String McoreProjectFirstPage_Diagram_Name_Value;

	public static String MCoreProjectFirstPage_Import_Model;

	public static String MCoreProjectFirstPage_Load;

	public static String MCoreProjectFirstPage_MComponent_Properties;

	public static String MCoreProjectFirstPage_MCore_Project;

	public static String MCoreProjectFirstPage_MCore_Project_Settings;

	public static String MCoreProjectFirstPage_New_Model;

	public static String MCoreProjectFirstPage_Project_Already_Exist;

	public static String MCoreProjectFirstPage_Project_Name;

	public static String BaseImportPage_Title;

	public static String BaseImportPage_Descr;

	public static String BaseImportPage_NoFileChoosenError;

	public static String BaseImportPage_WrongFilePathError;

	public static String BaseImportPage_UnknownSchemaError;

	public static String BaseImportPage_FilterMcoreFiles;

	public static String BaseImportPage_FilterEcoreFiles;

	public static String BaseImportPage_FilterAllFiles;

	public static String NewXoclProjectWizard_CaseSensitive_warning;

	public static String NewXoclProjectWizard_CopyMoveError;

	public static String NewXoclProjectWizard_CreationProblems;

	public static String NewXoclProjectWizard_EcoreModelCreationTaskName;

	public static String NewXoclProjectWizard_EcoreProject_description;

	public static String NewXoclProjectWizard_EcoreProject_title;

	public static String NewXoclProjectWizard_EcoreSelectionPageDescription;

	public static String NewXoclProjectWizard_EcoreSelectionPageTitle;

	public static String NewXoclProjectWizard_InternalError;

	public static String NewXoclProjectWizard_NewEcoreProject;

	public static String NewXoclProjectWizard_WizardTitle;

	public static String NewXoclProjectWizard_XoclProjectCreationTaskName;

	public static String NewXoclProjectWizardMainPage_CreateNewEcoreButtonLabel;

	public static String NewXoclProjectWizardMainPage_ProjectSettingsGroupName;

	public static String NewXoclProjectWizardMainPage_UseExisitingEcoreButtonLabel;

	public static String XoclEcoreModelCreationPage_EcoreFileNameLabel;

	public static String XoclEcoreModelCreationPage_InvalidFileExtensionError;

	public static String XoclEcoreModelCreationPageBase_PackageNameEmptyError;

	public static String XoclEcoreModelCreationPageBase_PackageNameLabel;

	public static String XoclEcoreModelCreationPageBase_PackageNsPrefixEmptyError;

	public static String XoclEcoreModelCreationPageBase_PackageNsPrefixLabel;

	public static String XoclEcoreModelCreationPageBase_PackageNsUriEmptyError;

	public static String XoclEcoreModelCreationPageBase_PackageNsUriLabel;

	public static String XoclEcoreModelCreationPageBase_InitialModelSettingsGroupLabel;

	public static String XoclEcoreModelCreationPageBase_RootClassNameEmptyError;

	public static String XoclEcoreModelCreationPageBase_RootClassNameLabel;

	public static String XoclEcoreModelCreationPageBase_UnknownXmlEncodingError;

	public static String XoclEcoreModelSelectionPage_AdditionalSettingsGroupName;

	public static String XoclEcoreModelSelectionPage_CopyOrMoveEcoreButtonLabel;

	public static String XoclEcoreModelSelectionPage_InvalidFileExtensionError;

	public static String XoclEcoreModelSelectionPage_LaunchRuntimeInstanceButtonLabel;

	public static String XoclEcoreModelSelectionPage_RunImplementActionButtonLabel;

	public static String MCoreModelCreationPage_MCore_Project;

	public static String MCoreModelCreationPage_MCore_Model_Settings;

	public static String MCoreModelCreationPage_MCore_Package_Name;

	public static String MCoreModelCreationPage_MCore_File_Name;

	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
