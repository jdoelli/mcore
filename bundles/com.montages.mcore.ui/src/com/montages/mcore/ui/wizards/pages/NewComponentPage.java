package com.montages.mcore.ui.wizards.pages;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.wizards.Messages;

public class NewComponentPage extends BasePage {

	public NewComponentPage(String pageName) {
		super(pageName);
		setTitle(Messages.MCoreProjectFirstPage_MCore_Project);
		setDescription(Messages.MCoreProjectFirstPage_MCore_Project_Settings);
		setImageDescriptor(McoreUIPlugin.getImage("image_lde"));
	}

	@Override
	public void createControl(Composite parent) {
		Composite composite = new Composite(parent, SWT.NULL);
		{
			GridLayout layout = new GridLayout(2, false);
			GridData data = new GridData(GridData.FILL_BOTH);
			composite.setLayout(layout);
			composite.setLayoutData(data);
		}

		initializeDialogUnits(parent);

		createTabItemNewModel(composite);

		setErrorMessage(null);
		setMessage(null);
		setControl(composite);

		Dialog.applyDialogFont(parent);
		setPageComplete(validatePage());
	}

	private void createTabItemNewModel(Composite parent) {
		textDomainType = createPropertyForm(parent, "Domain Type:", "org");
		textDomainName = createPropertyForm(parent, "Domain Name:", "langlets");
		textCompName = createPropertyForm(parent, "Component Name:", "My Component");
		textDiagramName = createPropertyForm(parent, "Diagram Name:", NewDiagramNamePage.BASE_DIAGRAM_NAME + " 1");
	}

	private Text createPropertyForm(Composite parent, String propertyName, String defaultValue) {
		Label label = new Label(parent, SWT.NONE);
		label.setText(propertyName);
		{
			GridData data = new GridData();
			data.grabExcessHorizontalSpace = false;
			data.horizontalSpan = 1;
			data.horizontalAlignment = GridData.BEGINNING;
			label.setLayoutData(data);
		}

		Text text = new Text(parent, SWT.SINGLE | SWT.BORDER);
		text.setText(defaultValue);
		{
			GridData data = new GridData(GridData.FILL_HORIZONTAL);
			data.horizontalSpan = 1;
			text.setLayoutData(data);
		}

		text.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				setPageComplete(validatePage());
			}
		});

		return text;
	}

}
