package com.montages.mcore.ui.preferencePages;

import java.io.IOException;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.osgi.framework.Bundle;

import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.mailer.EMailerConfigFromFile;
import com.montages.mcore.util.mailer.GMailConfig;
import com.montages.mcore.util.mailer.MailConfig;

public class McoreEmailIntegrationPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {

	private final static String EMAIL_ENABLING_CHECKBOX_KEY = "email.enabling.checkbox"; //$NON-NLS-1$

	private Text fromText;

	private Text smtpHost;

	private Text smtpPort;

	private Text smtpUser;

	private Text smtpPassword;

	private Button emailEnablingCheckbox;

	private MailConfig defaultMailConfig;

	@Override
	public void init(IWorkbench workbench) {
		initDefaultMailConfig();
	}

	public static boolean isEmailEnabled() {
		return McoreUIPlugin.getInstance().getPreferenceStore().getBoolean(EMAIL_ENABLING_CHECKBOX_KEY);
	}

	@Override
	public String getTitle() {
		return "MCore email integration"; //$NON-NLS-1$
	}

	@Override
	protected IPreferenceStore doGetPreferenceStore() {
		return McoreUIPlugin.getInstance().getPreferenceStore();
	}

	@Override
	protected Control createContents(Composite parent) {
		emailEnablingCheckbox = createEmailCheckBox(parent, "Enable email integration"); //$NON-NLS-1$
		Group emailGroupBox = new Group(parent, SWT.NULL);
		emailGroupBox.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		emailGroupBox.setLayout(new GridLayout(1, false));
		fromText = createEmailField(emailGroupBox, "From mail"); //$NON-NLS-1$
		smtpHost = createEmailField(emailGroupBox, "SMTP host"); //$NON-NLS-1$
		smtpPort = createEmailField(emailGroupBox, "SMTP port"); //$NON-NLS-1$
		smtpUser = createEmailField(emailGroupBox, "SMTP user"); //$NON-NLS-1$
		smtpPassword = createEmailField(emailGroupBox, "SMTP password", true); //$NON-NLS-1$

		emailEnablingCheckbox.addSelectionListener(new SelectionAdapter() {

			@Override
			public void widgetSelected(SelectionEvent event) {
				refreshEnabling();
			}
		});

		initializeValues();

		return new Composite(parent, SWT.NULL);
	}

	@Override
	public boolean performOk() {
		IPreferenceStore store = getPreferenceStore();
		store.setValue(EMailerConfigFromFile.SEND_FROM_KEY, fromText.getText());
		store.setValue(GMailConfig.MAIL_SMTP_HOST, smtpHost.getText());
		store.setValue(GMailConfig.MAIL_SMTP_PORT, smtpPort.getText());
		store.setValue(EMailerConfigFromFile.SMTP_USER_KEY, smtpUser.getText());
		store.setValue(EMailerConfigFromFile.SMTP_PASSWORD_KEY, smtpPassword.getText());
		store.setValue(EMAIL_ENABLING_CHECKBOX_KEY, emailEnablingCheckbox.getSelection());
		return super.performOk();
	}

	private void initializeValues() {
		IPreferenceStore store = getPreferenceStore();
		fromText.setText(getFromStoreOrDefault(store, EMailerConfigFromFile.SEND_FROM_KEY, defaultMailConfig.getFrom()));
		smtpHost.setText(getFromStoreOrDefault(store, GMailConfig.MAIL_SMTP_HOST, defaultMailConfig.getSmtpHost()));
		smtpPort.setText(getFromStoreOrDefault(store, GMailConfig.MAIL_SMTP_PORT, defaultMailConfig.getSmtpPort()));
		smtpUser.setText(getFromStoreOrDefault(store, EMailerConfigFromFile.SMTP_USER_KEY, defaultMailConfig.getSmtpUser()));
		smtpPassword.setText(getFromStoreOrDefault(store, EMailerConfigFromFile.SMTP_PASSWORD_KEY, defaultMailConfig.getSmtpPassword()));
		emailEnablingCheckbox.setSelection(store.getBoolean(EMAIL_ENABLING_CHECKBOX_KEY));
		refreshEnabling();
	}

	private String getFromStoreOrDefault(IPreferenceStore store, String key, String defaultValue) {
		String fromStore = store.getString(key);
		return fromStore == null || fromStore.isEmpty() ? defaultValue : fromStore;
	}

	private Text createEmailField(Composite parent, String labelText) {
		return createEmailField(parent, labelText, false);
	}

	private Text createEmailField(Composite parent, String labelText, boolean password) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(2, false));
		Label label = new Label(composite, SWT.NONE);
		label.setText(labelText + ": ");
		int style = SWT.BORDER;
		style = password ? style | SWT.PASSWORD : style;
		Text fromText = new Text(composite, style);
		fromText.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		return fromText;
	}

	private Button createEmailCheckBox(Composite parent, String label) {
		Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayoutData(new GridData(GridData.GRAB_HORIZONTAL | GridData.HORIZONTAL_ALIGN_FILL));
		composite.setLayout(new GridLayout(1, false));
		final Button emailCheckbox = new Button(composite, SWT.CHECK);
		emailCheckbox.setText(label);
		return emailCheckbox;
	}

	private void refreshEnabling() {
		boolean enabling = emailEnablingCheckbox.getSelection();
		fromText.setEnabled(enabling);
		smtpHost.setEnabled(enabling);
		smtpPort.setEnabled(enabling);
		smtpUser.setEnabled(enabling);
		smtpPassword.setEnabled(enabling);
	}

	private void initDefaultMailConfig() {
		Bundle bundle = Platform.getBundle(McoreUIPlugin.PLUGIN_ID);
		Path path = new Path(EMailerConfigFromFile.DEFAULT_CONFIG_PATH);
		URL url = FileLocator.find(bundle, path, null);
		try {
			defaultMailConfig = new GMailConfig(url);
		} catch (IOException e) {
			return;
		}
	}
}
