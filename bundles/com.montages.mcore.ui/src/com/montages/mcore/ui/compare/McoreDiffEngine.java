package com.montages.mcore.ui.compare;

import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.diff.DefaultDiffEngine;
import org.eclipse.emf.ecore.EReference;

public class McoreDiffEngine extends DefaultDiffEngine {

	public McoreDiffEngine() {
		super(new McoreDifProcessor());
	}

	@Override
	protected void computeDifferences(Match match, EReference reference, boolean checkOrdering) {
		super.computeDifferences(match, reference, checkOrdering);
	}

}
