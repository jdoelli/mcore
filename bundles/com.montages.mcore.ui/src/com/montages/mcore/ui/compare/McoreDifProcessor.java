package com.montages.mcore.ui.compare;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.compare.DifferenceKind;
import org.eclipse.emf.compare.DifferenceSource;
import org.eclipse.emf.compare.Match;
import org.eclipse.emf.compare.diff.DiffBuilder;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;

import com.montages.mcore.MExplicitlyTyped;
import com.montages.mcore.MOperationSignature;
import com.montages.mcore.MParameter;
import com.montages.mcore.MPropertiesContainer;
import com.montages.mcore.MProperty;
import com.montages.mcore.MVariable;
import com.montages.mcore.McorePackage;
import com.montages.mcore.annotations.AnnotationsPackage;
import com.montages.mcore.annotations.MClassifierAnnotations;
import com.montages.mcore.annotations.MExprAnnotation;
import com.montages.mcore.annotations.MOperationAnnotations;
import com.montages.mcore.annotations.MPropertyAnnotations;
import com.montages.mcore.expressions.ExpressionsPackage;
import com.montages.mcore.expressions.MAbstractExpressionWithBase;
import com.montages.mcore.expressions.MAbstractIf;
import com.montages.mcore.expressions.MApplication;
import com.montages.mcore.expressions.MElse;
import com.montages.mcore.expressions.MElseIf;
import com.montages.mcore.expressions.MNamedExpression;
import com.montages.mcore.expressions.MThen;
import com.montages.mcore.objects.ObjectsPackage;

public class McoreDifProcessor extends DiffBuilder {

	@Override
	public void referenceChange(Match match, EReference reference, EObject value, DifferenceKind kind, DifferenceSource source) {
		if (isInternal(reference)) {
			return;
		}

		for (CompareRule<?> rule : getRefRules()) {
			if (rule.isApplicableRule(reference, kind)) {
				if (rule.isMatch(match, source)) {
					return;
				}
			}
		}

		if (!isMatchCorrect(match, source)) {
			return;
		}

		if (DifferenceKind.ADD == kind || DifferenceKind.DELETE == kind) {
			EObject leftEO = match.getComparison().isThreeWay() ? match.getOrigin() : match.getLeft();
			EObject rightEO = (match.getComparison().isThreeWay() && source == DifferenceSource.LEFT) ? match.getLeft() : match.getRight();
			if (leftEO == null || rightEO == null) {
				return;
			}

			if (leftEO != null && rightEO != null) {
				if (reference.isMany()) {
					//simple check
					//common rule
					//will be change kind if size equals
					if (((List<?>) leftEO.eGet(reference)).size() == ((List<?>) rightEO.eGet(reference)).size()) {
						return;
					}
				} else {
					Object lValue = leftEO.eGet(reference);
					Object rValue = rightEO.eGet(reference);
					//will be change kind
					if ((lValue == null && rValue == null) || (rValue != null && lValue != null)) {
						return;
					}
				}
			}
		}
		if (!match.getSubmatches().isEmpty() && kind != DifferenceKind.ADD && kind == DifferenceKind.DELETE) {
			if (kind == DifferenceKind.CHANGE) {
				EObject leftEO = match.getComparison().isThreeWay() ? match.getOrigin() : match.getLeft();
				EObject rightEO = (match.getComparison().isThreeWay() && source == DifferenceSource.LEFT) ? match.getLeft() : match.getRight();
				if (leftEO != null && rightEO != null) {
					if (leftEO.eIsProxy() || rightEO.eIsProxy()) {
						return;
					}
				}
			}
		}

		if (value.eResource() != null) {
			//			if ("ecore".equals(value.eResource().getURI().fileExtension())) {
			//				return;
			//			}
			if ("genmodel".equals(value.eResource().getURI().fileExtension())) {
				return;
			}
			if ("http://www.eclipse.org/emf/2002/Ecore".equals(value.eResource().getURI().toString())) {
				return;
			}
		}
		if (value.eIsProxy()) {
			return;
		}
		super.referenceChange(match, reference, value, kind, source);
	}

	protected boolean isInternal(EReference reference) {
		if (McorePackage.eINSTANCE.getMPackage_InternalEPackage() == reference) {
			return true;
		}
		if (McorePackage.eINSTANCE.getMClassifier_InternalEClassifier() == reference) {
			return true;
		}
		if (McorePackage.eINSTANCE.getMOperationSignature_InternalEOperation() == reference) {
			return true;
		}
		if (McorePackage.eINSTANCE.getMProperty_InternalEStructuralFeature() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance__InternalDataValuesAsString() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance__InternalLiteralValuesAsString() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance__InternalReferencedObjectsAsString() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalContainedObject() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalDataValue() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalLiteralValue() == reference) {
			return true;
		}
		if (ObjectsPackage.eINSTANCE.getMPropertyInstance_InternalReferencedObject() == reference) {
			return true;
		}
		if (McorePackage.eINSTANCE.getMParameter_InternalEParameter() == reference) {
			return true;
		}
		if (McorePackage.eINSTANCE.getMLiteral_InternalEEnumLiteral() == reference) {
			return true;
		}
		return false;
	}

	public boolean isMatchCorrect(Match match, DifferenceSource source) {
		if (match.getComparison().isThreeWay()) {
			if (match.getOrigin() == null) {
				return false;
			}
			if (DifferenceSource.LEFT == source) {
				if (match.getLeft() == null) {
					return false;
				}
			} else {
				if (match.getRight() == null) {
					return false;
				}
			}
		} else {
			if (match.getLeft() == null || match.getRight() == null) {
				return false;
			}
		}
		return true;
	}

	@Override
	public void resourceAttachmentChange(Match match, String uri, DifferenceKind kind, DifferenceSource source) {
		if (uri != null && uri.endsWith(".ecore")) {
			return;
		}
		if (uri != null && uri.endsWith(".genmodel")) {
			return;
		}
		super.resourceAttachmentChange(match, uri, kind, source);
	}

	private List<CompareRule<?>> myRules;

	public List<CompareRule<?>> getRefRules() {
		if (myRules == null) {
			myRules = new ArrayList<McoreDifProcessor.CompareRule<?>>();
			myRules.add(new MPropertyOperationSignatureDelete());
			myRules.add(new MPropertyOperationSignatureAdd());
			myRules.add(new MPropertyAnnotationPropertyChange());
			myRules.add(new MPropertyAnnotationInitValueMove());
			myRules.add(new MPropertyAnnotationChoiceConstructionMove());
			myRules.add(new MAbstractExpressionWithBaseChange());
			myRules.add(new MClassifierAnnotationsPropertyAnnotationsAdd());
			myRules.add(new MClassifierAnnotationsPropertyAnnotationsDelete());
			myRules.add(new MExplicitlyTypedTypeChange());
			myRules.add(new MOperationSignatureParameterMove());
			myRules.add(new MOperationAnnotationsOperationSignatureChange());
			myRules.add(new MPropertyAnnotationResultDelete());
			myRules.add(new MPropertyAnnotationResultAdd());
			myRules.add(new MExprAnnotationExpressionAdd());
			myRules.add(new MExprAnnotationExpressionDelete());
			myRules.add(new MNamedExpressionExpressionMove());
			myRules.add(new MNamedExpressionExpressionAdd());
			myRules.add(new MNamedExpressionExpressionDelete());
			myRules.add(new MAbstractIfConditionAdd());
			myRules.add(new MAbstractIfConditionDelete());
			myRules.add(new MAbstractIfThenPartAdd());
			myRules.add(new MAbstractIfThenPartDelete());
			myRules.add(new MAbstractIfElseIfPartAdd());
			myRules.add(new MAbstractIfElseIfPartDelete());
			myRules.add(new MAbstractIfElsePartAdd());
			myRules.add(new MAbstractIfElsePartDelete());
			myRules.add(new MElseExpressionAdd());
			myRules.add(new MElseExpressionDelete());
			myRules.add(new MElseExpressionMove());
			myRules.add(new MElseIfConditionAdd());
			myRules.add(new MElseIfConditionDelete());
			myRules.add(new MElseIfThenPartAdd());
			myRules.add(new MElseIfThenPartDelete());
			myRules.add(new MThenExpressionMove());
			myRules.add(new MApplicationOperandsMove());
		}
		return myRules;
	}

	public static class MPropertyAnnotationInitValueMove extends CheackNullMove<MPropertyAnnotations> {

		public MPropertyAnnotationInitValueMove() {
			super(MPropertyAnnotations.class, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_InitValue());
		}
	}

	public static class MApplicationOperandsMove extends CheackNullMove<MApplication> {

		public MApplicationOperandsMove() {
			super(MApplication.class, ExpressionsPackage.eINSTANCE.getMApplication_Operands());
		}
	}

	public static class MThenExpressionMove extends CheackNullMove<MThen> {

		public MThenExpressionMove() {
			super(MThen.class, ExpressionsPackage.eINSTANCE.getMThen_Expression());
		}
	}

	public static class MElseIfThenPartAdd extends CheackNullAdd<MElseIf> {

		public MElseIfThenPartAdd() {
			super(MElseIf.class, ExpressionsPackage.eINSTANCE.getMElseIf_ThenPart());
		}
	}

	public static class MElseIfThenPartDelete extends CheackNullDelete<MElseIf> {

		public MElseIfThenPartDelete() {
			super(MElseIf.class, ExpressionsPackage.eINSTANCE.getMElseIf_ThenPart());
		}
	}

	public static class MElseIfConditionAdd extends CheackNullAdd<MElseIf> {

		public MElseIfConditionAdd() {
			super(MElseIf.class, ExpressionsPackage.eINSTANCE.getMElseIf_Condition());
		}
	}

	public static class MElseIfConditionDelete extends CheackNullDelete<MElseIf> {

		public MElseIfConditionDelete() {
			super(MElseIf.class, ExpressionsPackage.eINSTANCE.getMElseIf_Condition());
		}
	}

	public static class MElseExpressionAdd extends CheackNullAdd<MElse> {

		public MElseExpressionAdd() {
			super(MElse.class, ExpressionsPackage.eINSTANCE.getMElse_Expression());
		}
	}

	public static class MElseExpressionMove extends CheackNullMove<MElse> {

		public MElseExpressionMove() {
			super(MElse.class, ExpressionsPackage.eINSTANCE.getMElse_Expression());
		}
	}

	public static class MElseExpressionDelete extends CheackNullDelete<MElse> {

		public MElseExpressionDelete() {
			super(MElse.class, ExpressionsPackage.eINSTANCE.getMElse_Expression());
		}
	}

	public static class MAbstractIfElsePartAdd extends CheackNullAdd<MAbstractIf> {

		public MAbstractIfElsePartAdd() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ElsePart());
		}
	}

	public static class MAbstractIfElsePartDelete extends CheackNullDelete<MAbstractIf> {

		public MAbstractIfElsePartDelete() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ElsePart());
		}
	}

	public static class MAbstractIfElseIfPartAdd extends CheackNullAdd<MAbstractIf> {

		public MAbstractIfElseIfPartAdd() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ElseifPart());
		}
	}

	public static class MAbstractIfElseIfPartDelete extends CheackNullDelete<MAbstractIf> {

		public MAbstractIfElseIfPartDelete() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ElseifPart());
		}
	}

	public static class MAbstractIfThenPartAdd extends CheackNullAdd<MAbstractIf> {

		public MAbstractIfThenPartAdd() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ThenPart());
		}
	}

	public static class MAbstractIfThenPartDelete extends CheackNullDelete<MAbstractIf> {

		public MAbstractIfThenPartDelete() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_ThenPart());
		}
	}

	public static class MAbstractIfConditionAdd extends CheackNullAdd<MAbstractIf> {

		public MAbstractIfConditionAdd() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_Condition());
		}
	}

	public static class MAbstractIfConditionDelete extends CheackNullDelete<MAbstractIf> {

		public MAbstractIfConditionDelete() {
			super(MAbstractIf.class, ExpressionsPackage.eINSTANCE.getMAbstractIf_Condition());
		}
	}

	public static class MNamedExpressionExpressionDelete extends CheackNullDelete<MNamedExpression> {

		public MNamedExpressionExpressionDelete() {
			super(MNamedExpression.class, ExpressionsPackage.eINSTANCE.getMNamedExpression_Expression());
		}
	}

	public static class MNamedExpressionExpressionAdd extends CheackNullAdd<MNamedExpression> {

		public MNamedExpressionExpressionAdd() {
			super(MNamedExpression.class, ExpressionsPackage.eINSTANCE.getMNamedExpression_Expression());
		}
	}

	public static class MNamedExpressionExpressionMove extends CheackNullMove<MNamedExpression> {

		public MNamedExpressionExpressionMove() {
			super(MNamedExpression.class, ExpressionsPackage.eINSTANCE.getMNamedExpression_Expression());
		}
	}

	public static class MExprAnnotationExpressionAdd extends CheackNullAdd<MExprAnnotation> {

		public MExprAnnotationExpressionAdd() {
			super(MExprAnnotation.class, AnnotationsPackage.eINSTANCE.getMExprAnnotation_NamedExpression());
		}
	}

	public static class MExprAnnotationExpressionDelete extends CheackNullDelete<MExprAnnotation> {

		public MExprAnnotationExpressionDelete() {
			super(MExprAnnotation.class, AnnotationsPackage.eINSTANCE.getMExprAnnotation_NamedExpression());
		}
	}

	public static class MOperationAnnotationsOperationSignatureChange extends CompareRule<MOperationAnnotations> {

		public MOperationAnnotationsOperationSignatureChange() {
			super(MOperationAnnotations.class, AnnotationsPackage.eINSTANCE.getMOperationAnnotations_OperationSignature());
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.CHANGE;
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			MOperationAnnotations left = getLeft(match);
			MOperationAnnotations right = getRight(match, source);
			if (left == null || right == null) {
				return true;
			}
			MOperationSignature leftSignature = left.getOperationSignature();
			MOperationSignature rightSignature = right.getOperationSignature();

			for (MParameter leftPrameter : leftSignature.getParameter()) {
				boolean hasRightPair = false;
				String leftName = leftPrameter.getName() == null ? "" : leftPrameter.getName();
				for (MParameter rightParameter : rightSignature.getParameter()) {
					String rightName = rightParameter.getName() == null ? "" : rightParameter.getName();
					if (leftName.equals(rightName)) {
						if (leftPrameter.getMandatory() != null && leftPrameter.getMandatory().equals(rightParameter.getMandatory())) {
							if (leftPrameter.getSimpleType() == rightParameter.getSimpleType()) {
								if (leftPrameter.getSingular() != null && rightParameter.getSingular().equals(rightParameter.getSingular())) {
									hasRightPair = true;
									break;
								}
							}
						}
					}
				}
				if (!hasRightPair) {
					return false;
				}
			}

			return true;
		}
	}

	public static class MOperationSignatureParameterMove extends CheackNullMove<MOperationSignature> {

		public MOperationSignatureParameterMove() {
			super(MOperationSignature.class, McorePackage.eINSTANCE.getMOperationSignature_Parameter());
		}
	}

	public static class MExplicitlyTypedTypeChange extends CompareRule<MExplicitlyTyped> {

		public MExplicitlyTypedTypeChange() {
			super(MExplicitlyTyped.class, McorePackage.eINSTANCE.getMExplicitlyTyped_Type());
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.CHANGE;
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			MExplicitlyTyped left = getLeft(match);
			MExplicitlyTyped right = getRight(match, source);
			if (left instanceof MParameter && right instanceof MParameter) {
				MParameter leftPrameter = (MParameter) left;
				MParameter rightParameter = (MParameter) right;
				String leftName = leftPrameter.getName() == null ? "" : leftPrameter.getName();
				String rightName = rightParameter.getName() == null ? "" : rightParameter.getName();
				if (leftName.equals(rightName)) {
					if (leftPrameter.getMandatory() != null && leftPrameter.getMandatory().equals(rightParameter.getMandatory())) {
						if (leftPrameter.getSimpleType() == rightParameter.getSimpleType()) {
							if (leftPrameter.getSingular() != null && rightParameter.getSingular().equals(rightParameter.getSingular())) {
								return true;
							}
						}
					}
				}
			}
			return false;
		}
	}

	public static class MClassifierAnnotationsPropertyAnnotationsDelete extends CheackNull<MClassifierAnnotations> {

		public MClassifierAnnotationsPropertyAnnotationsDelete() {
			super(MClassifierAnnotations.class, AnnotationsPackage.eINSTANCE.getMClassifierAnnotations_PropertyAnnotations());
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			if (super.isMatch(match, source)) {
				return true;
			}
			MPropertiesContainer leftCont = (MPropertiesContainer) getLeft(match).eContainer();
			MPropertiesContainer rightCont = (MPropertiesContainer) getRight(match, source).eContainer();
			if (leftCont.getAnnotations().getPropertyAnnotations().size() == rightCont.getAnnotations().getPropertyAnnotations().size()) {
				return true;
			}
			return false;
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.DELETE;
		}
	}

	public static class MClassifierAnnotationsPropertyAnnotationsAdd extends CheackNullAdd<MClassifierAnnotations> {

		public MClassifierAnnotationsPropertyAnnotationsAdd() {
			super(MClassifierAnnotations.class, AnnotationsPackage.eINSTANCE.getMClassifierAnnotations_PropertyAnnotations());
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			if (super.isMatch(match, source)) {
				return true;
			}
			MPropertiesContainer leftCont = (MPropertiesContainer) getLeft(match).eContainer();
			MPropertiesContainer rightCont = (MPropertiesContainer) getRight(match, source).eContainer();
			if (leftCont.getAnnotations().getPropertyAnnotations().size() == rightCont.getAnnotations().getPropertyAnnotations().size()) {
				return true;
			}
			return false;
		}
	}

	public static class MAbstractExpressionWithBaseChange extends CompareRule<MAbstractExpressionWithBase> {

		public MAbstractExpressionWithBaseChange() {
			super(MAbstractExpressionWithBase.class, ExpressionsPackage.eINSTANCE.getMAbstractExpressionWithBase_BaseVar());
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.CHANGE;
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			MAbstractExpressionWithBase left = getLeft(match);
			MAbstractExpressionWithBase right = getRight(match, source);
			if (left == null || right == null) {
				return true;
			}
			MVariable leftVar = left.getBaseVar();
			MVariable rightVar = right.getBaseVar();
			if (rightVar == null || leftVar == null) {
				return true;
			}
			if (leftVar.getName() != null) {
				if (leftVar.getName().equals(rightVar.getName())) {
					return true;
				}
			} else {
				if (rightVar.getName() == null) {
					return true;
				}
			}
			return false;
		}
	}

	public static class MPropertyAnnotationResultAdd extends CheackNullAdd<MPropertyAnnotations> {

		public MPropertyAnnotationResultAdd() {
			super(MPropertyAnnotations.class, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_Result());
		}
	}

	public static class MPropertyAnnotationResultDelete extends CheackNullDelete<MPropertyAnnotations> {

		public MPropertyAnnotationResultDelete() {
			super(MPropertyAnnotations.class, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_Result());
		}
	}

	public static class MPropertyAnnotationPropertyChange extends CheackNull<MPropertyAnnotations> {

		public MPropertyAnnotationPropertyChange() {
			super(MPropertyAnnotations.class, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_Property());
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.CHANGE;
		}
	}

	public static class MPropertyAnnotationChoiceConstructionMove extends CheackNullMove<AnnotationsPackage> {

		public MPropertyAnnotationChoiceConstructionMove() {
			super(AnnotationsPackage.class, AnnotationsPackage.eINSTANCE.getMPropertyAnnotations_ChoiceConstruction());
		}
	}

	public static class MPropertyOperationSignatureDelete extends MPropertyOperationSignature {

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			MProperty left = getLeft(match);
			MProperty right = getRight(match, source);
			if (left == null || right == null) {
				return false;
			}
			return super.isMatch(match, source);
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.DELETE;
		}
	}

	public static class MPropertyOperationSignatureAdd extends MPropertyOperationSignature {

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.ADD;
		}

	}

	public abstract static class MPropertyOperationSignature extends CompareRule<MProperty> {

		public MPropertyOperationSignature() {
			super(MProperty.class, McorePackage.eINSTANCE.getMProperty_OperationSignature());
		}

		@Override
		public boolean isMatch(Match match, DifferenceSource source) {
			MProperty left = getLeft(match);
			MProperty right = getRight(match, source);
			if (left == null || right == null) {
				return true;
			}
			List<MOperationSignature> leftSignatures = left.getOperationSignature();
			List<MOperationSignature> rightSignatures = right.getOperationSignature();
			if (leftSignatures.size() == rightSignatures.size()) {
				return true;
			}
			return false;
		}
	}

	public abstract static class CheackNullAdd<T> extends CheackNull<T> {

		public CheackNullAdd(Class<T> toCast, EReference reference) {
			super(toCast, reference);
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.ADD;
		}
	}

	public abstract static class CheackNullDelete<T> extends CheackNull<T> {

		public CheackNullDelete(Class<T> toCast, EReference reference) {
			super(toCast, reference);
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.DELETE;
		}
	}

	public abstract static class CheackNullMove<T> extends CheackNull<T> {

		public CheackNullMove(Class<T> toCast, EReference reference) {
			super(toCast, reference);
		}

		@Override
		public DifferenceKind getKind() {
			return DifferenceKind.MOVE;
		}
	}

	public abstract static class CheackNull<T> extends CompareRule<T> {

		public CheackNull(Class<T> toCast, EReference reference) {
			super(toCast, reference);
		}

		public boolean isMatch(Match match, DifferenceSource source) {
			if (getLeft(match) == null || getRight(match, source) == null) {
				return true;
			}
			return false;
		}

	}

	public abstract static class CompareRule<T> {

		private final EReference myReference;

		private final Class<T> myToCast;

		public CompareRule(Class<T> toCast, EReference reference) {
			myToCast = toCast;
			myReference = reference;
		}

		public boolean isApplicableRule(EReference ref, DifferenceKind kind) {
			return ref == myReference && kind == getKind();
		}

		public abstract boolean isMatch(Match match, DifferenceSource source);

		public T getLeft(Match match) {
			EObject left = match.getComparison().isThreeWay() ? match.getOrigin() : match.getLeft();
			if (left == null) {
				return null;
			}
			return myToCast.cast(left);
		}

		public T getRight(Match match, DifferenceSource source) {
			EObject right = match.getComparison().isThreeWay() && source == DifferenceSource.LEFT ? match.getLeft() : match.getRight();
			if (right == null) {
				return null;
			}
			return myToCast.cast(right);
		}

		public abstract DifferenceKind getKind();
	}
}
