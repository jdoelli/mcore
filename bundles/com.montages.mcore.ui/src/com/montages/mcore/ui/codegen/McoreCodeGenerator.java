package com.montages.mcore.ui.codegen;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.codegen.ecore.generator.Generator;
import org.eclipse.emf.codegen.ecore.genmodel.GenFeature;
import org.eclipse.emf.codegen.ecore.genmodel.GenJDKLevel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModel;
import org.eclipse.emf.codegen.ecore.genmodel.GenModelFactory;
import org.eclipse.emf.codegen.ecore.genmodel.GenPackage;
import org.eclipse.emf.codegen.ecore.genmodel.GenResourceKind;
import org.eclipse.emf.codegen.ecore.genmodel.generator.GenBaseGeneratorAdapter;
import org.eclipse.emf.codegen.ecore.genmodel.util.GenModelUtil;
import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.BasicMonitor;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.transaction.util.TransactionUtil;
import org.eclipse.ui.PlatformUI;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclHelper;
import org.xocl.editorconfig.ui.codegen.actions.GenerateLaunchConfigurationAction;
import org.xocl.editorconfig.ui.codegen.actions.Messages;
import org.xocl.editorconfig.ui.codegen.utils.GenModelCustomizer;

import com.montages.common.McoreCommandUtil;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.workspace.FileFunctions;
import com.montages.mcore.util.McoreUtil;
import com.montages.mcore.util.ResourceService;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

public class McoreCodeGenerator {

	public void generate(Map<MComponent, Set<MComponent>> dependencies, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor(); 
		}

		Map<MComponent, GenModel> models = new HashMap<MComponent, GenModel>();
		Set<MComponent> components = filter(dependencies.keySet());
		
		ResourceSet newResourceSet = ResourceService.createResourceSet();
		Map<Object, McoreDiagramEditingDoamin> owners = new HashMap<Object, McoreEditingDomainFactory.McoreDiagramEditingDoamin>(components.size());

		McoreEditingDomainFactory domainFactory = McoreEditingDomainFactory.getInstance();
		try {
			for (MComponent c : components) {
				URI uri = EcoreUtil.getURI(c);
				String id = PathUtils.buildDomainID(newResourceSet.getURIConverter().normalize(uri));
				//create diagram domain for generate operation
				McoreDiagramEditingDoamin domain = domainFactory.exists(id)? domainFactory.createEditingDomain(id, c) : null;
				owners.put(c, domain);
				GenModel g = getOrCreateGenModel(c, domain == null ? newResourceSet : domain.getResourceSet(), monitor);
				if (g != null) {
					models.put(c, g);
				}
			}

			for (MComponent c : components) {
				GenModel m = models.get(c);
				for (MComponent d : dependencies.get(c)) {
					if (models.containsKey(d)) {
						m.getUsedGenPackages().addAll(models.get(d).getGenPackages());
					}
				}
			}

			for (MComponent c : models.keySet()) {
				doGenerate(models.get(c), owners.get(c), monitor);
			}

			for (MComponent c : models.keySet()) {
				createLaunchConfiguration(models.get(c), SubMonitor.convert(monitor, 1));
			}
		} finally {
			for (Object owner : owners.keySet()) {
				domainFactory.dispose(owners.get(owner), owner);
			}
		}
	}

	private Set<MComponent> filter(Set<MComponent> components) {
		for (Iterator<MComponent> it = components.iterator(); it.hasNext();) {
			MComponent current = it.next();
			if (current.getEName().equals("coreTypes")) {
				it.remove();
			}
		}
		return components;
	}

	protected void doGenerate(final GenModel genModel, final McoreDiagramEditingDoamin domain, final IProgressMonitor monitor) {
		URI genModelURI = EcoreUtil.getURI(genModel).trimFragment();
		if (!genModelURI.isPlatformResource()) return;

		if (!shouldGenerate(genModel)) return;

		monitor.setTaskName("Generating code ...");
		final GenerateModel cmd = new GenerateModel(genModel, monitor);

		McoreUIPlugin.waitAsyncRun(new Runnable() {
			
			@Override
			public void run() {
				McoreCommandUtil.executeUnsafeCommand(cmd, domain);
				if (domain != null) {
					domain.publish(McoreTransactionEventType.SAVE, this);
				} else {
					try {
						genModel.eResource().save(null);
					} catch (IOException e) {
						McoreUIPlugin.log(e);
					}
				}
			}
		});
		

		final BasicDiagnostic diagnostic = cmd.getDiagnostic();
		if (PlatformUI.isWorkbenchRunning() && diagnostic.getSeverity() == Diagnostic.ERROR) {
			PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
				public void run() {
					DiagnosticDialog.openProblem(
							PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
							Messages.GenerateCodeAction_GenerationError,
							Messages.GenerateCodeAction_GenerationErrorDesc,
							diagnostic);
				}
			});
		}
	}

	/**
	 * Checks if the ecore file's timestamp has been changed. If true then
	 * the timestampt is store, and the generation can happen. If false the 
	 * generation should abort.
	 */
	private boolean shouldGenerate(GenModel genModel) {
		boolean shouldGenerate = false;

		for (GenPackage pck: genModel.getGenPackages()) {
			EPackage firstPackage = pck.getEcorePackage();

			if (firstPackage != null && firstPackage.eResource() != null) {
				IFile file = FileFunctions.getFile(firstPackage.eResource().getURI());

				if (file != null) {
					boolean hasChanged = FileFunctions.hasChanged(file);
					if (hasChanged) {					
						FileFunctions.setModificationStamp(file);
					}
					shouldGenerate |= hasChanged;
				}

			}
		}

		return shouldGenerate;
	}

	protected void createLaunchConfiguration(GenModel genModel, IProgressMonitor monitor) {
		URI genModelURI = EcoreUtil.getURI(genModel).trimFragment();
		if (!genModelURI.isPlatformResource()) return;

		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(genModel.getModelPluginID().replace(".base", ""));
		GenerateLaunchConfigurationAction a = new GenerateLaunchConfigurationAction(genModel, project, genModel.getModelName());
		try {
			a.run(monitor);
		} catch (InvocationTargetException e) {
			McoreUIPlugin.log(e);
		} catch (InterruptedException e) {
			McoreUIPlugin.log(e);
		}
	}

	protected IProject createProject(MComponent component, String type, IProgressMonitor monitor) {
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		IProject project = root.getProject(component.getDerivedBundleName());
		IProject baseProject = root.getProject(project.getName() + "." + type);

		if (!baseProject.exists()) {
			try {
				baseProject.create(monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}
		if (!baseProject.isOpen()) {
			try {
				baseProject.open(monitor);
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
			}
		}

		return baseProject;
	}

	protected GenModel getOrCreateGenModel(MComponent component, final ResourceSet resourceSet, IProgressMonitor monitor) {
		if (component == null || component.eResource() == null || component.eResource().getResourceSet() == null) return null;

		component = McoreUtil.findComponentInTransactionResources(component);

		final URI normalized = resourceSet.getURIConverter().normalize(component.eResource().getURI());

		if (normalized.isPlatformPlugin()) {
			return getGenModel(resourceSet, component);
		} else {
			return createGenModel(resourceSet, component, monitor);
		}
	}

	protected GenModel getGenModel(ResourceSet resourceSet, MComponent component) {
		URI genURI = getGenModelURI(resourceSet, component);
		Resource resource = null;
		try {
			resource = resourceSet.getResource(genURI, true);
		} catch (Exception e) {
			return null;
		}

		if (resource != null && !resource.getContents().isEmpty()) {
			return (GenModel) resource.getContents().get(0);
		}

		return null;
	}

	protected GenModel createGenModel(final ResourceSet resourceSet, final MComponent component, IProgressMonitor monitor) {
		IProject modelProject = createProject(component, "base", monitor);
		IProject editProject = createProject(component, "edit", monitor);
		IProject editorProject = createProject(component, "editor", monitor);

		final URI genURI = getGenModelURI(resourceSet, component);
		final GenModel genModel = GenModelFactory.eINSTANCE.createGenModel();
        
		List<EPackage> ecores = collectEPackages(component);
		genModel.getForeignModel().clear();

		for (EPackage p: ecores) {
			URI uri = p.eResource().getURI();
			URI normalized = resourceSet.getURIConverter().normalize(uri);
			genModel.getForeignModel().add("../.." + normalized.toPlatformString(true));
		}

		genModel.setModelName(XoclHelper.firstUpperCase(component.getEName()));
		genModel.setRootExtendsClass("org.eclipse.emf.ecore.impl.MinimalEObjectImpl$Container");
		genModel.initialize(ecores);
		
		genModel.setDynamicTemplates(true);
		genModel.setForceOverwrite(false);
		genModel.setTemplateDirectory("platform:/plugin/org.xocl.templates/templates");
		genModel.getTemplatePluginVariables().add("org.xocl.core.opensource");
		genModel.setOperationReflection(true);
		genModel.setContainmentProxies(true);
		genModel.setComplianceLevel(GenJDKLevel.JDK60_LITERAL);

		genModel.setModelDirectory(modelProject.getFolder("src").getFullPath().toString());
		genModel.setModelPluginID(modelProject.getName());
		genModel.getModelPluginVariables().add(component.getDerivedBundleName());

		genModel.setEditDirectory(editProject.getFolder("src").getFullPath().toString());
		genModel.setEditPluginID(editProject.getName());

		genModel.setEditorDirectory(editorProject.getFolder("src").getFullPath().toString());
		genModel.setEditorPluginID(editorProject.getName());

		genModel.setTestsDirectory(null);
		genModel.setTestsPluginID(null);

		genModel.setCanGenerate(true);
		genModel.setImportOrganizing(true);
		genModel.setCodeFormatting(true);

		for (GenPackage g: genModel.getGenPackages()) {
			adjustGenPackages(g);
		}
		GenModelCustomizer.adjustEcoreAnnotatedGenModelProperties(genModel.getGenPackages());

		this.addCopyrightInformation(component, genModel);
		Resource resource;
		try {
			resource = resourceSet.getResource(genURI, true);
		} catch (Exception e) {
			resource = resourceSet.getResource(genURI, false);
		}
		final Resource resourceToSave = resource;
		McoreUIPlugin.waitAsyncRun(new Runnable() {
			
			@Override
			public void run() {
				MComponent resolvedComponent = McoreUtil.findComponentInTransactionResources(component);
				McoreDiagramEditingDoamin domain = (McoreDiagramEditingDoamin)TransactionUtil.getEditingDomain(resolvedComponent);
				McoreCommandUtil.executeUnsafeCommand(new UpdateGenModelResource(resourceToSave, genModel), domain);

				if (domain != null) {
					domain.publish(McoreTransactionEventType.SAVE, new Object());
				} else {
					try {
						resourceToSave.save(null);
					} catch (IOException e) {
						McoreUIPlugin.log(e);
					}
				}
			}
		});

		return resourceToSave.getContents().isEmpty() ? null: (GenModel)resourceToSave.getContents().get(0);
	}
	
    protected void addCopyrightInformation(MComponent component, GenModel genModel)
    {
        final String copyrightFile = "license.txt";
        IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
	    IProject project = root.getProject(component.getDerivedBundleName());
        IFile f = project.getFolder("model").getFile(copyrightFile);
	    String copyrightText = "";
        InputStream in;
        if(f.isAccessible())
				try {
					
					ByteArrayOutputStream result = new ByteArrayOutputStream();
					byte[] buffer = new byte[1024];
					int length;
				
						in = f.getContents();
						while ((length = in.read(buffer)) != -1) {
						    result.write(buffer, 0, length);
						}
						copyrightText= result.toString("UTF-8");
						in.close();
						genModel.setCopyrightFields(true);
						genModel.setCopyrightText(copyrightText);
						return;
				}
					    catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		genModel.setCopyrightText("");

	
    }

	private static void adjustGenPackages(GenPackage genPackage) {
		//we want to enforce loadInitialization even for big models, see GenPackageImpl#isBigModel 
		genPackage.setLoadInitialization(false);
		genPackage.setLiteralsInterface(true);
		
		genPackage.setResource(GenResourceKind.XML_LITERAL);

		List<GenFeature> allGenFeatures = genPackage.getAllGenFeatures();
		for (GenFeature genFeature : allGenFeatures) {
			EStructuralFeature eStructuralFeature = genFeature.getEcoreFeature();
			if (eStructuralFeature != null) {
				EClass eClass = eStructuralFeature.getEContainingClass();
				if (eClass != null) {
					String propertyCategory = XoclEmfUtil.findPropertyCategoryAnnotationText(eStructuralFeature, eClass);
					if (propertyCategory != null) {
						genFeature.setPropertyCategory(propertyCategory);
					}
					String propertyMultiline = XoclEmfUtil.findPropertyMultilineAnnotationText(eStructuralFeature, eClass);
					if (propertyMultiline != null) {
						genFeature.setPropertyMultiLine(Boolean.parseBoolean(propertyMultiline));
					}
				}
			}
		}

		for (GenPackage sub: genPackage.getSubGenPackages()) {
			adjustGenPackages(sub);
		}
	}

	private URI getGenModelURI(ResourceSet resourceSet, MComponent component) {
		URI normalized = resourceSet.getURIConverter().normalize(component.eResource().getURI());
		return getGenModelURI(resourceSet, normalized, component.getDerivedBundleName());
	}

	protected URI getGenModelURI(ResourceSet resourceSet, URI normalized, String bundleName) {
		final String fileName = normalized.trimFileExtension().lastSegment() + ".genmodel";

		if (normalized.isPlatformPlugin()) {
			return URI.createPlatformPluginURI(bundleName + "/model/" + fileName, true);
		} else {
			return URI.createPlatformResourceURI(bundleName + "/model/" + fileName, true);
		}
	}

	protected List<EPackage> collectEPackages(MComponent component) {
		List<EPackage> ecores = new ArrayList<EPackage>();
		for (MPackage p: component.getOwnedPackage()) {
			ecores.add(p.getInternalEPackage());
		}
		return ecores;
	}

	private static class UpdateGenModelResource extends AbstractCommand {

		private Resource myResource;

		private EObject myGenModel;

		public UpdateGenModelResource(Resource resource, EObject genModel) {
			this.myResource = resource;
			this.myGenModel = genModel;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public void execute() {
			if (!myResource.getContents().isEmpty()) {
				myResource.getContents().clear();
			}
			myResource.getContents().add(myGenModel);
		}

		@Override
		public void redo() {
		}
		
	}
	
	private static class GenerateModel extends AbstractCommand {

		final private IProgressMonitor myMonitor;

		final private GenModel myGenModel;

		final BasicDiagnostic diagnostic = new BasicDiagnostic();

		public GenerateModel(GenModel model, IProgressMonitor monitor) {
			myGenModel = model;
			myMonitor = monitor;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public void execute() {
			final Generator generator = GenModelUtil.createGenerator(myGenModel);

			diagnostic.add(generator.generate(myGenModel, GenBaseGeneratorAdapter.MODEL_PROJECT_TYPE, new BasicMonitor.EclipseSubProgress(myMonitor, 50)));
			diagnostic.add(generator.generate(myGenModel, GenBaseGeneratorAdapter.EDIT_PROJECT_TYPE, new BasicMonitor.EclipseSubProgress(myMonitor, 25)));
			diagnostic.add(generator.generate(myGenModel, GenBaseGeneratorAdapter.EDITOR_PROJECT_TYPE, new BasicMonitor.EclipseSubProgress(myMonitor, 25)));
		}

		@Override
		public void redo() {
		}
		
		public BasicDiagnostic getDiagnostic() {
			return diagnostic;
		} 
	}
}
