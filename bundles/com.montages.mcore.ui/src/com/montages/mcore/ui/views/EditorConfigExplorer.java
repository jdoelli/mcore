package com.montages.mcore.ui.views;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.StyledCellLabelProvider;
import org.eclipse.jface.viewers.StyledString;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.ViewerCell;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.montages.mcore.ui.McoreUIPlugin;

public class EditorConfigExplorer extends AbstractTableExplorer<Table, TableViewer> {

	public static final String ID = "com.montages.mcore.ui.views.EditConfigExplorer"; //$NON-NLS-1$	

	private final Listener openListener = new Listener() {
		@Override
		public void handleEvent(Event event) {
			if (event.keyCode == SWT.CR || event.keyCode == SWT.LF || event.type == SWT.MouseDoubleClick) {
				final TableItem[] selected = getViewer().getTable().getSelection();

				if (selected.length == 1) {
					final URI uri = (URI) selected[0].getData();

					Display.getDefault().asyncExec(new Runnable() {
						@Override
						public void run() {							
							final IWorkbenchWindow workbenchWindow = getSite().getWorkbenchWindow();
							final IWorkbenchPage page = workbenchWindow.getActivePage();

							try {
								page.openEditor(new URIEditorInput(uri), "org.xocl.editorconfig.presentation.EditorConfigEditorID");
							} catch (PartInitException e) {
								McoreUIPlugin.log(e);
							}

						}
					});	
				}
			}
		}
	};

	public EditorConfigExplorer() {}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);

		getViewer().setLabelProvider(new EditorsLabelProvider());
		getViewer().setContentProvider(ArrayContentProvider.getInstance());
		getViewer().setInput(getElements());
		getViewer().addFilter(getFilter());
		getViewer().getTable().addListener(SWT.MouseDoubleClick, openListener);
		getViewer().getTable().addListener(SWT.KeyDown, openListener);

		ResourcesPlugin.getWorkspace().addResourceChangeListener(new EditorsWorkspaceListerner());
	}

	@Override
	protected TableViewer createVeiwer(Table parent) {
		return new TableViewer(parent);
	}

	@Override
	protected Table createViewerComposete(Composite parent) {
		final Table table = new Table(parent, SWT.V_SCROLL | SWT.H_SCROLL | SWT.MULTI);
		{
			GridData data = new GridData(GridData.FILL_BOTH);
			data.grabExcessHorizontalSpace = true;
			data.grabExcessVerticalSpace = true;
			table.setLayoutData(data);
		}
		return table;
	}

	@Override
	protected Object getElements() {
		return computeElements().toArray();
	}

	
	private class EditorsWorkspaceListerner implements IResourceChangeListener {
		private final IResourceDeltaVisitor visitor = new IResourceDeltaVisitor() {
			@Override
			public boolean visit(IResourceDelta delta) throws CoreException {
				if ("editorconfig".equals(delta.getResource().getFileExtension()) && 
						delta.getKind() == IResourceDelta.ADDED || 
						delta.getKind() == IResourceDelta.REMOVED ||
						delta.getKind() == IResourceDelta.CHANGED) {

					PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {
						@Override
						public void run() {
							if (!getViewer().getControl().isDisposed()) {
								getViewer().setInput(getElements());
							}
						}
					});
				}
				return false;
			}
		};
		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			if (event != null && event.getDelta() != null) {
				try {
					event.getDelta().accept(visitor);
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private List<URI> computeElements() {
		final List<URI> elements = new ArrayList<URI>();

		try {
			EcorePlugin.getWorkspaceRoot().accept(new IResourceProxyVisitor() {
				@Override
				public boolean visit(IResourceProxy proxy) throws CoreException {
					IResource file = proxy.requestResource();
					if (file != null && "editorconfig".equals(file.getFileExtension())) {
						elements.add(URI.createPlatformResourceURI(file.getFullPath().toString(), false));
					}
					return true;
				}
			}, IResource.FILE);
		} catch (CoreException e) {
			e.printStackTrace();
		}
		return elements;
	}

	public class EditorsLabelProvider extends StyledCellLabelProvider implements ILabelProvider {

		private final Image image = McoreUIPlugin.getImage("image_table").createImage();

		@Override
		public void update(ViewerCell cell) {
			Object element = cell.getElement();
			int index = cell.getColumnIndex();

			if (element instanceof URI && index == 0) {
				URI elementURI = (URI) element;
				String fileName = elementURI.lastSegment();
				StyledString label = new StyledString(fileName);

				if (elementURI.isPlatformResource()) {
					label.append(" [workspace]", StyledString.QUALIFIER_STYLER);
				} else {
					label.append(" [plugin]", StyledString.QUALIFIER_STYLER);
				}

				cell.setText(label.getString());
				cell.setStyleRanges(label.getStyleRanges());
				cell.setImage(image);
			}
		}

		@Override
		public Image getImage(Object element) {
			return image;
		}

		@Override
		public String getText(Object element) {
			return element.toString();
		}

	}
	

}
