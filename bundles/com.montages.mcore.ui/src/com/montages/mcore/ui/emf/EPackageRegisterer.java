package com.montages.mcore.ui.emf;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;

import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MRepository;

public class EPackageRegisterer {

	public static void register(MRepository repository, ResourceSet resourceSet) {
		for (MComponent component: repository.getComponent()) {
			register(component, resourceSet);
		}
	}

	public static void register(MComponent component, ResourceSet resourceSet) {
		for (MPackage mPackage: component.getOwnedPackage()) {
			register(mPackage, resourceSet);
		}
	}
	
	public static void register(MPackage mPackage, ResourceSet resourceSet) {
		register(mPackage.getInternalEPackage(), resourceSet);
		for (MPackage subPackage: mPackage.getSubPackage()) {
			register(subPackage, resourceSet);
		}
	}

	public static void register(EPackage ePackage, ResourceSet resourceSet) {
		if (ePackage != null) {
			if (resourceSet != null) {
				resourceSet.getPackageRegistry().put(ePackage.getNsURI(), ePackage);
			} else {
				EPackage.Registry.INSTANCE.put(ePackage.getNsURI(), ePackage);
			}
		}
	}

}
