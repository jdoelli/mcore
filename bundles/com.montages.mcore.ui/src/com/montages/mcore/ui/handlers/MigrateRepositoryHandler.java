package com.montages.mcore.ui.handlers;

import java.util.Collection;
import java.util.List;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.ecore.resource.Resource;

import com.montages.mcore.MComponent;
import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.ui.operations.CreateProject;
import com.montages.mcore.ui.operations.GenerateEcore;
import com.montages.mcore.ui.operations.GenerateEditorConfig;
import com.montages.mcore.util.Dependencies;

public class MigrateRepositoryHandler extends ResourceSelectionHandler {

	@Override
	protected void doExcecute(Resource selectedResource, ExecutionEvent event) {
		migrate(selectedResource);
	}

	private void migrate(Resource resource) {
		Collection<MComponent> result = new Importer(resource).getComponents();
		final List<MComponent> sorted = Dependencies.compute(result).sort();

		Job job = new Job("Start migrating " + resource.getURI()) {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Start Import", (sorted.size() * 2) + 1);

				for (MComponent component: sorted) {
					monitor.setTaskName("Migrate " + component.getCalculatedName());
					// remove legacy
					component.getGeneratedEPackage().clear();
					// 
					// import component and initialize it
					//
					try {
						CreateProject.create(component, SubMonitor.convert(monitor, 1));
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}

				GenerateEcore.transform(sorted, SubMonitor.convert(monitor, 1));

				for (MComponent component: sorted) {
					GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1));
				}

				monitor.done();

				return Status.OK_STATUS;
			}
		};
		job.setUser(true);
		job.schedule();
	}

}
