package com.montages.mcore.ui.handlers;

import static org.eclipse.ui.handlers.HandlerUtil.getActiveMenuSelection;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.expressions.IEvaluationContext;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchSite;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.handlers.HandlerUtil;
import org.xocl.editorconfig.plugin.EditorConfigPlugin;

import com.montages.common.resource.EcoreResourceResolver;
import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.operations.GenerateEcore;
import com.montages.mcore.ui.operations.GenerateEditorConfig;
import com.montages.mcore.ui.operations.GenerateInstanceResources;
import com.montages.mcore.ui.operations.GenerateTableEditor;
import com.montages.mcore.util.McoreUtil;
import com.montages.mcore.util.TableEditorInput;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

public class OpenDynamicEditorHandler extends AbstractHandler {

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {

		IEvaluationContext context = (IEvaluationContext) event.getApplicationContext();

		final IStructuredSelection selection = (IStructuredSelection) getActiveMenuSelection(event);
		final Object firstElement = selection != null ? selection.getFirstElement() : context.getVariable("trigger");
		final IWorkbenchSite myWorkbench = selection == null ? (IWorkbenchSite) context.getVariable("myWorkbench")
				: HandlerUtil.getActiveSite(event);

		if (firstElement instanceof MResource) {

			final MResource selected = (MResource) firstElement;
			try {
				if (myWorkbench == null || myWorkbench.getWorkbenchWindow() == null)
					return null;
				myWorkbench.getWorkbenchWindow().run(true, false, new IRunnableWithProgress() {
					@Override
					public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
						monitor.beginTask("Opening Dynamic Editor", 3);
						openEditor(selected, event, monitor);
						monitor.done();
					}
				});
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private void openEditor(final MResource selected, final ExecutionEvent event, final IProgressMonitor monitor) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {

				// avoid getting a proxy

				MComponent component = (MComponent) selected.getContainingFolder().getRootFolder().eContainer();

				if (McoreUtil.getRootPackage(selected) == null) {
					GenerateEcore.transform(component, SubMonitor.convert(monitor, 1));
				}

				EPackage ePackage = McoreUtil.getRootPackage(selected);

				if (ePackage == null || component == null)
					return;

				URI baseURI = component.eResource().getURI();
				baseURI = component.eResource().getResourceSet().getURIConverter().normalize(baseURI);

				boolean updateEcore = baseURI.isPlatformResource();

				component = initModel(component, baseURI, updateEcore, monitor);

				IPath path = getResourcePath(selected, ePackage);

				// trim segments /model/model.mcore and add path to instances resource
				URI uri = baseURI.trimSegments(2).appendSegments(path.segments());
				URI editorConfigURI = getEditorConfigURI(ePackage);

				IEditorInput input = new TableEditorInput(uri, editorConfigURI, EcoreUtil.getURI(selected));
				IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				IWorkbenchPage page = workbenchWindow.getActivePage();

				try {
					page.openEditor(input, "com.montages.mcore.ui.editors.DynamicTableEditor");
				} catch (PartInitException e) {
//					McoreUIPlugin.log(e);
					e.printStackTrace();
				}
			}
		});
	}

	private static class InitModelCommand extends AbstractCommand {

		private MComponent component;
		private URI uri;
		private boolean updateEcore;
		private final IProgressMonitor monitor;

		public InitModelCommand(MComponent component, URI uri, boolean updateEcore, final IProgressMonitor monitor) {
			this.component = component;
			this.uri = uri;
			this.updateEcore = updateEcore;
			this.monitor = monitor;
		}

		@Override
		public boolean canExecute() {
			return true;
		}

		@Override
		public void execute() {
			int worked = 0;
			if (updateEcore) {
				GenerateEcore.transform(component, SubMonitor.convert(monitor, 1));
				// avoid getting a proxy
				component = (MComponent) component.eResource().getContents().get(0);

				if (shouldResetTableEditor(component)) {
					GenerateTableEditor.resetEditor(component, SubMonitor.convert(monitor, 1));
				}

				GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1));
				worked = 2;
			}

			if (uri.isPlatformResource()) {
				GenerateInstanceResources.generate(component, SubMonitor.convert(monitor, worked == 0 ? 3 : 1));
			}
		}

		protected boolean shouldResetTableEditor(MComponent component) {
			boolean shouldAvoid = component.isSetAvoidRegenerationOfEditorConfiguration()
					|| component.getAvoidRegenerationOfEditorConfiguration();

			return !component.getNamedEditor().isEmpty() && !component.getUseLegacyEditorconfig() && !shouldAvoid;
		}

		@Override
		public void redo() {
		}

		@Override
		public void undo() {
		}
	}

	private MComponent initModel(MComponent component, URI uri, boolean updateEcore, final IProgressMonitor monitor) {
		Object owner = new Object();
		McoreDiagramEditingDoamin domain = McoreEditingDomainFactory.getInstance()
				.createEditingDomain(PathUtils.buildDomainID(uri), owner);

		domain.publish(McoreTransactionEventType.STOP_RS_LISTENING, owner);
		InitModelCommand ecore = new InitModelCommand(component, uri, updateEcore, monitor);
		if (domain != null) {
			domain.getCommandStack().execute(ecore);
		}
		domain.publish(McoreTransactionEventType.START_RS_LISTENING, owner);
		domain.publish(McoreTransactionEventType.SAVE, owner);

		McoreEditingDomainFactory.getInstance().dispose(domain, owner);
		return component;
	}

	private URI getEditorConfigURI(EPackage ePackage) {
		URI ePackageURI = URI.createURI(ePackage.getNsURI());
		URI normalizedURI = EcoreResourceResolver.computeWorkspaceResourceMap().get(ePackageURI);
		if (normalizedURI == null) {
			normalizedURI = EditorConfigPlugin.INSTANCE.getEditorConfig(ePackage);
		}
		return normalizedURI.trimFileExtension().appendFileExtension("editorconfig");
	}

	private IPath getResourcePath(MResource source, EPackage ePackage) {
		String path = "/" + source.getCalculatedName() + "." + ePackage.getName().toLowerCase();
		EObject current = source;
		while (current.eContainer() != null
				&& !current.eContainer().eClass().equals(McorePackage.Literals.MCOMPONENT)) {
			current = current.eContainer();
			if (current instanceof MResourceFolder) {
				path = "/" + ((MResourceFolder) current).getEName() + path;
			}
		}

		return new Path(path);
	}
}
