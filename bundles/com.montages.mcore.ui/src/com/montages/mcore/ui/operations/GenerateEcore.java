package com.montages.mcore.ui.operations;

import static com.montages.mcore.util.ResourceService.createResourceSet;
import static com.montages.mcore.util.ResourceService.getMcoreOptions;
import static com.montages.mcore.util.ResourceService.getSaveOptions;
import static com.montages.mcore.util.ResourceService.normalize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.transaction.util.TransactionUtil;

import com.montages.common.McoreCommandUtil;
import com.montages.mcore.MComponent;
import com.montages.mcore.mcore2ecore.Mcore2Ecore;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.McoreUtil;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

/**
 * <p>
 * Generate Ecore Resources from Mcore Resources.
 * </p>
 * 
 * <p>
 * Generate an Ecore representation of the input MComponent, the resulting
 * representation is a set of EPackage that are saved in Resource whose URIs are
 * determined by the MComponent's URI. The save operation uses the option
 * Resource.OPTION_SAVE_ONLY_IF_CHANGED to avoid file modification if no changes
 * are made.
 * </p>
 * 
 *
 */
public class GenerateEcore {

	private static Mcore2Ecore mcore2ecore = new Mcore2Ecore();

	/**
	 * Generate an Ecore representation of the input MComponent, the resulting
	 * representation is a set of EPackage that are saved in Resource whose URIs are
	 * determined by the MComponent's URI. The save operation uses the option
	 * Resource.OPTION_SAVE_ONLY_IF_CHANGED to avoid file modification if no changes
	 * are made.
	 * 
	 */
	public static void transform(MComponent component, IProgressMonitor monitor) {
		transform(Dependencies.compute(component).sort(), monitor);
	}

	/**
	 * Generate an Ecore representation of the input list of MComponents, the
	 * resulting representation is a set of EPackage that are saved in Resource
	 * whose URIs are determined by the MComponents' URIs. The save operation uses
	 * the option Resource.OPTION_SAVE_ONLY_IF_CHANGED to avoid file modification if
	 * no changes are made.
	 * 
	 */
	public static void transform(List<MComponent> components, IProgressMonitor monitor) {
		for (MComponent component : components) {
			transform(component, false, monitor);
		}
	}

	/**
	 * Generate an Ecore representation of the input list of MComponents, the
	 * resulting representation is a set of EPackage that are saved in Resource
	 * whose URIs are determined by the MComponents' URIs. The save operation uses
	 * the option Resource.OPTION_SAVE_ONLY_IF_CHANGED to avoid file modification if
	 * no changes are made. If the parameter forceGeneration is true, then the
	 * option Resource.OPTION_SAVE_ONLY_IF_CHANGED is not used.
	 * 
	 * @param forceGeneration
	 */
	public static void transform(MComponent component, final boolean forceGeneration, IProgressMonitor monitor) {
		if (component == null) {
			return;
		}

		if (component.eResource() == null) {
			throw new IllegalArgumentException("MComponent must be contained in a Resource.");
		}

		ResourceSet componentRS = component.eResource().getResourceSet();
		final ResourceSet resourceSet = componentRS == null ? createResourceSet() : componentRS;

		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		final URI normalized = normalize(resourceSet, component.eResource().getURI());
		if (normalized.isPlatformPlugin()) {
			return;
		}

		final Object owner = new Object();
		String id = PathUtils.buildDomainID(normalized);
		final McoreEditingDomainFactory domainFactory = McoreEditingDomainFactory.getInstance();
		final McoreDiagramEditingDoamin domain = domainFactory.exists(id) ? domainFactory.createEditingDomain(id, owner)
				: (McoreDiagramEditingDoamin) TransactionUtil.getEditingDomain(component);
		final MComponent realComponent = McoreUtil.findComponentInTransactionResources(normalized, component, domain);

		final Map<String, Object> options = getMcoreOptions(domain != null ? domain.getResourceSet() : resourceSet);
		McoreUIPlugin.waitAsyncRun(new Runnable() {

			@Override
			public void run() {
				if (normalized.isPlatformResource()) {
					ResourceSet rs = domain == null ? resourceSet : domain.getResourceSet();

					for (Resource r : rs.getResources()) {
						URI uri = r.getURI();
						if (uri != null
								&& ("genmodel".equals(uri.fileExtension()) || uri.fileExtension().equals("genmodel"))) {
							r.unload();
						}
					}

					try {
						GenerateEcoreCommand cmd = new GenerateEcoreCommand(realComponent, rs);
						McoreCommandUtil.executeUnsafeCommand(cmd, domain);
						if (domain != null) {
							domain.publish(McoreTransactionEventType.SAVE, owner);
						} else {
							realComponent.eResource().save(options);
						}
						saveEcores(cmd.getResult(), forceGeneration);
					} catch (IOException e) {
						McoreUIPlugin.log(e);
					} finally {
						domainFactory.dispose(domain, owner);
					}
				}
			}
		});
	}

	/**
	 * Saves Ecore resources only if content has changed or if the parameter force
	 * is true.
	 */
	private static void saveEcores(List<Resource> resources, boolean force) {
		if (resources == null) {
			return;
		}

		Map<String, Object> options = getSaveOptions();
		if (!force) {
			options.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_FILE_BUFFER);
		}

		for (Resource resource : resources) {
			if (!resource.getContents().isEmpty()) {
				URI uri = normalize(resource.getResourceSet(), resource.getURI());
				if (uri.isPlatformResource()) {
					try {
						resource.save(options);
					} catch (IOException e) {
						McoreUIPlugin.log(e);
					}
				}
			}
		}
	}

	private static class GenerateEcoreCommand extends AbstractCommand {

		private final MComponent component;
		private final ResourceSet resourceSet;

		private final List<Resource> ecores = new ArrayList<>();

		public GenerateEcoreCommand(MComponent component, ResourceSet resourceSet) {
			this.component = component;
			this.resourceSet = resourceSet;
		}

		public boolean canExecute() {
			return true;
		}

		@Override
		public void execute() {
			ecores.clear();

			try {
				ecores.addAll(mcore2ecore.transform(resourceSet, component));
			} catch (CoreException e) {
				McoreUIPlugin.log(e);
				return;
			}
		}

		public List<Resource> getResult() {
			return ecores;
		}

		public void undo() {
		}

		@Override
		public void redo() {
		}
	}
}
