package com.montages.mcore.ui.operations;

import static com.montages.mcore.ui.dialogs.Messages.overwriteDialog_Message;
import static com.montages.mcore.ui.dialogs.Messages.overwriteDialog_Title;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.IWorkspaceDescription;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.ui.dialogs.DiagnosticDialog;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.PlatformUI;
import org.xocl.editorconfig.ui.codegen.actions.Messages;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.MComponentResourceIdMap;
/**
 * Enumerates all operations available on an mcore model. 
 * 
 */
public enum McoreOperations {

	GenerateEcoreOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			boolean force = getOption(Options.FORCE_ECORE_SAVE, options);
			monitor = getMonitor(monitor);
			monitor.beginTask("Generate Ecore", components.size());
			for (MComponent c: components) {
				GenerateEcore.transform(c, force, monitor);
			}
			return ValidateEcore.execute(component, components, options, monitor);
		}
	},
	ValidateEcore {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			monitor = getMonitor(monitor);
			monitor.beginTask("Validate Ecore", components.size());

			final Diagnostic diagnostic = ValidateComponent.validate(components);

			if (diagnostic.getSeverity() == Diagnostic.ERROR) {
				if (PlatformUI.isWorkbenchRunning()) {
					PlatformUI.getWorkbench().getDisplay().asyncExec(new Runnable() {

						public void run() {
							DiagnosticDialog.openProblem(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(), Messages.GenerateCodeAction_GenerationError,
									Messages.GenerateCodeAction_GenerationErrorDesc, diagnostic);
						}
					});
					return Status.CANCEL_STATUS;
				}
			}

			monitor.done();
			return Status.OK_STATUS;
		}
	},
	GenerateEditorConfigOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			monitor = getMonitor(monitor);
			monitor.beginTask("Generate Editor Config", 3);

			IStatus status = GenerateEcoreOperation.execute(component, components, options, SubMonitor.convert(monitor, 1));
			//Trigger resetEditor, when avoidRegen  is null or false
			if(component != null && (!component.isSetAvoidRegenerationOfEditorConfiguration() || component.getAvoidRegenerationOfEditorConfiguration() != true))
				if(component.getMainNamedEditor() != null)
				   if(component.getMainNamedEditor().getEditor() != null)				  
					   GenerateTableEditor.resetEditor(component, SubMonitor.convert(monitor, 1));
			if (status == Status.OK_STATUS) {
				GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1), null);
			}

			monitor.done();
			return status;
		}
	},
	GenerateInstancesOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			monitor = getMonitor(monitor);
			monitor.beginTask("Generate Instances", 3);
			GenerateInstanceResources.generate(component, SubMonitor.convert(monitor, 1));

			return Status.OK_STATUS;
		}
	},
	GenerateComponentOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			IStatus status = GenerateEditorConfigOperation.execute(component, components, options, monitor);

			if (status == Status.OK_STATUS) {
				status = GenerateInstancesOperation.execute(component, components, options, monitor);

				if (status == Status.OK_STATUS) {
					if (getOption(Options.FORCE_CODE_GENERATION, options)) {
						try {
							GenerateComponent.generate(component, SubMonitor.convert(monitor, 1));
						} catch (final IllegalArgumentException e) {
							showAsyncMessage("Error", e.getMessage());
							return Status.CANCEL_STATUS;
						}
					}
				}
			}

			return status;
		}
	},
	RunComponentOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			IStatus status = GenerateComponentOperation.execute(component, components, options, monitor);

			if (status == Status.OK_STATUS) {
				MComponentResourceIdMap resourceName2Id = MComponentResourceIdMap.mapResourceName2Id(component);
				RunComponent.run(component, resourceName2Id, SubMonitor.convert(monitor, 1));
			}

			monitor.done();
			McoreUIPlugin.openEditor(component);	
			return status;
		}
	},
	CleanOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			monitor.beginTask("Clean Project", 1);
			CleanComponent.clean(component, SubMonitor.convert(monitor, 1));
			monitor.done();
			return Status.OK_STATUS;
		}
	},
	DeleteOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor) {
			monitor.beginTask("Delete Component", components.size() * 2);
			for (MComponent current : components) {
				CleanComponent.clean(current, SubMonitor.convert(monitor, 1));
				new DeleteComponent().delete(current, SubMonitor.convert(monitor, 1));
			}
			monitor.done();
			return Status.OK_STATUS;
		}
	},
	BuildUpdateSiteOperation {

		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, final IProgressMonitor monitor) {
			if (components.size() != 1) return Status.OK_STATUS;
			monitor.beginTask("Build component", components.size());
				try {
					for (MComponent c: components) {
						final UpdateSiteBuilder generateUpdateSite = new UpdateSiteBuilder(c);
						final IProject updateProject = generateUpdateSite.generateUpdateProject(monitor);
						generateUpdateSite.build(updateProject);
						Display.getDefault().asyncExec(new Runnable() {

							@Override
							public void run() {
								try {
									generateUpdateSite.openInstallWizard(monitor);
								} catch (CoreException e) {
									showAsyncMessage("Error", e.getMessage());
								}
							}
						});
					}
				} catch (final CoreException e) {
					showAsyncMessage("Error", e.getMessage());
					return Status.CANCEL_STATUS;
				}
			return Status.OK_STATUS;
		}

	},
	CheckOutOperation {
		@Override
		public IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options,
				IProgressMonitor monitor) {
			if (monitor == null) {
				monitor = new NullProgressMonitor();
			}
			FeatureImporter featureImporter = null;
			try {
				featureImporter = new FeatureImporter(component);
			} catch (Exception e) {
				showAsyncMessage("Import error", e.getMessage());
				return Status.CANCEL_STATUS;
			}

			if (featureImporter.componentExistsInWorkspace()) {
				final boolean[] overwrite = new boolean[] {false};
				final boolean[] threadEnds = new boolean[] {false};
				Display.getDefault().asyncExec(new Runnable() {
					
					@Override
					public void run() {
						try {
							overwrite[0] = McoreUIPlugin.showConfirmDialof(overwriteDialog_Title, overwriteDialog_Message);
						} catch (Throwable t) {
							overwrite[0] = false;
						} finally {
							threadEnds[0] = true;
						}
					}
				});

				//TODO: thread sleep should be rewritten with wait methods
				while(!threadEnds[0]) {
					sleep();
				}
				
				if (!overwrite[0]) {
					return Status.CANCEL_STATUS;
				}

				CleanComponent.clean(component, SubMonitor.convert(monitor, 1));
				DeleteComponent deleteComponent = new DeleteComponent();
				deleteComponent.delete(component, SubMonitor.convert(monitor, 1));
				while(!deleteComponent.isDone()) {
					sleep();
				}
			}

			featureImporter.doImport(monitor);
			return Status.OK_STATUS;
		}

		void sleep() {
			try {
				Thread.sleep(200);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	};

	public enum Options {
		FORCE_ECORE_SAVE, FORCE_CODE_GENERATION;
	}

	/**
	 * Executes the current operation on the given mcore component. 
	 * 
	 * @param component
	 * @param options
	 * @param monitor
	 * @return execution status
	 */
	public IStatus execute(MComponent component, Map<Options, Boolean> options, IProgressMonitor monitor) {
		// https://mtg.bugz.cloudforge.com/bugz/show_bug.cgi?id=764
		// autobuilding should turn off while mcore execute operation
		boolean oldAutoBuilding = getWorkspace().isAutoBuilding();
		boolean AUTO_BUILDING_OFF_VALUE = false;

		IStatus result = null;
		try {

			if (oldAutoBuilding) {
				setAutoBuilding(AUTO_BUILDING_OFF_VALUE);
			}

			result = execute(component, Dependencies.compute(component).sort(), options == null ? Collections.<Options, Boolean> emptyMap() : options, monitor);
		} finally {
			if (oldAutoBuilding) {
				setAutoBuilding(oldAutoBuilding);
			}
		}
		return result;
	}

	private void setAutoBuilding(boolean autoBuilding) {
		IWorkspace workspace = getWorkspace();
		IWorkspaceDescription description = workspace.getDescription();
		try {
			description.setAutoBuilding(autoBuilding);
			workspace.setDescription(description);
		} catch (CoreException e) {
			McoreUIPlugin.log(e);
		}
	}

	private IWorkspace getWorkspace() {
		return ResourcesPlugin.getWorkspace();
	}

	public abstract IStatus execute(MComponent component, List<MComponent> components, Map<Options, Boolean> options, IProgressMonitor monitor);

	private static IProgressMonitor getMonitor(IProgressMonitor monitor) {
		if (monitor == null) {
			return new NullProgressMonitor();
		}
		return monitor;
	}

	private static boolean getOption(Options option, Map<Options, Boolean> options) {
		if (option == null || options == null)
			return false;

		Boolean b = options.get(option);
		return b == null ? false : b;
	}

	static void showAsyncMessage(final String name, final String message) {
		Display.getDefault().syncExec(new Runnable() {

			@Override
			public void run() {
				MessageDialog.openError(Display.getDefault().getActiveShell(), name, message);
			}
		});
	}

}
