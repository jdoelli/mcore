package com.montages.mcore.ui.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.xocl.ecore2editorconfig.DefaultGenerator;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.TableConfig;

import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.codegen.ui.conversions.Importer;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.RenameService;
import com.montages.mcore.util.ResourceService;
import com.montages.tableeditor2editorconfig.TabelEditor2EditorConfig;

public class GenerateEditorConfig {

	public static void generate(MComponent component, IProgressMonitor monitor) {
		generate(component, monitor, null);
	}

	public static void generate(MComponent component, IProgressMonitor monitor, Importer importer) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		if (component.eResource() != null && component.eResource().getURI().isPlatformPlugin()) {
			return;
		}

		ResourceSet resourceSet = new ResourceSetImpl();
		addURIMap(resourceSet, component.eResource().getResourceSet().getURIConverter().getURIMap());

		DefaultGenerator generator = new DefaultGenerator(resourceSet);
		Dependencies dependencies = Dependencies.compute(component);
		List<EPackage> ePackages = getUpperPackages(dependencies.sort());
		generator.initialize(ePackages);

		List<EditorConfig> transformedConfigs = null;
		if (!isLegacy(component)) {
			Resource eResource = null;
			if (component.getMainEditor() != null) {
				eResource = component.getMainEditor().eResource();
			} else {
				URI mtableEditorURI = createMTableEditorURI(component, resourceSet);
				eResource = resourceSet.createResource(mtableEditorURI);
			}
			TabelEditor2EditorConfig transformation = new TabelEditor2EditorConfig();
			transformedConfigs = transformation.transform(eResource);
		}

		for (MPackage p: component.getOwnedPackage()) {
			// URI must be calculated relative mcomponent
			URI editConfigURI = createEditorConfigURI(p, component, resourceSet);

			EPackage ePackage = p.getInternalEPackage();
			if (ePackage == null) {
				continue;
			}

			EditorConfig config = null;
			if (importer != null && !importer.getMPackageUUIDToEditorConfigsMap().isEmpty()) {
				config = importer.getMPackageUUIDToEditorConfigsMap().get(EcoreUtil.getURI(p).fragment());
				if (config != null) {
					config = EcoreUtil.copy(config);
					Resource r = resourceSet.createResource(editConfigURI);
					r.getContents().add(config);
				}
			}
			if (transformedConfigs != null && config == null) {
				for (EditorConfig editorConfig: transformedConfigs) {
					String mPackageNsURI = ePackage.getNsURI();
					if (mPackageNsURI != null && mPackageNsURI.equals(getPackageNsURIFromTableConfig(editorConfig))) {
						config = EcoreUtil.copy(editorConfig);
						Resource r = resourceSet.createResource(editConfigURI);
						r.getContents().add(config);
						continue;
					}
				}
			}
			if (!avoidRegeneration(component) && isLegacy(component)) {
				config = generator.generate(ePackage);
				config.eResource().setURI(editConfigURI);
			}
			if (config != null) {
				PluginExtensions.addEditorConfiguration(component, ePackage, config);
				save(config, null);
			}
		}
	}

	protected static String getPackageNsURIFromTableConfig(EditorConfig editorConfig) {
		TableConfig headerTableConfig = editorConfig.getHeaderTableConfig();
		if (headerTableConfig == null) {
			if (!editorConfig.getTableConfig().isEmpty()) { 
				headerTableConfig = editorConfig.getTableConfig().get(0);
			}
		}
		if (headerTableConfig == null) {
			throw new RuntimeException("EditorConfig:" + editorConfig + " has no one tableConfig."
					+ "Please check Your MTableEditor.");
		}
		EPackage intendedPackage = headerTableConfig.getIntendedPackage();
		if (intendedPackage == null) {
			throw new RuntimeException("Intended package in header tableConfig with label:" 
					+ headerTableConfig.getLabel()
					+ " and name:" + headerTableConfig.getName() 
					+ "  is null!");
		}
		return intendedPackage.getNsURI();
	}

	protected static boolean avoidRegeneration(MComponent component) {
		return component.isSetAvoidRegenerationOfEditorConfiguration()  && component.getAvoidRegenerationOfEditorConfiguration() != null
				&& component.getAvoidRegenerationOfEditorConfiguration();
	}

	protected static boolean isLegacy(MComponent component) {
		return component.getUseLegacyEditorconfig() != null && component.getUseLegacyEditorconfig();
	}

	protected static URI createMTableEditorURI(MComponent component, ResourceSet resourceSet) {
		URI mcoreURI = component.eResource().getURI();
		mcoreURI = resourceSet.getURIConverter().normalize(mcoreURI);
		return mcoreURI.trimFileExtension().appendFileExtension("mtableeditor");
	}

	protected static URI createEditorConfigURI(MPackage mPackage, MComponent component, ResourceSet resourceSet) {
		URI mcoreURI = component.eResource().getURI();
		mcoreURI = resourceSet.getURIConverter().normalize(mcoreURI);
		URI editConfigURI = mcoreURI.trimSegments(1).appendSegment(mPackage.getName().replace(" ", "").toLowerCase()).appendFileExtension("editorconfig");
		return editConfigURI;
	}

	protected static void addURIMap(ResourceSet resourceSet, Map<URI, URI> uriMap) {
		resourceSet.getURIConverter().getURIMap().putAll(uriMap);
	}

	private static List<EPackage> getUpperPackages(List<MComponent> components) {
		List<EPackage> ePackages = new ArrayList<EPackage>();
		for (MComponent component: components) {
			for (MPackage mPackage: component.getOwnedPackage()) {
				if (mPackage.getInternalEPackage() != null) {
					ePackages.add(mPackage.getInternalEPackage());
				}
			}
		}
		return ePackages;
	}

	private static void save(EditorConfig config, RenameService renameService) {
		try {
			config.eResource().save(
					new ResourceService().getEditorConfigSaveOptions(config.eResource().getResourceSet(), renameService));
		} catch (IOException e) {
			McoreUIPlugin.log(e);
		}
	}

}
