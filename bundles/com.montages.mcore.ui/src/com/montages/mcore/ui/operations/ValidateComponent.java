package com.montages.mcore.ui.operations;

import java.util.List;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.util.Diagnostician;

import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;

public class ValidateComponent {

	public static Diagnostic validate(MComponent component) {
		final BasicDiagnostic diagnostic = new BasicDiagnostic("com.montages.mcore.ui.operations.ValidateComponent",
				Diagnostic.INFO, "", new Object[] {});

		if (component.getEName().equals("coreTypes"))
			return diagnostic;

		for (MPackage mPackage : component.getOwnedPackage()) {
			if (mPackage.getInternalEPackage() != null) {
				Diagnostic diag = Diagnostician.INSTANCE.validate(mPackage.getInternalEPackage());
				diagnostic.add(diag);
			}
		}

		return diagnostic;
	}

	public static Diagnostic validate(List<MComponent> components) {
		final BasicDiagnostic diagnostic = new BasicDiagnostic("com.montages.mcore.ui.operations.ValidateComponent",
				Diagnostic.INFO, "", new Object[] {});

		for (MComponent component : components) {
			diagnostic.merge(validate(component));
		}

		return diagnostic;
	}

}
