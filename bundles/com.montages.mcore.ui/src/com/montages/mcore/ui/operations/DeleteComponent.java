package com.montages.mcore.ui.operations;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.McoreUIPlugin;

/**
 * {@link DeleteComponent}
 * 
 * 
 * This operation deletes a {@link MComponent} and the {@link IProject} it 
 * belongs to. The {@link McoreClean} should be called before to delete 
 * generated code and projects associated to the {@link MComponent}. 
 *
 */
public class DeleteComponent {

	private boolean done = false;

	public void delete(final MComponent component, IProgressMonitor m) {
		final IProgressMonitor monitor;
		if (m == null) {
			monitor = new NullProgressMonitor();
		} else {
			monitor = m;
		}

		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				try {
				final IWorkbenchWindow workbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
				final IWorkbenchPage page = workbenchWindow.getActivePage();
				final IEditorInput editorInput = new URIEditorInput(component.eResource().getURI());

				int i = 0;
				IEditorReference foundReference = null;
				while (i < page.getEditorReferences().length && foundReference == null) {
					IEditorReference editorReference = page.getEditorReferences()[i];
					IEditorInput currentInput = null;
					try {
						currentInput = editorReference.getEditorInput();
						if (editorInput.equals(currentInput)) {
							foundReference = editorReference;
						}
					} catch (PartInitException e) {}
					i++;
				}

				if (foundReference != null) {
					IEditorPart editorPart = foundReference.getEditor(true);
					page.closeEditor(editorPart, false);
				}

				final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
				final String bundleName = component.getDerivedBundleName();
				final IProject project = root.getProject(bundleName);

				try {
					project.delete(true, true, SubMonitor.convert(monitor, 1));
				} catch (CoreException e) {
					McoreUIPlugin.log(e);
				}
				} finally {
					done = true;
				}
			}
		});
	}

	public boolean isDone() {
		return done;
	}
}
