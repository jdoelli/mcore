package com.montages.mcore.ui.operations;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;

import com.montages.mcore.MComponent;
import com.montages.mcore.McorePackage;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.ResourceService;

public class DeleteInternalEcore {

	public static void execute(List<MComponent> components, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		Dependencies dependencies = Dependencies.compute(components);
		List<MComponent> sorted = dependencies.sort();

		monitor.beginTask("Deleting references to ecore", sorted.size() * 2);

		for (MComponent current: sorted) {
			monitor.subTask("Deleting " + current.getCalculatedName());
			deleteOnComponent(current);
			monitor.worked(1);
		}

		for (MComponent current: sorted) {
			monitor.subTask("Saving " + current.getCalculatedName());
			try {
				current.eResource().save(ResourceService.getSaveOptions());
			} catch (IOException e) {
				e.printStackTrace();
			}
			monitor.worked(1);
		}

		monitor.done();
	}

	public static void execute(MComponent component, IProgressMonitor monitor) {
		execute(Collections.singletonList(component), monitor);
	}

	private static void deleteOnComponent(MComponent component) {
		for (TreeIterator<EObject> it = component.eAllContents(); it.hasNext();) {
			EObject current = it.next();
			
			if (current.eClass().equals(McorePackage.Literals.MPACKAGE)) {
				current.eSet(McorePackage.Literals.MPACKAGE__INTERNAL_EPACKAGE, null);
			} else if (current.eClass().equals(McorePackage.Literals.MCLASSIFIER)) {
				current.eSet(McorePackage.Literals.MCLASSIFIER__INTERNAL_ECLASSIFIER, null);
			} else if (current.eClass().equals(McorePackage.Literals.MPROPERTY)) {
				current.eSet(McorePackage.Literals.MPROPERTY__INTERNAL_ESTRUCTURAL_FEATURE, null);
			} else if (current.eClass().equals(McorePackage.Literals.MOPERATION_SIGNATURE)) {
				current.eSet(McorePackage.Literals.MOPERATION_SIGNATURE__INTERNAL_EOPERATION, null);
			}  else if (current.eClass().equals(McorePackage.Literals.MPARAMETER)) {
				current.eSet(McorePackage.Literals.MPARAMETER__INTERNAL_EPARAMETER, null);
			}
		}
	}

}
