package com.montages.mcore.ui.operations;

import static com.montages.mcore.util.ResourceService.normalize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.command.AbstractCommand;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.swt.widgets.Display;

import com.montages.common.McoreCommandUtil;
import com.montages.common.McoreUtil;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.ecore2mcore.EObject2MObject.MElementRestoreEntry;
import com.montages.mcore.ecore2mcore.Resource2MResource;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.objects.ObjectsFactory;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.emf.EPackageRegisterer;
import com.montages.mcore.ui.workspace.FileFunctions;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.MComponentResourceIdMap;
import com.montages.mcore.util.ResourceService;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.McoreTransactionEventType;
import com.montages.transactions.PathUtils;

public class GenerateMResources {

	private static final List<String> WORKSPACE_FILTERS = Arrays.asList(new String[] {});

	public static void generate(MComponent component, MComponentResourceIdMap resourceIdMap,
			List<FolderRestoreEntry> folderMap, List<MElementRestoreEntry> propertyInstanceMap,
			IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		monitor.beginTask("Synchronizing instances", countMResources(component.getResourceFolder(), 0));

		ResourceSet resourceSet = ResourceService.createResourceSet();

		registerEPackages(component);

		final URI normalized = normalize(resourceSet, component.eResource().getURI());
		String id = PathUtils.buildDomainID(normalized);
		final Object owner = new Object();
		McoreEditingDomainFactory domainFactory = McoreEditingDomainFactory.getInstance();
		final McoreDiagramEditingDoamin domain = domainFactory.exists(id) ? domainFactory.createEditingDomain(id, owner)
				: null;
		try {
			final MComponent realComponent = com.montages.mcore.util.McoreUtil
					.findComponentInTransactionResources(component);
			final Transformer tr = new Transformer(realComponent, resourceIdMap, folderMap, propertyInstanceMap,
					domain != null ? domain.getResourceSet() : resourceSet);
			final TransformComponentCommand cmd = new TransformComponentCommand(tr, monitor);

			McoreUIPlugin.waitAsyncRun(new Runnable() {

				@Override
				public void run() {
					McoreCommandUtil.executeUnsafeCommand(cmd, domain);

					if (normalized.isPlatformResource()) {
						if (domain != null) {
							domain.publish(McoreTransactionEventType.SAVE, owner);
						} else {
							save(realComponent);
						}
					}
				}
			});
		} finally {
			if (domain != null) {
				domainFactory.dispose(domain, owner);
			}
		}
	}

	public static void generate(URI mResourceURI, Resource resource, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		monitor.beginTask("Synchronizing instances", 1);

		McoreEditingDomainFactory factory = McoreEditingDomainFactory.getInstance();
		String id = PathUtils.buildDomainID(mResourceURI.trimFragment());
		final Object owner = new Object();
		final McoreDiagramEditingDoamin domain = factory.exists(id) ? factory.createEditingDomain(id, owner) : null;

		ResourceSet resourceSet = domain != null ? domain.getResourceSet() : ResourceService.createResourceSet();
		EObject object = resourceSet.getEObject(mResourceURI, false);

		try {
			if (object instanceof MResource) {
				MResource target = (MResource) object;
				EObject root = EcoreUtil.getRootContainer(target);

				if (root instanceof MComponent) {
					MComponent component = (MComponent) root;

					final List<FolderRestoreEntry> folderMap = RunComponent.createFolderMap(component);
					final List<MElementRestoreEntry> propertyInstanceMap = RunComponent
							.createPropertyInstanceMap((XMLResource) component.eResource());

					Transformer t = new Transformer((MComponent) root, null, folderMap, propertyInstanceMap,
							resourceSet);

					if (domain != null) {
						TransformMResourceCommand cmd = new TransformMResourceCommand(t, target, resource.getURI(),
								monitor);
						McoreCommandUtil.executeUnsafeCommand(cmd, domain);

						Display.getDefault().asyncExec(new Runnable() {
							@Override
							public void run() {
								// Make sure we remove all instance resources before saving the content of the
								// mcore editor

								List<String> reserved = Arrays.asList( //
										"mcore", //
										"mtableeditor", //
										"editorconfig", //
										"ecore");

								List<URI> unloaded = domain.getResourceSet().getResources().stream() //
										.filter(res -> !reserved.contains(res.getURI().fileExtension())
												|| res.getURI().hasFragment()) //
										.peek(res -> res.unload()) //
										.map(res -> res.getURI()) //
										.collect(Collectors.toList());

								resourceSet.getResources().removeIf(res -> unloaded.contains(res.getURI()));

								domain.publish(McoreTransactionEventType.SAVE, this);
							}
						});
					}
				}
			}
		} finally {
			if (domain != null) {
				factory.dispose(domain, owner);
			}
		}
	}

	private static void registerEPackages(MComponent component) {
		List<MComponent> dependencies = Dependencies.compute(component).sort();
		for (MComponent c : dependencies) {
			EPackageRegisterer.register(c, component.eResource().getResourceSet());
		}
	}

	private static void save(MComponent component) {
		try {
			component.eResource().save(ResourceService.getSaveOptions());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static int countMResources(List<MResourceFolder> folders, int nb) {
		for (MResourceFolder f : folders) {
			nb += f.getResource().size();
			nb += countMResources(f.getFolder(), nb);
		}
		return nb;
	}

	private static class Transformer {

		private final MComponentResourceIdMap myResourceIdMap;

		private final MComponent component;

		private final Resource2MResource t;

		private final ResourceSet resourceSet;

		private final List<FolderRestoreEntry> folderMap;

		Transformer(MComponent component, MComponentResourceIdMap resourceIdMap, List<FolderRestoreEntry> folderMap,
				List<MElementRestoreEntry> propertyInstanceMap, ResourceSet resourceSet) {
			this.component = component;
			this.t = new Resource2MResource(Dependencies.compute(component).sort(), propertyInstanceMap);
			this.resourceSet = resourceSet;
			myResourceIdMap = resourceIdMap;
			this.folderMap = folderMap;
		}

		public void finish() {
			t.finish();
		}

		public void transform(List<IFolder> folders, MComponent container, IProgressMonitor monitor) {
			List<MResourceFolder> transformedFolders = new ArrayList<MResourceFolder>();
			for (IFolder folder : folders) {
				transformedFolders.add(transform(folder, null, "", monitor));
			}
			McoreUtil.unsafeListsMerge(container.getResourceFolder(), transformedFolders);
		}

		public MResourceFolder transform(IFolder folder, MResourceFolder container, String path,
				IProgressMonitor monitor) {
			final List<IFile> modelFiles = FileFunctions.collectFiles(folder, getExtensions(component));
			final List<MResourceFolder> mFolders = container == null ? component.getResourceFolder()
					: container.getFolder();
			MResourceFolder mFolder = findMFolder(folder.getName(), mFolders);

			if (mFolder == null) {
				mFolder = ObjectsFactory.eINSTANCE.createMResourceFolder();
				mFolder.setName(folder.getName());
			}

			String folderPath = FolderRestoreEntry.computePath(mFolder, path);
			if (folderPath != null) {
				String uuid = findUUID(folderPath);
				if (uuid != null) {
					((XMLResource) component.eResource()).setID(mFolder, uuid);
				}
			}

			transformFiles(modelFiles, mFolder, folderPath, monitor);

			List<IFolder> folders = FileFunctions.collectFolders(folder, WORKSPACE_FILTERS);
			List<MResourceFolder> transformedFolders = new ArrayList<MResourceFolder>();
			for (IFolder subFolder : folders) {
				transformedFolders.add(transform(subFolder, mFolder, folderPath, monitor));
			}

			McoreUtil.unsafeListsMerge(mFolder.getFolder(), transformedFolders);
			return mFolder;
		}

		private String findUUID(String path) {
			if (path == null || path.length() == 0) {
				return null;
			}
			for (FolderRestoreEntry entry : folderMap) {
				if (path.equals(entry.getPath())) {
					return entry.getUUID();
				}
			}
			return null;
		}

		private void transformFiles(Iterable<IFile> modelFiles, MResourceFolder mFolder, String path,
				IProgressMonitor monitor) {
			ArrayList<MResource> resources = new ArrayList<MResource>();
			for (IFile file : modelFiles) {
				URI fileURI = URI.createPlatformResourceURI(file.getFullPath().toString(), false);
				resources.add(transformFile(fileURI, mFolder, path, monitor));
			}
			McoreUtil.unsafeListsMerge(mFolder.getResource(), resources);
		}

		private String findOrCreateMResourceID(URI fileURI, MResource mResource, MResourceFolder mFolder) {
			String id = myResourceIdMap != null ? myResourceIdMap.findId(fileURI) : mResource.getId();
			return id == null || id.isEmpty() ? generateMResourceID(fileURI, mResource, mFolder) : id;
		}

		private String generateMResourceID(URI mResourceURI, MResource mResource, MResourceFolder mFolder) {
			return generateMResourceID(mResourceURI, mResource, mFolder, 0);
		}

		private String generateMResourceID(URI mResourceURI, MResource mResource, MResourceFolder mFolder, int offset) {
			String result = mResource.getName().substring(0, 1) + (mFolder.getResource().size() + offset);
			if (myResourceIdMap.findId(result) == null) {
				myResourceIdMap.add(mResourceURI, result);
				return result;
			}
			return generateMResourceID(mResourceURI, mResource, mFolder, ++offset);
		}

		private MResource transformFile(final URI fileURI, MResourceFolder mFolder, String path,
				IProgressMonitor monitor) {
			monitor.subTask("Generate " + fileURI.toString());

			final String fileExt = fileURI.fileExtension();
			final String fileName = fileURI.trimFileExtension().lastSegment();
			final MResource mResource = getMResource(fileName, fileExt, mFolder);

			mResource.setId(findOrCreateMResourceID(fileURI, mResource, mFolder));
			Resource resource = null;

			if (path != null && path.length() != 0) {
				String resourcePath = FolderRestoreEntry.computePath(mResource, path);
				if (resourcePath != null) {
					String uuid = findUUID(resourcePath);
					if (uuid != null) {
						((XMLResource) mResource.eResource()).setID(mResource, uuid);
					}
				}
			}

			try {
				resource = resourceSet.getResource(fileURI, false);
				if (resource == null) {
					resource = ResourceService.createXMLResourceWithUUID(fileURI);
					resourceSet.getResources().add(resource);
				}
				if (resource != null) {
					if (!resource.isLoaded()) {
						resource.load(resourceSet.getLoadOptions());
					}
				}
			} catch (Exception e) {
				if (resource != null && !resource.isLoaded()) {
					try {
						resource.load(resourceSet.getLoadOptions());
					} catch (IOException e1) {
					}
				}
				// does not exist
			}

			if (resource != null && !resource.getContents().isEmpty()) {
				t.transform(resource, mResource, (XMLResource) mResource.eResource());
				resource.unload();
			}

			monitor.worked(1);
			return mResource;
		}

		private MResource getMResource(String name, String ext, MResourceFolder container) {
			MResource mResource = findMResource(name + "." + ext, container);
//			if (mResource != null) {
//				OptimizedEcoreUtil.delete(mResource, true);
//			}

			if (mResource == null) {
				mResource = ObjectsFactory.eINSTANCE.createMResource();
				mResource.setName(name);
				// clear init value
//			mResource.getObject().clear();
			}
			mResource.setRootObjectPackage(findMPackage(ext));

			return mResource;
		}

		private MResource findMResource(String fileName, MResourceFolder mFolder) {
			for (MResource resource : mFolder.getResource()) {
				if (resource.getEName().equalsIgnoreCase(fileName)) {
					return resource;
				}
			}
			return null;
		}

		private MResourceFolder findMFolder(String folderName, List<MResourceFolder> folders) {
			for (MResourceFolder folder : folders) {
				if (folder.getEName().equalsIgnoreCase(folderName)) {
					return folder;
				}
			}
			return null;
		}

		private Collection<String> getExtensions(MComponent component) {
			LinkedHashSet<String> extensions = new LinkedHashSet<String>();
			for (MComponent nextComponent : Dependencies.compute(component).sort()) {
				for (MPackage mPackage : nextComponent.getOwnedPackage()) {
					getExtensions(mPackage, extensions);
				}
			}
			return extensions;
		}

		private <LIST extends Collection<String>> LIST getExtensions(MPackage mPackage, LIST extensions) {
			extensions.add(mPackage.getEName().toLowerCase());
			for (MPackage subMPackage : mPackage.getSubPackage()) {
				getExtensions(subMPackage, extensions);
			}
			return extensions;
		}

		private MPackage findMPackage(String ext) {
			if (ext == null || ext.length() == 0) {
				return null;
			}
			for (MComponent nextComponent : Dependencies.compute(component).sort()) {
				MPackage mPackege = findMPackage(nextComponent.getOwnedPackage(), ext);
				if (mPackege != null) {
					return mPackege;
				}
			}
			return null;
		}

		private MPackage findMPackage(List<MPackage> mPackages, String ext) {
			if (mPackages.isEmpty()) {
				return null;
			}
			for (MPackage pack : mPackages) {
				if (pack.getEName().toLowerCase().equals(ext)) {
					return pack;
				}
				MPackage mPackage = findMPackage(pack.getSubPackage(), ext);
				if (mPackage != null) {
					return mPackage;
				}
			}
			return null;
		}

		public MComponent getComponent() {
			return component;
		}
	}

	public static class FolderRestoreEntry {

		final String myUUID;

		final String myPath;

		public FolderRestoreEntry(String uuid, String path) {
			myPath = path;
			myUUID = uuid;
		}

		public String getPath() {
			return myPath;
		}

		public String getUUID() {
			return myUUID;
		}

		public static String computePath(MResourceFolder folder, String parentPath) {
			return computePath(folder.getEName(), parentPath);
		}

		public static String computePath(MResource res, String parentPath) {
			return computePath(res.getEName(), parentPath);
		}

		private static String computePath(String name, String parentPath) {
			return name == null ? null : parentPath + "/" + name;
		}
	}

	private static class TransformComponentCommand extends AbstractCommand {

		private Transformer myTransformer;
		private IProgressMonitor myMonitor;

		public TransformComponentCommand(Transformer transformer, IProgressMonitor monitor) {
			myTransformer = transformer;
			myMonitor = monitor;
		}

		public boolean canExecute() {
			return true;
		};

		@Override
		public void execute() {
			final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
			final IProject project = root.getProject(myTransformer.getComponent().getDerivedBundleName());
			final List<IFolder> folders = FileFunctions.collectFolders(project);
			myTransformer.transform(folders, myTransformer.getComponent(), myMonitor);
			myTransformer.finish();
		}

		public void undo() {
		};

		@Override
		public void redo() {
		}
	}

	private static class TransformMResourceCommand extends AbstractCommand {

		private Transformer myTransformer;
		private MResource myMResource;
		private IProgressMonitor myMonitor;
		private URI myResourceURI;

		public TransformMResourceCommand(Transformer transformer, MResource mResource, URI resourceURI,
				IProgressMonitor monitor) {
			myTransformer = transformer;
			myMResource = mResource;
			myResourceURI = resourceURI;
			myMonitor = monitor;
		}

		public boolean canExecute() {
			return true;
		};

		@Override
		public void execute() {
			myTransformer.transformFile(myResourceURI, myMResource.getContainingFolder(), null, myMonitor);
			myTransformer.finish();
		}

		public void undo() {
		};

		@Override
		public void redo() {
		}
	}
}
