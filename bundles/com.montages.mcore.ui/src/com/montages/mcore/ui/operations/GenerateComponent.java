package com.montages.mcore.ui.operations;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.codegen.McoreCodeGenerator;
import com.montages.mcore.util.Dependencies;

public class GenerateComponent {

	public static void generate(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		Dependencies dependencies = Dependencies.compute(component);
		if (dependencies.hasCycle()) {
			throw new IllegalArgumentException("MComponent cannot be implemented because it contains dependencies cycles\n"+dependencies.printCycles());
		} else {
			new McoreCodeGenerator().generate(dependencies.getMap(), monitor);
		}
	}

}
