package com.montages.mcore.ui.operations;

import static com.montages.mcore.util.ResourceService.getMcoreOptions;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.emf.transaction.util.TransactionUtil;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MEditor;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.util.ResetEditorCommand;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MColumnConfig;
import com.montages.mtableeditor.MEditorConfig;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MTableConfig;
import com.montages.mtableeditor.MtableeditorFactory;

public class GenerateTableEditor {

	public static void generate(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		if (component.eResource() != null && component.eResource().getURI().isPlatformPlugin()) {
			return;
		}

		for (MPackage mPackage : component.getOwnedPackage()) {
			transfromMPackageToMTE(mPackage);
		}
	}

	public static void resetEditor(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		MEditor editor = component.getMainNamedEditor().getEditor();
		TransactionalEditingDomain domain = TransactionUtil.getEditingDomain(editor);

		if (domain != null) {
			domain.getCommandStack().execute(new ResetEditorCommand(editor));
		}
	}

	public static MEditorConfig generateImport(MComponent component, IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		MEditorConfig mEditorConfig = MtableeditorFactory.eINSTANCE.createMEditorConfig();
		for (MPackage mPackage : component.getOwnedPackage()) {
			MRowFeatureCell mRowFeatureCell = MtableeditorFactory.eINSTANCE.createMRowFeatureCell();
			MClassToCellConfig mClassToCellConfig = MtableeditorFactory.eINSTANCE.createMClassToCellConfig();
			MColumnConfig mColumnConfig = MtableeditorFactory.eINSTANCE.createMColumnConfig();
			MTableConfig mTableConfig = MtableeditorFactory.eINSTANCE.createMTableConfig();

			/*
			 * Setting the basic values for the MTableEditor First it creates the new Table
			 * for the Class then it starts to create the Columns and Cells The Cell will
			 * get the information of mProperty, then it will be added as new Cell and
			 * Column to the Table Then the Table is added to the EditorConfig
			 */

			for (MClassifier mClassifier : mPackage.getClassifier()) {
				mTableConfig.setIntendedClass(mClassifier);
				mTableConfig.setIntendedPackage(mPackage);
				mTableConfig.setName(mClassifier.getEName());
				mTableConfig.setLabel(mClassifier.getName());
				mTableConfig.setDisplayHeader(true);
				mTableConfig.setDisplayDefaultColumn(false);

				for (MProperty mProperty : mClassifier.getProperty()) {
					if (mProperty.getContainment() == false && mProperty.getContainingPropertiesContainer() != null)
						continue;
					mRowFeatureCell.setFeature(mProperty);

					mClassToCellConfig.setClass(mClassifier);
					mClassToCellConfig.setCellConfig(mRowFeatureCell);
					mRowFeatureCell.setClass(mClassifier);

					mColumnConfig.setName(mProperty.getEName());
					mColumnConfig.setLabel(mProperty.getName());
					mColumnConfig.getClassToCellConfig().add(mRowFeatureCell);
					mColumnConfig.setContainingTableConfig(mTableConfig);
					mTableConfig.getColumnConfig().add(mColumnConfig);
				}

				mEditorConfig.getMTableConfig().add(mTableConfig);

			}

			// sets the header table
			if (!mEditorConfig.getMTableConfig().isEmpty()) {
				mEditorConfig.setHeaderTableConfig(mEditorConfig.getMTableConfig().get(0));
			}

			/*
			 * Creating a new mtableeditor file with mEditorConfig
			 */
			Resource resource = createMTableEditorResource(mPackage);//
			resource.getContents().add(mEditorConfig);
			ResourceSet resourceSet = resource.getResourceSet();

			try {
				resource.save(getMcoreOptions(resourceSet, null));
			} catch (Exception exception) {
				McoreUIPlugin.log(exception);
			}
		}
		return mEditorConfig;

	}

	public static Resource createMTableEditorResource(MPackage mPackage) {
		URI fileURI = createMTableEditorURI(mPackage);
		return mPackage.eResource().getResourceSet().createResource(fileURI);
	}

	public static URI createMTableEditorURI(MPackage mPackage) {
		URI mComponentURI = mPackage.eResource().getURI();
		URI fileURI = URI.createURI(mComponentURI.trimSegments(1).toString());
		fileURI = fileURI.appendSegment(createMTableEditorFileName(mPackage));
		return fileURI;
	}

	public static String createMTableEditorFileName(MPackage mPackage) {
		return mPackage.getEName().toLowerCase() + ".mtableeditor";
	}

	public static void transfromMPackageToMTE(MPackage mPackage) {
		MEditorConfig mEditorConfig = MtableeditorFactory.eINSTANCE.createMEditorConfig();

		MRowFeatureCell mRowFeatureCell = MtableeditorFactory.eINSTANCE.createMRowFeatureCell();
		MClassToCellConfig mClassToCellConfig = MtableeditorFactory.eINSTANCE.createMClassToCellConfig();
		MColumnConfig mColumnConfig = MtableeditorFactory.eINSTANCE.createMColumnConfig();
		MTableConfig mTableConfig = MtableeditorFactory.eINSTANCE.createMTableConfig();

		/*
		 * Setting the basic values for the MTableEditor First it creates the new Table
		 * for the Class then it starts to create the Columns and Cells The Cell will
		 * get the information of mProperty, then it will be added as new Cell and
		 * Column to the Table Then the Table is added to the EditorConfig
		 */

		for (MClassifier mClassifier : mPackage.getClassifier()) {
			mTableConfig.setIntendedClass(mClassifier);
			mTableConfig.setIntendedPackage(mPackage);
			mTableConfig.setName(mClassifier.getEName());
			mTableConfig.setLabel(mClassifier.getName());
			mTableConfig.setDisplayHeader(true);
			mTableConfig.setDisplayDefaultColumn(false);

			for (MProperty mProperty : mClassifier.getProperty()) {
				mRowFeatureCell.setFeature(mProperty);

				mClassToCellConfig.setClass(mClassifier);
				mClassToCellConfig.setCellConfig(mRowFeatureCell);
				mRowFeatureCell.setClass(mClassifier);

				mColumnConfig.setName(mProperty.getEName());
				mColumnConfig.setLabel(mProperty.getName());
				mColumnConfig.getClassToCellConfig().add(mRowFeatureCell);
				mColumnConfig.setContainingTableConfig(mTableConfig);
				mTableConfig.getColumnConfig().add(mColumnConfig);
			}

			mEditorConfig.getMTableConfig().add(mTableConfig);

		}

		// sets the header table
		if (!mEditorConfig.getMTableConfig().isEmpty()) {
			mEditorConfig.setHeaderTableConfig(mEditorConfig.getMTableConfig().get(0));
		}

		/*
		 * This if-statement ads the generated mEditorConfig as MNamedEditor Sets this
		 * MNamedEditor as the main one, the useLegacyEditorconfig on false and
		 * avoidRegenerationOfEditorconfiguration on true
		 */
		MComponent component = mPackage.getContainingComponent();
		if (!component.getNamedEditor().isEmpty()) {
			component.getNamedEditor().get(0).setEditor(mEditorConfig);
			if (!component.isSetMainNamedEditor()) {
				component.setUseLegacyEditorconfig(false);
				// SetAvoidRegen back to false, since we trigger resetEditor upon running the
				// component
				component.setAvoidRegenerationOfEditorConfiguration(false);
				component.setMainNamedEditor(component.getNamedEditor().get(0));
			}
		}

		/*
		 * Creating a new mtableeditor file with mEditorConfig
		 */
		Resource resource = createMTableEditorResource(mPackage);//
		resource.getContents().add(mEditorConfig);
		ResourceSet resourceSet = resource.getResourceSet();

		try {
			resource.save(getMcoreOptions(resourceSet, null));
		} catch (Exception exception) {
			McoreUIPlugin.log(exception);
		}
	}
}
