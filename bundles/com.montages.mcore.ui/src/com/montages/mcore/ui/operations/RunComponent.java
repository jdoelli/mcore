package com.montages.mcore.ui.operations;

import static com.montages.mcore.ui.workspace.FileFunctions.getFileFromResource;
import static com.montages.mcore.ui.workspace.FileFunctions.insureIsSave;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.debug.core.DebugEvent;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.IDebugEventSetListener;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.debug.core.Launch;
import org.eclipse.debug.core.model.IProcess;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.swt.widgets.Display;

import com.montages.mcore.MComponent;
import com.montages.mcore.MProperty;
import com.montages.mcore.ecore2mcore.EObject2MObject;
import com.montages.mcore.ecore2mcore.EObject2MObject.MElementRestoreEntry;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.MResource;
import com.montages.mcore.objects.MResourceFolder;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.operations.GenerateMResources.FolderRestoreEntry;
import com.montages.mcore.util.MComponentResourceIdMap;
import com.montages.mcore.util.ResourceService;
import com.montages.transactions.McoreEditingDomainFactory;
import com.montages.transactions.McoreEditingDomainFactory.McoreDiagramEditingDoamin;
import com.montages.transactions.PathUtils;

public class RunComponent {

	public static void run(MComponent component, final MComponentResourceIdMap resourceIdMap,
			IProgressMonitor monitor) {
		if (monitor == null) {
			monitor = new NullProgressMonitor();
		}

		final IProject project = findProject(component);
		if (project == null || !project.isAccessible()) {
			throw new IllegalArgumentException("Cannot find or access project " + component.getDerivedBundleName());
		}

		IFile launchFile = findLaunchFile(project);
		if (launchFile == null) {
			throw new IllegalArgumentException(
					"Cannot find launch configuration file for project " + project.getName());
		}

		final ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
		final ILaunchConfiguration configuration = manager.getLaunchConfiguration(launchFile);
		Launch launch = null;

		EList<EObject> contents = ResourceService.createResourceSet().getResource(EcoreUtil.getURI(component), true)
				.getContents();
		final MComponent reloadedComponent = component.eIsProxy() && !contents.isEmpty() ? (MComponent) contents.get(0)
				: component;
		final List<FolderRestoreEntry> folderMap = createFolderMap(reloadedComponent);
		final List<MElementRestoreEntry> propertyInstanceMap = createPropertyInstanceMap(
				(XMLResource) reloadedComponent.eResource());

		try {

			launch = (Launch) configuration.launch("run", null, true);
		} catch (CoreException e) {
			McoreUIPlugin.log(e);
		}

		if (launch != null) {
			IProcess[] processes = launch.getProcesses();
			if (processes.length > 0) {
				final IProcess process = processes[0];
				if (!process.isTerminated()) {
					DebugPlugin.getDefault().addDebugEventListener(new IDebugEventSetListener() {

						@Override
						public void handleDebugEvents(DebugEvent[] events) {
							for (DebugEvent e : events) {
								if (process.equals(e.getSource())) {
									if (e.getKind() == DebugEvent.TERMINATE) {
										DebugPlugin.getDefault().removeDebugEventListener(this);
										Display.getDefault().asyncExec(new PostProcess(resourceIdMap, project,
												folderMap, reloadedComponent, propertyInstanceMap));
									}
								}
							}
						}
					});
				}
			}
		}
	}

	private static IProject findProject(MComponent component) {
		return ResourcesPlugin.getWorkspace().getRoot().getProject(component.getDerivedBundleName());
	}

	private static IFile findLaunchFile(IProject project) {
		IFile launchFile = null;
		IResource[] members = null;
		try {
			members = project.members();
		} catch (CoreException e) {
			e.printStackTrace();
			return launchFile;
		}

		int i = 0;
		while (launchFile == null && i < members.length) {
			IResource member = members[i];
			if ("launch".equals(member.getFileExtension())) {
				launchFile = (IFile) member;
			}
			i++;
		}

		return launchFile;
	}

	public static List<FolderRestoreEntry> createFolderMap(MComponent component) {
		List<FolderRestoreEntry> result = new ArrayList<GenerateMResources.FolderRestoreEntry>();
		result.addAll(createFolderMap(component.getResourceFolder(), ""));
		return result;
	}

	public static List<FolderRestoreEntry> createFolderMap(List<MResourceFolder> folders, String path) {
		if (folders.isEmpty()) {
			return Collections.emptyList();
		}
		List<FolderRestoreEntry> result = new ArrayList<GenerateMResources.FolderRestoreEntry>();
		for (MResourceFolder folder : folders) {
			String folderPath = FolderRestoreEntry.computePath(folder, path);
			String uuid = folder.eResource().getURIFragment(folder);
			if (folderPath != null || uuid != null) {
				result.add(new FolderRestoreEntry(uuid, folderPath));
				result.addAll(createFolderMap(folder.getFolder(), folderPath));
				result.addAll(createResourceMap(folder.getResource(), folderPath));
			}
		}
		return result;
	}

	private static List<FolderRestoreEntry> createResourceMap(List<MResource> resources, String path) {
		if (resources.isEmpty()) {
			return Collections.emptyList();
		}
		List<FolderRestoreEntry> result = new ArrayList<GenerateMResources.FolderRestoreEntry>();
		for (MResource res : resources) {
			String folderPath = FolderRestoreEntry.computePath(res, path);
			String uuid = res.eResource().getURIFragment(res);
			if (folderPath != null || uuid != null) {
				result.add(new FolderRestoreEntry(uuid, folderPath));
			}
		}
		return result;
	}

	public static List<MElementRestoreEntry> createPropertyInstanceMap(XMLResource mcoreResource) {
		List<MElementRestoreEntry> result = new ArrayList<EObject2MObject.MElementRestoreEntry>();
		for (Iterator<EObject> it = mcoreResource.getAllContents(); it.hasNext();) {
			EObject next = it.next();
			MElementRestoreEntry instance = null;
			if (next instanceof MPropertyInstance) {
				String parentUUID = mcoreResource.getID(next.eContainer());
				String propertyUUID = mcoreResource.getID(next);
				MProperty mProperty = ((MPropertyInstance) next).getProperty();
				if (mProperty == null) {
					continue;
				}
				if (mProperty.eResource() == null) {
					continue;
				}
				String mpropertyUUID = mProperty.eResource().getURIFragment(mProperty);
				instance = new MElementRestoreEntry(parentUUID, propertyUUID, mpropertyUUID);
			} else if (next instanceof MObjectReference) {
				String parentUUID = mcoreResource.getID(next.eContainer());
				String objectRefUUID = mcoreResource.getID(next);
				MObject mObject = ((MObjectReference) next).getReferencedObject();
				if (mObject == null) {
					continue;
				}
				if (mObject.eResource() == null) {
					continue;
				}
				String keyUUID = mObject.eResource().getURIFragment(mObject);
				instance = new MElementRestoreEntry(parentUUID, objectRefUUID, keyUUID);
			} else if (next instanceof MDataValue) {
				String parentUUID = mcoreResource.getID(next.eContainer());
				String dataValueUUID = mcoreResource.getID(next);
				instance = new MElementRestoreEntry(parentUUID, dataValueUUID, "");
			} else if (next instanceof MLiteralValue) {
				String parentUUID = mcoreResource.getID(next.eContainer());
				String literalUUID = mcoreResource.getID(next);
				instance = new MElementRestoreEntry(parentUUID, literalUUID, "");
			}

			if (instance != null) {
				result.add(instance);
			}
		}
		return result;
	}

	private static final class PostProcess implements Runnable {

		private final MComponentResourceIdMap resourceIdMap;

		private final IProject project;

		private final List<FolderRestoreEntry> folderMap;

		private final MComponent component;

		private final List<MElementRestoreEntry> propertyInstanceMap;

		private PostProcess(MComponentResourceIdMap resourceIdMap, IProject project, List<FolderRestoreEntry> folderMap,
				MComponent component, List<MElementRestoreEntry> propertyInstanceMap) {
			this.resourceIdMap = resourceIdMap;
			this.project = project;
			this.folderMap = folderMap;
			this.component = component;
			this.propertyInstanceMap = propertyInstanceMap;
		}

		@Override
		public void run() {
			Job job = new Job("") {

				@Override
				protected IStatus run(IProgressMonitor monitor) {
					try {
						project.refreshLocal(IResource.DEPTH_INFINITE, new NullProgressMonitor());
					} catch (CoreException e) {
						McoreUIPlugin.log(e);
					}

					// reload resource in case changes have been made
					// during runtime execution
					ResourceSet resourceSet = ResourceService.createResourceSet();
					URI mcoreURI = EcoreUtil.getURI(component);
					URI mcorePlatformURI = resourceSet.getURIConverter().normalize(mcoreURI).trimFragment();
					String domainID = PathUtils.buildDomainID(mcorePlatformURI);
					McoreEditingDomainFactory domainFactory = McoreEditingDomainFactory.getInstance();
					McoreDiagramEditingDoamin domain = domainFactory.exists(domainID)
							? domainFactory.createEditingDomain(domainID, this)
							: null;
					try {
						resourceSet = domain != null ? domain.getResourceSet() : resourceSet;

						insureIsSave(getFileFromResource(component.eResource()));
						Resource resource = resourceSet.getResource(mcorePlatformURI, true);
						try {
							if (!resource.isLoaded()) {
								resource.load(null);
							}
						} catch (IOException e) {
							McoreUIPlugin.log(e);
						}
						MComponent reloadedComponent = (MComponent) resource.getContents().get(0);
						GenerateMResources.generate(reloadedComponent, resourceIdMap, folderMap, propertyInstanceMap,
								monitor);
					} finally {
						domainFactory.dispose(domain, this);
					}
					monitor.done();
					return Status.OK_STATUS;
				}
			};
			job.setUser(true);
			job.schedule();
		}
	}
}
