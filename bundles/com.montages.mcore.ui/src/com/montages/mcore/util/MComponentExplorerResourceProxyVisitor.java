package com.montages.mcore.util;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IResource;

import com.montages.common.resource.McoreResourceProxyVisitor;

public class MComponentExplorerResourceProxyVisitor extends McoreResourceProxyVisitor {

	private static final int PROJECT_MODEL_NESTING_LEVEL = 2;

	@Override
	protected boolean isModelFile(IResource resource) {
		return super.isModelFile(resource) && isProjectModelFolder(resource.getParent());
	}

	private boolean isProjectModelFolder(IContainer container) {
		if (container == null || container.getParent() == null || container.getProject() == null) {
			return false;
		}
		String containerProjectName = container.getProject().getName();
		return calculateNestingLevel(container, 0) == PROJECT_MODEL_NESTING_LEVEL && container.getParent().getName().equals(containerProjectName);
	}

	private int calculateNestingLevel(IContainer container, int curLevel) {
		if (container.getParent() != null) {
			return calculateNestingLevel(container.getParent(), ++curLevel);
		}
		return curLevel;
	}
}
