package com.montages.mcore.util.mailer;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class XMLPropertiesHelper {

	public static Properties readFile(String path) throws IOException {
		return readFile(new File(path));
	}

	public static Properties readFile(File file) throws IOException {
		InputStream input = new BufferedInputStream(new FileInputStream(file));
		return readStream(input);
	}

	public static Properties readStream(InputStream in) throws IOException {
		Properties result = new Properties();
		try {
			result.loadFromXML(in);
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				//
			}
		}
		return result;
	}

	public static Properties loadXMLPropertiesOrNull(File configFile) {
		if (!configFile.exists()) {
			return null;
		}
		Properties config;
		InputStream input = null;
		try {
			input = new BufferedInputStream(new FileInputStream(configFile));
			config = new Properties();
			config.loadFromXML(input);
		} catch (IOException e) {
			return null;
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
				}
			}
		}
		return config;
	}

	public static String getStringOrFail(Properties properties, String key) throws IOException {
		String result = properties.getProperty(key);
		if (result == null) {
			throw new IOException("Property: '" + key + "' is required");
		}
		return result.trim();
	}

	public static Integer getIntProperty(Properties properties, String key) throws IOException {
		String result = properties.getProperty(key);
		if (result == null) {
			return null;
		}
		try {
			return Integer.parseInt(result.trim());
		} catch (NumberFormatException e) {
			throw new IOException("Expected integer value for key : '" + key + "', actual:" + result);
		}
	}

	public static int getIntPropertyOrDefault(Properties properties, String key, int defaultInt) {
		try {
			Integer intProperty = getIntProperty(properties, key);
			return intProperty == null ? defaultInt : intProperty.intValue();
		} catch (IOException e) {
			return defaultInt;
		}
	}

	public static double getDoubleProperty(Properties properties, String key) throws IOException {
		String result = properties.getProperty(key);
		if (result == null) {
			throw new IOException("Expected double value for key : '" + key + "', actual:" + result);
		}
		try {
			return Double.valueOf(result.trim()).doubleValue();
		} catch (NumberFormatException e) {
			throw new IOException("Expected double value for key : '" + key + "', actual:" + result);
		}
	}

	public static double getDoublePropertyOrDefault(Properties properties, String key, double defaultValue) {
		try {
			return getDoubleProperty(properties, key);
		} catch (IOException e) {
			return defaultValue;
		}
	}

	public static boolean getBooleanProperty(Properties properties, String key) throws IOException {
		String result = properties.getProperty(key);
		if (result == null) {
			throw new IOException("Expected boolean value for key : '" + key + "', actual:" + result);
		}
		try {
			return Boolean.valueOf(result.trim()).booleanValue();
		} catch (NumberFormatException e) {
			throw new IOException("Expected boolean value for key : '" + key + "', actual:" + result);
		}
	}

	public static boolean getBooleanPropertyOrDefault(Properties properties, String key, boolean defaultValue) {
		try {
			return getBooleanProperty(properties, key);
		} catch (IOException e) {
			return defaultValue;
		}
	}

	public static TimeUnit getTimeUnitProperty(Properties properties, String key) throws IOException {
		String result = properties.getProperty(key);
		if (result == null) {
			throw new IOException("Expected time unit value for key : '" + key + "', actual:" + result);
		}
		try {
			return TimeUnit.valueOf(result.trim());
		} catch (NumberFormatException e) {
			throw new IOException("Expected time unit value for key : '" + key + "', actual:" + result);
		}
	}

	public static TimeUnit getTimeUnitPropertyOrDefault(Properties properties, String key, TimeUnit defaultValue) {
		try {
			return getTimeUnitProperty(properties, key);
		} catch (IOException e) {
			return defaultValue;
		}
	}
}