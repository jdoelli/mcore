package com.montages.mcore.util.mailer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;

public interface IMailer {

	public void send(MailMessage message) throws MessagingException, AddressException;

	public interface MailMessage {

		public String getSubject();

		public String getBody();

		public List<File> getAttachments();
	}

	public class StandartMailMessageImpl implements MailMessage {

		private final String myMsg;

		private final String mySubject;

		private final List<File> myAttachments;

		public StandartMailMessageImpl(String subject, String msg) {
			myMsg = msg;
			mySubject = subject;
			myAttachments = new ArrayList<File>();
		}

		@Override
		public String getBody() {
			return myMsg;
		}

		@Override
		public String getSubject() {
			return mySubject;
		}

		public void addAttachments(File attacments) {
			myAttachments.add(attacments);
		}

		public void addAttachments(List<File> attacments) {
			myAttachments.addAll(attacments);
		}

		@Override
		public List<File> getAttachments() {
			return myAttachments;
		}
	}
}
