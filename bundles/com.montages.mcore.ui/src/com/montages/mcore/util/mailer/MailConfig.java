package com.montages.mcore.util.mailer;

import java.util.Properties;

public interface MailConfig {

	public static final String DEFAULT_SMTP_PORT = "465";

	public static final String DEFAULT_SMTP_AUTH = "true";

	/**
	 * Host whose mail services will be used
	 */
	public String getHost();

	public String getFrom();

	public String getTo();

	public String getUser();

	public String getStoreProtocol();

	public String getTransportProtocol();

	public String getSmtpHost();

	public String getSmtpUser();

	public String getSmtpPassword();

	public Properties getAllMailProperties();

	public String getSmtpPort();

	public String getSmtpAuth();

	public String getSubject();

	public String getBody();

}
