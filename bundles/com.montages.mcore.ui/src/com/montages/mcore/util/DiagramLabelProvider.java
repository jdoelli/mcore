package com.montages.mcore.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.ui.provider.ExtendedImageRegistry;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.swt.graphics.Image;

import com.montages.mcore.ui.McoreUIPlugin;

public class DiagramLabelProvider implements ILabelProvider {

	private final Image IMAGE = ExtendedImageRegistry.getInstance().getImage(McoreUIPlugin.getImage("image_diagram"));

	@Override
	public void addListener(ILabelProviderListener listener) {
	}

	@Override
	public void dispose() {
	}

	@Override
	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	@Override
	public void removeListener(ILabelProviderListener listener) {
	}

	@Override
	public Image getImage(Object element) {
		if (element instanceof URI) {
			URI elementURI = (URI) element;
			if (!McoreUtil.isDiagramURI(elementURI)) {
				return null;
			}
			return IMAGE;
		}
		return null;
	}

	@Override
	public String getText(Object element) {
		if (element instanceof URI) {
			URI elementURI = (URI) element;
			if (!McoreUtil.isDiagramURI(elementURI)) {
				return null;
			}
			if (elementURI.fileExtension() != null) {
				elementURI = elementURI.trimFileExtension();
			}

			return elementURI.toString().replace("platform:/resource/", "diagram:/") + (elementURI.isPlatformResource() ? " [workspace]" : " [repository]");
		}
		return null;
	}

}