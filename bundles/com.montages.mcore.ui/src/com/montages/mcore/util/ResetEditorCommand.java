package com.montages.mcore.util;

import java.util.Collection;

import org.eclipse.emf.common.command.Command;

import com.montages.mcore.MEditor;

public class ResetEditorCommand implements Command {

	MEditor editor;
	public ResetEditorCommand(MEditor editor)
	{
	 this.editor=editor;	
	}
	@Override
	public boolean canExecute() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void execute() {
		// TODO Auto-generated method stub
         editor.resetEditor();
	}

	@Override
	public boolean canUndo() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void undo() {
		// TODO Auto-generated method stub

	}

	@Override
	public void redo() {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<?> getResult() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<?> getAffectedObjects() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return "TriggerReset";
	}

	@Override
	public String getDescription() {
		// TODO Auto-generated method stub
		return "TriggerReset";
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public Command chain(Command command) {
		// TODO Auto-generated method stub
		return null;
	}

}
