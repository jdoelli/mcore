package com.montages.mcore.util;

import org.eclipse.emf.common.ui.URIEditorInput;
import org.eclipse.emf.common.util.URI;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;

public class TableEditorInput extends URIEditorInput implements IEditorInput, IPersistableElement{

	private URI editorConfigURI;
	private URI mResourceURI;

	public TableEditorInput(IMemento memento) {
		super(memento);
	}

	public TableEditorInput(URI uri, URI editorConfigURI, URI mResourceURI) {
		super(uri);
		this.editorConfigURI = editorConfigURI;
		this.mResourceURI = mResourceURI;
	}

	public URI getEditorConfigURI() {
		return editorConfigURI;
	}

	public URI getMResourceURI() {
		return mResourceURI;
	}
	
	@Override
	public void saveState(IMemento memento) {
		super.saveState(memento);
		memento.putString("editorConfigURI", editorConfigURI.toString());
		memento.putString("mResourceURI", mResourceURI.toString());
	}
	
	@Override
	protected void loadState(IMemento memento) {
		super.loadState(memento);
		editorConfigURI = URI.createURI( memento.getString("editorConfigURI") );
		mResourceURI = URI.createURI( memento.getString("mResourceURI") );
	}

	@Override
	public String getFactoryId() {
		return TableEditorInputFactory.ID;
	}

}
