package com.montages.mcore.codegen.ui.conversions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.EcoreUtil.Copier;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;
import org.eclipse.gmf.runtime.notation.Diagram;

import com.montages.mcore.MComponent;
import com.montages.mcore.MRepository;

public class ExportImportUtil {

	public static List<MComponent> copyComponentsToRepo(List<MComponent> components, MRepository repo) {
		Map<MComponent, Resource> resourceMap = getResourceMap(components, repo.eResource());
		List<MComponent> copiedComponents = copyEObjects(components, resourceMap);
		repo.getComponent().addAll(copiedComponents);
		repo.eResource().getContents().removeAll(copiedComponents);
		return components;
	}

	private static <T extends EObject> Map<T, Resource> getResourceMap(List<T> components, Resource res) {
		Map<T, Resource> resourceMap = new HashMap<T, Resource>();
		for (T eo: components) {
			resourceMap.put((T)eo, res);
		}
		return resourceMap;
	}

	public static Map<URI, Diagram> copyDiagramToResource(List<Diagram> diagrams, Resource res) {
		List<Diagram> copies = copyEObjects(diagrams, getResourceMap(diagrams, res));
		Map<URI, Diagram> result = new HashMap<URI, Diagram>();
		firstLoop:
		for (Diagram copy: copies) {
			String copyID = EcoreUtil.getURI(copy).fragment();
			for (Diagram origin: diagrams) {
				String originID = EcoreUtil.getURI(origin).fragment();
				if (copyID.equals(originID)) {
					result.put(origin.eResource().getURI(), copy);
					continue firstLoop;
				}
			}
		}
		return result;
	}

	public static <T extends EObject> T copy(T eObject) {
		Copier copier = new Copier();
		EObject result = copier.copy(eObject);

		@SuppressWarnings("unchecked")
		T t = (T) result;
		return t;
	}

	public static <T extends EObject> List<T> copyEObjects(List<T> components, Map<T, Resource> res) {
		MCoreCopier<T> copier = new MCoreCopier<T>(res);
		return copier.copyEObjects(components);
	}

	private static class MCoreCopier<T extends EObject> extends EcoreUtil.Copier {

		private static final long serialVersionUID = -8008238628069208504L;

		private final Map<T, Resource> myCustomMCoreResources;

		private XMLResourceImpl currentRes = null;

		public MCoreCopier(Map<T, Resource> resources) {
			myCustomMCoreResources = resources;
		}

		public List<T> copyEObjects(List<T> components) {
			List<T> copies = new ArrayList<T>();
			for (T comp: components) {
				setCurrentRes((XMLResourceImpl)myCustomMCoreResources.get(comp));
				@SuppressWarnings("unchecked")
				T copy = (T) copy(comp);
				getCurrentRes().getContents().add(copy);
				copies.add(copy);
			}
			copyReferences();
			return copies;
		}

		@Override
		public EObject copy(EObject eObject) {
			if (eObject == null) {
				return null;
			}
			EObject eo = super.copy(eObject);
			URI uri = EcoreUtil.getURI(eObject);
			String id = uri == null ? null : uri.fragment();
			if (id == null) {
				id = EcoreUtil.generateUUID();
			}
			getCurrentRes().setID(eo, id);
			return eo;
		}

		public XMLResourceImpl getCurrentRes() {
			return currentRes;
		}

		public void setCurrentRes(XMLResourceImpl currentRes) {
			this.currentRes = currentRes;
		}
	}
}
