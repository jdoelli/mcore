package com.montages.mcore.codegen.ui.handlers;

import static com.montages.mcore.ui.operations.McoreOperations.GenerateComponentOperation;

import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.handlers.McoreSelectionHandler;
import com.montages.mcore.ui.operations.McoreOperations.Options;

public class McoreGenHandler extends McoreSelectionHandler {

	@Override
	protected Job createJob(final MComponent component) {
		Job job = new Job("Mcore Code Generator") {
			protected IStatus run(IProgressMonitor monitor) {
				return GenerateComponentOperation.execute(component, 
						Collections.singletonMap(Options.FORCE_CODE_GENERATION,  true), 
						monitor);
			};
		};
		return job;
	}

}
