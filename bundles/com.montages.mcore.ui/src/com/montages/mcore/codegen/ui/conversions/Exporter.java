package com.montages.mcore.codegen.ui.conversions;

import static com.montages.mcore.util.ResourceService.getMcoreOptions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.Diagram;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginFactory;
import org.xocl.editorconfig.EditorConfig;

import com.montages.common.CommonPlugin;
import com.montages.common.resource.DiagramResourceResolver;
import com.montages.mcore.MComponent;
import com.montages.mcore.MPackage;
import com.montages.mcore.MRepository;
import com.montages.mcore.McoreFactory;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.operations.PluginExtensions;
import com.montages.mcore.util.Dependencies;
import com.montages.mcore.util.RenameService;
import com.montages.mcore.util.ResourceService;
import com.montages.mtableeditor.MEditorConfig;

/**
 * Exporter performs an export from a list of mcore components into a single
 * mcore repository.
 * 
 */
public class Exporter {

	private final URIConverter myUriConverter;

	private final URI myUri;

	private MRepository myRepository;

	private Map<EditorConfig, EditorConfigModelAssociationType> myConfigs;

	private ExtensionType myExtension;

	private RenameService myRenameService = new RenameService();

	private boolean myShouldExportPluginComponents = false;

	private List<MComponent> pluginComponents = new ArrayList<MComponent>();

	private List<URI> diagramsToExport;

	public Exporter(URIConverter uriConverter, URI uri) {
		myUriConverter = uriConverter;
		myUri = uri;
		myConfigs = new HashMap<EditorConfig, EditorConfigModelAssociationType>();
		myExtension = PluginFactory.eINSTANCE.createExtensionType();
		diagramsToExport = Collections.emptyList();
	}

	public Exporter(URI uri) {
		this(ResourceService.createURIConverter(), uri);
	}

	public URI getUri() {
		return myUri;
	}

	/**
	 * Creates a single mcore repository from a list of mcore components.
	 * 
	 * @param components
	 * @return repository
	 */
	protected MRepository createMRepository(List<MComponent> components) {
		ResourceSet resourceSet = createResourceSet();

		ExportMCoreResourceFactory fact = new ExportMCoreResourceFactory();
		Resource r = fact.createResource(URI.createFileURI("Export"));
		MRepository repository = McoreFactory.eINSTANCE.createMRepository();
		r.getContents().add(repository);

		resourceSet.getResources().add(r);

		ExportImportUtil.copyComponentsToRepo(components, repository);
		for (MComponent component : repository.getComponent()) {
			try {
				exportEditorConfigs(component, myExtension.getEditorConfigModelAssociation());
			} catch (IOException e) {
				McoreUIPlugin.log(e);
			}
			exportMTE(component, repository);
		}
		
		return repository;
	}

	/**
	 * Creates a single repository containing the given component and its dependencies. 
	 * 
	 * @param component
	 * @return repository
	 */
	protected MRepository createMRepository(MComponent component) {
		List<MComponent> allDependent = Dependencies.compute(component).sort();
		List<MComponent> mainAndWorkspaceDeps = new ArrayList<MComponent>(allDependent.size());
		mainAndWorkspaceDeps.add(component);
		ResourceSet rs = component.eResource().getResourceSet();
		for (MComponent next : allDependent) {
			if (next == component) {
				continue;
			}
			URI uri = next.eResource().getURI();
			URI normaized = myUriConverter.normalize(uri);
			if (normaized.isPlatformPlugin()) {
				pluginComponents.add(next);
			} else {
				mainAndWorkspaceDeps.add(next);
			}
			rs.getResources().add(next.eResource());
		}
		return createMRepository(mainAndWorkspaceDeps);
	}

	public MRepository getMRepository() {
		if (myRepository == null) {
			myRepository = createMRepository();
		}
		if (shouldExportPluginComponents()) {
			for (MComponent c : pluginComponents) {
				if (!myRepository.getComponent().contains(c)) {
					myRepository.getComponent().add(c);
				}
			}
		} else {
			myRepository.getComponent().removeAll(pluginComponents);
		}
		return myRepository;
	}

	/**
	 * Creates a single repository from the URI locating a single mcore component. The 
	 * resulting repository contains the given component and its dependencies. 
	 * 
	 * @param componentURI
	 * @return repository
	 */
	private MRepository createMRepository() {
		ResourceSet resourceSet = createResourceSet();

		Resource resource = resourceSet.getResource(myUri, true);
		if (resource != null && resource.getContents().size() == 1) {
			EObject root = resource.getContents().get(0);

			if (root instanceof MComponent) {
				return createMRepository((MComponent) root);
			}
		}

		return null;
	}

	/**
	 * Export file located at uri to file located at outputFolder with name {@link MComponent#getEName()} and time stamp
	 * 
	 * @param uri in repository to export
	 * @param path to folder in file system
	 * @return exported file
	 */
	public File exportMCore(String outputFolder) throws IOException {
		MRepository repository = getMRepository();
		if (repository.getComponent().isEmpty()) {
			throw new RuntimeException("Nothing to export");
		}
		return exportMCore(repository, outputFolder, createMCoreFileNameWithTimestamp(repository.getComponent().get(0).getEName()));
	}

	/**
	 * 
	 * @param uri in repository to export
	 * @param path to folder in file system
	 * @param file name to export
	 * @return exported file
	 */
	public File exportMCore(String outputPathFolder, String fileName) throws IOException {
		MRepository repository = getMRepository();
		return exportMCore(repository, outputPathFolder, fileName);
	}

	protected File exportMCore(MRepository repository, String outputPathFolder, String fileName) throws IOException {
		File file = createFile(outputPathFolder + File.separator + fileName);
		URI output = URI.createFileURI(file.getAbsolutePath());

		Resource resource = repository.eResource();
		resource.setURI(output);

		//create output resource
		resource.getContents().add(myExtension);
		resource.getContents().addAll(myConfigs.keySet());
		for (MComponent component: repository.getComponent()) {
			if (component.getMainEditor() != null) {
				resource.getContents().add(component.getMainEditor());
			}
		}

		addDiagramToExport(resource);

		resource.save(getMcoreOptions(resource.getResourceSet(), myRenameService));
		resource.unload();

		return file;
	}

	/**
	 * Find .editorconfig files near .mcore file and export they.
	 * 
	 * @param component
	 * @param outputFolder absolute path to output folder
	 * @param packageToEditorConfigMap save mapping components packages names to editConfigs files. this parameter may be <code>null<code>
	 */
	public List<EditorConfig> exportEditorConfigs(MComponent component, List<EditorConfigModelAssociationType> associations) throws IOException {
		URI componentResourceURI = component.eResource().getURI();
		componentResourceURI = myUriConverter.normalize(componentResourceURI);

		if (componentResourceURI == null) {
			return Collections.emptyList();
		}

		String projectName = component.getDerivedBundleName();
		boolean isPluginComponent = componentResourceURI.isPlatformPlugin();

		List<EditorConfigModelAssociationType> baseAssociations = isPluginComponent ? PluginExtensions.getBundleExtensionType(projectName) : PluginExtensions.getResourceExtensionType(projectName);

		if (baseAssociations.isEmpty()) {
			return Collections.emptyList();
		}

		List<EditorConfig> result = new ArrayList<EditorConfig>();
		ResourceSet rs = component.eResource().getResourceSet();
		for (MPackage mPackage : component.getOwnedPackage()) {
			EPackage internalPackage = mPackage.getInternalEPackage();
			if (internalPackage == null) {
				continue;
			}
			if (internalPackage.eIsProxy()) {
				EcoreUtil.resolveAll(internalPackage);
			}

			String editorConfiguation = getEditorConfigNama(baseAssociations, internalPackage.getNsURI());
			if (editorConfiguation.isEmpty()) {
				continue;
			}

			String editorConfigFilePath = projectName + "/" + editorConfiguation;

			URI uri = null;
			if (isPluginComponent) {
				uri = URI.createPlatformPluginURI(editorConfigFilePath, false);
			} else {
				uri = URI.createPlatformResourceURI(editorConfigFilePath, false);
			}

			EditorConfig editorConfig = exportEditorConfig(uri, rs);
			if (editorConfig != null) {
				result.add(editorConfig);
				EditorConfigModelAssociationType association = PluginFactory.eINSTANCE.createEditorConfigModelAssociationType();
				association.setUri(EcoreUtil.getURI(mPackage).fragment());
				association.setConfig(EcoreUtil.getURI(editorConfig).fragment());
				associations.add(association);

				myConfigs.put(editorConfig, association);
			}
		}
		return result;
	}

	/**
	 * Collect MTE file and export it.
	 * 
	 * @param component
	 * @param outputFolder absolute path to output folder
	 * @param packageToEditorConfigMap save mapping components packages names to editConfigs files. this parameter may be <code>null<code>
	 */
	public List<MEditorConfig> exportMTE(MComponent component, MRepository mResource) {
		if (component.getMainNamedEditor() == null || false == component.getMainNamedEditor().getEditor() instanceof MEditorConfig) {
			return Collections.emptyList();
		}

		String id = EcoreUtil.getURI(component).fragment();
		if (id == null || id.isEmpty()) {
			return Collections.emptyList();
		}

		MEditorConfig mte = (MEditorConfig)component.getMainEditor();
		Map<MEditorConfig, Resource> resourceMap = new HashMap<MEditorConfig, Resource>(1);
		resourceMap.put(mte, mResource.eResource());
		List<MEditorConfig> result = ExportImportUtil.copyEObjects(Arrays.asList(new MEditorConfig[]{mte}), resourceMap);
		for (MComponent copy: mResource.getComponent()) {
			if (id.equals(EcoreUtil.getURI(copy).fragment())) {
				copy.getMainNamedEditor().setEditor(result.get(0));
				break;
			}
		}
		return result;
	}

	public String getEditorConfigNama(List<EditorConfigModelAssociationType> associations, String packageURI) {
		for (EditorConfigModelAssociationType association : associations) {
			association.getUri().equals(packageURI);
			return association.getConfig();
		}
		return "";
	}

	public EditorConfig exportEditorConfig(URI uri, ResourceSet rs) throws IOException {
		Resource resource = rs.getResource(uri, true);

		if (resource.getContents().isEmpty()) {
			return null;
		}

		return (EditorConfig) resource.getContents().get(0);
	}

	protected File createFile(String mcoreFilePath) throws IOException {
		File file = new File(mcoreFilePath);

		if (file.exists()) {
			file.delete();
		}
		file.getParentFile().mkdirs();
		file.createNewFile();

		return file;
	}

	public String createMCoreFileNameWithTimestamp(String name) {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMddHHmm");
		return name + "_" + format.format(new Date()) + ".mcore";
	}

	protected List<MComponent> findMComponents(Resource resource) {
		List<MComponent> mComponents = new ArrayList<MComponent>();

		if (!resource.getContents().isEmpty()) {
			EObject root = resource.getContents().get(0);

			if (root instanceof MComponent) {
				mComponents.add((MComponent) root);
			} else if (root instanceof MRepository) {
				MRepository repository = (MRepository) root;
				mComponents.addAll(repository.getComponent());
			}
		}
		return mComponents;
	}

	private void addDiagramToExport(Resource resource) {
		ExportImportUtil.copyDiagramToResource(loadDiagrams(getSelectedDiagram(), resource.getResourceSet()), resource);
	}

	public boolean shouldExportPluginComponents() {
		return myShouldExportPluginComponents;
	}

	public void setShouldExportPluginComponents(boolean shouldExport) {
		myShouldExportPluginComponents = shouldExport;
	}

	public boolean canExportPluginComponents() {
		return !pluginComponents.isEmpty();
	}

	private ResourceSet createResourceSet() {
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getURIConverter().getURIMap().putAll(CommonPlugin.computeURIMap());
		resourceSet.getLoadOptions().putAll(getMcoreOptions(resourceSet, null));
		return resourceSet;
	}

	public List<URI> getSelectedDiagram() {
		return diagramsToExport;
	}

	public List<URI> getAllDiagrams() {
		return new ArrayList<URI>(collectDiagramsURIs());
	}

	public void setDiagramToExport(List<URI> diagramsToExport) {
		this.diagramsToExport = diagramsToExport;
	}

	private List<URI> collectDiagramsURIs() {
		URI mcoreURI = myUriConverter.normalize(myUri);
		Map<URI, URI> diagramToComponentMap = DiagramResourceResolver.getInstance().collectWorkspaceResource();
		List<URI> offeredDigramsToExport = new ArrayList<URI>();
		for (URI diagramURI : diagramToComponentMap.keySet()) {
			if (mcoreURI.equals(diagramToComponentMap.get(diagramURI))) {
				offeredDigramsToExport.add(diagramURI);
			}
		}
		return offeredDigramsToExport;
	}

	public List<Diagram> loadDiagrams(List<URI> diagramsURIs, ResourceSet rs) {
		List<Diagram> diagram = new ArrayList<Diagram>();
		for (URI uri : diagramsURIs) {
			Resource res = rs.getResource(uri, true);
			if (res.getContents().isEmpty()) {
				continue;
			}
			EcoreUtil.resolveAll(res);
			diagram.add((Diagram) res.getContents().get(0));
		}
		return diagram;
	}
}
