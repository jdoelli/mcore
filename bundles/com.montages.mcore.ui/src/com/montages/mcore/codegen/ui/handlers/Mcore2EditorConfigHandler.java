package com.montages.mcore.codegen.ui.handlers;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.handlers.McoreSelectionHandler;
import com.montages.mcore.ui.operations.GenerateEditorConfig;

public class Mcore2EditorConfigHandler extends McoreSelectionHandler {

	@Override
	protected Job createJob(final MComponent component) {
		return new Job("Generate Editor Configuration") {
			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Generate EditorConfig", 1);
				
				Boolean editConfiguration = null;
				editConfiguration =component.getAvoidRegenerationOfEditorConfiguration() ==null ? null : component.getAvoidRegenerationOfEditorConfiguration();
				if (editConfiguration != null && editConfiguration)
				component.setAvoidRegenerationOfEditorConfiguration(false);
				
				GenerateEditorConfig.generate(component, SubMonitor.convert(monitor, 1));
				
				
				component.setAvoidRegenerationOfEditorConfiguration(editConfiguration);

				monitor.done();
				return Status.OK_STATUS;
			}
		};
	}

}
