package com.montages.mcore.codegen.ui.handlers;

import static com.montages.mcore.ui.operations.McoreOperations.GenerateEcoreOperation;

import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.handlers.McoreSelectionHandler;
import com.montages.mcore.ui.operations.McoreOperations.Options;

public class Mcore2EcoreHandler extends McoreSelectionHandler {

	@Override
	protected Job createJob(final MComponent component) {
		Job job = new Job("Generate Ecore") {
			protected IStatus run(IProgressMonitor monitor) {
				return GenerateEcoreOperation.execute(component,
						Collections.singletonMap(Options.FORCE_CODE_GENERATION, false),
						monitor);
			};
		};
		return job;
	}

}
