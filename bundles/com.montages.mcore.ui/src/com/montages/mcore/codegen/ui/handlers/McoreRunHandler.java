package com.montages.mcore.codegen.ui.handlers;

import static com.montages.mcore.ui.operations.McoreOperations.RunComponentOperation;

import java.util.Collections;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.jobs.Job;

import com.montages.mcore.MComponent;
import com.montages.mcore.ui.handlers.McoreSelectionHandler;
import com.montages.mcore.ui.operations.McoreOperations.Options;

public class McoreRunHandler extends McoreSelectionHandler {

	@Override
	protected Job createJob(final MComponent component) {
		Job job = new Job("Mcore Runtime") {
			protected IStatus run(IProgressMonitor monitor) {
				return RunComponentOperation.execute(component, 
						Collections.singletonMap(Options.FORCE_CODE_GENERATION,  true), 
						monitor);
			};
		};
		
		return job;
	}

}
