package com.montages.mcore.codegen.ui.conversions;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.gmf.runtime.notation.View;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mtableeditor.MClassToCellConfig;
import com.montages.mtableeditor.MClassToTableConfig;
import com.montages.mtableeditor.MReferenceToTableConfig;
import com.montages.mtableeditor.MRowFeatureCell;
import com.montages.mtableeditor.MTableConfig;

interface ElementResolverHelper {

	ElementResolverHelper[] mEditorCongifResolverHelpers = new ElementResolverHelper[] { new MTableConfig2IntendedPackageResolverHelper(), //
			new MTableConfig2IntendedClassResolverHelper(), //
			new MRowFeatureCell2PropertyResolverHelper(), //
			new MReferenceToTableConfig2PropertyResolverHelper(), //
			new MClassToTableConfig2ClassResolverHelper(), //
			new MClassToTableConfig2IntendedPackageResolverHelper(), //
			new MClassToCellConfig2IntendedPackageResolverHelper() };

	ElementResolverHelper[] mDiagramResolverHelpers = new ElementResolverHelper[] { new View2ElementResoverHelper()};

	/**
	 * 
	 * @param eObject
	 * 
	 * @return <code>true</code> if element has reference to elements from .mcore model
	 */
	public boolean hasMCoreRelationship(EObject eObject);

	/**
	 * @param eObject
	 * @return ID of references mcore element
	 */
	public String getMcoreElementID(EObject eObject);

	/**
	 * Set resolved mcore element in eObject
	 * 
	 * @param eObject is an object which contains reference to mcore element
	 * @param element is a new .mcore model element for reference
	 */
	public void setNewMcoreElement(EObject eObject, EObject element);

	static class MTableConfig2IntendedPackageResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MTableConfig;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MTableConfig) eObject).setIntendedPackage((MPackage) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MTableConfig) eObject).getIntendedPackage();
		}
	}

	static class MTableConfig2IntendedClassResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MTableConfig;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MTableConfig) eObject).setIntendedClass((MClassifier) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MTableConfig) eObject).getIntendedClass();
		}
	}

	static class MRowFeatureCell2PropertyResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MRowFeatureCell;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MRowFeatureCell) eObject).setFeature((MProperty) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MRowFeatureCell) eObject).getFeature();
		}
	}

	static class MReferenceToTableConfig2PropertyResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MReferenceToTableConfig;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MReferenceToTableConfig) eObject).setReference((MProperty) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MReferenceToTableConfig) eObject).getReference();
		}
	}

	static class MClassToTableConfig2ClassResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MClassToTableConfig;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MClassToTableConfig) eObject).setClass((MClassifier) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MClassToTableConfig) eObject).getClass_();
		}
	}

	static class MClassToTableConfig2IntendedPackageResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MClassToTableConfig;
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MClassToTableConfig) eObject).setIntendedPackage((MPackage) element);
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((MClassToTableConfig) eObject).getIntendedPackage();
		}
	}

	static class MClassToCellConfig2IntendedPackageResolverHelper extends AbstractElementResolverHelper {

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof MClassToCellConfig;
		}

		@Override
		protected MClassifier getElement(EObject eObject) {
			return ((MClassToCellConfig) eObject).getClass_();
		}

		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			((MClassToCellConfig) eObject).setClass((MClassifier) element);
		}
	}

	static class View2ElementResoverHelper extends AbstractElementResolverHelper {
		@Override
		public void setNewMcoreElement(EObject eObject, EObject element) {
			if(hasMCoreRelationship(eObject)) {
				((View)eObject).setElement(element);
			}
		}

		@Override
		public boolean hasMCoreRelationship(EObject eObject) {
			return eObject instanceof View;
		}

		@Override
		protected EObject getElement(EObject eObject) {
			return ((View)eObject).getElement();
		}
	}

	/**
	 * Helper to resolver EObject
	 * 
	 */
	static abstract class AbstractElementResolverHelper implements ElementResolverHelper {
		public static String getFragment(EObject eObject) {
			return EcoreUtil.getURI(eObject).fragment();
		}

		@Override
		public String getMcoreElementID(EObject eObject) {
			EObject element = getElement(eObject);
			return element != null ? getFragment(element) : null;
		}

		protected abstract EObject getElement(EObject eObject);
	}
}
