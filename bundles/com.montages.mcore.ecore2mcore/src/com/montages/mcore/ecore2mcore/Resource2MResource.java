package com.montages.mcore.ecore2mcore;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.XMLResource;

import com.montages.common.McoreUtil;
import com.montages.mcore.MComponent;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MResource;

public class Resource2MResource extends EObject2MObject {

	public Resource2MResource(List<MComponent> components, List<MElementRestoreEntry> propertyInstanceMap) {
		super(components, propertyInstanceMap);
	}

	public void transform(Resource source, MResource target, XMLResource resource) {
		ArrayList<MObject> transformed = new ArrayList<MObject>();
		for (EObject eObject : source.getContents()) {
			MObject mObject = transform(eObject, resource);
			if (mObject != null) {
				transformed.add(mObject);
			}
		}
		McoreUtil.unsafeListsMerge(target.getObject(), transformed);
	}

	public void finish() {
		clear();
	}

}
