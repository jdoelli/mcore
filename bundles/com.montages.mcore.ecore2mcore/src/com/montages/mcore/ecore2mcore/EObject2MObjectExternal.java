package com.montages.mcore.ecore2mcore;

import java.util.Collections;
import java.util.List;

import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.xmi.XMLResource;

import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.objects.MObject;

public class EObject2MObjectExternal extends EObject2MObject {

	public EObject2MObjectExternal(List<MComponent> components) {
		super(components, Collections.<MElementRestoreEntry>emptyList());
	}

	public void transform(MComponent component) {
		clear();

		for (EPackage ePackage : component.getGeneratedEPackage()) {
			for (EClassifier eClass : ePackage.getEClassifiers()) {

				MClassifier mClassifier = findMClassifier(eClass);
				
				for (EAnnotation eAnnotation : eClass.getEAnnotations()) {

					MGeneralAnnotation mGeneralAnnotation = findAnnotation(
							eAnnotation, mClassifier.getGeneralAnnotation());

					for (EObject eObject : eAnnotation.getContents()) {
						MObject mObject = transform(eObject, (XMLResource)mGeneralAnnotation.eResource());
						if (mObject != null) {
							mGeneralAnnotation.getGeneralContent().add(mObject);
						}
					}
				}
			}
		}
	}

}
