package com.montages.mcore.ecore2mcore;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.xmi.XMLResource;

import com.montages.common.McoreUtil;
import com.montages.mcore.MClassifier;
import com.montages.mcore.MComponent;
import com.montages.mcore.MLiteral;
import com.montages.mcore.MPackage;
import com.montages.mcore.MProperty;
import com.montages.mcore.annotations.MGeneralAnnotation;
import com.montages.mcore.objects.MDataValue;
import com.montages.mcore.objects.MLiteralValue;
import com.montages.mcore.objects.MObject;
import com.montages.mcore.objects.MObjectReference;
import com.montages.mcore.objects.MPropertyInstance;
import com.montages.mcore.objects.ObjectsFactory;

public abstract class EObject2MObject {

	private final Map<EObject, MObject> processed = new HashMap<EObject, MObject>();
	private final Map<EClass, MClassifier> mapOfClasses = new HashMap<EClass, MClassifier>();
	private final Map<EPackage, MPackage> mapOfPackages = new HashMap<EPackage, MPackage>();
	private final Map<MClassifier, List<MProperty>> mapOfListFeatures = new HashMap<MClassifier, List<MProperty>>();
	private final Map<EStructuralFeature, MProperty> mapOfFeatures = new HashMap<EStructuralFeature, MProperty>();
	private final List<MElementRestoreEntry> restoreUUIDMap;
	private final List<String> mapOfUUID = new ArrayList<String>();
	private XMLResource res;

	private final List<MComponent> components;

	public EObject2MObject(List<MComponent> components, List<MElementRestoreEntry> restoreUUIDMap) {
		this.components = components;
		this.restoreUUIDMap = restoreUUIDMap;
	}

	protected void clear() {
		processed.clear();
		mapOfClasses.clear();
		mapOfPackages.clear();
		mapOfListFeatures.clear();
		mapOfFeatures.clear();
		mapOfUUID.clear();
		res = null;
	}

	protected MObject transform(EObject eObject) {
		return transform(eObject, null);
	}

	protected MObject transform(EObject eObject, XMLResource res) {
		if (eObject == null) {
			return null;
		}
		if (this.res == null) {
			this.res = res;
		}
		if (processed.containsKey(eObject)) {
			return processed.get(eObject);
		}

		MObject mObject = findMObject(eObject);
		if (mObject == null) {
			mObject = ObjectsFactory.eINSTANCE.createMObject();
			setMObjectUUID(mObject, eObject);
		}

		processed.put(eObject, mObject);

		MClassifier mClassifier = findMClassifier(eObject);

		List<MPropertyInstance> instances = new ArrayList<>();
		if (mClassifier != null) {
			mObject.setType(mClassifier);
			instances.addAll(processEAttributes(eObject, mObject, mClassifier));
			instances.addAll(processEReferences(eObject, mObject, mClassifier));
		}

		McoreUtil.unsafeListsMerge(mObject.getPropertyInstance(), instances);

		return mObject;
	}

	private void setMObjectUUID(MObject mObject, EObject eObject) {
		if (res == null || eObject.eResource() == null) {
			return;
		}
		String uuid = eObject.eResource().getURIFragment(eObject);
		res.setID(mObject, uuid);
		mapOfUUID.add(uuid);
	}

	private MObject findMObject(EObject eObject) {
		if (res == null || eObject.eResource() == null) {
			return null;
		}
		String uuid = eObject.eResource().getURIFragment(eObject);
		EObject mObject = res.getEObject(uuid);
		
		if (mObject instanceof MObject) {
			return (MObject) mObject;
		}
		return null;
	}

	protected MClassifier findMClassifier(EObject eObject) {
		if (mapOfClasses.containsKey(eObject.eClass())) {
			return mapOfClasses.get(eObject.eClass());
		}

		EPackage ePackage = eObject.eClass().getEPackage();
		MPackage mPackage = findMPackage(ePackage);
		MClassifier mClassifier = null;

		if (mPackage != null) {
			Iterator<MClassifier> it = mPackage.getClassifier().iterator();
			while(mClassifier == null && it.hasNext()) {
				MClassifier current = it.next();
				if (current.getEName().equals(eObject.eClass().getName())) {
					mClassifier = current;
				}
			}
		}

		if (mClassifier != null) {
			mapOfClasses.put(eObject.eClass(), mClassifier);
		}

		return mClassifier;
	}

	protected MPackage findMPackage(EPackage ePackage) {
		if (mapOfPackages.containsKey(ePackage)) {
			return mapOfPackages.get(ePackage);
		}

		MPackage mPackage = null;
		Iterator<MComponent> it = components.iterator();
		while(mPackage == null && it.hasNext()) {
			MComponent current = it.next();
			mPackage = findMPackage(ePackage, current.getOwnedPackage());
		}

		if (mPackage != null) {
			mapOfPackages.put(ePackage, mPackage);
		}

		return mPackage;
	}

	protected MPackage findMPackage(EPackage ePackage, EList<MPackage> mPackages) {
		for (MPackage mPackage : mPackages) {			
			String nsURI = mPackage.getDerivedNsURI();
			if (ePackage.getNsURI().equalsIgnoreCase(nsURI)) {
				return mPackage;
			}
			MPackage found = findMPackage(ePackage, mPackage.getSubPackage());
			if (found != null) {
				return found;
			}
		}
		return null;
	}

	protected List<MPropertyInstance> processEAttributes(EObject eObject, MObject mObject, MClassifier mClassifier) {
		ArrayList<MPropertyInstance> instances = new ArrayList<MPropertyInstance>();
		for (EAttribute eAttribute : eObject.eClass().getEAllAttributes()) {
			if (!eAttribute.isDerived() || !eAttribute.isTransient() || eObject.eIsSet(eAttribute)) {
				MPropertyInstance instance = processEAttribute(eObject, eAttribute, mObject, mClassifier);
				if (instance != null) {
					instances.add(instance);
				}
			}
		}
		return instances;
	}

	protected MPropertyInstance processEAttribute(EObject eObject, EAttribute eAttribute, MObject mObject, MClassifier mClassifier) {
		final MProperty mProperty = findMProperty(mClassifier, eAttribute);

		if (mProperty == null)
			return null;

		final MPropertyInstance mPropertyInstance = createPropertyInstance(mProperty);

		MPropertyInstance resotred = restorePropertyInstance(mObject, mProperty, mPropertyInstance);
		if (resotred == null) {
			return null;
		}

		if (eAttribute.isMany()) {
			Collection<?> value = (Collection<?>) eObject.eGet(eAttribute);
			if (value != null && !value.isEmpty()) {
				if (eAttribute.getEAttributeType() instanceof EEnum) {
					List<MLiteralValue> literals = transformManyLiterals(resotred, eAttribute, value);
					McoreUtil.unsafeListsMerge(resotred.getInternalLiteralValue(), literals);
				} else {
					List<MDataValue> dataValues = transformManyDataValues(resotred, eAttribute, value);
					McoreUtil.unsafeListsMerge(resotred.getInternalDataValue(), dataValues);
				}
			}
		} else {
			Object value = eObject.eGet(eAttribute);
			if (isValid(value)) {
				if (eAttribute.getEAttributeType() instanceof EEnum) {
					ArrayList<MLiteralValue> literals = new ArrayList<MLiteralValue>();
					MLiteralValue literal = transformMLiteral(resotred, eAttribute, value);
					if (literal != null) {
						literals.add(literal);
					}
					McoreUtil.unsafeListsMerge(resotred.getInternalLiteralValue(), literals);
				} else {
					ArrayList<MDataValue> mDataValues = new ArrayList<MDataValue>();
					MDataValue mData = setPropertyInstanceDataValue(resotred, eAttribute, value);
					if (mData != null) {
						mDataValues.add(mData);
					}
					McoreUtil.unsafeListsMerge(resotred.getInternalDataValue(), mDataValues);
				}
			}
		}
		return resotred;
	}

	private boolean isValid(Object value) {
		if (value != null) {
			if (value instanceof String) {
				return !((String) value).trim().isEmpty();
			} else {
				return true;
			}
		} else {
			return false;
		}
	}

	protected List<MLiteralValue> transformManyLiterals(MPropertyInstance inst, EAttribute attr, Collection<?> values) {
		ArrayList<MLiteralValue> mLiteralValues = new ArrayList<MLiteralValue>();
		for (Object value : values) {
			MLiteralValue mValue = transformMLiteral(inst, attr, value);
			if (mValue != null) {
				mLiteralValues.add(mValue);
			}
		}
		return mLiteralValues;
	}

	protected List<MDataValue> transformManyDataValues(MPropertyInstance inst, EAttribute attr, Collection<?> values) {
		ArrayList<MDataValue> mDataValues = new ArrayList<MDataValue>();
		for (Object value : values) {
				MDataValue dataValue = setPropertyInstanceDataValue(inst, attr, value);
				if (dataValue != null) {
					mDataValues.add(dataValue);
				}
			}
		return mDataValues;
	}

	protected MLiteralValue transformMLiteral(MPropertyInstance mPropertyInstance, EAttribute eAttribute, Object value) {
		String stringValue = EcoreUtil.convertToString(eAttribute.getEAttributeType(), value);

		MLiteralValue v = ObjectsFactory.eINSTANCE.createMLiteralValue();
		Iterator<MLiteral> it = mPropertyInstance.getProperty().getType().getLiteral().iterator();
		MLiteral lit = null;

		while (it.hasNext() && lit == null) {
			MLiteral literal = it.next();
			if (literal.getConstantLabel() != null && literal.getConstantLabel().equalsIgnoreCase(stringValue)) {
				lit = literal;
			} else if (literal.getName().equalsIgnoreCase(stringValue)) {
				lit = literal;
			}
		}

		if (lit == null) {
			return null;
		}

		v = restoreSimpleValue(mPropertyInstance, v);
		if (v != null) {
			v.setLiteralValue(lit);
		}

		return v;
	}

	protected MDataValue setPropertyInstanceDataValue(MPropertyInstance mPropertyInstance, EAttribute eAttribute, Object value) {
		String stringValue = EcoreUtil.convertToString(eAttribute.getEAttributeType(), value);
		MDataValue mDataValue = ObjectsFactory.eINSTANCE.createMDataValue();
		MDataValue restoredDataValue = restoreSimpleValue(mPropertyInstance, mDataValue);
		if (restoredDataValue != null) {
			restoredDataValue.setDataValue(stringValue);
		}
		return restoredDataValue;
	}

	protected List<MPropertyInstance> processEReferences(EObject eObject, MObject mObject, MClassifier mClassifier) {
		List<MPropertyInstance> instances = new ArrayList<>();

		for (EReference eReference : eObject.eClass().getEAllReferences()) {
			if (eObject.eIsSet(eReference) && !eReference.isDerived() && !eReference.isTransient()) {
				MPropertyInstance instance = processEReference(eObject, eReference, mObject, mClassifier);
				if (instance != null) {
					instances.add(instance);
				}
			}
		}
		return instances;
	}

	protected MPropertyInstance processEReference(EObject eObject, EReference eReference, MObject mObject, MClassifier mClassifier) {
		MProperty mProperty = findMProperty(mClassifier, eReference);

		if (mProperty == null)
			return null;

		MPropertyInstance defaultPropertyInstance = createPropertyInstance(mProperty);

		MPropertyInstance restored= restorePropertyInstance(mObject, mProperty, defaultPropertyInstance);
		if (restored == null) {
			return null;
		}

		if (eReference.isMany()) {
			if (eReference.isContainment()) {
				List<MObject> mObjects = restoreManyContainmentReference(eObject, mObject, restored, eReference, (Collection<?>) eObject.eGet(eReference));
				McoreUtil.unsafeListsMerge(restored.getInternalContainedObject(), mObjects);
			} else {
				List<MObjectReference> mObjectReferences = restoreManyNonContainmentReference(eObject, mObject, restored, eReference, (Collection<?>) eObject.eGet(eReference));
				McoreUtil.unsafeListsMerge(restored.getInternalReferencedObject(), mObjectReferences);
			}
		} else {
			if(eReference.isContainment()) {
				ArrayList<MObject> mObjects = new ArrayList<MObject>();
				MObject result = restoreContainmentRefenrece(eObject, mObject, restored, eReference, eObject.eGet(eReference));
				if (result != null) {
					mObjects.add(result);
				}
				McoreUtil.unsafeListsMerge(restored.getInternalContainedObject(), mObjects);
			} else {
				ArrayList<MObjectReference> mObjectReferences = new ArrayList<MObjectReference>();
				MObjectReference result = restoreReference(eObject, mObject, restored, eReference, eObject.eGet(eReference));
				if (result != null) {
					mObjectReferences.add(result);
				}
				McoreUtil.unsafeListsMerge(restored.getInternalReferencedObject(), mObjectReferences);
			}
		}

		return restored;
	}

	@SuppressWarnings("unchecked")
	private <T extends EObject> T restoreElementWithObjectKey(EObject parent, EObject keyObject, T mSelf) {
		if (res == null) {
			return mSelf;
		}

		String parentUUID = res.getID(parent);
		String mPropertyUUID = res.getID(keyObject);
		T result = mSelf;
		
		// not yet stored
		if (parentUUID == null || mPropertyUUID == null) {
			return result;
		}
		
		for (MElementRestoreEntry entry: restoreUUIDMap) {
			if (entry.getParentUUID().equals(parentUUID) && mPropertyUUID.equals(entry.getKeyID())) {
				if (mapOfUUID.contains(entry.getSelfUUID())) {
					break;
				}
				result = (T) res.getEObject(entry.getSelfUUID());
				if (result == null) {
					result = mSelf;
					res.setID(mSelf, entry.getSelfUUID());
					mapOfUUID.add(entry.getSelfUUID());
				}
				break;
			}
		}
		return result;
	}

	private MPropertyInstance restorePropertyInstance(EObject parent, EObject keyObject, MPropertyInstance mSelf) {
		if (res == null) {
			return mSelf;
		}
		String parentUUID = res.getID(parent);
		String mPropertyUUID = null;
		if (keyObject.eResource() != null) {
			mPropertyUUID = keyObject.eResource().getURIFragment(keyObject);
		}
		mPropertyUUID = mPropertyUUID == null ? res.getID(keyObject): mPropertyUUID;
		for (MElementRestoreEntry entry: restoreUUIDMap) {
			if (entry.getParentUUID().equals(parentUUID) && mPropertyUUID.equals(entry.getKeyID())) {
				if (mapOfUUID.contains(entry.getSelfUUID())) {
					return mSelf;
				}
				MPropertyInstance instance = (MPropertyInstance)res.getEObject(entry.getSelfUUID());
				if (instance != null) {
					return instance;
				}
				res.setID(mSelf, entry.getSelfUUID());
				mapOfUUID.add(entry.getSelfUUID());
				return mSelf;
			}
		}
		return mSelf;
	}

	@SuppressWarnings("unchecked")
	private <T extends EObject> T restoreSimpleValue(EObject parent, T mData) {
		if (res == null) {
			return mData;
		}
		String parentUUID = res.getID(parent);
		T result = null;
		for (MElementRestoreEntry entry: restoreUUIDMap) {
			if (entry.getParentUUID().equals(parentUUID)) {
				if (mapOfUUID.contains(entry.getSelfUUID())) {
					continue;
				}
				result = (T) res.getEObject(entry.getSelfUUID());
				if (result == null) {
					res.setID(mData, entry.getSelfUUID());
				}
				mapOfUUID.add(entry.getSelfUUID());
				break;
			}
		}
		return result != null ? result : mData;
	}

	private List<MObject> restoreManyContainmentReference(EObject eObject, MObject mObject, MPropertyInstance mInstance, EReference eReference, Collection<?> values) {
		ArrayList<MObject> mobjects = new ArrayList<MObject>();
		for (Object value : values) {
			MObject newMObject = restoreContainmentRefenrece(eObject, mObject, mInstance, eReference, value);
			if (newMObject != null) {
				mobjects.add(newMObject);
			}
		}
		return mobjects;
	}

	private List<MObjectReference> restoreManyNonContainmentReference(EObject eObject, MObject mObject, MPropertyInstance mInstance, EReference eReference, Collection<?> values) {
		ArrayList<MObjectReference> mobjectreferences = new ArrayList<MObjectReference>();
		for (Object value : values) {
			MObjectReference newMObject = restoreReference(eObject, mObject, mInstance, eReference, value);
			if (newMObject != null) {
				mobjectreferences.add(newMObject);
			}
		}
		return mobjectreferences;
	}

	protected MObject restoreContainmentRefenrece(EObject eObject, MObject mObject, MPropertyInstance mInstance, EReference eReference, Object value) {
		if (false == value instanceof EObject) {
			return null;
		}
		return getReferencedMObject(mObject, eObject, (EObject) value);
	}
	
	protected MObjectReference restoreReference(EObject eObject, MObject mObject, MPropertyInstance mInstance, EReference eReference, Object value) {
		MObject mValue = getReferencedMObject(mObject, eObject, (EObject) value);
		MObjectReference mObjectReference = restoreElementWithObjectKey(mInstance, mValue, ObjectsFactory.eINSTANCE.createMObjectReference());

		if (mValue != null) {
			mObjectReference.setReferencedObject(mValue);
		}
		return mObjectReference;
	}

	private MObject getReferencedMObject(MObject current, EObject source, EObject value) {
		if (processed.containsKey(value)) {
			return processed.get(value);
		}

		if (source.equals(value)) {
			return current;
		} else {
			return transform(value, res);
		}
	}

	protected MPropertyInstance createPropertyInstance(MProperty mProperty) {
		MPropertyInstance mPropertyInstance = ObjectsFactory.eINSTANCE.createMPropertyInstance();
		mPropertyInstance.setProperty(mProperty);
		// bug667
		// mPropertyInstance.setLocalKey(mProperty.getEName());

		return mPropertyInstance;
	}

	protected MProperty findMProperty(MClassifier mClassifier, EStructuralFeature eFeature) {
		if (mapOfFeatures.containsKey(eFeature)) {
			return mapOfFeatures.get(eFeature);
		}

		List<MProperty> features;
		if (mapOfListFeatures.containsKey(mClassifier)) {
			features = mapOfListFeatures.get(mClassifier);
		} else {
			features = mClassifier.allFeatures();
			mapOfListFeatures.put(mClassifier, features);
		}

		MProperty property = findMProperty(features, eFeature.getName());

		if (property == null) {
			if (mClassifier.getContainingPackage().getEName().equals("model")) {
				if (mClassifier.getName().equals("Package")) {
					if (eFeature.getName().equals("eClassifiers")) {
						property = findMProperty(features, "classifier");
					} else if (eFeature.getName().equals("eSubpackages")) {
						property = findMProperty(features, "package");
					}
				} else if (mClassifier.getName().equals("Class")) {
					if (eFeature.getName().equals("eStructuralFeatures")) {
						property = findMProperty(features, "feature");
					}
				}
			}
		}

		if (property != null) {
			mapOfFeatures.put(eFeature, property);
		}

		return property;
	}

	private MProperty findMProperty(List<MProperty> properties, String propertyName) {
		MProperty property = null;
		Iterator<MProperty> it = properties.iterator();
		while(property == null && it.hasNext()) {
			MProperty current = it.next();
			if (current.getEName().equals(propertyName)) {
				property = current;
			}
		}

		return property;
	}

	protected MGeneralAnnotation findAnnotation(EAnnotation eAnnotation, EList<MGeneralAnnotation> generalAnnotation) {
		for (MGeneralAnnotation annotation : generalAnnotation) {
			if (annotation.getSource().equals(eAnnotation.getSource())) {
				return annotation;
			}
		}
		return null;
	}

	public static class MElementRestoreEntry {

		private final String parentUUID, selfUUID, propertyUUID;

		public MElementRestoreEntry(String parentUUID, String selfUUID, String propertyUUID) {
			this.parentUUID = parentUUID;
			this.selfUUID = selfUUID;
			this.propertyUUID = propertyUUID;
		}

		public String getParentUUID() {
			return parentUUID;
		}

		public String getSelfUUID() {
			return selfUUID;
		}

		public String getKeyID() {
			return propertyUUID;
		}
	}
}
