/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.common.edit.ui.provider;

import java.util.Arrays;
import java.util.Date;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.ui.celleditor.ExtendedComboBoxCellEditor;
import org.eclipse.emf.common.ui.celleditor.ExtendedDialogCellEditor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EModelElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EStringToStringMapEntryImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor.PropertyValueWrapper;
import org.eclipse.emf.edit.ui.EMFEditUIPlugin;
import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.ICellEditorValidator;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.xocl.common.ui.celleditors.DateTimeCellEditor;
import org.xocl.common.ui.celleditors.FontCellEditor;
import org.xocl.common.ui.celleditors.TextAndDialogCellEditor;
import org.xocl.common.ui.dialogs.MultiLineInputDialog;
import org.xocl.common.ui.dialogs.OCLExpressionDialog;
import org.xocl.core.edit.provider.DataTimeCellEditorProvider;
import org.xocl.core.expr.IOCLExpressionContextProvider;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.XoclEmfUtil;

/**
 * @author Max Stepanov
 *
 */
public class PropertyDescriptor extends org.eclipse.emf.edit.ui.provider.PropertyDescriptor {

	/**
	 * @param object
	 * @param itemPropertyDescriptor
	 */
	public PropertyDescriptor(Object object, IItemPropertyDescriptor itemPropertyDescriptor) {
		super(object, itemPropertyDescriptor);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.ui.provider.PropertyDescriptor#createEDataTypeCellEditor(org.eclipse.emf.ecore.EDataType, org.eclipse.swt.widgets.Composite)
	 */
	@Override
	protected CellEditor createEDataTypeCellEditor(final EDataType eDataType, Composite composite) {
		EStructuralFeature eFeature = (EStructuralFeature) itemPropertyDescriptor.getFeature(object);
		Class<?> instanceClass = ((EAttribute) eFeature).getEAttributeType().getInstanceClass();

		if (eFeature instanceof EAttribute && (instanceClass == String.class)) {
			IOCLExpressionContextProvider oclExpressionContextProvider;

			if (itemPropertyDescriptor instanceof IOCLExpressionContextProvider) {
				oclExpressionContextProvider = (IOCLExpressionContextProvider) itemPropertyDescriptor;
			} else {
				oclExpressionContextProvider = (IOCLExpressionContextProvider) Platform.getAdapterManager().getAdapter(eFeature, IOCLExpressionContextProvider.class);
			}

			if (oclExpressionContextProvider != null) {
				final OCLExpressionContext expressionContext = oclExpressionContextProvider.getOCLExpressionContext(object);
				if (expressionContext != null) {
					return new ExtendedDialogCellEditor(composite, getEditLabelProvider()) {
						protected EDataTypeValueHandler valueHandler = new EDataTypeValueHandler(eDataType);

						@Override
						public void create(Composite parent) {
							super.create(parent);
						}

						@Override
						protected Object openDialogBox(Control cellEditorWindow) {
							OCLExpressionDialog dialog = new OCLExpressionDialog(
									cellEditorWindow.getShell(),
									EMFEditUIPlugin.INSTANCE.getString(
											"_UI_FeatureEditorDialog_title", //$NON-NLS-1$
											new Object [] { getDisplayName(), getEditLabelProvider().getText(object) }),
											Messages.PropertyDescriptor_DialogMessage,
											valueHandler.toString(getValue()),
											expressionContext.getContext(),
											expressionContext.getVariables(),
											expressionContext.getResultTypes(), 
											expressionContext.isEditable());

							return dialog.open() == Window.OK ? valueHandler.toValue(dialog.getValue()) : null;
						}
					};
				}
			} else if (hasMixedEditorAnnotation(eFeature)) {
				// Ordinary text
				return new TextAndDialogCellEditor(composite, SWT.MULTI) {
					protected EDataTypeValueHandler valueHandler = new EDataTypeValueHandler(eDataType);

					@Override
					protected Object openDialogBox(Control cellEditorWindow) {
						MultiLineInputDialog dialog = new MultiLineInputDialog(
								cellEditorWindow.getShell(),
								EMFEditUIPlugin.INSTANCE.getString("_UI_FeatureEditorDialog_title", new Object [] { getDisplayName(), getEditLabelProvider().getText(object) }), //$NON-NLS-1$
								EMFEditUIPlugin.INSTANCE.getString("_UI_MultiLineInputDialog_message"), //$NON-NLS-1$
								valueHandler.toString(getValue()),
								valueHandler, true);
						return dialog.open() == Window.OK ? valueHandler.toValue(dialog.getValue()) : null;
					}

					@Override
					protected Object convertTextToValue(String text) {
						return text;
					}

					@Override
					protected String convertValueToText(Object value) {
						return (String) value;
					}
				};
			}
		} else if (eFeature instanceof EAttribute && (instanceClass == URI.class) && "Font".equals(eDataType.getName())) { //$NON-NLS-1$
			return new FontCellEditor(composite);
		} else if ((object instanceof EStringToStringMapEntryImpl) && "value".equals(eFeature.getName())) { //$NON-NLS-1$
			EStringToStringMapEntryImpl mapEntry = (EStringToStringMapEntryImpl) object;
			EObject eContainer = mapEntry.eContainer();
			if (eContainer instanceof EAnnotation) {
				EAnnotation eAnnotation = (EAnnotation) eContainer;
				if (XoclEmfUtil.EDITOR_CONFIG_ANNOTATION_SOURCE.equals(eAnnotation.getSource())
						|| XoclEmfUtil.OVERRIDE_EDITOR_CONFIG_ANNOTATION_SOURCE.equals(eAnnotation.getSource())) {
					if ("propertyCategory".equals(mapEntry.getKey())) { //$NON-NLS-1$
						return new EDataTypeCellEditor(eDataType, composite);
					} else if ("createColumn".equals(mapEntry.getKey()) //$NON-NLS-1$
							|| "propertyMultiline".equals(mapEntry.getKey()) //$NON-NLS-1$
							|| "mixedEditor".equals(mapEntry.getKey())) { //$NON-NLS-1$
						ExtendedComboBoxCellEditor editor = new ExtendedComboBoxCellEditor(
								composite, Arrays.asList(new Object []{ Boolean.FALSE, Boolean.TRUE }), getEditLabelProvider(), true) {
							@Override
							public Object doGetValue() {
								Object value = super.doGetValue();
								if (value instanceof Boolean) {
									return value.toString();
								}
								return value;
							}
						};	
						editor.setValue(Boolean.valueOf(mapEntry.getValue()));
						return editor;
					}
				}
			}
		} else if (eFeature instanceof EAttribute && (instanceClass == Date.class)){
			Object value = itemPropertyDescriptor.getPropertyValue(object);
			if (value instanceof PropertyValueWrapper) {
				value = ((PropertyValueWrapper) value).getEditableValue(null);
			}
			Date date = null;
			if (value instanceof Date) {
				date = (Date) value;
			} else if (value instanceof String) {
				date = (Date) EcoreUtil.createFromString(EcorePackage.Literals.EDATE, (String) value);				
			}
			if (itemPropertyDescriptor instanceof DataTimeCellEditorProvider) {
				return ((DataTimeCellEditorProvider)itemPropertyDescriptor).getDateTimeCellEditor(composite, date);
			} else 
			return new DateTimeCellEditor(composite, date, new ICellEditorValidator() {
				private EDataTypeValueHandler delegateValueHandler = new EDataTypeValueHandler(eDataType);
				public String isValid(Object value) {
					if ((value == null) ||
							((value instanceof String) && (((String) value).length() == 0))
							|| (value instanceof Date)) {
						return null;
					}
					return delegateValueHandler.isValid(value);
				}
			});
		}
		return super.createEDataTypeCellEditor(eDataType, composite);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.ui.provider.PropertyDescriptor#getEditLabelProvider()
	 */
	@Override
	protected ILabelProvider getEditLabelProvider() {
		final ILabelProvider labelProvider = super.getEditLabelProvider();
		return new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof EStructuralFeature) {
					return ((EStructuralFeature) element).getEContainingClass().getName() + "::" //$NON-NLS-1$
							+ labelProvider.getText(element);
				}
				return labelProvider.getText(element);
			}

			@Override
			public Image getImage(Object element) {
				return labelProvider.getImage(element);
			}
		};
	}

	@Override
	public CellEditor createPropertyEditor(Composite composite) {
		Object feature = itemPropertyDescriptor.getFeature(object);
		if (feature instanceof EAttribute) {
			EAttribute eAttribute = (EAttribute) feature;
			Class<?> instanceClass = ((EAttribute) eAttribute).getEAttributeType().getInstanceClass();
			if (instanceClass == String.class) {
				// Check for read only single line strings
				// A simple single line text editor must be shown in this case
				if (!eAttribute.isChangeable()) {
					Object value = itemPropertyDescriptor.getPropertyValue(object);
					if (value instanceof PropertyValueWrapper) {
						value = ((PropertyValueWrapper) value).getEditableValue(null);
					}
					if ((value instanceof String) && hasMixedEditorAnnotation(eAttribute)) {
						String readOnlyString = (String) value;
						if ((readOnlyString.indexOf('\r') >= 0) 
								|| (readOnlyString.indexOf('\n') >= 0)) {
							// Multiline readonly text
							return new TextAndDialogCellEditor(composite, SWT.MULTI, false) {
								protected EDataTypeValueHandler valueHandler = new EDataTypeValueHandler(EcorePackage.Literals.ESTRING);

								@Override
								protected Object openDialogBox(Control cellEditorWindow) {
									MultiLineInputDialog dialog = new MultiLineInputDialog(
											cellEditorWindow.getShell(),
											EMFEditUIPlugin.INSTANCE.getString("_UI_FeatureEditorDialog_title", new Object [] { getDisplayName(), getEditLabelProvider().getText(object) }), //$NON-NLS-1$
											"", //$NON-NLS-1$
											valueHandler.toString(getValue()),
											valueHandler, false);
									return dialog.open() == Window.OK ? valueHandler.toValue(dialog.getValue()) : null;
								}

								@Override
								protected Object convertTextToValue(String text) {
									return text;
								}

								@Override
								protected String convertValueToText(Object value) {
									return (String) value;
								}
							};
						} else {
							// Readonly single line text. Enabling the readonly editor
							// for copy-pasting in the same style as for multiline readonly text.
							return new EDataTypeCellEditor(EcorePackage.Literals.ESTRING, composite) {
								@Override
								protected Control createControl(Composite parent) {
									Control control = super.createControl(parent);
									text.setEditable(false);
									return control;
								}

							};
						}
					}
				}
			}
		}
		return super.createPropertyEditor(composite);
	}

	private boolean hasMixedEditorAnnotation(EModelElement eModelElement) {
		for (EAnnotation eAnnotation : eModelElement.getEAnnotations()) {
			if (XoclEmfUtil.EDITOR_CONFIG_ANNOTATION_SOURCE.equals(eAnnotation.getSource())) {
				String mixedEditor = eAnnotation.getDetails().get("mixedEditor"); //$NON-NLS-1$
				if (Boolean.parseBoolean(mixedEditor)) {
					return true;
				}
			}
		}
		return false;
	}
}