/**
 */
package org.langlets.plugin.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.langlets.plugin.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class PluginFactoryImpl extends EFactoryImpl implements PluginFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static PluginFactory init() {
		try {
			PluginFactory thePluginFactory = (PluginFactory)EPackage.Registry.INSTANCE.getEFactory(PluginPackage.eNS_URI);
			if (thePluginFactory != null) {
				return thePluginFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new PluginFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case PluginPackage.DOCUMENT_ROOT: return createDocumentRoot();
			case PluginPackage.EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE: return createEditorConfigModelAssociationType();
			case PluginPackage.EXTENSION_TYPE: return createExtensionType();
			case PluginPackage.PLUGIN_TYPE: return createPluginType();
			case PluginPackage.COMPONENT_TYPE: return createComponentType();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DocumentRoot createDocumentRoot() {
		DocumentRootImpl documentRoot = new DocumentRootImpl();
		return documentRoot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigModelAssociationType createEditorConfigModelAssociationType() {
		EditorConfigModelAssociationTypeImpl editorConfigModelAssociationType = new EditorConfigModelAssociationTypeImpl();
		return editorConfigModelAssociationType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExtensionType createExtensionType() {
		ExtensionTypeImpl extensionType = new ExtensionTypeImpl();
		return extensionType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginType createPluginType() {
		PluginTypeImpl pluginType = new PluginTypeImpl();
		return pluginType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentType createComponentType() {
		ComponentTypeImpl componentType = new ComponentTypeImpl();
		return componentType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PluginPackage getPluginPackage() {
		return (PluginPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static PluginPackage getPackage() {
		return PluginPackage.eINSTANCE;
	}

} //PluginFactoryImpl
