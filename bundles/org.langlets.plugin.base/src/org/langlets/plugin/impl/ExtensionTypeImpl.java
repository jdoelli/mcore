/**
 */
package org.langlets.plugin.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.BasicFeatureMap;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.FeatureMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.langlets.plugin.ComponentType;
import org.langlets.plugin.EditorConfigModelAssociationType;
import org.langlets.plugin.ExtensionType;
import org.langlets.plugin.PluginPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Extension Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getGroup <em>Group</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getEditorConfigModelAssociation <em>Editor Config Model Association</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getId <em>Id</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.langlets.plugin.impl.ExtensionTypeImpl#getPoint <em>Point</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ExtensionTypeImpl extends MinimalEObjectImpl.Container implements ExtensionType {
	/**
	 * The cached value of the '{@link #getGroup() <em>Group</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGroup()
	 * @generated
	 * @ordered
	 */
	protected FeatureMap group;

	/**
	 * The cached value of the '{@link #getComponent() <em>Component</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getComponent()
	 * @generated
	 * @ordered
	 */
	protected EList<ComponentType> component;

	/**
	 * The default value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected static final String ID_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getId() <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getId()
	 * @generated
	 * @ordered
	 */
	protected String id = ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPoint() <em>Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPoint()
	 * @generated
	 * @ordered
	 */
	protected static final String POINT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPoint() <em>Point</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPoint()
	 * @generated
	 * @ordered
	 */
	protected String point = POINT_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExtensionTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return PluginPackage.Literals.EXTENSION_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FeatureMap getGroup() {
		if (group == null) {
			group = new BasicFeatureMap(this, PluginPackage.EXTENSION_TYPE__GROUP);
		}
		return group;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EditorConfigModelAssociationType> getEditorConfigModelAssociation() {
		return getGroup().list(PluginPackage.Literals.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ComponentType> getComponent() {
		if (component == null) {
			component = new EObjectContainmentEList<ComponentType>(ComponentType.class, this, PluginPackage.EXTENSION_TYPE__COMPONENT);
		}
		return component;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return id;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		String oldId = id;
		id = newId;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PluginPackage.EXTENSION_TYPE__ID, oldId, id));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PluginPackage.EXTENSION_TYPE__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPoint() {
		return point;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPoint(String newPoint) {
		String oldPoint = point;
		point = newPoint;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, PluginPackage.EXTENSION_TYPE__POINT, oldPoint, point));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case PluginPackage.EXTENSION_TYPE__GROUP:
				return ((InternalEList<?>)getGroup()).basicRemove(otherEnd, msgs);
			case PluginPackage.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION:
				return ((InternalEList<?>)getEditorConfigModelAssociation()).basicRemove(otherEnd, msgs);
			case PluginPackage.EXTENSION_TYPE__COMPONENT:
				return ((InternalEList<?>)getComponent()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case PluginPackage.EXTENSION_TYPE__GROUP:
				if (coreType) return getGroup();
				return ((FeatureMap.Internal)getGroup()).getWrapper();
			case PluginPackage.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION:
				return getEditorConfigModelAssociation();
			case PluginPackage.EXTENSION_TYPE__COMPONENT:
				return getComponent();
			case PluginPackage.EXTENSION_TYPE__ID:
				return getId();
			case PluginPackage.EXTENSION_TYPE__NAME:
				return getName();
			case PluginPackage.EXTENSION_TYPE__POINT:
				return getPoint();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case PluginPackage.EXTENSION_TYPE__GROUP:
				((FeatureMap.Internal)getGroup()).set(newValue);
				return;
			case PluginPackage.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION:
				getEditorConfigModelAssociation().clear();
				getEditorConfigModelAssociation().addAll((Collection<? extends EditorConfigModelAssociationType>)newValue);
				return;
			case PluginPackage.EXTENSION_TYPE__COMPONENT:
				getComponent().clear();
				getComponent().addAll((Collection<? extends ComponentType>)newValue);
				return;
			case PluginPackage.EXTENSION_TYPE__ID:
				setId((String)newValue);
				return;
			case PluginPackage.EXTENSION_TYPE__NAME:
				setName((String)newValue);
				return;
			case PluginPackage.EXTENSION_TYPE__POINT:
				setPoint((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case PluginPackage.EXTENSION_TYPE__GROUP:
				getGroup().clear();
				return;
			case PluginPackage.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION:
				getEditorConfigModelAssociation().clear();
				return;
			case PluginPackage.EXTENSION_TYPE__COMPONENT:
				getComponent().clear();
				return;
			case PluginPackage.EXTENSION_TYPE__ID:
				setId(ID_EDEFAULT);
				return;
			case PluginPackage.EXTENSION_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case PluginPackage.EXTENSION_TYPE__POINT:
				setPoint(POINT_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case PluginPackage.EXTENSION_TYPE__GROUP:
				return group != null && !group.isEmpty();
			case PluginPackage.EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION:
				return !getEditorConfigModelAssociation().isEmpty();
			case PluginPackage.EXTENSION_TYPE__COMPONENT:
				return component != null && !component.isEmpty();
			case PluginPackage.EXTENSION_TYPE__ID:
				return ID_EDEFAULT == null ? id != null : !ID_EDEFAULT.equals(id);
			case PluginPackage.EXTENSION_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case PluginPackage.EXTENSION_TYPE__POINT:
				return POINT_EDEFAULT == null ? point != null : !POINT_EDEFAULT.equals(point);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (group: ");
		result.append(group);
		result.append(", id: ");
		result.append(id);
		result.append(", name: ");
		result.append(name);
		result.append(", point: ");
		result.append(point);
		result.append(')');
		return result.toString();
	}

} //ExtensionTypeImpl
