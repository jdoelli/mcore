/**
 */
package org.langlets.plugin;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.langlets.plugin.PluginFactory
 * @model kind="package"
 *        extendedMetaData="qualified='false'"
 * @generated
 */
public interface PluginPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "plugin";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "platform:/resource/org.langlets.plugin/model/plugin.xsd";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "plugin";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	PluginPackage eINSTANCE = org.langlets.plugin.impl.PluginPackageImpl.init();

	/**
	 * The meta object id for the '{@link org.langlets.plugin.impl.DocumentRootImpl <em>Document Root</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.plugin.impl.DocumentRootImpl
	 * @see org.langlets.plugin.impl.PluginPackageImpl#getDocumentRoot()
	 * @generated
	 */
	int DOCUMENT_ROOT = 0;

	/**
	 * The feature id for the '<em><b>Mixed</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__MIXED = 0;

	/**
	 * The feature id for the '<em><b>XMLNS Prefix Map</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XMLNS_PREFIX_MAP = 1;

	/**
	 * The feature id for the '<em><b>XSI Schema Location</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = 2;

	/**
	 * The feature id for the '<em><b>Plugin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT__PLUGIN = 3;

	/**
	 * The number of structural features of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Document Root</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOCUMENT_ROOT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.langlets.plugin.impl.EditorConfigModelAssociationTypeImpl <em>Editor Config Model Association Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.plugin.impl.EditorConfigModelAssociationTypeImpl
	 * @see org.langlets.plugin.impl.PluginPackageImpl#getEditorConfigModelAssociationType()
	 * @generated
	 */
	int EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE = 1;

	/**
	 * The feature id for the '<em><b>Config</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE__CONFIG = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE__URI = 1;

	/**
	 * The number of structural features of the '<em>Editor Config Model Association Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Editor Config Model Association Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.langlets.plugin.impl.ExtensionTypeImpl <em>Extension Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.plugin.impl.ExtensionTypeImpl
	 * @see org.langlets.plugin.impl.PluginPackageImpl#getExtensionType()
	 * @generated
	 */
	int EXTENSION_TYPE = 2;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Editor Config Model Association</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION = 1;

	/**
	 * The feature id for the '<em><b>Component</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__COMPONENT = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__ID = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__NAME = 4;

	/**
	 * The feature id for the '<em><b>Point</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE__POINT = 5;

	/**
	 * The number of structural features of the '<em>Extension Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Extension Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTENSION_TYPE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link org.langlets.plugin.impl.PluginTypeImpl <em>Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.plugin.impl.PluginTypeImpl
	 * @see org.langlets.plugin.impl.PluginPackageImpl#getPluginType()
	 * @generated
	 */
	int PLUGIN_TYPE = 3;

	/**
	 * The feature id for the '<em><b>Group</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE__GROUP = 0;

	/**
	 * The feature id for the '<em><b>Extension</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE__EXTENSION = 1;

	/**
	 * The number of structural features of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Add Extension</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE___ADD_EXTENSION__STRING_COMPONENTTYPE = 0;

	/**
	 * The operation id for the '<em>Add Extension</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE___ADD_EXTENSION__STRING_EDITORCONFIGMODELASSOCIATIONTYPE = 1;

	/**
	 * The operation id for the '<em>Find Extensions</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE___FIND_EXTENSIONS__STRING = 2;

	/**
	 * The operation id for the '<em>Find Editor Config Association</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE___FIND_EDITOR_CONFIG_ASSOCIATION__STRING = 3;

	/**
	 * The number of operations of the '<em>Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLUGIN_TYPE_OPERATION_COUNT = 4;


	/**
	 * The meta object id for the '{@link org.langlets.plugin.impl.ComponentTypeImpl <em>Component Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.langlets.plugin.impl.ComponentTypeImpl
	 * @see org.langlets.plugin.impl.PluginPackageImpl#getComponentType()
	 * @generated
	 */
	int COMPONENT_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Model</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__MODEL = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE__URI = 1;

	/**
	 * The number of structural features of the '<em>Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Component Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_TYPE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link org.langlets.plugin.DocumentRoot <em>Document Root</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Document Root</em>'.
	 * @see org.langlets.plugin.DocumentRoot
	 * @generated
	 */
	EClass getDocumentRoot();

	/**
	 * Returns the meta object for the attribute list '{@link org.langlets.plugin.DocumentRoot#getMixed <em>Mixed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Mixed</em>'.
	 * @see org.langlets.plugin.DocumentRoot#getMixed()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EAttribute getDocumentRoot_Mixed();

	/**
	 * Returns the meta object for the map '{@link org.langlets.plugin.DocumentRoot#getXMLNSPrefixMap <em>XMLNS Prefix Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XMLNS Prefix Map</em>'.
	 * @see org.langlets.plugin.DocumentRoot#getXMLNSPrefixMap()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XMLNSPrefixMap();

	/**
	 * Returns the meta object for the map '{@link org.langlets.plugin.DocumentRoot#getXSISchemaLocation <em>XSI Schema Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>XSI Schema Location</em>'.
	 * @see org.langlets.plugin.DocumentRoot#getXSISchemaLocation()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_XSISchemaLocation();

	/**
	 * Returns the meta object for the containment reference '{@link org.langlets.plugin.DocumentRoot#getPlugin <em>Plugin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Plugin</em>'.
	 * @see org.langlets.plugin.DocumentRoot#getPlugin()
	 * @see #getDocumentRoot()
	 * @generated
	 */
	EReference getDocumentRoot_Plugin();

	/**
	 * Returns the meta object for class '{@link org.langlets.plugin.EditorConfigModelAssociationType <em>Editor Config Model Association Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Editor Config Model Association Type</em>'.
	 * @see org.langlets.plugin.EditorConfigModelAssociationType
	 * @generated
	 */
	EClass getEditorConfigModelAssociationType();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.EditorConfigModelAssociationType#getConfig <em>Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Config</em>'.
	 * @see org.langlets.plugin.EditorConfigModelAssociationType#getConfig()
	 * @see #getEditorConfigModelAssociationType()
	 * @generated
	 */
	EAttribute getEditorConfigModelAssociationType_Config();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.EditorConfigModelAssociationType#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.langlets.plugin.EditorConfigModelAssociationType#getUri()
	 * @see #getEditorConfigModelAssociationType()
	 * @generated
	 */
	EAttribute getEditorConfigModelAssociationType_Uri();

	/**
	 * Returns the meta object for class '{@link org.langlets.plugin.ExtensionType <em>Extension Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extension Type</em>'.
	 * @see org.langlets.plugin.ExtensionType
	 * @generated
	 */
	EClass getExtensionType();

	/**
	 * Returns the meta object for the attribute list '{@link org.langlets.plugin.ExtensionType#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see org.langlets.plugin.ExtensionType#getGroup()
	 * @see #getExtensionType()
	 * @generated
	 */
	EAttribute getExtensionType_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link org.langlets.plugin.ExtensionType#getEditorConfigModelAssociation <em>Editor Config Model Association</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Editor Config Model Association</em>'.
	 * @see org.langlets.plugin.ExtensionType#getEditorConfigModelAssociation()
	 * @see #getExtensionType()
	 * @generated
	 */
	EReference getExtensionType_EditorConfigModelAssociation();

	/**
	 * Returns the meta object for the containment reference list '{@link org.langlets.plugin.ExtensionType#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Component</em>'.
	 * @see org.langlets.plugin.ExtensionType#getComponent()
	 * @see #getExtensionType()
	 * @generated
	 */
	EReference getExtensionType_Component();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.ExtensionType#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see org.langlets.plugin.ExtensionType#getId()
	 * @see #getExtensionType()
	 * @generated
	 */
	EAttribute getExtensionType_Id();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.ExtensionType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see org.langlets.plugin.ExtensionType#getName()
	 * @see #getExtensionType()
	 * @generated
	 */
	EAttribute getExtensionType_Name();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.ExtensionType#getPoint <em>Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Point</em>'.
	 * @see org.langlets.plugin.ExtensionType#getPoint()
	 * @see #getExtensionType()
	 * @generated
	 */
	EAttribute getExtensionType_Point();

	/**
	 * Returns the meta object for class '{@link org.langlets.plugin.PluginType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type</em>'.
	 * @see org.langlets.plugin.PluginType
	 * @generated
	 */
	EClass getPluginType();

	/**
	 * Returns the meta object for the attribute list '{@link org.langlets.plugin.PluginType#getGroup <em>Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Group</em>'.
	 * @see org.langlets.plugin.PluginType#getGroup()
	 * @see #getPluginType()
	 * @generated
	 */
	EAttribute getPluginType_Group();

	/**
	 * Returns the meta object for the containment reference list '{@link org.langlets.plugin.PluginType#getExtension <em>Extension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extension</em>'.
	 * @see org.langlets.plugin.PluginType#getExtension()
	 * @see #getPluginType()
	 * @generated
	 */
	EReference getPluginType_Extension();

	/**
	 * Returns the meta object for the '{@link org.langlets.plugin.PluginType#addExtension(java.lang.String, org.langlets.plugin.ComponentType) <em>Add Extension</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extension</em>' operation.
	 * @see org.langlets.plugin.PluginType#addExtension(java.lang.String, org.langlets.plugin.ComponentType)
	 * @generated
	 */
	EOperation getPluginType__AddExtension__String_ComponentType();

	/**
	 * Returns the meta object for the '{@link org.langlets.plugin.PluginType#addExtension(java.lang.String, org.langlets.plugin.EditorConfigModelAssociationType) <em>Add Extension</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extension</em>' operation.
	 * @see org.langlets.plugin.PluginType#addExtension(java.lang.String, org.langlets.plugin.EditorConfigModelAssociationType)
	 * @generated
	 */
	EOperation getPluginType__AddExtension__String_EditorConfigModelAssociationType();

	/**
	 * Returns the meta object for the '{@link org.langlets.plugin.PluginType#findExtensions(java.lang.String) <em>Find Extensions</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Extensions</em>' operation.
	 * @see org.langlets.plugin.PluginType#findExtensions(java.lang.String)
	 * @generated
	 */
	EOperation getPluginType__FindExtensions__String();

	/**
	 * Returns the meta object for the '{@link org.langlets.plugin.PluginType#findEditorConfigAssociation(java.lang.String) <em>Find Editor Config Association</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Find Editor Config Association</em>' operation.
	 * @see org.langlets.plugin.PluginType#findEditorConfigAssociation(java.lang.String)
	 * @generated
	 */
	EOperation getPluginType__FindEditorConfigAssociation__String();

	/**
	 * Returns the meta object for class '{@link org.langlets.plugin.ComponentType <em>Component Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Type</em>'.
	 * @see org.langlets.plugin.ComponentType
	 * @generated
	 */
	EClass getComponentType();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.ComponentType#getModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Model</em>'.
	 * @see org.langlets.plugin.ComponentType#getModel()
	 * @see #getComponentType()
	 * @generated
	 */
	EAttribute getComponentType_Model();

	/**
	 * Returns the meta object for the attribute '{@link org.langlets.plugin.ComponentType#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see org.langlets.plugin.ComponentType#getUri()
	 * @see #getComponentType()
	 * @generated
	 */
	EAttribute getComponentType_Uri();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	PluginFactory getPluginFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link org.langlets.plugin.impl.DocumentRootImpl <em>Document Root</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.plugin.impl.DocumentRootImpl
		 * @see org.langlets.plugin.impl.PluginPackageImpl#getDocumentRoot()
		 * @generated
		 */
		EClass DOCUMENT_ROOT = eINSTANCE.getDocumentRoot();

		/**
		 * The meta object literal for the '<em><b>Mixed</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DOCUMENT_ROOT__MIXED = eINSTANCE.getDocumentRoot_Mixed();

		/**
		 * The meta object literal for the '<em><b>XMLNS Prefix Map</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XMLNS_PREFIX_MAP = eINSTANCE.getDocumentRoot_XMLNSPrefixMap();

		/**
		 * The meta object literal for the '<em><b>XSI Schema Location</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__XSI_SCHEMA_LOCATION = eINSTANCE.getDocumentRoot_XSISchemaLocation();

		/**
		 * The meta object literal for the '<em><b>Plugin</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOCUMENT_ROOT__PLUGIN = eINSTANCE.getDocumentRoot_Plugin();

		/**
		 * The meta object literal for the '{@link org.langlets.plugin.impl.EditorConfigModelAssociationTypeImpl <em>Editor Config Model Association Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.plugin.impl.EditorConfigModelAssociationTypeImpl
		 * @see org.langlets.plugin.impl.PluginPackageImpl#getEditorConfigModelAssociationType()
		 * @generated
		 */
		EClass EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE = eINSTANCE.getEditorConfigModelAssociationType();

		/**
		 * The meta object literal for the '<em><b>Config</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE__CONFIG = eINSTANCE.getEditorConfigModelAssociationType_Config();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EDITOR_CONFIG_MODEL_ASSOCIATION_TYPE__URI = eINSTANCE.getEditorConfigModelAssociationType_Uri();

		/**
		 * The meta object literal for the '{@link org.langlets.plugin.impl.ExtensionTypeImpl <em>Extension Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.plugin.impl.ExtensionTypeImpl
		 * @see org.langlets.plugin.impl.PluginPackageImpl#getExtensionType()
		 * @generated
		 */
		EClass EXTENSION_TYPE = eINSTANCE.getExtensionType();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTENSION_TYPE__GROUP = eINSTANCE.getExtensionType_Group();

		/**
		 * The meta object literal for the '<em><b>Editor Config Model Association</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION_TYPE__EDITOR_CONFIG_MODEL_ASSOCIATION = eINSTANCE.getExtensionType_EditorConfigModelAssociation();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXTENSION_TYPE__COMPONENT = eINSTANCE.getExtensionType_Component();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTENSION_TYPE__ID = eINSTANCE.getExtensionType_Id();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTENSION_TYPE__NAME = eINSTANCE.getExtensionType_Name();

		/**
		 * The meta object literal for the '<em><b>Point</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTENSION_TYPE__POINT = eINSTANCE.getExtensionType_Point();

		/**
		 * The meta object literal for the '{@link org.langlets.plugin.impl.PluginTypeImpl <em>Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.plugin.impl.PluginTypeImpl
		 * @see org.langlets.plugin.impl.PluginPackageImpl#getPluginType()
		 * @generated
		 */
		EClass PLUGIN_TYPE = eINSTANCE.getPluginType();

		/**
		 * The meta object literal for the '<em><b>Group</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLUGIN_TYPE__GROUP = eINSTANCE.getPluginType_Group();

		/**
		 * The meta object literal for the '<em><b>Extension</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PLUGIN_TYPE__EXTENSION = eINSTANCE.getPluginType_Extension();

		/**
		 * The meta object literal for the '<em><b>Add Extension</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLUGIN_TYPE___ADD_EXTENSION__STRING_COMPONENTTYPE = eINSTANCE.getPluginType__AddExtension__String_ComponentType();

		/**
		 * The meta object literal for the '<em><b>Add Extension</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLUGIN_TYPE___ADD_EXTENSION__STRING_EDITORCONFIGMODELASSOCIATIONTYPE = eINSTANCE.getPluginType__AddExtension__String_EditorConfigModelAssociationType();

		/**
		 * The meta object literal for the '<em><b>Find Extensions</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLUGIN_TYPE___FIND_EXTENSIONS__STRING = eINSTANCE.getPluginType__FindExtensions__String();

		/**
		 * The meta object literal for the '<em><b>Find Editor Config Association</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PLUGIN_TYPE___FIND_EDITOR_CONFIG_ASSOCIATION__STRING = eINSTANCE.getPluginType__FindEditorConfigAssociation__String();

		/**
		 * The meta object literal for the '{@link org.langlets.plugin.impl.ComponentTypeImpl <em>Component Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see org.langlets.plugin.impl.ComponentTypeImpl
		 * @see org.langlets.plugin.impl.PluginPackageImpl#getComponentType()
		 * @generated
		 */
		EClass COMPONENT_TYPE = eINSTANCE.getComponentType();

		/**
		 * The meta object literal for the '<em><b>Model</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_TYPE__MODEL = eINSTANCE.getComponentType_Model();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_TYPE__URI = eINSTANCE.getComponentType_Uri();

	}

} //PluginPackage
