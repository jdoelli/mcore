/**
 */
package org.xocl.semantics.provider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;
import org.xocl.core.edit.provider.ItemPropertyDescriptor;
import org.xocl.semantics.SemanticsPackage;
import org.xocl.semantics.XValue;

/**
 * This is the item provider adapter for a {@link org.xocl.semantics.XValue} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class XValueItemProvider extends XSemanticsElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public XValueItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addUpdatedObjectValuePropertyDescriptor(object);
			addBooleanValuePropertyDescriptor(object);
			addIntegerValuePropertyDescriptor(object);
			addDoubleValuePropertyDescriptor(object);
			addStringValuePropertyDescriptor(object);
			addDateValuePropertyDescriptor(object);
			addLiteralValuePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addIsCorrectPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCreateClonePropertyDescriptor(object);
			}
			addEObjectValuePropertyDescriptor(object);
			addParameterArgumentsPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Updated Object Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUpdatedObjectValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Updated Object Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_updatedObjectValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_updatedObjectValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__UPDATED_OBJECT_VALUE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Boolean Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBooleanValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Boolean Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_booleanValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_booleanValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__BOOLEAN_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Integer Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIntegerValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Integer Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_integerValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_integerValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__INTEGER_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Double Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoubleValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Double Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_doubleValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_doubleValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__DOUBLE_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the String Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addStringValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the String Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_stringValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_stringValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__STRING_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Date Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDateValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Date Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_dateValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_dateValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__DATE_VALUE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Literal Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLiteralValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Literal Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_literalValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_literalValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__LITERAL_VALUE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Is Correct feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsCorrectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Correct feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_isCorrect_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_isCorrect_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__IS_CORRECT, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Create Clone feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCreateClonePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Create Clone feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_createClone_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_createClone_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__CREATE_CLONE, false, false, false, null, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the EObject Value feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addEObjectValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EObject Value feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_eObjectValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_eObjectValue_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__EOBJECT_VALUE, false, false, false, null, null, null));
	}

	/**
	 * This adds a property descriptor for the Parameter Arguments feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addParameterArgumentsPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Parameter Arguments feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_XValue_parameterArguments_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_XValue_parameterArguments_feature",
								"_UI_XValue_type"),
						SemanticsPackage.Literals.XVALUE__PARAMETER_ARGUMENTS, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns XValue.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/XValue"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((XValue) object).getKindLabel();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(XValue.class)) {
		case SemanticsPackage.XVALUE__BOOLEAN_VALUE:
		case SemanticsPackage.XVALUE__INTEGER_VALUE:
		case SemanticsPackage.XVALUE__DOUBLE_VALUE:
		case SemanticsPackage.XVALUE__STRING_VALUE:
		case SemanticsPackage.XVALUE__DATE_VALUE:
		case SemanticsPackage.XVALUE__IS_CORRECT:
		case SemanticsPackage.XVALUE__PARAMETER_ARGUMENTS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !SemanticsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}

}
