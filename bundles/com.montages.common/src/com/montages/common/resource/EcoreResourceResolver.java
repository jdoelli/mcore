package com.montages.common.resource;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class EcoreResourceResolver {

	private final Map<URI, URI> uriMap;

	public EcoreResourceResolver() {
		uriMap = EcoreResourceResolver.computeWorkspaceResourceMap();
	}

	public static Map<URI, URI> computeWorkspaceResourceMap() {
		final Map<URI, URI> map = new HashMap<URI, URI>();
		try {
			EcorePlugin.getWorkspaceRoot().accept(new IResourceProxyVisitor() {
				@Override
				public boolean visit(IResourceProxy proxy) throws CoreException {
					IResource file = proxy.requestResource();
					if (IResource.FILE == file.getType() && "mcore".equals(file.getFileExtension())) {
						IContainer folder = file.getParent();

						folder.accept(new IResourceProxyVisitor() {
							@Override
							public boolean visit(IResourceProxy proxy) throws CoreException {
								IResource res = proxy.requestResource();

								if (IResource.FILE == res.getType() && "ecore".equals(res.getFileExtension())) {
									IFile ecoreFile = (IFile) res;
									if (ecoreFile.exists()) {
										XMLHandler handler = new XMLHandler();
										ResourceResolverHelper.readXML(ecoreFile, handler);

										String bundleURI = handler.getBundleURI();
										if (bundleURI != null && !bundleURI.trim().isEmpty()) {
											map.put(URI.createURI(bundleURI), 
													URI.createPlatformResourceURI(
															ecoreFile.getFullPath().toString(), false));
										}
										return false;
									}
								}
								return true;
							}
						}, IResource.FILE);
					}
					return true;
				}
			}, IResource.FILE);
		} catch (CoreException e) {
			e.printStackTrace();
		}

		return map;
	}

	public URI unnormalize(URI uri) {
		final String fragment = uri.fragment();
		final String query = uri.query();

		URI baseURI = uri.trimFragment().trimQuery();
		URI found = null;

		final Iterator<URI> it = uriMap.keySet().iterator();

		while(found == null && it.hasNext()) {
			URI current = it.next();
			if (baseURI.equals(uriMap.get(current))) {
				found = current;
			}
		}

		if (found != null) {
			baseURI = found;
		}

		return baseURI.appendFragment(fragment).appendQuery(query);
	}

	private static class XMLHandler extends DefaultHandler {
		private String bundleURI = null;

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if ("ecore:EPackage".equals(qName)) {
				bundleURI = attributes.getValue("nsURI");
			}
			if (bundleURI != null) {
				throw new SAXException("Found " + bundleURI);
			}
		};
		public String getBundleURI() {
			return bundleURI;
		}
	}

}
