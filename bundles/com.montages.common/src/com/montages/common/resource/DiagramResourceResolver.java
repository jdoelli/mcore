package com.montages.common.resource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceProxy;
import org.eclipse.core.resources.IResourceProxyVisitor;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;

import com.montages.common.McoreUtil;

public class DiagramResourceResolver {

	private static final DiagramResourceResolver INSTANCE = new DiagramResourceResolver();

	public static DiagramResourceResolver getInstance() {
		return INSTANCE;
	}

	/**
	 * diagrams' uris to components' uris
	 * All uris is placed in workspace
	 * All uris look like "platform:/resource/..."
	 */
	private static Map<URI, URI> diagramToComponentMap = new HashMap<URI, URI>();

	private DiagramResourceResolver() {
	}

	public Map<URI, URI> collectWorkspaceResource() {
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		try {
			DiagramResourceVisitor visitor = new DiagramResourceVisitor(diagramToComponentMap);
			root.accept(visitor, IResource.FILE);
			for (URI uri : visitor.getDiagramToRemove()) {
				diagramToComponentMap.remove(uri);
			}
			diagramToComponentMap.putAll(visitor.getNewModels());
		} catch (CoreException e) {
			e.printStackTrace();
		}

		return diagramToComponentMap;
	}

	/**
	 * 
	 * @param componentURI is a platform based component uri
	 * @return List with related diagrams for a component
	 */
	public List<URI> collectDiagramForComponent(URI componentURI) {
		if (componentURI == null) {
			return Collections.emptyList();
		}
		ArrayList<URI> result = new ArrayList<URI>();
		for (URI diagramURI: diagramToComponentMap.keySet()) {
			if (componentURI.equals(diagramToComponentMap.get(diagramURI))) {
				result.add(diagramURI);
			}
		}
		return result;
	}

	private static class DiagramResourceVisitor implements IResourceProxyVisitor {

		private final Map<URI, URI> knownDigrams;

		private final Map<URI, URI> newDiagrams = new HashMap<URI, URI>();;

		private Set<URI> diagramsToDelete = new HashSet<URI>();

		public DiagramResourceVisitor(Map<URI, URI> knownDiagrams) {
			this.knownDigrams = knownDiagrams;
			diagramsToDelete.addAll(knownDiagrams.keySet());
		}

		@Override
		public boolean visit(IResourceProxy proxy) throws CoreException {
			IResource resource = proxy.requestResource();
			if (resource == null) {
				return false;
			}
			if (resource.getType() == IProject.FILE) {
				IFile file = (IFile) resource;
				if (!"mcore_diagram".equals(file.getFullPath().getFileExtension())) {
					return true;
				}
				URI diagramURI = URI.createPlatformResourceURI(file.getFullPath().toPortableString(), true);
				if (knownDigrams.containsKey(diagramURI)) {
					diagramsToDelete.remove(diagramURI);
				}
				URI mcoreURI = McoreUtil.getMcoreURIFromDiagram(diagramURI);
				if (mcoreURI == null) {
					diagramsToDelete.add(diagramURI);
					return false;
				}
				newDiagrams.put(diagramURI, mcoreURI.trimFragment());
				return true;
			}
			if (resource.getType() == IProject.PROJECT) {
				String name = resource.getName();
				if (name.endsWith(".base") || name.endsWith(".edit") || name.endsWith(".editor") || name.startsWith(".")) {
					return false;
				}
			}
			return true;
		}

		public Map<URI, URI> getNewModels() {
			return newDiagrams;
		}

		public Set<URI> getDiagramToRemove() {
			return diagramsToDelete;
		}
	}
}
