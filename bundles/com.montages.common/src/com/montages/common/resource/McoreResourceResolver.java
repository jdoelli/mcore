package com.montages.common.resource;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.osgi.framework.BundleContext;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class McoreResourceResolver {

	public static Set<URI> computePluginModels(BundleContext context, Map<URI, URI> uriMap) {
		final Set<URI> computedPluginModels = new HashSet<URI>();

		IConfigurationElement[] extensionPoints = Platform.getExtensionRegistry().getConfigurationElementsFor("com.montages.component");

		for (IConfigurationElement element : extensionPoints) {
			String model = element.getAttribute("model");
			String uri = element.getAttribute("uri");
			String bundleName = element.getContributor().getName();
			if (bundleName != null && model != null && uri != null) {
				URI locationURI = null;
				try {
					locationURI = URI.createPlatformPluginURI(bundleName, true).appendSegments(model.split("/"));
				} catch (IllegalArgumentException e) {
					e.printStackTrace();
					continue;
				}

				if (locationURI != null) {
					URI bundleURI = null;
					try {
						bundleURI = URI.createURI(uri.replace("http:", "mcore:"));
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
						continue;
					}

					if (bundleURI != null) {
						addUriMapping(bundleURI, locationURI, computedPluginModels, uriMap);
					}
				}
			}
		}

		return computedPluginModels;
	}

	public static Set<URI> computeWorkspaceModels(Map<URI, URI> uriMap) {
		return computeWorkspaceModels(uriMap, new McoreResourceProxyVisitor());
	}

	public static Set<URI> computeWorkspaceModels(Map<URI, URI> uriMap, McoreResourceProxyVisitor resourceProxyVisitor) {
		final Set<URI> computedWorkspaceModels = new HashSet<URI>();
		final List<IResource> models = new ArrayList<IResource>();
		final IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

		try {
			root.accept(resourceProxyVisitor, IResource.FILE);
			models.addAll(resourceProxyVisitor.getModels());
		} catch (CoreException e) {
			e.printStackTrace();
		}

		for (IResource res : models) {
			IProject project = res.getProject();
			IFile file = project.getFile("plugin.xml");

			URI locationURI = URI.createPlatformResourceURI(res.getFullPath().toString(), true);
			URI bundleURI = readXML(file);

			if (bundleURI != null) {
				addUriMapping(bundleURI, locationURI, computedWorkspaceModels, uriMap);
			}
		}

		return computedWorkspaceModels;
	}

	private static void addUriMapping(URI bundleURI, URI locationURI, Set<URI> target, Map<URI, URI> map) {
		// folder mapping to help resolve 
		// location of ecore companion files
		final URI bundleFolder = bundleURI.appendSegment("");
		final URI locationFolder = getLocationFolder(locationURI);

		// add fileName to mcore component uri
		bundleURI = bundleURI.appendSegment(locationURI.lastSegment());

		map.put(bundleURI, locationURI);
		map.put(bundleFolder, locationFolder);
		target.add(bundleURI);
	}

	private static URI readXML(IFile file) {
		XMLHandler handler = new XMLHandler();
		ResourceResolverHelper.readXML(file, handler);

		String bundleURI = handler.getBundleURI();
		if (bundleURI != null && !bundleURI.trim().isEmpty()) {
			return URI.createURI(bundleURI.replace("http:", "mcore:"));
		}
		return null;
	}

	public static URI getLocationFolder(URI platformURI) {
		final URI locationFolder = platformURI.trimSegments(1).appendSegment("");
		return locationFolder;
	}

	public static class XMLHandler extends DefaultHandler {

		private String bundleURI = null;

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			if (qName.equals("component") && attributes.getValue("uri") != null) {
				bundleURI = attributes.getValue("uri");
			}

			if (bundleURI != null) {
				throw new SAXException("Found " + bundleURI);
			}
		};

		public String getBundleURI() {
			return bundleURI;
		}
	}
}
