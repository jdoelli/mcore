package com.montages.common.resource;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

public class CustomResourceLocator extends ResourceSetImpl.MappedResourceLocator {

	public CustomResourceLocator(ResourceSetImpl resourceSet) {
		super(resourceSet);
	}

	@Override
	public Resource getResource(URI uri, boolean loadOnDemand) {

		// Ensure that the cached URI converter and maps are up-to-date.
		//
		cacheURIConverter();

		// Check if the normalization map contains a mapping for the URI.
		//
		URI normalizedURI = normalizationMap.get(uri);

		if (normalizedURI == null) {
			// If not, use the cached URI converter to normalize the URI and store the
			// result for reuse.
			//
			normalizedURI = cachedURIConverter.normalize(uri);
			normalizationMap.put(uri, normalizedURI);
		}

		// Determine the list of resources associated with the normalized URI.
		//
		EList<Resource> value = resourceMap.get(normalizedURI);
		if (value != null) {
			// If there is one, it mustn't be empty, so get the first one.
			//
			Resource resource = value.get(0);

			// If we're demand loading and the resource isn't loaded yet, demand load it.
			//
			if (loadOnDemand && !resource.isLoaded()) {
				demandLoadHelper(resource);
			}
			return resource;
		}

		// !! diff from super is the if
		//
		if (!cachedURIConverter.getURIMap().containsKey(uri)) {
			// Try to locate the resource via delegation only if the
			// uri is not in the uriMap.
			//
			Resource delegatedResource = delegatedGetResource(uri, loadOnDemand);
			if (delegatedResource != null) {
				map(normalizedURI, delegatedResource);
				return delegatedResource;
			}
		}

		// Failing actually locating the resource, if we're demand loading, then demand
		// create one and load it.
		//
		if (loadOnDemand) {
			if ("mcore".equals(uri.scheme())) {
				uri = URI.createURI(uri.toString().replaceFirst("mcore://", "http://"));
			}

			Resource resource = demandCreateResource(normalizedURI);
			if (resource == null) {
				throw new RuntimeException(
						"Cannot create a resource for '" + uri + "'; a registered resource factory is needed");
			}
			demandLoadHelper(resource);
			return resource;
		}

		// If all that fails, there' no corresponding resource to return.
		//
		return null;
	}

}
