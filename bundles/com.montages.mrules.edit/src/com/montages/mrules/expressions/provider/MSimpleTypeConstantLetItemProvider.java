/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.classifiers.ASimpleType;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MSimpleTypeConstantLet;

import com.montages.mrules.expressions.impl.MSimpleTypeConstantLetImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MSimpleTypeConstantLet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MSimpleTypeConstantLetItemProvider extends MConstantLetItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MSimpleTypeConstantLetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addSimpleTypePropertyDescriptor(object);
			addConstant1PropertyDescriptor(object);
			addConstant2PropertyDescriptor(object);
			addConstant3PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Simple Type feature.
		 * The list of possible choices is constructed by OCL OrderedSet{acore::classifiers::ASimpleType::Boolean, acore::classifiers::ASimpleType::Real, acore::classifiers::ASimpleType::Integer, acore::classifiers::ASimpleType::String}
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MSimpleTypeConstantLet_simpleType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MSimpleTypeConstantLet_simpleType_feature",
								"_UI_MSimpleTypeConstantLet_type"),
						ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<ASimpleType> result = new ArrayList<ASimpleType>();
						Collection<? extends ASimpleType> superResult = (Collection<? extends ASimpleType>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MSimpleTypeConstantLetImpl) object).evalSimpleTypeChoiceConstruction(result);

						if (!result.contains(null)) {
							result.add(null);
						}

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the Constant1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant1 feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MSimpleTypeConstantLet_constant1_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MSimpleTypeConstantLet_constant1_feature",
								"_UI_MSimpleTypeConstantLet_type"),
						ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Constant2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant2 feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MSimpleTypeConstantLet_constant2_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MSimpleTypeConstantLet_constant2_feature",
								"_UI_MSimpleTypeConstantLet_type"),
						ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Constant3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addConstant3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Constant3 feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MSimpleTypeConstantLet_constant3_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MSimpleTypeConstantLet_constant3_feature",
								"_UI_MSimpleTypeConstantLet_type"),
						ExpressionsPackage.Literals.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns MSimpleTypeConstantLet.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MSimpleTypeConstantLet"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MSimpleTypeConstantLetImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MSimpleTypeConstantLet.class)) {
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__SIMPLE_TYPE:
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT1:
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT2:
		case ExpressionsPackage.MSIMPLE_TYPE_CONSTANT_LET__CONSTANT3:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
