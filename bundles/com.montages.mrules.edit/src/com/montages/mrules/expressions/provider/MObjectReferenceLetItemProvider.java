/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.values.AObject;

import com.montages.mrules.expressions.ExpressionsPackage;

import com.montages.mrules.expressions.impl.MObjectReferenceLetImpl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MObjectReferenceLet} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MObjectReferenceLetItemProvider extends MConstantLetItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MObjectReferenceLetItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAObjectTypePropertyDescriptor(object);
			addAConstant1PropertyDescriptor(object);
			addAConstant2PropertyDescriptor(object);
			addAConstant3PropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AObject Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAObjectTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AObject Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MObjectReferenceLet_aObjectType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MObjectReferenceLet_aObjectType_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__AOBJECT_TYPE, true, false, true, null, null, null));
	}

	/**
	 * This adds a property descriptor for the AConstant1 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAConstant1PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AConstant1 feature.
		 * The list of possible choices is constraint by OCL if aObjectType.oclIsUndefined() then false
		else trg.aClassifier = aObjectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MObjectReferenceLet_aConstant1_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MObjectReferenceLet_aConstant1_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT1, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<AObject> result = new ArrayList<AObject>();
				Collection<? extends AObject> superResult = (Collection<? extends AObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<AObject> iterator = result.iterator(); iterator.hasNext();) {
					AObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object).evalAConstant1ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the AConstant2 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAConstant2PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AConstant2 feature.
		 * The list of possible choices is constraint by OCL if aObjectType.oclIsUndefined() then false
		else trg.aClassifier = aObjectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MObjectReferenceLet_aConstant2_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MObjectReferenceLet_aConstant2_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT2, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<AObject> result = new ArrayList<AObject>();
				Collection<? extends AObject> superResult = (Collection<? extends AObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<AObject> iterator = result.iterator(); iterator.hasNext();) {
					AObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object).evalAConstant2ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This adds a property descriptor for the AConstant3 feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAConstant3PropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AConstant3 feature.
		 * The list of possible choices is constraint by OCL if aObjectType.oclIsUndefined() then false
		else trg.aClassifier = aObjectType endif
		 */
		itemPropertyDescriptors.add(new ItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MObjectReferenceLet_aConstant3_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MObjectReferenceLet_aConstant3_feature",
						"_UI_MObjectReferenceLet_type"),
				ExpressionsPackage.Literals.MOBJECT_REFERENCE_LET__ACONSTANT3, true, false, true, null, null, null) {
			@SuppressWarnings("unchecked")
			@Override
			public Collection<?> getChoiceOfValues(Object object) {
				List<AObject> result = new ArrayList<AObject>();
				Collection<? extends AObject> superResult = (Collection<? extends AObject>) super.getChoiceOfValues(
						object);
				if (superResult != null) {
					result.addAll(superResult);
				}
				for (Iterator<AObject> iterator = result.iterator(); iterator.hasNext();) {
					AObject trg = iterator.next();
					if (trg == null) {
						continue;
					}
					if (!((MObjectReferenceLetImpl) object).evalAConstant3ChoiceConstraint(trg)) {
						iterator.remove();
					}
				}
				return result;
			}
		});
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MObjectReferenceLetImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

}
