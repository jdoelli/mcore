/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.mrules.expressions.ExpressionsFactory;
import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MApplication;

import com.montages.mrules.expressions.impl.MApplicationImpl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MApplication} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MApplicationItemProvider extends MChainOrApplicationItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MApplicationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addANamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedNameConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABusinessNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingFolderPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUriPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAllPackagesPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAResourcePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAFolderPropertyDescriptor(object);
			}
			addOperatorPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addANamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AName feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aName_feature", "_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ANAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Name Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedNameConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Name Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ANamed_aUndefinedNameConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aUndefinedNameConstant_feature",
						"_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABusiness Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABusinessNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABusiness Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aBusinessName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aBusinessName_feature",
								"_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Folder feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingFolderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Folder feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AStructuringElement_aContainingFolder_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AStructuringElement_aContainingFolder_feature",
						"_UI_AStructuringElement_type"),
				AcorePackage.Literals.ASTRUCTURING_ELEMENT__ACONTAINING_FOLDER, false, false, false, null,
				getString("_UI_zACorePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUri feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AStructuringElement_aUri_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AStructuringElement_aUri_feature",
						"_UI_AStructuringElement_type"),
				AcorePackage.Literals.ASTRUCTURING_ELEMENT__AURI, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAll Packages feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllPackagesPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Packages feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AStructuringElement_aAllPackages_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AStructuringElement_aAllPackages_feature",
						"_UI_AStructuringElement_type"),
				AcorePackage.Literals.ASTRUCTURING_ELEMENT__AALL_PACKAGES, false, false, false, null,
				getString("_UI_zACoreHelpersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the APackage feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the APackage feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AAbstractFolder_aPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AAbstractFolder_aPackage_feature",
								"_UI_AAbstractFolder_type"),
						AcorePackage.Literals.AABSTRACT_FOLDER__APACKAGE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AResource feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAResourcePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AResource feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AAbstractFolder_aResource_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AAbstractFolder_aResource_feature",
								"_UI_AAbstractFolder_type"),
						AcorePackage.Literals.AABSTRACT_FOLDER__ARESOURCE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AFolder feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFolderPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFolder feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AAbstractFolder_aFolder_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AAbstractFolder_aFolder_feature",
								"_UI_AAbstractFolder_type"),
						AcorePackage.Literals.AABSTRACT_FOLDER__AFOLDER, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Operator feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOperatorPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Operator feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MApplication_operator_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MApplication_operator_feature",
								"_UI_MApplication_type"),
						ExpressionsPackage.Literals.MAPPLICATION__OPERATOR, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MApplication_doAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MApplication_doAction_feature",
						"_UI_MApplication_type"),
				ExpressionsPackage.Literals.MAPPLICATION__DO_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(ExpressionsPackage.Literals.MAPPLICATION__OPERANDS);
			childrenFeatures.add(ExpressionsPackage.Literals.MAPPLICATION__CONTAINED_COLLECTOR);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns MApplication.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MApplication"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MApplicationImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MApplication.class)) {
		case ExpressionsPackage.MAPPLICATION__ANAME:
		case ExpressionsPackage.MAPPLICATION__AUNDEFINED_NAME_CONSTANT:
		case ExpressionsPackage.MAPPLICATION__ABUSINESS_NAME:
		case ExpressionsPackage.MAPPLICATION__AURI:
		case ExpressionsPackage.MAPPLICATION__OPERATOR:
		case ExpressionsPackage.MAPPLICATION__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case ExpressionsPackage.MAPPLICATION__OPERANDS:
		case ExpressionsPackage.MAPPLICATION__CONTAINED_COLLECTOR:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
				ExpressionsFactory.eINSTANCE.createMChain()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
				ExpressionsFactory.eINSTANCE.createMDataValueExpr()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
				ExpressionsFactory.eINSTANCE.createMLiteralValueExpr()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MAPPLICATION__OPERANDS,
				ExpressionsFactory.eINSTANCE.createMApplication()));

		newChildDescriptors.add(createChildParameter(ExpressionsPackage.Literals.MAPPLICATION__CONTAINED_COLLECTOR,
				ExpressionsFactory.eINSTANCE.createMCollectionExpression()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
