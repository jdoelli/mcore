/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractBaseDefinition;
import com.montages.mrules.expressions.MAbstractExpressionWithBase;

import com.montages.mrules.expressions.impl.MAbstractExpressionWithBaseImpl;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MAbstractExpressionWithBase} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractExpressionWithBaseItemProvider extends MAbstractExpressionItemProvider
		implements IEditingDomainItemProvider, IStructuredItemContentProvider, ITreeItemContentProvider,
		IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractExpressionWithBaseItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Override SetCommand for XUpdate execution
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected org.eclipse.emf.common.command.Command createSetCommand(org.eclipse.emf.edit.domain.EditingDomain domain,
			org.eclipse.emf.ecore.EObject owner, org.eclipse.emf.ecore.EStructuralFeature feature, Object value,
			int index) {

		return super.createSetCommand(domain, owner, feature, value, index);

	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addBaseAsCodePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseDefinitionPropertyDescriptor(object);
			}
			addABaseVarPropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addABaseExitTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABaseExitSimpleTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseExitMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addBaseExitSingularPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Base As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_baseAsCode_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpressionWithBase_baseAsCode_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpressionWithBase_base_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpressionWithBase_base_feature",
								"_UI_MAbstractExpressionWithBase_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Definition feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseDefinitionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Definition feature.
		 * The list of possible choices is constructed by OCL self.getScope()
		 */
		itemPropertyDescriptors
				.add(new ItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpressionWithBase_baseDefinition_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpressionWithBase_baseDefinition_feature",
								"_UI_MAbstractExpressionWithBase_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_DEFINITION, true, false, false,
						null, null, new String[] { "org.eclipse.ui.views.properties.expert" }) {
					@SuppressWarnings("unchecked")
					@Override
					public Collection<?> getChoiceOfValues(Object object) {
						List<MAbstractBaseDefinition> result = new ArrayList<MAbstractBaseDefinition>();
						Collection<? extends MAbstractBaseDefinition> superResult = (Collection<? extends MAbstractBaseDefinition>) super.getChoiceOfValues(
								object);
						if (superResult != null) {
							result.addAll(superResult);
						}
						result = ((MAbstractExpressionWithBaseImpl) object)
								.evalBaseDefinitionChoiceConstruction(result);

						result.remove(null);

						return result;
					}
				});
	}

	/**
	 * This adds a property descriptor for the ABase Var feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABaseVarPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABase Var feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_aBaseVar_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpressionWithBase_aBaseVar_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_VAR, true, false, true, null, null,
				null));
	}

	/**
	 * This adds a property descriptor for the ABase Exit Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABaseExitTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABase Exit Type feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractExpressionWithBase_aBaseExitType_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractExpressionWithBase_aBaseExitType_feature",
						"_UI_MAbstractExpressionWithBase_type"),
				ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_TYPE, false, false, false, null,
				null, new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABase Exit Simple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABaseExitSimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABase Exit Simple Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpressionWithBase_aBaseExitSimpleType_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpressionWithBase_aBaseExitSimpleType_feature",
								"_UI_MAbstractExpressionWithBase_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_SIMPLE_TYPE, false,
						false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Exit Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Mandatory feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpressionWithBase_baseExitMandatory_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpressionWithBase_baseExitMandatory_feature",
								"_UI_MAbstractExpressionWithBase_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY, false, false,
						false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Base Exit Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addBaseExitSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Base Exit Singular feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractExpressionWithBase_baseExitSingular_feature"),
						getString("_UI_PropertyDescriptor_description",
								"_UI_MAbstractExpressionWithBase_baseExitSingular_feature",
								"_UI_MAbstractExpressionWithBase_type"),
						ExpressionsPackage.Literals.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR, false, false,
						false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null,
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns MAbstractExpressionWithBase.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/MAbstractExpressionWithBase"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MAbstractExpressionWithBase) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractExpressionWithBase.class)) {
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_AS_CODE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__ABASE_EXIT_SIMPLE_TYPE:
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_MANDATORY:
		case ExpressionsPackage.MABSTRACT_EXPRESSION_WITH_BASE__BASE_EXIT_SINGULAR:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ExpressionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
