/**
 */
package com.montages.mrules.expressions.provider;

import com.montages.acore.abstractions.provider.ATypedItemProvider;

import com.montages.mrules.expressions.ExpressionsPackage;
import com.montages.mrules.expressions.MAbstractBaseDefinition;

import com.montages.mrules.expressions.impl.MAbstractBaseDefinitionImpl;

import com.montages.mrules.provider.MrulesEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.expressions.MAbstractBaseDefinition} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MAbstractBaseDefinitionItemProvider extends ATypedItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MAbstractBaseDefinitionItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addCalculatedBasePropertyDescriptor(object);
			addCalculatedAsCodePropertyDescriptor(object);
			addDebugPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Calculated Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Base feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractBaseDefinition_calculatedBase_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractBaseDefinition_calculatedBase_feature",
						"_UI_MAbstractBaseDefinition_type"),
				ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_BASE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Calculated As Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedAsCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated As Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MAbstractBaseDefinition_calculatedAsCode_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MAbstractBaseDefinition_calculatedAsCode_feature",
						"_UI_MAbstractBaseDefinition_type"),
				ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__CALCULATED_AS_CODE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This adds a property descriptor for the Debug feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDebugPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Debug feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MAbstractBaseDefinition_debug_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MAbstractBaseDefinition_debug_feature",
								"_UI_MAbstractBaseDefinition_type"),
						ExpressionsPackage.Literals.MABSTRACT_BASE_DEFINITION__DEBUG, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, null, null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MAbstractBaseDefinitionImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MAbstractBaseDefinition.class)) {
		case ExpressionsPackage.MABSTRACT_BASE_DEFINITION__CALCULATED_BASE:
		case ExpressionsPackage.MABSTRACT_BASE_DEFINITION__CALCULATED_AS_CODE:
		case ExpressionsPackage.MABSTRACT_BASE_DEFINITION__DEBUG:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MrulesEditPlugin.INSTANCE;
	}

}
