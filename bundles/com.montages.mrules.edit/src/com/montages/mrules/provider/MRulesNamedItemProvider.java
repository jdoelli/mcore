/**
 */
package com.montages.mrules.provider;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.mrules.MRulesNamed;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.impl.MRulesNamedImpl;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.MRulesNamed} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MRulesNamedItemProvider extends MRulesElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRulesNamedItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addALabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAKindBasePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addARenderedKindPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingComponentPropertyDescriptor(object);
			}
			addATPackageUriPropertyDescriptor(object);
			addATClassifierNamePropertyDescriptor(object);
			addATFeatureNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addATPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATFeaturePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATCoreAStringClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addANamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedNameConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABusinessNamePropertyDescriptor(object);
			}
			addSpecialENamePropertyDescriptor(object);
			addNamePropertyDescriptor(object);
			addShortNamePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addENamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addFullLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addLocalStructuralNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCalculatedShortNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCorrectNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addCorrectShortNamePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ALabel feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addALabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ALabel feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aLabel_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ALABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AKind Base feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKindBasePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKind Base feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aKindBase_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aKindBase_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AKIND_BASE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ARendered Kind feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARenderedKindPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARendered Kind feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aRenderedKind_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aRenderedKind_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Component feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingComponentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Component feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aContainingComponent_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aContainingComponent_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Package Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackageUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package Uri feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AElement_aTPackageUri_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackageUri_feature",
						"_UI_AElement_type"),
				AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE_URI, true, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AT Classifier Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifierName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifierName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Feature Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeatureNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeatureName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeatureName_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AT Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTPackage_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Classifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTClassifier_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTFeature_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_FEATURE, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AT Core AString Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATCoreAStringClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AT Core AString Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AElement_aTCoreAStringClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AElement_aTCoreAStringClass_feature",
								"_UI_AElement_type"),
						AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS, false, false, false, null,
						getString("_UI_zACorePackagePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addANamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AName feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aName_feature", "_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ANAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Name Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedNameConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Name Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ANamed_aUndefinedNameConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aUndefinedNameConstant_feature",
						"_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABusiness Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABusinessNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABusiness Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aBusinessName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aBusinessName_feature",
								"_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Special EName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addSpecialENamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Special EName feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRulesNamed_specialEName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_specialEName_feature",
								"_UI_MRulesNamed_type"),
						MrulesPackage.Literals.MRULES_NAMED__SPECIAL_ENAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_SpecialECoreSettingsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRulesNamed_name_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_name_feature",
								"_UI_MRulesNamed_type"),
						MrulesPackage.Literals.MRULES_NAMED__NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Short Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRulesNamed_shortName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_shortName_feature",
								"_UI_MRulesNamed_type"),
						MrulesPackage.Literals.MRULES_NAMED__SHORT_NAME, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_AbbreviationandNamePropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the EName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addENamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the EName feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRulesNamed_eName_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_eName_feature",
						"_UI_MRulesNamed_type"),
				MrulesPackage.Literals.MRULES_NAMED__ENAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Full Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addFullLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Full Label feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRulesNamed_fullLabel_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_fullLabel_feature",
						"_UI_MRulesNamed_type"),
				MrulesPackage.Literals.MRULES_NAMED__FULL_LABEL, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Local Structural Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addLocalStructuralNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Local Structural Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRulesNamed_localStructuralName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_localStructuralName_feature",
								"_UI_MRulesNamed_type"),
						MrulesPackage.Literals.MRULES_NAMED__LOCAL_STRUCTURAL_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_AbbreviationandNamePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRulesNamed_calculatedName_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_calculatedName_feature",
						"_UI_MRulesNamed_type"),
				MrulesPackage.Literals.MRULES_NAMED__CALCULATED_NAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_AbbreviationandNamePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Calculated Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCalculatedShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Calculated Short Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRulesNamed_calculatedShortName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_calculatedShortName_feature",
								"_UI_MRulesNamed_type"),
						MrulesPackage.Literals.MRULES_NAMED__CALCULATED_SHORT_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
						getString("_UI_AbbreviationandNamePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Correct Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRulesNamed_correctName_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_correctName_feature",
						"_UI_MRulesNamed_type"),
				MrulesPackage.Literals.MRULES_NAMED__CORRECT_NAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the Correct Short Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addCorrectShortNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Correct Short Name feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRulesNamed_correctShortName_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRulesNamed_correctShortName_feature",
						"_UI_MRulesNamed_type"),
				MrulesPackage.Literals.MRULES_NAMED__CORRECT_SHORT_NAME, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ValidationPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return ((MRulesNamedImpl) object).evalOclLabel();
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MRulesNamed.class)) {
		case MrulesPackage.MRULES_NAMED__ALABEL:
		case MrulesPackage.MRULES_NAMED__AKIND_BASE:
		case MrulesPackage.MRULES_NAMED__ARENDERED_KIND:
		case MrulesPackage.MRULES_NAMED__AT_PACKAGE_URI:
		case MrulesPackage.MRULES_NAMED__AT_CLASSIFIER_NAME:
		case MrulesPackage.MRULES_NAMED__AT_FEATURE_NAME:
		case MrulesPackage.MRULES_NAMED__ANAME:
		case MrulesPackage.MRULES_NAMED__AUNDEFINED_NAME_CONSTANT:
		case MrulesPackage.MRULES_NAMED__ABUSINESS_NAME:
		case MrulesPackage.MRULES_NAMED__SPECIAL_ENAME:
		case MrulesPackage.MRULES_NAMED__NAME:
		case MrulesPackage.MRULES_NAMED__SHORT_NAME:
		case MrulesPackage.MRULES_NAMED__ENAME:
		case MrulesPackage.MRULES_NAMED__FULL_LABEL:
		case MrulesPackage.MRULES_NAMED__LOCAL_STRUCTURAL_NAME:
		case MrulesPackage.MRULES_NAMED__CALCULATED_NAME:
		case MrulesPackage.MRULES_NAMED__CALCULATED_SHORT_NAME:
		case MrulesPackage.MRULES_NAMED__CORRECT_NAME:
		case MrulesPackage.MRULES_NAMED__CORRECT_SHORT_NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MrulesItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
