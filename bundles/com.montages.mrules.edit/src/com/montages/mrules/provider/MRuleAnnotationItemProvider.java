/**
 */
package com.montages.mrules.provider;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.mrules.MRuleAnnotation;
import com.montages.mrules.MrulesPackage;

import com.montages.mrules.expressions.ExpressionsFactory;

import com.montages.mrules.expressions.provider.MBaseChainItemProvider;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.mrules.MRuleAnnotation} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class MRuleAnnotationItemProvider extends MBaseChainItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MRuleAnnotationItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAAnnotatedPropertyDescriptor(object);
			addAAnnotatingPropertyDescriptor(object);
			addAExpectedReturnTypeOfAnnotationPropertyDescriptor(object);
			addAExpectedReturnSimpleTypeOfAnnotationPropertyDescriptor(object);
			addIsReturnValueOfAnnotationMandatoryPropertyDescriptor(object);
			addIsReturnValueOfAnnotationSingularPropertyDescriptor(object);
			addUseExplicitOclPropertyDescriptor(object);
			addOclChangedPropertyDescriptor(object);
			addOclCodePropertyDescriptor(object);
			if (shouldShowAdvancedProperties()) {
				addDoActionPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AAnnotated feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAnnotatedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAnnotated feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AAnnotation_aAnnotated_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AAnnotation_aAnnotated_feature",
								"_UI_AAnnotation_type"),
						AbstractionsPackage.Literals.AANNOTATION__AANNOTATED, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AAnnotating feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAnnotatingPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAnnotating feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AAnnotation_aAnnotating_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AAnnotation_aAnnotating_feature",
								"_UI_AAnnotation_type"),
						AbstractionsPackage.Literals.AANNOTATION__AANNOTATING, false, false, false, null,
						getString("_UI_zACoreAbstractionsPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AExpected Return Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAExpectedReturnTypeOfAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AExpected Return Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_aExpectedReturnTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MRuleAnnotation_aExpectedReturnTypeOfAnnotation_feature", "_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_TYPE_OF_ANNOTATION, false, false, false, null,
				getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AExpected Return Simple Type Of Annotation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAExpectedReturnSimpleTypeOfAnnotationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AExpected Return Simple Type Of Annotation feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_aExpectedReturnSimpleTypeOfAnnotation_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MRuleAnnotation_aExpectedReturnSimpleTypeOfAnnotation_feature",
						"_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION, false, false,
				false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Of Annotation Mandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueOfAnnotationMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Of Annotation Mandatory feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_isReturnValueOfAnnotationMandatory_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MRuleAnnotation_isReturnValueOfAnnotationMandatory_feature", "_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Is Return Value Of Annotation Singular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addIsReturnValueOfAnnotationSingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Is Return Value Of Annotation Singular feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_isReturnValueOfAnnotationSingular_feature"),
				getString("_UI_PropertyDescriptor_description",
						"_UI_MRuleAnnotation_isReturnValueOfAnnotationSingular_feature", "_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_TypingPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Use Explicit Ocl feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addUseExplicitOclPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Use Explicit Ocl feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_MRuleAnnotation_useExplicitOcl_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_MRuleAnnotation_useExplicitOcl_feature",
								"_UI_MRuleAnnotation_type"),
						MrulesPackage.Literals.MRULE_ANNOTATION__USE_EXPLICIT_OCL, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ExplicitOCLPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the Ocl Changed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOclChangedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Ocl Changed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_oclChanged_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRuleAnnotation_oclChanged_feature",
						"_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__OCL_CHANGED, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ExplicitOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Ocl Code feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addOclCodePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Ocl Code feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_oclCode_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRuleAnnotation_oclCode_feature",
						"_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__OCL_CODE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ExplicitOCLPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the Do Action feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDoActionPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the Do Action feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_MRuleAnnotation_doAction_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_MRuleAnnotation_doAction_feature",
						"_UI_MRuleAnnotation_type"),
				MrulesPackage.Literals.MRULE_ANNOTATION__DO_ACTION, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_ActionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_EXPRESSION);
			childrenFeatures.add(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_TUPLE);
			childrenFeatures.add(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_CONSTANT);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((MRuleAnnotation) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(MRuleAnnotation.class)) {
		case MrulesPackage.MRULE_ANNOTATION__AEXPECTED_RETURN_SIMPLE_TYPE_OF_ANNOTATION:
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_MANDATORY:
		case MrulesPackage.MRULE_ANNOTATION__IS_RETURN_VALUE_OF_ANNOTATION_SINGULAR:
		case MrulesPackage.MRULE_ANNOTATION__USE_EXPLICIT_OCL:
		case MrulesPackage.MRULE_ANNOTATION__OCL_CHANGED:
		case MrulesPackage.MRULE_ANNOTATION__OCL_CODE:
		case MrulesPackage.MRULE_ANNOTATION__DO_ACTION:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		case MrulesPackage.MRULE_ANNOTATION__NAMED_EXPRESSION:
		case MrulesPackage.MRULE_ANNOTATION__NAMED_TUPLE:
		case MrulesPackage.MRULE_ANNOTATION__NAMED_CONSTANT:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add(createChildParameter(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_EXPRESSION,
				ExpressionsFactory.eINSTANCE.createMNamedExpression()));

		newChildDescriptors.add(createChildParameter(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_TUPLE,
				ExpressionsFactory.eINSTANCE.createMTuple()));

		newChildDescriptors.add(createChildParameter(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_TUPLE,
				ExpressionsFactory.eINSTANCE.createMNewObject()));

		newChildDescriptors.add(createChildParameter(MrulesPackage.Literals.MRULE_ANNOTATION__NAMED_CONSTANT,
				ExpressionsFactory.eINSTANCE.createMNamedConstant()));
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return MrulesEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !MrulesItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
