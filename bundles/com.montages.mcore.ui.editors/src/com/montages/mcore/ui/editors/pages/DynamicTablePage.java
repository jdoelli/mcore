package com.montages.mcore.ui.editors.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.presentation.EcoreEditorPlugin;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.action.EditingDomainActionBarContributor;
import org.eclipse.emf.edit.ui.celleditor.AdapterFactoryTreeEditor;
import org.eclipse.emf.edit.ui.dnd.EditingDomainViewerDropAdapter;
import org.eclipse.emf.edit.ui.dnd.LocalTransfer;
import org.eclipse.emf.edit.ui.dnd.ViewerDragAdapter;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryLabelProvider;
import org.eclipse.emf.edit.ui.provider.UnwrappingSelectionProvider;
import org.eclipse.emf.edit.ui.view.ExtendedPropertySheetPage;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.StructuredViewer;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.forms.IManagedForm;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.forms.editor.FormPage;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.views.properties.IPropertySheetPage;
import org.eclipse.ui.views.properties.PropertySheetPage;
import org.xocl.common.edit.ui.provider.AdapterFactoryContentProvider;
import org.xocl.editorconfig.ui.TreeViewerManager;
import org.xocl.editorconfig.ui.ViewManagerContentProvider;

import com.montages.mcore.ui.editors.DynamicTableEditor;
import com.montages.mcore.ui.editors.viewers.DynamicTreeViewerManager;

public class DynamicTablePage extends FormPage implements IMenuListener {

	private AdapterFactoryEditingDomain editingDomain;
	private ComposedAdapterFactory adapterFactory;
	private TreeViewer tableViewer;
	private TreeViewerManager tableManager;
	private URI editorConfigURI;
	protected ISelection selection;
	protected List<PropertySheetPage> propertySheetPages = new ArrayList<PropertySheetPage>();
	private Resource primaryResource;

	public DynamicTablePage(FormEditor editor, String id, String title) {
		super(editor, id, title);
		this.editingDomain = (AdapterFactoryEditingDomain) ((DynamicTableEditor) editor).getEditingDomain();
		this.adapterFactory = ((DynamicTableEditor) editor).getAdapterFactory();
	}

	public Viewer getViewer() {
		return tableViewer;
	}

	@Override
	protected void createFormContent(IManagedForm managedForm) {
		FormToolkit toolkit = managedForm.getToolkit();
		ScrolledForm form = managedForm.getForm();
		form.setText(getTitle());

		Composite body = form.getBody();
		toolkit.decorateFormHeading(form.getForm());
		toolkit.paintBordersFor(body);
		managedForm.getForm().getBody().setLayout(new GridLayout(1, false));

		Tree tree = new Tree(body, SWT.FULL_SELECTION | SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		tree.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 1, 1));
		tree.setHeaderVisible(true);
		tree.setLinesVisible(true);
		tableViewer = new TreeViewer(tree);

		TreeColumn objectColumn = new TreeColumn(tree, SWT.NONE);
		objectColumn.setText("Label");
		objectColumn.setResizable(true);
		objectColumn.setWidth(250);

		TreeColumn selfColumn = new TreeColumn(tree, SWT.NONE);
		selfColumn.setText("Label");
		selfColumn.setResizable(true);
		selfColumn.setWidth(200);

		tableViewer.setLabelProvider(new AdapterFactoryLabelProvider(adapterFactory));
		tableViewer.setColumnProperties(new String[] { "a", "b" });

		new AdapterFactoryTreeEditor(tableViewer.getTree(), adapterFactory);
		tableManager = new DynamicTreeViewerManager(tableViewer, editingDomain);
		tableViewer.setContentProvider(new ViewManagerContentProvider(editingDomain.getAdapterFactory(), tableManager) {
			@Override
			public Object[] getElements(Object object) {
				if (object instanceof ResourceSet) {
					ResourceSet resourceSet = (ResourceSet) object;
					EcoreUtil.resolveAll(resourceSet);

					return resourceSet.getResources().stream() //
							.filter(e -> !Arrays.asList("ecore", "editorconfig").contains(e.getURI().fileExtension())) //
							.toArray();
				}
				return super.getElements(object);
			}
		});
		tableManager.setContentProvider((ViewManagerContentProvider) tableViewer.getContentProvider());
		tableManager.getResourceSet().getResource(editorConfigURI, true);
		tableManager.initForEditor(this);

		if (primaryResource != null) {
			tableManager.setPrimaryResource(primaryResource);
		}
		tableManager.setContextMenu(createContextMenuFor(tableViewer));
		tableViewer.setInput(editingDomain.getResourceSet());

		tableViewer.setSelection(new StructuredSelection(primaryResource));
		tableViewer.expandToLevel(primaryResource, 2);

//		editingDomain.getResourceSet().eSetDeliver(true);
//		editingDomain.getResourceSet().eAdapters().add(new EContentAdapter() {
//			@Override
//			public void notifyChanged(Notification notification) {
//				if (notification.getEventType() == DynamicTableEditor.REFRESH) {
//					getViewer().getControl().getDisplay().asyncExec(new Runnable() {
//						@Override
//						public void run() {
//							tableViewer.refresh();
//						}
//					});
//				}
//				if (notification.getEventType() == DynamicTableEditor.RELOAD_CONFIG) {
//					getViewer().getControl().getDisplay().asyncExec(new Runnable() {
//						@Override
//						public void run() {
////							tableManager.reloadEditorConfig();
//						}
//					});
//				}
//			}
//		});

		getSite().setSelectionProvider(tableViewer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> T getAdapter(Class<T> adapter) {
		if (adapter.equals(IPropertySheetPage.class)) {
			return (T) getPropertySheetPage();
		}
		return super.getAdapter(adapter);
	}

	public IPropertySheetPage getPropertySheetPage() {
		PropertySheetPage propertySheetPage = new ExtendedPropertySheetPage(editingDomain,
				ExtendedPropertySheetPage.Decoration.LIVE, EcoreEditorPlugin.getPlugin().getDialogSettings()) {

			@Override
			public void setSelectionToViewer(List<?> selection) {
				DynamicTablePage.this.setSelectionToViewer(selection);
				DynamicTablePage.this.setFocus();
			}

			@Override
			public void setActionBars(IActionBars actionBars) {
				super.setActionBars(actionBars);
				getActionBarContributor().shareGlobalActions(this, actionBars);
			}

		};

		propertySheetPage.setPropertySourceProvider(new AdapterFactoryContentProvider(adapterFactory));
		propertySheetPages.add(propertySheetPage);

		return propertySheetPage;
	}

	public EditingDomainActionBarContributor getActionBarContributor() {
		return (EditingDomainActionBarContributor) getEditorSite().getActionBarContributor();
	}

	public void setSelectionToViewer(Collection<?> collection) {
		final Collection<?> theSelection = collection;
		// Make sure it's okay.
		//
		if (theSelection != null && !theSelection.isEmpty()) {
			Runnable runnable = new Runnable() {
				public void run() {
					// Try to select the items in the current content viewer of the editor.
					//
					if (getViewer() != null) {
						getViewer().setSelection(new StructuredSelection(theSelection.toArray()), true);
					}
				}
			};
			getSite().getShell().getDisplay().asyncExec(runnable);
		}
	}

	public void restoreExpandedElements(Object[] previousElements) {
		Object[] reloaded = new Object[previousElements.length];

		for (int i = 0; i < previousElements.length; i++) {
			Object current = previousElements[i];
			if (current instanceof EObject) {
				if (((EObject) current).eIsProxy()) {
					EObject resolved = EcoreUtil.resolve((EObject) current, editingDomain.getResourceSet());
					reloaded[i] = resolved;
				} else {
					reloaded[i] = current;
				}
			} else {
				reloaded[i] = current;
			}
		}

		((TreeViewer) getViewer()).setExpandedElements(reloaded);
	}

	public URI getEditorConfigURI() {
		return editorConfigURI;
	}

	public void setEditorConfigURI(URI editorConfigURI) {
		this.editorConfigURI = editorConfigURI;
	}

	protected IMenuManager createContextMenuFor(StructuredViewer viewer) {
		MenuManager contextMenu = new MenuManager("#PopUp");
		contextMenu.add(new Separator("additions"));
		contextMenu.setRemoveAllWhenShown(true);
		contextMenu.addMenuListener(this);
		Menu menu = contextMenu.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		getSite().registerContextMenu(contextMenu, new UnwrappingSelectionProvider(viewer));

		int dndOperations = DND.DROP_COPY | DND.DROP_MOVE | DND.DROP_LINK;
		Transfer[] transfers = new Transfer[] { LocalTransfer.getInstance(), LocalSelectionTransfer.getTransfer() };
		viewer.addDragSupport(dndOperations, transfers, new ViewerDragAdapter(viewer));
		viewer.addDropSupport(dndOperations, transfers, new EditingDomainViewerDropAdapter(editingDomain, viewer));
		return contextMenu;
	}

	@Override
	public void menuAboutToShow(IMenuManager manager) {
		((IMenuListener) getEditorSite().getActionBarContributor()).menuAboutToShow(manager);
	}

	public void setPrimaryResource(Resource resource) {
		this.primaryResource = resource;
	}

	public List<PropertySheetPage> getPropertySheetPages() {
		return propertySheetPages;
	}

	@Override
	public boolean isDirty() {
		return ((BasicCommandStack) editingDomain.getCommandStack()).isSaveNeeded();
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		getEditor().doSave(monitor);
	}

}
