package com.montages.mcore.ui.editors;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.emf.common.command.BasicCommandStack;
import org.eclipse.emf.common.command.Command;
import org.eclipse.emf.common.command.CommandStack;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.XMLResource;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.emf.edit.provider.ComposedAdapterFactory;
import org.eclipse.emf.edit.ui.util.EditUIUtil;
import org.eclipse.jface.dialogs.ProgressMonitorDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.actions.WorkspaceModifyOperation;
import org.eclipse.ui.forms.editor.FormEditor;
import org.eclipse.ui.views.properties.PropertySheetPage;

import com.montages.mcore.dynamic.DynamicEditingDomainFactory;
import com.montages.mcore.dynamic.DynamicSetup;
import com.montages.mcore.ui.McoreUIPlugin;
import com.montages.mcore.ui.editors.adapters.InitValueAdapter;
import com.montages.mcore.ui.editors.pages.DynamicTablePage;
import com.montages.mcore.ui.operations.GenerateMResources;
import com.montages.mcore.util.TableEditorInput;

public class DynamicTableEditor extends FormEditor implements IEditingDomainProvider, ISelectionProvider {

	public static final String ID = "com.montages.mcore.ui.editors.DynamicTableEditor"; //$NON-NLS-1$
	public static final int REFRESH = 1001;
	public static final int RELOAD_CONFIG = 1002;

	private AdapterFactoryEditingDomain editingDomain;

	private void initEditingDomain() {
		editingDomain = new DynamicEditingDomainFactory().createEditingDomain();

		editingDomain.getCommandStack().addCommandStackListener(event -> {
			getContainer().getDisplay().asyncExec(new Runnable() {
				public void run() {
					firePropertyChange(IEditorPart.PROP_DIRTY);
					// Try to select the affected objects.
					//
					DynamicTablePage currentPage = (DynamicTablePage) getActivePageInstance();
					Command mostRecentCommand = ((CommandStack) event.getSource()).getMostRecentCommand();
					if (mostRecentCommand != null) {
						currentPage.setSelectionToViewer(mostRecentCommand.getAffectedObjects());
					}
					for (Iterator<PropertySheetPage> i = currentPage.getPropertySheetPages().iterator(); i.hasNext();) {
						PropertySheetPage propertySheetPage = i.next();
						if (propertySheetPage.getControl().isDisposed()) {
							i.remove();
						} else {
							propertySheetPage.refresh();
						}
					}
				}
			});
		});
	}

	public ComposedAdapterFactory getAdapterFactory() {
		return (ComposedAdapterFactory) editingDomain.getAdapterFactory();
	}

	public BasicCommandStack getCommandStack() {
		return (BasicCommandStack) editingDomain.getCommandStack();
	}

	@Override
	public EditingDomain getEditingDomain() {
		return editingDomain;
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		setSite(site);
		setInputWithNotify(input);
		setPartName(input.getName());
		site.setSelectionProvider(this);
		initEditingDomain();

		ResourcesPlugin.getWorkspace().addResourceChangeListener(componentChangeListener,
				IResourceChangeEvent.POST_CHANGE);
		ResourcesPlugin.getWorkspace().addResourceChangeListener(editorConfigChangeListerner,
				IResourceChangeEvent.POST_CHANGE);
	}

	@Override
	protected void addPages() {
		IEditorInput input = getEditorInput();

		if (!(input instanceof TableEditorInput)) {
			throw new IllegalStateException("Invalid editor input");
		}

		URI resourceURI = EditUIUtil.getURI(input);
		URI editorConfigURI = ((TableEditorInput) input).getEditorConfigURI();
		URI componentURI = ((TableEditorInput) input).getMResourceURI();

		if (componentURI == null || !componentURI.isPlatformResource()) {
			throw new IllegalStateException("Cannot load dynamic editor from component outside workspace");
		}

		try {
			// Load instance resource
			Resource resource = new DynamicSetup() //
					.withDomain(editingDomain) //
					.withComponent(componentURI) //
					.withInstance(resourceURI) //
					.setup();

			// move instances resource to the top
			//
			getEditingDomain().getResourceSet().getResources().move(0, resource);
			resource.eAdapters().add(new InitValueAdapter(getEditingDomain()));
			setPartName(resourceURI.lastSegment());

			DynamicTablePage page = new DynamicTablePage(this, "first", "Table Editor");
			page.setEditorConfigURI(editorConfigURI);
			page.setPrimaryResource(resource);
			addPage(page);
		} catch (Exception e) {
			McoreUIPlugin.log(e);
		}
	}

	@Override
	public void dispose() {
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(componentChangeListener);
		ResourcesPlugin.getWorkspace().removeResourceChangeListener(editorConfigChangeListerner);
		super.dispose();
	}

	@Override
	public <T> T getAdapter(Class<T> adapter) {
		if (getActivePageInstance() != null) {
			return getActivePageInstance().getAdapter(adapter);
		}
		return super.getAdapter(adapter);
	}

	@Override
	public void doSave(IProgressMonitor monitor) {
		final Map<Object, Object> saveOptions = new HashMap<Object, Object>();
		saveOptions.put(Resource.OPTION_SAVE_ONLY_IF_CHANGED, Resource.OPTION_SAVE_ONLY_IF_CHANGED_MEMORY_BUFFER);
		saveOptions.put(XMLResource.OPTION_ENCODING, "UTF-8");

		Object[] expandedElements = null;
		DynamicTablePage currentPage = null;
		if (getActivePageInstance() instanceof DynamicTablePage) {
			currentPage = (DynamicTablePage) getActivePageInstance();
			expandedElements = ((TreeViewer) currentPage.getViewer()).getExpandedElements();
		}

		WorkspaceModifyOperation operation = new WorkspaceModifyOperation() {
			@Override
			protected void execute(IProgressMonitor monitor)
					throws CoreException, InvocationTargetException, InterruptedException {

				ResourceSet resourceSet = getEditingDomain().getResourceSet();

				for (Resource resource : resourceSet.getResources()) {
					URI normalized = resourceSet.getURIConverter().normalize(resource.getURI());

					// we don't allow changes to ecore and editor files
					//
					if (Arrays.asList("ecore", "editorconfig").contains(normalized.fileExtension())) {
						continue;
					}

					if (resource.getURI().isPlatformResource() && !editingDomain.isReadOnly(resource)) {
						try {
							if (!resource.getContents().isEmpty()) {
								resource.save(saveOptions);
							}
						} catch (IOException e) {
							McoreUIPlugin.log(e);
							continue;
						}

						URI mResourceURI = ((TableEditorInput) getEditorInput()).getMResourceURI();

						GenerateMResources.generate(mResourceURI, resource, SubMonitor.convert(monitor, 1));
					}
				}
			}
		};

		try {
			ResourcesPlugin.getWorkspace().removeResourceChangeListener(componentChangeListener);

			new ProgressMonitorDialog(getSite().getShell()).run(true, false, operation);
			getCommandStack().saveIsDone();
			firePropertyChange(IEditorPart.PROP_DIRTY);

			if (currentPage != null && expandedElements != null) {
				currentPage.restoreExpandedElements(expandedElements);
			}

			ResourcesPlugin.getWorkspace().addResourceChangeListener(componentChangeListener,
					IResourceChangeEvent.POST_CHANGE);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void doSaveAs() {
	}

	@Override
	public boolean isSaveAsAllowed() {
		return false;
	}

	@Override
	public void addSelectionChangedListener(ISelectionChangedListener listener) {
		if (getActivePageInstance() instanceof DynamicTablePage) {
			DynamicTablePage page = (DynamicTablePage) getActivePageInstance();
			page.getViewer().addSelectionChangedListener(listener);
		}
	}

	@Override
	public ISelection getSelection() {
		if (getActivePageInstance() instanceof DynamicTablePage) {
			return ((DynamicTablePage) getActivePageInstance()).getViewer().getSelection();
		}
		return StructuredSelection.EMPTY;
	}

	@Override
	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		if (getActivePageInstance() instanceof DynamicTablePage) {
			DynamicTablePage page = (DynamicTablePage) getActivePageInstance();
			page.getViewer().removeSelectionChangedListener(listener);
		}
	}

	@Override
	public void setSelection(ISelection selection) {
		if (getActivePageInstance() instanceof DynamicTablePage) {
			DynamicTablePage page = (DynamicTablePage) getActivePageInstance();
			page.getViewer().setSelection(selection);
		}
	}

	protected IResourceChangeListener editorConfigChangeListerner = new IResourceChangeListener() {

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			try {
				event.getDelta().accept(new IResourceDeltaVisitor() {
					@Override
					public boolean visit(IResourceDelta delta) throws CoreException {
						return doVisit(delta);
					}
				});
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		private boolean doVisit(IResourceDelta delta) {
			IEditorInput input = getEditorInput();
			if (input instanceof TableEditorInput) {
				TableEditorInput tableInput = (TableEditorInput) input;
				if (delta.getResource().getType() == IResource.FILE) {
					if (delta.getKind() == IResourceDelta.CHANGED && delta.getFlags() != IResourceDelta.MARKERS) {
						handleChange(tableInput, delta.getFullPath());
					}
				}
				return true;
			}
			return false;
		}

		private void handleChange(TableEditorInput tableInput, IPath path) {
			URI deltaURI = URI.createPlatformResourceURI(path.toString(), true);
			URI editorURI = tableInput.getEditorConfigURI();

			if (deltaURI.equals(editorURI)) {
//				editingDomain.getResourceSet().eNotify(new NotificationImpl(RELOAD_CONFIG, null, null) {
//					public Object getNotifier() {
//						return editingDomain.getResourceSet();
//					};
//				});
			}
		}
	};

	protected IResourceChangeListener componentChangeListener = new IResourceChangeListener() {

		@Override
		public void resourceChanged(IResourceChangeEvent event) {
			try {
				event.getDelta().accept(new IResourceDeltaVisitor() {
					@Override
					public boolean visit(IResourceDelta delta) throws CoreException {
						return doVisit(delta);
					}
				});
			} catch (CoreException e) {
				e.printStackTrace();
			}
		}

		private boolean doVisit(IResourceDelta delta) {
			IEditorInput input = getEditorInput();
			if (input instanceof TableEditorInput) {
				TableEditorInput tableInput = (TableEditorInput) input;
				if (delta.getResource().getType() == IResource.FILE) {
					if (delta.getKind() == IResourceDelta.CHANGED && delta.getFlags() != IResourceDelta.MARKERS) {
						handleChange(tableInput, delta.getFullPath());
					}
				}
				return true;
			}
			return false;
		}

		private void handleChange(TableEditorInput tableInput, IPath path) {
			URI deltaURI = URI.createPlatformResourceURI(path.toString(), true);
			URI componentURI = tableInput.getMResourceURI().trimFragment();

			if (deltaURI.equals(componentURI)) {
//				MComponent component = getComponent(componentURI);
//				if (component != null) {
//					try {
//						new Mcore2Ecore().transform(getEditingDomain().getResourceSet(), component);
//					} catch (CoreException e) {
//						e.printStackTrace();
//					}
//					List<EPackage> changes = McoreEcoreUtil.collectEPackages(component);
//					handleChangedPackages(changes);
//					reloadResource();
//				}
			}
		}

//		private void reloadResource() {
//			Resource main = getEditingDomain().getResourceSet().getResources().get(0);
//			if (main.isLoaded()) {
//				main.unload();
//				try {
//					main.load(null);
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//			Notification n = new NotificationImpl(RELOAD_CONFIG, null, null) {
//				@Override
//				public Object getNotifier() {
//					return getEditingDomain().getResourceSet();
//				}
//			};
//			getEditingDomain().getResourceSet().eNotify(n);
//		}
//
//		private void handleChangedPackages(List<EPackage> changes) {
//			ResourceSet rs = getEditingDomain().getResourceSet();
//			for (EPackage changed: changes) {
//				Resource r = rs.getResource(URI.createURI(changed.getNsURI()), false);
//				if (r != null) {
//					r.getContents().clear();
//					r.getContents().add(changed);
//					dynamicAdapter.adapt(getEditingDomain().getResourceSet(), changed);
//				}
//			}
//		}

	};

}
