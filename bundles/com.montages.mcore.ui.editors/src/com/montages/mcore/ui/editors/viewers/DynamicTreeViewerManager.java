package com.montages.mcore.ui.editors.viewers;

import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.TreeViewer;
import org.xocl.editorconfig.internal.ui.actions.ReloadEditorConfigAction;
import org.xocl.editorconfig.ui.TreeViewerManager;

public class DynamicTreeViewerManager extends TreeViewerManager {

	private final ResourceSet resourceSet;

	public DynamicTreeViewerManager(TreeViewer viewer, AdapterFactoryEditingDomain editingDomain) {
		super(viewer, editingDomain);
		this.resourceSet = editingDomain.getResourceSet();
	}

	@Override
	public ResourceSet getResourceSet() {
		return resourceSet;
	}
	
	@Override
	public ResourceSet getEditorConfigResourceSet() {
		return getResourceSet();
	}

	@Override
	protected List<IAction> createActions() {
		return Arrays.<IAction>asList(new ReloadEditorConfigAction(this));
	}
}
