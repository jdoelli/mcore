/**
 */

package com.montages.acore;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AFolder</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.acore.AcorePackage#getAFolder()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Folder'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aUriDerive='let e1: String = if aContainingFolder.oclIsUndefined()\n  then null\n  else aContainingFolder.aUri\nendif.concat(\'/\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif\n'"
 * @generated
 */

public interface AFolder extends AAbstractFolder {
} // AFolder
