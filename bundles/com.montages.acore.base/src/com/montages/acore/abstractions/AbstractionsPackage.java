/**
 */
package com.montages.acore.abstractions;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.abstractions.AbstractionsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore'"
 * @generated
 */
public interface AbstractionsPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "abstractions";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Abstractions";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.abstractions";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	AbstractionsPackage eINSTANCE = com.montages.acore.abstractions.impl.AbstractionsPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.AElementImpl <em>AElement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.AElementImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAElement()
	 * @generated
	 */
	int AELEMENT = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ALABEL = 0;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AKIND_BASE = 1;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ARENDERED_KIND = 2;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__ACONTAINING_COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_PACKAGE_URI = 4;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_CLASSIFIER_NAME = 5;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_FEATURE_NAME = 6;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_PACKAGE = 7;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_CLASSIFIER = 8;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_FEATURE = 9;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT__AT_CORE_ASTRING_CLASS = 10;

	/**
	 * The number of structural features of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_FEATURE_COUNT = 11;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___AINDENT_LEVEL = 0;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___AINDENTATION_SPACES = 1;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___AINDENTATION_SPACES__INTEGER = 2;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ASTRING_OR_MISSING__STRING = 3;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ASTRING_IS_EMPTY__STRING = 4;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = 5;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = 6;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___APACKAGE_FROM_URI__STRING = 7;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = 8;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = 9;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_ASTRING_CLASS = 10;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AREAL_CLASS = 11;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AINTEGER_CLASS = 12;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT___ACORE_AOBJECT_CLASS = 13;

	/**
	 * The number of operations of the '<em>AElement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AELEMENT_OPERATION_COUNT = 14;

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.ANamedImpl <em>ANamed</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.ANamedImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getANamed()
	 * @generated
	 */
	int ANAMED = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ALABEL = AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AKIND_BASE = AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ARENDERED_KIND = AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ACONTAINING_COMPONENT = AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_PACKAGE_URI = AELEMENT__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_CLASSIFIER_NAME = AELEMENT__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_FEATURE_NAME = AELEMENT__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_PACKAGE = AELEMENT__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_CLASSIFIER = AELEMENT__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_FEATURE = AELEMENT__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AT_CORE_ASTRING_CLASS = AELEMENT__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ANAME = AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__AUNDEFINED_NAME_CONSTANT = AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED__ABUSINESS_NAME = AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_FEATURE_COUNT = AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___AINDENT_LEVEL = AELEMENT___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___AINDENTATION_SPACES = AELEMENT___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___AINDENTATION_SPACES__INTEGER = AELEMENT___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ASTRING_OR_MISSING__STRING = AELEMENT___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ASTRING_IS_EMPTY__STRING = AELEMENT___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___APACKAGE_FROM_URI__STRING = AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_ASTRING_CLASS = AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AREAL_CLASS = AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AINTEGER_CLASS = AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED___ACORE_AOBJECT_CLASS = AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>ANamed</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ANAMED_OPERATION_COUNT = AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.AAnnotatableImpl <em>AAnnotatable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.AAnnotatableImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAAnnotatable()
	 * @generated
	 */
	int AANNOTATABLE = 2;

	/**
	 * The feature id for the '<em><b>AAnnotation</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATABLE__AANNOTATION = 0;

	/**
	 * The number of structural features of the '<em>AAnnotatable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATABLE_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>AAnnotatable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATABLE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.AAnnotationImpl <em>AAnnotation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.AAnnotationImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAAnnotation()
	 * @generated
	 */
	int AANNOTATION = 3;

	/**
	 * The feature id for the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATION__AANNOTATED = 0;

	/**
	 * The feature id for the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATION__AANNOTATING = 1;

	/**
	 * The number of structural features of the '<em>AAnnotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATION_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>AAnnotation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AANNOTATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.ATypedImpl <em>ATyped</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.ATypedImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getATyped()
	 * @generated
	 */
	int ATYPED = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ALABEL = AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AKIND_BASE = AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ARENDERED_KIND = AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ACONTAINING_COMPONENT = AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_PACKAGE_URI = AELEMENT__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_CLASSIFIER_NAME = AELEMENT__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_FEATURE_NAME = AELEMENT__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_PACKAGE = AELEMENT__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_CLASSIFIER = AELEMENT__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_FEATURE = AELEMENT__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AT_CORE_ASTRING_CLASS = AELEMENT__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ACLASSIFIER = AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AMANDATORY = AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ASINGULAR = AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ATYPE_LABEL = AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__AUNDEFINED_TYPE_CONSTANT = AELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED__ASIMPLE_TYPE = AELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>ATyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED_FEATURE_COUNT = AELEMENT_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___AINDENT_LEVEL = AELEMENT___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___AINDENTATION_SPACES = AELEMENT___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___AINDENTATION_SPACES__INTEGER = AELEMENT___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ASTRING_OR_MISSING__STRING = AELEMENT___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ASTRING_IS_EMPTY__STRING = AELEMENT___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___APACKAGE_FROM_URI__STRING = AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_ASTRING_CLASS = AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AREAL_CLASS = AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AINTEGER_CLASS = AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ACORE_AOBJECT_CLASS = AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>ATyped</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATYPED_OPERATION_COUNT = AELEMENT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link com.montages.acore.abstractions.impl.AVariableImpl <em>AVariable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.abstractions.impl.AVariableImpl
	 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAVariable()
	 * @generated
	 */
	int AVARIABLE = 5;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ALABEL = ANAMED__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AKIND_BASE = ANAMED__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ARENDERED_KIND = ANAMED__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ACONTAINING_COMPONENT = ANAMED__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_PACKAGE_URI = ANAMED__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_CLASSIFIER_NAME = ANAMED__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_FEATURE_NAME = ANAMED__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_PACKAGE = ANAMED__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_CLASSIFIER = ANAMED__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_FEATURE = ANAMED__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AT_CORE_ASTRING_CLASS = ANAMED__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ANAME = ANAMED__ANAME;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AUNDEFINED_NAME_CONSTANT = ANAMED__AUNDEFINED_NAME_CONSTANT;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ABUSINESS_NAME = ANAMED__ABUSINESS_NAME;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ACLASSIFIER = ANAMED_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AMANDATORY = ANAMED_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ASINGULAR = ANAMED_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ATYPE_LABEL = ANAMED_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__AUNDEFINED_TYPE_CONSTANT = ANAMED_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>ASimple Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE__ASIMPLE_TYPE = ANAMED_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>AVariable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE_FEATURE_COUNT = ANAMED_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___AINDENT_LEVEL = ANAMED___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___AINDENTATION_SPACES = ANAMED___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___AINDENTATION_SPACES__INTEGER = ANAMED___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ASTRING_OR_MISSING__STRING = ANAMED___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ASTRING_IS_EMPTY__STRING = ANAMED___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = ANAMED___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___APACKAGE_FROM_URI__STRING = ANAMED___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = ANAMED___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = ANAMED___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ACORE_ASTRING_CLASS = ANAMED___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ACORE_AREAL_CLASS = ANAMED___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ACORE_AINTEGER_CLASS = ANAMED___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ACORE_AOBJECT_CLASS = ANAMED___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AType As Ocl</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = ANAMED_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>AVariable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVARIABLE_OPERATION_COUNT = ANAMED_OPERATION_COUNT + 1;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ANamed</em>'.
	 * @see com.montages.acore.abstractions.ANamed
	 * @generated
	 */
	EClass getANamed();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ANamed#getAName <em>AName</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AName</em>'.
	 * @see com.montages.acore.abstractions.ANamed#getAName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ANamed#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Name Constant</em>'.
	 * @see com.montages.acore.abstractions.ANamed#getAUndefinedNameConstant()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_AUndefinedNameConstant();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ANamed#getABusinessName <em>ABusiness Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ABusiness Name</em>'.
	 * @see com.montages.acore.abstractions.ANamed#getABusinessName()
	 * @see #getANamed()
	 * @generated
	 */
	EAttribute getANamed_ABusinessName();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AElement</em>'.
	 * @see com.montages.acore.abstractions.AElement
	 * @generated
	 */
	EClass getAElement();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getALabel <em>ALabel</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ALabel</em>'.
	 * @see com.montages.acore.abstractions.AElement#getALabel()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ALabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getAKindBase <em>AKind Base</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AKind Base</em>'.
	 * @see com.montages.acore.abstractions.AElement#getAKindBase()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_AKindBase();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getARenderedKind <em>ARendered Kind</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ARendered Kind</em>'.
	 * @see com.montages.acore.abstractions.AElement#getARenderedKind()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ARenderedKind();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AElement#getAContainingComponent <em>AContaining Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Component</em>'.
	 * @see com.montages.acore.abstractions.AElement#getAContainingComponent()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_AContainingComponent();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getATPackageUri <em>AT Package Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AT Package Uri</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATPackageUri()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ATPackageUri();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getATClassifierName <em>AT Classifier Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AT Classifier Name</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATClassifierName()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ATClassifierName();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.AElement#getATFeatureName <em>AT Feature Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AT Feature Name</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATFeatureName()
	 * @see #getAElement()
	 * @generated
	 */
	EAttribute getAElement_ATFeatureName();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AElement#getATPackage <em>AT Package</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AT Package</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATPackage()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_ATPackage();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AElement#getATClassifier <em>AT Classifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AT Classifier</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATClassifier()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_ATClassifier();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AElement#getATFeature <em>AT Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AT Feature</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATFeature()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_ATFeature();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AElement#getATCoreAStringClass <em>AT Core AString Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AT Core AString Class</em>'.
	 * @see com.montages.acore.abstractions.AElement#getATCoreAStringClass()
	 * @see #getAElement()
	 * @generated
	 */
	EReference getAElement_ATCoreAStringClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aIndentLevel() <em>AIndent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AIndent Level</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aIndentLevel()
	 * @generated
	 */
	EOperation getAElement__AIndentLevel();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aIndentationSpaces() <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AIndentation Spaces</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aIndentationSpaces()
	 * @generated
	 */
	EOperation getAElement__AIndentationSpaces();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aIndentationSpaces(java.lang.Integer) <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AIndentation Spaces</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aIndentationSpaces(java.lang.Integer)
	 * @generated
	 */
	EOperation getAElement__AIndentationSpaces__Integer();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aStringOrMissing(java.lang.String) <em>AString Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AString Or Missing</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aStringOrMissing(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AStringOrMissing__String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aStringIsEmpty(java.lang.String) <em>AString Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AString Is Empty</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aStringIsEmpty(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AStringIsEmpty__String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aListOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String) <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AList Of String To String With Separator</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aListOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AListOfStringToStringWithSeparator__EList_String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aListOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList) <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AList Of String To String With Separator</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aListOfStringToStringWithSeparator(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAElement__AListOfStringToStringWithSeparator__EList();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aPackageFromUri(java.lang.String) <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>APackage From Uri</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aPackageFromUri(java.lang.String)
	 * @generated
	 */
	EOperation getAElement__APackageFromUri__String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aClassifierFromUriAndName(java.lang.String, java.lang.String) <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AClassifier From Uri And Name</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aClassifierFromUriAndName(java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AClassifierFromUriAndName__String_String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aFeatureFromUriAndNames(java.lang.String, java.lang.String, java.lang.String) <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AFeature From Uri And Names</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aFeatureFromUriAndNames(java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getAElement__AFeatureFromUriAndNames__String_String_String();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aCoreAStringClass() <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AString Class</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aCoreAStringClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAStringClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aCoreARealClass() <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AReal Class</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aCoreARealClass()
	 * @generated
	 */
	EOperation getAElement__ACoreARealClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aCoreAIntegerClass() <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AInteger Class</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aCoreAIntegerClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAIntegerClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.AElement#aCoreAObjectClass() <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ACore AObject Class</em>' operation.
	 * @see com.montages.acore.abstractions.AElement#aCoreAObjectClass()
	 * @generated
	 */
	EOperation getAElement__ACoreAObjectClass();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.AAnnotatable <em>AAnnotatable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AAnnotatable</em>'.
	 * @see com.montages.acore.abstractions.AAnnotatable
	 * @generated
	 */
	EClass getAAnnotatable();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.abstractions.AAnnotatable#getAAnnotation <em>AAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAnnotation</em>'.
	 * @see com.montages.acore.abstractions.AAnnotatable#getAAnnotation()
	 * @see #getAAnnotatable()
	 * @generated
	 */
	EReference getAAnnotatable_AAnnotation();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.AAnnotation <em>AAnnotation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AAnnotation</em>'.
	 * @see com.montages.acore.abstractions.AAnnotation
	 * @generated
	 */
	EClass getAAnnotation();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AAnnotation#getAAnnotated <em>AAnnotated</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotated</em>'.
	 * @see com.montages.acore.abstractions.AAnnotation#getAAnnotated()
	 * @see #getAAnnotation()
	 * @generated
	 */
	EReference getAAnnotation_AAnnotated();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.AAnnotation#getAAnnotating <em>AAnnotating</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AAnnotating</em>'.
	 * @see com.montages.acore.abstractions.AAnnotation#getAAnnotating()
	 * @see #getAAnnotation()
	 * @generated
	 */
	EReference getAAnnotation_AAnnotating();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.ATyped <em>ATyped</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ATyped</em>'.
	 * @see com.montages.acore.abstractions.ATyped
	 * @generated
	 */
	EClass getATyped();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.abstractions.ATyped#getAClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClassifier</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getAClassifier()
	 * @see #getATyped()
	 * @generated
	 */
	EReference getATyped_AClassifier();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ATyped#getAMandatory <em>AMandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AMandatory</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getAMandatory()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_AMandatory();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ATyped#getASingular <em>ASingular</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASingular</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getASingular()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_ASingular();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ATyped#getATypeLabel <em>AType Label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AType Label</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getATypeLabel()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_ATypeLabel();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ATyped#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUndefined Type Constant</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getAUndefinedTypeConstant()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_AUndefinedTypeConstant();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.abstractions.ATyped#getASimpleType <em>ASimple Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ASimple Type</em>'.
	 * @see com.montages.acore.abstractions.ATyped#getASimpleType()
	 * @see #getATyped()
	 * @generated
	 */
	EAttribute getATyped_ASimpleType();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.abstractions.ATyped#aTypeAsOcl(com.montages.acore.APackage, com.montages.acore.classifiers.AClassifier, com.montages.acore.classifiers.ASimpleType, java.lang.Boolean) <em>AType As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AType As Ocl</em>' operation.
	 * @see com.montages.acore.abstractions.ATyped#aTypeAsOcl(com.montages.acore.APackage, com.montages.acore.classifiers.AClassifier, com.montages.acore.classifiers.ASimpleType, java.lang.Boolean)
	 * @generated
	 */
	EOperation getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.abstractions.AVariable <em>AVariable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AVariable</em>'.
	 * @see com.montages.acore.abstractions.AVariable
	 * @generated
	 */
	EClass getAVariable();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	AbstractionsFactory getAbstractionsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.ANamedImpl <em>ANamed</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.ANamedImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getANamed()
		 * @generated
		 */
		EClass ANAMED = eINSTANCE.getANamed();

		/**
		 * The meta object literal for the '<em><b>AName</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ANAME = eINSTANCE.getANamed_AName();

		/**
		 * The meta object literal for the '<em><b>AUndefined Name Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__AUNDEFINED_NAME_CONSTANT = eINSTANCE.getANamed_AUndefinedNameConstant();

		/**
		 * The meta object literal for the '<em><b>ABusiness Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ANAMED__ABUSINESS_NAME = eINSTANCE.getANamed_ABusinessName();

		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.AElementImpl <em>AElement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.AElementImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAElement()
		 * @generated
		 */
		EClass AELEMENT = eINSTANCE.getAElement();

		/**
		 * The meta object literal for the '<em><b>ALabel</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ALABEL = eINSTANCE.getAElement_ALabel();

		/**
		 * The meta object literal for the '<em><b>AKind Base</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AKIND_BASE = eINSTANCE.getAElement_AKindBase();

		/**
		 * The meta object literal for the '<em><b>ARendered Kind</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__ARENDERED_KIND = eINSTANCE.getAElement_ARenderedKind();

		/**
		 * The meta object literal for the '<em><b>AContaining Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__ACONTAINING_COMPONENT = eINSTANCE.getAElement_AContainingComponent();

		/**
		 * The meta object literal for the '<em><b>AT Package Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AT_PACKAGE_URI = eINSTANCE.getAElement_ATPackageUri();

		/**
		 * The meta object literal for the '<em><b>AT Classifier Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AT_CLASSIFIER_NAME = eINSTANCE.getAElement_ATClassifierName();

		/**
		 * The meta object literal for the '<em><b>AT Feature Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AELEMENT__AT_FEATURE_NAME = eINSTANCE.getAElement_ATFeatureName();

		/**
		 * The meta object literal for the '<em><b>AT Package</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__AT_PACKAGE = eINSTANCE.getAElement_ATPackage();

		/**
		 * The meta object literal for the '<em><b>AT Classifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__AT_CLASSIFIER = eINSTANCE.getAElement_ATClassifier();

		/**
		 * The meta object literal for the '<em><b>AT Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__AT_FEATURE = eINSTANCE.getAElement_ATFeature();

		/**
		 * The meta object literal for the '<em><b>AT Core AString Class</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AELEMENT__AT_CORE_ASTRING_CLASS = eINSTANCE.getAElement_ATCoreAStringClass();

		/**
		 * The meta object literal for the '<em><b>AIndent Level</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___AINDENT_LEVEL = eINSTANCE.getAElement__AIndentLevel();

		/**
		 * The meta object literal for the '<em><b>AIndentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___AINDENTATION_SPACES = eINSTANCE.getAElement__AIndentationSpaces();

		/**
		 * The meta object literal for the '<em><b>AIndentation Spaces</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___AINDENTATION_SPACES__INTEGER = eINSTANCE.getAElement__AIndentationSpaces__Integer();

		/**
		 * The meta object literal for the '<em><b>AString Or Missing</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ASTRING_OR_MISSING__STRING = eINSTANCE.getAElement__AStringOrMissing__String();

		/**
		 * The meta object literal for the '<em><b>AString Is Empty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ASTRING_IS_EMPTY__STRING = eINSTANCE.getAElement__AStringIsEmpty__String();

		/**
		 * The meta object literal for the '<em><b>AList Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = eINSTANCE
				.getAElement__AListOfStringToStringWithSeparator__EList_String();

		/**
		 * The meta object literal for the '<em><b>AList Of String To String With Separator</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = eINSTANCE
				.getAElement__AListOfStringToStringWithSeparator__EList();

		/**
		 * The meta object literal for the '<em><b>APackage From Uri</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___APACKAGE_FROM_URI__STRING = eINSTANCE.getAElement__APackageFromUri__String();

		/**
		 * The meta object literal for the '<em><b>AClassifier From Uri And Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = eINSTANCE
				.getAElement__AClassifierFromUriAndName__String_String();

		/**
		 * The meta object literal for the '<em><b>AFeature From Uri And Names</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = eINSTANCE
				.getAElement__AFeatureFromUriAndNames__String_String_String();

		/**
		 * The meta object literal for the '<em><b>ACore AString Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_ASTRING_CLASS = eINSTANCE.getAElement__ACoreAStringClass();

		/**
		 * The meta object literal for the '<em><b>ACore AReal Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AREAL_CLASS = eINSTANCE.getAElement__ACoreARealClass();

		/**
		 * The meta object literal for the '<em><b>ACore AInteger Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AINTEGER_CLASS = eINSTANCE.getAElement__ACoreAIntegerClass();

		/**
		 * The meta object literal for the '<em><b>ACore AObject Class</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AELEMENT___ACORE_AOBJECT_CLASS = eINSTANCE.getAElement__ACoreAObjectClass();

		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.AAnnotatableImpl <em>AAnnotatable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.AAnnotatableImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAAnnotatable()
		 * @generated
		 */
		EClass AANNOTATABLE = eINSTANCE.getAAnnotatable();

		/**
		 * The meta object literal for the '<em><b>AAnnotation</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AANNOTATABLE__AANNOTATION = eINSTANCE.getAAnnotatable_AAnnotation();

		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.AAnnotationImpl <em>AAnnotation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.AAnnotationImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAAnnotation()
		 * @generated
		 */
		EClass AANNOTATION = eINSTANCE.getAAnnotation();

		/**
		 * The meta object literal for the '<em><b>AAnnotated</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AANNOTATION__AANNOTATED = eINSTANCE.getAAnnotation_AAnnotated();

		/**
		 * The meta object literal for the '<em><b>AAnnotating</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AANNOTATION__AANNOTATING = eINSTANCE.getAAnnotation_AAnnotating();

		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.ATypedImpl <em>ATyped</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.ATypedImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getATyped()
		 * @generated
		 */
		EClass ATYPED = eINSTANCE.getATyped();

		/**
		 * The meta object literal for the '<em><b>AClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATYPED__ACLASSIFIER = eINSTANCE.getATyped_AClassifier();

		/**
		 * The meta object literal for the '<em><b>AMandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__AMANDATORY = eINSTANCE.getATyped_AMandatory();

		/**
		 * The meta object literal for the '<em><b>ASingular</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__ASINGULAR = eINSTANCE.getATyped_ASingular();

		/**
		 * The meta object literal for the '<em><b>AType Label</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__ATYPE_LABEL = eINSTANCE.getATyped_ATypeLabel();

		/**
		 * The meta object literal for the '<em><b>AUndefined Type Constant</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__AUNDEFINED_TYPE_CONSTANT = eINSTANCE.getATyped_AUndefinedTypeConstant();

		/**
		 * The meta object literal for the '<em><b>ASimple Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATYPED__ASIMPLE_TYPE = eINSTANCE.getATyped_ASimpleType();

		/**
		 * The meta object literal for the '<em><b>AType As Ocl</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN = eINSTANCE
				.getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean();

		/**
		 * The meta object literal for the '{@link com.montages.acore.abstractions.impl.AVariableImpl <em>AVariable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.abstractions.impl.AVariableImpl
		 * @see com.montages.acore.abstractions.impl.AbstractionsPackageImpl#getAVariable()
		 * @generated
		 */
		EClass AVARIABLE = eINSTANCE.getAVariable();

	}

} //AbstractionsPackage
