/**
 */

package com.montages.acore.abstractions;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AAnnotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.AAnnotation#getAAnnotated <em>AAnnotated</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.AAnnotation#getAAnnotating <em>AAnnotating</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getAAnnotation()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Annotation'"
 * @generated
 */

public interface AAnnotation extends EObject {
	/**
	 * Returns the value of the '<em><b>AAnnotated</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotated</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotated</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAAnnotation_AAnnotated()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotated'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	AAnnotatable getAAnnotated();

	/**
	 * Returns the value of the '<em><b>AAnnotating</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotating</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotating</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getAAnnotation_AAnnotating()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotating'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	AAnnotation getAAnnotating();

} // AAnnotation
