/**
 */

package com.montages.acore.abstractions.impl;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AbstractionsPackage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AAnnotation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.impl.AAnnotationImpl#getAAnnotated <em>AAnnotated</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AAnnotationImpl#getAAnnotating <em>AAnnotating</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AAnnotationImpl extends MinimalEObjectImpl.Container implements AAnnotation {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotated <em>AAnnotated</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotated
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotatedDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAnnotating <em>AAnnotating</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAnnotating
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAnnotatingDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AAnnotationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbstractionsPackage.Literals.AANNOTATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotatable getAAnnotated() {
		AAnnotatable aAnnotated = basicGetAAnnotated();
		return aAnnotated != null && aAnnotated.eIsProxy() ? (AAnnotatable) eResolveProxy((InternalEObject) aAnnotated)
				: aAnnotated;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotatable basicGetAAnnotated() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AANNOTATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATED;

		if (aAnnotatedDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotatedDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatedDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query,
					AbstractionsPackage.Literals.AANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAnnotatable result = (AAnnotatable) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotation getAAnnotating() {
		AAnnotation aAnnotating = basicGetAAnnotating();
		return aAnnotating != null && aAnnotating.eIsProxy()
				? (AAnnotation) eResolveProxy((InternalEObject) aAnnotating) : aAnnotating;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AAnnotation basicGetAAnnotating() {
		/**
		 * @OCL null
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AANNOTATION;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AANNOTATION__AANNOTATING;

		if (aAnnotatingDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAnnotatingDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AANNOTATION, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAnnotatingDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query,
					AbstractionsPackage.Literals.AANNOTATION, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AAnnotation result = (AAnnotation) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbstractionsPackage.AANNOTATION__AANNOTATED:
			if (resolve)
				return getAAnnotated();
			return basicGetAAnnotated();
		case AbstractionsPackage.AANNOTATION__AANNOTATING:
			if (resolve)
				return getAAnnotating();
			return basicGetAAnnotating();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbstractionsPackage.AANNOTATION__AANNOTATED:
			return basicGetAAnnotated() != null;
		case AbstractionsPackage.AANNOTATION__AANNOTATING:
			return basicGetAAnnotating() != null;
		}
		return super.eIsSet(featureID);
	}

} //AAnnotationImpl
