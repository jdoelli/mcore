/**
 */

package com.montages.acore.abstractions.impl;

import com.montages.acore.APackage;

import com.montages.acore.abstractions.ATyped;
import com.montages.acore.abstractions.AVariable;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AVariable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getAMandatory <em>AMandatory</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getASingular <em>ASingular</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getATypeLabel <em>AType Label</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.impl.AVariableImpl#getASimpleType <em>ASimple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AVariableImpl extends ANamedImpl implements AVariable {
	/**
	 * The default value of the '{@link #getAMandatory() <em>AMandatory</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AMANDATORY_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASingular() <em>ASingular</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean ASINGULAR_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getATypeLabel() <em>AType Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ATYPE_LABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedTypeConstant() <em>AUndefined Type Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_TYPE_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getASimpleType() <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType()
	 * @generated
	 * @ordered
	 */
	protected static final ASimpleType ASIMPLE_TYPE_EDEFAULT = ASimpleType.NONE;

	/**
	 * The cached value of the '{@link #getASimpleType() <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASimpleType()
	 * @generated
	 * @ordered
	 */
	protected ASimpleType aSimpleType = ASIMPLE_TYPE_EDEFAULT;

	/**
	 * This is true if the ASimple Type attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aSimpleTypeESet;

	/**
	 * The parsed OCL expression for the body of the '{@link #aTypeAsOcl <em>AType As Ocl</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aTypeAsOcl
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAMandatory <em>AMandatory</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAMandatory
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aMandatoryDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASingular <em>ASingular</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASingular
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSingularDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATypeLabel <em>AType Label</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATypeLabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTypeLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedTypeConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedTypeConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AbstractionsPackage.Literals.AVARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getAClassifier() {
		AClassifier aClassifier = basicGetAClassifier();
		return aClassifier != null && aClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aClassifier) : aClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetAClassifier() {
		/**
		 * @OCL let nl: acore::classifiers::AClassifier = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AVARIABLE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAMandatory() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AVARIABLE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__AMANDATORY;

		if (aMandatoryDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aMandatoryDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aMandatoryDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getASingular() {
		/**
		 * @OCL false
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AVARIABLE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__ASINGULAR;

		if (aSingularDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSingularDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSingularDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATypeLabel() {
		/**
		 * @OCL if (let e0: Boolean = aClassifier = null in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in 
		if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif
		else (let e0: String = if aClassifier.oclIsUndefined()
		then null
		else aClassifier.aName
		endif.concat('*') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AVARIABLE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__ATYPE_LABEL;

		if (aTypeLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTypeLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTypeLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedTypeConstant() {
		/**
		 * @OCL '<A Type Is Undefined>'
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AbstractionsPackage.Literals.AVARIABLE;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT;

		if (aUndefinedTypeConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedTypeConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedTypeConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASimpleType getASimpleType() {
		return aSimpleType;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setASimpleType(ASimpleType newASimpleType) {
		ASimpleType oldASimpleType = aSimpleType;
		aSimpleType = newASimpleType == null ? ASIMPLE_TYPE_EDEFAULT : newASimpleType;
		boolean oldASimpleTypeESet = aSimpleTypeESet;
		aSimpleTypeESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE,
					oldASimpleType, aSimpleType, !oldASimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetASimpleType() {
		ASimpleType oldASimpleType = aSimpleType;
		boolean oldASimpleTypeESet = aSimpleTypeESet;
		aSimpleType = ASIMPLE_TYPE_EDEFAULT;
		aSimpleTypeESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET, AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE,
					oldASimpleType, ASIMPLE_TYPE_EDEFAULT, oldASimpleTypeESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetASimpleType() {
		return aSimpleTypeESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aTypeAsOcl(APackage aOclPackage, AClassifier classifier, ASimpleType simpleType, Boolean aSingular) {

		/**
		 * @OCL if  aClassifier.oclIsUndefined() and (aSimpleType = acore::classifiers::ASimpleType::None)  
		then 'MISSING TYPE' 
		else let t: String = 
		if  aSimpleType<> acore::classifiers::ASimpleType::None
		then 
		if aSimpleType= acore::classifiers::ASimpleType::Real  then 'Real' 
		else if aSimpleType= acore::classifiers::ASimpleType::Object then 'ecore::EObject'
		else if aSimpleType= acore::classifiers::ASimpleType::Annotation then 'ecore::EAnnotation'
		else if aSimpleType= acore::classifiers::ASimpleType::Attribute then 'ecore::EAttribute'
		else if aSimpleType= acore::classifiers::ASimpleType::Class then 'ecore::EClass'
		else if aSimpleType= acore::classifiers::ASimpleType::Classifier then 'ecore::EClassifier'
		else if aSimpleType= acore::classifiers::ASimpleType::DataType then 'ecore::EDataType'
		else if aSimpleType= acore::classifiers::ASimpleType::Enumeration then 'ecore::Enumeration'
		else if aSimpleType= acore::classifiers::ASimpleType::Feature then 'ecore::EStructuralFeature'
		else if aSimpleType= acore::classifiers::ASimpleType::Literal then 'ecore::EEnumLiteral'
		else if aSimpleType= acore::classifiers::ASimpleType::KeyValue then 'ecore::EStringToStringMapEntry'
		else if aSimpleType= acore::classifiers::ASimpleType::NamedElement then 'ecore::NamedElement'
		else if aSimpleType= acore::classifiers::ASimpleType::Operation then 'ecore::EOperation'
		else if aSimpleType= acore::classifiers::ASimpleType::Package then 'ecore::EPackage'
		else if aSimpleType= acore::classifiers::ASimpleType::Parameter then 'ecore::EParameter'
		else if aSimpleType= acore::classifiers::ASimpleType::Reference then 'ecore::EReference'
		else if aSimpleType=  acore::classifiers::ASimpleType::TypedElement then 'ecore::ETypedElement'
		else if aSimpleType =  acore::classifiers::ASimpleType::Date then 'ecore::EDate' 
		else aSimpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif
		else 
		/* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified 
		let pkg: String = 
		if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat('::')
		  else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then '' else mClassifier.containingPackage.eName.allLowerCase().concat('::') endif 
		endif
		in pkg.concat(mClassifier.eName) 
		NEW Create string with all package qualifiers *\/
		let pkg:String=
		if aClassifier.aContainingPackage   =null then ''
		else aClassifier.aContainingPackage.aName.concat('::' ) endif                            in
		pkg.concat(aClassifier.aName)
		endif
		in 
		
		if  aSingular.oclIsUndefined() then 'MISSING ARITY INFO' else 
		if aSingular=true then t else 'OrderedSet('.concat(t).concat(') ') endif
		endif
		endif
		
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.ATYPED);
		EOperation eOperation = AbstractionsPackage.Literals.ATYPED.getEOperations().get(0);
		if (aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL = helper
						.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, body, helper.getProblems(),
						AbstractionsPackage.Literals.AVARIABLE, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(
				aTypeAsOclacoreAPackageclassifiersAClassifierclassifiersASimpleTypeecoreEBooleanObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, AbstractionsPackage.Literals.AVARIABLE,
					eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("aOclPackage", aOclPackage);

			evalEnv.add("classifier", classifier);

			evalEnv.add("simpleType", simpleType);

			evalEnv.add("aSingular", aSingular);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AbstractionsPackage.AVARIABLE__ACLASSIFIER:
			if (resolve)
				return getAClassifier();
			return basicGetAClassifier();
		case AbstractionsPackage.AVARIABLE__AMANDATORY:
			return getAMandatory();
		case AbstractionsPackage.AVARIABLE__ASINGULAR:
			return getASingular();
		case AbstractionsPackage.AVARIABLE__ATYPE_LABEL:
			return getATypeLabel();
		case AbstractionsPackage.AVARIABLE__AUNDEFINED_TYPE_CONSTANT:
			return getAUndefinedTypeConstant();
		case AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE:
			return getASimpleType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE:
			setASimpleType((ASimpleType) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE:
			unsetASimpleType();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AbstractionsPackage.AVARIABLE__ACLASSIFIER:
			return basicGetAClassifier() != null;
		case AbstractionsPackage.AVARIABLE__AMANDATORY:
			return AMANDATORY_EDEFAULT == null ? getAMandatory() != null : !AMANDATORY_EDEFAULT.equals(getAMandatory());
		case AbstractionsPackage.AVARIABLE__ASINGULAR:
			return ASINGULAR_EDEFAULT == null ? getASingular() != null : !ASINGULAR_EDEFAULT.equals(getASingular());
		case AbstractionsPackage.AVARIABLE__ATYPE_LABEL:
			return ATYPE_LABEL_EDEFAULT == null ? getATypeLabel() != null
					: !ATYPE_LABEL_EDEFAULT.equals(getATypeLabel());
		case AbstractionsPackage.AVARIABLE__AUNDEFINED_TYPE_CONSTANT:
			return AUNDEFINED_TYPE_CONSTANT_EDEFAULT == null ? getAUndefinedTypeConstant() != null
					: !AUNDEFINED_TYPE_CONSTANT_EDEFAULT.equals(getAUndefinedTypeConstant());
		case AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE:
			return isSetASimpleType();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == ATyped.class) {
			switch (derivedFeatureID) {
			case AbstractionsPackage.AVARIABLE__ACLASSIFIER:
				return AbstractionsPackage.ATYPED__ACLASSIFIER;
			case AbstractionsPackage.AVARIABLE__AMANDATORY:
				return AbstractionsPackage.ATYPED__AMANDATORY;
			case AbstractionsPackage.AVARIABLE__ASINGULAR:
				return AbstractionsPackage.ATYPED__ASINGULAR;
			case AbstractionsPackage.AVARIABLE__ATYPE_LABEL:
				return AbstractionsPackage.ATYPED__ATYPE_LABEL;
			case AbstractionsPackage.AVARIABLE__AUNDEFINED_TYPE_CONSTANT:
				return AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT;
			case AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE:
				return AbstractionsPackage.ATYPED__ASIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == ATyped.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ATYPED__ACLASSIFIER:
				return AbstractionsPackage.AVARIABLE__ACLASSIFIER;
			case AbstractionsPackage.ATYPED__AMANDATORY:
				return AbstractionsPackage.AVARIABLE__AMANDATORY;
			case AbstractionsPackage.ATYPED__ASINGULAR:
				return AbstractionsPackage.AVARIABLE__ASINGULAR;
			case AbstractionsPackage.ATYPED__ATYPE_LABEL:
				return AbstractionsPackage.AVARIABLE__ATYPE_LABEL;
			case AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT:
				return AbstractionsPackage.AVARIABLE__AUNDEFINED_TYPE_CONSTANT;
			case AbstractionsPackage.ATYPED__ASIMPLE_TYPE:
				return AbstractionsPackage.AVARIABLE__ASIMPLE_TYPE;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == ATyped.class) {
			switch (baseOperationID) {
			case AbstractionsPackage.ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN:
				return AbstractionsPackage.AVARIABLE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN;
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AbstractionsPackage.AVARIABLE___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN:
			return aTypeAsOcl((APackage) arguments.get(0), (AClassifier) arguments.get(1),
					(ASimpleType) arguments.get(2), (Boolean) arguments.get(3));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (aSimpleType: ");
		if (aSimpleTypeESet)
			result.append(aSimpleType);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aLabel aTypeLabel
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public String getALabel() {
		EClass eClass = (AbstractionsPackage.Literals.AVARIABLE);
		EStructuralFeature eOverrideFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AbstractionsPackage.PLUGIN_ID, derive, helper.getProblems(),
						eClass, eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AbstractionsPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AVariableImpl
