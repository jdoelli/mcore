/**
 */
package com.montages.acore.abstractions.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.AAnnotation;
import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.ATyped;
import com.montages.acore.abstractions.AVariable;
import com.montages.acore.abstractions.AbstractionsFactory;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.annotations.impl.AnnotationsPackageImpl;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.classifiers.impl.ClassifiersPackageImpl;

import com.montages.acore.impl.AcorePackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.impl.ValuesPackageImpl;

import com.montages.acore.values.rules.RulesPackage;

import com.montages.acore.values.rules.impl.RulesPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class AbstractionsPackageImpl extends EPackageImpl implements AbstractionsPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aNamedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aAnnotatableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aAnnotationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aTypedEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aVariableEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.abstractions.AbstractionsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private AbstractionsPackageImpl() {
		super(eNS_URI, AbstractionsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link AbstractionsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static AbstractionsPackage init() {
		if (isInited)
			return (AbstractionsPackage) EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI);

		// Obtain or create and register package
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof AbstractionsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new AbstractionsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI)
						: AnnotationsPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theAbstractionsPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theAbstractionsPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAbstractionsPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(AbstractionsPackage.eNS_URI, theAbstractionsPackage);
		return theAbstractionsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getANamed() {
		return aNamedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_AUndefinedNameConstant() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getANamed_ABusinessName() {
		return (EAttribute) aNamedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAElement() {
		return aElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ALabel() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_AKindBase() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ARenderedKind() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_AContainingComponent() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ATPackageUri() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ATClassifierName() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAElement_ATFeatureName() {
		return (EAttribute) aElementEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_ATPackage() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_ATClassifier() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_ATFeature() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAElement_ATCoreAStringClass() {
		return (EReference) aElementEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AIndentLevel() {
		return aElementEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AIndentationSpaces() {
		return aElementEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AIndentationSpaces__Integer() {
		return aElementEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AStringOrMissing__String() {
		return aElementEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AStringIsEmpty__String() {
		return aElementEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AListOfStringToStringWithSeparator__EList_String() {
		return aElementEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AListOfStringToStringWithSeparator__EList() {
		return aElementEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__APackageFromUri__String() {
		return aElementEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AClassifierFromUriAndName__String_String() {
		return aElementEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__AFeatureFromUriAndNames__String_String_String() {
		return aElementEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAStringClass() {
		return aElementEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreARealClass() {
		return aElementEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAIntegerClass() {
		return aElementEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAElement__ACoreAObjectClass() {
		return aElementEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAAnnotatable() {
		return aAnnotatableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAnnotatable_AAnnotation() {
		return (EReference) aAnnotatableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAAnnotation() {
		return aAnnotationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAnnotation_AAnnotated() {
		return (EReference) aAnnotationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAnnotation_AAnnotating() {
		return (EReference) aAnnotationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getATyped() {
		return aTypedEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getATyped_AClassifier() {
		return (EReference) aTypedEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_AMandatory() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_ASingular() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_ATypeLabel() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_AUndefinedTypeConstant() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getATyped_ASimpleType() {
		return (EAttribute) aTypedEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean() {
		return aTypedEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAVariable() {
		return aVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AbstractionsFactory getAbstractionsFactory() {
		return (AbstractionsFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aNamedEClass = createEClass(ANAMED);
		createEAttribute(aNamedEClass, ANAMED__ANAME);
		createEAttribute(aNamedEClass, ANAMED__AUNDEFINED_NAME_CONSTANT);
		createEAttribute(aNamedEClass, ANAMED__ABUSINESS_NAME);

		aElementEClass = createEClass(AELEMENT);
		createEAttribute(aElementEClass, AELEMENT__ALABEL);
		createEAttribute(aElementEClass, AELEMENT__AKIND_BASE);
		createEAttribute(aElementEClass, AELEMENT__ARENDERED_KIND);
		createEReference(aElementEClass, AELEMENT__ACONTAINING_COMPONENT);
		createEAttribute(aElementEClass, AELEMENT__AT_PACKAGE_URI);
		createEAttribute(aElementEClass, AELEMENT__AT_CLASSIFIER_NAME);
		createEAttribute(aElementEClass, AELEMENT__AT_FEATURE_NAME);
		createEReference(aElementEClass, AELEMENT__AT_PACKAGE);
		createEReference(aElementEClass, AELEMENT__AT_CLASSIFIER);
		createEReference(aElementEClass, AELEMENT__AT_FEATURE);
		createEReference(aElementEClass, AELEMENT__AT_CORE_ASTRING_CLASS);
		createEOperation(aElementEClass, AELEMENT___AINDENT_LEVEL);
		createEOperation(aElementEClass, AELEMENT___AINDENTATION_SPACES);
		createEOperation(aElementEClass, AELEMENT___AINDENTATION_SPACES__INTEGER);
		createEOperation(aElementEClass, AELEMENT___ASTRING_OR_MISSING__STRING);
		createEOperation(aElementEClass, AELEMENT___ASTRING_IS_EMPTY__STRING);
		createEOperation(aElementEClass, AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING);
		createEOperation(aElementEClass, AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST);
		createEOperation(aElementEClass, AELEMENT___APACKAGE_FROM_URI__STRING);
		createEOperation(aElementEClass, AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING);
		createEOperation(aElementEClass, AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING);
		createEOperation(aElementEClass, AELEMENT___ACORE_ASTRING_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AREAL_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AINTEGER_CLASS);
		createEOperation(aElementEClass, AELEMENT___ACORE_AOBJECT_CLASS);

		aAnnotatableEClass = createEClass(AANNOTATABLE);
		createEReference(aAnnotatableEClass, AANNOTATABLE__AANNOTATION);

		aAnnotationEClass = createEClass(AANNOTATION);
		createEReference(aAnnotationEClass, AANNOTATION__AANNOTATED);
		createEReference(aAnnotationEClass, AANNOTATION__AANNOTATING);

		aTypedEClass = createEClass(ATYPED);
		createEReference(aTypedEClass, ATYPED__ACLASSIFIER);
		createEAttribute(aTypedEClass, ATYPED__AMANDATORY);
		createEAttribute(aTypedEClass, ATYPED__ASINGULAR);
		createEAttribute(aTypedEClass, ATYPED__ATYPE_LABEL);
		createEAttribute(aTypedEClass, ATYPED__AUNDEFINED_TYPE_CONSTANT);
		createEAttribute(aTypedEClass, ATYPED__ASIMPLE_TYPE);
		createEOperation(aTypedEClass, ATYPED___ATYPE_AS_OCL__APACKAGE_ACLASSIFIER_ASIMPLETYPE_BOOLEAN);

		aVariableEClass = createEClass(AVARIABLE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AcorePackage theAcorePackage = (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aNamedEClass.getESuperTypes().add(this.getAElement());
		aTypedEClass.getESuperTypes().add(this.getAElement());
		aVariableEClass.getESuperTypes().add(this.getANamed());
		aVariableEClass.getESuperTypes().add(this.getATyped());

		// Initialize classes, features, and operations; add parameters
		initEClass(aNamedEClass, ANamed.class, "ANamed", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getANamed_AName(), ecorePackage.getEString(), "aName", null, 0, 1, ANamed.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_AUndefinedNameConstant(), ecorePackage.getEString(), "aUndefinedNameConstant", null, 0,
				1, ANamed.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getANamed_ABusinessName(), ecorePackage.getEString(), "aBusinessName", null, 0, 1, ANamed.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aElementEClass, AElement.class, "AElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAElement_ALabel(), ecorePackage.getEString(), "aLabel", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_AKindBase(), ecorePackage.getEString(), "aKindBase", null, 0, 1, AElement.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ARenderedKind(), ecorePackage.getEString(), "aRenderedKind", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_AContainingComponent(), theAcorePackage.getAComponent(), null,
				"aContainingComponent", null, 0, 1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ATPackageUri(), ecorePackage.getEString(), "aTPackageUri", null, 0, 1,
				AElement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ATClassifierName(), ecorePackage.getEString(), "aTClassifierName", null, 0, 1,
				AElement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getAElement_ATFeatureName(), ecorePackage.getEString(), "aTFeatureName", null, 0, 1,
				AElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_ATPackage(), theAcorePackage.getAPackage(), null, "aTPackage", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_ATClassifier(), theClassifiersPackage.getAClassifier(), null, "aTClassifier", null,
				0, 1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_ATFeature(), theClassifiersPackage.getAFeature(), null, "aTFeature", null, 0, 1,
				AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAElement_ATCoreAStringClass(), theClassifiersPackage.getAClassifier(), null,
				"aTCoreAStringClass", null, 0, 1, AElement.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getAElement__AIndentLevel(), ecorePackage.getEIntegerObject(), "aIndentLevel", 0, 1, IS_UNIQUE,
				IS_ORDERED);

		initEOperation(getAElement__AIndentationSpaces(), ecorePackage.getEString(), "aIndentationSpaces", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getAElement__AIndentationSpaces__Integer(), ecorePackage.getEString(),
				"aIndentationSpaces", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEIntegerObject(), "size", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AStringOrMissing__String(), ecorePackage.getEString(), "aStringOrMissing", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "p", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AStringIsEmpty__String(), ecorePackage.getEBooleanObject(), "aStringIsEmpty",
				0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "s", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AListOfStringToStringWithSeparator__EList_String(), ecorePackage.getEString(),
				"aListOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "separator", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AListOfStringToStringWithSeparator__EList(), ecorePackage.getEString(),
				"aListOfStringToStringWithSeparator", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "elements", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__APackageFromUri__String(), theAcorePackage.getAPackage(), "aPackageFromUri", 0,
				1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "packageUri", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AClassifierFromUriAndName__String_String(),
				theClassifiersPackage.getAClassifier(), "aClassifierFromUriAndName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAElement__AFeatureFromUriAndNames__String_String_String(),
				theClassifiersPackage.getAFeature(), "aFeatureFromUriAndNames", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "uri", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "className", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "featureName", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAStringClass(), theClassifiersPackage.getAClassifier(), "aCoreAStringClass", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreARealClass(), theClassifiersPackage.getAClassifier(), "aCoreARealClass", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAIntegerClass(), theClassifiersPackage.getAClassifier(), "aCoreAIntegerClass",
				0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAElement__ACoreAObjectClass(), theClassifiersPackage.getAClassifier(), "aCoreAObjectClass", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEClass(aAnnotatableEClass, AAnnotatable.class, "AAnnotatable", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAAnnotatable_AAnnotation(), this.getAAnnotation(), null, "aAnnotation", null, 0, -1,
				AAnnotatable.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aAnnotationEClass, AAnnotation.class, "AAnnotation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAAnnotation_AAnnotated(), this.getAAnnotatable(), null, "aAnnotated", null, 0, 1,
				AAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAAnnotation_AAnnotating(), this.getAAnnotation(), null, "aAnnotating", null, 0, 1,
				AAnnotation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aTypedEClass, ATyped.class, "ATyped", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getATyped_AClassifier(), theClassifiersPackage.getAClassifier(), null, "aClassifier", null, 0, 1,
				ATyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_AMandatory(), ecorePackage.getEBooleanObject(), "aMandatory", null, 1, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_ASingular(), ecorePackage.getEBooleanObject(), "aSingular", null, 1, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_ATypeLabel(), ecorePackage.getEString(), "aTypeLabel", null, 0, 1, ATyped.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_AUndefinedTypeConstant(), ecorePackage.getEString(), "aUndefinedTypeConstant", null, 0,
				1, ATyped.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getATyped_ASimpleType(), theClassifiersPackage.getASimpleType(), "aSimpleType", null, 0, 1,
				ATyped.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		op = initEOperation(getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean(),
				ecorePackage.getEString(), "aTypeAsOcl", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theAcorePackage.getAPackage(), "aOclPackage", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theClassifiersPackage.getAClassifier(), "classifier", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theClassifiersPackage.getASimpleType(), "simpleType", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEBooleanObject(), "aSingular", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aVariableEClass, AVariable.class, "AVariable", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aNamedEClass, source, new String[] { "mName", "Named" });
		addAnnotation(getANamed_AName(), source, new String[] { "mName", "Name" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source, new String[] { "mName", "Undefined Name Constant" });
		addAnnotation(getANamed_ABusinessName(), source, new String[] { "mName", "Business Name" });
		addAnnotation(aElementEClass, source, new String[] { "mName", "Element" });
		addAnnotation(getAElement__AIndentLevel(), source, new String[] { "mName", "Indent Level" });
		addAnnotation(getAElement__AIndentationSpaces(), source, new String[] { "mName", "Indentation Spaces" });
		addAnnotation(getAElement__AIndentationSpaces__Integer(), source,
				new String[] { "mName", "Indentation Spaces" });
		addAnnotation(getAElement__AStringOrMissing__String(), source, new String[] { "mName", "String Or Missing" });
		addAnnotation((getAElement__AStringOrMissing__String()).getEParameters().get(0), source,
				new String[] { "mName", "p" });
		addAnnotation(getAElement__AStringIsEmpty__String(), source, new String[] { "mName", "String Is Empty" });
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList_String(), source,
				new String[] { "mName", "List Of String To String With Separator" });
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList(), source,
				new String[] { "mName", "List Of String To String With Separator" });
		addAnnotation(getAElement__APackageFromUri__String(), source, new String[] { "mName", "Package From Uri" });
		addAnnotation(getAElement__AClassifierFromUriAndName__String_String(), source,
				new String[] { "mName", "Classifier From Uri And Name" });
		addAnnotation(getAElement__AFeatureFromUriAndNames__String_String_String(), source,
				new String[] { "mName", "Feature From Uri And Names" });
		addAnnotation(getAElement__ACoreAStringClass(), source, new String[] { "mName", "Core A String Class" });
		addAnnotation(getAElement__ACoreARealClass(), source, new String[] { "mName", "Core A Real Class" });
		addAnnotation(getAElement__ACoreAIntegerClass(), source, new String[] { "mName", "Core A Integer Class" });
		addAnnotation(getAElement__ACoreAObjectClass(), source, new String[] { "mName", "Core A Object Class" });
		addAnnotation(getAElement_ALabel(), source, new String[] { "mName", "Label" });
		addAnnotation(getAElement_AKindBase(), source, new String[] { "mName", "Kind Base" });
		addAnnotation(getAElement_ARenderedKind(), source, new String[] { "mName", "Rendered Kind" });
		addAnnotation(getAElement_AContainingComponent(), source, new String[] { "mName", "Containing Component" });
		addAnnotation(getAElement_ATPackageUri(), source, new String[] { "mName", "T Package Uri" });
		addAnnotation(getAElement_ATClassifierName(), source, new String[] { "mName", "T Classifier Name" });
		addAnnotation(getAElement_ATFeatureName(), source, new String[] { "mName", "T Feature Name" });
		addAnnotation(getAElement_ATPackage(), source, new String[] { "mName", "T Package" });
		addAnnotation(getAElement_ATClassifier(), source, new String[] { "mName", "T Classifier" });
		addAnnotation(getAElement_ATFeature(), source, new String[] { "mName", "T Feature" });
		addAnnotation(getAElement_ATCoreAStringClass(), source, new String[] { "mName", "T Core A String Class" });
		addAnnotation(aAnnotatableEClass, source, new String[] { "mName", "Annotatable" });
		addAnnotation(aAnnotationEClass, source, new String[] { "mName", "Annotation" });
		addAnnotation(getAAnnotation_AAnnotated(), source, new String[] { "mName", "Annotated" });
		addAnnotation(getAAnnotation_AAnnotating(), source, new String[] { "mName", "Annotating" });
		addAnnotation(aTypedEClass, source, new String[] { "mName", "Typed" });
		addAnnotation(getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean(), source,
				new String[] { "mName", "Type As Ocl" });
		addAnnotation(getATyped_AMandatory(), source, new String[] { "mName", "Mandatory" });
		addAnnotation(getATyped_ASingular(), source, new String[] { "mName", "Singular" });
		addAnnotation(getATyped_ATypeLabel(), source, new String[] { "mName", "Type Label" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source, new String[] { "mName", "Undefined Type Constant" });
		addAnnotation(aVariableEClass, source, new String[] { "mName", "Variable" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelDerive", "aName\n" });
		addAnnotation(aTypedEClass, source, new String[] { "aLabelDerive", "aTypeLabel\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aNamedEClass, source, new String[] { "aLabelCreateColumn", "false" });
		addAnnotation(aTypedEClass, source, new String[] { "aLabelCreateColumn", "true" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getANamed_AName(), source, new String[] { "derive", "aUndefinedNameConstant\n" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "derive", "\'<A Name Is Undefined> \'\n" });
		addAnnotation(getANamed_ABusinessName(), source, new String[] { "derive",
				"let chain : String = aName in\nif chain.oclIsUndefined()\n  then null\n  else chain .camelCaseToBusiness()\n  endif\n" });
		addAnnotation(getAElement__AIndentLevel(), source, new String[] { "body",
				"if eContainer().oclIsUndefined() \n  then 0\nelse if eContainer().oclIsKindOf(AElement)\n  then eContainer().oclAsType(AElement).aIndentLevel() + 1\n  else 0 endif endif\n" });
		addAnnotation(getAElement__AIndentationSpaces(), source, new String[] { "body",
				"let numberOfSpaces: Integer = let e1: Integer = aIndentLevel() * 4 in \n if e1.oclIsInvalid() then null else e1 endif in\naIndentationSpaces(numberOfSpaces)\n" });
		addAnnotation(getAElement__AIndentationSpaces__Integer(), source, new String[] { "body",
				"let sizeMinusOne: Integer = let e1: Integer = size - 1 in \n if e1.oclIsInvalid() then null else e1 endif in\nif (let e0: Boolean = size < 1 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\'\n  else (let e0: String = aIndentationSpaces(sizeMinusOne).concat(\' \') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif\n" });
		addAnnotation(getAElement__AStringOrMissing__String(), source,
				new String[] { "body", "if (aStringIsEmpty(p)) \n  =true \nthen \'MISSING\'\n  else p\nendif\n" });
		addAnnotation(getAElement__AStringIsEmpty__String(), source, new String[] { "body",
				"if ( s.oclIsUndefined()) \n  =true \nthen true else if (let e0: Boolean = \'\' = let e0: String = s.trim() in \n if e0.oclIsInvalid() then null else e0 endif in \n if e0.oclIsInvalid() then null else e0 endif)=true then true\n  else false\nendif endif\n" });
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList_String(), source, new String[] { "body",
				"let f:String = elements->asOrderedSet()->first() in\r\nif f.oclIsUndefined()\r\n  then \'\'\r\n  else if elements-> size()=1 then f else\r\n    let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=\'\'|ac.concat(separator).concat(it)) in\r\n    f.concat(rest)\r\n  endif endif\r\n" });
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList(), source, new String[] { "body",
				"let defaultSeparator: String = \', \' in\naListOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)\n" });
		addAnnotation(getAElement__APackageFromUri__String(), source, new String[] { "body",
				"if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aPackageFromUri(packageUri)\nendif\n" });
		addAnnotation(getAElement__AClassifierFromUriAndName__String_String(), source, new String[] { "body",
				"let p: acore::APackage = aPackageFromUri(uri) in\nif p = null\n  then null\n  else p.aClassifierFromName(name) endif\n" });
		addAnnotation(getAElement__AFeatureFromUriAndNames__String_String_String(), source, new String[] { "body",
				"let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in\nlet cAsClass: acore::classifiers::AClassType = let chain: acore::classifiers::AClassifier = c in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClassType)\n    then chain.oclAsType(acore::classifiers::AClassType)\n    else null\n  endif\n  endif in\nif cAsClass = null\n  then null\n  else cAsClass.aFeatureFromName(featureName) endif\n" });
		addAnnotation(getAElement__ACoreAStringClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAStringName: String = \'AString\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)\n" });
		addAnnotation(getAElement__ACoreARealClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreARealName: String = \'AReal\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)\n" });
		addAnnotation(getAElement__ACoreAIntegerClass(), source, new String[] { "body",
				"let aCoreClassifiersPackageUri: String = \'http://www.langlets.org/ACore/ACore/Classifiers\' in\nlet aCoreAIntegerName: String = \'AInteger\' in\naClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)\n" });
		addAnnotation(getAElement__ACoreAObjectClass(), source, new String[] { "body",
				"let aCoreValuesPackageUri: String = \'http://www.langlets.org/ACore/ACore/Values\' in\nlet aCoreAObjectName: String = \'AObject\' in\naClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)\n" });
		addAnnotation(getAElement_ALabel(), source, new String[] { "derive", "aRenderedKind\n" });
		addAnnotation(getAElement_AKindBase(), source, new String[] { "derive", "self.eClass().name\n" });
		addAnnotation(getAElement_ARenderedKind(), source, new String[] { "derive",
				"let e1: String = aIndentationSpaces().concat(let chain12: String = aKindBase in\nif chain12.toUpperCase().oclIsUndefined() \n then null \n else chain12.toUpperCase()\n  endif) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getAElement_AContainingComponent(), source, new String[] { "derive",
				"if self.eContainer().oclIsTypeOf(acore::AComponent)\r\n  then self.eContainer().oclAsType(acore::AComponent)\r\n  else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)\r\n  then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent\r\n  else null endif endif" });
		addAnnotation(getAElement_ATPackage(), source,
				new String[] { "derive", "let p: String = aTPackageUri in\naPackageFromUri(p)\n" });
		addAnnotation(getAElement_ATClassifier(), source, new String[] { "derive",
				"let p: String = aTPackageUri in\nlet c: String = aTClassifierName in\naClassifierFromUriAndName(p, c)\n" });
		addAnnotation(getAElement_ATFeature(), source, new String[] { "derive",
				"let p: String = aTPackageUri in\nlet c: String = aTClassifierName in\nlet f: String = aTFeatureName in\naFeatureFromUriAndNames(p, c, f)\n" });
		addAnnotation(getAElement_ATCoreAStringClass(), source, new String[] { "derive", "aCoreAStringClass()\n" });
		addAnnotation(getAAnnotatable_AAnnotation(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAAnnotation_AAnnotated(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAAnnotation_AAnnotating(), source, new String[] { "derive", "null\n" });
		addAnnotation(getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean(), source, new String[] { "body",
				"if  aClassifier.oclIsUndefined() and (aSimpleType = acore::classifiers::ASimpleType::None)  \r\n  then \'MISSING TYPE\' \r\n  else let t: String = \r\n      if  aSimpleType<> acore::classifiers::ASimpleType::None\r\n        then \r\n          if aSimpleType= acore::classifiers::ASimpleType::Real  then \'Real\' \r\n          else if aSimpleType= acore::classifiers::ASimpleType::Object then \'ecore::EObject\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Annotation then \'ecore::EAnnotation\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Attribute then \'ecore::EAttribute\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Class then \'ecore::EClass\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Classifier then \'ecore::EClassifier\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::DataType then \'ecore::EDataType\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Enumeration then \'ecore::Enumeration\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Feature then \'ecore::EStructuralFeature\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Literal then \'ecore::EEnumLiteral\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::KeyValue then \'ecore::EStringToStringMapEntry\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::NamedElement then \'ecore::NamedElement\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Operation then \'ecore::EOperation\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Package then \'ecore::EPackage\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Parameter then \'ecore::EParameter\'\r\n           else if aSimpleType= acore::classifiers::ASimpleType::Reference then \'ecore::EReference\'\r\n           else if aSimpleType=  acore::classifiers::ASimpleType::TypedElement then \'ecore::ETypedElement\'\r\n           else if aSimpleType =  acore::classifiers::ASimpleType::Date then \'ecore::EDate\' \r\n            else aSimpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif\r\n        else \r\n          /* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified \r\n\t      let pkg: String = \r\n\t        if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat(\'::\')\r\n\t          else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then \'\' else mClassifier.containingPackage.eName.allLowerCase().concat(\'::\') endif \r\n\t        endif\r\n\t      in pkg.concat(mClassifier.eName) \r\n\t      NEW Create string with all package qualifiers */\r\n\t      let pkg:String=\r\n\t       if aClassifier.aContainingPackage   =null then \'\'\r\n\t       else aClassifier.aContainingPackage.aName.concat(\'::\' ) endif                            in\r\n\t       pkg.concat(aClassifier.aName)\r\n      endif\r\n\tin \r\n\r\n    if  aSingular.oclIsUndefined() then \'MISSING ARITY INFO\' else \r\n      if aSingular=true then t else \'OrderedSet(\'.concat(t).concat(\') \') endif\r\n    endif\r\nendif\r\n\r\n" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassifier = null in nl\n" });
		addAnnotation(getATyped_AMandatory(), source, new String[] { "derive", "false\n" });
		addAnnotation(getATyped_ASingular(), source, new String[] { "derive", "false\n" });
		addAnnotation(getATyped_ATypeLabel(), source, new String[] { "derive",
				"if (let e0: Boolean = aClassifier = null in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in \n if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif\n  else (let e0: String = if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif.concat(\'*\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "derive", "\'<A Type Is Undefined>\'\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getANamed_AName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getANamed_ABusinessName(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement__AIndentLevel(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AIndentationSpaces(), source, new String[] {});
		addAnnotation(getAElement__AIndentationSpaces__Integer(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AStringOrMissing__String(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AStringIsEmpty__String(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList_String(), source, new String[] {});
		addAnnotation(getAElement__AListOfStringToStringWithSeparator__EList(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__APackageFromUri__String(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AClassifierFromUriAndName__String_String(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__AFeatureFromUriAndNames__String_String_String(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAStringClass(), source,
				new String[] { "propertyCategory", "z A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreARealClass(), source,
				new String[] { "propertyCategory", "z A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAIntegerClass(), source,
				new String[] { "propertyCategory", "z A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement__ACoreAObjectClass(), source,
				new String[] { "propertyCategory", "z A Core/Package", "createColumn", "true" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "createColumn", "true", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "createColumn", "true", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_AContainingComponent(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_ATPackageUri(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_ATClassifierName(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_ATFeatureName(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAElement_ATPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_ATClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_ATFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Abstractions" });
		addAnnotation(getAElement_ATCoreAStringClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Package" });
		addAnnotation(getAAnnotatable_AAnnotation(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAAnnotation_AAnnotated(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAAnnotation_AAnnotating(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getATyped__ATypeAsOcl__APackage_AClassifier_ASimpleType_Boolean(), source,
				new String[] { "propertyCategory", "z A Core/OCL", "createColumn", "true" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getATyped_AMandatory(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getATyped_ASingular(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getATyped_ATypeLabel(), source,
				new String[] { "createColumn", "true", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getATyped_ASimpleType(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getANamed_AName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_AUndefinedNameConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getANamed_ABusinessName(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ALabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_AKindBase(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ARenderedKind(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_AContainingComponent(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ATPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ATClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ATFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAElement_ATCoreAStringClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AMandatory(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_ASingular(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_ATypeLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getATyped_AUndefinedTypeConstant(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

} //AbstractionsPackageImpl
