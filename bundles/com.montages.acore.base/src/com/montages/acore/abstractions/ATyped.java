/**
 */

package com.montages.acore.abstractions;

import com.montages.acore.APackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ASimpleType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ATyped</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getAClassifier <em>AClassifier</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getAMandatory <em>AMandatory</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getASingular <em>ASingular</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getATypeLabel <em>AType Label</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getAUndefinedTypeConstant <em>AUndefined Type Constant</em>}</li>
 *   <li>{@link com.montages.acore.abstractions.ATyped#getASimpleType <em>ASimple Type</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Typed'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aLabelDerive='aTypeLabel\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aLabelCreateColumn='true'"
 * @generated
 */

public interface ATyped extends AElement {
	/**
	 * Returns the value of the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AClassifier</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AClassifier</em>' reference.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_AClassifier()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassifier = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassifier getAClassifier();

	/**
	 * Returns the value of the '<em><b>AMandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AMandatory</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AMandatory</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_AMandatory()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Mandatory'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAMandatory();

	/**
	 * Returns the value of the '<em><b>ASingular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASingular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASingular</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_ASingular()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Singular'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getASingular();

	/**
	 * Returns the value of the '<em><b>AType Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AType Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AType Label</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_ATypeLabel()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Type Label'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = aClassifier = null in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen aUndefinedTypeConstant else if (let e0: Boolean = aSingular = true in \n if e0.oclIsInvalid() then null else e0 endif)=true then if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif\n  else (let e0: String = if aClassifier.oclIsUndefined()\n  then null\n  else aClassifier.aName\nendif.concat(\'*\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getATypeLabel();

	/**
	 * Returns the value of the '<em><b>AUndefined Type Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUndefined Type Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUndefined Type Constant</em>' attribute.
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_AUndefinedTypeConstant()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Undefined Type Constant'"
	 *        annotation="http://www.xocl.org/OCL derive='\'<A Type Is Undefined>\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUndefinedTypeConstant();

	/**
	 * Returns the value of the '<em><b>ASimple Type</b></em>' attribute.
	 * The literals are from the enumeration {@link com.montages.acore.classifiers.ASimpleType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASimple Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASimple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see #isSetASimpleType()
	 * @see #unsetASimpleType()
	 * @see #setASimpleType(ASimpleType)
	 * @see com.montages.acore.abstractions.AbstractionsPackage#getATyped_ASimpleType()
	 * @model unsettable="true"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='false'"
	 * @generated
	 */
	ASimpleType getASimpleType();

	/** 
	 * Sets the value of the '{@link com.montages.acore.abstractions.ATyped#getASimpleType <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	  
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>ASimple Type</em>' attribute.
	 * @see com.montages.acore.classifiers.ASimpleType
	 * @see #isSetASimpleType()
	 * @see #unsetASimpleType()
	 * @see #getASimpleType()
	 * @generated
	 */

	void setASimpleType(ASimpleType value);

	/**
	 * Unsets the value of the '{@link com.montages.acore.abstractions.ATyped#getASimpleType <em>ASimple Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetASimpleType()
	 * @see #getASimpleType()
	 * @see #setASimpleType(ASimpleType)
	 * @generated
	 */
	void unsetASimpleType();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.abstractions.ATyped#getASimpleType <em>ASimple Type</em>}' attribute is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ASimple Type</em>' attribute is set.
	 * @see #unsetASimpleType()
	 * @see #getASimpleType()
	 * @see #setASimpleType(ASimpleType)
	 * @generated
	 */
	boolean isSetASimpleType();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Type As Ocl'"
	 *        annotation="http://www.xocl.org/OCL body='if  aClassifier.oclIsUndefined() and (aSimpleType = acore::classifiers::ASimpleType::None)  \r\n  then \'MISSING TYPE\' \r\n  else let t: String = \r\n      if  aSimpleType<> acore::classifiers::ASimpleType::None\r\n        then \r\n          if aSimpleType= acore::classifiers::ASimpleType::Real  then \'Real\' \r\n          else if aSimpleType= acore::classifiers::ASimpleType::Object then \'ecore::EObject\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Annotation then \'ecore::EAnnotation\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Attribute then \'ecore::EAttribute\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Class then \'ecore::EClass\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Classifier then \'ecore::EClassifier\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::DataType then \'ecore::EDataType\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Enumeration then \'ecore::Enumeration\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Feature then \'ecore::EStructuralFeature\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Literal then \'ecore::EEnumLiteral\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::KeyValue then \'ecore::EStringToStringMapEntry\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::NamedElement then \'ecore::NamedElement\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Operation then \'ecore::EOperation\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Package then \'ecore::EPackage\'\r\n          else if aSimpleType= acore::classifiers::ASimpleType::Parameter then \'ecore::EParameter\'\r\n           else if aSimpleType= acore::classifiers::ASimpleType::Reference then \'ecore::EReference\'\r\n           else if aSimpleType=  acore::classifiers::ASimpleType::TypedElement then \'ecore::ETypedElement\'\r\n           else if aSimpleType =  acore::classifiers::ASimpleType::Date then \'ecore::EDate\' \r\n            else aSimpleType.toString() endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif endif  endif\r\n        else \r\n          /* OLD_ Create a string with the package name for mClassifier, if the OCL package differs from it or it is not specified \r\n\t      let pkg: String = \r\n\t        if oclPackage.oclIsUndefined() then mClassifier.containingPackage.eName.allLowerCase().concat(\'::\')\r\n\t          else if (oclPackage = mClassifier.containingPackage) or (mClassifier.containingPackage.allSubpackages->includes(oclPackage)) then \'\' else mClassifier.containingPackage.eName.allLowerCase().concat(\'::\') endif \r\n\t        endif\r\n\t      in pkg.concat(mClassifier.eName) \r\n\t      NEW Create string with all package qualifiers \052/\r\n\t      let pkg:String=\r\n\t       if aClassifier.aContainingPackage   =null then \'\'\r\n\t       else aClassifier.aContainingPackage.aName.concat(\'::\' ) endif                            in\r\n\t       pkg.concat(aClassifier.aName)\r\n      endif\r\n\tin \r\n\r\n    if  aSingular.oclIsUndefined() then \'MISSING ARITY INFO\' else \r\n      if aSingular=true then t else \'OrderedSet(\'.concat(t).concat(\') \') endif\r\n    endif\r\nendif\r\n\r\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/OCL' createColumn='true'"
	 * @generated
	 */
	String aTypeAsOcl(APackage aOclPackage, AClassifier classifier, ASimpleType simpleType, Boolean aSingular);

} // ATyped
