/**
 */

package com.montages.acore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AAbstract Folder</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.AAbstractFolder#getAPackage <em>APackage</em>}</li>
 *   <li>{@link com.montages.acore.AAbstractFolder#getAResource <em>AResource</em>}</li>
 *   <li>{@link com.montages.acore.AAbstractFolder#getAFolder <em>AFolder</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.AcorePackage#getAAbstractFolder()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Abstract Folder'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aAllPackagesDerive='let e1: OrderedSet(acore::APackage)  = let chain11: OrderedSet(acore::APackage)  = aPackage.aAllPackages->asOrderedSet() in\nif chain11->asOrderedSet()->oclIsUndefined() \n then null \n else chain11->asOrderedSet()\n  endif->union(aFolder.aAllPackages->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
 * @generated
 */

public interface AAbstractFolder extends AStructuringElement {
	/**
	 * Returns the value of the '<em><b>APackage</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.APackage}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>APackage</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>APackage</em>' reference list.
	 * @see com.montages.acore.AcorePackage#getAAbstractFolder_APackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<APackage> getAPackage();

	/**
	 * Returns the value of the '<em><b>AResource</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.AResource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AResource</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AResource</em>' reference list.
	 * @see com.montages.acore.AcorePackage#getAAbstractFolder_AResource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AResource> getAResource();

	/**
	 * Returns the value of the '<em><b>AFolder</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.AFolder}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFolder</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFolder</em>' reference list.
	 * @see com.montages.acore.AcorePackage#getAAbstractFolder_AFolder()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AFolder> getAFolder();

} // AAbstractFolder
