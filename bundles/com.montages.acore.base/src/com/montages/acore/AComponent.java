/**
 */

package com.montages.acore;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AComponent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.AComponent#getAComponentId <em>AComponent Id</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getABaseUri <em>ABase Uri</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getADefaultUri <em>ADefault Uri</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getAUndefinedIdConstant <em>AUndefined Id Constant</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getAUsed <em>AUsed</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getAMainPackage <em>AMain Package</em>}</li>
 *   <li>{@link com.montages.acore.AComponent#getAMainResource <em>AMain Resource</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.AcorePackage#getAComponent()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Component'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aUriDerive='let e1: String = aBaseUri.concat(\'/\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif\n' aContainingComponentDerive='self\n'"
 *        annotation="http://www.xocl.org/OVERRIDE_EDITORCONFIG aUriCreateColumn='false' aContainingComponentCreateColumn='false'"
 * @generated
 */

public interface AComponent extends AAbstractFolder {
	/**
	 * Returns the value of the '<em><b>AComponent Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AComponent Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AComponent Id</em>' attribute.
	 * @see com.montages.acore.AcorePackage#getAComponent_AComponentId()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Component Id'"
	 *        annotation="http://www.xocl.org/OCL derive='aUndefinedIdConstant\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAComponentId();

	/**
	 * Returns the value of the '<em><b>ABase Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ABase Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ABase Uri</em>' attribute.
	 * @see com.montages.acore.AcorePackage#getAComponent_ABaseUri()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Base Uri'"
	 *        annotation="http://www.xocl.org/OCL derive='aDefaultUri\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getABaseUri();

	/**
	 * Returns the value of the '<em><b>ADefault Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ADefault Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADefault Uri</em>' attribute.
	 * @see com.montages.acore.AcorePackage#getAComponent_ADefaultUri()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Default Uri'"
	 *        annotation="http://www.xocl.org/OCL derive='\'http://www.langlets.org\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getADefaultUri();

	/**
	 * Returns the value of the '<em><b>AUndefined Id Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUndefined Id Constant</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUndefined Id Constant</em>' attribute.
	 * @see com.montages.acore.AcorePackage#getAComponent_AUndefinedIdConstant()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Undefined Id Constant'"
	 *        annotation="http://www.xocl.org/OCL derive='\'<A Id Is Undefined> \'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	String getAUndefinedIdConstant();

	/**
	 * Returns the value of the '<em><b>AUsed</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.AComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AUsed</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AUsed</em>' reference list.
	 * @see com.montages.acore.AcorePackage#getAComponent_AUsed()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Used'"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AComponent> getAUsed();

	/**
	 * Returns the value of the '<em><b>AMain Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AMain Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AMain Package</em>' reference.
	 * @see com.montages.acore.AcorePackage#getAComponent_AMainPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Main Package'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::APackage = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getAMainPackage();

	/**
	 * Returns the value of the '<em><b>AMain Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AMain Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AMain Resource</em>' reference.
	 * @see com.montages.acore.AcorePackage#getAComponent_AMainResource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Main Resource'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::AResource = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AResource getAMainResource();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Package From Uri'"
	 *        annotation="http://www.xocl.org/OCL body='let localPackage: OrderedSet(acore::APackage)  = aAllPackages->asOrderedSet()->select(it: acore::APackage | let e0: Boolean = it.aUri = packageUri in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::APackage)  = localPackage in\nif chain->notEmpty().oclIsUndefined() \n then null \n else chain->notEmpty()\n  endif) \n  =true \nthen let chain: OrderedSet(acore::APackage)  = localPackage in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n  else let chain: OrderedSet(acore::APackage)  = aUsed.aPackageFromUri(packageUri)->reject(oclIsUndefined())->asOrderedSet() in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n'"
	 * @generated
	 */
	APackage aPackageFromUri(String packageUri);

} // AComponent
