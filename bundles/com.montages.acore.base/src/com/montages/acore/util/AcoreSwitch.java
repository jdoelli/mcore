/**
 */
package com.montages.acore.util;

import com.montages.acore.*;

import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ANamed;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see com.montages.acore.AcorePackage
 * @generated
 */
public class AcoreSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static AcorePackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AcoreSwitch() {
		if (modelPackage == null) {
			modelPackage = AcorePackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @parameter ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
		case AcorePackage.ASTRUCTURING_ELEMENT: {
			AStructuringElement aStructuringElement = (AStructuringElement) theEObject;
			T result = caseAStructuringElement(aStructuringElement);
			if (result == null)
				result = caseANamed(aStructuringElement);
			if (result == null)
				result = caseAElement(aStructuringElement);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AcorePackage.AABSTRACT_FOLDER: {
			AAbstractFolder aAbstractFolder = (AAbstractFolder) theEObject;
			T result = caseAAbstractFolder(aAbstractFolder);
			if (result == null)
				result = caseAStructuringElement(aAbstractFolder);
			if (result == null)
				result = caseANamed(aAbstractFolder);
			if (result == null)
				result = caseAElement(aAbstractFolder);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AcorePackage.ACOMPONENT: {
			AComponent aComponent = (AComponent) theEObject;
			T result = caseAComponent(aComponent);
			if (result == null)
				result = caseAAbstractFolder(aComponent);
			if (result == null)
				result = caseAStructuringElement(aComponent);
			if (result == null)
				result = caseANamed(aComponent);
			if (result == null)
				result = caseAElement(aComponent);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AcorePackage.AFOLDER: {
			AFolder aFolder = (AFolder) theEObject;
			T result = caseAFolder(aFolder);
			if (result == null)
				result = caseAAbstractFolder(aFolder);
			if (result == null)
				result = caseAStructuringElement(aFolder);
			if (result == null)
				result = caseANamed(aFolder);
			if (result == null)
				result = caseAElement(aFolder);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AcorePackage.APACKAGE: {
			APackage aPackage = (APackage) theEObject;
			T result = caseAPackage(aPackage);
			if (result == null)
				result = caseAStructuringElement(aPackage);
			if (result == null)
				result = caseANamed(aPackage);
			if (result == null)
				result = caseAElement(aPackage);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		case AcorePackage.ARESOURCE: {
			AResource aResource = (AResource) theEObject;
			T result = caseAResource(aResource);
			if (result == null)
				result = caseAStructuringElement(aResource);
			if (result == null)
				result = caseANamed(aResource);
			if (result == null)
				result = caseAElement(aResource);
			if (result == null)
				result = defaultCase(theEObject);
			return result;
		}
		default:
			return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AStructuring Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AStructuring Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAStructuringElement(AStructuringElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AAbstract Folder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AAbstract Folder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAAbstractFolder(AAbstractFolder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AComponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AComponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAComponent(AComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AFolder</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AFolder</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAFolder(AFolder object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>APackage</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>APackage</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAPackage(APackage object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AResource</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AResource</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAResource(AResource object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>AElement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAElement(AElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>ANamed</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseANamed(ANamed object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //AcoreSwitch
