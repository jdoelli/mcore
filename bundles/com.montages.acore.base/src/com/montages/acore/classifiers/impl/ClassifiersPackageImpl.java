/**
 */
package com.montages.acore.classifiers.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AbstractionsPackageImpl;

import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.annotations.impl.AnnotationsPackageImpl;

import com.montages.acore.classifiers.AAttribute;
import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ADataType;
import com.montages.acore.classifiers.AEnumeration;
import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.AOperation;
import com.montages.acore.classifiers.AParameter;
import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.AReference;
import com.montages.acore.classifiers.ASimpleType;
import com.montages.acore.classifiers.ClassifiersFactory;
import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.impl.AcorePackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.impl.ValuesPackageImpl;

import com.montages.acore.values.rules.RulesPackage;

import com.montages.acore.values.rules.impl.RulesPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassifiersPackageImpl extends EPackageImpl implements ClassifiersPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aClassifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aDataTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aEnumerationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aClassTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aFeatureEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aAttributeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aReferenceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aOperationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aParameterEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum aSimpleTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aStringEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aIntegerEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aBooleanEDataType = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType aRealEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.classifiers.ClassifiersPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ClassifiersPackageImpl() {
		super(eNS_URI, ClassifiersFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ClassifiersPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ClassifiersPackage init() {
		if (isInited)
			return (ClassifiersPackage) EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI);

		// Obtain or create and register package
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ClassifiersPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ClassifiersPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI)
						: AnnotationsPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theClassifiersPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theClassifiersPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theClassifiersPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ClassifiersPackage.eNS_URI, theClassifiersPackage);
		return theClassifiersPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAClassifier() {
		return aClassifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_ASpecializedClassifier() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveDataType() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveEnumeration() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AActiveClass() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AContainingPackage() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AAsDataType() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassifier_AAsClass() {
		return (EReference) aClassifierEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassifier_AIsStringClassifier() {
		return (EAttribute) aClassifierEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassifier__AAssignableTo__AClassifier() {
		return aClassifierEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassifier__AAllProperty() {
		return aClassifierEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getADataType() {
		return aDataTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getADataType_ASpecializedDataType() {
		return (EReference) aDataTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getADataType_ADataTypePackage() {
		return (EReference) aDataTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAEnumeration() {
		return aEnumerationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAEnumeration_ALiteral() {
		return (EReference) aEnumerationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAClassType() {
		return aClassTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassType_AAbstract() {
		return (EAttribute) aClassTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_ASpecializedClass() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AFeature() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AOperation() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AAllFeature() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AAllOperation() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AObjectLabel() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAClassType_ASuperTypesLabel() {
		return (EAttribute) aClassTypeEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAClassType_AAllStoredFeature() {
		return (EReference) aClassTypeEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__ACreate() {
		return aClassTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__AFeatureFromName__String() {
		return aClassTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__AOperationFromNameAndTypes__String_AClassifier() {
		return aClassTypeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__AAllGeneralizedClass() {
		return aClassTypeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__AAllSpecializedClass() {
		return aClassTypeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAClassType__AAllProperty() {
		return aClassTypeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAFeature() {
		return aFeatureEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AStored() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_APersisted() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AChangeable() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAFeature_AActiveFeature() {
		return (EAttribute) aFeatureEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAAttribute() {
		return aAttributeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAAttribute_ADataType() {
		return (EReference) aAttributeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAAttribute_AActiveAttribute() {
		return (EAttribute) aAttributeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAReference() {
		return aReferenceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAReference_AClassType() {
		return (EReference) aReferenceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAReference_AContainement() {
		return (EAttribute) aReferenceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getAReference_AActiveReference() {
		return (EAttribute) aReferenceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAOperation() {
		return aOperationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAOperation_AParameter() {
		return (EReference) aOperationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAOperation_AContainingProperty() {
		return (EReference) aOperationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAParameter() {
		return aParameterEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAParameter_AContainingOperation() {
		return (EReference) aParameterEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAProperty() {
		return aPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAProperty_AContainingClass() {
		return (EReference) aPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAProperty_AOperation() {
		return (EReference) aPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getASimpleType() {
		return aSimpleTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAString() {
		return aStringEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAInteger() {
		return aIntegerEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getABoolean() {
		return aBooleanEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getAReal() {
		return aRealEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassifiersFactory getClassifiersFactory() {
		return (ClassifiersFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aClassifierEClass = createEClass(ACLASSIFIER);
		createEReference(aClassifierEClass, ACLASSIFIER__ASPECIALIZED_CLASSIFIER);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_DATA_TYPE);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_ENUMERATION);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AACTIVE_CLASS);
		createEReference(aClassifierEClass, ACLASSIFIER__ACONTAINING_PACKAGE);
		createEReference(aClassifierEClass, ACLASSIFIER__AAS_DATA_TYPE);
		createEReference(aClassifierEClass, ACLASSIFIER__AAS_CLASS);
		createEAttribute(aClassifierEClass, ACLASSIFIER__AIS_STRING_CLASSIFIER);
		createEOperation(aClassifierEClass, ACLASSIFIER___AASSIGNABLE_TO__ACLASSIFIER);
		createEOperation(aClassifierEClass, ACLASSIFIER___AALL_PROPERTY);

		aDataTypeEClass = createEClass(ADATA_TYPE);
		createEReference(aDataTypeEClass, ADATA_TYPE__ASPECIALIZED_DATA_TYPE);
		createEReference(aDataTypeEClass, ADATA_TYPE__ADATA_TYPE_PACKAGE);

		aEnumerationEClass = createEClass(AENUMERATION);
		createEReference(aEnumerationEClass, AENUMERATION__ALITERAL);

		aClassTypeEClass = createEClass(ACLASS_TYPE);
		createEAttribute(aClassTypeEClass, ACLASS_TYPE__AABSTRACT);
		createEReference(aClassTypeEClass, ACLASS_TYPE__ASPECIALIZED_CLASS);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AFEATURE);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AOPERATION);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AALL_FEATURE);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AALL_OPERATION);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AOBJECT_LABEL);
		createEAttribute(aClassTypeEClass, ACLASS_TYPE__ASUPER_TYPES_LABEL);
		createEReference(aClassTypeEClass, ACLASS_TYPE__AALL_STORED_FEATURE);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___ACREATE);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___AFEATURE_FROM_NAME__STRING);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___AOPERATION_FROM_NAME_AND_TYPES__STRING_ACLASSIFIER);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___AALL_GENERALIZED_CLASS);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___AALL_SPECIALIZED_CLASS);
		createEOperation(aClassTypeEClass, ACLASS_TYPE___AALL_PROPERTY);

		aFeatureEClass = createEClass(AFEATURE);
		createEAttribute(aFeatureEClass, AFEATURE__ASTORED);
		createEAttribute(aFeatureEClass, AFEATURE__APERSISTED);
		createEAttribute(aFeatureEClass, AFEATURE__ACHANGEABLE);
		createEAttribute(aFeatureEClass, AFEATURE__AACTIVE_FEATURE);

		aAttributeEClass = createEClass(AATTRIBUTE);
		createEReference(aAttributeEClass, AATTRIBUTE__ADATA_TYPE);
		createEAttribute(aAttributeEClass, AATTRIBUTE__AACTIVE_ATTRIBUTE);

		aReferenceEClass = createEClass(AREFERENCE);
		createEReference(aReferenceEClass, AREFERENCE__ACLASS_TYPE);
		createEAttribute(aReferenceEClass, AREFERENCE__ACONTAINEMENT);
		createEAttribute(aReferenceEClass, AREFERENCE__AACTIVE_REFERENCE);

		aOperationEClass = createEClass(AOPERATION);
		createEReference(aOperationEClass, AOPERATION__APARAMETER);
		createEReference(aOperationEClass, AOPERATION__ACONTAINING_PROPERTY);

		aParameterEClass = createEClass(APARAMETER);
		createEReference(aParameterEClass, APARAMETER__ACONTAINING_OPERATION);

		aPropertyEClass = createEClass(APROPERTY);
		createEReference(aPropertyEClass, APROPERTY__ACONTAINING_CLASS);
		createEReference(aPropertyEClass, APROPERTY__AOPERATION);

		// Create enums
		aSimpleTypeEEnum = createEEnum(ASIMPLE_TYPE);

		// Create data types
		aStringEDataType = createEDataType(ASTRING);
		aIntegerEDataType = createEDataType(AINTEGER);
		aBooleanEDataType = createEDataType(ABOOLEAN);
		aRealEDataType = createEDataType(AREAL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		AcorePackage theAcorePackage = (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);
		ValuesPackage theValuesPackage = (ValuesPackage) EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI);
		AnnotationsPackage theAnnotationsPackage = (AnnotationsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aClassifierEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aDataTypeEClass.getESuperTypes().add(this.getAClassifier());
		aEnumerationEClass.getESuperTypes().add(this.getADataType());
		aClassTypeEClass.getESuperTypes().add(this.getAClassifier());
		aClassTypeEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotatable());
		aFeatureEClass.getESuperTypes().add(this.getAProperty());
		aFeatureEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotatable());
		aAttributeEClass.getESuperTypes().add(this.getAFeature());
		aReferenceEClass.getESuperTypes().add(this.getAFeature());
		aOperationEClass.getESuperTypes().add(theAbstractionsPackage.getATyped());
		aOperationEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aOperationEClass.getESuperTypes().add(theAbstractionsPackage.getAAnnotatable());
		aParameterEClass.getESuperTypes().add(theAbstractionsPackage.getAVariable());
		aPropertyEClass.getESuperTypes().add(theAbstractionsPackage.getATyped());
		aPropertyEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());

		// Initialize classes, features, and operations; add parameters
		initEClass(aClassifierEClass, AClassifier.class, "AClassifier", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAClassifier_ASpecializedClassifier(), this.getAClassifier(), null, "aSpecializedClassifier",
				null, 0, -1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveDataType(), ecorePackage.getEBooleanObject(), "aActiveDataType", null, 0,
				1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveEnumeration(), ecorePackage.getEBooleanObject(), "aActiveEnumeration",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AActiveClass(), ecorePackage.getEBooleanObject(), "aActiveClass", null, 0, 1,
				AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AContainingPackage(), theAcorePackage.getAPackage(), null, "aContainingPackage",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AAsDataType(), this.getADataType(), null, "aAsDataType", null, 0, 1,
				AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassifier_AAsClass(), this.getAClassType(), null, "aAsClass", null, 0, 1, AClassifier.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassifier_AIsStringClassifier(), ecorePackage.getEBooleanObject(), "aIsStringClassifier",
				null, 0, 1, AClassifier.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID,
				IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAClassifier__AAssignableTo__AClassifier(), ecorePackage.getEBooleanObject(),
				"aAssignableTo", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAClassifier(), "classifier", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAClassifier__AAllProperty(), this.getAProperty(), "aAllProperty", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(aDataTypeEClass, ADataType.class, "ADataType", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getADataType_ASpecializedDataType(), this.getADataType(), null, "aSpecializedDataType", null, 0,
				-1, ADataType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getADataType_ADataTypePackage(), theAcorePackage.getAPackage(), null, "aDataTypePackage", null,
				0, 1, ADataType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aEnumerationEClass, AEnumeration.class, "AEnumeration", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAEnumeration_ALiteral(), theValuesPackage.getALiteral(), null, "aLiteral", null, 0, -1,
				AEnumeration.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aClassTypeEClass, AClassType.class, "AClassType", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAClassType_AAbstract(), ecorePackage.getEBooleanObject(), "aAbstract", null, 0, 1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_ASpecializedClass(), this.getAClassType(), null, "aSpecializedClass", null, 0, -1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AFeature(), this.getAFeature(), null, "aFeature", null, 0, -1, AClassType.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AOperation(), this.getAOperation(), null, "aOperation", null, 0, -1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AAllFeature(), this.getAFeature(), null, "aAllFeature", null, 0, -1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AAllOperation(), this.getAOperation(), null, "aAllOperation", null, 0, -1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AObjectLabel(), theAnnotationsPackage.getAObjectLabel(), null, "aObjectLabel",
				null, 0, 1, AClassType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getAClassType_ASuperTypesLabel(), ecorePackage.getEString(), "aSuperTypesLabel", null, 0, 1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getAClassType_AAllStoredFeature(), this.getAFeature(), null, "aAllStoredFeature", null, 0, -1,
				AClassType.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getAClassType__ACreate(), theValuesPackage.getAObject(), "aCreate", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAClassType__AFeatureFromName__String(), this.getAFeature(), "aFeatureFromName", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "featureName", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAClassType__AOperationFromNameAndTypes__String_AClassifier(), this.getAOperation(),
				"aOperationFromNameAndTypes", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "operationName", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAClassifier(), "parameterType", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getAClassType__AAllGeneralizedClass(), this.getAClassType(), "aAllGeneralizedClass", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getAClassType__AAllSpecializedClass(), this.getAClassType(), "aAllSpecializedClass", 0, -1,
				IS_UNIQUE, IS_ORDERED);

		initEOperation(getAClassType__AAllProperty(), this.getAProperty(), "aAllProperty", 0, -1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(aFeatureEClass, AFeature.class, "AFeature", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getAFeature_AStored(), ecorePackage.getEBooleanObject(), "aStored", null, 0, 1, AFeature.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_APersisted(), ecorePackage.getEBooleanObject(), "aPersisted", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_AChangeable(), ecorePackage.getEBooleanObject(), "aChangeable", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAFeature_AActiveFeature(), ecorePackage.getEBooleanObject(), "aActiveFeature", null, 0, 1,
				AFeature.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aAttributeEClass, AAttribute.class, "AAttribute", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAAttribute_ADataType(), this.getADataType(), null, "aDataType", null, 1, 1, AAttribute.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAAttribute_AActiveAttribute(), ecorePackage.getEBooleanObject(), "aActiveAttribute", null, 0,
				1, AAttribute.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aReferenceEClass, AReference.class, "AReference", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAReference_AClassType(), this.getAClassType(), null, "aClassType", null, 1, 1,
				AReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getAReference_AContainement(), ecorePackage.getEBooleanObject(), "aContainement", null, 0, 1,
				AReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getAReference_AActiveReference(), ecorePackage.getEBooleanObject(), "aActiveReference", null, 0,
				1, AReference.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aOperationEClass, AOperation.class, "AOperation", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAOperation_AParameter(), this.getAParameter(), null, "aParameter", null, 0, -1,
				AOperation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAOperation_AContainingProperty(), this.getAProperty(), null, "aContainingProperty", null, 0,
				1, AOperation.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aParameterEClass, AParameter.class, "AParameter", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAParameter_AContainingOperation(), this.getAOperation(), null, "aContainingOperation", null,
				0, 1, AParameter.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aPropertyEClass, AProperty.class, "AProperty", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAProperty_AContainingClass(), this.getAClassType(), null, "aContainingClass", null, 0, 1,
				AProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAProperty_AOperation(), this.getAOperation(), null, "aOperation", null, 0, -1,
				AProperty.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(aSimpleTypeEEnum, ASimpleType.class, "ASimpleType");
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.NONE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.STRING);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.BOOLEAN);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.REAL);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.INTEGER);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.DATE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.OBJECT);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.PACKAGE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.CLASS);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.CLASSIFIER);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.DATA_TYPE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.ENUMERATION);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.LITERAL);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.FEATURE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.ATTRIBUTE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.REFERENCE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.OPERATION);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.PARAMETER);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.ANNOTATION);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.KEY_VALUE);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.NAMED_ELEMENT);
		addEEnumLiteral(aSimpleTypeEEnum, ASimpleType.TYPED_ELEMENT);

		// Initialize data types
		initEDataType(aStringEDataType, String.class, "AString", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aIntegerEDataType, int.class, "AInteger", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aBooleanEDataType, boolean.class, "ABoolean", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);
		initEDataType(aRealEDataType, double.class, "AReal", IS_SERIALIZABLE, !IS_GENERATED_INSTANCE_CLASS);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
		// http://www.xocl.org/OVERRIDE_EDITORCONFIG
		createOVERRIDE_EDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aClassifierEClass, source, new String[] { "mName", "Classifier" });
		addAnnotation(getAClassifier__AAssignableTo__AClassifier(), source, new String[] { "mName", "Assignable To" });
		addAnnotation(getAClassifier__AAllProperty(), source, new String[] { "mName", "All Property" });
		addAnnotation(getAClassifier_ASpecializedClassifier(), source,
				new String[] { "mName", "Specialized Classifier" });
		addAnnotation(getAClassifier_AActiveDataType(), source, new String[] { "mName", "Active Data Type" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source, new String[] { "mName", "Active Enumeration" });
		addAnnotation(getAClassifier_AActiveClass(), source, new String[] { "mName", "Active Class" });
		addAnnotation(getAClassifier_AContainingPackage(), source, new String[] { "mName", "Containing Package" });
		addAnnotation(getAClassifier_AAsDataType(), source, new String[] { "mName", "As Data Type" });
		addAnnotation(getAClassifier_AAsClass(), source, new String[] { "mName", "As Class" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source, new String[] { "mName", "Is String Classifier" });
		addAnnotation(aDataTypeEClass, source, new String[] { "mName", "Data Type" });
		addAnnotation(getADataType_ASpecializedDataType(), source, new String[] { "mName", "Specialized Data Type" });
		addAnnotation(getADataType_ADataTypePackage(), source, new String[] { "mName", "Data Type Package" });
		addAnnotation(aStringEDataType, source, new String[] { "mName", "String" });
		addAnnotation(aIntegerEDataType, source, new String[] { "mName", "Integer" });
		addAnnotation(aBooleanEDataType, source, new String[] { "mName", "Boolean" });
		addAnnotation(aRealEDataType, source, new String[] { "mName", "Real" });
		addAnnotation(aEnumerationEClass, source, new String[] { "mName", "Enumeration" });
		addAnnotation(aClassTypeEClass, source, new String[] { "mName", "Class Type" });
		addAnnotation(getAClassType__ACreate(), source, new String[] { "mName", "Create" });
		addAnnotation(getAClassType__AFeatureFromName__String(), source, new String[] { "mName", "Feature From Name" });
		addAnnotation(getAClassType__AOperationFromNameAndTypes__String_AClassifier(), source,
				new String[] { "mName", "Operation From Name And Types" });
		addAnnotation(getAClassType__AAllGeneralizedClass(), source, new String[] { "mName", "All Generalized Class" });
		addAnnotation(getAClassType__AAllSpecializedClass(), source, new String[] { "mName", "All Specialized Class" });
		addAnnotation(getAClassType__AAllProperty(), source, new String[] { "mName", "All Property" });
		addAnnotation(getAClassType_AAbstract(), source, new String[] { "mName", "Abstract" });
		addAnnotation(getAClassType_ASpecializedClass(), source, new String[] { "mName", "Specialized Class" });
		addAnnotation(getAClassType_AAllFeature(), source, new String[] { "mName", "All Feature" });
		addAnnotation(getAClassType_AAllOperation(), source, new String[] { "mName", "All Operation" });
		addAnnotation(getAClassType_ASuperTypesLabel(), source, new String[] { "mName", "Super Types Label" });
		addAnnotation(getAClassType_AAllStoredFeature(), source, new String[] { "mName", "All Stored Feature" });
		addAnnotation(aFeatureEClass, source, new String[] { "mName", "Feature" });
		addAnnotation(getAFeature_AStored(), source, new String[] { "mName", "Stored" });
		addAnnotation(getAFeature_APersisted(), source, new String[] { "mName", "Persisted" });
		addAnnotation(getAFeature_AChangeable(), source, new String[] { "mName", "Changeable" });
		addAnnotation(getAFeature_AActiveFeature(), source, new String[] { "mName", "Active Feature" });
		addAnnotation(aAttributeEClass, source, new String[] { "mName", "Attribute" });
		addAnnotation(getAAttribute_AActiveAttribute(), source, new String[] { "mName", "Active Attribute" });
		addAnnotation(aReferenceEClass, source, new String[] { "mName", "Reference" });
		addAnnotation(getAReference_AContainement(), source, new String[] { "mName", "Containement" });
		addAnnotation(getAReference_AActiveReference(), source, new String[] { "mName", "Active Reference" });
		addAnnotation(aOperationEClass, source, new String[] { "mName", "Operation" });
		addAnnotation(getAOperation_AContainingProperty(), source, new String[] { "mName", "Containing Property" });
		addAnnotation(aParameterEClass, source, new String[] { "mName", "Parameter" });
		addAnnotation(getAParameter_AContainingOperation(), source, new String[] { "mName", "Containing Operation" });
		addAnnotation(aPropertyEClass, source, new String[] { "mName", "Property" });
		addAnnotation(getAProperty_AContainingClass(), source, new String[] { "mName", "Containing Class" });
		addAnnotation(aSimpleTypeEEnum, source, new String[] { "mName", "Simple Type" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getAClassifier__AAssignableTo__AClassifier(), source, new String[] { "body",
				"if (let e0: Boolean = self = classifier in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else let e0: Boolean = aSpecializedClassifier.aAssignableTo(classifier)->reject(oclIsUndefined())->asOrderedSet()->includes(true)   in \n if e0.oclIsInvalid() then null else e0 endif\nendif\n" });
		addAnnotation(getAClassifier__AAllProperty(), source, new String[] { "body", "OrderedSet{}\n" });
		addAnnotation(getAClassifier_ASpecializedClassifier(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassifier_AActiveDataType(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AActiveClass(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "derive", "let nl: acore::APackage = null in nl\n" });
		addAnnotation(getAClassifier_AAsDataType(), source, new String[] { "derive",
				"if (let e0: Boolean = if (aActiveDataType)= true \n then true \n else if (aActiveEnumeration)= true \n then true \nelse if ((aActiveDataType)= null or (aActiveEnumeration)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::ADataType)\n    then chain.oclAsType(acore::classifiers::ADataType)\n    else null\n  endif\n  endif\n  else null\nendif\n" });
		addAnnotation(getAClassifier_AAsClass(), source, new String[] { "derive",
				"if (aActiveClass) \n  =true \nthen let chain: acore::classifiers::AClassifier = self in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AClassType)\n    then chain.oclAsType(acore::classifiers::AClassType)\n    else null\n  endif\n  endif\n  else null\nendif\n" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source, new String[] { "derive",
				"let e1: Boolean = if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif))= false \n then false \n else if (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= false \n then false \nelse if ((let e2: Boolean = if aContainingComponent.oclIsUndefined()\n  then null\n  else aContainingComponent.aUri\nendif = \'http://www.langlets.org/ACoreC/ACore/Classifiers\' in \n if e2.oclIsInvalid() then null else e2 endif)= null or (let e3: Boolean = aName = \'AString\' in \n if e3.oclIsInvalid() then null else e3 endif)= null) = true \n then null \n else true endif endif endif in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getADataType_ASpecializedDataType(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getADataType_ADataTypePackage(), source, new String[] { "derive", "aContainingPackage\n" });
		addAnnotation(getAEnumeration_ALiteral(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassType__ACreate(), source,
				new String[] { "body", "let nl: acore::values::AObject = null in nl\n" });
		addAnnotation(getAClassType__AFeatureFromName__String(), source, new String[] { "body",
				"let fs: OrderedSet(acore::classifiers::AFeature)  = aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | let e0: Boolean = it.aName = featureName in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nif (let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->isEmpty().oclIsUndefined() \n then null \n else chain->isEmpty()\n  endif) \n  =true \nthen null\n  else let chain: OrderedSet(acore::classifiers::AFeature)  = fs in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\nendif\n" });
		addAnnotation(getAClassType__AOperationFromNameAndTypes__String_AClassifier(), source,
				new String[] { "body", "let nl: acore::classifiers::AOperation = null in nl\n" });
		addAnnotation(getAClassType__AAllGeneralizedClass(), source, new String[] { "body", "OrderedSet{}\n" });
		addAnnotation(getAClassType__AAllSpecializedClass(), source, new String[] { "body", "OrderedSet{}\n" });
		addAnnotation(getAClassType__AAllProperty(), source, new String[] { "body",
				"let var0: OrderedSet(acore::classifiers::AFeature)  = let e1: OrderedSet(acore::classifiers::AFeature)  = self.aAllFeature->asOrderedSet()->union(let chain: OrderedSet(acore::classifiers::AProperty)  = aAllOperation.aContainingProperty->reject(oclIsUndefined())->asOrderedSet() in\nchain->iterate(i:acore::classifiers::AProperty; r: OrderedSet(acore::classifiers::AFeature)=OrderedSet{} | if i.oclIsKindOf(acore::classifiers::AFeature) then r->including(i.oclAsType(acore::classifiers::AFeature))->asOrderedSet() \n else r endif)) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif in\nvar0\n" });
		addAnnotation(getAClassType_AAbstract(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAClassType_ASpecializedClass(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassType_AFeature(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassType_AOperation(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAClassType_AAllFeature(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::classifiers::AFeature)  = aFeature->asOrderedSet()->union(aSpecializedClass.aAllFeature->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getAClassType_AAllOperation(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::classifiers::AOperation)  = aOperation->asOrderedSet()->union(aSpecializedClass.aAllOperation->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getAClassType_ASuperTypesLabel(), source, new String[] { "derive",
				"let length: Integer = let chain: OrderedSet(acore::classifiers::AClassType)  = aSpecializedClass->asOrderedSet() in\nif chain->size().oclIsUndefined() \n then null \n else chain->size()\n  endif in\nlet superTypeNames: OrderedSet(String)  = aSpecializedClass.aName->reject(oclIsUndefined())->asOrderedSet() in\nif (let e0: Boolean = length = 0 in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen \'\' else if (let e0: Boolean = length = 1 in \n if e0.oclIsInvalid() then null else e0 endif)=true then (let e0: String = \'->\'.concat(let chain01: OrderedSet(String)  = superTypeNames in\nif chain01->first().oclIsUndefined() \n then null \n else chain01->first()\n  endif) in \n if e0.oclIsInvalid() then null else e0 endif)\n  else (let e0: String = \'->(\'.concat(aListOfStringToStringWithSeparator(superTypeNames)).concat(\')\') in \n if e0.oclIsInvalid() then null else e0 endif)\nendif endif\n" });
		addAnnotation(getAClassType_AAllStoredFeature(), source, new String[] { "derive",
				"aAllFeature->asOrderedSet()->select(it: acore::classifiers::AFeature | it.aStored)->asOrderedSet()->excluding(null)->asOrderedSet() \n" });
		addAnnotation(getAFeature_AStored(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAFeature_APersisted(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "derive", "if (aStored) \n  =true \nthen true\n  else false\nendif\n" });
		addAnnotation(getAFeature_AActiveFeature(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "derive", "let nl: acore::classifiers::ADataType = null in nl\n" });
		addAnnotation(getAAttribute_AActiveAttribute(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAReference_AClassType(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassType = null in nl\n" });
		addAnnotation(getAReference_AContainement(), source, new String[] { "derive", "false\n" });
		addAnnotation(getAReference_AActiveReference(), source, new String[] { "derive", "true\n" });
		addAnnotation(getAOperation_AParameter(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAOperation_AContainingProperty(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAParameter_AContainingOperation(), source, new String[] { "derive", "null\n" });
		addAnnotation(getAProperty_AContainingClass(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassType = null in nl\n" });
		addAnnotation(getAProperty_AOperation(), source, new String[] { "derive", "null\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getAClassifier__AAssignableTo__AClassifier(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassifier__AAllProperty(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassifier_ASpecializedClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AActiveClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AAsDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AAsClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Package" });
		addAnnotation(getADataType_ASpecializedDataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getADataType_ADataTypePackage(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAEnumeration_ALiteral(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType__ACreate(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassType__AFeatureFromName__String(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassType__AOperationFromNameAndTypes__String_AClassifier(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassType__AAllGeneralizedClass(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassType__AAllSpecializedClass(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "true" });
		addAnnotation(getAClassType_AAbstract(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_ASpecializedClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_AFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_AOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_AAllFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_AAllOperation(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAClassType_AObjectLabel(), source,
				new String[] { "propertyCategory", "z A Core/Semantics", "createColumn", "false" });
		addAnnotation(getAClassType_ASuperTypesLabel(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core Helpers" });
		addAnnotation(getAClassType_AAllStoredFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core Helpers" });
		addAnnotation(getAFeature_AStored(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAFeature_APersisted(), source,
				new String[] { "createColumn", "true", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAFeature_AActiveFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAAttribute_AActiveAttribute(), source,
				new String[] { "createColumn", "true", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAReference_AClassType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAReference_AContainement(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAReference_AActiveReference(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAOperation_AParameter(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAOperation_AContainingProperty(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "false" });
		addAnnotation(getAParameter_AContainingOperation(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "false" });
		addAnnotation(getAProperty_AContainingClass(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Classifiers" });
		addAnnotation(getAProperty_AOperation(), source,
				new String[] { "propertyCategory", "z A Core/Classifiers", "createColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getAClassifier_ASpecializedClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveEnumeration(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AActiveClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AContainingPackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AAsDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AAsClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassifier_AIsStringClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataType_ASpecializedDataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataType_ADataTypePackage(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAEnumeration_ALiteral(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AAbstract(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_ASpecializedClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AOperation(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AAllFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AAllOperation(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_ASuperTypesLabel(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAClassType_AAllStoredFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AStored(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_APersisted(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AChangeable(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAFeature_AActiveFeature(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAttribute_ADataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAAttribute_AActiveAttribute(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AClassType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AContainement(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAReference_AActiveReference(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAOperation_AParameter(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAProperty_AContainingClass(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aDataTypeEClass, source, new String[] { "aActiveDataTypeDerive", "true\n",
				"aSpecializedClassifierDerive", "aSpecializedDataType->asOrderedSet()\n" });
		addAnnotation(aEnumerationEClass, source,
				new String[] { "aActiveEnumerationDerive", "true\n", "aActiveDataTypeDerive", "false\n" });
		addAnnotation(aClassTypeEClass, source, new String[] { "aActiveClassDerive", "true\n",
				"aSpecializedClassifierDerive", "aSpecializedClass->asOrderedSet()\n", "aLabelDerive",
				"let e1: String = aName.concat(aSuperTypesLabel) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(aAttributeEClass, source,
				new String[] { "aClassifierDerive", "aDataType\n", "aActiveFeatureDerive", "aActiveAttribute\n" });
		addAnnotation(aReferenceEClass, source,
				new String[] { "aClassifierDerive", "aClassType\n", "aActiveFeatureDerive", "aActiveReference\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_EDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_EDITORCONFIG";
		addAnnotation(aAttributeEClass, source, new String[] { "aClassifierCreateColumn", "false" });
		addAnnotation(aReferenceEClass, source, new String[] { "aClassifierCreateColumn", "false" });
	}

} //ClassifiersPackageImpl
