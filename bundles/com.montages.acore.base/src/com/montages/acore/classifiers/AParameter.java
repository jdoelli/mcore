/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.abstractions.AVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AParameter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AParameter#getAContainingOperation <em>AContaining Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAParameter()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Parameter'"
 * @generated
 */

public interface AParameter extends AVariable {
	/**
	 * Returns the value of the '<em><b>AContaining Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Operation</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAParameter_AContainingOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Operation'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='false'"
	 * @generated
	 */
	AOperation getAContainingOperation();

} // AParameter
