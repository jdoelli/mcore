/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.values.ALiteral;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AEnumeration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AEnumeration#getALiteral <em>ALiteral</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAEnumeration()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Enumeration'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aActiveEnumerationDerive='true\n' aActiveDataTypeDerive='false\n'"
 * @generated
 */

public interface AEnumeration extends ADataType {
	/**
	 * Returns the value of the '<em><b>ALiteral</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.ALiteral}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ALiteral</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ALiteral</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAEnumeration_ALiteral()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<ALiteral> getALiteral();

} // AEnumeration
