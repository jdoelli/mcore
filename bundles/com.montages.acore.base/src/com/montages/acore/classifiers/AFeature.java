/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.abstractions.AAnnotatable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AFeature</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AFeature#getAStored <em>AStored</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AFeature#getAPersisted <em>APersisted</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AFeature#getAChangeable <em>AChangeable</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AFeature#getAActiveFeature <em>AActive Feature</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAFeature()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Feature'"
 * @generated
 */

public interface AFeature extends AProperty, AAnnotatable {
	/**
	 * Returns the value of the '<em><b>AStored</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AStored</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AStored</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAFeature_AStored()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Stored'"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAStored();

	/**
	 * Returns the value of the '<em><b>APersisted</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>APersisted</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>APersisted</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAFeature_APersisted()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Persisted'"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='true' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAPersisted();

	/**
	 * Returns the value of the '<em><b>AChangeable</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AChangeable</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AChangeable</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAFeature_AChangeable()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Changeable'"
	 *        annotation="http://www.xocl.org/OCL derive='if (aStored) \n  =true \nthen true\n  else false\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAChangeable();

	/**
	 * Returns the value of the '<em><b>AActive Feature</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Feature</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Feature</em>' attribute.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAFeature_AActiveFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveFeature();

} // AFeature
