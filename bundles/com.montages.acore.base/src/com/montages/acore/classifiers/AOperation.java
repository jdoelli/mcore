/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.abstractions.AAnnotatable;
import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.ATyped;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AOperation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AOperation#getAParameter <em>AParameter</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AOperation#getAContainingProperty <em>AContaining Property</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAOperation()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Operation'"
 * @generated
 */

public interface AOperation extends ATyped, ANamed, AAnnotatable {
	/**
	 * Returns the value of the '<em><b>AParameter</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AParameter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AParameter</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AParameter</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAOperation_AParameter()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AParameter> getAParameter();

	/**
	 * Returns the value of the '<em><b>AContaining Property</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Property</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Property</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAOperation_AContainingProperty()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Property'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='false'"
	 * @generated
	 */
	AProperty getAContainingProperty();

} // AOperation
