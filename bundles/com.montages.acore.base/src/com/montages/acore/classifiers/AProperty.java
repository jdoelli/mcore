/**
 */

package com.montages.acore.classifiers;

import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.ATyped;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AProperty</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.classifiers.AProperty#getAContainingClass <em>AContaining Class</em>}</li>
 *   <li>{@link com.montages.acore.classifiers.AProperty#getAOperation <em>AOperation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.classifiers.ClassifiersPackage#getAProperty()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Property'"
 * @generated
 */

public interface AProperty extends ATyped, ANamed {
	/**
	 * Returns the value of the '<em><b>AContaining Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Class</em>' reference.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAProperty_AContainingClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Class'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AClassType = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Classifiers'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	AClassType getAContainingClass();

	/**
	 * Returns the value of the '<em><b>AOperation</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AOperation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AOperation</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AOperation</em>' reference list.
	 * @see com.montages.acore.classifiers.ClassifiersPackage#getAProperty_AOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Classifiers' createColumn='false'"
	 * @generated
	 */
	EList<AOperation> getAOperation();

} // AProperty
