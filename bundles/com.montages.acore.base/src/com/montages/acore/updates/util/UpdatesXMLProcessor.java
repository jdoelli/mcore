/**
 */
package com.montages.acore.updates.util;

import com.montages.acore.updates.UpdatesPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class UpdatesXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UpdatesXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		UpdatesPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the UpdatesResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new UpdatesResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new UpdatesResourceFactoryImpl());
		}
		return registrations;
	}

} //UpdatesXMLProcessor
