/**
 */
package com.montages.acore.updates;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.updates.UpdatesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore'"
 * @generated
 */
public interface UpdatesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "updates";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Updates";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.updates";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UpdatesPackage eINSTANCE = com.montages.acore.updates.impl.UpdatesPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.updates.impl.AUpdateImpl <em>AUpdate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.updates.impl.AUpdateImpl
	 * @see com.montages.acore.updates.impl.UpdatesPackageImpl#getAUpdate()
	 * @generated
	 */
	int AUPDATE = 0;

	/**
	 * The feature id for the '<em><b>AObject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE__AOBJECT = 0;

	/**
	 * The feature id for the '<em><b>AUpdate Mode</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE__AUPDATE_MODE = 1;

	/**
	 * The feature id for the '<em><b>AValue</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE__AVALUE = 2;

	/**
	 * The feature id for the '<em><b>ADummy2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE__ADUMMY2 = 3;

	/**
	 * The number of structural features of the '<em>AUpdate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>AUpdate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AUPDATE_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.updates.AUpdateMode <em>AUpdate Mode</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.updates.AUpdateMode
	 * @see com.montages.acore.updates.impl.UpdatesPackageImpl#getAUpdateMode()
	 * @generated
	 */
	int AUPDATE_MODE = 1;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.updates.AUpdate <em>AUpdate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AUpdate</em>'.
	 * @see com.montages.acore.updates.AUpdate
	 * @generated
	 */
	EClass getAUpdate();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.updates.AUpdate#getAObject <em>AObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AObject</em>'.
	 * @see com.montages.acore.updates.AUpdate#getAObject()
	 * @see #getAUpdate()
	 * @generated
	 */
	EReference getAUpdate_AObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.updates.AUpdate#getAUpdateMode <em>AUpdate Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AUpdate Mode</em>'.
	 * @see com.montages.acore.updates.AUpdate#getAUpdateMode()
	 * @see #getAUpdate()
	 * @generated
	 */
	EAttribute getAUpdate_AUpdateMode();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.updates.AUpdate#getAValue <em>AValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AValue</em>'.
	 * @see com.montages.acore.updates.AUpdate#getAValue()
	 * @see #getAUpdate()
	 * @generated
	 */
	EReference getAUpdate_AValue();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.updates.AUpdate#getADummy2 <em>ADummy2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>ADummy2</em>'.
	 * @see com.montages.acore.updates.AUpdate#getADummy2()
	 * @see #getAUpdate()
	 * @generated
	 */
	EAttribute getAUpdate_ADummy2();

	/**
	 * Returns the meta object for enum '{@link com.montages.acore.updates.AUpdateMode <em>AUpdate Mode</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>AUpdate Mode</em>'.
	 * @see com.montages.acore.updates.AUpdateMode
	 * @generated
	 */
	EEnum getAUpdateMode();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UpdatesFactory getUpdatesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.updates.impl.AUpdateImpl <em>AUpdate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.updates.impl.AUpdateImpl
		 * @see com.montages.acore.updates.impl.UpdatesPackageImpl#getAUpdate()
		 * @generated
		 */
		EClass AUPDATE = eINSTANCE.getAUpdate();

		/**
		 * The meta object literal for the '<em><b>AObject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUPDATE__AOBJECT = eINSTANCE.getAUpdate_AObject();

		/**
		 * The meta object literal for the '<em><b>AUpdate Mode</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUPDATE__AUPDATE_MODE = eINSTANCE.getAUpdate_AUpdateMode();

		/**
		 * The meta object literal for the '<em><b>AValue</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AUPDATE__AVALUE = eINSTANCE.getAUpdate_AValue();

		/**
		 * The meta object literal for the '<em><b>ADummy2</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute AUPDATE__ADUMMY2 = eINSTANCE.getAUpdate_ADummy2();

		/**
		 * The meta object literal for the '{@link com.montages.acore.updates.AUpdateMode <em>AUpdate Mode</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.updates.AUpdateMode
		 * @see com.montages.acore.updates.impl.UpdatesPackageImpl#getAUpdateMode()
		 * @generated
		 */
		EEnum AUPDATE_MODE = eINSTANCE.getAUpdateMode();

	}

} //UpdatesPackage
