/**
 */

package com.montages.acore;

import com.montages.acore.values.AObject;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AResource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.AResource#getAObject <em>AObject</em>}</li>
 *   <li>{@link com.montages.acore.AResource#getAActiveResource <em>AActive Resource</em>}</li>
 *   <li>{@link com.montages.acore.AResource#getARootObjectPackage <em>ARoot Object Package</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.AcorePackage#getAResource()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Resource'"
 * @generated
 */

public interface AResource extends AStructuringElement {
	/**
	 * Returns the value of the '<em><b>AObject</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AObject</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AObject</em>' reference list.
	 * @see com.montages.acore.AcorePackage#getAResource_AObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	EList<AObject> getAObject();

	/**
	 * Returns the value of the '<em><b>AActive Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AActive Resource</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AActive Resource</em>' attribute.
	 * @see com.montages.acore.AcorePackage#getAResource_AActiveResource()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Active Resource'"
	 *        annotation="http://www.xocl.org/OCL derive='true\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	Boolean getAActiveResource();

	/**
	 * Returns the value of the '<em><b>ARoot Object Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ARoot Object Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ARoot Object Package</em>' reference.
	 * @see com.montages.acore.AcorePackage#getAResource_ARootObjectPackage()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Root Object Package'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::APackage = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core'"
	 *        annotation="http://www.xocl.org/GENMODEL propertyFilterFlags='org.eclipse.ui.views.properties.expert'"
	 * @generated
	 */
	APackage getARootObjectPackage();

} // AResource
