/**
 */

package com.montages.acore.annotations.impl;

import com.montages.acore.AComponent;
import com.montages.acore.APackage;

import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ANamed;
import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.annotations.AObjectConstraint;
import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AObject Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getALabel <em>ALabel</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getAKindBase <em>AKind Base</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getARenderedKind <em>ARendered Kind</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getAContainingComponent <em>AContaining Component</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATPackageUri <em>AT Package Uri</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATClassifierName <em>AT Classifier Name</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATFeatureName <em>AT Feature Name</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATPackage <em>AT Package</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATClassifier <em>AT Classifier</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATFeature <em>AT Feature</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getATCoreAStringClass <em>AT Core AString Class</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getAName <em>AName</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getAUndefinedNameConstant <em>AUndefined Name Constant</em>}</li>
 *   <li>{@link com.montages.acore.annotations.impl.AObjectConstraintImpl#getABusinessName <em>ABusiness Name</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AObjectConstraintImpl extends AClassAnnotationImpl implements AObjectConstraint {
	/**
	 * The default value of the '{@link #getALabel() <em>ALabel</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel()
	 * @generated
	 * @ordered
	 */
	protected static final String ALABEL_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAKindBase() <em>AKind Base</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase()
	 * @generated
	 * @ordered
	 */
	protected static final String AKIND_BASE_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getARenderedKind() <em>ARendered Kind</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind()
	 * @generated
	 * @ordered
	 */
	protected static final String ARENDERED_KIND_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getATPackageUri() <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackageUri()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_PACKAGE_URI_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATPackageUri() <em>AT Package Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackageUri()
	 * @generated
	 * @ordered
	 */
	protected String aTPackageUri = AT_PACKAGE_URI_EDEFAULT;

	/**
	 * This is true if the AT Package Uri attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTPackageUriESet;

	/**
	 * The default value of the '{@link #getATClassifierName() <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifierName()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_CLASSIFIER_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATClassifierName() <em>AT Classifier Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifierName()
	 * @generated
	 * @ordered
	 */
	protected String aTClassifierName = AT_CLASSIFIER_NAME_EDEFAULT;

	/**
	 * This is true if the AT Classifier Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTClassifierNameESet;

	/**
	 * The default value of the '{@link #getATFeatureName() <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeatureName()
	 * @generated
	 * @ordered
	 */
	protected static final String AT_FEATURE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getATFeatureName() <em>AT Feature Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeatureName()
	 * @generated
	 * @ordered
	 */
	protected String aTFeatureName = AT_FEATURE_NAME_EDEFAULT;

	/**
	 * This is true if the AT Feature Name attribute has been set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	protected boolean aTFeatureNameESet;

	/**
	 * The default value of the '{@link #getAName() <em>AName</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName()
	 * @generated
	 * @ordered
	 */
	protected static final String ANAME_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getAUndefinedNameConstant() <em>AUndefined Name Constant</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant()
	 * @generated
	 * @ordered
	 */
	protected static final String AUNDEFINED_NAME_CONSTANT_EDEFAULT = null;

	/**
	 * The default value of the '{@link #getABusinessName() <em>ABusiness Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName()
	 * @generated
	 * @ordered
	 */
	protected static final String ABUSINESS_NAME_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentLevel <em>AIndent Level</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentLevel
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentLevelBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentationSpaces <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentationSpacesBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aIndentationSpaces <em>AIndentation Spaces</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aIndentationSpaces
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aIndentationSpacesecoreEIntegerObjectBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aStringOrMissing <em>AString Or Missing</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aStringOrMissing
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aStringOrMissingecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aStringIsEmpty <em>AString Is Empty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aStringIsEmpty
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aStringIsEmptyecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aListOfStringToStringWithSeparator <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aListOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aListOfStringToStringWithSeparator <em>AList Of String To String With Separator</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aListOfStringToStringWithSeparator
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aListOfStringToStringWithSeparatorecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aPackageFromUri <em>APackage From Uri</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aPackageFromUri
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aPackageFromUriecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aClassifierFromUriAndName <em>AClassifier From Uri And Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aClassifierFromUriAndName
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aFeatureFromUriAndNames <em>AFeature From Uri And Names</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aFeatureFromUriAndNames
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAStringClass <em>ACore AString Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAStringClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAStringClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreARealClass <em>ACore AReal Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreARealClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreARealClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAIntegerClass <em>ACore AInteger Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAIntegerClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAIntegerClassBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aCoreAObjectClass <em>ACore AObject Class</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aCoreAObjectClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aCoreAObjectClassBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getALabel <em>ALabel</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getALabel
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aLabelDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAKindBase <em>AKind Base</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAKindBase
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aKindBaseDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARenderedKind <em>ARendered Kind</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARenderedKind
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRenderedKindDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingComponent <em>AContaining Component</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingComponent
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingComponentDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATPackage <em>AT Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATClassifier <em>AT Classifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATClassifier
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTClassifierDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATFeature <em>AT Feature</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATFeature
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTFeatureDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getATCoreAStringClass <em>AT Core AString Class</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getATCoreAStringClass
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aTCoreAStringClassDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAName <em>AName</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aNameDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAUndefinedNameConstant <em>AUndefined Name Constant</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAUndefinedNameConstant
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aUndefinedNameConstantDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getABusinessName <em>ABusiness Name</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getABusinessName
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aBusinessNameDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AObjectConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getALabel() {
		/**
		 * @OCL aRenderedKind
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ALABEL;

		if (aLabelDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aLabelDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aLabelDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAKindBase() {
		/**
		 * @OCL self.eClass().name
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AKIND_BASE;

		if (aKindBaseDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aKindBaseDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aKindBaseDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getARenderedKind() {
		/**
		 * @OCL let e1: String = aIndentationSpaces().concat(let chain12: String = aKindBase in
		if chain12.toUpperCase().oclIsUndefined() 
		then null 
		else chain12.toUpperCase()
		endif) in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ARENDERED_KIND;

		if (aRenderedKindDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRenderedKindDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRenderedKindDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent getAContainingComponent() {
		AComponent aContainingComponent = basicGetAContainingComponent();
		return aContainingComponent != null && aContainingComponent.eIsProxy()
				? (AComponent) eResolveProxy((InternalEObject) aContainingComponent) : aContainingComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AComponent basicGetAContainingComponent() {
		/**
		 * @OCL if self.eContainer().oclIsTypeOf(acore::AComponent)
		then self.eContainer().oclAsType(acore::AComponent)
		else if self.eContainer().oclIsKindOf(acore::abstractions::AElement)
		then self.eContainer().oclAsType(acore::abstractions::AElement).aContainingComponent
		else null endif endif
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__ACONTAINING_COMPONENT;

		if (aContainingComponentDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingComponentDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingComponentDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AComponent result = (AComponent) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATPackageUri() {
		return aTPackageUri;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATPackageUri(String newATPackageUri) {
		String oldATPackageUri = aTPackageUri;
		aTPackageUri = newATPackageUri;
		boolean oldATPackageUriESet = aTPackageUriESet;
		aTPackageUriESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI,
					oldATPackageUri, aTPackageUri, !oldATPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATPackageUri() {
		String oldATPackageUri = aTPackageUri;
		boolean oldATPackageUriESet = aTPackageUriESet;
		aTPackageUri = AT_PACKAGE_URI_EDEFAULT;
		aTPackageUriESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI, oldATPackageUri, AT_PACKAGE_URI_EDEFAULT,
					oldATPackageUriESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATPackageUri() {
		return aTPackageUriESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATClassifierName() {
		return aTClassifierName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATClassifierName(String newATClassifierName) {
		String oldATClassifierName = aTClassifierName;
		aTClassifierName = newATClassifierName;
		boolean oldATClassifierNameESet = aTClassifierNameESet;
		aTClassifierNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME, oldATClassifierName, aTClassifierName,
					!oldATClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATClassifierName() {
		String oldATClassifierName = aTClassifierName;
		boolean oldATClassifierNameESet = aTClassifierNameESet;
		aTClassifierName = AT_CLASSIFIER_NAME_EDEFAULT;
		aTClassifierNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME, oldATClassifierName,
					AT_CLASSIFIER_NAME_EDEFAULT, oldATClassifierNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATClassifierName() {
		return aTClassifierNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getATFeatureName() {
		return aTFeatureName;
	}

	/**
	 * <!-- begin-user-doc -->
	 
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setATFeatureName(String newATFeatureName) {
		String oldATFeatureName = aTFeatureName;
		aTFeatureName = newATFeatureName;
		boolean oldATFeatureNameESet = aTFeatureNameESet;
		aTFeatureNameESet = true;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME, oldATFeatureName, aTFeatureName,
					!oldATFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unsetATFeatureName() {
		String oldATFeatureName = aTFeatureName;
		boolean oldATFeatureNameESet = aTFeatureNameESet;
		aTFeatureName = AT_FEATURE_NAME_EDEFAULT;
		aTFeatureNameESet = false;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.UNSET,
					AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME, oldATFeatureName, AT_FEATURE_NAME_EDEFAULT,
					oldATFeatureNameESet));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetATFeatureName() {
		return aTFeatureNameESet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getATPackage() {
		APackage aTPackage = basicGetATPackage();
		return aTPackage != null && aTPackage.eIsProxy() ? (APackage) eResolveProxy((InternalEObject) aTPackage)
				: aTPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetATPackage() {
		/**
		 * @OCL let p: String = aTPackageUri in
		aPackageFromUri(p)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_PACKAGE;

		if (aTPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getATClassifier() {
		AClassifier aTClassifier = basicGetATClassifier();
		return aTClassifier != null && aTClassifier.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aTClassifier) : aTClassifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetATClassifier() {
		/**
		 * @OCL let p: String = aTPackageUri in
		let c: String = aTClassifierName in
		aClassifierFromUriAndName(p, c)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_CLASSIFIER;

		if (aTClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature getATFeature() {
		AFeature aTFeature = basicGetATFeature();
		return aTFeature != null && aTFeature.eIsProxy() ? (AFeature) eResolveProxy((InternalEObject) aTFeature)
				: aTFeature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature basicGetATFeature() {
		/**
		 * @OCL let p: String = aTPackageUri in
		let c: String = aTClassifierName in
		let f: String = aTFeatureName in
		aFeatureFromUriAndNames(p, c, f)
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_FEATURE;

		if (aTFeatureDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTFeatureDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTFeatureDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AFeature result = (AFeature) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier getATCoreAStringClass() {
		AClassifier aTCoreAStringClass = basicGetATCoreAStringClass();
		return aTCoreAStringClass != null && aTCoreAStringClass.eIsProxy()
				? (AClassifier) eResolveProxy((InternalEObject) aTCoreAStringClass) : aTCoreAStringClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier basicGetATCoreAStringClass() {
		/**
		 * @OCL aCoreAStringClass()
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.AELEMENT__AT_CORE_ASTRING_CLASS;

		if (aTCoreAStringClassDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aTCoreAStringClassDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aTCoreAStringClassDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassifier result = (AClassifier) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAName() {
		/**
		 * @OCL aUndefinedNameConstant
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ANAME;

		if (aNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAUndefinedNameConstant() {
		/**
		 * @OCL '<A Name Is Undefined> '
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT;

		if (aUndefinedNameConstantDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aUndefinedNameConstantDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aUndefinedNameConstantDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getABusinessName() {
		/**
		 * @OCL let chain : String = aName in
		if chain.oclIsUndefined()
		then null
		else chain .camelCaseToBusiness()
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AnnotationsPackage.Literals.AOBJECT_CONSTRAINT;
		EStructuralFeature eFeature = AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME;

		if (aBusinessNameDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aBusinessNameDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, derive, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aBusinessNameDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			String result = (String) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Integer aIndentLevel() {

		/**
		 * @OCL if eContainer().oclIsUndefined() 
		then 0
		else if eContainer().oclIsKindOf(AElement)
		then eContainer().oclAsType(AElement).aIndentLevel() + 1
		else 0 endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(0);
		if (aIndentLevelBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentLevelBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentLevelBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Integer) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aIndentationSpaces() {

		/**
		 * @OCL let numberOfSpaces: Integer = let e1: Integer = aIndentLevel() * 4 in 
		if e1.oclIsInvalid() then null else e1 endif in
		aIndentationSpaces(numberOfSpaces)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(1);
		if (aIndentationSpacesBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentationSpacesBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentationSpacesBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aIndentationSpaces(Integer size) {

		/**
		 * @OCL let sizeMinusOne: Integer = let e1: Integer = size - 1 in 
		if e1.oclIsInvalid() then null else e1 endif in
		if (let e0: Boolean = size < 1 in 
		if e0.oclIsInvalid() then null else e0 endif) 
		=true 
		then ''
		else (let e0: String = aIndentationSpaces(sizeMinusOne).concat(' ') in 
		if e0.oclIsInvalid() then null else e0 endif)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(2);
		if (aIndentationSpacesecoreEIntegerObjectBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aIndentationSpacesecoreEIntegerObjectBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aIndentationSpacesecoreEIntegerObjectBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("size", size);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aStringOrMissing(String p) {

		/**
		 * @OCL if (aStringIsEmpty(p)) 
		=true 
		then 'MISSING'
		else p
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(3);
		if (aStringOrMissingecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aStringOrMissingecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aStringOrMissingecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("p", p);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean aStringIsEmpty(String s) {

		/**
		 * @OCL if ( s.oclIsUndefined()) 
		=true 
		then true else if (let e0: Boolean = '' = let e0: String = s.trim() in 
		if e0.oclIsInvalid() then null else e0 endif in 
		if e0.oclIsInvalid() then null else e0 endif)=true then true
		else false
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(4);
		if (aStringIsEmptyecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aStringIsEmptyecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aStringIsEmptyecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("s", s);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aListOfStringToStringWithSeparator(EList<String> elements, String separator) {

		/**
		 * @OCL let f:String = elements->asOrderedSet()->first() in
		if f.oclIsUndefined()
		then ''
		else if elements-> size()=1 then f else
		let rest:String = elements->excluding(f)->asOrderedSet()->iterate(it:String;ac:String=''|ac.concat(separator).concat(it)) in
		f.concat(rest)
		endif endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(5);
		if (aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aListOfStringToStringWithSeparatorecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			evalEnv.add("separator", separator);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String aListOfStringToStringWithSeparator(EList<String> elements) {

		/**
		 * @OCL let defaultSeparator: String = ', ' in
		aListOfStringToStringWithSeparator(elements->asOrderedSet(), defaultSeparator)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(6);
		if (aListOfStringToStringWithSeparatorecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aListOfStringToStringWithSeparatorecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aListOfStringToStringWithSeparatorecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("elements", elements);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (String) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage aPackageFromUri(String packageUri) {

		/**
		 * @OCL if aContainingComponent.oclIsUndefined()
		then null
		else aContainingComponent.aPackageFromUri(packageUri)
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(7);
		if (aPackageFromUriecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aPackageFromUriecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageFromUriecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("packageUri", packageUri);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (APackage) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aClassifierFromUriAndName(String uri, String name) {

		/**
		 * @OCL let p: acore::APackage = aPackageFromUri(uri) in
		if p = null
		then null
		else p.aClassifierFromName(name) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(8);
		if (aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierFromUriAndNameecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("name", name);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AFeature aFeatureFromUriAndNames(String uri, String className, String featureName) {

		/**
		 * @OCL let c: acore::classifiers::AClassifier = aClassifierFromUriAndName(uri, className) in
		let cAsClass: acore::classifiers::AClassType = let chain: acore::classifiers::AClassifier = c in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AClassType)
		then chain.oclAsType(acore::classifiers::AClassType)
		else null
		endif
		endif in
		if cAsClass = null
		then null
		else cAsClass.aFeatureFromName(featureName) endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(9);
		if (aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureFromUriAndNamesecoreEStringecoreEStringecoreEStringBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("uri", uri);

			evalEnv.add("className", className);

			evalEnv.add("featureName", featureName);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AFeature) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAStringClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAStringName: String = 'AString' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAStringName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(10);
		if (aCoreAStringClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAStringClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAStringClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreARealClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreARealName: String = 'AReal' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreARealName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(11);
		if (aCoreARealClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreARealClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreARealClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAIntegerClass() {

		/**
		 * @OCL let aCoreClassifiersPackageUri: String = 'http://www.langlets.org/ACore/ACore/Classifiers' in
		let aCoreAIntegerName: String = 'AInteger' in
		aClassifierFromUriAndName(aCoreClassifiersPackageUri, aCoreAIntegerName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(12);
		if (aCoreAIntegerClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAIntegerClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAIntegerClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifier aCoreAObjectClass() {

		/**
		 * @OCL let aCoreValuesPackageUri: String = 'http://www.langlets.org/ACore/ACore/Values' in
		let aCoreAObjectName: String = 'AObject' in
		aClassifierFromUriAndName(aCoreValuesPackageUri, aCoreAObjectName)
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (AbstractionsPackage.Literals.AELEMENT);
		EOperation eOperation = AbstractionsPackage.Literals.AELEMENT.getEOperations().get(13);
		if (aCoreAObjectClassBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aCoreAObjectClassBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AnnotationsPackage.PLUGIN_ID, body, helper.getProblems(),
						AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aCoreAObjectClassBodyOCL);
		try {
			XoclErrorHandler.enterContext(AnnotationsPackage.PLUGIN_ID, query,
					AnnotationsPackage.Literals.AOBJECT_CONSTRAINT, eOperation);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ALABEL:
			return getALabel();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AKIND_BASE:
			return getAKindBase();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ARENDERED_KIND:
			return getARenderedKind();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ACONTAINING_COMPONENT:
			if (resolve)
				return getAContainingComponent();
			return basicGetAContainingComponent();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI:
			return getATPackageUri();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME:
			return getATClassifierName();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME:
			return getATFeatureName();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE:
			if (resolve)
				return getATPackage();
			return basicGetATPackage();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER:
			if (resolve)
				return getATClassifier();
			return basicGetATClassifier();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE:
			if (resolve)
				return getATFeature();
			return basicGetATFeature();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CORE_ASTRING_CLASS:
			if (resolve)
				return getATCoreAStringClass();
			return basicGetATCoreAStringClass();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ANAME:
			return getAName();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AUNDEFINED_NAME_CONSTANT:
			return getAUndefinedNameConstant();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ABUSINESS_NAME:
			return getABusinessName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI:
			setATPackageUri((String) newValue);
			return;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME:
			setATClassifierName((String) newValue);
			return;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME:
			setATFeatureName((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI:
			unsetATPackageUri();
			return;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME:
			unsetATClassifierName();
			return;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME:
			unsetATFeatureName();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ALABEL:
			return ALABEL_EDEFAULT == null ? getALabel() != null : !ALABEL_EDEFAULT.equals(getALabel());
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AKIND_BASE:
			return AKIND_BASE_EDEFAULT == null ? getAKindBase() != null : !AKIND_BASE_EDEFAULT.equals(getAKindBase());
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ARENDERED_KIND:
			return ARENDERED_KIND_EDEFAULT == null ? getARenderedKind() != null
					: !ARENDERED_KIND_EDEFAULT.equals(getARenderedKind());
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ACONTAINING_COMPONENT:
			return basicGetAContainingComponent() != null;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI:
			return isSetATPackageUri();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME:
			return isSetATClassifierName();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME:
			return isSetATFeatureName();
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE:
			return basicGetATPackage() != null;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER:
			return basicGetATClassifier() != null;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE:
			return basicGetATFeature() != null;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CORE_ASTRING_CLASS:
			return basicGetATCoreAStringClass() != null;
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ANAME:
			return ANAME_EDEFAULT == null ? getAName() != null : !ANAME_EDEFAULT.equals(getAName());
		case AnnotationsPackage.AOBJECT_CONSTRAINT__AUNDEFINED_NAME_CONSTANT:
			return AUNDEFINED_NAME_CONSTANT_EDEFAULT == null ? getAUndefinedNameConstant() != null
					: !AUNDEFINED_NAME_CONSTANT_EDEFAULT.equals(getAUndefinedNameConstant());
		case AnnotationsPackage.AOBJECT_CONSTRAINT__ABUSINESS_NAME:
			return ABUSINESS_NAME_EDEFAULT == null ? getABusinessName() != null
					: !ABUSINESS_NAME_EDEFAULT.equals(getABusinessName());
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eBaseStructuralFeatureID(int derivedFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.AOBJECT_CONSTRAINT__ALABEL:
				return AbstractionsPackage.AELEMENT__ALABEL;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AKIND_BASE:
				return AbstractionsPackage.AELEMENT__AKIND_BASE;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__ARENDERED_KIND:
				return AbstractionsPackage.AELEMENT__ARENDERED_KIND;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__ACONTAINING_COMPONENT:
				return AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI:
				return AbstractionsPackage.AELEMENT__AT_PACKAGE_URI;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME:
				return AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME:
				return AbstractionsPackage.AELEMENT__AT_FEATURE_NAME;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE:
				return AbstractionsPackage.AELEMENT__AT_PACKAGE;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER:
				return AbstractionsPackage.AELEMENT__AT_CLASSIFIER;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE:
				return AbstractionsPackage.AELEMENT__AT_FEATURE;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CORE_ASTRING_CLASS:
				return AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (derivedFeatureID) {
			case AnnotationsPackage.AOBJECT_CONSTRAINT__ANAME:
				return AbstractionsPackage.ANAMED__ANAME;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__AUNDEFINED_NAME_CONSTANT:
				return AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT;
			case AnnotationsPackage.AOBJECT_CONSTRAINT__ABUSINESS_NAME:
				return AbstractionsPackage.ANAMED__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		return super.eBaseStructuralFeatureID(derivedFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedStructuralFeatureID(int baseFeatureID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.AELEMENT__ALABEL:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__ALABEL;
			case AbstractionsPackage.AELEMENT__AKIND_BASE:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AKIND_BASE;
			case AbstractionsPackage.AELEMENT__ARENDERED_KIND:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__ARENDERED_KIND;
			case AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__ACONTAINING_COMPONENT;
			case AbstractionsPackage.AELEMENT__AT_PACKAGE_URI:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE_URI;
			case AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER_NAME;
			case AbstractionsPackage.AELEMENT__AT_FEATURE_NAME:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE_NAME;
			case AbstractionsPackage.AELEMENT__AT_PACKAGE:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_PACKAGE;
			case AbstractionsPackage.AELEMENT__AT_CLASSIFIER:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CLASSIFIER;
			case AbstractionsPackage.AELEMENT__AT_FEATURE:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_FEATURE;
			case AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AT_CORE_ASTRING_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (baseFeatureID) {
			case AbstractionsPackage.ANAMED__ANAME:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__ANAME;
			case AbstractionsPackage.ANAMED__AUNDEFINED_NAME_CONSTANT:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__AUNDEFINED_NAME_CONSTANT;
			case AbstractionsPackage.ANAMED__ABUSINESS_NAME:
				return AnnotationsPackage.AOBJECT_CONSTRAINT__ABUSINESS_NAME;
			default:
				return -1;
			}
		}
		return super.eDerivedStructuralFeatureID(baseFeatureID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == AElement.class) {
			switch (baseOperationID) {
			case AbstractionsPackage.AELEMENT___AINDENT_LEVEL:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENT_LEVEL;
			case AbstractionsPackage.AELEMENT___AINDENTATION_SPACES:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENTATION_SPACES;
			case AbstractionsPackage.AELEMENT___AINDENTATION_SPACES__INTEGER:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENTATION_SPACES__INTEGER;
			case AbstractionsPackage.AELEMENT___ASTRING_OR_MISSING__STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ASTRING_OR_MISSING__STRING;
			case AbstractionsPackage.AELEMENT___ASTRING_IS_EMPTY__STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ASTRING_IS_EMPTY__STRING;
			case AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;
			case AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;
			case AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___APACKAGE_FROM_URI__STRING;
			case AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;
			case AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;
			case AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_ASTRING_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AREAL_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AINTEGER_CLASS;
			case AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS:
				return AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AOBJECT_CLASS;
			default:
				return -1;
			}
		}
		if (baseClass == ANamed.class) {
			switch (baseOperationID) {
			default:
				return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENT_LEVEL:
			return aIndentLevel();
		case AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENTATION_SPACES:
			return aIndentationSpaces();
		case AnnotationsPackage.AOBJECT_CONSTRAINT___AINDENTATION_SPACES__INTEGER:
			return aIndentationSpaces((Integer) arguments.get(0));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ASTRING_OR_MISSING__STRING:
			return aStringOrMissing((String) arguments.get(0));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ASTRING_IS_EMPTY__STRING:
			return aStringIsEmpty((String) arguments.get(0));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING:
			return aListOfStringToStringWithSeparator((EList<String>) arguments.get(0), (String) arguments.get(1));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST:
			return aListOfStringToStringWithSeparator((EList<String>) arguments.get(0));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___APACKAGE_FROM_URI__STRING:
			return aPackageFromUri((String) arguments.get(0));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING:
			return aClassifierFromUriAndName((String) arguments.get(0), (String) arguments.get(1));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING:
			return aFeatureFromUriAndNames((String) arguments.get(0), (String) arguments.get(1),
					(String) arguments.get(2));
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_ASTRING_CLASS:
			return aCoreAStringClass();
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AREAL_CLASS:
			return aCoreARealClass();
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AINTEGER_CLASS:
			return aCoreAIntegerClass();
		case AnnotationsPackage.AOBJECT_CONSTRAINT___ACORE_AOBJECT_CLASS:
			return aCoreAObjectClass();
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (aTPackageUri: ");
		if (aTPackageUriESet)
			result.append(aTPackageUri);
		else
			result.append("<unset>");
		result.append(", aTClassifierName: ");
		if (aTClassifierNameESet)
			result.append(aTClassifierName);
		else
			result.append("<unset>");
		result.append(", aTFeatureName: ");
		if (aTFeatureNameESet)
			result.append(aTFeatureName);
		else
			result.append("<unset>");
		result.append(')');
		return result.toString();
	}

} //AObjectConstraintImpl
