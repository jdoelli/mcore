/**
 */

package com.montages.acore.annotations.impl;

import com.montages.acore.annotations.AObjectLabel;
import com.montages.acore.annotations.AnnotationsPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AObject Label</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */

public abstract class AObjectLabelImpl extends AClassAnnotationImpl implements AObjectLabel {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AObjectLabelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AnnotationsPackage.Literals.AOBJECT_LABEL;
	}

} //AObjectLabelImpl
