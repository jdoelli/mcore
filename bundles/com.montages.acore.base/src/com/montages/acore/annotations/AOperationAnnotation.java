/**
 */

package com.montages.acore.annotations;

import com.montages.acore.abstractions.AAnnotation;

import com.montages.acore.classifiers.AOperation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AOperation Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.annotations.AOperationAnnotation#getAAnnotatedOperation <em>AAnnotated Operation</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.annotations.AnnotationsPackage#getAOperationAnnotation()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Operation Annotation'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aAnnotatedDerive='aAnnotatedOperation\n'"
 * @generated
 */

public interface AOperationAnnotation extends AAnnotation {
	/**
	 * Returns the value of the '<em><b>AAnnotated Operation</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotated Operation</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotated Operation</em>' reference.
	 * @see com.montages.acore.annotations.AnnotationsPackage#getAOperationAnnotation_AAnnotatedOperation()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotated Operation'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Annotations' createColumn='false'"
	 * @generated
	 */
	AOperation getAAnnotatedOperation();

} // AOperationAnnotation
