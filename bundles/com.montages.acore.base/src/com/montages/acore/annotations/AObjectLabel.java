/**
 */

package com.montages.acore.annotations;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AObject Label</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see com.montages.acore.annotations.AnnotationsPackage#getAObjectLabel()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Object Label'"
 * @generated
 */

public interface AObjectLabel extends AClassAnnotation {
} // AObjectLabel
