/**
 */

package com.montages.acore.annotations;

import com.montages.acore.abstractions.AAnnotation;

import com.montages.acore.classifiers.AClassType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>AClass Annotation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.annotations.AClassAnnotation#getAAnnotatedClass <em>AAnnotated Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.annotations.AnnotationsPackage#getAClassAnnotation()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Class Annotation'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aAnnotatedDerive='aAnnotatedClass\n'"
 * @generated
 */

public interface AClassAnnotation extends AAnnotation {
	/**
	 * Returns the value of the '<em><b>AAnnotated Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAnnotated Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAnnotated Class</em>' reference.
	 * @see com.montages.acore.annotations.AnnotationsPackage#getAClassAnnotation_AAnnotatedClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Annotated Class'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Annotations' createColumn='false'"
	 * @generated
	 */
	AClassType getAAnnotatedClass();

} // AClassAnnotation
