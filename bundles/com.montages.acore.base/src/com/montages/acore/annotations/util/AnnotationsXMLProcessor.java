/**
 */
package com.montages.acore.annotations.util;

import com.montages.acore.annotations.AnnotationsPackage;

import java.util.Map;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.xmi.util.XMLProcessor;

/**
 * This class contains helper methods to serialize and deserialize XML documents
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AnnotationsXMLProcessor extends XMLProcessor {

	/**
	 * Public constructor to instantiate the helper.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AnnotationsXMLProcessor() {
		super((EPackage.Registry.INSTANCE));
		AnnotationsPackage.eINSTANCE.eClass();
	}

	/**
	 * Register for "*" and "xml" file extensions the AnnotationsResourceFactoryImpl factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected Map<String, Resource.Factory> getRegistrations() {
		if (registrations == null) {
			super.getRegistrations();
			registrations.put(XML_EXTENSION, new AnnotationsResourceFactoryImpl());
			registrations.put(STAR_EXTENSION, new AnnotationsResourceFactoryImpl());
		}
		return registrations;
	}

} //AnnotationsXMLProcessor
