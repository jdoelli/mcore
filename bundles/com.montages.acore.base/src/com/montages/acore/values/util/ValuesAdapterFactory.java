/**
 */
package com.montages.acore.values.util;

import com.montages.acore.abstractions.AElement;
import com.montages.acore.abstractions.ANamed;

import com.montages.acore.values.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see com.montages.acore.values.ValuesPackage
 * @generated
 */
public class ValuesAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ValuesPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValuesAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ValuesPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ValuesSwitch<Adapter> modelSwitch = new ValuesSwitch<Adapter>() {
		@Override
		public Adapter caseAValue(AValue object) {
			return createAValueAdapter();
		}

		@Override
		public Adapter caseADataValue(ADataValue object) {
			return createADataValueAdapter();
		}

		@Override
		public Adapter caseALiteral(ALiteral object) {
			return createALiteralAdapter();
		}

		@Override
		public Adapter caseAObject(AObject object) {
			return createAObjectAdapter();
		}

		@Override
		public Adapter caseASlot(ASlot object) {
			return createASlotAdapter();
		}

		@Override
		public Adapter caseAElement(AElement object) {
			return createAElementAdapter();
		}

		@Override
		public Adapter caseANamed(ANamed object) {
			return createANamedAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.values.AValue <em>AValue</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.values.AValue
	 * @generated
	 */
	public Adapter createAValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.values.ADataValue <em>AData Value</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.values.ADataValue
	 * @generated
	 */
	public Adapter createADataValueAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.values.ALiteral <em>ALiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.values.ALiteral
	 * @generated
	 */
	public Adapter createALiteralAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.values.AObject <em>AObject</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.values.AObject
	 * @generated
	 */
	public Adapter createAObjectAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.values.ASlot <em>ASlot</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.values.ASlot
	 * @generated
	 */
	public Adapter createASlotAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.abstractions.AElement <em>AElement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.abstractions.AElement
	 * @generated
	 */
	public Adapter createAElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link com.montages.acore.abstractions.ANamed <em>ANamed</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see com.montages.acore.abstractions.ANamed
	 * @generated
	 */
	public Adapter createANamedAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ValuesAdapterFactory
