/**
 */

package com.montages.acore.values.rules;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ARule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.values.rules.ARule#getASelfContext <em>ASelf Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.ARule#getATrgContext <em>ATrg Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.ARule#getATrgContextSingular <em>ATrg Context Singular</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.ARule#getASrcContext <em>ASrc Context</em>}</li>
 *   <li>{@link com.montages.acore.values.rules.ARule#getASrcContextSingular <em>ASrc Context Singular</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.values.rules.RulesPackage#getARule()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Rule'"
 * @generated
 */

public interface ARule extends EObject {
	/**
	 * Returns the value of the '<em><b>ASelf Context</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AClassType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASelf Context</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASelf Context</em>' reference list.
	 * @see com.montages.acore.values.rules.RulesPackage#getARule_ASelfContext()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Self Context'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Rules' createColumn='false'"
	 * @generated
	 */
	EList<AClassType> getASelfContext();

	/**
	 * Returns the value of the '<em><b>ATrg Context</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATrg Context</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATrg Context</em>' reference list.
	 * @see com.montages.acore.values.rules.RulesPackage#getARule_ATrgContext()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Trg Context'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Rules' createColumn='false'"
	 * @generated
	 */
	EList<AClassifier> getATrgContext();

	/**
	 * Returns the value of the '<em><b>ATrg Context Singular</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ATrg Context Singular</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ATrg Context Singular</em>' attribute.
	 * @see com.montages.acore.values.rules.RulesPackage#getARule_ATrgContextSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Trg Context Singular'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Rules' createColumn='false'"
	 * @generated
	 */
	Boolean getATrgContextSingular();

	/**
	 * Returns the value of the '<em><b>ASrc Context</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.classifiers.AClassifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASrc Context</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASrc Context</em>' reference list.
	 * @see #isSetASrcContext()
	 * @see #unsetASrcContext()
	 * @see com.montages.acore.values.rules.RulesPackage#getARule_ASrcContext()
	 * @model unsettable="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Src Context'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Rules' createColumn='false'"
	 * @generated
	 */
	EList<AClassifier> getASrcContext();

	/**
	 * Unsets the value of the '{@link com.montages.acore.values.rules.ARule#getASrcContext <em>ASrc Context</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isSetASrcContext()
	 * @see #getASrcContext()
	 * @generated
	 */
	void unsetASrcContext();

	/**
	 * Returns whether the value of the '{@link com.montages.acore.values.rules.ARule#getASrcContext <em>ASrc Context</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>ASrc Context</em>' reference list is set.
	 * @see #unsetASrcContext()
	 * @see #getASrcContext()
	 * @generated
	 */
	boolean isSetASrcContext();

	/**
	 * Returns the value of the '<em><b>ASrc Context Singular</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.Boolean}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ASrc Context Singular</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ASrc Context Singular</em>' attribute list.
	 * @see com.montages.acore.values.rules.RulesPackage#getARule_ASrcContextSingular()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Src Context Singular'"
	 *        annotation="http://www.xocl.org/OCL derive='null\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Rules' createColumn='false'"
	 * @generated
	 */
	EList<Boolean> getASrcContextSingular();

} // ARule
