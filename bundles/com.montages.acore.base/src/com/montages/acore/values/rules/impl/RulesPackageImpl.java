/**
 */
package com.montages.acore.values.rules.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AbstractionsPackageImpl;

import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.annotations.impl.AnnotationsPackageImpl;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.classifiers.impl.ClassifiersPackageImpl;

import com.montages.acore.impl.AcorePackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.impl.ValuesPackageImpl;

import com.montages.acore.values.rules.ARule;
import com.montages.acore.values.rules.RulesFactory;
import com.montages.acore.values.rules.RulesPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RulesPackageImpl extends EPackageImpl implements RulesPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aRuleEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.values.rules.RulesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RulesPackageImpl() {
		super(eNS_URI, RulesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RulesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RulesPackage init() {
		if (isInited)
			return (RulesPackage) EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);

		// Obtain or create and register package
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof RulesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new RulesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI)
						: AnnotationsPackage.eINSTANCE);
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ValuesPackage.eNS_URI) instanceof ValuesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI) : ValuesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theRulesPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theValuesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theRulesPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theValuesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRulesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RulesPackage.eNS_URI, theRulesPackage);
		return theRulesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getARule() {
		return aRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getARule_ASelfContext() {
		return (EReference) aRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getARule_ATrgContext() {
		return (EReference) aRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getARule_ATrgContextSingular() {
		return (EAttribute) aRuleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getARule_ASrcContext() {
		return (EReference) aRuleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getARule_ASrcContextSingular() {
		return (EAttribute) aRuleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RulesFactory getRulesFactory() {
		return (RulesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aRuleEClass = createEClass(ARULE);
		createEReference(aRuleEClass, ARULE__ASELF_CONTEXT);
		createEReference(aRuleEClass, ARULE__ATRG_CONTEXT);
		createEAttribute(aRuleEClass, ARULE__ATRG_CONTEXT_SINGULAR);
		createEReference(aRuleEClass, ARULE__ASRC_CONTEXT);
		createEAttribute(aRuleEClass, ARULE__ASRC_CONTEXT_SINGULAR);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(aRuleEClass, ARule.class, "ARule", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getARule_ASelfContext(), theClassifiersPackage.getAClassType(), null, "aSelfContext", null, 0,
				-1, ARule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getARule_ATrgContext(), theClassifiersPackage.getAClassifier(), null, "aTrgContext", null, 0, -1,
				ARule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getARule_ATrgContextSingular(), ecorePackage.getEBooleanObject(), "aTrgContextSingular", null, 0,
				1, ARule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getARule_ASrcContext(), theClassifiersPackage.getAClassifier(), null, "aSrcContext", null, 0, -1,
				ARule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getARule_ASrcContextSingular(), ecorePackage.getEBooleanObject(), "aSrcContextSingular", null, 0,
				-1, ARule.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aRuleEClass, source, new String[] { "mName", "Rule" });
		addAnnotation(getARule_ASelfContext(), source, new String[] { "mName", "Self Context" });
		addAnnotation(getARule_ATrgContext(), source, new String[] { "mName", "Trg Context" });
		addAnnotation(getARule_ATrgContextSingular(), source, new String[] { "mName", "Trg Context Singular" });
		addAnnotation(getARule_ASrcContext(), source, new String[] { "mName", "Src Context" });
		addAnnotation(getARule_ASrcContextSingular(), source, new String[] { "mName", "Src Context Singular" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getARule_ASelfContext(), source, new String[] { "derive", "null\n" });
		addAnnotation(getARule_ATrgContext(), source, new String[] { "derive", "null\n" });
		addAnnotation(getARule_ATrgContextSingular(), source, new String[] { "derive", "null\n" });
		addAnnotation(getARule_ASrcContextSingular(), source, new String[] { "derive", "null\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getARule_ASelfContext(), source,
				new String[] { "propertyCategory", "z A Core/Rules", "createColumn", "false" });
		addAnnotation(getARule_ATrgContext(), source,
				new String[] { "propertyCategory", "z A Core/Rules", "createColumn", "false" });
		addAnnotation(getARule_ATrgContextSingular(), source,
				new String[] { "propertyCategory", "z A Core/Rules", "createColumn", "false" });
		addAnnotation(getARule_ASrcContext(), source,
				new String[] { "propertyCategory", "z A Core/Rules", "createColumn", "false" });
		addAnnotation(getARule_ASrcContextSingular(), source,
				new String[] { "propertyCategory", "z A Core/Rules", "createColumn", "false" });
	}

} //RulesPackageImpl
