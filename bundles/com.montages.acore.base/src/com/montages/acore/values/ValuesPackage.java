/**
 */
package com.montages.acore.values;

import com.montages.acore.abstractions.AbstractionsPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see com.montages.acore.values.ValuesFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/GenModel basePackage='com.montages.acore'"
 * @generated
 */
public interface ValuesPackage extends EPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String PLUGIN_ID = "com.montages.acore.base";

	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "values";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.montages.com/aCore/ACore/Values";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "acore.values";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ValuesPackage eINSTANCE = com.montages.acore.values.impl.ValuesPackageImpl.init();

	/**
	 * The meta object id for the '{@link com.montages.acore.values.impl.AValueImpl <em>AValue</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.impl.AValueImpl
	 * @see com.montages.acore.values.impl.ValuesPackageImpl#getAValue()
	 * @generated
	 */
	int AVALUE = 0;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__ALABEL = AbstractionsPackage.AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AKIND_BASE = AbstractionsPackage.AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__ARENDERED_KIND = AbstractionsPackage.AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__ACONTAINING_COMPONENT = AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_PACKAGE_URI = AbstractionsPackage.AELEMENT__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_CLASSIFIER_NAME = AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_FEATURE_NAME = AbstractionsPackage.AELEMENT__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_PACKAGE = AbstractionsPackage.AELEMENT__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_CLASSIFIER = AbstractionsPackage.AELEMENT__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_FEATURE = AbstractionsPackage.AELEMENT__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__AT_CORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__ACLASSIFIER = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AContaining Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE__ACONTAINING_SLOT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE_FEATURE_COUNT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___AINDENT_LEVEL = AbstractionsPackage.AELEMENT___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___AINDENTATION_SPACES = AbstractionsPackage.AELEMENT___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.AELEMENT___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ASTRING_OR_MISSING__STRING = AbstractionsPackage.AELEMENT___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.AELEMENT___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___APACKAGE_FROM_URI__STRING = AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ACORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ACORE_AREAL_CLASS = AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ACORE_AINTEGER_CLASS = AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE___ACORE_AOBJECT_CLASS = AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AValue</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AVALUE_OPERATION_COUNT = AbstractionsPackage.AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.values.impl.ADataValueImpl <em>AData Value</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.impl.ADataValueImpl
	 * @see com.montages.acore.values.impl.ValuesPackageImpl#getADataValue()
	 * @generated
	 */
	int ADATA_VALUE = 1;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ALABEL = AVALUE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AKIND_BASE = AVALUE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ARENDERED_KIND = AVALUE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ACONTAINING_COMPONENT = AVALUE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_PACKAGE_URI = AVALUE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_CLASSIFIER_NAME = AVALUE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_FEATURE_NAME = AVALUE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_PACKAGE = AVALUE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_CLASSIFIER = AVALUE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_FEATURE = AVALUE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AT_CORE_ASTRING_CLASS = AVALUE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ACLASSIFIER = AVALUE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AContaining Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ACONTAINING_SLOT = AVALUE__ACONTAINING_SLOT;

	/**
	 * The feature id for the '<em><b>AValue</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__AVALUE = AVALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AData Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE__ADATA_TYPE = AVALUE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>AData Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE_FEATURE_COUNT = AVALUE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___AINDENT_LEVEL = AVALUE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___AINDENTATION_SPACES = AVALUE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___AINDENTATION_SPACES__INTEGER = AVALUE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ASTRING_OR_MISSING__STRING = AVALUE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ASTRING_IS_EMPTY__STRING = AVALUE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___APACKAGE_FROM_URI__STRING = AVALUE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AVALUE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AVALUE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ACORE_ASTRING_CLASS = AVALUE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ACORE_AREAL_CLASS = AVALUE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ACORE_AINTEGER_CLASS = AVALUE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE___ACORE_AOBJECT_CLASS = AVALUE___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>AData Value</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ADATA_VALUE_OPERATION_COUNT = AVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.values.impl.ALiteralImpl <em>ALiteral</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.impl.ALiteralImpl
	 * @see com.montages.acore.values.impl.ValuesPackageImpl#getALiteral()
	 * @generated
	 */
	int ALITERAL = 2;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ALABEL = AVALUE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AKIND_BASE = AVALUE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ARENDERED_KIND = AVALUE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ACONTAINING_COMPONENT = AVALUE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_PACKAGE_URI = AVALUE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_CLASSIFIER_NAME = AVALUE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_FEATURE_NAME = AVALUE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_PACKAGE = AVALUE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_CLASSIFIER = AVALUE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_FEATURE = AVALUE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AT_CORE_ASTRING_CLASS = AVALUE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ACLASSIFIER = AVALUE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AContaining Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ACONTAINING_SLOT = AVALUE__ACONTAINING_SLOT;

	/**
	 * The feature id for the '<em><b>AName</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ANAME = AVALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AUndefined Name Constant</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AUNDEFINED_NAME_CONSTANT = AVALUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>ABusiness Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__ABUSINESS_NAME = AVALUE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AEnumeration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AENUMERATION = AVALUE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AQualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL__AQUALIFIED_NAME = AVALUE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>ALiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL_FEATURE_COUNT = AVALUE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___AINDENT_LEVEL = AVALUE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___AINDENTATION_SPACES = AVALUE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___AINDENTATION_SPACES__INTEGER = AVALUE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ASTRING_OR_MISSING__STRING = AVALUE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ASTRING_IS_EMPTY__STRING = AVALUE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___APACKAGE_FROM_URI__STRING = AVALUE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AVALUE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AVALUE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ACORE_ASTRING_CLASS = AVALUE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ACORE_AREAL_CLASS = AVALUE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ACORE_AINTEGER_CLASS = AVALUE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL___ACORE_AOBJECT_CLASS = AVALUE___ACORE_AOBJECT_CLASS;

	/**
	 * The number of operations of the '<em>ALiteral</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ALITERAL_OPERATION_COUNT = AVALUE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link com.montages.acore.values.impl.AObjectImpl <em>AObject</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.impl.AObjectImpl
	 * @see com.montages.acore.values.impl.ValuesPackageImpl#getAObject()
	 * @generated
	 */
	int AOBJECT = 3;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ALABEL = AVALUE__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AKIND_BASE = AVALUE__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ARENDERED_KIND = AVALUE__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACONTAINING_COMPONENT = AVALUE__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_PACKAGE_URI = AVALUE__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_CLASSIFIER_NAME = AVALUE__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_FEATURE_NAME = AVALUE__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_PACKAGE = AVALUE__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_CLASSIFIER = AVALUE__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_FEATURE = AVALUE__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AT_CORE_ASTRING_CLASS = AVALUE__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AClassifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACLASSIFIER = AVALUE__ACLASSIFIER;

	/**
	 * The feature id for the '<em><b>AContaining Slot</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACONTAINING_SLOT = AVALUE__ACONTAINING_SLOT;

	/**
	 * The feature id for the '<em><b>AClass Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACLASS_TYPE = AVALUE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>ASlot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ASLOT = AVALUE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AContaining Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACONTAINING_OBJECT = AVALUE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AContainment Reference</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACONTAINMENT_REFERENCE = AVALUE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AContaining Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__ACONTAINING_RESOURCE = AVALUE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AFeature Without Slot</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AFEATURE_WITHOUT_SLOT = AVALUE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AAll Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT__AALL_OBJECT = AVALUE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>AObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_FEATURE_COUNT = AVALUE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___AINDENT_LEVEL = AVALUE___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___AINDENTATION_SPACES = AVALUE___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___AINDENTATION_SPACES__INTEGER = AVALUE___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ASTRING_OR_MISSING__STRING = AVALUE___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ASTRING_IS_EMPTY__STRING = AVALUE___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AVALUE___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___APACKAGE_FROM_URI__STRING = AVALUE___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AVALUE___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AVALUE___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACORE_ASTRING_CLASS = AVALUE___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACORE_AREAL_CLASS = AVALUE___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACORE_AINTEGER_CLASS = AVALUE___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACORE_AOBJECT_CLASS = AVALUE___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>ANew Slot</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ANEW_SLOT__AFEATURE_ELIST = AVALUE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>ASlot From Feature</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ASLOT_FROM_FEATURE__AFEATURE = AVALUE_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>AChoosable For AClass</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT___ACHOOSABLE_FOR_ACLASS__ACLASSTYPE = AVALUE_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>AObject</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AOBJECT_OPERATION_COUNT = AVALUE_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link com.montages.acore.values.impl.ASlotImpl <em>ASlot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see com.montages.acore.values.impl.ASlotImpl
	 * @see com.montages.acore.values.impl.ValuesPackageImpl#getASlot()
	 * @generated
	 */
	int ASLOT = 4;

	/**
	 * The feature id for the '<em><b>ALabel</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__ALABEL = AbstractionsPackage.AELEMENT__ALABEL;

	/**
	 * The feature id for the '<em><b>AKind Base</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AKIND_BASE = AbstractionsPackage.AELEMENT__AKIND_BASE;

	/**
	 * The feature id for the '<em><b>ARendered Kind</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__ARENDERED_KIND = AbstractionsPackage.AELEMENT__ARENDERED_KIND;

	/**
	 * The feature id for the '<em><b>AContaining Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__ACONTAINING_COMPONENT = AbstractionsPackage.AELEMENT__ACONTAINING_COMPONENT;

	/**
	 * The feature id for the '<em><b>AT Package Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_PACKAGE_URI = AbstractionsPackage.AELEMENT__AT_PACKAGE_URI;

	/**
	 * The feature id for the '<em><b>AT Classifier Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_CLASSIFIER_NAME = AbstractionsPackage.AELEMENT__AT_CLASSIFIER_NAME;

	/**
	 * The feature id for the '<em><b>AT Feature Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_FEATURE_NAME = AbstractionsPackage.AELEMENT__AT_FEATURE_NAME;

	/**
	 * The feature id for the '<em><b>AT Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_PACKAGE = AbstractionsPackage.AELEMENT__AT_PACKAGE;

	/**
	 * The feature id for the '<em><b>AT Classifier</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_CLASSIFIER = AbstractionsPackage.AELEMENT__AT_CLASSIFIER;

	/**
	 * The feature id for the '<em><b>AT Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_FEATURE = AbstractionsPackage.AELEMENT__AT_FEATURE;

	/**
	 * The feature id for the '<em><b>AT Core AString Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AT_CORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT__AT_CORE_ASTRING_CLASS;

	/**
	 * The feature id for the '<em><b>AKey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AKEY = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>AInstantiated Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AINSTANTIATED_FEATURE = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>AValue</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AVALUE = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>AIs Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AIS_CONTAINMENT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>AContaining Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__ACONTAINING_OBJECT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>AFeature Is Class Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AFEATURE_IS_CLASS_TYPED = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>AFeature Is Data Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AFEATURE_IS_DATA_TYPED = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>ADirectly Contained Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__ADIRECTLY_CONTAINED_OBJECT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>AAll Contained Object</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AALL_CONTAINED_OBJECT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>AFeature Of Wrong Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT__AFEATURE_OF_WRONG_CLASS = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>ASlot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT_FEATURE_COUNT = AbstractionsPackage.AELEMENT_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>AIndent Level</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AINDENT_LEVEL = AbstractionsPackage.AELEMENT___AINDENT_LEVEL;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AINDENTATION_SPACES = AbstractionsPackage.AELEMENT___AINDENTATION_SPACES;

	/**
	 * The operation id for the '<em>AIndentation Spaces</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AINDENTATION_SPACES__INTEGER = AbstractionsPackage.AELEMENT___AINDENTATION_SPACES__INTEGER;

	/**
	 * The operation id for the '<em>AString Or Missing</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ASTRING_OR_MISSING__STRING = AbstractionsPackage.AELEMENT___ASTRING_OR_MISSING__STRING;

	/**
	 * The operation id for the '<em>AString Is Empty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ASTRING_IS_EMPTY__STRING = AbstractionsPackage.AELEMENT___ASTRING_IS_EMPTY__STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING = AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST_STRING;

	/**
	 * The operation id for the '<em>AList Of String To String With Separator</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST = AbstractionsPackage.AELEMENT___ALIST_OF_STRING_TO_STRING_WITH_SEPARATOR__ELIST;

	/**
	 * The operation id for the '<em>APackage From Uri</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___APACKAGE_FROM_URI__STRING = AbstractionsPackage.AELEMENT___APACKAGE_FROM_URI__STRING;

	/**
	 * The operation id for the '<em>AClassifier From Uri And Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING = AbstractionsPackage.AELEMENT___ACLASSIFIER_FROM_URI_AND_NAME__STRING_STRING;

	/**
	 * The operation id for the '<em>AFeature From Uri And Names</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING = AbstractionsPackage.AELEMENT___AFEATURE_FROM_URI_AND_NAMES__STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>ACore AString Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ACORE_ASTRING_CLASS = AbstractionsPackage.AELEMENT___ACORE_ASTRING_CLASS;

	/**
	 * The operation id for the '<em>ACore AReal Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ACORE_AREAL_CLASS = AbstractionsPackage.AELEMENT___ACORE_AREAL_CLASS;

	/**
	 * The operation id for the '<em>ACore AInteger Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ACORE_AINTEGER_CLASS = AbstractionsPackage.AELEMENT___ACORE_AINTEGER_CLASS;

	/**
	 * The operation id for the '<em>ACore AObject Class</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___ACORE_AOBJECT_CLASS = AbstractionsPackage.AELEMENT___ACORE_AOBJECT_CLASS;

	/**
	 * The operation id for the '<em>AEvaluate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AEVALUATE = AbstractionsPackage.AELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>AUpdate</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT___AUPDATE__AUPDATEMODE_ELIST = AbstractionsPackage.AELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>ASlot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ASLOT_OPERATION_COUNT = AbstractionsPackage.AELEMENT_OPERATION_COUNT + 2;

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.AValue <em>AValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AValue</em>'.
	 * @see com.montages.acore.values.AValue
	 * @generated
	 */
	EClass getAValue();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AValue#getAClassifier <em>AClassifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClassifier</em>'.
	 * @see com.montages.acore.values.AValue#getAClassifier()
	 * @see #getAValue()
	 * @generated
	 */
	EReference getAValue_AClassifier();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AValue#getAContainingSlot <em>AContaining Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Slot</em>'.
	 * @see com.montages.acore.values.AValue#getAContainingSlot()
	 * @see #getAValue()
	 * @generated
	 */
	EReference getAValue_AContainingSlot();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.ADataValue <em>AData Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AData Value</em>'.
	 * @see com.montages.acore.values.ADataValue
	 * @generated
	 */
	EClass getADataValue();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ADataValue#getAValue <em>AValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AValue</em>'.
	 * @see com.montages.acore.values.ADataValue#getAValue()
	 * @see #getADataValue()
	 * @generated
	 */
	EAttribute getADataValue_AValue();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.ADataValue#getADataType <em>AData Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AData Type</em>'.
	 * @see com.montages.acore.values.ADataValue#getADataType()
	 * @see #getADataValue()
	 * @generated
	 */
	EReference getADataValue_ADataType();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.ALiteral <em>ALiteral</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ALiteral</em>'.
	 * @see com.montages.acore.values.ALiteral
	 * @generated
	 */
	EClass getALiteral();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.ALiteral#getAEnumeration <em>AEnumeration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AEnumeration</em>'.
	 * @see com.montages.acore.values.ALiteral#getAEnumeration()
	 * @see #getALiteral()
	 * @generated
	 */
	EReference getALiteral_AEnumeration();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ALiteral#getAQualifiedName <em>AQualified Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AQualified Name</em>'.
	 * @see com.montages.acore.values.ALiteral#getAQualifiedName()
	 * @see #getALiteral()
	 * @generated
	 */
	EAttribute getALiteral_AQualifiedName();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.AObject <em>AObject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>AObject</em>'.
	 * @see com.montages.acore.values.AObject
	 * @generated
	 */
	EClass getAObject();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AObject#getAClassType <em>AClass Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AClass Type</em>'.
	 * @see com.montages.acore.values.AObject#getAClassType()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AClassType();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.AObject#getASlot <em>ASlot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ASlot</em>'.
	 * @see com.montages.acore.values.AObject#getASlot()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_ASlot();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AObject#getAContainingObject <em>AContaining Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Object</em>'.
	 * @see com.montages.acore.values.AObject#getAContainingObject()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AContainingObject();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AObject#getAContainmentReference <em>AContainment Reference</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContainment Reference</em>'.
	 * @see com.montages.acore.values.AObject#getAContainmentReference()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AContainmentReference();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.AObject#getAContainingResource <em>AContaining Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Resource</em>'.
	 * @see com.montages.acore.values.AObject#getAContainingResource()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AContainingResource();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.AObject#getAFeatureWithoutSlot <em>AFeature Without Slot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AFeature Without Slot</em>'.
	 * @see com.montages.acore.values.AObject#getAFeatureWithoutSlot()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AFeatureWithoutSlot();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.AObject#getAAllObject <em>AAll Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Object</em>'.
	 * @see com.montages.acore.values.AObject#getAAllObject()
	 * @see #getAObject()
	 * @generated
	 */
	EReference getAObject_AAllObject();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.values.AObject#aNewSlot(com.montages.acore.classifiers.AFeature, org.eclipse.emf.common.util.EList) <em>ANew Slot</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ANew Slot</em>' operation.
	 * @see com.montages.acore.values.AObject#aNewSlot(com.montages.acore.classifiers.AFeature, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getAObject__ANewSlot__AFeature_EList();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.values.AObject#aSlotFromFeature(com.montages.acore.classifiers.AFeature) <em>ASlot From Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>ASlot From Feature</em>' operation.
	 * @see com.montages.acore.values.AObject#aSlotFromFeature(com.montages.acore.classifiers.AFeature)
	 * @generated
	 */
	EOperation getAObject__ASlotFromFeature__AFeature();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.values.AObject#aChoosableForAClass(com.montages.acore.classifiers.AClassType) <em>AChoosable For AClass</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AChoosable For AClass</em>' operation.
	 * @see com.montages.acore.values.AObject#aChoosableForAClass(com.montages.acore.classifiers.AClassType)
	 * @generated
	 */
	EOperation getAObject__AChoosableForAClass__AClassType();

	/**
	 * Returns the meta object for class '{@link com.montages.acore.values.ASlot <em>ASlot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>ASlot</em>'.
	 * @see com.montages.acore.values.ASlot
	 * @generated
	 */
	EClass getASlot();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ASlot#getAKey <em>AKey</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AKey</em>'.
	 * @see com.montages.acore.values.ASlot#getAKey()
	 * @see #getASlot()
	 * @generated
	 */
	EAttribute getASlot_AKey();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.ASlot#getAInstantiatedFeature <em>AInstantiated Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AInstantiated Feature</em>'.
	 * @see com.montages.acore.values.ASlot#getAInstantiatedFeature()
	 * @see #getASlot()
	 * @generated
	 */
	EReference getASlot_AInstantiatedFeature();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.ASlot#getAValue <em>AValue</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AValue</em>'.
	 * @see com.montages.acore.values.ASlot#getAValue()
	 * @see #getASlot()
	 * @generated
	 */
	EReference getASlot_AValue();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ASlot#getAIsContainment <em>AIs Containment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AIs Containment</em>'.
	 * @see com.montages.acore.values.ASlot#getAIsContainment()
	 * @see #getASlot()
	 * @generated
	 */
	EAttribute getASlot_AIsContainment();

	/**
	 * Returns the meta object for the reference '{@link com.montages.acore.values.ASlot#getAContainingObject <em>AContaining Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>AContaining Object</em>'.
	 * @see com.montages.acore.values.ASlot#getAContainingObject()
	 * @see #getASlot()
	 * @generated
	 */
	EReference getASlot_AContainingObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ASlot#getAFeatureIsClassTyped <em>AFeature Is Class Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AFeature Is Class Typed</em>'.
	 * @see com.montages.acore.values.ASlot#getAFeatureIsClassTyped()
	 * @see #getASlot()
	 * @generated
	 */
	EAttribute getASlot_AFeatureIsClassTyped();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ASlot#getAFeatureIsDataTyped <em>AFeature Is Data Typed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AFeature Is Data Typed</em>'.
	 * @see com.montages.acore.values.ASlot#getAFeatureIsDataTyped()
	 * @see #getASlot()
	 * @generated
	 */
	EAttribute getASlot_AFeatureIsDataTyped();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.ASlot#getADirectlyContainedObject <em>ADirectly Contained Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>ADirectly Contained Object</em>'.
	 * @see com.montages.acore.values.ASlot#getADirectlyContainedObject()
	 * @see #getASlot()
	 * @generated
	 */
	EReference getASlot_ADirectlyContainedObject();

	/**
	 * Returns the meta object for the reference list '{@link com.montages.acore.values.ASlot#getAAllContainedObject <em>AAll Contained Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>AAll Contained Object</em>'.
	 * @see com.montages.acore.values.ASlot#getAAllContainedObject()
	 * @see #getASlot()
	 * @generated
	 */
	EReference getASlot_AAllContainedObject();

	/**
	 * Returns the meta object for the attribute '{@link com.montages.acore.values.ASlot#getAFeatureOfWrongClass <em>AFeature Of Wrong Class</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>AFeature Of Wrong Class</em>'.
	 * @see com.montages.acore.values.ASlot#getAFeatureOfWrongClass()
	 * @see #getASlot()
	 * @generated
	 */
	EAttribute getASlot_AFeatureOfWrongClass();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.values.ASlot#aEvaluate() <em>AEvaluate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AEvaluate</em>' operation.
	 * @see com.montages.acore.values.ASlot#aEvaluate()
	 * @generated
	 */
	EOperation getASlot__AEvaluate();

	/**
	 * Returns the meta object for the '{@link com.montages.acore.values.ASlot#aUpdate(com.montages.acore.updates.AUpdateMode, org.eclipse.emf.common.util.EList) <em>AUpdate</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>AUpdate</em>' operation.
	 * @see com.montages.acore.values.ASlot#aUpdate(com.montages.acore.updates.AUpdateMode, org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getASlot__AUpdate__AUpdateMode_EList();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ValuesFactory getValuesFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link com.montages.acore.values.impl.AValueImpl <em>AValue</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.impl.AValueImpl
		 * @see com.montages.acore.values.impl.ValuesPackageImpl#getAValue()
		 * @generated
		 */
		EClass AVALUE = eINSTANCE.getAValue();

		/**
		 * The meta object literal for the '<em><b>AClassifier</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AVALUE__ACLASSIFIER = eINSTANCE.getAValue_AClassifier();

		/**
		 * The meta object literal for the '<em><b>AContaining Slot</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AVALUE__ACONTAINING_SLOT = eINSTANCE.getAValue_AContainingSlot();

		/**
		 * The meta object literal for the '{@link com.montages.acore.values.impl.ADataValueImpl <em>AData Value</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.impl.ADataValueImpl
		 * @see com.montages.acore.values.impl.ValuesPackageImpl#getADataValue()
		 * @generated
		 */
		EClass ADATA_VALUE = eINSTANCE.getADataValue();

		/**
		 * The meta object literal for the '<em><b>AValue</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ADATA_VALUE__AVALUE = eINSTANCE.getADataValue_AValue();

		/**
		 * The meta object literal for the '<em><b>AData Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ADATA_VALUE__ADATA_TYPE = eINSTANCE.getADataValue_ADataType();

		/**
		 * The meta object literal for the '{@link com.montages.acore.values.impl.ALiteralImpl <em>ALiteral</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.impl.ALiteralImpl
		 * @see com.montages.acore.values.impl.ValuesPackageImpl#getALiteral()
		 * @generated
		 */
		EClass ALITERAL = eINSTANCE.getALiteral();

		/**
		 * The meta object literal for the '<em><b>AEnumeration</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ALITERAL__AENUMERATION = eINSTANCE.getALiteral_AEnumeration();

		/**
		 * The meta object literal for the '<em><b>AQualified Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ALITERAL__AQUALIFIED_NAME = eINSTANCE.getALiteral_AQualifiedName();

		/**
		 * The meta object literal for the '{@link com.montages.acore.values.impl.AObjectImpl <em>AObject</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.impl.AObjectImpl
		 * @see com.montages.acore.values.impl.ValuesPackageImpl#getAObject()
		 * @generated
		 */
		EClass AOBJECT = eINSTANCE.getAObject();

		/**
		 * The meta object literal for the '<em><b>AClass Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__ACLASS_TYPE = eINSTANCE.getAObject_AClassType();

		/**
		 * The meta object literal for the '<em><b>ASlot</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__ASLOT = eINSTANCE.getAObject_ASlot();

		/**
		 * The meta object literal for the '<em><b>AContaining Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__ACONTAINING_OBJECT = eINSTANCE.getAObject_AContainingObject();

		/**
		 * The meta object literal for the '<em><b>AContainment Reference</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__ACONTAINMENT_REFERENCE = eINSTANCE.getAObject_AContainmentReference();

		/**
		 * The meta object literal for the '<em><b>AContaining Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__ACONTAINING_RESOURCE = eINSTANCE.getAObject_AContainingResource();

		/**
		 * The meta object literal for the '<em><b>AFeature Without Slot</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__AFEATURE_WITHOUT_SLOT = eINSTANCE.getAObject_AFeatureWithoutSlot();

		/**
		 * The meta object literal for the '<em><b>AAll Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AOBJECT__AALL_OBJECT = eINSTANCE.getAObject_AAllObject();

		/**
		 * The meta object literal for the '<em><b>ANew Slot</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AOBJECT___ANEW_SLOT__AFEATURE_ELIST = eINSTANCE.getAObject__ANewSlot__AFeature_EList();

		/**
		 * The meta object literal for the '<em><b>ASlot From Feature</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AOBJECT___ASLOT_FROM_FEATURE__AFEATURE = eINSTANCE.getAObject__ASlotFromFeature__AFeature();

		/**
		 * The meta object literal for the '<em><b>AChoosable For AClass</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AOBJECT___ACHOOSABLE_FOR_ACLASS__ACLASSTYPE = eINSTANCE
				.getAObject__AChoosableForAClass__AClassType();

		/**
		 * The meta object literal for the '{@link com.montages.acore.values.impl.ASlotImpl <em>ASlot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see com.montages.acore.values.impl.ASlotImpl
		 * @see com.montages.acore.values.impl.ValuesPackageImpl#getASlot()
		 * @generated
		 */
		EClass ASLOT = eINSTANCE.getASlot();

		/**
		 * The meta object literal for the '<em><b>AKey</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASLOT__AKEY = eINSTANCE.getASlot_AKey();

		/**
		 * The meta object literal for the '<em><b>AInstantiated Feature</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASLOT__AINSTANTIATED_FEATURE = eINSTANCE.getASlot_AInstantiatedFeature();

		/**
		 * The meta object literal for the '<em><b>AValue</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASLOT__AVALUE = eINSTANCE.getASlot_AValue();

		/**
		 * The meta object literal for the '<em><b>AIs Containment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASLOT__AIS_CONTAINMENT = eINSTANCE.getASlot_AIsContainment();

		/**
		 * The meta object literal for the '<em><b>AContaining Object</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASLOT__ACONTAINING_OBJECT = eINSTANCE.getASlot_AContainingObject();

		/**
		 * The meta object literal for the '<em><b>AFeature Is Class Typed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASLOT__AFEATURE_IS_CLASS_TYPED = eINSTANCE.getASlot_AFeatureIsClassTyped();

		/**
		 * The meta object literal for the '<em><b>AFeature Is Data Typed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASLOT__AFEATURE_IS_DATA_TYPED = eINSTANCE.getASlot_AFeatureIsDataTyped();

		/**
		 * The meta object literal for the '<em><b>ADirectly Contained Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASLOT__ADIRECTLY_CONTAINED_OBJECT = eINSTANCE.getASlot_ADirectlyContainedObject();

		/**
		 * The meta object literal for the '<em><b>AAll Contained Object</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ASLOT__AALL_CONTAINED_OBJECT = eINSTANCE.getASlot_AAllContainedObject();

		/**
		 * The meta object literal for the '<em><b>AFeature Of Wrong Class</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ASLOT__AFEATURE_OF_WRONG_CLASS = eINSTANCE.getASlot_AFeatureOfWrongClass();

		/**
		 * The meta object literal for the '<em><b>AEvaluate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ASLOT___AEVALUATE = eINSTANCE.getASlot__AEvaluate();

		/**
		 * The meta object literal for the '<em><b>AUpdate</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ASLOT___AUPDATE__AUPDATEMODE_ELIST = eINSTANCE.getASlot__AUpdate__AUpdateMode_EList();

	}

} //ValuesPackage
