/**
 */

package com.montages.acore.values;

import com.montages.acore.abstractions.ANamed;

import com.montages.acore.classifiers.AEnumeration;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ALiteral</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.values.ALiteral#getAEnumeration <em>AEnumeration</em>}</li>
 *   <li>{@link com.montages.acore.values.ALiteral#getAQualifiedName <em>AQualified Name</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.values.ValuesPackage#getALiteral()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Literal'"
 *        annotation="http://www.xocl.org/OVERRIDE_OCL aClassifierDerive='aEnumeration\n'"
 * @generated
 */

public interface ALiteral extends AValue, ANamed {
	/**
	 * Returns the value of the '<em><b>AEnumeration</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AEnumeration</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AEnumeration</em>' reference.
	 * @see com.montages.acore.values.ValuesPackage#getALiteral_AEnumeration()
	 * @model required="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AEnumeration = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 * @generated
	 */
	AEnumeration getAEnumeration();

	/**
	 * Returns the value of the '<em><b>AQualified Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AQualified Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AQualified Name</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getALiteral_AQualifiedName()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Qualified Name'"
	 *        annotation="http://www.xocl.org/OCL derive='let var0: String = let e1: String = if aEnumeration.oclIsUndefined()\n  then null\n  else aEnumeration.aName\nendif.concat(\'::\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif in\nvar0\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Abstractions' createColumn='false'"
	 * @generated
	 */
	String getAQualifiedName();

} // ALiteral
