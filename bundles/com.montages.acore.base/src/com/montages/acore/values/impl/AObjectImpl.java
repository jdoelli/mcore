/**
 */

package com.montages.acore.values.impl;

import com.montages.acore.AResource;

import com.montages.acore.classifiers.AClassType;
import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.AFeature;
import com.montages.acore.classifiers.AReference;

import com.montages.acore.values.AObject;
import com.montages.acore.values.ASlot;
import com.montages.acore.values.AValue;
import com.montages.acore.values.ValuesPackage;

import java.lang.reflect.InvocationTargetException;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AObject</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAClassType <em>AClass Type</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getASlot <em>ASlot</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAContainingObject <em>AContaining Object</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAContainmentReference <em>AContainment Reference</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAContainingResource <em>AContaining Resource</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAFeatureWithoutSlot <em>AFeature Without Slot</em>}</li>
 *   <li>{@link com.montages.acore.values.impl.AObjectImpl#getAAllObject <em>AAll Object</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AObjectImpl extends AValueImpl implements AObject {
	/**
	 * The parsed OCL expression for the body of the '{@link #aNewSlot <em>ANew Slot</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aNewSlot
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aNewSlotclassifiersAFeaturevaluesAValueBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aSlotFromFeature <em>ASlot From Feature</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aSlotFromFeature
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aSlotFromFeatureclassifiersAFeatureBodyOCL;

	/**
	 * The parsed OCL expression for the body of the '{@link #aChoosableForAClass <em>AChoosable For AClass</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #aChoosableForAClass
	 * @templateTag DFGFI01
	 * @generated
	 */
	private static OCLExpression aChoosableForAClassclassifiersAClassTypeBodyOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassType <em>AClass Type</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassType
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aClassTypeDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getASlot <em>ASlot</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getASlot
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aSlotDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingObject <em>AContaining Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainmentReference <em>AContainment Reference</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainmentReference
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainmentReferenceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAContainingResource <em>AContaining Resource</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAContainingResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aContainingResourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFeatureWithoutSlot <em>AFeature Without Slot</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFeatureWithoutSlot
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFeatureWithoutSlotDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllObject <em>AAll Object</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aAllObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAClassifier <em>AClassifier</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAClassifier
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aClassifierDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AObjectImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ValuesPackage.Literals.AOBJECT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType getAClassType() {
		AClassType aClassType = basicGetAClassType();
		return aClassType != null && aClassType.eIsProxy() ? (AClassType) eResolveProxy((InternalEObject) aClassType)
				: aClassType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassType basicGetAClassType() {
		/**
		 * @OCL let nl: acore::classifiers::AClassType = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__ACLASS_TYPE;

		if (aClassTypeDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aClassTypeDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassTypeDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AClassType result = (AClassType) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ASlot> getASlot() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__ASLOT;

		if (aSlotDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aSlotDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aSlotDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<ASlot> result = (EList<ASlot>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject getAContainingObject() {
		AObject aContainingObject = basicGetAContainingObject();
		return aContainingObject != null && aContainingObject.eIsProxy()
				? (AObject) eResolveProxy((InternalEObject) aContainingObject) : aContainingObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AObject basicGetAContainingObject() {
		/**
		 * @OCL if aContainingSlot.oclIsUndefined()
		then null
		else aContainingSlot.aContainingObject
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__ACONTAINING_OBJECT;

		if (aContainingObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AObject result = (AObject) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AReference getAContainmentReference() {
		AReference aContainmentReference = basicGetAContainmentReference();
		return aContainmentReference != null && aContainmentReference.eIsProxy()
				? (AReference) eResolveProxy((InternalEObject) aContainmentReference) : aContainmentReference;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AReference basicGetAContainmentReference() {
		/**
		 * @OCL let chain: acore::classifiers::AFeature = if aContainingSlot.oclIsUndefined()
		then null
		else aContainingSlot.aInstantiatedFeature
		endif in
		if chain.oclIsUndefined()
		then null
		else if chain.oclIsKindOf(acore::classifiers::AReference)
		then chain.oclAsType(acore::classifiers::AReference)
		else null
		endif
		endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__ACONTAINMENT_REFERENCE;

		if (aContainmentReferenceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainmentReferenceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainmentReferenceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AReference result = (AReference) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AResource getAContainingResource() {
		AResource aContainingResource = basicGetAContainingResource();
		return aContainingResource != null && aContainingResource.eIsProxy()
				? (AResource) eResolveProxy((InternalEObject) aContainingResource) : aContainingResource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AResource basicGetAContainingResource() {
		/**
		 * @OCL let nl: acore::AResource = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__ACONTAINING_RESOURCE;

		if (aContainingResourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aContainingResourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aContainingResourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			AResource result = (AResource) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFeature> getAFeatureWithoutSlot() {
		/**
		 * @OCL if aClassType.oclIsUndefined()
		then OrderedSet{}
		else aClassType.aAllFeature
		endif->select(it: acore::classifiers::AFeature | let e0: Boolean = aSlot.aInstantiatedFeature->reject(oclIsUndefined())->asOrderedSet()->excludes(it)   in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() 
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__AFEATURE_WITHOUT_SLOT;

		if (aFeatureWithoutSlotDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFeatureWithoutSlotDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFeatureWithoutSlotDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFeature> result = (EList<AFeature>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAAllObject() {
		/**
		 * @OCL let e1: OrderedSet(acore::values::AObject)  = aSlot.aAllContainedObject->asOrderedSet()->including(self) ->asOrderedSet()   in 
		if e1->oclIsInvalid() then OrderedSet{} else e1 endif
		
		 * @templateTag GGFT01
		 */
		EClass eClass = ValuesPackage.Literals.AOBJECT;
		EStructuralFeature eFeature = ValuesPackage.Literals.AOBJECT__AALL_OBJECT;

		if (aAllObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aAllObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AObject> result = (EList<AObject>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASlot aNewSlot(AFeature feature, EList<AValue> value) {

		/**
		 * @OCL let nl: acore::values::ASlot = null in nl
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ValuesPackage.Literals.AOBJECT);
		EOperation eOperation = ValuesPackage.Literals.AOBJECT.getEOperations().get(0);
		if (aNewSlotclassifiersAFeaturevaluesAValueBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aNewSlotclassifiersAFeaturevaluesAValueBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, body, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aNewSlotclassifiersAFeaturevaluesAValueBodyOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("feature", feature);

			evalEnv.add("value", value);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASlot) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASlot aSlotFromFeature(AFeature feature) {

		/**
		 * @OCL let slots: OrderedSet(acore::values::ASlot)  = aSlot->asOrderedSet()->select(it: acore::values::ASlot | let e0: Boolean = it.aInstantiatedFeature = feature in 
		if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in
		let chain: OrderedSet(acore::values::ASlot)  = slots in
		if chain->first().oclIsUndefined() 
		then null 
		else chain->first()
		endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ValuesPackage.Literals.AOBJECT);
		EOperation eOperation = ValuesPackage.Literals.AOBJECT.getEOperations().get(1);
		if (aSlotFromFeatureclassifiersAFeatureBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aSlotFromFeatureclassifiersAFeatureBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, body, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aSlotFromFeatureclassifiersAFeatureBodyOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("feature", feature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (ASlot) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean aChoosableForAClass(AClassType classType) {

		/**
		 * @OCL let e1: Boolean = classType.aActiveClass = (let e2: Boolean = classType.aAbstract <> true in 
		if e2.oclIsInvalid() then null else e2 endif) in 
		if e1.oclIsInvalid() then null else e1 endif
		
		 * @templateTag IGOT01
		 */
		EClass eClass = (ValuesPackage.Literals.AOBJECT);
		EOperation eOperation = ValuesPackage.Literals.AOBJECT.getEOperations().get(2);
		if (aChoosableForAClassclassifiersAClassTypeBodyOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setOperationContext(eClass, eOperation);

			String body = XoclEmfUtil.findBodyAnnotationText(eOperation, eClass());

			try {
				aChoosableForAClassclassifiersAClassTypeBodyOCL = helper.createQuery(body);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, body, helper.getProblems(),
						ValuesPackage.Literals.AOBJECT, eOperation);
			}
		}

		Query query = OCL_ENV.createQuery(aChoosableForAClassclassifiersAClassTypeBodyOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, ValuesPackage.Literals.AOBJECT, eOperation);

			EvaluationEnvironment<?, ?, ?, ?, ?> evalEnv = query.getEvaluationEnvironment();

			evalEnv.add("classType", classType);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (Boolean) xoclEval.evaluateElement(eOperation, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case ValuesPackage.AOBJECT__ACLASS_TYPE:
			if (resolve)
				return getAClassType();
			return basicGetAClassType();
		case ValuesPackage.AOBJECT__ASLOT:
			return getASlot();
		case ValuesPackage.AOBJECT__ACONTAINING_OBJECT:
			if (resolve)
				return getAContainingObject();
			return basicGetAContainingObject();
		case ValuesPackage.AOBJECT__ACONTAINMENT_REFERENCE:
			if (resolve)
				return getAContainmentReference();
			return basicGetAContainmentReference();
		case ValuesPackage.AOBJECT__ACONTAINING_RESOURCE:
			if (resolve)
				return getAContainingResource();
			return basicGetAContainingResource();
		case ValuesPackage.AOBJECT__AFEATURE_WITHOUT_SLOT:
			return getAFeatureWithoutSlot();
		case ValuesPackage.AOBJECT__AALL_OBJECT:
			return getAAllObject();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case ValuesPackage.AOBJECT__ACLASS_TYPE:
			return basicGetAClassType() != null;
		case ValuesPackage.AOBJECT__ASLOT:
			return !getASlot().isEmpty();
		case ValuesPackage.AOBJECT__ACONTAINING_OBJECT:
			return basicGetAContainingObject() != null;
		case ValuesPackage.AOBJECT__ACONTAINMENT_REFERENCE:
			return basicGetAContainmentReference() != null;
		case ValuesPackage.AOBJECT__ACONTAINING_RESOURCE:
			return basicGetAContainingResource() != null;
		case ValuesPackage.AOBJECT__AFEATURE_WITHOUT_SLOT:
			return !getAFeatureWithoutSlot().isEmpty();
		case ValuesPackage.AOBJECT__AALL_OBJECT:
			return !getAAllObject().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
		case ValuesPackage.AOBJECT___ANEW_SLOT__AFEATURE_ELIST:
			return aNewSlot((AFeature) arguments.get(0), (EList<AValue>) arguments.get(1));
		case ValuesPackage.AOBJECT___ASLOT_FROM_FEATURE__AFEATURE:
			return aSlotFromFeature((AFeature) arguments.get(0));
		case ValuesPackage.AOBJECT___ACHOOSABLE_FOR_ACLASS__ACLASSTYPE:
			return aChoosableForAClass((AClassType) arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aClassifier aClassType
	
	 * @templateTag INS02
	 * @generated
	 */
	@Override
	public AClassifier basicGetAClassifier() {
		EClass eClass = (ValuesPackage.Literals.AOBJECT);
		EStructuralFeature eOverrideFeature = ValuesPackage.Literals.AVALUE__ACLASSIFIER;

		if (aClassifierDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aClassifierDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(ValuesPackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aClassifierDeriveOCL);
		try {
			XoclErrorHandler.enterContext(ValuesPackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (AClassifier) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AObjectImpl
