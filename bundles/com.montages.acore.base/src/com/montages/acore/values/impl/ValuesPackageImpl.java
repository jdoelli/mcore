/**
 */
package com.montages.acore.values.impl;

import com.montages.acore.AcorePackage;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.impl.AbstractionsPackageImpl;

import com.montages.acore.annotations.AnnotationsPackage;

import com.montages.acore.annotations.impl.AnnotationsPackageImpl;

import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.classifiers.impl.ClassifiersPackageImpl;

import com.montages.acore.impl.AcorePackageImpl;

import com.montages.acore.updates.UpdatesPackage;

import com.montages.acore.updates.impl.UpdatesPackageImpl;

import com.montages.acore.values.ADataValue;
import com.montages.acore.values.ALiteral;
import com.montages.acore.values.AObject;
import com.montages.acore.values.ASlot;
import com.montages.acore.values.AValue;
import com.montages.acore.values.ValuesFactory;
import com.montages.acore.values.ValuesPackage;

import com.montages.acore.values.rules.RulesPackage;

import com.montages.acore.values.rules.impl.RulesPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ValuesPackageImpl extends EPackageImpl implements ValuesPackage {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aDataValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aLiteralEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aSlotEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see com.montages.acore.values.ValuesPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ValuesPackageImpl() {
		super(eNS_URI, ValuesFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ValuesPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ValuesPackage init() {
		if (isInited)
			return (ValuesPackage) EPackage.Registry.INSTANCE.getEPackage(ValuesPackage.eNS_URI);

		// Obtain or create and register package
		ValuesPackageImpl theValuesPackage = (ValuesPackageImpl) (EPackage.Registry.INSTANCE
				.get(eNS_URI) instanceof ValuesPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI)
						: new ValuesPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		AcorePackageImpl theAcorePackage = (AcorePackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AcorePackage.eNS_URI) instanceof AcorePackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI) : AcorePackage.eINSTANCE);
		AbstractionsPackageImpl theAbstractionsPackage = (AbstractionsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI) instanceof AbstractionsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AbstractionsPackage.eNS_URI)
						: AbstractionsPackage.eINSTANCE);
		ClassifiersPackageImpl theClassifiersPackage = (ClassifiersPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI) instanceof ClassifiersPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(ClassifiersPackage.eNS_URI)
						: ClassifiersPackage.eINSTANCE);
		AnnotationsPackageImpl theAnnotationsPackage = (AnnotationsPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(AnnotationsPackage.eNS_URI) instanceof AnnotationsPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(AnnotationsPackage.eNS_URI)
						: AnnotationsPackage.eINSTANCE);
		RulesPackageImpl theRulesPackage = (RulesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(RulesPackage.eNS_URI) instanceof RulesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI) : RulesPackage.eINSTANCE);
		UpdatesPackageImpl theUpdatesPackage = (UpdatesPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI) instanceof UpdatesPackageImpl
						? EPackage.Registry.INSTANCE.getEPackage(UpdatesPackage.eNS_URI) : UpdatesPackage.eINSTANCE);

		// Create package meta-data objects
		theValuesPackage.createPackageContents();
		theAcorePackage.createPackageContents();
		theAbstractionsPackage.createPackageContents();
		theClassifiersPackage.createPackageContents();
		theAnnotationsPackage.createPackageContents();
		theRulesPackage.createPackageContents();
		theUpdatesPackage.createPackageContents();

		// Initialize created meta-data
		theValuesPackage.initializePackageContents();
		theAcorePackage.initializePackageContents();
		theAbstractionsPackage.initializePackageContents();
		theClassifiersPackage.initializePackageContents();
		theAnnotationsPackage.initializePackageContents();
		theRulesPackage.initializePackageContents();
		theUpdatesPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theValuesPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ValuesPackage.eNS_URI, theValuesPackage);
		return theValuesPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAValue() {
		return aValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAValue_AClassifier() {
		return (EReference) aValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAValue_AContainingSlot() {
		return (EReference) aValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getADataValue() {
		return aDataValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getADataValue_AValue() {
		return (EAttribute) aDataValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getADataValue_ADataType() {
		return (EReference) aDataValueEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getALiteral() {
		return aLiteralEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getALiteral_AEnumeration() {
		return (EReference) aLiteralEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getALiteral_AQualifiedName() {
		return (EAttribute) aLiteralEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAObject() {
		return aObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AClassType() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_ASlot() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AContainingObject() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AContainmentReference() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AContainingResource() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AFeatureWithoutSlot() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAObject_AAllObject() {
		return (EReference) aObjectEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAObject__ANewSlot__AFeature_EList() {
		return aObjectEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAObject__ASlotFromFeature__AFeature() {
		return aObjectEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAObject__AChoosableForAClass__AClassType() {
		return aObjectEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getASlot() {
		return aSlotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getASlot_AKey() {
		return (EAttribute) aSlotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getASlot_AInstantiatedFeature() {
		return (EReference) aSlotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getASlot_AValue() {
		return (EReference) aSlotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getASlot_AIsContainment() {
		return (EAttribute) aSlotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getASlot_AContainingObject() {
		return (EReference) aSlotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getASlot_AFeatureIsClassTyped() {
		return (EAttribute) aSlotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getASlot_AFeatureIsDataTyped() {
		return (EAttribute) aSlotEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getASlot_ADirectlyContainedObject() {
		return (EReference) aSlotEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getASlot_AAllContainedObject() {
		return (EReference) aSlotEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getASlot_AFeatureOfWrongClass() {
		return (EAttribute) aSlotEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getASlot__AEvaluate() {
		return aSlotEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getASlot__AUpdate__AUpdateMode_EList() {
		return aSlotEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValuesFactory getValuesFactory() {
		return (ValuesFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		aValueEClass = createEClass(AVALUE);
		createEReference(aValueEClass, AVALUE__ACLASSIFIER);
		createEReference(aValueEClass, AVALUE__ACONTAINING_SLOT);

		aDataValueEClass = createEClass(ADATA_VALUE);
		createEAttribute(aDataValueEClass, ADATA_VALUE__AVALUE);
		createEReference(aDataValueEClass, ADATA_VALUE__ADATA_TYPE);

		aLiteralEClass = createEClass(ALITERAL);
		createEReference(aLiteralEClass, ALITERAL__AENUMERATION);
		createEAttribute(aLiteralEClass, ALITERAL__AQUALIFIED_NAME);

		aObjectEClass = createEClass(AOBJECT);
		createEReference(aObjectEClass, AOBJECT__ACLASS_TYPE);
		createEReference(aObjectEClass, AOBJECT__ASLOT);
		createEReference(aObjectEClass, AOBJECT__ACONTAINING_OBJECT);
		createEReference(aObjectEClass, AOBJECT__ACONTAINMENT_REFERENCE);
		createEReference(aObjectEClass, AOBJECT__ACONTAINING_RESOURCE);
		createEReference(aObjectEClass, AOBJECT__AFEATURE_WITHOUT_SLOT);
		createEReference(aObjectEClass, AOBJECT__AALL_OBJECT);
		createEOperation(aObjectEClass, AOBJECT___ANEW_SLOT__AFEATURE_ELIST);
		createEOperation(aObjectEClass, AOBJECT___ASLOT_FROM_FEATURE__AFEATURE);
		createEOperation(aObjectEClass, AOBJECT___ACHOOSABLE_FOR_ACLASS__ACLASSTYPE);

		aSlotEClass = createEClass(ASLOT);
		createEAttribute(aSlotEClass, ASLOT__AKEY);
		createEReference(aSlotEClass, ASLOT__AINSTANTIATED_FEATURE);
		createEReference(aSlotEClass, ASLOT__AVALUE);
		createEAttribute(aSlotEClass, ASLOT__AIS_CONTAINMENT);
		createEReference(aSlotEClass, ASLOT__ACONTAINING_OBJECT);
		createEAttribute(aSlotEClass, ASLOT__AFEATURE_IS_CLASS_TYPED);
		createEAttribute(aSlotEClass, ASLOT__AFEATURE_IS_DATA_TYPED);
		createEReference(aSlotEClass, ASLOT__ADIRECTLY_CONTAINED_OBJECT);
		createEReference(aSlotEClass, ASLOT__AALL_CONTAINED_OBJECT);
		createEAttribute(aSlotEClass, ASLOT__AFEATURE_OF_WRONG_CLASS);
		createEOperation(aSlotEClass, ASLOT___AEVALUATE);
		createEOperation(aSlotEClass, ASLOT___AUPDATE__AUPDATEMODE_ELIST);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RulesPackage theRulesPackage = (RulesPackage) EPackage.Registry.INSTANCE.getEPackage(RulesPackage.eNS_URI);
		AbstractionsPackage theAbstractionsPackage = (AbstractionsPackage) EPackage.Registry.INSTANCE
				.getEPackage(AbstractionsPackage.eNS_URI);
		ClassifiersPackage theClassifiersPackage = (ClassifiersPackage) EPackage.Registry.INSTANCE
				.getEPackage(ClassifiersPackage.eNS_URI);
		AcorePackage theAcorePackage = (AcorePackage) EPackage.Registry.INSTANCE.getEPackage(AcorePackage.eNS_URI);
		UpdatesPackage theUpdatesPackage = (UpdatesPackage) EPackage.Registry.INSTANCE
				.getEPackage(UpdatesPackage.eNS_URI);

		// Add subpackages
		getESubpackages().add(theRulesPackage);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		aValueEClass.getESuperTypes().add(theAbstractionsPackage.getAElement());
		aDataValueEClass.getESuperTypes().add(this.getAValue());
		aLiteralEClass.getESuperTypes().add(this.getAValue());
		aLiteralEClass.getESuperTypes().add(theAbstractionsPackage.getANamed());
		aObjectEClass.getESuperTypes().add(this.getAValue());
		aSlotEClass.getESuperTypes().add(theAbstractionsPackage.getAElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(aValueEClass, AValue.class, "AValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAValue_AClassifier(), theClassifiersPackage.getAClassifier(), null, "aClassifier", null, 0, 1,
				AValue.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAValue_AContainingSlot(), this.getASlot(), null, "aContainingSlot", null, 0, 1, AValue.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aDataValueEClass, ADataValue.class, "ADataValue", IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getADataValue_AValue(), ecorePackage.getEString(), "aValue", null, 0, 1, ADataValue.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getADataValue_ADataType(), theClassifiersPackage.getADataType(), null, "aDataType", null, 1, 1,
				ADataValue.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(aLiteralEClass, ALiteral.class, "ALiteral", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getALiteral_AEnumeration(), theClassifiersPackage.getAEnumeration(), null, "aEnumeration", null,
				1, 1, ALiteral.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getALiteral_AQualifiedName(), ecorePackage.getEString(), "aQualifiedName", null, 0, 1,
				ALiteral.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEClass(aObjectEClass, AObject.class, "AObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAObject_AClassType(), theClassifiersPackage.getAClassType(), null, "aClassType", null, 0, 1,
				AObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAObject_ASlot(), this.getASlot(), null, "aSlot", null, 0, -1, AObject.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getAObject_AContainingObject(), this.getAObject(), null, "aContainingObject", null, 0, 1,
				AObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAObject_AContainmentReference(), theClassifiersPackage.getAReference(), null,
				"aContainmentReference", null, 0, 1, AObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAObject_AContainingResource(), theAcorePackage.getAResource(), null, "aContainingResource",
				null, 0, 1, AObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAObject_AFeatureWithoutSlot(), theClassifiersPackage.getAFeature(), null,
				"aFeatureWithoutSlot", null, 0, -1, AObject.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getAObject_AAllObject(), this.getAObject(), null, "aAllObject", null, 0, -1, AObject.class,
				IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getAObject__ANewSlot__AFeature_EList(), this.getASlot(), "aNewSlot", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theClassifiersPackage.getAFeature(), "feature", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAValue(), "value", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAObject__ASlotFromFeature__AFeature(), this.getASlot(), "aSlotFromFeature", 0, 1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theClassifiersPackage.getAFeature(), "feature", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getAObject__AChoosableForAClass__AClassType(), ecorePackage.getEBooleanObject(),
				"aChoosableForAClass", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theClassifiersPackage.getAClassType(), "classType", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(aSlotEClass, ASlot.class, "ASlot", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getASlot_AKey(), ecorePackage.getEString(), "aKey", null, 0, 1, ASlot.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getASlot_AInstantiatedFeature(), theClassifiersPackage.getAFeature(), null,
				"aInstantiatedFeature", null, 0, 1, ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getASlot_AValue(), this.getAValue(), null, "aValue", null, 0, -1, ASlot.class, IS_TRANSIENT,
				IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getASlot_AIsContainment(), ecorePackage.getEBooleanObject(), "aIsContainment", null, 0, 1,
				ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, IS_DERIVED,
				IS_ORDERED);
		initEReference(getASlot_AContainingObject(), this.getAObject(), null, "aContainingObject", null, 0, 1,
				ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getASlot_AFeatureIsClassTyped(), ecorePackage.getEBooleanObject(), "aFeatureIsClassTyped", null,
				0, 1, ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEAttribute(getASlot_AFeatureIsDataTyped(), ecorePackage.getEBooleanObject(), "aFeatureIsDataTyped", null, 0,
				1, ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);
		initEReference(getASlot_ADirectlyContainedObject(), this.getAObject(), null, "aDirectlyContainedObject", null,
				0, -1, ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEReference(getASlot_AAllContainedObject(), this.getAObject(), null, "aAllContainedObject", null, 0, -1,
				ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getASlot_AFeatureOfWrongClass(), ecorePackage.getEBooleanObject(), "aFeatureOfWrongClass", null,
				0, 1, ASlot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				IS_DERIVED, IS_ORDERED);

		initEOperation(getASlot__AEvaluate(), this.getAValue(), "aEvaluate", 0, -1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getASlot__AUpdate__AUpdateMode_EList(), theUpdatesPackage.getAUpdate(), "aUpdate", 0, -1,
				IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theUpdatesPackage.getAUpdateMode(), "updateMode", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getAValue(), "trg", 0, -1, IS_UNIQUE, IS_ORDERED);

		// Create annotations
		// http://www.montages.com/mCore/MCore
		createMCoreAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/EDITORCONFIG
		createEDITORCONFIGAnnotations();
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OVERRIDE_OCL
		createOVERRIDE_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.montages.com/mCore/MCore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createMCoreAnnotations() {
		String source = "http://www.montages.com/mCore/MCore";
		addAnnotation(aValueEClass, source, new String[] { "mName", "Value" });
		addAnnotation(getAValue_AContainingSlot(), source, new String[] { "mName", "Containing Slot" });
		addAnnotation(aDataValueEClass, source, new String[] { "mName", "Data Value" });
		addAnnotation(getADataValue_AValue(), source, new String[] { "mName", "Value" });
		addAnnotation(aLiteralEClass, source, new String[] { "mName", "Literal" });
		addAnnotation(getALiteral_AQualifiedName(), source, new String[] { "mName", "Qualified Name" });
		addAnnotation(aObjectEClass, source, new String[] { "mName", "Object" });
		addAnnotation(getAObject__ANewSlot__AFeature_EList(), source, new String[] { "mName", "New Slot" });
		addAnnotation(getAObject__ASlotFromFeature__AFeature(), source, new String[] { "mName", "Slot From Feature" });
		addAnnotation(getAObject__AChoosableForAClass__AClassType(), source,
				new String[] { "mName", "Choosable For A Class" });
		addAnnotation(getAObject_AContainingObject(), source, new String[] { "mName", "Containing Object" });
		addAnnotation(getAObject_AContainmentReference(), source, new String[] { "mName", "Containment Reference" });
		addAnnotation(getAObject_AContainingResource(), source, new String[] { "mName", "Containing Resource" });
		addAnnotation(getAObject_AFeatureWithoutSlot(), source, new String[] { "mName", "Feature Without Slot" });
		addAnnotation(getAObject_AAllObject(), source, new String[] { "mName", "All Object" });
		addAnnotation(aSlotEClass, source, new String[] { "mName", "Slot" });
		addAnnotation(getASlot__AEvaluate(), source, new String[] { "mName", "Evaluate" });
		addAnnotation(getASlot__AUpdate__AUpdateMode_EList(), source, new String[] { "mName", "Update" });
		addAnnotation(getASlot_AKey(), source, new String[] { "mName", "Key" });
		addAnnotation(getASlot_AInstantiatedFeature(), source, new String[] { "mName", "Instantiated Feature" });
		addAnnotation(getASlot_AIsContainment(), source, new String[] { "mName", "Is Containment" });
		addAnnotation(getASlot_AContainingObject(), source, new String[] { "mName", "Containing Object" });
		addAnnotation(getASlot_AFeatureIsClassTyped(), source, new String[] { "mName", "Feature Is Class Typed" });
		addAnnotation(getASlot_AFeatureIsDataTyped(), source, new String[] { "mName", "Feature Is Data Typed" });
		addAnnotation(getASlot_ADirectlyContainedObject(), source,
				new String[] { "mName", "Directly Contained Object" });
		addAnnotation(getASlot_AAllContainedObject(), source, new String[] { "mName", "All Contained Object" });
		addAnnotation(getASlot_AFeatureOfWrongClass(), source, new String[] { "mName", "Feature Of Wrong Class" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(getAValue_AClassifier(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassifier = null in nl\n" });
		addAnnotation(getAValue_AContainingSlot(), source,
				new String[] { "derive", "let nl: acore::values::ASlot = null in nl\n" });
		addAnnotation(getADataValue_AValue(), source, new String[] { "derive", "\'TO BE DONE\'\n" });
		addAnnotation(getADataValue_ADataType(), source,
				new String[] { "derive", "let nl: acore::classifiers::ADataType = null in nl\n" });
		addAnnotation(getALiteral_AEnumeration(), source,
				new String[] { "derive", "let nl: acore::classifiers::AEnumeration = null in nl\n" });
		addAnnotation(getALiteral_AQualifiedName(), source, new String[] { "derive",
				"let var0: String = let e1: String = if aEnumeration.oclIsUndefined()\n  then null\n  else aEnumeration.aName\nendif.concat(\'::\').concat(aName) in \n if e1.oclIsInvalid() then null else e1 endif in\nvar0\n" });
		addAnnotation(getAObject__ANewSlot__AFeature_EList(), source,
				new String[] { "body", "let nl: acore::values::ASlot = null in nl\n" });
		addAnnotation(getAObject__ASlotFromFeature__AFeature(), source, new String[] { "body",
				"let slots: OrderedSet(acore::values::ASlot)  = aSlot->asOrderedSet()->select(it: acore::values::ASlot | let e0: Boolean = it.aInstantiatedFeature = feature in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet()  in\nlet chain: OrderedSet(acore::values::ASlot)  = slots in\nif chain->first().oclIsUndefined() \n then null \n else chain->first()\n  endif\n" });
		addAnnotation(getAObject__AChoosableForAClass__AClassType(), source, new String[] { "body",
				"let e1: Boolean = classType.aActiveClass = (let e2: Boolean = classType.aAbstract <> true in \n if e2.oclIsInvalid() then null else e2 endif) in \n if e1.oclIsInvalid() then null else e1 endif\n" });
		addAnnotation(getAObject_AClassType(), source,
				new String[] { "derive", "let nl: acore::classifiers::AClassType = null in nl\n" });
		addAnnotation(getAObject_ASlot(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getAObject_AContainingObject(), source, new String[] { "derive",
				"if aContainingSlot.oclIsUndefined()\n  then null\n  else aContainingSlot.aContainingObject\nendif\n" });
		addAnnotation(getAObject_AContainmentReference(), source, new String[] { "derive",
				"let chain: acore::classifiers::AFeature = if aContainingSlot.oclIsUndefined()\n  then null\n  else aContainingSlot.aInstantiatedFeature\nendif in\nif chain.oclIsUndefined()\n  then null\n  else if chain.oclIsKindOf(acore::classifiers::AReference)\n    then chain.oclAsType(acore::classifiers::AReference)\n    else null\n  endif\n  endif\n" });
		addAnnotation(getAObject_AContainingResource(), source,
				new String[] { "derive", "let nl: acore::AResource = null in nl\n" });
		addAnnotation(getAObject_AFeatureWithoutSlot(), source, new String[] { "derive",
				"if aClassType.oclIsUndefined()\n  then OrderedSet{}\n  else aClassType.aAllFeature\nendif->select(it: acore::classifiers::AFeature | let e0: Boolean = aSlot.aInstantiatedFeature->reject(oclIsUndefined())->asOrderedSet()->excludes(it)   in \n if e0.oclIsInvalid() then null else e0 endif)->asOrderedSet()->excluding(null)->asOrderedSet() \n" });
		addAnnotation(getAObject_AAllObject(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::values::AObject)  = aSlot.aAllContainedObject->asOrderedSet()->including(self) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getASlot__AEvaluate(), source, new String[] { "body", "null" });
		addAnnotation(getASlot__AUpdate__AUpdateMode_EList(), source, new String[] { "body", "null" });
		addAnnotation(getASlot_AKey(), source, new String[] { "derive", "\'\'\n" });
		addAnnotation(getASlot_AInstantiatedFeature(), source,
				new String[] { "derive", "let nl: acore::classifiers::AFeature = null in nl\n" });
		addAnnotation(getASlot_AValue(), source, new String[] { "derive", "OrderedSet{}\n" });
		addAnnotation(getASlot_AIsContainment(), source, new String[] { "derive", "false\n" });
		addAnnotation(getASlot_AContainingObject(), source,
				new String[] { "derive", "let nl: acore::values::AObject = null in nl\n" });
		addAnnotation(getASlot_AFeatureIsClassTyped(), source, new String[] { "derive",
				"if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveClass\nendif) \n  =true \nthen true\n  else false\nendif\n" });
		addAnnotation(getASlot_AFeatureIsDataTyped(), source, new String[] { "derive",
				"if (let e0: Boolean = if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveDataType\nendif)= true \n then true \n else if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveEnumeration\nendif)= true \n then true \nelse if ((if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveDataType\nendif)= null or (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveEnumeration\nendif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else false\nendif\n" });
		addAnnotation(getASlot_ADirectlyContainedObject(), source, new String[] { "derive",
				"if (aIsContainment) \n  =true \nthen let chain: OrderedSet(acore::values::AValue)  = aValue->asOrderedSet() in\nchain->iterate(i:acore::values::AValue; r: OrderedSet(acore::values::AObject)=OrderedSet{} | if i.oclIsKindOf(acore::values::AObject) then r->including(i.oclAsType(acore::values::AObject))->asOrderedSet() \n else r endif)\n  else OrderedSet{}\nendif\n" });
		addAnnotation(getASlot_AAllContainedObject(), source, new String[] { "derive",
				"let e1: OrderedSet(acore::values::AObject)  = aDirectlyContainedObject->asOrderedSet()->union(aDirectlyContainedObject.aSlot.aAllContainedObject->reject(oclIsUndefined())->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n" });
		addAnnotation(getASlot_AFeatureOfWrongClass(), source, new String[] { "derive",
				"let e1: Boolean = if aContainingObject.aClassType.oclIsUndefined()\n  then OrderedSet{}\n  else aContainingObject.aClassType.aAllFeature\nendif->excludes(aInstantiatedFeature)   in \n if e1.oclIsInvalid() then null else e1 endif\n" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EDITORCONFIG</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEDITORCONFIGAnnotations() {
		String source = "http://www.xocl.org/EDITORCONFIG";
		addAnnotation(getAValue_AClassifier(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getAValue_AContainingSlot(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getADataValue_AValue(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getADataValue_ADataType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getALiteral_AEnumeration(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getALiteral_AQualifiedName(), source,
				new String[] { "propertyCategory", "z A Core/Abstractions", "createColumn", "false" });
		addAnnotation(getAObject__ANewSlot__AFeature_EList(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "true" });
		addAnnotation(getAObject__ASlotFromFeature__AFeature(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "true" });
		addAnnotation(getAObject__AChoosableForAClass__AClassType(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "true" });
		addAnnotation(getAObject_AClassType(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getAObject_ASlot(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getAObject_AContainingObject(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "false" });
		addAnnotation(getAObject_AContainmentReference(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "false" });
		addAnnotation(getAObject_AContainingResource(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "false" });
		addAnnotation(getAObject_AFeatureWithoutSlot(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getAObject_AAllObject(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getASlot__AEvaluate(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "true" });
		addAnnotation(getASlot__AUpdate__AUpdateMode_EList(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "true" });
		addAnnotation(getASlot_AKey(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getASlot_AInstantiatedFeature(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getASlot_AValue(), source,
				new String[] { "createColumn", "false", "propertyCategory", "z A Core/Values" });
		addAnnotation(getASlot_AIsContainment(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "false" });
		addAnnotation(getASlot_AContainingObject(), source,
				new String[] { "propertyCategory", "z A Core/Values", "createColumn", "false" });
		addAnnotation(getASlot_AFeatureIsClassTyped(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getASlot_AFeatureIsDataTyped(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getASlot_ADirectlyContainedObject(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getASlot_AAllContainedObject(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
		addAnnotation(getASlot_AFeatureOfWrongClass(), source,
				new String[] { "propertyCategory", "z A Core Helpers", "createColumn", "false" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(getAValue_AClassifier(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getAValue_AContainingSlot(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataValue_AValue(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
		addAnnotation(getADataValue_ADataType(), source,
				new String[] { "propertyFilterFlags", "org.eclipse.ui.views.properties.expert" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OVERRIDE_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOVERRIDE_OCLAnnotations() {
		String source = "http://www.xocl.org/OVERRIDE_OCL";
		addAnnotation(aDataValueEClass, source, new String[] { "aClassifierDerive", "aDataType\n" });
		addAnnotation(aLiteralEClass, source, new String[] { "aClassifierDerive", "aEnumeration\n" });
		addAnnotation(aObjectEClass, source, new String[] { "aClassifierDerive", "aClassType\n" });
	}

} //ValuesPackageImpl
