/**
 */

package com.montages.acore.values;

import com.montages.acore.abstractions.AElement;

import com.montages.acore.classifiers.AFeature;

import com.montages.acore.updates.AUpdate;
import com.montages.acore.updates.AUpdateMode;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ASlot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link com.montages.acore.values.ASlot#getAKey <em>AKey</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAInstantiatedFeature <em>AInstantiated Feature</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAValue <em>AValue</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAIsContainment <em>AIs Containment</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAContainingObject <em>AContaining Object</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAFeatureIsClassTyped <em>AFeature Is Class Typed</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAFeatureIsDataTyped <em>AFeature Is Data Typed</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getADirectlyContainedObject <em>ADirectly Contained Object</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAAllContainedObject <em>AAll Contained Object</em>}</li>
 *   <li>{@link com.montages.acore.values.ASlot#getAFeatureOfWrongClass <em>AFeature Of Wrong Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see com.montages.acore.values.ValuesPackage#getASlot()
 * @model abstract="true"
 *        annotation="http://www.montages.com/mCore/MCore mName='Slot'"
 * @generated
 */

public interface ASlot extends AElement {
	/**
	 * Returns the value of the '<em><b>AKey</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AKey</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AKey</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AKey()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Key'"
	 *        annotation="http://www.xocl.org/OCL derive='\'\'\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 * @generated
	 */
	String getAKey();

	/**
	 * Returns the value of the '<em><b>AInstantiated Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AInstantiated Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AInstantiated Feature</em>' reference.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AInstantiatedFeature()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Instantiated Feature'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::classifiers::AFeature = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 * @generated
	 */
	AFeature getAInstantiatedFeature();

	/**
	 * Returns the value of the '<em><b>AValue</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AValue}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AValue</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AValue</em>' reference list.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AValue()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.xocl.org/OCL derive='OrderedSet{}\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG createColumn='false' propertyCategory='z A Core/Values'"
	 * @generated
	 */
	EList<AValue> getAValue();

	/**
	 * Returns the value of the '<em><b>AIs Containment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AIs Containment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AIs Containment</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AIsContainment()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Is Containment'"
	 *        annotation="http://www.xocl.org/OCL derive='false\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Values' createColumn='false'"
	 * @generated
	 */
	Boolean getAIsContainment();

	/**
	 * Returns the value of the '<em><b>AContaining Object</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AContaining Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AContaining Object</em>' reference.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AContainingObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Containing Object'"
	 *        annotation="http://www.xocl.org/OCL derive='let nl: acore::values::AObject = null in nl\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Values' createColumn='false'"
	 * @generated
	 */
	AObject getAContainingObject();

	/**
	 * Returns the value of the '<em><b>AFeature Is Class Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature Is Class Typed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature Is Class Typed</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AFeatureIsClassTyped()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Feature Is Class Typed'"
	 *        annotation="http://www.xocl.org/OCL derive='if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveClass\nendif) \n  =true \nthen true\n  else false\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core Helpers' createColumn='false'"
	 * @generated
	 */
	Boolean getAFeatureIsClassTyped();

	/**
	 * Returns the value of the '<em><b>AFeature Is Data Typed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature Is Data Typed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature Is Data Typed</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AFeatureIsDataTyped()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Feature Is Data Typed'"
	 *        annotation="http://www.xocl.org/OCL derive='if (let e0: Boolean = if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveDataType\nendif)= true \n then true \n else if (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveEnumeration\nendif)= true \n then true \nelse if ((if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveDataType\nendif)= null or (if aInstantiatedFeature.aClassifier.oclIsUndefined()\n  then null\n  else aInstantiatedFeature.aClassifier.aActiveEnumeration\nendif)= null) = true \n then null \n else false endif endif endif in \n if e0.oclIsInvalid() then null else e0 endif) \n  =true \nthen true\n  else false\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core Helpers' createColumn='false'"
	 * @generated
	 */
	Boolean getAFeatureIsDataTyped();

	/**
	 * Returns the value of the '<em><b>ADirectly Contained Object</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>ADirectly Contained Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>ADirectly Contained Object</em>' reference list.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_ADirectlyContainedObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Directly Contained Object'"
	 *        annotation="http://www.xocl.org/OCL derive='if (aIsContainment) \n  =true \nthen let chain: OrderedSet(acore::values::AValue)  = aValue->asOrderedSet() in\nchain->iterate(i:acore::values::AValue; r: OrderedSet(acore::values::AObject)=OrderedSet{} | if i.oclIsKindOf(acore::values::AObject) then r->including(i.oclAsType(acore::values::AObject))->asOrderedSet() \n else r endif)\n  else OrderedSet{}\nendif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core Helpers' createColumn='false'"
	 * @generated
	 */
	EList<AObject> getADirectlyContainedObject();

	/**
	 * Returns the value of the '<em><b>AAll Contained Object</b></em>' reference list.
	 * The list contents are of type {@link com.montages.acore.values.AObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AAll Contained Object</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AAll Contained Object</em>' reference list.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AAllContainedObject()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='All Contained Object'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: OrderedSet(acore::values::AObject)  = aDirectlyContainedObject->asOrderedSet()->union(aDirectlyContainedObject.aSlot.aAllContainedObject->reject(oclIsUndefined())->asOrderedSet()) ->asOrderedSet()   in \n    if e1->oclIsInvalid() then OrderedSet{} else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core Helpers' createColumn='false'"
	 * @generated
	 */
	EList<AObject> getAAllContainedObject();

	/**
	 * Returns the value of the '<em><b>AFeature Of Wrong Class</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>AFeature Of Wrong Class</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>AFeature Of Wrong Class</em>' attribute.
	 * @see com.montages.acore.values.ValuesPackage#getASlot_AFeatureOfWrongClass()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Feature Of Wrong Class'"
	 *        annotation="http://www.xocl.org/OCL derive='let e1: Boolean = if aContainingObject.aClassType.oclIsUndefined()\n  then OrderedSet{}\n  else aContainingObject.aClassType.aAllFeature\nendif->excludes(aInstantiatedFeature)   in \n if e1.oclIsInvalid() then null else e1 endif\n'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core Helpers' createColumn='false'"
	 * @generated
	 */
	Boolean getAFeatureOfWrongClass();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.montages.com/mCore/MCore mName='Evaluate'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Values' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	EList<AValue> aEvaluate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model trgMany="true"
	 *        annotation="http://www.montages.com/mCore/MCore mName='Update'"
	 *        annotation="http://www.xocl.org/EDITORCONFIG propertyCategory='z A Core/Values' createColumn='true'"
	 *        annotation="http://www.xocl.org/OCL body='null'"
	 * @generated
	 */
	EList<AUpdate> aUpdate(AUpdateMode updateMode, EList<AValue> trg);

} // ASlot
