/**
 */

package com.montages.acore.impl;

import com.montages.acore.AAbstractFolder;
import com.montages.acore.AFolder;
import com.montages.acore.APackage;
import com.montages.acore.AResource;
import com.montages.acore.AcorePackage;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AAbstract Folder</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.impl.AAbstractFolderImpl#getAPackage <em>APackage</em>}</li>
 *   <li>{@link com.montages.acore.impl.AAbstractFolderImpl#getAResource <em>AResource</em>}</li>
 *   <li>{@link com.montages.acore.impl.AAbstractFolderImpl#getAFolder <em>AFolder</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AAbstractFolderImpl extends AStructuringElementImpl implements AAbstractFolder {
	/**
	 * The parsed OCL expression for the derivation of '{@link #getAPackage <em>APackage</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aPackageDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAResource <em>AResource</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aResourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAFolder <em>AFolder</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAFolder
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aFolderDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAAllPackages <em>AAll Packages</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAAllPackages
	 * @templateTag DFGFI05
	 * @generated
	 */
	private static OCLExpression aAllPackagesDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";
	/**
	 * The OVERRIDE_OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI11
	 * @generated
	 */
	private static final String OVERRIDE_OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OVERRIDE_OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AAbstractFolderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcorePackage.Literals.AABSTRACT_FOLDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<APackage> getAPackage() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.AABSTRACT_FOLDER;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__APACKAGE;

		if (aPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.AABSTRACT_FOLDER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.AABSTRACT_FOLDER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<APackage> result = (EList<APackage>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AResource> getAResource() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.AABSTRACT_FOLDER;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__ARESOURCE;

		if (aResourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aResourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.AABSTRACT_FOLDER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aResourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.AABSTRACT_FOLDER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AResource> result = (EList<AResource>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AFolder> getAFolder() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.AABSTRACT_FOLDER;
		EStructuralFeature eFeature = AcorePackage.Literals.AABSTRACT_FOLDER__AFOLDER;

		if (aFolderDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aFolderDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.AABSTRACT_FOLDER, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aFolderDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.AABSTRACT_FOLDER,
					eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AFolder> result = (EList<AFolder>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AcorePackage.AABSTRACT_FOLDER__APACKAGE:
			return getAPackage();
		case AcorePackage.AABSTRACT_FOLDER__ARESOURCE:
			return getAResource();
		case AcorePackage.AABSTRACT_FOLDER__AFOLDER:
			return getAFolder();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AcorePackage.AABSTRACT_FOLDER__APACKAGE:
			return !getAPackage().isEmpty();
		case AcorePackage.AABSTRACT_FOLDER__ARESOURCE:
			return !getAResource().isEmpty();
		case AcorePackage.AABSTRACT_FOLDER__AFOLDER:
			return !getAFolder().isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @overrideOCL aAllPackages let e1: OrderedSet(acore::APackage)  = let chain11: OrderedSet(acore::APackage)  = aPackage.aAllPackages->asOrderedSet() in
	if chain11->asOrderedSet()->oclIsUndefined() 
	then null 
	else chain11->asOrderedSet()
	endif->union(aFolder.aAllPackages->asOrderedSet()) ->asOrderedSet()   in 
	if e1->oclIsInvalid() then OrderedSet{} else e1 endif
	
	 * @templateTag INS03
	 * @generated
	 */
	@Override
	public EList<APackage> getAAllPackages() {
		EClass eClass = (AcorePackage.Literals.AABSTRACT_FOLDER);
		EStructuralFeature eOverrideFeature = AcorePackage.Literals.ASTRUCTURING_ELEMENT__AALL_PACKAGES;

		if (aAllPackagesDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eOverrideFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eOverrideFeature, eClass());

			try {
				aAllPackagesDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(), eClass,
						eOverrideFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aAllPackagesDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, eClass, eOverrideFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			return (EList<APackage>) xoclEval.evaluateElement(eOverrideFeature, query);

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}
} //AAbstractFolderImpl
