/**
 */

package com.montages.acore.impl;

import com.montages.acore.APackage;
import com.montages.acore.AResource;
import com.montages.acore.AcorePackage;

import com.montages.acore.values.AObject;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.ocl.ParserException;

import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;

import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;

import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;

import org.eclipse.ocl.options.EvaluationOptions;
import org.eclipse.ocl.options.ParsingOptions;

import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclEvaluator;

import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>AResource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link com.montages.acore.impl.AResourceImpl#getAObject <em>AObject</em>}</li>
 *   <li>{@link com.montages.acore.impl.AResourceImpl#getAActiveResource <em>AActive Resource</em>}</li>
 *   <li>{@link com.montages.acore.impl.AResourceImpl#getARootObjectPackage <em>ARoot Object Package</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */

public abstract class AResourceImpl extends AStructuringElementImpl implements AResource {
	/**
	 * The default value of the '{@link #getAActiveResource() <em>AActive Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveResource()
	 * @generated
	 * @ordered
	 */
	protected static final Boolean AACTIVE_RESOURCE_EDEFAULT = null;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAObject <em>AObject</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAObject
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aObjectDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getAActiveResource <em>AActive Resource</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAActiveResource
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aActiveResourceDeriveOCL;

	/**
	 * The parsed OCL expression for the derivation of '{@link #getARootObjectPackage <em>ARoot Object Package</em>}' property.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getARootObjectPackage
	 * @templateTag DFGFI02
	 * @generated
	 */
	private static OCLExpression aRootObjectPackageDeriveOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
		EvaluationOptions.setOption(OCL_ENV.getEvaluationEnvironment(), EvaluationOptions.DYNAMIC_DISPATCH, true);
	}

	/**
	 * The cache for OCL expressions.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI14
	 * @generated
	 */
	private Map<ETypedElement, Object> cachedValues = new HashMap<ETypedElement, Object>();

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	* <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName, EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return AcorePackage.Literals.ARESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<AObject> getAObject() {
		/**
		 * @OCL OrderedSet{}
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ARESOURCE;
		EStructuralFeature eFeature = AcorePackage.Literals.ARESOURCE__AOBJECT;

		if (aObjectDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aObjectDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ARESOURCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aObjectDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ARESOURCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			EList<AObject> result = (EList<AObject>) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean getAActiveResource() {
		/**
		 * @OCL true
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ARESOURCE;
		EStructuralFeature eFeature = AcorePackage.Literals.ARESOURCE__AACTIVE_RESOURCE;

		if (aActiveResourceDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aActiveResourceDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ARESOURCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aActiveResourceDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ARESOURCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			Boolean result = (Boolean) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage getARootObjectPackage() {
		APackage aRootObjectPackage = basicGetARootObjectPackage();
		return aRootObjectPackage != null && aRootObjectPackage.eIsProxy()
				? (APackage) eResolveProxy((InternalEObject) aRootObjectPackage) : aRootObjectPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackage basicGetARootObjectPackage() {
		/**
		 * @OCL let nl: acore::APackage = null in nl
		
		 * @templateTag GGFT01
		 */
		EClass eClass = AcorePackage.Literals.ARESOURCE;
		EStructuralFeature eFeature = AcorePackage.Literals.ARESOURCE__AROOT_OBJECT_PACKAGE;

		if (aRootObjectPackageDeriveOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setAttributeContext(eClass, eFeature);

			String derive = XoclEmfUtil.findDeriveAnnotationText(eFeature, eClass());

			try {
				aRootObjectPackageDeriveOCL = helper.createQuery(derive);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(AcorePackage.PLUGIN_ID, derive, helper.getProblems(),
						AcorePackage.Literals.ARESOURCE, eFeature);
			}
		}

		Query query = OCL_ENV.createQuery(aRootObjectPackageDeriveOCL);
		try {
			XoclErrorHandler.enterContext(AcorePackage.PLUGIN_ID, query, AcorePackage.Literals.ARESOURCE, eFeature);

			XoclEvaluator xoclEval = new XoclEvaluator(this, cachedValues);
			APackage result = (APackage) xoclEval.evaluateElement(eFeature, query);

			return result;

		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case AcorePackage.ARESOURCE__AOBJECT:
			return getAObject();
		case AcorePackage.ARESOURCE__AACTIVE_RESOURCE:
			return getAActiveResource();
		case AcorePackage.ARESOURCE__AROOT_OBJECT_PACKAGE:
			if (resolve)
				return getARootObjectPackage();
			return basicGetARootObjectPackage();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case AcorePackage.ARESOURCE__AOBJECT:
			return !getAObject().isEmpty();
		case AcorePackage.ARESOURCE__AACTIVE_RESOURCE:
			return AACTIVE_RESOURCE_EDEFAULT == null ? getAActiveResource() != null
					: !AACTIVE_RESOURCE_EDEFAULT.equals(getAActiveResource());
		case AcorePackage.ARESOURCE__AROOT_OBJECT_PACKAGE:
			return basicGetARootObjectPackage() != null;
		}
		return super.eIsSet(featureID);
	}

} //AResourceImpl
