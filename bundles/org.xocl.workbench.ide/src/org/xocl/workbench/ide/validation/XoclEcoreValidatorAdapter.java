package org.xocl.workbench.ide.validation;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.ETypedElement;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.emf.ecore.util.EcoreValidator;
import org.xocl.core.expr.OCLExpressionUtils;
import org.xocl.core.expr.XOCLElementContext;
import org.xocl.workbench.ide.builder.Messages;

public class XoclEcoreValidatorAdapter extends EObjectValidator {

	@Override
	public boolean validate(EClass eClass, EObject eObject, DiagnosticChain diagnostics, Map<Object, Object> context) {

		if (context != null)
			context.put(EcoreValidator.STRICT_NAMED_ELEMENT_NAMES, false);
		boolean result = EcoreValidator.INSTANCE.validate(eClass, eObject, diagnostics, context);
		result &= super.validate(eClass, eObject, diagnostics, context);

		if (eObject instanceof EAnnotation) {
			EAnnotation eAnnotation = (EAnnotation) eObject;
			EStructuralFeature modelFeat = null;
			if (eAnnotation.getEModelElement() instanceof EStructuralFeature)
				modelFeat = (EStructuralFeature) eAnnotation.getEModelElement();
			for (Iterator<Map.Entry<String, String>> j = eAnnotation.getDetails().entrySet().iterator(); j.hasNext();) {
				Map.Entry<String, String> mapEntry = j.next();

				if (modelFeat != null) {

					if (modelFeat.getName().contains("$InnerValue"))
						break;
					if ((modelFeat.isDerived() && modelFeat.isChangeable() ) || (modelFeat.isChangeable() && modelFeat.isTransient()))
						if (mapEntry.getKey().contains("update") || mapEntry.getKey().contains("Feature")
								|| mapEntry.getKey().contains("Value") || mapEntry.getKey().contains("Object")
								|| mapEntry.getKey().contains("FeaturePackage")
								|| mapEntry.getKey().contains("FeatureClass") || mapEntry.getKey().contains("Feature"))
							break;

				}
				try {
					XOCLElementContext elementContext = XOCLElementContext.getElementContext(eAnnotation,
							mapEntry.getKey());
					if (elementContext != null && (!(mapEntry.getKey().startsWith("update")
							|| mapEntry.getKey().startsWith("save") || mapEntry.getKey().startsWith("copy")))) {
						ResourceSet resourceSet = (eObject.eResource() == null) ? null
								: eObject.eResource().getResourceSet();
						if (resourceSet == null) {
							resourceSet = new ResourceSetImpl();
						}
						Diagnostic diagnostic = OCLExpressionUtils.validateExpression(mapEntry.getValue(),
								elementContext, resourceSet);

						if (diagnostic.getSeverity() != Diagnostic.OK) {
							EObject eContainer = eAnnotation.eContainer();
							String location = elementContext.getContextClassifier().getName();

							if (eContainer instanceof EStructuralFeature || eContainer instanceof EOperation) {
								location += "." + ((ETypedElement) eContainer).getName(); //$NON-NLS-1$
							}

							location = MessageFormat.format(Messages.EcoreModelValidator_AnnotationLocationDiagnostic,
									new Object[] { location, mapEntry.getKey() });
							URI source = eAnnotation.eResource().getURI()
									.appendFragment(eAnnotation.eResource().getURIFragment((EObject) mapEntry));

							List<?> data = diagnostic.getData();
							BasicDiagnostic modifiedDiagnostic = new BasicDiagnostic(diagnostic.getSeverity(),
									source.toString(), diagnostic.getCode(), location,
									(data != null) ? data.toArray() : null);
							for (Diagnostic childDiagnostic : diagnostic.getChildren()) {
								modifiedDiagnostic.add(childDiagnostic);
							}

							diagnostics.add(modifiedDiagnostic);
							result = false;
						}
					}
				} catch (DiagnosticException e) {
					Diagnostic diagnostic = e.getDiagnostic();
					diagnostics.add(diagnostic);
					result = false;
				}
			}
		}
		return result;
	}
}
