package org.xocl.workbench.ide.builder;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.workbench.ide.builder.messages"; //$NON-NLS-1$
	public static String EcoreModelValidator_AnnotationLocationDiagnostic;
	public static String EcoreModelValidator_LoadResourceFailed;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
