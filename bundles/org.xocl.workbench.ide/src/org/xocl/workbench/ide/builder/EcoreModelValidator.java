/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.workbench.ide.builder;

import java.util.Iterator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.xocl.workbench.ide.IDEPlugin;

import com.montages.common.resource.CustomResourceLocator;
import com.montages.common.resource.EcoreResourceResolver;

/**
 * @author Max Stepanov
 *
 */
public class EcoreModelValidator {
	
	public static final String OLD_MARKER_TYPE = "org.xocl.workbench.ide.ecoreProblem"; //$NON-NLS-1$
	public static final String MARKER_TYPE = EValidator.MARKER;
	
	public void validate(IFile file) throws CoreException {
		file.deleteMarkers(OLD_MARKER_TYPE, false, IResource.DEPTH_ZERO);
		file.deleteMarkers(MARKER_TYPE, false, IResource.DEPTH_ZERO);
		
		URI uri = URI.createPlatformResourceURI(file.getFullPath().toString(), true);
		ResourceSet resourceSet = new ResourceSetImpl();
		resourceSet.getURIConverter().getURIMap().putAll(EcoreResourceResolver.computeWorkspaceResourceMap());
		new CustomResourceLocator((ResourceSetImpl) resourceSet);

		Resource resource;
		try {
			resource = resourceSet.getResource(uri, true);
		} catch (RuntimeException e) {
			if (e instanceof Resource.Diagnostic) {
				Resource.Diagnostic diag = (Resource.Diagnostic) e;
				String message = diag.getMessage();
				if (message.indexOf(": ") > 0) { //$NON-NLS-1$
					message = message.substring(message.indexOf(": ")+2); //$NON-NLS-1$
				}
				addMarker(file, message, diag.getLine(), IMarker.SEVERITY_ERROR);
			} else {
				IDEPlugin.log(e);
			}
			return;
		}
		if (resource == null) {
			addMarker(file, Messages.EcoreModelValidator_LoadResourceFailed, 0, IMarker.SEVERITY_WARNING);
			return;
		}
		
		for (Iterator<EObject> i = resource.getContents().iterator(); i.hasNext(); ) {
			Diagnostic diagnostic = Diagnostician.INSTANCE.validate(i.next());
			if (diagnostic.getSeverity() != Diagnostic.OK) {
				if (diagnostic.getChildren().isEmpty()) {
					new MarkerHelper(file).createMarkers(diagnostic);
				} else {
					for (Diagnostic child : diagnostic.getChildren()) {
						new MarkerHelper(file).createMarkers(child);
					}
				}
			}
		}
	}
	

	private static void addMarker(IFile file, String message, int lineNumber, int severity) {
		try {
			IMarker marker = file.createMarker(MARKER_TYPE);
			marker.setAttribute(IMarker.MESSAGE, message);
			marker.setAttribute(IMarker.SEVERITY, severity);
			if (lineNumber == -1) {
				lineNumber = 1;
			}
			marker.setAttribute(IMarker.LINE_NUMBER, lineNumber);
		} catch (CoreException e) {
			IDEPlugin.log(e.getStatus());
		}
	}

	private class MarkerHelper extends org.eclipse.emf.common.ui.MarkerHelper {

		private final IFile resource;
		
		/**
		 * 
		 */
		public MarkerHelper(IFile resource) {
			super();
			this.resource = resource;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.emf.common.ui.MarkerHelper#getMarkerID()
		 */
		@Override
		protected String getMarkerID() {
			return MARKER_TYPE;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.emf.common.ui.MarkerHelper#getFile(org.eclipse.emf.common.util.Diagnostic)
		 */
		@Override
		protected IFile getFile(Diagnostic diagnostic) {
			IFile file = super.getFile(diagnostic);
			if (file != null) {
				return file;
			}
			return resource;
		}

		/* (non-Javadoc)
		 * @see org.eclipse.emf.common.ui.MarkerHelper#adjustMarker(org.eclipse.core.resources.IMarker, org.eclipse.emf.common.util.Diagnostic, org.eclipse.emf.common.util.Diagnostic)
		 */
		@Override
		protected void adjustMarker(IMarker marker, Diagnostic diagnostic, Diagnostic parentDiagnostic) throws CoreException {
			String source = diagnostic.getSource();
			String location = null;
			if (parentDiagnostic != null) {
				source = parentDiagnostic.getSource();
				location = parentDiagnostic.getMessage();
			}
			marker.setAttribute(EValidator.URI_ATTRIBUTE, source);
			marker.setAttribute(IMarker.LOCATION, location);
		}

	}
}