/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.workbench.ui.actions;

import java.text.MessageFormat;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.emf.common.util.DiagnosticException;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.edit.command.SetCommand;
import org.eclipse.emf.edit.domain.AdapterFactoryEditingDomain;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.window.Window;
import org.eclipse.osgi.util.NLS;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;
import org.xocl.common.ui.dialogs.OCLExpressionDialog;
import org.xocl.core.expr.XOCLElementContext;
import org.xocl.workbench.ide.IDEPlugin;
import org.xocl.workbench.ide.builder.EcoreModelValidator;

/**
 * @author Max Stepanov
 *
 */
public class OpenOCLEditorActionDelegate implements IObjectActionDelegate {
	
	private IWorkbenchPart targetPart;
	private EObject selectedObject;
	
	/* (non-Javadoc)
	 * @see org.eclipse.ui.IObjectActionDelegate#setActivePart(org.eclipse.jface.action.IAction, org.eclipse.ui.IWorkbenchPart)
	 */
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		this.targetPart = targetPart;
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IActionDelegate#run(org.eclipse.jface.action.IAction)
	 */
	public void run(IAction action) {
		if (selectedObject != null) {
			EAnnotation eAnnotation = (EAnnotation) selectedObject.eContainer();
			@SuppressWarnings("unchecked")
			Map.Entry<String, String> mapEntry = (Map.Entry<String, String>)selectedObject;
			String detailKey = mapEntry.getKey();
			XOCLElementContext elementContext = null;
			try {
				elementContext = XOCLElementContext.getElementContext(eAnnotation, detailKey);
			} catch (DiagnosticException e) {
				MessageDialog.openInformation(targetPart.getSite().getShell(), Messages.OpenOCLEditorActionDelegate_EditorTitle, NLS.bind(Messages.OpenOCLEditorActionDelegate_OpenFailedError, e.getDiagnostic().getMessage()));
				validateEcore();
				return;
			}
			if (elementContext == null) {
				MessageDialog.openInformation(targetPart.getSite().getShell(), Messages.OpenOCLEditorActionDelegate_EditorTitle, Messages.OpenOCLEditorActionDelegate_UnknownAnnotationSourceError);
				validateEcore();
				return;
			}
			
			OCLExpressionDialog dlg = new OCLExpressionDialog(targetPart.getSite().getShell(),
					Messages.OpenOCLEditorActionDelegate_EditorTitle,
					MessageFormat.format(Messages.OpenOCLEditorActionDelegate_DialogMessage, new Object[] { detailKey }),
					mapEntry.getValue(), elementContext.getContextClassifier(), elementContext.getVariables(), elementContext.getResultTypes());
			if (dlg.open() == Window.OK) {
				String value = dlg.getValue();
				if (!value.equals(mapEntry.getValue())) {
				EditingDomain editingDomain = getEditingDomain(selectedObject);
				editingDomain.getCommandStack().execute(
						SetCommand.create(editingDomain, selectedObject,
								EcorePackage.Literals.ESTRING_TO_STRING_MAP_ENTRY__VALUE, dlg.getValue()));
				}
			}
		}
	}

	private void validateEcore() {
		Resource eResource = selectedObject.eResource();
		if ((eResource != null) && (eResource.getURI().isPlatformResource())) {
			IPath resourcePath = new Path(eResource.getURI().toPlatformString(true));
			IFile ecoreFile = ResourcesPlugin.getWorkspace().getRoot().getFile(resourcePath);
			if ((ecoreFile != null) && (ecoreFile.exists())) {
				try {
					new EcoreModelValidator().validate(ecoreFile);
				} catch (CoreException e) {
					IDEPlugin.log(e);
				}
			}
		}
	}

	/* (non-Javadoc)
	 * @see org.eclipse.ui.IActionDelegate#selectionChanged(org.eclipse.jface.action.IAction, org.eclipse.jface.viewers.ISelection)
	 */
	public void selectionChanged(IAction action, ISelection selection) {
		selectedObject = null;
		if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
			Object source = ((IStructuredSelection)selection).getFirstElement();
			Object element = source;
			if (element instanceof IAdaptable) {
				element = ((IAdaptable)source).getAdapter(java.util.Map.Entry.class);
				if (element == null) {
					element = ((IAdaptable)source).getAdapter(EObject.class);
				}
				if (element instanceof EObject) {
					selectedObject = (EObject) element;
				}
			} else if (element instanceof Map.Entry) {
				if (element instanceof EObject) {
					selectedObject = (EObject) element;
				}				
			}
		}
	}

	private EditingDomain getEditingDomain(EObject eObject) {
		EditingDomain result = AdapterFactoryEditingDomain.getEditingDomainFor(eObject);
		if (result == null) {
			if (targetPart instanceof IEditingDomainProvider) {
				result = ((IEditingDomainProvider) targetPart).getEditingDomain();
			}
		}
		return result;
	}

}
