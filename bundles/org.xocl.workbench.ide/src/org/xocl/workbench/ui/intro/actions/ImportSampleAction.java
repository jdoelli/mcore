/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */
package org.xocl.workbench.ui.intro.actions;

import java.util.Properties;

import org.eclipse.jface.action.Action;
import org.eclipse.pde.core.plugin.IPluginModelBase;
import org.eclipse.pde.core.plugin.PluginRegistry;
import org.eclipse.pde.internal.ui.wizards.imports.PluginImportOperation;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.intro.IIntroSite;
import org.eclipse.ui.intro.config.IIntroAction;
import org.xocl.workbench.ide.IDEPlugin;

/**
 * @author Marc Moser
 *
 */
@SuppressWarnings("restriction")
public class ImportSampleAction extends Action implements IIntroAction {

	private static final String SAMPLE_EMPLOYEE_ID = "org.xocl.examples.employee";
	private static final String SAMPLE_CAPMOD_ID = "org.xocl.workbench.ide.capmod";
	
	private String sampleId;

	/**
	 *  
	 */
	public ImportSampleAction() {
	}

	public void run(IIntroSite site, Properties params) {
		sampleId = params.getProperty("id");
		if (sampleId == null) {
			return;
		}

		Runnable r = null;
		if (sampleId.equalsIgnoreCase(SAMPLE_EMPLOYEE_ID) && ensureProperContext(SAMPLE_EMPLOYEE_ID)) {
			r = createEmployeeRunnable();
		} else if (sampleId.equalsIgnoreCase(SAMPLE_CAPMOD_ID) && ensureProperContext(SAMPLE_CAPMOD_ID)) {
			r = createCapmodRunnable();
		} else {
			return;
		}
		
		Shell currentShell = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell();
		currentShell.getDisplay().asyncExec(r);
	}

	private Runnable createEmployeeRunnable() {
		return new Runnable() {
			public void run() {
				try {
					IPluginModelBase[] models = new IPluginModelBase[4];
					models[0] = PluginRegistry.findModel("org.xocl.examples.employee");
					models[1] = PluginRegistry.findModel("org.xocl.examples.employee.edit");
					models[2] = PluginRegistry.findModel("org.xocl.examples.employee.editor");
					models[3] = PluginRegistry.findModel("org.xocl.examples.employee.tests");
					final PluginImportOperation op = new PluginImportOperation(models, PluginImportOperation.IMPORT_BINARY_WITH_LINKS, true);
					op.run(null);
				} catch (Exception e) {
					IDEPlugin.log(e);
				}
			}
		};
	}

	private Runnable createCapmodRunnable() {
		return new Runnable() {
			public void run() {
				try {
					// TODO
				} catch (Exception e) {
					IDEPlugin.log(e);
				}
			}
		};
	}

	private boolean ensureProperContext(String id) {
		// TODO check if projects not yet present in workspace
		//			if (MessageDialog.openQuestion(PDEPlugin.getActiveWorkbenchShell(), PDEUIMessages.ShowSampleAction_msgTitle, PDEUIMessages.ShowSampleAction_msgDesc)) {
		//				return downloadFeature();
		//			}
		return true;
	}
}
