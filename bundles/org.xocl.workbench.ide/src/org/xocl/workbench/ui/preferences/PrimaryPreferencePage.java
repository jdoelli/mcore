package org.xocl.workbench.ui.preferences;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.xocl.core.XOCLPlugin;
import org.xocl.core.util.XoclConstants;

public class PrimaryPreferencePage extends PreferencePage implements
		IWorkbenchPreferencePage {

	private Button caching;

	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout gridLayout = new GridLayout();
		composite.setLayout(gridLayout);
		
		caching = new Button(composite, SWT.CHECK);
		caching.setText("Activate caching?");
		caching.setSelection(XOCLPlugin.getDefault().getPrefs().getBoolean(XoclConstants.CACHING, false));

		return composite;
	}

	@Override
	protected void performDefaults() {
		if (caching != null) {
			boolean checkBoxValue = XOCLPlugin.getDefault().getPrefs().getBoolean(XoclConstants.CACHING, false);
			caching.setSelection(checkBoxValue);
			super.performDefaults();
		}
	}

	@Override
	public boolean performOk() {
		if (caching != null) {
			boolean checkBoxValue = caching.getSelection();
			XOCLPlugin.getDefault().getPrefs().putBoolean(XoclConstants.CACHING, checkBoxValue);
//			XOCLPlugin.getDefault().savePluginPreferences();
		}
		return true;
	}

	public void init(IWorkbench workbench) {
	}
}
