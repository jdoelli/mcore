/**
 */
package com.montages.acore.classifiers.provider;

import com.montages.acore.abstractions.provider.ANamedItemProvider;

import com.montages.acore.classifiers.AClassifier;
import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.provider.AcoreEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.classifiers.AClassifier} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class AClassifierItemProvider extends ANamedItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AClassifierItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addASpecializedClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveEnumerationPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAsDataTypePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAAsClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAIsStringClassifierPropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ASpecialized Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASpecializedClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASpecialized Classifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AClassifier_aSpecializedClassifier_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aSpecializedClassifier_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__ASPECIALIZED_CLASSIFIER, false, false, false, null,
				getString("_UI_zACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Data Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aActiveDataType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aActiveDataType_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_DATA_TYPE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Enumeration feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveEnumerationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Enumeration feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aActiveEnumeration_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aActiveEnumeration_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_ENUMERATION, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_AClassifier_aActiveClass_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aActiveClass_feature",
						"_UI_AClassifier_type"),
				ClassifiersPackage.Literals.ACLASSIFIER__AACTIVE_CLASS, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aContainingPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aContainingPackage_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__ACONTAINING_PACKAGE, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAs Data Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAsDataTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAs Data Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aAsDataType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aAsDataType_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__AAS_DATA_TYPE, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AAs Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAsClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAs Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aAsClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aAsClass_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__AAS_CLASS, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AIs String Classifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAIsStringClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AIs String Classifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AClassifier_aIsStringClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AClassifier_aIsStringClassifier_feature",
								"_UI_AClassifier_type"),
						ClassifiersPackage.Literals.ACLASSIFIER__AIS_STRING_CLASSIFIER, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePackagePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AClassifier) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AClassifier.class)) {
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_DATA_TYPE:
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_ENUMERATION:
		case ClassifiersPackage.ACLASSIFIER__AACTIVE_CLASS:
		case ClassifiersPackage.ACLASSIFIER__AIS_STRING_CLASSIFIER:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ClassifiersItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
