/**
 */
package com.montages.acore.classifiers.provider;

import com.montages.acore.abstractions.AbstractionsPackage;

import com.montages.acore.abstractions.provider.ATypedItemProvider;

import com.montages.acore.classifiers.AProperty;
import com.montages.acore.classifiers.ClassifiersPackage;

import com.montages.acore.provider.AcoreEditPlugin;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.classifiers.AProperty} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class APropertyItemProvider extends ATypedItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APropertyItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addANamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedNameConstantPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addABusinessNamePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingClassPropertyDescriptor(object);
			}
			addAOperationPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AName feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addANamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AName feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aName_feature", "_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ANAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Name Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedNameConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Name Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ANamed_aUndefinedNameConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aUndefinedNameConstant_feature",
						"_UI_ANamed_type"),
				AbstractionsPackage.Literals.ANAMED__AUNDEFINED_NAME_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ABusiness Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addABusinessNamePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ABusiness Name feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ANamed_aBusinessName_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ANamed_aBusinessName_feature",
								"_UI_ANamed_type"),
						AbstractionsPackage.Literals.ANAMED__ABUSINESS_NAME, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreAbstractionsPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AProperty_aContainingClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AProperty_aContainingClass_feature",
								"_UI_AProperty_type"),
						ClassifiersPackage.Literals.APROPERTY__ACONTAINING_CLASS, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AOperation feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAOperationPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AOperation feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_AProperty_aOperation_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_AProperty_aOperation_feature",
								"_UI_AProperty_type"),
						ClassifiersPackage.Literals.APROPERTY__AOPERATION, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((AProperty) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(AProperty.class)) {
		case ClassifiersPackage.APROPERTY__ANAME:
		case ClassifiersPackage.APROPERTY__AUNDEFINED_NAME_CONSTANT:
		case ClassifiersPackage.APROPERTY__ABUSINESS_NAME:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !ClassifiersItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
