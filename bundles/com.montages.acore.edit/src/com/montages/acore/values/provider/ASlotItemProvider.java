/**
 */
package com.montages.acore.values.provider;

import com.montages.acore.abstractions.provider.AElementItemProvider;

import com.montages.acore.provider.AcoreEditPlugin;

import com.montages.acore.values.ASlot;
import com.montages.acore.values.ValuesPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.values.ASlot} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ASlotItemProvider extends AElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ASlotItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addAKeyPropertyDescriptor(object);
			addAInstantiatedFeaturePropertyDescriptor(object);
			addAValuePropertyDescriptor(object);
			addAIsContainmentPropertyDescriptor(object);
			addAContainingObjectPropertyDescriptor(object);
			addAFeatureIsClassTypedPropertyDescriptor(object);
			addAFeatureIsDataTypedPropertyDescriptor(object);
			addADirectlyContainedObjectPropertyDescriptor(object);
			addAAllContainedObjectPropertyDescriptor(object);
			addAFeatureOfWrongClassPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AKey feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAKeyPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AKey feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ASlot_aKey_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aKey_feature", "_UI_ASlot_type"),
				ValuesPackage.Literals.ASLOT__AKEY, false, false, false, ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				getString("_UI_zACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AInstantiated Feature feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAInstantiatedFeaturePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AInstantiated Feature feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aInstantiatedFeature_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aInstantiatedFeature_feature",
								"_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__AINSTANTIATED_FEATURE, false, false, false, null,
						getString("_UI_zACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AValue feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAValuePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AValue feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aValue_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aValue_feature", "_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__AVALUE, false, false, false, null,
						getString("_UI_zACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AIs Containment feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAIsContainmentPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AIs Containment feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aIsContainment_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aIsContainment_feature",
								"_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__AIS_CONTAINMENT, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreValuesPropertyCategory"),
						null));
	}

	/**
	 * This adds a property descriptor for the AContaining Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aContainingObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aContainingObject_feature",
								"_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__ACONTAINING_OBJECT, false, false, false, null,
						getString("_UI_zACoreValuesPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AFeature Is Class Typed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFeatureIsClassTypedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFeature Is Class Typed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ASlot_aFeatureIsClassTyped_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aFeatureIsClassTyped_feature",
						"_UI_ASlot_type"),
				ValuesPackage.Literals.ASLOT__AFEATURE_IS_CLASS_TYPED, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AFeature Is Data Typed feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFeatureIsDataTypedPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFeature Is Data Typed feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ASlot_aFeatureIsDataTyped_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aFeatureIsDataTyped_feature",
						"_UI_ASlot_type"),
				ValuesPackage.Literals.ASLOT__AFEATURE_IS_DATA_TYPED, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the ADirectly Contained Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addADirectlyContainedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ADirectly Contained Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aDirectlyContainedObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aDirectlyContainedObject_feature",
								"_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__ADIRECTLY_CONTAINED_OBJECT, false, false, false, null,
						getString("_UI_zACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AAll Contained Object feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAAllContainedObjectPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AAll Contained Object feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ASlot_aAllContainedObject_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aAllContainedObject_feature",
								"_UI_ASlot_type"),
						ValuesPackage.Literals.ASLOT__AALL_CONTAINED_OBJECT, false, false, false, null,
						getString("_UI_zACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This adds a property descriptor for the AFeature Of Wrong Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAFeatureOfWrongClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AFeature Of Wrong Class feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ASlot_aFeatureOfWrongClass_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ASlot_aFeatureOfWrongClass_feature",
						"_UI_ASlot_type"),
				ValuesPackage.Literals.ASLOT__AFEATURE_OF_WRONG_CLASS, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreHelpersPropertyCategory"), null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((ASlot) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ASlot.class)) {
		case ValuesPackage.ASLOT__AKEY:
		case ValuesPackage.ASLOT__AIS_CONTAINMENT:
		case ValuesPackage.ASLOT__AFEATURE_IS_CLASS_TYPED:
		case ValuesPackage.ASLOT__AFEATURE_IS_DATA_TYPED:
		case ValuesPackage.ASLOT__AFEATURE_OF_WRONG_CLASS:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * Return the resource locator for this item provider's resources.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		return AcoreEditPlugin.INSTANCE;
	}

}
