/**
 */
package com.montages.acore.abstractions.provider;

import com.montages.acore.abstractions.ATyped;
import com.montages.acore.abstractions.AbstractionsPackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.abstractions.ATyped} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class ATypedItemProvider extends AElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ATypedItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addAClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAMandatoryPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASingularPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addATypeLabelPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAUndefinedTypeConstantPropertyDescriptor(object);
			}
			addASimpleTypePropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the AClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AClassifier feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aClassifier_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aClassifier_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ACLASSIFIER, false, false, false, null,
						getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AMandatory feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAMandatoryPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AMandatory feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aMandatory_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aMandatory_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__AMANDATORY, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASingular feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASingularPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASingular feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aSingular_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aSingular_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ASINGULAR, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AType Label feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addATypeLabelPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AType Label feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aTypeLabel_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aTypeLabel_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ATYPE_LABEL, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AUndefined Type Constant feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAUndefinedTypeConstantPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AUndefined Type Constant feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_ATyped_aUndefinedTypeConstant_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aUndefinedTypeConstant_feature",
						"_UI_ATyped_type"),
				AbstractionsPackage.Literals.ATYPED__AUNDEFINED_TYPE_CONSTANT, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASimple Type feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASimpleTypePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASimple Type feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_ATyped_aSimpleType_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_ATyped_aSimpleType_feature",
								"_UI_ATyped_type"),
						AbstractionsPackage.Literals.ATYPED__ASIMPLE_TYPE, true, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACoreClassifiersPropertyCategory"),
						null));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((ATyped) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(ATyped.class)) {
		case AbstractionsPackage.ATYPED__AMANDATORY:
		case AbstractionsPackage.ATYPED__ASINGULAR:
		case AbstractionsPackage.ATYPED__ATYPE_LABEL:
		case AbstractionsPackage.ATYPED__AUNDEFINED_TYPE_CONSTANT:
		case AbstractionsPackage.ATYPED__ASIMPLE_TYPE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AbstractionsItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
