/**
 */
package com.montages.acore.provider;

import com.montages.acore.APackage;
import com.montages.acore.AcorePackage;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IEditingDomainItemProvider;
import org.eclipse.emf.edit.provider.IItemLabelProvider;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.emf.edit.provider.IStructuredItemContentProvider;
import org.eclipse.emf.edit.provider.ITreeItemContentProvider;
import org.eclipse.emf.edit.provider.ViewerNotification;

import org.xocl.core.edit.provider.ItemPropertyDescriptor;

/**
 * This is the item provider adapter for a {@link com.montages.acore.APackage} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class APackageItemProvider extends AStructuringElementItemProvider implements IEditingDomainItemProvider,
		IStructuredItemContentProvider, ITreeItemContentProvider, IItemLabelProvider, IItemPropertySource {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public APackageItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);
			if (shouldShowAdvancedProperties()) {
				addARootObjectClassPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAClassifierPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASubPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAContainingPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addASpecialUriPropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActivePackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveRootPackagePropertyDescriptor(object);
			}
			if (shouldShowAdvancedProperties()) {
				addAActiveSubPackagePropertyDescriptor(object);
			}
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the ARoot Object Class feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addARootObjectClassPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ARoot Object Class feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_APackage_aRootObjectClass_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_APackage_aRootObjectClass_feature",
								"_UI_APackage_type"),
						AcorePackage.Literals.APACKAGE__AROOT_OBJECT_CLASS, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AClassifier feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAClassifierPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AClassifier feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_APackage_aClassifier_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_APackage_aClassifier_feature",
						"_UI_APackage_type"),
				AcorePackage.Literals.APACKAGE__ACLASSIFIER, false, false, false, null,
				getString("_UI_zACorePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASub Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASubPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASub Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_APackage_aSubPackage_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_APackage_aSubPackage_feature",
						"_UI_APackage_type"),
				AcorePackage.Literals.APACKAGE__ASUB_PACKAGE, false, false, false, null,
				getString("_UI_zACorePropertyCategory"), new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AContaining Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAContainingPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AContaining Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_APackage_aContainingPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_APackage_aContainingPackage_feature",
								"_UI_APackage_type"),
						AcorePackage.Literals.APACKAGE__ACONTAINING_PACKAGE, false, false, false, null,
						getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the ASpecial Uri feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addASpecialUriPropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the ASpecial Uri feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_APackage_aSpecialUri_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_APackage_aSpecialUri_feature",
								"_UI_APackage_type"),
						AcorePackage.Literals.APACKAGE__ASPECIAL_URI, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActivePackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_APackage_aActivePackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_APackage_aActivePackage_feature",
								"_UI_APackage_type"),
						AcorePackage.Literals.APACKAGE__AACTIVE_PACKAGE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Root Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveRootPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Root Package feature.
		 */
		itemPropertyDescriptors.add(createItemPropertyDescriptor(
				((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(), getResourceLocator(),
				getString("_UI_APackage_aActiveRootPackage_feature"),
				getString("_UI_PropertyDescriptor_description", "_UI_APackage_aActiveRootPackage_feature",
						"_UI_APackage_type"),
				AcorePackage.Literals.APACKAGE__AACTIVE_ROOT_PACKAGE, false, false, false,
				ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
				new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This adds a property descriptor for the AActive Sub Package feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addAActiveSubPackagePropertyDescriptor(Object object) {
		/*
		 * This adds a property descriptor for the AActive Sub Package feature.
		 */
		itemPropertyDescriptors
				.add(createItemPropertyDescriptor(((ComposeableAdapterFactory) adapterFactory).getRootAdapterFactory(),
						getResourceLocator(), getString("_UI_APackage_aActiveSubPackage_feature"),
						getString("_UI_PropertyDescriptor_description", "_UI_APackage_aActiveSubPackage_feature",
								"_UI_APackage_type"),
						AcorePackage.Literals.APACKAGE__AACTIVE_SUB_PACKAGE, false, false, false,
						ItemPropertyDescriptor.GENERIC_VALUE_IMAGE, getString("_UI_zACorePropertyCategory"),
						new String[] { "org.eclipse.ui.views.properties.expert" }));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		//Montages Change to show containingFeatureName
		EStructuralFeature containingFeature = ((EObject) object).eContainingFeature();
		String containingFeatureName = (containingFeature == null ? "" : containingFeature.getName());

		String label = ((APackage) object).getATClassifierName();
		//Montages change from Organizational Unit Marketing to <organizational unit> Marketing
		return label == null || label.length() == 0 ? "<" + containingFeatureName + ">"
				: "<" + containingFeatureName + ">" + " " + label;
	}

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(APackage.class)) {
		case AcorePackage.APACKAGE__ASPECIAL_URI:
		case AcorePackage.APACKAGE__AACTIVE_PACKAGE:
		case AcorePackage.APACKAGE__AACTIVE_ROOT_PACKAGE:
		case AcorePackage.APACKAGE__AACTIVE_SUB_PACKAGE:
			fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
			return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean shouldShowAdvancedProperties() {
		return !AcoreItemProviderAdapterFactory.HIDE_ADVANCED_PROPERTIES;
	}
}
