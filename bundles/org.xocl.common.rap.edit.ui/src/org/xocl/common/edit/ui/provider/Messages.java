package org.xocl.common.edit.ui.provider;

import org.eclipse.osgi.util.NLS;

public class Messages extends NLS {
	private static final String BUNDLE_NAME = "org.xocl.common.edit.ui.provider.messages"; //$NON-NLS-1$
	public static String PropertyDescriptor_DialogMessage;
	static {
		// initialize resource bundle
		NLS.initializeMessages(BUNDLE_NAME, Messages.class);
	}

	private Messages() {
	}
}
