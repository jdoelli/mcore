/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.common.edit.ui.provider;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.IItemPropertySource;
import org.eclipse.ui.views.properties.IPropertyDescriptor;

/**
 * @author Max Stepanov
 *
 */
public class PropertySource extends org.eclipse.emf.edit.ui.provider.PropertySource {

	/**
	 * @param object
	 * @param itemPropertySource
	 */
	public PropertySource(Object object, IItemPropertySource itemPropertySource) {
		super(object, itemPropertySource);
	}

	/* (non-Javadoc)
	 * @see org.eclipse.emf.edit.ui.provider.PropertySource#createPropertyDescriptor(org.eclipse.emf.edit.provider.IItemPropertyDescriptor)
	 */
	@Override
	protected IPropertyDescriptor createPropertyDescriptor(IItemPropertyDescriptor itemPropertyDescriptor) {
		return new PropertyDescriptor(object, itemPropertyDescriptor);
	}


}
