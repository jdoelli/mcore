/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.xocl.editorconfig;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Edit Provider Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getEditProviderCell()
 * @model
 * @generated
 */
public interface EditProviderCell extends CellConfig {
} // EditProviderCell
