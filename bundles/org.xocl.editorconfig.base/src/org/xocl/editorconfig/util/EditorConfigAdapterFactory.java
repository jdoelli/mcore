/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigAdapterFactory.java,v 1.6 2010/01/03 17:44:38 xocl.kutterxocl Exp $
 */
package org.xocl.editorconfig.util;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;
import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.OclCell;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.EditorConfigPackage
 * @generated
 */
public class EditorConfigAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static EditorConfigPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = EditorConfigPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject) object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditorConfigSwitch<Adapter> modelSwitch = new EditorConfigSwitch<Adapter>() {
		@Override
		public Adapter caseEditorConfig(EditorConfig object) {
			return createEditorConfigAdapter();
		}

		@Override
		public Adapter caseEClassToTableConfigMapEntry(
				Map.Entry<EClass, TableConfig> object) {
			return createEClassToTableConfigMapEntryAdapter();
		}

		@Override
		public Adapter caseEReferenceToTableConfigMapEntry(
				Map.Entry<EReference, TableConfig> object) {
			return createEReferenceToTableConfigMapEntryAdapter();
		}

		@Override
		public Adapter caseTableConfig(TableConfig object) {
			return createTableConfigAdapter();
		}

		@Override
		public Adapter caseColumnConfig(ColumnConfig object) {
			return createColumnConfigAdapter();
		}

		@Override
		public Adapter caseEClassToCellConfigMapEntry(
				Map.Entry<EClass, CellConfig> object) {
			return createEClassToCellConfigMapEntryAdapter();
		}

		@Override
		public Adapter caseCellConfig(CellConfig object) {
			return createCellConfigAdapter();
		}

		@Override
		public Adapter caseRowFeatureCell(RowFeatureCell object) {
			return createRowFeatureCellAdapter();
		}

		@Override
		public Adapter caseOclCell(OclCell object) {
			return createOclCellAdapter();
		}

		@Override
		public Adapter caseEditProviderCell(EditProviderCell object) {
			return createEditProviderCellAdapter();
		}

		@Override
		public Adapter defaultCase(EObject object) {
			return createEObjectAdapter();
		}
	};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject) target);
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.EditorConfig <em>Editor Config</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.EditorConfig
	 * @generated
	 */
	public Adapter createEditorConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EClass To Table Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEClassToTableConfigMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EReference To Table Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEReferenceToTableConfigMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.TableConfig <em>Table Config</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.TableConfig
	 * @generated
	 */
	public Adapter createTableConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.ColumnConfig <em>Column Config</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.ColumnConfig
	 * @generated
	 */
	public Adapter createColumnConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map.Entry <em>EClass To Cell Config Map Entry</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map.Entry
	 * @generated
	 */
	public Adapter createEClassToCellConfigMapEntryAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.CellConfig <em>Cell Config</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.CellConfig
	 * @generated
	 */
	public Adapter createCellConfigAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.RowFeatureCell <em>Row Feature Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.RowFeatureCell
	 * @generated
	 */
	public Adapter createRowFeatureCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.OclCell <em>Ocl Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.OclCell
	 * @generated
	 */
	public Adapter createOclCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link org.xocl.editorconfig.EditProviderCell <em>Edit Provider Cell</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see org.xocl.editorconfig.EditProviderCell
	 * @generated
	 */
	public Adapter createEditProviderCellAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //EditorConfigAdapterFactory
