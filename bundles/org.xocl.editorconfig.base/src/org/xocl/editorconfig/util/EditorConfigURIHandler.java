package org.xocl.editorconfig.util;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.xmi.impl.URIHandlerImpl;

import com.montages.common.resource.EcoreResourceResolver;

public class EditorConfigURIHandler extends URIHandlerImpl {

	private final ResourceSet resourceSet;
	private final Map<URI, URI> ecores;

	public EditorConfigURIHandler(ResourceSet resourceSet) {
		assert resourceSet != null;
		this.resourceSet = resourceSet;
		this.ecores = EcoreResourceResolver.computeWorkspaceResourceMap();
	}

	@Override
	public URI deresolve(URI uri) {
		final URI normalized = resourceSet.getURIConverter().normalize(uri); 

		for (URI key: ecores.keySet()) {
			URI value = ecores.get(key);

			if (value.equals(uri.trimFragment()) || value.equals(normalized.trimFragment())) {
				return key.appendFragment(uri.fragment());
			}
		}

		return super.deresolve(uri);
	}

	@Override
	public URI resolve(URI uri) {
		final URI normalized = resourceSet.getURIConverter().normalize(uri);

		if (normalized.isPlatform() && normalized.fileExtension().equals("ecore")) {
			return normalized;
		}

		return super.resolve(uri);
	}

}
