/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigResourceImpl.java,v 1.7 2013/04/15 15:22:36 mtg.hillairet Exp $
 */
package org.xocl.editorconfig.util;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.xmi.impl.XMLResourceImpl;

/**
 * <!-- begin-user-doc -->
 * The <b>Resource </b> associated with the package.
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.util.EditorConfigResourceFactoryImpl
 * @generated
 */
public class EditorConfigResourceImpl extends XMLResourceImpl {
	/**
	 * Creates an instance of the resource.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param uri the URI of the new resource.
	 * @generated
	 */
	public EditorConfigResourceImpl(URI uri) {
		super(uri);
	}

} //EditorConfigResourceImpl
