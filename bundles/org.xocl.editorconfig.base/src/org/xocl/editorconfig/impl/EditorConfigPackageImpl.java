/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigPackageImpl.java,v 1.30 2011/07/20 17:04:54 mtg.kutter Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.EPackageImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.CellEditBehaviorOption;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.OclCell;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorConfigPackageImpl extends EPackageImpl implements
		EditorConfigPackage {

	/**
	 * The cached OCL constraint restricting the classes of root elements.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * templateTag PC01
	 */
	private static OCLExpression rootConstraintOCL;

	/**
	 * The shared instance of the OCL facade.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC02
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Convenience method to safely add a variable in the OCL Environment.
	 * 
	 * @param variableName
	 *            The name of the variable.
	 * @param variableType
	 *            The type of the variable.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC03
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * Utility method checking if an {@link EClass} can be choosen as a model root.
	 * 
	 * @param trg
	 *            The {@link EClass} to check.
	 * @return <code>true</code> if trg can be choosen as a model root.
	 *
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @templateTag PC04
	 */
	public boolean evalRootConstraint(EClass trg) {
		if (rootConstraintOCL == null) {
			EAnnotation ocl = this.getEAnnotation("http://www.xocl.org/OCL");
			String choiceConstraint = (String) ocl.getDetails().get(
					"rootConstraint");

			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(EcorePackage.Literals.EPACKAGE);

			addEnvironmentVariable("trg", EcorePackage.Literals.ECLASS);

			try {
				rootConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(PLUGIN_ID,
						choiceConstraint, helper.getProblems(),
						EditorConfigPackage.eINSTANCE, "rootConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(rootConstraintOCL);
		try {
			XoclErrorHandler.enterContext(PLUGIN_ID, query);
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass editorConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eClassToTableConfigMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eReferenceToTableConfigMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass tableConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass columnConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eClassToCellConfigMapEntryEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cellConfigEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rowFeatureCellEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass oclCellEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass editProviderCellEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cellEditBehaviorOptionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum cellLocateBehaviorOptionEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EDataType fontEDataType = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see org.xocl.editorconfig.EditorConfigPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private EditorConfigPackageImpl() {
		super(eNS_URI, EditorConfigFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this
	 * model, and for any others upon which it depends.  Simple
	 * dependencies are satisfied by calling this method on all
	 * dependent packages before doing anything else.  This method drives
	 * initialization for interdependent packages directly, in parallel
	 * with this package, itself.
	 * <p>Of this package and its interdependencies, all packages which
	 * have not yet been registered by their URI values are first created
	 * and registered.  The packages are then initialized in two steps:
	 * meta-model objects for all of the packages are created before any
	 * are initialized, since one package's meta-model objects may refer to
	 * those of another.
	 * <p>Invocation of this method will not affect any packages that have
	 * already been initialized.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static EditorConfigPackage init() {
		if (isInited)
			return (EditorConfigPackage) EPackage.Registry.INSTANCE
					.getEPackage(EditorConfigPackage.eNS_URI);

		// Obtain or create and register package
		EditorConfigPackageImpl theEditorConfigPackage = (EditorConfigPackageImpl) (EPackage.Registry.INSTANCE
				.getEPackage(eNS_URI) instanceof EditorConfigPackageImpl ? EPackage.Registry.INSTANCE
				.getEPackage(eNS_URI)
				: new EditorConfigPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theEditorConfigPackage.createPackageContents();

		// Initialize created meta-data
		theEditorConfigPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theEditorConfigPackage.freeze();

		return theEditorConfigPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEditorConfig() {
		return editorConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEditorConfig_TableConfig() {
		return (EReference) editorConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEditorConfig_HeaderTableConfig() {
		return (EReference) editorConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEditorConfig_ClassToTableConfigMap() {
		return (EReference) editorConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEditorConfig_SuperConfig() {
		return (EReference) editorConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEClassToTableConfigMapEntry() {
		return eClassToTableConfigMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToTableConfigMapEntry_Key() {
		return (EReference) eClassToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToTableConfigMapEntry_Value() {
		return (EReference) eClassToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToTableConfigMapEntry_IntendedPackage() {
		return (EReference) eClassToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEReferenceToTableConfigMapEntry() {
		return eReferenceToTableConfigMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEReferenceToTableConfigMapEntry_Key() {
		return (EReference) eReferenceToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEReferenceToTableConfigMapEntry_Value() {
		return (EReference) eReferenceToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEReferenceToTableConfigMapEntry_Label() {
		return (EAttribute) eReferenceToTableConfigMapEntryEClass
				.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTableConfig() {
		return tableConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableConfig_Name() {
		return (EAttribute) tableConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableConfig_Label() {
		return (EAttribute) tableConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableConfig_DisplayDefaultColumn() {
		return (EAttribute) tableConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableConfig_DisplayHeader() {
		return (EAttribute) tableConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTableConfig_Font() {
		return (EAttribute) tableConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableConfig_Column() {
		return (EReference) tableConfigEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableConfig_ReferenceToTableConfigMap() {
		return (EReference) tableConfigEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableConfig_IntendedPackage() {
		return (EReference) tableConfigEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTableConfig_IntendedClass() {
		return (EReference) tableConfigEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getColumnConfig() {
		return columnConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColumnConfig_Name() {
		return (EAttribute) columnConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColumnConfig_Label() {
		return (EAttribute) columnConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColumnConfig_Width() {
		return (EAttribute) columnConfigEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getColumnConfig_Font() {
		return (EAttribute) columnConfigEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getColumnConfig_CellConfigMap() {
		return (EReference) columnConfigEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEClassToCellConfigMapEntry() {
		return eClassToCellConfigMapEntryEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToCellConfigMapEntry_Key() {
		return (EReference) eClassToCellConfigMapEntryEClass
				.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEClassToCellConfigMapEntry_Value() {
		return (EReference) eClassToCellConfigMapEntryEClass
				.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCellConfig() {
		return cellConfigEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCellConfig_Font() {
		return (EAttribute) cellConfigEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCellConfig_Color() {
		return (EAttribute) cellConfigEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRowFeatureCell() {
		return rowFeatureCellEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRowFeatureCell_Feature() {
		return (EReference) rowFeatureCellEClass.getEStructuralFeatures()
				.get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRowFeatureCell_CellEditBehavior() {
		return (EAttribute) rowFeatureCellEClass.getEStructuralFeatures()
				.get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRowFeatureCell_CellLocateBehavior() {
		return (EAttribute) rowFeatureCellEClass.getEStructuralFeatures()
				.get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getOclCell() {
		return oclCellEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOclCell_Expression() {
		return (EAttribute) oclCellEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getOclCell_CellLocateBehavior() {
		return (EAttribute) oclCellEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEditProviderCell() {
		return editProviderCellEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCellEditBehaviorOption() {
		return cellEditBehaviorOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getCellLocateBehaviorOption() {
		return cellLocateBehaviorOptionEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EDataType getFont() {
		return fontEDataType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigFactory getEditorConfigFactory() {
		return (EditorConfigFactory) getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated)
			return;
		isCreated = true;

		// Create classes and their features
		editorConfigEClass = createEClass(EDITOR_CONFIG);
		createEReference(editorConfigEClass, EDITOR_CONFIG__TABLE_CONFIG);
		createEReference(editorConfigEClass, EDITOR_CONFIG__HEADER_TABLE_CONFIG);
		createEReference(editorConfigEClass,
				EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP);
		createEReference(editorConfigEClass, EDITOR_CONFIG__SUPER_CONFIG);

		eClassToTableConfigMapEntryEClass = createEClass(ECLASS_TO_TABLE_CONFIG_MAP_ENTRY);
		createEReference(eClassToTableConfigMapEntryEClass,
				ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__KEY);
		createEReference(eClassToTableConfigMapEntryEClass,
				ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__VALUE);
		createEReference(eClassToTableConfigMapEntryEClass,
				ECLASS_TO_TABLE_CONFIG_MAP_ENTRY__INTENDED_PACKAGE);

		eReferenceToTableConfigMapEntryEClass = createEClass(EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY);
		createEReference(eReferenceToTableConfigMapEntryEClass,
				EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY);
		createEReference(eReferenceToTableConfigMapEntryEClass,
				EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE);
		createEAttribute(eReferenceToTableConfigMapEntryEClass,
				EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL);

		tableConfigEClass = createEClass(TABLE_CONFIG);
		createEAttribute(tableConfigEClass, TABLE_CONFIG__NAME);
		createEAttribute(tableConfigEClass, TABLE_CONFIG__LABEL);
		createEAttribute(tableConfigEClass,
				TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN);
		createEAttribute(tableConfigEClass, TABLE_CONFIG__DISPLAY_HEADER);
		createEAttribute(tableConfigEClass, TABLE_CONFIG__FONT);
		createEReference(tableConfigEClass, TABLE_CONFIG__COLUMN);
		createEReference(tableConfigEClass,
				TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP);
		createEReference(tableConfigEClass, TABLE_CONFIG__INTENDED_PACKAGE);
		createEReference(tableConfigEClass, TABLE_CONFIG__INTENDED_CLASS);

		columnConfigEClass = createEClass(COLUMN_CONFIG);
		createEAttribute(columnConfigEClass, COLUMN_CONFIG__NAME);
		createEAttribute(columnConfigEClass, COLUMN_CONFIG__LABEL);
		createEAttribute(columnConfigEClass, COLUMN_CONFIG__WIDTH);
		createEAttribute(columnConfigEClass, COLUMN_CONFIG__FONT);
		createEReference(columnConfigEClass, COLUMN_CONFIG__CELL_CONFIG_MAP);

		eClassToCellConfigMapEntryEClass = createEClass(ECLASS_TO_CELL_CONFIG_MAP_ENTRY);
		createEReference(eClassToCellConfigMapEntryEClass,
				ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY);
		createEReference(eClassToCellConfigMapEntryEClass,
				ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE);

		cellConfigEClass = createEClass(CELL_CONFIG);
		createEAttribute(cellConfigEClass, CELL_CONFIG__FONT);
		createEAttribute(cellConfigEClass, CELL_CONFIG__COLOR);

		rowFeatureCellEClass = createEClass(ROW_FEATURE_CELL);
		createEReference(rowFeatureCellEClass, ROW_FEATURE_CELL__FEATURE);
		createEAttribute(rowFeatureCellEClass,
				ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR);
		createEAttribute(rowFeatureCellEClass,
				ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR);

		oclCellEClass = createEClass(OCL_CELL);
		createEAttribute(oclCellEClass, OCL_CELL__EXPRESSION);
		createEAttribute(oclCellEClass, OCL_CELL__CELL_LOCATE_BEHAVIOR);

		editProviderCellEClass = createEClass(EDIT_PROVIDER_CELL);

		// Create enums
		cellEditBehaviorOptionEEnum = createEEnum(CELL_EDIT_BEHAVIOR_OPTION);
		cellLocateBehaviorOptionEEnum = createEEnum(CELL_LOCATE_BEHAVIOR_OPTION);

		// Create data types
		fontEDataType = createEDataType(FONT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized)
			return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		rowFeatureCellEClass.getESuperTypes().add(this.getCellConfig());
		oclCellEClass.getESuperTypes().add(this.getCellConfig());
		editProviderCellEClass.getESuperTypes().add(this.getCellConfig());

		// Initialize classes and features; add operations and parameters
		initEClass(editorConfigEClass, EditorConfig.class, "EditorConfig",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEditorConfig_TableConfig(), this.getTableConfig(),
				null, "tableConfig", null, 0, -1, EditorConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		getEditorConfig_TableConfig().getEKeys()
				.add(this.getTableConfig_Name());
		initEReference(getEditorConfig_HeaderTableConfig(), this
				.getTableConfig(), null, "headerTableConfig", null, 0, 1,
				EditorConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getEditorConfig_ClassToTableConfigMap(), this
				.getEClassToTableConfigMapEntry(), null,
				"classToTableConfigMap", null, 0, -1, EditorConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				!IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEditorConfig_SuperConfig(), this.getEditorConfig(),
				null, "superConfig", null, 0, 1, EditorConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		EOperation op = addEOperation(editorConfigEClass,
				this.getTableConfig(), "getTableConfig", 0, 1, IS_UNIQUE,
				IS_ORDERED);
		addEParameter(op, ecorePackage.getEClass(), "eClass", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		initEClass(eClassToTableConfigMapEntryEClass, Map.Entry.class,
				"EClassToTableConfigMapEntry", !IS_ABSTRACT, !IS_INTERFACE,
				!IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEClassToTableConfigMapEntry_Key(), ecorePackage
				.getEClass(), null, "key", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEClassToTableConfigMapEntry_Value(), this
				.getTableConfig(), null, "value", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEClassToTableConfigMapEntry_IntendedPackage(),
				ecorePackage.getEPackage(), null, "intendedPackage", null, 0,
				1, Map.Entry.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(eReferenceToTableConfigMapEntryEClass, Map.Entry.class,
				"EReferenceToTableConfigMapEntry", !IS_ABSTRACT, !IS_INTERFACE,
				!IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEReferenceToTableConfigMapEntry_Key(), ecorePackage
				.getEReference(), null, "key", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEReferenceToTableConfigMapEntry_Value(), this
				.getTableConfig(), null, "value", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getEReferenceToTableConfigMapEntry_Label(), ecorePackage
				.getEString(), "label", null, 0, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE,
				!IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(tableConfigEClass, TableConfig.class, "TableConfig",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTableConfig_Name(), ecorePackage.getEString(),
				"name", null, 1, 1, TableConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableConfig_Label(), ecorePackage.getEString(),
				"label", null, 0, 1, TableConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableConfig_DisplayDefaultColumn(), ecorePackage
				.getEBoolean(), "displayDefaultColumn", "false", 0, 1,
				TableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableConfig_DisplayHeader(), ecorePackage
				.getEBoolean(), "displayHeader", "true", 0, 1,
				TableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTableConfig_Font(), this.getFont(), "font", null, 0,
				1, TableConfig.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getTableConfig_Column(), this.getColumnConfig(), null,
				"column", null, 0, -1, TableConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTableConfig_ReferenceToTableConfigMap(), this
				.getEReferenceToTableConfigMapEntry(), null,
				"referenceToTableConfigMap", null, 0, -1, TableConfig.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getTableConfig_IntendedPackage(), ecorePackage
				.getEPackage(), null, "intendedPackage", null, 0, 1,
				TableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEReference(getTableConfig_IntendedClass(),
				ecorePackage.getEClass(), null, "intendedClass", null, 0, 1,
				TableConfig.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE,
				!IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		op = addEOperation(tableConfigEClass, this.getTableConfig(),
				"getTableConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEReference(), "eReference", 1, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(columnConfigEClass, ColumnConfig.class, "ColumnConfig",
				!IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getColumnConfig_Name(), ecorePackage.getEString(),
				"name", null, 1, 1, ColumnConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getColumnConfig_Label(), ecorePackage.getEString(),
				"label", null, 0, 1, ColumnConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getColumnConfig_Width(), ecorePackage.getEInt(),
				"width", null, 0, 1, ColumnConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getColumnConfig_Font(), this.getFont(), "font", null, 0,
				1, ColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getColumnConfig_CellConfigMap(), this
				.getEClassToCellConfigMapEntry(), null, "cellConfigMap", null,
				0, -1, ColumnConfig.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = addEOperation(columnConfigEClass, this.getCellConfig(),
				"getCellConfig", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEClass(), "eClass", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = addEOperation(columnConfigEClass, ecorePackage.getEJavaObject(),
				"getColumnValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "row", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		addEOperation(columnConfigEClass, this.getFont(), "getColumnFont", 0,
				1, IS_UNIQUE, IS_ORDERED);

		initEClass(eClassToCellConfigMapEntryEClass, Map.Entry.class,
				"EClassToCellConfigMapEntry", !IS_ABSTRACT, !IS_INTERFACE,
				!IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEClassToCellConfigMapEntry_Key(), ecorePackage
				.getEClass(), null, "key", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEReference(getEClassToCellConfigMapEntry_Value(), this
				.getCellConfig(), null, "value", null, 1, 1, Map.Entry.class,
				!IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE,
				IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(cellConfigEClass, CellConfig.class, "CellConfig",
				IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCellConfig_Font(), this.getFont(), "font", null, 0,
				1, CellConfig.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getCellConfig_Color(), ecorePackage.getEString(),
				"color", null, 0, 1, CellConfig.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		op = addEOperation(cellConfigEClass, ecorePackage.getEJavaObject(),
				"getCellValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "row", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		op = addEOperation(cellConfigEClass, ecorePackage.getEBoolean(),
				"getColorValue", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEObject(), "row", 1, 1, IS_UNIQUE,
				IS_ORDERED);

		addEOperation(cellConfigEClass, this.getFont(), "getCellFont", 0, 1,
				IS_UNIQUE, IS_ORDERED);

		initEClass(rowFeatureCellEClass, RowFeatureCell.class,
				"RowFeatureCell", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRowFeatureCell_Feature(), ecorePackage
				.getEStructuralFeature(), null, "feature", null, 1, 1,
				RowFeatureCell.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES,
				!IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRowFeatureCell_CellEditBehavior(), this
				.getCellEditBehaviorOption(), "cellEditBehavior", "Selection",
				0, 1, RowFeatureCell.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);
		initEAttribute(getRowFeatureCell_CellLocateBehavior(), this
				.getCellLocateBehaviorOption(), "cellLocateBehavior",
				"Selection", 0, 1, RowFeatureCell.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);

		initEClass(oclCellEClass, OclCell.class, "OclCell", !IS_ABSTRACT,
				!IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getOclCell_Expression(), ecorePackage.getEString(),
				"expression", null, 1, 1, OclCell.class, !IS_TRANSIENT,
				!IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE,
				!IS_DERIVED, IS_ORDERED);
		initEAttribute(getOclCell_CellLocateBehavior(), this
				.getCellLocateBehaviorOption(), "cellLocateBehavior",
				"Selection", 0, 1, OclCell.class, !IS_TRANSIENT, !IS_VOLATILE,
				IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED,
				IS_ORDERED);

		initEClass(editProviderCellEClass, EditProviderCell.class,
				"EditProviderCell", !IS_ABSTRACT, !IS_INTERFACE,
				IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(cellEditBehaviorOptionEEnum, CellEditBehaviorOption.class,
				"CellEditBehaviorOption");
		addEEnumLiteral(cellEditBehaviorOptionEEnum,
				CellEditBehaviorOption.SELECTION);
		addEEnumLiteral(cellEditBehaviorOptionEEnum,
				CellEditBehaviorOption.PULL_DOWN);
		addEEnumLiteral(cellEditBehaviorOptionEEnum,
				CellEditBehaviorOption.READ_ONLY);

		initEEnum(cellLocateBehaviorOptionEEnum,
				CellLocateBehaviorOption.class, "CellLocateBehaviorOption");
		addEEnumLiteral(cellLocateBehaviorOptionEEnum,
				CellLocateBehaviorOption.NONE);
		addEEnumLiteral(cellLocateBehaviorOptionEEnum,
				CellLocateBehaviorOption.SELECTION);

		// Initialize data types
		initEDataType(fontEDataType, URI.class, "Font", IS_SERIALIZABLE,
				!IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.xocl.org/GENMODEL
		createGENMODELAnnotations();
		// http://www.xocl.org/OCL
		createOCLAnnotations();
		// http://www.xocl.org/mdcm/v3
		createV3Annotations();
		// http://www.xocl.org/mdcm/v4
		createV4Annotations();
		// http://www.xocl.org/mdcm/v2
		createV2Annotations();
		// http://www.xocl.org/EXPRESSION_OCL
		createEXPRESSION_OCLAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/GENMODEL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createGENMODELAnnotations() {
		String source = "http://www.xocl.org/GENMODEL";
		addAnnotation(this, source, new String[] { "prefix", "EditorConfig",
				"GENMODEL_modelName", "EditorConfig" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/mdcm/v3</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createV3Annotations() {
		String source = "http://www.xocl.org/mdcm/v3";
		addAnnotation(getEditorConfig_SuperConfig(), source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/mdcm/v4</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createV4Annotations() {
		String source = "http://www.xocl.org/mdcm/v4";
		addAnnotation(getEditorConfig_SuperConfig(), source, new String[] {
				"many=true", "" });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/mdcm/v2</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createV2Annotations() {
		String source = "http://www.xocl.org/mdcm/v2";
		addAnnotation(getTableConfig_ReferenceToTableConfigMap(), source,
				new String[] {});
		addAnnotation(editProviderCellEClass, source, new String[] {});
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createOCLAnnotations() {
		String source = "http://www.xocl.org/OCL";
		addAnnotation(this, source, new String[] { "rootConstraint",
				"trg.name = \'EditorConfig\'" });
		addAnnotation(
				getEClassToTableConfigMapEntry_Key(),
				source,
				new String[] {
						"choiceConstraint",
						"let p:ecore::EPackage = self.intendedPackage\r\nin \r\nif p.oclIsUndefined() then true\r\nelse if p.eClassifiers->includes(trg) then true\r\nelse p.eSubpackages.eClassifiers->includes(trg)\r\nendif endif" });
		addAnnotation(
				getEReferenceToTableConfigMapEntry_Key(),
				source,
				new String[] {
						"choiceConstraint",
						"let \r\n  tc:editorconfig::TableConfig = self.eContainer().oclAsType(editorconfig::TableConfig) in\r\nlet ic:ecore::EClass = tc.intendedClass in\r\nlet intended:Boolean = \r\n  if not ic.oclIsUndefined()\r\n  then\r\n    ic.eAllReferences->includes(trg)\r\n  else true endif in \r\nlet unique:Boolean =\r\n  self.key=trg or not (tc.referenceToTableConfigMap.key->includes(trg))\r\n in unique" });
		addAnnotation(tableConfigEClass, source, new String[] { "label",
				"name->asOrderedSet()" });
		addAnnotation(
				getTableConfig_IntendedClass(),
				source,
				new String[] {
						"choiceConstraint",
						"let p:ecore::EPackage = self.intendedPackage\r\nin \r\nif p.oclIsUndefined() then true\r\nelse if p.eClassifiers->includes(trg) then true\r\nelse p.eSubpackages.eClassifiers->includes(trg)\r\nendif endif" });
		addAnnotation(
				getEClassToCellConfigMapEntry_Key(),
				source,
				new String[] {
						"choiceConstraint",
						"let cc:editorconfig::ColumnConfig = self.eContainer().oclAsType(editorconfig::ColumnConfig)\r\nin \r\nlet tc:editorconfig::TableConfig = cc.eContainer().oclAsType(editorconfig::TableConfig) in\r\nlet  ic:ecore::EClass = tc.intendedClass in \r\nlet intended:Boolean = if not ic.oclIsUndefined()\r\n  then   \r\n      trg=ic or trg.eAllSuperTypes->includes(ic)\r\n  else\r\n    let ip:ecore::EPackage = tc.intendedPackage in\r\n    if ip.oclIsUndefined() then true \r\n    else \r\n      if ip.eClassifiers->includes(trg) then true\r\n      else ip.eSubpackages.eClassifiers->includes(trg)\r\n      endif\r\n    endif\r\n  endif\r\nin\r\nlet unique:Boolean = \r\n  self.key = trg or not (cc.cellConfigMap.key->includes(trg))\r\nin \r\nintended and unique" });
		addAnnotation(
				getRowFeatureCell_Feature(),
				source,
				new String[] {
						"choiceConstruction",
						"eContainer().oclAsType(EClassToCellConfigMapEntry).key.eAllStructuralFeatures",
						"choiceConstraint",
						"if trg.oclIsTypeOf(ecore::EReference) then\r\n  not trg.oclAsType(ecore::EReference).containment\r\nelse \r\n  true\r\nendif " });
	}

	/**
	 * Initializes the annotations for <b>http://www.xocl.org/EXPRESSION_OCL</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEXPRESSION_OCLAnnotations() {
		String source = "http://www.xocl.org/EXPRESSION_OCL";
		addAnnotation(getCellConfig_Color(), source, new String[] {});
		addAnnotation(getOclCell_Expression(), source, new String[] {});
	}

} //EditorConfigPackageImpl
