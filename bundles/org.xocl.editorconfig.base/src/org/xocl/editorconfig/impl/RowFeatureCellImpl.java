/**
 * <copyright>
 * </copyright>
 *
 * $Id: RowFeatureCellImpl.java,v 1.19 2011/07/20 17:04:54 mtg.kutter Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.CellEditBehaviorOption;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.RowFeatureCell;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Row Feature Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.RowFeatureCellImpl#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.RowFeatureCellImpl#getCellEditBehavior <em>Cell Edit Behavior</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.RowFeatureCellImpl#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class RowFeatureCellImpl extends CellConfigImpl implements
		RowFeatureCell {
	/**
	 * The cached value of the '{@link #getFeature() <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature()
	 * @generated
	 * @ordered
	 */
	protected EStructuralFeature feature;

	/**
	 * The default value of the '{@link #getCellEditBehavior() <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellEditBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final CellEditBehaviorOption CELL_EDIT_BEHAVIOR_EDEFAULT = CellEditBehaviorOption.SELECTION;

	/**
	 * The cached value of the '{@link #getCellEditBehavior() <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellEditBehavior()
	 * @generated
	 * @ordered
	 */
	protected CellEditBehaviorOption cellEditBehavior = CELL_EDIT_BEHAVIOR_EDEFAULT;

	/**
	 * The default value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected static final CellLocateBehaviorOption CELL_LOCATE_BEHAVIOR_EDEFAULT = CellLocateBehaviorOption.SELECTION;

	/**
	 * The cached value of the '{@link #getCellLocateBehavior() <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellLocateBehavior()
	 * @generated
	 * @ordered
	 */
	protected CellLocateBehaviorOption cellLocateBehavior = CELL_LOCATE_BEHAVIOR_EDEFAULT;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getFeature <em>Feature</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression featureChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the construction of valid choices of '{@link #getFeature <em>Feature</em>}' property.
	 * Is combined with the choice constraint definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFeature
	 * @templateTag DFGFI04
	 * @generated
	 */
	private static OCLExpression featureChoiceConstructionOCL;

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions
				.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RowFeatureCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.ROW_FEATURE_CELL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature getFeature() {
		if (feature != null && feature.eIsProxy()) {
			InternalEObject oldFeature = (InternalEObject) feature;
			feature = (EStructuralFeature) eResolveProxy(oldFeature);
			if (feature != oldFeature) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EditorConfigPackage.ROW_FEATURE_CELL__FEATURE,
							oldFeature, feature));
			}
		}
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EStructuralFeature basicGetFeature() {
		return feature;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFeature(EStructuralFeature newFeature) {
		EStructuralFeature oldFeature = feature;
		feature = newFeature;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.ROW_FEATURE_CELL__FEATURE, oldFeature,
					feature));
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type EStructuralFeature
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL if trg.oclIsTypeOf(ecore::EReference) then
	not trg.oclAsType(ecore::EReference).containment
	else 
	true
	endif 
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalFeatureChoiceConstraint(EStructuralFeature trg) {
		EClass eClass = EditorConfigPackage.Literals.ROW_FEATURE_CELL;
		if (featureChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = EditorConfigPackage.Literals.ROW_FEATURE_CELL__FEATURE;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				featureChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, choiceConstraint, helper
								.getProblems(), eClass,
						"FeatureChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(featureChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "FeatureChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * Evaluates the OCL defined choice construction for the '<em><b>Feature</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the choice being of type ArrayList<EStructuralFeature>
	 * Inside the constraint, the choice can be accessed as 'choice'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL eContainer().oclAsType(EClassToCellConfigMapEntry).key.eAllStructuralFeatures
	 * @templateTag GFI02
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public List<EStructuralFeature> evalFeatureChoiceConstruction(
			List<EStructuralFeature> choice) {
		EClass eClass = EditorConfigPackage.Literals.ROW_FEATURE_CELL;
		if (featureChoiceConstructionOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			// create a variable declaring our global application context object
			Variable choiceVar = EcoreFactory.eINSTANCE.createVariable();
			choiceVar.setName("choice");
			choiceVar.setType(OCL_ENV.getEnvironment().getOCLStandardLibrary()
					.getSequence());
			// add it to the global OCL environment
			OCL_ENV.getEnvironment().addElement(choiceVar.getName(), choiceVar,
					true);
			EStructuralFeature eStructuralFeature = EditorConfigPackage.Literals.ROW_FEATURE_CELL__FEATURE;

			String choiceConstruction = XoclEmfUtil
					.findChoiceConstructionAnnotationText(eStructuralFeature,
							eClass());

			try {
				featureChoiceConstructionOCL = helper
						.createQuery(choiceConstruction);
			} catch (ParserException e) {
				return choice;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, choiceConstruction,
						helper.getProblems(), eClass,
						"FeatureChoiceConstruction");
			}
		}
		Query query = OCL_ENV.createQuery(featureChoiceConstructionOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "FeatureChoiceConstruction");
			query.getEvaluationEnvironment().add("choice", choice);
			List<EStructuralFeature> result = new ArrayList<EStructuralFeature>(
					(Collection<EStructuralFeature>) query.evaluate(this));
			result.remove(null);
			return result;
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return choice;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellEditBehaviorOption getCellEditBehavior() {
		return cellEditBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellEditBehavior(CellEditBehaviorOption newCellEditBehavior) {
		CellEditBehaviorOption oldCellEditBehavior = cellEditBehavior;
		cellEditBehavior = newCellEditBehavior == null ? CELL_EDIT_BEHAVIOR_EDEFAULT
				: newCellEditBehavior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR,
					oldCellEditBehavior, cellEditBehavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellLocateBehaviorOption getCellLocateBehavior() {
		return cellLocateBehavior;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCellLocateBehavior(
			CellLocateBehaviorOption newCellLocateBehavior) {
		CellLocateBehaviorOption oldCellLocateBehavior = cellLocateBehavior;
		cellLocateBehavior = newCellLocateBehavior == null ? CELL_LOCATE_BEHAVIOR_EDEFAULT
				: newCellLocateBehavior;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR,
					oldCellLocateBehavior, cellLocateBehavior));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getCellValue(EObject row) {
		EClass eContainingClass = getFeature().getEContainingClass();
		if (eContainingClass != null && eContainingClass.isInstance(row)) {
//		Assert.isLegal(getFeature().getEContainingClass().isInstance(row),
//				"Row object is not an instance of feature' EClass");
			return row.eGet(getFeature());
		}
		return null;

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.ROW_FEATURE_CELL__FEATURE:
			if (resolve)
				return getFeature();
			return basicGetFeature();
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			return getCellEditBehavior();
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			return getCellLocateBehavior();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.ROW_FEATURE_CELL__FEATURE:
			setFeature((EStructuralFeature) newValue);
			return;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			setCellEditBehavior((CellEditBehaviorOption) newValue);
			return;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			setCellLocateBehavior((CellLocateBehaviorOption) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.ROW_FEATURE_CELL__FEATURE:
			setFeature((EStructuralFeature) null);
			return;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			setCellEditBehavior(CELL_EDIT_BEHAVIOR_EDEFAULT);
			return;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			setCellLocateBehavior(CELL_LOCATE_BEHAVIOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.ROW_FEATURE_CELL__FEATURE:
			return feature != null;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_EDIT_BEHAVIOR:
			return cellEditBehavior != CELL_EDIT_BEHAVIOR_EDEFAULT;
		case EditorConfigPackage.ROW_FEATURE_CELL__CELL_LOCATE_BEHAVIOR:
			return cellLocateBehavior != CELL_LOCATE_BEHAVIOR_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (cellEditBehavior: ");
		result.append(cellEditBehavior);
		result.append(", cellLocateBehavior: ");
		result.append(cellLocateBehavior);
		result.append(')');
		return result.toString();
	}

} //RowFeatureCellImpl
