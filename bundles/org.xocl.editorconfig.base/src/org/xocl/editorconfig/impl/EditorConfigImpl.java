/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigImpl.java,v 1.12 2009/07/25 12:34:20 xocl.stepanovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Editor Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.EditorConfigImpl#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EditorConfigImpl#getHeaderTableConfig <em>Header Table Config</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EditorConfigImpl#getClassToTableConfigMap <em>Class To Table Config Map</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EditorConfigImpl#getSuperConfig <em>Super Config</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EditorConfigImpl extends EObjectImpl implements EditorConfig {
	/**
	 * The cached value of the '{@link #getTableConfig() <em>Table Config</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTableConfig()
	 * @generated
	 * @ordered
	 */
	protected EList<TableConfig> tableConfig;

	/**
	 * The cached value of the '{@link #getHeaderTableConfig() <em>Header Table Config</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHeaderTableConfig()
	 * @generated
	 * @ordered
	 */
	protected TableConfig headerTableConfig;

	/**
	 * The cached value of the '{@link #getClassToTableConfigMap() <em>Class To Table Config Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getClassToTableConfigMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, TableConfig> classToTableConfigMap;

	/**
	 * The cached value of the '{@link #getSuperConfig() <em>Super Config</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSuperConfig()
	 * @generated
	 * @ordered
	 */
	protected EditorConfig superConfig;

	private static final EClass EObject_EClass = EcorePackage.eINSTANCE
			.getEObject();

	private transient Map<EClass, TableConfig> cachedClassToTableConfigMap = new HashMap<EClass, TableConfig>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditorConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.EDITOR_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TableConfig> getTableConfig() {
		if (tableConfig == null) {
			tableConfig = new EObjectContainmentEList.Resolving<TableConfig>(
					TableConfig.class, this,
					EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG);
		}
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig getHeaderTableConfig() {
		if (headerTableConfig != null && headerTableConfig.eIsProxy()) {
			InternalEObject oldHeaderTableConfig = (InternalEObject) headerTableConfig;
			headerTableConfig = (TableConfig) eResolveProxy(oldHeaderTableConfig);
			if (headerTableConfig != oldHeaderTableConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG,
							oldHeaderTableConfig, headerTableConfig));
			}
		}
		return headerTableConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig basicGetHeaderTableConfig() {
		return headerTableConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHeaderTableConfig(TableConfig newHeaderTableConfig) {
		TableConfig oldHeaderTableConfig = headerTableConfig;
		headerTableConfig = newHeaderTableConfig;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG,
					oldHeaderTableConfig, headerTableConfig));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, TableConfig> getClassToTableConfigMap() {
		if (classToTableConfigMap == null) {
			classToTableConfigMap = new EcoreEMap<EClass, TableConfig>(
					EditorConfigPackage.Literals.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY,
					EClassToTableConfigMapEntryImpl.class,
					this,
					EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP);
		}
		return classToTableConfigMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfig getSuperConfig() {
		if (superConfig != null && superConfig.eIsProxy()) {
			InternalEObject oldSuperConfig = (InternalEObject) superConfig;
			superConfig = (EditorConfig) eResolveProxy(oldSuperConfig);
			if (superConfig != oldSuperConfig) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG,
							oldSuperConfig, superConfig));
			}
		}
		return superConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfig basicGetSuperConfig() {
		return superConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuperConfig(EditorConfig newSuperConfig) {
		EditorConfig oldSuperConfig = superConfig;
		superConfig = newSuperConfig;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG,
					oldSuperConfig, superConfig));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public TableConfig getTableConfig(EClass eClass) {
		TableConfig tableConfig = cachedClassToTableConfigMap.get(eClass);
		if (tableConfig == null
				&& !cachedClassToTableConfigMap.containsKey(eClass)) {
			Stack<EClass> stack = new Stack<EClass>();
			stack.push(eClass);
			boolean hasType = false;
			while (!stack.isEmpty()) {
				EClass type = stack.pop();
				hasType = getClassToTableConfigMap().containsKey(type);
				if (hasType) {
					tableConfig = getClassToTableConfigMap().get(type);
					break;
				}
				if (type != null) {
					EList<EClass> superTypes = type.getESuperTypes();
					if (!superTypes.isEmpty()) {
						stack.addAll(superTypes);
					}
				}
			}
			if (!hasType && tableConfig == null) {
				tableConfig = getClassToTableConfigMap().get(EObject_EClass);
			}
			cachedClassToTableConfigMap.put(eClass, tableConfig);
		}
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
			return ((InternalEList<?>) getTableConfig()).basicRemove(otherEnd,
					msgs);
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			return ((InternalEList<?>) getClassToTableConfigMap()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
			return getTableConfig();
		case EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG:
			if (resolve)
				return getHeaderTableConfig();
			return basicGetHeaderTableConfig();
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			if (coreType)
				return getClassToTableConfigMap();
			else
				return getClassToTableConfigMap().map();
		case EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG:
			if (resolve)
				return getSuperConfig();
			return basicGetSuperConfig();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
			getTableConfig().clear();
			getTableConfig().addAll(
					(Collection<? extends TableConfig>) newValue);
			return;
		case EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG:
			setHeaderTableConfig((TableConfig) newValue);
			return;
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			((EStructuralFeature.Setting) getClassToTableConfigMap())
					.set(newValue);
			return;
		case EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG:
			setSuperConfig((EditorConfig) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
			getTableConfig().clear();
			return;
		case EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG:
			setHeaderTableConfig((TableConfig) null);
			return;
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			getClassToTableConfigMap().clear();
			return;
		case EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG:
			setSuperConfig((EditorConfig) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.EDITOR_CONFIG__TABLE_CONFIG:
			return tableConfig != null && !tableConfig.isEmpty();
		case EditorConfigPackage.EDITOR_CONFIG__HEADER_TABLE_CONFIG:
			return headerTableConfig != null;
		case EditorConfigPackage.EDITOR_CONFIG__CLASS_TO_TABLE_CONFIG_MAP:
			return classToTableConfigMap != null
					&& !classToTableConfigMap.isEmpty();
		case EditorConfigPackage.EDITOR_CONFIG__SUPER_CONFIG:
			return superConfig != null;
		}
		return super.eIsSet(featureID);
	}

} //EditorConfigImpl
