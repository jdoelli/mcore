/**
 * <copyright>
 * </copyright>
 *
 * $Id: EReferenceToTableConfigMapEntryImpl.java,v 1.10 2010/02/05 09:45:34 xocl.stepanovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EReference To Table Config Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl#getTypedKey <em>Key</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl#getTypedValue <em>Value</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EReferenceToTableConfigMapEntryImpl#getLabel <em>Label</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EReferenceToTableConfigMapEntryImpl extends EObjectImpl implements
		BasicEMap.Entry<EReference, TableConfig> {
	/**
	 * The cached value of the '{@link #getTypedKey() <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected EReference key;

	/**
	 * The cached value of the '{@link #getTypedValue() <em>Value</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedValue()
	 * @generated
	 * @ordered
	 */
	protected TableConfig value;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getTypedKey <em>Key</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression keyChoiceConstraintOCL;

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions
				.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EReferenceToTableConfigMapEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypedKey() {
		if (key != null && key.eIsProxy()) {
			InternalEObject oldKey = (InternalEObject) key;
			key = (EReference) eResolveProxy(oldKey);
			if (key != oldKey) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY,
							oldKey, key));
			}
		}
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference basicGetTypedKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedKey(EReference newKey) {
		EReference oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY,
					oldKey, key));
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Key</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type EReference
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let 
	tc:editorconfig::TableConfig = self.eContainer().oclAsType(editorconfig::TableConfig) in
	let ic:ecore::EClass = tc.intendedClass in
	let intended:Boolean = 
	if not ic.oclIsUndefined()
	then
	ic.eAllReferences->includes(trg)
	else true endif in 
	let unique:Boolean =
	self.key=trg or not (tc.referenceToTableConfigMap.key->includes(trg))
	in
	trg.containment and intended and unique
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalTypedKeyChoiceConstraint(EReference trg) {
		EClass eClass = EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY;
		if (keyChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				keyChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, choiceConstraint, helper
								.getProblems(), eClass,
						"TypedKeyChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(keyChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "TypedKeyChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig getTypedValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject) value;
			value = (TableConfig) eResolveProxy(oldValue);
			if (value != oldValue) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE,
							oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig basicGetTypedValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedValue(TableConfig newValue) {
		TableConfig oldValue = value;
		value = newValue;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE,
					oldValue, value));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(
					this,
					Notification.SET,
					EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL,
					oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY:
			if (resolve)
				return getTypedKey();
			return basicGetTypedKey();
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE:
			if (resolve)
				return getTypedValue();
			return basicGetTypedValue();
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL:
			return getLabel();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY:
			setTypedKey((EReference) newValue);
			return;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE:
			setTypedValue((TableConfig) newValue);
			return;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL:
			setLabel((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY:
			setTypedKey((EReference) null);
			return;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE:
			setTypedValue((TableConfig) null);
			return;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL:
			setLabel(LABEL_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__KEY:
			return key != null;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__VALUE:
			return value != null;
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY__LABEL:
			return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT
					.equals(label);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (label: ");
		result.append(label);
		result.append(')');
		return result.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected int hash = -1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHash() {
		if (hash == -1) {
			Object theKey = getKey();
			hash = (theKey == null ? 0 : theKey.hashCode());
		}
		return hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHash(int hash) {
		this.hash = hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getKey() {
		return getTypedKey();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(EReference key) {
		setTypedKey(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig getValue() {
		return getTypedValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig setValue(TableConfig value) {
		TableConfig oldValue = getValue();
		setTypedValue(value);
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EMap<EReference, TableConfig> getEMap() {
		EObject container = eContainer();
		return container == null ? null
				: (EMap<EReference, TableConfig>) container
						.eGet(eContainmentFeature());
	}

} //EReferenceToTableConfigMapEntryImpl
