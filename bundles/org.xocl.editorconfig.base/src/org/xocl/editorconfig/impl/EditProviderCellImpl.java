/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditProviderCellImpl.java,v 1.4 2010/11/30 08:04:06 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import org.eclipse.emf.ecore.EClass;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.EditorConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Edit Provider Cell</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class EditProviderCellImpl extends CellConfigImpl implements
		EditProviderCell {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EditProviderCellImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.EDIT_PROVIDER_CELL;
	}

} //EditProviderCellImpl
