/**
 * <copyright>
 * </copyright>
 *
 * $Id: ColumnConfigImpl.java,v 1.7 2009/09/21 10:19:20 xocl.stepanovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Column Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.ColumnConfigImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.ColumnConfigImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.ColumnConfigImpl#getWidth <em>Width</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.ColumnConfigImpl#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.ColumnConfigImpl#getCellConfigMap <em>Cell Config Map</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class ColumnConfigImpl extends EObjectImpl implements ColumnConfig {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected static final int WIDTH_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getWidth() <em>Width</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getWidth()
	 * @generated
	 * @ordered
	 */
	protected int width = WIDTH_EDEFAULT;

	/**
	 * The default value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected static final URI FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected URI font = FONT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCellConfigMap() <em>Cell Config Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCellConfigMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EClass, CellConfig> cellConfigMap;

	private static final String EMPTY_STRING = "";

	private static final EClass EObject_EClass = EcorePackage.eINSTANCE
			.getEObject();

	private transient Map<EClass, CellConfig> cachedCellConfigMap = new HashMap<EClass, CellConfig>();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ColumnConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.COLUMN_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.COLUMN_CONFIG__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.COLUMN_CONFIG__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWidth(int newWidth) {
		int oldWidth = width;
		width = newWidth;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.COLUMN_CONFIG__WIDTH, oldWidth, width));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getFont() {
		return font;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFont(URI newFont) {
		URI oldFont = font;
		font = newFont;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.COLUMN_CONFIG__FONT, oldFont, font));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EClass, CellConfig> getCellConfigMap() {
		if (cellConfigMap == null) {
			cellConfigMap = new EcoreEMap<EClass, CellConfig>(
					EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY,
					EClassToCellConfigMapEntryImpl.class, this,
					EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP);
		}
		return cellConfigMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public CellConfig getCellConfig(EClass eClass) {
		CellConfig cellConfig = cachedCellConfigMap.get(eClass);
		if (cellConfig == null && !cachedCellConfigMap.containsKey(eClass)) {
			Stack<EClass> stack = new Stack<EClass>();
			stack.push(eClass);
			while (!stack.isEmpty()) {
				EClass type = stack.pop();
				cellConfig = getCellConfigMap().get(type);
				if (cellConfig != null) {
					break;
				}
				EList<EClass> superTypes = type.getESuperTypes();
				if (!superTypes.isEmpty()) {
					stack.addAll(superTypes);
				}
			}
			if (cellConfig == null) {
				cellConfig = getCellConfigMap().get(EObject_EClass);
			}
			cachedCellConfigMap.put(eClass, cellConfig);
		}
		return cellConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Object getColumnValue(EObject row) {
		CellConfig cellConfig = getCellConfig(row.eClass());
		if (cellConfig != null) {
			return cellConfig.getCellValue(row);
		}
		return EMPTY_STRING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI getColumnFont() {
		URI font = getFont();
		if (font != null) {
			return font;
		}
		return ((TableConfig) eContainer()).getFont();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP:
			return ((InternalEList<?>) getCellConfigMap()).basicRemove(
					otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.COLUMN_CONFIG__NAME:
			return getName();
		case EditorConfigPackage.COLUMN_CONFIG__LABEL:
			return getLabel();
		case EditorConfigPackage.COLUMN_CONFIG__WIDTH:
			return new Integer(getWidth());
		case EditorConfigPackage.COLUMN_CONFIG__FONT:
			return getFont();
		case EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP:
			if (coreType)
				return getCellConfigMap();
			else
				return getCellConfigMap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.COLUMN_CONFIG__NAME:
			setName((String) newValue);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__LABEL:
			setLabel((String) newValue);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__WIDTH:
			setWidth(((Integer) newValue).intValue());
			return;
		case EditorConfigPackage.COLUMN_CONFIG__FONT:
			setFont((URI) newValue);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP:
			((EStructuralFeature.Setting) getCellConfigMap()).set(newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.COLUMN_CONFIG__NAME:
			setName(NAME_EDEFAULT);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__LABEL:
			setLabel(LABEL_EDEFAULT);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__WIDTH:
			setWidth(WIDTH_EDEFAULT);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__FONT:
			setFont(FONT_EDEFAULT);
			return;
		case EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP:
			getCellConfigMap().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.COLUMN_CONFIG__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT
			.equals(name);
		case EditorConfigPackage.COLUMN_CONFIG__LABEL:
			return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT
			.equals(label);
		case EditorConfigPackage.COLUMN_CONFIG__WIDTH:
			return width != WIDTH_EDEFAULT;
		case EditorConfigPackage.COLUMN_CONFIG__FONT:
			return FONT_EDEFAULT == null ? font != null : !FONT_EDEFAULT
			.equals(font);
		case EditorConfigPackage.COLUMN_CONFIG__CELL_CONFIG_MAP:
			return cellConfigMap != null && !cellConfigMap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", label: ");
		result.append(label);
		result.append(", width: ");
		result.append(width);
		result.append(", font: ");
		result.append(font);
		result.append(')');
		return result.toString();
	}


} //ColumnConfigImpl
