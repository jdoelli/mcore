/**
 * <copyright>
 * </copyright>
 *
 * $Id: TableConfigImpl.java,v 1.17 2010/11/26 04:18:59 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Helper;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclHelper;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Table Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getName <em>Name</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#isDisplayDefaultColumn <em>Display Default Column</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#isDisplayHeader <em>Display Header</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getColumn <em>Column</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getReferenceToTableConfigMap <em>Reference To Table Config Map</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getIntendedPackage <em>Intended Package</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.TableConfigImpl#getIntendedClass <em>Intended Class</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class TableConfigImpl extends EObjectImpl implements TableConfig {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected static final String LABEL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLabel() <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLabel()
	 * @generated
	 * @ordered
	 */
	protected String label = LABEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isDisplayDefaultColumn() <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayDefaultColumn()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISPLAY_DEFAULT_COLUMN_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isDisplayDefaultColumn() <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayDefaultColumn()
	 * @generated
	 * @ordered
	 */
	protected boolean displayDefaultColumn = DISPLAY_DEFAULT_COLUMN_EDEFAULT;

	/**
	 * The default value of the '{@link #isDisplayHeader() <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayHeader()
	 * @generated
	 * @ordered
	 */
	protected static final boolean DISPLAY_HEADER_EDEFAULT = true;

	/**
	 * The cached value of the '{@link #isDisplayHeader() <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isDisplayHeader()
	 * @generated
	 * @ordered
	 */
	protected boolean displayHeader = DISPLAY_HEADER_EDEFAULT;

	/**
	 * The default value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected static final URI FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected URI font = FONT_EDEFAULT;

	/**
	 * The cached value of the '{@link #getColumn() <em>Column</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColumn()
	 * @generated
	 * @ordered
	 */
	protected EList<ColumnConfig> column;

	/**
	 * The cached value of the '{@link #getReferenceToTableConfigMap() <em>Reference To Table Config Map</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferenceToTableConfigMap()
	 * @generated
	 * @ordered
	 */
	protected EMap<EReference, TableConfig> referenceToTableConfigMap;

	/**
	 * The cached value of the '{@link #getIntendedPackage() <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntendedPackage()
	 * @generated
	 * @ordered
	 */
	protected EPackage intendedPackage;

	/**
	 * The cached value of the '{@link #getIntendedClass() <em>Intended Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntendedClass()
	 * @generated
	 * @ordered
	 */
	protected EClass intendedClass;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getIntendedClass <em>Intended Class</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIntendedClass
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression intendedClassChoiceConstraintOCL;

	/**
	 * The parsed OCL expression for the evaluation of the '{@link #evalOclLabel <em>label</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #evalOclLabel
	 * @templateTag DFGFI09
	 * @generated
	 */
	private static OCLExpression labelOCL;

	/**
	 * The OCL annotation source.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI10
	 * @generated
	 */
	private static final String OCL_ANNOTATION_SOURCE = "http://www.xocl.org/OCL";

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions
				.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TableConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.TABLE_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLabel(String newLabel) {
		String oldLabel = label;
		label = newLabel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__LABEL, oldLabel, label));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisplayDefaultColumn() {
		return displayDefaultColumn;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayDefaultColumn(boolean newDisplayDefaultColumn) {
		boolean oldDisplayDefaultColumn = displayDefaultColumn;
		displayDefaultColumn = newDisplayDefaultColumn;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN,
					oldDisplayDefaultColumn, displayDefaultColumn));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isDisplayHeader() {
		return displayHeader;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDisplayHeader(boolean newDisplayHeader) {
		boolean oldDisplayHeader = displayHeader;
		displayHeader = newDisplayHeader;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER,
					oldDisplayHeader, displayHeader));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getFont() {
		return font;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFont(URI newFont) {
		URI oldFont = font;
		font = newFont;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__FONT, oldFont, font));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ColumnConfig> getColumn() {
		if (column == null) {
			column = new EObjectContainmentEList.Resolving<ColumnConfig>(
					ColumnConfig.class, this,
					EditorConfigPackage.TABLE_CONFIG__COLUMN);
		}
		return column;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EReference, TableConfig> getReferenceToTableConfigMap() {
		if (referenceToTableConfigMap == null) {
			referenceToTableConfigMap = new EcoreEMap<EReference, TableConfig>(
					EditorConfigPackage.Literals.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY,
					EReferenceToTableConfigMapEntryImpl.class,
					this,
					EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP);
		}
		return referenceToTableConfigMap;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage getIntendedPackage() {
		if (intendedPackage != null && intendedPackage.eIsProxy()) {
			InternalEObject oldIntendedPackage = (InternalEObject) intendedPackage;
			intendedPackage = (EPackage) eResolveProxy(oldIntendedPackage);
			if (intendedPackage != oldIntendedPackage) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE,
							oldIntendedPackage, intendedPackage));
			}
		}
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EPackage basicGetIntendedPackage() {
		return intendedPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntendedPackage(EPackage newIntendedPackage) {
		EPackage oldIntendedPackage = intendedPackage;
		intendedPackage = newIntendedPackage;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE,
					oldIntendedPackage, intendedPackage));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntendedClass() {
		if (intendedClass != null && intendedClass.eIsProxy()) {
			InternalEObject oldIntendedClass = (InternalEObject) intendedClass;
			intendedClass = (EClass) eResolveProxy(oldIntendedClass);
			if (intendedClass != oldIntendedClass) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS,
							oldIntendedClass, intendedClass));
			}
		}
		return intendedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetIntendedClass() {
		return intendedClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIntendedClass(EClass newIntendedClass) {
		EClass oldIntendedClass = intendedClass;
		intendedClass = newIntendedClass;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS,
					oldIntendedClass, intendedClass));
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Intended Class</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type EClass
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let p:ecore::EPackage = self.intendedPackage
	in 
	if p.oclIsUndefined() then true
	else if p.eClassifiers->includes(trg) then true
	else p.eSubpackages.eClassifiers->includes(trg)
	endif endif
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalIntendedClassChoiceConstraint(EClass trg) {
		EClass eClass = EditorConfigPackage.Literals.TABLE_CONFIG;
		if (intendedClassChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = EditorConfigPackage.Literals.TABLE_CONFIG__INTENDED_CLASS;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				intendedClassChoiceConstraintOCL = helper
						.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, choiceConstraint, helper
								.getProblems(), eClass,
						"IntendedClassChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(intendedClassChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "IntendedClassChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * TODO Explain why this is overwritten. Can we not do it in OCL?
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public TableConfig getTableConfig(EReference eReference) {
		return getReferenceToTableConfigMap().get(eReference);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
			return ((InternalEList<?>) getColumn()).basicRemove(otherEnd, msgs);
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			return ((InternalEList<?>) getReferenceToTableConfigMap())
					.basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.TABLE_CONFIG__NAME:
			return getName();
		case EditorConfigPackage.TABLE_CONFIG__LABEL:
			return getLabel();
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			return isDisplayDefaultColumn() ? Boolean.TRUE : Boolean.FALSE;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER:
			return isDisplayHeader() ? Boolean.TRUE : Boolean.FALSE;
		case EditorConfigPackage.TABLE_CONFIG__FONT:
			return getFont();
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
			return getColumn();
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			if (coreType)
				return getReferenceToTableConfigMap();
			else
				return getReferenceToTableConfigMap().map();
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE:
			if (resolve)
				return getIntendedPackage();
			return basicGetIntendedPackage();
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS:
			if (resolve)
				return getIntendedClass();
			return basicGetIntendedClass();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.TABLE_CONFIG__NAME:
			setName((String) newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__LABEL:
			setLabel((String) newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			setDisplayDefaultColumn(((Boolean) newValue).booleanValue());
			return;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER:
			setDisplayHeader(((Boolean) newValue).booleanValue());
			return;
		case EditorConfigPackage.TABLE_CONFIG__FONT:
			setFont((URI) newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
			getColumn().clear();
			getColumn().addAll((Collection<? extends ColumnConfig>) newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			((EStructuralFeature.Setting) getReferenceToTableConfigMap())
					.set(newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE:
			setIntendedPackage((EPackage) newValue);
			return;
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS:
			setIntendedClass((EClass) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.TABLE_CONFIG__NAME:
			setName(NAME_EDEFAULT);
			return;
		case EditorConfigPackage.TABLE_CONFIG__LABEL:
			setLabel(LABEL_EDEFAULT);
			return;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			setDisplayDefaultColumn(DISPLAY_DEFAULT_COLUMN_EDEFAULT);
			return;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER:
			setDisplayHeader(DISPLAY_HEADER_EDEFAULT);
			return;
		case EditorConfigPackage.TABLE_CONFIG__FONT:
			setFont(FONT_EDEFAULT);
			return;
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
			getColumn().clear();
			return;
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			getReferenceToTableConfigMap().clear();
			return;
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE:
			setIntendedPackage((EPackage) null);
			return;
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS:
			setIntendedClass((EClass) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.TABLE_CONFIG__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT
					.equals(name);
		case EditorConfigPackage.TABLE_CONFIG__LABEL:
			return LABEL_EDEFAULT == null ? label != null : !LABEL_EDEFAULT
					.equals(label);
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_DEFAULT_COLUMN:
			return displayDefaultColumn != DISPLAY_DEFAULT_COLUMN_EDEFAULT;
		case EditorConfigPackage.TABLE_CONFIG__DISPLAY_HEADER:
			return displayHeader != DISPLAY_HEADER_EDEFAULT;
		case EditorConfigPackage.TABLE_CONFIG__FONT:
			return FONT_EDEFAULT == null ? font != null : !FONT_EDEFAULT
					.equals(font);
		case EditorConfigPackage.TABLE_CONFIG__COLUMN:
			return column != null && !column.isEmpty();
		case EditorConfigPackage.TABLE_CONFIG__REFERENCE_TO_TABLE_CONFIG_MAP:
			return referenceToTableConfigMap != null
					&& !referenceToTableConfigMap.isEmpty();
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_PACKAGE:
			return intendedPackage != null;
		case EditorConfigPackage.TABLE_CONFIG__INTENDED_CLASS:
			return intendedClass != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", label: ");
		result.append(label);
		result.append(", displayDefaultColumn: ");
		result.append(displayDefaultColumn);
		result.append(", displayHeader: ");
		result.append(displayHeader);
		result.append(", font: ");
		result.append(font);
		result.append(')');
		return result.toString();
	}

	/**
	 * Evaluates the label calculated by OCL 'label' annotation. <!--
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL name->asOrderedSet()
	 * @templateTag INS01
	 * @generated
	 */
	public String evalOclLabel() {
		EClass eClass = EditorConfigPackage.Literals.TABLE_CONFIG;
		if (labelOCL == null) {
			Helper helper = OCL_ENV.createOCLHelper();
			helper.setContext(eClass);
			EAnnotation ocl = eClass.getEAnnotation(OCL_ANNOTATION_SOURCE);
			String label = (String) ocl.getDetails().get("label");

			try {
				labelOCL = helper.createQuery(label);
			} catch (ParserException e) {
				return null;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, label, helper
								.getProblems(), eClass, "label");
			}
		}
		Query query = OCL_ENV.createQuery(labelOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "label");
			return XoclHelper.format(query.evaluate(this));
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return null;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}


} //TableConfigImpl
