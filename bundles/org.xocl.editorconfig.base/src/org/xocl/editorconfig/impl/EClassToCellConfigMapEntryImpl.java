/**
 * <copyright>
 * </copyright>
 *
 * $Id: EClassToCellConfigMapEntryImpl.java,v 1.11 2010/02/13 06:25:13 xocl.kutterxocl Exp $
 */
package org.xocl.editorconfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCL.Query;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.util.XoclEmfUtil;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.core.util.XoclLibrary.XoclEnvironmentFactory;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>EClass To Cell Config Map Entry</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl#getTypedKey <em>Key</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.EClassToCellConfigMapEntryImpl#getTypedValue <em>Value</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class EClassToCellConfigMapEntryImpl extends EObjectImpl implements
		BasicEMap.Entry<EClass, CellConfig> {
	/**
	 * The cached value of the '{@link #getTypedKey() <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey()
	 * @generated
	 * @ordered
	 */
	protected EClass key;

	/**
	 * The cached value of the '{@link #getTypedValue() <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedValue()
	 * @generated
	 * @ordered
	 */
	protected CellConfig value;

	/**
	 * The parsed OCL expression for the constraint of valid choices of '{@link #getTypedKey <em>Key</em>}' property.
	 * Is combined with the choice construction definition.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTypedKey
	 * @templateTag DFGFI03
	 * @generated
	 */
	private static OCLExpression keyChoiceConstraintOCL;

	/**
	 * The OCL environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI12
	 * @generated
	 */
	private static final OCL OCL_ENV = OCL
			.newInstance(new XoclEnvironmentFactory());

	/**
	 * Set OCL environment options.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag DFGFI13
	 * @generated
	 */
	static {
		ParsingOptions.setOption(OCL_ENV.getEnvironment(), ParsingOptions
				.implicitRootClass(OCL_ENV.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * Utility function to safely add a Variable in the global parsing environment.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param variableName the name of the variable to be added
	 * @param variableType the type of the variable to be added
	 * @templateTag DFGFI15
	 * @generated
	 */
	private static void addEnvironmentVariable(String variableName,
			EClassifier variableType) {
		OCL_ENV.getEnvironment().deleteElement(variableName);
		Variable trgVar = EcoreFactory.eINSTANCE.createVariable();
		trgVar.setName(variableName);
		trgVar.setType(variableType);
		OCL_ENV.getEnvironment().addElement(variableName, trgVar, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EClassToCellConfigMapEntryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypedKey() {
		if (key != null && key.eIsProxy()) {
			InternalEObject oldKey = (InternalEObject) key;
			key = (EClass) eResolveProxy(oldKey);
			if (key != oldKey) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY,
							oldKey, key));
			}
		}
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass basicGetTypedKey() {
		return key;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedKey(EClass newKey) {
		EClass oldKey = key;
		key = newKey;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY,
					oldKey, key));
	}

	/**
	 * Evaluates the OCL defined choice constraint for the '<em><b>Key</b></em>' reference.
	 * The constraint is applied in the context of the source of the reference, and the target of the reference being of type EClass
	 * Inside the constraint, the target can be accessed as 'trg'. 
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @OCL let cc:editorconfig::ColumnConfig = self.eContainer().oclAsType(editorconfig::ColumnConfig)
	in 
	let tc:editorconfig::TableConfig = cc.eContainer().oclAsType(editorconfig::TableConfig) in
	let  ic:ecore::EClass = tc.intendedClass in 
	let intended:Boolean = if not ic.oclIsUndefined()
	then   
	  trg=ic or trg.eAllSuperTypes->includes(ic)
	else
	let ip:ecore::EPackage = tc.intendedPackage in
	if ip.oclIsUndefined() then true 
	else 
	  if ip.eClassifiers->includes(trg) then true
	  else ip.eSubpackages.eClassifiers->includes(trg)
	  endif
	endif
	endif
	in
	let unique:Boolean = 
	self.key = trg or not (cc.cellConfigMap.key->includes(trg))
	in 
	intended and unique
	 * @templateTag GFI01
	 * @generated
	 */
	public boolean evalTypedKeyChoiceConstraint(EClass trg) {
		EClass eClass = EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY;
		if (keyChoiceConstraintOCL == null) {
			OCL.Helper helper = OCL_ENV.createOCLHelper();

			helper.setContext(eClass);

			//the class of the feature  TODO: is this the right one
			EReference eReference = EditorConfigPackage.Literals.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY;
			addEnvironmentVariable("trg", eReference.getEType());

			String choiceConstraint = XoclEmfUtil
					.findChoiceConstraintAnnotationText(eReference, eClass());

			try {
				keyChoiceConstraintOCL = helper.createQuery(choiceConstraint);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, choiceConstraint, helper
								.getProblems(), eClass,
						"TypedKeyChoiceConstraint");
			}
		}
		Query query = OCL_ENV.createQuery(keyChoiceConstraintOCL);
		try {
			XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, query,
					eClass, "TypedKeyChoiceConstraint");
			query.getEvaluationEnvironment().clear();
			query.getEvaluationEnvironment().add("trg", trg);
			return ((Boolean) query.evaluate(this)).booleanValue();
		} catch (Throwable e) {
			XoclErrorHandler.handleException(e);
			return false;
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellConfig getTypedValue() {
		if (value != null && value.eIsProxy()) {
			InternalEObject oldValue = (InternalEObject) value;
			value = (CellConfig) eResolveProxy(oldValue);
			if (value != oldValue) {
				InternalEObject newValue = (InternalEObject) value;
				NotificationChain msgs = oldValue
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
								null, null);
				if (newValue.eInternalContainer() == null) {
					msgs = newValue
							.eInverseAdd(
									this,
									EOPPOSITE_FEATURE_BASE
											- EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
									null, msgs);
				}
				if (msgs != null)
					msgs.dispatch();
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(
							this,
							Notification.RESOLVE,
							EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
							oldValue, value));
			}
		}
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellConfig basicGetTypedValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetTypedValue(CellConfig newValue,
			NotificationChain msgs) {
		CellConfig oldValue = value;
		value = newValue;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
					oldValue, newValue);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTypedValue(CellConfig newValue) {
		if (newValue != value) {
			NotificationChain msgs = null;
			if (value != null)
				msgs = ((InternalEObject) value)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
								null, msgs);
			if (newValue != null)
				msgs = ((InternalEObject) newValue)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
								null, msgs);
			msgs = basicSetTypedValue(newValue, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE,
					newValue, newValue));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			return basicSetTypedValue(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY:
			if (resolve)
				return getTypedKey();
			return basicGetTypedKey();
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			if (resolve)
				return getTypedValue();
			return basicGetTypedValue();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY:
			setTypedKey((EClass) newValue);
			return;
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			setTypedValue((CellConfig) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY:
			setTypedKey((EClass) null);
			return;
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			setTypedValue((CellConfig) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__KEY:
			return key != null;
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY__VALUE:
			return value != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected int hash = -1;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHash() {
		if (hash == -1) {
			Object theKey = getKey();
			hash = (theKey == null ? 0 : theKey.hashCode());
		}
		return hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHash(int hash) {
		this.hash = hash;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Setting intendedClass of TableConfig, if the field is not yet set.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EClass getKey() {
		EClass res = getTypedKey();
		if (res == null) {
			TableConfig tc = (TableConfig) eContainer().eContainer();
			EClass ic = tc.getIntendedClass();
			if (ic != null) {
				setKey(ic);
				res = ic;
			}
		}
		return res;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setKey(EClass key) {
		setTypedKey(key);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellConfig getValue() {
		return getTypedValue();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellConfig setValue(CellConfig value) {
		CellConfig oldValue = getValue();
		setTypedValue(value);
		return oldValue;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EMap<EClass, CellConfig> getEMap() {
		EObject container = eContainer();
		return container == null ? null : (EMap<EClass, CellConfig>) container
				.eGet(eContainmentFeature());
	}

} //EClassToCellConfigMapEntryImpl
