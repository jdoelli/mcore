/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfigFactoryImpl.java,v 1.7 2010/01/03 17:44:39 xocl.kutterxocl Exp $
 */
package org.xocl.editorconfig.impl;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EFactoryImpl;
import org.eclipse.emf.ecore.plugin.EcorePlugin;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.CellEditBehaviorOption;
import org.xocl.editorconfig.CellLocateBehaviorOption;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditProviderCell;
import org.xocl.editorconfig.EditorConfig;
import org.xocl.editorconfig.EditorConfigFactory;
import org.xocl.editorconfig.EditorConfigPackage;
import org.xocl.editorconfig.OclCell;
import org.xocl.editorconfig.RowFeatureCell;
import org.xocl.editorconfig.TableConfig;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class EditorConfigFactoryImpl extends EFactoryImpl implements
		EditorConfigFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static EditorConfigFactory init() {
		try {
			EditorConfigFactory theEditorConfigFactory = (EditorConfigFactory) EPackage.Registry.INSTANCE
					.getEFactory("http://www.xocl.org/EditorConfig");
			if (theEditorConfigFactory != null) {
				return theEditorConfigFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new EditorConfigFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case EditorConfigPackage.EDITOR_CONFIG:
			return createEditorConfig();
		case EditorConfigPackage.ECLASS_TO_TABLE_CONFIG_MAP_ENTRY:
			return (EObject) createEClassToTableConfigMapEntry();
		case EditorConfigPackage.EREFERENCE_TO_TABLE_CONFIG_MAP_ENTRY:
			return (EObject) createEReferenceToTableConfigMapEntry();
		case EditorConfigPackage.TABLE_CONFIG:
			return createTableConfig();
		case EditorConfigPackage.COLUMN_CONFIG:
			return createColumnConfig();
		case EditorConfigPackage.ECLASS_TO_CELL_CONFIG_MAP_ENTRY:
			return (EObject) createEClassToCellConfigMapEntry();
		case EditorConfigPackage.ROW_FEATURE_CELL:
			return createRowFeatureCell();
		case EditorConfigPackage.OCL_CELL:
			return createOclCell();
		case EditorConfigPackage.EDIT_PROVIDER_CELL:
			return createEditProviderCell();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName()
					+ "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case EditorConfigPackage.CELL_EDIT_BEHAVIOR_OPTION:
			return createCellEditBehaviorOptionFromString(eDataType,
					initialValue);
		case EditorConfigPackage.CELL_LOCATE_BEHAVIOR_OPTION:
			return createCellLocateBehaviorOptionFromString(eDataType,
					initialValue);
		case EditorConfigPackage.FONT:
			return createFontFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case EditorConfigPackage.CELL_EDIT_BEHAVIOR_OPTION:
			return convertCellEditBehaviorOptionToString(eDataType,
					instanceValue);
		case EditorConfigPackage.CELL_LOCATE_BEHAVIOR_OPTION:
			return convertCellLocateBehaviorOptionToString(eDataType,
					instanceValue);
		case EditorConfigPackage.FONT:
			return convertFontToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '"
					+ eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfig createEditorConfig() {
		EditorConfigImpl editorConfig = new EditorConfigImpl();
		return editorConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EClass, TableConfig> createEClassToTableConfigMapEntry() {
		EClassToTableConfigMapEntryImpl eClassToTableConfigMapEntry = new EClassToTableConfigMapEntryImpl();
		return eClassToTableConfigMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EReference, TableConfig> createEReferenceToTableConfigMapEntry() {
		EReferenceToTableConfigMapEntryImpl eReferenceToTableConfigMapEntry = new EReferenceToTableConfigMapEntryImpl();
		return eReferenceToTableConfigMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TableConfig createTableConfig() {
		TableConfigImpl tableConfig = new TableConfigImpl();
		return tableConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ColumnConfig createColumnConfig() {
		ColumnConfigImpl columnConfig = new ColumnConfigImpl();
		return columnConfig;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<EClass, CellConfig> createEClassToCellConfigMapEntry() {
		EClassToCellConfigMapEntryImpl eClassToCellConfigMapEntry = new EClassToCellConfigMapEntryImpl();
		return eClassToCellConfigMapEntry;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RowFeatureCell createRowFeatureCell() {
		RowFeatureCellImpl rowFeatureCell = new RowFeatureCellImpl();
		return rowFeatureCell;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OclCell createOclCell() {
		OclCellImpl oclCell = new OclCellImpl();
		return oclCell;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditProviderCell createEditProviderCell() {
		EditProviderCellImpl editProviderCell = new EditProviderCellImpl();
		return editProviderCell;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellEditBehaviorOption createCellEditBehaviorOptionFromString(
			EDataType eDataType, String initialValue) {
		CellEditBehaviorOption result = CellEditBehaviorOption
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCellEditBehaviorOptionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CellLocateBehaviorOption createCellLocateBehaviorOptionFromString(
			EDataType eDataType, String initialValue) {
		CellLocateBehaviorOption result = CellLocateBehaviorOption
				.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException("The value '" + initialValue
					+ "' is not a valid enumerator of '" + eDataType.getName()
					+ "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCellLocateBehaviorOptionToString(EDataType eDataType,
			Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI createFontFromString(EDataType eDataType, String initialValue) {
		return URI.createURI(initialValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertFontToString(EDataType eDataType, Object instanceValue) {
		return super.convertToString(eDataType, instanceValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EditorConfigPackage getEditorConfigPackage() {
		return (EditorConfigPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static EditorConfigPackage getPackage() {
		return EditorConfigPackage.eINSTANCE;
	}

} //EditorConfigFactoryImpl
