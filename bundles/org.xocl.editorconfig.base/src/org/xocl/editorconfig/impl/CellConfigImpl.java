/**
 * <copyright>
 * </copyright>
 *
 * $Id: CellConfigImpl.java,v 1.2 2010/12/08 04:13:19 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.ocl.EvaluationEnvironment;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.OCLExpression;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.ocl.options.ParsingOptions;
import org.xocl.core.expr.OCLExpressionContext;
import org.xocl.core.util.XoclErrorHandler;
import org.xocl.editorconfig.CellConfig;
import org.xocl.editorconfig.ColumnConfig;
import org.xocl.editorconfig.EditorConfigPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cell Config</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.impl.CellConfigImpl#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.impl.CellConfigImpl#getColor <em>Color</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class CellConfigImpl extends EObjectImpl implements CellConfig {
	/**
	 * The default value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected static final URI FONT_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFont() <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFont()
	 * @generated
	 * @ordered
	 */
	protected URI font = FONT_EDEFAULT;

	/**
	 * The default value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected static final String COLOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getColor() <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getColor()
	 * @generated
	 * @ordered
	 */
	protected String color = COLOR_EDEFAULT;

	/**
	 * OCL environment that evaluates cell expression
	 */
	private static OCL oclEnv = OCL.newInstance();

	/**
	 * Parsed color expression
	 */
	private OCLExpression colorExpression;

	/**
	 * Set OCL environment options.
	 */
	static {
		ParsingOptions.setOption(oclEnv.getEnvironment(), ParsingOptions
				.implicitRootClass(oclEnv.getEnvironment()),
				EcorePackage.eINSTANCE.getEObject());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CellConfigImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return EditorConfigPackage.Literals.CELL_CONFIG;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public URI getFont() {
		return font;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFont(URI newFont) {
		URI oldFont = font;
		font = newFont;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.CELL_CONFIG__FONT, oldFont, font));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getColor() {
		return color;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setColor(String newColor) {
		String oldColor = color;
		color = newColor;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					EditorConfigPackage.CELL_CONFIG__COLOR, oldColor, color));
	}

	/**
	 * Returns the OCL expression context for the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @templateTag GFI03
	 * @generated NOT
	 */
	public OCLExpressionContext getColorOCLExpressionContext() {
		return new OCLExpressionContext(
				((EClassToCellConfigMapEntryImpl) eContainer()).getKey(), null,
				new EClassifier[] { OCLExpressionContext
						.getOCLStandardLibrary().getBoolean() });
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Object getCellValue(EObject row) {

		// TODO: implement this method (no OCL found!)
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean getColorValue(EObject row) {
		if (colorExpression == null) {
			String expressionText = getColor();
			if (expressionText == null) {
				return false;
			}
			OCL.Helper oclHelper = oclEnv.createOCLHelper();
			EClass eContext = ((EClassToCellConfigMapEntryImpl) eContainer())
					.getKey();
			oclHelper.setContext(eContext);
			Variable rowVar = EcoreFactory.eINSTANCE.createVariable();
			rowVar.setName("row"); //$NON-NLS-1$
			rowVar.setType(eContext);
			// add it to the global OCL environment
			oclEnv.getEnvironment().deleteElement(rowVar.getName());
			oclEnv.getEnvironment().addElement(rowVar.getName(), rowVar, true);
			try {
				colorExpression = oclHelper.createQuery(expressionText);
			} catch (ParserException e) {
				return false;
			} finally {
				XoclErrorHandler.handleQueryProblems(
						EditorConfigPackage.PLUGIN_ID, expressionText,
						oclHelper.getProblems(), eClass(), eClass()
								.getEAllOperations().get(0)); /* TODO: remove explicit operation index*/
				oclEnv.getEnvironment().deleteElement(rowVar.getName());
			}
		}
		OCL.Query oclQuery = oclEnv.createQuery(colorExpression);
		XoclErrorHandler.enterContext(EditorConfigPackage.PLUGIN_ID, oclQuery,
				EditorConfigPackage.Literals.OCL_CELL, "getColorValue"); //$NON-NLS-1$
		try {
			EvaluationEnvironment<EClassifier, ?, ?, EClass, EObject> evalEnv = oclQuery
					.getEvaluationEnvironment();
			evalEnv.clear();
			evalEnv.add("row", row); //$NON-NLS-1$
			Object result = oclQuery.evaluate(row);
			return result.equals(true);
		} finally {
			XoclErrorHandler.leaveContext();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public URI getCellFont() {
		URI font = getFont();
		if (font != null) {
			return font;
		}
		return ((ColumnConfig) ((EClassToCellConfigMapEntryImpl) eContainer()).eContainer()).getColumnFont();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case EditorConfigPackage.CELL_CONFIG__FONT:
			return getFont();
		case EditorConfigPackage.CELL_CONFIG__COLOR:
			return getColor();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case EditorConfigPackage.CELL_CONFIG__FONT:
			setFont((URI) newValue);
			return;
		case EditorConfigPackage.CELL_CONFIG__COLOR:
			setColor((String) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.CELL_CONFIG__FONT:
			setFont(FONT_EDEFAULT);
			return;
		case EditorConfigPackage.CELL_CONFIG__COLOR:
			setColor(COLOR_EDEFAULT);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case EditorConfigPackage.CELL_CONFIG__FONT:
			return FONT_EDEFAULT == null ? font != null : !FONT_EDEFAULT
					.equals(font);
		case EditorConfigPackage.CELL_CONFIG__COLOR:
			return COLOR_EDEFAULT == null ? color != null : !COLOR_EDEFAULT
					.equals(color);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (font: ");
		result.append(font);
		result.append(", color: ");
		result.append(color);
		result.append(')');
		return result.toString();
	}

} //CellConfigImpl
