/**
 * <copyright>
 * </copyright>
 *
 * $Id: RowFeatureCell.java,v 1.3 2009/07/10 07:51:06 xocl.kutterxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.ecore.EStructuralFeature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Row Feature Cell</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.RowFeatureCell#getFeature <em>Feature</em>}</li>
 *   <li>{@link org.xocl.editorconfig.RowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}</li>
 *   <li>{@link org.xocl.editorconfig.RowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getRowFeatureCell()
 * @model
 * @generated
 */
public interface RowFeatureCell extends CellConfig {
	/**
	 * Returns the value of the '<em><b>Feature</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Feature</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Feature</em>' reference.
	 * @see #setFeature(EStructuralFeature)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getRowFeatureCell_Feature()
	 * @model required="true"
	 *        annotation="http://www.xocl.org/OCL choiceConstruction='eContainer().oclAsType(EClassToCellConfigMapEntry).key.eAllStructuralFeatures' choiceConstraint='if trg.oclIsTypeOf(ecore::EReference) then\r\n  not trg.oclAsType(ecore::EReference).containment\r\nelse \r\n  true\r\nendif '"
	 * @generated
	 */
	EStructuralFeature getFeature();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.RowFeatureCell#getFeature <em>Feature</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Feature</em>' reference.
	 * @see #getFeature()
	 * @generated
	 */
	void setFeature(EStructuralFeature value);

	/**
	 * Returns the value of the '<em><b>Cell Edit Behavior</b></em>' attribute.
	 * The default value is <code>"Selection"</code>.
	 * The literals are from the enumeration {@link org.xocl.editorconfig.CellEditBehaviorOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Edit Behavior</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Edit Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellEditBehaviorOption
	 * @see #setCellEditBehavior(CellEditBehaviorOption)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getRowFeatureCell_CellEditBehavior()
	 * @model default="Selection"
	 * @generated
	 */
	CellEditBehaviorOption getCellEditBehavior();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.RowFeatureCell#getCellEditBehavior <em>Cell Edit Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cell Edit Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellEditBehaviorOption
	 * @see #getCellEditBehavior()
	 * @generated
	 */
	void setCellEditBehavior(CellEditBehaviorOption value);

	/**
	 * Returns the value of the '<em><b>Cell Locate Behavior</b></em>' attribute.
	 * The default value is <code>"Selection"</code>.
	 * The literals are from the enumeration {@link org.xocl.editorconfig.CellLocateBehaviorOption}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cell Locate Behavior</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @see #setCellLocateBehavior(CellLocateBehaviorOption)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getRowFeatureCell_CellLocateBehavior()
	 * @model default="Selection"
	 * @generated
	 */
	CellLocateBehaviorOption getCellLocateBehavior();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.RowFeatureCell#getCellLocateBehavior <em>Cell Locate Behavior</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cell Locate Behavior</em>' attribute.
	 * @see org.xocl.editorconfig.CellLocateBehaviorOption
	 * @see #getCellLocateBehavior()
	 * @generated
	 */
	void setCellLocateBehavior(CellLocateBehaviorOption value);

} // RowFeatureCell
