/**
 * <copyright>
 * </copyright>
 *
 * $Id: TableConfig.java,v 1.12 2010/11/26 04:18:58 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Table Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getName <em>Name</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getLabel <em>Label</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#isDisplayDefaultColumn <em>Display Default Column</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#isDisplayHeader <em>Display Header</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getColumn <em>Column</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getReferenceToTableConfigMap <em>Reference To Table Config Map</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getIntendedPackage <em>Intended Package</em>}</li>
 *   <li>{@link org.xocl.editorconfig.TableConfig#getIntendedClass <em>Intended Class</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig()
 * @model annotation="http://www.xocl.org/OCL label='name->asOrderedSet()'"
 * @generated
 */
public interface TableConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_Name()
	 * @model required="true"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Label</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Label</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Label</em>' attribute.
	 * @see #setLabel(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_Label()
	 * @model
	 * @generated
	 */
	String getLabel();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#getLabel <em>Label</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Label</em>' attribute.
	 * @see #getLabel()
	 * @generated
	 */
	void setLabel(String value);

	/**
	 * Returns the value of the '<em><b>Display Default Column</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Default Column</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Default Column</em>' attribute.
	 * @see #setDisplayDefaultColumn(boolean)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_DisplayDefaultColumn()
	 * @model default="false"
	 * @generated
	 */
	boolean isDisplayDefaultColumn();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#isDisplayDefaultColumn <em>Display Default Column</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Default Column</em>' attribute.
	 * @see #isDisplayDefaultColumn()
	 * @generated
	 */
	void setDisplayDefaultColumn(boolean value);

	/**
	 * Returns the value of the '<em><b>Display Header</b></em>' attribute.
	 * The default value is <code>"true"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Display Header</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Display Header</em>' attribute.
	 * @see #setDisplayHeader(boolean)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_DisplayHeader()
	 * @model default="true"
	 * @generated
	 */
	boolean isDisplayHeader();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#isDisplayHeader <em>Display Header</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Display Header</em>' attribute.
	 * @see #isDisplayHeader()
	 * @generated
	 */
	void setDisplayHeader(boolean value);

	/**
	 * Returns the value of the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Font</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font</em>' attribute.
	 * @see #setFont(URI)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_Font()
	 * @model dataType="org.xocl.editorconfig.Font"
	 * @generated
	 */
	URI getFont();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#getFont <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font</em>' attribute.
	 * @see #getFont()
	 * @generated
	 */
	void setFont(URI value);

	/**
	 * Returns the value of the '<em><b>Column</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.editorconfig.ColumnConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Column</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Column</em>' containment reference list.
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_Column()
	 * @model containment="true" resolveProxies="true"
	 * @generated
	 */
	EList<ColumnConfig> getColumn();

	/**
	 * Returns the value of the '<em><b>Reference To Table Config Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EReference},
	 * and the value is of type {@link org.xocl.editorconfig.TableConfig},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reference To Table Config Map</em>' map.
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_ReferenceToTableConfigMap()
	 * @model mapType="org.xocl.editorconfig.EReferenceToTableConfigMapEntry<org.eclipse.emf.ecore.EReference, org.xocl.editorconfig.TableConfig>"
	 * @generated
	 */
	EMap<EReference, TableConfig> getReferenceToTableConfigMap();

	/**
	 * Returns the value of the '<em><b>Intended Package</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Package</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intended Package</em>' reference.
	 * @see #setIntendedPackage(EPackage)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_IntendedPackage()
	 * @model
	 * @generated
	 */
	EPackage getIntendedPackage();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#getIntendedPackage <em>Intended Package</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intended Package</em>' reference.
	 * @see #getIntendedPackage()
	 * @generated
	 */
	void setIntendedPackage(EPackage value);

	/**
	 * Returns the value of the '<em><b>Intended Class</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Intended Class</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Intended Class</em>' reference.
	 * @see #setIntendedClass(EClass)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getTableConfig_IntendedClass()
	 * @model annotation="http://www.xocl.org/OCL choiceConstraint='let p:ecore::EPackage = self.intendedPackage\r\nin \r\nif p.oclIsUndefined() then true\r\nelse if p.eClassifiers->includes(trg) then true\r\nelse p.eSubpackages.eClassifiers->includes(trg)\r\nendif endif'"
	 * @generated
	 */
	EClass getIntendedClass();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.TableConfig#getIntendedClass <em>Intended Class</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Intended Class</em>' reference.
	 * @see #getIntendedClass()
	 * @generated
	 */
	void setIntendedClass(EClass value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model eReferenceRequired="true"
	 * @generated
	 */
	TableConfig getTableConfig(EReference eReference);

} // TableConfig
