/**
 * <copyright>
 * </copyright>
 *
 * $Id: CellConfig.java,v 1.2 2010/11/30 08:04:08 xocl.igdalovxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cell Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.CellConfig#getFont <em>Font</em>}</li>
 *   <li>{@link org.xocl.editorconfig.CellConfig#getColor <em>Color</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getCellConfig()
 * @model abstract="true"
 * @generated
 */
public interface CellConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Font</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Font</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Font</em>' attribute.
	 * @see #setFont(URI)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getCellConfig_Font()
	 * @model dataType="org.xocl.editorconfig.Font"
	 * @generated
	 */
	URI getFont();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.CellConfig#getFont <em>Font</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Font</em>' attribute.
	 * @see #getFont()
	 * @generated
	 */
	void setFont(URI value);

	/**
	 * Returns the value of the '<em><b>Color</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Color</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Color</em>' attribute.
	 * @see #setColor(String)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getCellConfig_Color()
	 * @model
	 * @generated
	 */
	String getColor();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.CellConfig#getColor <em>Color</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Color</em>' attribute.
	 * @see #getColor()
	 * @generated
	 */
	void setColor(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rowRequired="true"
	 * @generated
	 */
	Object getCellValue(EObject row);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model rowRequired="true"
	 * @generated
	 */
	boolean getColorValue(EObject row);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" dataType="org.xocl.editorconfig.Font"
	 * @generated
	 */
	URI getCellFont();

} // CellConfig
