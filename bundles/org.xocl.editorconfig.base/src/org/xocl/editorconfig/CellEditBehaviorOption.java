/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package org.xocl.editorconfig;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Cell Edit Behavior Option</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see org.xocl.editorconfig.EditorConfigPackage#getCellEditBehaviorOption()
 * @model
 * @generated
 */
public enum CellEditBehaviorOption implements Enumerator {
	/**
	 * The '<em><b>Selection</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SELECTION_VALUE
	 * @generated
	 * @ordered
	 */
	SELECTION(1, "Selection", "Selection"),

	/**
	 * The '<em><b>Pull Down</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PULL_DOWN_VALUE
	 * @generated
	 * @ordered
	 */
	PULL_DOWN(2, "PullDown", "PullDown"), /**
											 * The '<em><b>Read Only</b></em>' literal object.
											 * <!-- begin-user-doc -->
											 * <!-- end-user-doc -->
											 * @see #READ_ONLY_VALUE
											 * @generated
											 * @ordered
											 */
	READ_ONLY(3, "ReadOnly", "ReadOnly");

	/**
	 * The '<em><b>Selection</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Selection</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SELECTION
	 * @model name="Selection"
	 * @generated
	 * @ordered
	 */
	public static final int SELECTION_VALUE = 1;

	/**
	 * The '<em><b>Pull Down</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Pull Down</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PULL_DOWN
	 * @model name="PullDown"
	 * @generated
	 * @ordered
	 */
	public static final int PULL_DOWN_VALUE = 2;

	/**
	 * The '<em><b>Read Only</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>Read Only</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #READ_ONLY
	 * @model name="ReadOnly"
	 * @generated
	 * @ordered
	 */
	public static final int READ_ONLY_VALUE = 3;

	/**
	 * An array of all the '<em><b>Cell Edit Behavior Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final CellEditBehaviorOption[] VALUES_ARRAY = new CellEditBehaviorOption[] {
			SELECTION, PULL_DOWN, READ_ONLY, };

	/**
	 * A public read-only list of all the '<em><b>Cell Edit Behavior Option</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<CellEditBehaviorOption> VALUES = Collections
			.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Cell Edit Behavior Option</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CellEditBehaviorOption get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CellEditBehaviorOption result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cell Edit Behavior Option</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CellEditBehaviorOption getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			CellEditBehaviorOption result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Cell Edit Behavior Option</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CellEditBehaviorOption get(int value) {
		switch (value) {
		case SELECTION_VALUE:
			return SELECTION;
		case PULL_DOWN_VALUE:
			return PULL_DOWN;
		case READ_ONLY_VALUE:
			return READ_ONLY;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private CellEditBehaviorOption(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
		return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
		return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}

} //CellEditBehaviorOption
