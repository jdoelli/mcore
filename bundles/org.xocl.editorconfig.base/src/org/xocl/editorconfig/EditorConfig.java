/**
 * <copyright>
 * </copyright>
 *
 * $Id: EditorConfig.java,v 1.10 2009/07/21 06:58:05 xocl.stepanovxocl Exp $
 */
package org.xocl.editorconfig;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Editor Config</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xocl.editorconfig.EditorConfig#getTableConfig <em>Table Config</em>}</li>
 *   <li>{@link org.xocl.editorconfig.EditorConfig#getHeaderTableConfig <em>Header Table Config</em>}</li>
 *   <li>{@link org.xocl.editorconfig.EditorConfig#getClassToTableConfigMap <em>Class To Table Config Map</em>}</li>
 *   <li>{@link org.xocl.editorconfig.EditorConfig#getSuperConfig <em>Super Config</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xocl.editorconfig.EditorConfigPackage#getEditorConfig()
 * @model
 * @generated
 */
public interface EditorConfig extends EObject {
	/**
	 * Returns the value of the '<em><b>Table Config</b></em>' containment reference list.
	 * The list contents are of type {@link org.xocl.editorconfig.TableConfig}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Table Config</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table Config</em>' containment reference list.
	 * @see org.xocl.editorconfig.EditorConfigPackage#getEditorConfig_TableConfig()
	 * @model containment="true" resolveProxies="true" keys="name"
	 * @generated
	 */
	EList<TableConfig> getTableConfig();

	/**
	 * Returns the value of the '<em><b>Header Table Config</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Header Table Config</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Header Table Config</em>' reference.
	 * @see #setHeaderTableConfig(TableConfig)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getEditorConfig_HeaderTableConfig()
	 * @model
	 * @generated
	 */
	TableConfig getHeaderTableConfig();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.EditorConfig#getHeaderTableConfig <em>Header Table Config</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Header Table Config</em>' reference.
	 * @see #getHeaderTableConfig()
	 * @generated
	 */
	void setHeaderTableConfig(TableConfig value);

	/**
	 * Returns the value of the '<em><b>Class To Table Config Map</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EClass},
	 * and the value is of type {@link org.xocl.editorconfig.TableConfig},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Class To Table Config Map</em>' map isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Class To Table Config Map</em>' map.
	 * @see org.xocl.editorconfig.EditorConfigPackage#getEditorConfig_ClassToTableConfigMap()
	 * @model mapType="org.xocl.editorconfig.EClassToTableConfigMapEntry<org.eclipse.emf.ecore.EClass, org.xocl.editorconfig.TableConfig>"
	 * @generated
	 */
	EMap<EClass, TableConfig> getClassToTableConfigMap();

	/**
	 * Returns the value of the '<em><b>Super Config</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Super Config</em>' reference.
	 * @see #setSuperConfig(EditorConfig)
	 * @see org.xocl.editorconfig.EditorConfigPackage#getEditorConfig_SuperConfig()
	 * @model annotation="http://www.xocl.org/mdcm/v4 many\075true=''"
	 * @generated
	 */
	EditorConfig getSuperConfig();

	/**
	 * Sets the value of the '{@link org.xocl.editorconfig.EditorConfig#getSuperConfig <em>Super Config</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Super Config</em>' reference.
	 * @see #getSuperConfig()
	 * @generated
	 */
	void setSuperConfig(EditorConfig value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model eClassRequired="true"
	 * @generated
	 */
	TableConfig getTableConfig(EClass eClass);

} // EditorConfig
