/**
 * <copyright>
 *
 * Copyright (c) 2008-2018 Montages AG.
 * All rights reserved.   
 * </copyright>
 * OCL support, www.xocl.org
 */

package org.xocl.common.ui.widgets;

import org.eclipse.emf.common.util.BasicDiagnostic;
import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.resource.FontDescriptor;
import org.eclipse.jface.text.ITextOperationTarget;
import org.eclipse.jface.text.source.ISourceViewer;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.ecore.OCL;
import org.eclipse.ocl.ecore.Variable;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyAdapter;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.widgets.Composite;
import org.xocl.common.ui.ocl.engine.IOCLFactory;
import org.xocl.common.ui.ocl.engine.TargetMetamodel;
import org.xocl.core.expr.OCLExpressionUtils;
import org.xocl.core.util.DelegatingPackageRegistry;
import org.xocl.core.util.XoclLibrary;

/**
 * @author Max Stepanov
 *
 */
public class OCLExpressionWidget extends
		org.xocl.common.ui.ocl.ui.widget.OCLExpressionWidget {

	private Variable[] variables;
	private EClassifier[] requiredResultTypes;
	
	private final IOCLFactory<Object> ocl4EcoreFactory = new EcoreOCLFactory();
	
	private FontDescriptor fontDescriptor;
	private Font font;
	/**
	 * @param parent
	 */
	public OCLExpressionWidget(Composite parent) {
		super(parent);
		fontDescriptor = FontDescriptor.createFrom(parent.getFont()).increaseHeight(2);
		font = fontDescriptor.createFont(parent.getDisplay());
		applyTextWidgetFont();
		
		getViewer().getTextWidget().addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				switch (e.keyCode) {
				case ' ':
					if (isControlPressed(e)) {
						// undo the last space character
						getViewer().doOperation(ITextOperationTarget.UNDO);
						getViewer().doOperation(ISourceViewer.CONTENTASSIST_PROPOSALS);
					}
					break;
				case SWT.ARROW_UP:
				case SWT.ARROW_DOWN:
					if (isControlPressed(e) && isAltPressed(e)) {
						changeFontSize(e.keyCode == SWT.ARROW_UP ? +1 : -1);
						e.doit = false;
					}
					break;
				}
			}
			
			private boolean isControlPressed(KeyEvent e) {
				return (e.stateMask & SWT.CTRL) == SWT.CTRL;
			}

			private boolean isAltPressed(KeyEvent e) {
				return (e.stateMask & SWT.ALT) == SWT.ALT;
			}
		});
	}
	
	private void applyTextWidgetFont() {
		getViewer().getTextWidget().setFont(font);
	}
	
	private void changeFontSize(int delta) {
		FontDescriptor oldDescriptor = fontDescriptor;
		Font oldFont = font;
		fontDescriptor = oldDescriptor.increaseHeight(delta);
		font = fontDescriptor.createFont(getDisplay());
		applyTextWidgetFont();
		oldDescriptor.destroyFont(oldFont);
	}
	
	/* (non-Javadoc)
	 * @see internal.org.eclipse.emf.search.ocl.ui.widget.OCLExpressionWidget#setContext(org.eclipse.emf.ecore.EObject)
	 */
	@Override
	public void setContext(EObject ctx) {
		if (ctx == null) {
			EClass eClass = EcoreFactory.eINSTANCE.createEClass();
			eClass.setName("EObject");
			ctx = eClass;
		}
		super.setContext(ctx);
	}

	/**
	 * @return the variables
	 */
	public Variable[] getVariables() {
		return variables;
	}

	/**
	 * @param variables the variables to set
	 */
	public void setVariables(Variable[] variables) {
		this.variables = variables;
	}

	/**
	 * @return the requiredResultType
	 */
	public EClassifier[] getResultTypes() {
		return requiredResultTypes;
	}

	/**
	 * @param requiredResultType the requiredResultType to set
	 */
	public void setResultTypes(EClassifier[] resultTypes) {
		this.requiredResultTypes = resultTypes;
	}

	/**
	 * Returns parser error message or null
	 */
	public Diagnostic validateExpression() {
		BasicDiagnostic diagnostic = new BasicDiagnostic();
		try {
			EClassifier resultType = (EClassifier) super.evaluateExpression(getExpression(), null);
			if ((requiredResultTypes == null || OCLExpressionUtils.checkTypes(requiredResultTypes, resultType, diagnostic))
					&& diagnostic.getSeverity() == Diagnostic.OK) {
				return BasicDiagnostic.OK_INSTANCE;
			}
		} catch (ParserException e) {
			return BasicDiagnostic.toDiagnostic(e);
		}
		return diagnostic;
	}

	/* (non-Javadoc)
	 * @see internal.org.eclipse.emf.search.ocl.ui.widget.OCLExpressionWidget#createOCL()
	 */
	@Override
	protected org.eclipse.ocl.OCL<?, Object, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL() {
		if (getTargetMetamodel() == TargetMetamodel.Ecore) {
			return ocl4EcoreFactory.createOCL();
		}
		return super.createOCL();
	}

	/* (non-Javadoc)
	 * @see internal.org.eclipse.emf.search.ocl.ui.widget.OCLExpressionWidget#getOCLFactory()
	 */
	@Override
	protected IOCLFactory<Object> getOCLFactory() {
		if (getTargetMetamodel() == TargetMetamodel.Ecore) {
			return ocl4EcoreFactory;
		}
		return super.getOCLFactory();
	}

	private class EcoreOCLFactory implements IOCLFactory<Object> {
		public TargetMetamodel getTargetMetamodel() {
			return getTargetMetamodel();
		}

		@SuppressWarnings("unchecked")
		public org.eclipse.ocl.OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL() {
			EObject eObject = getContext();
			Resource resource = eObject != null ? eObject.eResource() : null;
			ResourceSet resourceSet = resource != null ? resource.getResourceSet() : null;
			return OCLExpressionUtils.configureOCL(OCL.newInstance(
					resourceSet != null ? new XoclLibrary.XoclEnvironmentFactory(
							OCLExpressionUtils.createPackageRegistry(EcoreEnvironmentFactory.INSTANCE.getEPackageRegistry(), resourceSet)
	        			) : new XoclLibrary.XoclEnvironmentFactory()
					), variables);
		}

		@SuppressWarnings("unchecked")
		public org.eclipse.ocl.OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL(Resource res) {
			return OCLExpressionUtils.configureOCL(OCL.newInstance(new XoclLibrary.XoclEnvironmentFactory(
					new DelegatingPackageRegistry(getContext().eResource()
							.getResourceSet().getPackageRegistry(),
							EPackage.Registry.INSTANCE)), res), variables);
		}

		public Object getContextClassifier(EObject object) {
			return getContext();
		}

		public String getName(Object modelElement) {
			return ((ENamedElement) modelElement).getName();
		}
	}
	
}
