/*******************************************************************************
 * Copyright (c) 2007, 2008 Anyware Technologies and others. All rights reserved. This program
 * and the accompanying materials are made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution, and is
 * available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * OCLExpressionWidget.java
 * 
 * Contributors:
 *		Lucas Bigeardel (Anyware Technologies) - initial API and implementation
 *		Lucas Bigeardel - layouts
 ******************************************************************************/

package org.xocl.common.ui.ocl.ui.widget;

import org.eclipse.emf.ecore.ENamedElement;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.ocl.AbstractEnvironmentFactory;
import org.eclipse.ocl.OCL;
import org.eclipse.ocl.ParserException;
import org.eclipse.ocl.Query;
import org.eclipse.ocl.ecore.EcoreEnvironmentFactory;
import org.eclipse.ocl.expressions.OCLExpression;
import org.eclipse.ocl.helper.ConstraintKind;
import org.eclipse.ocl.helper.OCLHelper;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.xocl.common.ui.ocl.engine.IOCLFactory;
import org.xocl.common.ui.ocl.engine.ModelingLevel;
import org.xocl.common.ui.ocl.engine.TargetMetamodel;
import org.xocl.common.ui.ocl.ui.viewer.ColorManager;
import org.xocl.common.ui.ocl.ui.viewer.OCLDocument;
import org.xocl.common.ui.ocl.ui.viewer.OCLSourceViewer;
import org.xocl.core.util.DelegatingPackageRegistry;

public class OCLExpressionWidget extends Composite {
	private final OCLSourceViewer viewer;
	private final OCLDocument document;
	private EObject context;
	private TargetMetamodel targetMetaModel = TargetMetamodel.Ecore;
	private AbstractEnvironmentFactory<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> environmentFactory;

	private final IOCLFactory<Object> ocl4EcoreFactory = new EcoreOCLFactory();

	private ModelingLevel modelingLevel = ModelingLevel.M2;

	public TargetMetamodel getTargetMetamodel() {
		return targetMetaModel;
	}

	public void setTargetMetamodel(TargetMetamodel metamodel) {
		targetMetaModel = metamodel;
		document.setOCLFactory(getOCLFactory());
	}

	public ModelingLevel getModelingLevel() {
		return modelingLevel;
	}

	public void setModelingLevel(ModelingLevel level) {
		modelingLevel = level;
	}

	public EObject getContext() {
		return context;
	}

	public void setContext(EObject ctx) {
		context = ctx;
		document.setOCLContext(ctx);
	}
	
	public AbstractEnvironmentFactory<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> getEnvironmentFactory() {
		return environmentFactory;
	}

	public void setEnvironmentFactory(
			AbstractEnvironmentFactory<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> envFactory) {
		environmentFactory = envFactory;
	}

	/**
	 * A key listener that listens for the Enter key to evaluate the OCL
	 * expression.
	 */
	private class InputKeyListener implements KeyListener {

		public void keyPressed(KeyEvent e) {
			switch (e.keyCode) {
			case SWT.CR:
				if (!viewer.isContentAssistActive() && (e.stateMask & (SWT.CTRL | SWT.SHIFT)) == 0) {
					String text = document.get();
					evaluate(text);
				}
				break;
			}
		}

		public void keyReleased(KeyEvent e) {
			switch (e.keyCode) {
			case ' ':
				if ((e.stateMask & SWT.CTRL) == SWT.CTRL) {
					viewer.getContentAssistant().showPossibleCompletions();
				}
			}
		}
	}

	private class EcoreOCLFactory implements IOCLFactory<Object> {
		public TargetMetamodel getTargetMetamodel() {
			return targetMetaModel;
		}

		@SuppressWarnings("unchecked")
		public OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL() {
			return OCL.newInstance(new EcoreEnvironmentFactory());
		}

		@SuppressWarnings("unchecked")
		public OCL<?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL(Resource res) {
			return OCL.newInstance(new EcoreEnvironmentFactory(
					new DelegatingPackageRegistry(context.eResource()
							.getResourceSet().getPackageRegistry(),
							EPackage.Registry.INSTANCE)), res);
		}

		public Object getContextClassifier(EObject object) {
			return context.eClass();
		}

		public String getName(Object modelElement) {
			return ((ENamedElement) modelElement).getName();
		}
	}

	protected OCL<?, Object, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> createOCL() {
		return ocl4EcoreFactory.createOCL();
	}

	protected IOCLFactory<Object> getOCLFactory() {
		return ocl4EcoreFactory;
	}

	/**
	 * Evaluates an OCL expression using the OCL Interpreter's {@link OCLHelper}
	 * API.
	 * 
	 * @param expression
	 *            an OCL expression
	 * 
	 * @return <code>true</code> on successful evaluation; <code>false</code>
	 *         if the expression failed to parse or evaluate
	 */
	boolean evaluate(String expression) {
		boolean result = true;

		if (context == null) {
			result = false;
		} else {
			try {
				evaluateExpression(expression, context);
			} catch (Exception e) {
				result = false;
			}
		}
		return result;
	}
	
	/**
	 * 
	 * @param expression
	 * @return
	 * @throws ParserException
	 */
	protected Object evaluateExpression(String expression, Object object) throws ParserException {
		// create an OCL helper to do our parsing and evaluating
		OCL<?, Object, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?> ocl = createOCL();
		OCLHelper<Object, ?, ?, ?> helper = ocl.createOCLHelper();

		// set our helper's context classifier to parse against it
		ConstraintKind kind = modelingLevel.setContext(helper, context,getOCLFactory());

		switch (modelingLevel) {
			case M2:
				OCLExpression<Object> parsed = helper.createQuery(expression);
				Query<Object, ?, ?> query = ocl.createQuery(parsed);
				if (object != null) {
					return query.evaluate(object);
				} else {
					return query.resultType();
				}
			case M1:
				helper.createConstraint(kind, expression);
				break;
		}
		return null;
	}
	
	public OCLExpressionWidget(Composite parent) {
		super(parent, SWT.NONE);

		setLayout(GridLayoutFactory.fillDefaults().create());
		viewer = new OCLSourceViewer(this, new ColorManager(), SWT.BORDER | SWT.MULTI | SWT.V_SCROLL | SWT.H_SCROLL);

		document = new OCLDocument();
		document.setModelingLevel(modelingLevel);
		viewer.setDocument(document);

		viewer.getTextWidget().addKeyListener(new InputKeyListener());

		viewer.getTextWidget().setLayout(new GridLayout(1, false));

		GridData gd1 = new GridData(GridData.FILL_BOTH);
		gd1.heightHint = gd1.minimumHeight = 150;
		viewer.getTextWidget().setLayoutData(gd1);
		
		GridData gd2 = new GridData(GridData.FILL_BOTH);
		gd2.heightHint = gd2.minimumHeight = 150;

		viewer.getControl().setLayoutData(gd2);
	}

	public boolean evaluate() {
		return evaluate(getExpression());
	}

	public String getExpression() {
		return document.get();
	}

	public void setExpression(String expr) {
		document.set(expr);
	}

	public SourceViewer getViewer() {
		return viewer;
	}
}
