XOCL / MCore
---


Update Site: https://m-core.gitlab.io/mcore/release


# LDE Product Download Links

LDE for Windows [lde-win32.win32.x86_64.zip](https://m-core.gitlab.io/mcore/lde/releases/com.montages.lde-win32.win32.x86_64.zip)

LDE for MacOS [lde-macosx.cocoa.x86_64.zip](https://m-core.gitlab.io/mcore/lde/releases/com.montages.lde-macosx.cocoa.x86_64.zip)

